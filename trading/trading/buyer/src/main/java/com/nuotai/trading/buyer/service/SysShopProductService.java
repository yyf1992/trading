package com.nuotai.trading.buyer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.SysShopProductMapper;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 
 * @author zyn
 * 
 */

@Service
@Transactional
public class SysShopProductService {

	@Autowired
	private SysShopProductMapper sysShopProductMapper;

	public SysShopProduct get(String id) {
		return sysShopProductMapper.get(id);
	}

	public List<SysShopProduct> queryList(Map<String, Object> map) {
		return sysShopProductMapper.queryList(map);
	}

	public int queryCount(Map<String, Object> map) {
		return sysShopProductMapper.queryCount(map);
	}

	public void add(SysShopProduct sysShopProduct) {
		sysShopProductMapper.add(sysShopProduct);
	}

	public void update(SysShopProduct sysShopProduct) {
		sysShopProductMapper.update(sysShopProduct);
	}

	public void delete(String id) {
		sysShopProductMapper.delete(id);
	}

	public List<SysShopProduct> queryList(SearchPageUtil searchPageUtil) {
		return sysShopProductMapper.queryList(searchPageUtil);
	}

	public JSONObject saveSetPrice(String shopProductStr) {
		JSONObject json = new JSONObject();
		String[] shopProductArray = shopProductStr.split("@");
		for (String shopProduct : shopProductArray) {
			String[] shopArray = shopProduct.split("\\|");
			// 先判断重复数据
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("shopId", shopArray[0].trim());
			params.put("barcode", shopArray[5].trim());
			List<SysShopProduct> oldList = sysShopProductMapper.selectByMap(params);
			if (oldList != null && oldList.size() > 0) {
				SysShopProduct sus = new SysShopProduct();
				// 有重复数据就更新
				sus.setUpdateDate(new Date());
				sus.setSellPrice(new BigDecimal(shopArray[9].trim()));
				sus.setShopId(shopArray[0].trim());
				sus.setBarcode(shopArray[5].trim());
				int result=sysShopProductMapper.saveEditPrice(sus);
				if(result>0){
					continue;
				}else{
					json.put("success", false);
					json.put("msg", "出货价格设置失败！"); 
					return json;
				}
			}else{
				SysShopProduct sys = new SysShopProduct();
				// 没有重复数据
				sys.setId(ShiroUtils.getUid());
				sys.setCompanyId(ShiroUtils.getCompId());
				sys.setShopId(shopArray[0].trim());
				sys.setShopCode(shopArray[1].trim());
				sys.setShopName(shopArray[2].trim());
				sys.setProductName(shopArray[3].trim());
				sys.setProductCode(shopArray[4].trim());
				sys.setBarcode(shopArray[5].trim());
				sys.setSkuName(shopArray[6].trim());
				sys.setSkuCode(shopArray[7].trim());
				sys.setCostPrice(new BigDecimal(shopArray[8].trim()));
				sys.setSellPrice(new BigDecimal(shopArray[9].trim()));
				sys.setCreateDate(new Date());
				sys.setUpdateDate(new Date());
				int result=sysShopProductMapper.saveSetPrice(sys);
				if(result>0){
					continue;
				}else{
					json.put("success", false);
					json.put("msg", "出货价格设置失败！");
					return json;
				}
			}
		}
		json.put("success", true);
		json.put("msg", "出货价格设置成功！");
		return json;
	}
	
	public JSONObject insertOrUpdatePrice(List <SysShopProduct> shopList) {
		JSONObject json = new JSONObject();
		for (SysShopProduct sysShopProduct : shopList) {
			// 先判断重复数据
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("shopName", sysShopProduct.getShopName());
			params.put("barcode", sysShopProduct.getBarcode());
			List<SysShopProduct> oldList = sysShopProductMapper.selectByMap(params);
			if (oldList != null && oldList.size() > 0) {
				// 存在就更新
				sysShopProduct.setUpdateDate(new Date());
				int result=sysShopProductMapper.saveEditPrice(sysShopProduct);
				if(result>0){
					continue;
				}else{
					json.put("success", false);
					json.put("msg", "出货价格设置失败！"); 
					return json;
				}
			}
			else{
				// 不存在就插入
				sysShopProduct.setId(ShiroUtils.getUid());
				sysShopProduct.setCreateDate(new Date());
				sysShopProduct.setUpdateDate(new Date());
				int result=sysShopProductMapper.saveSetPrice(sysShopProduct);
				if(result>0){
					continue;
				}else{
					json.put("success", false);
					json.put("msg", "出货价格设置失败！");
					return json;
				}
			}
		}
		json.put("success", true);
		json.put("msg", "出货价格设置成功！");
		return json;
	}

	public SysShopProduct selectByPrimaryKey(String id) {
		return sysShopProductMapper.selectByPrimaryKey(id);
	}

	public JSONObject saveEditPrice(SysShopProduct sysShopProduct) {
		sysShopProduct.setUpdateDate(new Date());
		JSONObject json = new JSONObject();
		int result = 0;
		result = sysShopProductMapper.saveEditPrice(sysShopProduct);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "出货价格修改成功！");
		} else {
			json.put("success", false);
			json.put("msg", "出货价格修改失败！");
		}
		return json;
	}

	public List<SysShopProduct> selectByMap(Map<String, Object> params) {
		return sysShopProductMapper.selectByMap(params);
	}

}
