package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.SellerOrder;
import com.nuotai.trading.dao.BaseDao;

import java.util.Map;

/**
 * The interface B seller order mapper.
 *
 * @author "
 * @date 2017 -08-25 16:00:47
 */
public interface BSellerOrderMapper extends BaseDao<SellerOrder> {
    /**
     * Delete by primary key.
     *
     * @param id the id
     */
    void deleteByPrimaryKey(String id);

    /**
     * Gets by buyer order id.
     *
     * @param buyOrderId the order id
     * @return the by buyer order id
     */
    SellerOrder getByBuyerOrderId(String buyOrderId);

    /**
     * Update by primary key selective.
     *
     * @param soUpd the so upd
     */
    void updateByPrimaryKeySelective(SellerOrder soUpd);

    /**
     * Gets by order code.
     * 根据订单号查询订单信息
     * @param orderCode the order code
     * @return the by order code
     */
    SellerOrder getByOrderCode(String orderCode);

    //根据财务对账状态修改订单对账、付款状态
    int updateOrderBuyBill(Map<String, Object> params);
}
