package com.nuotai.trading.buyer.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class BuyOrderProduct {
    private String id;
    // 订单主键Id
    private String orderId;
    //訂單編號
    private String orderCode;
    //訂單日期（外部）
    private Date orderDate;
    // 计划单号
    private String applyCode;
    // 货号
    private String proCode;
    // 商品名称
    private String proName;
    // 规格代码
    private String skuCode;
    // 规格名称
    private String skuName;
    // 条形码
    private String skuOid;
    // 优惠金额
    private BigDecimal preferentialPrice;
    // 采购单价
    private BigDecimal price;
    private BigDecimal priceSum;
    // 数量
    private Integer goodsNumber;
    // 锁定数量
    private Integer lockGoodsNumber;
    //已发货数量
    private Integer deliveredNum;
    //到货数量
    private Integer arrivalNum;
    //采购未到货数量
    private Integer unArrivalNum;
    // 单位id
    private String unitId;
    private String unitName;
    // 仓库id
    private String wareHouseId;
    // 仓库id
    private String wareHouseName;
    // 修改价格
    private BigDecimal updatePrice;
    // 备注
    private String remark;
    //是否需要发票Y:是N:否
    private String isNeedInvoice;
    //店铺(外部)
    private String shopId;
    private String shopCode;
    private String shopName;
    // 供应商名称(外部)
    private String suppName;
    //要求到货日期（外部）
    private Date predictArred;
    //訂單提報人（外部）
    private String createName;
    //訂單狀態（外部）
    private String orderStatus;
    //订单备注
    private String orderRemark;
    private List<Map<String,Object>> deliveryList;
  //关联的供应商列表
    private List<Map<String,Object>> supplierLinksList;
}