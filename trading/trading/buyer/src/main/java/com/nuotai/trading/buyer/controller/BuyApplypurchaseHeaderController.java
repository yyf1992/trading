package com.nuotai.trading.buyer.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.nuotai.trading.buyer.model.BuyApplypurchaseHeader;
import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.buyer.model.OmsMonthSales;
import com.nuotai.trading.buyer.service.BuyApplypurchaseHeaderService;
import com.nuotai.trading.buyer.service.BuyApplypurchaseItemService;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.buyer.service.OmsMonthSalesService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.OutWhareaLogService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 
 * 店铺采购计划
 * @author dxl"
 * @date 2017-08-07 14:11:21
 */
@Controller
@RequestMapping("buyer/applyPurchaseHeader")
public class BuyApplypurchaseHeaderController extends BaseController{
	@Autowired
	private BuyApplypurchaseHeaderService applypurchaseHeaderService;
	@Autowired
	private BuyProductSkuService productSkuService;
	@Autowired
	private BuyApplypurchaseItemService applypurchaseItemService;
	@Autowired
	private BuyOrderService orderService;
	@Autowired
	private BuyAddressService shippingAddressService;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private BuyShopProductService shopProductService;
	@Autowired
	private OutWhareaLogService outWhareaLogService;
	@Autowired
	private OmsMonthSalesService omsMonthSalesService;
	
	/**
	 * 创建采购计划选择商品
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadAllProductList")
	public String loadInterflowProductList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        params.put("searchCompanyId", ShiroUtils.getCompId());
        //获取已经选择的原材料barcode
        searchPageUtil.setObject(params);
        List<BuyProductSku> productList = productSkuService.selectByPage(searchPageUtil);
        searchPageUtil.getPage().setList(productList);
        model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/addSalePlan/productList";
	}
	
	/**
	 * 创建采购计划保存
	 * @param user
	 * @param company
	 * @return
	 */
	@RequestMapping("saveAddPurchase")
	public void saveAddPurchase(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "buyer/applyPurchaseHeader/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "buyer/applyPurchaseHeader/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "buyer/applyPurchaseHeader/loadApplyPurchaseDetails?verifyDetails=1");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = applypurchaseHeaderService.saveAddPurchase(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = applypurchaseHeaderService.verifySuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = applypurchaseHeaderService.verifyError(id);
		return json.toString();
	}
	
	/**
	 * 店铺采购计划列表页面
	 * @param searchPageUtil
	 * @return
	 */
	@RequestMapping("applyPurchaseList")
	public String buyOrderList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "0");
		}
		//获得不同状态数量
		applypurchaseHeaderService.getPurchaseNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/planList/applyPurchaseList";
	}
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = applypurchaseHeaderService.loadData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 查看采购计划详情页面 
	 * @return
	 */
	@RequestMapping("/loadApplyPurchaseDetails")
	public String loadApplyPurchaseDetails(String id,String verifyDetails){
		BuyApplypurchaseHeader header = applypurchaseHeaderService.selectById(id);
		model.addAttribute("purchaseHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/planList/loadApplyPurchaseDetails";
	}
	
	/**
	 * 选择类目下拉
	 * @return
	 */
	/*@RequestMapping(method = RequestMethod.GET, value = "loadCategory")
	@ResponseBody
	public String loadCategory() {
		String result = "[]";
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("companyId", ShiroUtils.getCompId());
		List<BuyCategory> categoryList = categoryService.getAllCategory(map);
		if (categoryList != null && categoryList.size() > 0) {
			result = JSONArray.toJSONString(categoryList);
		}
		return result;
	}*/
	
	/**
	 * 采购下单页面
	 */
	@RequestMapping("purchaseProductList")
	public String purchaseProductList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "0");
		}
		//获得不同状态数量
		applypurchaseItemService.getProductTypeNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/productSum/purchaseProductList";
	}
	
	/**
	 * 采购下单页面数据
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "loadPurchaseProductListData", method = RequestMethod.GET)
	public String loadPurchaseProductListData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = applypurchaseItemService.loadPurchaseProductListData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	
	/**
	 * 加载采购明细页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadPurchaseDetails", method = RequestMethod.POST)
	public String loadPurchaseDetails(String barcode) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("barcode", barcode);
		List<Map<String,Object>> barcodeList = applypurchaseItemService.selectProductByBarcode(map);
		model.addAttribute("barcodeList", barcodeList);
		return "platform/buyer/applypurchase/productSum/purchaseDetails";
	}
	
	/**
	 * 通过商品条形码获取店铺采购情况
	 * @param barcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPurchaseItemList", method = RequestMethod.POST)
	@ResponseBody
	public String getPurchaseItemList(String barcode) throws Exception{
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("barcode", barcode);
		List<Map<String,Object>> barcodeList = applypurchaseItemService.selectProductByBarcode(map);
		jsonObject.put("list", barcodeList);
		return jsonObject.toString();
	}
	
	/**
	 * 批量生成采购订单操作
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("beatchCreateOrder")
	public String beatchCreateOrder(String idStr) throws Exception {
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("barcodeStr", idStr);
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		List<BuyApplypurchaseItem> apItemList = applypurchaseItemService.selectAllProductByBeatchCreateOrder(map);
		if(apItemList != null && apItemList.size() > 0 ){
			for(BuyApplypurchaseItem item : apItemList){
				map.clear();
				map.put("barcode", item.getBarcode());
				List<Map<String,Object>> barcodeList = applypurchaseItemService.selectProductByBarcode(map);
				item.setPurchaseList(barcodeList);
			}
		}
		model.addAttribute("apItemList", apItemList);
		return "platform/buyer/applypurchase/productSum/beatchCreateOrder";
	}
	
	/**
	 * 保存定制单价
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping("addCustomPrice")
	public String addCustomPrice(BuyApplypurchaseItem buyApplypurchaseItem,@RequestParam Map<String,String> params){
		int a = 0;
		String idStr = params.get("idStr");
		String[] s = idStr.split(",");
		String barcode = "";
		for (String id : s){
			id = id.replaceAll("'", "");
			if (!"undefined".equals(id)){
				if (buyApplypurchaseItem.getCustomizePrices() != null){
					buyApplypurchaseItem.setId(id);
					buyApplypurchaseItem.setIsCustomize(1);
					if (buyApplypurchaseItem.getCustomizeType() == 0){
						buyApplypurchaseItem.setAfterPrice(buyApplypurchaseItem.getPurchasePrices().add(buyApplypurchaseItem.getCustomizePrices()));
					}else{
						buyApplypurchaseItem.setCustomizePrices(
								buyApplypurchaseItem.getPurchasePrices().multiply(
										buyApplypurchaseItem.getCustomizePrices().multiply(new BigDecimal(0.01))
										) 
								);
						buyApplypurchaseItem.setAfterPrice(
								buyApplypurchaseItem.getPurchasePrices().add(
										buyApplypurchaseItem.getPurchasePrices().multiply(
												buyApplypurchaseItem.getCustomizePrices().multiply(new BigDecimal(0.01))
												) 
										)
								);
					}
					a = applypurchaseItemService.updateByPrimaryKeyAndBarcode(buyApplypurchaseItem);
				}else{
					a = applypurchaseItemService.updateByPrimaryKeyAndBarcode(buyApplypurchaseItem);
				}
				BuyApplypurchaseItem buyApplypurchaseItemUpdate = applypurchaseItemService.selectByPrimaryKey(id);
				if (buyApplypurchaseItemUpdate != null){
					barcode += "'"+buyApplypurchaseItemUpdate.getBarcode()+"',";
				}			
			}
			
		}	
		String barcodeStr = params.containsKey("barcodeStr")?params.get("barcodeStr").toString():"";
		if (barcodeStr != null){
			barcodeStr = barcodeStr.substring(0, barcodeStr.length()-1);
		}	
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("idStr", s[0]);
		map.put("companyId", ShiroUtils.getCompId());
		map.put("barcodeStr", barcodeStr);
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		//查询定制商品修改后的价格
		List<BuyApplypurchaseItem> apItemCustomList = applypurchaseItemService.selectAllProductByBeatchCreateOrderAndaddCustomPrice(map);
		if (!ObjectUtil.isEmpty(barcode)){
			String[] barcodeUpdate = barcode.split(",");
			String[] barcodeAll = barcodeStr.split(",");
			String barcodeNotU = "";
			for(String barcodeA : barcodeAll){
				barcodeA = barcodeA.replaceAll("'", "");
				for (String barcodeU : barcodeUpdate){
					barcodeU = barcodeU.replaceAll("'", "");
					if (!barcodeA.equals(barcodeU)){
						barcodeNotU += "'"+barcodeA+"',";
					}
				}
			}
			if (!ObjectUtil.isEmpty(barcodeNotU)){
			    barcodeNotU = barcodeNotU.substring(0, barcodeNotU.length()-1);			
				map.put("barcodeStr", barcodeNotU);
				List<BuyApplypurchaseItem> apItemList = applypurchaseItemService.selectAllProductByBeatchCreateOrder(map);
				apItemCustomList.addAll(0, apItemList);
			}		
		}	
		
		model.addAttribute("apItemList", apItemCustomList);
		model.addAttribute("idStr", idStr);
		return "platform/buyer/applypurchase/productSum/beatchCreateOrderCustom";
	}
	
	/**
	 * 批量生成采购订单操作，下一步
	 * @param params
	 * @return
	 */
	@RequestMapping("next")
	public String next(@RequestParam Map<String,String> params){
		JSONObject json = new JSONObject();
		JSONArray itemArray = new JSONArray();
		String infoStr = params.get("infoStr");
		String[] s = infoStr.split("@");
		Map<String, String> map = orderService.parseToMap(s);
		for(Map.Entry<String, String> entry : map.entrySet()){
			System.out.print("Key = "+entry.getKey()+",value="+entry.getValue());
			String value = entry.getValue().substring(1,entry.getValue().length() - 1);
		    String[] supplier = value.split(".NTNT.");
			JSONObject itemJson = new JSONObject();
			itemJson.put("supplierId", entry.getKey());
			itemJson.put("supplierName", supplier[2]);
			itemJson.put("person", supplier[0]);
			itemJson.put("phone", supplier[1]);
			String[] shop = value.split("@");
			
			JSONArray spArray = new JSONArray();
		    for(int i = 0;i < shop.length;i++){
		    	JSONObject shopJson = new JSONObject();
		    	String item = shop[i].substring(shop[i].indexOf("{"),shop[i].length());
				net.sf.json.JSONObject jsonItem = net.sf.json.JSONObject.fromObject(item);
			    net.sf.json.JSONArray shopArray = jsonItem.getJSONArray("shopList");
			    for(int j = 0 ; j < shopArray.size(); j++){
			    	net.sf.json.JSONObject jsonShop = shopArray.getJSONObject(j);
			    	shopJson.put("applyCode", jsonShop.get("applyCode").toString());
			    	//shopJson.put("shopId", jsonShop.get("shopId").toString());
			    	shopJson.put("goodsNumber", jsonShop.get("goodsNumber").toString());
			    	shopJson.put("productCode", jsonShop.get("productCode").toString());
			    	shopJson.put("productName", jsonShop.get("productName").toString());
			    	shopJson.put("skuCode", jsonShop.get("skuCode").toString());
			    	shopJson.put("skuName", jsonShop.get("skuName").toString());
			    	shopJson.put("skuOid", jsonShop.get("skuOid").toString());
			    	shopJson.put("unitId", jsonShop.get("unitId").toString());
			    	shopJson.put("price", jsonShop.get("price").toString());
			    	shopJson.put("invoice", jsonShop.get("invoice").toString());
			    	shopJson.put("warehouseId", jsonShop.get("warehouseId").toString());
			    	shopJson.put("remark", jsonShop.get("remark").toString());
			    	if (jsonShop.get("afterPrice") != null && jsonShop.get("afterPrice").toString() != "undefined"){
			    		shopJson.put("afterPrice", jsonShop.get("afterPrice").toString());
			    	}else{
			    		shopJson.put("afterPrice", null);
			    	}
			    	
			    	if (jsonShop.get("isCustomize") != null){
			    		shopJson.put("isCustomize", jsonShop.get("isCustomize").toString());
			    	}else{
			    		shopJson.put("isCustomize", -1);
			    	}
			    	if (jsonShop.get("customizePrices") != null){
			    		shopJson.put("customizePrices", jsonShop.get("customizePrices").toString());
			    	}
			    	if (jsonShop.get("predictArred") != null){
			    		shopJson.put("predictArred", jsonShop.get("predictArred").toString());
			    	}
			    	String jsonString = JSON.toJSONString(shopJson, SerializerFeature.DisableCircularReferenceDetect);
			    	spArray.add(JSONObject.parse(jsonString));
			    }
		    }
			itemJson.put("shopArray", spArray);
			itemArray.add(itemJson);
		}
		json.put("itemArray", itemArray);
		
		//查询收获地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue());
		addressParams.put("compId", ShiroUtils.getCompId());
		List<BuyAddress> addressList = shippingAddressService.selectByMap(addressParams);
		model.addAttribute("addressList", addressList);
		model.addAttribute("nextData", json);
		return "platform/buyer/applypurchase/productSum/nextOrder";
	}
	
	/**
	 * 批量生成采购订单保存
	 */
	@RequestMapping("saveBeatchCreateOrder")
	public void saveBeatchCreateOrder(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/purchase/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/purchase/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "platform/buyer/purchase/verifyDetail");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = orderService.saveBeatchCreateOrder(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 修改采购计划保存
	 * @param user
	 * @param company
	 * @return
	 */
	@RequestMapping("saveUpdatePurchase")
	public void saveUpdatePurchase(@RequestParam Map<String,Object> params){
		updatePurchase(params.get("id").toString(), params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "buyer/applyPurchaseHeader/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "buyer/applyPurchaseHeader/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "buyer/applyPurchaseHeader/loadApplyPurchaseDetails?verifyDetails=1");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = applypurchaseHeaderService.saveUpdatePurchase(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 选择商品后显示数据（调用oms接口）
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("productInterfaceQuerying")
	@ResponseBody
	public String productInterfaceQuerying(@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		String barcode = (String) (map.get("skuoid") == null ? "" : map.get("skuoid").toString().replaceAll("\\s*", ""));
		List<OmsMonthSales> monthSalesList = omsMonthSalesService.selectByBarcode(barcode);
		if(monthSalesList != null && monthSalesList.size() > 0){
			int totalMonthDms = 0;
			int totalnum = 0;
			int dayNum = 0;
			for(OmsMonthSales monthSales : monthSalesList){
				int num = monthSales.getNum();
				totalnum = totalnum + num;
				dayNum = Integer.parseInt(monthSales.getEndDate().substring(monthSales.getEndDate().length() - 2));//计算日均销量要除以的天数
			}
			double a = totalnum;
			double b = dayNum;
			double i = a/b;
			totalMonthDms = totalMonthDms + new BigDecimal(i+"").setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
			json.put("monthSaleNum", totalnum);//本月销量
			json.put("monthDms", totalMonthDms);//本月日均销量(四舍五入取整)
		}else{
			json.put("monthSaleNum", 0);//本月销量
			json.put("monthDms", 0);//本月日均销量
		}
		//杉橙库存数查询
		
		String levelUrl = Constant.OMS_INTERFACE_URL + "getGoods?skuoid=" + barcode;
		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		if(arrayString != null){
			net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(arrayString);
			net.sf.json.JSONArray productArray = net.sf.json.JSONArray.fromObject(jsonObject.get("list"));

			List<net.sf.json.JSONObject> list = (List<net.sf.json.JSONObject>) net.sf.json.JSONArray
					.toCollection(productArray, net.sf.json.JSONObject.class);
			if(list != null && list.size() > 0){
				net.sf.json.JSONObject stockObject = list.get(0);
				int stock = stockObject.getInt("warncount");
				json.put("warncount", stock);
			}else{
				json.put("warncount", 0);
			}
		}else{
			json.put("warncount", 0);
		}
		//在途订单数量查询（采购系统未到货数+买卖系统未到货数（订单数-到货数））
		//1采购系统未到货数
		/*String cgLevelUrl = Constant.PURCHASE_INTERFACE_URL +"getProductUnArrivalList?skuOid=" + barcode;
		
		String cgArrayString = InterfaceUtil.searchLoginService(cgLevelUrl);
		net.sf.json.JSONArray cgProductArray = net.sf.json.JSONArray.fromObject(cgArrayString);
		
		List<Map<String,Object>> cgList = (List<Map<String,Object>>) net.sf.json.JSONArray
				.toCollection(cgProductArray, HashMap.class);*/
		int unArrivalNumSum = 0;
		/*if(cgList!= null && cgList.size() > 0){
			for(Map<String,Object> mapResult : cgList){
				int unArrivalNum = 0;
				if(mapResult.get("arrivalcount") != null && !"".equals(mapResult.get("arrivalcount").toString())){
					unArrivalNum = Integer.parseInt(mapResult.get("realcount").toString()) 
								- Integer.parseInt(mapResult.get("arrivalcount").toString());
				}else{
					unArrivalNum = Integer.parseInt(mapResult.get("realcount").toString());
				}
				unArrivalNumSum = unArrivalNumSum + unArrivalNum;
			}
		}else{
			unArrivalNumSum = 0;
		}*/
		
		//2买卖系统未到货数
		Map<String,Object> mapParem = new HashMap<String, Object>();
		mapParem.put("companyId", ShiroUtils.getCompId());
		mapParem.put("skuOid", barcode);
		String unArrivalNum2 = orderProductService.selectOrderUnArrivalNumByBarcode(mapParem);
		if(unArrivalNum2 == null){
			unArrivalNum2 = "0";
		}
		json.put("transitNum", unArrivalNumSum + Integer.parseInt(unArrivalNum2));//在途订单数量
		// 外仓数量查询
		Map<String,Object> mapOWL = new HashMap<String,Object>();
		mapOWL.put("companyId", ShiroUtils.getCompId());
		mapOWL.put("barcode", barcode);
		List<OutWhareaLog> outWhareaLogList = outWhareaLogService.queryByMap(mapOWL);
		int outWhareaNum = 0;
		if(outWhareaLogList != null && outWhareaLogList.size() > 0){
			for(OutWhareaLog owl : outWhareaLogList){
				outWhareaNum = outWhareaNum + owl.getOutWhareaStock() + owl.getOutWhareaWayStock();
			}
		}
		json.put("outWhareaNum", outWhareaNum);//外仓数量
		
		return json.toJSONString();
	}
	
	/**
	 * 根据供应商查询商品采购单价
	 */
	@RequestMapping("getProductPriceBySupplier")
	@ResponseBody
	public String getProductPriceBySupplier(@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("linktype", "0");
		map.put("searchCompanyId", Constant.COMPANY_ID);
		List<Map<String,Object>> list = shopProductService.loadProductLinksByMap(map);
		if(list != null && list.size() > 0){
			Map<String,Object> mapResult = list.get(0);
			String price = mapResult.get("price") == null ? "" : mapResult.get("price").toString();
			json.put("price", price);//采购单价
		}else{
			json.put("price", 0);//采购单价
		}
		return json.toJSONString();
	}
	
	/**
	 * 取消采购计划
	 * @param purchaseId
	 * @return
	 */
	@RequestMapping(value = "/saveCancelPurchase", method = RequestMethod.POST)
	@ResponseBody
	public String saveCancelPurchase(String purchaseId){
		JSONObject json = new JSONObject();
		try {
			applypurchaseHeaderService.saveCancelPurchase(purchaseId);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 采购计划审批-待我审批首页
	 * @return
	 */
	@RequestMapping("approvedHtml")
	public String approvedHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/approved/approvedList";
	}
	
	/**
	 * 采购计划审批-待我审批数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("approvedData")
	public String approvedData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		applypurchaseHeaderService.loadApprovedData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/approved/approvedData";
	}
	
	/**
	 * 采购计划审批一键通过
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyAll")     
	@ResponseBody
	public String verifyAll(String id){
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(id)){
			String[] idStr = id.split(",");
			for(String orderId : idStr){
				json = applypurchaseHeaderService.verifyAll(orderId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json.toJSONString();
	}
	
	/**
	 * 采购计划审批加载采购明细页面
	 * @return
	 */
	@RequestMapping("/approvedDataDetails")
	public String approvedDataDetails(String id,String verifyDetails){
		BuyApplypurchaseHeader header = applypurchaseHeaderService.selectById(id);
		model.addAttribute("purchaseHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/approved/approvedDataDetails";
	}
}
