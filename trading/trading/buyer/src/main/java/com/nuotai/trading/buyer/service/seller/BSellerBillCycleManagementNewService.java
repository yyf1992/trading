package com.nuotai.trading.buyer.service.seller;

import com.nuotai.trading.buyer.dao.seller.BSellerBillCycleManagementNewMapper;
import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagementNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BSellerBillCycleManagementNewService {

    private static final Logger LOG = LoggerFactory.getLogger(BSellerBillCycleManagementNewService.class);

	@Autowired
	private BSellerBillCycleManagementNewMapper bSellerBillCycleManagementNewMapper;
	
	public BSellerBillCycleManagementNew get(String id){
		return bSellerBillCycleManagementNewMapper.get(id);
	}
	
	public List<BSellerBillCycleManagementNew> queryList(Map<String, Object> map){
		return bSellerBillCycleManagementNewMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return bSellerBillCycleManagementNewMapper.queryCount(map);
	}

	//同步添加账单申请信息到卖家
	public void addNewSellerBillCycle(BSellerBillCycleManagementNew bSellerBillCycleManagementNew){
		bSellerBillCycleManagementNewMapper.addNewSellerBillCycle(bSellerBillCycleManagementNew);
	}
	
	public void update(BSellerBillCycleManagementNew bSellerBillCycleManagementNew){
		bSellerBillCycleManagementNewMapper.update(bSellerBillCycleManagementNew);
	}
	
	public void delete(String id){
		bSellerBillCycleManagementNewMapper.delete(id);
	}
	

}
