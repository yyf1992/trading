package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagementNew;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @YYF "
 * @date 2017-09-13 16:01:52
 */
@Component
public interface BSellerBillCycleManagementNewMapper extends BaseDao<BSellerBillCycleManagementNew> {
	//同步申请数据到卖家
    int addNewSellerBillCycle(BSellerBillCycleManagementNew bSellerBillCycleManagementNew);
}
