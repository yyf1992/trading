package com.nuotai.trading.buyer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuySalePlanHeader;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.buyer.service.BuySalePlanHeaderService;
import com.nuotai.trading.buyer.service.BuySalePlanItemService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 
 * 销售计划
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
@Controller
@RequestMapping("buyer/salePlan")
public class BuySalePlanHeaderController extends BaseController {
	@Autowired
	private SysShopService shopService;
	@Autowired
	private BuySalePlanHeaderService salePlanHeaderService;
	@Autowired
	private BuySalePlanItemService salePlanItemService;
	
	/**
	 * 提报销售计划
	 * @return
	 */
	@RequestMapping("addSalePlan")
	public String addSalePlan(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId",ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		List<SysShop> shopList = shopService.selectByMap(map);
		model.addAttribute("shopList", net.sf.json.JSONArray.fromObject(shopList));
		model.addAttribute("shopList2", shopList);
		return "platform/buyer/applypurchase/addSalePlan/addSalePlan";
	}
	
	/**
	 * 提报销售计划保存
	 * @param user
	 * @param company
	 * @return
	 */
	@RequestMapping("saveAddSalePlan")
	public void saveAddPurchase(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "buyer/salePlan/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "buyer/salePlan/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "buyer/salePlan/loadSalePlanDetails?verifyDetails=1");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = salePlanHeaderService.saveAddSalePlan(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = salePlanHeaderService.verifySuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = salePlanHeaderService.verifyError(id);
		return json.toString();
	}
	
	/**
	 * 销售计划列表展示
	 */
	@RequestMapping("salePlanList")
	public String salePlanList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
        if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "0");
		}
		//获得不同状态数量
		salePlanHeaderService.getSalePlanNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanList/salePlanList";
	}
	
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = salePlanHeaderService.loadData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 审批下单页面
	 */
	@RequestMapping("loadVerifyCreateOrder")
	public String loadVerifyCreateOrder(String idStr){
		if (idStr != null && !"".equals(idStr)) {
			String[] ids = idStr.split(",");
			List<BuySalePlanItem> productList = salePlanItemService.getProductGroupByBarcode(ids);
			Map<String,Object> map = new HashMap<String, Object>();  
			map.put("idStr", ids);
			if(productList != null && productList.size() > 0){
				for(BuySalePlanItem item : productList){
					map.put("barcode", item.getBarcode());
					List<Map<String,Object>> shopItemList = salePlanItemService.getProductGroupByMap(map);
					item.setShopItemList(shopItemList);
				}
			}
			model.addAttribute("productList", productList);
		}
		return "platform/buyer/applypurchase/salePlanList/verifyCreateOrder";
	}
	
	/**
	 * 查看销售计划详情页面 
	 * @return
	 */
	@RequestMapping("/loadSalePlanDetails")
	public String loadApplyPurchaseDetails(String id,String verifyDetails){
		BuySalePlanHeader header = salePlanHeaderService.get(id);
		model.addAttribute("salePlanHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/salePlanList/loadSalePlanDetails";
	}
	
	
	/**
	 * 生成采购计划-销售计划商品汇总列表页面
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadSalePlanProductSum", method = RequestMethod.GET)
	public String loadSalePlanProductSum(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        params.put("companyId", ShiroUtils.getCompId());
        params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
        searchPageUtil.setObject(params);
        List<Map<String,Object>> productList = salePlanItemService.selectSalePlanProductSumByPage(searchPageUtil);
        searchPageUtil.getPage().setList(productList);
        model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanProductSum/productSumList";
	}
	
	/**
	 * 加载销售计划明细页面-部门列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadSalePlanShopList", method = RequestMethod.POST)
	public String loadSalePlanShopList(String barcode) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("barcode", barcode);
		List<Map<String,Object>> shopList = salePlanItemService.selectSalePlanShopListByBarcode(map);
		model.addAttribute("shopList", shopList);
		return "platform/buyer/applypurchase/salePlanProductSum/shopListDetails";
	}
	
	/**
	 * 批量生成采购计划操作
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("beatchCreatePurchasePlan")
	public String beatchCreatePurchasePlan(String idStr) throws Exception {
		if (idStr != null && !"".equals(idStr)) {
			String[] ids = idStr.split(",");
			List<BuySalePlanItem> productList = salePlanItemService.getProductGroupByBarcode(ids);
			Map<String,Object> map = new HashMap<String, Object>();  
			//map.put("idStr", ids);
			if(productList != null && productList.size() > 0){
				for(BuySalePlanItem item : productList){
					map.put("barcode", item.getBarcode());
					List<Map<String,Object>> shopList = salePlanItemService.selectSalePlanShopListByBarcode(map);
					if(shopList != null && shopList.size() > 0){
						item.setShopItemList(shopList);
					}
				}
			}
			model.addAttribute("productList", productList);
		}
		return "platform/buyer/applypurchase/salePlanList/verifyCreateOrder";
	}
	
	/**
	 * 销售计划审批-待我审批首页
	 * @return
	 */
	@RequestMapping("approvedHtml")
	public String approvedHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("companyId",ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		//获取部门的名称
		List<SysShop> shopList = shopService.selectByMap(params);
		model.addAttribute("shopList", shopList);
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanList";
	}
	
	/**
	 * 销售计划审批-待我审批数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("approvedData")
	public String approvedData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		salePlanHeaderService.loadApprovedData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanData";
	}
	
	/**
	 * 销售计划审批-查看销售计划详情页面 
	 * @return
	 */
	@RequestMapping("approvedDetails")
	public String approvedDetails(String id,String verifyDetails){
		BuySalePlanHeader header = salePlanHeaderService.get(id);
		model.addAttribute("salePlanHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanDataDetails";
	}
	
	/**
	 * 一键通过
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyAll")     
	@ResponseBody
	public String verifyAll(String id){
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(id)){
			String[] idStr = id.split(",");
			for(String orderId : idStr){
				json = salePlanHeaderService.verifyAll(orderId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json.toJSONString();
	}
}
