package com.nuotai.trading.buyer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.dao.BuyBillInterestNewMapper;
import com.nuotai.trading.buyer.model.BuyBillInterestNew;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillInterestNewService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillInterestNewService.class);

	@Autowired
	private BuyBillInterestNewMapper buyBillInterestNewMapper;

	//根据申请周期编号查询关联利息
	public List<BuyBillInterestNew> queryNewInterest(String id){
		return buyBillInterestNewMapper.queryNewInterest(id);
	}
	
	public List<BuyBillInterestNew> queryList(String id){
		return buyBillInterestNewMapper.queryList(id);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillInterestNewMapper.queryCount(map);
	}

	//添加申请关联利息
	public void addNewInterest(BuyBillInterestNew buyBillInterestNew){
		buyBillInterestNewMapper.addNewInterest(buyBillInterestNew);
	}
	
	public void update(BuyBillInterestNew buyBillInterestNew){
		buyBillInterestNewMapper.update(buyBillInterestNew);
	}
	
	public void delete(String id){
		buyBillInterestNewMapper.delete(id);
	}
	

}
