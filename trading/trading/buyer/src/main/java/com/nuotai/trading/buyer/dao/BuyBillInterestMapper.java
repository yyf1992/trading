package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillInterest;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("buyBillInterestMapper")
public interface BuyBillInterestMapper extends BaseDao<BuyBillInterest>{
	//添加账单周期关联利息数据
	int saveInterestInfo(Map<String, Object> map);
	
	//根据账单周期查询关联利息信息
	List<BuyBillInterest> getBillInteresInfo(String billCycleId);
	
	//根据账单周期编号删除关联利息
    int deleteByPrimaryKey(String billCycleId);

    int insert(BuyBillInterest record);

    int insertSelective(BuyBillInterest record);

    BuyBillInterest selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyBillInterest record);

    int updateByPrimaryKey(BuyBillInterest record);
}