package com.nuotai.trading.buyer.model.seller;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
@Data
public class BSellerCustomerItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后id
	private String customerId;
	//采购计划单号
	private String applyCode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String skuOid;
	//单位
	private String unitId;
	//发货号
	private String deliveryCode;
	//发货明细id
	private String deliveryItemId;
	//售后类型  0-换货，1-退款退货
	private String type;
	//转换类型[0-未转换, 1-换货转退货]
	private String transformType;
	//换货/退款退货数量
	private Integer goodsNumber;
	//换货到货数量
	private Integer exchangeArrivalNum;
	//退货数量
	private Integer returnNum;
	//价格
	private BigDecimal price;
	//收货数量
	private Integer receiveNumber;
	//仓库id
	private String wareHouseId;
	//是否索要发票
	private String isNeedInvoice;
	//备注
	private String remark;
	//返修金额
	private BigDecimal repairPrice;
	//卖家确认数量
	private Integer confirmNumber;
	//买家itemid
	private String buyCustomerItemId;
	//换货单价
	private BigDecimal exchangePrice;
}
