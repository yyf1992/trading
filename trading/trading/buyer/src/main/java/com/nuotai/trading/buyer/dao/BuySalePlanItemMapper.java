package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
public interface BuySalePlanItemMapper extends BaseDao<BuySalePlanItem> {
	List<BuySalePlanItem> selectByHeaderId(String headerId);
	
	/**
	 * 得到商品分组
	 * @param idStr
	 * @return
	 */
	List<BuySalePlanItem> getProductGroupByBarcode(@Param("idStr") String[] idStr);
	
	/**
	 * 审批下单根据条形码获取店铺信息
	 * @param map
	 * @return
	 */
	List<Map<String,Object>> getProductGroupByMap(Map<String, Object> map);
	
	List<Map<String,Object>> selectSalePlanProductSumByPage(SearchPageUtil searchPageUtil);
	
	List<Map<String,Object>> selectSalePlanShopListByBarcode(Map<String,Object> map);
	
	List<BuySalePlanItem> selectSalePlanShopListByMap(Map<String,Object> map);
	
	void updateByBuySalePlanItem(BuySalePlanItem buySalePlanItem);
	
	BuySalePlanItem selectLastSalesCompletionRateListByMap(Map<String,Object> map);
}
