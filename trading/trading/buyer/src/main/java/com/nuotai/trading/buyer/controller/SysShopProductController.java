package com.nuotai.trading.buyer.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysPurchasePrice;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.buyer.service.SysPurchasePriceService;
import com.nuotai.trading.buyer.service.SysShopProductService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


/**
 * @author zyn
 * @date 2017-08-17 17:09:13
 */
@Controller
@RequestMapping("platform/buyer/sysshopproduct")
public class SysShopProductController extends BaseController{
	@Autowired
	private SysShopProductService sysShopProductService;
	@Autowired
	private SysShopService sysShopService;
	@Autowired
	private BuyProductSkuService buyProductSkuService;
	@Autowired
	private SysPurchasePriceService sysPurchasePriceService;
	
	/**
	 * 商品出货价格列表
	 */
	@RequestMapping(value="/loadGoodsPriceList")
	public String list(SearchPageUtil searchPageUtil, @RequestParam Map<String, Object> param){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
			param.put("shopId", "-1");
		}
		param.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		//查询列表数据
		List<SysShopProduct> sysShopProductList = sysShopProductService.queryList(searchPageUtil);
		searchPageUtil.getPage().setList(sysShopProductList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		List<Map<String, Object>> sysShopList=sysShopService.selectByCompanyId(ShiroUtils.getCompId());
		model.addAttribute("sysShopList",sysShopList);
		model.addAttribute("param",param);
		return "platform/buyer/sysShopProduct/sysShopProductList";
	}
	
	/**
	 * 加载设置商品价格页面 
	 */
	@RequestMapping("/loadAddShopHtml")
	public String loadAddShopHtml(){
		List<SysShop> sysShopList=sysShopService.selectByCompanyId(ShiroUtils.getCompId());
		model.addAttribute("sysShopList", sysShopList);
		return "platform/buyer/sysShopProduct/addShop";
	}
	
	/**
	 * 加载商品
	 * @param params
	 * @return
	 */
	@RequestMapping("/selectGoods")
	public String selectGoods(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("searchCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List <BuyProductSku> skuList=buyProductSkuService.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(skuList);
		return "platform/buyer/sysShopProduct/skuList";
	}
	
	/**
	 * 保存出货价格设置
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/saveSetPrice",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveSetPrice(String shopProductStr){
		JSONObject json=sysShopProductService.saveSetPrice(shopProductStr);
		return json;
	} 

	/**
	 * 加载修改页面 
	 */
	@RequestMapping(value="/loadEditPrice",method=RequestMethod.POST)
	public String loadEditPrice(String id){
		SysShopProduct sysShopProduct=sysShopProductService.selectByPrimaryKey(id);
		model.addAttribute("SysShopProduct",sysShopProduct);
		return "platform/buyer/sysShopProduct/editPrice";
	}
	
	/**
	 * 保存修改出货价格
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/saveEditPrice",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject editPrice(SysShopProduct sysShopProduct){
		JSONObject json=sysShopProductService.saveEditPrice(sysShopProduct);
		return json;
	} 
	
	/**
	 * 加载商品价格提升方式列表
	 */
	@RequestMapping("/loadProductPurchasePriceList")
	public String loadProductPurchasePriceList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(map);
		//查询列表数据
		List<SysPurchasePrice> sysPurchasePricesList = sysPurchasePriceService.queryList(searchPageUtil);
		searchPageUtil.getPage().setList(sysPurchasePricesList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/buyer/sysShopProduct/priceIncMode";
	}
	
	/**
	 * 加载设置价格提升方式界面
	 * @return
	 */
	@RequestMapping(value="/loadSetPriceStatusHtml")
	public String loadSetPriceStatusHtml(){
		return "platform/buyer/sysShopProduct/setPriceStatus";
	}
	
	/**
	 * 加载商品列表
	 * @param params
	 * @param searchPageUtil
	 * @return
	 */
	@RequestMapping(value="/loadProductData")
	public String loadProductData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		params.put("companyId", ShiroUtils.getCompId());
		//页面上已经选择的商品
		String barcodeStr=params.containsKey("barcodeStr")?params.get("barcodeStr").toString():"";
		if(barcodeStr.length()>0){
			String[] barcodeArr=barcodeStr.split(",");
			params.put("barcodeArr",barcodeArr);
		}
		//数据库已经设置的商品
		List<String>  exitList=sysPurchasePriceService.selectExitProductId(ShiroUtils.getCompId());
		params.put("exitList",exitList);
		searchPageUtil.setObject(params);
		List<Map<String,Object>> productList =buyProductSkuService.inputSelect(searchPageUtil);
		searchPageUtil.getPage().setList(productList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/sysShopProduct/productList";
	}
	
	/**
	 * 保存价格提升方式设置
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/submitLatitude",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject submitLatitude(String barcodeStr){
		JSONObject json=sysPurchasePriceService.insertPurchaseLatitude(barcodeStr);
		return json;
	}
	
	/**
	 * 加载修改价格提升方式界面
	 */
	@RequestMapping(value="/loadEditPriceHtml",method=RequestMethod.POST)
	public String loadEditPriceHtml(String productId){
		Map<String,String> map =new HashedMap<>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("productId", productId);
		List<SysPurchasePrice> purchase=sysPurchasePriceService.selectByMap(map);
		model.addAttribute("purchase",purchase);
		return "platform/buyer/sysShopProduct/editPurchasePrice";
	}
	
	@RequestMapping(value="/saveEditPurchasePrice",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveEditPurchasePrice(SysPurchasePrice purchase){
		JSONObject json=sysPurchasePriceService.updatePurchase(purchase);
		return json;
	}
	
	@RequestMapping(value="/deletePurchasePrice",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject deletePurchasePrice(String productId){
		JSONObject json=sysPurchasePriceService.deletePurchase(productId);
		return json;
	}
	
	/**
	 * 加载导入数据页面
	 * @return
	 */
	@RequestMapping(value="/loadImportExcelHtml",method=RequestMethod.POST)
	public String loadImportExcelHtml(){
		return "platform/buyer/sysShopProduct/importExcel";
	}
	
	/**
	 * 导入出货价格
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/importShopPriceData",method=RequestMethod.POST)
	@ResponseBody
	public String importShopPriceData(@RequestParam(required = false) MultipartFile excel)
			throws Exception {
		JSONObject json = new JSONObject();
		if (excel != null) {
			String fileName = excel.getOriginalFilename();// 获取文件名
			String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
			InputStream is = excel.getInputStream();
			if ("xls".equals(ext) || "xlsx".equals(ext)) {// 判断文件格式
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is)
						: new XSSFWorkbook(is);
				Sheet sheet = workbook.getSheetAt(0);//获得第一个表单 
				List<SysShopProduct> excelData = new ArrayList<SysShopProduct>();
				int rowNum = sheet.getLastRowNum();// 获取excel行数
				for (int i = 1; i <= rowNum; i++) {
					try {
						Row row = sheet.getRow(i);
						if (!ExcelUtil.isBlankRow(row)) {
							if (row.getCell(0).getStringCellValue() != null && row.getCell(0).getStringCellValue()!= "") {
								//获取店铺名称
								String shopnameStr = row.getCell(0).getStringCellValue();
								String  productCode="";//货号
								String  productName="";//商品名称
								String  barcode="";//条形码
								productCode = row.getCell(1).getStringCellValue();
								productName = row.getCell(2).getStringCellValue();
								try {
									barcode = row.getCell(3).getStringCellValue();
								} catch (Exception e) {
									DecimalFormat df = new DecimalFormat("0");
									String contents = df.format(row.getCell(3).getNumericCellValue());
									barcode = contents;
								}
								BigDecimal sellPrice = new BigDecimal(row.getCell(4).getNumericCellValue());//出货价
								String[] shopnameArray = shopnameStr.split("@");
								for(String shopname : shopnameArray){
									SysShopProduct shopPro=new SysShopProduct();
									shopPro.setProductCode(productCode);
									shopPro.setProductName(productName);
									shopPro.setShopName(shopname);
									shopPro.setBarcode(barcode);
									shopPro.setSellPrice(sellPrice);
									shopPro.setCompanyId(ShiroUtils.getCompId());
									excelData.add(shopPro);
								}
							}
						}
					} catch (Exception e) {
						json.put("success", false);
						json.put("msg", e.getMessage());
						e.printStackTrace();
						return json.toString();
					}
				}
					json=sysShopProductService.insertOrUpdatePrice(excelData);
			} else {
				json.put("success", false);
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("success", false);
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	

	/**
	 * 下载模板
	 * @param response
	 * @param request
	 */
	@RequestMapping("/downloadExcel")
	public void downloadExcel(HttpServletResponse response,HttpServletRequest request) {
        try {
            //获取文件的路径
            String excelPath = request.getSession().getServletContext().getRealPath("statics/file/"+"店铺出货价格设置导入模板.xlsx");
            //String fileName = "采购价格设置模板.xlsx".toString(); // 文件的默认保存名
            // 读到流中
            InputStream inStream = new FileInputStream(excelPath);//文件的存放路径
            // 设置输出的格式
            response.reset();
            response.setContentType("bin");
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode("店铺出货价格设置导入模板.xlsx", "UTF-8"));
            OutputStream os = response.getOutputStream();
            // 循环取出流中的数据
            byte[] b = new byte[200];
            int len;

            while ((len = inStream.read(b)) > 0){
            	os.write(b, 0, len);
            }
            // 这里主要关闭。
            os.close();
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
