package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author zyn 
 * @date 2017-08-17 17:09:13
 */
@Data
public class SysShopProduct implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司id
	private String companyId;
	//店铺id
	private String shopId;
	//店铺代码
	private String shopCode;
	//店铺名称
	private String shopName;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//成本价
	private BigDecimal costPrice;
	//出货价
	private BigDecimal sellPrice;
	//创建时间
	private Date createDate;
	//修改时间
	private Date updateDate;
	//删除时间
	private Date delDate;
}
