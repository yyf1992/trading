package com.nuotai.trading.buyer.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysShopDeliveryData;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.buyer.service.SysShopDeliveryDataService;
import com.nuotai.trading.buyer.service.SysShopProductService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



/**
 * 发货取数店铺表
 * 
 * @author "
 * @date 2017-09-25 13:24:06
 */
@Controller
@RequestMapping("buyer/sysshopdeliverydata")
public class SysShopDeliveryDataController extends BaseController{
	@Autowired
	private SysShopDeliveryDataService sysShopDeliveryDataService;
	@Autowired
	private SysShopService sysShopService;
	@Autowired
	private SysShopProductService sysShopProductService;
	
	/**
	 * 发货取数店铺管理--发货取数店铺界面加载
	 */
	@RequestMapping("/loadshopDateManageHtml")
	public String loadshopDateManage(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> param){
	   if(searchPageUtil.getPage()==null){
		   searchPageUtil.setPage(new Page());
	   }
	   param.put("companyId", ShiroUtils.getCompId());
	   searchPageUtil.setObject(param);
	   List<Map<String, Object>> shopList=sysShopDeliveryDataService.selectByCompanyId(searchPageUtil);
	   searchPageUtil.getPage().setList(shopList);
	   return "platform/buyer/sysShopProduct/shopDateManage";
	}
	
	/**
	 * 发货取数店铺管理--添加店铺界面
	 */
	@RequestMapping("/loadInsertShopHtml")
	public String loadInsertShopHtml(){
		List<SysShop> shopList=sysShopService.selectByCompanyId(ShiroUtils.getCompId());
		model.addAttribute("shopList",shopList);
		return "platform/buyer/sysShopProduct/insertShop";
	}
	
	/**
	 * 发货取数店铺管理--保存添加店铺
	 */
	@RequestMapping(value="/saveInsertShop",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveInsertShop(String shopId){
		JSONObject json=new JSONObject();
		if("-1".equals(shopId)){
			json.put("success", false);
			json.put("msg", "您还没有选择店铺！");
			return json;
		}else{
			json=sysShopDeliveryDataService.saveInsertShop(shopId);
		}
		return json;
	}
	
	/**
	 * 发货取数店铺管理--删除店铺
	 */
	@RequestMapping(value="/deleteShop",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject deleteShop(String id){
		JSONObject json=sysShopDeliveryDataService.deleteShop(id);
		return json;
	}

	/**
	 * 加载店铺发货统计数据
	 * 
	 * @param searchPageUtil
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/shopShip", method = RequestMethod.GET)
	public String shopShipData(@RequestParam Map<String, String> map) throws Exception {
		Map<String,Object> amap = new HashMap<>();
		amap.put("companyId", ShiroUtils.getCompId());
		if (!map.containsKey("shopcode") 
			|| map.get("shopcode") == null
			|| "".equals(map.get("shopcode").toString())
			|| "null".equals(map.get("shopcode").toString())) {
			String shopcode = "";
			List<SysShopDeliveryData> shipshopList = sysShopDeliveryDataService.getShopByMap(amap);
			if(shipshopList != null && shipshopList.size() > 0){
				for(SysShopDeliveryData shop : shipshopList){
					shopcode += shop.getShopCode() + ",";
				}
			}else{
				shopcode += "-1";
			}
			if(shopcode.contains(",")){
				shopcode = shopcode.substring(0,shopcode.length()-1);
			}
			map.put("shopcode", shopcode);
		}
		
		// 开始时间
		String startDate = "";
		// 结束时间
		String endDate = "";
		if (map.containsKey("startDate") && map.get("startDate") != null
				&& !"".equals(map.get("startDate"))) {
			startDate = map.get("startDate").toString();
			if(startDate.contains("-")){
				startDate = startDate.replaceAll("-", "");
			}
		}else{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			startDate=yesterday.replaceAll("-", "");
			map.put("startDate",yesterday);
		}
		if (map.containsKey("endDate") && map.get("endDate") != null
				&& !"".equals(map.get("endDate"))) {
			endDate = map.get("endDate").toString();
			if(endDate.contains("-")){
				endDate = endDate.replaceAll("-", "");
			}
		}else{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			endDate=yesterday.replaceAll("-", "");
			map.put("endDate",yesterday);
		}
		
		String omsUrl = "http://121.199.179.23:30003/oms_interface/"+ "getOmsSendskuByMap?startDate="+startDate
			+"&endDate="+endDate;
		if(map.containsKey("procode")&&map.get("procode")!=null&&!"".equals(map.get("procode").toString())){
			omsUrl += "&procode="+map.get("procode").toString();
		}
		if(map.containsKey("skuoid")&&map.get("skuoid")!=null&&!"".equals(map.get("skuoid").toString())){
			omsUrl += "&skuoid="+map.get("skuoid").toString();
		}
		if(map.containsKey("shopcode")&&map.get("shopcode")!=null&&!"".equals(map.get("shopcode").toString())){
			omsUrl += "&shopcode="+map.get("shopcode").toString();
		}
		if(map.containsKey("latitude")&&map.get("latitude")!=null&&!"".equals(map.get("latitude").toString())){
			omsUrl += "&latitude="+map.get("latitude").toString();
		}else{
			map.put("latitude", "sday");
		}
		omsUrl=omsUrl.replaceAll(" ", "");
		//调用接口数据
		String shopSalesString = InterfaceUtil.searchLoginService(omsUrl);
		net.sf.json.JSONObject shopSalesJson = net.sf.json.JSONObject.fromObject(shopSalesString);
		
		String shopSalesListStr = shopSalesJson.getString("list");
		net.sf.json.JSONArray shopSalesArray = net.sf.json.JSONArray.fromObject(shopSalesListStr);
		List<net.sf.json.JSONObject> shopSalesList = (List<net.sf.json.JSONObject>) net.sf.json.JSONArray.toCollection(shopSalesArray, net.sf.json.JSONObject.class);
		//获得成本单价
		//Map<String, Double> goodsMap = util.getGoods();
		Map<String,net.sf.json.JSONObject> shopMap = new TreeMap<String,net.sf.json.JSONObject>();
		Map<String,net.sf.json.JSONObject> productMap = new TreeMap<String,net.sf.json.JSONObject>();
		Map<String,net.sf.json.JSONObject> shopProductMap = new TreeMap<String,net.sf.json.JSONObject>();
		Map<String,net.sf.json.JSONObject> tempMap = new TreeMap<String,net.sf.json.JSONObject>();
		
		if(shopSalesList != null && shopSalesList.size() > 0){
			for(net.sf.json.JSONObject shopSales : shopSalesList){
				String shopcode = shopSales.getString("shopcode");
				String productCode=shopSales.getString("procode");
				//发货数
				int skucount = shopSales.getInt("skucount");

				//发货金额
				double costfee = shopSales.getDouble("costfee");
				//发货成本
				double costprice = 0;
				if(skucount>0){
					costprice = Math.round((costfee/skucount)*10000)/10000.0;
				}
				//退货数
				int returncount = shopSales.getInt("returncount");
				//退货金额
				double returncostfee = shopSales.getDouble("returncostfee");
				//退货成本
				double returncostrice= 0;
				if(returncount>0){
					returncostrice = Math.round((returncostfee/returncount)*10000)/10000.0;
				}
				
				double costpriceNew = costprice!=0?costprice:(returncostrice!=0?returncostrice:0);
				//成本单价
				shopSales.put("costprice", costpriceNew);
				
				//获得销售单价
				Map<String,Object> params = new HashMap<String,Object>();
				params.put("shopCode", shopSales.get("shopcode"));
				params.put("barcode", shopSales.get("skuoid"));
				List<SysShopProduct> sspList = sysShopProductService.selectByMap(params);
				if(sspList != null && sspList.size() > 0){
					//有设置的售出单价
					SysShopProduct ssp = sspList.get(0);
					double price = ssp.getSellPrice().doubleValue();
					shopSales.put("soldPrice", ssp.getSellPrice());
					//发货金额
					shopSales.put("deliveryAmount", Math.round((skucount*price)*10000)/10000.0);
					//退货金额
					shopSales.put("returnAmount", Math.round((returncount*price)*10000)/10000.0);
				}else{
					shopSales.put("soldPrice", costpriceNew);
					//发货金额
					shopSales.put("deliveryAmount", costfee);
					//退货金额
					shopSales.put("returnAmount", returncostfee);
				}
				//发货成本
				shopSales.put("deliveryCosts", costfee);
				//退货成本
				shopSales.put("returnCosts", returncostfee);
				
				String syear="";
				String smonth="";
				String sday="";
				if(shopSales.containsKey("syear")){
				smonth+= shopSales.getString("syear");
				}
				if(shopSales.containsKey("smonth")){
					 syear += shopSales.getString("smonth");
				}
				if(shopSales.containsKey("sday")){
					 sday+= shopSales.getString("sday");
				}

				String latitude=map.get("latitude");
				String key = "";
				if ("sday".equals(latitude)) {
					key += "," + syear + "," + smonth + "," + sday;
				} else if ("smonth".equals(latitude)) {
					key += "," + syear + "," + smonth;
				} else if ("syear".equals(latitude)) {
					key += "," + syear;
				}
				
				if(map.containsKey("shop") && map.containsKey("productCode")){
					//根据店铺+货号汇总
					if(shopProductMap.containsKey(shopcode+productCode+key)){
						//汇总过得数据
						net.sf.json.JSONObject shopProductOld = shopProductMap.get(shopcode+productCode+key);
						//发货数
						skucount+=shopProductOld.getInt("skucount");
						shopProductOld.put("skucount", skucount);
						//发货金额
						shopProductOld.put("deliveryAmount", shopProductOld.getDouble("deliveryAmount")+shopSales.getDouble("deliveryAmount"));
						//退货数
						shopProductOld.put("returncount", shopProductOld.getInt("returncount")+shopSales.getInt("returncount"));
						//退货金额
						shopProductOld.put("returnAmount", shopProductOld.getDouble("returnAmount")+shopSales.getDouble("returnAmount"));
						shopProductOld.put("deliveryCosts", shopProductOld.getDouble("deliveryCosts")+shopSales.getDouble("deliveryCosts"));
						shopProductOld.put("returnCosts", shopProductOld.getDouble("returnCosts")+shopSales.getDouble("returnCosts"));
						shopProductMap.put(shopcode+productCode+key, shopProductOld);
					}else{
						//第一次汇总
						shopProductMap.put(shopcode+productCode+key, shopSales);
					}
				}else if(map.containsKey("shop")){
					//按店铺汇总
					if(shopMap.containsKey(shopcode+key)){
						//汇总过得数据
						net.sf.json.JSONObject shopSalesOld = shopMap.get(shopcode+key);
						//发货数
						skucount += shopSalesOld.getInt("skucount");
						shopSalesOld.put("skucount", skucount);
						//发货金额
						shopSalesOld.put("deliveryAmount", shopSalesOld.getDouble("deliveryAmount")+shopSales.getDouble("deliveryAmount"));
						//退货数
						shopSalesOld.put("returncount", shopSalesOld.getInt("returncount")+shopSales.getInt("returncount"));
						//退货金额
						shopSalesOld.put("returnAmount", shopSalesOld.getDouble("returnAmount")+shopSales.getDouble("returnAmount"));
						shopSalesOld.put("deliveryCosts", shopSalesOld.getDouble("deliveryCosts")+shopSales.getDouble("deliveryCosts"));
						shopSalesOld.put("returnCosts", shopSalesOld.getDouble("returnCosts")+shopSales.getDouble("returnCosts"));
						shopMap.put(shopcode+key, shopSalesOld);
					}else{
						//第一次汇总
						shopMap.put(shopcode+key, shopSales);
					}
					
				}else if(map.containsKey("productCode")){
					//按货号汇总
					if(productMap.containsKey(productCode+key)){
						//汇总过得数据
						net.sf.json.JSONObject productOld = productMap.get(productCode+key);
						//发货数
						skucount += productOld.getInt("skucount");
						productOld.put("skucount", skucount);
						//发货金额
						productOld.put("deliveryAmount", productOld.getDouble("deliveryAmount")+shopSales.getDouble("deliveryAmount"));
						//退货数
						productOld.put("returncount", productOld.getInt("returncount")+shopSales.getInt("returncount"));
						//退货金额
						productOld.put("returnAmount", productOld.getDouble("returnAmount")+shopSales.getDouble("returnAmount"));
						productOld.put("deliveryCosts", productOld.getDouble("deliveryCosts")+shopSales.getDouble("deliveryCosts"));
						productOld.put("returnCosts", productOld.getDouble("returnCosts")+shopSales.getDouble("returnCosts"));
						shopMap.put(productCode+key, shopSales);
					}else{
						//第一次汇总
						productMap.put(productCode+key, shopSales);
					}
				}else{
					// 按年月日
					if (tempMap.containsKey(key)) {
						// 汇总过得数据
						net.sf.json.JSONObject old = tempMap.get(key);
						// 发货数
						skucount += old.getInt("skucount");
						old.put("skucount", skucount);
						// 发货金额
						old.put("deliveryAmount",old.getDouble("deliveryAmount")+ shopSales.getDouble("deliveryAmount"));
						// 退货数
						old.put("returncount",old.getInt("returncount")+ shopSales.getInt("returncount"));
						// 退货金额
						old.put("returnAmount",old.getDouble("returnAmount")+ shopSales.getDouble("returnAmount"));
						old.put("deliveryCosts",old.getDouble("deliveryCosts")+ shopSales.getDouble("deliveryCosts"));
						old.put("returnCosts",old.getDouble("returnCosts")+ shopSales.getDouble("returnCosts"));
						tempMap.put(key, old);
					} else {
						// 第一次汇总
						tempMap.put(key, shopSales);
					}
				
				}
			}
		}
		
		List<Map<String, Object>> shopList = sysShopDeliveryDataService.selectByMap(amap);
		model.addAttribute("shopList",shopList);
		model.addAttribute("params", map);
		if(map.containsKey("shop") && map.containsKey("productCode")){
			model.addAttribute("shopProductMap", shopProductMap);
		}else if(map.containsKey("shop")){
			model.addAttribute("shopMap", shopMap);
		}else if(map.containsKey("productCode")){
			model.addAttribute("productMap", productMap);
		}
		else{
			model.addAttribute("shopSalesList", tempMap);
		}
		return "platform/buyer/sysShopProduct/shopShipData";
	}
	
}
