package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.utils.SearchPageUtil;

public interface BuyApplypurchaseItemMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyApplypurchaseItem record);

    int insertSelective(BuyApplypurchaseItem record);

    BuyApplypurchaseItem selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyApplypurchaseItem record);

    int updateByPrimaryKey(BuyApplypurchaseItem record);
    
    int updateByPrimaryKeyAndBarcode(BuyApplypurchaseItem record);
    
    List<BuyApplypurchaseItem> selectAllProductByPage(SearchPageUtil searchPageUtil);
    
    List<BuyApplypurchaseItem> selectAllProductByNoPage(Map<String, Object> map);
    
    List<Map<String, Object>> selectProductByBarcode(Map<String, Object> map);
    
    List<BuyApplypurchaseItem> selectByHeaderId(String applyId);
    
    List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrder(Map<String, Object> map);
    
    List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrderAndaddCustomPrice(Map<String, Object> map);
    
    List<Map<String, Object>> selectProductByStatistic(Map<String, Object> map);
    
    Map<String,Object> selectAlreadyPlanNumBySkuOid(Map<String, Object> map);
    
    Map<String,Object> selectCancelPlanNumBySkuOid(Map<String, Object> map);
    /**
     * 获得待下单的个数
     * @param map
     * @return
     */
    int getWaitOrderNum(Map<String, Object> map);
    /**
     * 获得待下单的数据
     * @param map
     * @return
     */
    List<Map<String, Object>> getWaitOrderByPage(SearchPageUtil searchPageUtil);
    
    List<Map<String, Object>> getWaitOrderByNoPage(Map<String, Object> map);
}