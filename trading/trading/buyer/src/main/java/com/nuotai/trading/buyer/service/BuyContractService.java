package com.nuotai.trading.buyer.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyContractMapper;
import com.nuotai.trading.buyer.dao.BuySupplierMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerContractMapper;
import com.nuotai.trading.buyer.model.BuyContract;
import com.nuotai.trading.buyer.model.seller.SellerContract;
import com.nuotai.trading.dao.BuyAttachmentMapper;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.dao.BuySupplierFriendMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class BuyContractService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyContractService.class);

	@Autowired
	private BuyContractMapper buyContractMapper;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	@Autowired
	private BuyAttachmentMapper attachmentMapper;
	@Autowired
	private BSellerContractMapper bSellerContractMapper;
	@Autowired
	private BuySupplierMapper buySupplierMapper;
	@Autowired
	private BuySupplierFriendMapper friendMapper;
	public BuyContract get(String id){
		return buyContractMapper.get(id);
	}
	
	public List<BuyContract> queryList(Map<String, Object> map){
		return buyContractMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyContractMapper.queryCount(map);
	}
	
	public void add(BuyContract buyContract){
		BuyCompany buyCompany = buyCompanyMapper.selectByPrimaryKey(ShiroUtils.getCompId());
		buyContract.setSponsorId(ShiroUtils.getCompId());
		buyContract.setSponsorName(buyCompany.getCompanyName());
		buyContract.setSponsorLinkman(buyCompany.getLinkMan());
		buyContract.setSponsorPhone(buyCompany.getTelNo());
		buyContract.setSponsorType("0");
		buyContract.setCreaterCompanyId(ShiroUtils.getCompId());
		buyContract.setCreaterId(ShiroUtils.getUserId());
		buyContract.setCreateDate(new Date());
		buyContract.setCreaterName(ShiroUtils.getUserName());
		buyContractMapper.add(buyContract);
	}
	
	public void update(BuyContract buyContract){
		buyContract.setUpdateId(ShiroUtils.getUserId());
		buyContract.setUpdateDate(new Date());
		buyContract.setUpdateName(ShiroUtils.getUserName());
		buyContractMapper.update(buyContract);
	}
	
	public void delete(String id){
		buyContractMapper.delete(id);
	}


    public Map<String,Object> queryContractList(String role, SearchPageUtil searchPageUtil, Map<String,Object> params) {
		Map<String,Object> map = new HashMap<String,Object>();
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
			params.put("status","");
		}
		params.put("companyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		//合同列表数据
		List<BuyContract> list = buyContractMapper.queryContractList(searchPageUtil);
		//合同总列表，用于计算数量
		List<BuyContract> listAll = buyContractMapper.queryContractListAll(params);
		//总记录数
		long allCount = searchPageUtil.getPage().getCount();
		map.put("allCount",allCount);
		//草稿数
		int draftCount = 0;
		//待我审批数
		int myApproval = 0;
		//待对方审批数
		int customerApproval = 0;
		//待传合同照数
		int contractPhotoCount = 0;
		//协议达成数
		int ok = 0;
		//驳回数
		int reject = 0;
		if(!ObjectUtil.isEmpty(listAll)&&listAll.size()>0){
			for (BuyContract buyContract:listAll) {
				switch (buyContract.getStatus()){
					case 0:
						draftCount++;
						break;
					case 1:
						myApproval++;
						break;
					case 2:
						customerApproval++;
						break;
					case 3:
						contractPhotoCount++;
						break;
					case 4:
						ok++;
						break;
					case 5:
						reject++;
						break;
					default:
						break;
				}
			}
		}
		map.put("total",listAll.size());
		map.put("draftCount",draftCount);
		map.put("myApprovalCount",myApproval);
		map.put("customerApprovalCount",customerApproval);
		map.put("contractPhotoCount",contractPhotoCount);
		map.put("okCount",ok);
		map.put("rejectCount",reject);

		map.put("contractList",list);
		return map;
    }

	/**
	 * 检查合同编号是否被占用
	 * @param contractNo
	 * @return
	 */
	public int checkContractNo(String contractId,String contractNo) {
		return buyContractMapper.checkContractNo(contractId,contractNo);
	}

	/**
	 * 协议达成
	 * @param oriAttachments 合同原件地址
	 * @param contractId
	 * @param validPeriodStart
	 * @param validPeriodEnd
	 * @param oriAttachments
	 */
	public void agreementReached(String contractId, String validPeriodStart, String validPeriodEnd, String oriAttachments) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//更新合同状态
		BuyContract contract = new BuyContract();
		contract.setId(contractId);
		contract.setStatus(4);
		contract.setValidPeriodStart(sdf.parse(validPeriodStart));
		contract.setValidPeriodEnd(sdf.parse(validPeriodEnd));
		buyContractMapper.update(contract);
		//将合同原件地址插入表中
		if(!ObjectUtil.isEmpty(oriAttachments)){
			List<String> oriContracts = JSONArray.parseArray(oriAttachments,String.class);
 			for (String url:oriContracts ) {
				BuyAttachment attachment = new BuyAttachment();
				attachment.setId(ShiroUtils.getUid());
				attachment.setUrl(url);
				attachment.setRelatedId(contractId);
				attachment.setType(0);
				attachmentMapper.insert(attachment);
			}
		}
	}

	/**
	 * 合同保存
	 * @param buyContract
	 * @return
	 */
	public Msg saveContract(BuyContract buyContract) {
		Msg msg = new Msg();
		try {
			if(!ObjectUtil.isEmpty(buyContract)){
				if(!ObjectUtil.isEmpty(buyContract.getId())){//修改合同
					String contractId = buyContract.getId();
					int count = this.checkContractNo(contractId,buyContract.getContractNo());
					if(count>0){
						msg.setFlag(false);
						msg.setMsg("该合同编号已存在！");
						return msg;
					}
					this.update(buyContract);
				}else{//新增合同
					int count = this.checkContractNo("",buyContract.getContractNo());
					if(count>0){
						msg.setFlag(false);
						msg.setMsg("该合同编号已存在！");
						return msg;
					}
					buyContract.setId(ShiroUtils.getUid());
					this.add(buyContract);
				}
			}
			msg.setFlag(true);
			msg.setMsg(buyContract.getId());//将合同ID放入msg
		} catch (Exception e) {
			e.printStackTrace();
			msg.setFlag(false);
			msg.setMsg("合同保存失败");
		}
		return msg;
	}

	/**
	 * 审批通过
	 * @param id
	 * @return
	 */
	public JSONObject approveSuccess(String id) {
		JSONObject json = new JSONObject();
		boolean isSendToSeller = false;
		//先判断供应商是否是互通的
		BuyContract buyContract = buyContractMapper.get(id);
		String sellerId = buyContract.getCustomerId();
		//互通好友ID不为空的情况下，传输到卖家，否则不传输，并且状态为待传合同照
		BuyContract contractUpd = new BuyContract();
		if(!ObjectUtil.isEmpty(buyContract.getCustomerId())){
			contractUpd.setId(id);
			//待传合同照
			contractUpd.setStatus(3);
			contractUpd.setBuyerApproveStatus(1);
			//客户审批通过
			contractUpd.setSellerApproveStatus(1);
			isSendToSeller = true;
		}else {
			contractUpd.setId(id);
			contractUpd.setStatus(3);
			contractUpd.setBuyerApproveStatus(1);
			contractUpd.setSellerApproveStatus(1);
		}
		int result = buyContractMapper.update(contractUpd);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
			//将合同发送到卖家合同里
			if(isSendToSeller){
				sendContractToSeller(buyContract,buyContract.getCustomerId());
			}

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 保存合同到供应商
	 * @param buyContract
	 * @param friendId
	 */
	private void sendContractToSeller(BuyContract buyContract,String friendId) {
		SellerContract sellerContract = bSellerContractMapper.getByBuyContractId(buyContract.getId());
		if(!ObjectUtil.isEmpty(sellerContract)){
			bSellerContractMapper.delete(sellerContract.getId());
		}
		sellerContract = new SellerContract();
		sellerContract.setId(ShiroUtils.getUid());//ID
		sellerContract.setBuyerContractId(buyContract.getId());
		sellerContract.setContractNo(buyContract.getContractNo());//合同编号
		sellerContract.setContractName(buyContract.getContractName());//合同名称
		sellerContract.setSponsorId(buyContract.getSponsorId());//发起人公司ID
		sellerContract.setSponsorName(buyContract.getSponsorName());//发起人公司名称
		sellerContract.setSponsorLinkman(buyContract.getSponsorLinkman());//发起人公司联系人
		sellerContract.setSponsorPhone(buyContract.getSponsorPhone());//发起人公司电话
		sellerContract.setSponsorType(buyContract.getSponsorType());//发起方式(0:买家发起 1:卖家发起)
		sellerContract.setCustomerId(buyContract.getCustomerId());//客户公司id
		sellerContract.setCustomerName(buyContract.getCustomerName());//客户公司名称
		sellerContract.setCustomerLinkman(buyContract.getCustomerLinkman());//客户联系人
		sellerContract.setCustomerPhone(buyContract.getCustomerPhone());//客户手机号
		sellerContract.setContentAddress(buyContract.getContentAddress());//合同内容地址
		sellerContract.setStatus(3);//状态 待传合同照
		sellerContract.setBuyerApproveStatus(1);//买方审批状态 通过
		sellerContract.setSellerApproveStatus(1);//卖方审批状态 通过
		sellerContract.setRemark(buyContract.getRemark());//备注
		sellerContract.setCreaterCompanyId(buyContract.getSponsorId());//创建人公司ID
		sellerContract.setCreaterId("");//创建人id
		sellerContract.setCreaterName("");//创建人
		sellerContract.setCreateDate(buyContract.getCreateDate());//创建时间
		bSellerContractMapper.add(sellerContract);

	}


	/**
	 * 审批未通过
	 * @param id
	 * @return
	 */
	public JSONObject approveError(String id) {
		JSONObject json = new JSONObject();
		BuyContract contract = new BuyContract();
		contract.setId(id);
		contract.setStatus(1);
		contract.setBuyerApproveStatus(2);
		int result = buyContractMapper.update(contract);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
}
