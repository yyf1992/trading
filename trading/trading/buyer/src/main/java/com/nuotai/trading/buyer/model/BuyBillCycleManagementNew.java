package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-09-13 11:45:51
 */
public class BuyBillCycleManagementNew{
	//
	private String id;
	//正式申请编号
	private String newBillCycleId;
	//
	private String buyCompanyId;
	//
	private String buyCompanyName;

	private String sellerCompanyId;

	private String sellerCompanyName;
	//账期开始日期
	private Integer cycleStartDate;
	//账期结束时间
	private Integer cycleEndDate;
	//结账周期
	private Integer checkoutCycle;
	//出账日期
	private Integer billStatementDate;
	//出账日期
	private Date outStatementDate;
	//账单周期状态：1 待我审批，2 待对方审核，3 审核通过，4 审批驳回(买家)，5 审核驳回（卖家）
	private String billDealStatus;
	//
	private String createUser;
	//账单周期状态：1 待我审批，2 待对方审核，3 审核通过，4 审批驳回(买家)，5 审核驳回（卖家）
	private Date createTime;

	private String createDate;

	private String createName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNewBillCycleId() {
		return newBillCycleId;
	}

	public void setNewBillCycleId(String newBillCycleId) {
		this.newBillCycleId = newBillCycleId;
	}

	public String getBuyCompanyId() {
		return buyCompanyId;
	}

	public void setBuyCompanyId(String buyCompanyId) {
		this.buyCompanyId = buyCompanyId;
	}

	public String getSellerCompanyId() {
		return sellerCompanyId;
	}

	public void setSellerCompanyId(String sellerCompanyId) {
		this.sellerCompanyId = sellerCompanyId;
	}

	public Integer getCycleStartDate() {
		return cycleStartDate;
	}

	public void setCycleStartDate(Integer cycleStartDate) {
		this.cycleStartDate = cycleStartDate;
	}

	public Integer getCycleEndDate() {
		return cycleEndDate;
	}

	public void setCycleEndDate(Integer cycleEndDate) {
		this.cycleEndDate = cycleEndDate;
	}

	public Integer getCheckoutCycle() {
		return checkoutCycle;
	}

	public void setCheckoutCycle(Integer checkoutCycle) {
		this.checkoutCycle = checkoutCycle;
	}

	public Integer getBillStatementDate() {
		return billStatementDate;
	}

	public void setBillStatementDate(Integer billStatementDate) {
		this.billStatementDate = billStatementDate;
	}

	public String getBillDealStatus() {
		return billDealStatus;
	}

	public void setBillDealStatus(String billDealStatus) {
		this.billDealStatus = billDealStatus;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public Date getOutStatementDate() {
		return outStatementDate;
	}

	public void setOutStatementDate(Date outStatementDate) {
		this.outStatementDate = outStatementDate;
	}

	public String getBuyCompanyName() {
		return buyCompanyName;
	}

	public void setBuyCompanyName(String buyCompanyName) {
		this.buyCompanyName = buyCompanyName;
	}

	public String getSellerCompanyName() {
		return sellerCompanyName;
	}

	public void setSellerCompanyName(String sellerCompanyName) {
		this.sellerCompanyName = sellerCompanyName;
	}
}
