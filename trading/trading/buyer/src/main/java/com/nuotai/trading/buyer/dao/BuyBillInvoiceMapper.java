package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillInvoice;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-10-12 17:45:09
 */
@Component
public interface BuyBillInvoiceMapper extends BaseDao<BuyBillInvoice> {
    //发票列表查询
    List<Map<String, Object>> queryInvoiceList(SearchPageUtil searchPageUtil);
    //根据条件查询发票导出数据
    List<BuyBillInvoice> queryInvoiceBuyMap(Map<String,Object> map);
    //根据状态查询数量
    int getInvoiceCountByStatus(Map<String,Object> map);
    //发票核实
    int updateBillInvoice(Map<String, Object> updateSavemap);
    //核实信息同步到卖家
    int updateSellerInvoice(Map<String, Object> updateSavemap);
    //核实驳回时删除关联
    int deleteInvoiceCommodity(Map<String, Object> updateSavemap);
    //根据账单详情查询发票详情
   BuyBillInvoice queryInvoiceByRecItem(Map<String, Object> updateSavemap);
}
