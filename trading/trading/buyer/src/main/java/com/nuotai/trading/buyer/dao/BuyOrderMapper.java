package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.seller.SellerOrder;
import com.nuotai.trading.buyer.model.seller.SellerOrderSupplierProduct;
import com.nuotai.trading.utils.SearchPageUtil;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface BuyOrderMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyOrder record);

    int insertSelective(BuyOrder record);

    BuyOrder selectByPrimaryKey(String id);

    BuyOrder selectByOrderCode(String orderCode);

    int updateByPrimaryKeySelective(BuyOrder record);

    int updateByPrimaryKey(BuyOrder record);
    
    List<BuyOrder> selectByPage(SearchPageUtil searchPageUtil);
    
    int selectOrdersNumByMap(Map<String, Object> params);

	int insertOrder(BuyOrder order);

    List<BuyOrder> selectByMap(Map<String, Object> map);

	List<Map<String, Object>> getSupplierUsageRate(Map<String, Object> map);
	
	List<Map<String, Object>> getSupplierArrivalRate(Map<String, Object> map);
	List<Map<String, Object>> getSupplierCompletionRate(Map<String, Object> map);
	
	List<BuyOrder> selectApprovedOrderByPage(SearchPageUtil searchPageUtil);
	List<String> selectApprovedId(Map<String, Object> map);

	List<Double> getLastPrice(@Param("barcode")String barcode, @Param("supplierId")String supplierId);

	//根据卖家订单id查询订单号
    Map<String,Object> selSellerOrder(String orderId);
    //根据财务对账状态修改订单对账、付款状态
    int updateOrderBuyBill(Map<String, Object> params);
}