package com.nuotai.trading.buyer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;

import lombok.Data;

@Data
public class BuyApplypurchaseHeader {
	// 主键id
	private String id;
	// 公司id
	private String companyId;
	// 店铺采购单号
	private String applyCode;

	// 标题
	private String title;

	// 说明
	private String remark;

	// 采购总数量
	private BigDecimal goodsCount;

	// 到货数量
	private BigDecimal arrivalCount;

	// 订单状态 0-等待审核；1-审核通过；2-审核不通过；3-完结；
	private String status;

	// 店铺id
	private String shopId;

	// 店铺代码
	private String shopCode;

	// 店铺名称
	private String shopName;
	// 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
	// 创建人名称
	private String createName;

	// 创建人
	private String creater;

	// 创建时间
	private Date createDate;
	//
    private String updateId;
    //
    private String updateName;
    //
    private Date updateDate;
    //
    private String delId;
    //
    private String delName;
    //
    private Date delDate;

	// 上传文件
	private String proof;

	// 平台审核意见-审核查询用
	private String storeVerifyReason;
	// 采购审核意见
	private String firmVerifyReason;
	// 要求到货时间
	private Date predictarred;
	private List<BuyApplypurchaseItem> itemList;
	private TradeVerifyHeader tradeHeader;
	private List<TradeVerifyPocess> pocessList;
	private String predictarredStr;
	// 是否允许修改0-可修改，1-不可修改
	private String isAllowUpdate;
	//存销售计划下单id用
	private String salePlanIdStr;
}