package com.nuotai.trading.buyer.dao.seller;

import java.util.Map;

import com.nuotai.trading.buyer.model.seller.BSellerCustomerItem;
import com.nuotai.trading.dao.BaseDao;

/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
public interface BSellerCustomerItemMapper extends BaseDao<BSellerCustomerItem> {
	
	void deleteByCustomerId(String customerId);

	int updateRealCount(Map<String, Object> itemMap);

	BSellerCustomerItem getByBuyItemId(String itemId);
}
