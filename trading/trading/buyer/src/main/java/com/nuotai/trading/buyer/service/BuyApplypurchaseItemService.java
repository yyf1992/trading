package com.nuotai.trading.buyer.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.buyer.dao.BuyApplypurchaseItemMapper;
import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyApplypurchaseItemService implements BuyApplypurchaseItemMapper{

	@Autowired
	private BuyApplypurchaseItemMapper mapper;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private BuyShopProductService shopProductService;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuyApplypurchaseItem record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyApplypurchaseItem record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuyApplypurchaseItem selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKey(record);
	}
	
	@Override
	public int updateByPrimaryKeyAndBarcode(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKeyAndBarcode(record);
	}
	
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByPage(SearchPageUtil searchPageUtil) {
		return mapper.selectAllProductByPage(searchPageUtil);
	}
	
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByNoPage(Map<String, Object> map) {
		return mapper.selectAllProductByNoPage(map);
	}

	@Override
	public List<Map<String, Object>> selectProductByBarcode(Map<String, Object> map) {
		//显示剩余要采购的商品
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> detailsList = mapper.selectProductByBarcode(map);
		if(detailsList != null && detailsList.size() > 0){
			for(Map<String, Object> mapResult : detailsList){
				// 用编号和条形码查询已下单商品数量
				Map<String,Object> mapPare = new HashMap<String,Object>();
				mapPare.put("companyId", ShiroUtils.getCompId());
				mapPare.put("applyCode", mapResult.get("apply_code"));
				mapPare.put("skuOid", mapResult.get("barcode"));
				Map<String,Object> mapAlready = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapPare);
				if(mapAlready != null){
					int alreadyPlanPurchaseNum = mapAlready.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(mapAlready.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapPare);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapPare);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					if(alreadyPlanPurchaseNum < Integer.parseInt(mapResult.get("apply_count").toString())){
						// 查询已下单未审核数量
						Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
						if(mapLock != null){
							int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum - lockGoodsNumber);
						}else{
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum);
						}
					}else{
						mapResult.put("overplusNum", 0);
					}
				}else{
					// 查询已下单未审核数量
					Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
					if(mapLock != null){
						int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString())  - lockGoodsNumber);
					}else{
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()));
					}
				}
				if(Integer.parseInt(mapResult.get("overplusNum").toString()) != 0){//剩余采购数量为0的不显示
					resultList.add(mapResult);
				}
			}
		}
		return resultList;
	}

	@Override
	public List<BuyApplypurchaseItem> selectByHeaderId(String applyId) {
		return mapper.selectByHeaderId(applyId);
	}

	@Override
	public List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrder(
			Map<String, Object> mapPare) {
		Map<String, Object> mapP = new HashMap<String, Object>();
		// 已计划采购量
		List<BuyApplypurchaseItem> itemList = mapper.selectAllProductByBeatchCreateOrder(mapPare);
		if(itemList != null && itemList.size() > 0){
			for(BuyApplypurchaseItem item : itemList){
				mapP.put("skuOid", item.getBarcode());
				mapP.put("companyId", ShiroUtils.getCompId());
				Map<String,Object> map = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapP);
				if(map != null){
					int alreadyPlanPurchaseNum = map.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(map.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapP);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapP);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					item.setAlreadyPlanPurchaseNum(alreadyPlanPurchaseNum);
				}else{
					item.setAlreadyPlanPurchaseNum(0);
				}
				//根据商品条形码查询关联的供应商列表
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
				params.put("linktype", "0");
				params.put("status", "3");//状态已通过
				params.put("searchCompanyId", ShiroUtils.getCompId());
				params.put("skuOid", item.getBarcode());
				List<Map<String,Object>> supplierLinksList = shopProductService.loadProductLinksByMap(params);
				if(supplierLinksList != null && supplierLinksList.size() > 0){
					for(Map<String,Object> supplierLinks : supplierLinksList){
						//产能
						int dailyOutput = Integer.parseInt(supplierLinks.get("dailyOutput").toString());
						//到货天数
						int storageDay = Integer.parseInt(supplierLinks.get("storageDay").toString());
						int goodsNum = item.getApplyCount() - item.getAlreadyPlanPurchaseNum() - item.getLockGoodsNumber();
						int c = 0;
						if(dailyOutput != 0){
							c = goodsNum%dailyOutput == 0 ? (goodsNum/dailyOutput) : (goodsNum/dailyOutput)+1;
						}
						//数量/产能+到货天数+审批天数
						int dayNum = c + storageDay + Constant.TRADEVERIFYDAY;
						SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.DATE, dayNum);//计算dayNum天后的时间
						String str2 = s.format(calendar.getTime());
						supplierLinks.put("predictArred", str2);
					}
					item.setSupplierLinksList(supplierLinksList);
				}
			}
		}
		return itemList;
	}

	@Override
	public List<Map<String, Object>> selectProductByStatistic(
			Map<String, Object> map) {
		return mapper.selectProductByStatistic(map);
	}
	
	public void getProductTypeNum(Map<String, Object> params) {
		//成品
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue()+"");
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("productType",Constant.ProductType.PRODUCT.getValue()+"");
		int finishedProductNum = getWaitOrderNum(selectMap);
		//原材料
		selectMap.put("productType", Constant.ProductType.MATERIAL.getValue()+"");
		int rawStockNum = getWaitOrderNum(selectMap);
		//辅料
		selectMap.put("productType", Constant.ProductType.ACCESSORIES.getValue()+"");
		int accessoriesNum = getWaitOrderNum(selectMap);
		//虚拟产品
		selectMap.put("productType", Constant.ProductType.VIRTUALPRODUCTS.getValue()+"");
		int inventedNum = getWaitOrderNum(selectMap);
		params.put("finishedProductNum", finishedProductNum);
		params.put("rawStockNum", rawStockNum);
		params.put("accessoriesNum", accessoriesNum);
		params.put("inventedNum", inventedNum);
	}
	
	public String loadPurchaseProductListData(SearchPageUtil searchPageUtil,Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue()+"");
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/applypurchase/productSum/";
		switch (tabId) {
		case 0://成品
			params.put("interest", "0");
			returnPath += "finishedProduct";
			break;
		case 1://原材料
			params.put("interest", "1");
			returnPath += "rawStock";
			break;
		case 2://辅料
			params.put("interest", "2");
			returnPath += "accessories";
			break;
		case 3://虚拟产品
			params.put("interest", "3");
			returnPath += "invented";
			break;
		default:
			returnPath += "finishedProduct";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("0".equals(interest)){
			//成品
			params.put("productType",Constant.ProductType.PRODUCT.getValue()+"");
		}else if("1".equals(interest)){
			//原材料
			params.put("productType", Constant.ProductType.MATERIAL.getValue()+"");
		}else if("2".equals(interest)){
			//辅料
			params.put("productType", Constant.ProductType.ACCESSORIES.getValue()+"");
		}
		else if("3".equals(interest)){
			//虚拟产品
			params.put("productType", Constant.ProductType.VIRTUALPRODUCTS.getValue()+"");
		}
		searchPageUtil.setObject(params);
		List<Map<String,Object>> purchaseList = getWaitOrderByPage(searchPageUtil);
		searchPageUtil.getPage().setList(purchaseList);
		return returnPath;
	}

	/**
	 * 查询定制商品修改后的价格
	 */
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrderAndaddCustomPrice(Map<String, Object> mapPare) {

		Map<String, Object> mapP = new HashMap<String, Object>();
		// 已计划采购量
		List<BuyApplypurchaseItem> itemList = mapper.selectAllProductByBeatchCreateOrderAndaddCustomPrice(mapPare);
		if(itemList != null && itemList.size() > 0){
			for(BuyApplypurchaseItem item : itemList){
				mapP.put("skuOid", item.getBarcode());
				mapP.put("companyId", ShiroUtils.getCompId());
				Map<String,Object> map = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapP);
				if(map != null){
					int alreadyPlanPurchaseNum = map.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(map.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapP);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapP);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					item.setAlreadyPlanPurchaseNum(alreadyPlanPurchaseNum);
				}else{
					item.setAlreadyPlanPurchaseNum(0);
				}
				//根据商品条形码查询关联的供应商列表
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
				params.put("linktype", "0");
				params.put("status", "3");//状态已通过
				params.put("searchCompanyId", ShiroUtils.getCompId());
				params.put("skuOid", item.getBarcode());
				List<Map<String,Object>> supplierLinksList = shopProductService.loadProductLinksByMap(params);
				if(supplierLinksList != null && supplierLinksList.size() > 0){
					item.setSupplierLinksList(supplierLinksList);
				}
			}
		}
		return itemList;
	}
	
	public Map<String,Object> selectAlreadyPlanNumBySkuOid(Map<String, Object> map) {
		return mapper.selectAlreadyPlanNumBySkuOid(map);
	}
	
	public Map<String,Object> selectCancelPlanNumBySkuOid(Map<String, Object> map) {
		return mapper.selectCancelPlanNumBySkuOid(map);
	}

	@Override
	public int getWaitOrderNum(Map<String, Object> map) {
		return mapper.getWaitOrderNum(map);
	}

	@Override
	public List<Map<String, Object>> getWaitOrderByPage(SearchPageUtil searchPageUtil) {
		return mapper.getWaitOrderByPage(searchPageUtil);
	}
	
	@Override
	public List<Map<String, Object>> getWaitOrderByNoPage(Map<String, Object> map) {
		return mapper.getWaitOrderByNoPage(map);
	}
}
