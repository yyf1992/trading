package com.nuotai.trading.buyer.service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationAdvanceMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationAdvanceMapper;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvance;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliationAdvance;
import com.nuotai.trading.dao.*;
import com.nuotai.trading.model.*;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

import com.nuotai.trading.buyer.dao.BuyBillReconciliationAdvanceEditMapper;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvanceEdit;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillReconciliationAdvanceEditService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillReconciliationAdvanceEditService.class);

	@Autowired
	private BuyBillReconciliationAdvanceEditMapper buyBillAdvanceEditMapper;
	@Autowired
	private BuyBillReconciliationAdvanceMapper buyBillRecAdvanceMapper;
	@Autowired
	private BSellerBillReconciliationAdvanceMapper sellerBillRecAdvanceMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;

	//列表数据展示
	public List<BuyBillReconciliationAdvanceEdit> queryAdvanceEditList(SearchPageUtil searchPageUtil){
		return buyBillAdvanceEditMapper.queryAdvanceEditList(searchPageUtil);
	}

	//根据条件查询充值数量
	public int queryEditCount(Map<String,Object> params){
		return buyBillAdvanceEditMapper.queryEditCount(params);
	}

	//根据状态查询充值数量
	public void queryEditCountByStatus(Map<String,Object> advanceMap){
		//查询所有状态数量
		advanceMap.put("acceptStatusCount", "0");
		int allEditCount = queryEditCount(advanceMap);

		//查询待内部审批
		advanceMap.put("acceptStatusCount", "1");
		int waitEditCount = queryEditCount(advanceMap);
		//内部审批通过
		advanceMap.put("acceptStatusCount", "2");
		int adoptEditCount = queryEditCount(advanceMap);
		//内部审批驳回
		advanceMap.put("acceptStatusCount", "3");
		int rejectEditCount = queryEditCount(advanceMap);

		advanceMap.put("allEditCount", allEditCount);
		advanceMap.put("waitEditCount",waitEditCount);
		advanceMap.put("adoptEditCount", adoptEditCount);
		advanceMap.put("rejectEditCount", rejectEditCount);
	}

	//付款待审批查询
	public void approveAdvanceEdit(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("buyCompanyId", ShiroUtils.getCompId());
		params.put("acceptStatus", "1");
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		//params1.put("siteId", "18030316174277880699");//阿里
		params1.put("siteId", "18040313401887543187");//阿里充值
		//params1.put("siteId", "18032617435211938508");//测试新
		//获得所有未审批的id
		List<String> acceptAdvanceIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> advanceEditIdListNew = new ArrayList<>();

		List<BuyBillReconciliationAdvanceEdit> billAdvanceEditList = buyBillAdvanceEditMapper.selAdvanceEditList(params);
		//List<BuyBillReconciliation> billRecList = billReconciliationMapper.selectByPrimaryKeyList(params);
		if(!billAdvanceEditList.isEmpty()){
			Map<String, String> acceptAdvanceEditIdMap = new HashMap<>();
			for(BuyBillReconciliationAdvanceEdit billAdvanceEidt : billAdvanceEditList){
				if(null != billAdvanceEidt.getId()){
					String editId =billAdvanceEidt.getId();
					acceptAdvanceEditIdMap.put(editId, editId);
				}

			}
			if(!acceptAdvanceIdList.isEmpty()){
				for(String acceptId : acceptAdvanceIdList){
					if(acceptAdvanceEditIdMap.containsKey(acceptId)){
						advanceEditIdListNew.add(acceptId);
					}
				}
			}
		}else{
			advanceEditIdListNew = acceptAdvanceIdList;
		}

		params.put("approved", true);
		params.put("advanceEditIdList", advanceEditIdListNew);
		searchPageUtil.setObject(params);
		List<BuyBillReconciliationAdvanceEdit> billAdvanceEditInfoList = buyBillAdvanceEditMapper.queryAdvanceEditList(searchPageUtil);
		searchPageUtil.getPage().setList(billAdvanceEditInfoList);
	}

	//一键通过
	public JSONObject verifyAdvanceEditAll(String acceptIdStr) {
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(acceptIdStr)){
			String[] idStr = acceptIdStr.split(",");
			for(String acceptId : idStr){
				verifyApproved(acceptId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json;
	}

	/**
	 * 审批
	 * @param acceptId
	 */
	public void verifyApproved(String acceptId){
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(acceptId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						rechargeSuccess(acceptId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
	}


	//添加预付款充值
	public List<String> addAdvanceCharge(Map<String,Object> addChargeMap){
		List<String> idList = new ArrayList<String>();
		String itemId = ShiroUtils.getUid();
		String createUserId = ShiroUtils.getUserId();
		String createUserName = ShiroUtils.getUserName();

		String advanceId = addChargeMap.get("advanceId").toString();
		String updateTotalStr = addChargeMap.get("updateTotal").toString();
		String billFileStr = addChargeMap.get("billFileStr").toString();
		String rechargeRemarks = addChargeMap.get("rechargeRemarks").toString();

		BigDecimal updateTotal = new BigDecimal(0);
		if(!"".equals(updateTotalStr)){
			updateTotal = new BigDecimal(updateTotalStr);
		}

		//账单对账详情
		Map<String,Object> advanceMap = new HashMap<String,Object>();
		advanceMap.put("id",advanceId);
		BuyBillReconciliationAdvance billAdvance = buyBillRecAdvanceMapper.queryAdvanceInfo(advanceMap);

		BuyBillReconciliationAdvanceEdit buyBillRecAdvanceEdit = new BuyBillReconciliationAdvanceEdit();
		buyBillRecAdvanceEdit.setId(itemId);
		buyBillRecAdvanceEdit.setAdvanceId(advanceId);
		buyBillRecAdvanceEdit.setUpdateTotal(updateTotal);
		buyBillRecAdvanceEdit.setCreateUserId(createUserId);
		buyBillRecAdvanceEdit.setCreateUserName(createUserName);
		buyBillRecAdvanceEdit.setCreateBillUserId(billAdvance.getCreateBillUserId());
		buyBillRecAdvanceEdit.setCreateBillUserName(billAdvance.getCreateBillUserName());
		buyBillRecAdvanceEdit.setFileAddress(billFileStr);
		buyBillRecAdvanceEdit.setEditStatus("1");
		buyBillRecAdvanceEdit.setAdvanceRemarks(rechargeRemarks);

		int addItemCount = buyBillAdvanceEditMapper.addAdvanceEdit(buyBillRecAdvanceEdit);
		if(addItemCount > 0){
			idList.add(itemId);
		}
		return idList;
	}

	//根据付款状态修改付款信息 驳回再次修改
	public void updateAdvanceCharge(Map<String,Object> updateAdvanceMap){
		List<String> idList = new ArrayList<String>();
		String itemId = updateAdvanceMap.get("itemId").toString();
		String updateTotalStr = updateAdvanceMap.get("updateTotal").toString();
		String billFileStr = updateAdvanceMap.get("billFileStr").toString();
		String rechargeRemarks = updateAdvanceMap.get("rechargeRemarks").toString();

		BigDecimal updateTotal = new BigDecimal(0);
		if(!"".equals(updateTotalStr)){
			updateTotal = new BigDecimal(updateTotalStr);
		}
		//预付款数据拼装
		BuyBillReconciliationAdvanceEdit buyBillRecAdvanceEdit = new BuyBillReconciliationAdvanceEdit();
		buyBillRecAdvanceEdit.setId(itemId);
		buyBillRecAdvanceEdit.setEditStatus("1");
		buyBillRecAdvanceEdit.setUpdateTotal(updateTotal);
		buyBillRecAdvanceEdit.setFileAddress(billFileStr);
		buyBillRecAdvanceEdit.setAdvanceRemarks(rechargeRemarks);

		int updateAdvanceCount = buyBillAdvanceEditMapper.updateAdvanceEdit(buyBillRecAdvanceEdit);
		if(updateAdvanceCount > 0){
			//String acceptPaymentId = updateAdvanceMap.get("acceptPaymentId").toString();
			//4.若有审批，则重置审批状态
			TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(itemId);
			if(!ObjectUtil.isEmpty(header)){
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				//获得最大index
				int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
				//添加审批
				TradeVerifyPocess pocessUpdate = new TradeVerifyPocess();
				pocessUpdate.setId(ShiroUtils.getUid());
				pocessUpdate.setHeaderId(header.getId());
				pocessUpdate.setUserId(ShiroUtils.getUserId());
				pocessUpdate.setUserName(ShiroUtils.getUserName());
				pocessUpdate.setStatus("7");//买家修改
				pocessUpdate.setRemark("预付款充值修改后再次提交");
				pocessUpdate.setVerifyIndex(maxIndex+1);
				pocessUpdate.setStartDate(new Date());
				pocessUpdate.setEndDate(new Date());
				tradeVerifyPocessMapper.add(pocessUpdate);
				maxIndex+=1;
				int index = maxIndex;
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				//删除未审批的数据
				tradeVerifyPocessMapper.deleteNotVerify(header.getId());

				if(!pocessList.isEmpty()){
					Map<String,String> userMap = new HashMap<>();
					for(TradeVerifyPocess pocess : pocessList){
						if("0".endsWith(pocess.getStatus())
								|| "1".endsWith(pocess.getStatus())
								|| "2".endsWith(pocess.getStatus())){
							if(userMap.containsKey(pocess.getUserId())){
								continue;
							}
							TradeVerifyPocess pocessNew = new TradeVerifyPocess();
							pocessNew.setId(ShiroUtils.getUid());
							pocessNew.setHeaderId(header.getId());
							pocessNew.setUserId(pocess.getUserId());
							pocessNew.setUserName(pocess.getUserName());
							pocessNew.setStatus("0");//等待审批
							pocessNew.setRemark(null);
							pocessNew.setVerifyIndex(index+1);
							pocessNew.setStartDate(new Date());
							pocessNew.setEndDate(new Date());
							tradeVerifyPocessMapper.add(pocessNew);
							userMap.put(pocess.getUserId(), pocess.getUserId());
							if(index == maxIndex){
								//待审批人员发送钉钉消息
								DingDingUtils.verifyDingDingMessage(pocessNew.getUserId(), pocessNew.getHeaderId());
							}
							index++;
						}
					}
				}
			}
		}
	}

	//内部审批通过
	public JSONObject rechargeSuccess(String acceptItemId){
		JSONObject json = new JSONObject();
		//预付款数据拼装
		BuyBillReconciliationAdvanceEdit billAdvanceEdit = new BuyBillReconciliationAdvanceEdit();
		billAdvanceEdit.setId(acceptItemId);
		billAdvanceEdit.setEditStatus("2");

		int updateEditCount = buyBillAdvanceEditMapper.updateAdvanceEdit(billAdvanceEdit);
		if(updateEditCount > 0){
			//根据编号查询付款详情
			Map<String,Object> queryEditMap = new HashMap<String,Object>();
			queryEditMap.put("id",acceptItemId);
			BuyBillReconciliationAdvanceEdit billAdvanceEditInfo = buyBillAdvanceEditMapper.queryAdvanceEdit(queryEditMap);
			String advanceId = billAdvanceEditInfo.getAdvanceId();
			BigDecimal updateTotal = billAdvanceEditInfo.getUpdateTotal();
			BigDecimal usableTotalOld = billAdvanceEditInfo.getUsableTotal();
			BigDecimal advanceTotalOld = billAdvanceEditInfo.getAdvanceTotal();
			//合计可用金额和总金额
			BigDecimal usableTotal = usableTotalOld.add(updateTotal);
			BigDecimal advanceTotal = advanceTotalOld.add(updateTotal);

			//更新买家预付款账单信息
			BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
			buyBillRecAdvance.setId(advanceId);
			buyBillRecAdvance.setUsableTotal(usableTotal);
			buyBillRecAdvance.setAdvanceTotal(advanceTotal);
			buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);

			//更新卖预付款账单信息
			BSellerBillReconciliationAdvance sellerAdvance = new BSellerBillReconciliationAdvance();
			sellerAdvance.setId(advanceId);
			sellerAdvance.setUsableTotal(usableTotal);
			sellerAdvance.setAdvanceTotal(advanceTotal);
			sellerBillRecAdvanceMapper.updateSellerAdvance(sellerAdvance);

			json.put("success", true);
			json.put("msg", "成功！");

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//内部付款审批驳回
	public JSONObject rechargeError(String acceptItemId){
		JSONObject json = new JSONObject();
		//预付款数据拼装
		BuyBillReconciliationAdvanceEdit billAdvanceEdit = new BuyBillReconciliationAdvanceEdit();
		billAdvanceEdit.setId(acceptItemId);
		billAdvanceEdit.setEditStatus("3");

		int updateEditCount = buyBillAdvanceEditMapper.updateAdvanceEdit(billAdvanceEdit);
		if(updateEditCount > 0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//内部审批回显查询
	public BuyBillReconciliationAdvanceEdit rechargeDetail(String acceptItemId){
		Map<String,Object> map = new HashMap<String,Object>();
		//根据编号查询付款详情
		Map<String,Object> queryEditMap = new HashMap<String,Object>();
		queryEditMap.put("id",acceptItemId);
		BuyBillReconciliationAdvanceEdit billAdvanceEditInfo = buyBillAdvanceEditMapper.queryAdvanceEdit(queryEditMap);
		return billAdvanceEditInfo;
	}

	//充值导出
	//根据条件查询预付款
	public List<BuyBillReconciliationAdvanceEdit> selAdvanceEditList(Map<String,Object> selMap){
		//根据条件查询预付款
		List<BuyBillReconciliationAdvanceEdit> advanceEditList = buyBillAdvanceEditMapper.selAdvanceEditList(selMap);
		return  advanceEditList;
	}

}
