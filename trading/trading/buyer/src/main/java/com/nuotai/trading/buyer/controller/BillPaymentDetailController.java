package com.nuotai.trading.buyer.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyBillReconciliationItem;
import com.nuotai.trading.buyer.model.BuyBillReconciliationPayment;
import com.nuotai.trading.buyer.service.BuyBillPaymentService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.UplaodUtil;
import com.nuotai.trading.utils.json.ServiceException;

//import com.nuotai.trading.seller.service.SellerBillCycleService;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-9-4
 */

@Controller
@RequestMapping("platform/buyer/billPaymentDetail")
public class BillPaymentDetailController extends BaseController {
	@Autowired
	private BuyBillPaymentService buyBillPaymentService;
	
	/**
	 * 发票列表查询
	 * @return
	 */
	@RequestMapping("billPaymentDetailList")
	public String billPaymentDetailList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("paymentStatus")){
			params.put("paymentStatus","0");
		}
		params.put("buyCompanyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		//列表查询
		List<BuyBillReconciliation> commodityBatchList = buyBillPaymentService.getRecToPamyment(searchPageUtil);

		buyBillPaymentService.queryPaymentCount(params);
		searchPageUtil.getPage().setList(commodityBatchList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billpaymentdetail/billPaymentDetailList";
	}

	/**
	 *付款
	 * @return
	 */
	@RequestMapping("updateSavePaymentInfo")
	@ResponseBody
	public String updateSavePaymentInfo(@RequestParam Map<String,Object> updateSaveMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = buyBillPaymentService.updateSavePayment(updateSaveMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "付款成功！");
			}else {
				json.put("error", false);
				json.put("msg", "付款失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 付款明细管理列表查询
	 * @return
	 */
	@RequestMapping("approveBillPayment")
	public String approveBillPayment(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		buyBillPaymentService.approveBillPayment(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billpaymentdetail/approveBillPayment";
	}

	// 审批一键通过
	@RequestMapping("verifyBillPaymentAll")
	@ResponseBody
	public String verifyBillRecAll(String acceptPaymentId){
		JSONObject json = buyBillPaymentService.verifyBillPaymentAll(acceptPaymentId);
		return json.toJSONString();
	}

	/**
	 *内部审批付款
	 * @return
	 */
	@RequestMapping("acceptBillPayment")
	@ResponseBody
	public void acceptBillPayment(@RequestParam final Map<String,Object> updateSaveMap){
		//Msg msg = new Msg();
		//Response res = new Response();
		//String paymentStatus = updateSaveMap.get("paymentStatus").toString();
		//if("4".equals(paymentStatus)){
			insert(updateSaveMap, new IService() {
				@Override
				public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billPaymentDetail/acceptPaymentSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billPaymentDetail/acceptPaymentError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billPaymentDetail/acceptPaymentDetail");//审批详细信息地址
					List<String> idList = buyBillPaymentService.acceptBillPayment(updateSaveMap);
					json.put("idList",idList);
					return json;
				}
			});
		/*}else if("6".equals(paymentStatus)){
			//更新的时候判断是否走审批，no的话不用走update里边的monitorUpdate方法，其他正常
			updateSaveMap.put("checkApprove","no");
			update(String.valueOf(updateSaveMap.get("acceptPaymentId")),updateSaveMap, new IService(){
				@Override
				public JSONObject init(Map<String,Object> params) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billPaymentDetail/acceptPaymentSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billPaymentDetail/acceptPaymentError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billPaymentDetail/acceptPaymentDetail");//审批详细信息地址
					List<String> idList = new ArrayList<>();
					idList.add(String.valueOf(updateSaveMap.get("acceptPaymentId")));
					json.put("idList", idList);
					buyBillPaymentService.acceptUpdateBillPayment(updateSaveMap);
					return json;
				}
			});
		}*/

	}

	//内部审批通过
	@RequestMapping(value = "/acceptPaymentSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String acceptPaymentSuccess(String id){
		JSONObject json = buyBillPaymentService.acceptPaymentSuccess(id);
		return json.toString();
	}

	//内部审批驳回
	@RequestMapping(value = "/acceptPaymentError", method = RequestMethod.GET)
	@ResponseBody
	public String acceptPaymentError(String id){
		JSONObject json = buyBillPaymentService.acceptPaymentError(id);
		return json.toString();
	}

	//审批数据回显
	@RequestMapping(value = "/acceptPaymentDetail",method = RequestMethod.GET)
	public String acceptPaymentDetail(String id){
		//付款审批数据查询
		BuyBillReconciliationPayment acceptPaymentInfo = buyBillPaymentService.queryPaymentDetail(id);
		if(null != acceptPaymentInfo){
			model.addAttribute("acceptPaymentInfo",acceptPaymentInfo);
		}
		return "platform/buyer/billcycles/billpaymentdetail/acceptPaymentDetail";
	}
	
	/**
	 * 获得付款信息
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getPaymentDetail",method = RequestMethod.GET)
	@ResponseBody
	public String getPaymentDetail(String id){
		//获得付款信息
		BuyBillReconciliationPayment acceptPaymentInfo = buyBillPaymentService.queryPaymentDetailNew(id);
		return acceptPaymentInfo==null?"{}":net.sf.json.JSONObject.fromObject(acceptPaymentInfo).toString();
	}

	/**
	 * 审批附件查看账单对账详情
	 * @param reconciliationId
	 * @return
	 */
	@RequestMapping("openRecItemList")
	public String openRecItemList(String reconciliationId){
		buyBillPaymentService.queryRecordAndReturnNew(reconciliationId,"",model);
		return "platform/buyer/billcycles/billpaymentdetail/openRecItemList";
	}

	/**
	 * Upload receipt attachment string.
	 * 上传付款票据
	 * @param files the files
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value="/uploadPaymentBill",method= RequestMethod.POST)
	@ResponseBody
	public String uploadPaymentBill(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
			throws Exception{
		JSONObject json = new JSONObject();
		try {
			List urlList = new ArrayList();
			for (MultipartFile file : files) {
				//获取文件名
				String fileName = file.getOriginalFilename();
				json.put("fileName", fileName);
				//获取文件后缀名
				String fileExtensionName = FilenameUtils.getExtension(fileName);
				if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
						&& !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
					json.put("result", "fail");
					json.put("msg", "上传文件格式不正确！");
					return json.toString();
				}
				//获取文件大小
				String fileSize = ShiroUtils.convertFileSize(file.getSize());
				UplaodUtil uplaodUtil = new UplaodUtil();
				String url = uplaodUtil.uploadFile(file,null,true);
				urlList.add(url);
			}
			json.put("urlList", urlList);
			json.put("result", "success");
		} catch (Exception e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		}
		return json.toString();
	}

	/**
	 * 付款单据详情
	 * @param request
	 * @return
	 */
	@RequestMapping("showRecItemInfo")
	public String showRecItemInfo(HttpServletRequest request){
		String reconciliationId = request.getParameter("reconciliationId").trim();
		//根据账单编号查询账单信息
		Map<String,Object> recItemMap = buyBillPaymentService.getRecItemInfo(reconciliationId);
		model.addAttribute("recItemMap", recItemMap);
		return "platform/buyer/billcycles/billpaymentdetail/showRecItemList";
	}

	/**
	 * 账单对账详情
	 * @param reconciliationId
	 * @return
	 */
	@RequestMapping("showRecItemListNew")
	public String showRecItemListNew(String reconciliationId,String paymentType){
		buyBillPaymentService.queryRecordAndReturnNew(reconciliationId,paymentType,model);
		return "platform/buyer/billcycles/billpaymentdetail/showRecItemListNew";
	}
	
	@RequestMapping("showRecItemListVerify")
	public String showRecItemListVerify(String id){
		buyBillPaymentService.queryRecordAndReturnNew(id,"2",model);
		return "platform/buyer/billcycles/billpaymentdetail/showRecItemListNew";
	}

	/**
	 * 账单详情查看票据
	 * @return
	 */
	@RequestMapping("queryFileUrlList")
	@ResponseBody
	public String queryFileUrlList(@RequestParam Map<String,Object> urlMap){
		JSONObject json = new JSONObject();
		String urlStrList = buyBillPaymentService.queryFileUrlList(urlMap);
		if(null != urlStrList && !"".equals(urlStrList)){
			model.addAttribute("urlStrList",urlStrList);
			json.put("urlList",urlStrList);
			json.put("success", true);
			json.put("msg", "账单附件加载成功！");
		}else if("".equals(urlStrList)){
			json.put("error", false);
			json.put("msg", "该账单未上传相关附件！");
		}else if(null == urlStrList) {
			json.put("error", false);
			json.put("msg", "该账单的相关附件加载失败，请重新打开！");
		}
		return json.toString();
	}

	/**
	 * 付款单据详情
	 * @param request
	 * @return
	 */
	@RequestMapping("showPaymentInfo")
	public String showPaymentInfo(HttpServletRequest request){
		String reconciliationId = request.getParameter("reconciliationId").trim();
		//根据账单编号查询账单信息
		List<BuyBillReconciliationPayment> billPaymentList = buyBillPaymentService.getPaymentList(reconciliationId);
		model.addAttribute("billPaymentList", billPaymentList);
		return "platform/buyer/billcycles/billpaymentdetail/showPaymentList";
	}

	/**
	 * 导出付款数据信息
	 * @param paymentMap
	 */
	@RequestMapping("buyPaymentExport")
	public void buyPaymentExport(@RequestParam Map<String,Object> paymentMap){
		paymentMap.put("buyCompanyId",ShiroUtils.getCompId());
		List<BuyBillReconciliation> buyPaymentList = buyBillPaymentService.queryPamymentList(paymentMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单付款报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"对账单号","采购员","供应商","应付款总额","已付款金额","应付款余额","出账开始日期","出账截止日期","确认状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("账单付款报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;

		if(null != buyPaymentList && buyPaymentList.size()>0){
			for(BuyBillReconciliation buyBillReconciliation : buyPaymentList){
				row=sheet.createRow(rowCount);
				//1.对账单号
				cell = row.createCell(0);
				cell.setCellValue(buyBillReconciliation.getId());
				cell.setCellStyle(normalStyle);
				//2.应付款总额
				cell = row.createCell(1);
				cell.setCellValue(buyBillReconciliation.getCreateBillUserName());
				cell.setCellStyle(normalStyle);
				//2.应付款总额
				cell = row.createCell(2);
				cell.setCellValue(buyBillReconciliation.getSellerCompanyName());
				cell.setCellStyle(normalStyle);
				//2.应付款总额
				cell = row.createCell(3);
				cell.setCellValue(fnum.format(buyBillReconciliation.getTotalAmount()));
				cell.setCellStyle(normalStyle);
				//3.已付款金额
				cell = row.createCell(4);
				cell.setCellValue(fnum.format(buyBillReconciliation.getActualPaymentAmount()));
				cell.setCellStyle(normalStyle);
				//4.应付款余额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(buyBillReconciliation.getResidualPaymentAmount()));
				cell.setCellStyle(normalStyle);
				//5.出账开始日期
				cell = row.createCell(6);
				cell.setCellValue(buyBillReconciliation.getStartBillStatementDateStr());
				cell.setCellStyle(normalStyle);
				//6.出账截止日期
				cell = row.createCell(7);
				cell.setCellValue(buyBillReconciliation.getEndBillStatementDateStr());
				cell.setCellStyle(normalStyle);
				//7.状态
				cell = row.createCell(8);
				String paymentStatus = buyBillReconciliation.getPaymentStatus();
				String statusStr = "";
				/*if("0".equals(paymentStatus)){
					statusStr = "待付款账单";
				}else*/ if("1".equals(paymentStatus)){
					statusStr = "待卖家确认收款";
				}else if("2".equals(paymentStatus)){
					statusStr = "卖家收款已确认";
				}else if("3".equals(paymentStatus)){
					statusStr = "付款待开票";
				}else if("4".equals(paymentStatus)){
					statusStr = "内部待审批";
				}else if("5".equals(paymentStatus)){
					statusStr = "审批已驳回";
				}
				cell.setCellValue(statusStr);
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("账单付款报表", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * 付款详情
	 * @param reconciliationId
	 */
	@RequestMapping("buyPaymentRecItemExport")
	public void buyPaymentRecItemExport(String reconciliationId){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,Object> recItemExportMap = buyBillPaymentService.queryPaymentRecItemExport(reconciliationId);
		BuyBillReconciliation buyBillReconciliation = (BuyBillReconciliation) recItemExportMap.get("buyBillReconciliation");
		Map<String,Map<String,Object>> daohuoMap = (Map<String, Map<String,Object>>) recItemExportMap.get("daohuoMap");
		Map<String,Map<String,Object>> recordReplaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("recordReplaceMap");
		Map<String,Map<String,Object>> replaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("replaceMap");
		Map<String,Map<String,Object>> customerMap = (Map<String, Map<String,Object>>) recItemExportMap.get("customerMap");

		// 第一步，创建一个webbook，对应一个Excel文件
		int rowaccess = 100;// 内存中缓存记录行数
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单详情报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第三步，在sheet中添加表头第0行
		Row row = sheet.createRow(0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		/*String[] cellHeadArray ={"账单开始日期","账单截止日期","供应商","到货数量","到货总金额","换货数量","换货总金额","退货数量","退货总金额","运费","总金额","状态"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}*/
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum()+1, row.getRowNum()+1, 1, 15);
		cell = row.createCell(0);
		String billDealStatus = buyBillReconciliation.getBillDealStatus();
		String dealStatusStr = "";
		if("2".equals(billDealStatus)){
			dealStatusStr = "待确认对账";
		}else if("3".equals(billDealStatus)){
			dealStatusStr = "已确认对账";
		}else if("4".equals(billDealStatus)){
			dealStatusStr = "已驳回对账";
		}
		cell.setCellValue("供应商:"+buyBillReconciliation.getSellerCompanyName()+"    出账周期:"+buyBillReconciliation.getStartBillStatementDateStr()+"至"+buyBillReconciliation.getEndBillStatementDateStr()+"    状态:"+dealStatusStr+"    奖惩金额:"+buyBillReconciliation.getCustomAmount());
		XSSFCellStyle borderStyle = (XSSFCellStyle)wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(headStyle);

		int rowCount = 1;
		//发货导出
		if(null != daohuoMap && daohuoMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = daohuoMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> daohuoMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				BigDecimal freight = (BigDecimal) daohuoMaps.get("freight");
				BigDecimal priceSum = (BigDecimal) daohuoMaps.get("itemPriceAndFreightSum");
				cell.setCellValue("订单类型:" + "采购" + "  运单号:" + daohuoMaps.get("waybillNo") + "  物流名称:" + daohuoMaps.get("logisticsCompany")
						+ "  司机名称:" + daohuoMaps.get("driverName") + "  运费:" + daohuoMaps.get("freight") + " 元" + "  总金额+运费:" + priceSum.add(freight));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "采购员","发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额","入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) daohuoMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.下单人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.下单人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(daohuoMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(daohuoMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//7.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//发货换货导出
		if(null != recordReplaceMap && recordReplaceMap.size()>0) {
			Collection values = recordReplaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> recReplaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				BigDecimal freight = (BigDecimal) recReplaceMaps.get("freight");
				BigDecimal priceSum = (BigDecimal) recReplaceMaps.get("itemPriceAndFreightSum");
				cell.setCellValue("订单类型:" + "采购" + "  运单号:" + recReplaceMaps.get("waybillNo") + "  物流名称:" + recReplaceMaps.get("logisticsCompany")
						+ "  司机名称:" + recReplaceMaps.get("driverName") + "  运费:" + recReplaceMaps.get("freight") + " 元" + "  总金额+运费:" + priceSum.add(freight));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "采购员","发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额","入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<BuyBillReconciliationItem> recReplaceItemList = (List<BuyBillReconciliationItem>) recReplaceMaps.get("itemList");
				if (null != recReplaceItemList && recReplaceItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : recReplaceItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.下单人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.下单人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(recReplaceMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(recReplaceMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//7.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//换货导出
		if(null != replaceMap && replaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = replaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> replaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				cell.setCellValue("订单类型:" + "换货" + "  总金额:" + replaceMaps.get("itemPriceSum"));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				//ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, row.getRowNum() + 1,  13);
				//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "发货单号", "到货日期", "到货总数量", "采购单价", "总金额"};
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "创建人", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) replaceMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.下单人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(replaceMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(replaceMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//7.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);

					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//退货导出
		if(null != customerMap && customerMap.size()>0) {
			Collection values = customerMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> customerMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				cell.setCellValue("订单类型:" + "退货" + "  总金额:" + customerMaps.get("itemPriceSum"));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				//ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, row.getRowNum() + 1,  13);
				//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "发货单号", "到货日期", "到货总数量", "采购单价", "对账单价", "总金额"};
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "创建人", "退货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<BuyBillReconciliationItem> customerItemList = (List<BuyBillReconciliationItem>) customerMaps.get("itemList");
				if (null != customerItemList && customerItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : customerItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.下单人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.创建人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(customerMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					cell.setCellValue(fnum.format(customerMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//7.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);

					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//第一行是订单信息
		row = sheet.createRow(rowCount);
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 13);
		cell = row.createCell(0);
		cell.setCellValue("总金额:" + buyBillReconciliation.getTotalAmount());
		XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(borderStyle1);

		if ((rowCount - 1) % rowaccess == 0) {
			try {
				((SXSSFSheet)sheet).flushRows();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		rowCount++;

		ExcelUtil.preExport("账单详情导出", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * 根据账单编号导出帐单详情简略信息
	 * @param reconciliationId
	 */
	@RequestMapping("buyPaymentRecItemRoughExport")
	public void buyPaymentRecItemRoughExport(String reconciliationId){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,Object> recItemExportMap = buyBillPaymentService.queryPaymentRecItemExport(reconciliationId);
		BuyBillReconciliation buyBillReconciliation = (BuyBillReconciliation) recItemExportMap.get("buyBillReconciliation");
		Map<String,Map<String,Object>> daohuoMap = (Map<String, Map<String,Object>>) recItemExportMap.get("daohuoMap");
		Map<String,Map<String,Object>> recordReplaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("recordReplaceMap");
		Map<String,Map<String,Object>> replaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("replaceMap");
		Map<String,Map<String,Object>> customerMap = (Map<String, Map<String,Object>>) recItemExportMap.get("customerMap");

		// 第一步，创建一个webbook，对应一个Excel文件
		int rowaccess = 100;// 内存中缓存记录行数
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单对账详情");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第三步，在sheet中添加表头第0行
		Row row = sheet.createRow(0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		/*String[] cellHeadArray ={"账单开始日期","账单截止日期","供应商","到货数量","到货总金额","换货数量","换货总金额","退货数量","退货总金额","运费","总金额","状态"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}*/
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum()+1, row.getRowNum()+1, 1, 15);
		cell = row.createCell(0);
		String billDealStatus = buyBillReconciliation.getBillDealStatus();
		String dealStatusStr = "";
		if("2".equals(billDealStatus)){
			dealStatusStr = "待确认对账";
		}else if("3".equals(billDealStatus)){
			dealStatusStr = "已确认对账";
		}else if("4".equals(billDealStatus)){
			dealStatusStr = "已驳回对账";
		}
		cell.setCellValue("供应商:"+buyBillReconciliation.getSellerCompanyName()+"    出账周期:"+buyBillReconciliation.getStartBillStatementDateStr()+"至"+buyBillReconciliation.getEndBillStatementDateStr()+"    状态:"+dealStatusStr+"    奖惩金额:"+buyBillReconciliation.getCustomAmount());
		XSSFCellStyle borderStyle = (XSSFCellStyle)wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(headStyle);

		int rowCount = 1;
		//因为第0行已经设置标题了,所以从行2开始写入数据
		//第一行是订单信息

		row = sheet.createRow(rowCount);
		//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额"};
		String[] cellHeadArray = {"采购订单号","创建人", "发货单号","货号", "商品名称", "规格代码", "规格名称", "条形码", "采购单价", "到货总数量", "总金额", "到货日期", "是否开票", "对账单价", "入库单号"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}

		rowCount++;
		//发货导出
		if(null != daohuoMap && daohuoMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = daohuoMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> daohuoMaps = (Map<String, Object>) object;


				List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) daohuoMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}
						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}
		//发货换货导出
		if(null != recordReplaceMap && recordReplaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = recordReplaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> recReplaceMaps = (Map<String, Object>) object;


				List<BuyBillReconciliationItem> recReplaceItemList = (List<BuyBillReconciliationItem>) recReplaceMaps.get("itemList");
				if (null != recReplaceItemList && recReplaceItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : recReplaceItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}
						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}
		//换货导出
		if(null != replaceMap && replaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = replaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> replaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) replaceMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}

				}
			}
		}
		//退货导出
		if(null != customerMap && customerMap.size()>0) {
			Collection values = customerMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> customerMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息

				List<BuyBillReconciliationItem> customerItemList = (List<BuyBillReconciliationItem>) customerMaps.get("itemList");
				if (null != customerItemList && customerItemList.size() > 0) {
					for (BuyBillReconciliationItem billRecItem : customerItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}

		ExcelUtil.preExport("账单货品数据导出", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * 付款明细导出
	 * @param reconciliationId
	 */
	@RequestMapping("buyPaymentItemExport")
	public  void  buyPaymentItemExport(String reconciliationId){

		String reportName = "付款明细报表";
		long curr_time = System.currentTimeMillis();
		int rowaccess = 100;// 内存中缓存记录行数
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);

		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(font2);
		String title[] = {"付款单号","付款金额","付款账号","付款类型","付款时间","备注","状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4000);
			}
		}
		int startRow = 2;
		/*map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		List<BuyApplypurchaseItem> apItemList = applypurchaseItemService.selectAllProductByNoPage(map);*/
		//根据账单编号查询账单信息
		List<BuyBillReconciliationPayment> billPaymentList = buyBillPaymentService.getPaymentList(reconciliationId);
		if (billPaymentList != null && billPaymentList.size() > 0) {
			for (int i = 0; i < billPaymentList.size(); i++) {
				BuyBillReconciliationPayment paymentItem = billPaymentList.get(i);
				row = sheet.createRow(startRow);//行

				// 付款单号
				Cell cell = row.createCell(0);
				cell.setCellValue(paymentItem.getId());
				cell.setCellStyle(cellStyle);

				// 付款金额
				cell = row.createCell(1);
				cell.setCellValue(fnum.format(paymentItem.getActualPaymentAmount()));
				cell.setCellStyle(cellStyle);

				//付款账号
				cell = row.createCell(2);
				cell.setCellValue(paymentItem.getBankAccount());
				cell.setCellStyle(cellStyle);

				//付款类型
				cell = row.createCell(3);
				String payType = paymentItem.getPaymentType();
				String payTypeStr = "";
				if("0".equals(payType)){
					payTypeStr = "现金";
				}else if("1".equals(payType)){
					payTypeStr = "银行转账";
				}else if("2".equals(payType)){
					payTypeStr = "支付宝";
				}else if("3".equals(payType)){
					payTypeStr = "微信";
				}
				cell.setCellValue(payTypeStr);
				cell.setCellStyle(cellStyle);

				//付款时间
				cell = row.createCell(4);
				cell.setCellValue(paymentItem.getPaymentDateStr());
				cell.setCellStyle(cellStyle);

				//备注
				cell = row.createCell(5);
				cell.setCellValue(paymentItem.getPaymentRemarks());
				cell.setCellStyle(cellStyle);

				//状态
				cell = row.createCell(6);
				String payStatus = paymentItem.getPaymentStatus();
				String payStatusStr = "";
				if("0".equals(payStatus)){
					payStatusStr = "付款待确认";
				}else if("1".equals(payStatus)){
					payStatusStr = "卖家待确认";
				}else if("2".equals(payStatus)){
					payStatusStr = "内部已驳回";
				}else if("3".equals(payStatus)){
					payStatusStr = "卖家已确认";
				}
				cell.setCellValue(payStatusStr);
				cell.setCellStyle(cellStyle);

				if ((startRow - 1) % rowaccess == 0) {
					try{
						((SXSSFSheet) sheet).flushRows();
					}catch (Exception e){
						e.printStackTrace();
					}
				}
				startRow++;
			}
		}

		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
}
