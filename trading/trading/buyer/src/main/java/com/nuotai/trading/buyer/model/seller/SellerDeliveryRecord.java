package com.nuotai.trading.buyer.model.seller;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.nuotai.trading.model.BuyAttachment;

/**
 * @author liuhui
 * @date 2017-09-20 16:08
 * @description
 **/
@Data
public class SellerDeliveryRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String id;
    //所属公司
    private String companyId;
    //发货单号
    private String deliverNo;
    //发货类型0:采购发货 1:换货发货
    private Integer deliverType;
    //供货商id
    private String supplierId;
    //供货商代码
    private String supplierCode;
    //供货商名称
    private String supplierName;
    //采购商公司ID
    private String buycompanyId;
    //采购商公司Name(外部)
    private String buycompanyName;
    //发货状态[0、全部发货；1、部分发货]
    private Integer status;
    //发货单[0待发货；1已发货；2已到货；3已作废]状态
    private Integer recordstatus;
    //是否已对账（0：未对账， 1：正在对帐，2：已完成对账）
    private String reconciliationStatus;
    //是否已经开票（0 未开票 1 正在开票 2 开票成功 3开票失败）
    private String isInvoiceStatus;
    //是否已付款（0：未付款， 1：正在付款，2：已付款）
    private String isPaymentStatus;
    //发货日期
    private Date sendDate;
    //到货日期
    private Date arrivalDate;
    //收货凭证地址
    private String receiptvoucherAddr;
    //收货凭证确认状态 0:未确认 1:已确认 2:驳回
    private String receiptvoucherCheck;
    //备注
    private String remark;
    //是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    //创建人id
    private String createrId;
    //创建人姓名
    private String createrName;
    //创建时间
    private Date createDate;
    //修改人id
    private String updateId;
    //修改人名称
    private String updateName;
    //修改时间
    private Date updateDate;
    //删除人id
    private String delId;
    //删除人名称
    private String delName;
    //删除时间
    private Date delDate;
	//物流信息ID
	private String logisticsId;
    //二维码地址
    private String qrcodeAddr;
    //代发类型（0.正常发货1.代发客户2.代发菜鸟仓3.代发京东仓）
    private String dropshipType;
	/**
	 *收货凭证附件
	 */
	private List<BuyAttachment> receiptVoucherList;
	/**
	 *是否已经推送WMS
	 */
	private String isSendWms;
    //收货地址
    private String receiveAddr;
    //发货明细
    List<SellerDeliveryRecordItem> itemList;
}
