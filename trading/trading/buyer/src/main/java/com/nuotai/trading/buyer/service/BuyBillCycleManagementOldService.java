package com.nuotai.trading.buyer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.dao.BuyBillCycleManagementOldMapper;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementOld;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillCycleManagementOldService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillCycleManagementOldService.class);

	@Autowired
	private BuyBillCycleManagementOldMapper buyBillCycleManagementOldMapper;

	//查询最近历史账单数据
	public BuyBillCycleManagementOld queryLastDateBillCycleOld(String id){
		return buyBillCycleManagementOldMapper.queryLastDateBillCycleOld(id);
	}
	
	public List<BuyBillCycleManagementOld> queryList(Map<String, Object> map){
		return buyBillCycleManagementOldMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillCycleManagementOldMapper.queryCount(map);
	}

	//添加历史账单数据
	public int addNewBillCycleOld(BuyBillCycleManagementOld buyBillCycleManagementOld){
		return  buyBillCycleManagementOldMapper.addNewBillCycleOld(buyBillCycleManagementOld);
	}
	
	public void update(BuyBillCycleManagementOld buyBillCycleManagementOld){
		buyBillCycleManagementOldMapper.update(buyBillCycleManagementOld);
	}
	//删除历史账单数据
	public int deleteNewBillCycleOld(String id){
		return buyBillCycleManagementOldMapper.deleteNewBillCycleOld(id);
	}
	

}
