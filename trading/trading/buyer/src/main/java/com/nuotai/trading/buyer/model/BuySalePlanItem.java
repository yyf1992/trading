package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;


/**
 * 
 * 销售计划明细表
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
@Data
public class BuySalePlanItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键id
	private String id;
	//主表id
	private String headerId;
	//店铺id
	private String shopId;
	//店铺code
	private String shopCode;
	//店铺名称
	private String shopName;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//销量
	private Integer salesNum;
	//入仓量
	private Integer putStorageNum;
	//商品类型 0成品1原材料2辅料3虚拟产品
    private int productType;
    //单位
    private String unitId;
    private String unitName;
    //店铺列表
    private List<Map<String ,Object>> shopItemList;
    // 已生成销售计划数
    private int confirmSalesNum;    
	//已销售数量
	private Integer num;
	//销售达成率
	private BigDecimal salesCompletionRate;
	
	//开始时间
	private Date startDate;
	//结束时间
	private Date endDate;
	
}
