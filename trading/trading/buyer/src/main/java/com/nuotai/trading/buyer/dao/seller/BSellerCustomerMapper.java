package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerCustomer;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 卖家售后
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
@Component
public interface BSellerCustomerMapper extends BaseDao<BSellerCustomer> {
	
	BSellerCustomer getByBuyCustomerId(String buyCustomerId);
	//根据对账状态修改售后
	int updateCustomerByReconciliation(Map<String, Object> map);
	//获取供应商处理售后时间
	List<Map<String, Object>> getSupplierAfterSaleRate(Map<String, Object> param);
}
