package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.nuotai.trading.buyer.model.BuyCustomerItem;
import com.nuotai.trading.dao.BaseDao;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-12 16:49:24
 */
@Component("buyCustomerItemMapper")
public interface BuyCustomerItemMapper extends BaseDao<BuyCustomerItem> {
	
	List<BuyCustomerItem> selectCustomerItemByCustomerId(String customerId);
	
	void deleteByCustomerId(String customerId);

	//根据对账条件查询售后详情
	List<BuyCustomerItem> queryCustomerItem(Map<String,Object> map);
	//获取退货数量
	List<Map<String,Object>> selectTuiHuoNum(Map<String, Object> param);

	int updateRealCount(Map<String, Object> itemMap);

	List<Map<String, Object>> getDeliveryByItemIdList(@Param("orderItemId")String orderItemId);
}
