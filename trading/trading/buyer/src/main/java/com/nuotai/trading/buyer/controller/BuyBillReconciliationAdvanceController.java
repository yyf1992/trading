package com.nuotai.trading.buyer.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillInvoice;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvance;
import com.nuotai.trading.buyer.service.BuyBillReconciliationAdvanceService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.seller.model.SellerBillReconciliation;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 * 
 * @author "
 * @date 2018-02-10 14:13:51
 */
@Controller
@RequestMapping("platform/buyer/billAdvance")
public class BuyBillReconciliationAdvanceController extends BaseController{
	@Autowired
	private BuyBillReconciliationAdvanceService buyBillRecAdvanceService;
	
	/**
	 * 列表
	 */
	@RequestMapping("billAdvanceList")
	public String billAdvanceList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("advanceStatus")){
			params.put("advanceStatus","0");
		}
		params.put("buyCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);

		List<BuyBillReconciliationAdvance> buyBillRecAdvanceList = buyBillRecAdvanceService.queryAdvanceList(searchPageUtil);
		buyBillRecAdvanceService.queryAdvanceCountByStatus(params);
		searchPageUtil.getPage().setList(buyBillRecAdvanceList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billadvance/billAdvanceList";
	}

	// 跳转到添加页面
	@RequestMapping("addAdvanceJump")
	public String addAdvanceJump(HttpServletRequest request){
		return "platform/buyer/billcycles/billadvance/addAdvance";
	}

	//根据供应商查询其所有下单人
	@RequestMapping("queryBillUser")
	@ResponseBody
	public String queryBillUser(@RequestParam Map<String,Object> queryBillUserMap){
		queryBillUserMap.put("companyId", ShiroUtils.getCompId());
		List<Map<String,Object>> createBillUserList = buyBillRecAdvanceService.queryBillUser(queryBillUserMap);
		return JSONObject.toJSONString(createBillUserList);
	}

	//根据供应商查询所有预付款账号
	@RequestMapping("queryAdvanceInfo")
	@ResponseBody
	public String queryAdvanceInfo(@RequestParam Map<String,Object> selAdvanceMap){
		selAdvanceMap.put("buyCompanyId",ShiroUtils.getCompId());
		List<BuyBillReconciliationAdvance> selAdvanceList = buyBillRecAdvanceService.selAdvanceList(selAdvanceMap);
		return JSONObject.toJSONString(selAdvanceList);
	}

	//根据公司编号和下单人编号查询是否存在
	@RequestMapping("isOrNoBillUser")
	@ResponseBody
	public String isOrNoBillUser(@RequestParam Map<String,Object> queryBillUserMap){
		queryBillUserMap.put("buyCompanyId", ShiroUtils.getCompId());
		JSONObject json = buyBillRecAdvanceService.isOrNoBillUser(queryBillUserMap);
		return json.toString();
	}

	//预付款信息保存
	@RequestMapping("saveAdvanceInfo")
	@ResponseBody
	public String saveAdvanceInfo(@RequestParam Map<String,Object> advanceMap){
		JSONObject json = buyBillRecAdvanceService.saveAdvanceInfo(advanceMap);
		return json.toString();
	}

	/**
	 * 付款明细管理列表查询
	 * @return
	 */
	@RequestMapping("approveBillAdvance")
	public String approveBillAdvance(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		buyBillRecAdvanceService.approveBillAdvance(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billadvance/approveBillAdvance";
	}

	// 审批一键通过
	@RequestMapping("verifyBillAdvanceAll")
	@ResponseBody
	public String verifyBillAdvanceAll(String acceptId){
		JSONObject json = buyBillRecAdvanceService.verifyBillAdvanceAll(acceptId);
		return json.toJSONString();
	}

	//内部审批
	@RequestMapping("acceptAdvance")
	@ResponseBody
	public void acceptAdvance(@RequestParam final Map<String,Object> updateSaveMap){
		String advanceStatus = updateSaveMap.get("advanceStatus").toString();
		if("1".equals(advanceStatus)){
			insert(updateSaveMap, new IService() {
				@Override
				public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billAdvance/acceptAdvanceSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billAdvance/acceptAdvanceError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billAdvance/acceptAdvanceDetail");//审批详细信息地址
					List<String> idList = buyBillRecAdvanceService.acceptAdvance(updateSaveMap);
					json.put("idList",idList);
					return json;
				}
			});
		}else if("4".equals(advanceStatus)){
			//更新的时候判断是否走审批，no的话不用走update里边的monitorUpdate方法，其他正常
			updateSaveMap.put("checkApprove","no");
			updateFuKuan(String.valueOf(updateSaveMap.get("acceptId")),updateSaveMap, new IService(){
				@Override
				public JSONObject init(Map<String,Object> params) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billAdvance/acceptAdvanceSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billAdvance/acceptAdvanceError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billAdvance/acceptAdvanceDetail");//审批详细信息地址
					List<String> idList = new ArrayList<>();
					idList.add(String.valueOf(updateSaveMap.get("acceptId")));
					json.put("idList", idList);
					buyBillRecAdvanceService.acceptUpdateAdvance(updateSaveMap);
					return json;
				}
			});
		}
	}

	//内部审批通过
	@RequestMapping(value = "/acceptAdvanceSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String acceptAdvanceSuccess(String id){
		JSONObject json = buyBillRecAdvanceService.acceptAdvanceSuccess(id);
		return json.toString();
	}

	//内部审批驳回
	@RequestMapping(value = "/acceptAdvanceError", method = RequestMethod.GET)
	@ResponseBody
	public String acceptAdvanceError(String id){
		JSONObject json = buyBillRecAdvanceService.acceptAdvanceError(id);
		return json.toString();
	}

	//审批数据回显
	@RequestMapping(value = "/acceptAdvanceDetail",method = RequestMethod.GET)
	public String acceptAdvanceDetail(String id){
		//预付款审批数据查询
		Map<String,Object> buyBillRecAdvance = buyBillRecAdvanceService.showAcceptAdvanceInfo(id);
		if(null != buyBillRecAdvance){
			model.addAttribute("buyBillRecAdvance",buyBillRecAdvance);
		}
		return "platform/buyer/billcycles/billadvance/acceptAdvanceDetailOld";
	}

	//发起卖家审批
	@RequestMapping("launchAdvance")
	@ResponseBody
	public String launchAdvance(String advanceId,String advanceStatus){
		JSONObject json = new JSONObject();
		try {
			int launchCount  = buyBillRecAdvanceService.launchAdvance(advanceId,advanceStatus);
			if(launchCount > 0){
				json.put("success", true);
				json.put("msg", "提交成功！");
			}else {
				json.put("error", false);
				json.put("msg", "提交失败！");
			}
		}catch (Exception e){
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	//充值记录
	@RequestMapping("updateAdvanceJump")
	public String updateAdvanceJump(HttpServletRequest request){
		String advanceId = request.getParameter("advanceId").toString();
		BuyBillReconciliationAdvance buyBillRecAdvance = buyBillRecAdvanceService.queryAcceptAdvanceInfo(advanceId);
		model.addAttribute("buyBillRecAdvance",buyBillRecAdvance);
		return "platform/buyer/billcycles/billadvance/updateAdvanceJump";
	}

	//修改预付款
	@RequestMapping("updateAdvanceInfo")
	@ResponseBody
	public String updateAdvanceInfo(@RequestParam Map<String, Object> updateMap){
		JSONObject json =buyBillRecAdvanceService.updateAdvanceInfo(updateMap);
		return json.toString();
	}

	//根据条件查询预付款修改记录
	@RequestMapping("queryAdvanceEditList")
	@ResponseBody
	public String queryAdvanceEditList(@RequestParam Map<String,Object> editMap){
		List<Map<String,Object>> advanceEditList = buyBillRecAdvanceService.queryAdvanceEditList(editMap);
		return JSONObject.toJSONString(advanceEditList);
	}

	//根据条件查询预付款明细
	@RequestMapping("selAdvanceChargeList")
	public String selAdvanceChargeList(@RequestParam Map<String,Object> selChargeMap){
		selChargeMap.put("editStatus","2");
		List<Map<String,Object>> advanceEditList = buyBillRecAdvanceService.queryAdvanceEditList(selChargeMap);
		model.addAttribute("advanceEditList", advanceEditList);
		model.addAttribute("params", selChargeMap);
		return "platform/buyer/billcycles/billadvance/billAdvanceChargeList";
	}

	/**
	 * 导出预付款数据
	 * @param invoiceMap
	 */
	@RequestMapping("buyerAdvanceExport")
	public void buyerAdvanceExport(@RequestParam Map<String,Object> invoiceMap){
		invoiceMap.put("buyCompanyId", ShiroUtils.getCompId());
		List<BuyBillReconciliationAdvance> advanceList = buyBillRecAdvanceService.selAdvanceList(invoiceMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("预付款账户报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"账户编号","供应商名称","采购员","可用金额","冻结金额","账户余额","开户日期","备注"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("预付款账户报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != advanceList && advanceList.size()>0){
			for(BuyBillReconciliationAdvance buyAdvanceInfo : advanceList){
				row=sheet.createRow(rowCount);
				//1.账户编号
				cell = row.createCell(0);
				cell.setCellValue(buyAdvanceInfo.getId());
				cell.setCellStyle(normalStyle);
				//2.供应商
				cell = row.createCell(1);
				cell.setCellValue(buyAdvanceInfo.getSellerCompanyName());
				cell.setCellStyle(normalStyle);
				//2.下单人
				cell = row.createCell(2);
				cell.setCellValue(buyAdvanceInfo.getCreateBillUserName());
				cell.setCellStyle(normalStyle);

				//5.可用金额
				cell = row.createCell(3);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getUsableTotal()));
				cell.setCellStyle(normalStyle);
				//6.锁定金额
				cell = row.createCell(4);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getLockTotal()));
				cell.setCellStyle(normalStyle);
				//7.总金额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getAdvanceTotal()));
				cell.setCellStyle(normalStyle);

				//3.添加日期
				cell = row.createCell(6);
				cell.setCellValue(buyAdvanceInfo.getCreateTimeStr());
				cell.setCellStyle(normalStyle);
				//4.添加备注
				cell = row.createCell(7);
				cell.setCellValue(buyAdvanceInfo.getCreateRemarks());
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("预付款账户信息", response);
		ExcelUtil.export(wb, response);
	}
}
