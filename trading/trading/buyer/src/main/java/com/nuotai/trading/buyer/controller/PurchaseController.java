package com.nuotai.trading.buyer.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 互通-向供应商下单
 * @author Administrator
 *
 */
@Controller
@RequestMapping("platform/buyer/purchase")
public class PurchaseController extends BaseController {
	@Autowired
	private BuyAddressService shippingAddressService;
	@Autowired
	private BuyOrderService buyOrderService;
	@Autowired
	private BuyOrderProductService buyOrderProductService;
	
	/**
	 * 向供应商采购商品(互通采购下单)
	 * @return
	 */
	@RequestMapping("addOrder")
	public String addOrder(@RequestParam Map<String,String> params){
		JSONObject json = null;
		model.addAttribute("numSum", params.containsKey("numSum")?params.get("numSum"):"0");
		model.addAttribute("totalPrice", params.containsKey("totalPrice")?params.get("totalPrice"):"0");
		if(params==null||params.isEmpty()){
			model.addAttribute("orderData", json);
			return "platform/buyer/interwork/purchase/addOrder";
		}
		json = new JSONObject();
		json.put("supplierId", params.get("supplierId"));
		json.put("supplierName", params.get("supplierName"));
		json.put("sellerPerson", params.get("sellerPerson"));
		json.put("sellerPhone", params.get("sellerPhone"));
		JSONArray itemArray = new JSONArray();
		String goodsStrArr = params.get("goodsStr");
		String[] goodsArr = goodsStrArr.split("@");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split(",");
			JSONObject itemJson = new JSONObject();
			/*skuOid+","+proCode+","+proName+","+skuCode+","+skuName+","
			+unitId+","+unitName+","+num+","+price+","+priceSum+","
			+wareId+","+wareName+","+invoice+","+remark+"@";*/
			itemJson.put("skuOid", goodsItem[0]);
			itemJson.put("proCode", goodsItem[1]);
			itemJson.put("proName", goodsItem[2]);
			itemJson.put("skuCode", goodsItem[3]);
			itemJson.put("skuName", goodsItem[4]);
			itemJson.put("unitId", goodsItem[5]);
			itemJson.put("unitName", goodsItem[6]);
			itemJson.put("num", goodsItem[7]);
			itemJson.put("price", goodsItem[8]);
			itemJson.put("priceSum", goodsItem[9]);
			itemJson.put("wareId", goodsItem[10]);
			itemJson.put("wareName", goodsItem[11]);
			itemJson.put("isNeedInvoice", goodsItem[12]);
			itemJson.put("remark", goodsItem.length==14?goodsItem[13]:"");
			itemArray.add(itemJson);
		}
		json.put("itemArray", itemArray);
		model.addAttribute("orderData", json);
		return "platform/buyer/interwork/purchase/addOrder";
	}
	
	/**
	 * 向供应商下单，下一步
	 * @param params
	 * @return
	 */
	@RequestMapping("next")
	public String next(@RequestParam Map<String,String> params){
		JSONObject json = new JSONObject();
		json.put("supplierId", params.get("supplierId"));
		json.put("supplierName", params.get("supplierName"));
		json.put("sellerPerson", params.get("sellerPerson"));
		json.put("sellerPhone", params.get("sellerPhone"));
		json.put("numSum", params.get("numSum"));
		json.put("totalPrice", params.get("totalPrice"));
		JSONArray itemArray = new JSONArray();
		String goodsStrArr = params.get("goodsStr");
		String[] goodsArr = goodsStrArr.split("@");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split(",");
			JSONObject itemJson = new JSONObject();
			/*skuOid+","+proCode+","+proName+","+skuCode+","+skuName+","
				+unitId+","+unitName+","+num+","+price+","+priceSum+","
				+wareId+","+wareName+","+invoice+","+remark+"@";*/
			itemJson.put("skuOid", goodsItem[0]);
			itemJson.put("proCode", goodsItem[1]);
			itemJson.put("proName", goodsItem[2]);
			itemJson.put("skuCode", goodsItem[3]);
			itemJson.put("skuName", goodsItem[4]);
			itemJson.put("unitId", goodsItem[5]);
			itemJson.put("unitName", goodsItem[6]);
			itemJson.put("num", goodsItem[7]);
			itemJson.put("price", goodsItem[8]);
			itemJson.put("priceSum", goodsItem[9]);
			itemJson.put("wareId", goodsItem[10]);
			itemJson.put("wareName", goodsItem[11]);
			itemJson.put("isNeedInvoice", goodsItem[12]);
			itemJson.put("remark", goodsItem.length==14?goodsItem[13]:"");
			itemArray.add(itemJson);
		}
		json.put("itemArray", itemArray);
		//查询收获地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue());
		addressParams.put("compId", ShiroUtils.getCompId());
		List<BuyAddress> addressList = shippingAddressService.selectByMap(addressParams);
		model.addAttribute("addressList", addressList);
		model.addAttribute("nextData", json);
		model.addAttribute("params", params);
		return "platform/buyer/interwork/purchase/nextOrder";
	}
	
	/**
	 * 向供应商下单
	 */
	@RequestMapping("saveOrderInsert")
	public void saveOrderInsert(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/purchase/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/purchase/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "platform/buyer/purchase/verifyDetail");//审批详细信息地址
				List<String> idList = buyOrderService.saveOrderNew(params);
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = buyOrderService.verifySuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = buyOrderService.verifyError(id);
		return json.toString();
	}
	
	
	/**
	 * 向供应商下单-保存
	 * @param params
	 * @return
	 */
	@RequestMapping("saveOrder")
	@ResponseBody
	public String saveOrder(@RequestParam Map<String,Object> params){
		JSONObject json = buyOrderService.saveOrder(params);
		return json.toString();
	}
	
	/**
	 * 下单成功页面
	 * @return
	 */
	@RequestMapping("success")
	public String success(){
		return "platform/buyer/interwork/purchase/success";
	}
	
	/**
	 * 查看详情
	 * @return
	 */
	@RequestMapping("detail")
	public String detail(String id){
		BuyOrder order = buyOrderService.selectByPrimaryKey(id);
		JSONObject json = getYstep(order);
		List<BuyOrderProduct> itemsList = buyOrderProductService.selectByOrderId(id);
		model.addAttribute("order", order);
		model.addAttribute("itemsList", itemsList);
		model.addAttribute("ystep", json);
		return "platform/buyer/interwork/purchase/detail";
	}
	
	/**
	 * 设置流程
	 * @param order
	 * @return
	 */
	private JSONObject getYstep(BuyOrder order) {
		if(order == null || "".equals(order.getId())){
			return null;
		}
		JSONObject result = new JSONObject();
		JSONArray stepArray = new JSONArray();
		JSONObject step1 = new JSONObject();
		step1.put("title", "提交订单");
		stepArray.add(step1);
		if(order.getIsCheck()==Constant.IsCheck.VERIFYNOTPASSED.getValue()){
			//内部审批-驳回
			JSONObject step2 = new JSONObject();
			step2.put("title", "内部审批被驳回");
			stepArray.add(step2);
			result.put("length", 2);
			result.put("flag", "内部审批被驳回");
			result.put("msg", "订单被审批驳回，您可以修改后再次提交");
			result.put("type", "1");
		}else if(order.getIsCheck()==Constant.IsCheck.ALREADYVERIFY.getValue()){
			//已审核
			JSONObject step2 = new JSONObject();
			step2.put("title", "内部已审批");
			stepArray.add(step2);
			if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//取消订单
				JSONObject step3 = new JSONObject();
				step3.put("title", "已取消");
				stepArray.add(step3);
				result.put("length", 3);
				result.put("flag", "已取消");
				result.put("msg", "订单被取消，订单完结");
				result.put("type", "1");
			}else if(order.getStatus()==Constant.OrderStatus.WAITORDER.getValue()){
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 3);
				result.put("flag", "待接单");
				result.put("msg", "订单已经推送至卖家，请耐心等待对方接单");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.WAITDELIVERY.getValue()){
				//待对方发货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 4);
				result.put("flag", "待发货");
				result.put("msg", "卖家已接单，请耐心等待对方发货");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.WAITRECEIVE.getValue()){
				//待收货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "卖家已发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 5);
				result.put("flag", "待收货");
				result.put("msg", "卖家已发货，请耐心等待到货");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.RECEIVED.getValue()){
				//已收货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "卖家已发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "已收货");
				stepArray.add(step5);
				result.put("length", 6);
				result.put("flag", "已收货");
				result.put("msg", "您已收货，订单完结");
				result.put("type", "2");
			}else if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//已取消
				JSONObject step3 = new JSONObject();
				step3.put("title", "已取消订单");
				stepArray.add(step3);
				result.put("length", 1);
				result.put("flag", "已取消订单");
				result.put("msg", "订单已取消");
				result.put("type", "2");
			}else if(order.getStatus()==Constant.OrderStatus.STOP.getValue()){
				//已终止
				JSONObject step3 = new JSONObject();
				step3.put("title", "已终止订单");
				stepArray.add(step3);
				result.put("length", 1);
				result.put("flag", "已终止订单");
				result.put("msg", "订单已终止");
				result.put("type", "2");
			}else{
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 3);
				result.put("flag", "待接单");
				result.put("msg", "订单已经推送至卖家，请耐心等待对方接单");
				result.put("type", "0");
			}
		}else{
			if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//已取消
				JSONObject step2 = new JSONObject();
				step2.put("title", "已取消订单");
				stepArray.add(step2);
				result.put("length",2);
				result.put("flag", "已取消订单");
				result.put("msg", "订单已取消");
				result.put("type", "2");
			}else {
				JSONObject step2 = new JSONObject();
				step2.put("title", "待内部审批");
				stepArray.add(step2);
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 2);
				result.put("flag", "待审批");
				result.put("msg", "订单正在进行内部审批，审批通过后会自动推送至卖家");
				result.put("type", "0");
			}

		}
		result.put("stepArray", stepArray);
		return result;
	}

	/**
	 * 审批查看详情
	 * @return
	 */
	@RequestMapping("verifyDetail")
	public String verifyDetail(String id){
		BuyOrder order = buyOrderService.selectByPrimaryKey(id);
		JSONObject json = getYstep(order);
		List<BuyOrderProduct> itemsList = buyOrderProductService.selectByOrderId(id);
		model.addAttribute("order", order);
		model.addAttribute("itemsList", itemsList);
		model.addAttribute("ystep", json);
		return "platform/buyer/interwork/purchase/verifyDetail";
	}
}
