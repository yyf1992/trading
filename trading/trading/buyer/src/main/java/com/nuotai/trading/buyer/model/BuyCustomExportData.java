package com.nuotai.trading.buyer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;
@Data
public class BuyCustomExportData {
	//售后单号
	private String customerCode;
	//订单时间
	private Date createDate;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格名称
	private String skuName;
	//条形码
	private String skuOid;
	//售后数量
	private Integer goodsNumber;
	//oms出库数量
	private Integer receiveNumber;
	//卖家收货数量
	private Integer confirmNumber;
	//售后类型  0-换货，1-退款退货
	private String type;
	//转换类型[0-未转换, 1-换货转退货]
	private String transformType;
	//申请原因
	private String reason;
	//创建人名称
	private String createName;
	//发货详情
	private String deliveryDetial;
    //发货数量
    private Integer deliveryNum;
    //到货数量
    private Integer arrivalNum;
    //供应商
    private String supplierName;
    //最近到货日期
    private String arrivalDate;
    //oms出库日期
    private Date omsFinishdate;
	//价格
	private BigDecimal price;
	//换货单价
	private BigDecimal exchangePrice;
	//返修金额
	private BigDecimal repairPrice;
}
