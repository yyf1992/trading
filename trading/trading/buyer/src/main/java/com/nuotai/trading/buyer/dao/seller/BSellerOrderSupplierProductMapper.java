package com.nuotai.trading.buyer.dao.seller;

import java.util.List;

import com.nuotai.trading.buyer.model.seller.SellerOrderSupplierProduct;
import com.nuotai.trading.dao.BaseDao;
import org.apache.ibatis.annotations.Param;

/**
 * The interface B seller order supplier product mapper.
 */
public interface BSellerOrderSupplierProductMapper extends BaseDao<SellerOrderSupplierProduct>{
    /**
     * Delete by primary key int.
     *
     * @param id the id
     * @return the int
     */
    int deleteByPrimaryKey(String id);

    /**
     * Delete by order id.
     *
     * @param orderId the order id
     */
    void deleteByOrderId(String orderId);

    /**
     * Add arrival num.
     * 新增到货数量
     * @param sellerOrderItemId the seller order item id
     * @param addNum        the addNum num
     */
    void addArrivalNum(@Param("sellerOrderItemId") String sellerOrderItemId,@Param("addNum") Integer addNum);
    
    /**
     * 根据orderId查询
     * @param orderId
     * @return
     */
    List<SellerOrderSupplierProduct> selectByOrderId(String orderId);
    

    /**
     * Select by primary key seller order supplier product.
     *
     * @param id the id
     * @return the seller order supplier product
     */
    SellerOrderSupplierProduct selectByPrimaryKey(String id);
    
    /**
     * Update delivered num.
     *
     * @param id          the id
     * @param deliveryNum the delivery num
     */
    void updateDeliveredNum(@Param("id") String id,@Param("deliveryNum") int deliveryNum);
}