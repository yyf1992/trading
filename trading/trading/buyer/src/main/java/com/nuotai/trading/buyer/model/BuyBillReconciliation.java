package com.nuotai.trading.buyer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class BuyBillReconciliation {
    private String id;

    private String paymentNo;

    private String companyId;
    
    private String buyCompanyName;

    private String sellerCompanyId;

    private String sellerCompanyName;

    private String createBillUserId;

    private String createBillUserName;

    private Date startBillStatementDate;
    
    private Date createDate;

    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	private Date endBillStatementDate;

    private String startBillStatementDateStr;

    private String endBillStatementDateStr;

    private int recordConfirmCount;
    
    private BigDecimal recordConfirmTotal;

    private int recordReplaceCount;

    private BigDecimal recordReplaceTotal;

    private int replaceConfirmCount;

    private BigDecimal replaceConfirmTotal;

    private BigDecimal freightSumCount;

    private int returnGoodsCount;
    
    private BigDecimal returnGoodsTotal;

    private int totalSumCount;

    private BigDecimal totalAmount;

    private BigDecimal advanceDeductTotal;//预付款金额

    private int startTotalAmount;

    private int endTotalAmount;

    private String status;

	private String billDealStatus;

    private String customStatus;

    private BigDecimal customAmount;

    private String sellerDealStatus;

    private String isFullInvoice;

    private String paymentStatus;

    private String bankAccount;

    private String invoiceFileStr;

    private String buyerReconciliationRemarks;

    private String sellerReconciliationRemarks;

    private String buyerPaymentRemarks;

    private String sellerPaymentRemarks;

    private String acceptPaymentId;

    private String paymentType;

    private String paymentFileAddr;

    private String paymentAddr;

    private BigDecimal actualPaymentAmount;

    private BigDecimal residualPaymentAmount;

    private String purchaseId;//采购部审批编号

    private String purchaseAddId;//内部审批添加编号

    private String purchaseCreateUserId;//采购部审批发起人

    private String purchaseUpdateUserId;//采购部审核人

    private String purchaseUpdateUserName;//采购部审批人名称

    private String purchaseApprovalTimeStr;//采购部审批创建时间

    private Date purchaseApprovalTime;

    private String purchaseApprovalRemarks;//采购部审批创建备注

    private String financeId;//财务部审批编号

    private String financeUpdateUserName;//财务部审批人名称

    private String financeCreateUserId;//财务部审批发起人

    private String financeUpdateUserId;//财务部审批人

    private String financeApprovalTimeStr;//财务部审批创建时间

    private Date financeApprovalTime;

    private String financeApprovalRemarks;//财务部审批创建备注
    
    private String orderId;

    private Map<String,Map<String,Object>> daohuoMap;//账单详情
    private Map<String,Map<String,Object>> recordReplaceMap;//账单详情
    private Map<String,Map<String,Object>> replaceMap;//账单详情
    private Map<String,Map<String,Object>> customerMap;//账单详情

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getBuyCompanyName() {
        return buyCompanyName;
    }

    public void setBuyCompanyName(String buyCompanyName) {
        this.buyCompanyName = buyCompanyName;
    }

    public String getSellerCompanyId() {
        return sellerCompanyId;
    }

    public void setSellerCompanyId(String sellerCompanyId) {
        this.sellerCompanyId = sellerCompanyId;
    }

    public String getSellerCompanyName() {
        return sellerCompanyName;
    }

    public void setSellerCompanyName(String sellerCompanyName) {
        this.sellerCompanyName = sellerCompanyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRecordConfirmCount() {
        return recordConfirmCount;
    }

    public void setRecordConfirmCount(int recordConfirmCount) {
        this.recordConfirmCount = recordConfirmCount;
    }

    public int getReturnGoodsCount() {
        return returnGoodsCount;
    }

    public void setReturnGoodsCount(int returnGoodsCount) {
        this.returnGoodsCount = returnGoodsCount;
    }

    public BigDecimal getRecordConfirmTotal() {
        return recordConfirmTotal;
    }

    public void setRecordConfirmTotal(BigDecimal recordConfirmTotal) {
        this.recordConfirmTotal = recordConfirmTotal;
    }

    public BigDecimal getReturnGoodsTotal() {
        return returnGoodsTotal;
    }

    public void setReturnGoodsTotal(BigDecimal returnGoodsTotal) {
        this.returnGoodsTotal = returnGoodsTotal;
    }

    public Date getStartBillStatementDate() {
        return startBillStatementDate;
    }

    public void setStartBillStatementDate(Date startBillStatementDate) {
        this.startBillStatementDate = startBillStatementDate;
    }

    public Date getEndBillStatementDate() {
        return endBillStatementDate;
    }

    public void setEndBillStatementDate(Date endBillStatementDate) {
        this.endBillStatementDate = endBillStatementDate;
    }

    public String getBillDealStatus() {
        return billDealStatus;
    }

    public void setBillDealStatus(String billDealStatus) {
        this.billDealStatus = billDealStatus;
    }

    public String getSellerDealStatus() {
        return sellerDealStatus;
    }

    public void setSellerDealStatus(String sellerDealStatus) {
        this.sellerDealStatus = sellerDealStatus;
    }

    public int getStartTotalAmount() {
        return startTotalAmount;
    }

    public void setStartTotalAmount(int startTotalAmount) {
        this.startTotalAmount = startTotalAmount;
    }

    public int getEndTotalAmount() {
        return endTotalAmount;
    }

    public void setEndTotalAmount(int endTotalAmount) {
        this.endTotalAmount = endTotalAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStartBillStatementDateStr() {
        return startBillStatementDateStr;
    }

    public void setStartBillStatementDateStr(String startBillStatementDateStr) {
        this.startBillStatementDateStr = startBillStatementDateStr;
    }

    public String getEndBillStatementDateStr() {
        return endBillStatementDateStr;
    }

    public void setEndBillStatementDateStr(String endBillStatementDateStr) {
        this.endBillStatementDateStr = endBillStatementDateStr;
    }

    public int getReplaceConfirmCount() {
        return replaceConfirmCount;
    }

    public void setReplaceConfirmCount(int replaceConfirmCount) {
        this.replaceConfirmCount = replaceConfirmCount;
    }

    public BigDecimal getReplaceConfirmTotal() {
        return replaceConfirmTotal;
    }

    public void setReplaceConfirmTotal(BigDecimal replaceConfirmTotal) {
        this.replaceConfirmTotal = replaceConfirmTotal;
    }

    public int getTotalSumCount() {
        return totalSumCount;
    }

    public void setTotalSumCount(int totalSumCount) {
        this.totalSumCount = totalSumCount;
    }

    public BigDecimal getFreightSumCount() {
        return freightSumCount;
    }

    public void setFreightSumCount(BigDecimal freightSumCount) {
        this.freightSumCount = freightSumCount;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentFileAddr() {
        return paymentFileAddr;
    }

    public void setPaymentFileAddr(String paymentFileAddr) {
        this.paymentFileAddr = paymentFileAddr;
    }

    public BigDecimal getResidualPaymentAmount() {
        return residualPaymentAmount;
    }

    public void setResidualPaymentAmount(BigDecimal residualPaymentAmount) {
        this.residualPaymentAmount = residualPaymentAmount;
    }

    public String getIsFullInvoice() {
        return isFullInvoice;
    }

    public void setIsFullInvoice(String isFullInvoice) {
        this.isFullInvoice = isFullInvoice;
    }

    public BigDecimal getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(BigDecimal actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getInvoiceFileStr() {
        return invoiceFileStr;
    }

    public void setInvoiceFileStr(String invoiceFileStr) {
        this.invoiceFileStr = invoiceFileStr;
    }

    public String getBuyerPaymentRemarks() {
        return buyerPaymentRemarks;
    }

    public void setBuyerPaymentRemarks(String buyerPaymentRemarks) {
        this.buyerPaymentRemarks = buyerPaymentRemarks;
    }

    public String getSellerPaymentRemarks() {
        return sellerPaymentRemarks;
    }

    public void setSellerPaymentRemarks(String sellerPaymentRemarks) {
        this.sellerPaymentRemarks = sellerPaymentRemarks;
    }

    public String getPaymentAddr() {
        return paymentAddr;
    }

    public void setPaymentAddr(String paymentAddr) {
        this.paymentAddr = paymentAddr;
    }

    public String getBuyerReconciliationRemarks() {
        return buyerReconciliationRemarks;
    }

    public void setBuyerReconciliationRemarks(String buyerReconciliationRemarks) {
        this.buyerReconciliationRemarks = buyerReconciliationRemarks;
    }

    public String getSellerReconciliationRemarks() {
        return sellerReconciliationRemarks;
    }

    public void setSellerReconciliationRemarks(String sellerReconciliationRemarks) {
        this.sellerReconciliationRemarks = sellerReconciliationRemarks;
    }

    public String getCreateBillUserId() {
        return createBillUserId;
    }

    public void setCreateBillUserId(String createBillUserId) {
        this.createBillUserId = createBillUserId;
    }

    public String getCreateBillUserName() {
        return createBillUserName;
    }

    public void setCreateBillUserName(String createBillUserName) {
        this.createBillUserName = createBillUserName;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getPurchaseApprovalRemarks() {
        return purchaseApprovalRemarks;
    }

    public void setPurchaseApprovalRemarks(String purchaseApprovalRemarks) {
        this.purchaseApprovalRemarks = purchaseApprovalRemarks;
    }

    public String getFinanceId() {
        return financeId;
    }

    public void setFinanceId(String financeId) {
        this.financeId = financeId;
    }

    public String getFinanceApprovalTimeStr() {
        return financeApprovalTimeStr;
    }

    public void setFinanceApprovalTimeStr(String financeApprovalTimeStr) {
        this.financeApprovalTimeStr = financeApprovalTimeStr;
    }

    public String getFinanceApprovalRemarks() {
        return financeApprovalRemarks;
    }

    public void setFinanceApprovalRemarks(String financeApprovalRemarks) {
        this.financeApprovalRemarks = financeApprovalRemarks;
    }

    public String getPurchaseApprovalTimeStr() {
        return purchaseApprovalTimeStr;
    }

    public void setPurchaseApprovalTimeStr(String purchaseApprovalTimeStr) {
        this.purchaseApprovalTimeStr = purchaseApprovalTimeStr;
    }

    public Date getPurchaseApprovalTime() {
        return purchaseApprovalTime;
    }

    public void setPurchaseApprovalTime(Date purchaseApprovalTime) {
        this.purchaseApprovalTime = purchaseApprovalTime;
    }

    public Date getFinanceApprovalTime() {
        return financeApprovalTime;
    }

    public void setFinanceApprovalTime(Date financeApprovalTime) {
        this.financeApprovalTime = financeApprovalTime;
    }

    public String getPurchaseCreateUserId() {
        return purchaseCreateUserId;
    }

    public void setPurchaseCreateUserId(String purchaseCreateUserId) {
        this.purchaseCreateUserId = purchaseCreateUserId;
    }

    public String getPurchaseUpdateUserId() {
        return purchaseUpdateUserId;
    }

    public void setPurchaseUpdateUserId(String purchaseUpdateUserId) {
        this.purchaseUpdateUserId = purchaseUpdateUserId;
    }

    public String getFinanceCreateUserId() {
        return financeCreateUserId;
    }

    public void setFinanceCreateUserId(String financeCreateUserId) {
        this.financeCreateUserId = financeCreateUserId;
    }

    public String getFinanceUpdateUserId() {
        return financeUpdateUserId;
    }

    public void setFinanceUpdateUserId(String financeUpdateUserId) {
        this.financeUpdateUserId = financeUpdateUserId;
    }

    public String getPurchaseUpdateUserName() {
        return purchaseUpdateUserName;
    }

    public void setPurchaseUpdateUserName(String purchaseUpdateUserName) {
        this.purchaseUpdateUserName = purchaseUpdateUserName;
    }

    public String getFinanceUpdateUserName() {
        return financeUpdateUserName;
    }

    public void setFinanceUpdateUserName(String financeUpdateUserName) {
        this.financeUpdateUserName = financeUpdateUserName;
    }

    public String getCustomStatus() {
        return customStatus;
    }

    public void setCustomStatus(String customStatus) {
        this.customStatus = customStatus;
    }

    public BigDecimal getCustomAmount() {
        return customAmount;
    }

    public void setCustomAmount(BigDecimal customAmount) {
        this.customAmount = customAmount;
    }

    public String getAcceptPaymentId() {
        return acceptPaymentId;
    }

    public void setAcceptPaymentId(String acceptPaymentId) {
        this.acceptPaymentId = acceptPaymentId;
    }

    public int getRecordReplaceCount() {
        return recordReplaceCount;
    }

    public void setRecordReplaceCount(int recordReplaceCount) {
        this.recordReplaceCount = recordReplaceCount;
    }

    public BigDecimal getRecordReplaceTotal() {
        return recordReplaceTotal;
    }

    public void setRecordReplaceTotal(BigDecimal recordReplaceTotal) {
        this.recordReplaceTotal = recordReplaceTotal;
    }

    public BigDecimal getAdvanceDeductTotal() {
        return advanceDeductTotal;
    }

    public void setAdvanceDeductTotal(BigDecimal advanceDeductTotal) {
        this.advanceDeductTotal = advanceDeductTotal;
    }

    public String getPurchaseAddId() {
        return purchaseAddId;
    }

    public void setPurchaseAddId(String purchaseAddId) {
        this.purchaseAddId = purchaseAddId;
    }

    public Map<String, Map<String, Object>> getDaohuoMap() {
        return daohuoMap;
    }

    public void setDaohuoMap(Map<String, Map<String, Object>> daohuoMap) {
        this.daohuoMap = daohuoMap;
    }

    public Map<String, Map<String, Object>> getRecordReplaceMap() {
        return recordReplaceMap;
    }

    public void setRecordReplaceMap(Map<String, Map<String, Object>> recordReplaceMap) {
        this.recordReplaceMap = recordReplaceMap;
    }

    public Map<String, Map<String, Object>> getReplaceMap() {
        return replaceMap;
    }

    public void setReplaceMap(Map<String, Map<String, Object>> replaceMap) {
        this.replaceMap = replaceMap;
    }

    public Map<String, Map<String, Object>> getCustomerMap() {
        return customerMap;
    }

    public void setCustomerMap(Map<String, Map<String, Object>> customerMap) {
        this.customerMap = customerMap;
    }
}