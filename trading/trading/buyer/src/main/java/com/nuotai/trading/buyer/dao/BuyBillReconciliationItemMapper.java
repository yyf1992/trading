package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillReconciliationItem;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-10-18 11:06:21
 */
@Component
public interface BuyBillReconciliationItemMapper extends BaseDao<BuyBillReconciliationItem> {

    //添加对账详情
    int addRecItem(BuyBillReconciliationItem buyBillReconciliationItem);
    //修改对账单价
    int updateRecItemPrice(Map<String,Object> updateMap);
    //根据账单编号查询对账详情
    List<BuyBillReconciliationItem> queryRecItemList(Map<String,Object> updateMap);
    //根据订单号查询附件
    List<Map<String,Object>> queryRecItemAddr(String deliverNo);
    //根据订单号查询售后附件
    Map<String,Object> queryCustomerProof(String deliverNo);
    //根据账单编号查询对账详情
    List<BuyBillReconciliationItem> queryRecItemListByInvoiceId(Map<String,Object> updateMap);
    //根据详情编号查询
    BuyBillReconciliationItem queryRecItem(String id);
    //根据发货、退货编号查询详情数据量
    int queryRecItemCount(Map<String,Object> map);
}
