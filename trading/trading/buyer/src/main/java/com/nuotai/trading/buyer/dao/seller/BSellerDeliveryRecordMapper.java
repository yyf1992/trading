package com.nuotai.trading.buyer.dao.seller;/**
 * @author liuhui
 * @date 2017-09-20 16:16
 * @description
 **/

import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecord;
import com.nuotai.trading.dao.BaseDao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 *
 * @author liuhui
 * @create 2017-09-20 16:16
 **/
@Component
public interface BSellerDeliveryRecordMapper extends BaseDao<SellerDeliveryRecord> {
    void updateByPrimaryKeySelective(SellerDeliveryRecord sellerDeliveryRecord);
    /**
     * 根据条件修改订单对账状态
     * @param map
     * @return
     */
    int updateDeliveryByReconciliation(Map<String,Object> map);
	int deleteById(@Param("id")String id);
    /**
     * Add qrcode addr.
     * 更新发货单二维码
     * @param deliverNo  the deliver no
     * @param qrcodeAddr the qrcode addr
     */
    int addQrcodeAddr(@Param("deliverNo") String deliverNo,@Param("qrcodeAddr") String qrcodeAddr);
    
	int insertSelective(SellerDeliveryRecord sellerDeliveryRecord);
}
