package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysPurchasePrice;
import com.nuotai.trading.dao.BaseDao;


/**
 * 商品采购价格表
 * 
 * @author "
 * @date 2017-10-25 14:32:58
 */
public interface SysPurchasePriceMapper extends BaseDao<SysPurchasePrice> {
	//通过公司id查询已经设置了采购价格的商品
	List <String> selectExitProductId(String compId);

	int insertPurchase(SysPurchasePrice purchasePrice);
	//加载采购报表
	List <Map<String, Object>> getPurchaseData(Map<String, String> map);

	List<SysPurchasePrice> selectByMap(Map<String, Object> attrMap);

	int updatePurchase(SysPurchasePrice purchase);

	int deletePurchase(String productId);
}
