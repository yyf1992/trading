package com.nuotai.trading.buyer.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.buyer.dao.OmsMonthSalesMapper;
import com.nuotai.trading.buyer.model.OmsMonthSales;



@Service
@Transactional
public class OmsMonthSalesService {

    private static final Logger LOG = LoggerFactory.getLogger(OmsMonthSalesService.class);

	@Autowired
	private OmsMonthSalesMapper omsMonthSalesMapper;
	
	public OmsMonthSales get(String id){
		return omsMonthSalesMapper.get(id);
	}
	
	public List<OmsMonthSales> queryList(Map<String, Object> map){
		return omsMonthSalesMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return omsMonthSalesMapper.queryCount(map);
	}
	
	public void add(OmsMonthSales omsMonthSales){
		omsMonthSalesMapper.add(omsMonthSales);
	}
	
	public void update(OmsMonthSales omsMonthSales){
		omsMonthSalesMapper.update(omsMonthSales);
	}
	
	public void delete(String id){
		omsMonthSalesMapper.delete(id);
	}
	
	public List<OmsMonthSales> selectByBarcode(String barcode){
		return omsMonthSalesMapper.selectByBarcode(barcode);
	}
	
}
