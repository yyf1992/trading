package com.nuotai.trading.buyer.dao;

import java.util.List;

import com.nuotai.trading.buyer.model.OmsMonthSales;
import com.nuotai.trading.dao.BaseDao;


/**
 * 
 * 
 * @author dxl"
 * @date 2018-03-19 17:24:29
 */
public interface OmsMonthSalesMapper extends BaseDao<OmsMonthSales> {
	List<OmsMonthSales> selectByBarcode(String barcode);
	
	//清除表内数据
	void deleteAll();
}
