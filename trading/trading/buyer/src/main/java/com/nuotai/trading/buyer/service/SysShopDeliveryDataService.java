package com.nuotai.trading.buyer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.SysShopDeliveryDataMapper;
import com.nuotai.trading.buyer.model.SysShopDeliveryData;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




@Service
@Transactional
public class SysShopDeliveryDataService {

    private static final Logger LOG = LoggerFactory.getLogger(SysShopDeliveryDataService.class);

	@Autowired
	private SysShopDeliveryDataMapper sysShopDeliveryDataMapper;
	@Autowired
	private SysShopService shopService;
	
	public SysShopDeliveryData get(String id){
		return sysShopDeliveryDataMapper.get(id);
	}
	
	public List<SysShopDeliveryData> queryList(Map<String, Object> map){
		return sysShopDeliveryDataMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysShopDeliveryDataMapper.queryCount(map);
	}
	
	public void add(SysShopDeliveryData sysShopDeliveryData){
		sysShopDeliveryDataMapper.add(sysShopDeliveryData);
	}
	
	public void update(SysShopDeliveryData sysShopDeliveryData){
		sysShopDeliveryDataMapper.update(sysShopDeliveryData);
	}
	
	public void delete(String id){
		sysShopDeliveryDataMapper.delete(id);
	}
	
	//新增发货取数店铺
	public JSONObject saveInsertShop(String shopId) {
		// TODO Auto-generated method stub
		JSONObject json=validateShop(shopId);
		if(!json.getBooleanValue("success")){
			return json;
		}
		SysShop sysShop=shopService.selectByPrimaryKey(shopId);
		int result=0;
		SysShopDeliveryData deliveryData=new SysShopDeliveryData();
		deliveryData.setPlatformId(sysShop.getPlatformId());
		deliveryData.setShopId(sysShop.getId());
		deliveryData.setShopCode(sysShop.getShopCode());
		deliveryData.setShopName(sysShop.getShopName());
		deliveryData.setId(ShiroUtils.getUid());
		deliveryData.setCompanyId(ShiroUtils.getCompId());
		deliveryData.setCreateDate(new Date());
		deliveryData.setCreateId(ShiroUtils.getUserId());
		deliveryData.setCreateName(ShiroUtils.getUserName());
		result=sysShopDeliveryDataMapper.insert(deliveryData);
		if(result>0){
			json.put("success", true);
			json.put("msg", "新增成功！");
		}else{
			json.put("success", false);
			json.put("msg", "新增失败！");
		}
		return json;
	}
	
	private JSONObject validateShop(String shopId) {
		// TODO Auto-generated method stub
		JSONObject json=new JSONObject();
		Map map=new HashMap<String, Object>();
		map.put("shopId", shopId);
		List<Map<String,Object>> list=sysShopDeliveryDataMapper.selectByMap(map);
		if(list.size()>0){
			json.put("success", false);
			json.put("msg", "店铺已存在！");
			return json;
		}
		
		json.put("success", true);
		json.put("msg", "");
		
		return json;
	}

	public List<Map<String, Object>> selectByMap(Map map) {
		// TODO Auto-generated method stub
		return sysShopDeliveryDataMapper.selectByMap(map);
	}

	public List<Map<String, Object>> selectByCompanyId(
			SearchPageUtil searchPageUtil) {
		// TODO Auto-generated method stub
		return sysShopDeliveryDataMapper.selectByCompanyId(searchPageUtil);
	}

	public JSONObject deleteShop(String id) {
		// TODO Auto-generated method stub
		JSONObject json=new JSONObject();
		int result=0;
		result=sysShopDeliveryDataMapper.deleteShop(id);
		if(result>0){
			json.put("success", true);
			json.put("msg", "删除成功！");
		}
		else{
			json.put("success", false);
			json.put("msg", "删除失败！");
		}
		return json;
	}

	public List<SysShopDeliveryData> getShopByMap(Map<String,Object> map) {
		// TODO Auto-generated method stub
		return sysShopDeliveryDataMapper.getShopByMap(map);
	}
	
	public List<String> selectShopId(String companyId) {
		// TODO Auto-generated method stub
		return sysShopDeliveryDataMapper.selectShopId(companyId);
	}
	

}
