package com.nuotai.trading.buyer.timer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.OmsMonthSalesMapper;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.OmsMonthSales;
import com.nuotai.trading.buyer.model.seller.BSellerCustomer;
import com.nuotai.trading.buyer.service.BuyCustomerService;
import com.nuotai.trading.buyer.service.OmsMonthSalesService;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 抓取oms商品本月销量定时器
 * @author dxl
 *
 */
@Component
public class OMSMonthSalesTimer {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private OmsMonthSalesMapper omsMonthSalesMapper;
	
	@Scheduled(cron = "0 0 1 * * ?")//每天凌晨1点执行
	//@Scheduled(cron = "0 */5 * * * ?")// 每隔5分钟
	public void omsMonthSalesNum(){
		TimeTask timeTask = timeTaskService.selectByCode("MONTH_SALES");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					try {
						this.grabMonthSales();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setCurrentStatus("2");
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	/**
	 * 抓取本月销量
	 */
	public void grabMonthSales() throws Exception{
		//本月销量取值，规则：15号之前算上个月整月的，15号之后算本月1号到当前时间
		// 开始时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date currentTime = new Date();
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        String startDate;
		String endDate;
		
        if(day < 15){//当前时间小于15号，取上个月1到到月底
            //获取前月的第一天
            Calendar cal_1=Calendar.getInstance();//获取当前日期 
            cal_1.add(Calendar.MONTH, -1);
            cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
            startDate = sdf.format(cal_1.getTime());
            
            //获取前月的最后一天
            Calendar cale = Calendar.getInstance();   
            cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天 
            endDate = sdf.format(cale.getTime());
        }else{
        	c.add(Calendar.MONTH, 0);
	        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
            startDate = sdf.format(c.getTime());
            
    		// 结束时间(当前时间)
    		endDate = sdf.format(currentTime);
        }
		
		if(endDate.contains("-")){
			endDate = endDate.replaceAll("-", "");
		}
		if(startDate.contains("-")){
        	startDate = startDate.replaceAll("-", "");
		}
		String omsUrl = Constant.OMS_INTERFACE_URL + "getOmsSalesReportByMap?startDate="+startDate+"&endDate="+endDate;
		String shopSalesString = InterfaceUtil.searchLoginService(omsUrl);
		net.sf.json.JSONObject shopSalesJson = net.sf.json.JSONObject.fromObject(shopSalesString);
		System.out.println("********抓取oms本月销量开始**********");
		if(!shopSalesJson.isNullObject()){
			String shopSalesListStr = shopSalesJson.getString("list");
			net.sf.json.JSONArray shopSalesArray = net.sf.json.JSONArray.fromObject(shopSalesListStr);
			List<net.sf.json.JSONObject> shopSalesList = (List<net.sf.json.JSONObject>) 
					net.sf.json.JSONArray.toCollection(shopSalesArray, net.sf.json.JSONObject.class);
			
			if(shopSalesList != null && shopSalesList.size() > 0){
				//同步前清空库存子表数据
				omsMonthSalesMapper.deleteAll();
				for(net.sf.json.JSONObject shopSales : shopSalesList){
					String productCode = shopSales.getString("procode");
					String productName = shopSales.getString("proname");
					String skuName = shopSales.getString("skuname");
					String barcode = shopSales.getString("skuoid");
					int num = shopSales.getInt("num");
					OmsMonthSales oms = new OmsMonthSales();
					oms.setId(ShiroUtils.getUid());
					oms.setProductCode(productCode);
					oms.setProductName(productName);
					oms.setSkuName(skuName);
					oms.setBarcode(barcode);
					oms.setNum(num);
					oms.setStartDate(startDate);
					oms.setEndDate(endDate);
					omsMonthSalesMapper.add(oms);
				}
			}
		}
		System.out.println("********抓取oms本月销量结束**********");
	}
}
