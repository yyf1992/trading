package com.nuotai.trading.buyer.service.seller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.buyer.dao.BuyDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.BuyDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.BuyOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderProductMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerLogisticsMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderSupplierProductMapper;
import com.nuotai.trading.buyer.model.BuyDeliveryRecord;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.model.seller.BSellerLogistics;
import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecord;
import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecordItem;
import com.nuotai.trading.buyer.model.seller.SellerOrder;
import com.nuotai.trading.buyer.model.seller.SellerOrderSupplierProduct;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.dao.WmsInputplanLogMapper;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.model.wms.WmsInputplanLog;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.qrcode.QRCodeUtil;
import com.oms.client.OMSResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Transactional
public class BSellerDeliveryRecordService {
	
	private static final Logger log = LoggerFactory.getLogger(BSellerDeliveryRecordService.class);
	
	@Autowired
	private BuyOrderService buyOrderService;
	@Autowired
	private BuyOrderMapper buyOrderMapper;
	@Autowired
	private BSellerLogisticsMapper bSellerLogisticsMapper;
	@Autowired
	private BSellerDeliveryRecordMapper bSellerDeliveryRecordMapper;
	@Autowired
	private BSellerDeliveryRecordItemMapper bSellerDeliveryRecordItemMapper;
	@Autowired
	private BSellerOrderMapper bSellerOrderMapper;
	@Autowired
	private BSellerOrderSupplierProductMapper bSellerOrderSupplierProductMapper;
	@Autowired
	private BuyDeliveryRecordMapper buyDeliveryRecordMapper;
	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;
	@Autowired
	private BuyOrderProductMapper buyOrderProductMapper;
	@Autowired
	private WmsInputplanLogMapper wmsInputplanLogMapper;
	@Autowired
	private TBusinessWhareaMapper tBusinessWhareaMapper;
	@Autowired
	private SysUserMapper sysUserMapper;
	
	/**
	 * 原材料采购创建发货单,发货
	 * @param params the params
	 * @throws Exception the exception
	 */
	public void saveDeliveryRecord(String orderId,Boolean deliveryAgain) throws Exception{
		BuyOrder order = buyOrderService.selectByPrimaryKey(orderId);
		SellerOrder sellerOrder = bSellerOrderMapper.getByBuyerOrderId(orderId);
		SysUser sysUser=sysUserMapper.selectByPrimaryKey(order.getCreateId());
		//保存物流信息
		String logisticsId = this.saveLogistics();
		SellerDeliveryRecord sellerDeliveryRecord = new SellerDeliveryRecord();
		sellerDeliveryRecord.setId(ShiroUtils.getUid());
		sellerDeliveryRecord.setCompanyId(order.getSuppId());
		sellerDeliveryRecord.setDeliverNo(ShiroUtils.getDeliveryNum());
		sellerDeliveryRecord.setDeliverType(0);//发货类型（0:采购发货 1:换货发货）
		sellerDeliveryRecord.setSupplierId(order.getSuppId());
		sellerDeliveryRecord.setSupplierName(order.getSuppName());
		sellerDeliveryRecord.setBuycompanyId(sysUser.getCompanyId());
		sellerDeliveryRecord.setStatus(0);
		sellerDeliveryRecord.setRecordstatus(Constant.DeliveryRecordStatus.DELIVERED.getValue());
		sellerDeliveryRecord.setReconciliationStatus("0");
		sellerDeliveryRecord.setIsInvoiceStatus("0");
		sellerDeliveryRecord.setIsPaymentStatus("0");
		sellerDeliveryRecord.setSendDate(new Date());
		sellerDeliveryRecord.setCreaterId(ShiroUtils.getUserId());
		sellerDeliveryRecord.setCreaterName(ShiroUtils.getUserName());
		sellerDeliveryRecord.setCreateDate(new Date());
		sellerDeliveryRecord.setLogisticsId(logisticsId);
		sellerDeliveryRecord.setDropshipType("0");
		sellerDeliveryRecord.setReceiveAddr(order.getProvince()+" "+order.getCity()+" "+order.getArea() +" "+order.getAddrName()+" "+order.getPersonName());
		//保存发货单主表
		int res= bSellerDeliveryRecordMapper.insertSelective(sellerDeliveryRecord);
		if(res>0){
			List<String> deliverNoList = new ArrayList<>();
			deliverNoList.add(sellerDeliveryRecord.getDeliverNo());
			//保存发货单子表
			List<SellerOrderSupplierProduct> sellerOrderSupplierProductList = bSellerOrderSupplierProductMapper.selectByOrderId(sellerOrder.getId());
			List<SellerDeliveryRecordItem> bSellerDeliveryRecordItemList =new ArrayList<>();
			for (SellerOrderSupplierProduct sellerOrderSupplierProduct : sellerOrderSupplierProductList) {
				SellerDeliveryRecordItem bSellerDeliveryRecordItem = new SellerDeliveryRecordItem();
				bSellerDeliveryRecordItem.setId(ShiroUtils.getUid());
				bSellerDeliveryRecordItem.setRecordId(sellerDeliveryRecord.getId());
				bSellerDeliveryRecordItem.setOrderId(sellerOrderSupplierProduct.getOrderId());
				bSellerDeliveryRecordItem.setOrderItemId(sellerOrderSupplierProduct.getId());
				bSellerDeliveryRecordItem.setBuyOrderId(sellerOrder.getBuyerOrderId());
				bSellerDeliveryRecordItem.setBuyOrderItemId(sellerOrderSupplierProduct.getBuyOrderItemId());
				bSellerDeliveryRecordItem.setApplyCode(sellerOrderSupplierProduct.getApplyCode());
				bSellerDeliveryRecordItem.setProductCode(sellerOrderSupplierProduct.getProductCode());
				bSellerDeliveryRecordItem.setProductCode(sellerOrderSupplierProduct.getProductName());
				bSellerDeliveryRecordItem.setSkuCode(sellerOrderSupplierProduct.getSkuCode());
				bSellerDeliveryRecordItem.setSkuName(sellerOrderSupplierProduct.getSkuName());
				bSellerDeliveryRecordItem.setBarcode(sellerOrderSupplierProduct.getBarcode());
				bSellerDeliveryRecordItem.setUnitId(sellerOrderSupplierProduct.getUnitId());
				bSellerDeliveryRecordItem.setSalePrice(sellerOrderSupplierProduct.getPrice());
				bSellerDeliveryRecordItem.setWarehouseId(sellerOrderSupplierProduct.getWarehouseId());
				bSellerDeliveryRecordItem.setWarehouseCode("");
				bSellerDeliveryRecordItem.setWarehouseName("");
				bSellerDeliveryRecordItem.setWarehouseHolder("");
				if(deliveryAgain){
					int deliveriedNum=buyDeliveryRecordItemMapper.countArrivalNum(orderId);
					bSellerDeliveryRecordItem.setDeliveryNum(sellerOrderSupplierProduct.getOrderNum()-deliveriedNum);
				}else{
					bSellerDeliveryRecordItem.setDeliveryNum(sellerOrderSupplierProduct.getOrderNum());//本次发货数量
				}
				bSellerDeliveryRecordItem.setIsNeedInvoice(sellerOrderSupplierProduct.getIsNeedInvoice());
				bSellerDeliveryRecordItem.setRemark(sellerOrderSupplierProduct.getRemark());
				bSellerDeliveryRecordItemList.add(bSellerDeliveryRecordItem);
				bSellerDeliveryRecordItemMapper.add(bSellerDeliveryRecordItem);
			}
			/**********生成发货单二维码***********/
			String content = Constant.TRADE_URL+"platform/buyer/receive/openQRcode?deliverNo=";
			//目标地址
			String destPath = Constant.QRCODE_SAVE_URL;
			for (String fhNo:deliverNoList) {
				try{
					System.out.println("content=["+content+fhNo+"]");
					String fineName = QRCodeUtil.encode(content+fhNo,"",destPath,true);
					String qrcodeAddr = Constant.QRCODE_VIEW_URL+fineName;
					if(!ObjectUtil.isEmpty(qrcodeAddr)){
						bSellerDeliveryRecordMapper.addQrcodeAddr(fhNo,qrcodeAddr);
					}
				}catch (Exception e) {
					System.out.println("发货单号："+fhNo+"创建二维码失败！");
					log.error("发货单号："+fhNo+"创建二维码失败！"+e.getMessage());
				}
			}
			
			if(sellerDeliveryRecord.getDeliverType()==0){
				if(!ObjectUtil.isEmpty(bSellerDeliveryRecordItemList)&&bSellerDeliveryRecordItemList.size()>0){
					for (SellerDeliveryRecordItem deliveryItem : bSellerDeliveryRecordItemList) {
						//本次发货数量
						int deliveryNum = ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum();
						//更新卖家订单详情中的已发货数量
						bSellerOrderSupplierProductMapper.updateDeliveredNum(deliveryItem.getBuyOrderItemId(),deliveryNum);
						System.out.println("卖家已发货数量更新完成！————————————————————");
						//更新买家订单详情中的已发货数量
						BuyOrderProduct buyOrderProduct = buyOrderProductMapper.selectByPrimaryKey(deliveryItem.getBuyOrderItemId());
						if(!ObjectUtil.isEmpty(buyOrderProduct)){
							buyOrderProductMapper.updateDeliveredNum(buyOrderProduct.getOrderId(),deliveryNum);
							System.out.println("买家已发货数量更新完成！————————————————————");
						}
					}
				}
				//更新卖家订单主表中的状态为发货完成
				sellerOrder.setStatus(3);
				bSellerOrderMapper.updateByPrimaryKeySelective(sellerOrder);
				System.out.println("卖家订单状态更新为发货完成！————————————————————");
				//更新买家订单主表中的状态为待收货状态
				BuyOrder o = new BuyOrder();
				o.setId(orderId);
				o.setStatus(3);
				buyOrderMapper.updateByPrimaryKeySelective(o);
				System.out.println("买家订单状态更新为待收货！————————————————————");
			}
			
			//保存发货信息到买家
			this.saveBuyDeliveryRecord(sellerDeliveryRecord, bSellerDeliveryRecordItemList);
			System.out.println("发货信息已保存至买家！————————————————————");
			
			ExecutorService exec = Executors.newSingleThreadExecutor();
			try{
				// 向wms推送入库申请单
				sellerDeliveryRecord.setItemList(bSellerDeliveryRecordItemList);
				Future<String> future = exec.submit(new PutInputPlanToWms(sellerDeliveryRecord));
				//任务处理超时时间设为 10s
				String rst = future.get(1000 * 10, TimeUnit.MILLISECONDS);
				System.out.println("向wms推送入库申请单结果"+rst);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				log.error("入库计划推送到WMS超时["+sellerDeliveryRecord.getDeliverNo()+"]");
			}
		}
	}
	
	/**
	 * 保存物流信息
	 *
	 * @param params the params
	 */
	public String saveLogistics(){
		BSellerLogistics logistics = new BSellerLogistics();
		logistics.setId(ShiroUtils.getUid());
		logistics.setDeliveryId("");
		logistics.setIsDel(Constant.IsDel.NODEL.getValue());
		logistics.setCreateDate(new Date());
		logistics.setCreateId(ShiroUtils.getUserId());
		logistics.setCreateName(ShiroUtils.getUserName());
		bSellerLogisticsMapper.add(logistics);
		return logistics.getId();
	}
	
	/**
	 * 保存发货信息到采购商
	 *
	 * @param sellerRecord   the seller record
	 * @param sellerItemList the seller item list
	 */
	public void saveBuyDeliveryRecord(SellerDeliveryRecord sellerRecord,List<SellerDeliveryRecordItem> bSellerDeliveryRecordItemList){
		//保存收货单主表
		BuyDeliveryRecord buyDeliveryRecord = new BuyDeliveryRecord();
		buyDeliveryRecord.setId(ShiroUtils.getUid());
		buyDeliveryRecord.setCompanyId(sellerRecord.getBuycompanyId());
		buyDeliveryRecord.setDeliverId(sellerRecord.getId());
		buyDeliveryRecord.setDeliverNo(sellerRecord.getDeliverNo());
		buyDeliveryRecord.setDeliverType(sellerRecord.getDeliverType());
		buyDeliveryRecord.setSupplierId(sellerRecord.getSupplierId());
		buyDeliveryRecord.setSupplierCode(sellerRecord.getSupplierCode());
		buyDeliveryRecord.setSupplierName(sellerRecord.getSupplierName());
		buyDeliveryRecord.setStatus(null);
		buyDeliveryRecord.setRecordstatus(0);
		buyDeliveryRecord.setReconciliationStatus(sellerRecord.getReconciliationStatus());
		buyDeliveryRecord.setIsInvoiceStatus(sellerRecord.getIsInvoiceStatus());
		buyDeliveryRecord.setIsPaymentStatus(sellerRecord.getIsPaymentStatus());
		buyDeliveryRecord.setReceiptvoucherAddr(sellerRecord.getReceiptvoucherAddr());
		buyDeliveryRecord.setReceiptvoucherCheck("0");
		buyDeliveryRecord.setRemark(sellerRecord.getRemark());
		buyDeliveryRecord.setLogisticsId(sellerRecord.getLogisticsId());
		buyDeliveryRecord.setQrcodeAddr(sellerRecord.getQrcodeAddr());
		buyDeliveryRecord.setIsDel(0);
		buyDeliveryRecord.setCreaterId(ShiroUtils.getUserId());
		buyDeliveryRecord.setCreateDate(new Date());
		buyDeliveryRecord.setCreaterName(ShiroUtils.getUserName());
		buyDeliveryRecord.setDropshipType(sellerRecord.getDropshipType());
		buyDeliveryRecord.setReceiveAddr(sellerRecord.getReceiveAddr());
		buyDeliveryRecordMapper.add(buyDeliveryRecord);
		//保存收货单分录表
		for (SellerDeliveryRecordItem sellerItem: bSellerDeliveryRecordItemList) {
			BuyDeliveryRecordItem buyDeliveryRecordItem = new BuyDeliveryRecordItem();
			SellerOrderSupplierProduct orderProduct = bSellerOrderSupplierProductMapper.selectByPrimaryKey(sellerItem.getOrderItemId());
			buyDeliveryRecordItem.setId(sellerItem.getId());
			buyDeliveryRecordItem.setRecordId(buyDeliveryRecord.getId());
			buyDeliveryRecordItem.setOrderId(sellerItem.getBuyOrderId());
			buyDeliveryRecordItem.setOrderItemId(sellerItem.getBuyOrderItemId());
			buyDeliveryRecordItem.setDeliveryItemId(sellerItem.getId());
			buyDeliveryRecordItem.setApplyCode(sellerItem.getApplyCode());
			buyDeliveryRecordItem.setProductCode(sellerItem.getProductCode());
			buyDeliveryRecordItem.setProductName(sellerItem.getProductName());
			buyDeliveryRecordItem.setSkuCode(sellerItem.getSkuCode());
			buyDeliveryRecordItem.setSkuName(sellerItem.getSkuName());
			buyDeliveryRecordItem.setBarcode(sellerItem.getBarcode());
			buyDeliveryRecordItem.setUnitId(sellerItem.getUnitId());
			buyDeliveryRecordItem.setSalePrice(sellerItem.getSalePrice());
			buyDeliveryRecordItem.setWarehouseId(sellerItem.getWarehouseId());
			buyDeliveryRecordItem.setWarehouseCode("");
			buyDeliveryRecordItem.setWarehouseName("");
			buyDeliveryRecordItem.setDeliveryNum(sellerItem.getDeliveryNum());
			buyDeliveryRecordItem.setDeliveredNum(sellerItem.getDeliveredNum());
			buyDeliveryRecordItem.setArrivalNum(0);
			buyDeliveryRecordItem.setStatus(sellerItem.getStatus());
			buyDeliveryRecordItem.setIsNeedInvoice(sellerItem.getIsNeedInvoice());
			buyDeliveryRecordItem.setRemark(sellerItem.getRemark());
			buyDeliveryRecordItemMapper.add(buyDeliveryRecordItem);
		}
	}

	/**
	 * Send wms input plan.
	 * 推送WMS入库计划，用于确认发货时推送失败的数据
	 * @param deliverNo the delivery no.
	 */
	public void sendWmsInputPlan (String deliverNo) throws Exception {
		Map<String,Object> map = new HashMap<>(2);
		WmsInputplanLog log = wmsInputplanLogMapper.getByDeliverNo(deliverNo);
		if(!ObjectUtil.isEmpty(log)){
			OMSResponse result = InterfaceUtil.NuotaiWmsClient(log.getMethod(),log.getToken(), net.sf.json.JSONObject.fromObject(log.getContent()));
			System.out.println("result="+result.isSuccess());
			if (result.isSuccess()) {
				log.setResult("Y");
				log.setErrorMsg("");
			}else {
				log.setInterfaceUrl(Constant.WMS_INTERFACE_URL);
				log.setErrorMsg(result.getMsg());
			}
			wmsInputplanLogMapper.update(log);
		}
	}

	static class TestCall implements Callable<String>{

		@Override
		public String call() throws Exception {
			Map<String,Object> map = new HashMap<>();
			net.sf.json.JSONObject header = new net.sf.json.JSONObject();
			header.put("title","采购入库");
			header.put("deliverycode","FHtest2");
			header.put("suppliername","liuhui1");
			map.put("header",header);
			map.put("whcode","49");
			map.put("creater", "liuhui");
			// 设置json中date类型转换格式
			List<net.sf.json.JSONObject> list = new ArrayList<>();
			net.sf.json.JSONObject detail = new net.sf.json.JSONObject();
			detail.put("procode", "001");
			detail.put("proname", "护膝");
			detail.put("skucode", "XL");
			detail.put("skuname", "大号");
			detail.put("skuoid", "bar0001");
			detail.put("warehouseCode", "49");
			list.add(detail);
			map.put("iteamList", list );
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(map);
			net.sf.json.JSONObject headerJson = jsonObj.getJSONObject("header");
			net.sf.json.JSONArray itemListJson = jsonObj.getJSONArray("iteamList");
			// 调用接口
			OMSResponse result = InterfaceUtil.NuotaiWmsClient("client/addDeliveryrecord","123", jsonObj);
			System.out.println("result:"+result);
			System.out.println("result.isSuccess()="+result.isSuccess());
			return result.toString();
		}
	}
	/**
	 * The type Put input plan to wms.
	 * 推送发货单到WMS
	 * 如果发货单明细中存在京东仓和菜鸟仓，则入库计划要去除掉该明细
	 */
	class PutInputPlanToWms implements Callable<String>{
		private SellerDeliveryRecord deliveryRecord;
		private String rst="";
		public PutInputPlanToWms(SellerDeliveryRecord deliveryRecord) {
			this.deliveryRecord = deliveryRecord;
		}
		@Override
		public String call() {
			//日志
			WmsInputplanLog log = new WmsInputplanLog();
			log.setId(ShiroUtils.getUid());
			//总Map
			Map<String, Object> map = new HashMap<String, Object>(4);
			//创建人
			String creater = "";
			//仓库编号
			String whcode = "";
			//组装入库申请单表头
			net.sf.json.JSONObject header = new net.sf.json.JSONObject();
			if(!ObjectUtil.isEmpty(deliveryRecord)){
				header.put("title",deliveryRecord.getDeliverType()==0?"采购入库":"买卖系统换货入库");
				header.put("deliverycode",deliveryRecord.getDeliverNo());
				header.put("suppliername",deliveryRecord.getSupplierName());
			}
			map.put("header", header);
			//发货详情
			List<SellerDeliveryRecordItem> itemList = deliveryRecord.getItemList();
			/***************判断代发货类型,正常发货根据发货明细的仓库处理，否则推送到代发仓********************/
			if(!ObjectUtil.isEmpty(itemList)&&itemList.size()>0){
				String dropshipType = ObjectUtil.isEmpty(deliveryRecord.getDropshipType())?"0":deliveryRecord.getDropshipType();
				if("0".equals(dropshipType)){
					//判断仓库是外仓，无需推送
					boolean isSend = true;
					for (SellerDeliveryRecordItem item:itemList){
						String warehouseId = item.getWarehouseId();
						TBusinessWharea warehouse = tBusinessWhareaMapper.getWharea(warehouseId);
						String warehouseCode = "";
						if(!ObjectUtil.isEmpty(warehouse)&&!ObjectUtil.isEmpty(warehouse.getWhareaCode())){
							warehouseCode = warehouse.getWhareaCode();
						}
						whcode = warehouseCode;
						if(ObjectUtil.isEmpty(warehouseCode)||"CN0001".equalsIgnoreCase(warehouseCode)||"JD0001".equalsIgnoreCase(warehouseCode)){
							isSend = false;
							break;
						}
					}
					if(!isSend){
						return "仓库是外仓，无需推送WMS...";
					}
				}else {
					whcode = "0004";//代发仓编号，这里写死
				}

			}
			Map<String,net.sf.json.JSONObject> goodsMap = new HashMap<String,net.sf.json.JSONObject>();
			//移除掉京东仓和菜鸟仓
			for (SellerDeliveryRecordItem sdItem : itemList) {
				//获取采购订单，组装创建人
				if(deliveryRecord.getDeliverType()==0){
					//采购
					BuyOrder bo = buyOrderMapper.selectByPrimaryKey(sdItem.getBuyOrderId());
					if(!ObjectUtil.isEmpty(bo)&&!ObjectUtil.isEmpty(bo.getCreateName())&&!creater.contains(bo.getCreateName())){
						creater += bo.getCreateName()+",";
					}
				}
				//组装入库计划明细
				String skuoid = sdItem.getBarcode();
				long skucount = sdItem.getDeliveryNum();
				net.sf.json.JSONObject detail = new net.sf.json.JSONObject();
				if(goodsMap.containsKey(skuoid)){
					detail = goodsMap.get(skuoid);
					skucount += detail.getLong("skucount");
				}else{
					detail.put("procode", sdItem.getProductCode());
					detail.put("proname", sdItem.getProductName());
					detail.put("skucode", sdItem.getSkuCode());
					detail.put("skuname", sdItem.getSkuName());
					detail.put("skuoid", sdItem.getBarcode());
				}
				detail.put("skucount", skucount);
				goodsMap.put(skuoid, detail);
			}
			if(creater.length()>0){
				creater = creater.substring(0,creater.length()-1);
			}
			map.put("creater", creater);
			map.put("whcode",whcode);
			List<net.sf.json.JSONObject> itemJsonList = new ArrayList<>();
			for(Map.Entry<String,net.sf.json.JSONObject> goods : goodsMap.entrySet()){
				itemJsonList.add(goods.getValue());
			}
			map.put("iteamList", itemJsonList);
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(map);
			// 调用接口
			System.out.println("向WMS推送数据：["+jsonObj.toString()+"]");
			OMSResponse result = null;
			try {
				result = InterfaceUtil.NuotaiWmsClient("client/addDeliveryrecord",String.valueOf(deliveryRecord.getSupplierId()), jsonObj);
				if (result.isSuccess()) {
					rst = "推送入库申请单成功！";
					log.setResult("Y");
				}else {
					rst = "推送失败！";
					log.setResult("N");
					log.setErrorMsg(result.getMsg());
				}
			}catch (TimeoutException e){
				log.setResult("N");
				log.setErrorMsg("连接超时");
			}catch (Exception e) {
				log.setResult("N");
				log.setErrorMsg(e.getMessage());
			}finally {
				log.setDeliverNo(deliveryRecord.getDeliverNo());
				log.setInterfaceUrl(Constant.WMS_INTERFACE_URL);
				log.setMethod("client/addDeliveryrecord");
				log.setToken(String.valueOf(deliveryRecord.getSupplierId()));
				log.setContent(jsonObj.toString());
				wmsInputplanLogMapper.deleteByDeliverNo(deliveryRecord.getDeliverNo());
				wmsInputplanLogMapper.add(log);
			}
			System.out.println("result:"+result);
			System.out.println("result.isSuccess()="+result.isSuccess());
			return rst;
		}
	}
}