package com.nuotai.trading.buyer.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillInvoice;
import com.nuotai.trading.buyer.service.BuyBillInvoiceService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

//import com.nuotai.trading.seller.service.SellerBillCycleService;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-8-28
 */

@Controller
@RequestMapping("platform/buyer/billInvoice")
public class BillInvoiceController extends BaseController {
	@Autowired
	private BuyBillInvoiceService buyBillInvoiceService;
	
	/**
	 * 发票列表查询
	 * @return
	 */
	@RequestMapping("billInvoiceList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("acceptInvoiceStatus")){
			params.put("acceptInvoiceStatus","0");
		}
		params.put("buyCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		//列表查询
		List<Map<String,Object>> commodityBatchList = buyBillInvoiceService.getInvoiceList(searchPageUtil);

		buyBillInvoiceService.queryInvoiceCountByStatus(params);
		searchPageUtil.getPage().setList(commodityBatchList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billinvoice/billInvoiceList";
	}

	/**
	 * 发票核实
	 * @param updateMap
	 * @return
	 */
	@RequestMapping("updateBillInvoice")
	@ResponseBody
	public String updateBillInvoice(@RequestParam Map<String, Object> updateMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = buyBillInvoiceService.updateBillInvoice(updateMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "发票核实成功！");
			}else {
				json.put("error", false);
				json.put("msg", "发票核实失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 根据发票编号查询关联订单
	 * @param recItemMap
	 * @return
	 */
	@RequestMapping("queryRecItemList")
	@ResponseBody
	public String queryRecItemList(@RequestParam Map<String,Object> recItemMap){
		Map<String,Object> recItemList = buyBillInvoiceService.queryRecItemByInvoiceId(recItemMap);
		return JSONObject.toJSONString(recItemList);
	}

	/**
	 * 导出发票数据
	 * @param invoiceMap
	 */
	@RequestMapping("buyInvoiceExport")
	public void buyInvoiceExport(@RequestParam Map<String,Object> invoiceMap){
		invoiceMap.put("buyCompanyId", ShiroUtils.getCompId());
		List<BuyBillInvoice> buyBillInvoices = buyBillInvoiceService.queryInvoiceBuyMap(invoiceMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单发票报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"开票编号","供应商名称","商品总金额","票据号","票据类型","票据金额","开票日期","状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("账单发票报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != buyBillInvoices && buyBillInvoices.size()>0){
			for(BuyBillInvoice buyBillInvoice : buyBillInvoices){
				row=sheet.createRow(rowCount);
				//1.开票编号
				cell = row.createCell(0);
				cell.setCellValue(buyBillInvoice.getSettlementNo());
				cell.setCellStyle(normalStyle);
				//2.供应商名称
				cell = row.createCell(1);
				cell.setCellValue(buyBillInvoice.getSellerCompanyName());
				cell.setCellStyle(normalStyle);
				//3.商品总金额
				cell = row.createCell(2);
				cell.setCellValue(fnum.format(buyBillInvoice.getTotalCommodityAmount()));
				cell.setCellStyle(normalStyle);
				//4.票据号
				cell = row.createCell(3);
				cell.setCellValue(buyBillInvoice.getInvoiceNo());
				cell.setCellStyle(normalStyle);
				//5.票据类型
				cell = row.createCell(4);
				String invoiceType = buyBillInvoice.getInvoiceType();
				String typeStr = "";
				if("1".equals(invoiceType)){
					typeStr = "专用发票";
				}else if("2".equals(invoiceType)){
					typeStr = "普通发票";
				}else if("3".equals(invoiceType)){
					typeStr = "其他发票";
				}
				cell.setCellValue(typeStr);
				cell.setCellStyle(normalStyle);
				//6.票据金额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(buyBillInvoice.getTotalInvoiceValue()));
				cell.setCellStyle(normalStyle);
				//7.开票日期
				cell = row.createCell(6);
				cell.setCellValue(buyBillInvoice.getCreateDateStr());
				cell.setCellStyle(normalStyle);
				//8.状态
				cell = row.createCell(7);
				String acceptStatus = buyBillInvoice.getAcceptInvoiceStatus();
				String statusStr = "";
				if("1".equals(acceptStatus)){
					statusStr = "待确认票据";
				}else if("2".equals(acceptStatus)){
					statusStr = "已确认票据";
				}else if("3".equals(acceptStatus)){
					statusStr = "已驳回票据";
				}else if("4".equals(acceptStatus)){
					statusStr = "已取消票据";
				}
				cell.setCellValue(statusStr);
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("账单发票信息", response);
		ExcelUtil.export(wb, response);
	}
}
