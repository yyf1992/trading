package com.nuotai.trading.buyer.dao;

import java.util.Map;

import com.nuotai.trading.buyer.model.BuySupplierUser;
import com.nuotai.trading.dao.BaseDao;
/**
 * 用户与供应商关联表
 * 
 * @author "
 * @date 2018-03-23 15:16:33
 */
public interface BuySupplierUserMapper extends BaseDao<BuySupplierUser> {
	
	int deleteByParams(Map<String, Object> params);
	
}
