package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysShopDeliveryData;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;



/**
 * 发货取数店铺表
 * 
 * @author "
 * @date 2017-09-25 13:24:06
 */
public interface SysShopDeliveryDataMapper extends BaseDao<SysShopDeliveryData> {
	//新增发货取数店铺
	int insert(SysShopDeliveryData deliveryData);
	
	List<Map<String, Object>> selectByMap(Map map);
	//根据公司Id查询
	List<Map<String, Object>> selectByCompanyId(SearchPageUtil searchPageUtil);
	//根据id删除
	int deleteShop(String id);
	//根据map查询
	List<SysShopDeliveryData>getShopByMap(Map<String,Object> map);
	//根据公司id查询店铺id
	List<String>selectShopId(String companuId);
}
