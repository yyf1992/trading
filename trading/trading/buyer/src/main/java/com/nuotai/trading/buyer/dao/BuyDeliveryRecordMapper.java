package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyDeliveryRecord;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 发货单信息
 *
 * @author gsf
 * @date 2017 -08-10
 */
@Component
public interface BuyDeliveryRecordMapper extends BaseDao<BuyDeliveryRecord> {
    /**
     * Query delivery record list list.
     *
     * @param map the map
     * @return the list
     */
//根据条件查询发货详情
    List<BuyDeliveryRecord> queryDeliveryRecordList(Map<String,Object> map);

    /**
     * Select delivery num by map int.
     * 获得不同状态数量
     *
     * @param selectMap the select map
     * @return the int
     */
    int selectDeliveryNumByMap(Map<String, Object> selectMap);

    /**
     * Select by page list.
     * 分页查询
     *
     * @param searchPageUtil the search page util
     * @return the list
     */
    List<BuyDeliveryRecord> selectByPage(SearchPageUtil searchPageUtil);

    /**
     * Update by primary key selective.
     * 更新收货单
     *
     * @param buyDeliveryRecord the buy delivery record
     */
    void updateByPrimaryKeySelective(BuyDeliveryRecord buyDeliveryRecord);

    /**
     * Query freight by rec id map.
     *
     * @param logisticsMap the logistics map
     * @return the map
     */
//根据发货编号查询运费
    Map<String,Object> queryFreightByRecId(Map<String,Object> logisticsMap);

    /**
     * 根据条件修改订单对账状态
     *
     * @param map the map
     * @return int
     */
    int updateDeliveryByReconciliation(Map<String,Object> map);

    /**
     * Select by map list.
     * 根据map条件查询
     * @param params the params
     * @return the list
     */
    List<BuyDeliveryRecord> selectByMap(Map<String, Object> params);

	int updatePushType(String deliverNo);
}
