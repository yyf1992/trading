package com.nuotai.trading.buyer.controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyCustomerItem;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.buyer.service.BuyApplypurchaseItemService;
import com.nuotai.trading.buyer.service.BuyCustomerItemService;
import com.nuotai.trading.buyer.service.BuyCustomerService;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.BuySalePlanHeaderService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IExcelService;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ElFunction;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 数据统计excel表格导出/导入
 * 
 * @author dxl
 * 
 */
@Controller
@RequestMapping("download")
@Scope("prototype")
@SuppressWarnings({"unchecked","static-access"})
public class ExcelUtilController extends BaseController {

	@Autowired
	private BuyProductSkuService productSkuService;
	@Autowired
	private BuyUnitService unitService;
	@Autowired
	private BuyApplypurchaseItemService applypurchaseItemService;
	@Autowired
	private BuyCustomerService customerService;
	@Autowired
	private BuyCustomerItemService customerItemService;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private BuySalePlanHeaderService salePlanHeaderService;
	
	/**
	 * 到货付款导入
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importApplyPurchaseExcel", method = RequestMethod.POST)
	@ResponseBody
	public String importDeliverycordPayExcel(
			@RequestParam(required = false) MultipartFile excel)
			throws Exception {
		JSONObject json = new JSONObject();
		if (excel != null) {
			String fileName = excel.getOriginalFilename();// 获取文件名
			String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
			InputStream is = excel.getInputStream();
			if ("xls".equals(ext) || "xlsx".equals(ext)) {// 判断文件格式
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is)
						: new XSSFWorkbook(is);
				Sheet sheet = workbook.getSheetAt(0);
				int rowNum = sheet.getLastRowNum();// 获取excel行数
				List<BuyApplypurchaseItem> itemList = new ArrayList<BuyApplypurchaseItem>();
				for (int i = 1; i <= rowNum; i++) {
					Row row = sheet.getRow(i);
					if (!ExcelUtil.isBlankRow(row)) {
						row.getCell(2).setCellType(CellType.STRING);
						String skuOid = row.getCell(2).getStringCellValue();//条形码
						//根据条形码查询规格信息，没有则不允许导入
						List<BuyProductSku> productList = productSkuService.selectByBarCode(skuOid);
						if(productList != null && productList.size() > 0){
							BuyProductSku product = productList.get(0);
							BuyApplypurchaseItem purchaseItem = new BuyApplypurchaseItem();
							String productCode = row.getCell(0).getStringCellValue();//货号
							purchaseItem.setProductCode(productCode);
							String productName = row.getCell(4).getStringCellValue();//商品名称
							purchaseItem.setProductName(productName);
							purchaseItem.setSkuCode(product.getSkuCode());
							purchaseItem.setSkuName(product.getSkuName());
							purchaseItem.setBarcode(skuOid);
							String productStatus = row.getCell(6).getStringCellValue();//产品状态
							purchaseItem.setProductStatus(productStatus);
							row.getCell(7).setCellType(CellType.STRING);
							String purchasCycle = row.getCell(7).getStringCellValue();//采购周期
							purchaseItem.setPurchasCycle(Integer.parseInt(purchasCycle));
							row.getCell(8).setCellType(CellType.STRING);
							String monthSaleNum = row.getCell(8).getStringCellValue();//本月销量
							purchaseItem.setMonthSaleNum(Integer.parseInt(monthSaleNum));
							row.getCell(9).setCellType(CellType.STRING);
							String monthDMS = row.getCell(9).getStringCellValue();//本月日均销量
							purchaseItem.setMonthDms(new Long(Math.round(Double.parseDouble(monthDMS))).intValue());
							row.getCell(10).setCellType(CellType.STRING);
							String SCStock = row.getCell(10).getStringCellValue();//杉橙当前库存
							purchaseItem.setScStock(Integer.parseInt(SCStock));
							row.getCell(11).setCellType(CellType.STRING);
							String outStock = row.getCell(11).getStringCellValue();//外仓库存数量
							purchaseItem.setOutStock(Integer.parseInt(outStock));
							row.getCell(12).setCellType(CellType.STRING);
							String transitNum = row.getCell(12).getStringCellValue();//在途订单数量
							purchaseItem.setTransitNum(Integer.parseInt(transitNum));
							row.getCell(13).setCellType(CellType.STRING);
							String totalStock = row.getCell(13).getStringCellValue();//合计库存数量
							purchaseItem.setTotalStock(Integer.parseInt(totalStock));
							row.getCell(14).setCellType(CellType.STRING);
							String salePlan = row.getCell(14).getStringCellValue();//销售计划
							purchaseItem.setSalePlan(Integer.parseInt(salePlan));
							row.getCell(15).setCellType(CellType.STRING);
							String differenceNum = row.getCell(15).getStringCellValue();//现需求差异量
							purchaseItem.setDifferenceNum(Integer.parseInt(differenceNum));
							row.getCell(16).setCellType(CellType.STRING);
							String predictNextMonthArrival  = row.getCell(16).getStringCellValue();//预估下月到货
							purchaseItem.setPredictNextMonthArrival(Integer.parseInt(predictNextMonthArrival));
							row.getCell(17).setCellType(CellType.STRING);
							String predictNextMonthStock  = row.getCell(17).getStringCellValue();//预估11月底库存
							purchaseItem.setPredictNextMonthStock(Integer.parseInt(predictNextMonthStock));
							row.getCell(18).setCellType(CellType.STRING);
							String applyCount = row.getCell(18).getStringCellValue();//下单数量
							purchaseItem.setApplyCount(Integer.parseInt(applyCount));
							purchaseItem.setUnitId(product.getUnitId());//单位
							BuyUnit unit = unitService.selectByPrimaryKey(product.getUnitId());
							purchaseItem.setUnitName(unit.getUnitName());
							purchaseItem.setProductType(product.getProductType());//商品类型
							itemList.add(purchaseItem);
						}else{
							json.put("status", "fail");
							json.put("msg", "导入的商品有误，系统中未找到第"+i+"行商品！");
							return json.toString();
						}
					}
				}
				json.put("status", "success");
				json.put("msg", "成功！");
				json.put("itemList", itemList);
			} else {
				json.put("status", "fail");
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("status", "fail");
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	
	/**
	 * 采购下单导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/purchaseStatistic")
	public void purchaseStatistic(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		String reportName = "采购未下单报表";
		long curr_time = System.currentTimeMillis();
		int rowaccess = 100;// 内存中缓存记录行数
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(font2);
		//String title[] = {"货号","商品名称","规格代码","规格名称","条形码","计划采购总数量","已下单采购量","已下单未审核采购量","剩余采购量"};
		String title[] = {"申请编号","商品货号","商品名称","规格代码","规格名称","条形码","计划采购数量","剩余采购数量",
				"要求到货日期","备注","申请人","申请时间"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4000);
			}
		}
		int startRow = 2;
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		//List<BuyApplypurchaseItem> apItemList = applypurchaseItemService.selectAllProductByNoPage(map);
		List<Map<String, Object>> apItemList = applypurchaseItemService.getWaitOrderByNoPage(map);
		if(apItemList != null && apItemList.size() > 0){
			for(Map<String, Object> item : apItemList){
				map.put("barcode", item.get("barcode").toString());
				List<Map<String, Object>> detailsList = applypurchaseItemService.selectProductByBarcode(map);
				if(detailsList != null && detailsList.size() > 0){
					for(Map<String, Object> mapResult : detailsList){
						// 用编号和条形码查询已下单商品数量
						Map<String,Object> mapPare = new HashMap<String,Object>();
						mapPare.put("companyId", ShiroUtils.getCompId());
						mapPare.put("applyCode", mapResult.get("apply_code"));
						mapPare.put("skuOid", mapResult.get("barcode"));
						Map<String,Object> mapAlready = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapPare);
						if(mapAlready != null){
							int alreadyPlanPurchaseNum = mapAlready.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(mapAlready.get("alreadyPlanPurchaseNum").toString());
							//已取消
							Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapPare);
							if(mapCancel != null){
								int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
								alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
							}
							//已终止
							Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapPare);
							if(mapStop != null){
								int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
								alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
							}
							if(alreadyPlanPurchaseNum < Integer.parseInt(mapResult.get("apply_count").toString())){
								// 查询已下单未审核数量
								Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
								if(mapLock != null){
									int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
									mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum - lockGoodsNumber);
								}else{
									mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum);
								}
							}else{
								mapResult.put("overplusNum", 0);
							}
						}else{
							// 查询已下单未审核数量
							Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
							if(mapLock != null){
								int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
								mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString())  - lockGoodsNumber);
							}else{
								mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()));
							}
						}
						if(Integer.parseInt(mapResult.get("overplusNum").toString()) != 0){//剩余采购数量为0的不显示
							resultList.add(mapResult);
						}
					}
				}
			}
		}
		int totalPlanNum = 0;
		int totalOverplusNum = 0;
		if (resultList != null && resultList.size() > 0) {
			for (int i = 0; i < resultList.size(); i++) {
				Map<String, Object> item = resultList.get(i);
				// 创建行
				row = sheet.createRow(startRow);
				
				// 申请部门
				/*Cell cell = row.createCell(0);
				cell.setCellValue(item.get("shop_name").toString());
				cell.setCellStyle(cellStyle);*/
				
				// 申请编号
				Cell cell = row.createCell(0);
				cell.setCellValue(item.get("apply_code").toString());
				cell.setCellStyle(cellStyle);
				
				// 货号
				cell = row.createCell(1);
				cell.setCellValue(item.get("product_code").toString());
				cell.setCellStyle(cellStyle);
				
				// 商品名称
				cell = row.createCell(2);
				cell.setCellValue(item.get("product_name").toString());
				cell.setCellStyle(cellStyle);
				
				//规格代码
				cell = row.createCell(3);
				cell.setCellValue(item.get("sku_code").toString());
				cell.setCellStyle(cellStyle);
				
				//规格名称
				cell = row.createCell(4);
				cell.setCellValue(item.get("sku_name").toString());
				cell.setCellStyle(cellStyle);
				
				//条形码
				cell = row.createCell(5);
				cell.setCellValue(item.get("barcode").toString());
				cell.setCellStyle(cellStyle);
				
				//计划采购数量
				cell = row.createCell(6);
				cell.setCellValue(item.get("apply_count").toString());
				cell.setCellStyle(cellStyle);
				
				//剩余采购量
				cell = row.createCell(7);
				cell.setCellValue(item.get("overplusNum").toString());
				cell.setCellStyle(cellStyle);
				
				//要求到货日期
				cell = row.createCell(8);
				if (item.get("predictArred") != null
						&& item.get("predictArred") != "") {
					cell.setCellValue((item.get("predictArred") + "").substring(
							0, 10));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				//备注
				cell = row.createCell(9);
				cell.setCellValue(item.get("remark").toString());
				cell.setCellStyle(cellStyle);
				
				//申请人
				cell = row.createCell(10);
				cell.setCellValue(item.get("create_name").toString());
				cell.setCellStyle(cellStyle);
				
				//申请时间
				cell = row.createCell(11);
				if (item.get("create_date") != null
						&& item.get("create_date") != "") {
					cell.setCellValue((item.get("create_date") + "").substring(
							0, 19));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
				totalPlanNum = totalPlanNum + Integer.parseInt(item.get("apply_count").toString());
				totalOverplusNum = totalOverplusNum + Integer.parseInt(item.get("overplusNum").toString());
			}
			// 总计创建行
			row = sheet.createRow(startRow);
			
			// 申请部门
			/*Cell cell = row.createCell(0);
			cell.setCellValue("合计");
			cell.setCellStyle(cellStyle);*/
			
			// 申请编号
			Cell cell = row.createCell(0);
			cell.setCellValue("合计");
			cell.setCellStyle(cellStyle);
			
			// 货号
			cell = row.createCell(1);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			// 商品名称
			cell = row.createCell(2);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//规格代码
			cell = row.createCell(3);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//规格名称
			cell = row.createCell(4);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//条形码
			cell = row.createCell(5);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//计划采购数量
			cell = row.createCell(6);
			cell.setCellValue(totalPlanNum+"");
			cell.setCellStyle(cellStyle);
			
			//剩余采购量
			cell = row.createCell(7);
			cell.setCellValue(totalOverplusNum+"");
			cell.setCellStyle(cellStyle);
			
			//要求到货日期
			cell = row.createCell(8);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//备注
			cell = row.createCell(9);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//申请人
			cell = row.createCell(10);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//申请时间
			cell = row.createCell(11);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
		}

		System.out.println("耗时:" + (System.currentTimeMillis() - curr_time) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
	
	/**
	 * 店铺采购计划导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/applyPurchaseListStatistic")
	public void applyPurchaseListStatistic(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String reportName = "采购计划报表";
		long curr_time = System.currentTimeMillis();
		int rowaccess = 100;// 内存中缓存记录行数
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(font2);
		//String title[] = {"编号","部门名称","申请日期","申请人","说明","状态"};
		String title[] = {"申请编号","商品货号","商品名称","规格代码","规格名称","条形码","状态","计划采购数量","剩余采购数量",
				"要求到货日期","备注","申请人","申请时间"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4000);
			}
		}
		int startRow = 2;
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		int tabId = map.containsKey("tabId")?Integer.parseInt(map.get("tabId").toString()):0;
		if(tabId == 1){
			//待审批
			map.put("status", Constant.PurchaseStatus.WAITVERIFY.getValue());
		}else if(tabId == 2){
			//审核通过
			map.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		}else if(tabId == 3){
			//审核不通过
			map.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		}else if(tabId == 4){
			//已取消
			map.put("status", Constant.PurchaseStatus.CANCEL.getValue());
		}
		
		int totalPlanNum = 0;
		int totalOverplusNum = 0;
		List<Map<String, Object>> detailsList = applypurchaseItemService.selectProductByStatistic(map);
		if(detailsList != null && detailsList.size() > 0){
			for(Map<String, Object> mapResult : detailsList){
				// 用编号和条形码查询已下单商品数量
				Map<String,Object> mapPare = new HashMap<String,Object>();
				mapPare.put("companyId", ShiroUtils.getCompId());
				mapPare.put("applyCode", mapResult.get("apply_code"));
				mapPare.put("skuOid", mapResult.get("barcode"));
				Map<String,Object> mapAlready = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapPare);
				if(mapAlready != null){
					int alreadyPlanPurchaseNum = mapAlready.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(mapAlready.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapPare);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapPare);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					if(alreadyPlanPurchaseNum < Integer.parseInt(mapResult.get("apply_count").toString())){
						// 查询已下单未审核数量
						Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
						if(mapLock != null){
							int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum - lockGoodsNumber);
						}else{
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum);
						}
					}else{
						mapResult.put("overplusNum", 0);
					}
				}else{
					// 查询已下单未审核数量
					Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
					if(mapLock != null){
						int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString())  - lockGoodsNumber);
					}else{
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()));
					}
				}
				// 创建行
				row = sheet.createRow(startRow);
				
				// 申请部门
				/*Cell cell = row.createCell(0);
				cell.setCellValue(mapResult.get("shop_name").toString());
				cell.setCellStyle(cellStyle);*/
				
				// 申请编号
				Cell cell = row.createCell(0);
				cell.setCellValue(mapResult.get("apply_code").toString());
				cell.setCellStyle(cellStyle);
				
				// 货号
				cell = row.createCell(1);
				cell.setCellValue(mapResult.get("product_code").toString());
				cell.setCellStyle(cellStyle);
				
				// 商品名称
				cell = row.createCell(2);
				cell.setCellValue(mapResult.get("product_name").toString());
				cell.setCellStyle(cellStyle);
				
				//规格代码
				cell = row.createCell(3);
				cell.setCellValue(mapResult.get("sku_code").toString());
				cell.setCellStyle(cellStyle);
				
				//规格名称
				cell = row.createCell(4);
				cell.setCellValue(mapResult.get("sku_name").toString());
				cell.setCellStyle(cellStyle);
				
				//条形码
				cell = row.createCell(5);
				cell.setCellValue(mapResult.get("barcode").toString());
				cell.setCellStyle(cellStyle);
				
				//状态
				cell = row.createCell(6);
				if(mapResult.get("status").toString().equals("0")){
					cell.setCellValue("等待审核");
				}else if(mapResult.get("status").toString().equals("1")){
					cell.setCellValue("审核通过");
				}else if(mapResult.get("status").toString().equals("2")){
					cell.setCellValue("审核不通过");
				}else if(mapResult.get("status").toString().equals("3")){
					cell.setCellValue("已取消");
				}
				cell.setCellStyle(cellStyle);
				
				//计划采购数量
				cell = row.createCell(7);
				cell.setCellValue(mapResult.get("apply_count").toString());
				cell.setCellStyle(cellStyle);
				
				//剩余采购量
				cell = row.createCell(8);
				cell.setCellValue(mapResult.get("overplusNum").toString());
				cell.setCellStyle(cellStyle);
				
				//要求到货日期
				cell = row.createCell(9);
				if (mapResult.get("predictArred") != null
						&& mapResult.get("predictArred") != "") {
					cell.setCellValue((mapResult.get("predictArred") + "").substring(
							0, 10));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				//备注
				cell = row.createCell(10);
				cell.setCellValue(mapResult.get("remark").toString());
				cell.setCellStyle(cellStyle);
				
				//申请人
				cell = row.createCell(11);
				cell.setCellValue(mapResult.get("create_name").toString());
				cell.setCellStyle(cellStyle);
				
				//申请时间
				cell = row.createCell(12);
				if (mapResult.get("create_date") != null
						&& mapResult.get("create_date") != "") {
					cell.setCellValue((mapResult.get("create_date") + "").substring(
							0, 19));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
				totalPlanNum = totalPlanNum + Integer.parseInt(mapResult.get("apply_count").toString());
				totalOverplusNum = totalOverplusNum + Integer.parseInt(mapResult.get("overplusNum").toString());
			}
			// 总计创建行
			row = sheet.createRow(startRow);
			
			// 申请部门
			/*Cell cell = row.createCell(0);
			cell.setCellValue("合计");
			cell.setCellStyle(cellStyle);*/
			
			// 申请编号
			Cell cell = row.createCell(0);
			cell.setCellValue("合计");
			cell.setCellStyle(cellStyle);
			
			// 货号
			cell = row.createCell(1);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			// 商品名称
			cell = row.createCell(2);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//规格代码
			cell = row.createCell(3);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//规格名称
			cell = row.createCell(4);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//条形码
			cell = row.createCell(5);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//状态
			cell = row.createCell(6);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//计划采购数量
			cell = row.createCell(7);
			cell.setCellValue(totalPlanNum+"");
			cell.setCellStyle(cellStyle);
			
			//剩余采购量
			cell = row.createCell(8);
			cell.setCellValue(totalOverplusNum+"");
			cell.setCellStyle(cellStyle);
			
			//要求到货日期
			cell = row.createCell(9);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//备注
			cell = row.createCell(10);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//申请人
			cell = row.createCell(11);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			//申请时间
			cell = row.createCell(12);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
		}

		System.out.println("耗时:" + (System.currentTimeMillis() - curr_time) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
	/**
	 * 店铺采购计划导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/customerListStatistic")
	public void customerListStatistic(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String reportName = "售后管理报表";
		long curr_time = System.currentTimeMillis();
		int rowaccess = 100;// 内存中缓存记录行数
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(font2);
		String title[] = {"货号","商品","规格代码","规格名称","条形码","单位","退/换货数量","申请原因","状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
		titleCell.setCellStyle(titleStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4000);
			}
		}
		int startRow = 2;
		
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		int tabId = map.containsKey("tabId")?Integer.parseInt(map.get("tabId").toString()):0;
		if(tabId == 0){
			//待内部审批
			map.put("status", Constant.CustomerStatus.WAITVERIFY.getValue());
		}else if(tabId == 1){
			//待对方确认
			map.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		}else if(tabId == 2){
			//审核不通过
			map.put("status", Constant.CustomerStatus.VERIFYNOPASS.getValue());
		}else if(tabId == 3){
			//取消
			map.put("status", Constant.CustomerStatus.CANCEL.getValue());
		}
		List<BuyCustomer> customerList = customerService.queryCustomerListByStatistic(map);
		if (customerList != null && customerList.size() > 0) {
			for (int i = 0; i < customerList.size(); i++) {
				BuyCustomer customer = customerList.get(i);
				// 创建行
				row = sheet.createRow(startRow);
				ExcelUtil.setRangeStyle(sheet, row.getRowNum()+1, row.getRowNum()+1, 1, title.length);
				// 编号
				Cell cell = row.createCell(0);
				cell.setCellValue("日期:"+sdf.format(customer.getCreateDate())+"  编号:"+customer.getCustomerCode()+"  供应商:"+customer.getSupplierName());
				XSSFCellStyle borderStyle = (XSSFCellStyle)wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle);
				startRow++;
				List<BuyCustomerItem> itemList = customerItemService.selectCustomerItemByCustomerId(customer.getId());
				for(BuyCustomerItem item : itemList){
					row=sheet.createRow(startRow);
					// 货号
					cell = row.createCell(0);
					cell.setCellValue(item.getProductCode());
					cell.setCellStyle(cellStyle);
					
					// 商品
					cell = row.createCell(1);
					cell.setCellValue(item.getProductName());
					cell.setCellStyle(cellStyle);
					
					//规格代码
					cell = row.createCell(2);
					cell.setCellValue(item.getSkuCode());
					cell.setCellStyle(cellStyle);
					
					//规格名称
					cell = row.createCell(3);
					cell.setCellValue(item.getSkuName());
					cell.setCellStyle(cellStyle);
					
					//条形码
					cell = row.createCell(4);
					cell.setCellValue(item.getSkuOid());
					cell.setCellStyle(cellStyle);
					
					//单位
					cell = row.createCell(5);
					cell.setCellValue(ElFunction.getUnitById(item.getUnitId()).getUnitName());
					cell.setCellStyle(cellStyle);
					
					//退/换货数量
					cell = row.createCell(6);
					cell.setCellValue(item.getGoodsNumber());
					cell.setCellStyle(cellStyle);
					
					//申请原因
					cell = row.createCell(7);
					cell.setCellValue(customer.getReason());
					cell.setCellStyle(cellStyle);
					
					//状态
					cell = row.createCell(8);
					if(customer.getStatus().equals("0")){
						cell.setCellValue("待内部审批");
					}else if(customer.getStatus().equals("1")){
						cell.setCellValue("待对方确认");
					}else if(customer.getStatus().equals("2")){
						cell.setCellValue("已驳回");
					}else if(customer.getStatus().equals("3")){
						cell.setCellValue("已取消");
					}
					cell.setCellStyle(cellStyle);
					startRow++;
				}
			}
		}

		System.out.println("耗时:" + (System.currentTimeMillis() - curr_time) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
	/**
	 * 销售计划导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/salePlanListStatistic")
	public void salePlanListStatistic(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String reportName = "销售计划报表";
		long curr_time = System.currentTimeMillis();
		int rowaccess = 100;// 内存中缓存记录行数
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(font2);
		String title[] = {"销售计划编号","销售周期","商品货号","商品名称","规格代码","规格名称","条形码","部门","销售计划",
				"入仓量","备注","状态","申请人","申请时间"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
		titleCell.setCellStyle(titleStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4000);
			}
		}
		int startRow = 2;
		
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		int tabId = map.containsKey("tabId")?Integer.parseInt(map.get("tabId").toString()):0;
		if("1".equals(tabId)){
			//待审批
			map.put("status", Constant.PurchaseStatus.WAITVERIFY.getValue());
		}else if("2".equals(tabId)){
			//审核通过
			map.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		}else if("3".equals(tabId)){
			//审核不通过
			map.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		}
		List<Map<String,Object>> sphList = salePlanHeaderService.selectByStatistic(map);
		int totalSalePlanNum = 0;
		int totalPutStorageNum = 0;
		if (sphList != null && sphList.size() > 0) {
			for (int i = 0; i < sphList.size(); i++) {
				Map<String,Object> sph = sphList.get(i);
				// 创建行
				row = sheet.createRow(startRow);
				
				// 申请编号
				Cell cell = row.createCell(0);
				cell.setCellValue(sph.get("plan_code").toString());
				cell.setCellStyle(cellStyle);
				
				//销售周期
				cell = row.createCell(1);
				if (sph.get("start_date") != null
						&& sph.get("start_date") != "") {
					cell.setCellValue((sph.get("start_date") + "").substring(0, 10)+" 至 "+(sph.get("end_date") + "").substring(0, 10));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				// 货号
				cell = row.createCell(2);
				cell.setCellValue(sph.get("product_code").toString());
				cell.setCellStyle(cellStyle);
				
				// 商品名称
				cell = row.createCell(3);
				cell.setCellValue(sph.get("product_name").toString());
				cell.setCellStyle(cellStyle);
				
				//规格代码
				cell = row.createCell(4);
				cell.setCellValue(sph.get("sku_code").toString());
				cell.setCellStyle(cellStyle);
				
				//规格名称
				cell = row.createCell(5);
				cell.setCellValue(sph.get("sku_name").toString());
				cell.setCellStyle(cellStyle);
				
				//条形码
				cell = row.createCell(6);
				cell.setCellValue(sph.get("barcode").toString());
				cell.setCellStyle(cellStyle);
				
				//部门
				cell = row.createCell(7);
				cell.setCellValue(sph.get("shop_name").toString());
				cell.setCellStyle(cellStyle);
				
				//销售计划
				cell = row.createCell(8);
				cell.setCellValue(sph.get("sales_num").toString());
				cell.setCellStyle(cellStyle);
				
				//入仓量
				cell = row.createCell(9);
				cell.setCellValue(sph.get("put_storage_num").toString());
				cell.setCellStyle(cellStyle);
				
				//备注
				cell = row.createCell(10);
				cell.setCellValue(sph.get("remark").toString());
				cell.setCellStyle(cellStyle);
				
				//状态
				cell = row.createCell(11);
				if(sph.get("status").toString().equals("0")){
					cell.setCellValue("等待审核");
				}else if(sph.get("status").toString().equals("1")){
					cell.setCellValue("审核通过");
				}else if(sph.get("status").toString().equals("2")){
					cell.setCellValue("审核不通过");
				}
				cell.setCellStyle(cellStyle);
				
				//申请人
				cell = row.createCell(12);
				cell.setCellValue(sph.get("create_name").toString());
				cell.setCellStyle(cellStyle);
				
				//申请时间
				cell = row.createCell(13);
				if (sph.get("create_date") != null
						&& sph.get("create_date") != "") {
					cell.setCellValue((sph.get("create_date") + "").substring(0, 19));
				} else {
					cell.setCellValue("");
				}
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
				totalSalePlanNum = totalSalePlanNum + Integer.parseInt(sph.get("sales_num").toString());
				totalPutStorageNum = totalPutStorageNum + Integer.parseInt(sph.get("put_storage_num").toString());
			}
			// 总计创建行
			row = sheet.createRow(startRow);
			
			// 申请编号
			Cell cell = row.createCell(0);
			cell.setCellValue("合计");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(1);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(2);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(3);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(4);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(5);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(6);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(7);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(8);
			cell.setCellValue(totalSalePlanNum + "");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(9);
			cell.setCellValue(totalPutStorageNum + "");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(10);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(11);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(12);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(13);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
		}

		System.out.println("耗时:" + (System.currentTimeMillis() - curr_time) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
	/**
	 * 销售计划导入
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importSalePlanExcel", method = RequestMethod.POST)
	@ResponseBody
	public String importSalePlanExcel(
			@RequestParam(required = false) MultipartFile excel)
			throws Exception {
		JSONObject json = new JSONObject();
		if (excel != null) {
			String fileName = excel.getOriginalFilename();// 获取文件名
			String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
			InputStream is = excel.getInputStream();
			if ("xls".equals(ext) || "xlsx".equals(ext)) {// 判断文件格式
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is)
						: new XSSFWorkbook(is);
				Sheet sheet = workbook.getSheetAt(0);
				int rowNum = sheet.getLastRowNum();// 获取excel行数
				List<BuySalePlanItem> itemList = new ArrayList<BuySalePlanItem>();
				for (int i = 1; i <= rowNum; i++) {
					Row row = sheet.getRow(i);
					if (!ExcelUtil.isBlankRow(row)) {
						row.getCell(4).setCellType(CellType.STRING);
						String skuOid = row.getCell(4).getStringCellValue();//条形码
						//根据条形码查询规格信息，没有则不允许导入
						List<BuyProductSku> productList = productSkuService.selectByBarCode(skuOid);
						if(productList != null && productList.size() > 0){
							BuyProductSku product = productList.get(0);
							BuySalePlanItem salePlanItem = new BuySalePlanItem();
							salePlanItem.setProductCode(product.getProductCode());
							salePlanItem.setProductName(product.getProductName());
							salePlanItem.setSkuCode(product.getSkuCode());
							salePlanItem.setSkuName(product.getSkuName());
							salePlanItem.setBarcode(skuOid);
							row.getCell(5).setCellType(CellType.STRING);
							String salePlan = row.getCell(5).getStringCellValue();//销售计划
							if(salePlan == null || salePlan == ""){
								salePlan= "0";
							}
							salePlanItem.setSalesNum(Integer.parseInt(salePlan));
							row.getCell(6).setCellType(CellType.STRING);
							String putStorageNum = row.getCell(6).getStringCellValue();//入仓量
							if(putStorageNum == null || putStorageNum == ""){
								putStorageNum= "0";
							}
							salePlanItem.setPutStorageNum(Integer.parseInt(putStorageNum));
							salePlanItem.setUnitId(product.getUnitId());//单位
							BuyUnit unit = unitService.selectByPrimaryKey(product.getUnitId());
							salePlanItem.setUnitName(unit.getUnitName());
							salePlanItem.setProductType(product.getProductType());//商品类型
							itemList.add(salePlanItem);
						}else{
							json.put("status", "fail");
							json.put("msg", "导入的商品有误，系统中未找到第"+i+"行商品！");
							return json.toString();
						}
					}
				}
				json.put("status", "success");
				json.put("msg", "成功！");
				json.put("itemList", itemList);
			} else {
				json.put("status", "fail");
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("status", "fail");
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	
	/**
	 * 导入销售计划-新的
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "importSalePlanExcelNew", method = RequestMethod.POST)
	@ResponseBody
	public void importSalePlanExcelNew(
			@RequestParam Map<String, Object> params,
			@RequestParam(required = false) MultipartFile excel)
			throws Exception {
		insertByImport(excel,params, new IExcelService(){
			@Override
			public JSONObject init(MultipartFile excel,Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				try {
					String fileName = excel.getOriginalFilename();// 获取文件名
					String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
					InputStream is = excel.getInputStream();
					boolean is03file = "xls".equals(ext) ? true : false;
					Workbook workbook = is03file ? new HSSFWorkbook(is): new XSSFWorkbook(is);
					Sheet sheet = workbook.getSheetAt(0);
					int rowNum = sheet.getLastRowNum();// 获取excel行数
					List<BuySalePlanItem> itemList = new ArrayList<BuySalePlanItem>();
					//获得所有的商品
					List<Map<String,Object>> productList = productSkuService.getImportAllProduct(ShiroUtils.getCompId());
					Map<String,Map<String,Object>> allGoodsMap = new HashMap<>();
					if(productList != null && !productList.isEmpty()){
						for(Map<String,Object> sku : productList){
							allGoodsMap.put(sku.get("barcode").toString(), sku);
						}
					}
					boolean importExcelResult = true;
					String msg = "";
					for (int i = 1; i <= rowNum; i++) {
						Row row = sheet.getRow(i);
						if (!ExcelUtil.isBlankRow(row)) {
							row.getCell(4).setCellType(CellType.STRING);
							String skuOid = row.getCell(4).getStringCellValue();//条形码
							//根据条形码查询规格信息，没有则不允许导入
							if(allGoodsMap.containsKey(skuOid)){
								Map<String,Object> product = allGoodsMap.get(skuOid);
								BuySalePlanItem salePlanItem = new BuySalePlanItem();
								salePlanItem.setProductCode(product.get("product_code").toString());
								salePlanItem.setProductName(product.get("product_name").toString());
								salePlanItem.setSkuCode(product.get("sku_code").toString());
								salePlanItem.setSkuName(product.get("sku_name").toString());
								salePlanItem.setBarcode(skuOid);
								row.getCell(5).setCellType(CellType.STRING);
								String salePlan = row.getCell(5).getStringCellValue();//销售计划
								if(salePlan == null || salePlan == "" || salePlan == "0"){
									//salePlan= "0";
									importExcelResult = false;
									msg = "第"+i+"行商品的销售计划必须大于0！";
									break;
								}
								try{
									salePlanItem.setSalesNum(Integer.parseInt(salePlan));
								}catch(Exception e){
									importExcelResult = false;
									msg = "第"+i+"行商品的销售计划为小数【"+salePlan+"】！";
									break;
								}
								row.getCell(6).setCellType(CellType.STRING);
								String putStorageNum = row.getCell(6).getStringCellValue();//入仓量
								if(putStorageNum == null || putStorageNum == ""){
									putStorageNum= "0";
								}
								try{
									salePlanItem.setPutStorageNum(Integer.parseInt(putStorageNum));
								}catch(Exception e){
									importExcelResult = false;
									msg = "第"+i+"行商品的入仓量为小数【"+putStorageNum+"】！";
									break;
								}
								salePlanItem.setUnitId(product.get("unit_id").toString());//单位
								salePlanItem.setUnitName(product.get("unit_name").toString());
								salePlanItem.setProductType((Integer)product.get("product_type"));//商品类型
								itemList.add(salePlanItem);
							}else{
								importExcelResult = false;
								msg = "导入的商品有误，系统中未找到第"+i+"行商品！";
								break;
							}
						}
					}
					if(importExcelResult){
						json.put("importStatus", true);
						json.put("verifySuccess", "buyer/salePlan/verifySuccess");//审批成功调用的方法
						json.put("verifyError", "buyer/salePlan/verifyError");//审批失败调用的方法
						json.put("relatedUrl", "buyer/salePlan/loadSalePlanDetails?verifyDetails=1");//审批详细信息地址
						List<String> idList = new ArrayList<String>();
						try {
							idList = salePlanHeaderService.saveAddSalePlanNew(params,itemList);
						} catch (Exception e) {
							e.printStackTrace();
						}
						json.put("idList", idList);
					}else{
						json.put("importStatus", false);
						json.put("importMsg", msg);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					json.put("importStatus", false);
					json.put("importMsg", e1.getMessage());
				}
				return json;
			}
		});
	}
}
