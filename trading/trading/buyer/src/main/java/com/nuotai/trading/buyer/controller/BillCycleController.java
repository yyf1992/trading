package com.nuotai.trading.buyer.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementOld;
import com.nuotai.trading.buyer.model.BuyBillInterest;
import com.nuotai.trading.buyer.model.BuyBillInterestOld;
import com.nuotai.trading.buyer.service.BuyBillCycleManagementOldService;
import com.nuotai.trading.buyer.service.BuyBillInterestOldService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.buyer.model.BuyBillCycleManagement;
import com.nuotai.trading.buyer.service.BuyBillCycleInterestService;
import com.nuotai.trading.buyer.service.BuyBillCycleService;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.Msg;
import com.nuotai.trading.utils.json.Response;
import com.nuotai.trading.utils.json.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */

@Controller
@RequestMapping("platform/buyer/billCycle")
public class BillCycleController extends BaseController {
	@Autowired
	private SysUserService sysUserService;//用户信息
	@Autowired
	private BuyBillCycleService buyBillCycleService;//账单周期
	@Autowired
	private BuyBillCycleInterestService buyBillCycleInterestService;//账单关联利息
	@Autowired
	private BuyBillCycleManagementOldService buyBillCycleManagementOldService;//历史账单周期
	@Autowired
	private BuyBillInterestOldService buyBillInterestOldService;//历史账单周期关联利息
	@Autowired
	private BuyCompanyService buyCompanyService; //公司管理
	
	/**
	 * 账单周期管理列表查询
	 * @return
	 */
	@RequestMapping("billCycleList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("billDealStatus")){
			params.put("billDealStatus","0");
		}
		params.put("buyCompanyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<Map<String, Object>> billCycleList = buyBillCycleService.getBillCycleList(searchPageUtil);
		buyBillCycleService.getBillCountByStatus(params);
		searchPageUtil.getPage().setList(billCycleList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billcyclemanagements/billCycleManagement";
	}

	/**
	 * 查询账单周期是否唯一
	 * @return
	 */
	@RequestMapping("queryHasBillCycle")
	@ResponseBody
	public String queryHasBillCycle(@RequestParam Map<String, Object> saveMap){
		JSONObject json = new JSONObject();
		List<BuyBillCycleManagement> billCycleList = buyBillCycleService.queryHasBillCycle(saveMap);
		if(0 == billCycleList.size()){
			json.put("error", false);
			json.put("msg", "暂无该供应商对应周期,可添加！");

		}else {
			json.put("success", true);
			json.put("msg", "该供应商对应的账单周期已存在，不可重复添加！");
		}
		return json.toString();
	}

	/**
	 * 账单周期管理添加跳转
	 * @param
	 * @return 
	 */
	@RequestMapping("addBillCycleJump")
	public String addBillCycleJump(HttpServletRequest request){
		//String queryType = request.getParameter("queryType");
		//model.addAttribute("queryType", queryType);
		return "platform/buyer/billcycles/billcyclemanagements/addBillCycleJump";
	}
	
	/**
	 * 账单周期信息及关联利息信息保存
	 * @param saveMap
	 * @return
	 */
	@RequestMapping("saveBillCycleInfo")
	//@ResponseBody
	public void saveBillCycleInfo(@RequestParam Map<String, Object> saveMap){
		Msg msg = new Msg();
		Response res = new Response();

		//if(idList.size() > 0){
			insert(saveMap, new IService() {
				@Override
				public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billCycle/verifySuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billCycle/verifyError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billCycle/verifyDetail");//审批详细信息地址
					List<String> idList = buyBillCycleService.saveBillCycleInfo(saveMap);
					json.put("idList",idList);
					return json;
				}
			});
		/*}else {
			msg.setFlag(state_success);
			res.setCode(40099);
			res.setMsg("保存失败");
			msg.setRes(res);
			out2html(response, msg.toJsonString());
		}*/
		/*JSONObject json = new JSONObject();
		try {
			int saveCount = buyBillCycleService.saveBillCycleInfo(saveMap);
			if(saveCount > 0){
				json.put("success", true);
				json.put("msg", "账单周期添加成功！");
			}else {
				json.put("success", false);
				json.put("msg", "账单周期添加失败！");
			}
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();*/
	}

	/**
	 * 审批通过 (注意备注都在审批表中)
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(final String id){
		JSONObject json = buyBillCycleService.verifySuccess(id);
		/*map.put("success", map.get("success"));
		map.put("msg", map.get("msg"));*/
		/*update(id, map, new IService() {
			@Override
			public Object init(Map<String, Object> params) throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/seller/billCycle/vSellerBillCycleSuccess");//审批成功调用的方法
				json.put("verifyError", "platform/seller/billCycle/vSellerBillCycleError");//审批失败调用的方法
				json.put("relatedUrl", "platform/seller/billCycle/vSellerBillCycleDetail");//审批详细信息地址
				json.put("idList",id);
				return json;
			}
		});*/
		return json.toString();
	}

	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = buyBillCycleService.verifyError(id);
		return json.toString();
	}

	/**
	 * 审批查看详情
	 * @return
	 */
	@RequestMapping(value ="verifyDetail", method = RequestMethod.GET)
	public String verifyDetail(String id){
		//账单结算周期详情
		BuyBillCycleManagement billCycleManagementInfo = buyBillCycleService.selectByPrimaryKey(id);
		model.addAttribute("billCycleManagementInfo",billCycleManagementInfo);
		//账单结算周期关联利息
		List<BuyBillInterest> interestList = buyBillCycleInterestService.getBillInteresInfo(id);
		model.addAttribute("interestList",interestList);
		//历史账单周期数据查询
		BuyBillCycleManagementOld billCycleManagementOld = buyBillCycleManagementOldService.queryLastDateBillCycleOld(id);
		model.addAttribute("billCycleManagementOld",billCycleManagementOld);
		if(null != billCycleManagementOld){
			//历史账单周期关联利息查询
			if(null != billCycleManagementOld.getId() && !"".equals(billCycleManagementOld.getId())){
				List<BuyBillInterestOld> billInterestOldList = buyBillInterestOldService.queryBillInterestOld(billCycleManagementOld.getId());
				model.addAttribute("billInterestOldList",billInterestOldList);
			}
		}

		//查询买家信息
//			BuyCompany buyerBean = buyCompanyService.selectByPrimaryKey(orderBean.getCompanyId());
//			model.addAttribute("buyerBean", buyerBean);
		//卖家信息
		return "platform/buyer/billcycles/billcyclemanagements/verifyDetail";
	}
	/**
	 * 下单成功页面
	 * @return
	 */
	/*@RequestMapping("success")
	public String success(String billCycleId){
		model.addAttribute("billCycleId", billCycleId);
		return "platform/buyer/billcycles/billcyclemanagements/success";
	}*/

	/**
	 * 审核进度回显
	 * @param billCycleId
	 * @return
	 */
	@RequestMapping("billCycleVerifyDetail")
	public String billCycleVerifyDetail(String billCycleId){
		return "platform/buyer/billcycles/billcyclemanagements/verifyDetail";
	}
	/**
	 * 账单周期信息修改跳转
	 * @param request
	 * @return
	 */
	@RequestMapping("updateBillCycleJump")
	public String updateBillCycleJump(HttpServletRequest request){
		//Map<String, Object> paramsMap = new HashMap<String, Object>();
		//String queryType = request.getParameter("queryType");
		String billCycleId = request.getParameter("billCycleId").trim();
		//paramsMap.put("billCycleId", billCycleId);
		//根据账单编号查询账单信息
		BuyBillCycleManagement billCycleMapByUpdate = buyBillCycleService.selectByPrimaryKey(billCycleId);
		List<BuyBillInterest> interestListByUpdate = buyBillCycleInterestService.getBillInteresInfo(billCycleId);
		
		//model.addAttribute("queryType", queryType);
		model.addAttribute("billCycleMapByUpdate", billCycleMapByUpdate);
		model.addAttribute("interestListByUpdate", interestListByUpdate);
		return "platform/buyer/billcycles/billcyclemanagements/updateBillCycleJump";
	}
	
	/**
	 * 修改账单周期及关联利息信息
	 * @param updateMap
	 * @return
	 */
	@RequestMapping("updateSaveBillCycleInfo")
	@ResponseBody
	public void updateSaveBillCycleInfo(@RequestParam final Map<String, Object> updateMap){
		Msg msg = new Msg();
		Response res = new Response();

		//if(idList.size() > 0){
			insert(updateMap, new IService() {
				@Override
				public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billCycleNew/verifyNewBillSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billCycleNew/verifyNewBillError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billCycleNew/verifyNewBillDetail");//审批详细信息地址
					List<String> idList = buyBillCycleService.updateSaveBillCycleInfo(updateMap);
					json.put("idList",idList);
					return json;
				}
			});
		/*}else {
			msg.setFlag(state_success);
			res.setCode(40099);
			res.setMsg("保存失败");
			msg.setRes(res);
			out2html(response, msg.toJsonString());
		}*/
		/*SONObject json1 = new JSONObject();

		try {
			final List<String> idList = buyBillCycleService.updateSaveBillCycleInfo(updateMap);
			if(idList.size() > 0){
				insert(updateMap, new IService() {
					@Override
					public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
						JSONObject json = new JSONObject();
						json.put("verifySuccess", "platform/buyer/billCycleNew/verifyNewBillSuccess");//审批成功调用的方法
						json.put("verifyError", "platform/buyer/billCycleNew/verifyNewBillError");//审批失败调用的方法
						json.put("relatedUrl", "platform/buyer/billCycleNew/verifyNewBillDetail");//审批详细信息地址
						json.put("idList",idList);
						return json;
					}
				});
				json1.put("success", true);
				json1.put("msg", "账单周期修改成功！");
			}else {
				json1.put("success", false);
				json1.put("msg", "账单周期修改失败！");
			}
		} catch (Exception e) {
			json1.put("success", false);
			json1.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json1.toString();*/
		/*Msg msg = new Msg();
		Response res = new Response();
		int updateSaveCount = buyBillCycleService.updateSaveBillCycleInfo(updateMap);*/
		/*if(updateSaveCount > 0){
			final List<String> idList = new ArrayList<String>();
			String billCycleId = (String) updateMap.get("billCycleId");//账单周期编号
			idList.add(billCycleId);
			update(billCycleId, updateMap, new IService() {
				@Override
				public Object init(Map<String, Object> params) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billCycle/verifySuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billCycle/verifyError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billCycle/verifyDetail");//审批详细信息地址
					json.put("idList",idList);
					return json;
				}
			});
			json.put("success", true);
			json.put("msg", "账单周期修改成功！");
		}else {
			msg.setFlag(state_success);
			res.setCode(40099);
			res.setMsg("保存失败");
			msg.setRes(res);
			out2html(response, msg.toJsonString());
		}*/
	}
	
	/**
	 * 根据账单周期编号查询关联利息
	 * @param interestMap
	 * @return
	 */
	@RequestMapping("queryInterestList")
	@ResponseBody
	public String queryInterestList(@RequestParam Map<String,Object> interestMap){
		String billCycleId = (String) interestMap.get("billCycleId");
		List<BuyBillInterest> interestList = buyBillCycleInterestService.getBillInteresInfo(billCycleId);
		return JSONObject.toJSONString(interestList);
	}
	
	/**
	 * 删除账单周期信息及关联信息
	 * @param
	 * @return
	 */
	@RequestMapping("deleteBillCycle")
	@ResponseBody
	public String deleteBillCycle(String id){
		//String billCycleId = request.getParameter("id");
		//根据账单编号查询账单信息
		JSONObject json = new JSONObject();
		try {
			buyBillCycleService.deleteByPrimaryKey(id);
			buyBillCycleInterestService.deleteByPrimaryKey(id);
			//sellerBillCycleService.deleteByPrimaryKey(id);
			json.put("success", true);
			json.put("msg", "删除成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "删除失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
}
