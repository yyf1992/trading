package com.nuotai.trading.buyer.service;

import org.apache.poi.hsmf.datatypes.PropertyValue.DoublePropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.mysql.fabric.xmlrpc.base.Data;
import com.nuotai.trading.buyer.dao.SysPurchasePriceMapper;
import com.nuotai.trading.buyer.model.SysPurchasePrice;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;



@Service
@Transactional
public class SysPurchasePriceService {

    private static final Logger LOG = LoggerFactory.getLogger(SysPurchasePriceService.class);

	@Autowired
	private SysPurchasePriceMapper sysPurchasePriceMapper;
	@Autowired
	private BuyProductSkuService skuService;
	
	public SysPurchasePrice get(String id){
		return sysPurchasePriceMapper.get(id);
	}
	
	public List<SysPurchasePrice> queryList(SearchPageUtil searchPageUtil){
		return sysPurchasePriceMapper.queryList(searchPageUtil);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysPurchasePriceMapper.queryCount(map);
	}
	
	public void add(SysPurchasePrice sysPurchasePrice){
		sysPurchasePriceMapper.add(sysPurchasePrice);
	}
	
	public void update(SysPurchasePrice sysPurchasePrice){
		sysPurchasePriceMapper.update(sysPurchasePrice);
	}
	
	public void delete(String id){
		sysPurchasePriceMapper.delete(id);
	}

	//通过公司id查询已经设置了采购价格的商品
	public List<String> selectExitProductId(String compId) {
		// TODO Auto-generated method stub
		return sysPurchasePriceMapper.selectExitProductId(compId);
	}

	public JSONObject insertPurchaseLatitude(String barcodeStr) {
		int result=0;
		JSONObject  json=new JSONObject();
		String [] purchaseObjArr=barcodeStr.split("\\|");
		for(String purchaseArr : purchaseObjArr){
			String [] purchase =purchaseArr.split(",");
			SysPurchasePrice purchasePrice = new SysPurchasePrice();
			purchasePrice.setId(ShiroUtils.getUid());
			purchasePrice.setCompanyId(ShiroUtils.getCompId());
			purchasePrice.setProductId(purchase[8].trim());
			purchasePrice.setProductCode(purchase[1].trim());
			purchasePrice.setProductName(purchase[0].trim());
			purchasePrice.setSkuCode(purchase[3].trim());
			purchasePrice.setSkuName(purchase[4].trim());
			purchasePrice.setBarcode(purchase[2].trim());
			purchasePrice.setUnitId(purchase[5].trim());
			purchasePrice.setPriceStatus(purchase[6].trim());
			purchasePrice.setPriceLatitude(new BigDecimal(purchase[7].trim()));
			purchasePrice.setCreateDate(new Date());
			purchasePrice.setUpdateDate(new Date());
			result=sysPurchasePriceMapper.insertPurchase(purchasePrice);
		}
		if(result>0){
			json.put("success", true);
			json.put("msg", "采购价格设置成功！");
		}else{
			json.put("success", false);
			json.put("msg", "采购价格设置失败！");
		}
		return json;
	}

	//加载采购报表
	public List<Map<String, Object>> getPurchaseData(Map<String, String> map) {
		// TODO Auto-generated method stub
		return sysPurchasePriceMapper.getPurchaseData(map);
	}

	public void updateFromExcel(List<SysPurchasePrice> purchaseList) {
		// TODO Auto-generated method stub
		if(purchaseList!=null && purchaseList.size()>0){
			Map<String,Object> attrMap=new HashMap<String,Object>();
			for (SysPurchasePrice sysPurchasePrice : purchaseList) {
				if(sysPurchasePrice.getBarcode()!=null){
					attrMap.put("barcode", sysPurchasePrice.getBarcode());
					List<SysPurchasePrice> exitList=sysPurchasePriceMapper.selectByMap(attrMap);
					if(exitList != null && exitList.size()>0){
						//如果存在就更新
						SysPurchasePrice updatepur=exitList.get(0);
						updatepur.setPriceStatus(sysPurchasePrice.getPriceStatus());
						updatepur.setPriceLatitude(sysPurchasePrice.getPriceLatitude());
						updatepur.setUpdateDate(new Date());
						sysPurchasePriceMapper.update(updatepur);
					}else{
						//不存在就插入
						List<BuyProductSku> sku=skuService.selectByBarCode(sysPurchasePrice.getBarcode());
						sysPurchasePrice.setId(ShiroUtils.getUid());
						sysPurchasePrice.setCompanyId(ShiroUtils.getCompId());
						sysPurchasePrice.setProductId(sku.get(0).getId());
						sysPurchasePrice.setProductCode(sku.get(0).getProductCode());
						sysPurchasePrice.setProductName(sku.get(0).getProductName());
						sysPurchasePrice.setSkuCode(sku.get(0).getSkuCode());
						sysPurchasePrice.setSkuName(sku.get(0).getSkuName());
						sysPurchasePrice.setUnitId(sku.get(0).getUnitId());
						sysPurchasePrice.setCreateDate(new Date());
						sysPurchasePriceMapper.insertPurchase(sysPurchasePrice);
					}
				}
			}
		}
	}

	public List<SysPurchasePrice> selectByMap(Map map) {
		// TODO Auto-generated method stub
		return sysPurchasePriceMapper.selectByMap(map);
	}

	public JSONObject updatePurchase(SysPurchasePrice purchase) {
		// TODO Auto-generated method stub
		JSONObject json=new JSONObject();
		purchase.setUpdateDate(new Date());
		int result=sysPurchasePriceMapper.updatePurchase(purchase);
		if(result>0){
			json.put("success", true);	
			json.put("msg", "价格修改成功！");	
		}else{
			json.put("success", false);	
			json.put("msg", "价格修改失败！");	
		}
		return json;
	}

	public JSONObject deletePurchase(String productId) {
		// TODO Auto-generated method stub
		JSONObject json=new JSONObject();
		int result=sysPurchasePriceMapper.deletePurchase(productId);
		if(result>0){
			json.put("success", true);	
			json.put("msg", "价格删除成功！");	
		}else{
			json.put("success", false);	
			json.put("msg", "价格删除失败！");	
		}
		return json;
	}
	
}
