package com.nuotai.trading.buyer.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyCustomExportData;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyCustomerItem;
import com.nuotai.trading.buyer.service.BuyCustomerItemService;
import com.nuotai.trading.buyer.service.BuyCustomerService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 售后服务
 * @author dxl
 *
 */
@Controller
@RequestMapping("platform/buyer/customer")
public class BuyCustomerController extends BaseController {
	@Autowired
	private BuyCustomerService customerService;
	@Autowired
	private BuyCustomerItemService customerItemService;
	@Autowired
	private BuyAddressService addressService;
	@Autowired
	private BuySupplierFriendService buySupplierFriendService;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	
	/**创建售后单
	 * 
	 * @return
	 */
	@RequestMapping("addCustomer")
	public String addCustomer(){
		//查询收货地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue());
		addressParams.put("compId", ShiroUtils.getCompId());
		List<BuyAddress> addressList = addressService.selectByMap(addressParams);
		//加载仓库下拉数据
		List<TBusinessWharea> whareaList = tBusinessWhareaService.getCompanyId();
		//加载供应商下拉数据
		List<Map<String,Object>> supplierList=buySupplierFriendService.getSupplier();
		model.addAttribute("addressList", addressList);
		model.addAttribute("whareaList", whareaList);
		model.addAttribute("supplierList", supplierList);
		return "platform/buyer/customer/addCustomer";
	}
	
	/**
	 * 创建售后单-选择商品页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadSelectProduct")
	public String loadSelectProduct(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		customerService.getAftermarketProduct(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/customer/productList";
	}
	
	/**
	 * 创建售后单保存
	 * @param map
	 * @return
	 */
	@RequestMapping("saveAddCustomer")
	public void saveAddCustomer(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/customer/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/customer/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "platform/buyer/customer/loadCustomerDetails");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = customerService.saveAddCustomer(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = customerService.verifySuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = customerService.verifyError(id);
		return json.toString();
	}
	
	/**
	 * 互通售后服务管理
	 */
	@RequestMapping("customerList")
	public String customerList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "99");
		}
		//获得不同状态数量
		customerService.getCustomerListNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/customer/customerList";
	}
	
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = customerService.loadCustomerData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 查看售后明细页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadCustomerDetails")
	public String loadCustomerDetails(String id,String returnUrl,String menuId) throws Exception {
		BuyCustomer customer = customerService.get(id);
		model.addAttribute("customer", customer);
		List<BuyCustomerItem> itemList = customerItemService.selectCustomerItemByCustomerId(id);
		model.addAttribute("itemList", itemList);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("menuId", menuId);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/customer/customerDetails";
	}
	
	/**
	 * 售后凭证展示页面
	 * @param proof
	 * @return
	 */
	@RequestMapping("showProof")
	public String showProof(String proof){
		model.addAttribute("proof", proof);
		return "platform/buyer/customer/showProof";
	}
	
	/**
	 * 取消售后申请保存
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/saveCancelCustomer", method = RequestMethod.POST)
	@ResponseBody
	public String saveCancelCustomer(String id){
		JSONObject json = new JSONObject();
		try {
			customerService.saveCancelCustomer(id);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 修改售后申请
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("updateCustomer")
	public String updateCustomer(String id) throws Exception {
		BuyCustomer customer = customerService.get(id);
		model.addAttribute("customer", customer);
		List<BuyCustomerItem> itemList = customerItemService.selectCustomerItemByCustomerId(id);
		model.addAttribute("itemList", itemList);
		//查询收货地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue());
		addressParams.put("compId", ShiroUtils.getCompId());
		List<BuyAddress> addressList = addressService.selectByMap(addressParams);
		//加载仓库下拉数据
		List<TBusinessWharea> whareaList = tBusinessWhareaService.getCompanyId();
		//加载供应商下拉数据
		List<Map<String,Object>> supplierList=buySupplierFriendService.getSupplier();
		model.addAttribute("addressList", addressList);
		model.addAttribute("whareaList", whareaList);
		model.addAttribute("supplierList", supplierList);
		return "platform/buyer/customer/updateCustomer";
	}
	
	/**
	 * 修改售后单保存
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveUpdateCustomer")
	public void saveUpdateCustomer(@RequestParam Map<String,Object> params){
		update(params.get("updateId").toString(),params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/customer/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/customer/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "platform/buyer/customer/loadCustomerDetails");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = customerService.saveUpdateCustomer(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 推送oms
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/pushOms", method = RequestMethod.POST)
	@ResponseBody
	public String pushOms(String id){
		JSONObject json = new JSONObject();
		try {
			json = customerService.pushOms(id);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 换货转为退货
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/conversionType", method = RequestMethod.POST)
	@ResponseBody
	public String conversionType(String itemId){
		JSONObject json = new JSONObject();
		try {
			json = customerService.conversionType(itemId);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	

	/**
	 * Export order.
	 * 售后订单导出
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportCustomer")
	public void exportOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<BuyCustomExportData> exportDataList = customerService.getExportList(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("售后订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		normalStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		normalStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		normalStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		normalStyle.setBorderTop(BorderStyle.THIN);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"售后单号","创建日期",
				"商品货号","商品名称","规格名称","条形码","售后数量","oms出库数量","卖家收货数量","售后类型","申请原因","创建人",
				"发货详情","发货数量","到货数量","供应商","最近到货日期","采购单价","单个返修金额","售后总金额","OMS出库时间"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i = 0; i < exportDataList.size(); i++) {
				BuyCustomExportData  exportData = exportDataList.get(i);
				//因为第0行已经设置标题了,所以从行2开始写入数据
				row=sheet.createRow(i+1);
				//1.售后单号
				cell = row.createCell(0);
				cell.setCellValue(exportData.getCustomerCode());
				cell.setCellStyle(normalStyle);
				//2.订单日期
				cell = row.createCell(1);
				cell.setCellValue(sdf.format(exportData.getCreateDate())+"");
				cell.setCellStyle(normalStyle);
				//3.商品货号
				cell = row.createCell(2);
				cell.setCellValue(exportData.getProductCode());
				cell.setCellStyle(normalStyle);
				//4.商品名称
				cell = row.createCell(3);
				cell.setCellValue(exportData.getProductName());
				cell.setCellStyle(normalStyle);
				//5.规格名称
				cell = row.createCell(4);
				cell.setCellValue(exportData.getSkuName());
				cell.setCellStyle(normalStyle);
				//6.条形码
				cell = row.createCell(5);
				cell.setCellValue(exportData.getSkuOid());
				cell.setCellStyle(normalStyle);
				//7.售后数量
				cell = row.createCell(6);
				cell.setCellValue(exportData.getGoodsNumber());
				cell.setCellStyle(normalStyle);
				//8.oms出库数量
				cell = row.createCell(7);
				cell.setCellValue(ObjectUtil.isEmpty(exportData.getReceiveNumber())?0:exportData.getReceiveNumber());
				cell.setCellStyle(normalStyle);
				//9.卖家收货数量
				cell = row.createCell(8);
				cell.setCellValue(ObjectUtil.isEmpty(exportData.getConfirmNumber())?0:exportData.getConfirmNumber());
				cell.setCellStyle(normalStyle);
				//10.售后类型
				cell = row.createCell(9);
				cell.setCellValue(exportData.getType());
				cell.setCellStyle(normalStyle);
				//11.申请原因
				cell = row.createCell(10);
				cell.setCellValue(exportData.getReason());
				cell.setCellStyle(normalStyle);
				//12.创建人
				cell = row.createCell(11);
				cell.setCellValue(exportData.getCreateName());
				cell.setCellStyle(normalStyle);
				//发货详情
				cell = row.createCell(12);
				cell.setCellValue(exportData.getDeliveryDetial());
				cell.setCellStyle(normalStyle);
				//14.发货数量
				cell = row.createCell(13);
				cell.setCellValue(exportData.getDeliveryNum());
				cell.setCellStyle(normalStyle);
				//15.到货数量
				cell = row.createCell(14);
				cell.setCellValue(exportData.getArrivalNum());
				cell.setCellStyle(normalStyle);
				//16.供应商
				cell = row.createCell(15);
				cell.setCellValue(exportData.getSupplierName());
				cell.setCellStyle(normalStyle);
				//17.最近到货日期
				cell = row.createCell(16);
				cell.setCellValue(exportData.getArrivalDate());
				cell.setCellStyle(normalStyle);
				//18.采购单价
				cell = row.createCell(17);
				if(exportData.getPrice() == null || "".equals(exportData.getPrice())){
					cell.setCellValue("");
				}else{
					cell.setCellValue(fnum.format(exportData.getPrice()));
				}
				cell.setCellStyle(normalStyle);
				//19.单个返修金额
				cell = row.createCell(18);
				if(exportData.getExchangePrice() == null || "".equals(exportData.getExchangePrice())){
					cell.setCellValue("");
				}else{
					cell.setCellValue(fnum.format(exportData.getExchangePrice()));
				}
				cell.setCellStyle(normalStyle);
				//20.售后总金额
				cell = row.createCell(19);
				if(exportData.getRepairPrice() == null || "".equals(exportData.getRepairPrice())){
					cell.setCellValue("");
				}else{
					cell.setCellValue(fnum.format(exportData.getRepairPrice()));
				}
				cell.setCellStyle(normalStyle);
				//21.oms出库时间
				cell = row.createCell(20);
				if(exportData.getOmsFinishdate()== null || "".equals(exportData.getOmsFinishdate())){
					cell.setCellValue("");	
				}else{
					cell.setCellValue(sdf.format(exportData.getOmsFinishdate())+"");
				}
				cell.setCellStyle(normalStyle);
			}
		}
		ExcelUtil.preExport("售后订单导出", response);
		ExcelUtil.export(wb, response);
	}
}
