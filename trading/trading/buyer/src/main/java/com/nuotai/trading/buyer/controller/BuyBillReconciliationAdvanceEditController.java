package com.nuotai.trading.buyer.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvance;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvanceEdit;
import com.nuotai.trading.buyer.service.BuyBillReconciliationAdvanceEditService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 * 
 * @author "
 * @date 2018-03-21 18:25:46
 */
@Controller
@RequestMapping("platform/buyer/billAdvanceItem")
public class BuyBillReconciliationAdvanceEditController extends BaseController{
	@Autowired
	private BuyBillReconciliationAdvanceEditService buyBillAdvanceEditService;

	//预付款详情列表展示
	@RequestMapping("billAdvanceItemList")
	public String billAdvanceItemList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("acceptStatus")){
			params.put("acceptStatus","0");
		}
		params.put("buyCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<BuyBillReconciliationAdvanceEdit> billAdvanceItemList = buyBillAdvanceEditService.queryAdvanceEditList(searchPageUtil);
		buyBillAdvanceEditService.queryEditCountByStatus(params);
		searchPageUtil.getPage().setList(billAdvanceItemList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billadvance/billAdvanceItemList";
	}

	// 跳转到添加页面
	@RequestMapping("openChargeAdvance")
	public String openChargeAdvance(HttpServletRequest request){
		return "platform/buyer/billcycles/billadvance/addChargeJump";
	}

	//修改驳回充值跳转
	@RequestMapping("updateChargeAdvance")
	public String updateChargeAdvance(HttpServletRequest request){
		String editId = request.getParameter("advanceEditId").toString();
		BuyBillReconciliationAdvanceEdit buyBillRecAdvanceEdit = buyBillAdvanceEditService.rechargeDetail(editId);
		model.addAttribute("buyBillRecAdvanceEdit",buyBillRecAdvanceEdit);
		return "platform/buyer/billcycles/billadvance/updateChargeJump";
	}

	/**
	 * 付款明细管理列表查询
	 * @return
	 */
	@RequestMapping("approveBillAdvanceEdit")
	public String approveBillAdvanceEdit(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		buyBillAdvanceEditService.approveAdvanceEdit(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/buyer/billcycles/billadvance/approveBillAdvance";
	}

	// 审批一键通过
	@RequestMapping("verifyBillAdvanceEditAll")
	@ResponseBody
	public String verifyBillAdvanceEditAll(String acceptId){
		JSONObject json = buyBillAdvanceEditService.verifyAdvanceEditAll(acceptId);
		return json.toJSONString();
	}

	//充值或修改充值
	@RequestMapping("addChargeAdvance")
	@ResponseBody
	public void addChargeAdvance(@RequestParam final Map<String,Object> addChargeMap){
		String itemStatus = addChargeMap.get("itemStatus").toString();
		if("1".equals(itemStatus)){
			insert(addChargeMap, new IService() {
				@Override
				public JSONObject init(Map<String, Object> saveMap) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billAdvanceItem/rechargeSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billAdvanceItem/rechargeError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billAdvanceItem/rechargeDetail");//审批详细信息地址
					List<String> idList = buyBillAdvanceEditService.addAdvanceCharge(addChargeMap);
					json.put("idList",idList);
					return json;
				}
			});
		}else if("3".equals(itemStatus)){
			//更新的时候判断是否走审批，no的话不用走update里边的monitorUpdate方法，其他正常
			addChargeMap.put("checkApprove","no");
			updateFuKuan(String.valueOf(addChargeMap.get("itemId")),addChargeMap, new IService(){
				@Override
				public JSONObject init(Map<String,Object> params) throws ServiceException {
					JSONObject json = new JSONObject();
					json.put("verifySuccess", "platform/buyer/billAdvanceItem/rechargeSuccess");//审批成功调用的方法
					json.put("verifyError", "platform/buyer/billAdvanceItem/rechargeError");//审批失败调用的方法
					json.put("relatedUrl", "platform/buyer/billAdvanceItem/rechargeDetail");//审批详细信息地址
					List<String> idList = new ArrayList<>();
					idList.add(String.valueOf(addChargeMap.get("itemId")));
					json.put("idList", idList);
					buyBillAdvanceEditService.updateAdvanceCharge(addChargeMap);
					return json;
				}
			});
		}
	}

	//内部审批通过
	@RequestMapping(value = "/rechargeSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String rechargeSuccess(String id){
		JSONObject json = buyBillAdvanceEditService.rechargeSuccess(id);
		return json.toString();
	}

	//内部审批驳回
	@RequestMapping(value = "/rechargeError", method = RequestMethod.GET)
	@ResponseBody
	public String rechargeError(String id){
		JSONObject json = buyBillAdvanceEditService.rechargeError(id);
		return json.toString();
	}

	//审批数据回显
	@RequestMapping(value = "/rechargeDetail",method = RequestMethod.GET)
	public String rechargeDetail(String id){
		//预付款审批数据查询
		BuyBillReconciliationAdvanceEdit billAdvanceEditInfo = buyBillAdvanceEditService.rechargeDetail(id);
		if(null != billAdvanceEditInfo){
			model.addAttribute("billAdvanceEditInfo",billAdvanceEditInfo);
		}
		return "platform/buyer/billcycles/billadvance/acceptAdvanceDetail";
	}

	//审批数据弹框回显
	@RequestMapping(value = "/rechargeDetailView",method = RequestMethod.GET)
	public String rechargeDetailView(String id){
		//预付款审批数据查询
		BuyBillReconciliationAdvanceEdit billAdvanceEditInfo = buyBillAdvanceEditService.rechargeDetail(id);
		if(null != billAdvanceEditInfo){
			model.addAttribute("billAdvanceEditInfo",billAdvanceEditInfo);
		}
		return "platform/buyer/billcycles/billadvance/acceptAdvanceDetail";
	}

	//充值记录审批数据回显
	@RequestMapping(value = "/acceptRechargeDetail",method = RequestMethod.GET)
	@ResponseBody
	public String acceptRechargeDetail(String id){
		//预付款审批数据查询
		BuyBillReconciliationAdvanceEdit billAdvanceEditInfo = buyBillAdvanceEditService.rechargeDetail(id);
		return billAdvanceEditInfo==null?"{}":net.sf.json.JSONObject.fromObject(billAdvanceEditInfo).toString();
	}

	@RequestMapping("advanceEditExport")
	public void advanceEditExport(@RequestParam Map<String,Object> advanceEditMap){
		advanceEditMap.put("buyCompanyId", ShiroUtils.getCompId());
		List<BuyBillReconciliationAdvanceEdit> advanceEditList = buyBillAdvanceEditService.selAdvanceEditList(advanceEditMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("预付款充值报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"充值单号","预付款账号","供应商","采购员","充值金额","申请时间","申请人","充值状态","审批状态","备注"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("预付款充值报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != advanceEditList && advanceEditList.size()>0){
			for(BuyBillReconciliationAdvanceEdit buyAdvanceEditInfo : advanceEditList){
				row=sheet.createRow(rowCount);
				//1.充值编号
				cell = row.createCell(0);
				cell.setCellValue(buyAdvanceEditInfo.getId());
				cell.setCellStyle(normalStyle);
				//1.预付款账户编号
				cell = row.createCell(1);
				cell.setCellValue(buyAdvanceEditInfo.getAdvanceId());
				cell.setCellStyle(normalStyle);
				//2.供应商
				cell = row.createCell(2);
				cell.setCellValue(buyAdvanceEditInfo.getSellerCompanyName());
				cell.setCellStyle(normalStyle);
				//2.下单人
				cell = row.createCell(3);
				cell.setCellValue(buyAdvanceEditInfo.getCreateBillUserName());
				cell.setCellStyle(normalStyle);

				//5.申请金额
				cell = row.createCell(4);
				cell.setCellValue(fnum.format(buyAdvanceEditInfo.getUpdateTotal()));
				cell.setCellStyle(normalStyle);
				//6.申请时间
				cell = row.createCell(5);
				cell.setCellValue(buyAdvanceEditInfo.getCreateTimeStr());
				cell.setCellStyle(normalStyle);
				//7.申请人
				cell = row.createCell(6);
				cell.setCellValue(buyAdvanceEditInfo.getCreateUserName());
				cell.setCellStyle(normalStyle);
				//3.充值状态
				cell = row.createCell(7);
				String editStatusP = buyAdvanceEditInfo.getEditStatus();
				String editStatusPStr = "";
				if("1".equals(editStatusP)){
					editStatusPStr = "充值中";
				}else if("2".equals(editStatusP)){
					editStatusPStr = "成功";
				}else if("3".equals(editStatusP)){
					editStatusPStr = "失败";
				}
				cell.setCellValue(editStatusPStr);
				cell.setCellStyle(normalStyle);
				//3.审批状态
				cell = row.createCell(8);
				String editStatus = buyAdvanceEditInfo.getEditStatus();
				String editStatusStr = "";
				if("1".equals(editStatus)){
					editStatusStr = "内部审批中";
				}else if("2".equals(editStatus)){
					editStatusStr = "内部审批通过";
				}else if("3".equals(editStatus)){
					editStatusStr = "内部审批驳回";
				}
				cell.setCellValue(editStatusStr);
				cell.setCellStyle(normalStyle);
				//4.添加备注
				cell = row.createCell(9);
				cell.setCellValue(buyAdvanceEditInfo.getAdvanceRemarks());
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("预付款充值信息", response);
		ExcelUtil.export(wb, response);
	}
}
