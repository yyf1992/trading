package com.nuotai.trading.buyer.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyApplypurchaseHeaderMapper;
import com.nuotai.trading.buyer.model.BuyApplypurchaseHeader;
import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.buyer.model.BuyApplypurchaseShop;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.TradeVerifyCopy;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.service.SysApprovalService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DateUtils;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyApplypurchaseHeaderService implements BuyApplypurchaseHeaderMapper{

	@Autowired
	private BuyApplypurchaseHeaderMapper mapper;
	@Autowired
	private BuyApplypurchaseItemService applypurchaseItemService;
	@Autowired
	private BuyApplypurchaseShopService applypurchaseShopService;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private SysApprovalService sysApprovalService;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuyApplypurchaseHeader record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyApplypurchaseHeader record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuyApplypurchaseHeader selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyApplypurchaseHeader record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BuyApplypurchaseHeader record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<BuyApplypurchaseHeader> selectByPage(
			SearchPageUtil searchPageUtil) {
		return mapper.selectByPage(searchPageUtil);
	}
	
	/**
	 * 创建采购计划保存
	 * @param map
	 * @throws Exception
	 */
	public List<String> saveAddPurchase(Map<String,Object> map) throws Exception{
		List<String> idList = new ArrayList<String>();
		ShiroUtils util = new ShiroUtils();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		BuyApplypurchaseHeader ph = new BuyApplypurchaseHeader();
		ph.setId(util.getUid());
		ph.setCompanyId(util.getCompId());
		ph.setTitle(map.get("title").toString());
		String remark = map.get("remark") == null ? "" : map.get("remark").toString();
		ph.setRemark(remark);
		ph.setGoodsCount(new BigDecimal(map.get("numSum").toString()));
		ph.setStatus("0");
		ph.setIsDel(Constant.IsDel.NODEL.getValue());
		ph.setCreater(user.getId());
		ph.setCreateName(user.getUserName());
		ph.setApplyCode("CGJH" + DateUtils.format(new Date(),"yyyyMMddHHmmss") + ShiroUtils.getRandom(3)); // 采购单号时间戳
		
		BuyApplypurchaseItem ai = new BuyApplypurchaseItem();
		// 拆分选择的商品信息 的字符串,同时添加明细表
		String[] goodsStr = map.get("goodsStr").toString().split("@NTNT@");
		for (int i = 0; i < goodsStr.length; i++) {
			String s2[] = goodsStr[i].split(".NTNT.");
			ai.setId(util.getUid());
			ai.setApplyId(ph.getId());
			ai.setProductCode(s2[1]);
			ai.setProductName(s2[2]);
			ai.setSkuCode(s2[3]);
			ai.setSkuName(s2[4]);
			ai.setBarcode(s2[0]);
			ai.setUnitId(s2[5]);
			ai.setUnitName(s2[6]);
			ai.setProductType(Integer.parseInt(s2[7]));
			ai.setApplyCount(Integer.parseInt(s2[8]));
			ai.setProductStatus(s2[9]);
			ai.setPurchasCycle(Integer.parseInt(s2[10]));
			ai.setMonthSaleNum(Integer.parseInt(s2[11]));
			ai.setMonthDms(Integer.parseInt(s2[12]));
			ai.setScStock(Integer.parseInt(s2[13]));
			ai.setOutStock(Integer.parseInt(s2[14]));
			ai.setTransitNum(Integer.parseInt(s2[15]));
			ai.setTotalStock(Integer.parseInt(s2[16]));
			ai.setSalePlan(Integer.parseInt(s2[17]));
			ai.setConfirmSalePlan(Integer.parseInt(s2[18]));
			ai.setDifferenceNum(Integer.parseInt(s2[19]));
			ai.setPredictNextMonthArrival(Integer.parseInt(s2[20]));
			ai.setPredictNextMonthStock(Integer.parseInt(s2[21]));
			ai.setRemark(s2[22] == null ? "" : s2[22]);
			ai.setPutStorageNum(Integer.parseInt(s2[23]));
			ai.setConfirmPutStorageNum(Integer.parseInt(s2[24]));
			
	    	String item = goodsStr[i].substring(goodsStr[i].indexOf("{"),goodsStr[i].length());
			net.sf.json.JSONObject jsonItem = net.sf.json.JSONObject.fromObject(item);
		    net.sf.json.JSONArray shopArray = jsonItem.getJSONArray("shopList");
		    BuyApplypurchaseShop shop = new BuyApplypurchaseShop();
		    for(int j = 0 ; j < shopArray.size(); j++){
		    	net.sf.json.JSONObject jsonShop = shopArray.getJSONObject(j);
		    	shop.setId(util.getUid());
		    	shop.setItemId(ai.getId());
		    	shop.setPlanCode(jsonShop.get("planCode").toString());
		    	shop.setShopId(jsonShop.get("shopId").toString());
		    	shop.setShopCode(jsonShop.get("shopCode").toString());
		    	shop.setShopName(jsonShop.get("shopName").toString());
		    	shop.setSaleStartDate(sf.parse(jsonShop.get("saleStartDate").toString()));
		    	shop.setSaleEndDate(sf.parse(jsonShop.get("saleEndDate").toString()));
		    	shop.setSaleDays(Integer.parseInt(jsonShop.get("saleDays").toString()));
		    	shop.setSalesNum(Integer.parseInt(jsonShop.get("salesNum").toString()));
		    	shop.setConfirmSalesNum(Integer.parseInt(jsonShop.get("confirmSalesNum").toString()));
		    	shop.setPutStorageNum(Integer.parseInt(jsonShop.get("putStorageNum").toString()));
		    	shop.setConfirmPutStorageNum(Integer.parseInt(jsonShop.get("confirmPutStorageNum").toString()));
		    	applypurchaseShopService.add(shop);
		    }
			applypurchaseItemService.insert(ai); // 店铺采购单明细表添加
		}
		ph.setCreateDate(date);
		String url3 = map.get("url3") == null ? "" : map.get("url3").toString();
		ph.setProof(url3);
		String predictArred_date = map.containsKey("predictArred") ? map.get("predictArred") != null
				&& !"".equals(map.get("predictArred")) ? map.get(
				"predictArred").toString() : "" : "";
		if (!"".equals(predictArred_date)) {
			ph.setPredictarred(sf.parse(predictArred_date));
		}
		mapper.insert(ph); // 采购单添加
		idList.add(ph.getId());
		return idList;
	}
	
	/**
	 * 采购计划审核通过
	 * @param id
	 * @return
	 */
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		BuyApplypurchaseHeader header = new BuyApplypurchaseHeader();
		header.setId(id);
		header.setStatus(Constant.PurchaseStatus.VERIFYPASS.getValue()+"");
		int result = mapper.updateByPrimaryKeySelective(header);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	
	/**
	 * 采购计划审核驳回
	 * @param id
	 * @return
	 */
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		BuyApplypurchaseHeader header = mapper.selectByPrimaryKey(id);
		header.setStatus(Constant.PurchaseStatus.VERIFYNOPASS.getValue()+"");
		int result = mapper.updateByPrimaryKeySelective(header);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 采购计划-列表订单数量
	 * @param params
	 */
	public void getPurchaseNum(Map<String, Object> params) {
		//待审核
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("status",Constant.PurchaseStatus.WAITVERIFY.getValue());
		int waitVerifyNum = selectPurchaseNumByMap(selectMap);
		//审核通过
		selectMap.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		int verifyPassNum = selectPurchaseNumByMap(selectMap);
		//审核不通过
		selectMap.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		int verifyNoPassNum = selectPurchaseNumByMap(selectMap);
		//完结
		selectMap.put("status", Constant.PurchaseStatus.CANCEL.getValue());
		int cancelNum = selectPurchaseNumByMap(selectMap);
		params.put("waitVerifyNum", waitVerifyNum);
		params.put("verifyPassNum", verifyPassNum);
		params.put("verifyNoPassNum", verifyNoPassNum);
		params.put("cancelNum", cancelNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			break;
		case 1:
			params.put("interest", "1");
			break;
		case 2:
			params.put("interest", "2");
			break;
		case 3:
			params.put("interest", "3");
			break;
		default:
			params.put("interest", "0");
			break;
		}
	}
	
	@Override
	public int selectPurchaseNumByMap(Map<String, Object> params) {
		return mapper.selectPurchaseNumByMap(params);
	}
	
	/**
	 * 获得数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public String loadData(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/applypurchase/planList/";
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			returnPath += "allPurchase";
			break;
		case 1:
			params.put("interest", "1");
			returnPath += "waitVerify";
			break;
		case 2:
			params.put("interest", "2");
			returnPath += "verifyPass";
			break;
		case 3:
			params.put("interest", "3");
			returnPath += "verifyNoPass";
			break;
		case 4:
			params.put("interest", "4");
			returnPath += "cancelPurchase";
			break;
		default:
			returnPath += "allPurchase";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待审批
			params.put("status", Constant.PurchaseStatus.WAITVERIFY.getValue());
		}else if("2".equals(interest)){
			//审核通过
			params.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		}else if("3".equals(interest)){
			//审核不通过
			params.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		}else if("4".equals(interest)){
			//已取消
			params.put("status", Constant.PurchaseStatus.CANCEL.getValue());
		}
		searchPageUtil.setObject(params);
		List<String> applyIdListNew = new ArrayList<>();
		//
		String procode = params.containsKey("procode")?params.get("procode")!=null?params.get("procode").toString():"":"";
		String planCode = params.containsKey("planCode")?params.get("planCode")!=null?params.get("planCode").toString():"":"";
//		String skucode = params.containsKey("skucode")?params.get("skucode")!=null?params.get("skucode").toString():"":"";
//		String skuname = params.containsKey("skuname")?params.get("skuname")!=null?params.get("skuname").toString():"":"";
		String skuoid = params.containsKey("skuoid")?params.get("skuoid")!=null?params.get("skuoid").toString():"":"";
		if(!StringUtils.isEmpty(procode)
			||!StringUtils.isEmpty(planCode)
//			||!StringUtils.isEmpty(skucode)
//			||!StringUtils.isEmpty(skuname)
			||!StringUtils.isEmpty(skuoid)
		){
			List<String> applyIdList2 = mapper.selectApprovedId(params);
			applyIdListNew = applyIdList2;
			params.put("approved", true);
			params.put("applyIdList", applyIdListNew);
		}
		searchPageUtil.setObject(params);
		List<BuyApplypurchaseHeader> applyPurchaseList = mapper.selectByPage(searchPageUtil);
		if(!applyPurchaseList.isEmpty()){
			for(BuyApplypurchaseHeader applyPurchase:applyPurchaseList){
				List<BuyApplypurchaseItem> applyItemList = applypurchaseItemService.selectByHeaderId(applyPurchase.getId());
				if(!applyItemList.isEmpty()){
					for (BuyApplypurchaseItem applyItem:applyItemList) {
						List<BuyApplypurchaseShop> applyShopList = applypurchaseShopService.selectByItemId(applyItem.getId());
						applyItem.setShopList(applyShopList);
					}
				}
				applyPurchase.setItemList(applyItemList);
				//是否下单
    			List<BuyOrderProduct> orderProductList = orderProductService.selectByApplyCode(applyPurchase.getApplyCode());
        		if(orderProductList != null && orderProductList.size() > 0){//已下单不允许修改
        			applyPurchase.setIsAllowUpdate("1");
        		}else{//可修改
        			applyPurchase.setIsAllowUpdate("0");
        		}
			}
		}
		searchPageUtil.getPage().setList(applyPurchaseList);
		return returnPath;
	}
	
	public BuyApplypurchaseHeader selectById(String id){
		BuyApplypurchaseHeader header = mapper.selectByPrimaryKey(id);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		if(header != null){
			if(header.getPredictarred() != null && !header.getPredictarred().equals("")){
				String str = sdf.format(header.getPredictarred());  
				header.setPredictarredStr(str);
			}else{
				header.setPredictarredStr("");
			}
			// 明细表
			List<BuyApplypurchaseItem> itemList = applypurchaseItemService.selectByHeaderId(id);
			if(itemList!= null && itemList.size() > 0){
				header.setItemList(itemList);
				for(BuyApplypurchaseItem item : itemList){
					List<BuyApplypurchaseShop> shopList = applypurchaseShopService.selectByItemId(item.getId());
					if(shopList!= null && shopList.size() > 0){
						item.setShopList(shopList);
					}
				}
			}
		}
		return header;
	}
	
	/**
	 * 修改采购计划保存
	 * @param map
	 * @throws Exception
	 */
	public List<String> saveUpdatePurchase(Map<String,Object> map) throws Exception{
		List<String> idList = new ArrayList<String>();
		ShiroUtils util = new ShiroUtils();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Date date = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		BuyApplypurchaseHeader ph = mapper.selectByPrimaryKey(map.get("id").toString());
		if(ph.getStatus().equals(Constant.PurchaseStatus.VERIFYPASS.getValue())){
			//没有设置审批流程未下单可修改
			SysVerifyHeader verifyheader = sysApprovalService.getApprovalByMenuId("17070718133683994602");//销售计划
        	if(verifyheader == null || verifyheader.getId() == null){//没有设置审批流程
        		//是否下单
    			List<BuyOrderProduct> orderProductList = orderProductService.selectByApplyCode(ph.getApplyCode());
        		if(orderProductList != null && orderProductList.size() > 0){//已下单不允许修改
        			throw new Exception("审核通过且已下单不允许修改！");
        		}else{
        			ph.setStatus("1");
        		}
        	}else{
        		ph.setStatus("0");
        	}
		}else if(ph.getStatus().equals(Constant.PurchaseStatus.CANCEL.getValue())){
			throw new Exception("取消后不允许修改！");
		}
		ph.setTitle(map.get("title").toString());
		String remark = map.get("remark") == null ? "" : map.get("remark").toString();
		ph.setRemark(remark);
		ph.setGoodsCount(new BigDecimal(map.get("numSum").toString()));
		ph.setShopId(map.get("shopId").toString());
		ph.setShopCode(map.get("shopCode").toString());
		ph.setShopName(map.get("shopName").toString());
		ph.setIsDel(Constant.IsDel.NODEL.getValue());
		ph.setUpdateId(user.getId());
		ph.setUpdateName(user.getUserName());
		
		List<BuyApplypurchaseItem> itemList = applypurchaseItemService.selectByHeaderId(ph.getId());
		if(itemList != null&& itemList.size() > 0){
			for(BuyApplypurchaseItem item : itemList){
				applypurchaseItemService.deleteByPrimaryKey(item.getId());
			}
		}
		BuyApplypurchaseItem ai = new BuyApplypurchaseItem();
		// 拆分选择的商品信息 的字符串,同时添加明细表
		String[] goodsStr = map.get("goodsStr").toString().split("@");
		for (int j = 0; j < goodsStr.length; j++) {
			String s2[] = goodsStr[j].split(",");
			ai.setId(util.getUid());
			ai.setApplyId(ph.getId());
			ai.setProductCode(s2[1]);
			ai.setProductName(s2[2]);
			ai.setSkuCode(s2[3]);
			ai.setSkuName(s2[4]);
			ai.setBarcode(s2[0]);
			ai.setUnitId(s2[5]);
			ai.setUnitName(s2[6]);
			ai.setProductStatus(s2[7]);
			ai.setPurchasCycle(Integer.parseInt(s2[8]));
			ai.setMonthSaleNum(Integer.parseInt(s2[9]));
			ai.setMonthDms(Integer.parseInt(s2[10]));
			ai.setScStock(Integer.parseInt(s2[11]));
			ai.setOutStock(Integer.parseInt(s2[12]));
			ai.setTransitNum(Integer.parseInt(s2[13]));
			ai.setTotalStock(Integer.parseInt(s2[14]));
			ai.setSalePlan(Integer.parseInt(s2[15]));
			ai.setDifferenceNum(Integer.parseInt(s2[16]));
			ai.setPredictNextMonthArrival(Integer.parseInt(s2[17]));
			ai.setPredictNextMonthStock(Integer.parseInt(s2[18]));
			ai.setRemark(s2[19] == null ? "" : s2[19]);
			ai.setApplyCount(Integer.parseInt(s2[20]));
			ai.setType(s2[21]);
			ai.setProductType(Integer.parseInt(s2[22]));
			if (!"".equals(s2[23])) {
				ai.setSaleStartDate(sf.parse(s2[23]));
				ai.setSaleEndDate(sf.parse(s2[24]));
			}
			ai.setSaleDays(Integer.parseInt(s2[25]));
			applypurchaseItemService.insert(ai); // 店铺采购单明细表添加
		}
		ph.setUpdateDate(date);
		String url3 = map.get("url3") == null ? "" : map.get("url3").toString();
		ph.setProof(url3);
		String predictArred_date = map.containsKey("predictArred") ? map.get("predictArred") != null
				&& !"".equals(map.get("predictArred")) ? map.get(
				"predictArred").toString() : "" : "";
		if (!"".equals(predictArred_date)) {
			ph.setPredictarred(sf.parse(predictArred_date));
		}
		mapper.updateByPrimaryKeySelective(ph); // 采购单修改
		idList.add(ph.getId());
		return idList;
	}

	/**
	 * 报表导出
	 */
	@Override
	public List<BuyApplypurchaseHeader> selectByStatistic(
			Map<String, Object> map) {
		return mapper.selectByStatistic(map);
	}
	
	/**
	 * 取消采购计划保存
	 * @param purchaseId
	 * @throws Exception
	 */
	public void saveCancelPurchase(String purchaseId) throws Exception{
		BuyApplypurchaseHeader header = selectByPrimaryKey(purchaseId);
		if(header != null){
			if(header.getStatus().equals(Constant.PurchaseStatus.WAITVERIFY.getValue()+"")
					|| header.getStatus().equals(Constant.PurchaseStatus.VERIFYNOPASS.getValue()+"")){
				header.setStatus(Constant.PurchaseStatus.CANCEL.getValue() + "");
				int result = mapper.updateByPrimaryKeySelective(header);
				if(result > 0){
					//修改审批状态
					TradeVerifyHeader verifyHeader = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(purchaseId);
					verifyHeader.setStatus("3");//撤销
					verifyHeader.setUpdateDate(new Date());
					verifyHeader.setUpdateUserId(ShiroUtils.getUserId());
					verifyHeader.setUpdateUserName(ShiroUtils.getUserName());
					tradeVerifyHeaderMapper.update(verifyHeader);
					if(!ObjectUtil.isEmpty(verifyHeader)){
						//删除所有未审批的数据
						tradeVerifyPocessMapper.deleteNotVerify(verifyHeader.getId());
						//获得最大index
						int maxIndex = tradeVerifyPocessMapper.getMaxIndex(verifyHeader.getId());
						//添加审批
						TradeVerifyPocess pocess = new TradeVerifyPocess();
						pocess.setId(ShiroUtils.getUid());
						pocess.setHeaderId(verifyHeader.getId());
						pocess.setUserId(ShiroUtils.getUserId());
						pocess.setUserName(ShiroUtils.getUserName());
						pocess.setStatus("4");//已撤销
						pocess.setRemark("已撤销");
						pocess.setVerifyIndex(maxIndex+1);
						pocess.setStartDate(new Date());
						pocess.setEndDate(new Date());
						tradeVerifyPocessMapper.add(pocess);
					}
				}
			}else{
				throw new Exception("该申请已审核通过，不允许取消！");
			}
		}else{
			throw new Exception("未找到申请信息，请刷新页面重试！");
		}
	}

	/**
	 * 获得得审批的采购计划
	 * @param searchPageUtil
	 * @param params
	 */
	public void loadApprovedData(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		params.put("companyId", ShiroUtils.getCompId());
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		params1.put("siteId", "17111514043383846004");
		//获得所有未审批的id
		List<String> applyIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> applyIdListNew = new ArrayList<>();
		//
		String procode = params.containsKey("procode")?params.get("procode")!=null?params.get("procode").toString():"":"";
		String planCode = params.containsKey("planCode")?params.get("planCode")!=null?params.get("planCode").toString():"":"";
//		String skucode = params.containsKey("skucode")?params.get("skucode")!=null?params.get("skucode").toString():"":"";
//		String skuname = params.containsKey("skuname")?params.get("skuname")!=null?params.get("skuname").toString():"":"";
		String skuoid = params.containsKey("skuoid")?params.get("skuoid")!=null?params.get("skuoid").toString():"":"";
		if(!StringUtils.isEmpty(procode)
			||!StringUtils.isEmpty(planCode)
//			||!StringUtils.isEmpty(skucode)
//			||!StringUtils.isEmpty(skuname)
			||!StringUtils.isEmpty(skuoid)
		){
			List<String> applyIdList2 = mapper.selectApprovedId(params);
			if(!applyIdList2.isEmpty()){
				Map<String, String> applyIdMap = new HashMap<>();
				for(String applyId : applyIdList2){
					applyIdMap.put(applyId, applyId);
				}
				if(!applyIdList.isEmpty()){
					for(String applyId : applyIdList){
						if(applyIdMap.containsKey(applyId)){
							applyIdListNew.add(applyId);
						}
					}
				}
			}
		}else{
			applyIdListNew = applyIdList;
		}
		params.put("approved", true);
		params.put("applyIdList", applyIdListNew);
		searchPageUtil.setObject(params);
		List<BuyApplypurchaseHeader> applyPurchaseList = mapper.selectApprovedByPage(searchPageUtil);
		if(!applyPurchaseList.isEmpty()){
			for(BuyApplypurchaseHeader applyPurchase:applyPurchaseList){
				List<BuyApplypurchaseItem> applyItemList = applypurchaseItemService.selectByHeaderId(applyPurchase.getId());
				if(!applyItemList.isEmpty()){
					for (BuyApplypurchaseItem applyItem:applyItemList) {
						List<BuyApplypurchaseShop> applyShopList = applypurchaseShopService.selectByItemId(applyItem.getId());
						applyItem.setShopList(applyShopList);
					}
				}
				applyPurchase.setItemList(applyItemList);
			}
		}
		searchPageUtil.getPage().setList(applyPurchaseList);
	}

	@Override
	public List<String> selectApprovedId(Map<String, Object> params) {
		return mapper.selectApprovedId(params);
	}

	@Override
	public List<BuyApplypurchaseHeader> selectApprovedByPage(
			SearchPageUtil searchPageUtil) {
		return mapper.selectApprovedByPage(searchPageUtil);
	}

	/**
	 * 一键审批
	 * @param applyId
	 * @return
	 */
	public JSONObject verifyAll(String applyId) {
		JSONObject json = new JSONObject();
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(applyId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						verifySuccess(applyId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
		return json;
	}

}
