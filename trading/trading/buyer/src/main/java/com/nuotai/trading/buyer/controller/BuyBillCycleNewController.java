package com.nuotai.trading.buyer.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.*;
import com.nuotai.trading.buyer.service.BuyBillCycleInterestService;
import com.nuotai.trading.buyer.service.BuyBillCycleService;
import com.nuotai.trading.buyer.service.BuyBillInterestNewService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.buyer.service.BuyBillCycleManagementNewService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 
 * 
 * @author "
 * @date 2017-09-13 11:45:51
 */
@Controller
@RequestMapping("platform/buyer/billCycleNew")
public class BuyBillCycleNewController extends BaseController{
	@Autowired
	private BuyBillCycleManagementNewService buyBillCycleManagementNewService;
	@Autowired
	private BuyBillInterestNewService buyBillInterestNewService;
	@Autowired
	private BuyBillCycleService buyBillCycleService;//账单周期
	@Autowired
	private BuyBillCycleInterestService buyBillCycleInterestService;//账单关联利息

	/**
	 * 审批通过 (注意备注都在审批表中)
	 * @param id
	 */
	@RequestMapping(value = "/verifyNewBillSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(final String id){
		JSONObject json = buyBillCycleManagementNewService.verifyNewBillSuccess(id);
		return json.toString();
	}

	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyNewBillError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyNewBillError(String id){
		JSONObject json = buyBillCycleManagementNewService.verifyNewBillError(id);
		return json.toString();
	}

	/**
	 * 审批查看详情
	 * @return
	 */
	@RequestMapping(value ="verifyNewBillDetail", method = RequestMethod.GET)
	public String verifyNewBillDetail(String id){
		//申请账单周期数据查询（新编号）
		BuyBillCycleManagementNew billCycleManagementNew = buyBillCycleManagementNewService.queryBillCycleByNewId(id);
		model.addAttribute("billCycleManagementNew",billCycleManagementNew);
		//历史账单周期关联利息查询（新编号）
		List<BuyBillInterestNew> billInterestNewList = buyBillInterestNewService.queryNewInterest(id);
		model.addAttribute("billInterestNewList",billInterestNewList);
		//账单结算周期详情
		BuyBillCycleManagement billCycleManagementInfo = buyBillCycleService.selectByPrimaryKey(billCycleManagementNew.getNewBillCycleId());
		model.addAttribute("billCycleManagementInfo",billCycleManagementInfo);
		//账单结算周期关联利息
		List<BuyBillInterest> interestList = buyBillCycleInterestService.getBillInteresInfo(billCycleManagementNew.getNewBillCycleId());
		model.addAttribute("interestList",interestList);

		//查询买家信息
		//BuyCompany buyerBean = buyCompanyService.selectByPrimaryKey(orderBean.getCompanyId());
		//model.addAttribute("buyerBean", buyerBean);
		//卖家信息
		return "platform/buyer/billcycles/billcyclemanagements/verifyNewBillDetail";
	}
}
