package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuySupplierAssessment;
import com.nuotai.trading.dao.BaseDao;

/**
 * 供应商绩效考核表
 * 
 * @author "
 * @date 2018-01-03 10:12:29
 */
public interface BuySupplierAssessmentMapper extends BaseDao<BuySupplierAssessment> {

	int insert(BuySupplierAssessment buySupplierAssessment);

	void updateAssesment(BuySupplierAssessment assesment);
	
}
