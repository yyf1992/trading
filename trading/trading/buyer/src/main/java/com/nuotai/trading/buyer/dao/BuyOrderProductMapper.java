package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.utils.SearchPageUtil;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * The interface Buy order product mapper.
 */
public interface BuyOrderProductMapper {
    /**
     * Delete by primary key int.
     *
     * @param id the id
     * @return the int
     */
    int deleteByPrimaryKey(String id);

    /**
     * Insert int.
     *
     * @param record the record
     * @return the int
     */
    int insert(BuyOrderProduct record);

    /**
     * Insert selective int.
     *
     * @param record the record
     * @return the int
     */
    int insertSelective(BuyOrderProduct record);

    /**
     * Select by primary key buy order product.
     *
     * @param id the id
     * @return the buy order product
     */
    BuyOrderProduct selectByPrimaryKey(String id);

    /**
     * Update by primary key selective int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKeySelective(BuyOrderProduct record);

    /**
     * Update by primary key int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKey(BuyOrderProduct record);

    /**
     * Select by order id list.
     *
     * @param orderId the order id
     * @return the list
     */
    List<BuyOrderProduct> selectByOrderId(String orderId);

    /**
     * 采购商品汇总-已计划采购数量
     *
     * @param map )
     * @return map map
     */
    Map<String,Object> selectAlreadyPlanPurchaseNumBySkuOid(Map<String, Object> map);

    /**
     * 采购商品汇总-已取消订单采购数量
     *
     * @param map )
     * @return map map
     */
    Map<String,Object> selectCancelPurchaseNumBySkuOid(Map<String, Object> map);

    /**
     * 采购商品汇总(已终止-已发货)数量
     *
     * @param map )
     * @return map map
     */
    Map<String,Object> selectStopPurchaseNumBySkuOid(Map<String, Object> map);

    /**
     * Select by order id and barcode buy order product.
     *
     * @param map the map
     * @return the buy order product
     */
    BuyOrderProduct selectByOrderIdAndBarcode(Map<String, Object> map);

    /**
     * 采购商品汇总-已下单未审核数量（锁定数量）
     *
     * @param map the map
     * @return map map
     */
    Map<String,Object> selectLockGoodsnumberBySkuOid(Map<String, Object> map);

    /**
     * Select order un arrival num by barcode string.
     *
     * @param map the map
     * @return the string
     */
    String selectOrderUnArrivalNumByBarcode(Map<String, Object> map);

    /**
     * Add arrival num.
     * 增加到货数量
     *
     * @param orderItemId the order item id
     * @param addNum      the addNum num
     */
    void addArrivalNum(@Param("orderItemId") String orderItemId, @Param("addNum") Integer addNum);

    /**
     * Select order receive products list.
     *
     * @param searchPageUtil the search page util
     * @return the list
     */
//售后模块-查询所有已收货商品
    List<Map<String,Object>> selectOrderReceiveProducts(SearchPageUtil searchPageUtil);

    /**
     * Query list list.
     * 查询列表
     *
     * @param orderItemMap the order item map
     * @return the list
     */
    List<BuyOrderProduct> queryList(Map<String, Object> orderItemMap);
    
    List<BuyOrderProduct> selectByApplyCode(String applyCode);

    /**
     * Gets not arrival export data.
     * 获取未到货的订单明细
     * @param map the map
     * @return the not arrival export data
     */
    List<BuyOrderProduct> getNotArrivalExportData(Map<String, Object> map);

	void updateDeliveredNum(@Param("id") String id,@Param("deliveryNum") int deliveryNum);
}