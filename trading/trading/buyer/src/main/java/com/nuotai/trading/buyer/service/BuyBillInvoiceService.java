package com.nuotai.trading.buyer.service;

import com.nuotai.trading.buyer.dao.BuyBillInvoiceMapper;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationItemMapper;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationMapper;
import com.nuotai.trading.buyer.model.BuyBillInvoice;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyBillReconciliationItem;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliation;
import com.nuotai.trading.seller.model.SellerBillReconciliationItem;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillInvoiceService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillInvoiceService.class);

	@Autowired
	private BuyBillInvoiceMapper buyBillInvoiceMapper;
	@Autowired
	private BuyBillReconciliationMapper buyBillReconciliationMapper;
	@Autowired
	private BuyBillReconciliationItemMapper buyBillReconciliationItemMapper;
	@Autowired
	private BSellerBillReconciliationMapper bSellerBillReconciliationMapper;

	//卖家查询发票列表信息
	public List<Map<String, Object>> getInvoiceList(
			SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> billInvoiceList = buyBillInvoiceMapper.queryInvoiceList(searchPageUtil);
		return billInvoiceList;
	}

	//根据审批状态查询数量
	public int getInvoiceCountByStatus(Map<String, Object> map) {
		return buyBillInvoiceMapper.getInvoiceCountByStatus(map);
	}

	//查询不同状态下数量
	public void queryInvoiceCountByStatus(Map<String, Object> invoiceMap){
		//查询所有状态数量
		invoiceMap.put("billInvoiceStatus", "0");
		int deliveryCount = getInvoiceCountByStatus(invoiceMap);

		//查询待确认
		invoiceMap.put("billInvoiceStatus", "1");
		int approvalBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//查询已确认
		invoiceMap.put("billInvoiceStatus", "2");
		int acceptBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//查询审批已通过周期数
		invoiceMap.put("billInvoiceStatus", "3");
		int stopBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//取消票据 closeBillCycleCount
		invoiceMap.put("billInvoiceStatus", "4");
		int closeBillCycleCount = getInvoiceCountByStatus(invoiceMap);

		invoiceMap.put("noConfirmInvoiceCount", deliveryCount);
		invoiceMap.put("approvalBillCycleCount", approvalBillCycleCount);
		invoiceMap.put("acceptBillCycleCount", acceptBillCycleCount);
		invoiceMap.put("stopBillCycleCount", stopBillCycleCount);
		invoiceMap.put("closeBillCycleCount", closeBillCycleCount);

	}

	public List<BuyBillInvoice> queryInvoiceBuyMap(Map<String,Object> map){
		return buyBillInvoiceMapper.queryInvoiceBuyMap(map);
	}

	//发票核实
	public int updateBillInvoice(Map<String, Object> updateSavemap){

		int result = buyBillInvoiceMapper.updateBillInvoice(updateSavemap);
		if(result > 0){
			buyBillInvoiceMapper.updateSellerInvoice(updateSavemap);

			String invoiceId = updateSavemap.get("id").toString();
			String isInvoiceStatus = updateSavemap.get("acceptInvoiceStatus").toString();
			Map<String,Object> recItemInvoiceStatus = new HashMap<String,Object>();
			recItemInvoiceStatus.put("invoiceId",invoiceId);
			recItemInvoiceStatus.put("isInvoiceStatus",isInvoiceStatus);
			//buyBillReconciliationItemMapper.updateRecItemPrice(recItemInvoiceStatus);
			//核实驳回清除关联表数据，通过更新对账付款表数据
			/*if("3".equals(isInvoiceStatus)){

				//buyBillReconciliationItemMapper.updateRecItemPrice(recItemInvoiceStatus);
				//buyBillInvoiceMapper.deleteInvoiceCommodity(recItemInvoiceStatus);
			}else */if("2".equals(isInvoiceStatus)){
				Map<String,Object> recItemInvoiceStatusPass = new HashMap<String,Object>();
				recItemInvoiceStatusPass.put("invoiceId",invoiceId);
				recItemInvoiceStatusPass.put("isInvoiceStatus",isInvoiceStatus);
				buyBillReconciliationItemMapper.updateRecItemPrice(recItemInvoiceStatusPass);
				//查询所有开票成功的订单详细
				/*List<BuyBillReconciliationItem> reconciliationItemList = buyBillReconciliationItemMapper.queryRecItemListByInvoiceId(recItemInvoiceStatus);*/
				List<BuyBillReconciliationItem> reconciliationItemList = buyBillReconciliationItemMapper.queryRecItemList(recItemInvoiceStatus);
				String reconciliationId = "";
				for(BuyBillReconciliationItem billRecItem : reconciliationItemList){
					if(null != billRecItem.getReconciliationId() && !"".equals(billRecItem.getReconciliationId())){
						reconciliationId = billRecItem.getReconciliationId();
						continue;
					}
				}
				//查询所有需要开票的订单详细
				Map<String,Object> recItemIsNeedVoiceMap = new HashMap<String,Object>();
				recItemIsNeedVoiceMap.put("reconciliationId",reconciliationId);
				recItemIsNeedVoiceMap.put("isNeedInvoice","Y");
				List<BuyBillReconciliationItem> recItemIsNeedVoiceCountList = buyBillReconciliationItemMapper.queryRecItemList(recItemIsNeedVoiceMap);
				Map<String,Object> recItemIsTrueVoiceMap = new HashMap<String,Object>();
				recItemIsTrueVoiceMap.put("reconciliationId",reconciliationId);
				recItemIsTrueVoiceMap.put("isInvoiceStatus",isInvoiceStatus);
				List<BuyBillReconciliationItem> recItemIsTrueVoiceCountList = buyBillReconciliationItemMapper.queryRecItemList(recItemIsTrueVoiceMap);
				if(recItemIsTrueVoiceCountList.size() == recItemIsNeedVoiceCountList.size()){
					//查询获取应付款总金额
					Map<String,Object> reconciliationMap = new HashMap<String,Object>();
					reconciliationMap.put("id",reconciliationId);
					BuyBillReconciliation buyBillReconciliationOld = buyBillReconciliationMapper.selectByPrimaryKey(reconciliationMap);

					//所有需要开票的订单都已开票通过，修改对账付款表 全部开票状态为：是
					BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
					String paymentNo = ShiroUtils.getUid();
					buyBillReconciliation.setId(reconciliationId);
					buyBillReconciliation.setPaymentNo(paymentNo);
					buyBillReconciliation.setResidualPaymentAmount(buyBillReconciliationOld.getTotalAmount());
					buyBillReconciliation.setPaymentStatus("4");//内部待审批
					buyBillReconciliation.setIsFullInvoice("1");
					//buyBillReconciliation.setPaymentStatus();
					buyBillReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
					BSellerBillReconciliation bSellerBillReconciliation = new BSellerBillReconciliation();
					bSellerBillReconciliation.setId(reconciliationId);
					bSellerBillReconciliation.setPaymentNo(paymentNo);
					bSellerBillReconciliation.setPaymentStatus("4");//内部待审批
					bSellerBillReconciliation.setResidualPaymentAmount(buyBillReconciliationOld.getTotalAmount());
					//bSellerBillReconciliation.setIsFullInvoice("1");
					bSellerBillReconciliationMapper.updateByPrimaryKeySelective(bSellerBillReconciliation);
				}
			}
		}
		return result;
	}

	//根据发票编号查询订单详情
	public Map<String,Object> queryRecItemByInvoiceId(Map<String,Object> recItemMap){
		//String invoiceId = recItemMap.get("invoiceId").toString();
		Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
		List<Map<String,Object>> deliveryItemList = new ArrayList<>();//合并返回的集合
		List<BuyBillReconciliationItem> deliveryRecItemList = buyBillReconciliationItemMapper.queryRecItemList(recItemMap);
		int arrivalNumCount = 0;
		BigDecimal priceSum = new BigDecimal(0);
		for(BuyBillReconciliationItem deliveryRecItem : deliveryRecItemList){
			Map<String,Object> itemMap = new HashMap<String,Object>();
			String isUpdateSale = deliveryRecItem.getIsUpdateSale();
			int arrivalNum = deliveryRecItem.getArrivalNum();//数量
			//BigDecimal salePrice = deliveryRecItem.getSalePrice();//单价
			BigDecimal salePrice = new BigDecimal(0);//单价
			if("1".equals(isUpdateSale)){
				salePrice = deliveryRecItem.getUpdateSalePrice();//单价
			}else {
				salePrice = deliveryRecItem.getSalePrice();//单价
			}
			BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
			BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

			arrivalNumCount = arrivalNumCount + arrivalNum;
			priceSum = priceSum.add(salePriceSum);//发票商品总额

			itemMap.put("deliveryRecItem",deliveryRecItem);
					/*itemMap.put("reconciliationId",reconciliationId);
					itemMap.put("buycompanyName",buycompanyName);*/
			itemMap.put("salePrice",salePrice);
			itemMap.put("salePriceSum",salePriceSum);
			deliveryItemList.add(itemMap);
		}
		deliveryItemMap.put("arrivalNumCount",arrivalNumCount);
		deliveryItemMap.put("priceSum",priceSum);
		deliveryItemMap.put("deliveryItemList",deliveryItemList);

		return deliveryItemMap;
	}

	public BuyBillInvoice get(String id){
		return buyBillInvoiceMapper.get(id);
	}
	
	public List<BuyBillInvoice> queryList(Map<String, Object> map){
		return buyBillInvoiceMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillInvoiceMapper.queryCount(map);
	}
	
	public void add(BuyBillInvoice buyBillInvoice){
		buyBillInvoiceMapper.add(buyBillInvoice);
	}
	
	public void update(BuyBillInvoice buyBillInvoice){
		buyBillInvoiceMapper.update(buyBillInvoice);
	}
	
	public void delete(String id){
		buyBillInvoiceMapper.delete(id);
	}
	

}
