package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementOld;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author "
 * @date 2017-09-08 19:12:52
 */
@Component("buyBillCycleManagementOldMapper")
public interface BuyBillCycleManagementOldMapper extends BaseDao<BuyBillCycleManagementOld> {
	//添加新历史数据
    int addNewBillCycleOld(BuyBillCycleManagementOld buyBillCycleManagementOld);
    //查询最近历史账单数据
    BuyBillCycleManagementOld queryLastDateBillCycleOld(String id);
    //根据编号删除历史账单数据
    int deleteNewBillCycleOld(String id);
}
