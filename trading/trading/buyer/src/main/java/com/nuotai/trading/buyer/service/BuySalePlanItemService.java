package com.nuotai.trading.buyer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.buyer.dao.BuySalePlanItemMapper;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuySalePlanItemService {

	@Autowired
	private BuySalePlanItemMapper buySalePlanItemMapper;
	@Autowired
	private BuyApplypurchaseItemService applypurchaseItemService;
	
	public BuySalePlanItem get(String id){
		return buySalePlanItemMapper.get(id);
	}
	
	public List<BuySalePlanItem> queryList(Map<String, Object> map){
		return buySalePlanItemMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buySalePlanItemMapper.queryCount(map);
	}
	
	public void add(BuySalePlanItem buySalePlanItem){
		buySalePlanItemMapper.add(buySalePlanItem);
	}
	
	public void update(BuySalePlanItem buySalePlanItem){
		buySalePlanItemMapper.update(buySalePlanItem);
	}
	
	public void delete(String id){
		buySalePlanItemMapper.delete(id);
	}
	
	public List<BuySalePlanItem> selectByHeaderId(String headerId){
		return buySalePlanItemMapper.selectByHeaderId(headerId);
	}

	public List<BuySalePlanItem> getProductGroupByBarcode(String[] idStr){
		return buySalePlanItemMapper.getProductGroupByBarcode(idStr);
	}
	
	public List<Map<String,Object>> getProductGroupByMap(Map<String, Object> map){
		return buySalePlanItemMapper.getProductGroupByMap(map);
	}
	
	public List<Map<String,Object>> selectSalePlanProductSumByPage(SearchPageUtil searchPageUtil){
		return buySalePlanItemMapper.selectSalePlanProductSumByPage(searchPageUtil);
	}
	
	public List<Map<String,Object>> selectSalePlanShopListByBarcode(Map<String,Object> map){
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> detailsList = buySalePlanItemMapper.selectSalePlanShopListByBarcode(map);
		if(detailsList != null && detailsList.size() > 0){
			for(Map<String, Object> mapResult : detailsList){
				// 用编号和条形码查询已下单商品数量
				Map<String,Object> mapPare = new HashMap<String,Object>();
				mapPare.put("companyId", ShiroUtils.getCompId());
				mapPare.put("planCode", mapResult.get("plan_code"));
				mapPare.put("skuOid", mapResult.get("barcode"));
				mapPare.put("shopId", mapResult.get("shop_id"));
				Map<String,Object> mapAlready = applypurchaseItemService.selectAlreadyPlanNumBySkuOid(mapPare);
				if(mapAlready != null){
					int alreadyPlanNum = mapAlready.get("alreadyPlanNum") == null ? 0 : Integer.parseInt(mapAlready.get("alreadyPlanNum").toString());
					//已取消
					/*Map<String,Object> mapCancel = applypurchaseItemService.selectCancelPlanNumBySkuOid(mapPare);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanNum = alreadyPlanNum - cancelPurchaseNum;
					}*/
					if(alreadyPlanNum < Integer.parseInt(mapResult.get("sales_num").toString())){
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("sales_num").toString()) - alreadyPlanNum);
					}else{
						mapResult.put("overplusNum", 0);
					}
				}else{
					mapResult.put("overplusNum", Integer.parseInt(mapResult.get("sales_num").toString()));
				}
				//上次销售达成率
				mapPare.put("barcode", mapResult.get("barcode"));
				BuySalePlanItem salePlanItem = buySalePlanItemMapper.selectLastSalesCompletionRateListByMap(mapPare);
				if(salePlanItem != null){
					mapResult.put("salesCompletionRate",salePlanItem.getSalesCompletionRate());
				}
				if(Integer.parseInt(mapResult.get("overplusNum").toString()) != 0){//剩余采购数量为0的不显示
					resultList.add(mapResult);
				}
			}
		}
		return resultList;
	}
	
	public BuySalePlanItem selectLastSalesCompletionRateListByMap(Map<String, Object> map){
		return buySalePlanItemMapper.selectLastSalesCompletionRateListByMap(map);
	}
}
