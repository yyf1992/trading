package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.dao.BaseDao;

/**
 * 
 * 
 * @author zyn
 * @date 2017-08-17 17:09:13
 */
public interface SysShopProductMapper extends BaseDao<SysShopProduct> {

	int saveSetPrice(SysShopProduct sysShopProduct);

	List<SysShopProduct> selectByMap(Map<String,Object> param);

	SysShopProduct selectByPrimaryKey(String id);

	int saveEditPrice(SysShopProduct sysShopProduct);
	
}
