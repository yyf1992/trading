package com.nuotai.trading.buyer.dao.seller;/**
 * @author liuhui
 * @date 2017-11-08 11:41
 * @description
 **/

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecordItem;
import com.nuotai.trading.dao.BaseDao;

/**
 *
 * @author liuhui
 * @create 2017-11-08 11:41
 **/
public interface BSellerDeliveryRecordItemMapper extends BaseDao<SellerDeliveryRecordItem> {

    /**
     * Update by primary key selective.
     * 按条件更新
     * @param item the upd item
     */
    void updateByPrimaryKeySelective(SellerDeliveryRecordItem item);

	int getItemCount(@Param("recordId")String recordId);

	List<Map<String,Object>> queryNotDeliveryList(Map<String, Object> params);

    /**
     * Gets iterm by delivery id.
     * 根据发货单ID获取明细
     *
     * @param map the delivery id
     * @return the iterm by delivery id
     */
    List<SellerDeliveryRecordItem> getItemByDeliveryId(Map<String,Object> map);
    
    List<Map<String,Object>> getItemByOrderId(String orderId);
}
