package com.nuotai.trading.buyer.service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyManualOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderProductMapper;
import com.nuotai.trading.buyer.model.BuyManualOrder;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.dao.BuyWarehouseLogMapper;
import com.nuotai.trading.dao.BuyWarehouseProdcutsMapper;
import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuyManualOrderService {
	@Autowired
	private BuyManualOrderMapper manualOrderMapper;
	@Autowired
	private BuyOrderMapper orderMapper;
	@Autowired
	private BuyOrderProductMapper orderProductMapper;
	@Autowired
	private BuyWarehouseLogMapper warehouseLogMapper;
	@Autowired
	private BuyWarehouseProdcutsMapper warehouseProdcutsMapper;

	public int deleteByPrimaryKey(String id) {
		return manualOrderMapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyManualOrder record) {
		return manualOrderMapper.insert(record);
	}

	public int insertSelective(BuyManualOrder record) {
		return manualOrderMapper.insertSelective(record);
	}

	public BuyManualOrder selectByPrimaryKey(String id) {
		return manualOrderMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuyManualOrder record) {
		return manualOrderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuyManualOrder record) {
		return manualOrderMapper.updateByPrimaryKey(record);
	}
	
	public BuyManualOrder selectByOrderId(String orderId) {
		return manualOrderMapper.selectByOrderId(orderId);
	}
	
	/**
	 * 手工下单审核通过
	 * @param id
	 * @return
	 */
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		BuyOrder order = new BuyOrder();
		order.setId(id);
		order.setIsCheck(1);
		order.setStatus(Constant.OrderStatus.FINISHED.getValue());
		int result = orderMapper.updateByPrimaryKeySelective(order);
		if(result>0){
			// 添加库存日志
			order = orderMapper.selectByPrimaryKey(id);
			List<BuyOrderProduct> orderProducts = orderProductMapper.selectByOrderId(order.getId());
			if(orderProducts != null && orderProducts.size() > 0 ){
				for(BuyOrderProduct orderProduct : orderProducts){
					BuyWarehouseLog wLog = new BuyWarehouseLog();
					wLog.setId(ShiroUtils.getUid());
					wLog.setBatchNo(ShiroUtils.RandomBatchNum());
					wLog.setOrderId(order.getId());
					wLog.setWarehouseId(orderProduct.getWareHouseId());
					wLog.setShopId("");
					wLog.setShopCode("");
					wLog.setShopName("");
					wLog.setChangeType(1);//入库
					wLog.setStockType(0);//采购入库
					wLog.setProductCode(orderProduct.getProCode());
					wLog.setProductName(orderProduct.getProName());
					wLog.setSkuCode(orderProduct.getSkuCode());
					wLog.setSkuName(orderProduct.getSkuName());
					wLog.setBarcode(orderProduct.getSkuOid());
					wLog.setPrice(orderProduct.getPrice());
					wLog.setNumber(orderProduct.getGoodsNumber());
					wLog.setFactNumber(orderProduct.getGoodsNumber());
					wLog.setStatus(0);
					wLog.setIsDel(Constant.IsDel.NODEL.getValue());
					wLog.setCreateId(order.getCreateId());
					wLog.setCreateName(order.getCreateName());
					wLog.setCreateDate(new Date());
					//warehouseLogMapper.add(wLog);
					
					BuyWarehouseProdcuts wProducts = new BuyWarehouseProdcuts();
					wProducts.setId(ShiroUtils.getUid());
					wProducts.setWarehouseId(orderProduct.getWareHouseId());
					wProducts.setProductCode(orderProduct.getProCode());
					wProducts.setShopId("");
					wProducts.setShopCode("");
					wProducts.setShopName("");
					wProducts.setProductName(orderProduct.getProName());
					wProducts.setSkuCode(orderProduct.getSkuCode());
					wProducts.setSkuName(orderProduct.getSkuName());
					wProducts.setBarcode(orderProduct.getSkuOid());
					wProducts.setTotalCost(orderProduct.getPriceSum());//成本总额
					//wProducts.setUsableStock();
					wProducts.setIsDel(Constant.IsDel.NODEL.getValue());
					wProducts.setCreateId(order.getCreateId());
					wProducts.setCreateName(order.getCreateName());
					wProducts.setCreateDate(new Date());
					//warehouseProdcutsMapper.add(wProducts);
				}
			}
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

}
