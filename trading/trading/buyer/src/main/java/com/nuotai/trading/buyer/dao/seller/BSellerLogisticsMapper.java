package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerLogistics;
import com.nuotai.trading.dao.BaseDao;

public interface BSellerLogisticsMapper extends BaseDao<BSellerLogistics>{
	//根据发货单获取物流信息
	public BSellerLogistics getSellerLogisticsByDeliveryId(String deliveryid);
	
	public void add(BSellerLogistics bSellerLogistics);
}
