$('.lf_system>div>h4').click(function(){
  $(this).next().slideToggle(300,'linear');
  $(this).find('span').toggleClass('fa-caret-up');
});
layui.use(['element','form','upload'], function() {
  var layer=layui.layer;
  //保存个人信息
 $('.data_save').click(function(){
   layer.open({
     type: 1,
     title:'系统提示',
     skin: 'layui-layer-rim', //加上边框
     area: ['315px', '135px'], //宽高
     content:$('.save_success')
   });
 });
  //修改密码
  /*$('.modify_sure>button').click(function(){
    layer.open({
      type: 1,
      title:'系统提示',
      skin: 'layui-layer-rim', //加上边框
      area: ['315px', '135px'], //宽高
      content:$('.modify_success')
    });
  });*/
  //删除系统消息
  $('.recovery>span').click(function(){
    layer.confirm('确定删除这条消息吗?',{
      skin:'layui-layer-rim',
      title:'系统提示',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
      $(this).parent().parent().parent().addClass('hide');
    }.bind(this));
  });
  //显示消息
  $('.system_sel>div').hover(function(){
    $(this).children('div').toggle();
  });
  //清空信息
  $('#msg_empty').click(function(){
    layer.confirm('确定要清空消息吗?',{
      skin:'layui-layer-rim',
      title:'系统提示',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
    });
  });
  $('#msg_shield').click(function(){
    layer.confirm('是否屏蔽系统通知信息?',{
      skin:'layui-layer-rim',
      title:'系统提示',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
    });
  });
  //删除验证消息
  $('.verify_list>div>b').click(function(){
    layer.confirm('确定删除这条消息吗?',{
      skin:'layui-layer-rim',
      title:'提醒',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
      $(this).parent().addClass('cut');
    }.bind(this));
  });
  //修改角色管理弹框
  $('.role_list .commodity_edit').click(function(){
    layer.open({
      type: 1,
      title:'角色管理',
      skin: 'layui-layer-rim role',
      area: ['460px', 'auto'],
      btn:['保存','关闭'],
      content:$('.role_management')
    });
  });
  //删除角色
  $('.role_list .commodity_del').click(function(){
    layer.confirm('<p style="font-size: 12px;">删除此角色后，具有此角色操作权限的子账户将恢复为系统初始的默认操作权限。确定要删除此角色吗？</p>',{
      skin:'layui-layer-rim',
      title:'提醒',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
    });
  });
  //新建角色弹框
  $('.role_add').click(function(){
    layer.open({
      type: 1,
      title:'角色管理',
      skin: 'layui-layer-rim role',
      area: ['460px', 'auto'],
      btn:['保存','关闭'],
      content:$('.role_new')
    });
  });
  //新建子账号弹框
  $('.account_add').click(function(){
    layer.open({
      type: 1,
      title:'新建子账号',
      skin: 'layui-layer-rim role',
      area: ['410px', 'auto'],
      btn:['保存','关闭'],
      content:$('.account_new')
    });
  });
  //账户修改弹框
  $('.account-list .commodity_edit').click(function(){
    layer.open({
      type: 1,
      title:'新建子账号',
      skin: 'layui-layer-rim role',
      area: ['410px', 'auto'],
      btn:['保存','关闭'],
      content:$('.account_edit')
    });
  });
  //账户删除弹框
  $('.account-list .commodity_del').click(function(){
    layer.confirm('<p style="font-size: 12px;">删除此账户后，该账户将不能再登录此企业的系统进行操作，确定要删除此账户吗？</p>',{
      skin:'layui-layer-rim',
      title:'提醒',
      area: ['320px','auto']
    },function(index){
      layer.close(index);
    });
  });
});
//消息列表页面
//显示垃圾桶
$('.message_list>div>div').hover(function(){
  $(this).children('.recovery').toggle();
  $(this).toggleClass('hover');
});
//消息验证页面
$('.verify_list>div').hover(function(){
  $(this).toggleClass('move');
  $(this).children('b').toggle();
});
//收货地址页面
$('.address_list>div').hover(function(){
  $(this).toggleClass('enter');
  $(this).children('b').toggle();
});
