
//重置查询条件
function reset_search(){
	$("#searchForm").find("ul").find("li:eq(0)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(1)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(2)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(3)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(4)").find("input").val("");
}
$(function(){
	selAllClient();
});
//功能点1:全选事件
function selAllClient(){
	var inputs=$('.customerList tbody input');
	$('.customerList thead input').click(function(){
		inputs.prop('checked',$(this).prop('checked'));
	});
	inputs.click(function(){
		var r=$('.customerList tbody input:not(:checked)');
		if(r.length==0){
			$('.customerList thead input').prop('checked',true);
		}else{
			$('.customerList thead input').prop('checked',false);
		}
	});
}
	
//删除客户
function deleteSellerClient(id){
	
	 layer.confirm('确定删除该客户吗？', {
	    icon:3,
	    title:'提醒',
	    closeBtn:2,
	    btn: ['确定','取消']
	  }, function(){
		  $.ajax({
				type : "post",
				url : "platform/sellers/client/deleteSellerClient",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1},function(){
							leftMenuClick(this,"platform/sellers/client/clientList","sellers");
						});
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	  });
}

//批量删除客户
function beatchDeleteClient(){
	var checked = $("input:checkbox[name='checkones']:checked");
	if (checked.length < 1) {
		layer.msg("请至少选择一条数据！",{icon:1});
		return;
	}
	var ids = "";
	checked.each(function() {
		ids += $(this).val() + ",";
	});
	ids = ids.substring(0, ids.length - 1);
	layer.confirm('确定删除客户吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/sellers/client/deleteBeatchSellerClient",
				data : {
					"ids" : ids
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1},function(){
							leftMenuClick(this,"platform/sellers/client/clientList","sellers");
						});
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}
	