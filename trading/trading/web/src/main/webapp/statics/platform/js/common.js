//加载审批流程
function loadVerify(){
	$('.opinion_view').each(function(){
		var opinionObj = $(this).find(".opinion");
		if(opinionObj.length == 0){
			var dataId = $(this).find(".orange").attr("data");
			pinJieVerifyDiv(dataId,$(this));
		}
	});
	loadTitle();
	getButtonList();
}
function loadTitle(){
	$("tbody tr").each(function(){
		$(this).find("td").each(function(){
			if($(this).children().length==0){
				$(this).attr("title",$(this).html());
			}
		});
	});
	$("table").colResizable();
}
function pinJieVerifyDiv(dataId,opinionViewObject){
	$.ajax({
		url : basePath+"platform/tradeVerify/verifyDetailCommon",
		data:{relatedId:dataId},
		success:function(data){
			var opinionObj = $("<div class='opinion'></div>");
			if(isJSONObject(data)&&data.code==500){
				opinionObj.append("<h3 class='wrap'>该用户设置为不需要内部审批</h3>");
			}else{
				opinionObj.append(data);
			}
			if(opinionViewObject.find(".opinion").length==0){
				opinionViewObject.append(opinionObj);
				opinionViewObject.parents("td").css("overflow","inherit");
				$(opinionViewObject).hover(
					function(){
						$(this).children('.opinion').toggle(10);
					}
				);
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function leftMenuClick(obj,url,type,id){
	var li = $(obj).parent();
	if($(obj).parent().is("li")){
		li = $(obj).parent();
	}else{
		if(id != ''){
			li = $("#"+id);
		}
	}
	if ($(li).hasClass('childLi')||$(li).hasClass('grandsonLi')) {
		$('.menu-active').css('color','#ABB1B7');
		$('.menu-active').removeClass('menu-active');
		$(li).children('a').addClass('menu-active');
		$(li).children('a').css('color','#fff');
	}
	
	 var contentObj = $(".content");
     var formObj = contentObj.find("form");
	 var user = {
		 form:formObj.serialize(),
		 menuId:id
     };
	$.ajax({
		type : "post",
		url : basePath+"platform/common/saveButtonSession",
		data:user,
		async:false,
		success:function(data){
			$.ajax({
				url : basePath+url,
				async:false,
				success:function(data2){
					layer.closeAll('loading');
					var str = data2.toString();
					if(type == 'system'){
						$(".content_system").empty();
						$(".content_system").html(str);
					}else{
						$(".content").empty();
						$(".content").html(str);
					}
				},
				error:function(){
					layer.closeAll('loading');
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
			layuiData();
			loadVerify();
			scroll(0,0);// 显示页面顶部
		},
		error:function(){
			layer.closeAll('loading');
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function layuiData(){
	layui.use(['form','laydate'], function(){
		var form = layui.form;
		 var form = layui.form
		  ,layer = layui.layer
		  ,laydate = layui.laydate;
		 $("input[lay-verify='date']").each(function(){
			 laydate.render({
				 elem: this
			 });
		 });
		  form.render();
	});
	getButtonList();
}
function getButtonList(){
	var buttonList = {};
	$.ajax({
		url : basePath+"platform/common/getButtonList",
		async:false,
		success:function(data){
			var result = eval('(' + data + ')');
			buttonList = isJSONObject(result)?result:eval('(' + result + ')');
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
	if(buttonList.length > 0){
		$("[button]").each(function(){
			var buttonName = $(this).attr("button");
			var isShow = false;
			$.each(buttonList,function(i,obj){
				var name = obj.name;
				if(buttonName==name){
					isShow = true;
					return false;
				}
			});
			if(!isShow){
				$(this).remove();
			}
		});
	}else{
		$("[button]").remove();
	}
}
function titleMenuClick(url,menuId){
	$.ajax({
		url : basePath+"platform/common/titleMenuClick",
		data:{
			"menuId":menuId
		},
		async:false,
		success:function(data){
			window.location.href = basePath+url;
			return;
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
$('.lf_buyer>div>h4').click(function(){
	  $(this).next().slideToggle(300,'linear');
	  $(this).find('span').toggleClass('fa-caret-up');
	});
	$('.lf_basic>div>h4').click(function(){
	  $(this).next().slideToggle(300,'linear');
	  $(this).find('span').toggleClass('fa-caret-up');
	});
	//功能点16:切换右侧好友窗口
	// $('.switch>span').click(function(){
	// 	// 
	//   // $('.switch>span.open').toggle();
	//   // $('.switch>span.close').toggle();
	//   // $('#side_pro').toggleClass('show');
	// });
	//表格增加和删除一行函数
	function addRow(str,tbl){
		var id = (new Date()).valueOf();
		var addTr=document.createElement('tr');
		addTr.setAttribute("id",id);
		addTr.innerHTML=str;
		tbl.after(addTr);
	}
	function delRow(tr){
	  $(tr).parent().parent().remove();
	}
	//功能点1:全选事件
	function selAll(){
	  var inputs=$('.order_list>p>span.chk_click input');
	  function checkedSpan(){
	    inputs.each(function(){
	      if($(this).prop('checked')==true){
	        $(this).parent().addClass('checked')
	      }else{
	        $(this).parent().removeClass('checked')
	      }
	    })
	  }
	  $('.batch_handle input').click(function(){
	    inputs.prop('checked',$(this).prop('checked'));
	    var inputs_all=$('.batch_handle input');
	    if($(this).prop('checked')==true){
	      inputs_all.each(function(){
	        $(this).parent().addClass('checked')
	      })
	    }else{
	      inputs_all.each(function(){
	        $(this).parent().removeClass('checked')
	      })
	    }
	    checkedSpan();
	  });
	  inputs.click(function(){
	    if($(this).prop('checked')==true){
	      $(this).parent().addClass('checked')
	    }else{
	      $(this).parent().removeClass('checked')
	    }
	    var r=$('.order_list>p>span.chk_click input:not(:checked)');
	    if(r.length==0){
	      $('.batch_handle input').prop('checked',true).parent().addClass('checked');
	    }else{
	      $('.batch_handle input').prop('checked',false).parent().removeClass('checked');
	    }
	  });
	}
	selAll();
	//功能点7:打印收货回单
	$(".do_print_btn").click(function(){
	  $("#print_yc").jqprint();
	});
	//商品或供应商或票据全选
	function list_sellAll(){
	  var inputs=$('.normal_list>table span.comm_sel input');
	  function checkedSpan(){
	    inputs.each(function(){
	      if($(this).prop('checked')==true){
	        $(this).parent().addClass('chk')
	      }else{
	        $(this).parent().removeClass('chk')
	      }
	    })
	  }
	  $('.normal_list>table thead input').click(function(){
	    inputs.prop('checked',$(this).prop('checked'));
	    checkedSpan();
	  });
	  inputs.click(function(){
	    checkedSpan();
	    var r=$('.normal_list>table tbody input:not(:checked)');
	    if(r.length==0){
	      $('.normal_list>table thead input').prop('checked',true).parent().addClass('chk');
	    }else{
	      $('.normal_list>table thead input').prop('checked',false).parent().removeClass('chk');
	    }
	  });
	}
	list_sellAll();
	function listO_sellAll(){
	  var inputs=$('.normal_listO>table span.comm_sel input');
	  function checkedSpan(){
	    inputs.each(function(){
	      if($(this).prop('checked')==true){
	        $(this).parent().addClass('chk')
	      }else{
	        $(this).parent().removeClass('chk')
	      }
	    })
	  }
	  $('.normal_listO>table thead input').click(function(){
	    inputs.prop('checked',$(this).prop('checked'));
	    checkedSpan();
	  });
	  inputs.click(function(){
	    checkedSpan();
	    var r=$('.normal_listO>table tbody input:not(:checked)');
	    if(r.length==0){
	      $('.normal_listO>table thead input').prop('checked',true).parent().addClass('chk');
	    }else{
	      $('.normal_listO>table thead input').prop('checked',false).parent().removeClass('chk');
	    }
	  });
	}
	listO_sellAll();
	layui.use(['layer','form','laydate'], function() {
	  var layer = layui.layer, form = layui.form, laydate = layui.laydate;
	  //添加好友
	  $('.last>b').click(function(){
	    layer.open({
	      type:1,
	      title:'添加好友',
	      area:['365px','auto'],
	      skin:'pop',
	      closeBtn:1,
	      shade:0,
	      content:$('.friends_add'),
	      btn:['确认申请','取消'],
	      yes:function(index){
              $.ajax({
                  type : "POST",
                  url :"interwork/applyFriend/saveInviteFriend",
                  data: {
                      "inviteePhone":$("#inviteePhone").val(),
                      "note": $("#note").val()
                  },
                  async:false,
                  success:function(data){
                      var result = eval('(' + data + ')');
                      var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                      if(resultObj.success){
                          layer.msg("操作成功！",{icon:1});
                          layer.close(index);
                      }else{
                          layer.msg(resultObj.msg,{icon:2});
                      }
                  },
                  error:function(){
                      layer.msg("保存失败，请稍后重试！",{icon:2});
                  }
              });
	      }
	    });
	  });
	  //查看收货回单
	  $('.check_receipt').click(function(){
	    layer.open({
	      type: 1,
	      title: '收货回单',
	      area: ['680px', 'auto'],
	      skin: 'pop ',
	      closeBtn:2,
	      content: $('.receipt_content')
	    });
	  });
	  //删除收货回单
	  $('.del_receipt>button').click(function(){
	    layer.confirm('您确定要将所选中的回单图片删除吗？', {
	      icon:3,
	      skin:'pop',
	      title:'提示',
	      closeBtn:2,
	      btn: ['确定','取消'] //按钮
	    }, function(){
	      var str=$('.big_img>img').attr('src');
	      var li=$('#icon_list li img[src="'+str+'"]').parent();
	      li.remove();
	      layer.msg('删除成功',{time:800});
	    });
	  });
	});
	//功能点8:回单证明大图
	var preview={
	  LIWIDTH:108,//保存每个li的宽
	  $ul:null,//保存小图片列表的ul
	  moved:0,//保存左移过的li
	  init:function(){//初始化功能
	    this.$ul=$(".view>ul");//查找ul
	    $(".view>a").click(function(e){//为两个按钮绑定单击事件

	      e.preventDefault();
	      if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
	        if($(e.target).is(".forward")){//如果是向前按钮
	          this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
	          this.moved++;//移动个数加1
	        }
	        else{//如果是向后按钮
	          this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
	          this.moved--;//移动个数减1
	        }
	        this.checkA();//每次移动完后，调用该方法
	      }
	    }.bind(this));
	    //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
	    this.$ul.on("mouseover","li>img",function(){
	      var src=$(this).attr("src");//获得当前img的src
	      //var i=src.lastIndexOf(".");//找到.的位置
	      //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
	      $(".big_img>img").attr("src",src);//设置中图片的src
	    });
	  },
	  checkA:function(){//检查a的状态
	    if(this.moved==0){//如果没有移动
	      $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
	    }
	    /*else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
	      $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
	    }*/
	    else{//否则，都启用
	      $("[class^=backward]").attr("class","backward");
	      $("[class^=forward]").attr("class","forward");
	    }
	  }
	};
	preview.init();
	//功能点2:查看审批流程
	$('.opinion_view').hover(
	  function(){
	    $(this).children('.opinion').toggle();
	  }
	);
	//table_pure 供应商管理全选
	function pure_sellAll(){
	  var inputs=$(".table_pure input[type='checkbox']");
	  $('.table_pure thead input').click(function(){
	    inputs.prop('checked',$(this).prop('checked'));
	  });
	  $('.table_pure tbody input').click(function(){
	    var r=$('.table_pure tbody input:not(:checked)');
	    if(r.length==0){
	      $('.table_pure thead input').prop('checked',true);
	    }else{
	      $('.table_pure thead input').prop('checked',false);
	    }
	  })
	}
	pure_sellAll();
	//搜索更多
	$('.search_more').click(function(){
	  $(this).toggleClass('clicked');
	  $(this).parent().nextAll('ul').toggle();
	})
	//弹出搜索框
	$('.search_more').click(function(){
	  layer.open({
	    type:1,
	    title:0,
	    area:['auto', 'auto'],
	    skin:'screenMore',
	    closeBtn:0,
	    content:$('.popSearch'),
	    btn:['搜索','关闭'],
	    yes:function(index){
      		$.ajax({
				url:"platform/product/selectMore",
				async:false,
				type:"post",
				data:$("#moreProductForm").serialize(),
				success:function(data){	
					layer.close(index);
					var str = data.toString();
					$("#mainDiv").html(str);
					},
				error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    }
	  });
	});
	
	function metaAll(obj){
		var spanObj = $(obj).parent();
		var tbodyObj = $(obj).parents("table").find("tbody");
		if($(spanObj).hasClass("checked")){
			//选中时点击，设置成未选中
			$(spanObj).removeClass("checked");
			$(obj).prop("checked",false);
			$(tbodyObj).find('input:checkbox').prop("checked",false);
			$(tbodyObj).find('span.barcodeAdd').removeClass("checked");
		}else{
			$(spanObj).addClass("checked");
			$(obj).prop("checked",true);
			$(tbodyObj).find('input:checkbox').prop("checked",true);
			$(tbodyObj).find('span.barcodeAdd').addClass("checked");
		}
	}
