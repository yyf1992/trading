//合同管理页面
//上传合同原件
layui.use(['upload','form','element'], function(){
    var layer = layui.layer;
    //合同管理页面的审批
    $('.contract_list td:last-child div.approval_contract').click(function(){
        layer.open({
            type: 1,
            title: '合同审批管理',
            area: ['380px', 'auto'],
            skin: 'pop',
            closeBtn:2,
            btn:['确定','关闭'],
            content: $('.contract_approval')
        })
    });
    //上传图片
    //layui.upload({
    //  url: '' //上传接口
    //  ,success: function(res){ //上传成功后的回调
    //    console.log(res);
    //  }
    //});
    //买家提交草稿箱
    $('.contract_list .submit').click(function(){
        layer.confirm('提交此合同后，将不能再修改合同内容，确定要提交吗？',{
            icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-待对方审批.html';
        })
    });
    //卖家提交草稿箱
    $('.contract_list .submit_s').click(function(){
        layer.confirm('提交此合同后，将不能再修改合同内容，确定要提交吗？',{
            icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-卖家-待对方审批.html';
        })
    });
    //删除合同原件
    $('.contractUpload').on('mouseenter','a',function(){
        $(this).children('p').addClass('moveIn');
    }).on('mouseleave','a',function(){
        $(this).children('p').removeClass('moveIn');
    });
    $('.contractUpload').on('click','a span',function(e){
        e.preventDefault();
        layer.confirm('确定要删除此合同原件吗？',{
            icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            $(this).parent().parent().remove();
        }.bind(this));
    });
    //买家确定提交合同?
    $('#contract_s_b').click(function(){
        layer.confirm('提交此合同后，将不能再修改合同内容，确定要提交吗？',{ icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-待对方审批.html';
        });
    });
    //卖家确定提交合同?
    $('#contract_s_s').click(function(){
        layer.confirm('提交此合同后，将不能再修改合同内容，确定要提交吗？',{ icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-卖家-待对方审批.html';
        });
    });
    //买家确定保存合同草稿?
    $('#contract_d_b').click(function(){
        layer.confirm('确定要将此合同保存成草稿吗？',{
            icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-草稿箱.html';
        });
    });
    //卖家确定保存合同草稿?
    $('#contract_d_s').click(function(){
        layer.confirm('确定要将此合同保存成草稿吗？',{
            icon:3,
            skin:'pop',
            closeBtn:2,
            title:'提醒'
        },function(index){
            layer.close(index);
            location.href='我的合同-卖家-草稿箱.html';
        });
    });
    // $("#oriContracts").live("change",function () {
    //     uploadOriContract($(this));
    // });
});
function loadContractList(){
    var formObj = $(".content").find("form");
    var url = "platform/"+role+"/contract/contractList";
    $.ajax({
        url : url,
        data:formObj.serialize(),
        async:false,
        success:function(data){
            $(".content").empty();
            var str = data.toString();
            $(".content").html(str);
            layuiData();
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//合同原件弹出框
function contractPreview(obj) {
    var contractId = $(obj).parent().parent().find("td")[0].innerText;
    loadContractOriginal(contractId);
    layer.open({
        type: 1,
        title: '预览合同原件',
        area: ['728px', 'auto'],
        skin: 'pop',
        closeBtn:2,
        content: $('#layer-photos-demo')
    });
}
//加载合同原件
function loadContractOriginal(contractId) {
    $("#layer-photos-demo").empty();
    $.ajax({
        url : basePath+"platform/"+role+"/contract/loadContractOriginal",
        data: {
            "relatedId": contractId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                var url = o.url;
                var a = "<a title='点击查看大图'>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#layer-photos-demo\");'>"+
                        "</a>";
                $("#layer-photos-demo").append(a);
            });
        }
    });
}
//合同原件大图
function openPhoto(div) {
    layer.photos({
        photos: div
        ,anim: 5
    });
}
//删除合同
$('.original').on('click','b',function(){
    $(this).parent().remove();
});

//上传合同原件弹出框
function openUpload(obj) {
    var contractId = $(obj).parent().parent().find("td")[0].innerText;
    var contractNo = $(obj).parent().parent().find("td")[1].innerText;
    layer.open({
        type: 1,
        title: '编号为：'+contractNo+'的合同照片上传',
        area: ['728px', 'auto'],
        skin: 'pop',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#uploadDiv'),
        yes : function(index){
            var validPeriodStart = $("#validPeriodStart").val();
            var validPeriodEnd = $("#validPeriodEnd").val();
            var oriAttachments = new Array();
            var attachments = $("input[name='fileUrl']");
            //验证日期和附件必输
            if(validPeriodStart==null||validPeriodStart==""||validPeriodEnd==null||validPeriodEnd==""){
                layer.msg("日期不能为空！",{icon:2});
                return;
            }
            if(attachments==undefined||attachments.length<=0){
                layer.msg("请上传合同原件！",{icon:2});
                return;
            }
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    oriAttachments.push(attachments[i].value);
                }
            }
            $.ajax({
                type:"POST",
                url : "platform/seller/contract/agreementReached",
                // async:false,
                data: {
                    "contractId": contractId,
                    "validPeriodStart": validPeriodStart,
                    "validPeriodEnd": validPeriodEnd,
                    "oriAttachments": JSON.stringify(oriAttachments)
                },
                success:function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("提交成功！",{icon:1});
                        layer.close(index);
                        loadContractList();
                    }
                },
                error:function(){
                    layer.msg("操作失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}
//上传原件
function uploadOriContract(obj) {
    var formData = new FormData();
    var id = $(obj).attr("id");
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isChrome = userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器
    if(isChrome){
        var files = document.getElementById(id).files;
        //追加文件数据
        for(var i=0;i<files.length;i++){
            formData.append("file[]", files[i]);
        }
    }else {
        var file = document.getElementById(id).files[0];
        formData.append("file[]", file);
    }
    // var files = document.getElementById(id).files;
    // //追加文件数据
    // for(var i=0;i<files.length;i++){
    //     formData.append("file[]", files[i]); //++++++++++
    // }
    var url = "platform/seller/contract/uploadOriContract";

    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                            "<b></b>"+
                            "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#oriContractDiv\");'>"+
                            "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                            "</span>";
                    $("#oriContractDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#oriContracts").replaceWith('<input type="file" multiple name="oriContracts" id="oriContracts" onchange="uploadOriContract(this);" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}
