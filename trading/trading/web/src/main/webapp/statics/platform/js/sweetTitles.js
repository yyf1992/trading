//标题提示效果处
var sweetTitles = {
    x: 10,
    y: 20,
    tipElements: "a",
    init: function () {
        $(this.tipElements).unbind("mouseover mouseout mousemove"); //移除之前绑定的时间防止冲突
        $('#tooltip').remove(); //pjax后防止提示未消失
        $(this.tipElements).mouseover(function (e) {
            this.myTitle = this.title;
            this.myHref = this.href;
            this.myHref = (this.myHref.length > 200 ? this.myHref.toString().substring(0, 200) + "..." : this.myHref);
            this.title = "";
            var tooltip = "";
            if (this.myTitle == "") {
                tooltip = "";
            }
            else {
                tooltip = "<div id='tooltip'><p>" + this.myTitle + "</p></div>";
                $('body').append(tooltip);
                $('#tooltip')
                    .css({
                        "opacity": "0.9",
                        "top": (e.pageY + 20) + "px",
                        "left": (e.pageX + 10) + "px"
                    }).show('fast');
            }

        }).mouseout(function () {
            if (this.myTitle != "") {
                this.title = this.myTitle;
                $('#tooltip').remove();
            }
        }).mousemove(function (e) {
            if (this.myTitle != "") {
                $('#tooltip')
                    .css({
                        "top": (e.pageY + 20) + "px",
                        "left": (e.pageX + 10) + "px"
                    });
            }
        });
    }
};