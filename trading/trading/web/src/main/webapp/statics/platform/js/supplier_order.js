layui.use(['layer','form','laydate'], function(){
  var layer = layui.layer,form=layui.form,laydate=layui.laydate;
  //手工录入订单审核
  $('.approvalOrder').click(function(){
    layer.open({
      type:1,
      title:'订单审批',
      area:['380px','auto'],
      skin:'pop',
      closeBtn:2,
      content:$('.manual_order'),
      btn:['确定','取消']
    });
  });
  //互通取消订单
  $('.cancel_order').click(function(){
    layer.confirm('您确定要取消订单吗?',{
      icon:3,
      skin:'pop',
      title:'提示',
      closeBtn:2,
    },function(index){
      layer.close(index);
      layer.msg('订单取消成功', {time:500});
    })
  });
  //手工录入取消订单
  $('.cancelOrder').click(function(){
    layer.confirm('您确定要取消订单吗?',{
      icon:3,
      skin:'pop',
      title:'提示',
      closeBtn:2,
    },function(index){
      layer.close(index);
      layer.msg('订单取消成功', {time:500});
    })
  });
  //互通订单审核
  $('.examine').click(function(){
    layer.open({
      type:1,
      title:'订单审批',
      area:['700px','auto'],
      skin:'pop',
      closeBtn:2,
      content:$('.order_approval'),
      btn:['确定','取消'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  //批量取消订单
  $('#batch_c').click(function(){
    layer.confirm('您确定要批量取消订单吗?',{
      icon:3,
      skin:'pop',
      title:'提示',
      closeBtn:2,
    },function(index){
      layer.close(index);
      layer.msg('订单取成功', {time:500});
    })
  });
  //收货订单管理页面
  $('.sign_agree').click(function(){
    layer.confirm('您确定同意签收该商品吗?',{
      icon:3,
      skin:'pop',
      title:'签收确认',
      closeBtn:2,
    },function(index){
      layer.close(index);
      location.href='买家-收货订单管理-已签收.html';
    })
  });
  //批量同意签收
  $('#sign_b').click(function(){
    layer.confirm('您确定同意批量签收商品吗?',{
      icon:3,
      skin:'pop',
      title:'批量签收确认',
      closeBtn:2,
    },function(index){
      layer.close(index);
      location.href='买家-收货订单管理-已签收.html';
    })
  });
  //驳回签收
  $('.sign_r').click(function(){
    layer.open({
      type:1,
      title:'提示',
      area:['380px','auto'],
      skin:'pop',
      closeBtn:2,
      content:$('.refuse_c'),
      btn:['保存','取消'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  //查看收货回单

});
//收货页面
function upload(){
  var upload=$('#upload');
  var nameContainer=$('#name');
  upload.onchange=function(){
    console.log(1);
    var name=[];
    for(var i=0;i<this.files.length;i++){
      name[i]= this.files[i].name;
      if(this.files[i].size>=307200){
        alert("文件"+this.files[i].name+"过大，不能超过300kb")
      }
    }
    nameContainer.innerHTML=name;
  }
}
upload();
//功能点2:查看物流信息
//$('.examination>table td:nth-child(2) div.see_logistics').hover(
//  function(){
//    $(this).children('.logistics').toggle();
//  }
//);
