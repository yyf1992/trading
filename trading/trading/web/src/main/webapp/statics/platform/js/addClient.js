
//弹出收货地址
function addShipAddr(obj){
	  layer.open({
      type: 1,
      title: '收货地址修改',
      area: ['380px', 'auto'],
      skin: 'pop ',
      closeBtn:2,
      content: $('.edit_addr'),
      btn: ['保存','取消'],
      yes:function(index){
    	var addr1 = $("#addr1 option:selected").text();
		if(addr1 === "省份"){
			layer.tips('请选择省份', '#addr1',{
				tips: [1, '#f00'],
	  			time: 4000
			});
			return;
		}
    	var addr2 = $("#addr2 option:selected").text();
		if(addr2 === "地级市"){
			layer.tips('请选择市', '#addr2',{
				tips: [1, '#f00'],
	  			time: 4000
			});
			return;
		}
    	var addr3 = $("#addr3 option:selected").text();
		if(addr3 === "县区"){
			layer.tips('请选择县区', '#addr3',{
				tips: [1, '#f00'],
	  			time: 4000
			});
			return;
		}
    	var addr4 = $('#addr4').val();
		if(addr4 === ""){
			layer.tips('请填写详细地址', '#addr4',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
        var str='';
        str+=$("#addr1 option:selected").text();
        str+=$("#addr2 option:selected").text();
        str+=$("#addr3 option:selected").text();
        str+=$('#addr4').val();
        if (str.toString().indexOf("县区")>0){
        	$(obj).html('点击选择收货地址');	
        }else{
        	$(obj).html(str);
        }  
        layer.close(index);
      }.bind(this)
    });
}

function addr1(){
	// 清空城市和区
	$("#addr2 option[value!='']").remove();
	$("#addr3 option[value!='']").remove();
	// 根据省份加载市
	var provinceId = $("#addr1").children('option:selected').val();
	$.ajax({
		type : "post",
		url : 'platform/reg/loadCityByProvinceId',
		data : {
			"provinceId" : provinceId
		},
		
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			$.each(result.citiesList, function(index, element) {
				var id = element.cityId;
                var name = element.city;
                var opt = "<option value='" + id + "'>" + name + "</option>";  
				$("#addr2").append(opt);
		    });
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

function addr2(){
// 清空区
	$("#addr3 option[value!='']").remove();
	// 根据市加载区
	var cityId = $("#addr2").children('option:selected').val();
	$.ajax({
		type : "post",
		url : 'platform/reg/loadAreaByCityId.html',
		data : {
			"cityId" : cityId
		},
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			$.each(result.areasList, function(index, element) {
				var id = element.areaId;
                var name = element.area;
                var opt = "<option value='" + id + "'>" + name + "</option>";  
				$("#addr3").append(opt);
		    });
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}



//联系人删除一行
function deleteClientLinkman(){
	$('.c_settings').on('click','.table_del',function(){
	  layer.confirm('确定删除该联系人吗？', {
	    icon:3,
	    title:'提醒',
	    skin:'pop',
	    closeBtn:2,
	    btn: ['确定','取消']
	  }, function(){
	    delRow(this);    
	    layer.msg('删除成功',{time:500});
	  }.bind(this));
	});
}

//客户联系人新增一行
$('.customer_a').click(function(e){
	  e.preventDefault();
	  var str='<td style="width:6%"><input type="radio" name="default"  onclick="setIsDefault(this)"></td><!-- 默认为1 -->'+
  	  '<td><input type="text" placeholder="联系人"></td>'+
		  '<td><input type="text" placeholder="手机"></td>'+
		  '<td>'+
		  ' <input type="text" placeholder="区号" style="width:40px">-'+
		  ' <input type="text" style="width:70px" placeholder="电话">'+
      '</td>'+
      '<td onclick="addShipAddr(this);">点击选择收货地址</td>'+
      '<td><input type="text" placeholder="传真"></td>'+
      '<td><input type="text" placeholder="QQ"></td>'+
      '<td><input type="text" placeholder="旺旺"></td>'+
      '<td><input type="text" placeholder="E-mail"></td>'+
      '<td><span class="table_del" onclick="deleteClientLinkman();"><b></b>删除</span>'+
      '<input type="hidden" id="isDefault" name="isDefault" value="0">'+
      '</td>';
var tbl=$('.c_settings tr:eq(-1)');
addRow(str,tbl);
});
function addLinkman(){

  var str='<td style="width:6%"><input type="radio" name="default"  onclick="setIsDefault(this)"></td><!-- 默认为1 -->'+
	  	  '<td><input type="text" placeholder="联系人"></td>'+
  		  '<td><input type="text" placeholder="手机"></td>'+
  		  '<td>'+
  		  ' <input type="text" placeholder="区号" style="width:40px">-'+
  		  ' <input type="text" style="width:70px" placeholder="电话">'+
          '</td>'+
          '<td onclick="addShipAddr(this);">点击选择收货地址</td>'+
          '<td><input type="text" placeholder="传真"></td>'+
          '<td><input type="text" placeholder="QQ"></td>'+
          '<td><input type="text" placeholder="旺旺"></td>'+
          '<td><input type="text" placeholder="E-mail"></td>'+
          '<td><span class="table_del" onclick="delClientLinkman();"><b></b>删除</span>'+
          '<input type="hidden" id="isDefault" name="isDefault" value="0">'+
          '</td>';
  var tbl=$('.c_settings tr:eq(-1)');
  addRow(str,tbl);
}	

//设置默认联系人
function setIsDefault(obj){
	$("#ContactSettings tr").each(function() {
		$(this).find("td:eq(9)").find("input").val("0");
	});
	$(obj).parents("tr").find("td:eq(9)").find("input").val("1");
}
//设置默认联系人
//function setIsDefault(obj){
//
//	var isDefault = $(obj).parents("tr").find("td:eq(8)").find("a").html();
//	if (isDefault == "设为默认"){	
//		$("#ContactSettings tr").each(function() {
//			$(this).find("td:eq(8)").find("input").val("0");
//			$(this).find("td:eq(8)").find("a").html("设为默认");
//		});
//		$(obj).parents("tr").find("td:eq(8)").find("input").val("1");
//		$(obj).parents("tr").find("td:eq(8)").find("a").html("取消默认");
//	}else{
//		$(obj).parents("tr").find("td:eq(8)").find("a").html("设为默认");
//	}
//
//	layer.msg("设置成功！",{icon:1});
//}

//对输入信息判断
function check(){
	var clientName = $("#clientName").val();
	if(clientName == ''){
		//layer.msg("请输入客户名称！",{icon:2});
		layer.tips('请输入客户名称！', '#clientName',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var bankAccount = $("#bankAccount").val();
	if(bankAccount == ''){
		//layer.msg("请输入银行账号！",{icon:2});
		layer.tips('请输入银行账号！', '#bankAccount',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var openBank = $("#openBank").val();
	if(openBank == ''){
		//layer.msg("请输入开户行！",{icon:2});
		layer.tips('请输入开户行！', '#openBank',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var accountName = $("#accountName").val();
	if(accountName == ''){
		//layer.msg("请输入户名！",{icon:2});
		layer.tips('请输入户名！', '#accountName',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var taxidenNum = $("#taxidenNum").val();
	if(taxidenNum == ''){
		//layer.msg("请输入纳税人识别号！",{icon:2});
		layer.tips('请输入纳税人识别号！', '#taxidenNum',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
}

//保存联系人
function saveClient(){
	var clientName = $("#clientName").val();
	if(clientName == ''){
		//layer.msg("请输入客户名称！",{icon:2});
		layer.tips('请输入客户名称！', '#clientName',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var bankAccount = $("#bankAccount").val();
	if(bankAccount == ''){
		//layer.msg("请输入银行账号！",{icon:2});
		layer.tips('请输入银行账号！', '#bankAccount',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var openBank = $("#openBank").val();
	if(openBank == ''){
		//layer.msg("请输入开户行！",{icon:2});
		layer.tips('请输入开户行！', '#openBank',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var accountName = $("#accountName").val();
	if(accountName == ''){
		//layer.msg("请输入户名！",{icon:2});
		layer.tips('请输入户名！', '#accountName',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}
	var taxidenNum = $("#taxidenNum").val();
	if(taxidenNum == ''){
		//layer.msg("请输入纳税人识别号！",{icon:2});
		layer.tips('请输入纳税人识别号！', '#taxidenNum',{
			tips: [2, '#f00'],
  			time: 4000
		});
		return;
	}

	var ContactSettings = new Array();
	var personInfor = "";
	var clientPhoneInfor = "";
	$("#ContactSettings tr").each(function(i) {
		var clientPerson = $(this).find("td:eq(1)").find("input").val();
		if(clientPerson==''){
			personInfor = "第"+(i+1)+"行没有填写联系人！";
			return false; 
		}
		var clientPhone = $(this).find("td:eq(2)").find("input").val();
		if(clientPhone==''){
			clientPhoneInfor = "第"+(i+1)+"行没有填写手机号！";
			return false; 
		}
		var zone = $(this).find("td:eq(3)").find("input:eq(0)").val();
		var telno = $(this).find("td:eq(3)").find("input:eq(1)").val();
		var shipAddress = $(this).find("td:eq(4)").html();
		var fax = $(this).find("td:eq(5)").find("input").val();
		var qq = $(this).find("td:eq(6)").find("input").val();
		var wangNo = $(this).find("td:eq(7)").find("input").val();
		var email = $(this).find("td:eq(8)").find("input").val();
		var isDefault = $(this).find("td:eq(9)").find("input").val();
		var item = new Object();
		item.clientPerson = clientPerson;
		item.clientPhone = clientPhone;
		item.zone = zone;
		item.telno = telno;
		item.shipAddress = shipAddress;
		item.fax = fax;
		item.qq = qq;
		item.wangNo = wangNo;
		item.email = email;
		item.isDefault = isDefault;
		ContactSettings.push(item);
	});
	if(personInfor!=''){
		layer.msg(personInfor,{icon:2});
		return;
	}
	if(clientPhoneInfor!=''){
		layer.msg(clientPhoneInfor,{icon:2});
		return;
	}

	$.ajax({
		type : "post",
		url : "platform/sellers/client/saveSellerClient",
		async: false,
		data : {
			"clientName" : clientName,
			"bankAccount" : bankAccount,
			"openBank" : openBank,
			"accountName" : accountName,
			"taxidenNum" : taxidenNum,
			"ContactSettings" : JSON.stringify(ContactSettings)
		},
		success : function(data) {
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				leftMenuClick(this,"platform/sellers/client/addClientSuccess","buyer");
			}else{
				layer.msg(resultObj.msg,{icon:2});
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
