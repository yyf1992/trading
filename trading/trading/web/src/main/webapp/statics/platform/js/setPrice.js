//选择商品  
function selectGood(obj){
	var trId=$(obj).parent().parent("tr").attr("id");
	var url = "platform/buyer/sysshopproduct/selectGoods";
	$.ajax({
		url:url,
		type:"get",
		data:{
			"page.pageSize":"5",
			"page.divId":"layOpen",
			"trId":trId
		},
		async:false,
		success:function(data){
			  layer.open({
			      type: 1,
			      skin:'unit_new',
			      title: '商品列表',
			      area: ['1200px','auto'],
			      content:data,
				  });
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}

//商品出货价格设置保存
function saveSetPrice(obj){
	var parentTr=$(obj).parents().find("#tableForm").find("table tbody tr")
	var repeat = true;
	var repeatStr = "";
	var adjust = true;
	var shopProductStr = ""; 
	
	$(parentTr).each(function(i) {
		var shopObj = $(this).find("select[name='shopId']");
		var shopId=$(shopObj).find("option:selected").attr("id");
		var shopCode = shopObj.val();
		var shopName = $(shopObj).find("option:selected").text();
		var productName=$(this).find("input[name=productName]").val();
		var productCode=$(this).find("input[name=productCode]").val();
		var skuName=$(this).find("input[name=skuName]").val();
		var skuCode=$(this).find("input[name=skuCode]").val();
		var costPrice=$(this).find("input[name=costPrice]").val();
		
		//验证商品信息是否为空
		var barcodeObj=$(this).find("input[name=barcode]");
		var barcode=barcodeObj.val();
		if(barcode == ""){
			adjust=false;
			var span = barcodeObj.parent().find("span");
			if(span.length == 0){
				span = $("<span></span>");
				barcodeObj.parent().append(span);
			}
			span.html("不能为空！");
			barcodeObj.parent().addClass("error");
		}
		
		//验证价格是否为空
		var priceObj=$(this).find("input[name=price]");
		var priceValue=priceObj.val();
		if(priceValue == ""){
			adjust=false;
			var span = priceObj.parent().find("span");
			if(span.length == 0){
				span = $("<span></span>");
				priceObj.parent().append(span);
			}
			span.html("不能为空！");
			priceObj.parent().addClass("error");
		}
		
		shopProductStr += shopId +"|" + shopCode+"|" + shopName+"|" + productName+"|"+ productCode+"|"+barcode+"|"+ skuName+"|"+ skuCode+"|"+ costPrice+"|"+priceValue+"@";
		
		//判断是否有重复数据
		var shopSkuoid = shopId+"|"+barcode+"@";
		if(repeatStr.indexOf(shopSkuoid)>-1){
			//有重复数据
			repeat = false;
			$(this).addClass("error");
		}else{
			repeatStr += shopSkuoid;
		}
	});
	
	if(adjust){
		if(repeat){
			if($(obj).parents("#tableForm").find("table tbody").find(".error").length == 0){
				$.ajax({
					url:"platform/buyer/sysshopproduct/saveSetPrice",
					type:'post',
					data : {
						"shopProductStr" : shopProductStr
					},
					async:false,
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385'); 
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385'); 
						}
					},
					error:function(){
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
					
				});
			}
		}
	}
}

function applyPurchaseKeyUp(obj){
	var type = "1";
	// 验证合法性
	if(objValidate(obj,type)){
		// 验证成功
		$(obj).parent().find("span").remove();
		$(obj).parent().removeClass("error");
	}else{
		//失败
		var str = "请输入整数或最多四位小数！";
		var span = $(obj).parent().find("span");
		if(span.length == 0){
			span = $("<span></span>");
			$(obj).parent().append(span);
		}
		span.html(str);
		$(obj).parent().addClass("error");
	}
}
//保存新增辅料
function saveMaterial(obj){
	var productCode = $("#productCode").val();
	var productName = $("#productName").val();
	var shortName = $("#shortName").val();
	var productType = $("input[name='productType'][checked]").val();
	var unitId = $('#unitId option:selected').val();
	// 附件信息
	var a3 = $("#attachment3Div").find("span").length;
	if (a3 <1){
		layer.msg("请上传商品图片！",{icon:7});
		return;
	}
	if(a3 > 1){
		layer.msg("图片只允许上传一个！",{icon:7});
		return;
	}
	var url3 = $("#attachment3Div").find("span").find("input").val();
	//var productForm=$(obj).parents().find("#productForm");
	$.ajax({
		url:"platform/product/saveProduct",
		type:"post",
		async:false,
		data:{
			"productName": productName,
			"productCode": productCode,
			"shortName": shortName,
			"productType": productType,
			"unitId":unitId,
			"mainPictureUrl": url3
		},
			//productForm.serialize(),
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1});
				leftMenuClick(this,'platform/product/allProduct?productType=2','buyer'); 
			}else{
				layer.msg(resultObj.msg,{icon:2});
				/*leftMenuClick(this,'platform/product/allProduct','buyer'); */
			}
		},error:function(){
			layer.msg("获取数据失吧，请稍后重试！",{icon:2});
		}
	});
}
//保存新增原材料
function saveMaterial(obj){
	var productCode = $("#productCode").val();
	var productName = $("#productName").val();
	var shortName = $("#shortName").val();
	var productType = $("input[name='productType'][checked]").val();
	var unitId = $('#unitId option:selected').val();
	// 附件信息
	var a3 = $("#attachment3Div").find("span").length;
	if (a3 <1){
		layer.msg("请上传商品图片！",{icon:7});
		return;
	}
	if(a3 > 1){
		layer.msg("图片只允许上传一个！",{icon:7});
		return;
	}
	var url3 = $("#attachment3Div").find("span").find("input").val();
	//var productForm=$(obj).parents().find("#productForm");
	$.ajax({
		url:"platform/product/saveProduct",
		type:"post",
		async:false,
		data:{
			"productName": productName,
			"productCode": productCode,
			"shortName": shortName,
			"productType": productType,
			"unitId":unitId,
			"mainPictureUrl": url3
		},
			//productForm.serialize(),
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1});
				leftMenuClick(this,'platform/product/allProduct?productType=1','buyer'); 
			}else{
				layer.msg(resultObj.msg,{icon:2});
				/*leftMenuClick(this,'platform/product/allProduct','buyer'); */
			}
		},error:function(){
			layer.msg("获取数据失吧，请稍后重试！",{icon:2});
		}
	});
}
//保存新增商品
function saveProduct(obj){

	var productCode = $("#productCode").val();
	var productName = $("#productName").val();
	var shortName = $("#shortName").val();
	var productType = $("input[name='productType'][checked]").val();
	var unitId = $('#unitId option:selected').val();
	// 附件信息
	var a3 = $("#attachment3Div").find("span").length;
	if (a3 <1){
		layer.msg("请上传商品图片！",{icon:7});
		return;
	}
	if(a3 > 1){
		layer.msg("图片只允许上传一个！",{icon:7});
		return;
	}
	var url3 = $("#attachment3Div").find("span").find("input").val();
//	var productForm=$(obj).parents().find("#productForm");
	$.ajax({
		url:"platform/product/saveProduct",
		type:"post",
		async:false,
		data:{
			"productName": productName,
			"productCode": productCode,
			"shortName": shortName,
			"productType": productType,
			"unitId":unitId,
			"mainPictureUrl": url3
		},
			//productForm.serialize(),
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1});
				leftMenuClick(this,'platform/product/allProduct','buyer'); 
			}else{
				layer.msg(resultObj.msg,{icon:2});
				/*leftMenuClick(this,'platform/product/allProduct','buyer'); */
			}
		},error:function(){
			layer.msg("获取数据失吧，请稍后重试！",{icon:2});
		}
	});
}
//修改保存商品
function updateProduct(obj){
	
	//var productForm=$(obj).parents().find("#productForm");
	var productCode = $("#productCode").val();
	var productName = $("#productName").val();
	var id = $("#id").val();
	var productType = $("#productTypeNum").val();
	var unitId = $('#unitId option:selected').val();
	$.ajax({
		url:"platform/product/updateProduct",
		type:"post",
		async:false,
		data:{
			"id":id,
			"productName": productName,
			"productCode": productCode,
			"unitId":unitId
		},
		//productForm.serialize(),
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1});
				leftMenuClick(this,'platform/product/allProduct?productType='+productType,'buyer'); 
			}else{
				layer.msg(resultObj.msg,{icon:2});
				/*leftMenuClick(this,'platform/product/allProduct','buyer'); */
			}
		},error:function(){
			layer.msg("获取数据失吧，请稍后重试！",{icon:2});
		}
	});
}
//新增商品sku
function newSku(id,productCode,productType,unitId,productName){
	$.ajax({
		url:"platform/product/loadAddSKU",
		type:"get",
		async:false,
		data:{
			"id":id,
			"productCode":productCode,
			"productType":productType,
			"unitId":unitId,
			"productName":productName
			},
		success:function(data){
			layer.open({
			    type: 1,
			    title: '新增商品SKU',
			    area: ['400px', 'auto'],
			    skin:'pop btnCenter',
			    closeBtn:2,
			    content:data,
			    btn:['保存','关闭'],
				yes:function(index,layerio){
					var dataForm = $("#skuForm").serialize();
					$.ajax({
						url:"platform/product/saveSku",
						type:"post",
						async:false,
						data:dataForm,
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,"platform/product/loadAllSKU?id="+id+"&productCode="+productCode+"&productType="+productType+"&unitId="+unitId+"&productName="+productName,'buyer');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,"platform/product/loadAllSKU?id="+id+"&productCode="+productCode+"&productType="+productType+"&unitId="+unitId+"&productName="+productName,'buyer');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}
//修改商品sku
function editSku(id,productId,productCode,productType,unitId,productName){
	$.ajax({
		url:"platform/product/loadEditSku",
		type:"post",
		data:{
			"id":id
			},
		async:false,
		success:function(data){
			layer.open({
			    type: 1,
			    title: '修改商品SKU',
			    area: ['400px', 'auto'],
			    skin:'pop btnCenter',
			    closeBtn:2,
			    content:data,
			    btn:['保存','关闭'],
				yes:function(index,layerio){
					$.ajax({
						url:"platform/product/saveEditSku",
						type:"post",
						async:false,
						data:$("#editSkuForm").serialize(),
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
//							var productCode=$("#editSkuForm").find('input[name="productCode"]').val();
//							var productType=$("#editSkuForm").find('input[name="productType"]').val();
//							var unitId=$("#editSkuForm").find('input[name="unitId"]').val();
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,"platform/product/loadAllSKU?id="+productId+"&productCode="+productCode+"&productType="+productType+"&unitId="+unitId+"&productName="+productName,'buyer');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,"platform/product/loadAllSKU?id="+productId+"&productCode="+productCode+"&productType="+productType+"&unitId="+unitId+"&productName="+productName,'buyer');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}
//删除商品sku
function deleteSku(id,productId,productCode,productType,unitId,productName){
	  layer.confirm('<p class="orange size_sm text-center"><img src="statics/platform/images/icon.jpg">&emsp;确定删除该商品SKU吗？</p>', {
		    //icon:3,
		    title:'提示',
		    skin:'pop btnCenter delete pop25',
		    closeBtn:0,
		    area:['310px','auto'],
		    btn: ['确定','取消']
		  }, function(){
	      		$.ajax({
					url:"platform/product/deleteSku",
					async:false,
					type:"post",
					data:{"id":id},
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,"platform/product/loadAllSKU?id="+productId+"&productCode="+productCode+"&productType="+productType+"&unitId="+unitId+"&productName="+productName,'buyer');
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error:function(){
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		  }.bind(this));
}
 //新增单位
 function addUnit(){
	 $.ajax({
		 url:"platform/product/loadAddUnitHtml",
		 async:false,
		 type:"get",
		 success:function(data){
			    layer.open({
			        type: 1,
			        skin:'pop',
			        title: '计量单位管理',
			        area: ['420px','auto'],
			        closeBtn:2,
			        btn:['保存','关闭'],
			        content: data,
			        yes:function(index){
			        	var unitStr="";
			        	var repeat=true;
			        	$(".unit_list>table>tbody>tr").each(function(){
			        		var exitUnitObj=$(this).find("td input[name='unitId']");
			        		if(exitUnitObj.length<=0){
				        		var newUnitObj=$(this).find("td:eq(1) input");
				        		var unitName=newUnitObj.val();
				        		if(unitName != ""){
				        			var unit=unitName+"@";
				        			if(unitStr.indexOf(unit)>-1){
					        			$(this).addClass("error");
					        			repeat=false;
					        		}else{
					        			unitStr+=unit;
					        		}
				        		}
				         		
			        		}
			        	});
			        	
			        		if(repeat){
			        			$.ajax({
			        				url:"platform/product/saveUnit",
			        				async:false,
			        				data:{"unitStr":unitStr},
			        				success:function(data){
			    						var result=eval('('+data+')');
			    						var resultObj=isJSONObject(result)?result:eval('('+result+')');
			    						if(resultObj.success){
			    							layer.close(index);
			    							layer.msg(resultObj.msg,{icon:1});
			    						}
			    						else{
			    							if(resultObj.msg=="关闭"){
			    								layer.close(index);
			    							}else{
			    								layer.msg(resultObj.msg,{icon:2});
			    							}
			    						}
			    						loadUnit();
			        				},
			        				error:function(){
			        					layer.msg("获取数据失败，请稍后重试！",{icon:2});
			        				}
			        			});
			        		}
			        },
					btn2 : function() {
						loadUnit();
					}
			      })
		 },
		 error:function(){
			 layer.msg("获取数据失败，请稍后重试！",{icon:2});
		 }
	 
	 })
 }
 function loadUnit(){
	 
		$.ajax({
			url:"platform/product/loadUnitList",
			success:function(data){
				$("#unitDivId").empty();
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				var selectObj = "<select name='unitId' id='unitId' lay-filter='aihao'>";
				$.each(resultObj,function(i,data){
					selectObj += "<option value='"+data.id+"'>"+data.unitName+"</option>";
				});
				selectObj += "</select>";
				$("#unitDivId").append(selectObj);
				var form = layui.form;
				form.render();				
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
 
 //保存提价方式设置
 function submitLatitude(obj){
		var adjust=true;
		var trObj = $(obj).parents().find(".commodityNew").find("tbody tr");
		$(trObj).each(function(i){
			var priceObj=$(this).find("input[name='price']");
			var price=priceObj.val();
			var barcodeObj=$(this).children("td").eq(2).find("input");
			var barcode=barcodeObj.val();
			if(price ==null || price.length ==0){
				adjust=false;
				$(priceObj).parent().addClass("error");
			}
			if(barcode ==null || barcode.length ==0){
				adjust=false;
				$(barcodeObj).parent("td").addClass("error");
			}
		});
		
		
		var barcodeStr="";
		$(".commodityNew>tbody>tr").each(function(i){
			var productId=$(this).find("input[type='hidden']").val();
			var productName=$(this).find("td:nth-child(1) input").val();
			var productCode=$(this).find("td:nth-child(2) input").val();
			var barcode=$(this).find("td:nth-child(3) input").val();
			var skuCode=$(this).find("td:nth-child(4) input").val();
			var skuName=$(this).find("td:nth-child(5) input").val();
			var unitId=$(this).find("td:nth-child(6) .unitId").val();
			var priceStstus=$(this).find("td:nth-child(7) select[name='priceType']").val();
			var priceLatitude=$(this).find("td:nth-child(8) input").val();
			barcodeStr+=productName+","+productCode+","+barcode+","+skuCode+","+skuName+","+unitId+","+priceStstus+","+priceLatitude+","+productId+"|";
		});
		if(adjust){
			if($(trObj).find("td").find(".error").length==0){
				$.ajax({
					url:"platform/buyer/sysshopproduct/submitLatitude",
					type:'post',
					data : {
						"barcodeStr" : barcodeStr
					},
					async:false,
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.msg(resultObj.msg,{icon:1,time:500});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
						}else{
							layer.msg(resultObj.msg,{icon:2,time:500});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
						}
					},
					error:function(){
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
					
				});
			}
		}
	}


//报表导出
function exportData(id,excelName){
		var e = window.event || e;
		e.preventDefault();
		$("#"+id).table2excel({
			// 不被导出的表格行的CSS class类
			exclude : ".noExl",
			// 导出的Excel文档的名称
			name: "Excel Document Name",
			// Excel文件的名称
			filename : excelName
		});
	}

//上传操作
function uploadImage() {
    var formData = new FormData();
    var files = document.getElementById("mainPictureUrl").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/product/uploadImage";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#attachment3Div\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#attachment3Div").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#mainPictureUrl").replaceWith('<input type="file" multiple name="mainPictureUrl" id="mainPictureUrl" onchange="uploadImage();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

	//同步OMS商品
	function synchronousOMSProduct(type){
		$.ajax({
			url:"platform/product/loadOMSProduct",
			type:"get",
			async:false,
			data:{
				"type":type
			},
			success:function(data){
				layer.open({
				    type: 1,
				    title: '同步OMS商品',
				    area: ['400px', 'auto'],
				    skin:'pop btnCenter',
				    closeBtn:2,
				    content:data,
				    btn:['确定','关闭'],
					yes:function(index,layerio){
						var dataForm = $("#skuOMSForm").serialize();
						$.ajax({
							url:"platform/product/synchronousOMSProduct",
							type:"post",
							async:false,
							data:dataForm,
							success:function(data){
								var result=eval('('+data+')');
								var resultObj=isJSONObject(result)?result:eval('('+result+')');
								if(resultObj.success){
									layer.close(index);
									layer.msg(resultObj.msg,{icon:1});
									leftMenuClick(this,"platform/product/allProduct?productType="+resultObj.type,"baseInfo");
								}else{
									layer.msg(resultObj.msg,{icon:2});
									leftMenuClick(this,"platform/product/allProduct?productType="+resultObj.type,"baseInfo");
								}
							},
							error:function(){
								layer.msg("获取数据失败，稍后重试！",{icon:2});
							}
						});
					}
				});
			},
			error:function(){
				layer.msg("获取数据失败，稍后重试！",{icon:2});
			}
		});
	}
