//功能点2：分页
function paging(){
  function showPages(page, total) {
    var str='<a class="current">'+page+'</a>';
    for (var i=1;i<=3;i++){
      if (page - i > 1) {
        str='<a href="#">'+(page-i)+'</a> '+str;
      }
      if(page+i<total){
        str=str+' <a href="#">'+(page+i)+'</a>';
      }
    }
    if(page-4>1){
      str='... '+str;
    }
    if (page>1){
      str='<a href="#">&lt;上一页</a> <a href="#">1</a>'+ ' '+str;
    }
    if(page+4<total) {
      str=str+ ' ...';
    }
    if(page<total){
      str = str + ' <a href="#">' + total + '</a> <a href="#">下一页&gt;</a>';
    }
    str+=' 共'+total+'页,<span>300</span>个供应商&nbsp;&nbsp;转到 <input> 页&nbsp;&nbsp;<button>确定</button>';
    return str;
  }
  var total=110;
  for (var i=1;i<=total;i++) {
    var ret=showPages(10,total);
    $('.pager').html(ret);
  }
}
paging();
//功能点4:鼠标悬停在列表上的变化
$('.purchase_list').on('mouseover','tr',function(){
  $(this).find('b').css('background',"url('images/spirit.png') no-repeat -67px -13px");
}).on('mouseout','tr',function(){
  $(this).find('b').css('background',"url('images/spirit.png') no-repeat -159px -13px")
});
//功能点5:点击输入框，弹出下拉商品
$('.purchase_list').on('focus','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','block');
  $(this).siblings('ul').menu();
}).on('blur','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','none');
});
//功能点10:点击增加项，表格增加一行
$('.supplier+button').click(function(){
  var str=`<td><b></b></td>
                <td>
                  <input type="text" placeholder="输入商品名称/货号/条形码/规格等关键字">
                  <button class="layui-btn layui-btn-mini"><i class="layui-icon">&#xe61f;</i> 新增商品</button>
                  <ul class="menu">
                    <li><div>护具</div>
                      <ul>
                        <li><div>腿部护具</div>
                          <ul>
                            <li><div>腿部护具</div></li>
                            <li><div>膝盖护具</div></li>
                            <li><div>腕部护具</div></li>
                          </ul>
                        </li>
                        <li><div>膝盖护具</div></li>
                        <li><div>腕部护具</div></li>
                      </ul>
                    </li>
                    <li><div>腿部护具 膝盖护具 258585556</div>
                      <ul>
                        <li><div>腿部护具</div></li>
                        <li><div>膝盖护具</div></li>
                        <li><div>腕部护具</div></li>
                      </ul>
                    </li>
                    <li><div>护具 套装 成人</div></li>
                    <li><div>护具 套装 成人</div></li>
                    <li><div>护具 套装 成人</div></li>
                  </ul>
                <td>
                  <select name="unit">
                    <option value="">请选择</option>
                    <option value="">个</option>
                    <option value="">箱</option>
                    <option value="">套</option>
                  </select>
                </td>
                <td><input type="number" value="0" min="0"></td>
                <td>
                  <select name="ware">
                    <option value="">请选择</option>
                    <option value="">大仓</option>
                    <option value="">小仓</option>
                    <option value="">质检仓</option>
                  </select>
                </td>
                <td>
                  <select name="invoice" class="isInvoice">
                    <option value="">否</option>
                    <option value="">是</option>
                  </select>
                </td>
                <td><textarea name="remarks" placeholder="备注内容"></textarea></td>`;
  var tbl=$('.supplier tr:eq(-2)');
  addRow(str,tbl);
});
//删除表格某一行
$('.purchase_list').on('click','b',function(){
  delRow(this);
});
layui.use(['layer','form'], function(){
  var layer = layui.layer;
  //功能点11:弹出增加商品框
  $('.purchase_list').on('click','button',function(){
    layer.open({
      type: 1,
      title: '新增商品',
      area: ['700px', 'auto'],
      skin: 'add',
      closeBtn:2,
      btn:['保存'],
      content: $('.new_goods'),
      yes:function(index){
        layer.close(index);
        layer.msg('保存成功',{icon:1})
      }
    });
  });
});
//功能点12:新增商品弹出框表格增加、删除一行
$('.new_goods>button').click(function(){
  var str=`<td><b></b></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>`;
  var tbl=$('.new_goods>table tr:eq(-2)');
  addRow(str,tbl);
});
$('.new_goods>table').on('click','b',function(){
  delRow(this);
});
//功能点13:关闭弹出框
//$('.new_goods>i').click(function(){
//  $(this).parent().css('display','none');
//});
//功能点14：数量求和
function sum(){
  var sum=0;
  var total=$('.supplier .total');
  $('.supplier input[type="number"]').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
$('.supplier').on('click','input[type="number"]',function(){
  sum();
});
//失去焦点时求和
$('.supplier').on('blur','input[type="number"]',function(){
  sum();
});
//删除一行时求和
$('.supplier').on('click','b',function(){
  sum();
});
//功能点6:手工录入采购宝贝页面，点击不含运费，弹出输入框
$('.voucher>li input.no_postage').click(function(){
  $(this).parent().next().css('display','inline-block');
});
$('.voucher>li input.postage').click(function(){
  $(this).parent().nextAll('span').css('display','none');
});
//表格增加和删除一行函数
function addRow(str,tbl){
var addTr=document.createElement('tr');
  addTr.innerHTML=str;
  tbl.after(addTr);
}
function delRow(tr){
  $(tr).parent().parent().remove();
}
//供应商列表搜索
supplier_list.onkeyup=function(){
  $.ajax({
    type:'get',
    url:'',
    data:{'kw':supplier_list.val()},
    success:function(data){
      var html='';
      for(var i=0;i<data.length;i++){
        var obj=data[i];
        html+=`<li id="52">温州市奥妙鞋业有限公司</li>`;
      }
      $('.Pro_ul').html(html);
    }
  });
};
