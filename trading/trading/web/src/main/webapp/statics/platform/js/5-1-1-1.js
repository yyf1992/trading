layui.use(['layer','form','laydate'], function(){
  var layer = layui.layer;
// 功能点5:点击输入框，弹出下拉商品
$('.purchase_list').on('focus','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','block');
  $(this).siblings('ul').menu();
}).on('blur','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','none');
});
// 功能点10:点击增加项，表格增加一行
$('.supplier+button').click(function(){
  var str='<td><b></b></td><td><input type="text" placeholder="输入商品名称/货号/条形码/规格等关键字"><a class="increase_goods" href="商品管理-供应商互通-新增商品关联.html" target="_blank"><i></i>新增商品</a><ul class="menu"><li><div>护具</div><ul><li><div>腿部护具</div><ul><li><div>腿部护具</div></li><li><div>膝盖护具</div></li><li><div>腕部护具</div></li></ul></li><li><div>膝盖护具</div></li><li><div>腕部护具</div></li></ul></li><li><div>腿部护具 膝盖护具 258585556</div><ul><li><div>腿部护具</div></li><li><div>膝盖护具</div></li><li><div>腕部护具</div></li></ul></li><li><div>护具 套装 成人</div></li><li><div>护具 套装 成人</div></li><li><div>护具 套装 成人</div></li></ul></td><td class="lineH30">件</td><td class="lineH30">120.00</td><td><input type="number" value="0" min="0" class="text-center"></td><td><select name="ware"><option value="">请选择</option><option value="">大仓</option><option value="">小仓</option><option value="">质检仓</option></select></td><td><select name="invoice" class="isInvoice"><option value="">否</option><option value="">是</option></select></td><td><textarea name="remarks" placeholder="备注内容"></textarea></td>';
  var tbl=$('.supplier tr:eq(-2)');
  addRow(str,tbl);
});
// 删除表格某一行
$('.purchase_list').on('click','b',function(){
  layer.confirm('确定要删除该商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});
  // 功能点11:弹出增加商品框
  $('.purchase_list').on('click','span.increase_goods',function(){
    layer.open({
      type:1,
      title:'新增商品',
      area:['900px', '460px'],
      skin:'pop',
      closeBtn:2,
      content:$('.new_goods'),
      btn:['保存'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  // 邀请供应商互通
  $('.invite').click(function(){
    layer.open({
      type:1,
      title:'邀请互通好友',
      area:['330px', 'auto'],
      skin:'pop',
      closeBtn:2,
      content:$('.supplier_invite'),
      btn:['确定发送短信','取消'],
      yes:function(index){
        layer.close(index);
        layer.msg('发送成功',{time:800})
      }
    });
  });
});
// 功能点12:新增商品弹出框表格增加、删除一行
$('.new_goods>button').click(function(){
  var str='<td><i></i></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td>';
  var tbl=$('.new_goods table tr:eq(-1)');
  addRow(str,tbl);
});
$('.new_goods table').on('click','i',function(){
  layer.confirm('确定要删除该商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});
// 功能点14：数量求和
function sum(){
  var sum=0;
  var total=$('.supplier .total');
  $('.supplier input[type="number"]').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
$('.supplier').on('click','input[type="number"]',function(){
  sum();
});
// 失去焦点时求和
$('.supplier').on('blur','input[type="number"]',function(){
  sum();
});
// 删除一行时求和
$('.supplier').on('click','b',function(){
  sum();
});
// 功能点6:手工录入采购宝贝页面，点击不含运费，弹出输入框
$('.voucher>li input.no_postage').click(function(){
  $('.franking').css('display','inline-block');
});
$('.voucher>li input.postage').click(function(){
  $('.franking').css('display','none');
});
// 删除采购凭证
$('.voucherImg').on('click','b',function(){
  layer.confirm('确定要删除该图片吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    $(this).parent().remove();
  }.bind(this));
});
// 表格增加和删除一行函数
function addRow(str,tbl){
	var id = (new Date()).valueOf();
	var addTr=document.createElement('tr');
	addTr.setAttribute("id",id);
	addTr.innerHTML=str;
	tbl.after(addTr);
}
function delRow(tr){
  $(tr).parent().parent().remove();
}
// 手工录入采购商品页面
// 功能点15:点击增加项，表格增加一行
$('.manual+button').click(function(){
  var str='<td><b></b></td>'
        +'<td>'
        +'<input type="text" placeholder="输入商品名称/货号/条形码/规格等关键字">'
        +'<span class="increase_goods"><i></i>新增商品</span>'
        +'<ul class="menu">'
        +'<li><div>护具</div>'
        +'<ul>'
        +'<li><div>腿部护具腿部护具</div>'
        +'<ul>'
        +'<li><div>腿部护具</div></li>'
        +'<li><div>膝盖护具</div></li>'
        +'<li><div>腕部护具</div></li>'
        +'</ul>'
        +'</li>'
        +'<li><div>膝盖护具</div></li>'
        +'<li><div>腕部护具</div></li>'
        +'</ul>'
        +'</li>'
        +'<li><div>腿部护具 膝盖护具 258585556</div>'
        +'<ul>'
        +'<li><div>腿部护具</div></li>'
        +'<li><div>膝盖护具</div></li>'
        +'<li><div>腕部护具</div></li>'
        +'</ul>'
        +'</li>'
        +'<li><div>护具 套装 成人</div></li>'
        +'<li><div>护具 套装 成人</div></li>'
        +'<li><div>护具 套装 成人</div></li>'
        +'</ul>'
        +'<td class="lineH30">个</td>'
        +'<td><input type="number" value="0" min="0" class="unit_price"></td>'
        +'<td><input type="number" value="0" min="0" class="number"></td>'
        +'<td><input type="number" value="0" min="0" class="discount"></td>'
        +'<td class="subtotal lineH30">0.00</td>'
        +'<td>'
        +'<select name="">'
        +'<option value="">请选择</option>'
        +'<option value="0">大仓</option>'
        +'<option value="1">小仓</option>'
        +'<option value="2">质检仓</option>'
        +'</select>'
        +'</td>'
        +'<td><textarea name="" placeholder="备注内容"></textarea></td>';
  var tbl=$('.manual tr:eq(-2)');
  addRow(str,tbl);
});
// 数量求和
function calSum(){
  var sum=0;
  var total=$('.manual .num');
  $('.manual input.number').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
// 单击时求和
$('.manual').on('click','input.number',function(){
  calSum();
});
// 失去焦点时求和
$('.manual').on('blur','input.number',function(){
  calSum();
});
// 删除一行时求和
$('.manual').on('click','b',function(){
  calSum();
});
// 优惠金额求和
function discount(){
  var sum=0;
  var total=$('.manual .discount_sum');
  $('.manual input.discount').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum.toFixed(2));
}
// 单击时求和
$('.manual').on('click','input.discount',function(){
  discount();
});
// 失去焦点时求和
$('.manual').on('blur','input.discount',function(){
  discount();
});
// 删除一行时求和
$('.manual').on('click','b',function(){
  discount();
});
// 采购金额小计
// 求总金额
function calTotal(){
  var sum=0;
  $('.manual .subtotal').each(function(i,val){
    var p=$(this).html();
    sum+=parseFloat(p);
  });
  $('.manual .total').html(sum.toFixed(2));
}
// 单击单价时求和
$('.manual').on('click','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('click','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
// 失去焦点时求和
$('.manual').on('blur','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('blur','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
// 点击优惠金额时求和
$('.manual').on('click','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
// 失去焦点时
$('.manual').on('blur','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
// 下单页面
// 供应商采购提交订单
$('#supplierSubmit').click(function(){
  layer.open({
    type:1,
    title:'选择订单审批人',
    area:['565px', 'auto'],
    skin:'popGreen',
    closeBtn:1,
    content:$('.approverChoice'),
    btn:['确定','关闭'],
    yes:function(index){
      layer.close(index);
      location.href='向供应商提交订单成功.html';
    }
  });
});
// 切换收货地址
$('.addr').on('click','.addrItem',function(){
  $(this).addClass('selected').siblings().removeClass('selected');
});
$('.addr').on('mouseenter','.addrItem',function(){
  $(this).children('.borderBg').addClass('borderHover');
}).on('mouseleave','.addrItem',function(){
  $(this).children('.borderBg').removeClass('borderHover');
});
// 修改收货地址
$('.addr_modify').click(function(){
  layer.open({
    type: 1,
    title:'修改地址',
    skin: 'pop btnCenter',
    closeBtn:2,
    area: ['668px', '380px'],
    btn:['保存','关闭'],
    content:$('.address_modify')
  });
});
// 新增收货地址
$('#addr_new').click(function(){
  layer.open({
    type: 1,
    title:'新增收货地址',
    skin: 'pop btnCenter',
    closeBtn:2,
    area: ['668px', '380px'],
    btn:['保存','关闭'],
    content:$('.address_modify')
  });
});
// 采购计划模块
// 创建采购计划
/*
 * $('.purchasePlan').on('focus','td:nth-child(2) input',function(){
 * $(this).siblings('ul').css('display','block'); $(this).siblings('ul').menu();
 * }).on('blur','td:nth-child(2) input',function(){
 * $(this).siblings('ul').css('display','none'); });
 */
// 增加一行
$('#planAdd').click(function(){
	var saleDateNumber = Number($("#saleDateNumber").val()) + 1;
	var str="<td><b></b></td>"+
				"<td><input type='text' class='unAllowUpdate' name='productCode' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
				"<td><input type='text' class='unAllowUpdate' name='productName' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
				"<td><input type='text' class='unAllowUpdate' name='skuCode' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
				"<td><input type='text' class='unAllowUpdate' name='skuName' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
				"<td><input type='text' class='unAllowUpdate' name='skuOid' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'>" +
				"<input type='hidden' name='type' value='0'>" +
				"<input type='hidden' name='productType' value=''></td>"+
				"<td><input type='text' class='unAllowUpdate' name='unitName' style='width: 50px;border: 0' readonly='readonly'>"+
               	"<input type='hidden' name='unitId' ></td>"+
				"<td><input type='number' min='0' class='unAllowUpdate' onchange='sumNumber();' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+
				"<td><input type='text' class='allowUpdate' name='productStatus' placeholder='输入产品状态' size='8' style='border: 0'></td>"+
				"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0'></td>"+ 
				"<td><input type='text' name='saleDate' id='saleDate"+saleDateNumber+"' lay-verify='date' placeholder='开始日期 - 结束日期' class='allowUpdate' style='width: 190px'>" +
					"<input type='hidden' name='saleStartDate' value=''>" +
					"<input type='hidden' name='saleEndDate' value=''>" +
				"</td>"+
	            "<td><input type='number' class='allowUpdate' value='0' min='0' class='text-center' style='width: 50px;border: 0' readonly='readonly'></td>"+
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0' onkeyup='formulaCalculator1(this);'></td>"+ 
				"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0' onkeyup='formulaCalculator1(this);'></td>"+ 
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0' onkeyup='formulaCalculator1(this);'></td>"+ 
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0' onkeyup='formulaCalculator2(this);'></td>"+ 
				"<td><input type='number' min='0' class='unAllowUpdate' style='width: 50px;border: 0' value='0' readonly='readonly'></td>"+ 
				"<td><textarea placeholder='备注内容'></textarea></td>";
  var tbl=$('.purchasePlan tr:eq(-2)');
  addRow(str,tbl);
  loadDateScope("saleDate"+saleDateNumber);
  $("#saleDateNumber").val(saleDateNumber);
});
// 采购计划-提交订单
$('#planSubmit').click(function(){
  layer.open({
    type:1,
    title:'选择订单审批人',
    area:['565px', 'auto'],
    skin:'popGreen',
    closeBtn:1,
    content:$('.approverChoice'),
    btn:['确定','关闭'],
    yes:function(index){
      layer.close(index);
      location.href='我的采购计划-提交申请成功.html';
    }
  });
});
// 删除附件
// $('#planEnclosure').on('click','span b',function(){
// layer.confirm('您确定要删除此附件吗？</p>',{
// icon:3,
// skin:'pop',
// title:'提醒',
// closeBtn:2
// },function(index){
// layer.close(index);
// $(this).parent().remove();
// }.bind(this));
// });
// 采购计划列表页面
$('.purPlanList').on('click','.planApproval', function () {
  layer.open({
    type:1,
    title:'编号：<span>21656563</span> 的采购清单审批',
    area:['1000px', 'auto'],
    skin:'popGreen',
    closeBtn:1,
    content:$('.plain_frame'),
    btn:['确定','关闭']
  });
});
// 详情页
$('#currentApproval').click(function(){
  layer.open({
    type:1,
    title:'编号：<span>21656563</span> 的采购清单审批',
    area:['380px', 'auto'],
    skin:'popGreen',
    closeBtn:1,
    content:$('.plain_frame'),
    btn:['关闭']
  });
});
$('#purchaseSubmit').click(function(){
  layer.confirm('您确定要将所选商品批量生成采购订单吗？</p>',{
    icon:3,
    skin:'popBuyer btnCenter',
    title:'提醒',
    closeBtn:0
  },function(index){
    layer.close(index);
    location.href='我的采购计划-生成订单.html';
  });
});
/*$('.planList').on('click','tbody td:first-child b',function(){
  layer.confirm('您确定要删除此商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});*/
