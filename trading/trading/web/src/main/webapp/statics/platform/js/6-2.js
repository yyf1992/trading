/**
 * Created by Administrator on 2017/4/17.
 */
//功能点1：复选框选择事件
$('.pro_list input').click(function(){
  console.log(1);
  $(this).parent().toggleClass('checked');
});
//功能点2：分页
function showPages(page, total) {
  var str = '<a class="current">' + page + '</a>';
  for (var i = 1; i <= 3; i++) {
    if (page - i > 1) {
      str = '<a href="#">' + (page - i) + '</a> ' + str;
    }
    if (page + i < total) {
      str = str + ' <a href="#">' + (page + i)+'</a>';
    }
  }
  if (page - 4 > 1) {
    str = '... ' + str;
  }
  if (page > 1) {
    str = '<a href="#">&lt;上一页</a> <a href="#">1</a>' + ' ' + str;
  }
  if (page + 4 < total) {
    str = str + ' ...';
  }
  if (page < total) {
    str = str + ' <a href="#">' + total + '</a> <a href="#">下一页&gt;</a>';
  }
  str+=' 共'+total+'页&nbsp;&nbsp;转到 <input> 页&nbsp;&nbsp;<button>确定</button>';
  return str;
}
var total = 110;
for (var i = 1; i <= total; i++) {
  var ret = showPages(10, total);
  console.log(ret);
  $('.pager').html(ret);
}
//功能点3：查看规格价格
$('.pro_list td:nth-child(6) a').click(function(e){
  e.preventDefault();
  //$(this).next().removeClass('hide');
  $('.price').slideDown();
});
$('.price span').click(function () {
  //$(this).parent().addClass('hide');
  $('.price').slideUp();

});
//功能点4：下拉列表改颜色
//function changeColor(){
//  var option=$('.category select option:first');
//  var str=option.html();
//  if(str='请选择类别'){
//    option.addClass('gray')
//  }else{
//    option.removeClass('gray');
//  }
//}
//changeColor();