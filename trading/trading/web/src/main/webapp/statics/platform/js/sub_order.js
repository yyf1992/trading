/**
 * Created by Administrator on 2017/5/17.
 */
//功能点1:左侧导航条伸缩
$('.lf_nav1>div>h4').click(function(){
  $(this).next().slideToggle(300,'linear');
  $(this).find('span').toggleClass('fa-caret-up');
});
//功能点2:切换供应商窗口
$('.switch>span').click(function(){
  $('.switch>span.open').toggle();
  $('.switch>span.close').toggle();
  $('#side_pro').toggleClass('show');
});
//功能点3:地区选择
function selArea(){
  var b = AreaSelector({
    selProvinceId:'selProvince2',
    selCityId:'selCity2',
    selAreaId:'selArea2',
    areaOptionText:'县\区',
    onProvinceChange:function(){
    },
    onCityChange:function(){
    }
  });
}
selArea();
//功能点4：不需要物流，自提货物
$('.no_logistics input').click(function(){
  $(this).parent().toggleClass('checked');
});
//向供应商提交订单页面
//功能点4：采购清单求和
function sum(){
  var sum=0;
  var total=$('.commodities_list .sum');
  $('.commodities_list tr:not(:first-child) td:last-child').each(function(){
    var p=$(this).html();
    sum+=parseInt(p);
  });
  total.html(sum);
}
sum();
//手工录入采购订单页面
//function manual_num(){
//  var sum=0;
//  var total=$('.commodities_list .manual_num');
//  $('.commodities_list tr:not(:first-child) td:last-child').each(function(){
//    var p=$(this).html();
//    sum+=parseInt(p);
//  });
//  total.html(sum);
//}
//manual_num();