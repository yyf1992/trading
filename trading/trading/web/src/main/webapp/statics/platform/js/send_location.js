/**
 * Created by Administrator on 2017/5/8.
 */
    // 弹框
//  $(".layui-layer").css("position", "fixed");
//$('.layui-layer').each(function () {
//  var THIS = $(this);
//  THIS.css({'top': ($(window).height() - THIS.height()) / 2});
//})
//$('.sendMap_tan').hide();
//$('.siji_dx2').click(function () {
//  layer.open({
//    type: 1,
//    title: false,
//    closeBtn: 0,
//    area: ['740px', 'auto'],
//    shade: [0.5, '#000', false],
//    skin: 'layui-layer-nobg', //没有背景色
//    shadeClose: false,
//    closeBtn:1,
//    content: $(".sendMap_tan")
//  });
//  return false;
//})
$('.info_send').click(function(){
  $('.map_bg').fadeOut();
  layui.use(['layer'], function(){
    var layer = layui.layer;
    layer.open({
      type: 1,
      title: '司机手机号码',
      area: ['320px', '280px'],
      shade: 0.3,
      skin: 'phone layui-layer-molv', //没有背景色
      shadeClose: true,
      closeBtn:1,
      content: $('.send_sms')
    })
  });
});
// 发送回单
//var huidanT;
//$('.siji_dx1').click(function () {
//  huidanT = layer.open({
//    type: 1,
//    title: false,
//    closeBtn: 0,
//    area: ['410px', 'auto'],
//    shade: [0.5, '#000', false],
//    skin: 'layui-layer-nobg', //没有背景色
//    shadeClose: false,
//    closeBtn:1,
//    content: $(".huidanTan")
//  });
//  return false;
//})
//$('.hd_sub').click(function () {
//  layer.msg('<strong>发送成功!</strong>', {closeBtn: 1, time: 2000, icon: 1, shade: [0.4, '#000', true]});
//  layer.close(huidanT);
//})
//$('.hd_cal').click(function () {
//  layer.close(huidanT);
//})

// 发送地图
//var send_P;
//$('.info_send').click(function () {
//  send_P = layer.open({
//    type: 1,
//    title: false,
//    closeBtn: 0,
//    area: ['320px', 'auto'],
//    shade: [0.5, '#000', false],
//    skin: 'layui-layer-nobg', //没有背景色
//    shadeClose: false,
//    closeBtn:1,
//    content: $(".ditu_sendP"),
//    end: function () {
//      location.reload();
//    }
//  });
//发送短信
//点击发送地图，发送短信
//$('.info_send').click(function(){
//  $('.sms_bg').fadeIn();
//});
$('.send_sure').click(function () {
  var tel = $('.group input[name="tel"]').val();
  var re_tel = $('.group input[name="re_tel"]').val();
  if (tel == re_tel) {
    if(tel==""){
      layui.use(['layer'], function(){
        var layer = layui.layer;
        layer.alert('请输入手机号码', {
          icon: 0,
          skin: 'layer-ext-moon',
        })
      });
    }else{
      layui.use(['layer'], function(){
        var layer = layui.layer;
        layer.msg('发送成功！', {icon: 1});
      });
    }
  }else {
    $('.send_error').html('两次号码输入不一致');
  }
});
  $('.send_cancel').click(function(){
    $('.phone,.layui-layer-shade').fadeOut();
  });
//关闭输入提示框
$('.enter>h4>span').click(
  function(){
    $('.enter_bg').fadeOut();}
);
$('.enter_sure>button').click(
  function(){
    $('.enter_bg').fadeOut();}
);
// 百度地图API功能
var map = new BMap.Map("allmap");
var point = new BMap.Point(116.400244,39.926445);
map.centerAndZoom(point, 12);
map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
map.addControl(top_left_control);
map.addControl(top_left_navigation);
var overView = new BMap.OverviewMapControl();
var overViewOpen = new BMap.OverviewMapControl({isOpen:true, anchor: BMAP_ANCHOR_BOTTOM_RIGHT});
map.addControl(overView);          //添加默认缩略地图控件
map.addControl(overViewOpen);      //右下角，打开
var geoc = new BMap.Geocoder();
var marker = new BMap.Marker(point);// 创建标注
map.addOverlay(marker);             // 将标注添加到地图中
marker.enableDragging();           // 可拖拽
var label = new BMap.Label("我的标记",{offset:new BMap.Size(20,-10)});
marker.setLabel(label);
marker.addEventListener("dragend", function(e){  //拖动事件
  var pt = e.point;
  console.log(pt);
  var dizhi;
  geoc.getLocation(pt, function(rs){
    var addComp = rs.addressComponents;
    dizhi = addComp.city + addComp.district + addComp.street + addComp.streetNumber;
    document.getElementById('suggestId').value = dizhi;//更新地址数据
    var content = dizhi + "<br/><br/>经度：" + e.point.lng + "<br/>纬度：" + e.point.lat;
    var infoWindow = new BMap.InfoWindow("<p style='font-size:14px;'>" + content + "</p>");
    marker.openInfoWindow(infoWindow,map.getCenter());//将经纬度信息显示在提示框内
  });
  document.getElementById("zuobiao").value = e.point.lng + ", " + e.point.lat;//打印拖动结束坐标
});
$('.info_sec').click(function(){
  var str=$('#cityName').val();
  geoc.getPoint(str, function(point){
    if (point) {
      map.clearOverlays();
      map.centerAndZoom(point, 16);
      var marker=new BMap.Marker(point);
      map.addOverlay(marker);
      marker.enableDragging();           // 可拖拽
      var label = new BMap.Label("我的标记",{offset:new BMap.Size(20,-10)});
      marker.setLabel(label);
      marker.addEventListener("dragend", function(e){  //拖动事件
        var pt = e.point;
        console.log(pt);
        var dizhi;
        geoc.getLocation(pt, function(rs){
          var addComp = rs.addressComponents;
          dizhi = addComp.city + addComp.district + addComp.street + addComp.streetNumber;
          document.getElementById('suggestId').value = dizhi;//更新地址数据
          var content = dizhi + "<br/><br/>经度：" + e.point.lng + "<br/>纬度：" + e.point.lat;
          var infoWindow = new BMap.InfoWindow("<p style='font-size:14px;'>" + content + "</p>");
          marker.openInfoWindow(infoWindow,map.getCenter());//将经纬度信息显示在提示框内
        });
        document.getElementById("zuobiao").value = e.point.lng + ", " + e.point.lat;//打印拖动结束坐标
      });
    }else{
      alert("您选择地址没有解析到结果!");
    }
  }, "北京市");
});
