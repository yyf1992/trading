layui.use(['layer','form','laydate'], function() {
//layui.use(['layer','form'], function() {
  var layer = layui.layer, form = layui.form, laydate = layui.laydate;
  //var layer = layui.layer, form = layui.form;
  //利息明细弹框
  $('.bill_list tbody td:nth-child(4)').click(function(){
    layer.open({
      type:1,
      title:'<span style="color:#FF7519">温州市奥妙鞋业有限公司</span> 的利息明细',
      area:['420px','350px'],
      skin:'interest_top',
      closeBtn:1,
      content:$('.interest'),
      btn:['确定','取消'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  //账单结算周期审批
  $('.bill_approval').click(function(){
    layer.open({
      type:1,
      title:'账单审批',
      area:['420px','auto'],
      skin:'bill_title',
      closeBtn:1,
      content:$('.bill_exam'),
      btn:['确定','取消'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  //账单对账审批
  $('.bill_rec').click(function(){
    layer.open({
      type:1,
      title:'账单审批',
      area:['420px','auto'],
      skin:'bill_title',
      closeBtn:1,
      content:$('.bill_exam'),
      btn:['提交','取消'],
      yes:function(index){
        layer.close(index);
      }
    });
  });
  //打印对账单
  $('#bill_print').click(function(){
    layer.open({
      type:1,
      title:0,
      area:['1000px','auto'],
      closeBtn:2,
      content:$('.bill_content')
    })
  });
  //核实票据
  $('.bill_verify').click(function(){
    layer.open({
      type:1,
      title:'票据核实意见',
      area:['420px','auto'],
      skin:'bill_title',
      closeBtn:1,
      content:$('.bill_exam'),
      btn:['提交','取消'],
      yes:function(index){
        layer.close(index);
      }
    })
  });
  //查看商品明细
  $('.bill_commodity').click(function(){
    layer.open({
      type:1,
      title:'结算单号：<span>255165656525</span> 的商品明细',
      area:['900px','auto'],
      skin:'bill_title',
      closeBtn:1,
      content:$('.bill_inventory'),
      btn:['关闭'],
      yes:function(index){
        layer.close(index);
      }
    })
  });
  //查看票据
  $('.bill_view').click(function(){
    layer.open({
      type:1,
      title:'结算单号：<span>255165656525</span> 的票据',
      area:['700px','auto'],
      skin:'bill_title',
      closeBtn:1,
      content:$('.bill_invoice'),
    })
  });
  
});
/*//周期设置选择日期
$('#bill_present').focus(function(){
  $('.tab_ul').css({
    display:'block',
    left:136
  });
  $('.tab_ul>li').click(function(){
    var str=$(this).html();
    $('#bill_present').val(str);
    $('.tab_ul').hide();
    $('.tab_ul>li').unbind('click');
  })
});
  $('#bill_next').focus(function(){
    $('.tab_ul').css({
      display:'block',
      left:281
    });
    $('.tab_ul>li').click(function(){
      var str=$(this).html();
      $('#bill_next').val(str);
      $('.tab_ul').hide();
      $('#out_account').val(str).prop('disabled','true').css('cursor','not-allowed');
      $('.tab_ul>li').unbind('click');
    })
  });

//账额利息设置增加一行
$('#interest_add').click(function(){
  var str='<td><input type="number" name="overdueDate" value="0"></td><td><input type="number" name="overdueInterest" value="0"></td><td><select name="interestMethod"><option value="">请选择</option><option value="1">单利</option><option value="2">复利</option></select></td><td><button class="layui-btn layui-btn-mini layui-btn-normal"><i class="layui-icon"></i> 删除</button></td>';
  //var tbl=$('.bill_interest>table tr:eq(-1)');
  var tbl=$('#interestBody');
  addOneRow(str,tbl);
});

//表格增加和删除一行函数
function addOneRow(str,tbl){
    var addTr=document.createElement('tr');
    addTr.innerHTML=str;
    tbl.append(addTr);
}
//删除一行
//$('.bill_interest>table').on('click','button',function(){
$('#interestBody').on('click','button',function(){
  layer.confirm('您确定要删除该项吗？', {
    icon:3,
    skin:'pop',
    title:'提示',
    closeBtn:2,
    btn: ['确定','取消']
  }, function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});*/