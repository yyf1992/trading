var form;
//触发事件
var tab = {
	tabAdd: function(title,url,id){
		layer.load();
		//新增一个Tab项
		element.tabAdd('xbs_tab', {
			title: title 
			,content: '<iframe tab-id="'+id+'" frameborder="0" src="'+url+'" scrolling="yes" class="x-iframe"></iframe>'
			,id: id
		})
	}
	,tabDelete: function(othis){
		//删除指定Tab项
		element.tabDelete('xbs_tab', '44');
		othis.addClass('layui-btn-disabled');
	}
	,tabChange: function(id){
		//切换到指定Tab项
		element.tabChange('xbs_tab', id);
	}
};
$(function () {
	initializeLayui();
    //加载弹出层
    tableCheck = {
        init:function  () {
            $(".layui-form-checkbox").click(function(event) {
                if($(this).hasClass('layui-form-checked')){
                    $(this).removeClass('layui-form-checked');
                    if($(this).hasClass('header')){
                        $(".layui-form-checkbox").removeClass('layui-form-checked');
                    }
                }else{
                    $(this).addClass('layui-form-checked');
                    if($(this).hasClass('header')){
                        $(".layui-form-checkbox").addClass('layui-form-checked');
                    }
                }
                
            });
        },
        getData:function  () {
            var obj = $(".layui-form-checked").not('.header');
            var arr=[];
            obj.each(function(index, el) {
                arr.push(obj.eq(index).attr('data-id'));
            });
            return arr;
        }
    }
    //开启表格多选
    tableCheck.init();
    $('.container .left_open i').click(function(event) {
        if($('.left-nav').css('left')=='0px'){
            $('.left-nav').animate({left: '-221px'}, 100);
            $('.page-content').animate({left: '0px'}, 100);
            $('.page-content-bg').hide();
        }else{
            $('.left-nav').animate({left: '0px'}, 100);
            $('.page-content').animate({left: '221px'}, 100);
            if($(window).width()<768){
                $('.page-content-bg').show();
            }
        }

    });

    $('.page-content-bg').click(function(event) {
        $('.left-nav').animate({left: '-221px'}, 100);
        $('.page-content').animate({left: '0px'}, 100);
        $(this).hide();
    });

    $('.layui-tab-close').click(function(event) {
        $('.layui-tab-title li').eq(0).find('i').remove();
    });
});
function initializeLayui(){
	layui.use(['form','element'],
    function() {
        layer = layui.layer,form = layui.form;
        element = layui.element;
    });
	form = layui.form
}

/*弹出层*/
/*
    参数解释：
    title   标题
    url     请求的url
    id      需要操作的数据id
    w       弹出层宽度（缺省调默认值）
    h       弹出层高度（缺省调默认值）
*/
function x_admin_show(title,url,w,h){
    if (title == null || title == '') {
        title=false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '') {
        w=($(window).width()*0.9);
    };
    if (h == null || h == '') {
        h=($(window).height() - 50);
    };
    layer.open({
        type: 2,
        area: [w+'px', h +'px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade:0.4,
        title: title,
        content: url
    });
}
/*关闭弹出框口*/
function x_admin_close(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}
function setSystemTime(){
	// 显示页面时间
	var today = new Date();//定义日期对象   
	var yyyy = today.getFullYear();//通过日期对象的getFullYear()方法返回年    
	var MM = today.getMonth() + 1;//通过日期对象的getMonth()方法返回年    
	var dd = today.getDate();//通过日期对象的getDate()方法返回年     
	var hh = today.getHours();//通过日期对象的getHours方法返回小时   
	var mm = today.getMinutes();//通过日期对象的getMinutes方法返回分钟   
	var ss = today.getSeconds();//通过日期对象的getSeconds方法返回秒   
	// 如果分钟或小时的值小于10，则在其值前加0，比如如果时间是下午3点20分9秒的话，则显示15：20：09   
	MM = checkTime(MM);
	dd = checkTime(dd);
	mm = checkTime(mm);
	ss = checkTime(ss);
	var day; //用于保存星期（getDay()方法得到星期编号）
	if (today.getDay() == 0)
		day = "星期日 "
	if (today.getDay() == 1)
		day = "星期一 "
	if (today.getDay() == 2)
		day = "星期二 "
	if (today.getDay() == 3)
		day = "星期三 "
	if (today.getDay() == 4)
		day = "星期四 "
	if (today.getDay() == 5)
		day = "星期五 "
	if (today.getDay() == 6)
		day = "星期六 "
	document.getElementById('nowDateTimeSpan').innerHTML = yyyy + "年" + MM + "月" + dd + "日 " + day;
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}