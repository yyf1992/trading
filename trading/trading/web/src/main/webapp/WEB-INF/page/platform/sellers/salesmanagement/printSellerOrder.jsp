<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/common.jsp"%>
<script src="${staticsPath}/platform/js/jquery-1.4.4.min.js"></script>
<script src="${staticsPath}/platform/js/jquery.jqprint-0.3.js"></script>
<script type="text/javascript">
	$(function() {
		// 打印
		$("#print_sure").click(function() {
			$("#print_yc").jqprint();
		});
		// 汇总优惠价格
		var sum = "";
		$("#printOrderTbody tr").each(function(){
			var preferentialPrice = $(this).find("td:eq(7)").text();
			sum = Number(sum) + Number(preferentialPrice);
		});
		$("#sumPreferentialPrice").text(sum);
	});
</script>
<body>
	<div class="print_t">
		<div>
			<img src="<%=basePath%>statics/platform/images/reg_logo.jpg">
		</div>
	</div>
	<!--主体-->
	<div class="print_m">
		<div id="print_yc">
			<h3 class="print_title">买卖订单明细</h3>
			<table class="table_n">
				<tr>
					<td style="width:50%">订单号：<span>${order.orderCode}</span>
					</td>
					<td style="width:20%">日期：<span><fmt:formatDate value="${order.createDate}" type="both"/></span>
					</td>
					<td style="width:30%"></td>
				</tr>
			</table>
			<table class="table_n">
				<tr>
					<td style="width:50%">采购商名称：<span>${order.suppName}</span>
					</td>
					<td style="width:20%">负责人：<span>${order.personName}</span>
					</td>
					<td style="width:30%" class="text-right">手机号：<span>${order.receiptPhone}</span>
					</td>
				</tr>
				<tr>
					<td colspan="3">收货地址：${order.personName},${order.receiptPhone},${order.addrName}
					</td>
				</tr>
			</table>
			<table class="table_n">
				<tr>
					<td style="width:50%">供应商名称：<span>${order.companyName}</span>
					</td>
					<td style="width:20%">负责人：<span>${order.person}</span>
					</td>
					<td style="width:30%" class="text-right">手机号：<span>${order.phone}</span>
					</td>
				</tr>
			</table>
			<table class="layui-table table_c">
				<thead>
					<tr>
						<td style="width:20%">商品</td>
						<td style="width:10%">条形码</td>
						<!-- <td style="width:10%">对方商品条形码</td> -->
						<td style="width:5%">单位</td>
						<td style="width:8%">仓库</td>
						<td style="width:15%">备注</td>
						<td style="width:6%">单价</td>
						<td style="width:6%">数量</td>
						<td style="width:10%">优惠金额</td>
						<td style="width:10%">商品总价</td>
					</tr>
				</thead>
				<tbody id="printOrderTbody">
					<c:forEach var="product" items="${order.supplierProductList}">
						<tr>
							<td class="tl">${product.productName}</td>
							<td>${product.barcode}</td>
							<!-- <td>15646453464</td> -->
							<td>${el:getUnitById(product.unitId).unitName}</td>
							<td>大仓</td>
							<td class="tl">${product.remark}</td>
							<td>${product.price}</td>
							<td>${product.orderNum}</td>
							<td>${product.discountMoney}</td>
							<td>${product.totalMoney}</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6" class="">合计</td>
						<td class="table_fw">${order.goodsNum}</td>
						<td class="table_fw"><label id="sumPreferentialPrice"></label></td>
						<td class="table_fw">${order.goodsPrice}</td>
					</tr>
				</tfoot>
			</table>
			<table class="table_n table_b mt">
				<tr>
					<td style="width:30%">制单人：<span>${sessionScope.CURRENT_USER.userName}</span></td>
					<td style="width:30%">财务：<span><!-- 李XX --></span></td>
					<td style="width:30%">仓库：<span><!-- 济南仓 --></span></td>
					<td style="width:10%" class="text-right">收货人：<span>${order.personName}</span></td>
				</tr>
			</table>
		</div>
		<div class="print_b">
			<span id="print_sure">确定打印</span>
		</div>
	</div>
</body>