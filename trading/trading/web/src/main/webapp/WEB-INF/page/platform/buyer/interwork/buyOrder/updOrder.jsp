<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	//用户封装删除的明细
	var delArray = new Array();
    /**
	 * 删除订单明细
     */
    function delItem(obj) {
        layer.confirm('确定删除该行？', {
            icon:3,
            title:'提醒',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            var delItemId =  $(obj).parent().parent().find("input[name='itemId']").val();
            delArray.push(delItemId);
            $(obj).parent().parent().remove();
            layer.close(index);
        }.bind(obj));
    }
    /**
	 * 订单数量修改后计算总价
	 */
    function calMoney(obj,oriNum) {
        var newNum = $(obj).val();
		if(newNum<=0){
            $(obj).val(oriNum);
            newNum = oriNum;
		}
		var price = $(obj).parent().parent().find("td")[7].innerText;
		var totalMoney = (parseFloat(newNum)*parseFloat(price)).toFixed(4);
        $(obj).parent().parent().find("td")[8].innerText = totalMoney;
    }
    /**
	 * 保存修改后的订单
     */
    function saveUpdOrder() {
        /* var predictArred = $("#predictArred").val();
        if(predictArred==""||predictArred==null||predictArred==undefined){
            layer.msg("要求到货日期不能为空！",{icon:2});
            return;
		} */
        var itemLength = $("#tboby tr").length;
        if(itemLength<=0){
            layer.msg("订单明细不能为空！",{icon:2});
            return;
		}
		var updArray = new Array();
		var predictArredError = "";
        $("#tboby tr").each(function(i) {
            var itemId = $(this).find("input[name='itemId']").val();
            var goodsNumber = $(this).find("input[name='goodsNumber']").val();
            var price = $(this).find("input[name='itemPrice']").val();
            var remark = $(this).find("input[name='itemRemark']").val();
            var predictArred = $(this).find("input[name='predictArred']").val();
            if(predictArred==""||predictArred==undefined||predictArred==null){
				predictArredError = "第"+(i+1)+"行要求到货日期不能为空！";
				return false;
			}
            var o = new Object();
            o.id = itemId;
            o.goodsNumber = goodsNumber;
            o.price = price;
            o.remark = remark;
            o.predictArred = new Date(predictArred);
            updArray.push(o);
        });
        if(predictArredError != ''){
			layer.msg(predictArredError,{icon:2});
			return;
		}
        var url = basePath+"platform/buyer/buyOrder/updOrder";
        $.ajax({
            type : "POST",
            url : url,
            data: {
                "orderId": '${order.id}',
                //"predictArred": predictArred,
                "delArray": JSON.stringify(delArray),
                "updArray": JSON.stringify(updArray),
                "menuName":'17070718133683994604'
            },
            async:false,
            success:function(data){
                console.log(data);
                if(data.flag){
                    var res = data.res;
                    if(res.code==40000){
                        //调用成功
                        showSuccess(res.data);
                    }else if(res.code==40010){
                        //调用失败
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40011){
                        //需要设置审批流程
                        layer.msg(res.msg,{time:500,icon:2},function(){
                            setApprovalUser(url,res.data,function(data){
                                showSuccess(data);
                            });
                        });
                        return false;
                    }else if(res.code==40012){
                        //对应菜单必填
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40099){
                        //保存失败
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40013){
                        //不需要审批
                        notNeedApproval(res.data,function(data){
                            showSuccess(data);
                        });
                        return false;
                    }
                }else{
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                    return false;
                }
            },
            error:function(){
                layer.msg("提交失败，请稍后重试！",{icon:2});
            }
        });
    }
    /**
	 * 保存成功
     */
    function showSuccess() {
        layer.msg("保存成功！",{icon:1});
        leftMenuClick(this,'platform/buyer/buyOrder/buyOrderList','buyer','17070718432862058335');
    }
</script>
<div>
	<div class="order_d">
		<p class="line mt"></p>
		<p>
			<span class="order_title">供应商信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>供应商名称：<span>${order.suppName}</span>
				</td>
				<td>负责人：<span>${order.person}</span>
				</td>
				<td>手机号：<span>${order.phone}</span>
				</td>
			</tr>
		</table>
		<p class="line mt"></p>
		<p>
			<span class="order_title">订单信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>订单号：<span>${order.orderCode}</span></td>
				<td>下单日期：<span><fmt:formatDate value="${order.createDate}" type="both"/></span></td>
				<td>下单人：<span>${order.createName}</span></td>
			</tr>
			<%-- <tr>
				<td>
					要求到货日期：<input type="text" name="predictArred" id="predictArred" lay-verify="date" value="<fmt:formatDate value="${order.predictArred}" type="date"/>" placeholder="年/月/日" class="layui-input" style="width: 100px;display: inline-block;">
				</td>
			</tr> --%>
		</table>
		<%--<div class="c66 size_sm orderNote mt">备注：<input type="text" id="orderRemark" name="orderRemark" value="${order.remark}" style="width: 700px;"/></div>--%>
		<table class="table_pure detailed_list">
			<thead>
				<tr>
					<!-- <td style="width:10%">店铺</td> -->
					<td style="width:12%">采购计划编号</td>
					<td style="width:25%">商品</td>
					<td style="width:8%">条形码</td>
					<td style="width:5%">单位</td>
					<td style="width:10%">入仓</td>
					<td style="width:7%">是否含发票</td>
					<td style="width:10%">数量</td>
					<td style="width:7%">采购单价</td>
					<td style="width:7%">商品总额</td>
					<td style="width:10%">备注</td>
					<td style="width:9%">要求到货日期</td>
					<td style="width:8%">操作</td>
					<td style="display: none;">隐藏字段</td>
				</tr>
			</thead>
			<tbody id="tboby">
			<c:forEach var="item" items="${order.orderProductList}">
				<tr>
					<%-- <td>${item.shopName}</td> --%>
					<td>${item.applyCode}</td>
					<td>${item.proCode}|${item.proName }|${item.skuCode}</td>
					<td title="${item.skuOid}">${item.skuOid}</td>
					<td>${item.unitName}</td>
					<td>${item.wareHouseName}</td>
					<td>
						<c:if test="${item.isNeedInvoice=='Y'}">是</c:if>
						<c:if test="${item.isNeedInvoice=='N'}">否</c:if>
					</td>
					<td><input type="number" min="0" name="goodsNumber" value="${item.goodsNumber}" onchange="calMoney(this,${item.goodsNumber});" style="width:100%"/></td>
					<td>${item.price}</td>
					<td>${item.priceSum}</td>
					<td><input type="text" name="itemRemark" value="${item.remark}" style="width:100%"/></td>
					<td><input type="text" name="predictArred" lay-verify="date" placeholder="年/月/日" class="layui-input" style="width: 100px" value="<fmt:formatDate value='${item.predictArred}' type='date'/>"></td>
					<td><a href="javascript:void(0);" onclick="delItem(this);"><i class="layui-icon">&#xe7ea;</i>删除</a></td>
					<td>
						<input type="hidden" name="itemId" value="${item.id}">
						<input type="hidden" name="itemPrice" value="${item.price}">
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="text-right" style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/buyOrder/showUpdOrder?id=${order.id}','buyer','17112115053610102016');"><span class="contractBuild">撤销修改</span></a>&nbsp;&nbsp;
		<a href="javascript:void(0)" onclick="saveUpdOrder('${order.id}');"><span class="contractBuild">保存</span></a>
	</div>
</div>