<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td style="width: 70%">
			<ul>
				<li style="width: 50%">商品</li>
				<li style="width: 10%">条形码</li>
				<li style="width: 10%">退/换货数量</li>
				<li style="width: 10%">oms出库数</li>
				<li style="width: 10%">卖家收货数</li>
				<li style="width: 10%">售后类型</li>
			</ul></td>
		<td style="width: 10%">申请原因</td>
		<td style="width: 10%">状态</td>
		<td style="width: 10%">操作</td>
	</tr>
</table>
<div class="orderList serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
		<div>
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> <span class="order_num"><span>售后单号:</span>
					${customer.customerCode}</span> <span class="order_name">${customer.supplierName}</span>
				<c:if test="${customer.arrivalType==1}">
					<span style="margin-left: 30px;">oms出库时间：<fmt:formatDate value="${customer.omsFinishdate}" type="both"/></span>
				</c:if>
			</p>
			<table>
				<tr>
					<td style="width: 70%"><c:forEach var="customerItem" items="${customer.itemList}">
							<ul class="clear">
								<li style="width: 50%"><span class="defaultImg"></span>
									<div>
										${customerItem.productCode}|${customerItem.productName} <br>
										<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
											${customerItem.skuName}</span>
									</div></li>
								<li style="width: 10%">${customerItem.skuOid}</li>
								<%--<li>${el:getUnitById(customerItem.unitId).unitName}</li>--%>
								<li style="width: 10%">${customerItem.goodsNumber}</li>
								<li style="width: 10%">${customerItem.receiveNumber}</li>
								<li style="width: 10%">${customerItem.confirmNumber}</li>
								<li style="width: 10%">
									<c:choose>
										<c:when test="${customerItem.type==0}">
											<c:choose>
												<c:when test="${customerItem.updateTypeFlag}">
													<span>换货中</span>
													<i class="layui-icon" style="font-size:1px;cursor:pointer;" 
														onclick="showTips('${customerItem.confirmNumber-customerItem.deliveryNum}','${customerItem.deliveryNum}');">&#xe725;</i>
													<a href="javascript:void(0)" onclick="conversionType('${customerItem.id}','${customer.createId}');" class="layui-btn layui-btn-normal layui-btn-mini">
														<i class="layui-icon">&#xe669;</i>转为退货</a>
												</c:when>
												<c:otherwise>换货结束</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${customerItem.type==1}">退货</c:when>
										<c:when test="${customerItem.type==2}">转货转退货</c:when>
									</c:choose>
								</li>
							</ul>
						</c:forEach>
					</td>
					<td style="width: 10%">${customer.reason}</td>
					<td style="width: 10%">
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待内部审批</c:when>
								<c:when test="${customer.status==1}">待卖家同意</c:when>
								<c:when test="${customer.status==2}">已驳回</c:when>
								<c:when test="${customer.status==3}">已取消</c:when>
								<c:when test="${customer.status==4}">卖家已收货</c:when>
								<c:when test="${customer.status==5}">待卖家收货</c:when>
							</c:choose>
						</div>
						<div>
							<c:choose>
								<c:when test="${customer.arrivalType==1}">oms已出库</c:when>
								<c:otherwise>oms未出库</c:otherwise>
							</c:choose>
						</div>
						<div class="opinion_view">
							<span class="orange" data="${customer.id}">查看审批流程</span>
						</div>
					</td>
					<td style="width: 10%">
						<a href="javascript:void(0)" button="详情"onclick="leftMenuClick(this,'platform/buyer/customer/loadCustomerDetails?id=${customer.id}'+'&returnUrl=platform/buyer/customer/customerList&menuId=17091216212559741744','buyer','17112215171918654466')"class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe695;</i>详情</a>
						<c:if test="${customer.status == 4 && customer.pushType == 0}">
							<!-- 卖家同意售后 -->
							<a href="javascript:void(0)" button="推送oms出库" onclick="pushOms('${customer.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
								推送oms出库</a>
						</c:if>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>