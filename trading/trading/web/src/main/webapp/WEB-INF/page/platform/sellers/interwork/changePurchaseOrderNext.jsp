<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<script>
$(function(){
	//切换收货地址
	$('.addr>div:not(#addr_new)').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	//计算总数量和总价格
	$("#nextOrder div[name='itemDiv']").each(function(){
		var supplierId = $(this).find("input").val();
		sumNumAndPrice(supplierId);
	});
	layuiData();
});
	function showAddr(supplierId) {
		if ($("#is_since_" + supplierId).is(":checked")) {
			$("#address_" + supplierId).hide();
			$("#addressAdd_" + supplierId).hide();
		} else {
			$("#address_" + supplierId).show();
			$("#addressAdd_" + supplierId).show();
		}
	}
	// 根据supplierId汇总数量和价格
	function sumNumAndPrice(supplierId) {
		var sumNum = "";
		var sumPrice = "";
		$("#shopTbody_" + supplierId + " tr").each(function() {
			var num = $(this).find("td:eq(5)").text();
			var price = $(this).find("td:eq(6)").find("span").html();
			var priceN = accMul(num,price);
			$(this).find("td:eq(7)").text(priceN);
			sumNum = Number(sumNum) + Number(num);
			var sPrice = $(this).find("td:eq(7)").text();
			sumPrice = Number(sumPrice) + Number(sPrice);
		});
		$("#shopTfootNum_" + supplierId).text(sumNum);
		$("#shopTfootPrice_" + supplierId).text(sumPrice);
	}
	//精确的乘法结果
	function accMul(arg1,arg2){
		var m=0,s1=arg1.toString(),s2=arg2.toString();
		try{m+=s1.split(".")[1].length}catch(e){}
		try{m+=s2.split(".")[1].length}catch(e){}
		return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
	}
	function saveOrder() {
		var infoStr = "";
		//收货地址判空
		var is_since = "1";
		if(!$("input[name='is_since']").is(":checked")){
			is_since = "0";
			if($('.addr>div.selected').length!=1){
				layer.msg("请选择收货地址！",{icon:2});
				return;
			}
		}
		var predictArredError = "";
		$("#nextOrder div[name='itemDiv']").each(function() {
			var supplierId = $(this).find("input").val();
			var supplierName = $(this).find("input:eq(1)").val();
			var person = $(this).find("input:eq(2)").val();
			var phone = $(this).find("input:eq(3)").val();
			var goodsNum = $("#shopTfootNum_" + supplierId)
					.text();
			var goodsPrice = $("#shopTfootPrice_" + supplierId)
					.text();

			var addressId = $(".addr>div.selected input[name='addressId']")
					.val();
			var remark = $(this).find("textarea").val();
			var productItem = {};
			productItem["productList"] = getProductList(supplierId);
			infoStr = infoStr + supplierId + "," + supplierName
					+ "," + person + "," + phone + ","
					+ goodsNum + "," + goodsPrice + ","
					+ is_since + "," + addressId + "," + remark
					+ "," //+ predictArred + ","
					+ JSON.stringify(productItem) + "@";
		});
		infoStr = infoStr.substring(0, infoStr.length - 1);
		if(predictArredError != ''){
			layer.msg(predictArredError,{icon:2});
			return;
		}
		var url = "platform/buyer/buyOrder/saveChangePurchaseOrder";
		$.ajax({
			type : "post",
			url : url,
			async : false,
			data : {
				"buyOrderId" : $("#buyOrderId").val(),
				"infoStr" : infoStr,
				"menuName" : "17070718133683994604"
			},
			success : function(data) {
				if (data.flag) {
					var res = data.res;
					if (res.code == 40000) {
						//调用成功
						saveSucess();
					} else if (res.code == 40010) {
						//调用失败
						layer.msg(res.msg, {
							icon : 2
						});
						return false;
					} else if (res.code == 40011) {
						//需要设置审批流程
						layer.msg(res.msg, {
							time : 500,
							icon : 2
						}, function() {
							setApprovalUser(url, res.data, function(data) {
								saveSucess(data);
							});
						});
						return false;
					} else if (res.code == 40012) {
						//对应菜单必填
						layer.msg(res.msg, {
							icon : 2
						});
						return false;
					} else if (res.code == 40013) {
						//不需要审批
						notNeedApproval(res.data, function(data) {
							saveSucess(data);
						});
						return false;
					}
				} else {
					layer.msg("获取数据失败，请稍后重试！", {
						icon : 2
					});
					return false;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {
					icon : 2
				});
			}
		});
	}

	function getProductList(supplierId) {
		var productList = new Array();
		$("#shopTbody_" + supplierId + " tr").each(
				function() {
					var product = {};
					var productCode = $(this).find("td:eq(0)").find(
							"input:eq(0)").val();
					var productName = $(this).find("td:eq(0)").find(
							"input:eq(1)").val();
					var skuCode = $(this).find("td:eq(0)").find("input:eq(2)")
							.val();
					var skuName = $(this).find("td:eq(0)").find("input:eq(3)")
							.val();
					var skuOid = $(this).find("td:eq(0)").find("input:eq(4)")
							.val();
					var unitId = $(this).find("td:eq(1)").find("input").val();
					var warehouseId = $(this).find("td:eq(2)").find("input").val();
					var predictArred = $(this).find("td:eq(3)").text();
					var remark = $(this).find("td:eq(4)").text();
					var num = $(this).find("td:eq(5)").text();
					var price = $(this).find("td:eq(6)").find("span").html();
					var priceSum = $(this).find("td:eq(7)").text();
					product["productCode"] = productCode;
					product["productName"] = productName;
					product["skuCode"] = skuCode;
					product["skuName"] = skuName;
					product["skuOid"] = skuOid;
					product["unitId"] = unitId;
					product["warehouseId"] = warehouseId;
					product["predictArred"] = predictArred;
					product["remark"] = remark;
					product["num"] = num;
					product["price"] = price;
					product["priceSum"] = priceSum;
					productList.push(product);
				});
		return productList;
	}
	//保存成功
	function saveSucess(ids) {
		layer.msg("保存成功！", {icon : 1});
		leftMenuClick(this, "platform/seller/interworkGoods/interworkGoodsList.html","sellers","17070718471195397330");
	}
	//修改地址
	function updateAddress(obj) {
		var parentDiv = $(obj).parent().parent();
		$.ajax({
			type : "POST",
			url : "platform/address/updateAddress",
			data : {
				"id" : parentDiv.find("input[name='addressId']").val()
			},
			success : function(data) {
				var str = data.toString();
				layer.open({
					type : 1,
					title : '修改收货地址',
					skin : 'pop',
					closeBtn : 2,
					area : [ '770px', 'auto' ],
					btn : [ '保存', '关闭' ],
					content : str,
					yes : function(index, layero) {
						var isDefault = 1;
						if ($("#updateIsDefault").is(":checked")) {//选中  
							isDefault = 0
						}
						// 确定保存
						$.ajax({
							type : "POST",
							url : "platform/address/saveUpdateAddress",
							async : false,
							data : {
								"id" : $("#updateId").val(),
								"personName" : $("#updatePersonName").val(),
								"province" : $("#updateProvince").val(),
								"city" : $("#updateCity").val(),
								"area" : $("#updateArea").val(),
								"addrName" : $("#updateAddrName").val(),
								"phone" : $("#updatePhone").val(),
								"zone" : $("#updateZone").val(),
								"telNo" : $("#updateTelNo").val(),
								"isDefault" : isDefault
							},
							success : function(data) {
								var result = eval('(' + data + ')');
								var resultObj = isJSONObject(result) ? result
										: eval('(' + result + ')');
								if (resultObj.success) {
									layer.close(index);
									ResetAddress(resultObj.address, parentDiv);
								} else {
									layer.msg(resultObj.msg, {
										icon : 2
									});
									return false;
								}
							},
							error : function() {
								layer.msg("获取数据失败，请稍后重试！", {
									icon : 2
								});
							}
						});
					}
				});
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {
					icon : 2
				});
			}
		});
	}
	function addAddress() {
		$.ajax({
			type : "POST",
			url : "platform/address/addAddress",
			success : function(data) {
				layer.open({
					type : 1,
					title : '新增收货地址',
					skin : 'pop',
					closeBtn : 2,
					area : [ '770px', 'auto' ],
					btn : [ '保存', '关闭' ],
					content : data,
					yes : function(index, layero) {
						var isDefault = 1;
						if ($("#isDefault").is(":checked")) {//选中  
							isDefault = 0
						}
						// 确定保存
						$.ajax({
							type : "POST",
							url : "platform/address/saveAddAddress",
							async : false,
							data : {
								"personName" : $("#personName").val(),
								"province" : $("#province").val(),
								"city" : $("#city").val(),
								"area" : $("#area").val(),
								"addrName" : $("#addrName").val(),
								"phone" : $("#phone").val(),
								"zone" : $("#zone").val(),
								"telNo" : $("#telNo").val(),
								"isDefault" : isDefault
							},
							success : function(data) {
								var result = eval('(' + data + ')');
								var resultObj = isJSONObject(result) ? result
										: eval('(' + result + ')');
								if (resultObj.success) {
									layer.close(index);
									ResetAddress(resultObj.address, null);
								} else {
									layer.msg(resultObj.msg, {
										icon : 2
									});
									return false;
								}
							},
							error : function() {
								layer.msg("获取数据失败，请稍后重试！", {
									icon : 2
								});
							}
						});
					}
				});
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {
					icon : 2
				});
			}
		});
	}
	//重置收货地址
	function ResetAddress(resultObj, divObj) {
		var divStr = "";
		if (resultObj.isDefault == 0) {
			divStr += "<img src=\"<%=basePath%>statics/platform/images/default_small_b.jpg\" class=\"defaultAddressImg\">";
	}
	divStr += "<div class='borderBg'></div>";
	divStr += "<div class='addr_hd'>";
	divStr += "<span>"+resultObj.provinceName+"</span>";
	divStr += "<span><b>"+resultObj.cityName+"</b></span>";
	divStr += " （<span>"+resultObj.personName+"</span> 收）";
	divStr += "</div>";
	divStr += "<div class='addr_bd'>";
	divStr += "<span>"+resultObj.areaName+"</span>";
	divStr += " <span>"+resultObj.addrName+"</span>";
	divStr += " <span>"+resultObj.phone+"</span>";
	divStr += "</div>";
	divStr += "<div class='addr_md text-right'>";
	divStr += "<span class='addr_modify' onclick='updateAddress(this);'><i class='layui-icon'>&#xe691;</i>修改</span>";
	divStr += "</div>";
	divStr += "<input type='hidden' name='addressId' value='"+resultObj.id+"'>";
	if(divObj==null||divObj.length==0){
		var divId=new Date().getTime();
		divObj = $("<div class='addrItem '>",{id:divId});
		divObj.append(divStr);
		$(".addr>.addrItem:last").after(divObj);
		divObj.addClass('selected').siblings().removeClass('selected');
	}else{
		divObj.empty();
		divObj.append(divStr);
		divObj.addClass('selected').siblings().removeClass('selected');
	}
	$('.addr>div').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	if(resultObj.isDefault==0){
		$(".isDefault .defaultAddressImg").remove();
		$(".isDefault").removeClass("isDefault");
		divObj.addClass("isDefault");
	}
}
</script>
<style>
	.nextTr td{
	 word-wrap:break-word;
	 white-space:inherit;
	 word-break: break-all;
	}
</style>
<div id="nextOrder">
	<div class="progress_o">
		<span class="cart"><b></b> 我的购物车</span>
		<span class="order_pr current"><b></b> 填写核对订单信息</span>
		<span class="order_s"><b></b> 成功提交订单 </span>
	</div>
	<!--收货信息-->
	<h4 class="no_logistics">
		选择收货地址
		<span class="since"><input type="checkbox" id="is_since_${item.supplierId}" onchange="showAddr('${item.supplierId}')">不需要物流，自提货物</span>
	</h4>
	<div class="addr addr_buyer">
		<c:forEach var="address" items="${addressList}" varStatus="status">
			<div
				class="addrItem <c:if test='${address.isDefault==0}'>isDefault selected</c:if>"
				id="addr_${status.count}">
				<c:if test="${address.isDefault==0}">
					<img
						src="<%=basePath%>statics/platform/images/default_small_b.jpg"
						class="defaultAddressImg">
				</c:if>
				<div class="borderBg"></div>
				<div class="addr_hd">
					<span>${el:getProvinceById(address.province).province}</span> <span><b>${el:getCityById(address.city).city}</b>
					</span> （<span>${address.personName}</span> 收）
				</div>
				<div class="addr_bd">
					<span>${el:getAreaById(address.area).area}</span> <span>${address.addrName}</span>
					<span>${address.phone}</span>
				</div>
				<div class="addr_md text-right">
					<span class="addr_modify" onclick="updateAddress(this);"><i
						class="layui-icon">&#xe691;</i>修改</span>
				</div>
				<input type="hidden" name="addressId" value="${address.id}">
			</div>
		</c:forEach>
		<div class="addrAdd text-center" id="addr_new"
			onclick="addAddress();">
			<img src="<%=basePath%>statics/platform/images/addr_a.jpg">
			<p>
				<b>新增收货地址</b>
			</p>
		</div>
	</div>
	<c:forEach var="item" items="${nextData.itemArray}">
		<div name="itemDiv" style="background: #F8F8F8;border: 1px solid #EAEAEA;">
			<h4 class="page_title mt">
				供应商：${item.supplierName}
				<input type="hidden" name="supplierId" value="${item.supplierId}">
				<input type="hidden" name="supplierName" value="${item.supplierName}">
				<input type="hidden" name="person" value="${item.person}">
				<input type="hidden" name="phone" value="${item.phone}">
			</h4>
			<p class="saleList">
				<b>采购清单</b>
			</p>
			<table class="table_pure commodities_list mt">
				<thead>
					<tr>
						<td style="width:20%">商品</td>
						<td style="width:5%">单位</td>
						<td style="width:7%">仓库</td>
						<td style="width:10%">要求到货日期</td>
						<td style="width:10%">备注</td>
						<td style="width:6%">申请数量</td>
						<td style="width:7%">单价</td>
						<td style="width:7%">总价</td>
					</tr>
				</thead>
				<tbody id="shopTbody_${item.supplierId}">
					<c:forEach var="shop" items="${item.shopArray}">
						<tr class="nextTr">
							<td>${shop.productCode}|${shop.productName}|${shop.skuCode}|${shop.skuName}|${shop.skuOid}
								<input type="hidden" value="${shop.productCode}">
								<input type="hidden" value="${shop.productName}">
								<input type="hidden" value="${shop.skuCode}">
								<input type="hidden" value="${shop.skuName}">
								<input type="hidden" value="${shop.skuOid}">
							</td>
							<td>${el:getUnitById(shop.unitId).unitName}
								<input type="hidden" value="${shop.unitId}">
							</td>
							<td>${el:getBusinessWhareaById(shop.warehouseId).whareaName}
								<input type="hidden" value="${shop.warehouseId}">
							</td>
							<td>${shop.predictArred}</td>
							<td title="${shop.remark}">${shop.remark}</td>
							<td align="right">${shop.goodsNumber}</td>
							<td>
								<span>${shop.price}</span>
							<td></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">合计</td>
						<td class="text-center fw" id="shopTfootNum_${item.supplierId}"></td>
						<td>-</td>
						<td class="text-center fw" id="shopTfootPrice_${item.supplierId}"></td>
					</tr>
				</tfoot>
			</table>
			<div class="size_sm">
		      <p class="ms">给供应商留言：</p>
		      <textarea placeholder="可以告诉供应商您的特殊要求" class="orderRemark" name="supplierRemark"></textarea>
		    </div>
		</div><br>
	</c:forEach>
	<div class="text-center">
		<a href="javascript:void(0)"><button class="next_step" onclick="saveOrder();">提交订单</button></a>
		<input type="hidden" id="buyOrderId" value="${buyOrderId}">
	</div>
</div>