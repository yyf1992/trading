<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
function edit(id){
	$.ajax({
		url:basePath+"platform/sysUser/loadEditPerson",
		async:false,
		type:"post",
		data:{"id":id},
		success:function(data){
			layer.open({
			type:1,
			title:"修改人员",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:basePath+"platform/sysUser/updateSysUser",
  		    		data:$("#editSysForm").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/sysUser/loadPersonList','system','17100920491875166658'); 
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/sysUser/loadPersonList','system','17100920491875166658'); 
						}
				},
  		    		error:function(){
  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function deleteSys(id){
	layer.confirm("删除此账户后，该账户将不能再登录此企业的系统进行操作，确定要删除此账户吗？",{skin:"layui-layer-rim",
	      title:"提醒",
	      area: ["320px","auto"]
	      },function(index){
	      		$.ajax({
				url:basePath+"platform/sysUser/deleteSysUser",
				async:false,
				type:"post",
				data:{"id":id},
				success:function(data){
					var result=eval('('+data+')');
					var resultObj=isJSONObject(result)?result:eval('('+result+')');
					if(resultObj.success){
						layer.msg(resultObj.msg,{icon:1});
						leftMenuClick(this,'platform/sysUser/loadPersonList','system','17100920491875166658'); 
					}else{
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	      }.bind(id));
}
function insert(){
	$.ajax({
		url:basePath+"platform/sysUser/loadAddSysUser",
		type:"post",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				title:"新增人员",
				skin: 'layui-layer-rim',
  		        area: ['400px', 'auto'],
  		        content:data,
  		        btn:['确定','取消'],
  		        yes:function(index,layerio){
  		        	$.ajax({
  		        		url:basePath+"platform/sysUser/saveAdd",
  		        		type:"post",
  		        		data:$("#sysUserForm").serialize(),
  		        		async:false,
  		        		success:function(data){
  		        			var result=eval('('+data+')');
					    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysUser/loadPersonList','system','17100920491875166658');
							}else{
								layer.msg(resultObj.msg,{icon:2});
							}
  		        		},
  		        		error:function(){
  		        			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		        		}
  		        	});
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//设置角色
function setRole(userId){
	$.ajax({
		url:basePath+"platform/tradeRole/instalRoleUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置角色",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//设置店铺
function setShop(userId){
	$.ajax({
		url:basePath+"platform/sysshop/instalShopUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置关联店铺",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//设置供应商
function setSupplier(userId){
	$.ajax({
		url:basePath+"platform/buyer/supplier/instalSupplierUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置关联供应商",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="content_modify">
	<div class="content_role">
		<form class="layui-form" action="platform/sysUser/loadPersonList">
			<ul class="order_search personSearch">
				<li class="range"><label>员工名称：</label> <input type="text"
					placeholder="输入员工姓名" name="userName"
					value="${searchPageUtil.object.userName}"></li>
				<li class="state"><label>登录账户：</label> <input type="text"
					placeholder="输入登录账户" name="loginName"
					value="${searchPageUtil.object.loginName}"></li>
				<li class="state"><label>加入日期：</label>
					<div class="layui-input-inline">
						<input type="text" name="createDate"
							value="${searchPageUtil.object.createDate}" lay-verify="date"
							placeholder="请选择日期" autocomplete="off" class="layui-input">
					</div></li>
				<li class="wa"><label>状态：</label>
					<div class="layui-input-inline">
						<select name="status" lay-filter="aihao">
							<option value="-1"
								<c:if test="${searchPageUtil.object.status=='-1'}">selected="selected"</c:if>>全部</option>
							<option value="0"
								<c:if test="${searchPageUtil.object.status==0}">selected="selected"</c:if>>正常</option>
							<option value="1"
								<c:if test="${searchPageUtil.object.status==1}">selected="selected"</c:if>>禁用</option>
						</select>
					</div></li>
				<li class="nomargin"><button class="search"
						onclick="loadPlatformData();">搜索</button>
				</li>
			</ul>
			<input id="pageNo" name="page.pageNo" type="hidden"
				value="${searchPageUtil.page.pageNo}" /> <input id="pageSize"
				name="page.pageSize" type="hidden"
				value="${searchPageUtil.page.pageSize}" />
		</form>
		<div class="mt">
			<a href="javascript:void(0)" button="新增"
				onclick="insert();"
				class="layui-btn layui-btn-add layui-btn-small rt">
				<i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
		<table class="table_pure personList">
			<thead>
				<tr>
					<td style="width:10%">编号</td>
					<td style="width:8%">员工姓名</td>
					<td style="width:20%">登录账户</td>
					<td style="width:8%">状态</td>
					<td style="width:20%">操作日期</td>
					<td style="width:20%">操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="sysUser" items="${searchPageUtil.page.list}"
					varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${sysUser.user_name}</td>
						<td>${sysUser.login_name}</td>
						<td><c:choose>
								<c:when test="${sysUser.is_del==0}">正常</c:when>
								<c:otherwise>禁用</c:otherwise>
							</c:choose>
						</td>
						<td><c:choose>
								<c:when test="${sysUser.update_date==null}">
									<fmt:formatDate value="${sysUser.create_date}" type="both" />
								</c:when>
								<c:otherwise>
									<fmt:formatDate value="${sysUser.update_date}" type="both" />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<a href="javascript:void(0)" button="修改"
								onclick="edit('${sysUser.id}');"
								class="layui-btn layui-btn-update layui-btn-mini">
								<i class="layui-icon">&#xe691;</i>修改</a>
							<a onclick="setRole('${sysUser.id}');" button="设置角色"
									class="layui-btn layui-btn-normal layui-btn-mini">
									<i class="layui-icon">&#xeaf3;</i>设置角色</a>
							<a href="javascript:void(0)" onclick="setShop('${sysUser.id}');" button="设置店铺"
								class="layui-btn layui-btn-update layui-btn-mini">
							<i class="layui-icon">&#xeaf3;</i>设置店铺</a>
							<a onclick="setSupplier('${sysUser.id}');" button="设置供应商"
									class="layui-btn layui-btn-normal layui-btn-mini">
									<i class="layui-icon">&#xeaf3;</i>设置供应商</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page}</div>
	</div>
</div>
