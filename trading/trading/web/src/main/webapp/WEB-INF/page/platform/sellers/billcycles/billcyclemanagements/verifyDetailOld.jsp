<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function(){
        if ('${billCycleManagementInfo.billDealStatus}'<4){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "待内部审批"
                },{
                    title: "待对方审批"
                },{
                    title: "审批已通过"
                }/*,{
                    title: "内部审批驳回"
                },{
                    title: "对方审批驳回"
                }*/]
            });
            
            var step = parseInt('${billCycleManagementInfo.billDealStatus}');
            $(".ystep1").setStep(step);
        }
    });

</script>
<%--<div >--%>
	<div class="order_top mt">
		<div class="lf order_status" >
			<h4 class="mt">当前订单状态</h4>
			<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==1}">待内部审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==2}">待对方审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==3}">审批已通过</c:when>--%>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==4}">内部审批驳回</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==5}">对方审批驳回</c:when>--%>
			  <c:when test="${billCycleManagementInfo.billDealStatus == 2}">待内部审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==3}">审批已通过</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==5}">审批驳回</c:when>
		  </c:choose>
        </span>
			</p>
			<p class="order_remarks text-center"></p>
		</div>
		<div class="lf order_progress">
			<div class="ystep1"></div>
		</div>
	</div>
	<div>
		<h4 class="mt">账单结算周期信息</h4>
		<div class="order_d">
			<p><span class="order_title">采购商信息</span></p>
			<table class="table_info">
				<tr>
					<td>采购商名称：<span>淘宝</span></td>
					<td>负责人：<span>小萌</span></td>
					<td>手机号：<span>12435</span></td>
				</tr>
			</table>
			<p class="line mt"></p>
			<p><span class="order_title">供应商信息</span></p>
			<table class="table_info">
				<tr>
					<td>供应商名称：<span>天猫</span></td>
					<td>负责人：<span>萌萌</span></td>
					<td>手机号：<span>12234325</span></td>
				</tr>
			</table>

			<p class="line mt"></p>
			<p><span class="order_title">账单结算周期信息</span></p>
			<table class="table_info">
				<c:if test="${!empty sellerCycleManagementOld}">
					<tr>
						<td>原结账周期（天）：<span>历史结账周期</span></td>
						<td>结账周期（天）：<span>${sellerBillCycleManagement.checkoutCycle}</span></td>
					</tr>
					<tr>
						<td>原出账日期（日）：<span>原出账日期</span></td>
						<td>出账日期（日）：<span>${sellerBillCycleManagement.billStatementDate}</span></td>
					</tr>
					<tr>
						<td>原创建日期：<span>原创建日期</span></td>
						<td>创建日期：<span>${sellerBillCycleManagement.createDate}</span></td>
					</tr>
					<tr>
						<td>原操作人：<span>原操作人</span></td>
						<td>操作人：<span>${sellerBillCycleManagement.updateName}</span></td>
					</tr>
				</c:if>
				<c:if test="${empty billCycleManagementOld}">
					<tr>
						<td>结账周期（天）：<span>${sellerBillCycleManagement.checkoutCycle}</span></td>
						<td>出账日期（日）：<span>${sellerBillCycleManagement.billStatementDate}</span></td>
						<td>创建日期：<span>${sellerBillCycleManagement.createDate}</span></td>
					</tr>
					<tr>
						<td>操作人：<span>${sellerBillCycleManagement.updateName}</span></td>
					</tr>
				</c:if>

			</table>

			<p class="line mt"></p>
			<p><span class="order_title">账单结算周期关联利息信息</span></p>
			<c:if test="${!empty sellerCycleManagementOld}">
				<table class="table_pure detailed_list">
					<thead>
					<tr>
						<td style="width:33%">原逾期（天）</td>
						<td style="width:33%">原月利率（%）</td>
						<td style="width:33%">原利息计算方式</td>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="interestListOld" items="${sellerCycleManagementOld}">
						<tr>
							<td>${interestListOld.overdueDate}</td>
							<td>${interestListOld.overdueInterest}</td>
							<td>
								<c:choose>
									<c:when test="${interestListOld.interestCalculationMethod eq '1'}">单利</c:when>
									<c:when test="${interestListOld.interestCalculationMethod eq '2'}">复利</c:when>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</c:if>

			<table class="table_pure detailed_list">
				<thead>
				<tr>
					<td style="width:33%">逾期（天）</td>
					<td style="width:33%">月利率（%）</td>
					<td style="width:33%">利息计算方式</td>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="interestList" items="${interestList}">
					<tr>
						<td>${interestList.overdueDate}</td>
						<td>${interestList.overdueInterest}</td>
						<%--<td>${product.interestCalculationMethod}</td>--%>
						<td>
							<c:choose>
								<c:when test="${interestList.interestCalculationMethod eq '1'}">单利</c:when>
								<c:when test="${interestList.interestCalculationMethod eq '2'}">复利</c:when>
							</c:choose>
						</td>
						<%--<td>
							<c:if test="${interestList.interestCalculationMethod == '1'}"><td>单利</td></c:if>
							<c:if test="${interestList.interestCalculationMethod == '2'}"><td>复利</td></c:if>
						</td>--%>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
<%--
</div>--%>
