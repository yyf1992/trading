<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <title>合同打印</title>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/contracts.css">
    <script type="text/javascript">
        function printContract() {
            $('#printDiv').hide();
            window.print();
        }
    </script>
    <style media="print">
        @page {
            size: auto;  /* auto is the initial value */
            margin: 0mm; /* this affects the margin in the printer settings */
        }
    </style>
</head>
<div class="content">
    <div class="add_contract">
        ${htmlContent}
        <div class="contract_submit">
            <div class="layui-btn layui-btn-normal layui-btn-small" id="printDiv" onclick="printContract();">打印</div>
        </div>
    </div>

</div>
