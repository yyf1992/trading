<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		loadProvince("updateProvince","${address.province}");
		// 根据省份加载市
		var provinceId = $("#updateProvince").val();
		loadCity(provinceId,"updateCity","${address.city}");
		// 根据市加载区
		var cityId = $("#updateCity").val();
		loadArea(cityId,"updateArea","${address.area}");
		//省改变
		$("#updateProvince").change(function(){
			// 根据省份加载市
			var provinceId = $("#updateProvince").val();
			loadCity(provinceId,"updateCity","");
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		//市改变
		$("#updateCity").change(function() {
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
	});
</script>
<form action="">
	<input type="hidden" id="updateId" value="${address.id}">
	<div class="address_group">
		<span>收货人:</span> <input type="text" id="updatePersonName" style="width: 207px" value="${address.personName}">
	</div>
	<div class="address_group">
		<span>收货地址:</span>
		<select id="updateProvince" name="updateProvince"></select>
		<select id="updateCity" name="updatecity"></select><input type="hidden" id="companyCityId" value="${address.city}"/>
		<select id="updateArea" name="updateArea"></select><input type="hidden" id="companyAreaId" value="${address.area}"/>
	</div>
	<div class="address_group">
		<span>详细地址:</span> <input type="text" id="updateAddrName" style="width: 90%"
			value="${address.addrName}">
	</div>
	<div class="address_group">
		<span>手机号码:</span>
		<input type="tel" id="updatePhone" style="width: 207px" value="${address.phone}">
		<span>固定电话:</span>
		<input type="text" id="updateZone" style="width: 65px" value="${address.zone}"> -
		<input type="text" id="updateTelNo" style="width: 110px" value="${address.telNo}">
	</div>
	<div class="address_group">
		<span></span> <label><input type="checkbox" id="updateIsDefault" <c:if test="${address.isDefault == 0}">checked="checked"</c:if>> 设为默认地址</label>
	</div>
</form>