<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<title>商品库存明细</title>
<link rel="stylesheet" href="<%=basePath %>css/common.css">
<link rel="stylesheet" href="<%=basePath %>css/layui/css/layui.css">
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<style>

</style>
<div>
  <form action="platform/baseInfo/inventorymanage/prodcutsDetails">
   <div class="stock_explain">
   <h3 class="stock_detail mt">商品库存明细</h3>
    <ul>
  	  <li >
        <span>商品:</span>
        <p>${buyWarehouseProdcuts.productName}</p>
      </li>
      <li>
        <span>货号:</span>
        <p>${buyWarehouseProdcuts.productCode}</p>
      </li>
      <li>
        <span>规格代码:</span>
        <p>${buyWarehouseProdcuts.skuCode}</p>
      </li>
      <li>
        <span>规格名称:</span>
        <p>${buyWarehouseProdcuts.skuName}</p>
      </li>
      <li>
        <span>条形码:</span>
        <p>${buyWarehouseProdcuts.barcode}</p>
      </li>
    </ul>
    </div>
   <input id="id" name="id" type="hidden" value="${buyWarehouseProdcuts.id}" />
   <input type="hidden" name="barcode" id="barcode" value="${buyWarehouseProdcuts.barcode}"> 
   <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
   <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
   <h4 class="mt">商品店铺库存明细</h4>
   <table  class="table_pure ">
     <thead>
       <tr>
         <td style="width:5%">序号</td>
         <td style="width:20%">店铺名称</td>
         <td style="width:10%">锁定库存</td>
         <td style="width:10%">可用库存</td>
         <td style="width:10%">总库存</td>
       </tr>
     </thead>
     <tbody>
       <c:forEach var="prodcutsList" items="${searchPageUtil.page.list}" varStatus="status">
      	<tr>
	        <td>${status.count}</td>
	        <td>${prodcutsList.shopName}</td>
	        <td>${prodcutsList.usableStock}</td>
	        <td>${prodcutsList.lockStock}</td>
	        <td>${prodcutsList.stocks}</td>           
      	</tr>
      </c:forEach>
     </tbody>
   </table>
   <div class="pager">${searchPageUtil.page}</div>
  </form>
   <div class="btn_p text-center mp30">
 		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/baseInfo/inventorymanage/prodcutsInventory','baseInfo')">
			<span class="preview order_p_s">返回</span>
		</a>
  </div>
</div>
