<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/common.jsp"%>
<div>
	<div class="supplier_succ">
		<img src="${basePath}statics/platform/images/ok1.png">
		<p>恭喜您的供应商添加成功！</p>
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/addSupplier','buyer')" class="layui-btn layui-btn-small layui-btn-danger">继续添加供应商</a>
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/supplierList','buyer')" class="layui-btn layui-btn-small layui-btn-normal">转到供应商列表页</a>
	</div>
</div>
