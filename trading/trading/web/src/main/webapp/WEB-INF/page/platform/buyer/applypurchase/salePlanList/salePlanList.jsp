<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<script type="text/javascript">
	$(function(){
		//搜索更多
		$(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});
		//设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
		if("${params.tabId}"!='0'){
			$("#salePlanContent select[name='interest']").prop("disabled", true);
		}
		//日期
		loadDate("startDate","endDate");
	
		$("#salePlanContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
		$(".tab a").click(function(){
			var id = $(this).attr("id");
			//清空查询条件
			$("#salePlanContent #resetButton").click();
			var index = id.split("_")[1];
			$("#salePlanContent input[name='tabId']").val(index);
			loadPlatformData();
		});
	    //初始化数据
	    selectData();
	    //展开查询条件
	    $(".search_more").click();
	});
	function selectData(){
		var formObj = $("#salePlanContent").find("form");
		$.ajax({
			url : basePath+"buyer/salePlan/loadData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				$("#tabContent").empty();
				var str = data.toString();
				$("#tabContent").html(str);
	            $(".tab>a").removeClass("hover");
	            $(".tab #tab_${params.tabId}").addClass("hover");
	            loadVerify();
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	//导出
	$("#salePlanListExportButton").click(function(e){
		e.preventDefault();
		var formData = $("form").serialize();
		var url = "download/salePlanListStatistic?" + formData;
		window.open(url);
	});
</script>
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待审批（<span>${params.waitVerifyNum }</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_2">审批已通过（<span>${params.verifyPassNum }</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_3">审批未通过（<span>${params.verifyNoPassNum }</span>）</a> <b>|</b>
</div>
<div id="salePlanContent">
	<!--搜索栏-->
	<form class="layui-form" action="buyer/salePlan/salePlanList">
		<div class="search_top mt">
			<input type="text" placeholder="输入商品名称/货号/条形码进行搜索" id="productSelectValue" name="productSelectValue" value="${params.productSelectValue}">
			<button id="selectButton">搜索</button><span class='search_more sBuyer'></span>
			<span class="search_more sBuyer"></span>
		</div>
		<a button="生成采购计划" href="javascript:leftMenuClick(this,'buyer/salePlan/loadSalePlanProductSum','buyer','18010916113246780386');" class="layui-btn layui-btn-danger layui-btn-small rt">
		<i class="layui-icon">&#xe67d;</i>生成采购计划</a>
		<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" style="margin-right: 10px;" id="salePlanListExportButton">
		<i class="layui-icon">&#xe7a0;</i> 导出</a>
		<ul class="order_search seller_order">
            <li><label>计划编号:</label>
				<input type="text" placeholder="输入计划编号" style="width:217px" name="planCode" value="${params.planCode}">
			</li>
			<li class="range"><label>创建日期:</label>
				<div class="layui-input-inline">
                    <input type="text" name="createStartDate" id="createStartDate" lay-verify="date" value="${params.createStartDate}" class="layui-input" placeholder="开始日期">
                </div> 至
                <div class="layui-input-inline">
                    <input type="text" name="createEndDate" id="createEndDate" lay-verify="date" value="${params.createEndDate}" class="layui-input" placeholder="截止日期">
                </div>
			</li>
			<li class="range nomargin"><label>创建人:</label>
				<input type="text" placeholder="输入创建人" name="createName" value="${params.createName}">
			</li>
			<li class="range"><label>销售周期:</label>
				<div class="layui-input-inline">
                    <input type="text" name="saleStartDate" id="saleStartDate" lay-verify="date" value="${params.saleStartDate}" class="layui-input" placeholder="开始日期">
                </div> 至
                <div class="layui-input-inline">
                    <input type="text" name="saleEndDate" id="saleEndDate" lay-verify="date" value="${params.saleEndDate}" class="layui-input" placeholder="截止日期">
                </div>
			</li>
            <li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
        </ul>
        <input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent"></div>
</div>
