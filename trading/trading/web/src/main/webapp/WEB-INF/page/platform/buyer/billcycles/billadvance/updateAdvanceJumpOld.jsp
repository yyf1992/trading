<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增预付款</title>
    <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/updateAdvance.js"></script>
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
</head>

<div id="addAdvanceDiv">
    <%--<div class="layui-row">--%>
        <%--<div class="layui-col-md3 layui-col-md-offset3">--%>

        <%--</div>--%>
    <%--</div>--%>
  <div class="layui-row" style="overflow: hidden">
      <div class="layui-col-md12 layui-col-md-offset1">
          <input type="hidden" id="advanceId" value="${buyBillRecAdvance.id}">
          <div class="layui-form-item">
              <label class="labelStyle">供应商:</label>
              <input type="hidden" id="sellerCompanyId" name="sellerCompanyId" value="${buyBillRecAdvance.sellerCompanyId}">
              <input style="width:20%;height: 30px" disabled="disabled" id="sellerCompanyName" name="sellerCompanyName" value="${buyBillRecAdvance.sellerCompanyName}" >
          </div>
          <div class="layui-form-item">
              <label class="labelStyle">采购员:</label>
              <input type="hidden" id="createBillUserId" name="createBillUserId" value="${buyBillRecAdvance.createBillUserId}">
              <input style="width:20%;height: 30px" disabled="disabled" id="createBillUserName" name="createBillUserName" value="${buyBillRecAdvance.createBillUserName}" >
          </div>
          <div class="layui-form-item">
              <label class="labelStyle">充值金额:</label>
              <%--<div class="layui-input-inline">--%>
                  <%--<input type="hidden" id="usableTotalOld" value="${buyBillRecAdvance.usableTotal}">--%>
                  <input type="number" min="0" name="usableTotal" id="usableTotal"  style="width: 200px;height: 30px;" class="" placeholder="预付款金额">
              <%--</div>--%>
          </div>
          <div class="bill_cycle layui-form-item">
              <label class="labelStyle">充值凭证:</label>
              <%--<div class="upload_license" style='width: 150px;height: 20px'>--%>
              <%-- <input type="hidden" id="urlStrOld" value="${buyBillRecAdvance.fileAddress}">--%>
              <input type="file" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadPayment();" style="width: 70px;height: 23px;" >
              <%--</div>--%>
              <p></p>
              <span class="labelStyle" style="white-space: nowrap">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
              <div class="scanning_copy original" id="advanceFileDiv"></div>
          </div>
          <div class="bill_cycle layui-form-item">
              <label class="labelStyle">备注:</label>
              <textarea type="text" id="taskRemarks" name="taskRemarks" style="width: 300px;height: 100px;" class="layui-input"></textarea>
          </div>
          <style>
              .labelStyle{float:left;display:block;padding:9px 15px;width:100px;font-weight:400;text-align:right}
          </style>
      </div>

    </div>
    
  <div class="text-center mp30">
    <a href="javascript:void(0);">
    <button class="layui-btn layui-btn-normal layui-btn-small" id="billCycleInfo_add">确认提交</button>
    <span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer');">返回列表</span>
    </a>
  </div>
</div>
 
