<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../../common/path.jsp"%>
<style>
.purchasePlanOut select{
	width: 90%
}
.unAllowUpdate{
	cursor:not-allowed;
	background:#EBEBE4;
	height:25px;
}
.allowUpdate{
	height:25px;
}
.upload-group{
  line-height: 28px;
}
.upload-file{
  position: relative;
  vertical-align: baseline;
}
.upload-input{
  position: absolute;
  width:100%;
  height: 100%;
  opacity: 0;
  z-index: 9999;
}
</style>
<script type="text/javascript">
var shopList = <%=request.getAttribute("shopList")%>;
var shopArrayStr = JSON.parse(JSON.stringify(shopList));
var form;
layui.use('form', function(){
  	form = layui.form;
	form.render("select");
	// 删除采购凭证
	$('.voucherImg').on('click','b',function(){
	  layer.confirm('确定要删除该图片吗？</p>',{
	    icon:3,
	    skin:'pop',
	    title:'提醒',
	    closeBtn:2
	  },function(index){
	    layer.close(index);
	    $(this).parent().remove();
	  }.bind(this));
	});
	$('#planAdd').click(function(){
		var str="<td><b></b></td>"+
					"<td style='overflow:visible;'>"+$('#addDeptSelect').html()+"</td>"+
					"<td><input type='text' class='unAllowUpdate' name='productCode' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='productName' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='skuCode' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='skuName' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='skuOid' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly' style='border: 0'>" +
					"<input type='hidden' name='productType' value=''>"+
					"<input type='hidden' name='unitId' value=''>"+
					"<input type='hidden' name='unitName' value=''></td>"+
					"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0'></td>"+ 
					"<td><input type='number' min='0' class='allowUpdate' style='width: 50px;border: 0' value='0'></td>";
	  var tbl=$('.purchasePlan tr:eq(-1)');
	  addRow(str,tbl);
	  form.render("select");
	});
	// 删除采购计划
	$('.purchasePlan').on('click','tbody td:first-child b',function(){
	  layer.confirm('您确定要删除此计划采购的商品吗？</p>',{
	    icon:3,
	    skin:'popBuyer btnCenter',
	    title:'提醒',
	    closeBtn:0
	  },function(index){
	    layer.close(index);
	    delRow(this);
	  }.bind(this));
	});
	// 提交保存
	$("#submitButton").click(function(){
		var title = $("#title").val();
		if(title == ''){
			layer.msg("请填写标题！",{icon:7});
			return;
		}else if(title.length > 30){
			layer.msg("标题长度不能大于30个字符！",{icon:7});
			return;
		}
		// 周期
		var saleDate = $("#saleDate").val();
		if(saleDate == ''){
			layer.msg("请选择销售周期！",{icon:7});
			return;
		}
		var skuoIdArr="";
		var shopError = "";
		var goodsStr="";
		var pronameErrot="";
		var numError="";
		//var ContactSettings = new Array();
		$("#productTbody>tr:gt(0)").each(function(i){
			var skuOid = $(this).find("input[name='skuOid']").val();
			if(skuOid != ''){
				var proCode = $(this).find("input[name='productCode']").val();
				var proName = $(this).find("input[name='productName']").val();
				var skuCode = $(this).find("input[name='skuCode']").val();
				var skuName = $(this).find("input[name='skuName']").val();
				var shopIdArr = $(this).find("td:eq(1)").find("option:selected").val();
				if(shopIdArr==''){
					shopError="第"+(i+1)+"行没有选择部门！";
					return false;
				}
				var shopId=shopIdArr.split(",")[0];
				var shopCode=shopIdArr.split(",")[1];
				var shopName=shopIdArr.split(",")[2];
				if(skuoIdArr.indexOf(skuOid+shopId+",")>-1){
					pronameErrot = "第"+(i+1)+"行商品重复！";
					return false;
				}else{
					skuoIdArr += skuOid+shopId+",";
				}
				//销量
				var salesNum = $(this).find("td:eq(7) input").val();
				if(parseInt(salesNum)<=0){
					numError="第"+(i+1)+"行销量必须大于0！";
					return false;
				}
				//入仓量
				var putStorageNum = $(this).find("td:eq(8) input").val();
				if(parseInt(putStorageNum)<0 || putStorageNum == ""){
					numError="第"+(i+1)+"行入仓量不能为空或负数！";
					return false;
				}
				//商品类型
				var productType = $(this).find("input[name='productType']").val();
				//单位
				var unitId = $(this).find("input[name='unitId']").val();
				var unitName = $(this).find("input[name='unitName']").val();
				goodsStr += skuOid+".NTNT."+proCode+".NTNT."+proName+".NTNT."+skuCode+".NTNT."+skuName+".NTNT."+shopId+".NTNT."
						+shopCode+".NTNT."+shopName+".NTNT."+salesNum+".NTNT."+putStorageNum+".NTNT."+productType+".NTNT."+unitId+".NTNT."+unitName+"@NTNT@";
			}
		});
		goodsStr = goodsStr.substring(0, goodsStr.length - 6);
		if(shopError!=''){
			layer.msg(shopError,{icon:7});
			return;
		}
		if(skuoIdArr==''){
			layer.msg("请添加商品！",{icon:7});
			return;
		}
		if(pronameErrot!=''){
			layer.msg(pronameErrot,{icon:7});
			return;
		}
		if(numError!=''){
			layer.msg(numError,{icon:7});
			return;
		}
		// 附件信息
		var a3 = $("#attachment3Div").find("span").length;
		if(a3 > 1){
			layer.msg("附件只允许上传一个！",{icon:7});
			return;
		}
		var url3 = $("#attachment3Div").find("span").find("input").val();
		var url = "buyer/salePlan/saveAddSalePlan";
		$.ajax({
			type : "POST",
			url : url,
			async: false,
			data : {
				"title" : title,
				"startDate" : $("#startDate").val(),
				"endDate" : $("#endDate").val(),
				"saleDays" : $("#saleDays").val(),
				"goodsStr" : goodsStr,
				//"ContactSettings" : JSON.stringify(ContactSettings),
				"remark" : $("#remark").val(),
				"url3" : url3,
				"menuName" : "17070718133683994400"
			},
			success : function(data) {
				if(data.flag){
					var res = data.res;
					if(res.code==40000){
						//调用成功
						saveSucess();
					}else if(res.code==40010){
						//调用失败
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40011){
						//需要设置审批流程
						layer.msg(res.msg,{time:500,icon:2},function(){
							setApprovalUser(url,res.data,function(data){
								saveSucess(data);
							});
						});
						return false;
					}else if(res.code==40012){
						//对应菜单必填
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40013){
						//不需要审批
						notNeedApproval(res.data,function(data){
							saveSucess(data);
						});
						return false;
					}
				}else{
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
					return false;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
	//导入
	$("#salePlanBeatchImport").click(function (e){
		e.preventDefault();
		var htmlStr = "<form class='layui-form'><div id='importExcelDiv'>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>计划标题:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<input type='text' id='importTitle' class='layui-input' style='width: 200px;'>"
			+"	</div>"
			+"</div>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>销售周期:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<input type='text' id='importSaleDate' lay-verify='date' placeholder='开始日期 - 结束日期' class='layui-input' style='width: 200px'>"
	        +"      <input type='hidden' id='importStartDate'>"
	        +"      <input type='hidden' id='importEndDate'>"
	        +"      <input type='hidden' id='importSaleDays'>"
			+"	</div>"
			+"</div>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>部门:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<select id='importShop' lay-verify='required' lay-search='' lay-filter='pageFilter'>"
	        +"      	<option value='''>直接选择或搜索选择</option>";
		$.each(shopArrayStr,function(i){
			var shopOptionStr = shopArrayStr[i].id+","+shopArrayStr[i].shopCode+","+shopArrayStr[i].shopName;
			htmlStr +="<option value='"+shopOptionStr+"'>"+shopArrayStr[i].shopName+"</option>";
		});
        htmlStr +="      </select>"
			+"	</div>"
			+"</div>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>选择文件:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<input type='file' name='excel' id='excel' />"
			+"	</div>"
			+"</div>"
			+"</div></form>"
			+"<script>"
			+"	loadDateImportScope('importSaleDate');"
			+" 	$('#importTitle').val($('#title').val());"
			+" 	$('#importSaleDate').val($('#saleDate').val());"
			+" 	$('#importStartDate').val($('#startDate').val());"
			+" 	$('#importEndDate').val($('#endDate').val());"
			+" 	$('#importSaleDays').val($('#saleDays').val());"
			+"<\/script>";
		layer.open({
			type:1,
	    	area: ['400px', 'auto'],
			fix: false, //不固定
			maxmin: true,
			shadeClose: true,
        	shade:0.4,
			title:"导入数据",
	    	content:htmlStr,
	    	btn:['确定','取消','下载模板'],
			yes:function(){
				if($("#importTitle").val() == ""){
					layer.msg("请输入标题！",{icon:2});
					return false;
				}
				if($("#importSaleDate").val() == ""){
					layer.msg("请选择销售周期！",{icon:2});
					return false;
				}
				if($("#importShop").val() == ""){
					layer.msg("请选择部门！",{icon:2});
					return false;
				}
				if($("#excel").val() == ""){
					layer.msg("请选择excel文件！",{icon:2});
					return false;
				}else{
					var shop = $("#importShop").val();
					var fileObj = document.getElementById("excel").files[0];
					var url = "download/importSalePlanExcelNew";
					var formDate = new FormData();
					formDate.append("excel", fileObj);
					formDate.append("shopId", shop.split(",")[0]);
					formDate.append("shopCode", shop.split(",")[1]);
					formDate.append("shopName", shop.split(",")[2]);
					formDate.append("importTitle", $("#importTitle").val());
					formDate.append("importSaleDate", $("#importSaleDate").val());
					formDate.append("importStartDate", $("#importStartDate").val());
					formDate.append("importEndDate", $("#importEndDate").val());
					formDate.append("importSaleDays", $("#importSaleDays").val());
					formDate.append("importExcelTrade_excel", "excel");
					formDate.append("menuName", "17070718133683994400");
					var xhr = new XMLHttpRequest();
					xhr.open("post", url, true);
					xhr.send(formDate);
					xhr.onreadystatechange = function() {
						if (xhr.readyState == 4 && xhr.status == 200) {
							var b = xhr.responseText;
							var result = eval('(' + b + ')');
				            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				            var res = resultObj.res;
							if(res.code==40000){
								//调用成功
								saveSucess();
							}else if(res.code==40010){
								//调用失败
								layer.msg(res.msg,{icon:2});
								return false;
							}else if(res.code==40011){
								//需要设置审批流程
								layer.msg(res.msg,{time:500,icon:2},function(){
									setApprovalUser(url,res.data,function(data){
										saveSucess(data);
									});
								});
								return false;
							}else if(res.code==40012){
								//对应菜单必填
								layer.msg(res.msg,{icon:2});
								return false;
							}else if(res.code==40013){
								//不需要审批
								notNeedApproval(res.data,function(data){
									saveSucess(data);
								});
								return false;
							}
						}
					}
				}
			},btn3:function(){
			    download('${basePath}statics/file/salePlanModel.xls');
			}
		});
		form.render("select");
	});
});
//销售日期范围
loadDateScope("saleDate");
//日期范围
function loadDateScope(saleDate){
	layui.use('laydate', function(){
	    var laydate = layui.laydate;
	    laydate.render({
		  elem: '#'+saleDate,
		  range: true,
		  done: function(value, date){//选择后回调
		    var scope = value.replace(/\s+/g,"").split("-");
		    var startDate =  scope[0] + "-" + scope[1] + "-" + scope[2];
		    var endDate =  scope[3] + "-" + scope[4] + "-" + scope[5];
		    var saleDays = DateDiff(startDate,endDate);
		    $("#saleDays").val(saleDays);
		    $("#startDate").val(startDate);
		    $("#endDate").val(endDate);
		  }
		});
	});
}
//计算天数差的函数，通用  
function  DateDiff(sDate1,  sDate2){    //sDate1和sDate2是2006-12-18格式  
    var  aDate,  oDate1,  oDate2,  iDays  
    aDate  =  sDate1.split("-")  
    oDate1  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])    //转换为12-18-2006格式  
    aDate  =  sDate2.split("-")  
    oDate2  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])  
    iDays  =  parseInt(Math.abs(oDate1  -  oDate2)  /  1000  /  60  /  60  /24) + 1;    //把相差的毫秒数转换为天数   + 1
    return  iDays  
}
function loadDateImportScope(saleDate){
	layui.use('laydate', function(){
	    var laydate = layui.laydate;
	    laydate.render({
		  elem: '#'+saleDate,
		  range: true,
		  done: function(value, date){//选择后回调
		    var scope = value.replace(/\s+/g,"").split("-");
		    var startDate =  scope[0] + "-" + scope[1] + "-" + scope[2];
		    var endDate =  scope[3] + "-" + scope[4] + "-" + scope[5];
		    var saleDays = DateDiff(startDate,endDate);
		    $("#importSaleDays").val(saleDays);
		    $("#importStartDate").val(startDate);
		    $("#importEndDate").val(endDate);
		  }
		});
	});
}
// 下载
function download(src) {
	var $a = $("<a></a>").attr("href", src).attr("download", "销售计划导入模版.xls");
    $a[0].click();
}
//保存成功
function saveSucess(ids){
	layer.msg("保存成功！",{icon:1});
	leftMenuClick(this,"buyer/salePlan/salePlanList","buyer","17070718133683994602");
}
//商品弹出框显示
function showInterflowProduct(obj) {
	var checkObj;
    loadInterflowProduct(obj);
    var fullIndex = layer.open({
        type: 1,
        title: '选择商品',
        area: ['900px', '500px'],
		fix: false, //不固定
		maxmin: true,
		shadeClose: true,
       	shade:0.4,
        //btn:['确定','关闭'],
        content: $('#commSelPlan'),
    });
}
//查找商品
function loadInterflowProduct(obj) {
	var id=$(obj).parents("tr").attr("id");
	$.ajax({
        //url:"platform/product/loadInterflowProductList",
        url : "buyer/applyPurchaseHeader/loadAllProductList",
        data:{
            "page.divId":'commSelPlan',
            "selectProductId" : id
        },
        async:false,
        success:function(data){
            $("#commSelPlan").empty();
            var str = data.toString();
            $("#commSelPlan").html(str);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
</script>
<div id="addPurchaseDiv" style="width: 100%; overflow: auto;">
	<!--搜索部分-->
	<form class="layui-form" action="">
		<div class="search_supplier mt">
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>标题:</label>
				<div class="layui-input-inline">
					<input type="text" id="title" name="title" class="layui-input" value="">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>销售周期：</label>
				<div class="layui-input-inline">
					<input type="text" name="saleDate" id="saleDate" lay-verify="date" placeholder="开始日期 - 结束日期" class="layui-input" style="width: 190px">
	               	<input type="hidden" id="startDate" value="">
	               	<input type="hidden" id="endDate" value="">
	               	<input type="hidden" id="saleDays" value=""/>
				</div>
			</div>
		</div>
		<div class="newBuild mt">
			<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small rt" 
				id="salePlanBeatchImport">
				<i class="layui-icon">&#xe98e;</i>导入
			</a>
	    </div>
		<!--采购列表部分-->
		<div class="purchasePlanOut mt" style="overflow:visible;">
	        <table class="table_pure purchasePlan mt" style="width: 100%">
	          <thead>
	          <tr>
	            <td width="40px">删除</td>
	            <td width="150px">部门</td>
	            <td width="150px">货号</td>
	            <td width="150px">商品名称</td>
	            <td width="150px">规格代码</td>
	            <td width="150px">规格名称</td>
	            <td width="150px">条形码</td>
	            <td width="80px">销售计划</td>
	            <td width="87px">入仓量</td>
	          </tr>
	          <tr>
	            <td colspan="9"></td>
	          </tr>
	          </thead>
	          <tbody id="productTbody">
	          	<tr style="display: none"><td colspan="9"></td></tr>
	          	<tr id="bbb">
	               <td><b></b></td>
	               <td style="overflow:visible;">
	               	<select id="shopId" name="shopId" lay-verify="required" lay-search="" lay-filter="pageFilter">
						<option value="">直接选择或搜索选择</option>
						<c:forEach var="shop" items="${shopList2}">
							<option value="${shop.id},${shop.shopCode},${shop.shopName}">${shop.shopName}</option>
						</c:forEach>
					</select>
	               </td>
	               <td><input type="text" class="unAllowUpdate" name="productCode" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly" style="border: 0"></td>
	               <td><input type="text" class="unAllowUpdate" name="productName" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly" style="border: 0"></td>
	               <td><input type="text" class="unAllowUpdate" name="skuCode" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly" style="border: 0"></td>
	               <td><input type="text" class="unAllowUpdate" name="skuName" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly" style="border: 0"></td>
	               <td>
	               		<input type="text" class="unAllowUpdate" name="skuOid" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly" style="border: 0">
	               		<input type="hidden" name="productType" value="">
	               		<input type="hidden" name="unitId" value="">
	               		<input type="hidden" name="unitName" value="">
	               </td>
	               <td><input type="number" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0" ></td><!-- 销量 -->
	               <td><input type="number" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0" ></td><!-- 入仓量 -->
	            </tr>
	          </tbody>
	        </table>
	      </div>
	</form>
	<button class="tr_add_buyer" id="planAdd"></button>
	<div class="size_sm">
		<p class="mt">说明：</p>
		<textarea placeholder="请输入说明内容" class="orderRemark" id="remark"></textarea>
	</div>
	<div class="size_sm mp mt upload-group">
            <span class="mt">附件:</span>
            <div class="upload-file size_sm layui-inline">
              <input type="file" class="upload-input" name="attachment3" id="attachment3" onchange="fileUpload(this);">
              <button type="button" class="layui-btn layui-btn-primary layui-btn-small">上传文件</button>
            </div>
            <span class="c99">仅支持JPG,GIF,PNG,JPEG,XLS,XLSX格式，且文件小于4M。</span>
            <div class="voucherImg" id="attachment3Div" style="margin-left: 35px"></div>
    </div>
	<div class="text-right">
		<a href="javascript:;" class="layui-btn layui-btn-normal layui-btn-small" id="submitButton">
			<i class="layui-icon">&#xe764;</i>提交
		</a>
		<!-- <a><input type="button" class="next_step" value="提交"></a> -->
	</div>
	<!--选择商品弹出框-->
    <div class="commSelPlan" id="commSelPlan"></div>
    <div id="addDeptSelect" hidden="true">
		<select name="shopId" lay-verify="required" lay-search="" lay-filter="pageFilter">
			<option value="">直接选择或搜索选择</option>
			<c:forEach var="shop" items="${shopList2}">
				<option value="${shop.id},${shop.shopCode},${shop.shopName}">${shop.shopName}</option>
			</c:forEach>
		</select>
	</div>
</div>