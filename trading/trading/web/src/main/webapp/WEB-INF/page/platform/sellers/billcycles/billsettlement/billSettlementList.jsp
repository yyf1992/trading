<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单供应商结算列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billsettlement/billSettlementList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    //重置查询条件
    function resetformData(){
    	$("#sellerCompanyName").val("");
    	$("#updateUserName").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
    	$("#billDealStatus").val("0");
    }
	</script>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);"  class="hover">所有</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.billRecordStatus eq '1'}">class="hover"</c:if>>待我付款（<span>${params.approvalBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.billRecordStatus eq '2'}">class="hover"</c:if>>已付款（<span>${params.acceptBillReconciliationCount}</span>）</a> <b>|</b>
</div>
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="/platform/buyer/billSettlement/billSettlementList">
		<div class="search_top mt">
			<input id="sellerCompanyName" type='text' placeholder='输入合作方名称进行搜索' name="sellerCompanyName" value="${params.sellerCompanyName}"/>
			<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			<span class="search_more sBuyer"></span>
		</div>

		<ul class="order_search re_search">
			<li >
				<label>结算单号:</label>
				<input type="text" placeholder="输入结算单号进行搜索" style="width:217px"/>
			</li>
			<li class="range">
				<label>商品总金额:</label>
				<input type="number" placeholder="￥">
				-
				<input type="number" placeholder="￥">
			</li>
			<li>
				<label>运费:</label>
				<input type="number" placeholder="￥">
				-
				<input type="number" placeholder="￥">
			</li>
			<li class="ship nomargin">
				<label>类型:</label>
				<div class="layui-input-inline">
					<select name="interest" lay-filter="aihao">
						<option value="0" selected>全部</option>
						<option value="1">专用发票</option>
						<option value="2">普通发票</option>
						<option value="3">其他票据</option>
					</select>
				</div>
			</li>
			<li>
				<label>票据单号:</label>
				<input type="text" placeholder="输入票据号" style="width:178px">
			</li>
			<li class="">
				<label>应收款余额:</label>
				<input type="number" placeholder="￥">
				-
				<input type="number" placeholder="￥">
			</li>
			<li class="ship_d">
				<label>日期:</label>
				<div class="layui-input-inline">
					<input class="layui-input" placeholder="开始日" id="LAY_demorange_s">
				</div>
				-
				<div class="layui-input-inline">
					<input class="layui-input" placeholder="截止日" id="LAY_demorange_e">
				</div>
			</li>
			<li class="ship nomargin">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select name="interest" lay-filter="aihao">
						<option value="0" selected>全部</option>
						<option value="1">待我付款</option>
						<option value="2">已付款</option>
					</select>
				</div>
			</li>
			<li>
				<label>票据金额:</label>
				<input type="number" placeholder="￥">
				-
				<input type="number" placeholder="￥">
			</li>
			<li>
				<label>结算总金额:</label>
				<input type="number" placeholder="￥">
				-
				<input type="number" placeholder="￥">
			</li>
			<!-- <li class="range"><button type="reset" id="resetButton" onclick="resetformData();">重置</button></li> -->
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
	</form>
	  <div class="receive_list">
		  <table class="table_pure settle_list">
			  <thead>
			  <tr>
				  <td style="width:7%">结算单号</td>
				  <td style="width:9%">供应商名称</td>
				  <td style="width:7%">票据号</td>
				  <td style="width:7%">票据类型</td>
				  <td style="width:7%">票据金额</td>
				  <td style="width:7%">商品总金额</td>
				  <td style="width:7%">运费</td>
				  <td style="width:7%">逾期利息</td>
				  <td style="width:7%">应付款总额</td>
				  <td style="width:7%">实际付款金额</td>
				  <td style="width:7%">应付款余额</td>
				  <td style="width:9%">日期</td>
				  <td style="width:7%">状态</td>
				  <td style="width:5%">操作</td>
			  </tr>
			  <tr>
				  <td colspan="14"></td>
			  </tr>
			  </thead>
			  <tbody>
			  <tr>
				  <td>255165656525</td>
				  <td>温州市奥妙鞋业有限公司</td>
				  <td>46464842154122</td>
				  <td>专用发票</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>0.00</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>2017-06-19</td>
				  <td>
					  <span>待我付款</span>
					  <div class="check_receipt" onclick="showBillReceipt();">查看票据</div>
				  </td>
				  <td><div class="bill_payment layui-btn layui-btn-normal layui-btn-mini" onclick="showPaymentAgree();">我要付款</div></td>
			  </tr>
			  <tr>
				  <td>255165656525</td>
				  <td>温州市奥妙鞋业有限公司</td>
				  <td>46464842154122</td>
				  <td>专用发票</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>0.00</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>2017-06-19</td>
				  <td>
					  <span>待我付款</span>
					  <div class="check_receipt" onclick="showBillReceipt();">查看票据</div>
				  </td>
				  <td><div class="bill_payment layui-btn layui-btn-normal layui-btn-mini" onclick="showPaymentAgree();">我要付款</div></td>
			  </tr>
			  <tr>
				  <td>255165656525</td>
				  <td>温州市奥妙鞋业有限公司</td>
				  <td>46464842154122</td>
				  <td>专用发票</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>0.00</td>
				  <td>8500.00</td>
				  <td>8500.00</td>
				  <td>0.00</td>
				  <td>2017-06-19</td>
				  <td>
					  <span>已付款</span>
					  <div class="check_receipt" onclick="showBillReceipt();">查看票据</div>
					  <div class="check_receipt" onclick="showVoucher();">付款凭证</div>
				  </td>
				  <td></td>
			  </tr>
			  </tbody>
		  </table>
	  </div>
		<div class="pager"></div>
	<%--</form>--%>
</div>

<!--收款票据-->
<div id="linkReceipt" class="interest" style="display:none;">
  <div id="receiptBody" class="receipt_content">
	  <div class="big_img">
		  <img src="">
	  </div>
	  <div class="del_receipt">
		  <button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
	  </div>
	  <div class="view">
		  <a href="#" class="backward_disabled"></a>
		  <a href="#" class="forward"></a>
		  <ul id="icon_list"></ul>
	  </div>
  </div>
</div>

<!--收款凭据-->
<div id="linkVoucher" class="interest" style="display:none;">
	<div id="voucherBody" class="receipt_content">
		<div class="big_img">
			<img src="">
		</div>
		<div class="del_receipt">
			<button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
		</div>
		<div class="view">
			<a href="#" class="backward_disabled"></a>
			<a href="#" class="forward"></a>
			<ul id="voucherList"></ul>
		</div>
	</div>
</div>

<!--我要付款-->
<div class="bill_pay plain_frame" style="display:none;" id="paymentAgree">
  <form action="">
    <ul>
      <li>
        <span>实付款金额</span>：
		  <input type="number" placeholder="实际付款金额">
      </li>
      <li>
        <span>银行账户</span>：
		  <select name="" style="width:158px">
			  <option value="">4681148465112636</option>
		  </select>
		  <a href="添加供应商.html"><button class="layui-btn layui-btn-primary layui-btn-small">添加账户</button></a>
      </li>
	  <li>
		<span>付款类型</span>：
		<select name="">
			<option value="">现金</option>
			<option value="">银行转账</option>
			<option value="">支付宝</option>
			<option value="">微信</option>
		</select>
	  </li>
	  <li>
		<span>付款凭证</span>：
		<div class="upload_license ">
			<input type="file" multiple>
			<p></p>
		</div>
	  </li>
	  <li>
		<span>备注</span>：
		<textarea placeholder="请输入备注" style="width:235px"></textarea>
	  </li>
    </ul>
  </form>
</div>