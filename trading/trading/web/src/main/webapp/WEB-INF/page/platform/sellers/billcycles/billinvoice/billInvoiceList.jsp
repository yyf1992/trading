<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单发票列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billinvoice/billInvoiceList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<script type="text/javascript">
	$(function(){
	    //条件预加载
        $("#settlementNo").val("${params.settlementNo}");
        $("#invoiceNo").val("${params.invoiceNo}");
        $("#startCommodityAmount").val("${params.startCommodityAmount}");
        $("#endCommodityAmount").val("${params.endCommodityAmount}");
        $("#acceptInvoiceStatus").val("${params.acceptInvoiceStatus}");
        $("#invoiceType").val("${params.invoiceType}");

        $("#buyCompanyName").val("${params.buyCompanyName}");
        $("#startDate").val("${params.createStartDate}");
        $("#endDate").val("${params.createEndDate}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
	});
    //标签页改变
    function setStatus(obj,status) {
        $("#acceptInvoiceStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
    //重置查询条件
    function resetformData(){
        $("#settlementNo").val("");
        $("#invoiceNo").val("");
        $("#startCommodityAmount").val("");
        $("#endCommodityAmount").val("");
        $("#acceptInvoiceStatus").val("0");
        $("#invoiceType").val("");

        $("#buyCompanyName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
    }
	</script>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '0'}">class="hover"</c:if>>待开票据（<span>${params.noConfirmInvoiceCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '1'}">class="hover"</c:if>>待确认票据（<span>${params.approvalBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '2'}">class="hover"</c:if>>已确认票据（<span>${params.acceptBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '3'}">class="hover"</c:if>>已驳回票据（<span>${params.stopBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '4'}">class="hover"</c:if>>已取消票据（<span>${params.closeBillCycleCount}</span>）</a> <b>|</b>
</div>
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/seller/billInvoice/billInvoiceList">
		<ul class="order_search">
			<li >
				<label>开票编号:</label>
				<input type="text" placeholder="输入结算单号" id="settlementNo" name="settlementNo" />
			</li>
			<li class="comm">
				<label>采购商名称:</label>
				<input type="text" placeholder="请输入采购商名称" id="buyCompanyName" name="buyCompanyName" >
			</li>
			<li class="comm">
				<label>票据号:</label>
				<input type="text" placeholder="请输入票据号" id="invoiceNo" name="invoiceNo" >
			</li>
			<li class="range">
				<label>商品总金额:</label>
				<input type="number" placeholder="￥" id="startCommodityAmount" name="startCommodityAmount" >
				-
				<input type="number" placeholder="￥" id="endCommodityAmount" name="endCommodityAmount" >
			</li>
			<li class="range nomargin">
				<label>票据状态:</label>
				<div class="layui-input-inline">
					<select name="acceptInvoiceStatus" id="acceptInvoiceStatus" lay-filter="aihao">
						<option value="0" >待开票据</option>
						<option value="1" >待确认票据</option>
						<option value="2" >已确认票据</option>
						<option value="3" >已驳回票据</option>
						<option value="4" >已取消票据</option>
					</select>
				</div>
			</li>
			<li class="range nomargin">
				<label>票据类型:</label>
				<div class="layui-input-inline">
					<select id="invoiceType" name="invoiceType" lay-filter="aihao">
						<option value="" >全部</option>
						<option value="1" >专用发票</option>
						<option value="2" >普通发票</option>
						<option value="3" >其他发票</option>
					</select>
				</div>
			</li>
			<li class="range nomargin">
				<label>开票日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="createStartDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="createEndDate" id="endDate" lay-verify="date"  class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="rang"><button type="button" class="search" onclick="loadPlatformData();">搜索</button></li>
			<li class="rang"><button type="reset" class="search" onclick="resetformData();">重置</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />
			<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="sellerInvoiceExport();">
				<i class="layui-icon">&#xe8bf;</i> 导出
			</a>
		</ul>
	</form>
		<!--列表区-->
		<table class="table_pure invoice_list">
			<thead>
			<tr>
				<td style="width:15%">开票编号</td>
				<td style="width:15%">采购商名称</td>
				<td style="width:10%">商品总金额</td>
				<td style="width:10%">票据号</td>
				<td style="width:5%">票据类型</td>
				<td style="width:10%">票据金额</td>
				<%--<td style="width:10%">运费</td>--%>
				<td style="width:10%">开票日期</td>
				<td style="width:7%">状态</td>
				<td style="width:8%">操作</td>
			</tr>
			</thead>
				<tbody>
				<c:forEach var="billInvoice" items="${searchPageUtil.page.list}">
					<tr class="text-center">
						<td>${billInvoice.settlementNo}</td>
						<td>${billInvoice.buyCompanyName}</td>
						<td>${billInvoice.totalCommodityAmount}</td>
						<td>${billInvoice.invoiceNo}</td>
						<td>
						  <c:choose>
							<c:when test="${billInvoice.invoiceType == 1}">专用发票</c:when>
							<c:when test="${billInvoice.invoiceType == 2}">普通发票</c:when>
							<c:when test="${billInvoice.invoiceType == 3}">其他发票</c:when>
						  </c:choose>
						</td>
						<td>${billInvoice.totalInvoiceValue}</td>
						<%--<td>${billInvoice.freight}</td>--%>
						<td>${billInvoice.createDateStr}</td>
						<td>
						  <c:choose>
							<c:when test="${billInvoice.acceptInvoiceStatus == 1}">待确认票据</c:when>
							<c:when test="${billInvoice.acceptInvoiceStatus == 2}">已确认票据</c:when>
						    <c:when test="${billInvoice.acceptInvoiceStatus == 3}">已驳回票据</c:when>
						    <c:when test="${billInvoice.acceptInvoiceStatus == 4}">已取消票据</c:when>
						  </c:choose>

						    <%--<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/sellerManualOrderDetails?orderId=${sellerBillReconciliation.sellerDeliveryRecordItem.orderId}','sellers');" class="approval">商品明细</a>--%>
						</td>
						<td>
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showInvoiceReceipt('${billInvoice.settlementNo}','${billInvoice.invoiceFileAddr}');">查看票据</span>
							<%--<div class="bank_view" onclick="recItemList('${billInvoice.settlementNo}','${billInvoice.id}');">商品明细</div>--%>
							<div>
								<a href="javascript:void(0)" class="layui-btn layui-btn-mini" onclick="recItemList('${billInvoice.settlementNo}','${billInvoice.id}');">商品明细</a>
							</div>
							<div>
								<c:choose>
									<c:when test="${billInvoice.acceptInvoiceStatus==3}">
										<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-mini" onclick="leftMenuClick(this,'platform/seller/billInvoice/updateInvoiceJump?invoiceId=${billInvoice.id}','sellers');">再次修改</a>
									</c:when>
								</c:choose>
							</div>
							<c:choose>
								<c:when test="${billInvoice.acceptInvoiceStatus==3}">
									<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-mini" onclick="closeInvoice('${billInvoice.id}');">取消发票</a>
								</c:when>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>


		<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
</div>

<!--商品明细-->
<div id="recItemList" class="interest" style="display:none;">
	<div>
		<table class="table_yellow">
			<thead>
			<tr>
				<td style="width:20%">发货单号</td>
				<td style="width:10%">采购员</td>
				<td style="width:20%">订单号</td>
				<td style="width:20%">商品</td>
				<td style="width:10%">条形码</td>
				<td style="width:5%">单位</td>
				<td style="width:10%">单价</td>
				<td style="width:5%">到货数量</td>
				<%--<td style="width:10%">优惠金额</td>--%>
				<td style="width:10%">总金额</td>
			</tr>
			</thead>
			<tbody id="recItemBody">
			</tbody>
		</table>
	</div>
</div>


<!--发票票据-->
<div id="invoiceReceipt" class="receipt_content layui-layer-wrap" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>