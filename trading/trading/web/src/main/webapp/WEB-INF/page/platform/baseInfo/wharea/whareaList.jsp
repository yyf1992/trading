<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<title>仓库区域管理</title>
<style type="text/css">
.order_search>li .reset{
display:inline-block;
  margin-left:5px;
  background:#F6F5F5;
  color:#333;
  border:1px solid #BFBFBF;
  border-radius:3px;
  width:128px;
  height:25px;
  line-height:25px;
  font-weight:bold;
  text-align:center;
  cursor: pointer;
  outline:0;
}
.defectWare>li input{
    height: 25px;
    width: 25px;
    border: 1px solid #BFBFBF;
    padding-left: 5px;
}
.nomargin{
	position:relative;
	left:20px
}                 
.text-center td{
 word-wrap:break-word;
 word-WRBP: break-word;
}                                               
</style>
<script src="statics/platform/js/wharea.js"></script>
<!-- 仓库列表 -->
<div>
  <form id="searchForm" action="platform/baseInfo/wharea/whareaList">
  <div>
    <ul class="order_search customer_s">
      <li>
        <label>仓库代码:</label>
        <input type="text" placeholder="输入仓库代码" id="Code" name="whareaCode" value="${tBusinessWharea.whareaCode}">
      </li>
      <li>
        <label>仓库名称:</label>
        <input type="text" placeholder="输入仓库名称" id="Name" name="whareaName" value="${tBusinessWharea.whareaName}">
      </li>
      <li>
      	<label>残次品:</label>
      	<input type="checkbox" id="defectWare" name="defectWare" value="是" style="vertical-align:middle; text-align:center; width: 15px ; height: 20px">
      </li>
      <li>
	  	<button type="button" class="search" onclick="loadWhArarList();">搜索</button>
      	<span class="reset" onclick="recovery();">重置</span>  
	  </li>
    </ul>
   </div>
   <table class="table_pure whareaList mt management">
    <thead>
      <tr>
        <td>代码</td>
        <td>名称</td>
        <td>状态</td>
        <td>仓库类型</td>
        <td>创建日期</td>
        <td>残次品仓</td>
      </tr>
      <tr>
        <td colspan="6"></td>
      </tr>
    </thead>
    <tbody>
    	<c:forEach var="tBusinessWhareaList" items="${tBusinessWhareaList}">        	
	       <tr class="text-center">
	        <%--  <td style="vertical-align:middle; text-align:center;"><input type="checkbox" name="checkones" value="${tBusinessWhareaList.id}"></td> --%>
	         <td style="vertical-align:middle; text-align:center;">${tBusinessWhareaList.whareaCode}</td> 
	         <td style="vertical-align:middle; text-align:center;">${tBusinessWhareaList.whareaName}</td>
	         <td style="vertical-align:middle; text-align:center;">
	         	<c:if test="${tBusinessWhareaList.status == 'Y'}">正常</c:if>
	         	<c:if test="${tBusinessWhareaList.status == 'N'}">停用</c:if>
	         </td>
	         <td style="vertical-align:middle; text-align:center;">
	         	<c:if test="${tBusinessWhareaList.whAreaType == '1'}">业务仓</c:if>
	    		<c:if test="${tBusinessWhareaList.whAreaType == '2'}">管理仓</c:if>
	         </td>
	         <td><fmt:formatDate value="${tBusinessWhareaList.createDate}" type="both"/></td>
	         <td style="vertical-align:middle; text-align:center;">${tBusinessWhareaList.defectWare}</td>
	      <!--     <td>
	           <a href="javascript:void(0)" onclick="updateWharea(this);" class="layui-btn layui-btn layui-btn-mini">修改</a>
	           <a onclick="deleteWharea('${tBusinessWhareaList.id}');" class="customer_d layui-btn layui-btn-danger layui-btn-mini">删除</a>
	         </td> -->
	       </tr>
        </c:forEach>
    </tbody>
   </table>
   </form>
   
   <!-- 新整页面 -->
<div class="plain_frame add_wharea" style="display: none;">
	<form action="">
      	<table class="layui-table">
      		<tr>
      			<td>
      				<label>仓库代码:</label>
        			<input type="text" placeholder="输入仓库代码" id="add_Code" name="whareaCode"  style="width: 200px ; height: 30px">
      			</td>
      		</tr>
      		<tr>
      			<td>
      				<label>仓库名称:</label>
        			<input type="text" placeholder="输入仓库名称" id="add_Name" name="whareaName" style="width: 200px ; height: 30px">
      			</td>
      		</tr>
 			<tr>
      			<td>
      				<label>仓库类型:</label>
        			<select id="add_whAreaType"  name="whAreaType" style="width: 200px ; height: 30px">
        				<option disabled selected>请选择</option>
						<option>业务仓</option>
						<option>管理仓</option>
	        		</select>
      			</td>
      		</tr>
      		<tr>
      			<td>
      				<label>仓库状态:</label>
        			<select name="status" id="add_status" style="width: 200px ; height: 30px">
	        			<option>正常</option>
	        			<option>停用</option>
	        		</select>
      			</td>
      		</tr>
      		<tr>
      			<td>
      				<label>残次品	:</label>
        			<input type="checkbox" id="add_defect" name="defectWares" title="残次品" value="是" style="width: 50px ; height: 20px">
      			</td>
      		</tr>
      	</table>
      </form>
</div>

<!-- 修改页面 -->
<div class="plain_frame update_wharea" style="display: none;">
	<form action="">
      	<table class="layui-table upWharea">
      	
      		<tr>
      			<td>
      				<label>仓库代码:</label>
        			<input type="text" placeholder="输入仓库代码" id="update_whareaCode" name="update_whareaCode" value="" style="width: 180px ; height: 25px">
      			</td>
      		</tr>
      		<tr>
      			<td>
      				<label>仓库名称:</label>
        			<input type="text" placeholder="输入仓库名称" id="update_whareaName" name="update_whareaName" value="" style="width: 180px ; height: 25px">
      			</td>
      		</tr>
 			<tr>
      			<td>
      				<label>仓库类型:</label>
        			<select id="update_whAreaType"  name="update_whAreaType" style="width: 180px ; height: 25px">
        					<option>管理仓</option>
        					<option>业务仓</option>
	        		</select>
      			</td>
      		</tr>
      		<tr>
      			<td>					
      				<label>仓库状态:</label>
        			<select name="update_status" id="update_status" style="width: 180px ; height: 25px">
        					<option>正常</option>
        					<option>停用</option>
	        		</select>
      			</td>
      		</tr>
      		<tr>
      			<td>
      				<label>残次品	:</label>
        			<input type="checkbox" id="update_defect" name="update_defect" title="残次品" style="width: 50px ; height: 20px; vertical-align:middle; text-align:center;">
      			</td>
      		</tr>

      	</table>
      </form>
</div>
</div>

