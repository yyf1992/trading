<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script type="text/javascript">
//为选择的商品赋值
function chooseThis(skuId,trId){
	var checkOb=$("input[name='sku']:checked");
	var td=checkOb.parent().parent().find("td");
	$("#"+trId+" :nth-child(2) .goodsIpt").val(td.eq(2).text());
	$("#"+trId+" :nth-child(3) .goodsIpt").val(td.eq(3).text());
	$("#"+trId+" :nth-child(4) .goodsIpt").val(td.eq(4).text());
	$("#"+trId+" :nth-child(5) .goodsIpt").val(td.eq(5).text());
	$("#"+trId+" :nth-child(6) .goodsIpt").val(td.eq(6).text());
	$("#"+trId+" :nth-child(7) .goodsIpt").val(td.eq(7).text());
	$("#"+trId+" input[type='hidden']").val(td.eq(0).find("input[type='radio']").val());
	//去除错误提示
	$("#"+trId).removeClass("error"); 
	$("#"+trId+" :nth-child(4)").removeClass("error"); 
	$("#"+trId+" :nth-child(4) span").remove(); 
	layer.closeAll();
}

function selectProduct(obj){
	$.ajax({
		url:"platform/buyer/sysshopproduct/selectGoods",
		type:"get",
		data:$("#skuForm").serialize(),
		async:false,
		success:function(data){
			$(".commPop").html(data);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}
</script>

<!--商品弹框-->
<div class="commPop">
<form action="platform/buyer/sysshopproduct/selectGoods" id="skuForm">
  <div class="materialQuery mt">
    <input type="text" placeholder="货号/商品名称/条形码" id="selectValue" name="selectValue" value="${searchPageUtil.object.selectValue}">
    <img src="statics/platform/images/find.jpg" class="" onclick="selectProduct();">
    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
  	<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
  	<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
  	<input id="id" name="trId" type="hidden" value="${searchPageUtil.object.trId}" />
  </div>
</form>
  <div class="materialContent">
    <table class="table_pure">
      <thead>
      <tr>
        <td style="width:5%"></td>
        <td style="width:5%">序号</td>
        <td style="width:25%">商品名称</td>
        <td style="width:10%">货号</td>
        <td style="width:15%">条形码</td>
        <td style="width:15%">规格名称</td>
        <td style="width:15%">规格代码</td>
        <td style="width:10%">成本价</td>
      </tr>
      </thead>
      <tbody id="productTbody">
      <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
	      <tr>
	        <td><input type="radio" value="${item.id}" name="sku" onclick="chooseThis('${item.id}','${searchPageUtil.object.trId}');"></td>
	        <td>${status.count}</td>
	        <td>${item.productName}</td>
	        <td>${item.productCode}</td>
	        <td>${item.barcode}</td>
	        <td>${item.skuName}</td>
	        <td>${item.skuCode}</td>
	        <td><fmt:formatNumber type="number" pattern="0.00" value="${item.price}"/></td>
	      </tr>
	  </c:forEach>
      </tbody>
    </table>
    <div class="pager" id="page">${searchPageUtil.page}</div>
  </div>
</div>