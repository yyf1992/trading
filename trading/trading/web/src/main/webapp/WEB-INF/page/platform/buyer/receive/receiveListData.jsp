<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../common/path.jsp"%>
<script src="<%=basePath%>/statics/platform/js/common.js"></script>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<script src="<%=basePath%>/js/delivery/receiveListData.js"></script>
<%--收货单列表数据--%>
<table class="order_detail">
	<tr>
		<td style="width:90%">
			<ul>
				<li style="width:25%">商品</li>
				<li style="width:15%">订单号</li>
				<li style="width:5%">单位</li>
				<li style="width:8%">发货数量</li>
				<li style="width:8%">到货数量</li>
				<li style="width:8%">单价</li>
				<li style="width:8%">总价</li>
				<!-- <li style="width:8%">发货类型</li> -->
				<li style="width:18%">备注</li>
			</ul>
		</td>
		<td style="width:7%">状态</td>
		<td style="width:7%">操作</td>
	</tr>
</table>
<c:forEach var="receive" items="${searchPageUtil.page.list}">
	<div class="order_list" id="dataList">
		<p>
			<span class="order_num layui-col-sm2">发货单号: <b>${receive.deliverNo}</b></span>
			<span class="order_num layui-col-sm3">供应商: <b>${receive.supplierName}</b></span>
			<span class="apply_time layui-col-sm2">发货日期:<fmt:formatDate value="${receive.createDate}" type="date"></fmt:formatDate></span>
			<c:if test="${not empty receive.arrivalDate}">
				<span class="apply_time layui-col-sm2">到货日期:<fmt:formatDate value="${receive.arrivalDate}" type="date"></fmt:formatDate></span>
			</c:if>
			<span class="apply_time layui-col-sm1">仓库:${receive.warehouseName}</span>
			<span class="apply_time layui-col-sm1">发货类型:<c:if test="${receive.dropshipType == 0}">正常发货</c:if><c:if test="${receive.dropshipType == 1}">代发客户</c:if><c:if test="${receive.dropshipType == 2}">代发菜鸟仓</c:if><c:if test="${receive.dropshipType == 3}">代发京东仓</c:if> 
			</span>
		</p>
		<table>
			<tr>
				<td style="width:90%">
					<c:forEach var="item" items="${receive.itemList}" varStatus="i">
						<ul class="clear">
							<li style="width:25%">
								${item.productCode}|${item.productName}|${item.skuCode}|${item.skuName}
							</li>
							<li style="width:15%">
								<c:forEach var="orderCode" items="${fn:split(item.orderCode,',')}" >
									<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/buyOrder/showOrderDetail?orderCode=${orderCode}'+
											'&returnUrl=platform/buyer/receive/receiveList&menuId=17070718440469618695','buyer');" style="color: blue;">${orderCode}</a><br/>
								</c:forEach>
							</li>
							<li style="width:5%">
								<c:choose>
									<c:when test="${item.unitName != null}">${item.unitName}</c:when>
									<c:otherwise>件</c:otherwise>
								</c:choose>
							</li>
							<li style="width:8%">${item.deliveryNum}</li>
							<li style="width:8%;text-align: center;">${item.arrivalNum}</li>
							<li style="width:8%">${item.salePrice}</li>
							<li style="width:8%">${item.deliveryNum*item.salePrice}</li>
							<%-- <li style="width:8%">
								<c:choose>
									<c:when test="${item.status == '0'}">全部发货</c:when>
									<c:otherwise>部分发货</c:otherwise>
								</c:choose>
							</li> --%>
							<li style="width:18%">${item.remark}</li>
						</ul>
					</c:forEach>
				</td>
				<td style="width:7%">
					<c:choose>
						<c:when test="${receive.recordstatus == '0'}">待收货</c:when>
						<c:when test="${receive.recordstatus == '1'}">已收货</c:when>
						<c:otherwise>待收货</c:otherwise>
					</c:choose>
					<c:if test="${not empty receive.receiptVoucherList && fn:length(receive.receiptVoucherList) >0}">
						<br/><a href="javascript:void(0);"  class="approval" onclick="receiptAttachmentPreview('${receive.deliverNo}');">查看收货单</a>
					</c:if>
					<br/><a href="javascript:void(0);" onclick="openConfirmReceipt('${receive.id}','${receive.isFromWms}','${receive.recordstatus}');" class="approval">发货详情</a>
					<br/>
					<c:choose>
						<c:when test="${receive.reconciliationStatus == '0'}">未对账</c:when>
						<c:when test="${receive.reconciliationStatus == '1'}">对帐中</c:when>
						<c:when test="${receive.reconciliationStatus == '2'}">已对账</c:when>
						<c:otherwise>未对账</c:otherwise>
					</c:choose>
				</td>
				<td style="width:7%">
					<c:choose>
						<c:when test="${receive.recordstatus == '0'  && receive.isFromWms == 'N'}">
							<%--<a href="javascript:void(0);" onclick="openConfirmReceipt('${receive.id}','${receive.isFromWms}','${receive.recordstatus}');" class="layui-btn layui-btn-normal layui-btn-mini"><i class='layui-icon'>&#xe63c;</i>确认收货</a><br/>--%>
						</c:when>
						<c:when test="${receive.recordstatus == '1'}">
							<%--<c:if test="${empty receive.receiptVoucherList || fn:length(receive.receiptVoucherList)<=0}">
								<a href="javascript:void(0);" onclick="openUploadReceipt('${receive.deliverNo}');" class="layui-btn layui-btn-mini layui-btn-normal"><i class='layui-icon'>&#xea5d;</i>上传收货单</a><br/>
							</c:if>--%>
							<a href="javascript:void(0);" onclick="openUploadReceipt('${receive.deliverNo}');" class="layui-btn layui-btn-mini layui-btn-normal"><i class='layui-icon'>&#xea5d;</i>上传收货单</a><br/>
							<c:if test="${not empty receive.qrcodeAddr}">
								<a href="javascript:void(0);" onclick="showQrcode('${receive.qrcodeAddr}');" class="layui-btn layui-btn-mini layui-btn-normal"><i class='layui-icon'>&#xe909;</i>扫码上传</a><br/>
							</c:if>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${receive.warehouseName=='代发仓' && receive.recordstatus==1 && receive.pushType=='F'}">
							<a href="javascript:void(0);" onclick="pushOMS('${receive.id}');" 
								class="layui-btn layui-btn-primary layui-btn-mini"><i class='layui-icon'>&#xe763;</i>推送oms出库</a><br/>
						</c:when>
					</c:choose>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
<%--收货单上传弹出框--%>
<div id="uploadReceiptDiv" style="display: none;">
	<p>
		<span>发货单号：</span><span id="uploadDeliverNo"></span>
	</p>
	<label>收货回单:</label>
	<div class="upload_system">
		<input type="file" name="uploadReceipt" id="uploadReceipt" onchange="uploadReceipt();">
		<p></p>
		<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
	</div>
	<div class="scanning_copy original" id="addAttachmentDiv"></div>
</div>

<%--收货回单预览--%>
<div class='contractView layer-photos-demo' id='viewReceiptDiv' style="display: none;">
	<%--<div class="big_img" >
		<img id="bigImg" style="width: 700px;height: 500px;">
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>--%>
</div>

<div id='qrcodeDiv' style="display: none;">
	<img id="qrcodeImg" src="" style="width: 300px;height: 300px;"/>
</div>