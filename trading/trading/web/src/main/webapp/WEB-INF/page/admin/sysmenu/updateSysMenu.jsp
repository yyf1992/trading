<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$("#updateSysMenuForm").validate({
	rules:{
		name:{
			required: true,
			maxlength:20
		},
		isMenu:{
			required: true
		}
	},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.form-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.form-group').removeClass('error');
		$(element).parents('.form-group').addClass('success');
	},
	submitHandler : function(){
		$.ajax({
		  	url : "admin/sysMenu/saveUpdate",
		  	type: "post",
		  	data:$("#updateSysMenuForm").serialize(),
		  	async:false,
		  	success:function(data){
		  		var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					loadAdminData();
					//阻止表单提交
					return false;
				} else {
					layer.msg(resultObj.msg,{icon:2});
					//阻止表单提交
					return false;
				}
		  	},
		  	error:function(){
		  		layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  		//阻止表单提交
				return false;
		  	}
		});
	}
});

$("#isMenu").change(function(){
	setDiv();
});
function setDiv(){
	var ismenu = $("#isMenu").val();
	if(ismenu == 0){
		//菜单
		$("#iconDiv").show();
		$("#addressDiv").show();
	}else{
		//按钮
		$("#iconDiv").hide();
		$("#addressDiv").hide();
	}
}
</script>
	<form class="form-inline " id="updateSysMenuForm" novalidate="novalidate">
<div class="modal-content" style="width: 600px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel">修改系统菜单</h4>
	</div>
	<div class="modal-body row-fluid ">
		<div class=" padding-top-sm row">
				<div class="form-group col-md">
					<label for="exampleInputEmail2" class="label-two">名称：</label> 
					<input value="${sysMenu.name}"
						type="text" class="form-control-new" name="name" placeholder="名称" style="width: 60%">
				</div>
				<div class="form-group col-md padding-top-sm">
					<label class="label-four">父级菜单：</label>
					<select class="form-control-new" name="parentId" style="width: 60%" disabled="disabled">
						<option value="">请选择</option>
						<c:forEach var="sysMenuSelect" items="${parentMenuList}">
							<c:if test="${sysMenu.parentId == sysMenuSelect.id}">
								<option value="${sysMenuSelect.id}" selected>
							</c:if>
							<c:if test="${sysMenu.parentId != sysMenuSelect.id}">
								<option value="${sysMenuSelect.id}">
							</c:if>
								<!-- 有子级 -->
								<c:forEach begin="2" end="${sysMenu.level}">&nbsp;&nbsp;&nbsp;</c:forEach>
								${sysMenuSelect.name}
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group col-md padding-top-sm">
					<label class="label-two">状态：</label>
					<c:if test="${sysMenu.status == 0}">
							<input type="radio" name="status" checked value="0"/>启用
							<input type="radio" name="status" value="1"/>禁用
						</c:if>
						<c:if test="${sysMenu.status == 1}">
							<input type="radio" name="status"  value="0"/>启用
							<input type="radio" name="status" checked value="1"/>禁用
						</c:if>
				</div>
			<c:if test="${sysMenu.isMenu == 0}">
				<div class="form-group col-md padding-top-sm" id="iconDiv">
					<label for="exampleInputEmail2" class="label-two">样式：</label>
					<span id="iconHideSpan" style="display: none;">${sysMenu.icon}</span>
					<input type="text" class="form-control-new" name="icon" placeholder="图片样式" style="width: 60%">
					<script type="text/javascript">
					var buttonStr = $("#iconHideSpan").html();
					$("input[name='icon']").val(buttonStr);
				</script>
				</div>
			</c:if>
			<c:if test="${sysMenu.isMenu == 0}">
				<div class="form-group col-md padding-top-sm" id="addressDiv">
					<label for="exampleInputEmail2" class="label-two">地址：</label>
					<input value="${sysMenu.url}"
						type="text" class="form-control-new" name="url" style="width: 60%"
						placeholder="地址">
				</div>
			</c:if>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
		<button type="submit" class="btn btn-sm2 btn-00967b" id="saveButton">保存</button>
	</div>
</div>
	<!--隐藏域  -->
	<input type="hidden" name="id" value="${sysMenu.id}">
	<input type="hidden" name="isMenu" value="${sysMenu.isMenu}">
	<input type="hidden" name="parentId" value="${sysMenu.parentId}">
</form>
