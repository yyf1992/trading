<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/common.jsp"%>
<div>
	<div class="order_success">
		<img src="${basePath}statics/platform/images/ok.png">
		<h3>恭喜您提交申请成功！</h3>
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'buyer/manualOrder/manualOrderList','buyer')" class="purchase">转到手工录入的订单</a>
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'buyer/manualOrder/manualOrderDetails?orderId=${orderId}','buyer')" class="view_details">查看详情</a>
		<a href="buyer/manualOrder/printOrder?orderId=${orderId}" class="print" target="_blank">打印采购单</a>
	</div>
</div>
