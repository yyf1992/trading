<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>公司管理</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ include file="../../platform/common/path.jsp"%>
<%@ include file="../resources.jsp"%>
<script type="text/javascript">
$(function(){
	//全选
	$("#checkAll").change(function(){
		var status = $(this).is(":checked");
		if(status){
			$("input:checkbox[name='checkone']").prop("checked",status);
		}else{
			$("input:checkbox[name='checkone']").prop("checked",status);
		}
	});
});
//停用
function updateCompanyIsDel(id,isDel){
	var str = "";
	if(isDel == "0"){
		str = "确认启用？启用后关联人员也会启用！";
	}else if(isDel == "1"){
		str = "确认停用？停用后关联人员也会停用！";
	}
	layer.confirm(str, {
	  btn: ["确定","取消"] //按钮
	}, function(){
		$.ajax({
			url : basePath+"admin/adminCompany/updateCompanyIsDel.html",
			data:{
				"id" : id,
				"isDel" : isDel
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					loadAdminData();
				} else {
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
}
//设置公司角色
function instalCompanyRole(obj){
	var checked = $("input:checkbox[name='checkone']:checked");
	if(checked.length != 1){
		layer.msg("请选择一条数据！",{icon:7});
		return;
	}
	var isDel = checked.parent().parent().find("td:eq(8)").find("input").val();
	if(isDel == "1"){
		layer.msg("公司已停用，不允许设置角色！",{icon:7});
		return;
	}
	var status = checked.parent().parent().find("td:eq(10)").find("input").val();
	if(status != "1"){
		layer.msg("公司待审核或审核不通过，不允许设置角色！",{icon:7});
		return;
	}
	$.ajax({
		url : basePath+"admin/adminCompany/instalCompanyRole.html",
		data:{"id":checked.val()},
		success:function(data){
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
// 审核
function Verify(id) {
	$.ajax({
		url : basePath+"admin/adminCompany/verifyCompany.html",
		data : {
			"id" : id
		},
		success : function(data) {
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
// 查看
function loadDetails(id){
	$.ajax({
		url : basePath+"admin/adminCompany/loadDetails.html",
		data : {
			"id" : id
		},
		success : function(data) {
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
</head>
<body style="background-color: white;">
	<!--内容-->
<div id="content">
	<div class="container-fluid">
		<div class="row-fluid">
			<!--搜索条件 start-->
			<div class="form-serch">
				<div class="widget-box">
					<div class="widget-content" style="padding-bottom:5px">
						<form class="form-inline clearfix" id="selectForm"
							action="<%=basePath %>admin/adminCompany/loadCompanyList.html?">
							<div class="form-group">
								<label for="exampleInputName2">编号：</label> <input type="text"
									class="form-control" id="companyCode" name="companyCode" placeholder="编号"
									value="${searchPageUtil.object.companyCode}">
							</div>
							<div class="form-group">
								<label for="exampleInputName2">公司：</label> <input type="text"
									class="form-control" id="companyName" name="companyName" placeholder="公司"
									value="${searchPageUtil.object.companyName}">
							</div>
							<div class="form-group">
								<label for="exampleInputName2">联系人：</label> <input type="text"
									class="form-control" id="linkMan" name="linkMan" placeholder="联系人"
									value="${searchPageUtil.object.linkMan}">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-sm2 btn-success " id="selectButton"
									onclick="loadAdminData();">查询</button>
							</div>
							<!-- 分页隐藏数据 -->
							<input id="pageNo" name="pageAdmin.pageNo" type="hidden"
								value="${searchPageUtil.pageAdmin.pageNo}" /> <input id="pageSize"
								name="pageAdmin.pageSize" type="hidden"
								value="${searchPageUtil.pageAdmin.pageSize}" />
						</form>
					</div>
				</div>
			</div>
			<!--搜索条件 end-->
			<!--tableys start-->
			<div class="widget-box">
				<div class="widget-title title-lg">
					<span class="icon"> <i class="icon-th"></i> </span>
					<h5>对系统公司进行操作</h5>
					<div class="pull-left" style="margin-top: 8px;">
						<!-- <button class="btn btn-info" onclick="updateSysUser(this)">修改</button>
						<button class="btn btn-warning" onclick="stopCompany(this)">停用</button> -->
						<button class="btn btn-success" onclick="instalCompanyRole(this)">设置角色</button>
					</div>
				</div>
				<div class="widget-content nopadding" id="dataList">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
						role="grid">
						<table class="table table-bordered table-striped with-check">
							<thead>
								<tr>
									<th>
										<input type="checkbox" id="checkAll" name="title-table-checkbox" />
									</th>
									<th>编号</th>
									<th>名称</th>
									<th>详细地址</th>
									<th>联系人</th>
									<th>登录账号</th>
									<th>电话</th>
									<th>email</th>
									<th>是否停用</th>
									<th>注册时间</th>
									<th>状态</th>
									<th>审核时间</th>
									<th>审核意见</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody id="companyTbody">
								<c:forEach var="company" items="${searchPageUtil.pageAdmin.list}">
									<tr class="text-center">
										<td><input type="checkbox" name="checkone" value="${company.id}" /></td>
										<td>${company.companyCode}</td> 
										<td>${company.companyName}</td>
										<td>${company.detailAddress}</td>
										<td>${company.linkMan}</td>
										<td>${company.fax1}</td>
										<td>${company.zone} - ${company.telNo}</td>
										<td>${company.email}</td>
										<td>
											<c:choose>
												<c:when test="${company.isDel==0}">启用</c:when>
												<c:when test="${company.isDel==1}">停用</c:when>
											</c:choose>
											<input type="hidden" value="${company.isDel}"/>
										</td>
										<td><fmt:formatDate value="${company.createDate}" type="both"/></td>
										<td>
											<c:choose>
												<c:when test="${company.status==0}">待审核</c:when>
												<c:when test="${company.status==1}">已通过</c:when>
												<c:when test="${company.status==2}">未通过</c:when>
											</c:choose>
											<input type="hidden" value="${company.status}"/>
										</td>
										<td><fmt:formatDate value="${company.verifyDate}" type="both"/></td>
										<td>${company.reason}</td>
										<td>
											<input type="button" class="btn btn-sm2 btn-success" value="查看" onclick="loadDetails('${company.id}');"/>
											<c:if test="${company.status == 0}">
												<input type="button" class="btn btn-sm2 btn-warning" value="审核" onclick="Verify('${company.id}');"/>
											</c:if>
											<c:if test="${company.isDel==1}">
												<input type="button" class="btn btn-sm2 btn-info" value="启用" onclick="updateCompanyIsDel('${company.id}','0')">
											</c:if>
											<c:if test="${company.isDel==0}">
												<input type="button" class="btn btn-sm2 btn-danger" value="停用" onclick="updateCompanyIsDel('${company.id}','1')">
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div id="page">${searchPageUtil.pageAdmin }</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal fade bs-example-modal-lg" id="modalDiv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document" id="modalDialogDiv"></div>
	</div>
</body>
</html>