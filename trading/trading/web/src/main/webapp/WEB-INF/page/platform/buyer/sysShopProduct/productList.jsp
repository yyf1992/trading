<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script type="text/javascript">
//为选择的商品赋值
function chooseThis(id,unitId){
	var checkOb=$("input[name='product']:checked");
	var td=checkOb.parent().parent().find("td");
	$("#"+id+" :nth-child(1) .searchList .goodsIpt").val(td.eq(3).text());
	$("#"+id+" :nth-child(2) .goodsIpt").val(td.eq(2).text());
	$("#"+id+" :nth-child(3) .goodsIpt").val(td.eq(6).text());
	$("#"+id+" :nth-child(4) .goodsIpt").val(td.eq(4).text());
	$("#"+id+" :nth-child(5) .goodsIpt").val(td.eq(5).text());
	$("#"+id+" :nth-child(6) .goodsIpt").val(td.eq(7).html());
	$("#"+id+" :nth-child(6) .unitId").val(unitId);
	$("#"+id+" input[type='hidden']").val(td.eq(0).find("input[type='radio']").val());
	//去除错误提示
	$("#"+id+" :nth-child(3)").removeClass("error");
	layer.closeAll();
}

function selectProduct(obj){
	$.ajax({
		url:"platform/buyer/sysshopproduct/loadProductData",
		type:"get",
		data:$("#proForm").serialize(),
		async:false,
		success:function(data){
			$(".commPop").html(data);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}
</script>

<!--商品弹框-->
<div class="commPop">
<form action="platform/buyer/sysshopproduct/loadProductData" id="proForm">
  <div class="materialQuery mt">
    <input type="text" placeholder="货号/商品名称/条形码" id="inputSelect" name="inputSelect" value="${searchPageUtil.object.inputSelect}">&emsp;
    <img src="statics/platform/images/find.jpg" class="" onclick="selectProduct();">
    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
  	<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
  	<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
  	<input id="id" name="productId" type="hidden" value="${searchPageUtil.object.productId}" />
  	<input id="barcodeStr" name="barcodeStr" type="hidden" value="${searchPageUtil.object.barcodeStr}" />
  </div>
</form>
  <div class="materialContent">
    <table class="table_pure">
      <thead>
      <tr>
        <td style="width:5%"></td>
        <td style="width:5%">序号</td>
        <td style="width:15%">货号</td>
        <td style="width:20%">商品名称</td>
        <td style="width:17%">规格代码</td>
        <td style="width:17%">规格名称</td>
        <td style="width:15%">条形码</td>
        <td style="width:6%">单位</td>
      </tr>
      </thead>
      <tbody id="productTbody">
      <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
	      <tr>
	        <td><input type="radio" value="${item.id}" name="product" onclick="chooseThis('${searchPageUtil.object.productId}','${item.unit_id}');"></td>
	        <td>${status.count}</td>
	        <td>${item.product_code}</td>
	        <td>${item.product_name}</td>
	        <td>${item.sku_code}</td>
	        <td>${item.sku_name}</td>
	        <td>${item.barcode}</td>
	        <td>${item.unit_name}</td>
	      </tr>
	  </c:forEach>
      </tbody>
    </table>
    <div class="pager" id="page">${searchPageUtil.page}</div>
  </div>
</div>