<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/commodity_all.css"/>
<link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/common.css"/>
<style>
    .cc:after{
        content: '';
        display: block;
        clear: both;
    }
    .cc input,.cc select{
        width:80px;
        height: 20px;
        margin-bottom: 5px;
    }
    .materialContent{
        overflow-y: auto;
    }
}
</style>
<script type="text/javascript">
    function searchProductSku() {
        $.ajax({
            url:"buyer/applyPurchaseHeader/loadAllProductList",
            data:$("#productLinkForm").serialize(),
            async:false,
            success:function(data){
                $(".productLinkDiv").html(data);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！", {icon : 2});
            }
        });
    }
    //为选择的商品赋值
function setProductInfo(obj){
	var productCode=$(obj).parent().parent().find("td")[1].innerText;
    var productName=$(obj).parent().parent().find("td")[2].innerText;
    var skuCode=$(obj).parent().parent().find("td")[3].innerText;
    var skuName=$(obj).parent().parent().find("td")[4].innerText;
    var skuOid=$(obj).parent().parent().find("td")[5].innerText;
    var unitId = $(obj).parent().parent().find("td:eq(6)").find("input").val();
    var unitName = $(obj).parent().parent().find("td")[6].innerText;
    var productType = $(obj).parent().parent().find("td:eq(8)").find("input").val();
    var selectProductId = $("#selectProductId").val();
    $("#"+selectProductId).find("[name='productCode']").val(productCode);
    $("#"+selectProductId).find("[name='productName']").val(productName);
    $("#"+selectProductId).find("[name='skuCode']").val(skuCode);
    $("#"+selectProductId).find("[name='skuName']").val(skuName);
    $("#"+selectProductId).find("[name='skuOid']").val(skuOid);
    $("#"+selectProductId).find("[name='productType']").val(productType);
    $("#"+selectProductId).find("[name='unitId']").val(unitId);
    $("#"+selectProductId).find("[name='unitName']").val(unitName);
	layer.closeAll();
}
</script>
<!--商品弹框-->
<div class="productLinkDiv">
    <form action="buyer/applyPurchaseHeader/loadAllProductList" id="productLinkForm">
        <div class="cc mt">
            <span>商品货号:</span> <input type="text" id="searchProductCode" name="searchProductCode" value="${searchPageUtil.object.searchProductCode}" class="mr" placeholder="商品货号">
            <span>商品名称:</span> <input type="text" id="searchProductName" name="searchProductName" value="${searchPageUtil.object.searchProductName}" class="mr" placeholder="商品名称">
            <span>条 形 码:</span> <input type="text" id="searchBarCode" name="searchBarCode" value="${searchPageUtil.object.searchBarCode}" class="" placeholder="条形码">
            <span>规格代码:</span> <input type="text" id="searchSkuCode" name="searchSkuCode" value="${searchPageUtil.object.searchSkuCode}" class="mr" placeholder="规格代码">
            <span>规格名称:</span> <input type="text" id="searchSkuName" name="searchSkuName" value="${searchPageUtil.object.searchSkuName}" class="mr" placeholder="规格名称">
            <img src="<%=basePath %>/statics/platform/images/find.jpg" onclick="searchProductSku();" class="rt">
            <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
            <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
            <input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
            <input type="hidden" id="selectProductId" name="selectProductId" value="${searchPageUtil.object.selectProductId}"/>
        </div>
    </form>
    <div class="materialContent" style="width: 100%; overflow: auto;">
        <table class="table_blue" id="selectProduct">
            <thead>
            <tr>
                    <td style="width:45px"></td>
                    <td style="width:100px">货号</td>
                    <td style="width:150px">商品</td>
                    <td style="width:150px">规格代码</td>
                    <td style="width:150px">规格名称</td>
                    <td style="width:120px">条形码</td>
                    <td style="width:50px">单位</td>
                    <td style="width:80px">价格</td>
                    <td style="width:80px">商品类型</td>
                </tr>
            </thead>
            <tbody id="productBody">
        	<c:forEach var="product" items="${searchPageUtil.page.list}">
        		<tr>
	                <td>
	                	<input type="radio" value='${product.id}' name="checkOne" onclick="setProductInfo(this);">
	                </td>
	                <td>${product.productCode}</td>
	                <td>${product.productName}</td>
	                <td>${product.skuCode}</td>
	                <td style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">${product.skuName}</td>
	                <td title='${product.barcode}'>${product.barcode}</td>
	                <td><input type='hidden' value='${product.unitId}'>${product.unitName}</td>
	                <td>${product.price}</td>
	                <td><c:choose>
                            <c:when test="${product.productType=='0'}">成品</c:when>
                            <c:when test="${product.productType=='1'}">原材料</c:when>
                            <c:when test="${product.productType=='2'}">辅料</c:when>
                            <c:when test="${product.productType=='3'}">虚拟产品</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                        <input type="hidden" value="${product.productType}">
                    </td>
				</tr>
        	</c:forEach>
        </tbody>
        </table>
    </div>
    <div class="pager" id="page">${searchPageUtil.page}</div>
</div>