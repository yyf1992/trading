<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%-- <%@ include file="../../common/common.jsp"%> --%>
<%@ include file="../../common/path.jsp"%>
<script src="<%=basePath%>statics/plugins/layer/layer.js"></script>
<script src="<%=basePath%>statics/platform/js/supplier.js"></script>
<script type="text/javascript">
$(function() {
	loadProvince("province","");
	// 根据省份加载市
	var provinceId = $("#province").val();
	loadCity(provinceId,"city","");
	// 根据市加载区
	var cityId = $("#city").val();
	loadArea(cityId,"area","");
	//省改变
	$("#province").change(function(){
		// 根据省份加载市
		var provinceId = $("#province").val();
		loadCity(provinceId,"city","");
		// 根据市加载区
		var cityId = $("#city").val();
		loadArea(cityId,"area","");
	});
	//市改变
	$("#city").change(function() {
		// 根据市加载区
		var cityId = $("#city").val();
		loadArea(cityId,"area","");
	});
	// 提交保存
	$(".next_step").click(function(){
		var suppName = $("#suppName").val();
		if(suppName == ''){
			layer.msg("请填写公司名称！",{icon:7});
			return;
		}
		var province = $("#area").val();
		if(province == ''){
			layer.msg("请填写所在地！",{icon:7});
			return;
		}
		var address = $("#address").val();
		if(address == ''){
			layer.msg("请填写详细地址！",{icon:7});
			return;
		}
		// 附件信息
		var a1 = $("#attachment1Div").find("span").length;
		var a1Str = ""
		if(a1 != 0){
			for(var i = 0;i < a1;i++){
				var url1 = $("#attachment1Div").find("span:eq("+i+")").find("input").val();
				a1Str = a1Str + url1 + "@";
			}
			a1Str = a1Str.substring(0, a1Str.length - 1);
		}
		// 联系人字符串拼接
		var linkmanStr = new Array();
		$("#linkmanTbody tr").each(function() {
			var person = $(this).find("td:eq(1)").find("input").val();
			var phone = $(this).find("td:eq(2)").find("input").val();
			var zone = $(this).find("td:eq(3)").find("input:eq(0)").val();
			var telNo = $(this).find("td:eq(3)").find("input:eq(1)").val();
			var fax = $(this).find("td:eq(4)").find("input").val();
			var qq = $(this).find("td:eq(5)").find("input").val();
			var wangNo = $(this).find("td:eq(6)").find("input").val();
			var email = $(this).find("td:eq(7)").find("input").val();
			var isDefault = $(this).find("td:eq(8)").find("input").val();
			var item = new Object();
			item.person = person;
			item.phone = phone;
			item.zone = zone;
			item.telNo = telNo;
			item.fax = fax;
			item.qq = qq;
			item.wangNo = wangNo;
			item.email = email;
			item.isDefault = isDefault;
			linkmanStr.push(item);
		});

		$.ajax({
			type : "POST",
			url : "platform/buyer/supplier/saveAddSupplier",
			async: false,
			data : {
				"suppName" : suppName,
				"province" : province,
				"city" : $("#city").val(),
				"area" : $("#area").val(),
				"address" : address,
				"bankNo" : $("#bankNo").val(),
				"taxpayerNo" : $("#taxpayerNo").val(),
				"a1Str" : a1Str,
				"linkmanStr" : JSON.stringify(linkmanStr)
			},
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					layer.msg("保存成功！",{icon:1});
					leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
});
// 设置默认联系人
function setIsDefault(obj){
	$("#linkmanTbody tr").each(function() {
		$(this).find("td:eq(8)").find("input").val("0");
	});
	$(obj).parents("tr").find("td:eq(8)").find("input").val("1");
}
</script>
<div class="add_supplier">
	<form action="">
		<div class="form_group">
			<label class="control_label"><span class="red">*</span> 供 应 商 名 称:</label>
			<input type="text" id="suppName" name="suppName" class="form_control" verif="required" placeholder="请输入公司名称">
		</div>
		<div class="form_group">
			<label class="control_label"><span class="red">*</span> 所 在 地:</label>
			<div class="info_city">
				<select id="province"></select>
				<select id="city"></select>
				<select id="area"></select>
			</div>
		</div>
		<div class="form_group">
			<label class="control_label"><span class="red">*</span> 详 细 地 址:</label>
			<textarea type="text" class="form_control_t" required name="address" id="address" 
				placeholder="请如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息" style="width:313px"></textarea>
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;营业证件扫描件:</label>
			<div class="upload_license">
				<input type="file" name="attachment1" id="attachment1" onchange="fileUpload(this);">
				<p></p>
				<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
			</div>
			<div class="img_license" id="attachment1Div"></div>
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;银 行 账 号:</label> <input
				type="text" class="form_control" placeholder="请输入银行账号" name="bankNo" id="bankNo">
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;纳税人识别号:</label> <input
				type="text" class="form_control" name="taxpayerNo" id="taxpayerNo" placeholder="请输入纳税人识别号">
		</div>
		<div class="setting">
			<h4>联系方式设置:</h4>
			<div class="btn_add mt">
				<a href="#" class="btn_new contract_a"></a>
			</div>
			<table class="table_blue contract_settings">
				<thead>
					<tr>
						<td style="width:4%">默认</td>
						<td style="width:10%">联系人</td>
						<td style="width:10%">手机号</td>
						<td style="width:20%">固话</td>
						<td style="width:12%">传真</td>
						<td style="width:10%">QQ</td>
						<td style="width:12%">旺旺</td>
						<td style="width:12%">E-mail</td>
						<td style="width:10%">操作</td>
					</tr>
				</thead>
				<tbody id="linkmanTbody">
					<tr>
						<td><input type="radio" name="default" checked onclick="setIsDefault(this)"></td><!-- 默认为1 -->
						<td><input type="text" placeholder="联系人" name="person">
						</td>
						<td><input type="text" placeholder="手机" name="phone">
						</td>
						<td><input type="text" placeholder="区号" style="width:50px" name="zone">
							- <input type="text" style="width:100px" placeholder="电话" name="telno">
						</td>
						<td><input type="text" placeholder="传真" name="fax">
						</td>
						<td><input type="text" placeholder="QQ" name="qq">
						</td>
						<td><input type="text" placeholder="旺旺" name="wangNo">
						</td>
						<td><input type="text" placeholder="E-mail" name="email">
						</td>
						<td>
							<input type="hidden" name="isDefault" value="1">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
	<div class="text-center">
		<input type="button" class="next_step" value="保存">
	</div>
</div>
