<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
function addShop(){
		$.ajax({
		url:"buyer/sysshopdeliverydata/loadInsertShopHtml",
		type:"get",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				title:"添加店铺",
				skin: 'layui-layer-rim',
  		        area: ['400px', 'auto'],
  		        content:data,
  		        btn:['确定','取消'],
  		        yes:function(index,layerio){
  		        var shopId=$("#sysShopDataForm select option:selected").val();
  		        	$.ajax({
  		        		url:"buyer/sysshopdeliverydata/saveInsertShop",
  		        		type:"post",
  		        		data:{"shopId":shopId},
  		        		async:false,
  		        		success:function(data){
  		        			var result=eval('('+data+')');
					    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'buyer/sysshopdeliverydata/loadshopDateManageHtml','buyer','17101210091527451577');
							}else{
								layer.msg(resultObj.msg,{icon:2,time:500});
							}
  		        		},
  		        		error:function(){
  		        			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		        		}
  		        	});
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function deleteShop(id){
 layer.confirm('确定删除?', {icon: 3, title:'提示'}, function(index){
	$.ajax({
		url:"buyer/sysshopdeliverydata/deleteShop",
		type:"post",
		async:false,
		data:{"id":id},
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1});
				leftMenuClick(this,'buyer/sysshopdeliverydata/loadshopDateManageHtml','buyer','17101210091527451577');
			}
			else{
				layer.msg(resultObj.msg,{icon:2});
				leftMenuClick(this,'buyer/sysshopdeliverydata/loadshopDateManageHtml','buyer','17101210091527451577');
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	})
	});
}
</script>
<div class="content_modify">
	<div class="content_role">
    	<form class="layui-form" action="buyer/sysshopdeliverydata/loadshopDateManageHtml">
		        <ul class="order_search personSearch">
		          <li class="range">
		            <label>店铺名称：</label>
		            <input type="text" placeholder="输入店铺名称" name="shopName" value="${searchPageUtil.object.shopName}">
		          </li>
		 		<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
		        </ul>
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		</form>
		<div class="newBuild mt" button="添加店铺"><button onclick="addShop();">+ 添加店铺</button></a></div>
		<table class="table_pure personList">
		          <thead>
		          <tr>
		            <td style="width:10%">编号</td>
		            <td style="width:40%">店铺code</td>
		            <td style="width:40%">店铺名称</td>
		            <td style="width:10%">操作</td>
		          </tr>
		          </thead>
		          <tbody>
		          <c:forEach var="shop" items="${searchPageUtil.page.list}" varStatus="status">
		            <tr>
		              <input type="hidden" value="${shop.shopId}">
		              <td>${status.count}</td>
		              <td>${shop.shopCode}</td>
		              <td>${shop.shopName}</td>
		              <td>
		                <span class="layui-btn layui-btn-delete layui-btn-mini" onclick="deleteShop('${shop.id}');" button="删除"><b></b>删除</span>
		              </td>
		            </tr>
		           </c:forEach>
		          </tbody>
		 </table>
		<div class="pager">${searchPageUtil.page}</div>
    </div>
</div>