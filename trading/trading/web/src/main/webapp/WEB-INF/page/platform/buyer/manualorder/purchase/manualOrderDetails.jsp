<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../../common/common.jsp"%>
<script src="<%=basePath%>statics/platform/js/toolTip.js"></script>
<script type="text/javascript">
	$(function() {
		var sum = "";
		$("#orderProductTbody tr").each(function(){
			var price = $(this).find("td:eq(5)").text();
			var number = $(this).find("td:eq(6)").text();
			sum = Number(sum) + Number(price) * Number(number);
		});
		$("#productSumPrice").text(sum);
	});
</script>
<div class="order_top mt">
	<div class="lf order_status">
		<h4 class="mt">当前订单状态</h4>
		<p class="mt text-center">
			<span class="order_state">
				<c:choose>
					<c:when test="${order.isCheck==0}">等待审核</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${order.status==3}">待收货</c:when>
							<c:when test="${order.status==4}">已收货</c:when>
							<c:when test="${order.status==6}">已取消</c:when>
							<c:when test="${order.status==7}">已驳回</c:when>
							<c:when test="${order.status==8}">已完成</c:when>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</span>
		</p>
		<p class="order_remarks text-center">订单正在审批中，请耐心等候供应商处理订单</p>
	</div>
	<div class="lf order_progress">
		<div class="ystep2"></div>
	</div>
</div>
<!-- <div class="info_l mt">
	<h4 class="logisticsInfo">物流信息</h4>
	<table class="table_info">
		<tr>
			<td>物流公司：<span>韵达快递</span>
			</td>
			<td>运单号码：<span>3926790793301</span>
			</td>
			<td>司机：<span>张XXX</span>
			</td>
		</tr>
		<tr>
			<td>手机号：<span>187XXXX73008</span>
			</td>
			<td>固定电话：<span>0531-897xxxxx</span>
			</td>
			<td>预计到达日期：<span>2017-04-25</span>
			</td>
		</tr>
	</table>
</div> -->
<div>
	<!--订单信息-->
	<h4 class="mt">订单信息</h4>
	<div class="order_d">
		<p>
			<span class="order_title">采购商信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>采购商名称：<span>${el:getCompanyById(order.companyId).companyName}</span>
				</td>
				<td>负责人：<span>${el:getUserById(order.createId).userName}</span>
				</td>
				<td>手机号：<span>${el:getUserById(order.createId).loginName}</span>
				</td>
			</tr>
			<tr>
				<td colspan="3">
				收货地址：${order.personName},${order.receiptPhone},${order.areaCode}-${order.planeNumber},
						${el:getProvinceById(order.province).province} ${el:getCityById(order.city).city} 
						${el:getCityById(order.area).area} ${order.addrName}
				</td>
			</tr>
			<tr>
				<td>运送方式：快递</td>
			</tr>
		</table>
		<p class="line mt"></p>
		<p>
			<span class="order_title">供应商信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>供应商名称：<span>${order.suppName}</span>
				</td>
				<td>负责人：<span>${order.person}</span>
				</td>
				<td>手机号：<span>${order.phone}</span>
				</td>
			</tr>
		</table>
		<p class="line mt"></p>
		<p>
			<span class="order_title">订单信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>订单号：<span>${order.orderCode}</span>
				</td>
				<td>创建时间：<span><fmt:formatDate value="${order.createDate}" type="both"/></span>
				</td>
				<td>审批时间：<span><fmt:formatDate value="${order.verifyDateFormat}" type="both"/></span>
				</td>
			</tr>
			<tr>
				<td>发货时间：<span><fmt:formatDate value="${order.createDate}" type="both"/></span>
				</td>
				<td>收货时间：<span><fmt:formatDate value="${order.createDate}" type="both"/></span>
				</td>
				<td>付款时间：<span><fmt:formatDate value="${manualOrder.paymentDate}" pattern="yyyy-MM-dd" /></span>
				</td>
			</tr>
			<tr>
				<td>付款方式：<span><c:choose>
							<c:when test="${manualOrder.paymentType==0}">银行卡转账</c:when>
							<c:when test="${manualOrder.paymentType==1}">现金</c:when>
							<c:when test="${manualOrder.paymentType==2}">网银支付</c:when>
						</c:choose></span>
				</td>
				<td>付款人：<span> ${manualOrder.paymentPerson}</span>
				</td>
				<td>经办人：<span> ${manualOrder.handler}</span>
				</td>
			</tr>
		</table>
		<div class="c66 size_sm orderNote"><!-- 采购商留言： --></div>
		<table class="table_pure detailed_list_s mt">
			<thead>
				<tr>
					<td style="width:20%">商品</td>
					<td style="width:10%">条形码</td>
					<td style="width:5%">单位</td>
					<td style="width:10%">仓库</td>
					<td style="width:15%">备注</td>
					<td style="width:10%">销售单价</td>
					<td style="width:10%">数量</td>
					<td style="width:10%">优惠金额</td>
					<td style="width:10%">商品总额</td>
				</tr>
			</thead>
			<tbody id="orderProductTbody">
				<c:forEach var="product" items="${orderProductList}">
					<tr>
						<td>${product.proCode} ${product.proName} ${product.skuCode} ${product.skuName}</td>
						<td>${product.skuOid}</td>
						<td>${el:getUnitById(product.unitId).unitName}</td>
						<td>${el:getWarehouseById(product.wareHouseId).wareHouseName}</td>
						<td>${product.remark}</td>
						<td>${product.price}</td>
						<td>${product.goodsNumber}</td>
						<td>${product.preferentialPrice}</td>
						<td>${product.priceSum}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="totalOrder">
		<ul class="submit_order">
			<li><label>商品总额:</label> <span>¥ <label id="productSumPrice"></label></span></li>
			<li><label>商品运费:</label> <span>¥ ${manualOrder.postage}</span></li>
			<li><label>采购费用:</label> <span>¥ ${manualOrder.purchasePrice}</span></li>
			<li><label>订单总金额:</label> <span class="orange">¥ ${order.goodsPrice}</span></li>
			<li><label>已付款金额:</label> <span class="orange">¥ ${manualOrder.paymentPrice}</span>
			</li>
		</ul>
	</div>
	<div>
		<p>采购凭证：</p>
		<div class="voucherImg orderVoucher">
			<c:if test="${manualOrder.purchVoucher != null && manualOrder.purchVoucher != ''}">
				<c:forEach var="purchVoucher" items="${manualOrder.purchVoucher.split(',')}">
					<span>
						<img src="${purchVoucher}" onMouseOver="toolTip('<img src=${purchVoucher}>')" onMouseOut="toolTip()">
					</span>
				</c:forEach>
			</c:if>
		</div>
		<p class="mp">付款凭证：</p>
		<div class="voucherImg orderVoucher">
			<c:if test="${manualOrder.paymentVoucher != null && manualOrder.paymentVoucher != ''}">
				<c:forEach var="paymentVoucher" items="${manualOrder.paymentVoucher.split(',')}">
					<span>
						<img src="${paymentVoucher}" onMouseOver="toolTip('<img src=${paymentVoucher}>')" onMouseOut="toolTip()">
					</span>
				</c:forEach>
			</c:if>
		</div>
		<p class="mp">合同原件：</p>
		<div class="voucherImg orderVoucher">
			<c:if test="${manualOrder.contractFile != null && manualOrder.contractFile != ''}">
				<c:forEach var="contractFile" items="${manualOrder.contractFile.split(',')}">
					<span>
						<img src="${contractFile}" onMouseOver="toolTip('<img src=${contractFile}>')" onMouseOut="toolTip()">
					</span>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<div class="btn_p text-center">
		<a href="buyer/manualOrder/printOrder?orderId=${order.id}" target="_blank">
			<span class="preview order_p">打印订单</span>
		</a>
	</div>
</div>