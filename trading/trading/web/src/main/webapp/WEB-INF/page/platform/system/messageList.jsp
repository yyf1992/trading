<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../common/path.jsp"%>
<script type="text/javascript">
    $(function() {
        $(".layui-tab-title li").click(function(){
            var id = $(this).attr("id");
            var index = id.split("_")[1];
            $("#tabId").val(index);
//            loadPlatformData();
            $(".layui-tab-title li").removeClass("layui-this");
            $(this).addClass("layui-this");
            loadData();
        });
        loadData();
    });
    function loadData(){
        var formObj = $("#mainForm");
        $.ajax({
            url : "platform/sysUser/loadMessageData",
            data:formObj.serialize(),
            async:false,
            success:function(data){
                var str = data.toString();
                $("#msgContent").empty();
                $("#msgContent").html(str);
                var count= $('.pager').find('span').html();
                $("#count1").html(count);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
</script>
<div>
    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
        <ul class="layui-tab-title basic_title">
            <li id="tab_0" <c:if test="${params.tabId eq 0}">class="layui-this"</c:if>>系统信息（<span>1</span>）</li>
            <li id="tab_1" <c:if test="${params.tabId eq 1}">class="layui-this"</c:if>>验证信息（<span id="count1">${params.friendCount}</span>）</li>
        </ul>
        <div class="layui-tab-content">
            <form action="platform/sysUser/messageList" id="mainForm">
                <input type="hidden" id="tabId" name="tabId" value="${params.tabId}">
                <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
                <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
            </form>
        </div>
    </div>
    <div id="msgContent"></div>

</div>