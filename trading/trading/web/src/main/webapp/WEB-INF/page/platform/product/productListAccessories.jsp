<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/setPrice.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script type="text/javascript">
  function editProduct(id,productCode,productType,unitId,productName) {
	  var contentObj = $(".content");
	     var formObj = contentObj.find("form");
		  var user = {
				form:formObj.serialize(),
				id:id,
				productCode:productCode,
				productType:productType,
				unitId:unitId,
				productName:productName
		        };
		$.ajax({
			url : "platform/product/loadAllSKU",
			async : false,
			data :user,
			success : function(data) {
				$(".content").empty();
				var str = data.toString();
				$(".content").html(str);
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {icon : 2},{time:500});
			}
		});
	}
//重置
function recovery(){
	$("#productCode").val("");
	$("#productName").val("");
	$("#skuCode").val("");
	$("#skuName").val("");
	$("#barcode").val("");
}
</script>
<div class="tab">
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=0','baseInfo');">成&emsp;品</a><b>|</b>
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=1','baseInfo');">原材料</a><b>|</b>
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=2','baseInfo');" class="hover">辅&emsp;料</a><b>|</b>
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=3','baseInfo');">虚拟产品</a><b>|</b>
</div>
<div>
     <!--搜索栏-->
	<form class="searchForm" action="platform/product/allProduct" id="productForm">
	  <ul class="order_search ">
	     <li>
           <label>商品货号</label>:
           <input type="text" placeholder="输入商品货号" style="width:150px" id="productCode" name="productCode" value="${searchPageUtil.object.productCode}">
         </li>
         <li>
         <label>商品名称</label>:
           <input type="text" placeholder="输入商品名称" style="width:150px" id="productName" name="productName" value="${searchPageUtil.object.productName}">
         </li>
         <li>
           <label>条形码</label>:
           <input type="text" placeholder="输入条形码" style="width:150px" id="barcode" name="barcode" value="${searchPageUtil.object.barcode}">
         </li>
         <li>
           <label>规格代码</label>:
           <input type="text" placeholder="输入规格代码" style="width:150px" id="skuCode" name="skuCode" value="${searchPageUtil.object.skuCode}">
         </li>       
         <li>
           <label>规格名称</label>:
           <input type="text" placeholder="输入规格名称" style="width:150px" id="skuName" name="skuName" value="${searchPageUtil.object.skuName}">
         </li>
         <li>
           <input type="hidden"style="width:150px" id="productType" name="productType" value="2">
           <button class="search" onclick="loadPlatformData();" style="width: 113px;">搜索</button>
           <button type="button" class="search" onclick="recovery();">重置</button>
         </li>
       </ul>
		<!--<span class="barcode rt"><b>打印条形码</b></span> -->
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<div class="mt" id="newProduct" style="overflow:hidden">
			<a class="rt" href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/loadAddProductHtml?productType=0','buyer');" class="layui-btn layui-btn-danger layui-btn-small rt">
	 			<img src="<%= basePath %>statics/platform/images/commNew.jpg"></img>
	 		</a>
	 		<a href="javascript:void(0)" onclick="synchronousOMSProduct(0);" style="width: 113px;height: 25px;font-size: 15px;" class="layui-btn layui-btn-danger layui-btn-small rt">从OMS同步商品</a>
		</div>
	</form>
<!--列表区-->
	<table class="order_detail">
		<tr>
			<td style="width:90%">
				<ul>
					<li style="width:360px">条形码</li>
					<li style="width:150px">规格代码</li>
					<li style="width:150px">规格名称</li>
					<li style="width:150px">销售单价</li>
					<li style="width:150px">标准库存</li>
					<li style="width:150px">库存下限</li>
					<li style="width:150px">销售天数</li>
				</ul>
			</td>
			<td style="width:10%">操作</td>
		</tr>
	</table>
	<c:forEach var="BuyProduct" items="${searchPageUtil.page.list}">
		<div class="order_list">
			<p>
				<span class="layui-col-sm2" style="width: 300px">
					货号:<b>${BuyProduct.product_code}</b>
				</span>
				<span class="layui-col-sm3" style="width: 300px">
					商品名称:<b>${BuyProduct.product_name}</b>
				</span>
				<span class="layui-col-sm1" title="${applyPurchase.remark}">
					单位:<b>${BuyProduct.unit_name}</b>
				</span>
			</p>
			<table>
				<tr>
					<td style="width:90%">
						<c:forEach var="sku" items="${BuyProduct.skuList}">
							<ul class="clear" style="padding: 10px">
								<li style="width:360px;min-height: 20px;">${sku.barcode}</li>
								<li style="width:150px;min-height: 20px;">${sku.skuCode}</li>
								<li style="width:150px;min-height: 20px;">${sku.skuName}</li>
								<li style="width:150px;min-height: 20px;">${sku.price}</li>
								<li style="width:150px;min-height: 20px;">${sku.standardStock}</li>
								<li style="width:150px;min-height: 20px;">${sku.minStock}</li>
								<li style="width:150px;min-height: 20px;">${sku.planSalesDays}</li>
							</ul>
						</c:forEach>
					</td>
					<td style="width:10%" class="operate">
						<a href="javascript:void(0)" 
					    	onclick="editProduct('${BuyProduct.id}','${BuyProduct.product_code}','${BuyProduct.product_type}','${BuyProduct.unit_id}','${BuyProduct.product_name}');"
					    	class="layui-btn layui-btn-update layui-btn-mini">
					    	<i class="layui-icon">&#xe7e9;</i>修改</a>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
    <div class="pager">${searchPageUtil.page}</div>
</div>