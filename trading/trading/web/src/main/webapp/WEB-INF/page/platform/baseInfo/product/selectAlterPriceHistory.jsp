<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib  prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!--价格修改历史-->
<div>
    <c:choose>    
       <c:when test="${fn:length(searchPageUtil.page.list) == 0}">    
       		<span style="text-align: center;display:block;font-size:15px;">无修改记录</span>
       </c:when>    
       <c:otherwise>    
           <table class="table_yellow schedule">
		        <thead>
			        <tr>
			            <td style="width:8%">条形码</td>
			            <td style="width:8%">原价格</td>
			            <td style="width:8%">修改价格</td>
			            <td style="width:15%">修改理由</td>
			            <td style="width:6%">修改人</td>
			            <td style="width:9%">修改时间</td>
			            <td style="width:5%">状态</td>
			        </tr>
		        </thead>
		        <tbody>
			        <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
			             <tr>
			                 <td>${item.barcode}</td>
			                 <td>${item.price}</td>
			                 <td>${item.alterPrice}</td>
			                 <td>${item.modifyReason}</td>
			                 <td>${item.createrName}</td>
			                 <td><fmt:formatDate value="${item.createDate}" type="BOTH"/></td>
			                 <td>
			                 	<c:if test="${item.status == 0}">审批中</c:if>
			                 	<c:if test="${item.status == 2}">已同意</c:if>
			                 	<c:if test="${item.status == 3}">已拒绝</c:if>
			                 </td>
			                 <c:if test="${item.barcode == null}">首次关联没有修改记录</c:if>
			             </tr>
			         </c:forEach>  
	             </tbody>
 		    </table>
       </c:otherwise>    
   </c:choose>
</div>
