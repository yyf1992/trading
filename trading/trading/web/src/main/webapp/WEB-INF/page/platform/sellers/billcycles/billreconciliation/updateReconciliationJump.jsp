<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增账单周期</title>
   <%-- <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billcycles/addBillCycle.js"></script>--%>
    <script src="<%=basePath%>statics/libs/jquery.min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
    <link rel="stylesheet" type="text/css" href="${staticsPath}statics/platform/css/jquery-ui-multiselect/jquery.multiselect.css" />
    <link rel="stylesheet" type="text/css" href="${staticsPath}statics/platform/css/jquery-ui-multiselect/jquery.multiselect.filter.css" />
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/jquery-ui-1.11.4/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${staticsPath}statics/platform/css/jquery-ui-multiselect/style.css" />
    <link rel="stylesheet" type="text/css" href="${staticsPath}statics/platform/css/jquery-ui-multiselect/prettify.css" />

    <script type="text/javascript" src="<%=basePath%>statics/platform/css/jquery-ui-1.11.4/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>statics/platform/css/jquery-ui-multiselect/prettify.js"></script>
    <script type="text/javascript" src="${staticsPath}statics/platform/js/jquery-ui-multiselect/jquery.multiselect.js"></script>
    <script type="text/javascript" src="${staticsPath}statics/platform/js/jquery-ui-multiselect/jquery.multiselect.filter.js"></script>
    <script src="<%=basePath%>statics/platform/js/colResizable-1.6.min.js"></script>
    <script type="text/javascript">
    // $(function(){
    var form;
    layui.use('form', function() {
        form = layui.form;
        form.render("select");
         //建单人数据回显
         openSelaData("1");

    	 //提交账单周期信息
    	 $('#billCycleInfo_add').click(function(){
    	     debugger
             var date = new Date();
             var month = date.getMonth() + 1;
             var strDate = date.getDate();
             if (month >= 1 && month <= 9) {
                 month =  month;
             }
             if (strDate >= 0 && strDate <= 9) {
                 strDate =  strDate;
             }
             var nowDate = date.getFullYear() +"-"+ month+"-"+strDate;

    	     var reconciliationId = $("#reconciliationId").val();
    		 var cycleStartDate=$("#startDate").val();
    		 if(cycleStartDate == ''){
    				layer.msg("请选择账期开始日期！",{icon:2});
    				return;
    			}else if(cycleStartDate > nowDate){
                 layer.msg("开始日期不得大于操作日期！",{icon:2});
                 return;
             }
    		 var cycleEndDate=$("#endDate").val();
    		 if(cycleEndDate == ''){
 				layer.msg("请选择账期终止日期！",{icon:2});
 				return;
 			   }else if(cycleEndDate > nowDate){
                 layer.msg("终止日期不得大于操作日期！",{icon:2});
                 return;
             }
             var buyCompanyId = $("#buyCompanyId").val();

             //获取创建人
             var createUserId = $("#sela").val();
             var createUserName = $("#selcetText").text();

             //alert("添加建单人编号"+createUserId);
             //alert("添加建单人名称"+createUserName);
             var createUserIdStr = "";
             if(null != createUserId){
                 if( createUserId.length > 0){
                     for(i = 0;createUserId.length > i;i++){
                         createUserIdStr = createUserIdStr+createUserId[i]+",";
                     }
                     createUserIdStr = createUserIdStr.substring(0,createUserIdStr.length -1).trim();
                 }else {
                     createUserIdStr = "";
                 }

             }else {
                 createUserIdStr = "";
             }


             var createUserNameStr = "";

             //alert("建单名称长度"+createUserNames.length);
             if(null != createUserName){
                 var createUserNames = createUserName.split(",");
                 if(createUserNames.length > 0){
                    // if(createUserNames.length > 1){
                         for(i = 0;createUserNames.length > i;i++){
                             createUserNameStr = createUserNameStr+createUserNames[i]+",";
                         }
                         createUserNameStr = createUserNameStr.substring(0,createUserNameStr.length -1).trim();
                     /*}else {
                         createUserNameStr = createUserName;
                     }*/
                 }else {
                     createUserNameStr = "";
                 }
             }else {
                 createUserNameStr = "";
             }

           //保存数据
         var url = basePath+"platform/seller/billReconciliation/updateReconciliationInfo";
         $.ajax({
             type : "POST",
             url : url,
             async:false,
             data: {
                 "reconciliationId":reconciliationId,
                 "startArrivalDate":cycleStartDate,
                 "endArrivalDate":cycleEndDate,
                 "companyId":buyCompanyId,
                 "companyName":"",
                 "createUserId":createUserIdStr,
                 "createUserName":createUserNameStr,
             },
             success:function(data){
                 /*layer.msg("修改成功！",{icon:1});
                 leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');*/
                 var result = eval('(' + data + ')');
                 var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                 if(resultObj.success){
                     layer.msg(resultObj.msg,{icon:1});
                     leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers')
                 }else{
                     layer.msg(resultObj.msg,{icon:2});
                 }
             },
             error:function(){
                 layer.msg("修改失败，请稍后重试！",{icon:2});
             }
         });
        });
     });

     //查询订单创建人
     $('#sela_ms').click(function () {
         openSelaData("2");
         $("#sela").multiselectfilter({
             label:"搜索:",
             placeholder:"请输入姓名关键字",
             width: 150,
         });
     });

     //简单人数据回显
     function openSelaData(openType) {
         $("#sela").empty();
         if("1"==openType){
             $("#sela").multiselect({
                 classes:'',
                 noneSelectedText: "==请选择==",
                 checkAllText: "选中全部",
                 uncheckAllText: '取消全选',
                 selecteText:'#选中',
                 selectedList:1000000,
                 autoOpen:true
             });
         }

         var date = new Date();
         var month = date.getMonth() + 1;
         var strDate = date.getDate();
         if (month >= 1 && month <= 9) {
             month =  month;
         }
         if (strDate >= 0 && strDate <= 9) {
             strDate =  strDate;
         }
         var nowDate = date.getFullYear() +"-"+ month+"-"+strDate;
         var reconciliationId = $("#reconciliationId").val();
         var cycleStartDate=$("#startDate").val();
         if(cycleStartDate == ''){
             layer.msg("请选择账期开始日期！",{icon:2});
             return;
         }else if(cycleStartDate > nowDate){
             layer.msg("开始日期不得大于操作日期！",{icon:2});
             return;
         }
         var cycleEndDate=$("#endDate").val();
         if(cycleEndDate == ''){
             layer.msg("请选择账期终止日期！",{icon:2});
             return;
         }else if(cycleEndDate > nowDate){
             layer.msg("终止日期不得大于操作日期！",{icon:2});
             return;
         }
         //供应商判空
         var companyId=$("#buyCompanyId").val();
         var supplierNameArr=$("#buyCompanyName").val();
         var supplierNameArr=$("#addBillCycleDiv #buyersId").text();
         var supplierNameArrss = supplierNameArr.substring(11,supplierNameArr.length);

         //根据周期和公司查询建单人
         var url = basePath+"platform/seller/billReconciliation/queryCreateBillUser";
         $.ajax({
             type : "POST",
             url : url,
             async:false,
             data: {
                 "startArrivalDate":cycleStartDate,
                 "endArrivalDate":cycleEndDate,
                 "companyId":companyId,
                 "companyName":supplierNameArr
             },
             success:function(data){
                 var result = eval("(" + data + ")");
                 var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
                 if(resultObj.success){
                     var createUserMap = resultObj.createBillUserLink;
                     for(i = 0;i<createUserMap.length;i++){
                         if(null != createUserMap[i]){
                             var buyersId = createUserMap[i].createId;
                             var buyersName = createUserMap[i].createName;
                             $("#sela").append("<option value='" + buyersId + "'>" + buyersName + "</option>");
                         }

                     }

                     $("#sela").multiselect("destroy").multiselect({
                         // 自定义参数，按自己需求定义
                         height: 175,
                         classes:'',
                         noneSelectedText: "==请选择==",
                         checkAllText: "选中全部",
                         uncheckAllText: '取消全选',
                         selecteText:'#选中',
                         selectedList:1000000,
                         autoOpen:true
                     });
                     /*$("#sela").multiselectfilter({
                         label:"搜索:",
                         placeholder:"请输入姓名关键字",
                         width: 150,
                     });*/
                     var createBillUserIdOld = $("#createBillUserId").val();
                    var createBillUserIdList = createBillUserIdOld.split(",");
                     $("#sela").val(createBillUserIdList);
                     $("#sela").multiselect('refresh');

                 }else{
                     layer.msg(resultObj.msg,{icon:2});
                 }
             },
             error:function(){
                 layer.msg("获取失败，请稍后重试！",{icon:2});
             }
         });
     }
    </script>
</head>
<div id="addBillCycleDiv">
 <!--  <form id="addForm" name="addForm" action=""> -->
  <div>
      <input type="hidden" id="reconciliationId" value="${sellerRecInfo.id}">
      <input type="hidden" id="createBillUserId" name="createBillUserId" value="${sellerRecInfo.createBillUserId}">
      <input type="hidden" id="createBillUserName" name="createBillUserName" value="${sellerRecInfo.createBillUserName}">
    <div class="bill_cycle">
        <label class="labelStyle">账单周期:</label>
            <%--<label >起始日期</label>--%>
            <div class="layui-input-inline">
                <input type="text" name="startBillStatementDate" id="startDate" lay-verify="date" value="${sellerRecInfo.startBillStatementDateStr}" class="layui-input" placeholder="开始日">
            </div>
        至
           <%-- <label >终止日期</label>--%>
            <div class="layui-input-inline">
                 <input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" value="${sellerRecInfo.endBillStatementDateStr}" class="layui-input" placeholder="截止日">
            </div>
    </div>
      <div class="bill_cycle">
          <label class="labelStyle">采购商名称:</label>
          <input type="hidden" id="buyCompanyId" name="buyCompanyId" value="${sellerRecInfo.buyCompanyId}">
          <input style="width:20%;height: 30px" disabled="disabled" id="buyCompanyName" name="buyCompanyName" value="${sellerRecInfo.buyCompanyName}" >
          <%--<div class="layui-input-inline" style="width:200px" id="addBillCycleSeller">
          </div>--%>
      </div>
      <br>
      <div class="noselect">
          <label class="labelStyle">采购/售后订单创建人:</label>
          <select id ="sela" multiple="multiple" >
              <%-- <option value="t1">上海</option>
               <option value="t2">武汉</option>
               <option value="t3">成都</option>
               <option value="t4">北京</option>
               <option value="t5">南京</option>--%>
          </select>
      </div>
    </div>
    
  <div class="text-center mp30">
    <a href="javascript:void(0);">
    <button class="layui-btn layui-btn-normal layui-btn-small" id="billCycleInfo_add">确定修改账单</button>
    <span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');">返回账单列表</span>
    <%--<button class="layui-btn layui-btn-small" onclick="hidOrShowInterst()">利息设置</button>--%>
    </a>
  </div>
</div>
<style>
    .labelStyle{float:left;display:block;padding:9px 15px;width:200px;font-weight:400;text-align:right}
    .noselect .layui-form-select{
        display: none;
    }
    .ui-multiselect{
        height:30px;
    }
</style>
