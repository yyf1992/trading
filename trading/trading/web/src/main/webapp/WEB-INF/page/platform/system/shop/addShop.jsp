<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="addShopForm">
<div class="account_group">
		<span>店铺编号:</span> <input type="text" name="shopCode" style="width: 207px" placeholder="输入店铺编号">
</div>
<div class="account_group">
		<span>店铺名称:</span> <input type="text" name="shopName" style="width: 207px" placeholder="输入店铺名称">
</div>
<div class="account_group">
		<span>选择平台:</span> 
		<select name="platformId">
		<option value="-1">全部平台</option>
			<c:forEach var="SysPlatform" items="${sysPlatformList}">
				<option value="${SysPlatform.id}"  id="${SysPlatform.id}">${SysPlatform.platformName}</option>
			</c:forEach>
		</select>
</div>
    <div class="account_group">
      <span>状态:</span>
      <label><input type="radio" name="isDel" checked value="0"> 启用</label>
      <label><input type="radio" name="isDel" value="1"> 禁用</label>
    </div>
</form>