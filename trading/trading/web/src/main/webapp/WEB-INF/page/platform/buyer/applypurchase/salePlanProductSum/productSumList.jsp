<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<script type="text/javascript">
	$(function() {
		pure_chkAll();
	});
	//table_pure 带复选框的全选
	function pure_chkAll() {
		var inputs = $(".table_pure .chk_click input[type='checkbox']");
		function checkedSpan() {
			inputs.each(function() {
				if ($(this).prop('checked') == true) {
					$(this).parent().addClass('checked')
				} else {
					$(this).parent().removeClass('checked')
				}
			})
		}
		$('.table_pure thead input[type="checkbox"]').click(function() {
			inputs.prop('checked', $(this).prop('checked'));
			checkedSpan();
		});
		$('.table_pure tbody input[type="checkbox"]')
				.click(
						function() {
							var r = $('.table_pure tbody input[type="checkbox"]:not(:checked)');
							if (r.length == 0) {
								$('.table_pure thead input[type="checkbox"]')
										.prop('checked', true);
							} else {
								$('.table_pure thead input[type="checkbox"]')
										.prop('checked', false);
							}
							console.log(11);
							checkedSpan();
						})
	}
	// 销售明细
	function loadSalePlanDetails(barcode) {
		$.ajax({
			type : "POST",
			url : "buyer/salePlan/loadSalePlanShopList",
			data : {
				"barcode" : barcode
			},
			success : function(data) {
				layer.open({
					type : 1,
					area:['1200px', 'auto'],
					fix: false, //不固定
					maxmin: true,
					shadeClose: true,
		        	shade:0.4,
					title:"销售明细",
			    	content: $('.purchaseContent').html(data),
			    	btn : [ '确定', '关闭' ]
				});
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", { icon : 2 });
			}
		});
	}
	// 批量生成采购订单
	function beatchCreatePurchaseOrder(){
		var checked = $("input:checkbox[name='checkone']:checked");
		if (checked.length < 1) {
			layer.msg("请至少选择一条数据！", { icon : 7 });
			return;
		}
		var idStr = "";
		checked.each(function() {
			idStr += $(this).val() + ",";
		});
		idStr = idStr.substring(0, idStr.length - 1);
		leftMenuClick(this,"buyer/salePlan/beatchCreatePurchasePlan?idStr=" + idStr,"buyer");
	}
</script>
<div>
	<!--搜索栏-->
	<form class="layui-form"
		action="buyer/salePlan/loadSalePlanProductSum" id="searchForm">
		<div class='search_two mt'>
			<input type='text' name="selectValue" placeholder='输入商品信息进行搜索' value="${searchPageUtil.object.selectValue }">
			<button onclick="loadPlatformData();">搜索</button>
		</div>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<!-- <div class="batch_handle mt" id="purchaseSubmit">
		<a onclick="beatchCreatePurchaseOrder();" class="c66">批量生成采购计划</a>
	</div> -->
	<a href="javascript:beatchCreatePurchaseOrder();" class="layui-btn layui-btn-danger layui-btn-small rt" >
		<i class="layui-icon">&#xe67d;</i> 批量生成采购计划</a>
	<table class="table_pure purchaseOrder">
		<thead>
			<tr>
				<td style="width:48px"><span class="chk_click"><input type="checkbox"></span></td>
                <td style="width:40px">序号</td>
                <td style="width:90px">货号</td>
                <td style="width:120px">商品名称</td>
                <td style="width:100px">规格代码</td>
                <td style="width:69px">规格名称</td>
                <td style="width:86px">条形码</td>
                <td style="width:86px">销售计划总数量</td>
                <td style="width:86px">已生成销售计划</td>
                <td style="width:86px">剩余销售计划</td>
                <td style="width:86px">操作</td>
			</tr>
		</thead>
		<tbody >
			<c:forEach var="product" items="${searchPageUtil.page.list}" varStatus="indexNum">
				<tr>
					<td><span class="chk_click"><input type="checkbox" name="checkone" value="${product.barcode}"></span></td>
					<td>${indexNum.count}</td>
					<td>${product.productCode}</td>
					<td>${product.productName}</td>
					<td title="${product.skuCode}">${product.skuCode}</td>
					<td>${product.skuName}</td>
					<td title="${product.barcode}">${product.barcode}</td>
					<td>${product.totalSalesNum}</td>
					<td>${product.alreadySalePlanNum}</td>
					<td>${product.totalSalesNum - product.alreadySalePlanNum}</td>
					<td>
						<a href="javascript:void(0)" onclick="loadSalePlanDetails('${product.barcode}');"
							class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>销售明细</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<!--分页-->
	<div class="pager">${searchPageUtil.page}</div>
	<!-- 销售明细弹窗 -->
	<div class="purchaseContent"></div>
</div>
