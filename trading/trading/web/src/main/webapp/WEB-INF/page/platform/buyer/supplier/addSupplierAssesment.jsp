<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/supplier.css">
<script src="<%=basePath%>statics/platform/js/supplier.js" type="text/javascript"></script>
<div>
	<table class="assesmentTable">
		<thead>
			<tr>
				<td>考核名称</td>
				<td>总分值</td>
				<td>备注</td>
				<td>状态</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><input type="text" placeholder="考核名称" class="assessmentName"></td>
				<td><input type="number" min="0.0" step="0.1" placeholder="分值" class="score"></td>
				<td><input type="text" placeholder="备注" class="remark"></td>
				<td><input type="radio" name="state" class="state" value="0" checked="checked">启用
					<input type="radio" name="state" class="state" value="1">禁用
				</td>
			</tr>
		</tbody>
	</table>
	
	<h4 class="page_title" style="margin-top: 20px;">考核项</h4>
	
	<table class="tableSmall">
			<thead>
				<tr>
					<td>考核项</td>
					<td><b>分值</b><input type="radio" name="scoreType" class="scoreType" value="0" checked="checked">
						<b>百分比</b><input type="radio" name="scoreType" class="scoreType" value="1">
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1、到货及时率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="arrivalRateScore"></td>
				</tr>
				<tr>
					<td>2、计划完成率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="completionRateScore"></td>
				</tr>
				<tr>
					<td>3、到货合格率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="qualificationRateScore"></td>
				</tr>
				<tr>
					<td>4、售后响应及时率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="afterSaleRateScore"></td>
				</tr>
				<tr>
					<td>合计：</td>
					<td><span id="total" style="color: red"></span></td>
				</tr>
			</tbody>
		</table>	
		<div class="submit">
			<button class="cancle"onclick="leftMenuClick(this,'platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml','buyer','18010209272828100705')">取消</button>
			<button class="save" onclick="saveInsert();">保存</button>
		</div>
</div>