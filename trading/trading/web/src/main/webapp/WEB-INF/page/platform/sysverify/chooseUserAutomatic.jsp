<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(".content_role .search").click(function(e){
	e.preventDefault();
	var formObj = $(this).parents("form");
	var formData = formObj.serialize();
	$.ajax({
		url:"platform/sysVerify/loadPersonAutomatic",
		data:formData,
		success:function(data){
			var divId = $("#divId").val();
			$("#"+divId).html(data);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
});
//人员选择
function chooseUser(obj){
	var userId = $(obj).find("input[name='userId']").val();
	var userName = $(obj).find("input[name='userName']").val();
	var selectType = $("input#selectType").val();
	var selectId = $("input#selectId").val();
	if(selectType=='1'){
		//审批人选择
		var liObj = "<li class='person'>";
		liObj += "	<img src='${basePath}statics/platform/images/arrow.png'>";
		liObj += "	<span class='userSpan'>";
		liObj += "		<b title='删除' style='display: none;'></b>";
		liObj += "		<input type='hidden' name='userId' value='"+userId+"'>";
		liObj += "		<input type='hidden' name='userName' value='"+userName+"'>";
		liObj += 		userName;
		liObj += "	</span>";
		liObj += "</li>";
		$("#"+selectId).find("li.person:last").after(liObj);
		$("#"+selectId).on("mouseenter","li span",function(){
	        $(this).children("b").show();
	        $(this).children("b").click(function(){
	        	$(this).parents("li").remove();
	        	loadLeftUser();
	        });
	    }).on("mouseleave","li span",function(){
	        $(this).children("b").hide();
	    });
   }
   loadLeftUser();
}
</script>
<div class="content_role">
	<form action="platform/sysVerify/loadPersonAutomatic">
		<ul class="order_search personSearch">
			<li class="range">
				<label>员工名称：</label> 
				<input type="text" placeholder="输入员工姓名" name="userName" value="${searchPageUtil.object.userName}">
			</li>
			<li class="nomargin">
				<button class="search">搜索</button>
			</li>
		</ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
		<input id="selectId" name="selectId" type="hidden" value="${searchPageUtil.object.selectId}" />
		<input id="selectType" name="selectType" type="hidden" value="${searchPageUtil.object.selectType}" />
		<input id="selectUsers" name="selectUsers" type="hidden" value="${searchPageUtil.object.selectUsers}" />
	</form>
	<table class="table_person personList">
		<thead>
			<tr>
				<td style="width:15%">员工姓名</td>
				<td style="width:20%">登录账户</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="sysUser" items="${searchPageUtil.page.list}" varStatus="status">
				<tr onclick="chooseUser(this);" style="cursor: pointer;">
					<td>
						<input type="hidden" name="userId" value="${sysUser.id}">
						<input type="hidden" name="userName" value="${sysUser.user_name}">
						${sysUser.user_name}
					</td>
					<td>${sysUser.login_name}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
</div>
