<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun"%>
<script type="text/javascript">
	$(function() {
		var form = layui.form;
		form.on('select(changeRepairePrice)', function(data){
			var type = data.value;//售后类型  0换货 1退货
			var price = $(this).parents("tr").find("td:eq(3)").text();
			var number = $(this).parents("tr").find("td:eq(5)").find("input").val();
			var exchangeTd = $(this).parents("tr").find("td:eq(6)");
			var exchangeMoney = $(exchangeTd).find("input").val();//单个返修费
			if(typeof(exchangeMoney) == "undefined" || exchangeMoney == ""){
				exchangeMoney=0;
			}
			var totalMoney;
			if(type==1){
				$(exchangeTd).find("input").val("0");
				if(typeof(number) != "undefined" && number != ""){
					totalMoney=price*number;
				}
				$(this).parents("tr").find("td:eq(7)").find("input").val(totalMoney);
				$(exchangeTd).find("input").attr("disabled","disabled");
			}else{
				$(this).parents("tr").find("td:eq(6)").find("input").removeAttr("disabled");
				if(typeof(number) != "undefined" && number != ""){
					totalMoney=(price+exchangeMoney)*number;
				}
			}
			var trLength=$("#addCustomProductTbody tr:not(:first)").length;
			var i=0;
			$("#addCustomProductTbody tr:not(:first)").each(function(){
				var type=$(this).find("td:eq(4)").find("option:selected").val();
				if(type==1){
					i++;
				}
			});
			if(i==trLength){
				//收货地址隐藏
				$(".afterAddress").hide();
			}else{
				$(".afterAddress").show();
			}
		});
		$(".customerWarehouse").html($("#addCustomerWarehouseDiv").html());
		// 删除
		$('.afterCommList').on('click','.table_del',function(){
			delRow(this);
		});
		//售后-选择商品
	  $('.afterCommSel').click(function(){
	  	var notId = "";
	  	$("#updateCustomProductTbody tr").each(function(){
	  		var id = $(this).find("td:eq(0)").find("input").val();
	  		if(typeof(id) != "undefined" && id != ""){
	  			notId = notId + id + ",";
	  		}
	  	});
	  	if(notId != ""){
	  		notId = notId.substring(0,notId.length - 1);
	  	}
	  	loadCustomerProduct(notId);
	  	var fullIndex = layer.open({
			type : 1,
			title: '选择商品',
			area:['1200px','auto'],
			skin:'popBuyer',
			closeBtn : 0,
			content : $('#afterCommPop'),//.html(data)
			btn:['批量添加','取消'],
			yes:function(index){
				var len = $(".table_pure span.barcodeAdd input:checkbox:checked").length;
				if(len == 0){
					layer.msg("请选择商品！",{icon:7});
					return;
				}
		        layer.close(index);
		        var type = $("input:radio[name='type']:checked").val();
		        var disableStr = '';
		        if(type == 1){
		        	disableStr = 'value="0" disabled="disabled"';
		        }
		        $("#productTbody span.barcodeAdd input:checkbox:checked").each(function(){
		        	var productCode = $(this).parents("tr").find("input[name='product_code']").val();
		        	var productName = $(this).parents("tr").find("input[name='product_name']").val();
		        	var skuCode = $(this).parents("tr").find("input[name='sku_code']").val();
		        	var skuName = $(this).parents("tr").find("input[name='sku_name']").val();
		        	var product=productName+"|"+skuCode+"|"+skuName;
		        	var barcode = $(this).parents("tr").find("td:eq(3)").text();
		        	var supplierName = $(this).parents("tr").find("td:eq(4)").text();
		        	var price = $(this).parents("tr").find("td:eq(5)").text();
		        	var supplierId = $(this).parents("tr").find("input[name='supplier_id']").val();
		        	var str ='<input type="hidden" value="'+productName+'"><input type="hidden" value="'+skuCode+'"><input type="hidden" value="'+skuName+'"><td>'+productCode+'</td><td>'+product+'</td><td>'+barcode+
        			'</td><td title="'+price+'">'+price+'</td>'+
        			'<td><select name="type" lay-filter="changeRepairePrice"><option value="0" selected>换货</option><option value="1">退货</option></select></td><td><input type="number" min="0" onchange="countMoney(this);"></td><td><input type="number" min="0" onchange="countMoney(this);"></td><td><input type="number" min="0" disabled="disabled"></td><td><span class="table_del"><b></b>删除</span></td>';
	        		var tbl=$('.afterCommList tr:eq(-1)');
			        addRow(str,tbl);
		        });
		    	form.render("select");
		      }
		});
	  });
	});
//退货计算
function retrunGoodsSum(){
	$("#tab1>tbody>tr").each(function(){
		var price = $(this).find("td:eq(3)").text();
		var num = $(this).find("td:eq(4)").find("input").val();
		var exchangePrice = $(this).find("td:eq(6)").find("input").val();
		if(typeof(num) != "undefined" && num != ""){
  			$(this).find("td:eq(7)").find("input").val(Number(price)*Number(num));//单价*数量
  		}
	});
}
//计算售后总金额
function countMoney(obj){
	var type = $(obj).parents("tr").find("option:selected").val();
	var price = $(obj).parents("tr").find("td:eq(3)").text();
	var number = $(obj).parents("tr").find("td:eq(5)").find("input").val();
	var exchangeTd = $(obj).parents("tr").find("td:eq(6)");
	var exchangeMoney = $(exchangeTd).find("input").val();
	if(typeof(exchangeMoney) == "undefined" || exchangeMoney == ""){
		exchangeMoney=0;
	}
	var totalMoney;
	if(typeof(number) != "undefined" && number != ""){
		if(type == 0){//换货
			$(obj).parents("tr").find("td:eq(6)").find("input").removeAttr("disabled");
			totalMoney=(parseFloat(price)+parseFloat(exchangeMoney))*number;
			$(obj).parents("tr").find("td:eq(7)").find("input").val(totalMoney);
		}else{//退货
			totalMoney = parseFloat(price) * number ;
			$(obj).parents("tr").find("td:eq(7)").find("input").val(totalMoney);
		}
	}
}
//查找商品
function loadCustomerProduct(notId) {
	var supplierId = $("#updateSupplierId").val();
	$.ajax({
        url:"platform/buyer/customer/loadSelectProduct",
        data:{
        	"supplierId" : supplierId,
        	"notId" : notId,
            "page.divId":'afterCommPop'
        },
        async:false,
        success:function(data){
            $("#afterCommPop").empty();
            var str = data.toString();
            $("#afterCommPop").html(str);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//提交修改售后单
function saveUpdateCustomer(){
	var title = $("#customerTitle").val();
	if(title == ''){
		layer.msg("请填写标题！",{icon:7});
		return;
	}else if(title.length > 30){
		layer.msg("标题长度不能大于30个字符！",{icon:7});
		return;
	}
	var warehouseId = $(".customerWarehouse").find("option:selected").val();
	if(warehouseId == ''){
		layer.msg("请选择出库仓库！",{icon:7});
		return;
	}
	var proLen = $("#updateCustomProductTbody tr").length;
	if(proLen == 1){
		layer.msg("请添加商品！",{icon:7});
		return;
	}else{
		var goodsStr="";
		var numError="";
		var repairPriceError="";
		//changeRepairPrice();
		var returnNum=[];
		$("#updateCustomProductTbody tr:not(:first)").each(function(i){
			var productName = $(this).find("input:eq(0)").val();
			var skuCode =$(this).find("input:eq(1)").val();
			var skuName = $(this).find("input:eq(2)").val();
			var productCode = $(this).find("td:eq(0)").text();
			var skuOid = $(this).find("td:eq(2)").text();
			var price = $(this).find("td:eq(3)").text();//最近采购单价
			var type=$(this).find("td:eq(4)").find("option:selected").val();//售后类型（1-换货，2-退货）
			var number = $(this).find("td:eq(5)").find("input").val();//售后数量
			var exchangeMoney = $(this).find("td:eq(6)").find("input").val();//返修费
			var repairPrice=$(this).find("td:eq(7)").find("input").val();//售后总金额
			if(number == ""){
				numError="第"+(i+1)+"行请填写数量！";
				return false;
			}
			if(type==0){//换货
				if(exchangeMoney == ""){
					repairPriceError="第"+(i+1)+"行请填写返修金额！";
					return false;
				}
			}else{//退货
				returnNum.push(type);
			}
			if(parseInt(number) <= 0){
				numError="第"+(i+1)+"行数量必须大于0！";
				return false;
			}
			goodsStr = goodsStr +  productCode + "," + productName + "," + skuCode + "," + skuName + "," + skuOid
						+ "," + number + "," + price + "," + warehouseId + "," + repairPrice + "," + type + "," + exchangeMoney + "@";
		});
		//goodsStr = goodsStr.substring(0, goodsStr.length - 1);
		var trLength=$("#addCustomProductTbody tr:not(:first)").length;
		if(returnNum.length==trLength){
			//收货地址隐藏
			$(".afterAddress").hide();
		}
		if(repairPriceError!=''){
			layer.msg(repairPriceError,{icon:7});
			return;
		}
		if(numError!=''){
			layer.msg(numError,{icon:7});
			return;
		}
		var a1 = $("#attachment1Div").find("span").length;
  		var a1Str = ""
		if(a1 != 0){
			for(var i = 0;i < a1;i++){
				var url1 = $("#attachment1Div").find("span:eq("+i+")").find("input").val();
				a1Str = a1Str + url1 + ",";
			}
			a1Str = a1Str.substring(0, a1Str.length - 1);
		}
		var url = "platform/buyer/customer/saveUpdateCustomer";
		$.ajax({
			type : "POST",
			url : url,
			async: false,
			data : {
				"updateId" : $("#updateId").val(),
				"title" : title,
				"warehouseId" : warehouseId,
				"goodsStr" : goodsStr,
				"reason" : $("#reason").val(),
				"proof" : a1Str,
				"addressId" : $("select[name='addressId'] option:selected").val(),
				"menuName" : "17091216210064879057"
			},
			success : function(data) {
				if(data.flag){
					var res = data.res;
					if(res.code==40000){
						//调用成功
						saveSucess();
					}else if(res.code==40010){
						//调用失败
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40011){
						//需要设置审批流程
						layer.msg(res.msg,{time:500,icon:2},function(){
							setApprovalUser(url,res.data,function(data){
								saveSucess(data);
							});
						});
						return false;
					}else if(res.code==40012){
						//对应菜单必填
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40013){
						//不需要审批
						notNeedApproval(res.data,function(data){
							saveSucess(data);
						});
						return false;
					}
				}else{
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
					return false;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//保存成功
function saveSucess(ids){
	layer.msg("保存成功！",{icon:1});
	leftMenuClick(this,"platform/buyer/customer/customerList","buyer","17091216212559741744");
}
</script>
	<div>
		<div class="progress123">
			<span class="step1 current"> <b></b> 提交售后申请 </span>
			<span class="step2"> <b></b> 售后审批 </span>
			<span class="step3"> <b></b> 售后处理完成 </span>
		</div>
		<form class="layui-form">
			<input type="hidden" id="updateId" value="${customer.id}">
			<input type="hidden" id="updateSupplierId" value="${customer.supplierId}">
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>标题:</label>
				<div class="layui-input-inline">
					<input type="text" id="customerTitle" name="customerTitle" class="layui-input" value="${customer.title}" placeholder="请输入标题">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>出库仓库:</label>
				<div class="layui-input-inline customerWarehouse"></div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>供应商:</label>
				<div class="layui-input-inline customerSupplier" style="margin-top:10px;">${customer.supplierName}</div>
			</div>
			<ul class="afterSale">
				<li><span class="mt">售后商品：</span>
					<div class="afterList">
						<span class="rt afterCommSel">选择商品</span>
						<table class="table_pure afterCommList" id="tab1">
							<thead>
								<tr>
									<td style="width: 12%;">货号</td>
									<td style="width: 20%;">商品规格</td>
									<td style="width: 12%;">条形码</td>
									<td style="width: 12%;">价格</td>
									<td style="width: 12%;">售后类型</td>
									<td style="width: 12%;">售后数量</td>
									<td style="width: 12%;">单个返修金额</td>
									<td style="width: 12%;">售后总金额</td>
									<td style="width: 8%;">操作</td>
								</tr>
							</thead>
							<tbody id="updateCustomProductTbody">
								<tr hidden="true"></tr>
								<c:forEach var="item" items="${itemList}">
									<tr>
										<input type="hidden" value="${item.productName}">
										<input type="hidden" value="${item.skuCode}">
										<input type="hidden" value="${item.skuName}">
										<td>${item.productCode}</td>
										<td>${item.productName}|${item.skuCode}|${item.skuName}</td>
										<td>${item.skuOid}</td>
										<td title="${item.price}">${item.price}</td>
										<td><select name="type"  lay-filter="changeRepairePrice">
												<option value="0" <c:if test="${item.type==0}">selected="selected"</c:if>>换货</option>
												<option value="1" <c:if test="${item.type==1}">selected="selected"</c:if>>退货</option>
              								</select>
										</td>
										<td><input type="number" min="0" value="${item.goodsNumber}" onchange="countMoney(this);"></td>
										<td>
											<input type="number" min="0" value="${item.exchangePrice}" <c:if test="${item.type==1}">disabled="disabled"</c:if> onchange="countMoney(this);">
										</td>
										<td><input type="number" min="0" value="${item.repairPrice}" disabled="disabled"></td>
										<td><span class="table_del"><b></b>删除</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div></li>
				<li><span class="mt">申请原因：</span>
					<div class="afterReason">
						<textarea id="reason" name="reason" placeholder="请输入申请原因">${customer.reason}</textarea>
					</div></li>
				<li>
					<div class="afterCommUpload">
						<div class="mt c66">商品图片/售后凭证：</div>
						<div class="afterImgUpload">
							<input type="file" multiple name="attachment1" id="attachment1" onchange="fileUpload(this);">
							<p></p>
							<span>为更好的解决商品售后问题，请上传商品图片或售后凭证。 <br>每张图片大小不超过4M，支持bmp,gif,jpg,png,jpeg格。</span>
						</div>
						<div class="afterImg" id="attachment1Div">
							<c:choose>
								<c:when test="${customer.proof != ''}">
									<c:forEach var="a1" items="${customer.proof.split(',')}">
										<span><b></b><img src="${a1}">
				                   		<input type="hidden" name="fileUrl" value="${a1}"></span>
									</c:forEach>
								</c:when>
							</c:choose>
						</div>
					</div></li>
				<li>
					<div class="mt afterAddress">售后收货地址：</div>
					<div style="margin-top: 20px;width:50%;">
						<select name="addressId" lay-verify="required" lay-search="" lay-filter="pageFilter">
							<c:forEach var="address" items="${addressList}">
								<option value="${address.id}" <c:if test="${address.id==customer.addressId}">selected="selected"</c:if>>${el:getProvinceById(address.province).province} ${el:getCityById(address.city).city} ${el:getAreaById(address.area).area} ${address.addrName}（${address.personName} 收）${address.phone}</option>
							</c:forEach>
						</select>
					</div>
				</li>
			</ul>
		</form>
		<div class="text-right">
			<a href="javascript:void(0)" class="next_step" onclick="saveUpdateCustomer();">提交</a>
		</div>
		<!--商品弹框-->
		<div class="commSelPlan" id="afterCommPop"></div>
	</div>
	<div id="addCustomerWarehouseDiv" hidden="true">
		<select id="whareaId" name="whareaId" lay-verify="required" lay-search="" lay-filter="pageFilter">
			<option value="">直接选择或搜索选择</option>
			<c:forEach var="wharea" items="${whareaList}">
				<option value="${wharea.id}"<c:if test="${customer.warehouseId==wharea.id}">selected="selected"</c:if>>${wharea.whareaName}</option>
			</c:forEach>
		</select>
	</div>