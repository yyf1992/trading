<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/commodity_all.css"/>
<link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/common.css"/>
<style>
    .cc:after{
        content: '';
        display: block;
        clear: both;
    }
    .cc input,.cc select{
        width:145px;
        height: 25px;
        margin-bottom: 15px;
    }
    .materialContent{
        height:500px;
        overflow-y: auto;
    }
</style>
<script type="text/javascript">
    function searchProductSku() {
        $.ajax({
            url:"platform/product/sku/showProductSkuPop",
            type:"get",
            data:$("#productSkuPopForm").serialize(),
            async:false,
            success:function(data){
                $(".productSkuPopDiv").html(data);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！", {icon : 2});
            }
        });
    }
    function checkAllProductSkuPop(obj) {
        var spanObj = $(obj).parent();
        var tbodyObj = $(obj).parents("table").find("tbody");
        if($(spanObj).hasClass("checked")){
            //选中时点击，设置成未选中
            $(spanObj).removeClass("checked");
            $(obj).prop("checked",false);
            $(tbodyObj).find('input:checkbox').prop("checked",false);
            $(tbodyObj).find('span.productSkuPopTr').removeClass("checked");
        }else{
            $(spanObj).addClass("checked");
            $(obj).prop("checked",true);
            $(tbodyObj).find('input:checkbox').prop("checked",true);
            $(tbodyObj).find('span.productSkuPopTr').addClass("checked");
        }
    }
    function checkedThisroductSkuPop(obj){
        if($(obj).parent().hasClass("checked")){
            $(obj).prop("checked",false);
            $(obj).parent().removeClass("checked");
        }else{
            $(obj).prop("checked",true);
            $(obj).parent().addClass("checked");
        }
    }
</script>
<!--商品弹框-->
<div class="productSkuPopDiv">
    <form action="platform/product/sku/showProductSkuPop" id="productSkuPopForm">
        <div class="cc mt">
            <span>商品货号:</span> <input type="text" id="searchProductCode" name="searchProductCode" value="${searchPageUtil.object.searchProductCode}" class="mr" placeholder="商品货号">
            <span>商品名称:</span> <input type="text" id="searchProductName" name="searchProductName" value="${searchPageUtil.object.searchProductName}" class="mr" placeholder="商品名称"><Br>
            <span>规格代码:</span> <input type="text" id="searchSkuCode" name="searchSkuCode" value="${searchPageUtil.object.searchSkuCode}" class="mr" placeholder="规格代码">
            <span>规格名称:</span> <input type="text" id="searchSkuName" name="searchSkuName" value="${searchPageUtil.object.searchSkuName}" class="mr" placeholder="规格名称"><Br>
            <span>条 形 码:</span> <input type="text" id="searchBarCode" name="searchBarCode" value="${searchPageUtil.object.searchBarCode}" class="mr" placeholder="条形码">          
            <span>商品类型:</span>
            <select name="searchProductType" lay-filter="aihao">
                <option value="" <c:if test="${empty searchPageUtil.object.searchProductType}">selected="selected"</c:if>>全部</option>
                <option value="0" <c:if test="${searchPageUtil.object.searchProductType=='0'}">selected="selected"</c:if>>成品</option>
                <option value="1" <c:if test="${searchPageUtil.object.searchProductType=='1'}">selected="selected"</c:if>>原材料</option>
                <option value="2" <c:if test="${searchPageUtil.object.searchProductType=='2'}">selected="selected"</c:if>>辅料</option>
            </select>
            <img src="<%=basePath %>/statics/platform/images/find.jpg" onclick="searchProductSku();" class="rt">
            <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
            <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
            <input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
        </div>
    </form>
    <div class="materialContent">
        <table class="table_pure">
            <thead>
            <tr>
                <td style="width:5%"><span class="productSkuPopTr"><input type="checkbox" onclick="checkAllProductSkuPop(this);"></span></td>
                <%--<td style="width:7%">序号</td>--%>
                <td style="width:11%">商品名称</td>
                <td style="width:20%">货号</td>
                <td style="width:15%">条形码</td>
                <td style="width:9%">规格代码</td>
                <td style="width:9%">规格名称</td>
                <td style="width:9%">单位</td>
                <td style="width:9%">商品类型</td>
            </tr>
            </thead>
            <tbody id="productSkuPopTab">
            <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
                <tr>
                    <td><span class="productSkuPopTr"><input type="checkbox" value="${item.id}" name="checkOne" onclick="checkedThisroductSkuPop(this);"></span></td>
                    <%--<td>${status.count}</td>--%>
                    <td>${item.productName}</td>
                    <td>${item.productCode}</td>
                    <td>${item.barcode}</td>
                    <td>${item.skuCode}</td>
                    <td>${item.skuName}</td>
                    <td>${item.unitName}</td>
                    <td>
                        <c:choose>
                            <c:when test="${item.productType=='0'}">成品</c:when>
                            <c:when test="${item.productType=='1'}">原材料</c:when>
                            <c:when test="${item.productType=='2'}">辅料</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="pager" id="page">${searchPageUtil.page}</div>
</div>