<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
$(function() {
	// 显示页面时间
	var today = new Date();//定义日期对象   
	var yyyy = today.getFullYear();//通过日期对象的getFullYear()方法返回年    
	var MM = today.getMonth() + 1;//通过日期对象的getMonth()方法返回年    
	var dd = today.getDate();//通过日期对象的getDate()方法返回年     
	var hh = today.getHours();//通过日期对象的getHours方法返回小时   
	var mm = today.getMinutes();//通过日期对象的getMinutes方法返回分钟   
	var ss = today.getSeconds();//通过日期对象的getSeconds方法返回秒   
	// 如果分钟或小时的值小于10，则在其值前加0，比如如果时间是下午3点20分9秒的话，则显示15：20：09   
	MM = checkTime(MM);
	dd = checkTime(dd);
	mm = checkTime(mm);
	ss = checkTime(ss);
	var day; //用于保存星期（getDay()方法得到星期编号）
	if (today.getDay() == 0)
		day = "星期日 "
	if (today.getDay() == 1)
		day = "星期一 "
	if (today.getDay() == 2)
		day = "星期二 "
	if (today.getDay() == 3)
		day = "星期三 "
	if (today.getDay() == 4)
		day = "星期四 "
	if (today.getDay() == 5)
		day = "星期五 "
	if (today.getDay() == 6)
		day = "星期六 "
	document.getElementById('nowDateTimeSpan').innerHTML = yyyy + "年" + MM + "月" + dd + "日 " + day;
});
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}
</script>
<div class="top">
	<div class="info">
		<span class="user_name">
			<span class="fa fa-user-o">欢迎回来:</span>
			${sessionScope.CURRENT_USER.userName}
		</span>
		<span id="nowDateTimeSpan">2017年3月3日 星期五</span>
		<c:set var="systemMenuId" scope="session" value=""/>
		<c:set var="systemMenuUrl" scope="session" value=""/>
		<c:forEach var="titleMenu" items="${sessionScope.AUTHORITYSYSMENU}">
			<c:if test="${titleMenu.position=='system'}">
				<c:set var="systemMenuId" scope="session" value="${titleMenu.id}"/>
				<c:set var="systemMenuUrl" scope="session" value="${titleMenu.url}"/>
			</c:if>
		</c:forEach>
		<c:if test="${systemMenuId!=''}">
			<b>|</b> 
			<a href="javacript:void(0)" onclick="titleMenuClick('${systemMenuUrl}','${systemMenuId}');return false;" class="modify">系统设置</a>
		</c:if>
		<b>|</b>
		<!-- <a href="#" class="news"> 
			<span class="fa fa-envelope-o"></span>
			<span class="count">3</span> 
		</a> 
		<b>|</b>  -->
		<!-- <a href="#" class="friend"><b></b>我的买卖好友</a>
		<b>|</b> --> 
		<a href="<%=basePath%>platform/loadOut" class="friend"><b></b>退出</a>
	</div>
</div>
