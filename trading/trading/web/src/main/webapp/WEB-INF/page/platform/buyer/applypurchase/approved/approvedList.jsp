<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
$(function(){
	$("#orderContent #selectButton").click(function(e){
		e.preventDefault();
		selectData();
	});
	selectData();
});
function selectData(){
	layer.load();
	var formObj = $("#orderContent").find("form");
	$.ajax({
		url : basePath+"buyer/applyPurchaseHeader/approvedData",
		data:formObj.serialize(),
		success:function(data){
			layer.closeAll();
            $("#tabContent").empty();
			var str = data.toString();
			$("#tabContent").html(str);
            loadVerify();
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
// 审核
function verifyApplyPurchase(id){
	
	if($(".verifyDetail").length>0)$(".verifyDetail").remove();
	$.ajax({
		url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
		data:{"id":id},
		success:function(data){
			var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
			detailDiv.addClass("verifyDetail");
			detailDiv.html(data);
			detailDiv.appendTo($("#orderContent"));
			detailDiv.find("#verify_side").addClass("show");
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
//一键通过
function verifyAllApplyPurchase(){
	
	var checkedObj = $("#tabContent input[name='checkone']:checked");
	if(checkedObj.length == 0){
		layer.msg('至少选择一条数据！');
		return;
	}else{
		layer.msg('你确定一键通过？', {
				time : 0,//不自动关闭
				btn : [ '确定', '取消' ],
				yes : function(index) {
					
					layer.close(index);
					var orderIdArray = "";
					$.each(checkedObj,function(i,o){
						orderIdArray += $(this).val() + ",";
					});
					layer.load();
					$.ajax({
						url:basePath+"buyer/applyPurchaseHeader/verifyAll",
						async:false,
						data:{"id":orderIdArray},
						success:function(data){
							selectData();
						},
						error:function(){
							layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
						}
					});
				}
			});
		}
	}
	function selectAll(obj) {
		if ($(obj).is(":checked")) {
			$("#tabContent input[name='checkone']").prop("checked", true);
		} else {
			$("#tabContent input[name='checkone']").prop("checked", false);
		}
	}
</script>
<!--页签-->
<div id="orderContent">
	<!--搜索栏-->
	<form class="" action="buyer/applyPurchaseHeader/approvedHtml" id="orderListForm">
		<ul class="order_search">
			<li class="comm">
				<label>采购计划单号:</label>
				<input type="text" placeholder="输入采购计划单号进行搜索" style="width:160px" name="applyCode" id="applyCode" value="${params.applyCode}">
			</li>
			<li class="comm">
				<label>销售计划单号:</label>
				<input type="text" placeholder="输入销售计划单号进行搜索" style="width:160px" name="planCode" id="planCode" value="${params.planCode}">
			</li>
			<li class="comm">
				<label>货号:</label>
				<input type="text" placeholder="输入货号进行搜索" style="width:110px" name="procode" id="procode" value="${params.procode}">
			</li>
			<li class="comm">
				<label>条形码:</label>
				<input type="text" placeholder="输入条形码进行搜索" style="width:120px" name="skuoid" id="skuoid" value="${params.skuoid}">
			</li>
			<li class="range nomargin">
				<label>申请人:</label>
				<input type="text" placeholder="输入申请人进行搜索" style="width:120px" name="createName" id="createName" value="${params.createName}">
			</li>
			<li class="range"><label>下单日期:</label>
				<div class="layui-input-inline">
					<input style="width:80px" type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
				</div> -
				<div class="layui-input-inline">
					<input style="width:80px" type="text" name="endDate" id="endDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range">
				<button type="button" id="selectButton" class="search">搜索</button>
				<button type="reset" id="resetButton" class="search">重置查询条件</button>
			</li>
		</ul>
		<input type="hidden" name="tabId" id="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<a href="javascript:void(0);"
		class="layui-btn layui-btn-normal layui-btn-small" onclick="verifyAllApplyPurchase();">
		<i class="layui-icon">&#xe6a3;</i> 一键通过
	</a>
	<div id="tabContent"></div>
</div>