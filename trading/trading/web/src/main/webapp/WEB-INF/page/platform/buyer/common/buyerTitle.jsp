<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<script>
var menuId="<%=request.getParameter("menuId")%>";
$(function(){
	$("a").removeClass("hover");
	$("li[name='menu_"+menuId+"']").find("a").addClass("hover");
});
</script>
<!--一、导航条-->
<div class="nav">
	<div class="navbar">
		<img src="<%=basePath%>statics/platform/images/new_logo.png">
		<ul>
			<li name="menu_1"><a href="javacript:void(0)" onclick="titleMenuClick('platform/loadIndexHtml','');return false;">首页</a></li>
		<c:forEach var="titleMenu" items="${sessionScope.AUTHORITYSYSMENU}">
			<c:if test="${titleMenu.position=='top'}">
				<li name="menu_${titleMenu.id}"><a href="javacript:void(0)" onclick="titleMenuClick('${titleMenu.url}','${titleMenu.id}');return false;">${titleMenu.name}</a></li>
			</c:if>
		</c:forEach>
		</ul>
	</div>
</div>