<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<%--收货单列表--%>
<script type="text/javascript">
    $(function(){
        //重新渲染select控件
//        var form = layui.form;
//        form.render("select");
        //设置选择的tab
        $(".tab>a").removeClass("hover");
        $(".tab #tab_${params.tabId}").addClass("hover");
        if("${params.tabId}"!='0'){
            $("#receiveListForm select[name='interest']").prop("disabled", true);
        }
        //日期
        loadDate("startDate","endDate");
        //初始化数据
        queryReceiveData();
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#resetButton").click();
            var index = id.split("_")[1];
            $("#receiveListForm input[name='tabId']").val(index);
            loadPlatformData('content');
        });
    });
    //状态下拉改变
    function setInterest(value) {
		$("#tabId").val(value);
    }
    //查询数据
    function queryReceiveData(){
        var formObj = $("#receiveListForm");
        $.ajax({
            url : basePath+"platform/buyer/receive/loadReceiveData",
            data:formObj.serialize(),
            async:false,
            success:function(data){
                $("#receiveContent").empty();
                var str = data.toString();
                $("#receiveContent").html(str);
                $(".tab>a").removeClass("hover");
                $(".tab #tab_"+$("#tabId").val()).addClass("hover");
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
    /**
     * 导出
     */
    function exportOrder() {
        var formData = $("#receiveListForm").serialize();
        var url = basePath+"platform/buyer/receive/exportReceive?"+ formData;
        window.open(url);
    }
    function pushOMS(id){
    	$.ajax({
            url : basePath+"platform/buyer/receive/pushOMS",
            data:{"id":id},
            async:false,
            success:function(data){
            	$("#searchButton").click();
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待收货（<span>${params.waitReceiveNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_2">已收货（<span>${params.receivedNum}</span>）</a> <b>|</b>
</div>
<div>
	<!--列表区-->
	<form id="receiveListForm" method="post" class="layui-form" action="platform/buyer/receive/receiveList">
		<ul class="order_search">
			  <li class="comm">
	           <label>商品货号</label>:
	           <input type="text" placeholder="输入商品货号" id="productCode" name="productCode" value="${params.productCode}">
	         </li>
	         <li class="comm">
	           <label>商品名称</label>:
	           <input type="text" placeholder="输入商品名称"  id="productName" name="productName" value="${params.productName}">
	         </li>
	         <li class="comm">
	           	<label>规格代码</label>:
	            <input type="text" placeholder="输入规格代码"  id="skuCode" name="skuCode" value="${params.skuCode}">
	         </li>       
	         <li class="comm">
	           	<label>规格名称</label>:
	            <input type="text" placeholder="输入规格名称"  id="skuName" name="skuName" value="${params.skuName}">
	         </li>
	         <li class="comm">
	           <label>条形码</label>:
	           <input type="text" placeholder="输入条形码"  id="barcode" name="barcode" value="${params.barcode}">
	         </li>
			<li class="comm">
				<label>发货单号:</label>
				<input type="text" placeholder="输入发货单号" id="deliverNo" name="deliverNo" value="${params.deliverNo}">
			</li>
			<li class="comm">
				<label>订单号:</label>
				<input type="text" placeholder="输入订单号" id="orderCode" name="orderCode" value="${params.orderCode}">
			</li>
			<li class="comm">
				<label>供应商:</label>
				<input type="text" placeholder="输入供应商" id="supplierName" name="supplierName" value="${params.supplierName}">
			</li>
			<li class="range">
				<label>发货日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
				</div>-
				<div class="layui-input-inline">
					<input type="text" name="endDate" id="endDate" lay-verify="date" value="${params.endDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="state">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select name="interest" id="interest" lay-filter="aihao" >
						<option value="0" <c:if test="${params.interest==0}">selected</c:if>>全部</option>
						<option value="1" <c:if test="${params.interest==1}">selected</c:if>>待收货</option>
						<option value="2" <c:if test="${params.interest==2}">selected</c:if>>已收货</option>
					</select>
				</div>
			</li>

			<li class="state">
				<label>仓库:</label>
				<div class="layui-input-inline">
					<select id="warehouseId" name="warehouseId" lay-filter="aihao">
						<option value="" <c:if test="${params.warehouseId==''}">selected</c:if>>全部</option>
						<c:forEach var="warehouse" items="${warehouseList}">
							<option value="${warehouse.id}" <c:if test="${params.warehouseId==warehouse.id}">selected</c:if>>${warehouse.whareaName}</option>
						</c:forEach>
					</select>
				</div>
			</li>
			<li class="range">
				<label>到货日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="arrivalDateBegin" id="arrivalDateBegin" lay-verify="date" value="${params.arrivalDateBegin}" class="layui-input" placeholder="开始日">
				</div>-
				<div class="layui-input-inline">
					<input type="text" name="arrivalDateEnd" id="arrivalDateEnd" lay-verify="date" value="${params.arrivalDateEnd}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range">
				<button type="button" onclick="queryReceiveData()" class="search" id="searchButton">搜索</button>
				<button type="reset" id="resetButton" class="search">重置查询条件</button>
			</li>
		</ul>
		<input type="hidden" name="tabId" id="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="exportOrder();">
		<i class="layui-icon">&#xe8bf;</i> 导出
	</a>
	<div id="receiveContent"></div>
</div>