<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单我的付款明细</title>
	<%--<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billpaymentdetail/billPaymentDetailList.js"></script>--%>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#paymentId").val("${params.paymentId}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#paymentStatus").val("${params.paymentStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
	});


    //重置查询条件
    function resetformData(){
        $("#paymentId").val("");
        $("#createBillUserName").val("");
    	$("#sellerCompanyName").val("");
    	$("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
        $("#startTotalAmount").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
    	$("#paymentStatus").val("0");
    }

    // 审核
    function approveBillPayment(id){
        if($(".verifyDetail").length>0)$(".verifyDetail").remove();
        $.ajax({
            url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
            data:{"id":id},
            success:function(data){
                var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
                detailDiv.addClass("verifyDetail");
                detailDiv.html(data);
                detailDiv.appendTo($("#billPaymentContent"));
                detailDiv.find("#verify_side").addClass("show");
            },
            error:function(){
                layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
            }
        });
    }

    //一键通过
    function verifyAll(){
        var checkedObj = $("#billPaymentContent input[name='checkone']:checked");
        if(checkedObj.length == 0){
            layer.msg('至少选择一条数据！');
            return;
        }else{
            layer.msg('你确定一键通过？', {
                time : 0,//不自动关闭
                btn : [ '确定', '取消' ],
                yes : function(index) {
                    layer.close(index);
                    var orderIdArray = "";
                    $.each(checkedObj,function(i,o){
                        orderIdArray += $(this).val() + ",";
                    });
                    layer.load();
                    $.ajax({
                        url:basePath+"platform/buyer/billPaymentDetail/verifyBillPaymentAll",
                        async:false,
                        data:{"acceptPaymentId":orderIdArray},
                        success:function(data){
                            //selectData();
                            layer.msg("审批成功！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billPaymentDetail/approveBillPayment','buyer')
                        },
                        error:function(){
                            layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        }
    }
    function selectAll(obj) {
        if ($(obj).is(":checked")) {
            $("#billPaymentContent input[name='checkone']").prop("checked", true);
        } else {
            $("#billPaymentContent input[name='checkone']").prop("checked", false);
        }
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<div id="billPaymentContent">
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billPaymentDetail/approveBillPayment">
		<ul class="order_search">
			<li >
				<label>付款单号:</label>
				<input type="text" placeholder="输入对账单号" id="paymentId" name="paymentId" />
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="comm">
				<label>供应商名称:</label>
				<input type="text" placeholder="请输入供应商名称" id="sellerCompanyName" name="sellerCompanyName" >
			</li>
			<%--<li class="range">
				<label>申请付款额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
			</li>--%>
			<li class="range nomargin">
				<label>付款申请日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />

			<li>
			  <li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
				<button type="reset" class="search" onclick="resetformData();">重置</button>

			  </li>
			<a href="javascript:void(0);"
			   class="layui-btn layui-btn-normal layui-btn-small" onclick="verifyAll();">
				<i class="layui-icon">&#xe6a3;</i> 一键通过
			</a>
			</li>

		</ul>
	</form>
	  <table class="table_pure payment_list">
	  <thead>
	    <tr>
		  <td style="width:5%"><input type="checkbox" name="checkAll" onclick="selectAll(this);"></td>
		  <td style="width:13%">对账单号</td>
		  <td style="width:10%">付款单号</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">供应商</td>
		  <td style="width:10%">申请金额</td>
		  <td style="width:10%">应付款总额</td>
		  <td style="width:10%">已付款金额</td>
		  <td style="width:10%">应付款余额</td>
		  <td style="width:11%">申请时间</td>
		  <td style="width:11%">审批流程</td>
		  <td style="width:10%">操作</td>
	    </tr>
	  </thead>
	  <tbody>
       <c:forEach var="billPayment" items="${searchPageUtil.page.list}">
	    <tr class="breakType">
			<td style="text-align: center"><input type="checkbox" name="checkone" value="${billPayment.acceptPaymentId}"></td>
			<td style="text-align: center">${billPayment.reconciliationId}</td>
			<td>${billPayment.id}</td>
			<td>${billPayment.createBillUserName}</td>
			<td>${billPayment.sellerCompanyName}</td>
			<td>${billPayment.actualPaymentAmount}</td>
			<td>${billPayment.totalAmount}</td>
			<td>${billPayment.actualPaymentAmountSum - billPayment.actualPaymentAmount}</td>
			<%--<td style="text-align: center">${billPayment.residualPaymentAmount }</td>--%>
			<td style="text-align: center">${billPayment.totalAmount - billPayment.actualPaymentAmountSum + billPayment.actualPaymentAmount}</td>
			<td>${billPayment.paymentDateStr}</td>
			<td style="text-align: center">
				<div class="opinion_view">
					<span class="orange" data="${billPayment.acceptPaymentId}">查看审批流程</span>
				</div>
			</td>
			<td >
			  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/billPaymentDetail/showRecItemListNew?reconciliationId=${billPayment.reconciliationId}&paymentType=1','buyer')" class="layui-btn layui-btn-normal layui-btn-mini">账单详情</a>
			  <span class="layui-btn layui-btn-danger layui-btn-mini" onclick="approveBillPayment('${billPayment.acceptPaymentId}');">付款审批</span>
			</td>
		</tr>
	   </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
</div>

<!--确认付款-->
<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="billPayment">
		<ul id="billPaymentBody">
		</ul>
</div>