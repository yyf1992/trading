<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的收款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/billAdvanceList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载条件
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startAdvanceDate}");
        $("#endDate").val("${params.endAdvanceDate}");
        $("#advanceStatus").val("${params.advanceStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    //预付款单据
    var preview={
        LIWIDTH:108,//保存每个li的宽
        $ul:null,//保存小图片列表的ul
        moved:0,//保存左移过的li
        init:function(){//初始化功能
            this.$ul=$(".view>ul");//查找ul
            $(".view>a").click(function(e){//为两个按钮绑定单击事件

                e.preventDefault();
                if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
                    if($(e.target).is(".forward")){//如果是向前按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
                        this.moved++;//移动个数加1
                    }
                    else{//如果是向后按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
                        this.moved--;//移动个数减1
                    }
                    this.checkA();//每次移动完后，调用该方法
                }
            }.bind(this));
            //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
            this.$ul.on("mouseover","li>img",function(){
                var src=$(this).attr("src");//获得当前img的src
                //var i=src.lastIndexOf(".");//找到.的位置
                //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
                $(".big_img>img").attr("src",src);//设置中图片的src
            });
        },
        checkA:function(){//检查a的状态
            if(this.moved==0){//如果没有移动
                $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
            }
            else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
                $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
            }
            else{//否则，都启用
                $("[class^=backward]").attr("class","backward");
                $("[class^=forward]").attr("class","forward");
            }
        }
    };
    preview.init();

    //标签页改变
    function setStatus(obj,status) {
        $("#advanceStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
    //重置查询条件
    function resetformData(){
       // alert("进来了吗");
        $("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#advanceStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.advanceStatus eq '0'}">class="hover"</c:if>>所有（<span>${params.advanceCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.advanceStatus eq '1'}">class="hover"</c:if>>待内部审批（<span>${params.insideApprovalCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.advanceStatus eq '2'}">class="hover"</c:if>>内部审批中（<span>${params.insideAcceptCenter}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.advanceStatus eq '3'}">class="hover"</c:if>>待卖家审批（<span>${params.insideAcceptCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.advanceStatus eq '4'}">class="hover"</c:if>>内部已驳回（<span>${params.insideRejectCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'5')" <c:if test="${searchPageUtil.object.advanceStatus eq '5'}">class="hover"</c:if>>卖家待确认（<span>${params.advanceApproveCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'6')" <c:if test="${searchPageUtil.object.advanceStatus eq '6'}">class="hover"</c:if>>卖家已确认（<span>${params.advancePassCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'7')" <c:if test="${searchPageUtil.object.advanceStatus eq '7'}">class="hover"</c:if>>卖家已驳回（<span>${params.advanceRejectCount}</span>）</a> <b>|</b>
</div>
<!--内容-->
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billAdvance/billAdvanceList">
		<ul class="order_search">
			<li class="comm">
				<label>供应商名称:</label>
				<input type="text" placeholder="请输入供应商名称" id="sellerCompanyName" name="sellerCompanyName" >
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="range nomargin">
				<label>添加日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startAdvanceDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endAdvanceDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range nomargin">
				<label>预付款状态:</label>
				<div class="layui-input-inline">
					<select name="advanceStatus" id="advanceStatus" lay-filter="aihao">
						<option value="0" >所有</option>
						<option value="1" >待内部审批</option>
						<option value="2" >内部审批中</option>
						<option value="3" >待卖家审批</option>
						<option value="4" >内部已驳回</option>
						<option value="5" >卖家待确认</option>
						<option value="6" >卖家已确认</option>
						<option value="7" >卖家已驳回</option>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />

			<div class="rt">
				<%--<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="addAdvanceInfo();">
					<i class="layui-icon">&#xebaa;</i> 添加
				</a>--%>
				<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="leftMenuClick(this,'platform/buyer/billAdvance/addAdvanceJump','buyer')">
					<i class="layui-icon">&#xebaa;</i>新增预付款账户
				</a>
				<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small" onclick="buyerAdvanceExport();">
					<i class="layui-icon">&#xe8bf;</i> 导出
				</a>

			</div>

		</ul>
	</form>
    <table class="table_pure payment_list">
	  <thead>
	  <tr>
		  <td style="width:15%">供应商</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">添加日期</td>
		  <td style="width:10%">添加备注</td>
		  <td style="width:10%">可用金额</td>
		  <td style="width:10%">锁定金额</td>
		  <td style="width:10%">预付总金额</td>
		  <td style="width:10%">确认状态</td>
		  <td style="width:10%">卖家审批备注</td>
		  <td style="width:10%">操作</td>
	  </tr>
	  </thead>
	  <tbody>
	    <c:forEach var="billAdvance" items="${searchPageUtil.page.list}">
		  <tr class="breakType">
			  <td style="text-align: center" title="${billAdvance.sellerCompanyName}">
			    ${billAdvance.sellerCompanyName}</td>
			  <td>${billAdvance.createBillUserName}</td>
			  <td>${billAdvance.createTimeStr}</td>
			  <td>${billAdvance.createRemarks}</td>
			  <td>${billAdvance.usableTotal}</td>
			  <td>${billAdvance.lockTotal}</td>
			  <td>${billAdvance.advanceTotal}</td>
			  <td style="text-align: center">
			    <div>
					<c:choose>
						<c:when test="${billAdvance.status==1}">待内部审批</c:when>
						<c:when test="${billAdvance.status==2}">内部审批中</c:when>
						<c:when test="${billAdvance.status==3}">待卖家审批</c:when>
						<c:when test="${billAdvance.status==4}">内部已驳回</c:when>
						<c:when test="${billAdvance.status==5}">卖家待确认</c:when>
						<c:when test="${billAdvance.status==6}">卖家已确认</c:when>
						<c:when test="${billAdvance.status==7}">卖家已驳回</c:when>
					</c:choose>
				</div>
			    <div class="opinion_view">
				  <span class="orange" data="${billAdvance.acceptId}">审批流程</span>
			    </div>
			   <%-- <div>
				  <a href="javascript:void(0);" onclick="showAdvanceReceipt('${billAdvance.id}',
						  '${billAdvance.fileAddress}','${billAdvance.createBillUserName}',
						  '${billAdvance.createTimeStr}');" class="approval">付款票据</a>
			    </div>--%>
			    <div>
					<a href="javascript:void(0);" onclick="showAdvanceEditList('${billAdvance.id}',
							'${billAdvance.createBillUserId}','${billAdvance.createBillUserName}',
							'');" class="approval">充值记录</a>
			    </div>
			  </td>
			  <td style="text-align: center">${billAdvance.updateRemarks}</td>
			  <td>
			    <div>
				  <c:choose>
					  <c:when test="${billAdvance.status==1 }">
					    <a href="javascript:void(0);" class="layui-btn layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billAdvance/updateAdvanceJump?advanceId=${billAdvance.id}','buyer')">预付金额充值</a>
					    <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="acceptBillPayment('${billAdvance.id}','${billAdvance.status}','${billAdvance.acceptId}');">提交内部审批</span>

					  </c:when>
					  <c:when test="${billAdvance.status==3}">
						  <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="launchAdvance('${billAdvance.id}');">提交卖家审批</span>
					  </c:when>
					  <c:when test="${billAdvance.status==4 ||billAdvance.status==6|| billAdvance.status==7}">
						  <a href="javascript:void(0);" class="layui-btn layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billAdvance/updateAdvanceJump?advanceId=${billAdvance.id}','buyer')">预付金额充值</a>
					  </c:when>
				  </c:choose>
			    </div>
			  </td>
		  </tr>
	    </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
</div>

<!--预付款票据-->
<div id="advanceReceipt" class="receipt_content layui-layer-wrap" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<!--回显预付款修改记录-->
<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="advanceEditDiv">
	<div>
		<table class="table_yellow">
			<thead>
			<tr>
				<td style="width:10%">采购员</td>
				<td style="width:10%">充值人</td>
				<td style="width:10%">充值时间</td>
				<td style="width:10%">充值金额</td>
				<td style="width:10%">充值备注</td>
				<td style="width:10%">充值状态</td>
				<td style="width:10%">充值单据</td>
			</tr>
			</thead>
			<tbody id="advanceEditBody">
			</tbody>
		</table>
	</div>
	<!--自定义付款款凭据-->
	<div id="showAdvenceFiles" class="receipt_content" style="display:none;">
		<div id="showAdvanceBigImg" class="big_img">
			<%--<img id="bigImg">--%>
		</div>
		<div class="view">
			<a href="#" class="backward_disabled"></a>
			<a href="#" class="forward"></a>
			<ul id="fileAdvanceList" class="icon_list"></ul>
		</div>
	</div>
</div>
<style>
	.view .icon_list {
		height: 92px;
		position:absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
	.view .icon_list li img:hover {
		border: 2px solid #e4393c;
		padding: 0;
	}
</style>