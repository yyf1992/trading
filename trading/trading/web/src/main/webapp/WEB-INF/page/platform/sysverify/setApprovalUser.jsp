<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
$(function(){
	loadLeftUser();
});
function loadLeftUser(){
	var selectUsers = "";
	//流程选择人员
	$("#rightUser #add").find("input[name='userId']").each(function(){
		selectUsers += $(this).val() + ",";
	});
	$.ajax({
		url:"platform/sysVerify/loadPersonAutomatic",
		data:{
			"page.divId":"leftUser",
			"selectId":"rightUser",
			"selectType":"1",
			"selectUsers":selectUsers
		},
		success:function(data){
			$("#leftUser").html(data);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div style="width: 1000px;">
	<div id="leftUser" class="list"></div>
	<div id="rightUser" class="list">
		<ul id="add">
            <li class="person" style="margin-left: 25px;"><span class="userSpan">发起人</span></li>
        </ul>
	</div>
</div>