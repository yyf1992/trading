<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<%--合并发货界面--%>
<script>
    function subData(){
        //发货数量判断
        var checkDeliveryNum = false;
		//组装发货明细
        var deliveryItems = new Array();
        $("#detail_interworkGoods tr").each(function() {
            var orderItemId = $(this).find("input[name='orderItemId']").val();
            var applyCode = $(this).find("input[name='applyCode']").val();
            var productCode = $(this).find("input[name='productCode']").val();
            var productName = $(this).find("input[name='productName']").val();
            var skuCode = $(this).find("input[name='skuCode']").val();
            var skuName = $(this).find("input[name='skuName']").val();
            var barcode = $(this).find("input[name='barcode']").val();
            var unitId = $(this).find("input[name='unitId']").val();
            var salePrice = $(this).find("input[name='price']").val();
            var warehouseId = $(this).find("input[name='warehouseId']").val();
            var warehouseCode = "";
            var warehouseName = "";
            var deliveryNum = $(this).find("input[name='deliveryNum']").val();
            var deliveredNum = $(this).find("input[name='deliveredNum']").val();
            var status = $(this).find("input[name='status']").val();
            var isNeedInvoice = $(this).find("input[name='isNeedInvoice']").val();
            var remark = $(this).find("input[name='remark']").val();
            var orderId = $(this).find("input[name='orderId']").val();
            var buyOrderId = $(this).find("input[name='buyOrderId']").val();
            var buyOrderItemId = $(this).find("input[name='buyOrderItemId']").val();
			var item = new Object();
			item.orderId = orderId;
            item.orderItemId = orderItemId;
            item.applyCode = applyCode;
			item.buyOrderId = buyOrderId;
			item.buyOrderItemId = buyOrderItemId;
			item.productCode = productCode;
			item.productName = productName;
            item.unitId = unitId;
			item.skuCode = skuCode;
			item.skuName = skuName;
			item.barcode = barcode;
			item.salePrice = salePrice;
			item.warehouseId = warehouseId;
			item.warehouseCode = warehouseCode;
			item.warehouseName = warehouseName;
			item.deliveryNum = deliveryNum;
			item.deliveredNum = deliveredNum;
			item.status = status;
            item.isNeedInvoice = isNeedInvoice;
			item.remark = remark;
            deliveryItems.push(item);
            if(deliveryNum<=0){
                checkDeliveryNum = true;
                return;
            }
        });
        if(deliveryItems.length<=0){
            layer.msg("发货明细不能为空！",{icon:2});
            return;
        }
        if(checkDeliveryNum){
            layer.msg("发货数量不能为0！",{icon:2});
            return;
        }
        var waybillNo = $("#waybillNo").val();
        /*if(waybillNo == ''){
            layer.msg("请输入运单号码！",{icon:2});
            return;
        }*/
        var logisticsCompany = $("#logisticsCompany").val();
        /*if(logisticsCompany == ''){
            layer.msg("请输入物流公司名称！",{icon:2});
            return;
        }*/
        var driverName = $("#driverName").val();
        /*if(driverName == ''){
            layer.msg("请输入司机姓名！",{icon:2});
            return;
        }*/
        var mobilePhone = $("#mobilePhone").val();
        var zoneCode = $("#zoneCode").val();
        var fixedPhone = $("#fixedPhone").val();
        var extPhone = $("#extPhone").val();
        /*if(mobilePhone == ' '&& fixedPhone== ''){
            layer.msg("请输入手机号或固话号码！",{icon:2});
            return;
        }*/
        var freight = $("#freight").val();
        /*if(freight == ''){
            layer.msg("请输入商品运费！",{icon:2});
            return;
        }*/
        var expectArrivalDate = $("#expectArrivalDate").val();
        /*if(expectArrivalDate == ''){
            layer.msg("请选择预达日期！",{icon:2});
            return;
        }*/
        //组装物流信息
        var logistics = new Object();
        logistics.waybillNo = waybillNo;
        logistics.logisticsCompany = logisticsCompany;
        logistics.driverName = driverName;
        logistics.mobilePhone = mobilePhone;
        logistics.zoneCode = zoneCode;
        logistics.fixedPhone = fixedPhone;
        logistics.extPhone = extPhone;
        logistics.freight = parseFloat(freight);
        logistics.expectArrivalDate = expectArrivalDate;
        var lodIndex;
        $.ajax({
            type : "post",
            url : "platform/seller/delivery/sendGoodsSub",
            data : {
                "logistics" : JSON.stringify(logistics),
                "deliveryItems" : JSON.stringify(deliveryItems),
				"buyCompanyId":'${buyCompanyId}',
                "deliverType":'${deliverType}',
				"dropshipType":'0',//代发仓库（0.正常发货1.代发客户2.代发菜鸟仓3.代发京东仓）
				"receiveAddr":''
            },
            beforeSend: function () {
                lodIndex = layer.load(1);
            },
            complete: function () {
                layer.close(lodIndex);
            },
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("保存成功!",{icon:1});
                    leftMenuClick(this,"platform/seller/interworkGoods/interworkGoodsList.html","seller");
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error : function() {
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }

    //发货数量输入框改变事件
    function setDeliveryNum(obj,maxNum) {
        var deliveryNum = $(obj).val();
        if(deliveryNum<0){
            $(obj).val(maxNum);
            deliveryNum = maxNum;
        }
        if(maxNum-deliveryNum<0){
            $(obj).val(maxNum);
            deliveryNum = maxNum;
        }
        if(deliveryNum-maxNum==0){
            $(obj).parent().parent().find("[name='status_info']").val("0");
            $(obj).parent().parent().find("[name='status']").val("0");
        }else {
            $(obj).parent().parent().find("[name='status_info']").val("1");
            $(obj).parent().parent().find("[name='status']").val("1");
        }
    }
    //删除行
    function delRow(obj) {
        layer.confirm('确定删除该行？', {
            icon:3,
            title:'提醒',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(){
            //判断该订单下的明细有多少条，如果只有1条，则删除时把订单信息一块删除
			var orderItems = $(obj).closest("table").find("tr");
			if(orderItems.length==1){
                $(obj).closest("table").prev().remove();
                $(obj).closest("table").remove();
			}else {
                $(obj).parent().parent().remove();
			}
            layer.msg('删除成功',{time:200});
        }.bind(obj));

    }
</script>
<div>
	<!--列表区-->
	<form id="mySingleForm" method="post" action="">
		<table class="order_detail">
			<tbody>
			<tr>
				<td style="width:25%">商品</td>
				<td style="width:10%">条形码</td>
				<td style="width:10%">数量</td>
				<td style="width:10%">已发货数量</td>
				<td style="width:10%">待发货数量</td>
				<td style="width:10%">本次发货数量</td>
				<td style="width:10%">发货类型</td>
				<td style="width:10%">备注</td>
				<td style="width:5%">操作</td>
				<td style="display: none;">存放隐藏字段</td>
			</tr>
			</tbody>
		</table>
		<div class="deliver_list mt" id="detail_interworkGoods">
			<c:forEach var="order" items="${orderList}">
			<p>
				<span class="apply_t"><fmt:formatDate value="${order.createDate}" type="both"></fmt:formatDate></span>
				<span class="order_n">订单号: <b>${order.orderCode}</b></span>
				<span>采购商：<b>${order.companyName}</b></span>
			</p>
			<table>
				<c:forEach var="product" items="${order.supplierProductList}">
					<tr>
						<td style="width:25%">
							<%--<img src="<%=basePath%>/statics/platform/images/defaulGoods.jpg">--%>
							<div>${product.productCode} ${product.productName} <br>
								<span>规格代码: ${product.skuCode}</span>
								<span>规格名称: ${product.skuName}</span>
							</div>
						</td>
						<td style="width:10%">${product.barcode}</td>
						<td style="width:10%">${product.orderNum}</td>
						<td style="width:10%">${product.deliveredNum}</td>
						<td style="width:10%">${product.waitDeliveryNum}</td>
						<td style="width:10%">
							<input type="number" min="0" max="${product.orderNum-product.deliveredNum}" id="deliveryNum" name="deliveryNum" value="0" onchange="setDeliveryNum(this,${product.orderNum-product.waitDeliveryNum-product.deliveredNum});"/>
						</td>
						<td style="width:10%">
							<select id="status_info" name="status_info" onchange="getStatus(this.value);" disabled>
								<option value="">请选择</option>
								<option value="0">全部发货</option>
								<option value="1">部分发货</option>
							</select>
						</td>
						<td style="width:10%">
							<input type="text" id="remark" name="remark" value="" />
						</td>
						<td style="width:5%">
							<a href="javascript:void(0);" onclick="delRow(this);" class="layui-btn layui-btn-primary layui-btn-mini"><i class="layui-icon">&#xe7ea;</i>删除</a>
						</td>
						<td style="display: none;">
							<input type="hidden" id="applyCode" name="applyCode" value="${product.applyCode}" />
							<input type="hidden" id="productCode" name="productCode" value="${product.productCode}" />
							<input type="hidden" id="productName" name="productName" value="${product.productName}" />
							<input type="hidden" id="skuCode" name="skuCode" value="${product.skuCode}" />
							<input type="hidden" id="skuName" name="skuName" value="${product.skuName}" />
							<input type="hidden" id="barcode" name="barcode" value="${product.barcode}" />
							<input type="hidden" id="unitId" name="unitId" value="${product.unitId}" />
							<input type="hidden" id="price" name="price" value="${product.price}" />
							<input type="hidden" id="deliveredNum" name="deliveredNum" value="${product.deliveredNum}" />
							<input type="hidden" id="isNeedInvoice" name="isNeedInvoice" value="${product.isNeedInvoice}" />
							<input type="hidden" id="status" name="status" value="" />
							<input type="hidden" id="warehouseId" name="warehouseId" value="${product.warehouseId}" />
							<input type="hidden" id="warehouseName" name="warehouseName" value="" />
							<input type="hidden" id="orderId" name="orderId" value="${order.id}" />
							<input type="hidden" id="orderItemId" name="orderItemId" value="${product.id}" />
							<input type="hidden" id="buyOrderId" name="buyOrderId" value="${order.buyerOrderId}" />
							<input type="hidden" id="buyOrderItemId" name="buyOrderItemId" value="${product.buyOrderItemId}" />
						</td>
					</tr>
				</c:forEach>
			</table>
			</c:forEach>
		</div>
		<h4 class="page_title mp30">物流明细</h4>
		<div class="receive">
			<ul>
				<li>
					<label>运单号码：</label>
					<input type="text" required placeholder="输入运单号码" id="waybillNo" name="waybillNo" />
				</li>
				<li>
					<label>物流公司：</label>
					<input type="text" required placeholder="输入物流公司名称" id="logisticsCompany" name="logisticsCompany" />
				</li>

				<li>
					<label>司机姓名：</label>
					<input type="text" required placeholder="输入司机姓名" id="driverName" name="driverName" />
				</li>
				<li>
					<label>司机手机：</label>
					<input type="tel" required placeholder="手机与固定电话至少填一个" id="mobilePhone" name="mobilePhone" />
				</li>
				<li>
					<label>商品运费：</label>
					<input type="number" min="0" required placeholder="输入商品运费" id="freight" name="freight" />元
				</li>
				<li>
					<label>固定电话：</label>
					<input type="tel" placeholder="区号" style="width:55px" id="zoneCode" name="zoneCode" />—
					<input type="tel" placeholder="电话号码" style="width:103px" id="fixedPhone" name="fixedPhone" />—
					<input type="tel" placeholder="分机号" style="width:55px" id="extPhone" name="extPhone" />
				</li>
				<li>
					<label>预达日期：</label>
					<div class="layui-input-inline">
						<input type="text" name="expectArrivalDate" id="expectArrivalDate" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
					</div>
				</li>
			</ul>
		</div>
		<div class="btn_p text-center">
			<span class="order_p" onclick="subData();">保存</span>
		</div>

	</form>
</div>