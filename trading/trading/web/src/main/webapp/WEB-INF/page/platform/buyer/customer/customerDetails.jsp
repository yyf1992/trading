<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun"%>
<script type="text/javascript">
	$(function(){
		layer.photos({
			photos: '#layer-photos-demo',
			anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
		});
	});
</script>
<div>
	<div class="progress123">
		<span class="step1"> <b></b> 提交售后申请 </span>
		<span class="step2 current"> <b></b> 售后审批 </span>
		<span class="step3"> <b></b> 售后处理完成 </span>
	</div>
	<ul class="afterSale">
		<li><span>售后单号：</span>
			<p>${customer.customerCode}</p></li>
		<li><span>供应商名称：</span>
			<p>${customer.supplierName}</p></li>
		<li><span class="mt">售后商品：</span>
			<div class="afterList">
				<table class="table_pure afterDetail">
					<thead>
						<tr>
							<td>货号</td>
							<td>商品名称</td>
							<td>规格代码</td>
							<td>规格名称</td>
							<td>条形码</td>
							<td>价格</td>
							<td>售后类型</td>
							<td>售后数量</td>
							<td>单个返修金额</td>
							<td>售后总金额</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${itemList}">
							<tr>
								<td>${item.productCode}</td>
								<td>${item.productName}</td>
								<td>${item.skuCode}</td>
								<td>${item.skuName}</td>
								<td>${item.skuOid}</td>
								<td>${item.price}</td>
								<td>
									<c:choose>
										<c:when test="${item.type==0}">换货</c:when>
										<c:when test="${item.type==1}">退货</c:when>
										<c:when test="${item.type==2}">换货转退货</c:when>
									</c:choose>
								</td>
								<td>${item.goodsNumber}</td>
								<td>${item.exchangePrice}</td>
								<td>${item.repairPrice}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div></li>
		<li><span>申请原因：</span>
			<p>${customer.reason}</p>
		</li>
		<c:if test="${customer.proof != null && customer.proof != ''}">
		<li><span>售后凭证：</span>
			<p class="afterImg layer-photos-demo" id="layer-photos-demo">
				<c:forEach var="proof" items="${customer.proof.split(',')}">
					<img src="${proof}" layer-src="${proof}" title="点击查看大图">
				</c:forEach>
			</p>
		</li>
		</c:if>
		<li><span>申请状态：</span>
			<p>
				<c:choose>
					<c:when test="${customer.status==0}">待内部审批</c:when>
					<c:when test="${customer.status==1}">待对方同意</c:when>
					<c:when test="${customer.status==2}">已驳回</c:when>
					<c:when test="${customer.status==3}">已取消</c:when>
					<c:when test="${customer.status==4}">对方已确认</c:when>
					<c:when test="${customer.status==5}">待对方确认</c:when>
				</c:choose>
			</p></li>
		<li><span>申请人：</span>
			<p>${customer.createName}</p></li>
		<li><span>申请时间：</span>
			<p><fmt:formatDate value="${customer.createDate}" type="both"/></p></li>
		<li><span>收货地址：</span>
			<p>${el:getProvinceById(customer.province).province} ${el:getCityById(customer.city).city} 
				${el:getAreaById(customer.area).area} ${customer.addrName}（${customer.linkman} 收）${customer.linkmanPhone}</p></li>
	</ul>
	<div class="text-center">
		<c:if test="${custom.status==0 || custom.status==3}">
			<a href="javascript:void(0)" class="next_step" onclick="cancelApply('${customer.id}','${customer.status}');">取消申请</a>
		</c:if>
		<a href="javascript:void(0)" class="next_step" onclick="leftMenuClick(this,'${returnUrl}?${form}','buyer','${menuId}');">返回</a>
	</div>
</div>
