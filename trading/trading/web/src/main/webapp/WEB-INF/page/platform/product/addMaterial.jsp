<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/setPrice.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/commodity_all.css"></link>
<script type="text/javascript">
$(function(){
	loadUnit();
	$('.voucherImg').on('click','b',function(){
		 $(this).parent().remove();
	});
});
</script>
<!--内容-->
<div class="content_modify">
	<h3 class="edit_title">新增原材料</h3>
	<form class="layui-form" id="productForm">
		<ul class="edit_content">
			<li>
			   <label><b>*</b> 商品货号：</label> <input type="text" placeholder="请输入商品货号" name="productCode" id="productCode"> 
			   <span class="rt" style="width: 546px">
					<label><b class="red">*</b> 单位：</label>
					<div class="layui-input-inline" id="unitDivId">
						<%-- <select name="unitId" lay-filter="aihao">
							<c:forEach var="BuyUnit" items="${buyUnitList}">
								<option value="${BuyUnit.id}" id="${BuyUnit.id}">${BuyUnit.unitName}</option>
							</c:forEach>
						</select> --%>
					 </div>
					 <span class="unit_add" onclick="addUnit();"><img
						src="<%=basePath%>statics/platform/images/unitAdd.jpg">
				    </span> 
				</span>
			</li>
			<li><label><b>*</b> 商品名称：</label> <input type="text"
				style="width:665px;" placeholder="请输入商品名称" name="productName" id="productName">
			</li>
			<li><label><b>*</b> 商品简称：</label> <input type="text"
				style="width:665px;" placeholder="请输入商品简称" name="shortName" id="shortName">
			</li>
			<li><label><b>*</b> 商品类型：</label>
   			 	<input type="radio" name="productType" value="0" title="成品">
				<input type="radio" name="productType" value="1" title="原材料" checked>
			 	<input type="radio" name="productType" value="2" title="辅料">
			 	<input type="radio" name="productType" value="3" title="虚拟产品">
			</li>
			<li>
				<label><b>*</b> 商品主图：</label>
				<div class="size_sm mp30">
					<div class="upload_license">
						<input type="file" name="mainPictureUrl" id="mainPictureUrl" onchange="uploadImage();">
						<p></p>
						<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
						<div class="voucherImg" id="attachment3Div"></div>
					</div>
				</div>
			</li>
		</ul>
	</form>

	<div class="text-center">
 		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=1','baseInfo')">
			<input type="button" class="next_step" value="返回">
		</a>
		<button class="skuSave" id="commSubmit" onclick="saveMaterial(this);">保存</button>
    </div>
</div>