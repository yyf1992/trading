<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<script type="text/javascript">
// 检查是否为空共用方法
function checkEmpty(name,msg){
	var p = $("<p id='check"+name+"'></p>").addClass("error");
	var val = $("#" + name).val();
	if(val == ""){
		$("#check" + name).remove();
		p.append("<span></span>" + msg);
		$("#" + name).after(p);
	}
}
//检查密码是否准确
function checkY_Password(obj) {
	var password = $(obj).val();
	if (password != "") {
		$.ajax({
			type : "POST",
			url : "platform/reg/checkPassword",
			async : true,
			data : {
				"sysUserId" : $("#sysUserId").val(),
				"password" : password
			},
			success : function(data) {
				$("#checky_password").remove();
				var dataObj = eval('(' + data + ')');
				var resultObj = isJSONObject(dataObj)?result:eval('(' + dataObj + ')');
				var msgSpan = $("<p id='checky_password'></p>").addClass("error");
				if (resultObj.result) {
					msgSpan.append("<span></span>原始密码不正确,请重新填写！");
					$(obj).after(msgSpan);
					return;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
// 保存修改
$(function() {
	$(".modify_sure").click(
			function() {
				var obj = $(this).parents("form");
				checkEmpty("y_password","请输入原密码！");
				checkEmpty("password","请输入新密码！");
				checkEmpty("rePassword","请输入确认密码！");
				// 验证密码
				if($(obj).find("p[class='error']").length < 1){
					$.ajax({
						type : "post",
						url : "platform/sysUser/saveUpdatePassword",
						data : {
							"sysUserId" : $("#sysUserId").val(),
							"password" : $("#password").val()
						},
						async : false,
						success : function(data) {
							var result = eval('(' + data + ')');
							var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
							if (resultObj.success) {
								layer.msg(resultObj.msg, {icon : 1});
								return;
							} else {
								layer.msg(resultObj.msg, {icon : 1});
								return;
							}
						},
						error : function() {
							layer.msg("获取数据失败，请稍后重试！", {icon : 2});
						}
					});
				}
			});
});
// 检查密码是否为空
function checkPassword(obj){
	var password = $(obj).val();
	if(password != ""){
		$("#checkpassword").remove();
	}
}
//检查确认密码是否与上次一致
function checkRePassword(obj){
	var rePassword = $(obj).val();
	var password = $("#password").val();
	$("#checkrePassword").remove();
	if (rePassword != "" && rePassword != password) {
		var msgSpan = $("<p id='checkrePassword'></p>").addClass("error");
		msgSpan.append("<span></span>输入的密码与上面密码不一致，请重新输入密码！");
		$(obj).after(msgSpan);
	}
}
</script>
<div>
	<p class="modify_title">
		<span></span>为了您的账户安全使用，您需要设置登录密码保障服务
	</p>
	<p class="account">
		买卖账户: <span>${sysUser.loginName}</span>
	</p>
	<p class="setup">
		设置<span>登录密码</span>
	</p>
	<form action="" class="fill_in">
		<input type="hidden" id="sysUserId" name="sysUserId" value="${sysUser.id}" />
		<div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 原密码:</label>
				<input type="password" id="y_password" class="form_control" onchange="checkY_Password(this)">
			</div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 新密码:</label>
				<input type="password" id="password" class="form_control" onchange="checkPassword(this);">
				<p>密码长度8~16位，其中数字，字母和符号至少包含两种</p>
			</div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 确认密码:</label>
				<input type="password" id="rePassword" class="form_control" onchange="checkRePassword(this)">
			</div>
			<input type="button" class="modify_sure" value="确定修改" />
		</div>
	</form>
</div>