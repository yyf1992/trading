<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加客户</title>
<!-- 添加客户以及联系人的操作 -->
<script src="<%=basePath %>/statics/platform/js/addClient.js"></script>
<style type="text/css">
.reset_client{
  width:90px;
  height:30px;
  background:linear-gradient(to bottom,#fff,#E9E9E9);
  border:1px solid #D6D6D6;
  margin-top:60px;
  cursor: pointer;
  outline:0;
  font-size:12px;
}

</style>
</head>
<body>
     <div class="tab tab_seller">
       <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/addClient','sellers')" class="hover">添加客户</a> <b>|</b>
       <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/clientList','sellers')">我的客户</a>
     </div>
     <div class="add_supplier">
       <form action="" >
         <div class="form_group">
           <label class="control_label"><span class="red">*</span> 客 户 名 称:</label>
           <input type="text" id="clientName" name="clientName" class="form_control" required placeholder="请输入客户名称" onmouseout="check();">
         </div>
         <div class="form_group">
           <label class="control_label"><span class="red">*</span>银 行 账 号:</label>
           <input type="text" id="bankAccount" name="bankAccount" class="form_control" required placeholder="请输入银行账号" onmouseout="check();">
         </div>
         <div class="form_group">
           <label class="control_label"><span class="red">*</span>开 户 行:</label>
           <input type="text" id="openBank" name="openBank" class="form_control" required placeholder="请输入开户行" onmouseout="check();">
         </div>
         <div class="form_group">
           <label class="control_label"><span class="red">*</span>户 名:</label>
           <input type="text" id="accountName" name="accountName" class="form_control" required placeholder="请输入户名" onmouseout="check();">
         </div>
         <div class="form_group">
           <label class="control_label"><span class="red">*</span>纳税人识别号:</label>
           <input type="text" id="taxidenNum" name="taxidenNum" class="form_control" required placeholder="请输入纳税人识别号" onmouseout="check();">
         </div>
         <div class="setting">
           <h4>联系方式设置:</h4>
           <div class="btn_add mt">
             <a href="#" class="btn_new customer_a" ></a>
           </div>
           <table class="table_blue c_settings">
             <thead>
             <tr>
               <td style="width:5%">默认</td>
               <td style="width:8%">联系人</td>
               <td style="width:10%">手机号</td>
               <td style="width:15%">固话</td>
               <td style="width:16%;height:20%">收货地址</td>
               <td style="width:9%">传真</td>
               <td style="width:9%">QQ</td>
               <td style="width:9%">旺旺</td>
               <td style="width:9%">E-mail</td>
               <td style="width:9%">操作</td>
             </tr>
             </thead>
             <tbody id="ContactSettings">
               <tr style="height:20%">
               	 <td><input type="radio" name="default" checked onclick="setIsDefault(this)"></td><!-- 默认为1 -->             
                 <td><input type="text" placeholder="联系人"></td>
                 <td><input type="text" placeholder="手机"></td>
                 <td>
                   <input type="text" placeholder="区号" style="width:40px">
                   -<input type="text" style="width:70px" placeholder="电话">
                 </td>
                 <td onclick="addShipAddr(this);" >点击选择收货地址</td>
                 <td><input type="text" placeholder="传真"></td>
                 <td><input type="text" placeholder="QQ"></td>
                 <td><input type="text" placeholder="旺旺"></td>
                 <td><input type="text" placeholder="E-mail"></td>
                 <td> <!-- <span class="table_del"><b></b>删除</span> -->
			          <input type="hidden" id="isDefault" name="isDefault" value="1">
		         </td>
               </tr>
             </tbody>
           </table>
         </div>
         <div class="text-center">
       		<button type="reset" class="reset_client">重置</button>  
         	<input type="button" class="next_step" value="保存" onclick="saveClient();">
         </div>
       </form>
<!--选择收货地址-->
<div class="plain_frame edit_addr" style="display: none;">
    <ul>
      <li>
        <span>收货地址:</span>
        <div>
	        <select name="province3" id="addr1" onchange="addr1();">
	        	<option  value="">省份</option>
	        	<c:forEach var="province" items="${provinceList}">
				 	<option value="${province.provinceId}">${province.province}</option>
				</c:forEach>
	        </select>
	        <select  name="city3" id="addr2" onchange="addr2();">
	        	<option value="">地级市</option>
	        </select>
	        <select name="area3" id="addr3">
	        	<option value="">县区</option>
	        </select>
        </div>
      </li>
      <li>
      	<div>
        <span>详细地址:</span>
        <textarea style="width: 265px;height: 80px;vertical-align: top;padding: 5px;font-size: 12px;"  
        	 placeholder="请如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息" id="addr4"></textarea>
        </div>
      </li>
    </ul>
</div>
     </div>
</body>
</html>
