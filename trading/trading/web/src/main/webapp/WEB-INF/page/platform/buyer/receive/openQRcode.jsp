﻿<!DOCTYPE html>
<html lang="lang=zh-cmn-Hans">
<%@ include file="../../common/path.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>诺泰买卖</title>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/common.css">
    <link rel="stylesheet" href="<%=basePath%>/statics/plugins/layui/css/layui.css">
    <style>
        html,body{
            width: 100%;
            min-height: 100%;
            margin: 0;
            padding: 0;
            font-family: simhei;
            background: #F4F4F4;
        }
        .mobile-nav{
            height: 40px;
            line-height: 40px;
            background: #FF3100;
            color:#fff;
            font-size: 16px;
            text-align: center;
        }
        .mobile-card{
            padding: 20px 10px;
            background: #fff;
            margin-bottom: 10px;
            box-shadow: 0 1px 2px #eee;
        }
        .mobile-btn{
            position: fixed;
            bottom: 0;
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #FF5500;
            color:#fff;
            font-size: 16px;
            text-align: center;
        }
        .text-black{
            color: #000;
        }
        .mobile-upload-item{
            position: relative;
            display: inline-block;
            margin-bottom: 10px;
            margin-right: 15px;
        }
        .mobile-upload-item>b{
            position: absolute;
            right: -6px;
            top: -6px;
            width: 15px;
            height: 15px;
            text-align: center;
            line-height: 15px;
            cursor: pointer;
            background: #FF5500;
            border-radius: 50%;
            color: #fff;
            font-size: 12px;
        }
        .mobile-upload-item>img{
            width: 80px;
            height: 80px;
        }
        .mobile-upload-btn{
            display: inline-block;
            position: relative;
        }
        .mobile-upload-btn>input{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            z-index: 9999;
        }
    </style>
</head>
<body>
<div class="mobile-nav">
    <div class="layui-col-xs12">收货单上传</div>
</div>
<div class="mobile-card">
    <span class="">发货单号：</span>${deliverNo}
</div>
<div class="mobile-card">
    <p style="margin-bottom: 15px">
        <span class="mobile-upload-btn">
			<input id="upload" accept="image/*" type="file" onfocus="this.blur()" onchange="upload();" multiple />
			<span class="layui-btn layui-btn-danger layui-btn-small layui-btn-radius">
					<i class="layui-icon">&#xe687;</i>上传图片
			</span>
		</span>
    </p>
    <div class="mobile-upload" id="imgDiv">
        <c:forEach var="attachment" items="${attachmentList}">
            <div class="mobile-upload-item">
                <b onTouchstart="del(this)">x</b>
                <input type='hidden' name='fileUrl' value='${attachment.url}'>
                <img src="${attachment.url}"/>
            </div>
        </c:forEach>
    </div>
</div>
<div class="mobile-btn" onTouchstart="saveAttachment()">提交</div>
</body>
<script src="<%=basePath%>/statics/platform/js/j.js"></script>
<script>
    $(function() {

    });
    //上传操作
    function upload() {
        var formData = new FormData();
        var files = document.getElementById("upload").files;
        //追加文件数据
        for(var i=0;i<files.length;i++){
            formData.append("file[]", files[i]);
        }
        var url = "platform/buyer/receive/uploadReceiptAttachment";
        var xhr = new XMLHttpRequest();
        xhr.open("post", url, true);
        xhr.send(formData);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var b = xhr.responseText;
                var re = eval('(' + b + ')');
                var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
                if (resultObj.result == "success") {
                    for(var i=0;i<resultObj.urlList.length;i++){
                        var url = resultObj.urlList[i];
                        var addDiv = "<div class='mobile-upload-item'><b onTouchstart='del(this)'>x</b>"+
                            "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                            "<img src='"+url+"'/></div>";
                        $("#imgDiv").append(addDiv);
                    }
                    //重新设置input按钮，防止无法重复提交文件
                    $("#upload").replaceWith('<input type="file" accept="image/*" onfocus="this.blur();" multiple name="upload" id="upload" onchange="upload();" title="'+new Date()+'"/>');
                }else {
//                    layer.msg(resultObj.msg,{icon:2});
                    alert(resultObj.msg);
                }
            }
        }
    }
    //保存操作
    function saveAttachment() {
        debugger;
        var attachmentsArr = new Array();
        var attachments = $("input[name='fileUrl']");
        if(attachments==undefined||attachments.length<=0){
            alert("请先上传图片！");
            return;
        }
        if(attachments!=undefined&&attachments.length>0){
            for(var i=0;i<attachments.length;i++){
                attachmentsArr.push(attachments[i].value);
            }
        }
        $.ajax({
            type:"POST",
            url : "platform/buyer/receive/saveReceiptAttachment",
            async:false,
            data: {
                "deliverNo": '${deliverNo}',
                "attachments": JSON.stringify(attachmentsArr)
            },
            success:function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
                if(resultObj.success){
                    alert("提交成功！");
                }
            },
            error:function(){
                alert("操作失败，请重试！");
            }
        });
    }
    //判断是否是json对象
    function isJSONObject(obj){
        var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;
        return isjson;
    }
    //检测一下当前浏览器是否支持File Api
    function isSupportFileApi() {
        if(window.File && window.FileList && window.FileReader && window.Blob) {
            alert("支持File Api");
        }else {
            alert("不支持File Api");
        }
    }
    function del(el){
        $(el).parent().remove();
    }
</script>
</html>