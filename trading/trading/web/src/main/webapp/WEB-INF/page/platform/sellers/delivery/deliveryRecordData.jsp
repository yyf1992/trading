<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<script src="<%=basePath%>/js/delivery/deliveryRecordData.js"></script>
<%--发货单管理列表数据--%>
<table class="order_detail">
	<tr>
		<td style="width:80%">
			<ul>
				<li style="width:30%">商品</li>
				<li style="width:20%">订单号</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">发货数量</li>
				<li style="width:10%">到货数量</li>
				<!-- <li style="width:10%">发货类型</li> -->
				<li style="width:25%">备注</li>
			</ul>
		</td>
		<td style="width:10%">状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="deliveryRecord" items="${searchPageUtil.page.list}">
	<div class="order_list" id="dataList">
		<p>
			<span class="layui-col-sm1"><fmt:formatDate value="${deliveryRecord.createDate}" type="both"></fmt:formatDate></span>
			<span class="layui-col-sm2" >发货单号: <b>${deliveryRecord.deliverNo}</b></span>
			<span class="order_num">
				发货日期: 
				<b>
					<c:choose>
						<c:when test="${deliveryRecord.sendDate!=null}">
							<fmt:formatDate value="${deliveryRecord.sendDate}"/>
						</c:when>
						<c:otherwise>未发货</c:otherwise>
					</c:choose>
				</b>
			</span>
			<span class="order_num">
				到货日期:
				<b>
					<c:choose>
						<c:when test="${deliveryRecord.arrivalDate!=null}">
							<fmt:formatDate value="${deliveryRecord.arrivalDate}"/>
						</c:when>
						<c:otherwise>未到货</c:otherwise>
					</c:choose>
				</b>
			</span>
			<%--<span class="order_num">采购商: <b>${deliveryRecord.buycompanyName}</b></span>--%>
		</p>
		<table>
			<tr>
				<td style="width:80%">
					<c:forEach var="item" items="${deliveryRecord.itemList}" varStatus="i">
						<ul class="clear">
							<li style="width:30%">
								<img src="<%=basePath%>/statics/platform/images/defaulGoods.jpg">
								<div>
									${item.productCode} ${item.productName}<br>
									<span>规格代码: ${item.skuCode}</span><br>
									<span>规格名称: ${item.skuName}</span>
								</div>
							</li>
							<li style="width:20%;word-break:break-all;">
								<c:forEach var="orderCode" items="${fn:split(item.orderCode,',')}" >
									<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/interworkGoods/interworkGoodsDetail.html?orderCode=${orderCode}'+
											'&returnUrl=platform/seller/delivery/deliveryRecordList&menuId=17091509132210239677','buyer');" style="color: blue;">${orderCode}</a><br/>
								</c:forEach>
							</li>
							<li style="width:5%">
								<c:choose>
									<c:when test="${item.unitName != null}">${item.unitName}</c:when>
									<c:otherwise>件</c:otherwise>
								</c:choose>
							</li>
							<li style="width:10%">${item.deliveryNum}</li>
							<li style="width:10%;text-align: center;">${item.arrivalNum}</li>
							<%-- <li style="width:10%">
								<c:choose>
									<c:when test="${item.status == 0}">全部发货</c:when>
									<c:otherwise>部分发货</c:otherwise>
								</c:choose>
							</li> --%>
							<li style="width:25%;white-space: normal;">${item.remark}</li>
						</ul>
					</c:forEach>
				</td>
				<td style="width:10%">
					<c:choose>
						<c:when test="${deliveryRecord.recordstatus == 0}">待发货</c:when>
						<c:when test="${deliveryRecord.recordstatus == 1}">发货中</c:when>
						<c:when test="${deliveryRecord.recordstatus == 2}">已收货</c:when>
						<c:when test="${deliveryRecord.recordstatus == 3}">已终止</c:when>
						<c:otherwise>待发货</c:otherwise>
					</c:choose>
					<br/>
					<a href="javascript:void(0)" onclick="deliveryDetail('${deliveryRecord.id}');" class="approval">发货详情</a>
					<c:if test="${not empty deliveryRecord.receiptVoucherList && fn:length(deliveryRecord.receiptVoucherList) >0}">
						<br/><a href="javascript:void(0);"  class="approval" onclick="receiptAttachmentPreview('${deliveryRecord.deliverNo}');">查看收货单</a>
					</c:if>
					<br/>
					<c:choose>
						<c:when test="${deliveryRecord.reconciliationStatus == '0'}">未对账</c:when>
						<c:when test="${deliveryRecord.reconciliationStatus == '1'}">对帐中</c:when>
						<c:when test="${deliveryRecord.reconciliationStatus == '2'}">已对账</c:when>
						<c:otherwise>未对账</c:otherwise>
					</c:choose>
				</td>
				<td style="width:10%">
					<c:choose>
						<c:when test="${deliveryRecord.recordstatus == 0}">
							<a href="javascript:void(0)" onclick="confirmDelivery('${deliveryRecord.id}');" class="layui-btn layui-btn-mini"><i class='layui-icon'>&#xe8af;</i>确认发货</a>
						</c:when>
						<c:when test="${deliveryRecord.recordstatus == 1 && deliveryRecord.pushType=='F'}">
							<a href="javascript:void(0)" onclick="sendToWms('${deliveryRecord.deliverNo}');" class="layui-btn layui-btn-mini"><i class='layui-icon'>&#xe8af;</i>重新发货</a>
						</c:when>
						<c:when test="${deliveryRecord.recordstatus == 1}">
							<a href="javascript:void(0)" onclick="printReceipt('${deliveryRecord.id}');" class="layui-btn layui-btn-mini"><i class='layui-icon'>&#xea0e;</i>打印发货单</a>
						</c:when>
						<c:when test="${deliveryRecord.recordstatus == 2}">
							<c:if test="${empty receive.receiptVoucherList || empty deliveryRecord.receiptVoucherList}">
								<a href="javascript:void(0)" onclick="openUploadReceipt('${deliveryRecord.deliverNo}');" class="layui-btn layui-btn-mini"><i class='layui-icon'>&#xea5d;</i>上传收货单</a>
							</c:if>
						</c:when>
					</c:choose>
					<c:if test="${deliveryRecord.isSendWms == 'N'}">
						<br/><a href="javascript:void(0)" onclick="sendToWms('${deliveryRecord.deliverNo}');" class="layui-btn layui-btn-mini"><i class='layui-icon'>&#xea58;</i>推送WMS</a>
					</c:if>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
<%--打印收货回单--%>
<div id="receiptDiv" style="display: none;"></div>
<%--收货单上传弹出框--%>
<div id="uploadReceiptDiv" style="display: none;">
	<p>
		<span>(发货)编号：</span><span id="uploadDeliverNo"></span>
	</p>
	<label>收货回单:</label>
	<div class="upload_system">
		<input type="file" name="uploadReceipt" id="uploadReceipt" onchange="uploadReceipt();">
		<p></p>
		<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
	</div>
	<div class="scanning_copy original" id="addAttachmentDiv"></div>
</div>

<%--收货回单预览--%>
<div class='contractView layer-photos-demo' id='viewReceiptDiv' style="display: none;"></div>