<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(function(){
	//加载审批类型
	loadVerifyType("${searchPageUtil.object.verifyType}");
	$(".verifyDiv table tbody tr").click(function(e){
		trClick($(this));
	});
});
</script>
<div class="verifyDiv">
	<form action="platform/tradeVerify/mineOriginateList" id="searchForm">
		<ul class="order_search supplier_query">
			<li class="comm">
				<label>审批类型:</label>
				<span id="verifyTypeSpan"></span>
			</li>
			<li class="nomargin">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
		</ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td style="width:15%">审批编号</td>
					<td style="width:15%">审批标题</td>
					<td style="width:10%">申请人</td>
					<td style="width:15%">发起时间</td>
					<td style="width:15%">结束时间</td>
					<td style="width:10%">状态</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="verifyHeader" items="${searchPageUtil.page.list}">
					<tr>
						<td>${verifyHeader.id}</td>
						<td>${verifyHeader.create_name}的【${verifyHeader.title}】</td>
						<td>${verifyHeader.create_name}</td>
						<td><fmt:formatDate value="${verifyHeader.hCreated}" type="both"/></td>
					<c:choose>
						<c:when test="${verifyHeader.hStatus==0}">
							<td></td>
							<td>${verifyHeader.approvalUser}审批中</td>
								
						</c:when>
						<c:when test="${verifyHeader.hStatus==1}">
							<td><fmt:formatDate value="${verifyHeader.update_date}" type="both"/></td>
							<td>审批完成（通过）</td>
						</c:when>
						<c:when test="${verifyHeader.hStatus==2}">
							<td><fmt:formatDate value="${verifyHeader.update_date}" type="both"/></td>
							<td>审批完成（已拒绝）</td>
						</c:when>
						<c:when test="${verifyHeader.hStatus==3}">
							<td><fmt:formatDate value="${verifyHeader.update_date}" type="both"/></td>
							<td>已撤销</td>
						</c:when>
					</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page}</div>
	</form>
</div>