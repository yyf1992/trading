<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
$(function(){
	loadSupplier("${orderData.supplierId}");
	//重新渲染select控件
	var form = layui.form;
	form.render("select");
	//给select添加监听事件
	form.on("select(changeSupplier)", function(data){
		var selectValue = data.value;
		if(selectValue==''){
			$("input[name='sellerPerson']").val("");
			$("input[name='sellerPhone']").val("");
		}else{
			$("input[name='sellerPerson']").val(selectValue.split(",")[1]);
			$("input[name='sellerPhone']").val(selectValue.split(",")[2]);
		}
		//已选择商品清空
		$(".table_pure tbody").empty();
		$("button.tr_add").click();
	});
	$("input[type='number']").bind("change",function(){
		validateInput($(this));
	}).bind("keyup",function(){
		validateInput($(this));
	});
});
//加载互通卖家好友下拉
function loadSupplier(supplierId){
	var obj={};
	obj.divId="addOrderSupplierDiv";
	obj.selectName="supplierName";
	obj.selectId="supplierId";
	obj.filter="changeSupplier";
	obj.selectValue=supplierId;
	supplierSelect(obj);
}
//下一步
function nextOrder(obj){
	//供应商判空
	var supplierIdArr=$("#addOrderDiv #supplierId").val();
	if(supplierIdArr == ''){
		layer.msg("请选择供应商！",{icon:2});
		return;
	}
	var supplierId=supplierIdArr.split(",")[0];
	var supplierName=$("#addOrderDiv #supplierId option:selected").text();
	var sellerPerson=$("#addOrderDiv input[name='sellerPerson']").val();
	var sellerPhone=$("#addOrderDiv input[name='sellerPhone']").val();
	var skuoIdArr="";
	var pronameErrot="";
	var unitError="";
	var wareError="";
	var numError="";
	var goodsStr="";
	$("#addOrderDiv table>tbody>tr").each(function(i){
		var skuOid = $(this).find("input[name='skuOid']").val();
		if(skuOid != ''){
			var proCode = $(this).find("input[name='proCode']").val();
			var proName = $(this).find("input[name='proName']").val();
			var skuCode = $(this).find("input[name='skuCode']").val();
			var skuName = $(this).find("input[name='skuName']").val();
			if(skuoIdArr.indexOf(skuOid+",")>-1){
				pronameErrot = "第"+(i+1)+"行商品重复！";;
				return false;
			}else{
				skuoIdArr += skuOid+",";
			}
			//单位验证
			var unitId = $(this).find("td:eq(2) select").val();
			if(unitId==''){
				unitError="第"+(i+1)+"行没有设置单位！";
				return false;
			}
			var unitName = $(this).find("td:eq(2) select option:selected").text();
			//数量
			var num = $(this).find("td:eq(3) input").val();
			if(parseInt(num)<=0){
				numError="第"+(i+1)+"行数量必须大于0！";
				return false;
			}
			if(!objValidate($(this).find("td:eq(3) input"),2)){
				numError="第"+(i+1)+"行数量必须为整数！";
				return false;
			}
			//单价
			var price = $(this).find("input[name='price']").val();
			//总价
			var priceSum=$(this).find("td:eq(5)").html();
			//仓库验证
			var wareId = $(this).find("td:eq(6) select").val();
			if(wareId==''){
				wareError="第"+(i+1)+"行没有设置仓库！";
				return false;
			}
			var wareName = $(this).find("td:eq(6) select option:selected").text();
			//索要发票
			var isNeedInvoice = $(this).find("td:eq(7) select").val();
			//备注
			var remark = $(this).find("td:eq(8) textarea").val();
			goodsStr+=skuOid+","+proCode+","+proName+","+skuCode+","+skuName+","
				+unitId+","+unitName+","+num+","+price+","+priceSum+","
				+wareId+","+wareName+","+isNeedInvoice+","+remark+"@";
		}
	});
	if(skuoIdArr==''){
		layer.msg("请选择商品！",{icon:2});
		return;
	}
	if(pronameErrot!=''){
		layer.msg(pronameErrot,{icon:2});
		return;
	}
	if(unitError!=''){
		layer.msg(unitError,{icon:2});
		return;
	}
	if(wareError!=''){
		layer.msg(wareError,{icon:2});
		return;
	}
	if(numError!=''){
		layer.msg(numError,{icon:2});
		return;
	}
	summaryNum();
	var numSum = $("#addOrderDiv table tfoot .total:first").html();
	var totalPrice = $("#addOrderDiv table tfoot .total:eq(1)").html();
	$.ajax({
		url:"platform/buyer/purchase/next",
		data:{
			"supplierId":supplierId,
			"supplierName":supplierName,
			"sellerPerson":sellerPerson,
			"sellerPhone":sellerPhone,
			"goodsStr":goodsStr,
			"numSum":numSum,
			"totalPrice":totalPrice
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div id="addOrderDiv">
	<!--搜索部分-->
	<div class="search_supplier mt">
		<form class="layui-form" action="">
			<div class="layui-inline">
				<label class="layui-form-label">
					<span class="red">*</span>供应商:
				</label>
				<div class="layui-input-inline" style="width:200px" id="addOrderSupplierDiv">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>
					联系人:</label>
				<div class="layui-input-inline">
					<input type="text" name="sellerPerson" class="layui-input noclikc" value="${orderData.sellerPerson}" required disabled>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>
					手机号:</label>
				<div class="layui-input-inline">
					<input type="tel" name="sellerPhone" class="layui-input noclikc"
						value="${orderData.sellerPhone}" required disabled>
				</div>
			</div>
		</form>
	</div>
	<!--采购列表部分-->
	<table class="table_pure purchase_list mt">
		<thead>
			<tr>
				<td style="width:5%">删除</td>
				<td style="width:30%">商品</td>
				<td style="width:9%">单位</td>
				<td style="width:8%">数量</td>
				<td style="width:8%">单价</td>
				<td style="width:8%">总价</td>
				<td style="width:13%">仓库</td>
				<td style="width:7%">索要发票</td>
				<td style="width:12%">备注</td>
			</tr>
			<tr><td colspan="7"></td></tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${orderData != null}">
					<c:forEach var="dataItem" items="${orderData.itemArray}" varStatus="status">
						<tr>
							<td>
								<b onclick="deleteTr(this);"></b>
								<input type="hidden" name="proName" value="${dataItem.proName}">
								<input type="hidden" name="proCode" value="${dataItem.proCode}">
								<input type="hidden" name="skuCode" value="${dataItem.skuCode}">
								<input type="hidden" name="skuName" value="${dataItem.skuName}">
								<input type="hidden" name="skuOid" value="${dataItem.skuOid}">
								<input type="hidden" name="price" value="${dataItem.price}">
							</td>
							<td style="text-align: left;">
								<input type="text" name="proNameText" placeholder="输入商品名称/货号/条形码等关键字" 
									onkeyup="selectGoods(this);" 
									value="${dataItem.proCode}|${dataItem.proName}|${dataItem.skuCode}|${dataItem.skuName}|${dataItem.skuOid}">
							</td>
							<td>
								<div id="unitDiv_${status.count}"></div>
							</td>
							<td>
								<input type="number" value="${dataItem.num}" min="0">
							</td>
							<td>${dataItem.price}</td>
							<td>${dataItem.priceSum}</td>
							<td>
								<div id="warehouseDiv_${status.count}"></div>
							</td>
							<td>
								<select name="isNeedInvoice" class="isInvoice">
									<option value="N" <c:if test="${dataItem.isNeedInvoice=='N'}">selected</c:if>>否</option>
									<option value="Y" <c:if test="${dataItem.isNeedInvoice=='Y'}">selected</c:if>>是</option>
								</select>
							</td>
							<td><textarea name="remarks" placeholder="备注内容">${dataItem.remark}</textarea></td>
						</tr>
						<script>
							//初始化
							//单位下拉
							var unitObj={};
							unitObj.divId="unitDiv_${status.count}";
							unitObj.selectValue="${dataItem.unitId}";
							unitSelect(unitObj);
							//仓库下拉
							var wareObj={};
							wareObj.divId="warehouseDiv_${status.count}";
							wareObj.selectValue="${dataItem.wareId}";
							warehouseSelect(wareObj);
						</script>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td>
							<b onclick="deleteTr(this);"></b>
							<input type="hidden" name="proName">
							<input type="hidden" name="proCode">
							<input type="hidden" name="skuCode">
							<input type="hidden" name="skuName">
							<input type="hidden" name="skuOid">
							<input type="hidden" name="price">
						</td>
						<td style="text-align: left;">
							<input type="text" name="proNameText" placeholder="输入商品名称/货号/条形码等关键字" onkeyup="selectGoods(this);">
						</td>
						<td>
							<div id="unitDiv_0"></div>
						</td>
						<td>
							<input type="number" value="0" min="0">
						</td>
						<td>0</td>
						<td>0</td>
						<td>
							<div id="warehouseDiv_0"></div>
						</td>
						<td>
							<select name="isNeedInvoice" class="isInvoice">
								<option value="N" >否</option>
								<option value="Y">是</option>
							</select>
						</td>
						<td><textarea name="remarks" placeholder="备注内容"></textarea></td>
					</tr>
					<script>
						//初始化
						//单位下拉
						var unitObj={};
						unitObj.divId="unitDiv_0";
						unitObj.selectValue="17070710044995394265";
						unitSelect(unitObj);
						//仓库下拉
						var wareObj={};
						wareObj.divId="warehouseDiv_0";
						warehouseSelect(wareObj);
					</script>
				</c:otherwise>
			</c:choose>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td>合计</td>
				<td></td>
				<td class="total">${numSum}</td>
				<td></td>
				<td  class="total">${totalPrice}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
	</table>
	<button class="tr_add" onclick="addOrderTr(this);"></button>
	<div class="text-center">
		<a href="javascript:void(0)"><button class="next_step" onclick="nextOrder(this);">下一步</button> </a>
	</div>
</div>