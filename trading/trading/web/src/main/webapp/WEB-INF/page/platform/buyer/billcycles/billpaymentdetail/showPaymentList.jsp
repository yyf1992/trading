<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单我的付款明细</title>
	<%--<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billpaymentdetail/billAdvanceList.js"></script>--%>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<script type="text/javascript">
        //收款凭据
        function showBillReceipt(receiptAddr) {
            findBillReceipt(receiptAddr);
            layer.open({
                type: 1,
                title: '付款票据',
                area: ['auto', 'auto'],
                skin:'popBuyer',
                //btns :2,
                closeBtn:2,
                btn:['关闭窗口'],
                content: $('#linkReceipt'),
                yes : function(index){
                    layer.close(index);
                }
            });
        }

        //收据发票(退货)
        function findBillReceipt(receiptAddr) {
            $(".big_img").empty();
            $("#icon_list").empty();
            if(receiptAddr != null || receiptAddr != ''){
                var receiptAddrList = receiptAddr.split(',');
                var bigImg = '';
                for(i = 0;i<receiptAddrList.length;i++){
                    var liObj = $("<li>");
                    $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
                    $("#icon_list").append(liObj);
                    bigImg = receiptAddrList[0];
                }
                //大图
                $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
            }
        }

        //返回收货列表
        function returnReceiveList() {
            leftMenuClick(this,"platform/buyer/billPaymentDetail/billPaymentDetailList","buyer");
        }

        /**
         * 导出
         */
        function buyPaymentItemExport() {
            // var formData = $("#searchForm").serialize();
            var reconciliationId = $("#reconciliationId").val();
            var url = "platform/buyer/billPaymentDetail/buyPaymentItemExport?reconciliationId="+ reconciliationId;
            window.open(url);
        }
	</script>
</head>
<!--内容-->
<!--页签-->
<div>
	<!--搜索栏-->
	  <table class="table_pure payment_list">
	  <thead>
	    <tr>
		  <td style="width:16%">付款单号</td>
		  <td style="width:10%">付款金额</td>
		  <%--<td style="width:16%">付款账号</td>--%>
		  <td style="width:10%">付款类型</td>
		  <td style="width:12%">付款时间</td>
		  <td style="width:15%">备注</td>
		  <td style="width:10%">状态</td>
		  <%--<td style="width:10%">操作</td>--%>
	    </tr>
	  </thead>
	  <tbody>
       <c:forEach var="billPayment" items="${billPaymentList}">
	    <tr>
			<td>
				<input type="hidden" id="reconciliationId" value="${billPayment.reconciliationId}">
				${billPayment.id}</td>
			<td>${billPayment.actualPaymentAmount}</td>
			<%--<td>${billPayment.bankAccount}</td>--%>
			<td>
			  <c:choose>
				<c:when test="${billPayment.paymentType==0}">现金</c:when>
				<c:when test="${billPayment.paymentType==1}">银行转账</c:when>
			    <c:when test="${billPayment.paymentType==2}">支付宝</c:when>
			    <c:when test="${billPayment.paymentType==3}">微信</c:when>
			  </c:choose>
			</td>
			<td>${billPayment.paymentDateStr}</td>
			<td>${billPayment.paymentRemarks}</td>
			<%--<td>${billPayment.paymentStatus}</td>--%>
			<td>
			  <c:choose>
				<c:when test="${billPayment.paymentStatus==0}">付款待确认</c:when>
				<c:when test="${billPayment.paymentStatus==1}">卖家待确认</c:when>
			    <c:when test="${billPayment.paymentStatus==2}">内部已驳回</c:when>
			    <c:when test="${billPayment.paymentStatus==3}">卖家已确认</c:when>
			  </c:choose>
			</td>
			<%--<td>
				<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${billPayment.paymentAddr}');">查看票据</span>
			</td>--%>
		</tr>
	   </c:forEach>
	  </tbody>
    </table>
    <div class="pager"></div>
	<%--</form>--%>
	<div class="print_b">
		<span class="table_n" onclick="buyPaymentItemExport();"><i class="layui-icon">&#xe7a0;</i> 导出</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer');">返回</span>
	</div>
	<%--<div class="text-right" style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/applyPurchaseList','buyer');"><span class="contractBuild">返回</span></a>
	</div>--%>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<!--确认付款-->
<div class="plain_frame bill_exam" style="display:none;" id="billPayment">
		<ul id="billPaymentBody">
		</ul>
</div>