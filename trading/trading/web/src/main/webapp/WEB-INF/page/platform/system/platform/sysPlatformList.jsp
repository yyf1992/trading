<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@	taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
function insert(){
		$.ajax({
		url:basePath+"platform/sysplatform/loadAddSysPlatform",
		type:"post",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				title:"添加平台",
				skin: 'layui-layer-rim',
  		        area: ['400px', 'auto'],
  		        content:data,
  		        btn:['确定','取消'],
  		        yes:function(index,layerio){
  		        	$.ajax({
  		        		url:basePath+"platform/sysplatform/saveInsert",
  		        		type:"post",
  		        		data:$("#sysPlatform").serialize(),
  		        		async:false,
  		        		success:function(data){
  		        			var result=eval('('+data+')');
					    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysplatform/loadSysPlatformList','system','17100920493416593335');
							}else{
								layer.msg(resultObj.msg,{icon:2});
							}
  		        		},
  		        		error:function(){
  		        			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		        		}
  		        	});
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

function editPlatform(id){
	$.ajax({
		url:basePath+"platform/sysplatform/loadEditPlatform",
		async:false,
		type:"post",
		data:{"id":id},
		success:function(data){
			layer.open({
			type:1,
			title:"修改平台",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:basePath+"platform/sysplatform/updateSysPlatform",
  		    		data:$("#editSysPlatform").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/sysplatform/loadSysPlatformList','system','17100920493416593335');
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/sysplatform/loadSysPlatformList','system','17100920493416593335');
						}
				},
  		    		error:function(){
  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="content_modify">
      <div class="content_role">
        <form class="layui-form" action="platform/sysplatform/loadSysPlatformList">
        <ul class="order_search platformSearch">
          <li class="state">
            <label>平台名称：</label>
            <input type="text" placeholder="输入平台名称" name="platformName" value="${searchPageUtil.object.platformName}">
          </li>
          <li class="state">
            <label>加入日期：</label>
            <div class="layui-input-inline">
              <input type="text" name="createDate" value="${searchPageUtil.object.createDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
            </div>
          </li>
          <li class="state">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select id="status" name="status" lay-filter="aihao">
					<option value="-1" <c:if test="${searchPageUtil.object.status=='-1'}">selected="selected"</c:if>>全部</option>
					<option value="0" <c:if test="${searchPageUtil.object.status==0}">selected="selected"</c:if>>正常</option>
					<option value="1" <c:if test="${searchPageUtil.object.status==1}">selected="selected"</c:if>>禁用</option>
					</select>
				</div>
          </li>
          <li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
        </ul>
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
	    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
        </form>
        <div class="mt">
		  	<a href="javascript:void(0)" button="新增"
		  		class="layui-btn layui-btn-add layui-btn-small rt"
		  		onclick="insert();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
        <table class="table_pure platformList">
          <thead>
          <tr>
            <td style="width:10%">编号</td>
            <td style="width:25%">平台名称</td>
            <td style="width:15%">加入日期</td>
            <td style="width:15%">状态</td>
            <td style="width:15%">操作日期</td>
            <td style="width:20%">操作</td>
          </tr>
          </thead>
          <tbody>
          	<c:forEach var="SysPlatform" items="${searchPageUtil.page.list}" varStatus="status">
          		<tr>
              		<td>${status.count}</td>
              		<td>${SysPlatform.platformName}</td>
              		<td><fmt:formatDate value="${SysPlatform.createDate}" type="both"/></td>
              		<td>
              			<c:choose>
						<c:when test="${SysPlatform.isDel==0}">正常</c:when>
						<c:otherwise>禁用</c:otherwise>
						</c:choose>
              		</td>
              		<td>
              		   <c:choose>
              			<c:when test="${SysPlatform.updateDate==null}"><fmt:formatDate value="${SysPlatform.createDate}" type="both"/></c:when>
              			<c:otherwise><fmt:formatDate value="${SysPlatform.updateDate}" type="both"/></c:otherwise>
              		   </c:choose>
              		</td>
              		<td>
              			<a href="javascript:void(0)" button="修改"
						onclick="editPlatform('${SysPlatform.id}');"
						class="layui-btn layui-btn-update layui-btn-mini">
						<i class="layui-icon">&#xe7e9;</i>修改</a>	
              		</td>
            	</tr>
          	</c:forEach>
          </tbody>
        </table>
		<div class="pager">${searchPageUtil.page}</div>
      </div>
</div>