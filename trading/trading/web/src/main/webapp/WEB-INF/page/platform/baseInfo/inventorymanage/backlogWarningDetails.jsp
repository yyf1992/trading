<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<title>商品库存明细</title>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/commodity_all.css"></link>
<style>

</style>
<div>
  <form action="platform/baseInfo/inventorymanage/backlogWarningDetails">
   <div class="stock_explain">
   <h3 class="stock_detail mt">商品库存明细</h3>
   <c:if test="${buyProduct.mainPictureUrl !=null }">
    	<img src="${buyProduct.mainPictureUrl }">
    </c:if>	
  	<c:if test="${buyProduct.mainPictureUrl ==null }">
    	 <span class="defaultImg"></span>
    </c:if>	
    <ul>
  	  <li >
        <span>商品名称:</span>
        <p>${buyWarehouseBacklog.productName}</p>
      </li>
      <li>
        <span>商品货号:</span>
        <p>${buyWarehouseBacklog.productCode}</p>
      </li>
      <li>
        <span>规格代码:</span>
        <p>${buyWarehouseBacklog.skuCode}</p>
      </li>
      <li>
        <span>规格名称:</span>
        <p>${buyWarehouseBacklog.skuName}</p>
      </li>
      <li>
        <span>条形码:</span>
        <p>&nbsp;${buyWarehouseBacklog.barcode}</p>
      </li>
    </ul>
    </div>
   <input id="id" name="id" type="hidden" value="${buyWarehouseBacklog.id}" />
   <input type="hidden" name="barcode" id="barcode" value="${buyWarehouseBacklog.barcode}"> 
   <input type="hidden" name="stocks" id="stocks" value="${buyWarehouseBacklog.stocks}">
   <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
   <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />

   <c:choose>
   	<c:when test="${searchPageUtil.page.list.size()==0}">
   		<table  class="table_pure ">
	     <thead>
	       <tr>
	         <td style="width:10%">订单类型</td>
	         <td style="width:15%">来源单号</td>
	         <td style="width:10%">到货日期</td>
	         <td style="width:10%">到货数量</td>
	         <td style="width:10%">积压库存</td>
	         <td style="width:10%">库存积压时间</td>
	       </tr>
	     </thead>
	   </table>
	   	<div class="noneData">
			<p>自2016年7月1日OMS系统无入库数据！</p>
		</div>
   	</c:when>
   	<c:otherwise>
	   <table  class="table_pure ">
	     <thead>
	       <tr>
	         <td style="width:10%">订单类型</td>
	         <td style="width:15%">来源单号</td>
	         <td style="width:10%">到货日期</td>
	         <td style="width:10%">到货数量</td>
	         <td style="width:10%">积压库存</td>
	         <td style="width:10%">库存积压时间</td>
	       </tr>
	     </thead>
	     <tbody id="WarningDetails">
	       <c:forEach var="prodcutsList" items="${searchPageUtil.page.list}" varStatus="status">
	      	<tr>
	      	    <td>
	      	    	<c:if test="${prodcutsList.businessType == '29'}">采购系统入库</c:if>
	      	    	<c:if test="${prodcutsList.businessType == '21'}">OMS采购入库</c:if>
	      	    	<c:if test="${prodcutsList.businessType == '31'}">盘点入库</c:if>
	      	    	<c:if test="${prodcutsList.businessType == 'SW'}">同步WMS入库</c:if>
	      	    </td>
		        <td title="${prodcutsList.batchNo}">${prodcutsList.batchNo}</td>
		        <td><fmt:formatDate value="${prodcutsList.storageDate}" type="both"/></td>
		        <td>${prodcutsList.number}</td> 
		        <td>${prodcutsList.remainingStock}</td>
		        <td>${prodcutsList.backlogdaynum}</td>
	      	</tr>
	      </c:forEach>
	     </tbody>
	   </table>
	  </c:otherwise>
   </c:choose>
   <div class="pager">${searchPageUtil.page}</div>
  </form>
   <div class="btn_p text-center mp30">
 		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/baseInfo/inventorymanage/backlogWarning','baseInfo')">
			<span class="preview order_p_s">返回</span>
		</a>
  </div>
</div>
