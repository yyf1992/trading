<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%@ include file="../../common/path.jsp"%>
<script type="text/javascript">
	$(function(){
	// 关闭图标
	$('.address_list>div').hover(function(){
		  $(this).toggleClass('enter');
		  $(this).children('b').toggle();
	});
	//新增收货地址
	$('.address_new>button').click(function(){
		$.ajax({
			type: "POST",
	  		url: "platform/address/addAddress",
	  		success: function(data){
	  			layer.open({
	  			    type: 1,
	  			    title:'新增收货地址',
	  			    skin: 'layui-layer-rim', //加上边框
	  			    area: ['770px', 'auto'], //宽高
	  			    btn:['确定','关闭'],
	  			    content:data, //$('.address_add')
	  			  	yes: function(index, layero){
	  			  		var isDefault = 1;
		  			  	if($("#isDefault").is(":checked")){//选中  
		  			  		isDefault = 0
		  			  	}
	  			    	// 确定保存
		  			  	$.ajax({
		  					type : "POST",
		  					url : "platform/address/saveAddAddress",
		  					async: false,
		  					data : {
		  						"personName" : $("#personName").val(),
		  						"province" : $("#province").val(),
		  						"city" : $("#city").val(),
		  						"area" : $("#area").val(),
		  						"addrName" : $("#addrName").val(),
		  						"phone" : $("#phone").val(),
		  						"zone" : $("#zone").val(),
		  						"telNo" : $("#telNo").val(),
		  						"isDefault" : isDefault
		  					},
		  					success : function(data) {
		  						var result = eval('(' + data + ')');
		  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
		  						if(resultObj.success){
		  							layer.close(index);
	  								leftMenuClick(this,'platform/address/loadAddressList','system','17100920490553231325')
		  							layer.msg(resultObj.msg,{icon:1});
		  						}else{
		  							layer.msg(resultObj.msg,{icon:2});
		  							return false;
		  						}
		  					},
		  					error : function() {
		  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  					}
		  				});
	  			  	}
	  			});
	  		},
	  		error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
	 	});
	});
});
//修改收货地址
function loadUpdateaddress(id){
	$.ajax({
		type: "POST",
  		url: "platform/address/updateAddress",
  		data : {
  			"id" : id
  		},
  		success: function(data){
  			layer.open({
  		      type: 1,
  		      title:'修改收货地址',
  		      skin: 'layui-layer-rim',
  		      area: ['770px', 'auto'],
  		      btn:['确定','关闭'],
  		      content:data,
  		    	yes: function(index, layero){
			  		var isDefault = 1;
	  			  	if($("#updateIsDefault").is(":checked")){//选中  
	  			  		isDefault = 0
	  			  	}
				    // 确定保存
	  			  	$.ajax({
	  					type : "POST",
	  					url : "platform/address/saveUpdateAddress",
	  					async: false,
	  					data : {
	  						"id" : $("#updateId").val(),
	  						"personName" : $("#updatePersonName").val(),
	  						"province" : $("#updateProvince").val(),
	  						"city" : $("#updateCity").val(),
	  						"area" : $("#updateArea").val(),
	  						"addrName" : $("#updateAddrName").val(),
	  						"phone" : $("#updatePhone").val(),
	  						"zone" : $("#updateZone").val(),
	  						"telNo" : $("#updateTelNo").val(),
	  						"isDefault" : isDefault
	  					},
	  					success : function(data) {
	  						var result = eval('(' + data + ')');
	  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  						if(resultObj.success){
	  							layer.close(index);
	  								leftMenuClick(this,'platform/address/loadAddressList','system','17100920490553231325')
	  							layer.msg(resultObj.msg,{icon:1});
	  						}else{
	  							layer.msg(resultObj.msg,{icon:2});
	  							return false;
	  						}
	  					},
	  					error : function() {
	  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  					}
	  				});
			  	}
  		    });
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
// 删除收货地址
function deleteaddress(obj){
	layer.confirm("<p style='text-align:left; padding-left:50px;'>确定要删除该地址?<br>删除后无法找回噢~</p>",{
	      skin:"layui-layer-rim",
	      title:"提醒",
	      area: ["320px","auto"]
	    },function(index){
	     	// 确定保存
		  	$.ajax({
				type : "POST",
				url : "platform/address/saveDeleteAddress",
				async: false,
				data : {
					"id" : $(obj).attr("id")
				},
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if(resultObj.success){
						layer.close(index);
				      	$(obj).parent().addClass('delete');
					}else{
						layer.close(index);
						layer.msg(resultObj.msg,{icon:2});
						return false;
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	 }.bind(obj));
}
</script>
<div>
	<p class="address_new" button="新增">
		地址管理
		<button>+ 新增收货地址</button>
	</p>
	<div class="address_list">
		<!-- 非默认 -->
		<c:forEach var="address" items="${addressList}">
			<div>
				<b id="${address.id}" onclick="deleteaddress(this);" button="删除"></b>
				<c:if test="${address.isDefault == 0}">
					<img src="<%=basePath%>statics/platform/images/default.jpg">
				</c:if>
				<div class="address_detail">
					<p class="address_name">
						<span>${address.personName}</span>&nbsp;收
					</p>
					<p class="address_addr">
						<b>${el:getProvinceById(address.province).province}</b> <b>${el:getCityById(address.city).city}</b>
						<b>${el:getAreaById(address.area).area}</b> <span>${address.addrName}</span>
					</p>
					<p class="address_tel">${address.phone}</p>
				</div>
				<div class="address_edit">
					<span title="修改" onclick="loadUpdateaddress('${address.id}');" button="修改">
					<b></b>
					</span>
				</div>
			</div>
		</c:forEach>
	</div>
</div>
