<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td>
			<ul>
				<li>商品</li>
				<li>条形码</li>
				<li>单位</li>
				<li>退/换货数量</li>
			</ul></td>
		<td>申请原因</td>
		<td>状态</td>
	</tr>
</table>
<div class="orderList serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
		<div>
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> <span class="order_num"><span>编号:</span>
					${customer.customerCode}</span> <span class="order_name">${el:getCompanyById(customer.companyId).companyName}</span>
			</p>
			<table>
				<tr>
					<td><c:forEach var="customerItem" items="${customer.itemList}">
							<ul class="clear">
								<li><span class="defaultImg"></span>
									<div>
										${customerItem.productCode}|${customerItem.productName} <br>
										<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
											${customerItem.skuName}</span>
									</div></li>
								<li>${customerItem.skuOid}</li>
								<li>${el:getUnitById(customerItem.unitId).unitName}</li>
								<li>${customerItem.goodsNumber}</li>
							</ul>
						</c:forEach></td>
					<td>${customer.reason}</td>
					<td>
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待我确认</c:when>
								<c:when test="${customer.status==1}">已确认</c:when>
							</c:choose>
						</div> <a href="javascript:void(0)"
						onclick="leftMenuClick(this,'seller/customer/loadCustomerDetails?id=${customer.id}','sellers')"
						class="approval">申请明细</a><br> <a href="javascript:void(0)"
						onclick="showProof('${customer.proof}');"
						class="check_receipt orange">查看售后凭证</a></td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>