<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的付款审批</title>
<script src="<%=basePath%>/statics/platform/js/common.js"></script>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
<script type="text/javascript">
    $(function(){
        if ('${acceptPaymentInfo.paymentStatus}'<3){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "待内部审批"
                },{
                    title: "审批已通过"
                },{
                    title: "审批已驳回"
                }/*,{
                    title: "对方审批驳回"
                }*/]
            });
            debugger;
            var step = parseInt('${acceptPaymentInfo.paymentStatus}');
            $(".ystep1").setStep(step);
        }
    });

   //附件显示隐藏
    var hideOrShow = true;
    function showBillReceipt(receiptAddr) {
        $(".big_img").empty();
        $("#icon_list").empty();
        if(receiptAddr != null || receiptAddr != ''){
            var receiptAddrList = receiptAddr.split(',');
            var bigImg = '';
            for(i = 0;i<receiptAddrList.length;i++){
                var liObj = $("<li>");
                $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
                $("#icon_list").append(liObj);
                bigImg = receiptAddrList[0];
            }
            //大图
            $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
        }
        if(hideOrShow){
            $("#linkReceipt").show();
            hideOrShow = false;
        }else {
            $("#linkReceipt").hide();
            hideOrShow = true
        }
    }

    //跳转账单详情
	function openBillRecItem(reconciliationId) {
		var url = "platform/buyer/billPaymentDetail/openRecItemList?reconciliationId="+reconciliationId;
        //leftMenuClick(this,url,'buyer')
		window.open(url);
    }
</script>
</head>
<%--<div >--%>
	<div class="order_top mt">
		<div class="lf order_status" >
			<h4 class="mt">付款审批状态</h4>
			<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==1}">待内部审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==2}">待对方审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==3}">审批已通过</c:when>--%>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==4}">内部审批驳回</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==5}">对方审批驳回</c:when>--%>
			  <c:when test="${acceptPaymentInfo.paymentStatus == 0}">待审批</c:when>
			  <c:when test="${acceptPaymentInfo.paymentStatus == 1}">审批通过</c:when>
			  <c:when test="${acceptPaymentInfo.paymentStatus == 2}">审批驳回</c:when>
		  </c:choose>
        </span>
			</p>
			<p class="order_remarks text-center"></p>
		</div>
		<div class="lf order_progress">
			<div class="ystep1"></div>
		</div>
	</div>
	<div>
		<h4 class="mt">付款审批信息</h4>
		<div class="order_d">
			<p><span class="order_title">供应商信息</span></p>
			<table class="table_info">
				<tr>
					<td>供应商名称：<span>${acceptPaymentInfo.sellerCompanyName}</span></td>
				</tr>
			</table>
			<p class="line mt"></p>
			<p><span class="order_title">付款信息</span></p>
			<div style="width:auto;height:100px;overflow:auto">
			<table class="table_pure detailed_list">
				<thead>
				<tr>
					<td style="width:15%">账单编号</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">本次付款金额</td>
					<td style="width:10%">应付款总额</td>
					<td style="width:10%">已付款金额</td>
					<td style="width:10%">应付款余额</td>
					<td style="width:10%">付款类型</td>
					<td style="width:10%">审批状态</td>
					<td style="width:10%">备注</td>
					<%--<td style="width:10%">付款凭证</td>--%>
				</tr>
				</thead>
				<tbody>
				<%--<c:forEach var="acceptPaymentList" items="${acceptPaymentInfo}">--%>
					<tr>
						<td style="text-align: center">
							<a href="javascript:void(0);" onclick="openBillRecItem('${acceptPaymentInfo.reconciliationId}');" class="approval">${acceptPaymentInfo.reconciliationId}</a>
						</td>
						<td>${acceptPaymentInfo.createBillUserName}</td>
						<td>${acceptPaymentInfo.actualPaymentAmount}</td>
						<td>${acceptPaymentInfo.totalAmount}</td>
						<td>${acceptPaymentInfo.actualPaymentAmountSum}</td>
						<td>${acceptPaymentInfo.residualPaymentAmount}</td>
						<td  style="text-align: center">
							<c:choose>
								<c:when test="${acceptPaymentInfo.paymentType eq '0'}">现金</c:when>
								<c:when test="${acceptPaymentInfo.paymentType eq '1'}">银行转账</c:when>
								<c:when test="${acceptPaymentInfo.paymentType eq '2'}">微信</c:when>
								<c:when test="${acceptPaymentInfo.paymentType eq '3'}">支付宝</c:when>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${acceptPaymentInfo.paymentStatus eq '0'}">待内部审批</c:when>
								<c:when test="${acceptPaymentInfo.paymentStatus eq '1'}">内部已通过</c:when>
								<c:when test="${acceptPaymentInfo.paymentStatus eq '2'}">内部已驳回</c:when>
								<c:when test="${acceptPaymentInfo.paymentStatus eq '3'}">卖家已确认</c:when>
							</c:choose>
						</td>
						<td>${acceptPaymentInfo.paymentRemarks}</td>
						<%--<td>
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${acceptPaymentInfo.paymentAddr}');">查看票据</span>
						</td>--%>
					</tr>
				<%--</c:forEach>--%>
				</tbody>
			</table>
			</div>
			<!--收款凭据-->
			<div id="linkReceipt" class="receipt_content" style="width:500px;height:250px;display:none;">
				<div class="big_img">
					<%--<img id="bigImg">--%>
				</div>
				<div class="view">
					<a href="#" class="backward_disabled"></a>
					<a href="#" class="forward"></a>
					<ul id="icon_list"></ul>
				</div>
			</div>
		</div>
	</div>
<%--
</div>--%>
