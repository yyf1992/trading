<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../common/path.jsp"%>
<div>
	<div class="order_success">
		<img src="<%=basePath%>statics/platform/images/ok.png">
		<h3>恭喜您物料配置设置成功！</h3>
		<a href="javascript:void(0)" class="purchase" onclick="leftMenuClick(this,'platform/baseDate/buyproductskubom/loadBomListHtml','buyer');">转到物料清单表</a>
		<a href="javascript:void(0)" class="view_details">查看详情</a>
		<a href="javascript:void(0)" class="print" target="_blank">打印采购单</a>
	</div>
</div>