<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/path.jsp"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<!--同步OMSsku弹框-->
<form id="skuOMSForm">
	<div class="skuIncrease skuAdd">
		<div>
			<span>&emsp;商品货号：</span> <input type="text"placeholder="输入商品货号" name="productCode">
		</div>
		<div>
			<span>&emsp;规格代码：</span> <input type="text"placeholder="输入规格代码" name="skuCode">
		</div>
		<div>
			<span>&emsp;规格名称：</span> <input type="text" placeholder="输入规格代码" name="skuName">
		</div>
		<div>
			<span>&emsp;条 形 码：</span> <input type="text" placeholder="输入商品条形码" name="barcode" 
				onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
		</div>
		<input id="type" name="type" type="hidden" value="${type}" />
		<div class="materialRemark size_sm mp30">
		  <p class="rt">
	       	<img src="statics/platform/images/shuoming.png" class="shuoming">
	      |
	      	<span>说明：请填写同步商品条件，默认同步全部商品数据，
	      	数据较大请等候</span>
	      </p>
		</div>
	</div>
</form>