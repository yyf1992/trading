<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
function insert(){
	$.ajax({
		url:basePath+"platform/sysshop/loadShopInsert",
		type:"get",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				content:data,
				title:'新建店铺',
				skin:'layui-layer-rim',
				area:['400px','auto'],
				btn:['确定','取消'],
				yes:function(index,layerio){
					$.ajax({
						url:basePath+"platform/sysshop/saveInsertShop",
						type:"post",
						async:false,
						data:$("#addShopForm").serialize(),
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysshop/loadSysShopList','system','17100920494996594508');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,'platform/sysshop/loadSysShopList','system','17100920494996594508');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}

function editShop(id){
		$.ajax({
		url:basePath+"platform/sysshop/loadEditShop",
		type:"post",
		data:{"id":id},
		async:false,
		success:function(data){
			layer.open({
				type:1,
				content:data,
				title:'修改店铺',
				skin:'layui-layer-rim',
				area:['400px','auto'],
				btn:['确定','取消'],
				yes:function(index,layerio){
					$.ajax({
						url:basePath+"platform/sysshop/saveEditShop",
						type:"post",
						async:false,
						data:$("#editShopForm").serialize(),
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysshop/loadSysShopList?'+$("#shopForm").serialize(),'system','17100920494996594508');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,'platform/sysshop/loadSysShopList?'+$("#shopForm").serialize(),'system','17100920494996594508');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="content_modify">
      <div class="content_role">
        <form class="layui-form" action="platform/sysshop/loadSysShopList" id="shopForm">
        <ul class="order_search platformSearch">
          <li class="state">
            <label>店铺名称：</label>
            <input type="text" placeholder="输入店铺名称" name="shopName" value="${searchPageUtil.object.shopName}">
          </li>
          <li class="state">
            <label>加入日期：</label>
            <div class="layui-input-inline">
              <input type="text" name="createDate" value="${searchPageUtil.object.createDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
            </div>
          </li>
          <li class="state">
            <label>状态：</label>
            <div class="layui-input-inline">
              <select name="status" lay-filter="aihao">
					<option value="-1" <c:if test="${searchPageUtil.object.status=='-1'}">selected="selected"</c:if>>全部</option>
					<option value="0" <c:if test="${searchPageUtil.object.status==0}">selected="selected"</c:if>>正常</option>
					<option value="1" <c:if test="${searchPageUtil.object.status==1}">selected="selected"</c:if>>禁用</option>
              </select>
            </div>
          </li>
          <li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
	    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
        </ul>
        </form>
        <div class="mt">
			<a href="javascript:void(0)" button="新增"
				class="layui-btn layui-btn-add layui-btn-small rt"
				onclick="insert();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
        <table class="table_pure platformList">
          <thead>
          <tr>
            <td style="width:10%">编号</td>
            <td style="width:25%">店铺名称</td>
            <td style="width:15%">加入日期</td>
            <td style="width:15%">状态</td>
            <td style="width:15%">操作日期</td>
            <td style="width:20%">操作</td>
          </tr>
          </thead>
          <tbody>
          	<c:forEach var="SysShop" items="${searchPageUtil.page.list}" varStatus="status">
          	  <tr>
              	<td>${status.count}</td>
              	<td>${SysShop.shopName}</td>
              	<td><fmt:formatDate value="${SysShop.createDate}" type="both"/></td>
              	<td>
              		<c:choose>
              			<c:when test="${SysShop.isDel==0}">正常</c:when>
              			<c:otherwise>禁用</c:otherwise>
              		</c:choose>
              	</td>
              	<td>
              		<c:choose>
              			<c:when test="${SysShop.updateDate==null}"><fmt:formatDate value="${SysShop.createDate}" type="both"/></c:when>
              			<c:otherwise><fmt:formatDate value="${SysShop.updateDate}" type="both"/></c:otherwise>
              		</c:choose>
              	</td>
              	<td>
              		<a href="javascript:void(0)" button="修改"
						onclick="editShop('${SysShop.id}');"
						class="layui-btn layui-btn-update layui-btn-mini">
						<i class="layui-icon">&#xe7e9;</i>修改</a> 
              	</td>
              </tr>
          	</c:forEach>
          </tbody>
        </table>
		<div class="pager">${searchPageUtil.page}</div>
      </div>
</div>