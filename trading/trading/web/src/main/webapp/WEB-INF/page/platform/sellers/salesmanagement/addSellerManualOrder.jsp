<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>  
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>手工添加订单</title>
<script src="<%=basePath %>/statics/platform/js/sellerManualOrder.js"></script>
<script type="text/javascript">

</script>

</head>
<body>
<div class="addSellerManualOrder">
    <div class="show clear">
      <div class="progress_o">
	      <span class="cart">
	        <b></b>
	      	  我的购物车
	      </span>
	      <span class="order_pr current">
	        <b></b>
	                    填写核对订单信息
	      </span>
	      <span class="order_s">
	        <b></b>
	                   成功提交订单
	      </span>
      </div>
      <h4 class="page_title mt">客户：${manualOrderMap.buyers}</h4>
      <h4 class="no_logistics">
                     收货信息
        <span class="since"><input type="checkbox" name="is_since">
       	 不需要物流，自提货物
        </span>
      </h4>
      <div class="addr">
         <c:choose>
      		<c:when test="${addressList != null}">
	      		<c:forEach var="address" items="${addressList}" varStatus="status">
					<div class="addrItem <c:if test="${address.isDefault == 1}">isDefault selected</c:if>" id="addr_${status.count}">
			          <c:if test="${address.isDefault == 1}">
			          	<img src="statics/platform/images/default_small.jpg">
			          </c:if>
			          <div class="borderBg"></div>
			          <div class="addr_hd">
			            <span class="size_md"><b id="personName">${address.clientPerson}</b></span> 收
			          </div>
			          <div class="addr_bd">
			            <span id="shipAddress">${address.shipAddress}</span>
			            <span id="receiptPhone">${address.clientPhone}</span>
			          </div>
			          <div class="addr_md text-right">
			            <span class="addr_modify" onclick="updateAddress(this);"><i class="layui-icon">&#xe691;</i>修改</span>
			          </div>
			          <input type="hidden" name="addressId" id="addressId" value="${address.id}">
			        </div>
	      		</c:forEach>        
		     </c:when>	     	
		 </c:choose>
		 <div class="addrAdd text-center" id="addr_new" onclick="addAddress();" >
             <img src="statics/platform/images/addr_a.jpg">
             <p><b>新增收货地址</b></p>
         </div>
      </div>
      
      <p class="saleList"><b>销售清单</b>
        <a href="javascript:void(0)" onClick="backAddManualOrder();" class="layui-btn layui-btn-mini layui-btn-normal rt">
        	<i class="layui-icon">&#xe691;</i> 返回购物车修改
        </a>
      </p>
      <table class="table_pure detailed_list_s mt">
        <thead>
        <tr>
          <td style="width:20%">商品</td>
          <td style="width:5%">单位</td>
          <td style="width:13%">备注</td>
          <td style="width:10%">销售单价</td>
          <td style="width:8%">数量</td>
          <td style="width:10%">优惠金额</td>
          <td style="width:12%">商品金额</td>
        </tr>
        </thead>
        <tbody>
        	<c:forEach var="productInfor" items="${manualOrderMap.productInforList.itemArray}">  
		        <tr>
		          <td>${productInfor.proCode}|${productInfor.proName}|${productInfor.skuCode}|${productInfor.colorCode}|${productInfor.skuOid}</td>
		          <td>${productInfor.unitName}</td>
		          <td>${productInfor.remark}</td>
		          <td>${productInfor.price}</td>
		          <td>${productInfor.goodsNumber}</td>
		          <td>${productInfor.updatePrice}</td>
		          <td>${productInfor.priceSum}</td>
		        </tr>
           </c:forEach>
        </tbody>
      </table>
      <div class="totalOrder">
        <ul class="submit_order seller">
          <li>
            <label>商品总额:</label>
            <span id="">¥ ${manualOrderMap.purchasePrice }</span>
          </li>
          <li>
            <label>其他费用:</label>
            <span id="">¥ ${manualOrderMap.otherFee }</span>
          </li>
          <li>
            <label>订单总额:</label>
            <span class="orange" id="">¥ ${manualOrderMap.paymentPrice}</span>
          </li>
        </ul>
      </div>
      <div class="size_sm">
        <p class="ms">备注：</p>
        <textarea placeholder="请填写备注" class="orderRemark" id="supplierRemark"></textarea>
      </div>
      <div class="text-right">
        <a href="javascript:void(0)" onclick="saveManualOrder();"><button class="next_step">提交订单</button></a>
      </div>
   </div>
   
	<input type="hidden" value="${manualOrderMap.purchasePrice}" id="purchasePrice">
	<input type="hidden" value="${manualOrderMap.paymentPrice}" id="paymentPrice">
	<input type="hidden" value="${manualOrderMap.otherFee}" id="otherFee">
	<input type="hidden" value="${manualOrderMap.date}" id="date">
	<input type="hidden" value="${manualOrderMap.paymentType}" id="paymentType">
	<input type="hidden" value="${manualOrderMap.paymentPerson}" id="paymentPerson">
	<input type="hidden" value="${manualOrderMap.handler}" id="handler">
	<input type="hidden" value="${manualOrderMap.sellerId}" id="sellerId">
	<input type="hidden" value="${manualOrderMap.buyers}" id="buyers">
	<input type="hidden" value="${manualOrderMap.contact_person}" id="contact_person">
	<input type="hidden" value="${manualOrderMap.clientPhone}" id="client_Phone">
	<input type="hidden" value="${manualOrderMap.productInfor}" id="productInfor">
	<input type="hidden" value="${manualOrderMap.num}" id="num">
	<input type="hidden" value="${manualOrderMap.discount_sum}" id="discount_sum">
	<input type="hidden" value="${manualOrderMap.total}" id="total">
	<input type="hidden" value="${manualOrderMap.annexVoucher}" id="annexVoucher">
</div>
</body>
</html>