<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
$(function(){
	//搜索更多
	$(".search_more").click(function(){
	  $(this).toggleClass("clicked");
	  $(this).parent().nextAll("ul").toggle();
	});
	//设置选择的tab
	$(".tab>a").removeClass("hover");
	$(".tab #tab_${params.tabId}").addClass("hover");
	if("${params.tabId}"!='0'){
		$("#orderContent select[name='interest']").prop("disabled", true);
	}
	//日期
	loadDate("startDate","endDate");

	$("#orderContent #selectButton").click(function(e){
		e.preventDefault();
		selectData();
	});
	$(".tab a").click(function(){
		var id = $(this).attr("id");
		//清空查询条件
		$("#orderContent #resetButton").click();
		var index = id.split("_")[1];
		$("#orderContent input[name='tabId']").val(index);
		loadPlatformData();
	});
    //初始化数据
    selectData();
    //展开查询条件
    $(".search_more").click();
});
function selectData(){
	var formObj = $("#orderContent").find("form");
	$.ajax({
		url : basePath+"platform/buyer/buyOrder/loadData",
		data:formObj.serialize(),
		async:false,
		success:function(data){
            $("#tabContent").empty();
			var str = data.toString();
			$("#tabContent").html(str);
            $(".tab>a").removeClass("hover");
            $(".tab #tab_${params.tabId}").addClass("hover");
            loadVerify();
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//状态下拉改变
function setInterest(value) {
    $("#tabId").val(value);
}

/**
 * 导出
 */
function exportOrder() {
    var formData = $("#rawMaterialOrderListForm").serialize();
    var url = basePath+"platform/buyer/buyOrder/exportOrder?"+formData;
    window.open(url);
}

/**
 * 未到货订单导出
 */
function exportNotArrivalOrder() {
    var formData = $("#rawMaterialOrderListForm").serialize();
    var url = basePath+"platform/buyer/buyOrder/exportNotArrivalOrder?"+formData;
    window.open(url);
}
function update(obj,orderId,userId){
	if(loginUserId=='admin' || loginUserId==userId){
		leftMenuClick(obj,'platform/buyer/buyOrder/showUpdOrder?id='+orderId,'buyer','17112115053610102016')
	}else{
		layer.msg("不是自己下的单无权修改");
		return;
	}
}
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有订单</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待内部审批（<span>${params.internalNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_5">待收货（<span>${params.waitReceiveNum}</span>）</a><b>|</b>
	<a href="javascript:void(0)" id="tab_6">已收货（<span>${params.receivedNum}</span>）</a><b>|</b>
</div>
<div id="orderContent">
	<!--搜索栏-->
	<form class="" action="platform/buyer/buyOrder/rawMaterialOrderList" id="rawMaterialOrderListForm">
		<div class="search_top mt">
			<input type="text" placeholder="商品名称/货号/条形码/订单号进行搜索" name="selectValue" id="selectValue" value="${params.selectValue}">
			<button id="selectButton">搜索</button><span class='search_more sBuyer'></span>
		</div>
		<ul class="order_search">
			<li class="comm">
				<label>供应商名称:</label> 
				<input type="text" placeholder="输入供应商名称进行搜索" style="width:262px" name="supplierName" id="supplierName" value="${params.supplierName}">
			</li>
			<li class="range nomargin"><label>交易状态:</label>
				<div class="layui-input-inline">
					<select name="interest" id="interest" lay-filter="aihao" onchange="setInterest(this.value)">
						<option value="0" <c:if test="${params.interest=='0'}">selected</c:if>>全部</option>
						<option value="1" <c:if test="${params.interest=='1'}">selected</c:if>>待内部审批</option>
						<option value="5" <c:if test="${params.interest=='5'}">selected</c:if>>待收货</option>
						<option value="6" <c:if test="${params.interest=='6'}">selected</c:if>>已收货</option>
					</select>
				</div>
			</li>
			<li class="range nomargin">
				<label>下单人:</label>
				<input type="text" placeholder="" style="width:200px" name="createName" id="createName" value="${params.createName}">
			</li>
			<li class="range"><label>下单日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
				</div>-
				<div class="layui-input-inline">
					<input type="text" name="endDate" id="endDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
		</ul>
		<input type="hidden" name="tabId" id="tabId" value="${params.tabId}">
		<input type="hidden" name="orderKind" id="orderKind" value="1">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="exportOrder();">
		<i class="layui-icon">&#xe8bf;</i> 导出所有
	</a>
	<c:if test="${params.interest != '6' && params.interest != '8' && params.interest != '9' && params.interest != '10'}">
		<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" style="margin-right: 10px;" onclick="exportNotArrivalOrder();">
			<i class="layui-icon">&#xe8bf;</i> 导出未到货
		</a>
	</c:if>
	<div id="tabContent"></div>
</div>