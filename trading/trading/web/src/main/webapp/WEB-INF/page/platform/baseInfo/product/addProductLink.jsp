<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%--<%@ include file="../../common/common.jsp"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<head>
    <title>新增商品互通</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script type="text/javascript" src="<%=basePath%>/js/product/addProductLink.js"></script>
    <script src="<%=basePath%>/statics/platform/js/commodity.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/commodity_interwork.css">
    <script type="text/javascript">
        var linktype='${linktype}';
        function recovery(){
        	$("#searchTelno").val("");
        	$("#searchCompanyName").val("");
        	$("#searchLinkman").val("");
        }
    </script>
</head>
<div>
    <form id="addForm" name="addForm" action="">
        <!--按钮组-->
        <div class="btn_add">
            <c:if test="${linktype == '0'}">
                <a href="javascript:void(0);" class="btn_new"></a>
                <a href="javascript:void(0);" class="btn_sub" onclick="saveProductLink();"></a>
            </c:if>
        </div>
        <table class="table_blue  commodity_add" id="linkTab">
            <thead>
            <tr>
                <td style="width:10%">商品</td>
                <td style="width:10%">货号</td>
                <td style="width:10%">条形码</td>
                <td style="width:10%">规格代码</td>
                <td style="width:10%">规格名称</td>
                <td style="width:10%">单位</td>
                <td style="width:10%">采购价</td>
                <td style="width:10%">日产能</td>
                <td style="width:10%">到货天数</td>
                <td style="width:10%">
                    <c:choose>
                        <c:when test="${linktype==0}">关联供应商</c:when>
                        <c:otherwise>关联客户</c:otherwise>
                    </c:choose>
                </td>
                <%--<td style="width:15%">对方商品条形码</td>--%>
                <td style="width:10%">备注</td>
                <td style="width:10%">操作</td>
            </tr>
            </thead>
            <tbody id="linkBody">

            </tbody>
        </table>
    </form>
    <!--选择商品弹出框-->
    <div class="commodity_sel" id="addProductLinkSkuPop" style="display:none;"></div>
    <!--选择互通公司弹出-->
    <div class="supplier_sel" id="linkcompanyPop" style="display:none;">
        <p>
            <span>
                <c:choose>
                    <c:when test="${linktype==0}">供应商:</c:when>
                    <c:otherwise>&nbsp;客户:</c:otherwise>
                </c:choose>
            </span>
            <input type="text" class="mr" id="searchCompanyName" name="searchCompanyName" placeholder="请输入公司名称">
        </p>
        <p>
            <span>联系人:</span> <input type="text" id="searchLinkman" name="searchLinkman" placeholder="请输入联系人">     
        </p>
        <p>
            <span>手机号:</span>
            <input type="text" id="searchTelno" name="searchTelno" placeholder="请输入手机号码">
            <button type="button" class="search" onclick="recovery();">重置</button>
            &nbsp;
            <button onclick="findCompany();">查找</button>
           
        </p>
        <div class="supp_list normal_listO">
            <table class="table_blue">
                <thead>
                <tr>
                    <td style="width:10%">
             <!--       <span class="comm_sel">
                    <input type="checkbox">
                  </span> -->
                    </td>
                    <td style="width:40%">
                        <c:choose>
                            <c:when test="${linktype==0}">供应商</c:when>
                            <c:otherwise>客户</c:otherwise>
                        </c:choose>
                    </td>
                    <td style="width:20%">联系人</td>
                    <td style="width:30%">手机号</td>
                </tr>
                </thead>
                <tbody id="companyBody">

                </tbody>
            </table>
        </div>
    </div>
</div>
<!--侧边栏-->
<!-- <div class="aside"></div> -->


