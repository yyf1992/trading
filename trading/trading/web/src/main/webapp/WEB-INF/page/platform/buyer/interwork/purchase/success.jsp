<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../../common/path.jsp"%>
<script>
function toOrderList(){
	$.ajax({
		url : "platform/buyer/buyOrder/buyOrderList",
		async:false,
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div>
	<div class="order_success">
		<img src="<%=basePath%>statics/platform/images/ok.png">
		<h3>恭喜您已下单成功！</h3>
		<a href="javascript:void(0)" class="purchase" onclick="toOrderList();">转到向供应商采购的订单</a>
		<a href="javascript:void(0)" class="view_details">查看详情</a>
		<a href="javascript:void(0)" class="print" target="_blank">打印采购单</a>
	</div>
</div>