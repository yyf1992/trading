<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="${basePath}statics/platform/css/supplier.css">
<div id="composeDetailsDiv">
	<h4 class="currentTitle">商品明细</h4>
	<div class="padding-sm-lr currentContent">
		<div class="mp">
			<span class="currentLabel">当前状态：</span>
			<span class="currentState size_sm">
				<c:choose>
					<c:when test="${bom.status==1}">等待审核</c:when>
					<c:when test="${bom.status==2}">审核通过</c:when>
					<c:when test="${bom.status==3}">审核不通过</c:when>
				</c:choose>
			</span>
		</div>
		<ul class="currentDetail mp30 mt">
			<li><span>编&emsp;&emsp;号</span>： ${verifyDetail.id}</li>
			<li><span>标题</span>： ${verifyDetail.title}</li>
			<li><span>商品名称</span>： ${bom.productName}</li>
			<li><span>申 请 人</span>： ${verifyDetail.companyId}</li>
			<li><span>申请日期</span>： <fmt:formatDate value="${verifyDetail.createDate }" type="both"/></li>
		</ul>
		<div class="currentExplain">
			<span>说&emsp;&emsp;明：</span>
			<p>
				${bom.remark}
			</p>
		</div>
		<p class="line mp30"></p>
	</div>
	<h4 class="currentTitle">物料配置清单</h4>
	<div class="padding-sm-lr currentContent">
		<table class="table_pure mp30 ApprovalList" style="margin-bottom: 20px;">
			<thead>
				<tr>
					<td width="5%">序号</td>
			        <td width="10%">货号</td>
			        <td width="25%">商品名称</td>
			        <td width="10%">规格</td>
			        <td width="15%">条形码</td>
			        <td width="10%">单位</td>
			        <td width="15%">配置数量</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var='item' items="${materialList}" varStatus="status">
			      <tr>
			        <td>${status.count}</td>
			        <td>${item.product_code}</td>
			        <td>${item.product_name}</td>
			        <td>${item.sku_code}</td>
			        <td>${item.sku_barcode}</td>
			        <td>${item.unit_name}</td>
			        <td>${item.compose_num}</td>
			      </tr>
			  	</c:forEach>
			</tbody>
		</table>
	</div>
</div>
