<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%--<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>--%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单发票列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billinvoice/commodityBatchList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#deliverNo").val("${params.deliverNo}");
        $("#createBillName").val("${params.createBillName}");
        $("#productName").val("${params.productName}");
        $("#buyCompanyName").val("${params.buyCompanyName}");
        $("#orderId").val("${params.orderId}");
        $("#acceptInvoiceStatus").val("${params.acceptInvoiceStatus}");
        $("#startArrivalDate").val("${params.startArrivalDate}");
        $("#endArrivalDate").val("${params.endArrivalDate}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});
    //标签页改变
    function setStatus(obj,status) {
        $("#acceptInvoiceStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
    //重置查询条件
    function resetformData(){
        $("#deliverNo").val("");
        $("#createBillName").val("");
        $("#productName").val("");
        $("#buyCompanyName").val("");
        $("#orderId").val("");
        $("#acceptInvoiceStatus").val("0");
        $("#startArrivalDate").val("");
        $("#endArrivalDate").val("");
    }
	</script>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '0'}">class="hover"</c:if>>待开票据（<span>${params.noConfirmInvoiceCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '1'}">class="hover"</c:if>>待确认票据（<span>${params.approvalBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '2'}">class="hover"</c:if>>已确认票据（<span>${params.acceptBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '3'}">class="hover"</c:if>>已驳回票据（<span>${params.stopBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.acceptInvoiceStatus eq '4'}">class="hover"</c:if>>已取消票据（<span>${params.closeBillCycleCount}</span>）</a> <b>|</b>
</div>
<div>
	<!--搜索栏 -->
	<%--<form id="searchForm" class="layui-form" action="/platform/seller/billInvoice/billInvoiceList">--%>
	<form id="searchForm" class="layui-form" action="platform/seller/billInvoice/billInvoiceList">
		<ul class="order_search">
			<li class="comm">
				<label>发货单号:</label>
				<input type="text" placeholder="请输入发货单号" id="deliverNo" name="deliverNo" />
			</li>
			<li class="comm">
				<label>下单人:</label>
				<input type="text" placeholder="请输入发货单号" id="createBillName" name="createBillName" />
			</li>
			<li class="comm">
				<label>商品名称:</label>
				<input type="text" placeholder="请输入商品名称" id="productName" name="productName" />
			</li>
			<li class="comm">
				<label>采购商名称:</label>
				<input type="text" placeholder="请输入客户名称" id="buyCompanyName" name="buyCompanyName" />
			</li>
			<li class="comm">
				<label>采购订单号:</label>
				<input type="text" placeholder="请输入订单号" id="orderId" name="orderId" />
			</li>

			<li class="range nomargin">
				<label>票据类型:</label>
				<div class="layui-input-inline">
					<select name="acceptInvoiceStatus" id="acceptInvoiceStatus" lay-filter="aihao">
						<option value="0" >待开票据</option>
						<option value="1" >待确认票据</option>
						<option value="2" >已确认票据</option>
						<option value="3" >已驳回票据</option>
						<option value="4" >已取消票据</option>
					</select>
				</div>
			</li>
			<li class="range">
				<label>到货日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startArrivalDate" id="startArrivalDate" lay-verify="date" class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endArrivalDate" id="endArrivalDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="rang"><button type="reset" class="search" onclick="resetformData();">重置</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />
			<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="sellerCommodityExport();">
				<i class="layui-icon">&#xe8bf;</i> 导出
			</a>
		</ul>
	</form>
	<%--<div id="tabContent">--%>
	<!--列表区-->
	<table class="table_pure purchaseOrder" style="width:100%">
		<thead>
		<tr>
			<td style="width:3%"><span class="chk_click"><input  onclick="selAllClient();" type="checkbox"></span></td>
			<td style="width:12%">对账单号</td>
			<td style="width:12%">发货单号</td>
			<td style="width:6%">下单人</td>
			<td style="width:8%">采购商名称</td>
			<td style="width:8%">商品名称</td>
			<td style="width:3%">单位</td>
			<td style="width:8%">单价</td>
			<td style="width:3%">到货数量</td>
			<td style="width:8%">总金额</td>
			<td style="width:10%">采购订单号</td>
			<%--<td style="width:10%">状态</td>--%>
			<td style="width:9%">到货日期</td>
			<%--<td style="width:5%">状态</td>--%>
			<td style="width:5%">操作</td>
		</tr>
		</thead>
<tbody>
<c:forEach var="deliveryItemInfo" items="${searchPageUtil.page.list}">
	<%--<table class="table_pure invoice_list">--%>
		<tr>
			<%--<td><input type="checkbox" name="checkone" value="${deliveryItemInfo.deliveryRecItem.id}"></td>--%>
			<td><span class="chk_click"><input type="checkbox" name="checkone" value="${deliveryItemInfo.deliveryRecItem.id}"></span></td>
			<td title="${deliveryItemInfo.deliveryRecItem.reconciliationId}">
				<input type="hidden" name="reconciliationId" value="${deliveryItemInfo.deliveryRecItem.reconciliationId}">
				${deliveryItemInfo.deliveryRecItem.reconciliationId}</td>
			<td title="${deliveryItemInfo.deliveryRecItem.deliverNo}">${deliveryItemInfo.deliveryRecItem.deliverNo}</td>
			<td>${deliveryItemInfo.deliveryRecItem.createBillName}</td>
			<td>${deliveryItemInfo.deliveryRecItem.buyCompanyName}</td>
			<td><div>
					${deliveryItemInfo.deliveryRecItem.productName}<br>
				<%--<span>规格: ${deliveryItemInfo.deliveryRecItem.skuCode}</span><br>
				<span>规格名称: ${deliveryItemInfo.deliveryRecItem.skuName}</span>--%>
			</div>
			</td>
			<td>${deliveryItemInfo.deliveryRecItem.unitName}</td>
			<td title="${deliveryItemInfo.salePrice}">${deliveryItemInfo.salePrice}</td>
			<td title="${deliveryItemInfo.deliveryRecItem.arrivalNum}">${deliveryItemInfo.deliveryRecItem.arrivalNum}</td>
			<td title="${deliveryItemInfo.salePriceSum}">${deliveryItemInfo.salePriceSum}</td>
			<td title="${deliveryItemInfo.deliveryRecItem.orderCode}">${deliveryItemInfo.deliveryRecItem.orderCode}</td>

			<td>${deliveryItemInfo.deliveryRecItem.arrivalDateStr}</td>
			<%--<td>
			  <c:choose>
				<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 0}">待开票据</c:when>
				<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 1}">待确认票据</c:when>
				<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 2}">已确认票据</c:when>
				<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 3}">已驳回票据</c:when>
			  </c:choose>
			</td>--%>
			<td><a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/billInvoice/drawUpInvoice?deliveryItemIds=${deliveryItemInfo.deliveryRecItem.id}','sellers');" class="layui-btn layui-btn-small ">开票</a></td>
		</tr>
	<%--</table>--%>
</c:forEach>
</tbody>
</table><br>
	<!--合并发货-->
	<div class="text-center">
		<button class="layui-btn layui-btn-normal layui-btn-small" id="batchInvoice">合并开票</button>
	</div>
	<!--分页-->
	<div class="pager">${searchPageUtil.page }</div>
<%--</div>--%>
</div>