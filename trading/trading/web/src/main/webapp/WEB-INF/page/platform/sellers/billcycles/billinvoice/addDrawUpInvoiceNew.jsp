<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billreconciliation/reconciliationDetail.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<%--<script type="text/javascript">
	</script>--%>
</head>

<div >
	<div id="print_yc">
	<div class="page_title">
		<table class="table_n">
			<tr>
				<td>采购商：<span>${buyBillReconciliation.buyCompanyName}</span></td>
				<td>出账周期：<span>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</span></td>
				<td>状态：
					<span>
				  <c:choose>
					  <c:when test="${buyBillReconciliation.billDealStatus==1}">等待对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==2}">待对方对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==3}">已完成对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==4}">内部审批已驳回</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==5}">对方审批已驳回</c:when>
				  </c:choose>
				</span></td>
			</tr>
		</table>
	</div>

	<div class="mp mt size_sm">
	<c:set var="priceSum" value="0" scope="page"></c:set>
	<c:forEach var="daohuoItem" items="${daohuoMap}">
		<div>
			<%--<div class="layui-row page_title" style="overflow: hidden;">
				<div class="layui-col-sm2">
					<span class="c99">运单号：</span>${daohuoItem.value.waybillNo}
				</div>
				<span class="layui-col-sm2"><span class="c99">物流名称：</span>${daohuoItem.value.logisticsCompany}</span>
				<span class="layui-col-sm2"><span class="c99">司机名称：</span>${daohuoItem.value.driverName}</span>
				<span class="layui-col-sm2"><span class="c99">运费：</span>${daohuoItem.value.freight}元</span>
				<span class="layui-col-sm2"><span class="c99">订单类型：</span>采购</span>

			</div>--%>
			<div class="page_title">
			<table class="table_n">
				<tr>
				  <td style="width: 25%"><span ><span class="c99">运单号：</span></span>${daohuoItem.value.waybillNo}</td>
				  <td style="width: 25%"><span ><span class="c99">物流名称：</span>${daohuoItem.value.logisticsCompany}</span></td>
				  <td style="width: 25%"><span ><span class="c99">司机名称：</span>${daohuoItem.value.driverName}</span></td>
				  <td style="width: 25%"><span ><span class="c99">运费：</span>${daohuoItem.value.freight}元</span></td>
				  <td style="width: 25%"><span ><span class="c99">订单类型：</span>采购</span></td>
				</tr>

			</table>
			</div>
			<table class="layui-table text-center" lay-even="" lay-size="sm"
				style="margin: 0;">
				<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:10%">条形码</td>
						<td style="width:25%">采购单号</td>
						<td style="width:30%">发货单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">到货总数量</td>
						<td style="width: 10%">是否开票</td>
						<td style="width:12%">采购单价</td>
						<td style="width:12%">对账单价</td>
						<td style="width:15%">总金额</td>
						<td style="width:15%">操作</td>
					</tr>
				</thead>
				<tbody>
				<c:set var="arrivalNumSum" value="0" scope="page"></c:set>
				<c:set var="itemPriceSum" value="0" scope="page"></c:set>
				<c:forEach var="goodsItem" items="${daohuoItem.value.itemList}">
					<tr>
						<td>${goodsItem.productCode }</td>
						<td>${goodsItem.productName }</td>
						<td>${goodsItem.skuCode }</td>
						<td>${goodsItem.skuName }</td>
						<td>${goodsItem.barcode }</td>
						<td>${goodsItem.orderCode }</td>
						<td>${goodsItem.deliverNo }</td>
						<td><fmt:formatDate value="${goodsItem.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
						<td>${goodsItem.arrivalNum }</td>
						<td>
							<c:choose>
								<c:when test="${goodsItem.isNeedInvoice == 'Y'}">是</c:when>
								<c:when test="${goodsItem.isNeedInvoice != 'Y'}">否</c:when>
							</c:choose>
						</td>
						<td>${goodsItem.salePrice }</td>
						<td>${goodsItem.updateSalePrice }</td>
						<td>${goodsItem.updateSalePrice*goodsItem.arrivalNum }</td>
						<td  >
							<div class="oprationRe">
								<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${daohuoItem.value.attachmentAddrStr}');">查看收据</span>
							</div>
						</td>
					</tr>
					<c:set var="arrivalNumSum" value="${arrivalNumSum+goodsItem.arrivalNum }" scope="page"></c:set>
					<c:set var="itemPriceSum" value="${itemPriceSum+goodsItem.updateSalePrice*goodsItem.arrivalNum }" scope="page"></c:set>
				</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>${arrivalNumSum }</td>
						<td></td>
						<td>-</td>
						<td>-</td>
						<td>${itemPriceSum }</td>
						<td>总金额+运费:${itemPriceSum+daohuoItem.value.freight }</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</div>
		<c:set var="priceSum" value="${priceSum+itemPriceSum+daohuoItem.value.freight }" scope="page"></c:set>
	</c:forEach>
<!--换货数据-->
		<c:forEach var="replaceItem" items="${replaceMap}">
			<div>
			<div class="page_title">
				<table class="table_n">
					<tr>
						<td style="width: 25%"><span ><span class="c99">运单号：</span></span>${replaceItem.value.waybillNo}</td>
						<td style="width: 25%"><span ><span class="c99">物流名称：</span>${replaceItem.value.logisticsCompany}</span></td>
						<td style="width: 25%"><span ><span class="c99">司机名称：</span>${replaceItem.value.driverName}</span></td>
						<td style="width: 25%"><span ><span class="c99">运费：</span>${replaceItem.value.freight}元</span></td>
						<td style="width: 25%"><span ><span class="c99">订单类型：</span>采购</span></td>
					</tr>

				</table>
			</div>
				<table class="layui-table text-center" lay-even="" lay-size="sm"
					   style="margin: 0;">
					<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:15%">条形码</td>
						<td style="width:25%">采购单号</td>
						<td style="width:30%">发货单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">到货总数量</td>
						<td style="width: 10%">是否开票</td>
						<td style="width:12%">采购单价</td>
						<%--<td>对账单价</td>--%>
						<td style="width:10%">总金额</td>
						<td style="width:10%">操作</td>
					</tr>
					</thead>
					<tbody>
					<c:set var="arrivalNumSum" value="0" scope="page"></c:set>
					<c:set var="itemPriceSum" value="0" scope="page"></c:set>
					<c:forEach var="replaceItemInfo" items="${replaceItem.value.itemList}">
						<tr>
							<td>${replaceItemInfo.productCode }</td>
							<td>${replaceItemInfo.productName }</td>
							<td>${replaceItemInfo.skuCode }</td>
							<td>${replaceItemInfo.skuName }</td>
							<td>${replaceItemInfo.barcode }</td>
							<td>${replaceItemInfo.orderCode }</td>
							<td>${replaceItemInfo.deliverNo }</td>
							<td><fmt:formatDate value="${replaceItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
							<td>${replaceItemInfo.arrivalNum }</td>
							<td>
								<c:choose>
									<c:when test="${replaceItemInfo.isNeedInvoice == 'Y'}">是</c:when>
									<c:when test="${replaceItemInfo.isNeedInvoice != 'Y'}">否</c:when>
								</c:choose>
							</td>
							<td>${replaceItemInfo.salePrice }</td>
							<%--<td>${replaceItemInfo.updateSalePrice }</td>--%>
							<td>${replaceItemInfo.updateSalePrice*replaceItemInfo.arrivalNum }</td>
							<td  >
								<div class="oprationRe1">
									<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${replaceItem.value.attachmentAddrStr}');">查看收据</span>
								</div>
							</td>
						</tr>
						<c:set var="arrivalNumSum" value="${arrivalNumSum+replaceItemInfo.arrivalNum }" scope="page"></c:set>
						<c:set var="itemPriceSum" value="${itemPriceSum+replaceItemInfo.updateSalePrice*replaceItemInfo.arrivalNum }" scope="page"></c:set>
					</c:forEach>
					</tbody>
					<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>${arrivalNumSum }</td>
						<td></td>
						<td>-</td>
						<td>-</td>
						<td>${itemPriceSum }</td>
						<td>总金额+运费:${itemPriceSum+replaceItem.value.freight }</td>
						<td></td>
					</tr>
					</tfoot>
				</table>
			</div>
			<c:set var="priceSum" value="${priceSum+itemPriceSum+replaceItem.value.freight }" scope="page"></c:set>
		</c:forEach>
<!--退货数据-->
		<c:forEach var="customerItem" items="${customerMap}">
			<div>
				<%--<div class="layui-row page_title" style="overflow: hidden;">
					<div class="layui-col-sm2">
						<span class="c99">运单号：</span>
					</div>
					<span class="layui-col-sm2"><span class="c99">物流名称：</span></span>
					<span class="layui-col-sm2"><span class="c99">司机名称：</span></span>
					<span class="layui-col-sm2"><span class="c99">运费：</span>元</span>
					<span class="layui-col-sm2"><span class="c99">订单类型：</span>退货</span>
				</div>--%>
			<div class="page_title">
				<table class="table_n">
					<tr>
						<td style="width: 25%"><span ><span class="c99">运单号：</span></span>${customerItem.value.waybillNo}</td>
						<td style="width: 25%"><span ><span class="c99">物流名称：</span>${customerItem.value.logisticsCompany}</span></td>
						<td style="width: 25%"><span ><span class="c99">司机名称：</span>${customerItem.value.driverName}</span></td>
						<td style="width: 25%"><span ><span class="c99">运费：</span>${customerItem.value.freight}元</span></td>
						<td style="width: 25%"><span ><span class="c99">订单类型：</span>退货</span></td>
					</tr>

				</table>
			</div>
				<table class="layui-table text-center" lay-even="" lay-size="sm"
					   style="margin: 0;">
					<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:10%">条形码</td>
						<%--<td width="150px">采购单号</td>--%>
						<td style="width:25%">发货单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">到货总数量</td>
						<td style="width: 10%"></td>
						<td style="width:15%">采购单价</td>
						<td style="width:15%">对账单价</td>
						<td style="width:10%">总金额</td>
						<td style="width:10%"></td>
					</tr>
					</thead>
					<tbody>
					<c:set var="arrivalNumSum" value="0" scope="page"></c:set>
					<c:set var="itemPriceSum" value="0" scope="page"></c:set>
					<c:forEach var="customerItemInfo" items="${customerItem.value.itemList}">
						<tr>
							<td>${customerItemInfo.productCode }</td>
							<td>${customerItemInfo.productName }</td>
							<td>${customerItemInfo.skuCode }</td>
							<td>${customerItemInfo.skuName }</td>
							<td>${customerItemInfo.barcode }</td>
							<%--<td>${goodsItem.orderCode }</td>--%>
							<td>${customerItemInfo.deliverNo }</td>
							<td><fmt:formatDate value="${customerItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
							<td>${customerItemInfo.arrivalNum }</td>
							<td>
								<c:choose>
									<c:when test="${customerItemInfo.isNeedInvoice == 'Y'}">是</c:when>
									<c:when test="${customerItemInfo.isNeedInvoice != 'Y'}">否</c:when>
								</c:choose>
							</td>
							<td>${customerItemInfo.salePrice }</td>
							<td>${customerItemInfo.updateSalePrice }</td>
							<td>${customerItemInfo.updateSalePrice*customerItemInfo.arrivalNum }</td>
							<td >
								<div class="oprationRe2">
									<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItem.value.attachmentAddrStr}');">查看收据</span>
								</div>
							</td>
						</tr>
						<c:set var="arrivalNumSum" value="${arrivalNumSum+customerItemInfo.arrivalNum }" scope="page"></c:set>
						<c:set var="itemPriceSum" value="${itemPriceSum+customerItemInfo.updateSalePrice*customerItemInfo.arrivalNum }" scope="page"></c:set>
					</c:forEach>
					</tbody>
					<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<%--<td></td>--%>
						<td></td>
						<td></td>
						<td>${arrivalNumSum }</td>
						<td></td>
						<td>-</td>
						<td>-</td>
						<td>${itemPriceSum }</td>
						<td>总金额:${itemPriceSum }</td>
						<td></td>
					</tr>
					</tfoot>
				</table>
			</div>
			<c:set var="priceSum" value="${priceSum-itemPriceSum}" scope="page"></c:set>
		</c:forEach>
	</div>
	<fieldset class="layui-elem-field layui-field-title">
		<legend>
			<b>总金额：<span class="red">${priceSum }元</span>
			</b>
		</legend>
	</fieldset>
</div>
	<div class="print_b">
		<span id="print_sure">打印账单</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/seller/billPaymentDetail/billPaymentDetailList','sellers');">返回</span>
	</div>
</div>

<!--商品价格变更审批-->
<div class="plain_frame price_change" id="updPriceDiv" style="display: none;">
	<ul>
		<li>
			<span>商品价格:</span>
			<input type="number" id="updPrice" name="updPrice"  min="0" >
		</li>
	</ul>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<img id="bigImg">
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $(".oprationRe").hide();
        $(".oprationRe1").hide();
        $(".oprationRe2").hide();
        $(".oprationRe3").hide();
        $(".oprationRe4").hide();
        $(".oprationRe").hide();
        $("#print_yc").jqprint();
        $(".oprationRe").show();
        $(".oprationRe1").show();
        $(".oprationRe2").show();
        $(".oprationRe3").show();
        $(".oprationRe4").show();
        $(".oprationRe").show();
    });
</script>