<%@ page language="java" import="java.util.*,com.nuotai.trading.utils.ShiroUtils" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script>
var basePath = "<%=basePath%>";
</script>
<!-- css -->
<link rel="stylesheet" href="<%=basePath%>statics/dingding/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=basePath%>statics/dingding/css/nuotai.css" />
<link rel="stylesheet" href="<%=basePath%>statics/plugins/layui/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/common.css" />
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/ystep/ystep.css">
<link rel="stylesheet" href="<%=basePath%>statics/css/nuotai.css" />

<!-- js -->
<script src="<%=basePath%>statics/platform/js/j.js"></script>
<script src="<%=basePath%>statics/platform/js/html2canvas.js"></script>
<script src="<%=basePath%>statics/platform/js/colResizable-1.6.min.js"></script>
<script src="<%=basePath%>statics/plugins/layer/layer.js"></script>
<script src="<%=basePath%>statics/plugins/layui/layui.js"></script>
<script src="<%=basePath%>statics/dingding/js/base64.js"></script>
<script src="<%=basePath%>statics/platform/js/ystep.js"></script>
