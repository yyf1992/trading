<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<%=basePath%>statics/platform/js/setPrice.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
setNum();
//添加一项单位
  $('.unit_raise').click(function(){
    var str="<td></td><td><input type='text' placeholder='输入单位名称'></td><td><span class='unit_delete'><b></b> 删除</span></td>";
    var tbl=$('.unit_input tr:eq(-1)');
    addRow(str,tbl);
    setNum();
  });
  //删除一项单位
  $('.unit_input').on('click','.unit_delete',function(){
   	var trObj = $(this).parent().parent("tr");
   	if(trObj.find("input[name='unitId']").length>0){
	    layer.confirm('<p class="orange size_sm text-center"><img src="<%=basePath%>statics/platform/images/icon.jpg">&emsp;确定删除该商品单位吗？</p>', {
	      title:'提示',
	      skin:'pop btnCenter pop25 delete',
	      closeBtn:0,
	      area:['310px','auto'],
	      btn: ['确定','取消']
	    }, function(){
	    		var id=trObj.find("input[name='unitId']").val();
			    $.ajax({
			    	url:"platform/product/delUnit",
			    	data:{"id":id},
			    	async:false,
					type:"post",
			    	success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.msg(resultObj.msg,{icon:1,time:50});
								trObj.remove();
								setNum();
							}else{
								layer.msg(resultObj.msg,{icon:2});
							}
						
			    	
			    	},error:function(){
			    	 layer.msg("获取数据失败，请稍后重试！",{icon:2});
			    	}
			    });
	    }.bind(this));
   	}else{
   		trObj.remove();
		setNum();
   	}
  });
});
function setNum(){
	$(".unit_list>table>tbody>tr").each(function(i){
		$(this).find("td:first").html(i+1);
	});
}

</script>
<!--新增单位弹出框-->
<div class="unit">
  <div class="unit_list mt" style="height: auto;">
    <table class="table_blue unit_input">
      <thead>
        <tr>
          <td style="width:15%">编号</td>
          <td style="width:55%">单位名称</td>
          <td style="width:30%">操作</td>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="BuyUnit" items="${unitList}" varStatus="status">
	              <tr>
	              <td></td>
	              <td><input type="text" id="${BuyUnit.id}" value="${BuyUnit.unitName}" readonly="readonly"></td>
	              <td>
	              	<input type="hidden" name="unitId" value="${BuyUnit.id}">
            		<span class="unit_delete"><b></b> 删除</span>
          		  </td>
	              </tr>
	    </c:forEach>
      </tbody>
    </table>
  </div>
  <button class="layui-btn layui-btn-normal layui-btn-mini unit_raise"><i class="layui-icon">&#xe608;</i> 增加项</button>
</div>