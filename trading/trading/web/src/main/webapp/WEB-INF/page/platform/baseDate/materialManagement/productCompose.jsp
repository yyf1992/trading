<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<script src="<%=basePath%>statics/platform/js/common.js" type="text/javascript"></script>
<script src="<%=basePath%>statics/platform/js/commodity.js" type="text/javascript"></script>
<script>
$(function(){
	if(${changeCompose != null }){
		$(".layui-form input.product").attr("disabled","disabled");
		$(".layui-form").find("ul").addClass("checked");
	}
}); 
</script>
<div class="content">
  <div>
    <h4 class="page_title">成品商品</h4>

   	<form class="layui-form">
		<%-- <div class="materialName mp30 size_sm">
			<span class="c66">商品名称：</span>
			<div class="layui-input-inline">
				<input type="text" id="productName" readonly="readonly" class="product" value="${bom.productName}">
			</div>
		</div> --%>
   		<input type="hidden" id="id" value="${bom.productId}" class="${bom.id}">
   		<input type="hidden" id="materialId">
		<ul class="materialDetail mp30">
		    <li><span>商品名称：</span><input type="text" id="productName" readonly="readonly" class="product" value="${bom.productName}"></li>
			<li><span>货&emsp;&emsp;号：</span><input name="productCode" id="productCode" readonly="readonly" class="product" value="${bom.productCode}"></li>
			<li><span>条 形 码：</span><input name="barcode" id="barcode" readonly="readonly" class="product" value="${bom.barcode}"></li>
			<li><span>规格代码：</span><input name="skuCode" id="skuCode" readonly="readonly" class="product" value="${bom.skuCode}"></li>
			<li><span>规格名称：</span><input name="skuName" id="skuName" readonly="readonly" class="product" value="${bom.skuName}"></li>
			<li><span>单&emsp;&emsp;位：</span><input name="unitName" id="unitName" readonly="readonly" class="product" value="${bom.unitName}"></li>
		</ul>
	</form>
    <h4 class="page_title">包含的物料商品</h4>
    <div class="materialRemark size_sm mp30">
      <span class="materialNew">选择商品</span>
      <p class="rt">
        <img src="statics/platform/images/shuoming.png" class="shuoming">
      |
      <span>说明：成品商品每计量单位规格情况下，配置的原材料每规格数量。例如1件成品商品对应原材料a ,5 件，原材料b ,3 套。</span>
      </p>
    </div>
    <table class="table_pure materialList mp30" id="skuTable">
      <thead>
      <tr>
        <td style="width:12%">货号</td>
        <td style="width:14%">商品名称</td>
        <td style="width:14%">规格代码</td>
        <td style="width:14%">规格名称</td>
        <td style="width:14%">条形码</td>
        <td style="width:5%">单位</td>
        <td style="width:7%">配置数量</td>
        <td style="width:7%">操作</td>
      </tr>
      <tr>
        <td colspan="8"></td>
      </tr>
      </thead>
      <tbody>
     	<c:forEach var="item" items="${materialList}">
     		<tr>
     		  <td>${item.productCode }</td>
              <td>${item.productName }</td>
              <td>${item.skuCode }</td>
              <td>${item.skuName }</td>
              <td>${item.skuBarcode }</td>
              <td>${item.unitName }</td>
              <td><input type="number" placeholder="0" value="${item.composeNum}" style="width: 50px;"></td>
              <td><span class="table_del"><b></b>删除</span></td>
     		</tr>
     	</c:forEach>
      </tbody>
    </table>
    <div class="size_sm mp30">
      <p class="mt">备注：</p>
      <textarea placeholder="请输入说明内容" class="orderRemark" id="remark">${bom.remark}</textarea>
    </div>
    <div class="text-right">
      <a href="javascript:void(0)" onclick="submit(this);" class="next_step">提交</a>
    </div>
  </div>
  <!--选择成品弹出框-->
  <div class="commodity_sel" id="addProduct" style="display:none;"></div>
  <!--选择原材料弹出框-->
  <div class="commodity_sel" id="addRawMaterial" style="display:none;"></div>
</div>