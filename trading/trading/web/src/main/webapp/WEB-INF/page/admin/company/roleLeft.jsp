<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
//查询
$("#leftUserButton").click(function(e){
	e.preventDefault();
	$("#leftRoleDiv #pageNo").val(1);
	loadAdminData("leftRoleDiv");
});
//全选
$("#leftRoleDiv input:checkbox[name='title-table-checkbox']").change(function(){
	var status = $(this).is(":checked");
	if(status){
		$("#leftRoleDiv input:checkbox[name='checkone']").prop("checked",status);
	}else{
		$("#leftRoleDiv input:checkbox[name='checkone']").prop("checked",status);
	}
});
</script>
<div class="row">
	<form class="form-inline "
		action="admin/sysRoleCompany/loadLeftRoleList.html">
		<input name="companyId" type="hidden" value="${searchPageUtilLeft.object.companyId}" />
		<!-- 分页隐藏数据 -->
		<input id="pageNo" name="pageAdmin.pageNo" type="hidden"
			value="${searchPageUtilLeft.pageAdmin.pageNo}" />
		<input id="pageSize"
			name="pageAdmin.pageSize" type="hidden"
			value="${searchPageUtilLeft.pageAdmin.pageSize}" />
		<input id="divId" name="pageAdmin.divId" type="hidden"
			value="${searchPageUtilLeft.pageAdmin.divId}" />
	</form>
</div>
<div class="widget-box margin-top-sm">
	<div class="widget-title title-xs">
		<span class="icon bordernone"><i class="icon-list"></i> </span>
		<h5>查询结果</h5>
	</div>
	<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
		role="grid">
		<table class="table table-bordered table-striped with-check">
			<thead>
				<tr>
					<th>
						<input type="checkbox" id="title-table-checkbox"
							name="title-table-checkbox" />
					</th>
					<th>角色代码</th>
					<th>角色名</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="sysUserLeft" items="${searchPageUtilLeft.pageAdmin.list}">
					<tr class="text-center">
						<td><input type="checkbox" name="checkone"
							value="${sysUserLeft.id}" />
						</td>
						<td>${sysUserLeft.roleCode}</td>
						<td>${sysUserLeft.roleName}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">${searchPageUtilLeft.pageAdmin }</div>
	</div>
</div>