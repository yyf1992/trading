<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
$(".content_role .search").click(function(e){
	e.preventDefault();
	var formObj = $(this).parents("form");
	var formData = formObj.serialize();
	$.ajax({
		url:"platform/sysVerify/setReferralUser",
		data:formData,
		success:function(data){
			$(".layui-layer-content").html(data);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
});
</script>
<div class="content_role" id="userDiv">
	<form action="platform/sysVerify/setReferralUser">
		<ul class="order_search personSearch">
			<li class="range">
				<label>员工名称：</label> 
				<input type="text" placeholder="输入员工姓名" name="userName" value="${searchPageUtil.object.userName}">
			</li>
			<li class="nomargin">
				<button class="search">搜索</button>
			</li>
		</ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
	</form>
	<table class="table_person personList">
		<thead>
			<tr>
				<td style="width:10%">选择</td>
				<td style="width:15%">员工姓名</td>
				<td style="width:20%">登录账户</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="sysUser" items="${searchPageUtil.page.list}" varStatus="status">
				<tr>
					<td><input type="radio" name="userId" value="${sysUser.id}"></td>
					<td>
						<input type="hidden" name="userName" value="${sysUser.user_name}">
						${sysUser.user_name}
					</td>
					<td>${sysUser.login_name}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
</div>
