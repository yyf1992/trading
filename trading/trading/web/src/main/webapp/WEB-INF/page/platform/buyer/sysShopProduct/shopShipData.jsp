<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/setPrice.css"></link>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script>
<script src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<div>
	<div class="searchForm">
		<form action="buyer/sysshopdeliverydata/shopShip">
			<ul class="search">
				<li>
					<!-- 判断店铺是否选中 -->
					<c:choose>
						<c:when test="${shopProductMap!=null}">
							<input type="checkbox" value="1" name="shop" checked="checked"><span>&nbsp;店铺</span>
						</c:when>
						<c:when test="${shopMap!=null}">
							<input type="checkbox" value="1" name="shop" checked="checked"><span>&nbsp;店铺</span>
						</c:when>
						<c:otherwise>
							<input type="checkbox" value="1" name="shop"><span>&nbsp;店铺</span>
						</c:otherwise>
					</c:choose>
				</li>
				<li>
					<!-- 判断货号是否选中 -->
					<c:choose>
						<c:when test="${shopProductMap!=null}">
							<input type="checkbox" value="1" name="productCode" checked="checked"><span>&nbsp;货号</span>
						</c:when>
						<c:when test="${productMap!=null}">
							<input type="checkbox" value="1" name="productCode" checked="checked"><span>&nbsp;货号</span>
						</c:when>
						<c:otherwise>
							<input type="checkbox" value="1" name="productCode"><span>&nbsp;货号</span>
						</c:otherwise>
					</c:choose>
				</li>
				<li>
					<!-- 店铺 -->
					<span class="inputBox">店铺代码:</span>
					<div class="layui-input-inline">
					   <select name="shopcode" lay-filter="aihao">
							<option value="">全部店铺</option>
							<c:forEach var="shop" items="${shopList}">
								<option value="${shop.shopCode}" <c:if test="${params.shopcode==shop.shopCode}">selected="selected"</c:if>>${shop.shopName}</option>
							</c:forEach>
		              </select>
					</div>
				</li>
				<li>
					<!-- 货号 -->
					<span class="inputBox">货号:</span>
					<div class="layui-input-inline">
					  <input type="text" name="procode"  value="${params.procode}" placeholder="货号" class="layui-input" >
					</div>
				</li>
				<li>
					<!-- 条形码 -->
					<span class="inputBox">条形码:</span>
					<div class="layui-input-inline">
					  <input type="text" name="skuoid"  value="${params.skuoid}" placeholder="条形码" class="layui-input" >
					</div>
				</li>
				<li>
					<label>起始时间</label>:
					<div class="layui-input-inline">
					  <input type="text" name="startDate"  value="${params.startDate}" lay-verify="date" placeholder="请选择起始日期" autocomplete="off" class="layui-input" >
					</div>
					-
					<div class="layui-input-inline">
					  <input type="text" name="endDate"  value="${params.endDate}" lay-verify="date" placeholder="请选择截止日期" autocomplete="off" class="layui-input" >
					</div>
				</li>
				<li class="seachStep">
					<label>查询维度:</label>
					<input type="radio" value="syear" name="latitude" <c:if test="${params.latitude=='syear'}">checked="checked"</c:if>><span> 年</span>
					<input type="radio" value="smonth" name="latitude" <c:if test="${params.latitude=='smonth'}">checked="checked"</c:if>><span> 月</span>
					<input type="radio" value="sday" name="latitude" <c:if test="${params.latitude=='sday'}">checked="checked"</c:if>><span> 日</span>
				</li>
			</ul>
			
			<ul  class="searchBt">
				<li>
					<button class="btn" onclick="loadPlatformData();">搜索</button>
					<button id="shopShipExportButton" class="btn" onclick="exportData('tableShip','店铺发货数据统计表');">数据导出</button>
				</li>
			</ul>
		</form>
	</div>
	<!-- 发货数据列表 -->
	<table	id="tableShip" class="table_pure shipShopList" >
		<thead>
			<tr>
	        <c:choose>
	        	<c:when test="${shopSalesList!=null && fn:length(shopSalesList) > 0}">
				  	<td style="width:6.25%">序号</td>
		            <td style="width:6.25%">出货数量</td>
		            <td style="width:6.25%">出货金额</td>
		            <td style="width:6.25%">出货成本</td>
		            <td style="width:6.25%">出货毛利</td>
		            <td style="width:6.25%">退货数量</td>
		            <td style="width:6.25%">退货金额</td>
		            <td style="width:6.25%">退货成本</td>
		            <td style="width:6.25%">出货单价</td>
		            <td style="width:6.25%">成本单价</td>
		            <c:choose>
					<c:when test="${params.latitude == 'syear'}">
						<td style="width:6.25%">年</td>
					</c:when>
					<c:when test="${params.latitude == 'smonth'}">
						<td style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
					</c:when>
					<c:when test="${params.latitude == 'sday'}">
						<td style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
						<td style="width:6.25%">日</td>
					</c:when>
				</c:choose>
				</c:when>
				<c:when test="${shopProductMap!=null  && fn:length(shopProductMap) > 0}">
				  	<td style="width:6.25%">序号</td>
		            <td style="width:6.25%">店铺</td>
		            <td style="width:6.25%">货号</td>
		            <td style="width:6.25%">出货数量</td>
		            <td style="width:6.25%">出货金额</td>
		            <td style="width:6.25%">出货成本</td>
		            <td style="width:6.25%">出货毛利</td>
		            <td style="width:6.25%">退货数量</td>
		            <td style="width:6.25%">退货金额</td>
		            <td style="width:6.25%">退货成本</td>
		            <c:choose>
					<c:when test="${params.latitude == 'syear'}">
						<td  style="width:6.25%">年</td>
					</c:when>
					<c:when test="${params.latitude == 'smonth'}">
						<td  style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
					</c:when>
					<c:when test="${params.latitude == 'sday'}">
						<td   style="width:6.25%">年</td>
						<td   style="width:6.25%">月</td>
						<td   style="width:6.25%">日</td>
					</c:when>
				</c:choose>
				</c:when>
				<c:when test="${shopMap!=null && fn:length(shopMap) > 0}">
					<td style="width:6.25%">序号</td>
            		<td style="width:6.25%">店铺</td>
					<td style="width:6.25%">出货数量</td>
		            <td style="width:6.25%">出货金额</td>
		            <td style="width:6.25%">出货成本</td>
		            <td style="width:6.25%">出货毛利</td>
		            <td style="width:6.25%">退货数量</td>
		            <td style="width:6.25%">退货金额</td>
		            <td style="width:6.25%">退货成本</td>
		            <c:choose>
					<c:when test="${params.latitude == 'syear'}">
						<td  style="width:6.25%">年</td>
					</c:when>
					<c:when test="${params.latitude == 'smonth'}">
						<td  style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
					</c:when>
					<c:when test="${params.latitude == 'sday'}">
						<td   style="width:6.25%">年</td>
						<td   style="width:6.25%">月</td>
						<td   style="width:6.25%">日</td>
					</c:when>
				</c:choose>
				</c:when>
				<c:when test="${productMap!=null && fn:length(productMap) > 0}">
					<td style="width:6.25%">序号</td>
            		<td style="width:6.25%">货号</td>
					<td style="width:6.25%">出货数量</td>
		            <td style="width:6.25%">出货金额</td>
		            <td style="width:6.25%">出货成本</td>
		            <td style="width:6.25%">出货毛利</td>
		            <td style="width:6.25%">退货数量</td>
		            <td style="width:6.25%">退货金额</td>
		            <td style="width:6.25%">退货成本</td>
		            <c:choose>
					<c:when test="${params.latitude == 'syear'}">
						<td style="width:6.25%">年</td>
					</c:when>
					<c:when test="${params.latitude == 'smonth'}">
						<td style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
					</c:when>
					<c:when test="${params.latitude == 'sday'}">
						<td style="width:6.25%">年</td>
						<td style="width:6.25%">月</td>
						<td style="width:6.25%">日</td>
					</c:when>
				</c:choose>
				</c:when>
				<c:otherwise>
					<div class="noneData">
						<p>暂无数据!</p>
					</div>
				</c:otherwise>
			</c:choose>
			</tr>
         	</thead>
         	<tbody>
         	<c:set var="skucountSum" value="0" scope="page"></c:set>
          	<c:set var="deliveryAmountSum" value="0" scope="page"></c:set>
          	<c:set var="deliveryCostsSum" value="0" scope="page"></c:set>
          	<c:set var="grossProfitSum" value="0" scope="page"></c:set>
          	<c:set var="returncountSum" value="0" scope="page"></c:set>
          	<c:set var="returnAmountSum" value="0" scope="page"></c:set>
          	<c:set var="returnCostSum" value="0" scope="page"></c:set>
          	<c:set var="soldPriceSum" value="0" scope="page"></c:set>
          	<c:set var="costPriceSum" value="0" scope="page"></c:set>
          	
	        <c:choose>
	        
	          	<c:when test="${shopSalesList!=null && fn:length(shopSalesList) > 0}">
				<c:forEach var="shopSales" items="${shopSalesList}" varStatus="status">
					<tr class="text-center">
						<c:set var="skucountSum" value="${skucountSum+shopSales.value.skucount}" scope="page"></c:set>
						<c:set var="deliveryAmountSum" value="${deliveryAmountSum+shopSales.value.deliveryAmount}" scope="page"></c:set>
						<c:set var="deliveryCostsSum" value="${deliveryCostsSum+shopSales.value.deliveryCosts}" scope="page"></c:set>
						<c:set var="grossProfitSum" value="${grossProfitSum+((shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000)}" scope="page"></c:set>
						<c:set var="returncountSum" value="${returncountSum+shopSales.value.returncount}" scope="page"></c:set>
						<c:set var="returnAmountSum" value="${returnAmountSum+shopSales.value.returnAmount}" scope="page"></c:set>
						<c:set var="returnCostSum" value="${returnCostSum+shopSales.value.returnCosts}" scope="page"></c:set>
						<c:set var="soldPriceSum" value="${soldPriceSum+shopSales.value.soldPrice}" scope="page"></c:set>
						<c:set var="costPriceSum" value="${costPriceSum+shopSales.value.costprice}" scope="page"></c:set>
						<td>${status.count}</td>
						<td>${shopSales.value.skucount}</td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryCosts}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${(shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000}" pattern="0.00" type="number"/></td>
						<td>${shopSales.value.returncount}</td>
						<td><fmt:formatNumber value="${shopSales.value.returnAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnCosts}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.soldPrice}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.costprice}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td>${shopSales.value.syear}</td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
								<td>${shopSales.value.sday}</td>
							</c:when>
						</c:choose>
					</tr>
				</c:forEach>
					<tr class="sumTr">
						<td>合计：</td>
						<td>${skucountSum}</td>
						<td><fmt:formatNumber value="${deliveryAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${deliveryCostsSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${grossProfitSum}" pattern="0.00" type="number"/></td>
						<td>${returncountSum}</td>
						<td><fmt:formatNumber value="${returnAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${returnCostSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${soldPriceSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${costPriceSum}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td></td>
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td></td>
								<td></td>
								<td></td>
							</c:when>
						</c:choose>
					</tr>
				</c:when>
				
				<c:when test="${shopProductMap!= null && fn:length(shopProductMap) > 0}">
				<c:forEach var="shopSales" items="${shopProductMap}" varStatus="status">
					<tr class="text-center">
						<c:set var="skucountSum" value="${skucountSum+shopSales.value.skucount}" scope="page"></c:set>
						<c:set var="deliveryAmountSum" value="${deliveryAmountSum+shopSales.value.deliveryAmount}" scope="page"></c:set>
						<c:set var="deliveryCostsSum" value="${deliveryCostsSum+shopSales.value.deliveryCosts}" scope="page"></c:set>
						<c:set var="grossProfitSum" value="${grossProfitSum+((shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000)}" scope="page"></c:set>
						<c:set var="returncountSum" value="${returncountSum+shopSales.value.returncount}" scope="page"></c:set>
						<c:set var="returnAmountSum" value="${returnAmountSum+shopSales.value.returnAmount}" scope="page"></c:set>
						<c:set var="returnCostSum" value="${returnCostSum+shopSales.value.returnCosts}" scope="page"></c:set>
						<td>${status.count}</td>
						<td>${shopSales.value.shopname}</td>
						<td>${shopSales.value.procode}</td>
						<td>${shopSales.value.skucount}</td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryCosts}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${(shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returncount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnCosts}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td>${shopSales.value.syear}</td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
								<td>${shopSales.value.sday}</td>
							</c:when>
						</c:choose>
					</tr>
				</c:forEach>
					<tr class="sumTr">
						<td></td>
						<td></td>
						<td>合计：</td>
						<td>${skucountSum}</td>
						<td><fmt:formatNumber value="${deliveryAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${deliveryCostsSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${grossProfitSum}" pattern="0.00" type="number"/></td>
						<td>${returncountSum}</td>
						<td><fmt:formatNumber value="${returnAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${returnCostSum}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td></td>
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td></td>
								<td></td>
								<td></td>
							</c:when>
						</c:choose>
					</tr>
				</c:when>
				
				<c:when test="${shopMap!= null && fn:length(shopMap) > 0}">
				<c:forEach var="shopSales" items="${shopMap}" varStatus="status">
					<tr class="text-center">
						<c:set var="skucountSum" value="${skucountSum+shopSales.value.skucount}" scope="page"></c:set>
						<c:set var="deliveryAmountSum" value="${deliveryAmountSum+shopSales.value.deliveryAmount}" scope="page"></c:set>
						<c:set var="deliveryCostsSum" value="${deliveryCostsSum+shopSales.value.deliveryCosts}" scope="page"></c:set>
						<c:set var="grossProfitSum" value="${grossProfitSum+((shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000)}" scope="page"></c:set>
						<c:set var="returncountSum" value="${returncountSum+shopSales.value.returncount}" scope="page"></c:set>
						<c:set var="returnAmountSum" value="${returnAmountSum+shopSales.value.returnAmount}" scope="page"></c:set>
						<c:set var="returnCostSum" value="${returnCostSum+shopSales.value.returnCosts}" scope="page"></c:set>
						<td>${status.count}</td>
						<td>${shopSales.value.shopname}</td>
						<td>${shopSales.value.skucount}</td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryCosts}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${(shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returncount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnCosts}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td>${shopSales.value.syear}</td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
								<td>${shopSales.value.sday}</td>
							</c:when>
						</c:choose>
					</tr>
				</c:forEach>
					<tr class="sumTr">
						<td></td>
						<td>合计：</td>
						<td>${skucountSum}</td>
						<td><fmt:formatNumber value="${deliveryAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${deliveryCostsSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${grossProfitSum}" pattern="0.00" type="number"/></td>
						<td>${returncountSum}</td>
						<td><fmt:formatNumber value="${returnAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${returnCostSum}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td></td>
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td></td>
								<td></td>
								<td></td>
							</c:when>
						</c:choose>
					</tr>
				</c:when>
				
				<c:when test="${productMap!= null && fn:length(productMap) > 0}">
				<c:forEach var="shopSales" items="${productMap}" varStatus="status">
					<tr class="text-center">
					<c:set var="skucountSum" value="${skucountSum+shopSales.value.skucount}" scope="page"></c:set>
						<c:set var="deliveryAmountSum" value="${deliveryAmountSum+shopSales.value.deliveryAmount}" scope="page"></c:set>
						<c:set var="deliveryCostsSum" value="${deliveryCostsSum+shopSales.value.deliveryCosts}" scope="page"></c:set>
						<c:set var="grossProfitSum" value="${grossProfitSum+((shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000)}" scope="page"></c:set>
						<c:set var="returncountSum" value="${returncountSum+shopSales.value.returncount}" scope="page"></c:set>
						<c:set var="returnAmountSum" value="${returnAmountSum+shopSales.value.returnAmount}" scope="page"></c:set>
						<c:set var="returnCostSum" value="${returnCostSum+shopSales.value.returnCosts}" scope="page"></c:set>
						<td>${status.count}</td>
						<td>${shopSales.value.procode}</td>
						<td>${shopSales.value.skucount}</td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.deliveryCosts}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${(shopSales.value.deliveryAmount*10000-shopSales.value.deliveryCosts*10000)/10000}" pattern="0.00" type="number"/></td>
						<td>${shopSales.value.returncount}</td>
						<td><fmt:formatNumber value="${shopSales.value.returnAmount}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${shopSales.value.returnCosts}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td>${shopSales.value.syear}</td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td>${shopSales.value.syear}</td>
								<td>${shopSales.value.smonth}</td>
								<td>${shopSales.value.sday}</td>
							</c:when>
						</c:choose>
					</tr>
				</c:forEach>
					<tr class="sumTr">
						<td></td>
						<td>合计：</td>
						<td>${skucountSum}</td>
						<td><fmt:formatNumber value="${deliveryAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${deliveryCostsSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${grossProfitSum}" pattern="0.00" type="number"/></td>
						<td>${returncountSum}</td>
						<td><fmt:formatNumber value="${returnAmountSum}" pattern="0.00" type="number"/></td>
						<td><fmt:formatNumber value="${returnCostSum}" pattern="0.00" type="number"/></td>
						<c:choose>
							<c:when test="${params.latitude == 'syear'}">
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'smonth'}">
								<td></td>
								<td></td>
							</c:when>
							<c:when test="${params.latitude == 'sday'}">
								<td></td>
								<td></td>
								<td></td>
							</c:when>
						</c:choose>
					</tr>
				</c:when>
			
			</c:choose>
		</tbody>
	</table>
</div>