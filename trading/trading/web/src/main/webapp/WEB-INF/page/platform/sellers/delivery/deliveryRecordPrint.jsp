<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
<style type="text/css">
	img[src=""]{
		opacity: 0;
	}
</style>
<div class="print_m">
	<div id="print_yc">
		<h3 class="print_title">发货单</h3>
		<table class="table_n">
			<tr>
				<td>发货单号：<span>${delivery.deliverNo}</span></td>
				<td>发货日期：<span><fmt:formatDate value="${delivery.sendDate}" type="date"/></span></td>
			</tr>
			<tr>
				<td>发货单位：<span>${delivery.supplierName}</span></td>
				<td>运单号：<span>${logistics.waybillNo}</span></td>
			</tr>
		</table>
		<table class="table_n">
			<tr>
				<td >收货单位：<span>${order.companyName}</span></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">收货地址：<%--${order.personName},${order.receiptPhone},--%>
					<%--<c:if test="${order.planeNumber !=null && order.planeNumber !=''}">
						<c:if test="${order.areaCode !=null && order.areaCode !=''}">
							${order.areaCode}-
						</c:if>
						${order.planeNumber},
					</c:if>--%>
					${order.provinceName}&nbsp;${order.cityName}&nbsp;${order.areaName}&nbsp;${order.addrName}
				</td>
			</tr>

		</table>
		<p class="table_r"  style="position: relative;">注：收货数量一致时，请在收货数量栏目框内打 “ √ ”，收货不一致则在其填写实际收货数量；二维码用于上传收货单。
			<img src="${delivery.qrcodeAddr}" style="position: absolute;bottom: 0px;right: 0;width: 120px;height: 120px"/>
		</p>
		<table class="layui-table table_c " >
			<thead>
				<tr>
					<td style="width:20%">订单号</td>
					<td style="width:15%">商品名称</td>
					<td style="width:10%">条形码</td>
					<td style="width:10%">单位</td>
					<td style="width:20%">备注</td>
					<td style="width:12%">销售单价</td>
					<td style="width:12%">发货数量</td>
					<td style="width:12%">到货数量</td>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="deliveryItem" items="${deliveryItemList}" varStatus="i">
				<tr>
					<td>${deliveryItem.orderCode}</td>
					<td style="white-space: normal">${deliveryItem.productName}</td>
					<td style="white-space: normal;word-break:break-all">${deliveryItem.barcode}</td>
					<td>${deliveryItem.unitName}</td>
					<td style="white-space: normal">${deliveryItem.remark}</td>
					<td>${deliveryItem.salePrice}</td>
					<td>${deliveryItem.deliveryNum}</td>
					<td></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<table class="table_n table_b mt">
			<tr>
				<td>收货人：（签字）</td>
				<td></td>
				<td>收货日期：&emsp;&emsp;&emsp;&emsp;年&emsp;&emsp;&emsp;月&emsp;&emsp;&emsp;日</td>
			</tr>
		</table>
	</div>
	<div class="print_b">
		<span id="print_sure">打印</span>
	</div>
</div>
<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $("#print_yc").jqprint();
    });
</script>