<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%--<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>--%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单发票列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billinvoice/addBillInvoice.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<script type="text/javascript">
	$(function(){
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});
    //标签页改变
    function setStatus(obj,status) {
        $("#acceptInvoiceStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
	</script>
</head>
<!--内容-->
<div>
	<div id="tabContent" style="width:auto; max-height:300px; overflow:auto">
	<!--列表区-->
	<table class="table_pure invoice_list">
		<thead>
		<tr>
			<td style="width:10%">对账单号</td>
			<td style="width:10%">发货单号</td>
			<td style="width:10%">下单人</td>
			<td style="width:10%">客户名称</td>
			<td style="width:10%">商品</td>
			<td style="width:10%">单位</td>
			<td style="width:10%">单价</td>
			<td style="width:5%">到货数量</td>
			<td style="width:9%">商品总金额</td>
			<td style="width:10%">订单号</td>
			<%--<td style="width:10%">状态</td>--%>
			<td style="width:9%">到货日期</td>
			<td style="width:5%">状态</td>
			<td style="width: 8%">操作</td>
		</tr>
		</thead>
	<tbody>
<c:forEach var="deliveryItemInfo" items="${drawUpInvoiceToCommodity.deliveryItemList}">
	<%--<div class="order_list" id="dataList">--%>
		<%--<table>--%>
			<tr>
				<td>
					<input type="hidden" name="recItemId" value="${deliveryItemInfo.deliveryRecItem.id}">
					<input type="hidden" name="reconciliationId" value="${deliveryItemInfo.deliveryRecItem.reconciliationId}">
						${deliveryItemInfo.deliveryRecItem.reconciliationId}</td>
				<td>${deliveryItemInfo.deliveryRecItem.deliverNo}</td>
				<td>${deliveryItemInfo.deliveryRecItem.createBillName}</td>
				<td>
					<input type="hidden" name="buyCompanyId" value="${deliveryItemInfo.deliveryRecItem.buyCompanyId}">
					<input type="hidden" name="buyCompanyName" value="${deliveryItemInfo.deliveryRecItem.buyCompanyName}">
					${deliveryItemInfo.deliveryRecItem.buyCompanyName}</td>
				<td>${deliveryItemInfo.deliveryRecItem.productName}</td>
				<td>${deliveryItemInfo.deliveryRecItem.unitName}</td>
				<td>
					<c:choose>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isUpdateSale == 1}">
							${deliveryItemInfo.deliveryRecItem.updateSalePrice}
						</c:when>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isUpdateSale != 1}">
							${deliveryItemInfo.deliveryRecItem.salePrice}
						</c:when>
					</c:choose>
						</td>
				<td>${deliveryItemInfo.deliveryRecItem.arrivalNum}</td>
				<td>
					<input type="hidden" name="salePriceSum" value="${deliveryItemInfo.salePriceSum}">
					${deliveryItemInfo.salePriceSum}</td>
				<td>${deliveryItemInfo.deliveryRecItem.orderCode}</td>

				<td>${deliveryItemInfo.deliveryRecItem.arrivalDateStr}</td>
				<td>
					<c:choose>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 0}">待开票据</c:when>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 1}">待对方确认票据</c:when>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 2}">已确认票据</c:when>
						<c:when test="${deliveryItemInfo.deliveryRecItem.isInvoiceStatus == 3}">驳回票据</c:when>
					</c:choose>
					<%--<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/sellerManualOrderDetails?orderId=${sellerBillReconciliation.sellerDeliveryRecordItem.orderId}','sellers');" class="approval">订单详情</a>--%>
				</td>
				<td>
					<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${deliveryItemInfo.attachmentAddrStr}');">查看单据</span>
				</td>
			</tr>
		<%--</table>--%>
	<%--</div>--%>
</c:forEach>
</tbody>
</table>
</div>
<div>
	<span>合计商品总金额：</span>
	<span>${drawUpInvoiceToCommodity.priceSum}</span>
	<input type="hidden" id="priceSum" name="priceSum" value="${drawUpInvoiceToCommodity.priceSum}">
</div>
<h4 class="page_title mp30">开票表单</h4><br>
<div class="voucher">
	<li>
		<label>票据类型：</label>
		<select name="invoiceType" id="invoiceType">
			<option value="0">请选择</option>
			<option value="1">专用发票</option>
			<option value="2">普通发票</option>
			<option value="3">其他票据</option>
		</select>
	</li><br>
	<li>
		<label>票据抬头：</label>
		<input type="text" required placeholder="输入发票抬头" id="invoiceHeader" name="invoiceHeader" />
	</li><br>

	<li>
		<label>票据总额：</label>
		<input type="number" required placeholder="输入票据总额" id="totalInvoiceValue" name="totalInvoiceValue" min="0"/>
	</li><br>
	<li>
		<span>票据编号：</span>
		<input type="text" required placeholder="请输入票据编号" id="invoiceNo" name="invoiceNo">
	</li><br>
	<li>
		<span>票据附件：</span>
		<div class="upload_license">
			<input type="file" multiple name="invoiceFileAddr" id="invoiceFileAddr" onchange="uploadInvoice();">
			<p></p>
			<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
		</div>
		<div class="scanning_copy original" id="invoiceFileDiv"></div>
	</li><br>
</div>

<div class="btn_p text-center">
	<span class="order_p" onclick="saveInvoice();">确认开票</span>
	<span class="order_p" onclick="leftMenuClick(this,'platform/seller/billInvoice/billInvoiceList','sellers');">返回</span>
</div>

</div>
</div>
</div>
<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>