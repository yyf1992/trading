<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>

    //功能点2:查看审批流程
    $('.opinion_view').hover(
        function(){
            $(this).children('.opinion').toggle();
        }
    );

    //功能点1:全选事件
    function selAll(){
        var inputs=$('.order_list>p>span.chk_click input');
        function checkedSpan(){
            inputs.each(function(){
                if($(this).prop('checked')==true){
                    $(this).parent().addClass('checked')
                }else{
                    $(this).parent().removeClass('checked')
                }
            })
        }

        inputs.click(function(){
            if($(this).prop('checked')==true){
                $(this).parent().addClass('checked')
            }else{
                $(this).parent().removeClass('checked')
            }
            var r=$('.order_list>p>span.chk_click input:not(:checked)');
            if(r.length==0){
                $('.batch_handle input').prop('checked',true).parent().addClass('checked');
            }else{
                $('.batch_handle input').prop('checked',false).parent().removeClass('checked');
            }
        });
    }
    selAll();

</script>

<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:76%">
			<ul class="clear">
				<li style="width:50%">商品</li>
				<li style="width:15%">条形码</li>
				<li style="width:10%">单位</li>
				<li style="width:10%">数量</li>
				<li style="width:15%">备注</li>
			</ul>
		</td>
		<td style="width:12%">交易状态</td>
		<td style="width:12%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list" id="dataList">
		<p>
			<span class="chk_click"><input type="checkbox" value="${orders.id}" name="checkone"></span>
			<span class="apply_time"><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></span>
			<span class="order_num">订单号: <b>${orders.orderCode}</b></span>
			<span>${orders.suppName}</span>
		</p>
		<table>
			<tr>
				<td style="width:76%">
					<c:forEach var="product" items="${orders.supplierProductList}" varStatus="i">
						<ul class="clear">
							<li style="width:50%">
								<!--  <img src="statics/platform/images/04.jpg"> -->
								<div>
										${product.productCode} ${product.productName}<br>
									<span>规格代码: ${product.skuCode}</span>
									<span>规格名称: ${product.skuName}</span>
								</div>
							</li>
							<li style="width:15%">${product.barcode}</li>
							<li style="width:10%">${product.unitName}</li>
							<li style="width:10%">${product.orderNum}</li>
							<li style="width:15%">${product.remark}</li>
						</ul>
					</c:forEach>
				</td>

				<td style="width:12%">
					<div>
						<c:choose>
							<c:when test="${orders.status==0}">内部审批</c:when>
							<c:when test="${orders.status==1}">待发货</c:when>
							<c:when test="${orders.status==2}">发货中</c:when>
							<c:when test="${orders.status==3}">发货完成</c:when>
							<c:when test="${orders.status==4}">已收货</c:when>
							<c:when test="${orders.status==5}">交易完成</c:when>
							<c:when test="${orders.status==6}">已取消</c:when>
							<c:when test="${orders.status==7}">已驳回订单</c:when>
							<c:otherwise>
								内部审批中
							</c:otherwise>
						</c:choose>
					</div>
					<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/sellerManualOrderDetails?orderId=${orders.id}','sellers');" class="approval">订单详情</a>
				<!-- 	<div class="opinion_view">
						<span class="orange">查看审批流程</span>
					</div>  -->
				</td>
				<td style="width:12%">
					<c:choose>
						<c:when test="${orders.isCheck==0}">待内部审核</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${orders.isCheck==1}">审核通过</c:when>
								<c:when test="${orders.isCheck==2}">审核未通过</c:when>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>

<!--分页-->
<div class="pager">${searchPageUtil.page}</div>



