<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function(){
        if ('${billCycleManagementNew.billDealStatus}'<4){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "待内部审批"
                },{
                    title: "待对方审批"
                },{
                    title: "审批已通过"
                }/*,{
                    title: "内部审批驳回"
                },{
                    title: "对方审批驳回"
                }*/]
            });
            
            var step = parseInt('${billCycleManagementNew.billDealStatus}');
            $(".ystep1").setStep(step);
        }
    });

</script>
<%--<div >--%>
	<div class="order_top mt">
		<div class="lf order_status" >
			<h4 class="mt">当前订单状态</h4>
			<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==1}">待内部审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==2}">待对方审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==3}">审批已通过</c:when>--%>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==4}">内部审批驳回</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==5}">对方审批驳回</c:when>--%>
			  <c:when test="${billCycleManagementNew.billDealStatus == 1}">待内部审批</c:when>
			  <c:when test="${billCycleManagementNew.billDealStatus==4}">内部审批驳回</c:when>
			  <c:when test="${billCycleManagementNew.billDealStatus == 2}">待对方审批</c:when>
			  <c:when test="${billCycleManagementNew.billDealStatus==3}">对方审批已通过</c:when>
			  <c:when test="${billCycleManagementNew.billDealStatus==5}">对方审批驳回</c:when>
		  </c:choose>
        </span>
			</p>
			<p class="order_remarks text-center"></p>
		</div>
		<div class="lf order_progress">
			<div class="ystep1"></div>
		</div>
	</div>
	<div>
		<h4 class="mt">账单结算周期信息修改申请</h4>
		<div class="order_d">
			<p><span class="order_title">采购商信息</span></p>
			<table class="table_info">
				<tr>
					<td>采购商名称：<span>${billCycleManagementNew.buyCompanyName}</span></td>
					<%--<td>负责人：<span>小萌</span></td>
					<td>手机号：<span>12435</span></td>--%>
				</tr>
			</table>
			<p class="line mt"></p>
			<p><span class="order_title">供应商信息</span></p>
			<table class="table_info">
				<tr>
					<td>供应商名称：<span>${billCycleManagementNew.sellerCompanyName}</span></td>
					<%--<td>负责人：<span>萌萌</span></td>
					<td>手机号：<span>12234325</span></td>--%>
				</tr>
			</table>

			<p class="line mt"></p>
			<p><span class="order_title">账单结算周期信息</span></p>
			<table class="table_info">
				<%--<tr>
					<td>申请结账周期（天）：<span>${billCycleManagementNew.checkoutCycle}</span></td>
					<td>结账周期（天）：<span>${billCycleManagementInfo.checkoutCycle}</span></td>
				</tr>--%>
				<tr>
					<td>申请出账日期（日）：<span>${billCycleManagementNew.billStatementDate}</span></td>
					<td>出账日期（日）：<span>${billCycleManagementInfo.billStatementDate}</span></td>
				</tr>
				<tr>
					<td>申请创建日期：<span>${billCycleManagementNew.createDate}</span></td>
					<td>创建日期：<span>${billCycleManagementInfo.createDate}</span></td>
				</tr>
				<tr>
					<td>申请创建人：<span>${billCycleManagementNew.createName}</span></td>
					<td>创建人：<span>${billCycleManagementInfo.createName}</span></td>
				</tr>
			</table>

			<p class="line mt"></p>
			<p><span class="order_title">账单结算周期关联利息信息</span></p>
			<div style="width:auto;height:100px;overflow:auto">
			<table class="table_pure detailed_list">
				<thead>
				<tr>
					<td style="width:33%">申请逾期（天）</td>
					<td style="width:33%">申请月利率（%）</td>
					<td style="width:33%">申请利息计算方式</td>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="interestListNew" items="${billInterestNewList}">
					<tr>
						<td>${interestListNew.overdueDate}</td>
						<td>${interestListNew.overdueInterest}</td>
						<td>
							<c:choose>
								<c:when test="${interestListNew.interestCalculationMethod eq '1'}">单利</c:when>
								<c:when test="${interestListNew.interestCalculationMethod eq '2'}">复利</c:when>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			</div>
			<p class="line mt"></p>
			<div style="width:auto;height:100px;overflow:auto">
			<table class="table_pure detailed_list">
				<thead>
				<tr>
					<td style="width:33%">逾期（天）</td>
					<td style="width:33%">月利率（%）</td>
					<td style="width:33%">利息计算方式</td>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="interestList" items="${interestList}">
					<tr>
						<td>${interestList.overdueDate}</td>
						<td>${interestList.overdueInterest}</td>
						<td>
							<c:choose>
								<c:when test="${interestList.interestCalculationMethod eq '1'}">单利</c:when>
								<c:when test="${interestList.interestCalculationMethod eq '2'}">复利</c:when>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			</div>
		</div>
	</div>
<%--
</div>--%>
