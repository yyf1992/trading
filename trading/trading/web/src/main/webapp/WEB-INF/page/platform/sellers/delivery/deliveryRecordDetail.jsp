<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<%--合并发货界面--%>
<script type="text/javascript">
	//返回发货单列表
	function returnDeliveryList() {
        leftMenuClick(this,"platform/seller/delivery/deliveryRecordList","sellers");
    }
</script>
<div>
	<!--列表区-->
	<form id="mySingleForm" method="post" action="">
		<table class="order_detail">
			<tbody>
			<tr>
				<td style="width:20%">商品</td>
				<td style="width:10%">条形码</td>
				<td style="width:5%">单位</td>
				<%--<td style="width:10%">当前库存</td>--%>
				<td style="width:10%">销售单价</td>
				<td style="width:5%">数量</td>
				<td style="width:10%">已发货数量</td>
				<td style="width:10%">本次发货数量</td>
				<!-- <td style="width:10%">发货类型</td> -->
				<td style="width:20%">备注</td>
			</tr>
			</tbody>
		</table>
		<div class="deliver_list mt" id="detail_interworkGoods">
			<c:forEach var="delivery" items="${deliveryList}">
			<p>
				<span class="order_n">订单号: <b>${delivery.orderCode}</b></span>
				<span class="apply_t"><fmt:formatDate value="${delivery.orderDate}" type="both"></fmt:formatDate></span>
				<span>${delivery.buycompanyName}</span>
			</p>
			<table>
				<c:forEach var="item" items="${delivery.itemList}">
					<tr>
						<td style="width:20%">
							<img src="<%=basePath%>/statics/platform/images/defaulGoods.jpg">
							<div>${item.productCode} ${item.productName} <br>
								<span>规格代码: ${item.skuCode}</span><br>
								<span>规格名称: ${item.skuName}</span>
							</div>
						</td>
						<td style="width:10%">${item.barcode}</td>
						<td style="width:5%">${item.unitName}</td>
						<%--<td style="width:10%">500000</td>--%>
						<td style="width:10%">${item.salePrice}</td>
						<td style="width:5%">${item.orderNum}</td>
						<td style="width:10%">${item.deliveredNum}</td>
						<td style="width:10%">${item.deliveryNum}</td>
						<%-- <td style="width:10%">
							<c:choose>
								<c:when test="${item.status == '0'}">全部发货</c:when>
								<c:otherwise>部分发货</c:otherwise>
							</c:choose>
						</td> --%>
						<td style="width:20%">${item.remark}</td>
					</tr>
				</c:forEach>
			</table>
			</c:forEach>
		</div>
		<div style="padding: 30px 10px; border: 1px solid #e5e5e5;">
			<ul class="layui-row" style="overflow: hidden">
				<li class="layui-col-sm3">
					<label>发货类型：</label>
					<select name="dropshipType" id="dropshipType" lay-filter="aihao" disabled>
						<option value="0" <c:if test="${deliveryRecord.dropshipType=='0'}">selected</c:if>>正常发货</option>
						<option value="1" <c:if test="${deliveryRecord.dropshipType=='1'}">selected</c:if>>代发客户</option>
						<option value="2" <c:if test="${deliveryRecord.dropshipType=='2'}">selected</c:if>>代发菜鸟仓</option>
						<option value="3" <c:if test="${deliveryRecord.dropshipType=='3'}">selected</c:if>>代发京东仓</option>
					</select>
				</li>
				<li  class="layui-col-sm5">
					<label class="layui-col-sm3 text-right">收货地址：</label>
					<div class="layui-col-sm9">
						${deliveryRecord.receiveAddr}
					</div>
				</li>
			</ul>
		</div>
		<h4 class="page_title mp30">物流明细</h4>
		<div class="receive">
			<ul>
				<li>
					<label>运单号码：</label>${logistics.waybillNo}
				</li>
				<li>
					<label>物流公司：</label>${logistics.logisticsCompany}
				</li>
				<li>
					<label>司机姓名：</label>${logistics.driverName}
				</li>
				<li>
					<label>司机手机：</label>${logistics.mobilePhone}
				</li>
				<li>
					<label>商品运费：</label>${logistics.freight}
				</li>
				<li>
					<label>固定电话：</label>${logistics.zoneCode}
					<c:if test="${logistics.zoneCode != null && logistics.zoneCode != ''}">${logistics.zoneCode}-</c:if>
					<c:if test="${logistics.fixedPhone != null && logistics.fixedPhone != ''}">${logistics.fixedPhone}-</c:if>
					<c:if test="${logistics.extPhone != null && logistics.extPhone != ''}">${logistics.extPhone}</c:if>
				</li>
				<li>
					<label>预达日期：</label><fmt:formatDate value="${logistics.expectArrivalDate}" ></fmt:formatDate>
				</li>
			</ul>
		</div>
		<div class="btn_p text-center">
			<a href="javascript:void(0)"
			 onclick="leftMenuClick(this,'platform/seller/delivery/deliveryRecordList?${form}','buyer','17070718133683994603');">
			<span class="order_p">返回</span></a>
		</div>

	</form>
</div>