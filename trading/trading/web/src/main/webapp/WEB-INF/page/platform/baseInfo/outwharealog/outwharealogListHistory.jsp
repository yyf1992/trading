<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
//重置
function recovery(){
	leftMenuClick(this,'platform/baseInfo/outwharealog/loadHistoryOutwharealogList',"baseInfo");
}
$(function(){
	$("#createName option[value='"+"${searchPageUtil.object.createName}"+"']").attr("selected","selected");
	$("#whareaName option[value='"+"${searchPageUtil.object.whareaName}"+"']").attr("selected","selected");
});
//导出
$("#outwharealogListExportButton").click(function(e){
	e.preventDefault();
	var formData = $("form").serialize();
	var url = basePath+"outwharealogHistoryListDownload/outwharealogHistoryListData?"+ formData;
	window.open(url);
});
</script>
<div>
    <form class="layui-form" action="platform/baseInfo/outwharealog/loadHistoryOutwharealogList">
     <ul class="order_search platformSearch">
       <li>
            <label>子公司：</label>
            <input type="text" placeholder="输入子公司" style="width:163px;" id="subcompanyName" name="subcompanyName" value="${searchPageUtil.object.subcompanyName}">
       </li>
       <li >
         	<label>货号：</label>
         	<input type="text" placeholder="输入货号" name="productCode" style="width:163px;" value="${searchPageUtil.object.productCode}">
       </li>
       <li>
         	<label>条形码：</label>
          	<input type="text" placeholder="输入条形码" name="barcode" value="${searchPageUtil.object.barcode}">
       </li>	     
       <li>
         	<label>仓库名称：</label>
        	<div class="layui-input-inline">
	          <select id="whareaName" name="whareaName" lay-verify="required" lay-search="">
				<option value="">请选择仓库</option>
				<option value="菜鸟仓">菜鸟仓</option>
				<option value="京东仓">京东仓</option>
			  </select>
			</div>
       </li>
       <li>
          	<label>店铺名称：</label>
          	<input type="text" placeholder="输入店铺名称" name="shopName" style="width:163px;" value="${searchPageUtil.object.shopName}">
       </li>
       <li class="spec">
          <label>成本区间：</label>
          <input placeholder="￥" type="number" style="width:73px" id="minPrice" name="minPrice" value="${searchPageUtil.object.minPrice}">
          -
          <input placeholder="￥" type="number" style="width:73px" id="maxPrice" name="maxPrice" value="${searchPageUtil.object.maxPrice}">
       </li>
       <li class="spec">
          <label>在仓数量：</label>
          <input  type="number" style="width:73px" id="minOutWhareaStock" name="minOutWhareaStock" value="${searchPageUtil.object.minOutWhareaStock}">
          -
          <input  type="number" style="width:73px" id="maxOutWhareaStock" name="maxOutWhareaStock" value="${searchPageUtil.object.maxOutWhareaStock}">
       </li>
       <li class="spec">
          <label>在途数量：</label>
          <input  type="number" style="width:73px" id="minOutWhareaWayStock" name="minOutWhareaWayStock" value="${searchPageUtil.object.minOutWhareaWayStock}">
          -
          <input  type="number" style="width:73px" id="maxOutWhareaWayStock" name="maxOutWhareaWayStock" value="${searchPageUtil.object.maxOutWhareaWayStock}">
       </li>
       <li class="spec">
          <label>库存金额：</label>
          <input placeholder="￥" type="number" style="width:73px" id="minOutWhareaTotalPrice" name="minOutWhareaTotalPrice" value="${searchPageUtil.object.minOutWhareaTotalPrice}">
          -
          <input placeholder="￥" type="number" style="width:73px" id="maxOutWhareaTotalPrice" name="maxOutWhareaTotalPrice" value="${searchPageUtil.object.maxOutWhareaTotalPrice}">
       </li>
       <li class="spec">
          <label>历史时间：</label>
          <div class="layui-input-inline">
            <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
            
          </div>
          <div class="layui-input-inline">
            <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
          </div>
       </li>
       <li>
          	<label>创建人：</label>
         	<div class="layui-input-inline">
	    	     <select id="createName" name="createName" lay-verify="required" lay-search="">
					<option value="">直接选择或搜索选择</option>
					<c:forEach var="companyUser" items="${companyUserList}">
						<option value="${companyUser.userName}">${companyUser.userName}</option>
					</c:forEach>
				</select>
			</div>
        </li>
        <li class="nomargin rt">
       		<button class="search" onclick="loadPlatformData();">搜索</button>
       		<button type="button" class="search" onclick="recovery();">重置</button>
       	</li>
     </ul>
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
 	     <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
    </form>
	 
     <div class="newBuild mt" style="overflow:hidden">
       <a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="outwharealogListExportButton" style="margin-left:10px">
		<i class="layui-icon">&#xe7a0;</i> 导出
       </a> 
     </div>   
     <table class="table_pure platformList">
       <thead>
	       <tr>
	         <td style="width:10%">子公司</td>
	         <td style="width:10%">店铺</td>
	         <td style="width:10%">货号</td>
	         <td style="width:10%">条形码</td>
	         <td style="width:10%">成本价</td>
	         <td style="width:10%">外仓在仓数量</td>
	         <td style="width:10%">外仓在途数量</td>
	         <td style="width:10%">外仓库存金额</td>
	         <td style="width:10%">仓库名称</td>
	         <td style="width:10%">创建人</td>
	         <td style="width:10%">历史时间</td>
	       </tr>
       </thead>
       <tbody>
	       	<c:forEach var="outWhareaLog" items="${searchPageUtil.page.list}" varStatus="status">
	       		<tr>
	           		<td>${outWhareaLog.subcompanyName}</td>
	           		<td>${outWhareaLog.shopName}</td>
	           		<td>${outWhareaLog.productCode}</td>
	           		<td>${outWhareaLog.barcode}</td>
	           		<td>${outWhareaLog.price}</td>
	           		<td>${outWhareaLog.outWhareaStock}</td>
	           		<td>${outWhareaLog.outWhareaWayStock}</td>
	           		<td>${outWhareaLog.outWhareaTotalPrice}</td>
	           		<td>${outWhareaLog.whareaName}</td>
	           		<td>${outWhareaLog.createName}</td>
	           		<td>
	           			<fmt:formatDate value="${outWhareaLog.historyDate}" type="date"/>
	           		</td>
	         	</tr>
	       	</c:forEach>
        </tbody>
      </table>
	  <div class="pager">${searchPageUtil.page}</div>
</div>