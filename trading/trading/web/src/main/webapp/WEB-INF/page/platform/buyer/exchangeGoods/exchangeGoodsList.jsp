<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(function(){
	//重新渲染select控件
	var form = layui.form;
	form.render("select");
	//搜索更多
	$(".search_more").click(function(){
	  $(this).toggleClass("clicked");
	  $(this).parent().nextAll("ul").toggle();
	});
	//设置选择的tab
	$(".tab>a").removeClass("hover");
	$(".tab #tab_${params.tabId}").addClass("hover");
	if("${params.tabId}"!='0'){
		$("#exchangeContent select[name='interest']").prop("disabled", true);
	}
	//日期
	loadDate("startDate","endDate");
	//初始化数据
	//selectData();
	$("#exchangeContent #selectButton").click(function(e){
		e.preventDefault();
		selectData();
	});
	$(".tab a").click(function(){
		var id = $(this).attr("id");
		//清空查询条件
		$("#exchangeContent #resetButton").click();
		var index = id.split("_")[1];
		$("#exchangeContent input[name='tabId']").val(index);
		loadPlatformData();
	});
});
function selectData(){
	var formObj = $("#exchangeContent").find("form");
	$.ajax({
		url : "platform/buyer/buyOrder/exchangeGoodsList",
		data:formObj.serialize(),
		async:false,
		success:function(data){
			var str = data.toString();
			$("#exchangeContent").html(str);
			loadVerify();
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div id="exchangeContent">
	<!--搜索栏-->
	<form class="layui-form" action="platform/buyer/buyOrder/exchangeGoodsList">
		<div class="search_top mt">
			<input type="text" placeholder="输入商品名称、货号、条形码或订单号进行搜索" name="selectValue" value="${params.selectValue}">
			<button id="selectButton">搜索</button><span class='search_more sBuyer'></span>
		</div>
		<ul class="order_search orderList_m">
			<li class="comm">
				<label>供应商名称:</label> 
				<input type="text" placeholder="输入供应商名称进行搜索" style="width:262px" name="supplierName" value="${searchPageUtil.object.supplierName}">
			</li>
			<li class="comm">
				<label>规格:</label>
				<input type="text" placeholder="输入规格搜索" style="width:200px" name="skucode" value="${searchPageUtil.object.skucode}">
			</li>
			<li class="range nomargin"><label>交易状态:</label>
				<div class="layui-input-inline">
					<select name="interest" lay-filter="aihao">
						<option value="0">全部</option>
						<option value="1" <c:if test="${searchPageUtil.object.interest==1}">selected</c:if>>待内部审批</option>
						<option value="2" <c:if test="${searchPageUtil.object.interest==2}">selected</c:if>>待接单</option>
						<option value="3" <c:if test="${searchPageUtil.object.interest==3}">selected</c:if>>待对方发货</option>
						<option value="4" <c:if test="${searchPageUtil.object.interest==4}">selected</c:if>>待收货</option>
						<option value="5" <c:if test="${searchPageUtil.object.interest==5}">selected</c:if>>交易已完成</option>
					</select>
				</div>
			</li>
			<li class="range"><label>下单日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" id="startDate" lay-verify="date" value="${searchPageUtil.object.startDate}" class="layui-input" placeholder="开始日">
				</div>
				<div class="layui-input-inline">
					<input type="text" name="endDate" id="endDate" lay-verify="date" value="${searchPageUtil.object.endDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
		</ul>
		<input type="hidden" name="tabId" value="${searchPageUtil.object.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent">
		<!--列表区-->
		<table class="order_detail">
			<tr>
				<td style="width:76%">
					<ul>
						<li style="width:30%">商品</li>
						<li style="width:10%">售后单号</li>
						<li style="width:12%">条形码</li>
						<li style="width:10%">单位</li>
						<li style="width:10%">数量</li>
						<li style="width:10%">单价</li>
						<li style="width:8%">总价</li>
						<li style="width:10%">备注</li>
					</ul></td>
				<td style="width:12%">交易状态</td>
				<td style="width:12%">操作</td>
			</tr>
		</table>
		<c:forEach var="orders" items="${searchPageUtil.page.list}">
			<div class="order_list">
				<p>
					<span class="apply_time"><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></span>
					<span class="order_num">订单号:
						<b>${orders.orderCode}</b>
					</span>
					<span class="order_num">${orders.suppName}</span>
					<c:if test="${orders.customerCreateName != '' && orders.customerCreateName != null}">
						<span class="order_num">售后创建人:${orders.customerCreateName}</span>
					</c:if>
					<c:if test="${orders.customerCreateDate != '' && orders.customerCreateDate != null}">
						<span>售后创建时间:<fmt:formatDate value="${orders.customerCreateDate}" type="both"></fmt:formatDate></span>
					</c:if>
				</p>
				<table>
					<tr>
						<td style="width:76%">
						<c:forEach var="product" items="${orders.orderProductList}">
							<ul class="clear">
								<li style="width:30%">
									<span class="defaultImg"></span>
									<div>
										${product.proCode}|${product.proName} <br>
										<span>规格代码:${product.skuCode}</span>
										<span>规格名称:${product.skuName}</span>
									</div></li>
								<li style="width:10%">
									<a href="javascript:void(0)" 
										onclick="leftMenuClick(this,'platform/buyer/customer/loadCustomerDetails?id=${orders.customerId}'+
										'&returnUrl=platform/buyer/buyOrder/exchangeGoodsList&menuId=17111614114647089721','buyer');" 
										style="color: blue;">${orders.customerCode}</a>
								</li>
								<li style="width:12%">${product.skuOid}</li>
								<li style="width:10%">${product.unitName}</li>
								<li style="width:10%">${product.goodsNumber}</li>
								<li style="width:10%">${product.price}</li>
								<li style="width:8%">${product.goodsNumber*product.price}</li>
								<li style="width:10%">${product.remark}</li>
							</ul>
						</c:forEach>
						</td>
						<td style="width:12%">
							<div>
								<c:choose>
									<c:when test="${orders.isCheck==0}">待内部审核</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${orders.status==0}">待接单</c:when>
											<c:when test="${orders.status==2}">待对方发货</c:when>
											<c:when test="${orders.status==3}">待收货</c:when>
											<c:when test="${orders.status==4}">已收货</c:when>
											<c:when test="${orders.status==6}">已取消</c:when>
											<c:when test="${orders.status==7}">已驳回</c:when>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
						</td>
						<td style="width:12%" class="operate">
							<a href="javascript:void(0)" button="详情"
								onclick="leftMenuClick(this,'platform/buyer/purchase/detail?id=${orders.id}','buyer','17112214505875474754');"
								class="layui-btn layui-btn-normal layui-btn-mini">
								<i class="layui-icon">&#xe695;</i>详情</a>
						</td>
					</tr>
				</table>
			</div>
		</c:forEach>
		<!--分页-->
		<div class="pager">${searchPageUtil.page}</div>
	</div>
</div>