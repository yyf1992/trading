<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
//全选
$("#rightRoleDiv input:checkbox[name='title-table-checkbox']").change(function(){
	var status = $(this).is(":checked");
	if(status){
		$("#rightRoleDiv input:checkbox[name='checkone']").prop("checked",status);
	}else{
		$("#rightRoleDiv input:checkbox[name='checkone']").prop("checked",status);
	}
});
</script>
<div class="widget-box">
	<div class="widget-title title-xs">
		<span class="icon bordernone"><i class="icon-group"></i> </span>
		<h5>人员角色</h5>
	</div>
	<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
		role="grid">
		<table class="table table-bordered table-striped with-check">
			<thead>
				<tr>
					<th><input type="checkbox" id="title-table-checkbox"
						name="title-table-checkbox"></th>
					<th>角色代码</th>
					<th>角色名</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="sysUserRight" items="${searchPageUtilRight.pageAdmin.list}">
				<tr class="text-center">
					<td><input type="checkbox" name="checkone"
						value="${sysUserRight.id}" />
					</td>
					<td>${sysUserRight.roleCode}</td>
					<td>${sysUserRight.roleName}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div id="page">${searchPageUtilRight.pageAdmin }</div>
	</div>
</div>
<form class="form-inline "
	action="admin/sysRoleUser/loadRightRoleList.html">
	<input name="userId" type="hidden"
			value="${searchPageUtilRight.object.userId}" />
	<!-- 分页隐藏数据 -->
	<input id="pageNo" name="pageAdmin.pageNo" type="hidden"
		value="${searchPageUtilRight.pageAdmin.pageNo}" />
	<input id="pageSize"
		name="pageAdmin.pageSize" type="hidden"
		value="${searchPageUtilRight.pageAdmin.pageSize}" />
	<input id="divId" name="pageAdmin.divId" type="hidden"
		value="${searchPageUtilRight.pageAdmin.divId}" />
</form>