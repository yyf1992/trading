<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>
<script type="text/javascript">
    $(function(){
        if ('${buyBillRecAdvance.status}'<4){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "待审批"
                },{
                    title: "审批通过"
                }/*,{
				 title: "内部审批驳回"
				 },{
				 title: "对方审批驳回"
				 }*/]
            });
            debugger;
            var step = parseInt('${buyBillRecAdvance.status }');
            $(".ystep1").setStep(step);
        }
    });

    //附件显示隐藏
    var hideOrShow = true;
    function showBillReceipt(receiptAddr) {
        $(".big_img").empty();
        $("#icon_list").empty();
        if(receiptAddr != null || receiptAddr != ''){
            var receiptAddrList = receiptAddr.split(',');
            var bigImg = '';
            for(i = 0;i<receiptAddrList.length;i++){
                var liObj = $("<li>");
                $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
                $("#icon_list").append(liObj);
                bigImg = receiptAddrList[0];
            }
            //大图
            $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
        }
        if(hideOrShow){
            $("#linkReceipt").show();
            hideOrShow = false;
        }else {
            $("#linkReceipt").hide();
            hideOrShow = true
        }
    }
</script>
<%--<div >--%>
<div class="order_top mt">
	<div class="lf order_status" >
		<h4 class="mt">充值审批状态</h4>
		<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <c:when test="${buyBillRecAdvance.status==1}">待内部审批</c:when>
			  <c:when test="${buyBillRecAdvance.status==2}">内部已通过</c:when>
			  <c:when test="${buyBillRecAdvance.status==3}">内部已驳回</c:when>
		  </c:choose>
        </span>
		</p>
		<p class="order_remarks text-center"></p>
	</div>
	<div class="lf order_progress">
		<div class="ystep1"></div>
	</div>
</div>
<div>
	<h4 class="mt">预付款充值信息</h4>
	<div class="order_d">
		<p class="line mt"></p>
		<p><span class="order_title">供应商信息</span></p>
		<table class="table_info">
			<tr>
				<td>供应商名称：<span>${buyBillRecAdvance.sellerCompanyName}</span></td>
			</tr>
		</table>

		<p class="line mt"></p>
		<p><span class="order_title">预付款充值信息</span></p>
		<div style="width:auto;height:100px;overflow:auto">
			<table class="table_pure detailed_list">
				<thead>
				<tr>
					<td style="width:12%">申请编号</td>
					<td style="width:12%">预付款账号</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">申请人</td>
					<td style="width:10%">申请时间</td>
					<td style="width:10%">申请金额</td>
					<td style="width:10%">申请备注</td>
					<td style="width:10%">充值状态</td>
					<%--<td style="width:10%">申请凭证</td>--%>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="billAdvanceEdit" items="${buyBillRecAdvance.buyBillRecAdvanceList}">
				  <tr>
				    <td style="text-align: center">${billAdvanceEdit.id}</td>
				    <td style="text-align: center">${billAdvanceEdit.advanceId}</td>
				    <td style="text-align: center">${billAdvanceEdit.createBillUserName}</td>
					<td>${billAdvanceEdit.createUserName}</td>
					<td>${billAdvanceEdit.createTimeStr}</td>
					<td>${billAdvanceEdit.updateTotal}</td>
					<td style="text-align: center">${billAdvanceEdit.advanceRemarks}</td>
					<td>
					  <c:choose>
						  <c:when test="${billAdvanceEdit.editStatus==1}">待内部审批</c:when>
						  <c:when test="${billAdvanceEdit.editStatus==2}">审批通过</c:when>
						  <c:when test="${billAdvanceEdit.editStatus==3}">审批驳回</c:when>
					  </c:choose>
					</td>
					<%--<td style="text-align: center">
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${buyBillRecAdvance.fileAddress}');">查看票据</span>
					</td>--%>
				  </tr>
				</c:forEach>
				</tbody>
				<%--<tr>
					<td>采购员：<span>${buyBillRecAdvance.createBillUserName}</span></td>
					<td>添加日期：<span>${buyBillRecAdvance.createTimeStr}</span></td>
					<td>添加备注：<span>${buyBillRecAdvance.createRemarks}</span></td>
				</tr>
				<tr>
					<td>可用金额：<span>${buyBillRecAdvance.usableTotal}</span></td>
					<td>锁定金额：<span>${buyBillRecAdvance.lockTotal}</span></td>
					<td>总金额：<span>${buyBillRecAdvance.advanceTotal}</span></td>
				</tr>--%>

			</table>
		</div>
		<!--收款凭据-->
		<div id="linkReceipt" class="receipt_content" style="width:500px;height:250px;display:none;">
			<div class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="icon_list"></ul>
			</div>
		</div>
	</div>
</div>
<%-- fileAddress
</div>--%>
