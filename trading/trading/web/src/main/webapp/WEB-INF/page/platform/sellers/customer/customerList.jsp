<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<script type="text/javascript">
	$(function(){
		var form; layui.use('form',function(){
			//重新渲染select控件
			form = layui.form;
			form.render("select");
		//搜索更多
        $(".search_more").click(function(){
            $(this).toggleClass("clicked");
            $(this).parent().nextAll("ul").toggle();
        });
        //设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#customContent #resetButton").click();
            var index = id.split("_")[1];
            $("#customContent input[name='tabId']").val(index);
			loadPlatformData();
        });
        //日期
		loadDate("startDate","endDate");
        //初始化数据
		selectData();
		$("#customContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
	});
	});
	function selectData(){
		var formObj = $("#customContent").find("form");
		$.ajax({
			url : "seller/customer/loadData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#tabContent").html(str);
				loadVerify();
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	
	// 展示售后凭证
	function showProof(proof){
		if(proof == null || proof == ""){
			layer.msg("未上传凭证！",{icon:7});
			return;
		}else{
			window.open("platform/buyer/customer/showProof?proof="+proof);  
		}
	}
	//卖家同意售后
	function agreeCustomer(customerId){
		layer.msg('确认同意售后？', {
			 time: 0,
		    btn: ['确认', '取消'],
		    yes: function(index){
				$.ajax({
					type : "POST",
					url : "seller/customer/saveAgreeCustomer",
					async: false,
					data : {
						"id" : customerId
					},
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if(resultObj.success){
							layer.msg("保存成功！",{icon:1});
							selectData();
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		    }
		  });
	}
	//卖家确认售后
	function confirmCustomer(obj){
		layer.msg('确认收货？', {
			 time: 0,
		    btn: ['确认', '取消'],
		    yes: function(index){
				var id = $(obj).parents("tr").find("td:eq(3)").find("input").val();
				var numError = "";
				var confirmStr = "";
				$(obj).parents("tr").find("td:eq(0)").find("ul").each(function(i){
					var receiveNumber = $(this).find("li:eq(4)").text();
					var confirmNumber =$(this).find("li:eq(5)").find("input[name='confirmNum']").val();
					if(confirmNumber == ""){
						numError="第"+(i+1)+"行请填写数量！";
						return false;
					}
					if(parseInt(confirmNumber) <= 0||parseInt(confirmNumber)>parseInt(receiveNumber)){
						numError="第"+(i+1)+"行数量必须大于0且小于到货数量！";
						return false;
					}
					var itemId = $(this).find("li:eq(5)").find("input:eq(1)").val();
					confirmStr = confirmStr + itemId + "," + confirmNumber + "@";
				});
				confirmStr = confirmStr.substring(0, confirmStr.length - 1);
				if(numError!=''){
					layer.msg(numError,{icon:7});
					return;
				}
				$.ajax({
					type : "POST",
					url : "seller/customer/saveVerifyCustomer",
					async: false,
					data : {
						"id" : id,
						"confirmStr" : confirmStr
					},
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if(resultObj.success){
							layer.msg("保存成功！",{icon:1});
							selectData();
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		    }
		});
	}
	//创建发货单
	function createDelivery(customerId){
		$.ajax({
			url : "seller/customer/loadCreateDeliveryHtml?customerId="+customerId,
			async:false,
			success:function(data){
				var str = data.toString();
				$(".content").html(str);
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_99">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_0">待同意（<span>${params.agreeNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_5">待收货（<span>${params.waitVerifyNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">已收货（<span>${params.passNum}</span>）</a> <b>|</b>
</div>
<div id="customContent">
	<!--搜索栏-->
	<form class="layui-form" action="seller/customer/customerList">
		<div class="search_top mt">
			<input type="text" placeholder="输入商品名称进行搜索" id="productName" name="productName" value="${params.productName}">
			<button id="selectButton">搜索</button>
			<span class="search_more sBuyer"></span>
		</div>
		<ul class="order_search seller_order">
            <li class="comm">
                <label>货号:</label>
                <input type="text" placeholder="输入货号进行搜索" id="productCode" name="productCode" style="width:110px" value="${params.productCode}">
            </li>
            <li class="comm">
                <label>规格代码:</label>
                <input type="text" placeholder="输入规格代码进行搜索" id="skuCode" name="skuCode" style="width:130px" value="${params.skuCode}">
            </li>
            <li class="comm">
                <label>规格名称:</label>
                <input type="text" placeholder="输入规格名称进行搜索" id="skuName" name="skuName" style="width:130px" value="${params.skuName}">
            </li>
            <li class="comm">
                <label>条形码:</label>
                <input type="text" placeholder="输入条形码进行搜索" id="skuOid" name="skuOid" style="width:120px" value="${params.skuOid}">
            </li>
            <li class="comm">
                <label>采购商商名称:</label>
                <input type="text" placeholder="输入采购商名称进行搜索" id="supplierName" name="supplierName" style="width:140px" value="${params.supplierName}">
            </li>
            <li class="comm">
                <label>编号:</label>
                <input type="text" placeholder="输入编号进行搜索" id="customerCode" name="customerCode" style="width:130px" value="${params.customerCode}">
            </li>
            <li class="range">
                <label>申请日期:</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="开始日" id="startDate" name="startDate" lay-verify="date" value="${params.startDate}">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="截止日" id="endDate" name="endDate" lay-verify="date" value="${params.endDate}">
                </div>
            </li>
            <li class="state nomargin">
                <label>售后类型:</label>
                <div class="layui-input-inline">
                    <select name="type" id="type" lay-filter="aihao">
                        <option value="" <c:if test="${params.type == ''}">selected</c:if>>全部</option>
                        <option value="0" <c:if test="${params.type == '0'}">selected</c:if>>换货</option>
                        <option value="1" <c:if test="${params.type == '1'}">selected</c:if>>退货</option>
                    </select>
                </div>
            </li>
            <li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
        </ul>
        <input type="hidden" name="interest" value="${params.interest}">
		<input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent"></div>
</div>
