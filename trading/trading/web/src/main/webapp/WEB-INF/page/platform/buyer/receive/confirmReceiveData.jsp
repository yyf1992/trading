<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<%--确认收货界面--%>
<script>
    function subData(){
        var deliveryId = "";
		//组装收货明细
        var deliveryItems = new Array();
        $("#productTab tr").each(function() {
            deliveryId = $(this).find("input[name='deliveryId']").val();
            var deliveryItemId = $(this).find("input[name='deliveryItemId']").val();
            var orderId = $(this).find("input[name='orderId']").val();
            var productCode = $(this).find("input[name='productCode']").val();
            var arrivalNum = $(this).find("input[name='arrivalNum']").val();
            var barcode = $(this).find("input[name='barcode']").val();
            var warehouseId = $(this).find("input[name='warehouseId']").val();
            var orderItemId = $(this).find("input[name='orderItemId']").val();
            var sellerOrderItemId = $(this).find("input[name='sellerOrderItemId']").val();
			var item = new Object();
			item.id = deliveryItemId;
			item.recordId = deliveryId;
			item.orderId = orderId;
			item.orderItemId = orderItemId;
			item.sellerOrderItemId = sellerOrderItemId;
			item.productCode = productCode;
			item.barcode = barcode;
			item.warehouseId = warehouseId;
			item.arrivalNum = arrivalNum;
            deliveryItems.push(item);
        });
        $.ajax({
            type : "post",
            url : "platform/buyer/receive/storageGoods",
            async: false,
            data : {
                "deliveryId":deliveryId,
                "deliveryItems" : JSON.stringify(deliveryItems)
            },
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("收货成功!",{icon:1});
                    leftMenuClick(this,"platform/buyer/receive/receiveList","buyer");
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error : function() {
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
	//返回收货列表
	function returnReceiveList() {
        leftMenuClick(this,"platform/buyer/receive/receiveList?${form}","buyer");
    }
</script>
<div>
	<!--列表区-->
	<form id="confirmReceiveForm" method="post" action="">
		<table class="order_detail">
			<tbody>
			<tr>
				<td style="width:30%">商品</td>
				<td style="width:10%">单位</td>
				<td style="width:10%">订单数量</td>
				<td style="width:10%">已到货数量</td>
				<td style="width:10%">本次发货数量</td>
				<td style="width:10%">本次到货数量</td>
				<td style="width:10%">仓库</td>
				<td style="width:10%">备注</td>
			</tr>
			</tbody>
		</table>
		<div class="deliver_list mt">
			<c:forEach var="receive" items="${receiveList}">
			<p>
				<span class="apply_t">订单日期: <fmt:formatDate value="${receive.orderDate}" type="both"></fmt:formatDate></span>
				<span class="order_n">订单号: <b>${receive.orderCode}</b></span>
				<span>供应商：${receive.supplierName}</span>
			</p>
			<table id="productTab">
				<c:forEach var="item" items="${receive.itemList}">
					<tr>
						<td style="width:30%">
							<img src="<%=basePath%>/statics/platform/images/defaulGoods.jpg">
							<div>${item.productCode} ${item.productName} <br>
								<span>规格代码: ${item.skuCode}</span><br>
								<span>规格名称: ${item.skuName}</span>
							</div>
						</td>
						<td style="width:10%">${item.unitName}</td>
						<td style="width:10%">
							<c:choose>
								<c:when test="${item.orderNum != null}">${item.orderNum}</c:when>
								<c:otherwise>
									${item.customerNum}
								</c:otherwise>
							</c:choose>
						</td>
						<td style="width:10%">${item.orderArrivalNum}</td>
						<td style="width:10%">${item.deliveryNum}</td>
						<td style="width:10%">${item.arrivalNum}
							<%--<input type="number" min="0" id="arrivalNum" name="arrivalNum" value="${item.arrivalNum}" />--%>
						</td>
						<td style="width:10%">${item.warehouseName}</td>
						<td style="width:10%">${item.remark}</td>
						<td style="width:0%">
							<input type="hidden" id="deliveryId" name="deliveryId" value="${item.recordId}" />
							<input type="hidden" id="deliveryItemId" name="deliveryItemId" value="${item.id}" />
							<input type="hidden" id="orderId" name="orderId" value="${item.orderId}" />
							<input type="hidden" id="productCode" name="productCode" value="${item.productCode}" />
							<input type="hidden" id="barcode" name="barcode" value="${item.barcode}" />
							<input type="hidden" id="warehouseId" name="warehouseId" value="${item.warehouseId}" />
							<input type="hidden" id="orderItemId" name="orderItemId" value="${item.orderItemId}" />
							<input type="hidden" id="sellerOrderItemId" name="sellerOrderItemId" value="${item.sellerOrderItemId}" />
						</td>
					</tr>
				</c:forEach>
			</table>
			</c:forEach>
		</div>
		<h4 class="page_title mp30">物流明细</h4>
		<div class="receive">
			<ul>
				<li>
					<label>运单号码：</label>${logistics.waybillNo}
				</li>
				<li>
					<label>物流公司：</label>${logistics.logisticsCompany}
				</li>
				<li>
					<label>司机姓名：</label>${logistics.driverName}
				</li>
				<li>
					<label>司机手机：</label>${logistics.mobilePhone}
				</li>
				<li>
					<label>商品运费：</label>${logistics.freight}
				</li>
				<li>
					<label>固定电话：</label>${logistics.zoneCode}
					<c:if test="${logistics.zoneCode != null && logistics.zoneCode != ''}">${logistics.zoneCode}-</c:if>
					<c:if test="${logistics.fixedPhone != null && logistics.fixedPhone != ''}">${logistics.fixedPhone}-</c:if>
					<c:if test="${logistics.extPhone != null && logistics.extPhone != ''}">${logistics.extPhone}</c:if>
				</li>
				<li>
					<label>预达日期：</label><fmt:formatDate value="${logistics.expectArrivalDate}" ></fmt:formatDate>
				</li>
			</ul>
		</div>
		<div class="btn_p text-center">
			<c:choose>
				<c:when test="${recordstatus == '0'  && isFromWms=='N'}">
					<%--<span class="order_p" onclick="subData();">确认收货</span>--%>
				</c:when>
				<c:otherwise></c:otherwise>
			</c:choose>
			<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/receive/receiveList?${form}','buyer');">
			<span class="order_p">返回</span></a>
<%-- 			&nbsp;&nbsp;<span class="order_p" onclick="leftMenuClick(this,'platform/buyer/receive/receiveList?from=${form}','buyer');">返回</span> --%>
		</div>

	</form>
</div>