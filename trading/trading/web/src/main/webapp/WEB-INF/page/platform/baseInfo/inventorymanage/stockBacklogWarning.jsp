<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<title>库存积压预警</title>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<script type="text/javascript">

function prodcutsDetails(obj){
	
	var parentDiv = $(obj).parent().parent();
	var id = parentDiv.find("#id").val();
	var barcode = parentDiv.find("#barcode").val();
	$.ajax({
		type: "POST",
		url: "platform/baseInfo/inventorymanage/backlogWarningDetails",
		data:{
			"id": id,
			"barcode":barcode
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

//列表数据查询
function loadBacklogWarningData() {
	if ($("#minBacklogdaynum").val() > $("#maxBacklogdaynum").val()){
		layer.tips('最大天数不得小于最小天数！', '#maxBacklogdaynum',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	if ($("#minDate").val() > $("#maxDate").val()){
		layer.tips('最大日期不得小于最小日期！', '#maxDate',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
    layer.load();
    var contentObj = $(".content");
    var formObj = contentObj.find("form");
    $.ajax({
        url : formObj.attr("action"),
        data:formObj.serialize(),
        async:false,
        success:function(data){
            layer.closeAll('loading');
            var str = data.toString();
            contentObj.html(str);
            layui.data();
        },
        error:function(){
            layer.closeAll('loading');
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//重置
function recovery(){

	$("#productCode").val("");
	$("#productName").val("");
	$("#skuCode").val("");
	$("#skuName").val("");
	$("#barcode").val("");
	$("#batchNo").val("");
	$("#minBacklogdaynum").val("");
	$("#maxBacklogdaynum").val("");
	$("#minDate").val("");
	$("#maxDate").val("");
	$("#businessType option:first").prop("selected","selected");
}

//导出
$("#warningExportButton").click(function(e){
	e.preventDefault();
	var formData = $("form").serialize();
	var url = "stockBacklogWarningDownload/stockBacklogWarningData?"+ formData;
	window.open(url);
});

$(function(){
	$("#businessType option[value='"+"${searchPageUtil.object.businessType}"+"']").attr("selected","selected");
});
layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //限定可选日期
	  var ins22 = laydate.render({
	    elem: '#minDate'
	    ,min: '1970-10-14'
	    ,max: -1
	    ,btns: ['clear']
	    ,ready: function(){
	  //ins22.hint('日期可选值设定在 <br> 今天之前');
	    }
	  });

	  var ins23 = laydate.render({
		    elem: '#maxDate'
		    ,min: '1970-10-14'
		    ,max: -1
		    ,btns: ['clear']
		    ,ready: function(){
		//ins22.hint('日期可选值设定在 <br> 今天之前');
		    }
	  });
	
});
</script>
<style>
.nomargin{
	position:relative;
	left:20px
}
.prodcutlog td{
 word-wrap:break-word;
 white-space:inherit;
 word-break: break-all;
} 
</style>
<div>
   <!--搜索栏-->
   <form class="searchForm" action="platform/baseInfo/inventorymanage/stockBacklogWarning">
         <ul class="order_search stock_list">
	         <li>
	           <label>商品货号</label>:
	           <input type="text" placeholder="输入商品货号" style="width:200px" id="productCode" name="productCode" value="${searchPageUtil.object.productCode}">
	         </li>
	         <li>
	           <label>商品名称</label>:
	           <input type="text" placeholder="输入商品名称" style="width:200px" id="productName" name="productName" value="${searchPageUtil.object.productName}">
	         </li>
	         <li>
           	   <label>规格代码</label>:
               <input type="text" placeholder="输入规格代码" style="width:200px" id="skuCode" name="skuCode" value="${searchPageUtil.object.skuCode}">
             </li>       
             <li>
           	   <label>规格名称</label>:
               <input type="text" placeholder="输入规格名称" style="width:200px" id="skuName" name="skuName" value="${searchPageUtil.object.skuName}">
             </li>	         
             <li>
	           <label>条形码</label>:
	           <input type="text" placeholder="输入条形码" style="width:200px" id="barcode" name="barcode" value="${searchPageUtil.object.barcode}">
	         </li>
	         <li>
	         <li>
	           <label>订单类型:</label>
	            <div class="layui-input-inline">              
		             <select id="businessType" name="businessType" style="width: 200px;">
		               <option value="" selected>全部</option>
		               <option value="29">采购系统入库</option>
		               <option value="21">OMS采购入库</option>
		               <option value="31">盘点入库</option>
		               <option value="SW">同步WMS入库</option>
		             </select>
	            </div>            
            </li>
            <li>
           	   <label>来源单号</label>:
               <input type="text" placeholder="输入来源单号" style="width:200px" id="batchNo" name="batchNo" value="${searchPageUtil.object.batchNo}">
            </li>	
            <li class="spec">
               <label>入库日期:</label>
               <div class="layui-input-inline">
                  <input type="text" name="minDate" id="minDate"  style="width:97px;" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">           
               </div>
               <div class="layui-input-inline">
                  <input type="text" name="maxDate" id="maxDate" style="width:97px;"  lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
               </div>
            </li>
            <li>
	           <label>积压天数:</label>
	           <input type="text" placeholder="开始" id="minBacklogdaynum" name="minBacklogdaynum" style="height: 25px; width: 97px;" value="${searchPageUtil.object.minBacklogdaynum}">	          
           	   <input type="text" placeholder="截止" id="maxBacklogdaynum" name="maxBacklogdaynum" style="height: 25px; width: 97px;" value="${searchPageUtil.object.maxBacklogdaynum}">	                     	   
             </li>
             <li>
               <button type="button" class="search" id="searchBtn" style="width:118px;" onclick="loadBacklogWarningData();">搜索</button>
      	       <button type="button" class="search" onclick="recovery();" style="width:118px;">重置</button>
             </li>
         </ul>
         <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
  	     <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
     </form> 
         <a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="warningExportButton">
			<i class="layui-icon">&#xe7a0;</i> 导出
		 </a>

  	    <!--列表区-->
	     <table class="table_pure mp backlog" id="backlogTable">
	       <thead>
	         <tr>
	           <td style="width:10%">商品货号</td>
	           <td style="width:18%">商品名称</td>
	           <td style="width:8%">规格代码</td>
	           <td style="width:8%">规格名称</td>
	           <td style="width:12%">条形码</td>
	           <td style="width:11%">订单类型</td>
		       <td style="width:13%">来源单号</td>
		       <td style="width:10%">入库日期</td>
		       <td style="width:9%">入库数量</td>
	           <td style="width:8%">积压库存</td>
	           <td style="width:10%">积压时间（天）</td>
	         </tr>
	       </thead>
	       <tbody id="backlog">
	       	<c:forEach var="prodcutsList" items="${searchPageUtil.page.list}" varStatus="status">
		      <tr class="prodcutlog">
		        <td title="${prodcutsList.productCode}">${prodcutsList.productCode}</td>
		        <td title="${prodcutsList.productName}">${prodcutsList.productName}</td>
		        <td title="${prodcutsList.skuCode}">${prodcutsList.skuCode}</td>
		        <td title="${prodcutsList.skuName}">${prodcutsList.skuName}</td>
		        <td title="${prodcutsList.barcode}">${prodcutsList.barcode}</td>
		        <td>
		   	    	<c:if test="${prodcutsList.businessType == '29'}">采购系统入库</c:if>
		   	    	<c:if test="${prodcutsList.businessType == '21'}">OMS采购入库</c:if>
		   	    	<c:if test="${prodcutsList.businessType == '31'}">盘点入库</c:if>
		   	    	<c:if test="${prodcutsList.businessType == 'SW'}">同步WMS入库</c:if>
		     	</td>
		        <td title="${prodcutsList.batchNo}">${prodcutsList.batchNo}</td>
		        <td><fmt:formatDate value="${prodcutsList.storageDate}" type="both"/></td>
		        <td>${prodcutsList.number}</td> 
		        <td>${prodcutsList.remainingStock}</td>
		        <td>
		        	<c:if test="${prodcutsList.backlogmaxday != null}">${prodcutsList.backlogmaxday}</c:if> 
		        </td>        
		      </tr>
		     </c:forEach>
	       </tbody>
	     </table>
 
     <div class="pager">${searchPageUtil.page}</div>

</div>