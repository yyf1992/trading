<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		/* var form = layui.form;
		form.render("select"); */
		//设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
		/* if("${params.tabId}"!='0'){
			$("#purchaseContent select[name='interest']").prop("disabled", true);
		} */
		//初始化数据
		selectData();
		$("#manualOrderContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
		$(".tab a").click(function(){
			var id = $(this).attr("id");
			var index = id.split("_")[1];
			$("#manualOrderContent input[name='tabId']").val(index);
			loadPlatformData();
		});
		//查看审核流程	
		$('.opinion_view').hover(
			function(){
				$(this).children('.opinion').toggle();
			}
		);
	});
	function selectData(){
		var formObj = $("#manualOrderContent").find("form");
		$.ajax({
			url : "buyer/manualOrder/loadData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#tabContent").html(str);
				loadVerify();
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	// 取消
	function cancelOrder(orderId){
		layer.confirm('您确定要取消订单吗?',{
			skin:'c_order',
			title:'提示'
	    },function(index){
			layer.close(index);
			$.ajax({
				type : "POST",
				url : "platform/buyer/buyOrder/saveCancelOrder",
				async: false,
				data : {
					"orderId" : orderId
				},
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if(resultObj.success){
						layer.msg("订单取消成功", {icon: 1},function(){
							leftMenuClick(this,"buyer/manualOrder/manualOrderList","buyer");
						});
					}else{
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
		});
	}
	// 审核
	function verify(id){
		if($(".verifyDetail").length>0)$(".verifyDetail").remove();
		$.ajax({
			url:"platform/tradeVerify/verifyDetailByRelatedId",
			data:{"id":id},
			success:function(data){
				var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
				detailDiv.addClass("verifyDetail");
				detailDiv.html(data);
				detailDiv.appendTo($("#manualOrderContent"));
				detailDiv.find("#verify_side").addClass("show");
			},
			error:function(){
				layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
			}
		});
	}
</script>
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有订单</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待内部审批（<span>${params.waitVerifyNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_2">已完成采购（<span>${params.overNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_3">已取消订单（<span>${params.cancelNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_4">已驳回订单（<span>${params.rejectNum}</span>）</a>
</div>
<div id="manualOrderContent">
	<!--搜索栏-->
	<form class="layui-form" action="buyer/manualOrder/manualOrderList" id="searchForm">
		<div class='search_top mt'>
			<input type='text' placeholder='输入商品名称或订单号进行搜索'>
			<button id="selectButton">搜索</button><span class='search_more sSeller'></span>
		</div>
		<input type="hidden" name="interest" value="${params.interest}">
		<input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent"></div>
</div>