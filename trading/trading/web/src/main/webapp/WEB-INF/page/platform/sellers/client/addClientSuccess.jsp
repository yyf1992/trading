<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ include file="../../common/common.jsp"%>
<html>
<head>
<script src="<%=basePath %>/statics/platform/js/addClient.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户添加成功</title>
</head>
<body>

	      <div class="content">
	        <div>
	          <div class="supplier_succ">
	            <img src="statics/platform/images/ok1.png">
	            <p>恭喜您的客户添加成功！</p>
	            <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/addClient','sellers')" class="layui-btn layui-btn-small layui-btn-danger">继续添加客户</a>
	            <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/clientList','sellers')" class="layui-btn layui-btn-small layui-btn-normal">转到客户列表页</a>
	          </div>
	        </div>
	      </div>
</body>
</html>
