<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>人员管理</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ include file="../../platform/common/path.jsp"%>
<%@ include file="../resources.jsp"%>
<script type="text/javascript">
$(function(){
	//全选
	$("#checkAll").change(function(){
		var status = $(this).is(":checked");
		if(status){
			$("input:checkbox[name='checkone']").prop("checked",status);
		}else{
			$("input:checkbox[name='checkone']").prop("checked",status);
		}
	});
});
//修改
function updateSysUser(obj){
	var checked = $("input:checkbox[name='checkone']:checked");
	if(checked.length != 1){
		layer.msg("请选择一条数据！",{icon:7});
		return;
	}
	$.ajax({
		url : basePath+"admin/adminSysUser/updateSysUser.html",
		data:{"id":checked.val()},
		success:function(data){
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteSysUser(obj){
	if($("input:checkbox[name='checkone']:checked").length < 1){
		layer.msg("请至少选择一条数据！",{icon:7});
		return;
	}
	layer.confirm('确认删除？删除后关联信息也会删除！', {
	  btn: ['确定','取消'] //按钮
	}, function(){
		var ids = "";
		$("input:checkbox[name='checkone']:checked").each(function(){
			ids += $(this).val() + ",";
		});
		$.ajax({
			url : basePath+"admin/adminSysUser/deleteSysUser.html",
			data:{"ids":ids},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					loadAdminData();
				} else {
					alert(resultObj.msg);
				}
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}, function(){
	});
}
//设置权限
function instalSysUserRole(obj){
	var checked = $("input:checkbox[name='checkone']:checked");
	if(checked.length != 1){
		layer.msg("请选择一条数据！",{icon:7});
		return;
	}
	$.ajax({
		url : basePath+"admin/adminSysUser/instalSysUserRole.html",
		data:{"id":checked.val()},
		success:function(data){
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
</head>
<body style="background-color: white;">
	<!--内容-->
<div id="content">
	<div class="container-fluid">
		<div class="row-fluid">
			<!--搜索条件 start-->
			<div class="form-serch">
				<div class="widget-box">
					<div class="widget-content" style="padding-bottom:5px">
						<form class="form-inline clearfix" id="selectForm"
							action="<%=basePath %>admin/adminSysUser/loadUserList.html?">
							<div class="form-group">
								<label for="exampleInputName2">公司：</label> <input type="text"
									class="form-control" id="company_name" name="company_name" placeholder="公司"
									value="${searchPageUtil.object.company_name}">
							</div>
							<div class="form-group">
								<label for="exampleInputName2">姓名：</label> <input type="text"
									class="form-control" id="user_name" name="user_name" placeholder="姓名"
									value="${searchPageUtil.object.user_name}">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-sm2 btn-success " id="selectButton"
									onclick="loadAdminData();">查询</button>
							</div>
							<!-- 分页隐藏数据 -->
							<input id="pageNo" name="pageAdmin.pageNo" type="hidden"
								value="${searchPageUtil.pageAdmin.pageNo}" /> <input id="pageSize"
								name="pageAdmin.pageSize" type="hidden"
								value="${searchPageUtil.pageAdmin.pageSize}" />
						</form>
					</div>
				</div>
			</div>
			<!--搜索条件 end-->
			<!--tableys start-->
			<div class="widget-box">
				<div class="widget-title title-lg">
					<span class="icon"> <i class="icon-th"></i> </span>
					<h5>对系统用户进行操作</h5>
					<div class="pull-left" style="margin-top: 8px;">
						<button class="btn btn-info" onclick="updateSysUser(this)">修改</button>
						<button class="btn btn-warning" onclick="deleteSysUser(this)">删除</button>
						<button class="btn btn-success" onclick="instalSysUserRole(this)">设置角色</button>
					</div>
				</div>
				<div class="widget-content nopadding" id="dataList">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
						role="grid">
						<table class="table table-bordered table-striped with-check">
							<thead>
								<tr>
									<th>
										<input type="checkbox" id="checkAll" name="title-table-checkbox" />
									</th>
									<th>登录名</th>
									<th>姓名</th>
									<th>是否主账号</th>
									<th>所属公司</th>
									<th>状态</th>
									<th>创建人</th>
									<th>创建日期</th>
									<th>最后一次修改人</th>
									<th>最后一次日期</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="sysUser" items="${searchPageUtil.pageAdmin.list}">
									<tr class="text-center">
										<td><input type="checkbox" name="checkone" value="${sysUser.id}" /></td>
										<td>${sysUser.login_name}</td>
										<td>${sysUser.user_name}</td>
										<td>
											<c:choose>
												<c:when test="${sysUser.is_parent==1}">主账号</c:when>
												<c:otherwise>子账号</c:otherwise>
											</c:choose>
										</td>
										<td>${sysUser.company_name}</td>
										<td>
											<c:choose>
												<c:when test="${sysUser.is_del==0}">正常</c:when>
												<c:otherwise>已删除</c:otherwise>
											</c:choose>
										</td>
										<td>${sysUser.create_name}</td>
										<td>${sysUser.create_date}</td>
										<td>${sysUser.update_name}</td>
										<td>${sysUser.update_date}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div id="page">${searchPageUtil.pageAdmin }</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal fade bs-example-modal-lg" id="modalDiv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document" id="modalDialogDiv"></div>
	</div>
</body>
</html>