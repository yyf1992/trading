<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<title>商品出入明细</title>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<script type="text/javascript">
//重置
function recovery(){
	leftMenuClick(this,'platform/baseInfo/inventorymanage/prodcutsRecording',"baseInfo");
}
$(function(){
	$("#shopId option[value='"+"${searchPageUtil.object.shopId}"+"']").attr("selected","selected");
	var supplierId = '${searchPageUtil.object.supplierId}';
	$("#supplierId").val(supplierId);
	$("#createName option[value='"+"${searchPageUtil.object.createName}"+"']").attr("selected","selected");
	$("#warehouseId option[value='"+"${searchPageUtil.object.warehouseId}"+"']").attr("selected","selected");
	$("#dropshipType option[value='"+"${searchPageUtil.object.dropshipType}"+"']").attr("selected","selected");
});
//导出
$("#recordingExportButton").click(function(e){
	e.preventDefault();
	var formData = $("form").serialize();
	var url = "recordingDownload/recordingData?"+ formData;
	window.open(url);
});
</script>
<style>
	.prodcutlog td{
	 word-wrap:break-word;
	 white-space:inherit;
	 word-break: break-all;
	}
</style>
<!--商品出入明细
<div class="tab">
   <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/baseInfo/inventorymanage/prodcutsInventory','baseInfo');" >商品库存余额表</a> <b>|</b>
   <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/baseInfo/inventorymanage/prodcutsRecording','baseInfo');" class="hover">商品出入明细</a> <b>|</b>
</div> -->
   <div>
     <form class="layui-form" action="platform/baseInfo/inventorymanage/prodcutsRecording">
       <ul class="order_search stock_list">
         <li>
           <label>商品货号</label>:
           <input type="text" placeholder="输入商品货号" style="width:163px;" id="productCode" name="productCode" value="${searchPageUtil.object.productCode}">
         </li>
         <li>
           <label>商品名称</label>:
           <input type="text" placeholder="输入商品名称" style="width:163px" id="productName" name="productName" value="${searchPageUtil.object.productName}">
         </li>
         <li>
           	<label>规格代码</label>:
            <input type="text" placeholder="输入规格代码" style="width:163px" id="skuCode" name="skuCode" value="${searchPageUtil.object.skuCode}">
         </li>       
         <li>
           	<label>规格名称</label>:
            <input type="text" placeholder="输入规格名称" style="width:163px" id="skuName" name="skuName" value="${searchPageUtil.object.skuName}">
         </li>
         <li>
           <label>条形码</label>:
           <input type="text" placeholder="输入条形码" style="width:163px" id="barcode" name="barcode" value="${searchPageUtil.object.barcode}">
         </li>
          <li>
           	<label>发货单号</label>:
            <input type="text" placeholder="输入采购单号" style="width:163px" id="batchNo" name="batchNo" value="${searchPageUtil.object.batchNo}">
         </li>
         <li>
           	<label>采购单号</label>:
            <input type="text" placeholder="输入采购单号" style="width:163px" id="orderCode" name="orderCode" value="${searchPageUtil.object.orderCode}">
         </li>
         
<!--          <li> -->
<!--            <label>部门名称:</label> -->
<!--            <input type="text" placeholder="输入店铺名称" id="shopName" name="shopName" style="width: 230px;">  -->
<!-- 			<div class="layui-input-inline"> -->
<!-- 				<select id="shopId" name="shopId" lay-verify="required" lay-search=""> -->
<!-- 					<option value="">直接选择或搜索选择</option> -->
<%-- 					<c:forEach var="shop" items="${shopList}"> --%>
<%-- 						<option value="${shop.id}">${shop.shopName}</option> --%>
<%-- 					</c:forEach> --%>
<!-- 				</select> -->
<!-- 			</div> -->
<!--          </li> -->
         <li class="spec">
           <label>价格区间:</label>
           <input placeholder="￥" type="number" style="width:73px" id="minPrice" name="minPrice" value="${searchPageUtil.object.minPrice}">
           -
           <input placeholder="￥" type="number" style="width:73px" id="maxPrice" name="maxPrice" value="${searchPageUtil.object.maxPrice}">
         </li>
         <li class="spec">
           <label>入库时间:</label>
           <div class="layui-input-inline">
             <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
             
           </div>
           <div class="layui-input-inline">
             <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
           </div>
         </li>
        
         <li>
           	<label>下单人</label>:
          <div class="layui-input-inline">
		         <select id="createName" name="createName" lay-verify="required" lay-search="">
					<option value="">直接选择或搜索选择</option>
					<c:forEach var="companyUser" items="${companyUserList}">
						<option value="${companyUser.userName}">${companyUser.userName}</option>
					</c:forEach>
				</select>
			</div>
         </li>
         <li>
	         <label>供应商:</label>&emsp;
	         <div class="layui-input-inline">
		         <select id="supplierId" name="supplierId" lay-verify="required" lay-search="">
					<option value="">直接选择或搜索选择</option>
					<c:forEach var="supplier" items="${supplierList}">
						<option value="${supplier.sellerId}">${supplier.sellerName}</option>
					</c:forEach>
				</select>
			</div>
         </li>
         <li>
	         <label>仓&emsp;&emsp;库:</label>
	         <div class="layui-input-inline">
		         <select id="warehouseId" name="warehouseId" lay-verify="required" lay-search="">
					<option value="">直接选择或搜索选择</option>
					<c:forEach var="wharea" items="${tBusinessWharea}">
						<option value="${wharea.id}">${wharea.whareaName}</option>
					</c:forEach>
				</select>
			</div>
         </li>
         <li>
	         <label>发货类型:</label>
	         <div class="layui-input-inline">
		         <select id="dropshipType" name="dropshipType" lay-verify="required" lay-search="">
		         	<option value="">所有</option>
					<option value="0">正常发货</option>
					<option value="1">代发客户</option>
					<option value="2">代发菜鸟</option>
					<option value="3">代发京东</option>
				</select>
			</div>
         </li>
         <li>
           	<label>入库单号</label>:
            <input type="text" placeholder="输入入库单号" style="width:163px" id="storageNo" name="storageNo" value="${searchPageUtil.object.storageNo}">
         </li>
          <li>
           	<label>采购计划单号</label>:
            <input type="text" placeholder="输入采购计划单号" style="width:163px" id="applyCode" name="applyCode" value="${searchPageUtil.object.applyCode}">
         </li>
         <li class="range rt">    
           <button class="search" style="width:118px;"  onclick="loadPlatformData();">搜索</button>
      	   <button type="button" class="search" onclick="recovery();">重置</button>
         </li> 
       </ul>
       <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
	   <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	 </form>
	 <a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="recordingExportButton">
		<i class="layui-icon">&#xe7a0;</i> 导出
     </a>
     <table class="table_pure mp backlog" style=" table-layout:fixed;">
       <thead>
         <tr>
<!--            <td style="width:8%">部门名称</td> -->
           <td style="width:8%">商品货号</td>
           <td style="width:7%">商品名称</td>
           <td style="width:8%">规格代码</td>
           <td style="width:7%">规格名称</td>
           <td style="width:8%">条形码</td>
           <td style="width:11%">发货单号</td>
           <td style="width:11%">采购单号</td>
           <td style="width:11%">采购计划单号</td>
           <td style="width:7%">供应商</td>
           <td style="width:7%">入库数量</td>          
           <td style="width:7%">采购金额</td>
           <td style="width:5%">入库名称</td>
           <td style="width:5%">发货类型</td>
           <td style="width:5%">下单人</td>
           <td style="width:6%">入库时间</td>
           <td style="width:6%">入库单号</td>
         </tr>
       </thead>
       <tbody>
            <c:forEach var="prodcutsList" items="${searchPageUtil.page.list}" varStatus="status">
		     <tr class="prodcutlog">
<%-- 		       <td title="${prodcutsList.shopName}">${prodcutsList.shopName}</td> --%>
		       <td title="${prodcutsList.productCode}">${prodcutsList.productCode}</td>
		       <td title="${prodcutsList.productName}">${prodcutsList.productName}</td>
		       <td title="${prodcutsList.skuCode}">${prodcutsList.skuCode}</td>
		       <td title="${prodcutsList.skuName}">${prodcutsList.skuName}</td>
		       <td title="${prodcutsList.barcode}">${prodcutsList.barcode}</td>
		       <td title="${prodcutsList.batchNo}">${prodcutsList.batchNo}</td>
		       <td title="${prodcutsList.orderCode}">${prodcutsList.orderCode}</td>
		       <td title="${prodcutsList.applyCode}">${prodcutsList.applyCode}</td>
			   <td title="${prodcutsList.suppName}">${prodcutsList.suppName}</td>
		       <td title="${prodcutsList.number}">${prodcutsList.number}</td>
		       <td title="${prodcutsList.price}">${prodcutsList.price}</td>
		       <td title="${prodcutsList.whareaName}">${prodcutsList.whareaName}</td>
		       <td title="${prodcutsList.dropshipType}">
			       <c:if test="${prodcutsList.dropshipType == 0}">正常发货</c:if>
			       <c:if test="${prodcutsList.dropshipType == 1}">代发客户</c:if>
			       <c:if test="${prodcutsList.dropshipType == 2}">代发菜鸟</c:if>
			       <c:if test="${prodcutsList.dropshipType == 3}">代发京东</c:if>
		       </td>
		       <td title="${prodcutsList.createName}">${prodcutsList.createName}</td>
		       <td title="${prodcutsList.storageDate}"><fmt:formatDate value="${prodcutsList.storageDate}" type="both"/></td>
		       <td title="${prodcutsList.storageNo}">${prodcutsList.storageNo}</td>
		     </tr>
		  </c:forEach>
       </tbody>
     </table>
     <div class="pager">${searchPageUtil.page}</div>
     
</div>