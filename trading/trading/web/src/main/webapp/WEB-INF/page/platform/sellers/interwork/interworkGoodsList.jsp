<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function(){
        //搜索更多
        $(".search_more").click(function(){
            $(this).toggleClass("clicked");
            $(this).parent().nextAll("ul").toggle();
        });
        //设置选择的tab
        $(".tab>a").removeClass("hover");
        $(".tab #tab_${params.tabId}").addClass("hover");

        //日期
//        loadDate("startDate","endDate");
        //初始化数据
        selectData();
        $("#orderContent #selectButton").click(function(e){
            e.preventDefault();
            selectData();
        });
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#orderContent #resetButton").click();
            var index = id.split("_")[1];
            $("#orderContent input[name='tabId']").val(index);
            loadPlatformData('content');
        });
    });
    function selectData(){
        var formObj = $("#orderContent").find("form");
        $.ajax({
            url : basePath+"platform/seller/interworkGoods/interworkGoodsData.html",
            data:formObj.serialize(),
            async:false,
            success:function(data){
                $("#tabContent").empty();
                var str = data.toString();
                $("#tabContent").html(str);
                $(".tab>a").removeClass("hover");
                $(".tab #tab_"+$("#interest").val()).addClass("hover");
                loadVerify();
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
    /**
     * 导出
     */
    function exportOrder() {
        var formData = $("#orderListForm").serialize();
        var url = basePath+"platform/seller/interworkGoods/exportOrder?"+ formData;
        window.open(url);
    }
    /**
     * 未到货订单导出
     */
    function exportNotArrivalOrder() {
        var formData = $("#orderListForm").serialize();
        var url = basePath+"platform/seller/interworkGoods/exportNotArrivalOrder?"+formData;
        window.open(url);
    }
    /**
     * 生成BOM表
     */
    function exportBOMOrder() {
        var formData = $("#orderListForm").serialize();
        var url = basePath+"platform/seller/order/exportBOMOrder?"+ formData;
        window.open(url);
    }
</script>
<!--页签-->
<div class="tab">
    <a href="javascript:void(0)" id="tab_99">所有</a> <b>|</b>
    <a href="javascript:void(0)" id="tab_0">待接单（<span>${params.waitOrderNum}</span>）</a> <b>|</b>
    <a href="javascript:void(0)" id="tab_1">待发货（<span>${params.waitDeliveryNum}</span>）</a><b>|</b>
    <a href="javascript:void(0)" id="tab_2">发货中（<span>${params.deliveringNum}</span>）</a><b>|</b>
    <a href="javascript:void(0)" id="tab_3">发货完成（<span>${params.deliveredNum}</span>）</a><b>|</b>
    <a href="javascript:void(0)" id="tab_4">已收货（<span>${params.receivedNum}</span>）</a><b>|</b>
    <%--<a href="javascript:void(0)" id="tab_5">交易完成（<span>${params.shipNum}</span>）</a><b>|</b>--%>
    <%--<a href="javascript:void(0)" id="tab_6">已取消（<span>${params.cancelNum}</span>）</a><b>|</b>--%>
    <a href="javascript:void(0)" id="tab_7">已驳回（<span>${params.rejectNum}</span>）</a> <b>|</b>
    <a href="javascript:void(0)" id="tab_8">已终止（<span>${params.stopNum}</span>）</a>
</div>
<div id="orderContent">
    <!--搜索栏-->
    <form class="layui-form" action="platform/seller/interworkGoods/interworkGoodsList.html" id="orderListForm">
        <ul class="order_search seller_order">
            <li class="comm">
                <label>客户名称:</label>
                <input type="text" placeholder="输入客户名称进行搜索" id="companyName" name="companyName"  value="${params.companyName}">
            </li>
            <%--<li class="state">--%>
                <%--<label>规格:</label>--%>
                <%--<input type="text" placeholder="输入规格搜索" style="width:200px" id="skucode" name="skucode" value="${params.skucode}">--%>
            <%--</li>--%>
            <li class="state nomargin">
                <label>交易状态:</label>
                <div class="layui-input-inline">
                    <select name="interest" id="interest" lay-filter="aihao">
                        <option value="99" <c:if test="${params.interest==99}">selected</c:if>>全部</option>
                        <option value="0" <c:if test="${params.interest==0}">selected</c:if>>待接单</option>
                        <option value="1" <c:if test="${params.interest==1}">selected</c:if>>待发货</option>
                        <option value="2" <c:if test="${params.interest==2}">selected</c:if>>发货中</option>
                        <option value="3" <c:if test="${params.interest==3}">selected</c:if>>发货完成</option>
                        <option value="4" <c:if test="${params.interest==4}">selected</c:if>>已收货</option>
                        <%--<option value="5" <c:if test="${params.interest==5}">selected</c:if>>交易完成</option>--%>
                        <%--<option value="6" <c:if test="${params.interest==6}">selected</c:if>>已取消订单</option>--%>
                        <option value="7" <c:if test="${params.interest==7}">selected</c:if>>已驳回订单</option>
                        <option value="8" <c:if test="${params.interest==8}">selected</c:if>>已终止</option>
                    </select>
                </div>
            </li>
            <li class="range">
                <label>订单时间:</label>
                <div class="layui-input-inline">
                    <input type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
                </div>-
                <div class="layui-input-inline">
                    <input type="text" name="endDate" id="endDate" lay-verify="date" value="${params.endDate}" class="layui-input" placeholder="截止日">
                </div>
            </li>
            <li class="comm">
                <label>&nbsp;&nbsp;订单号:</label>
                <div class="layui-input-inline">
                    <%--<select name="sendGoodType" lay-filter="aihao">--%>
                        <%--<option value="0" selected>全部</option>--%>
                        <%--<option value="1">全部发货</option>--%>
                        <%--<option value="2">部分发货</option>--%>
                    <%--</select>--%>
                        <input type="text" placeholder="订单号" id="orderCode" name="orderCode" value="${params.orderCode}">
                </div>
            </li>
            <li class="range"><button type="button" onclick="selectData()"  class="search">搜索</button></li>
            <li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
        </ul>
        <input type="hidden" name="tabId" value="${params.tabId}">
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
        <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
    </form>
    <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="exportBOMOrder();">
        <i class="layui-icon">&#xe8bf;</i> 生成BOM表
    </a>
    <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" style="margin-right: 10px;" onclick="exportOrder();">
        <i class="layui-icon">&#xe8bf;</i> 导出所有
    </a>
    <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" style="margin-right: 10px;" onclick="exportNotArrivalOrder();">
        <i class="layui-icon">&#xe8bf;</i> 导出未到货
    </a>
    <div id="tabContent"></div>
</div>