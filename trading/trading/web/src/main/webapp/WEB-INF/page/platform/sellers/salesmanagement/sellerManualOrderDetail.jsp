<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<title>审批时查看的详情页</title>
<script type="text/javascript">
$(function(){
	if ('${orderBean.status}'<6){
		 $(".ystep1").loadStep({
		        size: "large",
		        color: "blue",
		        steps: [{
		            title: "内部审批"
		        },{
		            title: "待发货"
		        },{
		            title: "发货中"
		        },{
		            title: "发货完成"
		        },{
		        	title: "已收货"
		        },{
		            title: "交易完成"
		        }]
		    });
		 
		 var step = parseInt('${orderBean.status}')+1;
		 $(".ystep1").setStep(step);
	}
   
});
</script>
<div class="content">
  <div class="order_top mt">
    <div class="lf order_status" style="width: 20%">
      <h4 class="mt">当前订单状态</h4>
      <p class="mt text-center">
        <span class="order_state">
          <c:choose>
            <c:when test="${orderBean.status==0}">内部审批</c:when>
			<c:when test="${orderBean.status==1}">待发货</c:when>
			<c:when test="${orderBean.status==2}">发货中</c:when>
			<c:when test="${orderBean.status==3}">发货完成</c:when>
			<c:when test="${orderBean.status==4}">已收货</c:when>
			<c:when test="${orderBean.status==5}">交易完成</c:when>
			<c:when test="${orderBean.status==6}">已取消</c:when>
			<c:when test="${orderBean.status==7}">已驳回订单</c:when>
            <c:otherwise>
              	内部审批
            </c:otherwise>
          </c:choose>
        </span>
      </p>
      <p class="order_remarks text-center"></p>
    </div>
    <div class="lf order_progress">
      <div class="ystep1"></div>
    </div>
  </div>
  <%--<div class="info_l mt">--%>
    <%--<h4 class="logisticsInfo">物流信息</h4>--%>
    <%--<table class="table_info">--%>
      <%--<tr>--%>
        <%--<td>物流公司：<span>韵达快递</span></td>--%>
        <%--<td>运单号码：<span>3926790793301</span></td>--%>
        <%--<td>司机：<span>张XXX</span></td>--%>
      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td>手机号：<span>187XXXX73008</span></td>--%>
        <%--<td>固定电话：<span>0531-897xxxxx</span></td>--%>
        <%--<td>预计到达日期：<span>2017-04-25</span></td>--%>
      <%--</tr>--%>
    <%--</table>--%>
  <%--</div>--%>
  <div>
    <h4 class="mt">订单信息</h4>
    <div class="order_d">
      <p><span class="order_title">采购商信息</span></p>
      <table class="table_info">
        <tr>
          <td>采购商名称：<span>${orderBean.suppName}</span></td>
          <td>负责人：<span>${orderBean.personName}</span></td>
          <td>手机号：<span>${orderBean.receiptPhone}</span></td>
        </tr>
        <tr>
          <td colspan="3">收货地址：${orderBean.personName},${orderBean.receiptPhone},${orderBean.addrName}</td>
        </tr>
        <%--<tr>--%>
          <%--<td>运送方式：快递</td>--%>
        <%--</tr>--%>
      </table>
      <p class="line mt"></p>
      <p><span class="order_title">供应商信息</span></p>
      <table class="table_info">
        <tr>
          <td>供应商名称：<span>${buyerBean.companyName}</span></td>
          <td>负责人：<span>${buyerBean.linkMan}</span></td>
          <td>手机号：<span>${buyerBean.zone}-${buyerBean.telNo}</span></td>
        </tr>
      </table>
      <p class="line mt"></p>
      <p><span class="order_title">订单信息</span></p>
      <table class="table_info">
         <tr>
          <td>订单号：<span>${orderBean.orderCode}</span></td>
          <td>创建时间：<span><fmt:formatDate value="${orderBean.createDate}" ></fmt:formatDate></span></td>
          <td>审批时间：<span>2017-04-25  09：55</span></td>
        </tr>
        <tr>
          <td>发货时间：<span><fmt:formatDate value="${sellerDeliveryRecord.sendDate}" ></fmt:formatDate></span></td>
          <td>收货时间：<span>${sellerDeliveryRecord.arrivalDate}</span></td>
          <td>收款日期：<span><fmt:formatDate value="${sellerManualOrder.paymentDate}" ></fmt:formatDate></span></td>
        </tr>
        <tr>
          <td>收款方式：
          	<span>
          		<c:choose>
					<c:when test="${sellerManualOrder.paymentType==0}">银行卡转账</c:when>
					<c:when test="${sellerManualOrder.paymentType==1}">现金</c:when>
					<c:when test="${sellerManualOrder.paymentType==2}">网银支付</c:when>
				</c:choose></span>
          </td>
          <td>收款人：<span>${sellerManualOrder.paymentPerson}</span></td>
          <td>经办人：<span>${sellerManualOrder.handler}</span></td>
        </tr>
      </table>
      <div class="c66 size_sm orderNote">备注：${orderBean.remark}</div>
      <table class="table_pure detailed_list_s mt">
        <thead>
        <tr>
          <td style="width:20%">商品</td>
          <td style="width:10%">条形码</td>
          <td style="width:5%">单位</td>
          <td style="width:10%">仓库</td>
          <td style="width:15%">备注</td>
          <td style="width:10%">销售单价</td>
          <td style="width:10%">数量</td>
          <td style="width:10%">优惠金额</td>
          <td style="width:10%">商品总额</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="product" items="${orderBean.supplierProductList}">
          <tr>
            <td>${product.productName}</td>
            <td>${product.barcode}</td>
            <td>${product.unitName}</td>
            <td>大仓</td>
            <td>${product.remark}</td>
            <td>${product.price}</td>
            <td>${product.orderNum}</td>
            <td>${product.discountMoney}</td>
            <td>${product.totalMoney}</td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
    <div class="totalOrder">
      <ul class="submit_order seller">
        <li>
          <label>商品总额:</label>
          <span>¥ ${sellerManualOrder.purchasePrice}</span>
        </li>
        <li>
          <label>其他费用:</label>
          <span>¥ ${sellerManualOrder.otherFee}</span>
        </li>
        <li>
          <label>订单总金额:</label>
          <span class="orange">¥ ${sellerManualOrder.purchasePrice}</span>
        </li>
        <li>
          <label>已收款金额:</label>
          <span class="orange">¥ ${sellerManualOrder.purchasePrice}</span>
        </li>
      </ul>
    </div>
    <div>
      <p>附件：</p>
      <div class="voucherImg orderVoucher">
			<c:if test="${sellerManualOrder.annexVoucher != null && sellerManualOrder.annexVoucher != ''}">
				<c:forEach var="annexVoucher" items="${sellerManualOrder.annexVoucher.split(',')}">
					<span>
						<img src="${annexVoucher}" onMouseOver="toolTip('<img src=${annexVoucher}>')" onMouseOut="toolTip()">
					</span>
				</c:forEach>
			</c:if>
	  </div>
    </div>
  </div>
</div>
