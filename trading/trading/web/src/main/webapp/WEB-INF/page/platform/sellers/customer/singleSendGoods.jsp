<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<script>
	layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //执行一个laydate实例
	  laydate.render({
	    elem: '#expectArrivalDate' //指定元素
	  });
	});
    function subData(){
		//组装发货明细
        var deliveryItems = new Array();
        //发货数量判断
		var checkDeliveryNum = false;
        $("#detail_interworkGoods tr").each(function() {
            var orderItemId = $(this).find("input[name='orderItemId']").val();
            var applyCode = $(this).find("input[name='applyCode']").val();
            var productCode = $(this).find("input[name='productCode']").val();
            var productName = $(this).find("input[name='productName']").val();
            var skuCode = $(this).find("input[name='skuCode']").val();
            var skuName = $(this).find("input[name='skuName']").val();
            var barcode = $(this).find("input[name='barcode']").val();
            var unitId = $(this).find("input[name='unitId']").val();
            var salePrice = $(this).find("input[name='price']").val();
            var warehouseId = $(this).find("input[name='warehouseId']").val();
            var warehouseCode = "";
            var warehouseName = "";
            var deliveryNum = $(this).find("input[name='deliveryNum']").val();
            var deliveredNum = $(this).find("input[name='deliveredNum']").val();
            var isNeedInvoice = $(this).find("input[name='isNeedInvoice']").val();
            var status = $(this).find("input[name='status']").val();
            var remark = $(this).find("input[name='remark']").val();
            var buyOrderItemId = $(this).find("input[name='buyOrderItemId']").val();
			var item = new Object();
			item.orderId = '${sellerCustomer.id}';
			item.orderItemId = orderItemId;
			item.applyCode = applyCode;
			item.buyOrderId = '${sellerCustomer.buyCustomerId}';
            item.buyOrderItemId = buyOrderItemId;
			item.productCode = productCode;
			item.productName = productName;
			item.unitId = unitId;
			item.skuCode = skuCode;
			item.skuName = skuName;
			item.barcode = barcode;
			item.salePrice = salePrice;
			item.warehouseId = warehouseId;
			item.warehouseCode = warehouseCode;
			item.warehouseName = warehouseName;
			item.deliveryNum = deliveryNum;
			item.deliveredNum = deliveredNum;
			item.status = status;
			item.isNeedInvoice = isNeedInvoice;
			item.remark = remark;
            deliveryItems.push(item);
            if(deliveryNum<=0){
                checkDeliveryNum = true;
                return;
            }
        });
        if(deliveryItems.length<=0){
            layer.msg("发货明细不能为空！",{icon:2});
            return;
        }
        if(checkDeliveryNum){
            layer.msg("发货数量不能为0！",{icon:2});
            return;
        }
        var waybillNo = $("#waybillNo").val();
        /*if(waybillNo == ''){
            layer.msg("请输入运单号码！",{icon:2});
            return;
        }*/
        var logisticsCompany = $("#logisticsCompany").val();
        /*if(logisticsCompany == ''){
		 layer.msg("请输入物流公司名称！",{icon:2});
		 return;
		 }*/
        var driverName = $("#driverName").val();
        /*if(driverName == ''){
            layer.msg("请输入司机姓名！",{icon:2});
            return;
        }*/
        var mobilePhone = $("#mobilePhone").val();
        var zoneCode = $("#zoneCode").val();
        var fixedPhone = $("#fixedPhone").val();
        var extPhone = $("#extPhone").val();
        /*if(mobilePhone == ' '&& fixedPhone== ''){
            layer.msg("请输入手机号或固话号码！",{icon:2});
            return;
        }*/
        var freight = $("#freight").val();
        /*if(freight == ''){
            layer.msg("请输入商品运费！",{icon:2});
            return;
        }*/
        var expectArrivalDate = $("#expectArrivalDate").val();
		if(expectArrivalDate == ''){
            layer.msg("请选择预达日期！",{icon:2});
            return;
        }
        //组装物流信息
        var logistics = new Object();
        logistics.waybillNo = waybillNo;
        logistics.logisticsCompany = logisticsCompany;
        logistics.driverName = driverName;
        logistics.mobilePhone = mobilePhone;
        logistics.zoneCode = zoneCode;
        logistics.fixedPhone = fixedPhone;
        logistics.extPhone = extPhone;
        logistics.freight = parseFloat(freight);
        logistics.expectArrivalDate = expectArrivalDate;
        var deliverType = $("#deliverType").val();
        var lodIndex;
        $.ajax({
            type : "post",
            url : "platform/seller/delivery/sendGoodsSub",
            data : {
                "orderId" : '${sellerCustomer.id}',
                "logistics" : JSON.stringify(logistics),
                "deliveryItems" : JSON.stringify(deliveryItems),
                "buyCompanyId":'${sellerCustomer.companyId}',
                "deliverType":'1',
                "dropshipType":$("#dropshipType").val(),//代发仓库（0.正常发货1.代发客户2.代发菜鸟仓3.代发京东仓）
                "receiveAddr":$("#receiveAddr").val()
            },
            beforeSend: function () {
                lodIndex = layer.load(1);
            },
            complete: function () {
                layer.close(lodIndex);
            },
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("保存成功!",{icon:1});
                    if(deliverType == "0"){
                    	leftMenuClick(this,'platform/seller/interworkGoods/interworkGoodsList.html','sellers','17070718471195397330');
                    }else{
                    	leftMenuClick(this,'seller/customer/customerList','sellers','17092016324036228398');
                    }
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error : function() {
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }

    //赋值所属仓库
    function getWarehouse(id,name){
        $("#warehouseId").val(id);
        $("#warehouseName").val(name);
    }
    //发货数量输入框改变事件
	function setDeliveryNum(obj,maxNum) {
        var deliveryNum = $(obj).val();
        if(deliveryNum<0){
            $(obj).val(maxNum);
            deliveryNum = maxNum;
		}
		if(maxNum-deliveryNum<0){
            $(obj).val(maxNum);
            deliveryNum = maxNum;
		}
		if(deliveryNum-maxNum==0){
            $(obj).parent().parent().find("[name='status_info']").val("0");
            $(obj).parent().parent().find("[name='status']").val("0");
		}else {
            $(obj).parent().parent().find("[name='status_info']").val("1");
            $(obj).parent().parent().find("[name='status']").val("1");
		}
    }
	//删除行
	function delRow(obj) {
        layer.confirm('确定删除该行？', {
            icon:3,
            title:'提醒',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(){
            //判断该订单下的明细有多少条，如果只有1条，则删除时把订单信息一块删除
            var orderItems = $(obj).closest("table").find("tr");
            if(orderItems.length==1){
                $(obj).closest("table").prev().remove();
                $(obj).closest("table").remove();
            }else {
                $(obj).parent().parent().remove();
            }
            layer.msg('删除成功',{time:200});
        }.bind(obj));

    }
</script>
<div>
	<!--列表区-->
	<form id="mySingleForm" name="mySingleForm" method="post" action="">
		<input type="hidden" id="deliverType" name="deliverType" value="1"/><%-- 1:换货订单 --%>
		<table class="order_detail">
			<tbody>
			<tr>
				<td style="width:25%">商品</td>
				<td style="width:10%">条形码</td>
				<td style="width:10%">数量</td>
				<td style="width:10%">已发货数量</td>
				<td style="width:10%">待发货数量</td>
				<td style="width:10%">本次发货数量</td>
				<td style="width:10%">备注</td>
				<td style="width:5%">操作</td>
				<td style="display: none;">存放隐藏字段</td>
			</tr>
			</tbody>
		</table>
		<div class="deliver_list mt" id="detail_interworkGoods">
			<p>
				<span class="apply_t"><fmt:formatDate value="${product.createDate}" type="both"></fmt:formatDate></span>
				<span class="order_n">订单号: <b>${sellerCustomer.customerCode}</b></span>
				<span>采购商：<b>${el:getCompanyById(sellerCustomer.companyId).companyName}</b></span>
			</p>
			<table>
				<c:forEach var="product" items="${itemList}">
					<c:if test="${product.type==0 }">
					<tr>
						<td style="width:25%">
							${product.productCode}|${product.productName}|${product.skuCode}|${product.skuName}
							<%-- <div>${product.productCode}|${product.productName}|${product.skuCode}|${product.skuName}
								<span>规格代码: ${product.skuCode}</span><br>
								<span>规格名称: ${product.skuName}</span>
							</div> --%>
						</td>
						<td style="width:10%">${product.skuOid}</td>
						<td style="width:10%">${product.confirmNumber}</td>
						<td style="width:10%">${product.deliverNum}</td>
						<td style="width:10%">${product.confirmNumber-product.deliverNum}</td>
						<td style="width:10%">
							<input type="number" min="0" max="${product.confirmNumber-product.deliverNum}" id="deliveryNum" name="deliveryNum" value="${product.confirmNumber-product.deliverNum}" onchange="setDeliveryNum(this,${product.confirmNumber-product.deliverNum});"/>
						</td>
						<td style="width:10%">
							<input type="text" id="remark" name="remark" style="width: 100%;height: 25px;"/>
						</td>
						<td style="width:5%">
							<a href="javascript:void(0);" onclick="delRow(this);" class="layui-btn layui-btn-primary layui-btn-mini"><i class="layui-icon">&#xe7ea;</i>删除</a>
						</td>
						<td style="display: none;">
							<input type="hidden" id="orderItemId" name="orderItemId" value="${product.id}" />
							<input type="hidden" id="applyCode" name="applyCode" value="${sellerCustomer.customerCode}" />
							<input type="hidden" id="productCode" name="productCode" value="${product.productCode}" />
							<input type="hidden" id="productName" name="productName" value="${product.productName}" />
							<input type="hidden" id="skuCode" name="skuCode" value="${product.skuCode}" />
							<input type="hidden" id="skuName" name="skuName" value="${product.skuName}" />
							<input type="hidden" id="barCode" name="barcode" value="${product.skuOid}" />
							<input type="hidden" id="price" name="price" value="${product.price}" />
							<input type="hidden" id="barCode" name="barcode" value="${product.skuOid}" />
							<input type="hidden" id="buyOrderItemId" name="buyOrderItemId" value="${product.buyCustomerItemId}" />
						</td>
					</tr>
					</c:if>
				</c:forEach>
			</table>
		</div>
		<div style="padding:10px; border: 1px solid #e5e5e5;">
			<ul class="layui-row" style="overflow: hidden">
				<li class="layui-col-sm3">
					<label>发货类型：</label>
					<select name="dropshipType" id="dropshipType" lay-filter="aihao" >
						<option value="0">正常发货</option>
						<option value="1">代发客户</option>
						<option value="2">代发菜鸟仓</option>
						<option value="3">代发京东仓</option>
					</select>
				</li>
				<li  class="layui-col-sm5">
					<label class="layui-col-sm3 text-right" style="position: relative;top: 5px;">收货地址：</label>
					<div class="layui-col-sm9">
						<input type="text" id="receiveAddr" name="receiveAddr" disabled="disabled" value="${el:getProvinceById(sellerCustomer.province).province} ${el:getCityById(sellerCustomer.city).city}${el:getAreaById(sellerCustomer.area).area} ${sellerCustomer.addrName}（${sellerCustomer.linkman} 收）${sellerCustomer.phone}" class="layui-input">
					</div>

				</li>
			</ul>
		</div>
		<h4 class="page_title mp30">物流明细</h4>
		<div class="receive">
			<ul>
				<li>
					<label>运单号码：</label>
					<input type="text" required placeholder="输入运单号码" id="waybillNo" name="waybillNo" />
				</li>
				<li>
					<label>物流公司：</label>
					<input type="text" required placeholder="输入物流公司名称" id="logisticsCompany" name="logisticsCompany" />
				</li>

				<li>
					<label>司机姓名：</label>
					<input type="text" required placeholder="输入司机姓名" id="driverName" name="driverName" />
				</li>
				<li>
					<label>司机手机：</label>
					<input type="tel" required placeholder="手机与固定电话至少填一个" id="mobilePhone" name="mobilePhone" />
				</li>
				<li>
					<label>商品运费：</label>
					<input type="number" min="0" required placeholder="输入商品运费" id="freight" name="freight" />元
				</li>
				<li>
					<label>固定电话：</label>
					<input type="tel" placeholder="区号" style="width:55px" 
						id="zoneCode" name="zoneCode" />—<input type="tel" placeholder="电话号码"
						 style="width:103px" id="fixedPhone" name="fixedPhone" />—<input type="tel" 
						 placeholder="分机号" style="width:55px" id="extPhone" name="extPhone" />
				</li>
				<li>
					<label>预达日期：</label>
					<div class="layui-input-inline">
						<input type="text" name="expectArrivalDate" id="expectArrivalDate" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
					</div>
				</li>
			</ul>
		</div>
		<div class="btn_p text-center">
			<span class="order_p" onclick="subData();">保存</span>
		</div>
	</form>
</div>