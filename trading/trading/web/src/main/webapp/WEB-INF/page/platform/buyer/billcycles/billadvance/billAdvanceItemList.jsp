<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的收款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/billAdvanceItemList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载条件
        $("#id").val("${params.id}");
        $("#advanceId").val("${params.advanceId}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startEditDate}");
        $("#endDate").val("${params.endEditDate}");
        $("#acceptStatus").val("${params.acceptStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select");
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    /*var form;
    layui.use('form', function() {
        form = layui.form;
        form.render("select");
        //下单人下拉
        loadAdvanceInfo();
    });*/

    //预付款单据
    var preview={
        LIWIDTH:108,//保存每个li的宽
        $ul:null,//保存小图片列表的ul
        moved:0,//保存左移过的li
        init:function(){//初始化功能
            this.$ul=$(".view>ul");//查找ul
            $(".view>a").click(function(e){//为两个按钮绑定单击事件

				debugger
                e.preventDefault();
                if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
                    if($(e.target).is(".forward")){//如果是向前按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
                        this.moved++;//移动个数加1
                    }
                    else{//如果是向后按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
                        this.moved--;//移动个数减1
                    }
                    this.checkA();//每次移动完后，调用该方法
                }
            }.bind(this));
            //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
            this.$ul.on("mouseover","li>img",function(){
                var src=$(this).attr("src");//获得当前img的src
                //var i=src.lastIndexOf(".");//找到.的位置
                //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
                $(".big_img>img").attr("src",src);//设置中图片的src
            });
        },
        checkA:function(){//检查a的状态
            if(this.moved==0){//如果没有移动
                $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
            }
            /*else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
                $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
            }*/
            else{//否则，都启用
                $("[class^=backward]").attr("class","backward");
                $("[class^=forward]").attr("class","forward");
            }
        }
    };
    preview.init();

    //标签页改变
    function setStatus(obj,status) {
        $("#acceptStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
    //重置查询条件
    function resetformData(){
       // alert("进来了吗");
        $("#id").val("");
        $("#advanceId").val("");
        $("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#acceptStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.acceptStatus eq '0'}">class="hover"</c:if>>所有（<span>${params.allEditCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.acceptStatus eq '1'}">class="hover"</c:if>>内部审批中（<span>${params.waitEditCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.acceptStatus eq '2'}">class="hover"</c:if>>内部审批通过（<span>${params.adoptEditCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.acceptStatus eq '3'}">class="hover"</c:if>>内部审批驳回（<span>${params.rejectEditCount}</span>）</a> <b>|</b>
</div>
<!--内容-->
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billAdvanceItem/billAdvanceItemList">
		<ul class="order_search">
			<li class="comm">
				<label>充值单号:</label>
				<input type="text" placeholder="请输入充值编号" id="id" name="id" >
			</li>
			<li class="comm">
				<label>付款账号:</label>
				<input type="text" placeholder="请输入付款账号" id="advanceId" name="advanceId" >
			</li>
			<li class="comm">
				<label>供应商名称:</label>
				<input type="text" placeholder="请输入供应商名称" id="sellerCompanyName" name="sellerCompanyName" >
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="range nomargin">
				<label>充值日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startEditDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endEditDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range nomargin">
				<label>充值审批状态:</label>
				<div class="layui-input-inline">
					<select name="acceptStatus" id="acceptStatus" lay-filter="aihao">
						<option value="0" >所有</option>
						<option value="1" >内部审批中</option>
						<option value="2" >内部审批通过</option>
						<option value="3" >内部审批驳回</option>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />


			<div class="rt">
				<%--<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="showAdvanceRecharge('','1');">
					<i class="layui-icon">&#xebaa;</i>预付款充值
				</a>--%>
				<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="leftMenuClick(this,'platform/buyer/billAdvanceItem/openChargeAdvance','buyer')">
					<i class="layui-icon">&#xebaa;</i>充值
				</a>
				<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small" onclick="buyerAdvanceExport();">
					<i class="layui-icon">&#xe8bf;</i> 导出
				</a>

			</div>

		</ul>
	</form>
    <table class="table_pure payment_list">
	  <thead>
	  <tr>
		  <td style="width:15%">充值单号</td>
		  <td style="width:10%">预付款账号</td>
		  <td style="width:10%">供应商</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">充值金额</td>
		  <td style="width:10%">申请时间</td>
		  <td style="width:8%">申请人</td>
		  <td style="width:8%">充值状态</td>
		  <td style="width:8%">审批状态</td>
		  <td style="width:10%">备注</td>
		  <td style="width:10%">操作</td>
	  </tr>
	  </thead>
	  <tbody>
	    <c:forEach var="billAdvanceEdit" items="${searchPageUtil.page.list}">
		  <tr class="breakType">
			  <input id="advanceIdOld" type="hidden" value="${billAdvanceEdit.advanceId}">
			  <input id="updateTotalOld" type="hidden" value="${billAdvanceEdit.updateTotal}">
			  <input id="fileAddressOld" type="hidden" value="${billAdvanceEdit.fileAddress}">
			  <input id="advanceRemarksOld" type="hidden" value="${billAdvanceEdit.advanceRemarks}">
			  <td style="text-align: center" title="${billAdvanceEdit.id}">
					  ${billAdvanceEdit.id}</td>
			  <td>${billAdvanceEdit.advanceId}</td>
			  <td style="text-align: center" title="${billAdvanceEdit.sellerCompanyName}">
			    ${billAdvanceEdit.sellerCompanyName}</td>
			  <td>${billAdvanceEdit.createBillUserName}</td>
			  <td style="text-align: center">${billAdvanceEdit.updateTotal}</td>
			  <td>${billAdvanceEdit.createTimeStr}</td>
			  <td>${billAdvanceEdit.createUserName}</td>

			  <td style="text-align: center">
				  <c:choose>
					  <c:when test="${billAdvanceEdit.editStatus==1}">充值中</c:when>
					  <c:when test="${billAdvanceEdit.editStatus==2}">成功</c:when>
					  <c:when test="${billAdvanceEdit.editStatus==3}">失败</c:when>
				  </c:choose>
			  </td>
			  <td style="text-align: center">
				  <div>
					  <c:choose>
						  <c:when test="${billAdvanceEdit.editStatus==1}">内部审批中</c:when>
						  <c:when test="${billAdvanceEdit.editStatus==2}">内部审批通过</c:when>
						  <c:when test="${billAdvanceEdit.editStatus==3}">内部审批驳回</c:when>
					  </c:choose>
				  </div>
				  <div class="opinion_view">
					  <span class="orange" data="${billAdvanceEdit.id}">审批流程</span>
				  </div>
			  </td>
			  <td>${billAdvanceEdit.advanceRemarks}</td>
			  <td>
				<span class="layui-btn layui-btn-mini" onclick="showAdvanceEditFile('${billAdvanceEdit.fileAddress}');">充值凭证</span>
			    <%--<div>
				  <c:choose>
					  <c:when test="${billAdvanceEdit.editStatus==3}">
						  <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="leftMenuClick(this,'platform/buyer/billAdvanceItem/updateChargeAdvance?advanceEditId=${billAdvanceEdit.id}','buyer')">驳回修改</span>
					  </c:when>
				  </c:choose>
			    </div>--%>
			  </td>
		  </tr>
	    </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
</div>

<!--预付款充值-->
<div class="plain_frame price_change" id="rechargeDiv" style="display: none">
	<%--<ul id="rechargeBody">
	</ul>--%>
			<div class="layui-inline">
				<label class="labelStyle">预付款账号:</label>
				<div class="layui-input-inline" style="width:200px" id="advanceAccount">
				</div>
			</div>
			<div class="bill_cycle">
                <label class="labelStyle">充值金额:</label>
                <div class="layui-input-inline">
                    <input type="number" min="0" name="chargeTotal" id="chargeTotal" value="${params.updateTotal}" style="width: 200px;height: 30px;" class="layui-input" placeholder="预付款金额">
                </div>
            </div>
            <div class="bill_cycle layui-form-item">
                <label class="labelStyle">充值凭证:</label>
                <%--<div class="upload_license" style='width: 150px;height: 20px'>--%>
                    <input type="file" multiple name="rechargeFileAddr" id="rechargeFileAddr" onchange="uploadPayment('add');" style="width: 75px;height: 23px;" >
               <%-- </div>--%>
                <p></p>
                <span class="labelStyle" style="white-space: nowrap">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
                <div class="scanning_copy original" id="billChargeFileDiv"></div>
            </div>
			<div class="bill_cycle">
				<label class="labelStyle">备注:</label>
				<textarea type="text" id="taskRemarks" name="taskRemarks" style="width: 300px;height: 100px;" class="layui-input"></textarea>
			</div>
</div>

<!--修改驳回充值-->
<div class="plain_frame price_change" id="updateRechargeDiv" style="display: none">
	<%--<ul id="rechargeBody">
	</ul>--%>
	<div class="layui-inline">
		<label class="labelStyle">预付款账号:</label>
		<div class="layui-input-inline">
			<input type="text" id="updateAdvanceId" value="${billAdvanceEdit.advanceId}">
		</div>
	</div>
	<div class="bill_cycle">
		<label class="labelStyle">充值金额:</label>
		<div class="layui-input-inline">
			<input type="number" min="0" name="updateChargeTotal" id="updateChargeTotal" style="width: 200px;height: 30px;" class="layui-input" value="${billAdvanceEdit.updateTotal}">
		</div>
	</div>
	<div class="bill_cycle layui-form-item">
		<label class="labelStyle">充值凭证:</label>
		<input type="file" multiple name="updateFileAddr" id="updateFileAddr" onchange="uploadPayment('update');" style="width: 75px;height: 23px;" >
		<p></p>
		<span class="labelStyle" style="white-space: nowrap">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
		<div class="scanning_copy original" id="updateChargeFileDiv"></div>
	</div>
	<div class="bill_cycle">
		<label class="labelStyle">备注:</label>
		<textarea type="text" id="updateRemarks" name="updateRemarks" style="width: 300px;height: 100px;" class="layui-input">
			${billAdvanceEdit.advanceRemarks}
		</textarea>
	</div>
</div>

<!--充值票据-->
<div id="advanceEditDiv" class="receipt_content layui-layer-wrap" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<style>
	.view .icon_list {
		height: 92px;
		position:absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
	.view .icon_list li img:hover {
		border: 2px solid #e4393c;
		padding: 0;
	}
</style>