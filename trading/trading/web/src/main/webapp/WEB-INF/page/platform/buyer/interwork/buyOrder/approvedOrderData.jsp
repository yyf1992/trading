<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<table class="order_detail">
	<tr>
		<td style="width:80%">
			<ul>
				<li style="width:5%"><input type="checkbox" name="checkAll" onclick="selectAll(this);"></li>
				<li style="width:25%">商品</li>
				<li style="width:10%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">采购数量</li>
				<li style="width:10%">单价</li>
				<li style="width:10%">总价</li>
				<li style="width:15%">备注</li>
				<li style="width:10%">要求到货日期</li>
			</ul>
		</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list" onclick="verify('${orders.id}');">
		<p>
			<span class="layui-col-sm3">下单日期:<b><fmt:formatDate
						value="${orders.createDate}" type="both"></fmt:formatDate>
			</b>
			</span> <span class="layui-col-sm3">采购单号:<b>${orders.orderCode}</b>
			</span> <span class="layui-col-sm3">供应商:<b>${orders.suppName}</b>
			</span> <span class="layui-col-sm1">下单人:<b>${orders.createName}</b>
			</span>
		</p>
		<table>
			<tr>
				<td style="width:80%"><c:forEach var="product"
						items="${orders.orderProductList}">
						<ul class="clear">
							<li style="width:5%;margin-top: 23px;"><input type="checkbox" name="checkone" value="${orders.id}"></li>
							<li style="width:25%"><span class="defaultImg"></span>
								<div style="text-align: left;">
									${product.proCode}<br />${product.proName} <br /> <span>规格代码:${product.skuCode}</span><br />
									<span>规格名称:${product.skuName}</span>
								</div>
							</li>
							<li style="width:10%">${product.skuOid}</li>
							<li style="width:5%">${product.unitName}</li>
							<li style="width:10%">${product.goodsNumber}</li>
							<li style="width:10%;text-align: center;">${product.price}</li>
							<li style="width:10%">${product.goodsNumber*product.price}</li>
							<li style="width:15%" title="${product.remark}">${product.remark}</li>
							<li style="width:10%">
								<fmt:formatDate value="${product.predictArred}"/>
							</li>
						</ul>
					</c:forEach></td>
				<td style="width:10%" class="operate">
					<a href="javascript:void(0)"
						onclick="leftMenuClick(this,'platform/buyer/buyOrder/approvedDataDetail?orderCode=${orders.orderCode}'+
									'&returnUrl=platform/buyer/buyOrder/approvedHtml&menuId=18020113531134956472','buyer');"
						class="layui-btn layui-btn-normal layui-btn-mini"> 
						<i class="layui-icon">&#xe695;</i>详情 </a> 
					<div class="opinion_view">
						<span class="orange" data="${orders.id}">查看审批流程</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>