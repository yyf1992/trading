<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<link rel="stylesheet" href="${basePath}statics/platform/css/supplier.css">
<script type="text/javascript">
	$(function() {
		//设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
		//搜索更多
        $(".search_more").click(function(){
            $(this).toggleClass("clicked");
            $(this).parent().nextAll("ul").toggle();
        });
		//初始化数据
		selectData();
		$("#purchaseContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
		//日期
		//loadDate("startDate","endDate");
		$(".tab a").click(function(){
			var id = $(this).attr("id");
			var index = id.split("_")[1];
			$("#purchaseContent input[name='tabId']").val(index);
			loadPlatformData();
		});
	});
	function selectData(){
		var formObj = $("#purchaseContent").find("form");
		$.ajax({
			url : basePath+"buyer/applyPurchaseHeader/loadData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#tabContent").html(str);
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
		loadVerify();
	}
	// 审核
	function verify(id){
		if($(".verifyDetail").length>0)$(".verifyDetail").remove();
		$.ajax({
			url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
			data:{"id":id},
			success:function(data){
				var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
				detailDiv.addClass("verifyDetail");
				detailDiv.html(data);
				detailDiv.appendTo($("#purchaseContent"));
				detailDiv.find("#verify_side").addClass("show");
			},
			error:function(){
				layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
			}
		});
	}
	//导出
	$("#applyPurchaseExportButton").click(function(e){
		e.preventDefault();
		var formData = $("form").serialize();
		var url = "download/applyPurchaseListStatistic?" + formData;
		window.open(url);
	});
	//取消订单
    function cancelPurchase(id) {
        layer.confirm('确认取消订单?', {
            icon:3,
            title:'提示',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            $.ajax({
                type : "POST",
                url : "buyer/applyPurchaseHeader/saveCancelPurchase",
                async: false,
                data : {
                    "purchaseId" : id
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("操作成功", {icon: 1},function(){
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });
    }
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待审批（<span>${params.waitVerifyNum }</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_2">审批已通过（<span>${params.verifyPassNum }</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_3">审批未通过（<span>${params.verifyNoPassNum }</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_4">已取消（<span>${params.cancelNum }</span>）</a> <b>|</b>
</div>
<div id="purchaseContent">
	<!--搜索栏-->
	<form class="layui-form" action="buyer/applyPurchaseHeader/applyPurchaseList" >
		<input type="hidden" name="interest" value="0">
		<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="applyPurchaseExportButton">
		<i class="layui-icon">&#xe7a0;</i> 导出</a>
		<ul class="order_search invoice_search">
			<li class="comm">
				<label>采购计划单号:</label>
				<input type="text" placeholder="输入采购计划单号进行搜索" style="width:160px" name="applyCode" id="applyCode" value="${params.applyCode}">
			</li>
			<li class="comm">
				<label>销售计划单号:</label>
				<input type="text" placeholder="输入销售计划单号进行搜索" style="width:160px" name="planCode" id="planCode" value="${params.planCode}">
			</li>
			<li class="comm">
				<label>货号:</label>
				<input type="text" placeholder="输入货号进行搜索" style="width:110px" name="procode" id="procode" value="${params.procode}">
			</li>
			<li class="comm">
				<label>条形码:</label>
				<input type="text" placeholder="输入条形码进行搜索" style="width:120px" name="skuoid" id="skuoid" value="${params.skuoid}">
			</li>
			<li class="range nomargin">
				<label>申请人:</label>
				<input type="text" placeholder="输入申请人进行搜索" style="width:120px" name="createName" id="createName" value="${params.createName}">
			</li>
			<li class="range"><label>下单日期:</label>
				<div class="layui-input-inline">
					<input style="width:80px" type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
				</div> -
				<div class="layui-input-inline">
					<input style="width:80px" type="text" name="endDate" id="endDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range">
				<button type="button" class="search" id="selectButton">搜索</button>
				<button type="reset" id="resetButton" class="search">重置查询条件</button>
			</li>
		</ul>
		<input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent"></div>
</div>
