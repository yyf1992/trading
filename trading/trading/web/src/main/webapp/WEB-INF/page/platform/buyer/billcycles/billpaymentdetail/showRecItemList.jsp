<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<%@ include file="../../../common/path.jsp"%>--%>
<%--<%@ include file="../../../common/common.jsp"%>--%>
<head>
	<title>账单对账列表</title>
	<%--<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/reconciliationDetail.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>--%>
	<script type="text/javascript">
        //标签页改变
        function setStatus(obj,status) {
            $("#billDealStatus").val(status);

            $('.tab a').removeClass("hover");
            $(obj).addClass("hover");
            loadPlatformData();
        }
        //返回收货列表
        function returnReceiveList() {
            leftMenuClick(this,"platform/buyer/billPaymentDetail/billPaymentDetailList","buyer");
        }
	</script>
</head>
<!--内容-->
<!--页签 -->
<div class="print_m">
	<div id="print_yc">
	<%--<h3 class="print_title">供应商对账账单</h3>--%>
	<h3 class="print_title">供应商：${recItemMap.sellerCompanyName} ${recItemMap.startBillStatementDateStr}至${recItemMap.endBillstatementDateStr}的账单明细</h3>
	<%--<table class="table_n">
		<tr>
			<td>供应商：<span>${recItemMap.sellerCompanyName}</span></td>
			<td>出账周期：<span>${recItemMap.startBillStatementDateStr}-${recItemMap.endBillstatementDateStr}</span>的账单明细</td>
		</tr>
	</table>--%>

		<!--列表区-->
	<table id="reconciliationData" class="table_pure payment_list" >
		<thead>
		<tr>
			<%--<td style="width:10%">完成日期</td>--%>
			<td style="width:12%">发货单号</td>
			<td style="width:7%">商品名称</td>
			<td style="width:6%">商品规格</td>
			<td style="width:3%">商品单位</td>
			<td style="width:7%">条形码</td>
			<td style="width:6%">商品单价</td>
			<td style="width:3%">商品数量</td>
			<td style="width:6%">商品金额</td>

			<td style="width:12%">订单号</td>
			<td style="width:6%">物流单号</td>
			<td style="width:6%">运费</td>
			<td style="width:6%">票据号</td>
			<td style="width:3%">票据类型</td>
			<td style="width:6%">票据金额</td>
			<%--<td id="oprationRe" style="width:9%">操作</td>--%>
		</tr>
		</thead>
		<tbody>

		<tr>
			<c:forEach var="buyBillReconciliation" items="${recItemMap.recordItemList}">
				<tr class="text-center">
				   <%--<td>${buyBillReconciliation.arrivalDateStr}</td>--%>
				   <td title="${buyBillReconciliation.orderCode}">${buyBillReconciliation.orderCode}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.productName}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.skuName}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.unitName}</td>
				   <td title="${buyBillReconciliation.buyDeliveryRecordItem.barcode}">${buyBillReconciliation.buyDeliveryRecordItem.barcode}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.salePrice}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.arrivalNum}</td>
				   <td>${buyBillReconciliation.salePriceSum}</td>

				   <td title="${buyBillReconciliation.deliverNo}">${buyBillReconciliation.deliverNo}</td>
				   <td title="${buyBillReconciliation.waybillNo}">${buyBillReconciliation.waybillNo}</td>
				   <td title="${buyBillReconciliation.buyDeliveryRecordItem.freight}">${buyBillReconciliation.buyDeliveryRecordItem.freight}</td>
				   <td title="${buyBillReconciliation.invoiceNo}">${buyBillReconciliation.invoiceNo}</td>
				   <td>
					   <c:choose>
						   <c:when test="${buyBillReconciliation.invoiceType == 1}">专用发票</c:when>
						   <c:when test="${buyBillReconciliation.invoiceType == 2}">普通发票</c:when>
						   <c:when test="${buyBillReconciliation.invoiceType == 3}">其他发票</c:when>
					   </c:choose>
				   </td>
				   <td title="${buyBillReconciliation.invoiceTotal}">${buyBillReconciliation.invoiceTotal}</td>
					<%--<td id="oprationRe1">
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${buyBillReconciliation.receiptvoucherAddr}');">查看收据</span>
					</td>--%>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<c:forEach var="recordReplaceItemList" items="${recItemMap.recordReplaceItemList}">
				<tr class="text-center">
					<%--<td>${recordReplaceItemList.arrivalDateStr}</td>--%>
					<td title="${recordReplaceItemList.deliverNo}">${recordReplaceItemList.deliverNo}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.productName}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.skuName}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.unitName}</td>
					<td title="${recordReplaceItemList.buyRecordReplaceItem.barcode}">${recordReplaceItemList.buyRecordReplaceItem.barcode}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.salePrice}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.arrivalNum}</td>
					<td>${recordReplaceItemList.replacePriceSum}</td>

					<td title="${recordReplaceItemList.orderCode}">${recordReplaceItemList.orderCode}</td>
					<td title="${recordReplaceItemList.waybillNo}">${recordReplaceItemList.waybillNo}</td>
					<td title="${recordReplaceItemList.buyRecordReplaceItem.freight}">${recordReplaceItemList.buyRecordReplaceItem.freight}</td>
					<td title="${recordReplaceItemList.invoiceNo}">${recordReplaceItemList.invoiceNo}</td>
					<td>
					  <c:choose>
					    <c:when test="${recordReplaceItemList.invoiceType == 1}">专用发票</c:when>
					    <c:when test="${recordReplaceItemList.invoiceType == 2}">普通发票</c:when>
					    <c:when test="${recordReplaceItemList.invoiceType == 3}">其他发票</c:when>
					  </c:choose>
					</td>
					<td title="${recordReplaceItemList.invoiceTotal}">${recordReplaceItemList.invoiceTotal}</td>
					<%--<td id="oprationRe2">
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${recordReplaceItemList.receiptvoucherAddr}');">查看收据</span>
					</td>--%>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<c:forEach var="customerItemList" items="${recItemMap.customerItemList}">
				<tr class="text-center">
					<%--<td>${customerItemList.verifyDateStr}</td>--%>
					<td title="${customerItemList.customerCode}">${customerItemList.customerCode}</td>
					<td>${customerItemList.buyCustomerItem.productName}</td>
					<td>${customerItemList.buyCustomerItem.skuName}</td>
					<td>${customerItemList.buyCustomerItem.unitName}</td>
					<td title="${customerItemList.buyCustomerItem.barcode}">${customerItemList.buyCustomerItem.barcode}</td>
					<td>${customerItemList.buyCustomerItem.salePrice}</td>
					<td>${customerItemList.buyCustomerItem.arrivalNum}</td>
					<td>${customerItemList.goodsPriceSum}</td>

					<td title="${customerItemList.orderCode}">${customerItemList.orderCode}</td>
					<td title="${customerItemList.waybillNo}">${customerItemList.waybillNo}</td>
					<td>${customerItemList.buyCustomerItem.freight}</td>
					<td title="${customerItemList.invoiceNo}">${customerItemList.invoiceNo}</td>
					<td>
						<c:choose>
							<c:when test="${customerItemList.invoiceType == 1}">专用发票</c:when>
							<c:when test="${customerItemList.invoiceType == 2}">普通发票</c:when>
							<c:when test="${customerItemList.invoiceType == 3}">其他发票</c:when>
						</c:choose>
					</td>
					<td title="${customerItemList.invoiceTotal}">${customerItemList.invoiceTotal}</td>
					<%--<td id="oprationRe3">
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItemList.proof}');">查看收据</span>
					</td>--%>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<td>合计</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>${recItemMap.totalSumCount}</td>
			<td>${recItemMap.totalAmount}</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td title="${recItemMap.invoiceTotalSum}">${recItemMap.invoiceTotalSum}</td>
			<%--<td id="oprationRe4"></td>--%>
		</tr>
		</tbody>
	</table>
	</div>
	<%--<div class="print_b">
		<input type="hidden" id="reconciliationId" name="reconciliationId" value="${recordAndReturnMap.reconciliationId}">
		<span onclick="updatePrice();">确认提交</span>
		<span id="print_sure">打印账单</span>
	</div>--%>
	<div class="btn_p text-center">
		<span class="order_p" onclick="returnReceiveList();">返回</span>
	</div>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<img id="bigImg">
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list">
			<%--<li>
				<img src="${basePath}statics/platform/images/01.jpg">
			</li>
			<li>
				<img src="${basePath}statics/platform/images/01.png">
			</li>
			<li>
				<img src="${basePath}statics/platform/images/2-2-01.png">
			</li>
			<li>
				<img src="${basePath}statics/platform/images/2-2-02.png">
			</li>
			<li>
				<img src="${basePath}statics/platform/images/2-2-03.png">
			</li>
			<li>
				<img src="${basePath}statics/platform/images/2-2-04.png">
			</li>--%>
		</ul>
	</div>
</div>

<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $("#oprationRe").hide();
        $("#oprationRe1").hide();
        $("#oprationRe2").hide();
        $("#oprationRe3").hide();
        $("#oprationRe4").hide();
        $("#print_yc").jqprint();
        $("#oprationRe").show();
        $("#oprationRe1").show();
        $("#oprationRe2").show();
        $("#oprationRe3").show();
        $("#oprationRe4").show();
    });
</script>