<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script> 
<script src="<%=basePath%>statics/platform/js/echarts.min.js"></script>
<script type="text/javascript" src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<script type="text/javascript">
var QualificatonRate = <%=request.getAttribute("map")%>;
var xArr = [];
var exchangeArr=[];
var returnArr=[];
var totalArr=[];
$.each(QualificatonRate,function(i,data){
	xArr.push(data.supplier_name);
	exchangeArr.push(data.exchangeNum);
	returnArr.push(data.tuiHuoNum);
	totalArr.push(data.arrivalNum);
});

 // 基于准备好的dom，初始化echarts实例
 var columnEcharts = echarts.init(document.getElementById('echartsColumn'));

 // 指定图表的配置项和数据
 var columnOption = {
 	    title : {
 	        text: '供应商到货合格率统计',
 	        subtext: '买卖系统'
 	    },
 	    tooltip : {
 	        trigger: 'axis'
 	    },
 	    legend: {
 	        data:['返修数量','退货数量','到货数量']
 	    },
 	    toolbox: {
 	        show : true,
 	        feature : {
 	            //dataView : {show: true, readOnly: false},		//数据视图
 	            magicType : {show: true, type: ['line', 'bar','stack','tiled']},
 	            restore : {show: true},
 	            saveAsImage : {show: true}
 	        }
 	    },
 	   	grid: {  
	  		bottom:'70'
	  	}, 
 	    calculable : true,
 	    xAxis : [
 	        {
 	            type : 'category',
 	            axisLabel :{  // 解决X轴显示不全
 	              interval:'auto', //0强制显示所有标签，如果设置为1，表示隔一个标签显示一个标签，如果为3，表示隔3个标签显示一个标签
				  formatter : function(value) {
						var ret = "";//拼接加\n返回的类目项  
						var maxLength = 6;//每项显示文字个数  
						var valLength = value.length;//X轴类目项的文字个数  
						var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
						if (rowN > 1)//如果类目项的文字大于6,  
						{
							for ( var i = 0; i < rowN; i++) {
								var temp = "";//每次截取的字符串  
								var start = i * maxLength;//开始截取的位置  
								var end = start + maxLength;//结束截取的位置  
								//这里也可以加一个是否是最后一行的判断，但是不加也没有影响，那就不加吧  
								temp = value.substring(start, end) + "\n";
								ret += temp; //凭借最终的字符串  
							}
							return ret;
						} else {
							return value;
						}
					}
				},
				data : []
			}],
			yAxis : [{
				type : 'value',
				axisLabel : {
					formatter : "{value} 次"
				}
			}],
			series : [{
				name : '返修数量',
				type : 'bar',
				data : [],
				markPoint : {
					data : [{
						type : 'max',
						name : '最大值'
					}, {
						type : 'min',
						name : '最小值'
					}]
				},
				markLine : {
					data : [{
						type : 'average',
						name : '平均值'
					}]
				}
			}, {
			name : '退货数量',
			type : 'bar',
			data : [],
			markPoint : {
				data : [{
					name : '年最高',
					value : 182.2,
					xAxis : 7,
					yAxis : 183
				}, {
					name : '年最低',
					value : 2.3,
					xAxis : 11,
					yAxis : 3
				}]
			},
			markLine : {
				data : [{
					type : 'average',
					name : '平均值'
				}]
			}
		}, {
			name : '到货数量',
			type : 'bar',
			data : [],
			markPoint : {
				data : [{
					name : '年最高',
					value : 182.2,
					xAxis : 7,
					yAxis : 183
				}, {
					name : '年最低',
					value : 2.3,
					xAxis : 11,
					yAxis : 3
				}]
			},
			markLine : {
				data : [{
					type : 'average',
					name : '平均值'
				}]
			}
		}]
	};
	
	columnOption.xAxis[0].data = xArr;
	columnOption.series[0].data = exchangeArr;
	columnOption.series[1].data = returnArr;
	columnOption.series[2].data = totalArr;
	// 使用刚指定的配置项和数据显示图表。
	columnEcharts.setOption(columnOption);
</script>
<div>
	<form action="platform/buyer/supplier/loadSupplierQualificationRateHtml">
		<ul class="order_search">
			<li class="state"><label>起始时间：</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" value="${amap.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
					-
				<div class="layui-input-inline">
					<input type="text" name="endDate" value="${amap.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
			</li>
			<li><label>供应商名称：</label> 
				<input type="text" placeholder="输入供应商名称" name="suppName" value="${amap.suppName}">
			</li>
			<li><label>商品名称：</label> 
				<input type="text" placeholder="输入商品名称" name="productName" value="${amap.productName}">
			</li>
			<li><label>货号：</label> 
				<input type="text" placeholder="输入货号" name="productCode" value="${amap.productCode}">
			</li>
			<li><label>条形码：</label> 
				<input type="text" placeholder="输入条形码" name="barcode" value="${amap.barcode}">
			</li>
			<li><label>规格名称：</label> 
				<input type="text" placeholder="输入规格名称" name="skuName" value="${amap.skuName}">
			</li>
			<li><label>规格代码：</label> 
				<input type="text" placeholder="输入规格代码" name="skuCode" value="${amap.skuCode}">
			</li>
			<li><label>创建人：</label> 
				<input type="text" placeholder="输入创建人" name="createrName" value="${amap.createrName}">
			</li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="exportData('supplierQualitityTable','供应商到货合格率');">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
		</ul>
	</form>
	<table class="table_pure supplierList" id="supplierQualitityTable">
		<thead>
			<tr>
				<td style="width:15%">供应商名称</td>
				<td style="width:20%">返修数量</td>
				<td style="width:20%">退货数量</td>
				<td style="width:20%">到货数量</td>
				<td style="width:20%">合格率</td>
			</tr>
		</thead>
		<tbody>
			<c:set var="exchangeTotal" value="0" scope="page"></c:set>
			<c:set var="tuiHuoNumTotal" value="0" scope="page"></c:set>
			<c:set var="arrivalTotal" value="0" scope="page"></c:set>
			<c:forEach var="item" items="${map}" varStatus="status">
				<c:set var="exchangeTotal" value="${item.value.exchangeNum + exchangeTotal }" scope="page"></c:set>
				<c:set var="tuiHuoNumTotal" value="${item.value.tuiHuoNum + tuiHuoNumTotal }" scope="page"></c:set>
				<c:set var="arrivalTotal" value="${item.value.arrivalNum + arrivalTotal }" scope="page"></c:set>
				<c:choose>
					<c:when test="${item.value.exchangeNum==0 && item.value.tuiHuoNum==0 && item.value.arrivalNum ==0}"></c:when>
				<c:otherwise>
				<tr>
					<td>${item.value.supplier_name}</td>
					<td>${item.value.exchangeNum}</td>
					<td>${item.value.tuiHuoNum}</td>
					<td>${item.value.arrivalNum}</td>
					<td><fmt:formatNumber value="${item.value.rate}" pattern="#.##"/>%</td>
				</tr>
				</c:otherwise>
				</c:choose>
			</c:forEach>
			<tr style="color: blue">
					<td>合计：</td>
					<td>${exchangeTotal}</td>
					<td>${tuiHuoNumTotal}</td>
					<td>${arrivalTotal}</td>
					<td>
						<c:choose>
							<c:when test="${Total==0 }">
								0%
							</c:when>
							<c:otherwise>
								<script>
									var successTotal = parseFloat("${arrivalTotal-exchangeTotal-tuiHuoNumTotal}");
									var total = parseFloat("${arrivalTotal}");
									$("#rateTotal").html(Math.round((successTotal*100/total)*100)/100);
								</script>
								<span id="rateTotal"></span>%
							</c:otherwise>
						</c:choose>
					</td>
			</tr>
		</tbody>
	</table>
	 <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	<div id="echartsColumn" style="width: 100%;height:600px; margin-top: 50px;"></div>
</div>