<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>
<head>
<!-- 对客户以及联系人的操作 -->
<script src="<%=basePath %>/statics/platform/js/updateClient.js"></script>
<title>修改客户</title>
</head>
<body>
<div class="tab tab_seller">
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/addClient','sellers')" class="hover">修改客户</a> <b>|</b>
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/clientList','sellers')">我的客户</a>
</div>
<div class="add_supplier">
  	<input type="hidden" id="id" name="id" value="${sellerClient.id}"/>
    <div class="form_group">
      <label class="control_label"><span class="red">*</span> 客 户 名 称:</label>
      <input type="text" id="clientName" name="clientName" class="form_control" required  value="${sellerClient.clientName}">
    </div>
    <div class="form_group">
      <label class="control_label"><span class="red">*</span>银 行 账 号:</label>
      <input type="text" id="bankAccount" name="bankAccount" class="form_control" required value="${sellerClient.bankAccount}">
    </div>
    <div class="form_group">
      <label class="control_label"><span class="red">*</span>开 户 行:</label>
      <input type="text" id="openBank" name="openBank" class="form_control" required value="${sellerClient.openBank}">
    </div>
    <div class="form_group">
      <label class="control_label"><span class="red">*</span>户 名:</label>
      <input type="text" id="accountName" name="accountName" class="form_control" required value="${sellerClient.accountName}">
    </div>
    <div class="form_group">
      <label class="control_label"><span class="red">*</span>纳税人识别号:</label>
      <input type="text" id="taxidenNum" name="taxidenNum" class="form_control" required value="${sellerClient.taxidenNum}">
    </div>
    <div class="setting">
      <h4>联系方式设置:</h4>
      <div class="btn_add mt">
             <a href="#" class="btn_new customer_b" ></a>
      </div>
      <table class="table_blue c_settings">
        <thead>
        <tr>
          <td style="width:6%">默认</td>
          <td style="width:8%">联系人</td>
          <td style="width:10%">手机号</td>
          <td style="width:15%">固话</td>
          <td style="width:16%">收货地址</td>
          <td style="width:9%">传真</td>
          <td style="width:9%">QQ</td>
          <td style="width:9%">旺旺</td>
          <td style="width:9%">E-mail</td>
          <td style="width:9%">操作</td>
        </tr>
        </thead>
        <tbody id="ContactSettings" style="vertical-align:middle; text-align:center;">
        	<c:choose>
        		<c:when test="${linkmanList.size()<= 0}">
        			<tr>       			   
        			   <td><input type="radio" name="default" checked onclick="setIsDefault(this)"></td><!-- 默认为1 -->
		               <td><input type="text" placeholder="联系人"></td>
		               <td><input type="text" placeholder="手机"></td>
		               <td>
		                 <input type="text" placeholder="区号" style="width:40px">
		                 -<input type="text" style="width:70px" placeholder="电话">
		               </td>
		               <td onclick="addShipAddress(this);">点击选择收货地址</td>
		               <td><input type="text" placeholder="传真"></td>
		               <td><input type="text" placeholder="QQ"></td>
		               <td><input type="text" placeholder="旺旺"></td>
		               <td><input type="text" placeholder="E-mail"></td>
		               <td> <!-- <span class="table_del"><b></b>删除</span> -->
		               	<input type="hidden" id="linkmanId" name="linkmanId" value="">
					 <!--     <a href="javascript:void(0)" onclick="setIsDefault(this)">设为默认</a> -->
						<input type="hidden" name="isDefault" value="0">
						<input type="hidden" name="isDel" value="0">
					   </td>
		             </tr>
        		</c:when>
        		<c:otherwise>
        			<c:forEach var="clientlinkman" items="${linkmanList}" varStatus="indexNum">
		          		<tr>
		          			<td>
								<c:if test="${clientlinkman.isDefault == 1}">
									<input type="radio" name="default" checked onclick="setIsDefault(this)">
								</c:if>
								<c:if test="${clientlinkman.isDefault == 0}">
									<input type="radio" name="default"  onclick="setIsDefault(this)"> 
								</c:if> 
						    </td>
			                <td><input type="text" placeholder="联系人" value="${clientlinkman.clientPerson }"></td>
			                <td><input type="text" placeholder="手机" value="${clientlinkman.clientPhone }"></td>
			                <td>
			                  <input type="text" placeholder="区号" style="width:40px" value="${clientlinkman.zone }">
			                  -<input type="text" style="width:70px" placeholder="电话" value="${clientlinkman.telno }">
			                </td>
			                <td style="vertical-align:middle; text-align:center;" onclick="addShipAddress(this);">
			                	<c:if test="${not empty clientlinkman.shipAddress}">
			                		${clientlinkman.shipAddress }
			                	</c:if>		                	
			                	<c:if test="${empty clientlinkman.shipAddress}">
			                		点击选择收货地址
			                	</c:if>            	
			                </td>
			                <td><input type="text" placeholder="传真" value="${clientlinkman.fax }"></td>
			                <td><input type="text" placeholder="QQ" value="${clientlinkman.qq }"></td>
			                <td><input type="text" placeholder="旺旺" value="${clientlinkman.wangNo }"></td>
			                <td><input type="text" placeholder="E-mail" value="${clientlinkman.email }"></td>
			                <td> 			                    
								<span class="table_del" onclick="delClientLinkman(this);"><b></b>删除</span>	
								<input type="hidden" id="linkmanId" name="linkmanId" value="${clientlinkman.id}"/>				                			                    	
								<input type="hidden" name="isDefault" value="${clientlinkman.isDefault}"/>
								<input type="hidden" name="isDel" value="${clientlinkman.isdel}"/>
							</td>
		            	</tr>
        			</c:forEach>
        		</c:otherwise>
        	  </c:choose>
               	
        </tbody>
      </table>
    </div>
  
  <div class="text-center">
    <input type="button" class="next_step"  onclick="saveUpdateClient();" value="保存">
  </div>
  
  <!--选择收货地址-->
<div class="plain_frame edit_addr" style="display: none;">
    <ul>
      <li>
        <span>收货地址:</span>
        <div>
	        <select name="province3" id="addr1" onchange ="addr1();">
	        	<option  value="">省份</option>
	        	<c:forEach var="province" items="${provinceList}">
				 	<option value="${province.provinceId}">${province.province}</option>
				</c:forEach>
	        </select>
	        <select name="city3" id="addr2" onchange ="addr2();">
	        	<option value="">地级市</option>
	        </select>
	        <select name="area3" id="addr3">
	        	<option value="">县区</option>
	        </select>
        </div>
      </li>
      <li>
        <span>详细地址:</span>
        <textarea placeholder="请如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息" id="addr4"></textarea>
      </li>
    </ul>
</div>
</div>
</body>
</html>