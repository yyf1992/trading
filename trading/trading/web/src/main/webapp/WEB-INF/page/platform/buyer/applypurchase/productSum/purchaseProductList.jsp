<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<script type="text/javascript">
	$(function() {
		//设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
		//初始化数据
		selectData();
		$("#putOrderContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
		$(".tab a").click(function(){
			var id = $(this).attr("id");
			var index = id.split("_")[1];
			$("#putOrderContent input[name='tabId']").val(index);
			loadPlatformData();
		});
		pure_chkAll();
	});
	function selectData(){
		var formObj = $("#putOrderContent").find("form");
		$.ajax({
			url : basePath+"buyer/applyPurchaseHeader/loadPurchaseProductListData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#tabContent").html(str);
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	//table_pure 带复选框的全选
	function pure_chkAll() {
		var inputs = $(".table_pure .chk_click input[type='checkbox']");
		function checkedSpan() {
			inputs.each(function() {
				if ($(this).prop('checked') == true) {
					$(this).parent().addClass('checked')
				} else {
					$(this).parent().removeClass('checked')
				}
			})
		}
		$('.table_pure thead input[type="checkbox"]').click(function() {
			inputs.prop('checked', $(this).prop('checked'));
			checkedSpan();
		});
		$('.table_pure tbody input[type="checkbox"]')
				.click(
						function() {
							var r = $('.table_pure tbody input[type="checkbox"]:not(:checked)');
							if (r.length == 0) {
								$('.table_pure thead input[type="checkbox"]')
										.prop('checked', true);
							} else {
								$('.table_pure thead input[type="checkbox"]')
										.prop('checked', false);
							}
							console.log(11);
							checkedSpan();
						})
	}
	// 采购明细
	function loadPurchaseDetails(barcode) {
		$.ajax({
			type : "POST",
			url : "buyer/applyPurchaseHeader/loadPurchaseDetails",
			data : {
				"barcode" : barcode
			},
			success : function(data) {
				layer.open({
					type : 1,
					area:['1200px', 'auto'],
					fix: false, //不固定
					maxmin: true,
					shadeClose: true,
		        	shade:0.4,
					title:"采购明细",
			    	content: $('.purchaseContent').html(data),
			    	btn : [ '确定', '关闭' ]
				});
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", { icon : 2 });
			}
		});
	}
	// 批量生成采购订单
	function beatchCreateOrder(){
		var checked = $("input:checkbox[name='checkone']:checked");
		if (checked.length < 1) {
			layer.msg("请至少选择一条数据！", { icon : 7 });
			return;
		}
		// 剩余采购量
		var status = false;
		var idStr = "";
		checked.each(function() {
			var surplusNum = $(this).parents("tr").find("td:eq(8)").text();
			if(Number(surplusNum) <= 0 ){
				status = true;
			}
			idStr += "'" + $(this).val() + "'" + ",";
		});
		if(status){
			layer.msg("请选择剩余采购量大于0的数据！", { icon : 7 });
			return;
		}
		idStr = idStr.substring(0, idStr.length - 1);
		leftMenuClick(this,"buyer/applyPurchaseHeader/beatchCreateOrder?idStr=" + idStr,"buyer");
	}
	//导出
	$("#purchaseExportButton").click(function(e){
		e.preventDefault();
		var formData = $("form").serialize();
		var url = "download/purchaseStatistic?"+ formData;
		window.open(url);
	});
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">成品（<span>${params.finishedProductNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">原材料（<span>${params.rawStockNum}</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_2">辅料（<span>${params.accessoriesNum}</span>）</a> <b>|</b> 
	<a href="javascript:void(0)" id="tab_3">虚拟产品（<span>${params.inventedNum}</span>）</a> <b>|</b> 
</div>
<div id="putOrderContent">
	<!--搜索栏-->
	<form class="layui-form"
		action="buyer/applyPurchaseHeader/purchaseProductList" id="searchForm">
		<div class='search_two mt'>
			<input type='text' name="productInfo" placeholder='输入商品信息进行搜索' value="${params.productInfo }">
			<button onclick="loadPlatformData();" id="selectButton">搜索</button>
		</div>
		<input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<!-- <div class="batch_handle mt" id="purchaseSubmit">
		<a onclick="beatchCreateOrder();" class="c66">批量生成采购订单</a>
	</div> -->
	<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="purchaseExportButton">
		<i class="layui-icon">&#xe7a0;</i> 导出</a>
	<a href="javascript:beatchCreateOrder();;" class="layui-btn layui-btn-danger layui-btn-small rt" style="margin-right: 10px;">
		<i class="layui-icon">&#xe67d;</i> 批量生成采购订单</a>
	<div id="tabContent"></div>
	<!-- 采购明细弹窗 -->
	<div class="purchaseContent"></div>
</div>
