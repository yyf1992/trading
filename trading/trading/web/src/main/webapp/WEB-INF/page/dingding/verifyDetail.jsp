<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<%@ include file="common/common_app.jsp"%>
	<script src="http://g.alicdn.com/dingding/dingtalk-pc-api/2.7.0/index.js"></script>
	<script src="<%=basePath%>statics/dingding/js/verify.js"></script>
</head>
<script>
$(function(){
	setVerifyHtml();
});
//设置审批展示
function setVerifyHtml(){
	layer.load();
	var siteId = "${tradeHeader.siteId}";
	var relatedId = "${tradeHeader.relatedId}";
    //if(siteId=="18032617435211938508"){//测试环境
        //if(siteId=="18030316174277880699"){//阿里正式
    if(siteId=="18040313401887543187"){//阿里充值
        //充值记录数据
        var data = getRelateDate(relatedId,"platform/buyer/billAdvanceItem/acceptRechargeDetail");
        var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
        htmlStr +="<li><span>充值编号：</span>"+data.id+"</li>";
        htmlStr +="<li><span>预付款账号：</span>"+data.advanceId+"</li>";
        htmlStr +="<li><span>供应商：</span>"+data.sellerCompanyName+"</li>";
        htmlStr +="<li><span>采购人员：</span>"+data.createBillUserName+"</li>";
        htmlStr +="<li><span>申请金额：</span>"+data.updateTotal+"&nbsp;元</li>";
        htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
        htmlStr +="<li><span>充值详情：</span>";
        htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.id+"\");'>";
        htmlStr +="	<input name='annexUrl' value='platform/buyer/billAdvanceItem/rechargeDetailView' type='hidden'>";
        htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
        htmlStr +="</a>";
        htmlStr +="<font color='#666'>点击查看充值详细信息</font>";
        htmlStr +="</li>";
        htmlStr +="<li id='relatedImgLi' style='display: none;'>";
        htmlStr +="<div id='layer-photos-demo' class='layer-photos-demo'>";
        htmlStr +="	<img id='relatedImg' layer-pid='relatedImg' layer-src='' src='' width='350px' style='cursor:pointer'>";
        htmlStr +="</div>";
        htmlStr +="</li>";
        $("#verify_side .appContent ul").empty();
        $("#verify_side .appContent ul").append(htmlStr);
        createRelatedImg(data.id,"platform/buyer/billAdvanceItem/rechargeDetailView");
    }else
    //if(siteId=="18032210464367148354"){//测试环境
    if(siteId=="17111617182446221877"){//阿里正式
        //付款，获得付款数据
        var data = getRelateDate(relatedId,"platform/buyer/billReconciliation/getBillRecDetail");
        var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
        htmlStr +="<li><span>账单编号：</span>"+data.billRecInfo.id+"</li>";
        htmlStr +="<li><span>账单周期：</span>"+data.billRecInfo.startBillStatementDateStr+"至"+data.billRecInfo.endBillStatementDateStr+"</li>";
        htmlStr +="<li><span>采购人员：</span>"+data.billRecInfo.createBillUserName+"</li>";
        htmlStr +="<li><span>到货总数：</span>"+(data.billRecInfo.recordConfirmCount + data.billRecInfo.recordReplaceCount)+"</li>";
        htmlStr +="<li><span>到货总额：</span>"+(data.billRecInfo.recordConfirmTotal + data.billRecInfo.recordReplaceTotal)+"&nbsp;元</li>";
        htmlStr +="<li><span>售后总数：</span>"+(data.billRecInfo.replaceConfirmCount + data.billRecInfo.returnGoodsCount)+"</li>";
        htmlStr +="<li><span>售后总额：</span>"+(data.billRecInfo.replaceConfirmTotal + data.billRecInfo.returnGoodsTotal)+"&nbsp;元</li>";
        htmlStr +="<li><span>运&nbsp;&nbsp;费：</span>"+data.billRecInfo.freightSumCount+"&nbsp;元</li>";
        htmlStr +="<li><span>奖惩总额：</span>"+data.billRecInfo.customAmount+"&nbsp;元</li>";
        htmlStr +="<li><span>-----------奖惩详情-----------</span></li>";
        htmlStr +="<li>";
        htmlStr +="<div id='showBillCustomList'>";
        htmlStr +="<table class='table_yellow'><thead><tr>";
        htmlStr +="<td style='width:10%'>申请人</td><td style='width:10%'>申请时间</td><td style='width:10%'>类型</td><td style='width:10%'>金额</td><td style='width:10%'>备注</td></tr></thead>";
        htmlStr +="<tbody id='billRecCustomBody'></tbody></table>";
        htmlStr +="</div>";
        htmlStr +="</li>";

        htmlStr +="<li><span>预付款抵扣额：</span>"+data.billRecInfo.advanceDeductTotal+"&nbsp;元</li>";
        htmlStr +="<li><span>-----------预付款抵扣详情-----------</span></li>";
        htmlStr +="<li>";
        htmlStr +="<div id='showAdvanceEditList''>";
        htmlStr +="<table class='table_yellow'><thead><tr>";
        htmlStr +="<td style='width:10%'>采购员</td><td style='width:10%'>申请人</td><td style='width:10%'>申请时间</td><td style='width:10%'>抵扣金额</td></tr></thead>";
        htmlStr +="<tbody id='advanceEditBody'></tbody></table>";
        htmlStr +="</div>";
        htmlStr +="</li>";

        htmlStr +="<li><span>账单总额：</span>"+data.billRecInfo.totalAmount+"&nbsp;元</li>";
        htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
        htmlStr +="<li><span>账单详情：</span>";
        htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.billRecInfo.id+"\");'>";
        htmlStr +="	<input name='annexUrl' value='platform/buyer/billReconciliation/showReconciliationDetailsVerify' type='hidden'>";
        htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
        htmlStr +="</a>";
        htmlStr +="<font color='#666'>点击查看账单详细信息</font>";
        htmlStr +="</li>";
        htmlStr +="<li id='relatedImgLi' style='display: none;'>";
        htmlStr +="<div id='layer-photos-demo' class='layer-photos-demo'>";
        htmlStr +="	<img id='relatedImg' layer-pid='relatedImg' layer-src='' src='' width='350px' style='cursor:pointer'>";
        htmlStr +="</div>";
        htmlStr +="</li>";
        $("#verify_side .appContent ul").empty();
        $("#verify_side .appContent ul").append(htmlStr);
        showCustomeInfo(data.customItemList);
        showAdvanceInfo(data.advanceEditList);
        createRelatedImg(data.billRecInfo.id,"platform/buyer/billReconciliation/showReconciliationDetailsVerify");
    }else
	//if(siteId=="18022814245915101199"){//测试环境
	if(siteId=="18030316164774916589"){//阿里正式
		//付款，获得付款数据
		var data = getRelateDate(relatedId,"platform/buyer/billPaymentDetail/getPaymentDetail");
		var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
		htmlStr +="<li><span>付款编号：</span>"+data.id+"</li>";
		htmlStr +="<li><span>账单编号：</span>"+data.reconciliationId+"</li>";
		htmlStr +="<li><span>账单总额：</span>"+data.totalAmount+"&nbsp;元</li>";
		/* htmlStr +="<li><span>待付金额：</span>"+data.residualPaymentAmount+"&nbsp;元</li>"; */
		htmlStr +="<li><span>本次申请：</span>"+data.actualPaymentAmount+"&nbsp;元</li>";
		htmlStr +="<li><span>备&nbsp;&nbsp;注：</span>"+data.paymentRemarks+"</li>";
		htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
		htmlStr +="<li><span>账单详情：</span>";
		htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.reconciliationId+"\");'>";
		htmlStr +="	<input name='annexUrl' value='platform/buyer/billPaymentDetail/showRecItemListVerify' type='hidden'>";
		htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
		htmlStr +="</a>";
		htmlStr +="<font color='#666'>点击查看账单详细信息</font>";
		htmlStr +="</li>";
		htmlStr +="<li id='relatedImgLi' style='display: none;'>";
		htmlStr +="	<img id='relatedImg' src='' width='350px' height='200px;' style='cursor:pointer' onclick='openImg(this);'>";
		htmlStr +="</li>";
		$("#verify_side .appContent ul").empty();
		$("#verify_side .appContent ul").append(htmlStr);
		createRelatedImg(data.reconciliationId,"platform/buyer/billPaymentDetail/showRecItemListVerify");
	}else{
		var url = $("#relatedUrl").val();
		createRelatedImg(relatedId,url);
	}
	layer.closeAll("loading");
}

//奖惩详情
function showCustomeInfo(customItemList) {
    debugger
    $.each(customItemList,function(i,customItem){
        debugger
        //奖惩类型
        var customType = customItem.customType;
        var customTypeStr = "";
        if(customType == "1"){
            customTypeStr = "奖励";
        }else if(customType == "2"){
            customTypeStr = "处罚";
        }
        var trObj = $("<tr>");
        $("<td>"+customItem.createCustomUserName+"</td>").appendTo(trObj);
        $("<td>"+customItem.createCustomTime+"</td>").appendTo(trObj);
        $("<td>"+customTypeStr+"</td>").appendTo(trObj);
        $("<td>"+customItem.customPaymentMoney+"</td>").appendTo(trObj);
        $("<td>"+customItem.customRemarks+"</td>").appendTo(trObj);
        $("#billRecCustomBody").append(trObj);
    });
}
//预付款抵扣详情
function showAdvanceInfo(advanceEditList) {
    debugger
    $.each(advanceEditList,function(i,advanceEdit){
        debugger
        var trObj = $("<tr>");
        $("<td>"+advanceEdit.createCustomUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createBillUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createTimeStr+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.updateTotal+"</td>").appendTo(trObj);
        $("#advanceEditBody").append(trObj);
    });
}

function getRelateDate(relatedId,url){
	$.ajax({
		url:basePath+url,
		async:false,
		data:{"id":relatedId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			returnData = resultObj;
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
	return returnData;
}
function createRelatedImg(id,url){
	$.ajax({
		url:basePath+url,
		data:{"id":id},
		success:function(data){
			var verifyDetailDiv = $("<div style='width:2000px'></div>");
			verifyDetailDiv.html(data);
			$("body").append(verifyDetailDiv);
			html2canvas(verifyDetailDiv, {
			　　onrendered: function(canvas) {
				 var imgUrl = canvas.toDataURL("image/png");
				 if(imgUrl != ''){
				 	$("#relatedImgLi").show();
				   　　  $("#relatedImg").attr("src",imgUrl);
				   	 verifyDetailDiv.remove();
				 }else{
				 	$("#relatedImgLi").hide();
				 }
			   }
			});
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
function openImg(obj){
	DingTalkPC.biz.util.openModal({
		"url": $(obj).attr("src"),
		"title": "详情图片"
	});
}
</script>
<body>
	<input type="hidden" id="relatedId" value="${tradeHeader.relatedId}">
	<input type="hidden" id="relatedUrl" value="${tradeHeader.relatedUrl}">
	<div id="verify_side">
		<div class="detail-content">
			<c:choose>
				<c:when test="${tradeHeader.status==1}">
					<!-- 通过 -->
					<div class="approve-result verify-success pullout"
						data-reactid=".0.1.0.0.1"></div>
				</c:when>
				<c:when test="${tradeHeader.status==2}">
					<!-- 拒绝 -->
					<div class="approve-result verify-refuse pullout"
						data-reactid=".0.1.0.0.1"></div>
				</c:when>
				<c:when test="${tradeHeader.status==3}">
					<!-- 撤销 -->
					<div class="approve-result verify-revoked pullout"
						data-reactid=".0.1.0.0.1"></div>
				</c:when>
			</c:choose>
			<div class="appContent">
				<ul>
					<li><span>审批编号：</span>${tradeHeader.id}</li>
					<li><span>审批类型：</span>【${tradeHeader.title}】</li>
					<li><span>申&nbsp;请&nbsp;人：</span>${tradeHeader.createName}</li>
					<li><span>申请日期：</span>
					 	<fmt:formatDate value="${tradeHeader.createDate}" pattern="yyyy-MM-dd HH:mm" />
					</li>
					<c:if test="${tradeHeader.status != '0'}">
						<li><span>结束日期：</span> 
							<fmt:formatDate value="${tradeHeader.updateDate}" pattern="yyyy-MM-dd HH:mm" /></li>
					</c:if>
					<li><span>附件：</span> <a href="javascrit:void(0)"
						onclick="openAnnex(this,'${tradeHeader.relatedId}');"> <input
							name="annexUrl" value="${tradeHeader.relatedUrl}" type="hidden">
								<img src="<%=basePath%>statics/platform/images/icon_14.png">
					</a> <font color="#666">点击附件查看审批关联信息</font></li>
					<li id="relatedImgLi" style="display: none;">
						<img id="relatedImg" src="" width="350px" height="200px;" style="cursor:pointer" onclick="openImg(this);">
					</li>
				</ul>
			</div>
			<div class="appProcess">
				<div class="organizer">
					<p>
						发起申请<span></span>
					</p>
					<div class="clear">
						<h4>${tradeHeader.startName}</h4>
						<span class="time-span">
							${tradeHeader.createDateFormat}&nbsp; 
							<fmt:formatDate value="${tradeHeader.createDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
					</div>
				</div>
				<c:forEach items="${pocessList}" var="pocess">
					<c:choose>
						<c:when test="${pocess.startIndext && pocess.status=='0'}">
							<div class="approvaling">
								<p>
									审批中<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='0'}">
							<div class="organizer">
								<p>
									等待审批<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='1'}">
							<div class="success">
								<p>
									已通过<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<c:if test="${pocess.fileList != null && pocess.fileList.size()>0}">
										<c:forEach var="fileObj" items="${pocess.fileList}">
											<p><a href="javascript:void(0)" onclick="downLoad('${fileObj.fileUrl}')">${fileObj.fileName}</a></p>
										</c:forEach>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='2'}">
							<div class="refuse">
								<p>
									已驳回<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<c:if test="${pocess.fileList != null && pocess.fileList.size()>0}">
										<c:forEach var="fileObj" items="${pocess.fileList}">
											<p><a href="javascript:void(0)" onclick="downLoad('${fileObj.fileUrl}')">${fileObj.fileName}</a></p>
										</c:forEach>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='3'}">
							<div class="success">
								<p>
									已转交<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<c:if test="${pocess.fileList != null && pocess.fileList.size()>0}">
										<c:forEach var="fileObj" items="${pocess.fileList}">
											<p><a href="javascript:void(0)" onclick="downLoad('${fileObj.fileUrl}')">${fileObj.fileName}</a></p>
										</c:forEach>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='4'}">
							<div class="refuse">
								<p>
									已撤销<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='6'}">
							<div class="refuse">
								<p>
									卖家驳回<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
						<c:when test="${!pocess.startIndext && pocess.status=='7'}">
							<div class="organizer">
								<p>
									重新申请<span></span>
								</p>
								<div class="clear">
									<h4>${pocess.userName}</h4>
									<c:if test="${pocess.remark != null}">
										<p>(${pocess.remark})</p>
									</c:if>
									<span class="time-span"> ${pocess.startDateFormat}&nbsp;
										<fmt:formatDate value="${pocess.endDate}" pattern="yyyy-MM-dd HH:mm" /> </span>
								</div>
							</div>
						</c:when>
					</c:choose>
				</c:forEach>
			</div>
			<!--抄送人-->
			<c:if test="${copyList != null && copyList.size() > 0}">
				<div class="CC">
					<p class="size_lg c66">
						抄送人 <span class="size_md">(${copyType})</span>
					</p>
					<div>
						<ul class="CCList">
							<c:forEach items="${copyList}" var="copy">
								<li><span class="userSpan">${copy.userName}</span>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</c:if>
		</div>
		<c:if test="${tradeHeader.status==0}">
			<!-- 审批未结束 -->
			<c:if test="${urge}">
				<!-- 可以催办 -->
				<div class="appButton">
					<button class="layui-btn layui-btn-normal layui-btn-radius" onclick="urge('${tradeHeader.id}');">催办</button>
				</div>
			</c:if>
			<c:choose>
				<c:when test="${verify}">
					<!-- 需要审批 -->
					<c:if test="${revoke}">
						<!-- 可以撤销 -->
						<div class="appButton-own">
							<button class="layui-btn-radius layui-btn" onclick="saveVerify('1','${tradeHeader.id}');">同意</button>
							<button class="layui-btn-danger layui-btn-radius layui-btn" onclick="saveVerify('2','${tradeHeader.id}');">拒绝</button>
							<button class="layui-btn-warm layui-btn-radius layui-btn" onclick="saveVerify('3','${tradeHeader.id}');">转交</button>
						</div>
					</c:if>
					<c:if test="${!revoke}">
						<!-- 不可以撤销 -->
						<div class="appButton">
							<button class="layui-btn-radius layui-btn" onclick="saveVerify('1','${tradeHeader.id}');">同意</button>
							<button class="layui-btn-danger layui-btn-radius layui-btn" onclick="saveVerify('2','${tradeHeader.id}');">拒绝</button>
							<button class="layui-btn-warm layui-btn-radius layui-btn" onclick="saveVerify('3','${tradeHeader.id}');">转交</button>
						</div>
					</c:if>
				</c:when>
				<c:otherwise>
					<!-- 不需要审批-->
					<c:if test="${!revoke}">
						<!-- 不可以撤销 -->
						<c:if test="${verifyOwen}">
							<!-- <h3 class="wrap">我已审批</h3> -->
						</c:if>
						<c:if test="${!verifyOwen}">
							<!-- <h3 class="wrap"></h3> -->
						</c:if>
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${tradeHeader.status!=0}">
			<!-- 审批结束 -->
			<!-- <h3 class="wrap">审批结束</h3> -->
		</c:if>
	</div>
	<input id="userId" value="${userId}" type="hidden">
	<input id="verifyId" value="${verifyId}" type="hidden">
</body>