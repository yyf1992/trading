<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../common/path.jsp"%>
<script type="text/javascript" src="<%=basePath%>js/traderole/tradeRole.js"></script>
<div>
	<form action="platform/tradeRole/tradeRoleList" id="searchForm" class="layui-form">
		<ul class="order_search supplier_query">
			<li class="comm">
                <label>角色代码:</label>
                <input type="text" placeholder="输入角色代码" name="roleCode" value="${searchPageUtil.object.roleCode}" >
			</li>
			<li class="comm">
                <label>角色名称:</label>
                <input type="text" placeholder="输入角色名称" name="roleName" value="${searchPageUtil.object.roleName}" >
			</li>
			<li class="nomargin">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
        </ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<div class="mt">
			<a href="javascript:void(0)" button="新增"
				class="layui-btn layui-btn-add layui-btn-small rt"
				onclick="loadAdd();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
	</form>
		<table class="table_pure supplierList mt">
		<thead>
			<tr>
				<td style="width:10%">角色代码</td>
                <td style="width:12%">角色名称</td>
                <td style="width:4%">类型</td>
                <td style="width:20%">备注</td>
                <td style="width:5%">状态</td>
                <td style="width:15%">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="tradeRole" items="${searchPageUtil.page.list}">
				<tr <c:if test="${tradeRole.status==1}">class="tr-disabled"</c:if>>
					<td>${tradeRole.roleCode}</td>
					<td>${tradeRole.roleName}</td>
					<td>
						<c:choose>
							<c:when test="${tradeRole.isAdmin==0}">
								<i class="layui-icon" title="普通">&#xe96b;</i>
							</c:when>
							<c:when test="${tradeRole.isAdmin==1}">
								<i class="layui-icon" title="管理员">&#xea52;</i>
							</c:when>
						</c:choose>
					</td>
					<td>${tradeRole.remark}</td>
					<td>
						<c:choose>
							<c:when test="${tradeRole.status==0}">启用</c:when>
							<c:when test="${tradeRole.status==1}"><font color="red">禁用</font></c:when>
						</c:choose>
					</td>
					<td>
						<a href="javascript:void(0)" button="设置权限"
							onclick="leftMenuClick(this,'platform/tradeRole/instalMenu?id=${tradeRole.id}','system')"
							class="layui-btn layui-btn-update layui-btn-mini">
							<i class="layui-icon">&#xeaf3;</i>设置权限</a>
						<c:choose>
							<c:when test="${tradeRole.status==0}">
								<a onclick="switchStatus('${tradeRole.id}','1');" button="禁用"
									class="layui-btn layui-btn-jinyong layui-btn-mini">
									<i class="layui-icon">&#xeaf2;</i>禁用</a>
							</c:when>
							<c:when test="${tradeRole.status==1}">
								<a onclick="switchStatus('${tradeRole.id}','0');" button="启用"
									class="layui-btn layui-btn-mini">
									<i class="layui-icon">&#xeabc;</i>启用</a>
							</c:when>
						</c:choose>
						<a href="javascript:void(0)" button="删除"
							onclick="deleteTradeRole('${tradeRole.id}')"
							class="layui-btn layui-btn-delete layui-btn-mini">
							<i class="layui-icon">&#xe7ea;</i>删除</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
</div>