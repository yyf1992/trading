<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../common/path.jsp"%>
<link rel="stylesheet"
	href="<%=basePath%>statics/platform/css/verify-common.css">
<script type="text/javascript">
$(function(){
	setVerifyHtml();
});
//设置审批展示
function setVerifyHtml(){
	layer.load();
	var siteId = "${tradeHeader.siteId}";
	var relatedId = "${tradeHeader.relatedId}";
    //if(siteId=="18032617435211938508"){//测试环境(新)
    //if(siteId=="18022814242829641325"){//测试环境
    //if(siteId=="18030316174277880699"){//阿里正式
    if(siteId=="18040313401887543187"){//阿里充值
        //充值记录数据
        var data = getRelateDate(relatedId,"platform/buyer/billAdvanceItem/acceptRechargeDetail");
        var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
        htmlStr +="<li><span>充值编号：</span>"+data.id+"</li>";
        htmlStr +="<li><span>预付款账号：</span>"+data.advanceId+"</li>";
        htmlStr +="<li><span>供应商：</span>"+data.sellerCompanyName+"</li>";
        htmlStr +="<li><span>采购人员：</span>"+data.createBillUserName+"</li>";
        htmlStr +="<li><span>申请金额：</span>"+data.updateTotal+"&nbsp;元</li>";
        htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
        htmlStr +="<li><span>充值详情：</span>";
        htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.id+"\");'>";
        htmlStr +="	<input name='annexUrl' value='platform/buyer/billAdvanceItem/rechargeDetailView' type='hidden'>";
        htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
        htmlStr +="</a>";
        htmlStr +="<font color='#666'>点击查看充值详细信息</font>";
        htmlStr +="</li>";
        htmlStr +="<li id='relatedImgLi' style='display: none;'>";
        htmlStr +="<div id='layer-photos-demo' class='layer-photos-demo'>";
        htmlStr +="	<img id='relatedImg' layer-pid='relatedImg' layer-src='' src='' width='350px' style='cursor:pointer'>";
        htmlStr +="</div>";
        htmlStr +="</li>";
        $("#verify_side .appContent ul").empty();
        $("#verify_side .appContent ul").append(htmlStr);
    }else
    //if(siteId=="18032210464367148354"){//测试环境
    if(siteId=="17111617182446221877"){//阿里正式
        //付款，获得付款数据
        var data = getRelateDate(relatedId,"platform/buyer/billReconciliation/getBillRecDetail");
        var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
        htmlStr +="<li><span>账单编号：</span>"+data.billRecInfo.id+"</li>";
        htmlStr +="<li><span>账单周期：</span>"+data.billRecInfo.startBillStatementDateStr+"至"+data.billRecInfo.endBillStatementDateStr+"</li>";
        htmlStr +="<li><span>采购人员：</span>"+data.billRecInfo.createBillUserName+"</li>";
        htmlStr +="<li><span>到货总数：</span>"+(data.billRecInfo.recordConfirmCount + data.billRecInfo.recordReplaceCount)+"</li>";
        htmlStr +="<li><span>到货总额：</span>"+(data.billRecInfo.recordConfirmTotal + data.billRecInfo.recordReplaceTotal)+"&nbsp;元</li>";
        htmlStr +="<li><span>售后总数：</span>"+(data.billRecInfo.replaceConfirmCount + data.billRecInfo.returnGoodsCount)+"</li>";
        htmlStr +="<li><span>售后总额：</span>"+(data.billRecInfo.replaceConfirmTotal + data.billRecInfo.returnGoodsTotal)+"&nbsp;元</li>";
        htmlStr +="<li><span>运&nbsp;&nbsp;费：</span>"+data.billRecInfo.freightSumCount+"&nbsp;元</li>";
        htmlStr +="<li><span>奖惩总额：</span>"+data.billRecInfo.customAmount+"&nbsp;元</li>";
        htmlStr +="<li><span>-----------奖惩详情-----------</span></li>";
        htmlStr +="<li>";
        htmlStr +="<div id='showBillCustomList'>";
        htmlStr +="<table class='table_yellow'><thead><tr>";
        htmlStr +="<td style='width:10%'>申请人</td><td style='width:10%'>申请时间</td><td style='width:10%'>类型</td><td style='width:10%'>金额</td><td style='width:10%'>备注</td></tr></thead>";
        htmlStr +="<tbody id='billRecCustomBody'></tbody></table>";
        //htmlStr +="</tbody></table>";advanceEditList
        htmlStr +="</div>";
        htmlStr +="</li>";

        htmlStr +="<li><span>预付款抵扣额：</span>"+data.billRecInfo.advanceDeductTotal+"&nbsp;元</li>";
        htmlStr +="<li><span>-----------预付款抵扣详情-----------</span></li>";
        htmlStr +="<li>";
        htmlStr +="<div id='showAdvanceEditList''>";
        htmlStr +="<table class='table_yellow'><thead><tr>";
        htmlStr +="<td style='width:10%'>采购员</td><td style='width:10%'>申请人</td><td style='width:10%'>申请时间</td><td style='width:10%'>抵扣金额</td></tr></thead>";
        htmlStr +="<tbody id='advanceEditBody'></tbody></table>";
        htmlStr +="</div>";
        htmlStr +="</li>";

        htmlStr +="<li><span>账单总额：</span>"+data.billRecInfo.totalAmount+"&nbsp;元</li>";
        htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
        htmlStr +="<li><span>账单详情：</span>";
        htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.billRecInfo.id+"\");'>";
        htmlStr +="	<input name='annexUrl' value='platform/buyer/billReconciliation/showReconciliationDetailsVerify' type='hidden'>";
        htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
        htmlStr +="</a>";
        htmlStr +="<font color='#666'>点击查看账单详细信息</font>";
        htmlStr +="</li>";
        htmlStr +="<li id='relatedImgLi' style='display: none;'>";
        htmlStr +="<div id='layer-photos-demo' class='layer-photos-demo'>";
        htmlStr +="	<img id='relatedImg' layer-pid='relatedImg' layer-src='' src='' width='350px' style='cursor:pointer'>";
        htmlStr +="</div>";
        htmlStr +="</li>";
        $("#verify_side .appContent ul").empty();
        $("#verify_side .appContent ul").append(htmlStr);
        showCustomeInfo(data.customItemList);
        showAdvanceInfo(data.advanceEditList);
    }else
    //if(siteId=="18022814245915101199"){//测试环境
    if(siteId=="18030316164774916589"){//阿里正式
		//付款，获得付款数据
		var data = getRelateDate(relatedId,"platform/buyer/billPaymentDetail/getPaymentDetail");
		var htmlStr = "<li><span>审批编号：</span>${tradeHeader.id}</li>";
		htmlStr +="<li><span>付款编号：</span>"+data.id+"</li>";
		htmlStr +="<li><span>账单编号：</span>"+data.reconciliationId+"</li>";
		htmlStr +="<li><span>账单总额：</span>"+data.totalAmount+"&nbsp;元</li>";
		htmlStr +="<li><span>本次申请：</span>"+data.actualPaymentAmount+"&nbsp;元</li>";
		htmlStr +="<li><span>备&nbsp;&nbsp;注：</span>"+data.paymentRemarks+"</li>";
		htmlStr +="<li><span>申请日期：</span><fmt:formatDate value='${tradeHeader.createDate}' pattern='yyyy-MM-dd HH:mm' /></li>";
		htmlStr +="<li><span>账单详情：</span>";
		htmlStr +="<a href='javascrit:void(0)' onclick='openAnnex(this,\""+data.reconciliationId+"\");'>";
		htmlStr +="	<input name='annexUrl' value='platform/buyer/billPaymentDetail/showRecItemListVerify' type='hidden'>";
		htmlStr +="	<img src='<%=basePath%>statics/platform/images/icon_14.png'>";
		htmlStr +="</a>";
		htmlStr +="<font color='#666'>点击查看账单详细信息</font>";
		htmlStr +="</li>";
		$("#verify_side .appContent ul").empty();
		$("#verify_side .appContent ul").append(htmlStr);
	}else{
	}
	layer.closeAll("loading");
}

//奖惩详情
function showCustomeInfo(customItemList) {
    debugger
    $.each(customItemList,function(i,customItem){
        debugger
        //奖惩类型
        var customType = customItem.customType;
        var customTypeStr = "";
        if(customType == "1"){
            customTypeStr = "奖励";
        }else if(customType == "2"){
            customTypeStr = "处罚";
        }
        var trObj = $("<tr>");
        $("<td>"+customItem.createCustomUserName+"</td>").appendTo(trObj);
        $("<td>"+customItem.createCustomTime+"</td>").appendTo(trObj);
        $("<td>"+customTypeStr+"</td>").appendTo(trObj);
        $("<td>"+customItem.customPaymentMoney+"</td>").appendTo(trObj);
        $("<td>"+customItem.customRemarks+"</td>").appendTo(trObj);
        $("#billRecCustomBody").append(trObj);
    });
}
//预付款抵扣详情
function showAdvanceInfo(advanceEditList) {
    debugger
    $.each(advanceEditList,function(i,advanceEdit){
        debugger
        var trObj = $("<tr>");
        $("<td>"+advanceEdit.createCustomUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createBillUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createUserName+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.createTimeStr+"</td>").appendTo(trObj);
        $("<td>"+advanceEdit.updateTotal+"</td>").appendTo(trObj);
        $("#advanceEditBody").append(trObj);
    });
}

function getRelateDate(relatedId,url){
	$.ajax({
		url:basePath+url,
		async:false,
		data:{"id":relatedId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			returnData = resultObj;
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
	return returnData;
}
function urge(tradeId){
	$.ajax({
		url:basePath+"platform/tradeVerify/urgeTrade",
		data:{"tradeId":tradeId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			layer.msg(resultObj.msg);
		},
		error:function(){
		}
	});
}
</script>
	<h3 class="wrap">${tradeHeader.createName}的【${tradeHeader.title}】</h3>
<c:choose>
	<c:when test="${tradeHeader.status==1}">
		<!-- 通过 -->
		<div class="approve-result verify-success pullout"
			data-reactid=".0.1.0.0.1"></div>
	</c:when>
	<c:when test="${tradeHeader.status==2}">
		<!-- 拒绝 -->
		<div class="approve-result verify-refuse pullout"
			data-reactid=".0.1.0.0.1"></div>
	</c:when>
	<c:when test="${tradeHeader.status==3}">
		<!-- 撤销 -->
		<div class="approve-result verify-revoked pullout"
			data-reactid=".0.1.0.0.1"></div>
	</c:when>
</c:choose>
<div class="appContent">
	<ul>
		<li><span>审批编号：</span>${tradeHeader.id}</li>
		<li><span>审批类型：</span>【${tradeHeader.title}】</li>
		<li><span>申&nbsp;请&nbsp;人：</span>${tradeHeader.createName}</li>
		<li><span>申请日期：</span> <fmt:formatDate
				value="${tradeHeader.createDate}" type="both" />
		</li>
		<c:if test="${tradeHeader.status != '0'}">
			<li><span>结束日期：</span> <fmt:formatDate
					value="${tradeHeader.updateDate}" type="both" /></li>
		</c:if>
	</ul>
</div>
<div style="margin-top: 1px;">
	<p>发起申请<span></span></p>
	<div class='clear'>
		<h4>${tradeHeader.createName}</h4>
		<span>
			<fmt:formatDate value="${tradeHeader.createDate}" type="both" />
		</span>
	</div>
</div>
<c:forEach items="${pocessList}" var="pocess">
<c:if test="${pocess.status!='5'}">
	<div>
		<p>
			<c:choose>
				<c:when test="${pocess.startIndext && pocess.status=='0'}">审批中</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='0'}">等待审批</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='1'}">已通过</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='2'}">已驳回</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='3'}">已转交</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='4'}">已撤销</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='6'}">卖家驳回</c:when>
				<c:when test="${!pocess.startIndext && pocess.status=='7'}">重新申请</c:when>
			</c:choose>
			<span></span>
		</p>
		<div class='clear'>
			<h4>${pocess.userName}&nbsp;${pocess.startDateFormat}</h4>
			<c:if test="${urge && pocess.startIndext && pocess.status=='0'}">
				<a href="javascript:;" onclick="urge('${tradeHeader.id}');" class="layui-btn layui-btn-normal layui-btn-mini">
					<i class="layui-icon"></i>催办
				</a>
			</c:if>
			<c:if test="${pocess.remark != null && pocess.remark != ''}">
				<p>(${pocess.remark})</p>
			</c:if>
			<c:if test="${pocess.fileList != null && pocess.fileList.size()>0}">
				<c:forEach var="fileObj" items="${pocess.fileList}">
					<p><a href="javascript:void(0)" onclick="downLoad('${fileObj.fileUrl}')">${fileObj.fileName}</a></p>
				</c:forEach>
			</c:if>
			<span><fmt:formatDate value="${pocess.endDate}" type="both" />
			</span>
		</div>
	</div>
</c:if>
</c:forEach>
<!--抄送人-->
<c:if test="${copyList != null && copyList.size() > 0}">
<div class="copyDiv" style="padding-top: 10px;">
	<div class="CC">
		<p class="size_lg c66">
			抄送人 <span class="size_md">(${copyType})</span>
		</p>
		<div>
			<ul class="CCList">
				<c:forEach items="${copyList}" var="copy">
					<li><span class="userSpan">${copy.userName}</span>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
</c:if>