<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%@ include file="../../../common/path.jsp"%>
<script>
$(function(){
	//切换收货地址
	$('.addr>div').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$("input[name='is_since']").change(function(){
		if($(this).is(":checked")){
			$(".addr").hide();
			$(".addr_add").hide();
		}else{
			$(".addr").show();
			$(".addr_add").show();
		}
	});
});
//返回购物车修改
function backAddOrder(){
	$.ajax({
		url:"platform/buyer/purchase/addOrder",
		data:{
			"supplierId":$("#nextOrder input[name='supplierId']").val(),
			"supplierName":$("#nextOrder input[name='supplierName']").val(),
			"sellerPerson":$("#nextOrder input[name='sellerPerson']").val(),
			"sellerPhone":$("#nextOrder input[name='sellerPhone']").val(),
			"numSum":$("#nextOrder input[name='numSum']").val(),
			"totalPrice":$("#nextOrder input[name='totalPrice']").val(),
			"goodsStr":$("#nextOrder input[name='goodsStr']").val()
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//修改地址
function updateAddress(obj){
	var parentDiv = $(obj).parent().parent();
	$.ajax({
		type: "POST",
		url: "platform/address/updateAddress",
		data:{
			"id":parentDiv.find("input[name='addressId']").val()
		},
		success:function(data){
			var str = data.toString();
			layer.open({
			    type: 1,
			    title:'修改收货地址',
			    skin: 'pop',
			    closeBtn:2,
			    area: ['770px', 'auto'],
			    btn:['保存','关闭'],
			    content:str,
			    yes: function(index, layero){
			  		var isDefault = 1;
	  			  	if($("#updateIsDefault").is(":checked")){//选中  
	  			  		isDefault = 0
	  			  	}
				    // 确定保存
	  			  	$.ajax({
	  					type : "POST",
	  					url : "platform/address/saveUpdateAddress",
	  					async: false,
	  					data : {
	  						"id" : $("#updateId").val(),
	  						"personName" : $("#updatePersonName").val(),
	  						"province" : $("#updateProvince").val(),
	  						"city" : $("#updateCity").val(),
	  						"area" : $("#updateArea").val(),
	  						"addrName" : $("#updateAddrName").val(),
	  						"phone" : $("#updatePhone").val(),
	  						"zone" : $("#updateZone").val(),
	  						"telNo" : $("#updateTelNo").val(),
	  						"isDefault" : isDefault
	  					},
	  					success : function(data) {
	  						var result = eval('(' + data + ')');
	  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  						if(resultObj.success){
	  							layer.close(index);
	  							ResetAddress(resultObj.address,parentDiv);
	  						}else{
	  							layer.msg(resultObj.msg,{icon:2});
	  							return false;
	  						}
	  					},
	  					error : function() {
	  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  					}
	  				});
			  	}
		  	});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function addAddress(){
	$.ajax({
		type: "POST",
  		url: "platform/address/addAddress",
  		success: function(data){
  			layer.open({
  			    type: 1,
		      	title:'新增收货地址',
		      	skin: 'pop',
		      	closeBtn:2,
		      	area: ['770px', 'auto'],
		      	btn:['保存','关闭'],
  			    content:data,
  			  	yes: function(index, layero){
  			  		var isDefault = 1;
	  			  	if($("#isDefault").is(":checked")){//选中  
	  			  		isDefault = 0
	  			  	}
  			    	// 确定保存
	  			  	$.ajax({
	  					type : "POST",
	  					url : "platform/address/saveAddAddress",
	  					async: false,
	  					data : {
	  						"personName" : $("#personName").val(),
	  						"province" : $("#province").val(),
	  						"city" : $("#city").val(),
	  						"area" : $("#area").val(),
	  						"addrName" : $("#addrName").val(),
	  						"phone" : $("#phone").val(),
	  						"zone" : $("#zone").val(),
	  						"telNo" : $("#telNo").val(),
	  						"isDefault" : isDefault
	  					},
	  					success : function(data) {
	  						var result = eval('(' + data + ')');
	  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  						if(resultObj.success){
	  							layer.close(index);
	  							ResetAddress(resultObj.address,null);
	  						}else{
	  							layer.msg(resultObj.msg,{icon:2});
	  							return false;
	  						}
	  					},
	  					error : function() {
	  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  					}
	  				});
  			  	}
  			});
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
 	});
}
//重置收货地址
function ResetAddress(resultObj,divObj){
	var divStr = "";
	if(resultObj.isDefault==0){
		divStr += "<img src=\"<%=basePath%>statics/platform/images/default_small_b.jpg\" class=\"defaultAddressImg\">";
	}
	divStr += "<div class='borderBg'></div>";
	divStr += "<div class='addr_hd'>";
	divStr += "<span>"+resultObj.provinceName+"</span>";
	divStr += "<span><b>"+resultObj.cityName+"</b></span>";
	divStr += " （<span>"+resultObj.personName+"</span> 收）";
	divStr += "</div>";
	divStr += "<div class='addr_bd'>";
	divStr += "<span>"+resultObj.areaName+"</span>";
	divStr += " <span>"+resultObj.addrName+"</span>";
	divStr += " <span>"+resultObj.phone+"</span>";
	divStr += "</div>";
	divStr += "<div class='addr_md text-right'>";
	divStr += "<span class='addr_modify' onclick='updateAddress(this);'><i class='layui-icon'>&#xe691;</i>修改</span>";
	divStr += "</div>";
	divStr += "<input type='hidden' name='addressId' value='"+resultObj.id+"'>";
	if(divObj==null||divObj.length==0){
		var divId=new Date().getTime();
		divObj = $("<div class='addrItem '>",{id:divId});
		divObj.append(divStr);
		$(".addr>.addrItem:last").after(divObj);
		divObj.addClass('selected').siblings().removeClass('selected');
	}else{
		divObj.empty();
		divObj.append(divStr);
		divObj.addClass('selected').siblings().removeClass('selected');
	}
	$('.addr>div').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	if(resultObj.isDefault==0){
		$(".isDefault .defaultAddressImg").remove();
		$(".isDefault").removeClass("isDefault");
		divObj.addClass("isDefault");
	}
}
function saveOrder(){
	//收货地址判空
	var is_since = "1";
	if(!$("input[name='is_since']").is(":checked")){
		is_since = "0";
		if($('.addr>div.selected').length!=1){
			layer.msg("请选择收货地址！",{icon:2});
			return;
		}
	}
	var url = "platform/buyer/purchase/saveOrderInsert";
	$.ajax({
		url:url,
		async:false,
		data:{
			"supplierId":$("#nextOrder input[name='supplierId']").val(),
			"supplierName":$("#nextOrder input[name='supplierName']").val(),
			"sellerPerson":$("#nextOrder input[name='sellerPerson']").val(),
			"sellerPhone":$("#nextOrder input[name='sellerPhone']").val(),
			"numSum":$("#nextOrder input[name='numSum']").val(),
			"totalPrice":$("#nextOrder input[name='totalPrice']").val(),
			"goodsStr":$("#nextOrder input[name='goodsStr']").val(),
			"supplierRemark":$("#nextOrder textarea[name='supplierRemark']").val(),
			"addressId":$("#nextOrder .addr>div.selected input[name='addressId']").val(),
			"is_since":is_since,
			"menuName":"17070718165043234845"
		},
		success:function(data){
			if(data.flag){
				var res = data.res;
				if(res.code==40000){
					//调用成功
					saveSucess();
				}else if(res.code==40010){
					//调用失败
					layer.msg(res.msg,{icon:2});
					return false;
				}else if(res.code==40011){
					//需要设置审批流程
					layer.msg(res.msg,{time:500,icon:2},function(){
						setApprovalUser(url,res.data,function(data){
							saveSucess(data);
						});
					});
					return false;
				}else if(res.code==40012){
					//对应菜单必填
					layer.msg(res.msg,{icon:2});
					return false;
				}else if(res.code==40013){
					//不需要审批
					notNeedApproval(res.data,function(data){
						saveSucess(data);
					});
					return false;
				}
			}else{
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
				return false;
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//保存成功
function saveSucess(ids){
	$.ajax({
		url:"platform/buyer/purchase/success",
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div id="nextOrder">
	<div class="progress_o">
		<span class="cart"><b></b> 我的购物车</span>
		<span class="order_pr current"><b></b> 填写核对订单信息</span>
		<span class="order_s"><b></b> 成功提交订单 </span>
	</div>
	<h4 class="page_title mt">供应商：${nextData.supplierName}</h4>
	<!--收货信息-->
	<h4 class="no_logistics">
		选择收货地址
		<span class="since"><input type="checkbox" name="is_since">不需要物流，自提货物</span>
	</h4>
	<div class="addr addr_buyer">
		<c:forEach var="address" items="${addressList}" varStatus="status">
			<div class="addrItem <c:if test='${address.isDefault==0}'>isDefault selected</c:if>" id="addr_${status.count}">
				<c:if test="${address.isDefault==0}">
					<img src="<%=basePath%>statics/platform/images/default_small_b.jpg" class="defaultAddressImg">
				</c:if>
				<div class="borderBg"></div>
				<div class="addr_hd">
					<span>${el:getProvinceById(address.province).province}</span>
					<span><b>${el:getCityById(address.city).city}</b></span> （<span>${address.personName}</span> 收）
				</div>
				<div class="addr_bd">
					<span>${el:getAreaById(address.area).area}</span> <span>${address.addrName}</span> <span>${address.phone}</span>
				</div>
				<div class="addr_md text-right">
					<span class="addr_modify" onclick="updateAddress(this);"><i class="layui-icon">&#xe691;</i>修改</span>
				</div>
				<input type="hidden" name="addressId" value="${address.id}">
			</div>
		</c:forEach>
		<div class="addrAdd text-center" id="addr_new" onclick="addAddress();">
          <img src="<%=basePath%>statics/platform/images/addr_a.jpg">
          <p><b>新增收货地址</b></p>
        </div>
	</div>
	<p class="saleList">
		<b>采购清单</b> <a href="javascript:void(0)" onclick="backAddOrder();"
			class="layui-btn layui-btn-mini layui-btn-danger rt"><i
			class="layui-icon">&#xe691;</i> 返回购物车修改</a>
	</p>
	<table class="table_pure commodities_list mt">
		<thead>
			<tr>
				<td style="width:35%">商品</td>
				<td style="width:7%">单位</td>
				<td style="width:7%">仓库</td>
				<td style="width:20%">备注</td>
				<td style="width:7%">申请数量</td>
				<td style="width:7%">单价</td>
				<td style="width:7%">总价</td>
				<td style="width:10%">是否含发票</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${nextData.itemArray}">
				<tr>
					<td>${item.proCode}|${item.proName}|${item.skuCode}|${item.skuName}|${item.skuOid}</td>
					<td>${item.unitName}</td>
					<td>${item.wareName}</td>
					<td>${item.remark}</td>
					<td>${item.num}</td>
					<td>${item.price}</td>
					<td>${item.priceSum}</td>
					<td>
						<c:choose>
							<c:when test="${item.isNeedInvoice=='N'}">否</c:when>
							<c:otherwise>是</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4">合计</td>
				<td class="text-center fw">${nextData.numSum}</td>
				<td>-</td>
				<td class="text-center fw">${nextData.totalPrice}</td>
				<td>-</td>
			</tr>
		</tfoot>
	</table>
	<div class="size_sm">
      <p class="ms">给供应商留言：</p>
      <textarea placeholder="可以告诉供应商您的特殊要求" class="orderRemark" name="supplierRemark"></textarea>
    </div>
	<div class="text-center">
		<a href="javascript:void(0)"><button class="next_step" onclick="saveOrder();">提交订单</button></a>
	</div>
	<input type="hidden" value="${params.supplierId}" name="supplierId">
	<input type="hidden" value="${params.supplierName}" name="supplierName">
	<input type="hidden" value="${params.sellerPerson}" name="sellerPerson">
	<input type="hidden" value="${params.sellerPhone}" name="sellerPhone">
	<input type="hidden" value="${params.numSum}" name="numSum">
	<input type="hidden" value="${params.totalPrice}" name="totalPrice">
	<input type="hidden" value="${params.goodsStr}" name="goodsStr">
</div>