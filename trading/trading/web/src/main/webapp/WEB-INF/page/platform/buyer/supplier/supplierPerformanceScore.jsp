<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script> 
<script type="text/javascript" src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<style>
ul li {
	padding: 3px;
}
</style>
<div>
	<form  class="layui-form" action="platform/buyer/supplier/loadSupplierPerformanceScoreHtml">
		<ul class="order_search">
			<li class="state">
            <label>考核名称：</label>
            <div class="layui-input-inline">
              <select name="assesmentId" lay-filter="aihao">
					<c:forEach items="${assesmentList }" var="item">
						<option value="${item.id }" <c:if test="${pmap.assesmentId==item.id }">selected="selected"</c:if>>${item.assessmentName }</option>
					</c:forEach>
              </select>
            </div>
          </li>
			<li class="state"><label>起始时间：</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" value="${pmap.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
					-
				<div class="layui-input-inline">
					<input type="text" name="endDate" value="${pmap.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
			</li>
			<li class="range"><label>供应商名称：</label> 
				<input type="text" placeholder="输入供应商名称" name="suppName" value="${pmap.suppName}">
			</li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="exportData('supplierPerformanceScoreTable','供应到绩效考核得分');">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
		</ul>
	</form>
	<c:choose>
		<c:when test="${fn:length(msg)>0 }">
			<div class="btn_p text-center">
				<p style="color: red;text-align: center;margin-bottom: 10px;font-size: 20px;">${msg}</p>
				<span class="order_p" onclick="leftMenuClick(this,'platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml','buyer','18010209272828100705');">新增绩效考核</span>
			</div>
		</c:when>
		<c:otherwise>
			<table class="table_pure assesmentList" id="supplierPerformanceScoreTable">
		          <thead>
		          <tr>
		            <td>供应商名称</td>
		            <td>考核总分</td>
		            <td>考核得分</td>
		            <td>考核项</td>
		            <td>得分</td>
		          </tr>
		          </thead>
		          <tbody> 
		          <c:forEach var="item" items="${performanceMap}" varStatus="status">
		          	<tr>
		              <td>${item.value.supplierName}</td>
		              <td>${item.value.score}</td>
		              <td><fmt:formatNumber type="number" value="${item.value.compleScore + item.value.arrivalScore + item.value.qualityScore +item.value.afterScore}" pattern="0.00" maxFractionDigits="2"/></td>
		              <td><ul>
		              		<li>到货及时率 </li>
		              		<li>计划完成率</li>
		              		<li>到货合格率</li>
		              		<li>售后响应及时率</li>
		              	  </ul>
		              </td>
		              <td><ul>
		              		<li><fmt:formatNumber type="number" value="${item.value.arrivalScore}" pattern="0.00" maxFractionDigits="2"/></li>
		              		<li><fmt:formatNumber type="number" value="${item.value.compleScore}" pattern="0.00" maxFractionDigits="2"/></li>
		              		<li><fmt:formatNumber type="number" value="${item.value.qualityScore}" pattern="0.00" maxFractionDigits="2"/></li>
		              		<li><fmt:formatNumber type="number" value="${item.value.afterScore}" pattern="0.00" maxFractionDigits="2"/></li>
		              	  </ul>
		              </td>
		            </tr>
		          </c:forEach>
		          </tbody>
			</table>
		</c:otherwise>
	</c:choose>
</div>