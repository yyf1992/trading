<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
function insert(){
	$.ajax({
		url:basePath+"platform/sysbank/loadBankInsert",
		type:"get",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				content:data,
				title:'新建付款账户',
				skin:'layui-layer-rim',
				area:['400px','auto'],
				btn:['确定','取消'],
				yes:function(index,layerio){
					$.ajax({
						url:basePath+"platform/sysbank/saveBankInsert",
						type:"post",
						async:false,
						data:$("#addSysBankForm").serialize(),
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}

function editAccount(id){
		$.ajax({
		url:basePath+"platform/sysbank/loadEditbank",
		type:"post",
		data:{"id":id},
		async:false,
		success:function(data){
			layer.open({
				type:1,
				content:data,
				title:'修改账户',
				skin:'layui-layer-rim',
				area:['400px','auto'],
				btn:['确定','取消'],
				yes:function(index,layerio){
					$.ajax({
						url:basePath+"platform/sysbank/saveBankEdit",
						type:"post",
						async:false,
						data:$("#editSysBankForm").serialize(),
						success:function(data){
							var result=eval('('+data+')');
							var resultObj=isJSONObject(result)?result:eval('('+result+')');
							if(resultObj.success){
								layer.close(index);
								layer.msg(resultObj.msg,{icon:1});
								leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
							}else{
								layer.msg(resultObj.msg,{icon:2});
								leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
							}
						},
						error:function(){
							layer.msg("获取数据失败，稍后重试！",{icon:2});
						}
					});
				}
			});
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="content_modify">
	<div class="content_role">
		<form  class="layui-form" action="platform/sysbank/loadSysBankList">
		        <ul class="order_search platformSearch">
		          <li class="state">
		            <label>户名：</label>
		            <input type="text" placeholder="输入户名" name="accountName" value="${searchPageUtil.object.accountName}">
		          </li>
		          <li class="state">
		            <label>加入日期：</label>
		            <div class="layui-input-inline">
		              <input type="text" name="createDate" value="${searchPageUtil.object.createDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
		            </div>
		          </li>
		          <li class="state">
						<label>状态:</label>
						<div class="layui-input-inline">
							<select id="status" name="status" lay-filter="aihao">
							<option value="-1" <c:if test="${searchPageUtil.object.status=='-1'}">selected="selected"</c:if>>全部</option>
							<option value="0" <c:if test="${searchPageUtil.object.status==0}">selected="selected"</c:if>>正常</option>
							<option value="1" <c:if test="${searchPageUtil.object.status==1}">selected="selected"</c:if>>禁用</option>
							</select>
						</div>
		          </li>
		          <li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
		        </ul>
		  <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		  <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		</form>
		<div class="mt">
			<a href="javascript:void(0)" button="新增"
				class="layui-btn layui-btn-add layui-btn-small rt"
				onclick="insert();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
        <table class="table_pure bankAccountList">
          <thead>
          <tr>
            <td style="width:5%">编号</td>
            <td style="width:25%">开户行</td>
            <td style="width:20%">账号</td>
            <td style="width:10%">户名</td>
            <td style="width:15%">状态</td>
            <td style="width:15%">创建日期</td>
            <td style="width:10%">操作</td>
          </tr>
          </thead>
          <tbody>
          <c:forEach var="SysBank" items="${searchPageUtil.page.list}" varStatus="status">
          	<tr>
              <td>${status.count}</td>
              <td>${SysBank.openBank}</td>
              <td>${SysBank.bankAccount}</td>
              <td>${SysBank.accountName}</td>
              <td>
				<c:choose>
					<c:when test="${SysBank.isDel==0}">正常</c:when>
					<c:when test="${SysBank.isDel==1}">禁用</c:when>
				</c:choose>
              </td>
              <td><fmt:formatDate value="${SysBank.createDate}" type="both"/></td>
              <td>
              	<a href="javascript:void(0)" button="修改"
					onclick="editAccount('${SysBank.id}');"
					class="layui-btn layui-btn-update layui-btn-mini">
					<i class="layui-icon">&#xe691;</i>修改</a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
        <div class="pager">${searchPageUtil.page}</div>
      </div>
</div>