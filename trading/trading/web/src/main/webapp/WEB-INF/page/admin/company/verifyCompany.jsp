<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$("#verifyCompanyForm").validate({
	rules:{
		name:{
			required: true,
			maxlength:20
		},
		ismenu:{
			required: true
		},
		button:{
			required: true
		}
	},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.form-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.form-group').removeClass('error');
		$(element).parents('.form-group').addClass('success');
	},
	submitHandler : function(){
		$.ajax({
		  	url : "admin/adminCompany/saveVerifyCompany",
		  	type: "post",
		  	data:$("#verifyCompanyForm").serialize(),
		  	async:false,
		  	success:function(data){
		  		var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					loadAdminData();
					//阻止表单提交
					return false;
				} else {
					layer.msg(resultObj.msg,{icon:2});
					//阻止表单提交
					return false;
				}
		  	},
		  	error:function(){
		  		layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  		//阻止表单提交
				return false;
		  	}
		});
	}
});
</script>
<form class="form-inline " id="verifyCompanyForm" novalidate="novalidate">
	<input type="hidden" name="id" value="${company.id}">
	<div class="modal-content" style="width: 600px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">系统公司审核</h4>
		</div>
		<div class="modal-body row-fluid ">
			<div class=" padding-top-sm row">
					<div class="form-group col-md padding-top-sm">
						<label class="label-two">状态：</label>
						<input type="radio" name="status" checked value="1"/>通过
						<input type="radio" name="status" value="2"/>驳回
					</div>
					<div class="form-group form-group-textarea col-md-12 padding-top-sm">
						<label>审核意见：</label>
						<textarea class="form-control" id="reason" name="reason" rows="3" style="width:76% !important"></textarea>
					</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
			<button type="submit" class="btn btn-sm2 btn-00967b" id="saveButton">保存</button>
		</div>
	</div>
</form>
