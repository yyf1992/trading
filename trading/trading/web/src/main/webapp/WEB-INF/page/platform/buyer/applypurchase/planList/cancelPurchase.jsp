<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:90%">
			<ul>
				<li style="width:360px">商品</li>
				<li style="width:100px">采购数量</li>
				<li style="width:150px">销售计划|入仓量</li>
				<li style="width:80px">本月销量</li>
				<li style="width:80px">日均销量</li>
				<li style="width:80px">杉橙库存</li>
				<li style="width:80px">外仓库存</li>
				<li style="width:80px">合计库<br/>存数量</li>
				<li style="width:80px">现需求<br/>差异量</li>
				<li style="width:80px">在途订<br/>单数量</li>
				<li style="width:80px">预估下<br/>月到货</li>
				<li style="width:80px">预估下<br/>月库存</li>
				<li style="width:80px">备注</li>
			</ul>
		</td>
		<td style="width:5%">状态</td>
		<td style="width:5%">操作</td>
	</tr>
</table>
<c:forEach var="applyPurchase" items="${searchPageUtil.page.list}">
	<div class="order_list">
		<p>
			<span class="layui-col-sm2" style="width: 300px">
				采购计划单号:<b>${applyPurchase.applyCode}</b>
			</span>
			<span class="layui-col-sm3" style="width: 300px">
				标题:<b>${applyPurchase.title}</b>
			</span>
			<span class="layui-col-sm3" style="width: 300px">
				提交日期:
				<b><fmt:formatDate value="${applyPurchase.createDate}" type="both"></fmt:formatDate></b>
			</span>
			<span class="layui-col-sm3" style="width: 300px">
				要求到货日期:<b><fmt:formatDate value="${applyPurchase.predictarred}" type="date"></fmt:formatDate></b>
			</span>
			<span class="layui-col-sm1" title="${applyPurchase.remark}">
				申请人:<b>${applyPurchase.createName}</b>
			</span>
		</p>
		<table>
			<tr>
				<td style="width:90%">
					<c:forEach var="product" items="${applyPurchase.itemList}">
						<ul class="clear" style="padding: 5px">
							<li style="width:340px">
								${product.productCode}|${product.barcode}|${product.productName}|${product.skuCode}|${product.skuName}
							</li>
							<li style="width:100px">${product.applyCount}</li>
							<li style="width:150px;">${product.confirmSalePlan}|${product.confirmPutStorageNum}</li>
							<li style="width:80px">${product.monthSaleNum}</li>
							<li style="width:80px">${product.monthDms}</li>
							<li style="width:80px">${product.scStock}</li>
							<li style="width:80px">${product.outStock}</li>
							<li style="width:80px">${product.totalStock}</li>
							<li style="width:80px">${product.differenceNum}</li>
							<li style="width:80px">${product.transitNum}</li>
							<li style="width:80px">${product.predictNextMonthArrival}</li>
							<li style="width:80px">${product.predictNextMonthArrival}</li>
							<li style="width:80px" title="${product.remark}">${product.remark}</li>
						</ul>
						<table style="width: 100%">
							<thead>
								<tr style="background: #F7F7F7">
									<td>销售计划单号</td>
									<td style="padding-top: 3px;">部门</td>
									<td style="padding-top: 3px;">销售计划</td>
									<td>入仓量</td>
									<td>销售周期</td>
								</tr>
							<c:forEach items="${product.shopList}" var="shopItem">
								<tr>
									<td>${shopItem.planCode}</td>
									<td style="padding-top: 3px;">${shopItem.shopName}</td>
									<td style="padding-top: 3px;">${shopItem.confirmSalesNum}</td>
									<td>${shopItem.confirmPutStorageNum}</td>
									<td>
										<fmt:formatDate value="${shopItem.saleStartDate}" type="date"></fmt:formatDate> 至 <fmt:formatDate value="${shopItem.saleEndDate}" type="date"></fmt:formatDate>
									</td>
								</tr>
							</c:forEach>
							</thead>
						</table>
					</c:forEach>
				</td>
				<td style="width:5%" class="operate">
					<div>
						<c:choose>
							<c:when test="${applyPurchase.status==0}">等待审核</c:when>
							<c:when test="${applyPurchase.status==1}">审核通过</c:when>
							<c:when test="${applyPurchase.status==2}">审核不通过</c:when>
							<c:when test="${applyPurchase.status==3}">已取消</c:when>
						</c:choose>
					</div>
					<div class="opinion_view">
						<span class="orange" data="${applyPurchase.id}">查看审批流程</span>
					</div>
				</td>
				<td style="width:5%" class="operate">
					<a href="javascript:void(0)" button="详情"
						onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/loadApplyPurchaseDetails?id=${applyPurchase.id}','buyer','17112209483332813171');"
						class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>详情</a>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>