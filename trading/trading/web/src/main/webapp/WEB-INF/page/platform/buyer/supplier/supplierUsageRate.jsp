<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script>
<script type="text/javascript" src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<script type="text/javascript">
var supplierList = <%=request.getAttribute("supplierList")%>;
</script>
<script src="<%=basePath%>statics/platform/js/echarts.min.js"></script>
<script src="<%=basePath%>statics/platform/js/echarts.js"></script>
<div>
	<form class="layui-form" action="platform/buyer/supplier/loadSupplierUsageRateHtml">
		<ul class="order_search">
			<li class="state"><label>起始时间：</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" value="${map.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
					-
				<div class="layui-input-inline">
					<input type="text" name="endDate" value="${map.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
			</li>
			<li class="range"><label>供应商名称：</label> 
				<input type="text" placeholder="输入供应商名称" name="suppName" value="${map.suppName}">
			</li>
			<li class="range"><label>商品名称：</label> 
				<input type="text" placeholder="输入商品名称" name="productName" value="${map.productName}">
			</li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="supplierUsageRateDate" onclick="exportData('supplierUsageRateTable','供应商使用率');">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
		</ul>
	</form>
	<table class="table_pure supplierList" id="supplierUsageRateTable">
		<thead>
			<tr>
				<td>供应商名称</td>
				<td>下单次数</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${supplierList}" varStatus="status">
				<tr>
					<td>${item.supp_name}</td>
					<td>${item.order_num}</td>
				</tr>
			</c:forEach>
			<tr style="color: blue">
					<td>下单总数：</td>
					<td><c:out value="${sum}" default="0"></c:out></td>
			</tr>
		</tbody>
	</table>
	<div id="echartsPie" style="width: 100%;height:500px;"></div>
</div>