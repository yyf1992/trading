<%@ page language="java" import="java.util.*,com.nuotai.trading.utils.ShiroUtils" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="basePath" value="<%=basePath %>" />
<!--当前登录用户ID-->
<c:set var="userId" value="<%=ShiroUtils.getUserId() %>" />
<!--当前登录用户公司ID-->
<c:set var="companyId" value="<%=ShiroUtils.getCompId() %>" />
<script type="text/javascript">
    var basePath = "${basePath}";
    var loginUserId = "${userId}";
    var loginCompanyId = "${companyId}";
    var menuListAttribute = <%=ShiroUtils.getSessionAttribute("AUTHORITYSYSMENU")%>;
	var menuArray = JSON.parse(JSON.stringify(menuListAttribute));
</script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="${basePath}statics/css/layui-icon-font.css">
<link rel="stylesheet" href="${basePath}statics/X-admin2.0/css/iconfont-font.css">
<link rel="stylesheet" href="${basePath}statics/X-admin2.0/css/xadmin.css">

<!-- js -->
<script src="${basePath}statics/X-admin2.0/js/jquery.min.js"></script>
<script src="${basePath}statics/X-admin2.0/lib/layui/layui.js" charset="utf-8"></script>
<script src="${basePath}statics/X-admin2.0/js/xadmin.js"></script>
<script src="${basePath}statics/X-admin2.0/js/menu.js"></script>
<script src="${basePath}statics/dingding/js/base64.js"></script>