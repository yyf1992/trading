<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%--<%@ include file="../../common/common.jsp"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<head>
	<title>产品互通</title>
	<script type="text/javascript">
        var linktype='${linktype}';
        layui.use('form', function(){
        	//重新渲染select控件
            var form = layui.form;
            form.render("select");
            //右侧我的好友
            $('.switch>span').click(function(){
                $('.switch>span.open').toggle();
                $('.switch>span.close').toggle();
                $('#side_pro').toggleClass('show');
            });
            $("#searchMore").click(function(){
                $(this).toggleClass("clicked");
                $(this).parent().nextAll("ul").toggle();
            });
            //初始化数据
            selectData();
        });
    	function selectData(){
    		var formObj = $("#productLinkListContent").find("form");
    		$.ajax({
    			url : basePath+"platform/product/productLinkList?linktype=0",
    			data:formObj.serialize(),
    			async:false,
    			success:function(data){
    				var str = data.toString();
    				$("#tabContent").html(str);
    				loadVerify();
    			},
    			error:function(){
    				layer.msg("获取数据失败，请稍后重试！",{icon:2});
    			}
    		});
    		//获得神审批流程
    		loadVerify();
    	}
        //标签页改变
        function setStatus(obj,status) {
        	$("#status").val(status)
            $('.tab a').removeClass("hover");
            $(obj).addClass("hover");
           // loadProductLinkData();
    		//初始化数据
        	selectData();
        }

        //价格修改确认
        function priceUpdConfirm(buyShopProductId){
            layer.open({
                type: 1,
                title: '价格确认',
                area: ['420px', 'auto'],
                skin:'change',
                closeBtn: 2,
//                shade: 0.8,
                id: 'LAY_layuipro', //设定一个id，防止重复弹出
                btn: ['确认', '取消'],
                moveType: 1, //拖拽模式，0或者1
                content: '<div style="padding: 30px; line-height: 20px;  font-weight: 300;">确认修改后的价格?</div>',
                yes : function(index){
                    var	updPrice = $("#updPrice").val();
                    $.ajax({
                        type : "POST",
                        url : "platform/product/comfirmShopProductPriceUpd",
                        data: {
                            "buyShopProductId": buyShopProductId
                        },
                        async:false,
                        success:function(data){
                            layer.close(index);
                            layer.msg("确认成功！",{icon:1},function(){
                                loadProductLinkData();
                            });
                        },
                        error:function(){
                            layer.msg("操作失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        }
        //审批
        function productLinkSellerAprove(id) {
            $.ajax({
                type : "POST",
                url : "platform/product/productLinkSellerAprove",
                data: {
                    "id": id
                },
                async:false,
                dataType: "json",
                success:function(data){
                    var result=eval('('+data+')');
                    var resultObj=isJSONObject(result)?result:eval('('+result+')');
                    if(resultObj.success){
                        layer.msg("确认成功！",{icon:1},function(){
                            loadProductLinkData();
                        });
                    }else{
                        layer.msg(data.msg,{icon:1});
                    }

                },
                error:function(){
                    layer.msg("操作失败，请稍后重试！",{icon:2});
                }
            });
        }
        //提交审批成功页面
        function saveSucess(res){
            layer.msg('提交审批成功！',{icon:2});
            $("#searchBtn").click();
        }
        //列表数据查询
        function loadProductLinkData() {
            layer.load();
            var contentObj = $(".content");
            var formObj = contentObj.find("form");
            $.ajax({
                url : formObj.attr("action"),
                data:formObj.serialize(),
                async:false,
                success:function(data){
                    layer.closeAll('loading');
                    var str = data.toString();
                    contentObj.empty();
                    contentObj.html(str);
                },
                error:function(){
                    layer.closeAll('loading');
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        }
      //重置
        function recovery(){
        	$("#productCode").val("");
        	$("#productName").val("");
        	$("#skuCode").val("");
        	$("#skuName").val("");
        	$("#barcode").val("");
        	$("#linkCompany").val("");
        }
	</script>
	<style>
		.text-center td{
		 word-wrap:break-word;
		 word-WRBP: break-word;
		}
	</style>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" id="" onclick="setStatus(this,'')" <c:if test="${searchPageUtil.object.status eq ''}">class="hover"</c:if>>所有（<span>${allCount}</span>）</a> <b>|</b>
    <a href="javascript:void(0);" id="1" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.status eq 1}">class="hover"</c:if>>待内部审批（<span>${internalCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" id="2" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.status eq 2}">class="hover"</c:if>>内部审批被拒绝（<span>${externalCount}</span>）</a> <b>|</b>	
	<a href="javascript:void(0);" id="3" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.status eq 3}">class="hover"</c:if>>已通过确认（<span>${okCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" id="3" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.status eq 4}">class="hover"</c:if>>已取消（<span>${cancelCount}</span>）</a> <b>|</b>	
</div>
<div id="productLinkListContent">
	<!--搜索栏-->
	<form id="layui-form" action="platform/product/loadProductList">
		<ul class="order_search interwork_search">
			<li>
				<label>商品货号:</label>
				<input type="text" placeholder="商品货号" name="productCode" id="productCode" value="${searchPageUtil.object.productCode}"/>
			</li>
			<li>
				<label>
					<c:choose>
						<c:when test="${linktype==0}">关联供应商:</c:when>
						<c:otherwise>&nbsp;关联客户:</c:otherwise>
					</c:choose>
				</label>
				<input type="text" name="linkCompany" id="linkCompany" value="${searchPageUtil.object.linkCompany}" placeholder="关联公司"/>
			</li>
			<li>
				<label>规格代码:</label>
				<input type="text" placeholder="规格代码" name="skuCode" id="skuCode" value="${searchPageUtil.object.skuCode}"/>
			</li>
			<li>
				<label>规格名称:</label>
				<input type="text" placeholder="规格名称" name="skuName" id="skuName" value="${searchPageUtil.object.skuName}"/>
			</li>
			<%-- <li>
				<label>状&emsp;&emsp;态:</label>
				<div class="layui-input-inline">
					<select id="status" name="status" lay-filter="aihao">
						<option value="" <c:if test="${searchPageUtil.object.status eq ''}">selected="selected"</c:if>>全部</option>
						<c:if test="${linktype=='0'}">
							<option value="1" <c:if test="${searchPageUtil.object.status eq 1}">selected="selected"</c:if>>待内部审批</option>
						</c:if>
						<c:if test="${linktype=='0'}">
							<option value="2" <c:if test="${searchPageUtil.object.status eq 2}">selected="selected"</c:if>>内部审核被拒绝</option>					
						</c:if>
						<c:if test="${linktype=='1'}">
							<option value="2" <c:if test="${searchPageUtil.object.status eq 2}">selected="selected"</c:if>>待内部审批</option>
						</c:if>
						<option value="3" <c:if test="${searchPageUtil.object.status eq 3}">selected="selected"</c:if>>已通过确认</option>
					</select>
				</div>
			</li> --%>
			<li>
				<label>条&emsp;形&emsp;码:</label>
				<input type="text" placeholder="条形码" name="barcode" id="barcode" value="${searchPageUtil.object.barcode}"/>
			</li>
			<li class="range">
				<button type="button" class="search" onclick="loadProductLinkData();">搜索</button>
				<button type="button" class="search" onclick="recovery();">重置</button>		
			</li>
		</ul>
		<!-- 分页隐藏数据 -->
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<!--关联类型-->
		<input id="linktype" name="linktype" type="hidden" value="${linktype}" />	
		<input id="status" name="status" type="hidden" value="${searchPageUtil.object.status}" />	
	</form>
	<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small rt" 
		onclick="leftMenuClick(this,'platform/product/addProductLink?linktype='+linktype,'17112910033928502412')">
		<i class="layui-icon">&#xebaa;</i>新增商品关联
	</a>
    <div id="tabContent"></div>
	<!--商品价格变更审批-->
	<div class="plain_frame price_change" id="updPriceDiv" style="display: none;">
		<ul>
			<li>
				<span>商品价格:</span>
				<input type="number" id="updPrice" name="updPrice"  min="0" >
			</li>
			<li>
				<span>修改理由:</span>
				<input type="text" id="modifyReason" name="modifyReason">
			</li>
		</ul>
	</div>
	<!-- 历史价格 -->
	<div class="alterPriceHistory" id="alterPriceHistory" style="display: none;"></div>
</div>
