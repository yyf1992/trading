<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$("input[name='checkAll']").click(function(){
	var checked = $(this).is(":checked");
	$("input[name='checkone']").prop("checked",checked);
});
function deleteVerify(id,status){
	$.ajax({
		type : "POST",
		url:"platform/sysVerify/deleteVerify",
		data:{
			"id":id,
			"status":status
		},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				loadPlatformData();
				layer.msg(resultObj.msg,{icon:1});
			}else{
				layer.msg(resultObj.msg,{icon:2});
				return false;
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div>
	<form action="platform/sysVerify/sysVerifyList" id="searchForm">
		<ul class="order_search supplier_query">
			<li class="comm">
                <label>审批标题:</label>
                <input type="text" placeholder="输入审批标题" name="title" value="${searchPageUtil.object.title}" >
			</li>
			<li class="nomargin">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
        </ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<div class="mt">
			<a href="javascript:void(0)" button="新增"
				onclick="leftMenuClick(this,'platform/sysVerify/loadAddHtml','system')"
				class="layui-btn layui-btn-add layui-btn-small rt">
				<i class="layui-icon">&#xebaa;</i> 添加</a>
		</div>
	<table class="table_pure supplierList mt">
		<thead>
			<tr>
				<td style="width:5%"><input type="checkbox" name="checkAll"></td>
				<td style="width:15%">审批标题</td>
                <td style="width:15%">对应菜单</td>
                <td style="width:10%">审批类型</td>
                <td style="width:5%">状态</td>
                <td style="width:8%">创建人</td>
                <td style="width:13%">创建时间</td>
                <td style="width:25%">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="verifyHeader" items="${searchPageUtil.page.list}">
				<tr <c:if test="${verifyHeader.status==1}">class="tr-disabled"</c:if>>
					<td><input type="checkbox" name="checkone" value="${verifyHeader.id}"></td>
					<td>【${verifyHeader.title}】</td>
					<td>${verifyHeader.menuName}</td>
					<td>
						<c:choose>
							<c:when test="${verifyHeader.verifyType==0}">固定审批流程</c:when>
							<c:when test="${verifyHeader.verifyType==1}">自动审批流程</c:when>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${verifyHeader.status==0}">启用</c:when>
							<c:when test="${verifyHeader.status==1}"><font color="red">禁用</font></c:when>
						</c:choose>
					</td>
					<td>${verifyHeader.createName}</td>
					<td>
						<fmt:formatDate value="${verifyHeader.createDate}" type="both"/>
					</td>
					<td>
						<a href="javascript:void(0)" button="修改"
							onclick="leftMenuClick(this,'platform/sysVerify/loadUpdateHtml?id=${verifyHeader.id}','system')"
							class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe7e9;</i>修改</a> 
						<a href="javascript:void(0)" button="查看详情"
							onclick="leftMenuClick(this,'platform/sysVerify/loadDetails?id=${verifyHeader.id}','system')"
							class="layui-btn layui-btn-gray layui-btn-mini">
							<i class="layui-icon">&#xe970;</i>查看详情</a> 
						<c:choose>
							<c:when test="${verifyHeader.status==0}">
								<a onclick="deleteVerify('${verifyHeader.id}','1');" button="禁用"
									class="layui-btn layui-btn-danger layui-btn-mini">
									<i class="layui-icon">&#xeaf2;</i>禁用</a>
							</c:when>
							<c:when test="${verifyHeader.status==1}">
								<a onclick="deleteVerify('${verifyHeader.id}','0');" button="启用"
									class="layui-btn layui-btn-mini">
									<i class="layui-icon">&#xeabc;</i>启用</a>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
	</form>
</div>