<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script>
<script src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<div class="content_modify">
	<div class="content_role">
		<form  class="layui-form" action="platform/buyer/syspurchaseprice/loadPurchaseData">
		        <ul class="order_search platformSearch">
		          <li class="state">
		            <label>采购单号：</label>
		            <input type="text" placeholder="输入采购单号" name="orderCode" value="${map.orderCode}">
		          </li>
		          <li class="state">
		            <label>发货单号：</label>
		            <input type="text" placeholder="输入发货单号" name="batchNo" value="${map.batchNo}">
		          </li>
		          <li class="state">
		            <label>开始时间：</label>
		            <div class="layui-input-inline">
		              <input type="text" name="startDate" value="${map.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
		            </div>
		          </li>
		          <li class="state">
		            <label>截止时间：</label>
		            <div class="layui-input-inline">
		              <input type="text" name="endDate" value="${map.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
		            </div>
		          </li>
		          <li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
		          <li class="nomargin"><button class="search" onclick="exportData('purchaseTable','采购数据报表');">数据导出</button></li>
		        </ul>
		</form>

        <table class="table_pure bankAccountList" id="purchaseTable">
          <thead>
          <tr>
            <td style="width:15%">采购单号</td>
            <td style="width:15%">发货单号</td>
            <td style="width:10%">货号</td>
            <td style="width:10%">条形码</td>
            <td style="width:7%">采购单价</td>
            <td style="width:7%">采购出货价</td>
            <td style="width:8%">实际到货数量</td>
            <td style="width:7%">采购总金额</td>
            <td style="width:7%">毛利</td>
            <td style="width:10%">时间</td>
          </tr>
          </thead>
          <tbody>
          <c:set var="priceSum" value="0" scope="page"></c:set>
          <c:set var="purchasePriceSum" value="0" scope="page"></c:set>
          <c:set var="factSum" value="0" scope="page"></c:set>
          <c:set var="grossProfitSum" value="0" scope="page"></c:set>
          <c:set var="totalMoneySum" value="0" scope="page"></c:set>
          <c:forEach var="item" items="${purchaseList}" varStatus="status">
          	<tr>
          	  <c:set var="priceSum" value="${priceSum+item.price}" scope="page"></c:set>
	          <c:set var="purchasePriceSum" value="${purchasePriceSum+item.purchasePrice}" scope="page"></c:set>
	          <c:set var="factSum" value="${factSum+item.fact_number}" scope="page"></c:set>
	          <c:set var="grossProfitSum" value="${grossProfitSum+item.totalMoney}" scope="page"></c:set>
	          <c:set var="totalMoneySum" value="${totalMoneySum+item.grossProfit}" scope="page"></c:set>
              <td>${item.order_code}&nbsp;</td>
              <td>${item.batch_no}&nbsp;</td>
              <td>${item.product_code}</td>
              <td>${item.barcode}</td>
              <td><fmt:formatNumber value="${item.price}" pattern="0.00" type="number"/></td>
              <td><fmt:formatNumber value="${item.purchasePrice}" pattern="0.00" type="number"/></td>
              <td>${item.fact_number}</td>
              <td><fmt:formatNumber value="${item.totalMoney}" pattern="0.00" type="number"/></td>
              <td><fmt:formatNumber value="${item.grossProfit}" pattern="0.00" type="number"/></td>
              <td><fmt:formatDate value="${item.create_date}" type="date"/></td>
            </tr>
          </c:forEach>
          <c:if test="${purchaseList != null && fn:length(purchaseList)>0}">
            	<tr style="color: blue">
            		<td></td>
            		<td></td>
            		<td></td>
            		<td>合计：</td>
            		<td><fmt:formatNumber value="${priceSum}" pattern="0.00" type="number"/></td>
            		<td><fmt:formatNumber value="${purchasePriceSum}" pattern="0.00" type="number"/></td>
            		<td><fmt:formatNumber value="${factSum}" pattern="0.00" type="number"/></td>
            		<td><fmt:formatNumber value="${grossProfitSum}" pattern="0.00" type="number"/></td>
            		<td><fmt:formatNumber value="${totalMoneySum}" pattern="0.00" type="number"/></td>
            		<td></td>
            	</tr>
           </c:if>
          </tbody>
        </table>
      </div>
</div>