<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script> 
<script src="<%=basePath%>statics/platform/js/echarts.min.js"></script>
<script type="text/javascript" src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<script type="text/javascript">
var arrivalList = <%=request.getAttribute("arrivalList")%>;
var xArr = [];
var succArr=[];
var totalArr=[];
$.each(arrivalList,function(i,data){
	xArr.push(data.supp_name);
	succArr.push(data.successNum);
	totalArr.push(data.orderSum);
});

 // 基于准备好的dom，初始化echarts实例
 var columnEcharts = echarts.init(document.getElementById('echartsColumn'));

 // 指定图表的配置项和数据
 var columnOption = {
 	    title : {
 	        text: '供应商及时到货行数和下单总行数',
 	        subtext: '买卖系统'
 	    },
 	    //提示框，鼠标悬浮交互时的信息提示    
 	    tooltip : {
 	        trigger: 'axis'
 	    },
 	   //图例，每个图表最多仅有一个图例    
 	    legend: {
 	        data:['及时到货行数','下单总行数']
 	    },
 	    //工具箱，每个图表最多仅有一个工具箱
 	    toolbox: {
 	        show : true,//显示策略，可选为：true（显示） | false（隐藏），默认值为false  
 	        feature : {//启用功能，目前支持feature，工具箱自定义功能回调处理
 	            //dataView : {show: true, readOnly: false},//数据视图
 	            magicType : {show: true, type: ['line', 'bar','stack','tiled']},
 	            restore : {show: true},
 	            saveAsImage : {show: true}
 	        }
 	    },
 	   	grid: {  
	  		bottom:'70'
	  	}, 
 	    calculable : true,
 	    //横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型 
 	    xAxis : [
 	        {
 	            type : 'category',//坐标轴类型，横轴默认为类目型'category'
 	            axisLabel :{  // 解决X轴显示不全
 	            interval:'auto', //0强制显示所有标签，如果设置为1，表示隔一个标签显示一个标签，如果为3，表示隔3个标签显示一个标签 ,'auto'（自动隐藏显示不下的）
 	            //rotate: 40,
				  formatter : function(value) {
						var ret = "";//拼接加\n返回的类目项  
						var maxLength = 6;//每项显示文字个数  
						var valLength = value.length;//X轴类目项的文字个数  
						var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
						if (rowN > 1)//如果类目项的文字大于6,  
						{
							for ( var i = 0; i < rowN; i++) {
								var temp = "";//每次截取的字符串  
								var start = i * maxLength;//开始截取的位置  
								var end = start + maxLength;//结束截取的位置  
								//这里也可以加一个是否是最后一行的判断，但是不加也没有影响，那就不加吧  
								temp = value.substring(start, end) + "\n";
								ret += temp; //凭借最终的字符串  
							}
							return ret;
						} else {
							return value;
						}
					}
				},
				data : []//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行    
			}],
			// 控制图的大小，调整下面这些值就可以
			//x 为直角坐标系内绘图网格左上角横坐标，数值单位px，支持百分比（字符串），如'50%'(显示区域横向中心),y 为左上纵坐标，x2为右下横坐标，y2为右下纵坐标。
			//grid:{
			//     x: 0,
			//     y: 0,
			//    x2: 0,
			//    y2: 0
			//  },
			//纵轴通常为数值型，但条形图时则纵轴为类目型 
			yAxis : [{
				type : 'value',//坐标轴类型，纵轴默认为数值型'value'
                splitArea: {show: false},//分隔区域，默认不显示    
				axisLabel : {
				formatter : "{value} 行"
				}
			}],
			//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象
			series : [{
				name : '及时到货行数',//系列名称，如果启用legend，该值将被legend.data索引相关
				type : 'bar',//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。
				data : [],//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值
				markPoint : {//系列中的数据标注内容
					data : [
					   {type : 'max', name: '最大值'},
					   {type : 'min', name: '最小值'}
					]
				},
				markLine : {//系列中的数据标线内容
					data : [
					   {type : 'average',name : '平均值'}
					]
				}
			}, {
			name : '下单总行数',//系列名称，如果启用legend，该值将被legend.data索引相关
			type : 'bar', //图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。
			data : [],
			markPoint : {
				 data : [
					 {type : 'max', name: '最大值'},
					 {type : 'min', name: '最小值'}
				 ]
			},
			markLine : {
				data : [
				   {type : 'average', name: '平均值'}
				]
			}
		}]
	};

	columnOption.xAxis[0].data = xArr;
	columnOption.series[0].data = succArr;
	columnOption.series[1].data = totalArr;
	// 使用刚指定的配置项和数据显示图表。
	columnEcharts.setOption(columnOption);
</script>
<div>
	<form class="layui-form" action="platform/buyer/supplier/getSupplierArrivalRateHtml">
		<ul class="order_search">
			<input type="hidden" name="byDate" value="${map.byDate }">
			<li class="state"><label>起始时间：</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" value="${map.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
					-
				<div class="layui-input-inline">
					<input type="text" name="endDate" value="${map.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
			</li>
			<li class="range"><label>供应商名称：</label> 
				<input type="text" placeholder="输入供应商名称" name="suppName" value="${map.suppName}">
			</li>
			<li class="range"><label>商品名称：</label> 
				<input type="text" placeholder="输入商品名称" name="proName" value="${map.proName}">
			</li>
			<li class="range"><label>货号：</label> 
				<input type="text" placeholder="输入货号" name="proCode" value="${map.proCode}">
			</li>
			<li class="range"><label>条形码：</label> 
				<input type="text" placeholder="输入条形码" name="skuOid" value="${map.skuOid}">
			</li>
			<li class="range"><label>规格名称：</label> 
				<input type="text" placeholder="输入规格名称" name="skuName" value="${map.skuName}">
			</li>
			<li class="range"><label>规格代码：</label> 
				<input type="text" placeholder="输入规格代码" name="skuCode" value="${map.skuCode}">
			</li>
			<li class="range"><label>创建人：</label> 
				<input type="text" placeholder="输入创建人" name="createName" value="${map.createName}">
			</li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="exportData('supplierArrivalTable','供应商到货及时率');">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
		</ul>
	</form>
	<table class="table_pure supplierList" id="supplierArrivalTable">
		<thead>
			<tr>
				<td>供应商名称</td>
				<td>下单总行数</td>
				<td>及时到货行数</td>
				<td>未及时到货行数</td>
				<td>到货及时率</td>
			</tr>
		</thead>
		<tbody>
			<c:set var="total" value="0" scope="page"></c:set>
			<c:set var="successTotal" value="0" scope="page"></c:set>
			<c:set var="failTotal" value="0" scope="page"></c:set>
			<c:forEach var="item" items="${arrivalList}">
				<tr>
				    <c:set var="total" value="${item.value.orderSum + total }" scope="page"></c:set>
				    <c:set var="successTotal" value="${item.value.successNum + successTotal }" scope="page"></c:set>
				    <c:set var="failTotal" value="${item.value.failNum+ failTotal }" scope="page"></c:set>
					<td>${item.value.supp_name}</td>
					<td>${item.value.orderSum}</td>
					<td>${item.value.successNum}</td>
					<td>${item.value.failNum}</td>
					<td><fmt:formatNumber value="${item.value.successNum/item.value.orderSum*100}" pattern="#.##"/>%</td>
				</tr>
			</c:forEach>
			<tr style="color: blue">
					<td>合计：</td>
					<td>${total}</td>
					<td>${successTotal}</td>
					<td>${failTotal}</td>
					<td>
						<c:choose>
							<c:when test="${total==0 }">
								0%
							</c:when>
							<c:otherwise>
								<script>
									var successTotal = parseFloat("${successTotal}");
									var total = parseFloat("${total}");
									$("#rateTotal").html(Math.round((successTotal*100/total)*100)/100);
								</script>
								<span id="rateTotal"></span>%
							</c:otherwise>
						</c:choose>
					</td>
			</tr>
		</tbody>
	</table>
	 <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	<div id="echartsColumn" style="width: 100%;height:600px; margin-top: 50px;"></div>
</div>