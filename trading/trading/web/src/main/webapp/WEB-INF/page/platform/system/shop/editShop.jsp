<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
  <form id="editShopForm">
    <div class="account_group">
      <span>店铺代码:</span>
     	<input type="text" name="shopCode" value="${SysShop.shopCode}">
    </div>
    <div class="account_group">
      <span>店铺名称:</span>
      <input type="text" name="shopName" value="${SysShop.shopName}">
    </div>
    <div class="account_group">
      <span>状态:</span>
      <c:if test="${SysShop.isDel==0}">
      	<label><input type="radio" name="isDel" checked value="0"> 正常</label>
      	<label><input type="radio" name="isDel" value="1"> 禁用</label>
      </c:if>
      <c:if test="${SysShop.isDel==1}">
      	<label><input type="radio" name="isDel" value="0"> 正常</label>
      	<label><input type="radio" name="isDel" checked value="1"> 禁用</label>
      </c:if>
    </div>
    <input type="hidden" name="id" value="${SysShop.id}">
</form>