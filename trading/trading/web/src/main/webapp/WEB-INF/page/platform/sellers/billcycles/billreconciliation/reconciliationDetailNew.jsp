<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billreconciliation/reconciliationDetail.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<%--<script type="text/javascript">
	</script>--%>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<div >
	<div id="print_yc">
	<div class="page_title">
		<table class="table_n">
			<tr>
				<td>采购商：<span>${buyBillReconciliation.buyCompanyName}</span></td>
				<td>出账周期：<span>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</span></td>
				<td>
					<input type="hidden" id="reconciliationId" value="${buyBillReconciliation.id}">
					状态：
					<span>
				  <c:choose>
					  <c:when test="${buyBillReconciliation.billDealStatus==1}">待发起对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==2}">待确认对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
					  <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
				  </c:choose>
				</span></td>
				<td>奖惩金额：<span>${buyBillReconciliation.customAmount}</span></td>
				<td>预付款抵扣金额：<span>${buyBillReconciliation.advanceDeductTotal}</span></td>
			</tr>
		</table>
	</div>

	<div class="mp mt size_sm">
	<c:set var="priceSum" value="0" scope="page"></c:set>

	<!--发货订单-->
	<c:forEach var="daohuoItem" items="${daohuoMap}">
		<div>
			<div class="page_title">
			<table class="table_n">
				<tr>
				  <td style="width: 20%"><span ><span class="c99">订单类型：</span>采购发货</span></td>
				  <td style="width: 20%"><span ><span class="c99">运单号：</span></span>${daohuoItem.value.waybillNo}</td>
				  <td style="width: 20%"><span ><span class="c99">物流名称：</span>${daohuoItem.value.logisticsCompany}</span></td>
				  <td style="width: 20%"><span ><span class="c99">司机名称：</span>${daohuoItem.value.driverName}</span></td>
				  <td style="width: 20%"><span ><span class="c99">运费：</span>${daohuoItem.value.freight}元</span></td>
				  <td style="width: 20%"><span ><span class="c99">总金额+运费：</span>${daohuoItem.value.itemPriceAndFreightSum+daohuoItem.value.freight}元</span></td>
				</tr>

			</table>
			</div>
			<table class="layui-table text-center" lay-even="" lay-size="sm"
				style="margin: 0;">
				<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:10%">条形码</td>
						<td style="width:25%">采购订单号</td>
						<td style="width:15%">采购员</td>
						<td style="width:30%">发货单号</td>
						<td style="width:30%">入库单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">到货总数量</td>
						<td style="width: 10%">是否开票</td>
						<td style="width:12%">采购单价</td>
						<td style="width:12%">对账单价</td>
						<td style="width:15%">总金额</td>
						<td style="width:15%" class="oprationRe">操作</td>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="goodsItem" items="${daohuoItem.value.itemList}">
					<tr class="breakType">
						<td>${goodsItem.productCode }</td>
						<td>${goodsItem.productName }</td>
						<td>${goodsItem.skuCode }</td>
						<td>${goodsItem.skuName }</td>
						<td>${goodsItem.barcode }</td>
						<td>${goodsItem.orderCode }</td>
						<td>${goodsItem.createBillName }</td>
						<td>${goodsItem.deliverNo }</td>
						<td>${goodsItem.storageNo }</td>
						<td><fmt:formatDate value="${goodsItem.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
						<td>${goodsItem.arrivalNum }</td>
						<td>
							<c:choose>
								<c:when test="${goodsItem.isNeedInvoice == 'Y'}">是</c:when>
								<c:when test="${goodsItem.isNeedInvoice != 'Y'}">否</c:when>
							</c:choose>
						</td>
						<td>${goodsItem.salePrice }</td>
						<td>${goodsItem.updateSalePrice }</td>
						<td>${goodsItem.updateSalePrice*goodsItem.arrivalNum }</td>
						<td class="oprationRe">
							<div >
								<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${goodsItem.deliverNo}','1');">查看收货凭据</span>
								<%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${daohuoItem.value.attachmentAddrStr}');">查看收据</span>--%>

							</div>
							<c:choose>
								<c:when test="${buyBillReconciliation.billDealStatus == 1}">
									<a href="javascript:void(0)" onclick="updPrice('${goodsItem.productName}','${goodsItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
								</c:when>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>${daohuoItem.value.arrivalNumSum }</td>
						<td></td>
						<td>-</td>
						<td>-</td>
						<td>${daohuoItem.value.itemPriceSum }</td>
						<%--<td>总金额+运费:${itemPriceSum+daohuoItem.value.freight }</td>--%>
						<td class="oprationRe"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</c:forEach>
	<!--采购换货-->
	<c:forEach var="recordReplaceItem" items="${recordReplaceMap}">
		<div>
			<div class="page_title">
				<table class="table_n">
					<tr>
						<td style="width: 20%"><span ><span class="c99">订单类型：</span>采购换货</span></td>
						<td style="width: 20%"><span ><span class="c99">运单号：</span></span>${recordReplaceItem.value.waybillNo}</td>
						<td style="width: 20%"><span ><span class="c99">物流名称：</span>${recordReplaceItem.value.logisticsCompany}</span></td>
						<td style="width: 20%"><span ><span class="c99">司机名称：</span>${recordReplaceItem.value.driverName}</span></td>
						<td style="width: 20%"><span ><span class="c99">运费：</span>${recordReplaceItem.value.freight}元</span></td>
						<td style="width: 20%"><span ><span class="c99">总金额+运费：</span>${recordReplaceItem.value.itemPriceAndFreightSum+recordReplaceItem.value.freight}元</span></td>
					</tr>

				</table>
			</div>
			<table class="layui-table text-center" lay-even="" lay-size="sm"
				   style="margin: 0;">
				<thead>
				<tr>
					<td style="width:15%">货号</td>
					<td style="width:10%">商品名称</td>
					<td style="width:10%">规格代码</td>
					<td style="width:10%">规格名称</td>
					<td style="width:10%">条形码</td>
					<td style="width:25%">采购订单号</td>
					<td style="width:15%">采购员</td>
					<td style="width:30%">发货单号</td>
					<td style="width:30%">入库单号</td>
					<td style="width:10%">到货日期</td>
					<td style="width:10%">到货总数量</td>
					<td style="width: 10%">是否开票</td>
					<td style="width:12%">返修单价</td>
					<td style="width:12%">对账单价</td>
					<td style="width:15%">总金额</td>
					<td style="width:15%" class="oprationRe5">操作</td>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="recReplaceItem" items="${recordReplaceItem.value.itemList}">
					<tr class="breakType">
						<td>${recReplaceItem.productCode }</td>
						<td>${recReplaceItem.productName }</td>
						<td>${recReplaceItem.skuCode }</td>
						<td>${recReplaceItem.skuName }</td>
						<td>${recReplaceItem.barcode }</td>
						<td>${recReplaceItem.orderCode }</td>
						<td>${recReplaceItem.createBillName }</td>
						<td>${recReplaceItem.deliverNo }</td>
						<td>${recReplaceItem.storageNo }</td>
						<td><fmt:formatDate value="${recReplaceItem.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
						<td>${recReplaceItem.arrivalNum }</td>
						<td>
							<c:choose>
								<c:when test="${recReplaceItem.isNeedInvoice == 'Y'}">是</c:when>
								<c:when test="${recReplaceItem.isNeedInvoice != 'Y'}">否</c:when>
							</c:choose>
						</td>
						<td>${recReplaceItem.salePrice }</td>
						<td>${recReplaceItem.updateSalePrice }</td>
						<td>${recReplaceItem.updateSalePrice*recReplaceItem.arrivalNum }</td>
						<td class="oprationRe5">
							<div >
								<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${recReplaceItem.deliverNo}','1');">查看收货凭据</span>
									<%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${daohuoItem.value.attachmentAddrStr}');">查看收据</span>--%>

							</div>
							<c:choose>
								<c:when test="${buyBillReconciliation.billDealStatus == 1}">
									<a href="javascript:void(0)" onclick="updPrice('${recReplaceItem.productName}','${recReplaceItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
								</c:when>
								<%--<c:when test="${buyBillReconciliation.billDealStatus == 4}">
									<a href="javascript:void(0)" onclick="updPrice('${goodsItem.productName}','${goodsItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
								</c:when>--%>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
				<tfoot>
				<tr>
					<td>合计</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>${recordReplaceItem.value.arrivalNumSum }</td>
					<td></td>
					<td>-</td>
					<td>-</td>
					<td>${recordReplaceItem.value.itemPriceSum }</td>
						<%--<td>总金额+运费:${itemPriceSum+daohuoItem.value.freight }</td>--%>
					<td class="oprationRe5"></td>
				</tr>
				</tfoot>
			</table>
		</div>
	</c:forEach>
<!--换货数据-->
		<c:forEach var="replaceItem" items="${replaceMap}">
			<div>
			<div class="page_title">
				<table class="table_n">
					<tr>
						<td style="width: 20%"><span ><span class="c99">订单类型：</span>售后换货</span></td>
						<td style="width: 20%"><span ><span class="c99">总金额：</span>${replaceItem.value.itemPriceSum}元</span></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
					</tr>

				</table>
			</div>
				<table class="layui-table text-center" lay-even="" lay-size="sm"
					   style="margin: 0;">
					<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:15%">条形码</td>
						<%--<td style="width:25%">采购订单号</td>--%>
						<td style="width:10%">创建人</td>
						<td style="width:30%">换货单号</td>
						<td style="width:30%">入库单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">换货总数量</td>
						<%--<td style="width: 10%">是否开票</td>--%>
						<td style="width:12%">换货单价</td>
						<td style="width:12%">对账单价</td>
						<td style="width:10%">总金额</td>
						<td style="width:10%" class="oprationRe1">操作</td>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="replaceItemInfo" items="${replaceItem.value.itemList}">
						<tr class="breakType">
							<td>${replaceItemInfo.productCode }</td>
							<td>${replaceItemInfo.productName }</td>
							<td>${replaceItemInfo.skuCode }</td>
							<td>${replaceItemInfo.skuName }</td>
							<td>${replaceItemInfo.barcode }</td>
							<%--<td>${replaceItemInfo.orderCode }</td>--%>
							<td>${replaceItemInfo.createBillName }</td>
							<td>${replaceItemInfo.deliverNo }</td>
							<td>-</td>
							<td><fmt:formatDate value="${replaceItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
							<td>${replaceItemInfo.arrivalNum }</td>
							<%--<td>
								<c:choose>
									<c:when test="${replaceItemInfo.isNeedInvoice == 'Y'}">是</c:when>
									<c:when test="${replaceItemInfo.isNeedInvoice != 'Y'}">否</c:when>
								</c:choose>
							</td>--%>
							<td>${replaceItemInfo.salePrice }</td>
							<td>${replaceItemInfo.updateSalePrice }</td>
							<td>${replaceItemInfo.updateSalePrice*replaceItemInfo.arrivalNum }</td>
							<td class="oprationRe1">
								<div >
									<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${replaceItemInfo.deliverNo}','2');">查看收货凭据</span>
									<%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${replaceItem.value.attachmentAddrStr}');">查看收据</span>--%>
								</div>
								<%--<c:choose>
									<c:when test="${buyBillReconciliation.billDealStatus == 1}">
										<a href="javascript:void(0)" onclick="updPrice('${replaceItemInfo.productName}','${replaceItemInfo.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
									</c:when>
								</c:choose>--%>
							</td>
						</tr>
					</c:forEach>
					</tbody>
					<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>${replaceItem.value.arrivalNumSum}</td>
						<%--<td></td>--%>
						<td>-</td>
						<td>-</td>
						<td>${replaceItem.value.itemPriceSum }</td>
						<%--<td>总金额:${itemPriceSum+replaceItem.value.freight }</td>--%>
						<td class="oprationRe1"></td>
					</tr>
					</tfoot>
				</table>
			</div>
		</c:forEach>
<!--退货数据-->
		<c:forEach var="customerItem" items="${customerMap}">
			<div>
				<table class="table_n">
					<tr>
						<td style="width: 20%"><span ><span class="c99">订单类型：</span>售后退货</span></td>
						<td style="width: 20%"><span ><span class="c99">总金额：</span>${customerItem.value.itemPriceSum}元</span></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
						<td style="width: 20%"></td>
					</tr>
				</table>
			</div>
				<table class="layui-table text-center" lay-even="" lay-size="sm"
					   style="margin: 0;">
					<thead>
					<tr class="breakType">
						<td style="width:15%">货号</td>
						<td style="width:10%">商品名称</td>
						<td style="width:10%">规格代码</td>
						<td style="width:10%">规格名称</td>
						<td style="width:10%">条形码</td>
						<%--<td width="150px">采购单号</td>--%>
						<td style="width:10%">创建人</td>
						<td style="width:25%">退货单号</td>
						<td style="width:25%">入库单号</td>
						<td style="width:10%">到货日期</td>
						<td style="width:10%">到货总数量</td>
						<%--<td style="width: 10%">是否开票</td>--%>
						<td style="width:15%">采购单价</td>
						<td style="width:15%">对账单价</td>
						<td style="width:10%">总金额</td>
						<td style="width:10%" class="oprationRe2">操作</td>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="customerItemInfo" items="${customerItem.value.itemList}">
						<tr>
							<td>${customerItemInfo.productCode }</td>
							<td>${customerItemInfo.productName }</td>
							<td>${customerItemInfo.skuCode }</td>
							<td>${customerItemInfo.skuName }</td>
							<td>${customerItemInfo.barcode }</td>
							<%--<td>${goodsItem.orderCode }</td>--%>
							<td>${customerItemInfo.createBillName }</td>
							<td>${customerItemInfo.deliverNo }</td>
							<td>-</td>
							<td><fmt:formatDate value="${customerItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
							<td>${customerItemInfo.arrivalNum }</td>
							<td>${customerItemInfo.salePrice }</td>
							<td>${customerItemInfo.updateSalePrice }</td>
							<td>${customerItemInfo.updateSalePrice*customerItemInfo.arrivalNum }</td>
							<td class="oprationRe2">
								<div >
									<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${customerItemInfo.deliverNo}','3');">查看收货凭据</span>
									<%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItem.value.attachmentAddrStr}');">查看收据</span>--%>
								</div>
								<%--<c:choose>
									<c:when test="${buyBillReconciliation.billDealStatus == 1}">
										<a href="javascript:void(0)" onclick="updPrice('${customerItemInfo.productName}','${customerItemInfo.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
									</c:when>
								</c:choose>--%>
							</td>
						</tr>
					</c:forEach>
					</tbody>
					<tfoot>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>${customerItem.value.arrivalNumSum}</td>
						<%--<td></td>--%>
						<td>-</td>
						<td>-</td>
						<td>${customerItem.value.itemPriceSum}</td>
						<td class="oprationRe2"></td>
					</tr>
					</tfoot>
				</table>
			</div>
		</c:forEach>
	</div>
	<fieldset class="layui-elem-field layui-field-title">
		<legend>
			<b>总金额：<span class="red">${buyBillReconciliation.totalAmount}元</span>
			</b>
		</legend>
	</fieldset>
</div>
	<div class="print_b">
		<span id="print_sure" onclick="removeUselessLine();">打印账单</span>
		<%--<span class="table_n" onclick="sellerRecItemExport();"><i class="layui-icon">&#xe7a0;</i> 导出</span>--%>
		<span class="table_n" onclick="sellerRecItemExport('detail');"><i class="layui-icon">&#xe7a0;</i> 所有数据导出</span>
		<span class="table_n" onclick="sellerRecItemExport('rough');"><i class="layui-icon">&#xe7a0;</i> 货品数据导出</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');">返回</span>
	</div>
</div>

<!--商品价格变更审批-->
<div class="plain_frame price_change" id="updPriceDiv" style="display: none;">
	<ul>
		<li>
			<span>商品价格:</span>
			<input type="number" id="updPrice" name="updPrice"  min="0" >
		</li>
	</ul>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $(".oprationRe").hide();
        $(".oprationRe1").hide();
        $(".oprationRe2").hide();
        $(".oprationRe3").hide();
        $(".oprationRe4").hide();
        $(".oprationRe5").hide();
        $("#print_yc").jqprint();
        $(".oprationRe").show();
        $(".oprationRe1").show();
        $(".oprationRe2").show();
        $(".oprationRe3").show();
        $(".oprationRe4").show();
        $(".oprationRe5").show();
    });
    //删除自动生的无用行
    function removeUselessLine() {
        $(".JCLRgrips").remove();
    }
</script>