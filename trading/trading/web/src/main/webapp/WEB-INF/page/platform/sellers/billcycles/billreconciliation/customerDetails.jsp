<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun"%>
<script type="text/javascript">
	$(function() {
		layer.photos({
			photos : '#layer-photos-demo',
			anim : 5
		//0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
		});
	});
</script>

<div>
	<div class="progress123">
		<span class="step1 current"> <b></b> 提交售后申请 </span><span
			class="step2 current"> <b></b> 供应商审批 </span><span class="step3">
			<b></b> 售后处理完成 </span>
	</div>
	<h4 class="currentTitle">申请明细</h4>
	<div class="padding-sm-lr currentContent">
		<div class="mp">
			<span class="currentLabel">当前状态：</span> <span
				class="currentState size_sm"> <c:choose>
					<c:when test="${customer.status==0}">待我审批</c:when>
					<c:when test="${customer.status==1}">已通过</c:when>
					<c:when test="${customer.status==2}">已驳回</c:when>
					<c:when test="${customer.status==3}">已取消</c:when>
					<c:when test="${customer.status==4}">完结</c:when>
				</c:choose> </span>
		</div>
		<ul class="currentDetail mp30">
			<li><span>编&emsp;&emsp;号</span>： ${customer.customerCode}</li>
			<li><span>售后类型</span>： <c:choose>
					<c:when test="${customer.type==0}">换货</c:when>
					<c:when test="${customer.type==1}">退货退款</c:when>
				</c:choose></li>
			<li><span>采 购 商</span>： ${el:getCompanyById(customer.companyId).companyName}</li>
			<li><span>申 请 人</span>： ${customer.createName}</li>
			<li><span>申请日期</span>： <fmt:formatDate
					value="${customer.createDate}" type="both" /></li>
		</ul>
		<div class="currentExplain">
			<span>申请原因：</span>
			<p>${customer.reason}</p>
		</div>
		<p class="line mp30"></p>
		<%-- <div class="text-right mp mt">
			<span id="currentApproval">
				<c:if test="${customer.status == 0}">
					<img onclick="loadVerify('${customer.id}','${customer.customerCode}');" src="${basePath}statics/platform/images/planApproval.jpg">
				</c:if>
			</span>
		</div> --%>
	</div>
	<h4 class="currentTitle">采购清单</h4>
	<div class="padding-sm-lr currentContent">
		<div class="mp30"></div>
		<table class="table_pure afterDetail">
			<thead>
				<tr>
					<td style="width:9%;">货号</td>
					<td style="width:14%;">商品名称</td>
					<td style="width:10%;">规格代码</td>
					<td style="width:15%;">规格名称</td>
					<td style="width:10%;">条形码</td>
					<td style="width:6%;">单位</td>
					<td style="width:13%;">发货号</td>
					<td style="width:7%;">价格</td>
					<td style="width:8%;">发货数量</td>
					<td style="width:8%;">换货/退货退款数量</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${itemList}">
					<tr>
						<td>${item.productCode}</td>
						<td>${item.productName}</td>
						<td>${item.skuCode}</td>
						<td>${item.skuName}</td>
						<td>${item.skuOid}</td>
						<td>${el:getUnitById(item.unitId).unitName}</td>
						<td title="${item.deliveryCode}">${item.deliveryCode}</td>
						<td title="${item.price}">${item.price}</td>
						<td>${item.receiveNumber}</td>
						<td>${item.goodsNumber}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="size_sm mp30 mt c66">售后收货地址：</div>
		<div class="size_sm">${el:getProvinceById(customer.province).province}
			${el:getCityById(customer.city).city}
			${el:getAreaById(customer.area).area}
			${customer.addrName}（${customer.linkman} 收）${customer.linkmanPhone}</div>
		<div class="size_sm c66 mp30">商品图片/售后凭证：</div>
		<div class="afterImgView layer-photos-demo mp" id="layer-photos-demo">
			<c:if test="${customer.proof != null && customer.proof != ''}">
				<c:forEach var="proof" items="${customer.proof.split(',')}">
					<span title="点击查看原图"><b></b><img src="${proof}">
					</span>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<div class="text-right" style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');"><span class="contractBuild">返回</span></a>
	</div>
</div>
