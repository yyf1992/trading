<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<div>
		<table class="table_yellow schedule">
			<thead>
				<tr>
					<td style="width:4%">序号</td>
			        <td style="width:14%">编号</td>
			        <td style="width:12%">部门名称</td>
			        <td style="width:6%">销售计划</td>
			        <td style="width:7%">剩余销售计划</td>
			        <td style="width:5%">入仓量</td>
			        <td style="width:16%">销售周期</td>
			        <td style="width:15%">备注</td>
			        <td style="width:8%">申请人</td>
			        <td style="width:13%">申请时间</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="shop" items="${shopList}" varStatus="indexNum">
					<tr>
						<td>${indexNum.count}</td>
						<td title="${shop.plan_code}">${shop.plan_code}</td>
						<td>${shop.shop_name}</td>
						<td>${shop.sales_num}</td>
						<td>${shop.overplusNum}</td>
						<td>${shop.put_storage_num}</td>
						<td><fmt:formatDate value="${shop.start_date}" type="date"></fmt:formatDate> 至 <fmt:formatDate
							value="${shop.end_date}" type="date"></fmt:formatDate></td>
						<td title="${shop.reamark}">${shop.remark}</td>
						<td>${shop.create_name}</td>
						<td><fmt:formatDate value="${shop.create_date}" type="both"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
