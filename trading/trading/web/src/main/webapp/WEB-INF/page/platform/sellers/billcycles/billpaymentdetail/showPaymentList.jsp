<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单我的付款明细</title>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<script type="text/javascript">
        //收款凭据
        function showBillReceipt(receiptAddr) {
            findBillReceipt(receiptAddr);
            layer.open({
                type: 1,
                title: '付款票据',
                area: ['auto', 'auto'],
                skin:'popBarcode',
                //btns :2,
                closeBtn:2,
                btn:['关闭窗口'],
                content: $('#linkReceipt'),
                yes : function(index){
                    layer.close(index);
                }
            });
        }

        //收据发票(退货)
        function findBillReceipt(receiptAddr) {
            $(".big_img").empty();
            $("#icon_list").empty();
            if(receiptAddr != null || receiptAddr != ''){
                var receiptAddrList = receiptAddr.split(',');
                var bigImg = '';
                for(i = 0;i<receiptAddrList.length;i++){
                    var liObj = $("<li>");
                    $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
                    $("#icon_list").append(liObj);
                    bigImg = receiptAddrList[0];
                }
                //大图
                $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
            }
        }

        //账单结算周期审批
        //$('.bill_approval').click(function(){
        function showPaymentAgree(paymentItemId) {
            layer.open({
                type:1,
                title:'收款确认',
                area:['380px','auto'],
                skin:'popBarcode',
                closeBtn:2,
                content:$('#billApproval'),
                btn:['确定','取消'],
                yes:function(index){
                    var reconciliationId = $("#reconciliationId").val();
                    var	agreeStatus = $("input[name='agreeStatus']:checked").val();
                    //alert("审批选择"+agreeStatus)
                    var	approvalRemarks = $("#approvalRemarks").val();
                    if(!agreeStatus){
                        layer.msg("请选择审批状态！",{icon:2});
                    }else if(approvalRemarks.length >300){
                        layer.msg("备注内容不超过300个字符！",{icon:2});
                    }else{
                        $.ajax({
                            type : "POST",
                            url : "platform/seller/billPaymentDetail/updatePaymentItem",
                            data: {
                                "id": paymentItemId,
								"reconciliationId":reconciliationId,
                                "paymentStatus": agreeStatus,
                                "receivablesRemarks": approvalRemarks
                            },
                            async:false,
                            success:function(data){
                                layer.close(index);
                                layer.msg("保存成功！",{icon:1});
                                //loadPlatformData();
                                leftMenuClick(this,'platform/seller/billPaymentDetail/billPaymentDetailList','seller')
                            },
                            error:function(){
                                layer.msg("保存失败，请稍后重试！",{icon:2});
                            }
                        });
                    }
                },
                no : function(index){
                    layer.close(index);
                }
            });
        }

        /**
         * 导出
         */
        function sellerPaymentItemExport() {
            // var formData = $("#searchForm").serialize();
            var reconciliationId = $("#reconciliationId").val();
            var url = "platform/seller/billPaymentDetail/sellerPaymentItemExport?reconciliationId="+ reconciliationId;
            window.open(url);
        }
        //返回收货列表
       /* function returnReceiveList() {
            leftMenuClick(this,"platform/seller/billPaymentDetail/billPaymentDetailList","sellers");
        }*/
	</script>
</head>
<!--内容-->
<!--页签-->
<div>
	<!--搜索栏-->
	  <table class="table_pure payment_list">
	  <thead>
	    <tr>
		  <td style="width:16%">付款单号</td>
		  <td style="width:10%">付款金额</td>
		  <%--<td style="width:16%">付款账号</td>--%>
		  <td style="width:10%">付款类型</td>
		  <td style="width:12%">付款时间</td>
		  <td style="width:15%">备注</td>
		  <td style="width:10%">状态</td>
		  <td style="width:10%">操作</td>
	    </tr>
	  </thead>
	  <tbody>
       <c:forEach var="billPayment" items="${billPaymentList}">
	    <tr>
			<td>
				<input type="hidden" id="reconciliationId" value="${billPayment.reconciliationId}">
				${billPayment.id}</td>
			<td>${billPayment.actualPaymentAmount}</td>
			<%--<td>${billPayment.bankAccount}</td>--%>
			<td>
			  <c:choose>
				<c:when test="${billPayment.paymentType==0}">现金</c:when>
				<c:when test="${billPayment.paymentType==1}">银行转账</c:when>
			    <c:when test="${billPayment.paymentType==2}">支付宝</c:when>
			    <c:when test="${billPayment.paymentType==3}">微信</c:when>
			  </c:choose>
			</td>
			<td>${billPayment.paymentDateStr}</td>
			<td>${billPayment.paymentRemarks}</td>
			<%--<td>${billPayment.paymentStatus}</td>--%>
			<td>
			  <c:choose>
				<c:when test="${billPayment.paymentStatus==0}">内部待确认</c:when>
				<c:when test="${billPayment.paymentStatus==1}">付款待确认</c:when>
			    <c:when test="${billPayment.paymentStatus==2}">内部已驳回</c:when>
			    <c:when test="${billPayment.paymentStatus==3}">付款已确认</c:when>
			  </c:choose>
			</td>
			<td>
			  <c:choose>
				<c:when test="${billPayment.paymentStatus==1}">
					<a href="javascript:void(0)" onclick="showPaymentAgree('${billPayment.id}');" class="layui-btn layui-btn-mini">收款确认</a>
				</c:when>
			  </c:choose>
				<%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${billPayment.paymentAddr}');">查看票据</span>--%>
			</td>
		</tr>
	   </c:forEach>
	  </tbody>
    </table>
    <div class="pager"></div>
	<div class="print_b">
		<span class="table_n" onclick="sellerPaymentItemExport();"><i class="layui-icon">&#xe7a0;</i> 导出</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/seller/billPaymentDetail/billPaymentDetailList','buyer');">返回</span>
	</div>
	<%--</form>--%>
	<%--<div class="btn_p text-center">
		<span class="order_p" onclick="returnReceiveList();">返回</span>
	</div>--%>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<!--确认付款-->
<div class="plain_frame bill_exam" style="display:none;" id="billApproval">
	<form action="">
		<ul>
			<li>
				<span>审批意见:</span>
				<label><input id="agree" type="radio" name="agreeStatus" value="1"> 同意</label>&nbsp;&nbsp;
				<%--<label><input id="reject" type="radio" name="agreeStatus" value="5"> 驳回</label>--%>
			</li>
			<li>
				<span>填写备注:</span>
				<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
			</li>
		</ul>
	</form>
</div>