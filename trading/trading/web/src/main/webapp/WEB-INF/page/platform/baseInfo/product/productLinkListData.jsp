<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%--<%@ include file="../../common/common.jsp"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
//修改价格
function updPrice(buyShopProductId){
    layer.open({
        type: 1,
        title: '修改商品价格',
        area: ['420px', 'auto'],
        skin:'change',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#updPriceDiv'),
        yes : function(index){
            var	updPrice = $("#updPrice").val();
            if(updPrice==""||updPrice==undefined||updPrice==null){
                layer.msg("价格不能为空！",{icon:2});
                return false;
			}
            var modifyReason = $("#modifyReason").val();
            if(modifyReason==""||modifyReason==undefined||modifyReason==null){
                layer.msg("修改理由不能为空！",{icon:2});
                return false;
			}
            var url = "platform/productlink/pricealter/updBuyShopProductPrice";
            $.ajax({
                type : "POST",
                url : url,
                data: {
                    "buyShopProductId": buyShopProductId,
                    "updPrice": updPrice,
                    "modifyReason":modifyReason,
                    "menuName":'17112910033928502412'
                },
                async:false,
                success:function(data){
                	//无论调用结果如何都要关闭弹出框
                	layer.close(index);
                    if(data.flag){
                        var res = data.res;
                        if(res.code==40000){
                            //调用成功,固定审批
                            updPricesuccess(res.data);
                        }else if(res.code==40010){
                            //调用失败
                            layer.msg(res.msg,{icon:2});
                            return false;
                        }else if(res.code==40011){
                            //需要设置审批流程
                            layer.msg(res.msg,{time:500,icon:2},function(){
                                setApprovalUser(url,res.data,function(data){
                                    updPricesuccess(data);
                                });
                            });
                            return false;
                        }else if(res.code==40012){
                            //对应菜单必填
                            layer.msg(res.msg,{icon:2});
                            return false;
                        }else if(res.code==40099){
                            //合同保存失败
                            layer.msg(res.msg,{icon:2});
                            return false;
                        }else if(res.code==40013){
                            //不需要审批
                            notNeedApproval(res.data,function(data){
                            	 updPricesuccess(data);
                            });
                            return false;
                        }
                    }else{
                        layer.msg("获取数据失败，请稍后重试！",{icon:2});
                        return false;
                    }
                    
                    /*;
                    layer.msg("保存成功！",{icon:1},function(){
                        loadProductLinkData();
                    });*/
                },
                error:function(){
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}
//取消关联
function cancelProductLink(id){
	layer.confirm('确定取消本商品的关联?',{
		icon:3,
	      skin:'popBuyer popB25 btnCenter deleteBuyer',
	      title:'提示',
	      closeBtn:0,
	      area:['310px','auto']
	    },function(index){
	      layer.close(index);
	      $.ajax({
			type : "POST",
			url : "platform/productlink/pricealter/cancelProductLink",
			async: false,
			data : {
				"id" : id
			},
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					selectProductLinkData();
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
    });
}
//取消成功
function selectProductLinkData(data) {
	var formData = $("form").serialize();
    layer.msg("取消成功！",{icon:1});
    leftMenuClick(this,'platform/product/loadProductList?linktype=0&'+ formData,"buyer");
}
//价格修改提交成功
function updPricesuccess(data) {
	var formData = $("form").serialize();
    layer.msg("价格修改申请提交成功！",{icon:1});
    leftMenuClick(this,'platform/product/loadProductList?linktype=0&'+ formData,"buyer");
}
//查询修改历史价格
function  selectAlterPriceHistory(relatedId) {
	$.ajax({
		type : "POST",
		url : basePath+"platform/productlink/pricealter/selectAlterPriceHistory",
		data : {
			"relatedId":relatedId
		},
		success : function(data) {
			layer.open({
				type : 1,
				area:['1200px', 'auto'],
				fix: false, //不固定
				maxmin: true,
				shadeClose: true,
	        	shade:0.4,
				title:"价格修改记录",
		    	content: $('.alterPriceHistory').html(data),
		    	btn : ['关闭' ]
			});
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！", { icon : 2 });
		}
	});
}
</script>
<table class="table_pure interwork_list">
	<thead>
	<tr>
		<td style="width: 25%">商品</td>
		<!-- <td style="width: 12%">商品名称</td>
		<td style="width: 8%">规格代码</td>
		<td style="width: 8%">规格名称</td>
		<td style="width: 10%">条形码</td> -->
		<td style="width: 4%">单位</td>
		<td style="width: 7%">采购单价</td>
		<td style="width: 7%">日产能</td>
        <td style="width: 5%">到货天数</td>
		<td style="width: 10%">
			<c:choose>
				<c:when test="${linktype==0}">关联供应商</c:when>
				<c:otherwise>关联客户</c:otherwise>
			</c:choose>
		</td>
		<!-- <td style="width: 10%">备注</td> -->
		<td style="width: 8%">状态</td>
		<td style="width: 6%">操作</td>
	</tr>
	</thead>
	<tbody id="prodcuts">
	<c:forEach var="buyShopProduct" items="${searchPageUtil.page.list}">
		<tr class="text-center">
			<td>
				${buyShopProduct.productCode}|${buyShopProduct.productName}|${buyShopProduct.skuCode}|${buyShopProduct.skuName}|${buyShopProduct.barcode}
			</td>
			<td>${buyShopProduct.unitName}</td>
			<td>${buyShopProduct.price}</td>
			<td>${buyShopProduct.dailyOutput}</td>
			<td>${buyShopProduct.storageDay}</td>
			<td>
				<c:choose>
					<c:when test="${linktype==0}">${buyShopProduct.supplierName}</c:when>
					<c:otherwise>${buyShopProduct.companyName}</c:otherwise>
				</c:choose>
			</td>
			<%-- <td title='${buyShopProduct.note}'>${buyShopProduct.note}</td> --%>
			<td>
				<c:choose>
					<c:when test="${linktype=='0'}">
							<c:if test="${buyShopProduct.status == '1'}">
								审批中				
							</c:if>
							<c:if test="${buyShopProduct.status == '2'}">
								内部审批被拒绝		
							</c:if>
							<c:if test="${buyShopProduct.status == '3'}">
								已通过确认		
							</c:if>
							<c:if test="${buyShopProduct.status == '4'}">
								已取消	
							</c:if>
							<br>
							<div class="opinion_view">						
								<span class="orange" data="${buyShopProduct.id}">查看审批流程</span>
						    </div>
						    <c:if test="${buyShopProduct.status == '3'}">
									<a href="javascript:void(0)" onclick="selectAlterPriceHistory('${buyShopProduct.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
							<i class="layui-icon"></i>历史价格</a>
							</c:if>						    
					</c:when>
					<c:otherwise>
						<c:if test="${buyShopProduct.status == '2' && linktype == '1'}">
							<a href="javascript:void(0)" onclick="productLinkSellerAprove('${buyShopProduct.id}');" class="layui-btn layui-btn-mini">确认</a>
						</c:if>
					</c:otherwise>
				</c:choose>
			</td>
			<td>
				<c:if test="${buyShopProduct.status == '3'}">
					<c:if test="${(buyShopProduct.isPriceUpd == 0 && buyShopProduct.priceUpdDate==null)}">
						<a href="javascript:void(0)" onclick="updPrice('${buyShopProduct.id}');" class="layui-btn layui-btn-update layui-btn-mini">修改价格</a>
						<a href="javascript:void(0)" onclick="cancelProductLink('${buyShopProduct.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
							<i class="layui-icon">&#xe7ea;</i>取消</a>							
				    </c:if>
					<c:if test="${(buyShopProduct.isPriceUpd == '1'&& buyShopProduct.isPriceUpdConfirm!=null)}">
						<a href="javascript:void(0)" onclick="updPrice('${buyShopProduct.id}');" class="layui-btn layui-btn-update layui-btn-mini">修改价格</a>
						<a href="javascript:void(0)" onclick="cancelProductLink('${buyShopProduct.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
							<i class="layui-icon">&#xe7ea;</i>取消</a>							
				    </c:if>
					<c:if test="${(buyShopProduct.isPriceUpd == 1 && buyShopProduct.isPriceUpdConfirm == null)}">
						审批中
						<input id="linktype" name="linktype" type="hidden" value="${buyShopProduct.relatedId}" />
						<br>	
						<div class="opinion_view">						
							<span class="orange" data="${buyShopProduct.relatedId}">查看审批流程</span>
						</div>
					</c:if>
				</c:if>
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<!--分页-->
<div class="pager">${searchPageUtil.page }</div>
