<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<script type="text/javascript">
var menuId="<%=request.getParameter("menuId")%>";
$(function(){
	$(".lf_buyer>div>h4").click(function(){
	  $(this).next().slideToggle(300,'linear');
	  $(this).find('span').toggleClass('fa-caret-up');
	});
	$(".lf_buyer li.childLi").click(function(){
		var id = $(this).attr("data");
		var child = $(".lf_buyer li[name='"+id+"']");
	  	child.slideToggle(300,'linear');
	  	$(this).find('span').toggleClass('fa-caret-up');
	});
});
</script>
<ul>
<c:forEach var="parentMenu" items="${sessionScope.AUTHORITYSYSMENU}">
	<c:if test="${parentMenu.id==menuId}">
		<c:forEach var="leftMenu" items="${parentMenu.childrenMenuList}">
			<li class="menu-item" data="${leftMenu.id}" id="${leftMenu.id}">
	            <a href="javascript:;">
	            	<i class="layui-icon">${leftMenu.icon}</i>
	            	<span>${leftMenu.name}</span>
	            	<i class="my-icon menu-more"></i>
	           	</a>
	            <ul>
	            <c:forEach var="children" items="${leftMenu.childrenMenuList}">
	                <li class="childLi" data="${children.id}" id="${children.id}">
	               	<c:choose>
	                	<c:when test="${children.childrenMenuList != null && children.childrenMenuList.size() > 0}">
		                	<a href="javascript:;"><span>${children.name}</span>
		                		<i class="my-icon menu-more"></i>
		                	</a>
	                		<ul>
				            <c:forEach var="grandson" items="${children.childrenMenuList}">
				                <li class="grandsonLi" name="${children.id}" id="${grandson.id}">
				                	<a href="javascript:;" onclick="leftMenuClick(this,'${grandson.url}','buyer','${grandson.id}')">
				                		<span>${grandson.name}</span>
			                		</a>
				               	</li>
				            </c:forEach>
				            </ul>
	                	</c:when>
	                	<c:otherwise>
	                		<a href="javascript:;" onclick="leftMenuClick(this,'${children.url}','buyer','${children.id}')">
	                			<span>${children.name}</span>
                			</a>
	                	</c:otherwise>
	               	</c:choose>
	               	</li>
	            </c:forEach>
	            </ul>
	        </li>
		</c:forEach>
	</c:if>
</c:forEach>
</ul>

<%-- <div class="lf_buyer">
	<h3></h3>
<c:forEach var="parentMenu" items="${sessionScope.AUTHORITYSYSMENU}">
	<c:if test="${parentMenu.id==menuId}">
		<c:forEach var="leftMenu" items="${parentMenu.childrenMenuList}">
		<div>
			<h4>
				${leftMenu.icon}${leftMenu.name}
				<c:choose>
					<c:when test="${leftMenu.childrenMenuList != null && leftMenu.childrenMenuList.size() >0}">
						<span class="fa fa-caret-down"></span>
					</c:when>
					<c:otherwise>
						<span class="fa fa-caret-left"></span>
					</c:otherwise>
				</c:choose>
			</h4>
			<ul>
			<c:forEach var="leftMenuChildren" items="${leftMenu.childrenMenuList}">
					<c:if test="${leftMenuChildren.url == ''}">
						<li class="childLi" data="${leftMenuChildren.id}" id="${leftMenuChildren.id}">
							<a href="javascript:void(0)">
								${leftMenuChildren.name}
								<c:choose>
									<c:when test="${leftMenuChildren.childrenMenuList != null && leftMenuChildren.childrenMenuList.size() >0}">
										<span class="fa fa-caret-down"></span>
									</c:when>
									<c:otherwise>
										<span class="fa fa-caret-left"></span>
									</c:otherwise>
								</c:choose>
							</a>
						</li>
						<c:forEach var="grandson" items="${leftMenuChildren.childrenMenuList}">
							<li name="${leftMenuChildren.id}" id="${grandson.id}">
								<a href="javascript:void(0)" onclick="leftMenuClick(this,'${grandson.url}','buyer','${grandson.id}')">
									&nbsp;&nbsp;&nbsp;
									${grandson.name}
								</a>
							</li>
						</c:forEach>
					</c:if>
					<c:if test="${leftMenuChildren.url != ''}">
						<li id="${leftMenuChildren.id}">
							<a href="javascript:void(0)" onclick="leftMenuClick(this,'${leftMenuChildren.url}','buyer','${leftMenuChildren.id}')">
								${leftMenuChildren.name}
							</a>
						</li>
					</c:if>
			</c:forEach>
			</ul>
		</div>
		</c:forEach>
	</c:if>
</c:forEach>
</div> --%>