<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<script type="text/javascript">
$(function() {
	$('.lf_system>div>h4').click(function() {
		$(this).next().slideToggle(300, 'linear');
		$(this).find('span').toggleClass('fa-caret-up');
	});
});
</script>
<ul>
	<c:forEach var="parentMenu" items="${sessionScope.AUTHORITYSYSMENU}">
		<c:if test="${parentMenu.id==menuId}">
			<c:forEach var="leftMenuChildren" items="${parentMenu.childrenMenuList}" varStatus="count">
				<li class="menu-item" id="${leftMenuChildren.id}">
					<a href="javascript:void(0)" onclick="leftMenuClick(this,'${leftMenuChildren.url}','system','${leftMenuChildren.id}')">
						${leftMenuChildren.name}
					</a>
				</li>
			</c:forEach>
		</c:if>
	</c:forEach>
</ul>
<%-- <div class="lf_system">
	<h3></h3>
	<div>
		<h4>
			<i class="layui-icon">&#xe7e5;</i>系统设置<span class='fa fa-caret-down'></span>
		</h4>
		<ul>
			<c:forEach var="parentMenu" items="${sessionScope.AUTHORITYSYSMENU}">
				<c:if test="${parentMenu.id==menuId}">
					<c:forEach var="leftMenuChildren" items="${parentMenu.childrenMenuList}" varStatus="count">
						<li id="${leftMenuChildren.id}">
							<a href="javascript:void(0)" onclick="leftMenuClick(this,'${leftMenuChildren.url}','system','${leftMenuChildren.id}')">
								${leftMenuChildren.name}
							</a>
						</li>
					</c:forEach>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div> --%>