<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script>
layui.use(['form'], function(){
  	var form = layui.form;
  	//渲染form
	form.render();
	//监听指定开关
   	form.on('switch(isAdminFilter)', function(data){
   		if(this.checked){
   			$("input[name='isAdmin']").val("1");
   		}else{
   			$("input[name='isAdmin']").val("0");
   		}
   	});
});
</script>
<form class="layui-form" action="platform/tradeRole/saveAdd">
	<div class="layui-form-item">
		<div class="layui-inline">
	      <label class="layui-form-label">角色代码</label>
	      <div class="layui-input-inline">
	        <input type="tel" name="roleCode" lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入角色代码">
	      </div>
	    </div>
	</div>
	<div class="layui-form-item">
		<div class="layui-inline">
	      <label class="layui-form-label">角色名称</label>
	      <div class="layui-input-inline">
	        <input type="tel" name="roleName" lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入角色名称">
	      </div>
	    </div>
	</div>
	<div class="layui-form-item">
    <label class="layui-form-label">是否管理员</label>
    <div class="layui-input-block">
    	<input type="checkbox" name="isAdmiinSwitch" lay-skin="switch" lay-filter="isAdminFilter" lay-text="是|否" value="0">
    </div>
  </div>
	<div class="layui-form-item layui-form-text layopen-input1">
		<label class="layui-form-label">备注</label>
		<div class="layui-input-block">
			<textarea placeholder="请输入内容" class="layui-textarea" name="remark"></textarea>
		</div>
	</div>
	<input name="isAdmin" type="hidden" value="0">
</form>
