<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/commodity_all.css">
<title>加载商品库存历史界面</title>
<script type="text/javascript">
function select(){
	var startDate = $("#startDate").val();
	if(startDate == ''){
		/*layer.tips('请选择开始时间！', '#startDate',{
			tips: [1, '#f00'],
  			time: 2000
		});*/
		return;
	}
	var endDate = $("#endDate").val();
	if(endDate == ''){
		/*layer.tips('请选择结束时间！', '#endDate',{
			tips: [1, '#f00'],
  			time: 2000
		});*/
		return;
	}
	loadPlatformData();
}

layui.use('laydate', function(){
//	  var myDate = new Date(new Date()-24*60*60*1000);
	var laydate = layui.laydate;
	//限定可选日期
	  var ins22 = laydate.render({
	    elem: '#startDate'
	    ,min: '1970-10-14'
	    ,max: -1
	    ,btns: ['clear']
	    ,ready: function(){
	//      ins22.hint('日期可选值设定在 <br> 今天之前');
	    }
	  });
	  var ins23 = laydate.render({
		    elem: '#endDate'
		    ,min: '1970-10-14'
		    ,max: -1
		    ,btns: ['clear']
		    ,ready: function(){
		//      ins22.hint('日期可选值设定在 <br> 今天之前');
		    }
		  });
});
</script>
<style>
.table_blue tbody tr{
	 height: 50px;
}
</style>
<div id="prodcutshistoryContent">
  <!--搜索栏-->
  <form class="layui-form" action="platform/baseInfo/inventorymanage/selectProdcuts">
    <ul class="order_search summary_list">
      <li class="range">
         <label>日&nbsp;期:</label>
         <div class="layui-input-inline">
             <input type="text" name="startDate" id="startDate" value="${map.startDate}"  lay-verify="date" placeholder="开始日" class="layui-input" style="width: 100px" required>
         </div>
         <div class="layui-input-inline">
               <input type="text" name="endDate" id="endDate" value="${map.endDate}" lay-verify="date" placeholder="截止日" class="layui-input" style="width: 100px" required>
         </div>
     </li>
      <li>
        <label>商品名称:</label>
        <input type="text" placeholder="输入商品名称进行搜索" id="productName" name="productName" value="${map.productName }">
      </li>
      <li>
        <label>商品货号:</label>
        <input type="text" placeholder="输入商品货号进行搜索" id="productCode" name="productCode" value="${map.productCode }" style="width: 240px;"> 
      </li>
      <li>
        <label>条形码:</label>
        <input type="text" placeholder="输入条形码" id="barcode" name="barcode" value="${map.barcode}" style="width: 208px">
      </li>
      <li>
        <label>仓&nbsp;&nbsp;库:</label>
        <div class="layui-input-inline">
            <select name="warehouseId" id="warehouseId">
				<option value="">请选择</option>
				<c:forEach var="warehouse" items="${warehouseList}">
					<option value="${warehouse.id}">${warehouse.whareaName}</option>
				</c:forEach>
			</select>
        </div>
      </li>

      <li class=" rt">
        <button class="search" onclick="select();">搜索</button>
      </li>
    </ul>
    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
    
     <!--列表区-->
  <table class="table_blue">
    <thead>
    <tr>
      <td style="width:5%" rowspan="2">货号</td>
      <td style="width:6%" rowspan="2">商品名称</td>
      <td style="width:5%" rowspan="2">规格代码</td>
      <td style="width:5%" rowspan="2">规格名称</td>
      <td style="width:5%" rowspan="2">条形码</td>
      <td style="width:4%" rowspan="2">仓库</td>
      <td style="width:6%" colspan="2">库存</td>
    </tr>
    <tr>
      <td>数量</td>
      <td>成本</td>
    </tr>
    </thead>
    <tbody id="prodcutsList">
    <c:forEach var="prodcutsList" items="${params}" varStatus="status">
	    <tr>
	      <td title="${prodcutsList.productCode}">${prodcutsList.productCode}</td>
	      <td title="${prodcutsList.productName}">${prodcutsList.productName}</td>
	      <td title="${prodcutsList.skuCode}">${prodcutsList.skuCode}</td>
	      <td title="${prodcutsList.skuName}">${prodcutsList.skuName}</td>
	      <td title="${prodcutsList.barcode}">${prodcutsList.barcode}</td>
	      <td>${prodcutsList.warehouseId}</td>
	      <td title="${prodcutsList.stocks}">${prodcutsList.stocks}</td>
	      <td title="${prodcutsList.totalCost}">${prodcutsList.totalCost}</td>
	    </tr>
    </c:forEach>
    </tbody>
  </table>


<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
  </form>

</div>

