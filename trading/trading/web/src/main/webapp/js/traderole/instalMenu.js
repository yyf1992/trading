$(function(){
	//获得菜单信息
	loadCompanyMenu(tradeRole.id);
});
function loadCompanyMenu(tradeId){
	layer.load();
	$.ajax({
		url : "platform/tradeRole/loadCompanyMenu",
		/*async:false,*/
		data:{"tradeId":tradeId},
		success:function(data){
			layer.closeAll('loading');
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var sysMenuList = resultObj.sysMenuList;
			var oldMenuList = resultObj.oldMenuList;
			var trObjStr = new Array();
			//递归拼接数据
			recursively(sysMenuList,trObjStr);
			$.each(trObjStr,function(i,data){
				$(".menuTable tbody").append(data);
			});
			//设置隐藏显示按钮
			setShowHidenButton();
			if(tradeRole.isAdmin=='1'){
				//管理员
				$("table.menuTable :checkbox").prop("checked",true);
				$("table.menuTable :checkbox").attr("disabled",true);
				$("table.menuTable select[name='verify']").show();
				$("table.menuTable select[name='verify']").val("0");
				$("table.menuTable select[name='verify']").attr("disabled",true);
			}else{
				//设置选中
				$.each(oldMenuList,function(i,oldMenu){
					var menuId = oldMenu.menuId;
					var trObj = $("table.menuTable tr[ownId='"+menuId+"']");
					trObj.find("input:checkbox[name='checkmenu']").prop("checked",true);
					trObj.find("select[name='verify']").val(oldMenu.verifyType);
					trObj.find("select[name='verify']").show();
				});
			}
		},
		error:function(){
			layer.closeAll('loading');
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//显示隐藏是否需要审批
function showHideSelect(){
	$("table.menuTable :checkbox").each(function(){
		var flag = $(this).is(":checked");
		var selectObj = $(this).parents("tr").find("select");
		if(selectObj.length>0){
			if(flag){
				selectObj.show();
			}else{
				selectObj.hide();
			}
		}
	});
}
//确认提交
function submitSave(){
	if(tradeRole.isAdmin=='1'){
		//管理员
		/*layer.msg("保存成功！", {icon : 1});
		leftMenuClick(this,'platform/tradeRole/tradeRoleList','system');*/
	}else{
		var checkbox = $("table.menuTable :checkbox:checked");
		var dataStr = "";
		$.each(checkbox,function(i,data){
			var menuId = $(this).val();
			var trObj = $(this).parents("tr");
			var verifyType = "0";
			if(trObj.find("select[name='verify']").length>0){
				verifyType = trObj.find("select[name='verify']").val();
			}
			dataStr += menuId+","+verifyType+"@";
		});
		layer.load();
		$.ajax({
			url:"platform/tradeRole/saveRoleMenu",
			data:{
				"roleId" : tradeRole.id,
				"dataStr":dataStr
			},
			success:function(data){
				layer.closeAll('loading');
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.flag){
					layer.msg("保存成功！", {icon : 1});
					leftMenuClick(this,'platform/tradeRole/tradeRoleList','system','17100920503219724288');
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error:function(){
				layer.closeAll('loading');
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//递归拼接tr
function recursively(sysMenuList,trObjStr){
	if(sysMenuList.length > 0){
		$.each(sysMenuList,function(i,data){
			var trObj = pinJieTrObj(data);
			trObjStr.push(trObj);
			var childFlag = data.childrenMenuList != null && data.childrenMenuList.length > 0;
			if(childFlag){
				recursively(data.childrenMenuList,trObjStr);
			}
		});
	}
}
//拼接tr
function pinJieTrObj(sysMenu){
	var childFlag = sysMenu.childrenMenuList != null && sysMenu.childrenMenuList.length > 0;
	var isMenu = sysMenu.isMenu==0?true:false;
	var isParent = sysMenu.parentId==null || sysMenu.parentId== '';
	var trObj = "";
	trObj = "<tr name='"+sysMenu.parentId+"' ownId='"+sysMenu.id+"'>";
	var level = parseInt(sysMenu.level);
	var marginLeft = level*30;
	trObj += "<td style='padding-left: "+marginLeft+"px;'>";
	trObj += "<input type='checkbox' name='checkmenu' value='"+sysMenu.id+"' onclick='checkedTr(this);'>";
	trObj += "<input type='hidden' name='sysMenuId' value='"+sysMenu.id+"'>";
	trObj += sysMenu.name;
	trObj += "</td>";
	if(sysMenu.position == 'top'){
		trObj += "<td>上部</td>";
	}else if(sysMenu.position == 'left'){
		trObj += "<td>左侧</td>";
	}else{
		trObj += "<td>中部按钮</td>";
	}
	if(isMenu){
		trObj += "<td>菜单</td>";
	}else{
		trObj += "<td>按钮</td>";
	}
	if(isMenu&&sysMenu.position == 'left'&&level>=3){
		trObj += "<td>";
		trObj += "<select name='verify' style='height:20px;display:none'>";
		trObj += "<option value='1'>是</option>";
		trObj += "<option value='0'>否</option>";
		trObj += "</select>";
		trObj += "</td>";
	}else{
		trObj += "<td></td>";
	}
	trObj += "</tr>";
	return trObj;
}
//切换显示
function switchSysMenu(obj){
	var trObj = $(obj).parents("tr");
	var id = trObj.find("input[name='sysMenuId']").val();
	var type = $(obj).find("i.layui-icon").attr("name");
	if(type=='close'){
		//改变图标
		$(obj).find("i.layui-icon").remove();
		var iObj = "<i class='layui-icon' name='open'>&#xe6c8;</i>";
		$(obj).append(iObj);
		//关闭所有的子
		closeAllChild(id);
	}else if(type=='open'){
		//改变图标
		$(obj).find("i.layui-icon").remove();
		var iObj = "<i class='layui-icon' name='close'>&#xea1a;</i>";
		$(obj).append(iObj);
		//只打开子
		$("tr[name='"+id+"']").show();
	}
}
//关闭所有的子
function closeAllChild(parentId){
	$("tr[name='"+parentId+"']").each(function(){
		var id = $(this).find("input[name='sysMenuId']").val();
		var iObj = $(this).find("i.layui-icon");
		if(iObj.length > 0){
			//有子数据
			//改变图标
			var iParentObj = iObj.parent();
			iObj.remove();
			iParentObj.append("<i class='layui-icon' name='open'>&#xe6c8;</i>");
			//关闭子
			closeAllChild(id);
		}
		$(this).hide();
	});
}
//选中
function checkedTr(obj){
	var flag = $(obj).is(":checked");
	var id = $(obj).val();
	//操作所有的子
	checkedAllChild(id,flag);
	//获得所有的兄弟
	var parentId = $(obj).parents("tr").attr("name");
	checkedAllParent(parentId,flag);
	//显示隐藏是否需要审批
	showHideSelect();
}
//设置父元素的选中状态
function checkedAllParent(id,flag){
	var trObj = $("tr[ownId='"+id+"']");
	if(trObj.length>0){
		if(flag){
			//选中时，选中所有的父
			trObj.find("input:checkbox[name='checkmenu']").prop("checked",flag);
		}else{
			//不选中时判断
			var flag2 = true;
			$("tr[name='"+id+"'] input:checkbox[name='checkmenu']").each(function(){
				if($(this).is(":checked")!=flag){
					flag2 = false;
					return false;
				}
			});
			if(flag2){
				trObj.find("input:checkbox[name='checkmenu']").prop("checked",flag);
			}
		}
		var parentId = trObj.attr("name");
		checkedAllParent(parentId,flag);
	}
}
//给所有的子设置选中状态
function checkedAllChild(parentId,flag){
	$("tr[name='"+parentId+"']").each(function(){
		var id = $(this).find("input[name='sysMenuId']").val();
		checkedAllChild(id,flag);
		$(this).find("input:checkbox[name='checkmenu']").prop("checked",flag);
	});
}
//设置隐藏显示按钮
function setShowHidenButton(){
	$(".menuTable tbody tr").each(function(){
		var id = $(this).attr("ownid");
		if($("tr[name='"+id+"']").length>0){
			var aObj = "<a href='javascript:void(0)' onclick='switchSysMenu(this)'><i class='layui-icon' name='close'>&#xea1a;</i></a>";
			$(this).find("td:first").prepend(aObj);
		}
	});
}