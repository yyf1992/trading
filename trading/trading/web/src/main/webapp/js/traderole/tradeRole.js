// 新增
function loadAdd(){
	$.ajax({
		url:basePath+"platform/tradeRole/loadAddHtml",
		success:function(data){
			layer.open({
				type:1,
				title:"新增角色",
				skin: 'layui-layer-rim',
	  		    area: ['450px', 'auto'],
	  		    btn:['保存','取消'],
	  		    content:data,
	  		    yes:function(index,layero){
	  		    	var formObj = $(layero).find("form");
	  		    	//角色代码判空
	  		    	var roleCode = formObj.find("input[name='roleCode']").val();
	  		    	if(roleCode == ''){
	  		    		layer.msg("角色代码不能为空！",{icon:2});
	  		    		return false;
	  		    	}
	  		    	//角色名称判空
	  		    	var roleName = formObj.find("input[name='roleName']").val();
	  		    	if(roleName == ''){
	  		    		layer.msg("角色名称不能为空！",{icon:2});
	  		    		return false;
	  		    	}
	  		    	var formData = formObj.serializeArray();
	  		    	var url = formObj.attr("action");
	  		    	layer.load();
	  		    	$.ajax({
	  		    		url:url,
	  		    		data:formData,
	  		    		success:function(data){
	  		    			var result = eval('(' + data + ')');
	  		    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  		    			if(resultObj.flag){
	  		    				//保存成功
	  		    				layer.closeAll();
	  		    				loadPlatformData();
	  		    			}else{
	  		    				layer.closeAll('loading');
		  		    			layer.msg(resultObj.msg,{icon:2});
	  		    			}
	  		    		},
	  		    		error:function(){
	  		    			layer.closeAll('loading');
	  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  		    		}
	  		    	});
	  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteTradeRole(id){
	layer.msg('确定删除吗？', {
	  time: 0 //不自动关闭
	  ,btn: ['确定', '取消']
	  ,yes: function(index){
	    layer.close(index);
	    layer.load();
    	$.ajax({
    		url:"platform/tradeRole/saveDelete",
    		data:{"id":id},
    		success:function(data){
    			var result = eval('(' + data + ')');
    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    			if(resultObj.flag){
    				//保存成功
    				layer.closeAll();
    				loadPlatformData();
    			}else{
    				layer.closeAll('loading');
	    			layer.msg(resultObj.msg,{icon:2});
    			}
    		},
    		error:function(){
    			layer.closeAll('loading');
    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
    		}
    	});
	  }
	});
}
//切换状态
function switchStatus(id,type){
	var typeName = type=='0'?'启用':'禁用';
	layer.msg('确定'+typeName+'吗？', {
	  time: 0 //不自动关闭
	  ,btn: ['确定', '取消']
	  ,yes: function(index){
	    layer.close(index);
	    layer.load();
    	$.ajax({
    		url:"platform/tradeRole/switchStatus",
    		data:{
				"id":id,
				"status":type
			},
    		success:function(data){
    			var result = eval('(' + data + ')');
    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    			if(resultObj.flag){
    				//保存成功
    				layer.closeAll();
    				loadPlatformData();
    			}else{
    				layer.closeAll('loading');
	    			layer.msg(resultObj.msg,{icon:2});
    			}
    		},
    		error:function(){
    			layer.closeAll('loading');
    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
    		}
    	});
	  }
	});
}
