/**
 * 导出
 */
function sellerRecExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/seller/billReconciliation/sellerRecExport?"+ formData;
    window.open(url);
}

//发起对账
function launchReconciliation(id){
    layer.confirm('确定发起对账吗？', {
            icon:3,
            title:'提示',
            closeBtn:2,
            skin:'popBarcode',
            btn: ['确定','取消']
        },
        function(){
            var url = "platform/seller/billReconciliation/launchReconciliation";
            $.ajax({
                type : "post",
                url : url,
                data : {
                    "id" : id
                },
                async : false,
                success : function(data) {
                    var result = eval('(' + data + ')');
                     var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                     if (resultObj.success) {
                     layer.msg(resultObj.msg,{icon:1},function(){
                     leftMenuClick(this,"platform/seller/billReconciliation/billReconciliationList","sellers");
                     });
                     } else {
                     layer.msg(resultObj.msg,{icon:2});
                     }
                    /*layer.msg("对账发起成功！",{icon:1});
                    leftMenuClick(this,"platform/seller/billReconciliation/billReconciliationList","sellers");*/
                },
                error : function() {
                    layer.msg("对账发起失败，请稍后重试！",{icon:2});
                }
            });
        });
}

//收款凭据
function showBillReceipt() {
    //var buyCompanyName = buyCompanyName;
   // alert(buyCompanyName);
    findBillReceipt();
    layer.open({
        type: 1,
        title: '收获回单',
        area: ['420px', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkReceipt'),
        yes : function(index){
            layer.close(index);
        }/*,
        no : function(index){
            layer.close(index);
        }*/
    });
}

//收据发票
function findBillReceipt() {
    $("#icon_list").html("");
    var trObj1 = $("<li>");
    var trObj2 = $("<li>");
    var trObj3 = $("<li>");
    var trObj4 = $("<li>");
    var trObj5 = $("<li>");
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj1);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj2);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj3);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj4);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj5);

    $("#icon_list").append(trObj1);
    $("#icon_list").append(trObj2);
    $("#icon_list").append(trObj3);
    $("#icon_list").append(trObj4);
    $("#icon_list").append(trObj5);
}



//账单结算周期审批
//$('.bill_approval').click(function(){
function reconciliationAccept(reconciliationId) {
  layer.open({
    type:1,
    title:'账单审批',
    area:['380px','auto'],
    skin:'popBarcode',
    closeBtn:2,
    content:$('.bill_exam'),
    btn:['确定','取消'],
    yes:function(index){
    	var	agreeStatus = $("input[name='agreeStatus']:checked").val();
    	//alert("审批选择"+agreeStatus)
    	var	approvalRemarks = $("#approvalRemarks").val();
    	if(!agreeStatus){
    		layer.msg("请选择审批状态！",{icon:2});
            return;
    	}else if(approvalRemarks.length >300){
           layer.msg("备注内容不超过300个字符！",{icon:2});
            return;
    	}else{
    		$.ajax({
                type : "POST",
                url : "platform/seller/billReconciliation/updateReconciliation",
                data: {
                	"reconciliationId": reconciliationId,
                    "billDealStatus": agreeStatus,
                    "dealRemarks": approvalRemarks
                },
                async:false,
                success:function(data){
                    layer.close(index);
                    layer.msg("保存成功！",{icon:1});
                    loadPlatformData();
                },
                error:function(){
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
    		});
    	}
    },no : function(index){
        layer.close(index);
    }
  });
}  
//});

//删除供应商
function beginAccept(){
	layer.confirm('确定发起对账吗？', {
	      icon:3,
	      title:'提示',
	      closeBtn:2,
          skin:'popBarcode',
	      btn: ['确定','取消']
	    },
        function(index){
        layer.close(index);
    });
}

//添加账单附件
function addBillRecFiles(reconciliationId,createBillUserName,startDateStr,endDateStr){
    findBillFileInfo();
    layer.open({
        type:1,
        title:'添加 '+createBillUserName+' '+startDateStr+'-'+endDateStr+' 周期账单附件',
        area:['auto','auto'],
        skin:'popBarcode',
        closeBtn:2,
        content:$('#billFiles'),
        btn:['确定','取消'],
        yes:function(index){

            var billFileStr = ""
            var attachments = $("input[name='fileUrl']");
           /* if(attachments==undefined||attachments.length<=0){
                layer.msg("请上传附件！",{icon:2});
                return;
            }*/
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    //attachmentsArr.push(attachments[i].value);
                    var fileAddr = attachments[i].value;
                    billFileStr += fileAddr + ",";
                }
            }
            billFileStr = billFileStr.substring(0,billFileStr.length-1);

            var	billFileRemarks = $("#billFileRemarks").val();
            if(billFileRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else{
                var url = "platform/seller/billReconciliation/addBillFileInfo"
                $.ajax({
                    type : "POST",
                    url : url,
                    data: {
                        "reconciliationId":reconciliationId,
                        "billFileStr":billFileStr,
                        "billFileRemarks": billFileRemarks
                    },
                    async:false,
                    success:function(data){
                     var result = eval('(' + data + ')');
                     var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                     if(resultObj.success){
                     layer.close(index);
                     layer.msg(resultObj.msg,{icon:1});
                     loadPlatformData();
                     //leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList?paymentStatus=1','buyer')
                     }else{
                     layer.msg(resultObj.msg,{icon:2});
                     }
                     },
                   /* success:function(data){
                        if(data.flag){
                            var res = data.res;
                            if(res.code==40000){
                                //调用成功
                                layer.msg("添加成功！",{icon:1});
                                leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer')
                            }
                        }else{
                            layer.msg("获取数据失败，请稍后重试！",{icon:2});
                            return false;
                        }
                    }*/
                    error:function(){
                        layer.msg("付款失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}

//付款明细
function findBillFileInfo() {
    $("#billFilesBody").empty();

    var liObj1 = $("<li>");
    $("<span>"+"账单票据："+"</span>").appendTo(liObj1);
    $("<div class='upload_license' style='width: 70px;height: 20px'>"+"<input type='file' multiple name='billFileAddr' id='billFileAddr'onchange='uploadPayment();' style='opacity: 1'>"+"</div>").appendTo(liObj1);
    $("<p></p>").appendTo(liObj1);
    $("<span style='white-space: nowrap'>"+"仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M"+"</span>").appendTo(liObj1);
    $("<div class='scanning_copy original' id='billFileDiv'></div>").appendTo(liObj1);
    $("#billFilesBody").append(liObj1);

    var liObj2 = $("<li>");
    $("<span>"+"备注："+"</span>").appendTo(liObj2);
    $("<textarea type='text' name='billFileRemarks' id='billFileRemarks'>").appendTo(liObj2);
    $("#billFilesBody").append(liObj2);
}

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("billFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/seller/billReconciliation/uploadBillFile";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#billFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#billFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#billFileAddr").replaceWith('<input type="file" style="opacity:1" multiple name="billFileAddr" id="billFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}

//账单附件回显
function openBillFile(reconciliationId,createBillUserName,startDateStr,endDateStr) {
    showBillFiles(reconciliationId);
    layer.open({
        type: 1,
        title: createBillUserName+''+startDateStr+'-'+endDateStr+'周期账单附件',
        area: ['900px', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillFileList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//账单附件信息查询
function showBillFiles(reconciliationId) {
   $("#billRecFileBody").empty();
   // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/seller/billReconciliation/queryBillFileList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            /*var createUserId = resultObj.createUserId;
            var createUserName = resultObj.createUserName;
            var deliveryItemList = resultObj.deliveryItemList;*/
            $.each(resultObj,function(i,o){
                //创建人编号
                var createUserId = o.createUserId;
                //上传人名称
                var createUserName = o.createUserName;
                //创建时间
                var createTime = o.createTime;
                //附件路径
                var filesAddress = o.filesAddress;
                //备注
                var remarks = o.remarks;

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTime+"</td>").appendTo(trObj);
                $("<td>"+remarks+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+filesAddress+"','recFile');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#billRecFileBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//附件显示隐藏
var hideOrShow = true;
function showBillRecFileList(receiptAddr,fileType) {
    if(receiptAddr != null || receiptAddr != ''){
        var bigImgShow = "";
        var smallImg = "";
        var showOrHidden = "";
        if(fileType == "custom"){
            $("#showCustomBigImg").empty();
            $("#fileCustomList").empty();
            bigImgShow = "#showCustomBigImg";
            smallImg = "#fileCustomList";
            showOrHidden = "#showCustomFiles";
        }else if(fileType == "recFile"){
            $("#showBigImg").empty();
            $("#fileList").empty();
            bigImgShow = "#showBigImg";
            smallImg = "#fileList";
            showOrHidden = "#showFiles";
        }
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $(smallImg).append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(bigImgShow).append($("<img id='bigImg' src='"+bigImg+"'></img>"));

        if(hideOrShow){
            $(showOrHidden).show();
            hideOrShow = false;
        }else {
            $(showOrHidden).hide();
            hideOrShow = true
        }
    }
}

//奖惩待确认
function acceptCustomPayment(reconciliationId) {
    layer.open({
        type:1,
        title:"买家奖惩审批",
        area:['380px','auto'],
        skin:'popBarcode',
        closeBtn:2,
        content:$('#acceptBillCustom'),
        btn:['确定','取消'],
        yes:function(index){
            var	customStatus = $("input[name='customStatus']:checked").val();
            if(!customStatus){
                layer.msg("请选择审批状态！",{icon:2});
                return;
            }
            var acceptUrl = "platform/seller/billReconciliation/acceptBillCustomRec";
            var	customRemarks = "";
            customRemarks = $("#customRemarks").val();
            if(customRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else{
                $.ajax({
                    type : "POST",
                    url : acceptUrl,
                    data: {
                        "reconciliationId": reconciliationId,
                        "customStatus": customStatus,
                        "customRemarks": customRemarks
                    },
                    async:false,
                    success:function(data){
                        layer.close(index);
                        layer.msg("保存成功！",{icon:1});
                        loadPlatformData();
                    },
                    error:function(){
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },no : function(index){
            layer.close(index);
        }
    });
}

//查看自定义付款详情
function showBillRecCustom(reconciliationId) {
    showBillCustomInfo(reconciliationId);
    layer.open({
        type: 1,
        title: '奖惩详情',
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillCustomList'),
        yes : function(index){
            layer.close(index);
        }
    });
}
//自定义附件信息查询
function showBillCustomInfo(reconciliationId) {
    $("#billRecCustomFileBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/seller/billReconciliation/queryBillCustomList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            /*var createUserId = resultObj.createUserId;
             var createUserName = resultObj.createUserName;
             var deliveryItemList = resultObj.deliveryItemList;*/
            $.each(resultObj,function(i,o){
                //创建人编号
                var createUserId = o.createCustomUserId;
                //上传人名称
                var createCustomUserName = o.createCustomUserName;
                //创建时间
                var createCustomTime = o.createCustomTime;
                //附件路径
                var customPhotoAddress = o.customPhotoAddress;
                //奖惩金额
                var customPaymentMoney = o.customPaymentMoney;
                //奖惩类型
                var customType = o.customType;
                var customTypeStr = "";
                if(customType == "1"){
                    customTypeStr = "奖励";
                }else if(customType == "2"){
                    customTypeStr = "处罚";
                }
                //添加备注
                var createRemarks = o.customRemarks;
                //审批状态
                var customStatus = o.customStatus;
                var customStatusStr = "";
                if(customStatus == "9"){
                    customStatusStr = "卖家待确认";
                }else if(customStatus == "10"){
                    customStatusStr = "卖家已确认";
                }else if(customStatus == "11"){
                    customStatusStr = "卖家已驳回";
                }
                //审批时间
                var updateCustomTime = o.updateCustomTime;
                //审批人编号
                var updateCustomUserId = o.updateCustomUserId;
                //审批人名称
                var updateCustomUserName = o.updateCustomUserName;
                //审批人备注
                var updateRemarks = o.updateRemarks;

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createCustomUserName+"</td>").appendTo(trObj);
                $("<td>"+createCustomTime+"</td>").appendTo(trObj);
                $("<td>"+customTypeStr+"</td>").appendTo(trObj);

                $("<td>"+customPaymentMoney+"</td>").appendTo(trObj);
                $("<td>"+createRemarks+"</td>").appendTo(trObj);
                $("<td>"+customStatusStr+"</td>").appendTo(trObj);
                $("<td>"+updateCustomUserName+"</td>").appendTo(trObj);

                $("<td>"+updateCustomTime+"</td>").appendTo(trObj);
                $("<td>"+updateRemarks+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+customPhotoAddress+"','custom');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#billRecCustomFileBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//查看预付款修改详情
function showAdvanceEdit(reconciliationId,sellerCompanyName) {
    showAdvanceEditInfo(reconciliationId,sellerCompanyName);
    layer.open({
        type: 1,
        title: '预付款抵扣详情',
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showAdvanceEditList'),
        yes : function(index){
            layer.close(index);
        }
    });
}
//预付款抵扣信息查询
function showAdvanceEditInfo(reconciliationId,sellerCompanyName) {
    $("#billAdvanceEditBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/seller/billAdvance/queryAdvanceEditList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                //下单人名称
                var createBillUserName = o.createBillUserName;
                //创建人
                var createUserName = o.createUserName;
                //创建时间
                var createTimeStr = o.createTimeStr;
                //预付款抵扣金额
                var updateTotal = o.updateTotal;

                var trObj = $("<tr>");
                $("<td>"+sellerCompanyName+"</td>").appendTo(trObj);
                $("<td>"+createBillUserName+"</td>").appendTo(trObj);
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTimeStr+"</td>").appendTo(trObj);
                $("<td>"+updateTotal+"</td>").appendTo(trObj);

                $("#billAdvanceEditBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}