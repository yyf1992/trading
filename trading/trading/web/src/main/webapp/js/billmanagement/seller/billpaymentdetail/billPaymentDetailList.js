/**
 * 导出
 */
function sellerPaymentExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/seller/billPaymentDetail/sellerPaymentExport?"+ formData;
    window.open(url);
}

//账单结算周期审批
function showPaymentAgree(reconciliationId) {
    layer.open({
        type:1,
        title:'收款确认',
        area:['380px','auto'],
        skin:'popBarcode',
        closeBtn:2,
        content:$('.bill_exam'),
        btn:['确定','取消'],
        yes:function(index){
            var	agreeStatus = $("input[name='agreeStatus']:checked").val();
            var	approvalRemarks = $("#approvalRemarks").val();
            if(!agreeStatus){
                layer.msg("请选择确认状态！",{icon:2});
                return;
            }else if(approvalRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else{
                $.ajax({
                    type : "POST",
                    url : "platform/seller/billPaymentDetail/updateSavePaymentInfo",
                    data: {
                        "reconciliationId": reconciliationId,
                        "billDealStatus": agreeStatus,
                        "paymentRemarks": approvalRemarks
                    },
                    async:false,
                    success:function(data){
                        layer.close(index);
                        layer.msg("保存成功！",{icon:1});
                        loadPlatformData();
                    },
                    error:function(){
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}