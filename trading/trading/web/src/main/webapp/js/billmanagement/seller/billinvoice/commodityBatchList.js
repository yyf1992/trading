/**
 * 导出
 */
function sellerCommodityExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/seller/billInvoice/sellerCommodityExport?"+ formData;
    window.open(url);
}

$(function() {
    pure_chkAll();
});
//table_pure 带复选框的全选
function pure_chkAll() {
    var inputs = $(".table_pure .chk_click input[type='checkbox']");
    function checkedSpan() {
        inputs.each(function() {
            if ($(this).prop('checked') == true) {
                $(this).parent().addClass('checked')
            } else {
                $(this).parent().removeClass('checked')
            }
        })
    }
    $('.table_pure thead input[type="checkbox"]').click(function() {
        inputs.prop('checked', $(this).prop('checked'));
        checkedSpan();
    });
    $('.table_pure tbody input[type="checkbox"]')
        .click(
            function() {
                var r = $('.table_pure tbody input[type="checkbox"]:not(:checked)');
                if (r.length == 0) {
                    $('.table_pure thead input[type="checkbox"]')
                        .prop('checked', true);
                } else {
                    $('.table_pure thead input[type="checkbox"]')
                        .prop('checked', false);
                }
                console.log(11);
                checkedSpan();
            })
}


//选中合并发货，必须是同一家采购商才可以合并发货
$('#batchInvoice').click(function(){
    var layer = layui.layer,form=layui.form;
    var deliveryItemIds = "";//订单详情ID
    if($("input:checkbox[name='checkone']:checked").length < 1){
        layer.msg("至少选中一项！",{icon:2});
        return;
    }else{
        //判断是否是同一家采购商
        var checkedItems = $("input:checkbox[name='checkone']:checked");
        var buyerIdFirst = $(checkedItems[0]).parent().parent().find("input[name='reconciliationId']").val();//第一条选中数据的采购商ID
        deliveryItemIds = $(checkedItems[0]).val();
        for(var i=1;i<checkedItems.length;i++){
            var currBuyerId = $(checkedItems[i]).parent().parent().find("input[name='reconciliationId']").val();
            var currOrderId = $(checkedItems[i]).val();
            if(buyerIdFirst!=currBuyerId){
                layer.msg("必须为同一对账批次才可以合并开票！",{icon:2});
                return;
            }else{
                deliveryItemIds += "," + currOrderId;
               // deliveryItemIds += "'" + currOrderId + "'" + ",";
            }
        }
    }
    //deliveryItemIds = deliveryItemIds.substring(0, deliveryItemIds.length - 1);
   // alert(deliveryItemIds);
    layer.confirm('您确定要将选中的商品合并开票吗?', {
        icon:3,
        title:'选中合并发货',
        skin:'pop',
        closeBtn:2,
        btn: ['确定','取消']
    }, function(index){
        layer.close(index);
        leftMenuClick(this,'platform/seller/billInvoice/drawUpInvoice?deliveryItemIds='+deliveryItemIds,'sellers');
    });
});