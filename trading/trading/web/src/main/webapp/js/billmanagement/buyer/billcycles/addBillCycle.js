/**
 * 账单周期管理JS
 * yuyafei
 * 2017-8-3 10:53:55
 */
//周期设置选择日期
$('#bill_present').focus(function(){
  $('.tab_ul').css({
    display:'block',
    left:136
  });
  $('.tab_ul>li').click(function(){
    var str=$(this).html();
    $('#bill_present').val(str);
    $('.tab_ul').hide();
    $('.tab_ul>li').unbind('click');
  })
});
$('#bill_next').focus(function(){
	$('.tab_ul').css({
	    display:'block',
	    left:136
	  });
	  $('.tab_ul>li').click(function(){
	    var str=$(this).html();
	    $('#bill_next').val(str);
	    $('.tab_ul').hide();
	    $('.tab_ul>li').unbind('click');
	  })
  });
$('#out_account').focus(function(){
	$('.tab_ul').css({
	    display:'block',
	    left:136
	  });
	  $('.tab_ul>li').click(function(){
	    var str=$(this).html();
	    $('#out_account').val(str);
	    $('.tab_ul').hide();
	    $('.tab_ul>li').unbind('click');
	  })
  });
 /* $('#bill_next').focus(function(){
    $('.tab_ul').css({
      display:'block',
      left:281
    });
    $('.tab_ul>li').click(function(){
      var str=$(this).html();
      $('#bill_next').val(str);
      $('.tab_ul').hide();
      $('#out_account').val(str).prop('disabled','true').css('cursor','not-allowed');
      $('.tab_ul>li').unbind('click');
    })
  });*/

//账额利息设置增加一行
$('#interest_add').click(function(){
  var str='<td><input type="number" name="overdueDate" value="0"></td><td><input type="number" name="overdueInterest" value="0"></td><td><select name="interestMethod"><option value="">请选择</option><option value="1">单利</option><option value="2">复利</option></select></td><td><button class="layui-btn layui-btn-mini layui-btn-normal"><i class="layui-icon"></i> 删除</button></td>';
  //var tbl=$('.bill_interest>table tr:eq(-1)');
  var tbl=$('#interestBody');
  addOneRow(str,tbl);
});

//表格增加和删除一行函数
function addOneRow(str,tbl){
    var addTr=document.createElement('tr');
    addTr.innerHTML=str;
    tbl.append(addTr);
}
//删除一行
//$('.bill_interest>table').on('click','button',function(){
$('#interestBody').on('click','button',function(){
  layer.confirm('您确定要删除该项吗？', {
    icon:3,
    skin:'popBarcode',
    title:'提示',
    closeBtn:2,
    btn: ['确定','取消']
  }, function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});

//利息显示或隐藏
var hideOrShow = true;
function hidOrShowInterst() {
    if(hideOrShow){
        $("#interstHidOrShow").show();
        hideOrShow = false;
    }else {
        $("#interstHidOrShow").hide();
        hideOrShow = true
    }
}