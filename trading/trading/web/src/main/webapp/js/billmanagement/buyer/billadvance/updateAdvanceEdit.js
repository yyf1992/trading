$(function(){
    //$("#taskRemarks").val($("#advanceRemarksOld").val());
    var urlStrOld = $("#fileAddressOld").val();
    if(null != urlStrOld && "" != urlStrOld){
        var urlList = new Array();
        urlList = urlStrOld.split(",");
        for(var i=0;i<urlList.length;i++){
            var url = urlList[i];
            var a = "<span>"+
                "<b onclick='deletePhoto();'></b>"+
                "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#advanceFileDiv\");'>"+
                "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                "</span>";
            $("#advanceFileDiv").append(a);
        }
        //重新设置input按钮，防止无法重复提交文件
        $("#advanceFileAddr").replaceWith('<input style="width: 70px;height: 23px;" type="file" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
    }
});

//提交账单周期信息
$('#billCycleInfo_add').click(function(){
    debugger
    //预付款充值编号
    var editId = $("#editId").val();
    var advanceId = $("#advanceId").val()

    var updateTotal = $("#updateTotal").val();
    if(!updateTotal){
        layer.msg("请填写预付款金额！",{icon:2});
        return;
    }else if(updateTotal < 0){
        layer.msg("充值金额不得小于0！",{icon:2});
        return;
    }
    var taskFileStr = ""
    var attachments = $("input[name='fileUrl']");
    /*if(attachments==undefined||attachments.length<=0){
        layer.msg("请上传附件！",{icon:2});
        return;
    }*/
    if(attachments!=undefined&&attachments.length>0){
        for(var i=0;i<attachments.length;i++){
            //attachmentsArr.push(attachments[i].value);
            var fileAddr = attachments[i].value;
            taskFileStr += fileAddr + ",";
        }
    }
    taskFileStr = taskFileStr.substring(0,taskFileStr.length-1);

    var	taskRemarks = $("#taskRemarks").val();
    if(taskRemarks.length >300){
        layer.msg("备注内容不超过300个字符！",{icon:2});
        return;
    }
    var url = "platform/buyer/billAdvanceItem/addChargeAdvance";
    $.ajax({
        type : "POST",
        url : url,
        data: {
            "itemId":editId,
            "itemStatus":"3",
            "advanceId": advanceId,
            "updateTotal": updateTotal,
            "billFileStr":taskFileStr,
            "rechargeRemarks": taskRemarks,
             //"menuName":"18032010273933278191" //58测试
            "menuName":"18032117250690714731"//阿里
        },
        async:false,
        success:function(data){
            if(data.flag){
                var res = data.res;
                if(res.code==40000){
                    //调用成功
                    layer.msg("提交成功！",{icon:1});
                    leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer')
                }else if(res.code==40010){
                    //调用失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40011){
                    //需要设置审批流程
                    layer.msg(res.msg,{time:500,icon:2},function(){
                        setApprovalUser(url,res.data,function(){
                            layer.msg("内部待审批！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer');
                        });
                    });
                    return false;
                }else if(res.code==40012){
                    //对应菜单必填
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40013){
                    //不需要审批
                    notNeedApproval(res.data,function(data){
                        layer.msg("付款成功！",{icon:1});
                        layer.close(index);
                        leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer');
                    });
                    return false;
                }
            }else{
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
                return false;
            }
        },
        error:function(){
            layer.msg("提交失败，请稍后重试！",{icon:2});
        }
    });
});

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("advanceFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billPaymentDetail/uploadPaymentBill";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#advanceFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#advanceFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#advanceFileAddr").replaceWith('<input type="file" style="width: 70px;height: 23px;" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}