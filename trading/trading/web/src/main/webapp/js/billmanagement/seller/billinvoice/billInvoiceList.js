//取消发票
function closeInvoice(id){
    layer.confirm('确定取消该发票吗？', {
            icon:3,
            title:'提示',
            closeBtn:2,
            skin:'popBarcode',
            btn: ['确定','取消']
        },
        function(){
            var url = "platform/seller/billInvoice/closeInvoice";
            $.ajax({
                type : "post",
                url : url,
                data : {
                    "id" : id
                },
                async : false,
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if (resultObj.success) {
                        layer.msg(resultObj.msg,{icon:1},function(){
                            leftMenuClick(this,"platform/seller/billInvoice/billInvoiceList?acceptInvoiceStatus="+'4'+"","sellers");
                        });
                    } else {
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("发票取消失败，请稍后重试！",{icon:2});
                }
            });
        });
}

/**
 * 导出
 */
function sellerInvoiceExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/seller/billInvoice/sellerInvoiceExport?"+ formData;
    window.open(url);
}

//商品明细
function recItemList(settlementNo,invoiceId) {
    findRecItem(invoiceId);
    layer.open({
        type: 1,
        title: '结算单号'+settlementNo+'的商品明细',
        area: ['900px', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#recItemList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//商品明细数据查询
function findRecItem(invoiceId) {
    $("#recItemBody").empty();
    data={
        "invoiceId":invoiceId
    };
    $.ajax({
        url : basePath+"platform/seller/billInvoice/queryRecItemList",
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var arrivalNumCount = resultObj.arrivalNumCount;
            var priceSum = resultObj.priceSum;
            var deliveryItemList = resultObj.deliveryItemList;
            //var innerHtml = $("<tr></tr>");
            $.each(deliveryItemList,function(i,o){
                //发货单号
                var orderCode = o.deliveryRecItem.orderCode;
                //下单人
                var createBillName = o.deliveryRecItem.createBillName;
                //订单号
                var deliverNo = o.deliveryRecItem.deliverNo;
                //商品
                var productName = o.deliveryRecItem.productName;
                //条形码
                var barcode = o.deliveryRecItem.barcode;
                //单位
                var unitName = o.deliveryRecItem.unitName;
                //单价
                var salePrice = o.salePrice;
                //到货数量
                var arrivalNum = o.deliveryRecItem.arrivalNum;
                //总金额
                var salePriceSum = o.salePriceSum;

                var trObj = $("<tr>");
                $("<td>"+deliverNo+"</td>").appendTo(trObj);
                $("<td>"+createBillName+"</td>").appendTo(trObj);
                $("<td>"+orderCode+"</td>").appendTo(trObj);
                $("<td>"+productName+"</td>").appendTo(trObj);
                $("<td>"+barcode+"</td>").appendTo(trObj);
                $("<td>"+unitName+"</td>").appendTo(trObj);
                $("<td>"+salePrice+"</td>").appendTo(trObj);
                $("<td>"+arrivalNum+"</td>").appendTo(trObj);
                $("<td>"+salePriceSum+"</td>").appendTo(trObj);

                $("#recItemBody").append(trObj);
            });

            var trSumObj = $("<tr>");
            var heji = "合计";
            $("<td>"+heji+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+"</td>").appendTo(trSumObj);
            $("<td>"+arrivalNumCount+"</td>").appendTo(trSumObj);
            $("<td>"+priceSum+"</td>").appendTo(trSumObj);

            $("#recItemBody").append(trSumObj);

        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}


//发票票据
function showInvoiceReceipt(settlementNo,invoiceFileAddr) {
    //var buyCompanyName = buyCompanyName;
   // alert(buyCompanyName);
    findInvoiceReceipt(invoiceFileAddr);
    layer.open({
        type: 1,
        title: '结算单号：'+settlementNo+"的票据",
        area: ['auto', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#invoiceReceipt'),
        yes : function(index){
            layer.close(index);
        }/*,
        no : function(index){
            layer.close(index);
        }*/
    });
}

//收据发票
function findInvoiceReceipt(invoiceFileAddr) {
    if(invoiceFileAddr != null || invoiceFileAddr != ''){
        $(".big_img").empty();
        $("#icon_list").empty();
        var receiptAddrList = invoiceFileAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            $("#bigImg").val(receiptAddrList[1]);
            var liObj = $("<li>");
            $("<img src='"+receiptAddrList[i]+"'></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

