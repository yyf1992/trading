/**
 * 导出
 */
function buyerAdvanceExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/buyer/billAdvance/buyerAdvanceExport?"+ formData;
    window.open(url);
}

//付款审批
function acceptBillPayment(advanceId,advanceStatus,acceptId){
    layer.confirm('确定提交内部审批吗？', {
        icon:3,
        title:'提示',
        closeBtn:2,
        skin:'popBuyer',
        btn: ['确定','取消']
    },
    function () {
        var url = "platform/buyer/billAdvance/acceptAdvance"
        $.ajax({
            type : "POST",
            url : url,
            data: {
                "advanceId":advanceId,
                "acceptId":acceptId,
                "advanceStatus":advanceStatus,
               //"menuName":"18021011430677627005" //本地
              // "menuName":"18022811175895965600" //58测试
               "menuName":"18030108115426271489"//阿里
            },
            async:false,
            success:function(data){
                if(data.flag){
                    var res = data.res;
                    if(res.code==40000){
                        //调用成功
                        layer.msg("提交成功！",{icon:1});
                        leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer')
                    }else if(res.code==40010){
                        //调用失败
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40011){
                        //需要设置审批流程
                        layer.msg(res.msg,{time:500,icon:2},function(){
                            setApprovalUser(url,res.data,function(){
                                layer.msg("内部待审批！",{icon:1});
                                leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer');
                            });
                        });
                        return false;
                    }else if(res.code==40012){
                        //对应菜单必填
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40013){
                        //不需要审批
                        notNeedApproval(res.data,function(data){
                            layer.msg("付款成功！",{icon:1});
                            layer.close(index);
                            leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer');
                        });
                        return false;
                    }
                }else{
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                    return false;
                }
            },
            error:function(){
                layer.msg("付款失败，请稍后重试！",{icon:2});
            }
        });
    });
}

//发票票据
function showAdvanceReceipt(advanceId,fileAddress,createBillUserName,createTimeStr) {
    findAdvanceReceipt(fileAddress);
    layer.open({
        type: 1,
        title: '采购员：'+createBillUserName+"在"+createTimeStr+"日创建预付款的票据",
        area: ['auto', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#advanceReceipt'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//收据发票
function findAdvanceReceipt(advanceFileAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(advanceFileAddr != null || advanceFileAddr != ''){
        var bigImg = '';
        var receiptAddrList = advanceFileAddr.split(',');
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src='"+receiptAddrList[i]+"'></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

//提交卖家审批
function launchAdvance(advanceId){
    layer.confirm('确定提交吗？', {
            icon:3,
            title:'提示',
            closeBtn:2,
            skin:'popBuyer',
            btn: ['确定','取消']
        },
        function(){
            var url = "platform/buyer/billAdvance/launchAdvance";
            $.ajax({
                type : "post",
                url : url,
                data : {
                    "advanceId" : advanceId,
                    "advanceStatus" : "5"
                },
                async : false,
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if (resultObj.success) {
                        layer.msg(resultObj.msg,{icon:1},function(){
                            leftMenuClick(this,"platform/buyer/billAdvance/billAdvanceList","buyer");
                        });
                    } else {
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("预付款提交失败，请稍后重试！",{icon:2});
                }
            });
        });
}

//查看充值记录
function showAdvanceEditList(advanceId,createBillUserId,createBillUserName,editStatus) {
    showUpdateEditInfo(advanceId,createBillUserId,createBillUserName,editStatus);
    layer.open({
        type: 1,
        title: '预付款充值记录',
        area: ['900px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#advanceEditDiv'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//自定义附件信息查询
function showUpdateEditInfo(advanceId,createBillUserId,createBillUserName,editStatus) {
    $("#advanceEditBody").empty();
    $.ajax({
        url : basePath+"platform/buyer/billAdvance/queryAdvanceEditList",
        data:  {
            "advanceId":advanceId,
            "createBillUserId":createBillUserId,
            "editStatus":editStatus
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                //充值人
                var createUserName = o.createUserName;
                //充值时间
                var createTimeStr = o.createTimeStr;
                //充值金额
                var updateTotal = o.updateTotal;
                //附件路径
                var fileAddress = o.fileAddress;
                //充值备注
                var advanceRemarks = o.advanceRemarks;
                //充值审批
                var editStatus = o.editStatus;
                var editStatusStr = "";
                if(editStatus == "1"){
                    editStatusStr = "待审批";
                }else if(editStatus == "2"){
                    editStatusStr = "审核通过";
                }else if(editStatus == "3"){
                    editStatusStr = "审核驳回";
                }

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createBillUserName+"</td>").appendTo(trObj);
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTimeStr+"</td>").appendTo(trObj);

                $("<td>"+updateTotal+"</td>").appendTo(trObj);
                $("<td>"+advanceRemarks+"</td>").appendTo(trObj);

                $("<td>"+editStatusStr+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showEditFileList('"+fileAddress+"');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#advanceEditBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//附件显示隐藏
var hideOrShow = true;
function showEditFileList(receiptAddr) {
    $("#showAdvanceBigImg").empty();
    $("#fileAdvanceList").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $("#fileAdvanceList").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $("#showAdvanceBigImg").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
    if(hideOrShow){
        $("#showAdvenceFiles").show();
        hideOrShow = false;
    }else {
        $("#showAdvenceFiles").hide();
        hideOrShow = true
    }
}
