/*$(function(){
    //下单人下拉
    loadAdvanceInfo();
});*/

var form;
layui.use('form', function() {
    form = layui.form;
    form.render("select");
    //下单人下拉
    loadAdvanceInfo();
});

//提交账单周期信息
$('#billCycleInfo_add').click(function(){
    debugger
    //供应商
    var advanceId=$("#advanceAccount #id").val();
    if(!advanceId || advanceId == ''){
        layer.msg("请选择预付款账户！",{icon:2});
        return;
    }
    var upadteTotal = $("#upadteTotal").val();
    if(null == upadteTotal ){
        layer.msg("充值金额不得为空！",{icon:2});
        return
    }else if(upadteTotal < 0){
        layer.msg("充值金额不得小于0！",{icon:2});
        return
    }

    var billFileStr = ""
    var recharges = $("input[name='fileUrl']");
    if(recharges!=undefined&&recharges.length>0){
        for(var i=0;i<recharges.length;i++){
            var fileAddr = recharges[i].value;
            billFileStr += fileAddr + ",";
        }
    }
    billFileStr = billFileStr.substring(0,billFileStr.length-1);

    var rechargeRemarks = $("#taskRemarks").val();//备注
    if(rechargeRemarks.length >300){
        layer.msg("备注内容不超过300个字符！",{icon:2});
        return
    }
    var url = "platform/buyer/billAdvanceItem/addChargeAdvance";
    $.ajax({
        type : "POST",
        url : url,
        data: {
            "itemId":"",
            "itemStatus":"1",
            "advanceId": advanceId,
            "updateTotal": upadteTotal,
            "billFileStr":billFileStr,
            "rechargeRemarks": rechargeRemarks,
             //"menuName":"18032010273933278191" //58测试
            "menuName":"18032117250690714731"//阿里充值
            //"menuName":"18030108115426271489"//阿里预付
        },
        async:false,
        success:function(data){
            if(data.flag){
                var res = data.res;
                if(res.code==40000){
                    //调用成功
                    layer.msg("提交成功！",{icon:1});
                    leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer')
                }else if(res.code==40010){
                    //调用失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40011){
                    //需要设置审批流程
                    layer.msg(res.msg,{time:500,icon:2},function(){
                        setApprovalUser(url,res.data,function(){
                            layer.msg("内部待审批！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer');
                        });
                    });
                    return false;
                }else if(res.code==40012){
                    //对应菜单必填
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40013){
                    //不需要审批
                    notNeedApproval(res.data,function(data){
                        layer.msg("付款成功！",{icon:1});
                        layer.close(index);
                        leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer');
                    });
                    return false;
                }
            }else{
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
                return false;
            }
        },
        error:function(){
            layer.msg("提交失败，请稍后重试！",{icon:2});
        }
    });
});

//加载互通卖家好友下拉
function loadAdvanceInfo(){
    debugger
    var obj={};
    obj.divId="advanceAccount";
    obj.createBillUserName="createBillUserName";
    obj.sellerCompanyName="sellerCompanyName";
    obj.advanceId="id";
    obj.filter="changeSupplier";
    selAdvanceInfo(obj);
}

// 采购商下拉共通
function selAdvanceInfo(obj){
    $("#"+obj.divId).empty();
    var url = basePath+"platform/buyer/billAdvance/queryAdvanceInfo"
    $.ajax({
        url : url,
        async:false,
        success:function(data){
            debugger
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var selectObj = $("<select lay-filter='"+obj.filter+"' lay-search=''></select>");
            if(obj.sellerCompanyName != null && obj.sellerCompanyName != '' && obj.createBillUserName != null && obj.createBillUserName != ''){
                //if( obj.advanceId != null && obj.advanceId != ''){
                selectObj.attr("name",obj.sellerCompanyName+"-"+ obj.createBillUserName);
            }
            if(obj.advanceId != null && obj.advanceId != ''){
                selectObj.attr("id",obj.advanceId);
            }
            $('<option>',{val:"",text:"请选择或输入关键字搜索"}).appendTo(selectObj);
            $.each(resultObj,function(i){
                var id = resultObj[i].id;
                var sellerCompanyName = resultObj[i].sellerCompanyName;
                var createBillUserName = resultObj[i].createBillUserName;
                $('<option>',{val:id,text:sellerCompanyName+"-"+createBillUserName}).appendTo(selectObj);
            });
            $("#"+obj.divId).append(selectObj);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("paymentFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billPaymentDetail/uploadPaymentBill";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#paymentFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#paymentFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#paymentFileAddr").replaceWith('<input type="file" style="width: 70px;height: 23px;" multiple name="paymentFileAddr" id="paymentFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}