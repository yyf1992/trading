//添加发票信息
function saveInvoice(){
    var recItemIds = "";
    var buyCompanyIds = "";
    $("#tabContent tbody>tr").each(function(i){
        var recItemId = $(this).find("input[name='recItemId']").val();
        var buyCompanyId = $(this).find("input[name='buyCompanyId']").val();

        recItemIds += recItemId +",";
        buyCompanyIds +=  buyCompanyId+",";
    })
    recItemIds = recItemIds.substring(0,recItemIds.length-1);
    buyCompanyIds = buyCompanyIds.substring(0,buyCompanyIds.length-1);

    var priceSum = $("#priceSum").val();
    var invoiceType = $("#invoiceType").val();
    var invoiceHeader = $("#invoiceHeader").val();
    var totalInvoiceValue = $("#totalInvoiceValue").val();
    var invoiceNo = $("#invoiceNo").val();

    var fileDivList = $("#invoiceFileDiv").find("span").length;
    /*var invoiceFileStr = ""
    if(fileDivList != 0){
        for(var i = 0;i < fileDivList;i++){
            var fileUrl = $("#invoiceFileDiv").find("span:eq("+i+")").find("input").val();
            invoiceFileStr = invoiceFileStr + fileUrl + ",";
        }
        invoiceFileStr = invoiceFileStr.substring(0, invoiceFileStr.length - 1);
    }*/

    var invoiceFileStr = ""
    var attachments = $("input[name='fileUrl']");
    if(invoiceType == undefined || invoiceType == 0){
        layer.msg("请选择票据类型！",{icon:2});
        return;
    }
    if(invoiceHeader == undefined || invoiceHeader == 0){
        layer.msg("请填写票据抬头！",{icon:2});
        return;
    }
    if(invoiceNo == undefined || invoiceNo == 0){
        layer.msg("请填写票据编号！",{icon:2});
        return;
    }
    if(attachments==undefined||attachments.length<=0){
        layer.msg("请上传附件！",{icon:2});
        return;
    }
    if(attachments!=undefined&&attachments.length>0){
        for(var i=0;i<attachments.length;i++){
            //attachmentsArr.push(attachments[i].value);
            var fileAddr = attachments[i].value;
            invoiceFileStr += fileAddr + ",";
        }
    }
    invoiceFileStr = invoiceFileStr.substring(0,invoiceFileStr.length-1);


    $.ajax({
        type : "POST",
        url:"platform/seller/billInvoice/saveInvoiceInfo",
        async:false,
        data: {
            "buyCompanyIds":buyCompanyIds,
            "salePriceSum":priceSum,
            "invoiceType":invoiceType,
            "invoiceHeader":invoiceHeader,
            "totalInvoiceValue":totalInvoiceValue,
            "invoiceNo":invoiceNo,
            "invoiceFileAddr":invoiceFileStr,
            "recItemIdList":recItemIds
        },
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if(resultObj.success){
                layer.msg(resultObj.msg,{icon:1});
                leftMenuClick(this,'platform/seller/billInvoice/billInvoiceList?acceptInvoiceStatus=1','sellers')
            }else{
                layer.msg(resultObj.msg,{icon:2});
            }
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//上传操作
function uploadInvoice() {
    var formData = new FormData();
    var files = document.getElementById("invoiceFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/seller/billInvoice/uploadInvoiceFile";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#invoiceFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#invoiceFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#invoiceFileAddr").replaceWith('<input type="file" multiple name="invoiceFileAddr" id="invoiceFileAddr" onchange="uploadInvoice();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}

//收款凭据
function showBillReceipt(receiptAddr) {
    if(receiptAddr != ''&& receiptAddr != null){
        findBillReceipt(receiptAddr);
        layer.open({
            type: 1,
            title: '收货回单',
            area: ['auto', 'auto'],
            skin:'popBarcode',
            //btns :2,
            closeBtn:2,
            btn:['关闭窗口'],
            content: $('#linkReceipt'),
            yes : function(index){
                layer.close(index);
            }
        });
    }else if(receiptAddr == ''){
        layer.msg("该账单未上传收货回单!", {icon: 2});
    }else if(receiptAddr == null){
        layer.msg("该账单的收货回单凭据加载失败，请重新打开!", {icon: 2});
    }

}

//收据发票(退货)
function findBillReceipt(receiptAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}
/*
//收据发票
function findBillReceipt(receiptAddr) {
    $("#icon_list").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img>"+receiptAddrList[i]+"</img>").appendTo(liObj);
            $("#icon_list").append(liObj);
        }
    }
}*/
