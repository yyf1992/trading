/**
 * 导出
 */
function buyRecExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/buyer/billReconciliation/buyRecExport?"+ formData;
    window.open(url);
}

//查看自定义付款详情
function showCustomPayment(reconciliationId) {
    showBillCustomInfo(reconciliationId);
    layer.open({
        type: 1,
        title: '奖惩详情',
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillCustomList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//自定义附件信息查询
function showBillCustomInfo(reconciliationId) {
    $("#billRecCustomFileBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/buyer/billReconciliation/queryBillCustomList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            /*var createUserId = resultObj.createUserId;
             var createUserName = resultObj.createUserName;
             var deliveryItemList = resultObj.deliveryItemList;*/
            $.each(resultObj,function(i,o){
                //创建人编号
                var createUserId = o.createCustomUserId;
                //上传人名称
                var createCustomUserName = o.createCustomUserName;
                //创建时间
                var createCustomTime = o.createCustomTime;
                //附件路径
                var customPhotoAddress = o.customPhotoAddress;
                //奖惩金额
                var customPaymentMoney = o.customPaymentMoney;
                //奖惩类型
                var customType = o.customType;
                var customTypeStr = "";
                if(customType == "1"){
                    customTypeStr = "奖励";
                }else if(customType == "2"){
                    customTypeStr = "处罚";
                }
                //添加备注
                var createRemarks = o.customRemarks;
                //审批状态
                var customStatus = o.customStatus;
                var customStatusStr = "";
                if(customStatus == "9"){
                    customStatusStr = "卖家待确认";
                }else if(customStatus == "10"){
                    customStatusStr = "卖家已确认";
                }else if(customStatus == "11"){
                    customStatusStr = "卖家已驳回";
                }
                //审批时间
                var updateCustomTime = o.updateCustomTime;
                //审批人编号
                var updateCustomUserId = o.updateCustomUserId;
                //审批人名称
                var updateCustomUserName = o.updateCustomUserName;
                //审批人备注
                var updateRemarks = o.updateRemarks;

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createCustomUserName+"</td>").appendTo(trObj);
                $("<td>"+createCustomTime+"</td>").appendTo(trObj);
                $("<td>"+customTypeStr+"</td>").appendTo(trObj);

                $("<td>"+customPaymentMoney+"</td>").appendTo(trObj);
                $("<td>"+createRemarks+"</td>").appendTo(trObj);
                $("<td>"+customStatusStr+"</td>").appendTo(trObj);
                $("<td>"+updateCustomUserName+"</td>").appendTo(trObj);

                $("<td>"+updateCustomTime+"</td>").appendTo(trObj);
                $("<td>"+updateRemarks+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+customPhotoAddress+"','custom');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#billRecCustomFileBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//查看预付款修改详情
function showAdvanceEdit(reconciliationId,sellerCompanyName) {
    showAdvanceEditInfo(reconciliationId,sellerCompanyName);
    layer.open({
        type: 1,
        title: '预付款抵扣详情',
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showAdvanceEditList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//预付款抵扣信息查询
function showAdvanceEditInfo(reconciliationId,sellerCompanyName) {
    $("#billAdvanceEditBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/buyer/billAdvance/queryAdvanceEditList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                //下单人名称
                var createBillUserName = o.createBillUserName;
                //创建人
                var createUserName = o.createUserName;
                //创建时间
                var createTimeStr = o.createTimeStr;
                //预付款抵扣金额
                var updateTotal = o.updateTotal;

                var trObj = $("<tr>");
                $("<td>"+sellerCompanyName+"</td>").appendTo(trObj);
                $("<td>"+createBillUserName+"</td>").appendTo(trObj);
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTimeStr+"</td>").appendTo(trObj);
                $("<td>"+updateTotal+"</td>").appendTo(trObj);

                $("#billAdvanceEditBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//自定义付款项
function customPayment(reconciliationId) {
    findBillCustomInfo();
     layer.open({
     type:1,
     title:"奖惩设置",
     area:['380px','auto'],
     skin:'',
     closeBtn:2,
     content:$('#customPaymentDiv'),
     btn:['确定','取消'],
     yes:function(index){
         var customType = $("input[name='customType']:checked").val();
         debugger
         if(!customType){
             layer.msg("请选择奖惩状态！",{icon:2});
             return;
         }
         var customMoney = $("#customMoney").val();
         if(!customMoney){
             layer.msg("奖惩金额不得为空！",{icon:2});
             return
         }else  if(customMoney < 0){
             layer.msg("金额不得小于0！",{icon:2});
             return
         }
         /*if(customType == "1"){
             if(customMoney < 0){
                 layer.msg("奖励金额不得小于0！",{icon:2});
                 return
             }
         }else if(customType == "2"){
             if(customMoney >= 0){
                 layer.msg("处罚金额不得大于0！",{icon:2});
                 return
             }
         }*/
         var billFileStr = ""
         var attachments = $("input[name='fileCustomUrl']");
         if(attachments!=undefined&&attachments.length>0){
             for(var i=0;i<attachments.length;i++){
                 var fileAddr = attachments[i].value;
                 billFileStr += fileAddr + ",";
             }
         }
         billFileStr = billFileStr.substring(0,billFileStr.length-1);

         var acceptUrl = "";
         acceptUrl = "platform/buyer/billReconciliation/customPayment";
         var customRemarks = $("#customRemarks").val();
         if(customRemarks.length >300){
         layer.msg("备注内容不超过300个字符！",{icon:2});
             return
         }else{
             $.ajax({
                 type : "POST",
                 url : acceptUrl,
                 data: {
                 "reconciliationId": reconciliationId,
                 "customType": customType,
                 "customMoney":customMoney,
                 "fileCustomUrl":billFileStr,
                 "customRemarks": customRemarks
                 },
                 async:false,
                 success:function(data){
                     /*layer.close(index);
                     layer.msg("提交成功！",{icon:1});
                     loadPlatformData();*/
                     var result = eval('(' + data + ')');
                     var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                     if(resultObj.success){
                         layer.close(index);
                         layer.msg(resultObj.msg,{icon:1});
                         loadPlatformData();
                     }else{
                         layer.msg(resultObj.msg,{icon:2});
                     }
                 },
                 error:function(){
                    layer.msg("提交失败，请稍后重试！",{icon:2});
                 }
             });
         }
     },no : function(index){
      layer.close(index);
      }
     });
 }

 function findBillCustomInfo() {
     $("#billCustomBody").empty();

     var liObj1 = $("<li>");
     $("<span>"+"奖惩类型："+"</span>").appendTo(liObj1);
     $("<label><input id='customAgree' type='radio' name='customType' value='1'>"+"奖励"+"</label>").appendTo(liObj1);
     $("<label><input id='customAgree' type='radio' name='customType' value='2'>"+"处罚"+"</label>").appendTo(liObj1);
     $("#billCustomBody").append(liObj1);

     var liObj2 = $("<li>");
     $("<span>"+"奖惩金额："+"</span>").appendTo(liObj2);
     $("<input type='number' id='customMoney' name='customMoney' min='0'>").appendTo(liObj2);
     $("#billCustomBody").append(liObj2);

     var liObj3 = $("<li>");
     $("<span>"+"奖惩票据："+"</span>").appendTo(liObj3);
     $("<div class='upload_license' style='width: 70px;height: 20px'>"+"<input type='file' multiple name='billCustomFileAddr' id='billCustomFileAddr'onchange='uploadPayment(\"custom\");' style='opacity: 1'>"+"</div>").appendTo(liObj3);
     $("<p></p>").appendTo(liObj3);
     $("<span style='white-space: nowrap'>"+"仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M"+"</span>").appendTo(liObj3);
     $("<div class='scanning_copy original' id='billCustomFileDiv'></div>").appendTo(liObj3);
     $("#billCustomBody").append(liObj3);

     var liObj4 = $("<li>");
     $("<span>"+"奖惩说明："+"</span>").appendTo(liObj4);
     $("<textarea type='text' name='customRemarks' id='customRemarks'>").appendTo(liObj4);
     $("#billCustomBody").append(liObj4);
 }

//加载审批流程
function loadVerify(){
    $('.opinion_view').each(function(){
        var opinionObj = $(this).find(".opinion");
        if(opinionObj.length == 0){
            var dataId = $(this).find(".orange").attr("data");
            pinJieVerifyDiv(dataId,$(this));
        }
    });
    loadTitle();
    getButtonList();
}
function loadTitle(){
    $("tbody tr").each(function(){
        $(this).find("td").each(function(){
            if($(this).children().length==0){
                $(this).attr("title",$(this).html());
            }
        });
    });
    $("table").colResizable();
}

//按钮权限修改为内部审批
function approvalInside(reconciliationId,billDealStatus,purchaseIdStr){
    layer.open({
        type:1,
        title:'是否发起内部审批？',
        area:['auto','auto'],
        skin:'',
        closeBtn:2,
        content:$('#insideBillRec'),
        btn:['确定','取消'],
        yes:function(index){
            debugger
            var purchaseId = "";
            if(null != purchaseId){
                purchaseId = purchaseIdStr;
            }
            var	agreeStatus = $("input[name='insideStatus']:checked").val();
            if(!agreeStatus){
                layer.msg("请选择审批状态！",{icon:2});
                return;
            }
            var	insideRemarks = $("#insideRemarks").val();
            if(insideRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }
            var url = ""
            debugger
            if(agreeStatus == '5'){
                url = "platform/buyer/billReconciliation/insideBillRec"
                $.ajax({
                    type : "POST",
                    url : url,
                    data: {
                        //"reconciliationId":reconciliationId,
                        //"purchaseId":purchaseId,
                        // "agreeStatus": agreeStatus,
                        //"insideRemarks":"insideRemarks",

                        "reconciliationId": reconciliationId,
                        "purchaseId":purchaseId,
                        "agreeStatus": agreeStatus,
                        "billDealStatus": billDealStatus,
                        "dealRemarks": insideRemarks,
                        //"approvalType":"",
                        "menuName":"17071814392298054492"
                    },
                    async:false,
                    success:function(data){
                        debugger
                        if(data.flag){
                            var res = data.res;
                            if(res.code==40000){
                                //调用成功
                                //saveSucess();
                                layer.msg("添加成功！",{icon:1});
                                layer.close(index);
                                //leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                                leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer')
                            }else if(res.code==40010){
                                //调用失败
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40011){
                                //需要设置审批流程
                                layer.msg(res.msg,{time:500,icon:2},function(){
                                    /*setApprovalUser(url,res.data,function(data){
                                     alert("成功了吗")
                                     saveSucess(data);
                                     });*/
                                    setApprovalUser(url,res.data,function(){
                                        //saveSucess();
                                        layer.msg("提交成功！",{icon:1});
                                        layer.close(index);
                                        leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer');
                                    });
                                });
                                return false;
                            }else if(res.code==40012){
                                //对应菜单必填
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40013){
                                //不需要审批
                                notNeedApproval(res.data,function(data){
                                    layer.msg("审批成功！",{icon:1});
                                    layer.close(index);
                                    leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer');
                                });
                            }
                        }else{
                            layer.msg("获取数据失败，请稍后重试！",{icon:2});
                            return false;
                        }
                    },
                    error:function(){
                        layer.msg("审批提交失败，请稍后重试！",{icon:2});
                    }
                });
            }
            if(agreeStatus == '4'){
                url = "platform/buyer/billReconciliation/updateReconciliation"
                $.ajax({
                    type : "POST",
                    url : url,
                    data: {
                        "reconciliationId": reconciliationId,
                        "purchaseId":purchaseId,
                        "agreeStatus": agreeStatus,
                        "billDealStatus": billDealStatus,
                        "dealRemarks": insideRemarks,
                       // "approvalType":"",
                    },
                    async:false,
                    success:function(data){
                        debugger
                        layer.close(index);
                        layer.msg("保存成功！",{icon:1});
                        leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer')
                        //loadPlatformData();
                    },
                    error:function(){
                        layer.msg("审批提交失败，请稍后重试！",{icon:2});
                    }
                });
            }


        },
        no : function(index){
            layer.close(index);
        }
    });
}

//按钮权限
/*function approvalMenuClick(obj,reconciliationId,acceptType,type,id){
    var li = $(obj).parent();
    if($(obj).parent().is("li")){
        li = $(obj).parent();
    }else{
        if(id != ''){
            li = $("#"+id);
        }
    }
    if($(li).closest(".lf_seller").length>0){
        var divObj = $(li).closest(".lf_seller");
        $(divObj).find("li").removeClass("menuLiSeller");
        $(li).addClass("menuLiSeller");
    }else if($(li).closest(".lf_buyer").length>0){
        var divObj = $(li).closest(".lf_buyer");
        $(divObj).find("li").removeClass("menuLiBuyer");
        $(li).addClass("menuLiBuyer");
    }else if($(li).closest(".lf_system").length>0){
        var divObj = $(li).closest(".lf_system");
        $(divObj).find("li").removeClass("menuLiBuyer");
        $(li).addClass("menuLiBuyer");
    }else if($(li).closest(".lf_basic").length>0){
        var divObj = $(li).closest(".lf_basic");
        $(divObj).find("li").removeClass("menuLiBuyer");
        $(li).addClass("menuLiBuyer");
    }
    if(type=='buyer'){
        //我是买家
        $(".menubuyer").removeClass("menubuyer");
        $(obj).addClass("menubuyer");
    }else if(type=='sellers'){
        //我是卖家
        $(".menusellers").removeClass("menusellers");
        $(obj).addClass("menusellers");
    }else if(type=='baseinfo'){
        //基础资料
        $(".lf_basic").removeClass("menubuyer");
        $(obj).addClass("menubuyer");
    }
    $.ajax({
        url : basePath+"platform/common/saveButtonSession",
        data:{
            "menuId":id
        },
        async:false,
        success:function(data){
            if(acceptType == 'accept'){//对接人审批
                layer.open({
                    type:1,
                    title:"账单审批",
                    area:['380px','auto'],
                    skin:'popBuyer',
                    closeBtn:2,
                    content:$('#acceptBillRec'),
                    btn:['确定','取消'],
                    yes:function(index){
                        var	agreeStatus = $("input[name='agreeStatus']:checked").val();
                        if(!agreeStatus){
                            layer.msg("请选择审批状态！",{icon:2});
                            return;
                        }
                        var acceptUrl = "";
                        if(agreeStatus == "4"){
                            acceptUrl = "platform/buyer/billReconciliation/updateReconciliation";
                        }else if (agreeStatus == "5"){
                            acceptUrl = "platform/buyer/billReconciliation/approvalBillReconciliation";
                        }
                        var	acceptRemarks = $("#acceptRemarks").val();
                        if(acceptRemarks.length >300){
                            layer.msg("备注内容不超过300个字符！",{icon:2});
                        }else{
                            $.ajax({
                                type : "POST",
                                url : acceptUrl,
                                data: {
                                    "reconciliationId": reconciliationId,
                                    "billDealStatus": agreeStatus,
                                    "dealRemarks": acceptRemarks,
                                    "approvalType":"purchase"
                                },
                                async:false,
                                success:function(data){
                                    layer.close(index);
                                    layer.msg("保存成功！",{icon:1});
                                    leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer','17071814392298054492')
                                    //loadPlatformData();
                                },
                                error:function(){
                                    layer.msg("保存失败，请稍后重试！",{icon:2});
                                }
                            });
                        }
                    },no : function(index){
                        layer.close(index);
                    }
                });
            }else {
                debugger
                var titleInfo = "";
                var passStatus = "";
                var returnStatus = "";
                var approvalType = "";
                if(acceptType == 'purchase'){//采购部
                    titleInfo = "采购部审批";
                    passStatus = "6";
                    returnStatus = "7";
                    approvalType = "purchase";
                }else if(acceptType == 'finance'){//财务部
                    titleInfo = "财务部审批";
                    passStatus = "3";
                    returnStatus = "8";
                    approvalType = "finance";
                }
                layer.open({
                    type:1,
                    title:titleInfo,
                    area:['380px','auto'],
                    skin:'popBuyer',
                    closeBtn:2,
                    content:$("#approvalBillReconciliation"),
                    btn:['确定','取消'],
                    yes:function(index){
                        var	approvalRemarks = $("#approvalRemarks").val();;
                        var	approvalStatus = $("input[name='approvalStatus']:checked").val();
                        if(!approvalStatus){
                            layer.msg("请选择审批状态！",{icon:2});
                            return;
                        }else if(approvalStatus == "pass"){//通过
                            approvalStatus = passStatus;
                        }else if(approvalStatus == "return"){
                            approvalStatus = returnStatus;
                        }
                        var approvalUrl = "";
                        if(approvalStatus == "3"){
                            approvalUrl = "platform/buyer/billReconciliation/updateReconciliation";
                        }else{
                            approvalUrl = "platform/buyer/billReconciliation/approvalBillReconciliation";
                        }

                        if(approvalRemarks.length >300){
                            layer.msg("备注内容不超过300个字符！",{icon:2});
                        }else{
                            $.ajax({
                                type : "POST",
                                url : approvalUrl,
                                data: {
                                    "reconciliationId": reconciliationId,
                                    "billDealStatus": approvalStatus,
                                    "dealRemarks": approvalRemarks,
                                    "approvalType":approvalType
                                },
                                async:false,
                                success:function(data2){
                                    layer.close(index);
                                    layer.msg("保存成功！",{icon:1});
                                    leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer','17071814392298054492')
                                    //loadPlatformData();
                                },
                                error:function(){
                                    layer.msg("保存失败，请稍后重试！",{icon:2});
                                }
                            });
                            layuiData();
                            //loadVerify();
                            scroll(0,0);// 显示页面顶部
                        }
                    },no : function(index){
                        layer.close(index);
                    }
                });
            }
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}*/

function layuiData(){
    layui.use(['form','laydate'], function(){
        var form = layui.form;
        var form = layui.form
            ,layer = layui.layer
            ,laydate = layui.laydate;
        $("input[lay-verify='date']").each(function(){
            laydate.render({
                elem: this
            });
        });
        form.render();
    });
    getButtonList();
}
function getButtonList(){
    var buttonList = {};
    $.ajax({
        url : basePath+"platform/common/getButtonList",
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            buttonList = isJSONObject(result)?result:eval('(' + result + ')');
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
    if(buttonList.length > 0){
        $("[button]").each(function(){
            var buttonName = $(this).attr("button");
            var isShow = false;
            $.each(buttonList,function(i,obj){
                var name = obj.name;
                if(buttonName==name){
                    isShow = true;
                    return false;
                }
            });
            if(!isShow){
                $(this).remove();
            }
        });
    }else{
        $("[button]").remove();
    }
}

//账单附件回显
function approvalShow(updateUserName,approvalTimeStr,approvalRemarks,acceptType) {
    $("#billRecApprovalShow").empty();
    var titleInfo = "";
    if(acceptType == 'purchase'){//采购部
        titleInfo = "采购部审批";

    }else if(acceptType == 'finance'){//财务部
        titleInfo = "财务部审批";
    }
    var trObj = $("<tr>");
    $("<td>"+updateUserName+"</td>").appendTo(trObj);
    $("<td>"+approvalTimeStr+"</td>").appendTo(trObj);
    $("<td>"+approvalRemarks+"</td>").appendTo(trObj);
    $("#billRecApprovalShow").append(trObj);

    layer.open({
        type: 1,
        title: titleInfo,
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillRecApproval'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//添加账单附件
function addBillRecFiles(reconciliationId,createBillUserName,startDateStr,endDateStr){
    findBillFileInfo();
    layer.open({
        type:1,
        title:'添加 '+createBillUserName+' '+startDateStr+'-'+endDateStr+' 周期账单附件',
        area:['auto','auto'],
        skin:'',
        closeBtn:2,
        content:$('#billFiles'),
        btn:['确定','取消'],
        yes:function(index){

            var billFileStr = ""
            var attachments = $("input[name='fileUrl']");
            /* if(attachments==undefined||attachments.length<=0){
             layer.msg("请上传附件！",{icon:2});
             return;
             }*/
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    //attachmentsArr.push(attachments[i].value);
                    var fileAddr = attachments[i].value;
                    billFileStr += fileAddr + ",";
                }
            }
            billFileStr = billFileStr.substring(0,billFileStr.length-1);

            var	billFileRemarks = $("#billFileRemarks").val();
            if(billFileRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else{
                var url = "platform/buyer/billReconciliation/addBillFileInfo"
                $.ajax({
                    type : "POST",
                    url : url,
                    data: {
                        "reconciliationId":reconciliationId,
                        "billFileStr":billFileStr,
                        "billFileRemarks": billFileRemarks
                    },
                    async:false,
                    success:function(data){
                        var result = eval('(' + data + ')');
                        var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                        if(resultObj.success){
                            layer.close(index);
                            layer.msg(resultObj.msg,{icon:1});
                            loadPlatformData();
                        }else{
                            layer.msg(resultObj.msg,{icon:2});
                        }
                    },
                    error:function(){
                        layer.msg("付款失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}

//付款明细
function findBillFileInfo() {
    $("#billFilesBody").empty();

    //var recFile = "recFile";
    var liObj1 = $("<li>");
    $("<span>"+"账单票据："+"</span>").appendTo(liObj1);
    $("<div class='upload_license' style='width: 70px;height: 20px'>"+"<input type='file' multiple name='billFileAddr' id='billFileAddr'onchange='uploadPayment(\"recFile\");' style='opacity: 1'>"+"</div>").appendTo(liObj1);
    $("<p></p>").appendTo(liObj1);
    $("<span style='white-space: nowrap'>"+"仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M"+"</span>").appendTo(liObj1);
    $("<div class='scanning_copy original' id='billFileDiv'></div>").appendTo(liObj1);
    $("#billFilesBody").append(liObj1);

    var liObj2 = $("<li>");
    $("<span>"+"备注："+"</span>").appendTo(liObj2);
    $("<textarea type='text' name='billFileRemarks' id='billFileRemarks'>").appendTo(liObj2);
    $("#billFilesBody").append(liObj2);
}
//上传操作
function uploadPayment(fileType) {
    var formData = new FormData();
    var files = "";
    var idOrName = "";
    var divId = "";
    var fileUrl = "";
    if(fileType == "custom"){
        files = document.getElementById("billCustomFileAddr").files;
        idOrName = "billCustomFileAddr";
        divId = "#billCustomFileDiv";
        fileUrl = "fileCustomUrl";
    }else if(fileType == "recFile"){
        files = document.getElementById("billFileAddr").files;
        idOrName = "billFileAddr";
        divId = "#billFileDiv";
        fileUrl = "fileUrl";
    }
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billReconciliation/uploadBillFile";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\""+divId+"\");'>"+
                        "<input type='hidden' name='"+fileUrl+"' value='"+url+"'>"+
                        "</span>";
                    $(divId).append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#"+idOrName).replaceWith('<input type="file" style="opacity:1" multiple name="'+idOrName+'" id="'+idOrName+'" onchange="uploadPayment(\''+fileType+'\');" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}
//账单附件回显
function openBillFile(reconciliationId,createBillUserName,startDateStr,endDateStr) {
    showBillFiles(reconciliationId);
    layer.open({
        type: 1,
        title: createBillUserName+''+startDateStr+'-'+endDateStr+'周期账单附件',
        area: ['900px', 'auto'],
        skin:'',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillFileList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//账单附件信息查询
function showBillFiles(reconciliationId) {
    $("#billRecFileBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/buyer/billReconciliation/queryBillFileList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            /*var createUserId = resultObj.createUserId;
             var createUserName = resultObj.createUserName;
             var deliveryItemList = resultObj.deliveryItemList;*/
            $.each(resultObj,function(i,o){
                //创建人编号
                var createUserId = o.createUserId;
                //上传人名称
                var createUserName = o.createUserName;
                //创建时间
                var createTime = o.createTime;
                //附件路径
                var filesAddress = o.filesAddress;
                //备注
                var remarks = o.remarks;

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTime+"</td>").appendTo(trObj);
                $("<td>"+remarks+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+filesAddress+"','recFile');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#billRecFileBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//附件显示隐藏
var hideOrShow = true;
function showBillRecFileList(receiptAddr,fileType) {
    if(receiptAddr != null || receiptAddr != ''){
        var bigImgShow = "";
        var smallImg = "";
        var showOrHidden = "";
        if(fileType == "custom"){
            $("#showCustomBigImg").empty();
            $("#fileCustomList").empty();
            bigImgShow = "#showCustomBigImg";
            smallImg = "#fileCustomList";
            showOrHidden = "#showCustomFiles";
        }else if(fileType == "recFile"){
            $("#showBigImg").empty();
            $("#fileList").empty();
            bigImgShow = "#showBigImg";
            smallImg = "#fileList";
            showOrHidden = "#showFiles";
        }
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $(smallImg).append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(bigImgShow).append($("<img id='bigImg' src='"+bigImg+"'></img>"));

        if(hideOrShow){
            $(showOrHidden).show();
            hideOrShow = false;
        }else {
            $(showOrHidden).hide();
            hideOrShow = true
        }
    }
}

//查看收货回单
$('.check_receipt').click(function(){
    layer.open({
        type: 1,
        title: '收货回单',
        area: ['680px', 'auto'],
        skin: ' ',
        closeBtn:2,
        content: $('.receipt_content')
    });
});
//删除收货回单
$('.del_receipt>button').click(function(){
    layer.confirm('您确定要将所选中的回单图片删除吗？', {
        icon:3,
        skin:'pop',
        title:'提示',
        closeBtn:2,
        btn: ['确定','取消'] //按钮
    }, function(){
        var str=$('.big_img>img').attr('src');
        var li=$('#icon_list li img[src="'+str+'"]').parent();
        li.remove();
        layer.msg('删除成功',{time:800});
    });
});
//功能点8:回单证明大图
var preview={
    LIWIDTH:108,//保存每个li的宽
    $ul:null,//保存小图片列表的ul
    moved:0,//保存左移过的li
    init:function(){//初始化功能
        this.$ul=$(".view>ul");//查找ul
        $(".view>a").click(function(e){//为两个按钮绑定单击事件

            e.preventDefault();
            if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
                if($(e.target).is(".forward")){//如果是向前按钮
                    this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
                    this.moved++;//移动个数加1
                }
                else{//如果是向后按钮
                    this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
                    this.moved--;//移动个数减1
                }
                this.checkA();//每次移动完后，调用该方法
            }
        }.bind(this));
        //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
        this.$ul.on("mouseover","li>img",function(){
            var src=$(this).attr("src");//获得当前img的src
            //var i=src.lastIndexOf(".");//找到.的位置
            //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
            $(".big_img>img").attr("src",src);//设置中图片的src
        });
    },
    checkA:function(){//检查a的状态
        if(this.moved==0){//如果没有移动
            $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
        }
       /* else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
            $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
        }*/
        else{//否则，都启用
            $("[class^=backward]").attr("class","backward");
            $("[class^=forward]").attr("class","forward");
        }
    }
};
preview.init();