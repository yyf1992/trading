/*$(function(){
    //供应商下拉
    loadSellerCompany();
    //下单人下拉
    loadBuyerUser();


});*/

var form;
layui.use('form', function() {
    form = layui.form;
    form.render("select");
    //供应商下拉
    loadSellerCompany();
    //下单人下拉
    loadBuyerUser();
});
//提交账单周期信息
$('#billCycleInfo_add').click(function(){
    debugger
    //供应商
    var companyIdArr=$("#sellerCompanyDiv #companyId").val();
    if(!companyIdArr || companyIdArr == ''){
        layer.msg("请选择供应商！",{icon:2});
        return;
    }
    var companyId=companyIdArr.split(",")[0];
    //下单人
    var userIdArr=$("#buyUserDiv #userId").val();
    if(!userIdArr || userIdArr == ''){
        layer.msg("请选择采购员！",{icon:2});
        return;
    }
    var userId=userIdArr.split(",")[0];

    //根据供应商和下单人查询是否存在
    var isOrNoBillUserUrl = basePath+"platform/buyer/billAdvance/isOrNoBillUser";
    $.ajax({
        type : "POST",
        url : isOrNoBillUserUrl,
        async:false,
        data: {
            "sellerCompanyId":companyId,
            "createBillUserId":userId,
        },
        success:function(data){
            var result = eval("(" + data + ")");
            var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
            if(resultObj.success){
                var	taskRemarks = $("#taskRemarks").val();
                if(taskRemarks.length >300){
                    layer.msg("备注内容不超过300个字符！",{icon:2});
                    return;
                }
                //保存数据
                var url = basePath+"platform/buyer/billAdvance/saveAdvanceInfo";
                $.ajax({
                    type : "POST",
                    url : url,
                    async:false,
                    data: {
                        "sellerCompanyId":companyId,
                        "userId":userId,
                        //"usableTotal":usableTotal,
                       // "taskFileStr":taskFileStr,
                        "taskRemarks":taskRemarks,
                    },
                    success:function(data){
                        var result = eval("(" + data + ")");
                        var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
                        if(resultObj.success){
                            layer.msg(resultObj.msg,{icon:1});
                            leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer')
                        }else{
                            layer.msg(resultObj.msg,{icon:2});
                        }
                    },
                    error:function(){
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                });
            }else{
                layer.msg(resultObj.msg,{icon:2});
            }
        },
        error:function(){
            layer.msg("此采购员数据获取失败，请稍后重试！",{icon:2});
        }
    });





});


//加载互通卖家好友下拉
function loadSellerCompany(){
    var obj={};
    obj.divId="sellerCompanyDiv";
    obj.companyName="companyName";
    obj.companyId="companyId";
    obj.filter="changeSupplier";
    recSellerCompany(obj);
}

// 采购商下拉共通
function recSellerCompany(obj){
    var url = basePath+"platform/common/getSupplierSelect"
    $.ajax({
        url : url,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var selectObj = $("<select lay-filter='"+obj.filter+"' lay-search=''></select>");
            if(obj.companyName != null && obj.companyName != ''){
                selectObj.attr("name",obj.companyName);
            }
            if(obj.companyId != null && obj.companyId != ''){
                selectObj.attr("id",obj.companyId);
            }
            $('<option>',{val:"",text:"请选择或输入关键字搜索"}).appendTo(selectObj);
            $.each(resultObj,function(i){
                var companyId = resultObj[i].sellerId;
                var companyName = resultObj[i].sellerName;
                $('<option>',{val:companyId,text:companyName}).appendTo(selectObj);
            });
            $("#"+obj.divId).append(selectObj);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//加载互通卖家好友下拉
function loadBuyerUser(){
    var obj={};
    obj.divId="buyUserDiv";
    obj.userName="userName";
    obj.userId="userId";
    obj.filter="changeSupplier";
    recBuyerSelect(obj);
}

// 采购商下拉共通
function recBuyerSelect(obj){
    var url = basePath+"platform/buyer/billAdvance/queryBillUser"
    $.ajax({
        url : url,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var selectObj = $("<select lay-filter='"+obj.filter+"' lay-search=''></select>");
            if(obj.userName != null && obj.userName != ''){
                selectObj.attr("name",obj.userName);
            }
            if(obj.userId != null && obj.userId != ''){
                selectObj.attr("id",obj.userId);
            }
            $('<option>',{val:"",text:"请选择或输入关键字搜索"}).appendTo(selectObj);
            $.each(resultObj,function(i){
                var userId = resultObj[i].id;
                var userName = resultObj[i].userName;
                $('<option>',{val:userId,text:userName}).appendTo(selectObj);
            });
            $("#"+obj.divId).append(selectObj);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("paymentFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billPaymentDetail/uploadPaymentBill";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#paymentFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#paymentFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#paymentFileAddr").replaceWith('<input type="file" style="opacity:1" multiple name="paymentFileAddr" id="paymentFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}