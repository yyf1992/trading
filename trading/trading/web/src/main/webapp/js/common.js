$(document).ready(function(){
	$("body").bind("click", function(evt) {
		if ($(evt.target).parents("ul.menu").length == 0) {
			$("ul.menu").remove();
		}
	});
	$(".contract_show.mp").each(function(){
		var height = $(this).height();
		if(height > 500){
			$(this).css({"height":"500px","overflow":"auto"});
		}
	});
});
//判断是否是json对象
function isJSONObject(obj){
	var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
	return isjson;
}

function loadDate(startId,endId){
	layui.use('laydate', function() {
		var laydate = layui.laydate;
		var start = {
			format: 'YYYY-MM-DD',
			max : '2099-06-16 23:59:59'	,
			istoday : false,
			choose : function(datas) {
				end.min = datas; // 开始日选好后，重置结束日的最小日期
				end.start = datas // 将结束日的初始值设定为开始日
			}
		};
		var end = {
			format: 'YYYY-MM-DD',
			max : '2099-06-16 23:59:59',
			istoday : false,
			choose : function(datas) {
				start.max = datas; // 结束日选好后，重置开始日的最大日期
			}
		};
		if ($("#"+startId).length > 0) {
			$("#"+startId).click(function() {
				start.elem = this;
				laydate(start);
			});
		}
		if ($("#"+endId).length > 0) {
			$("#"+endId).click(function() {
				end.elem = this
				laydate(end);
			});
		}
	});
}
// 后台管理分页查询方法
function loadAdminData(divId){
	var e = window.event || arguments[0];
	if(typeof(e)=='object'){
		e.preventDefault();
	}
	if(divId==null||divId==''||divId=='content'){
		var formObj = $("#content").find("form");
		var formData = formObj.serialize();
		var url = formObj.attr("action");
		if(url.indexOf("?")>0){
			url += formData;
		}else{
			url += "?"+formData;
		}
		var aFrame = parent.frames;
		aFrame[0].location.href=url;
	}else{
		var formObj = $("#"+divId).find("form");
		$.ajax({
			url : basePath+formObj.attr("action"),
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#"+divId).html(str);
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//买卖系统平台分页查询方法
function loadPlatformData(divId){
	layer.load();
	var e = window.event || arguments[0];
	if(typeof(e)=='object'){
		e.preventDefault();
	}
	if(divId==null||divId=='')divId='content';
	var contentObj;
	if(divId=='layOpen'){
		contentObj = $(".layui-layer-content");
	}else if(divId=='content'){
		if($(".content").length>0) contentObj=$(".content");
		if($(".content_modify").length>0) contentObj=$(".content_modify");
		if($(".content_system").length>0) contentObj=$(".content_system");
	}else{
		contentObj = $("#"+divId);
	}
	var formObj = contentObj.find("form");
	if(formObj.length > 0 && formObj.attr("action") != ''){
		$.ajax({
			url : basePath+formObj.attr("action"),
			data:formObj.serialize(),
			async:false,
			success:function(data){
				layer.closeAll('loading');
				var str = data.toString();
				contentObj.html(str);
				layuiData();
				loadVerify();
			},
			error:function(){
				layer.closeAll('loading');
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}else{
		window.location.reload();
	}
}
// 供应商下拉共通
function supplierSelect(obj){
	var url = basePath+"platform/common/getSupplierSelect"
	$.ajax({
		url : url,
		async:false,
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var selectObj = $("<select lay-filter='"+obj.filter+"' lay-search=''></select>");
			if(obj.selectName != null && obj.selectName != ''){
				selectObj.attr("name",obj.selectName);
			}
			if(obj.selectId != null && obj.selectId != ''){
				selectObj.attr("id",obj.selectId);
			}
			$('<option>',{val:"",text:"请选择"}).appendTo(selectObj);
			var selectValue = obj.selectValue;
			$.each(resultObj,function(i){
				var sellerId = resultObj[i].sellerId;
				var sellerName = resultObj[i].sellerName;
				var sellerPerson = resultObj[i].sellerPerson;
				var sellerPhone = resultObj[i].sellerPhone;
				if(selectValue != null && selectValue != ''){
					if(selectValue==sellerId){
						$('<option>',{val:sellerId+","+sellerPerson+","+sellerPhone,text:sellerName,selected:"selected"}).appendTo(selectObj);
						return;
					}
				}
				$('<option>',{val:sellerId+","+sellerPerson+","+sellerPhone,text:sellerName}).appendTo(selectObj);
			});
			$("#"+obj.divId).append(selectObj);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//单位下拉列表共通
function unitSelect(obj){
	var url = basePath+"platform/common/getUnitSelect"
	$.ajax({
		url : url,
		async:false,
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var selectObj = $("<select></select>");
			if(obj.selectName != null && obj.selectName != ''){
				selectObj.attr("name",obj.selectName);
			}
			if(obj.selectId != null && obj.selectId != ''){
				selectObj.attr("id",obj.selectId);
			}
			$('<option>',{val:"",text:"请选择"}).appendTo(selectObj);
			var selectValue = obj.selectValue;
			$.each(resultObj,function(i){
				var id = resultObj[i].id;
				var unitName = resultObj[i].unitName;
				if(selectValue != null && selectValue != ''){
					if(selectValue==id){
						$('<option>',{val:id,text:unitName,selected:"selected"}).appendTo(selectObj);
						return;
					}
				}
				$('<option>',{val:id,text:unitName}).appendTo(selectObj);
			});
			$("#"+obj.divId).append(selectObj);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//仓库下拉
function warehouseSelect(obj){
	var url = basePath+"platform/common/getWarehouseSelect"
	$.ajax({
		url : url,
		async:false,
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var selectObj = $("<select></select>");
			if(obj.selectName != null && obj.selectName != ''){
				selectObj.attr("name",obj.selectName);
			}
			if(obj.selectId != null && obj.selectId != ''){
				selectObj.attr("id",obj.selectId);
			}
			$('<option>',{val:"",text:"请选择"}).appendTo(selectObj);
			var selectValue = obj.selectValue;
			$.each(resultObj,function(i){
				var wareId = resultObj[i].id;
				var wareHouseName = resultObj[i].whareaName;
				if(selectValue != null && selectValue != ''){
					if(selectValue==wareId){
						$('<option>',{val:wareId,text:wareHouseName,selected:"selected"}).appendTo(selectObj);
						return;
					}
				}
				$('<option>',{val:wareId,text:wareHouseName}).appendTo(selectObj);
			});
			$("#"+obj.divId).append(selectObj);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//商品查询（供应商互通）
function selectGoods(obj){
	//供应商判空
	var supplierIdArr=$("#addOrderDiv #supplierId").val();
	if(supplierIdArr == ''){
		layer.msg("请选择供应商！",{icon:2});
		return;
	}
	var supplierId=supplierIdArr.split(",")[0];
	var goodsUlObj = $(obj).parent().find("ul");
	if(goodsUlObj.length == 0){
		goodsUlObj = $("<ul></ul>");
		goodsUlObj.attr("class","menu ui-menu ui-widget ui-widget-content");
		$(obj).parent().append(goodsUlObj);
	}
	goodsUlObj.empty();
	goodsUlObj.css("display","block");
	
	var selectValue = $(obj).val();
	if(selectValue != ''){
		var url = basePath+"platform/common/selectGoods"
		$.ajax({
			url : url,
			data:{
				"selectValue":selectValue,
				"supplierId":supplierId
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				$.each(resultObj,function(i){
					var liObj = $('<li>');
					$("<input>",{name:"proCode",value:resultObj[i].product_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"proName",value:resultObj[i].product_name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuCode",value:resultObj[i].sku_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuName",value:resultObj[i].sku_name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuOid",value:resultObj[i].barcode,hidden:true}).appendTo(liObj);
					$("<input>",{name:"unitId",value:resultObj[i].unit_id,hidden:true}).appendTo(liObj);
					$("<input>",{name:"price",value:resultObj[i].price,hidden:true}).appendTo(liObj);
					var showText = resultObj[i].product_code+"|"
						+resultObj[i].product_name+"|"
						+resultObj[i].sku_code+"|"
						+resultObj[i].sku_name+"|"
						+resultObj[i].barcode;
					var abridgeText = (showText.length)>25?showText.substring(0,25)+"...":showText;
					$("<a href='javascript:void(0)' onclick='chooseGoods(this)' title='"+showText+"'>"+abridgeText+"</a>").appendTo(liObj);
					liObj.appendTo(goodsUlObj);
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//选择商品
function chooseGoods(obj){
	var ul = $(obj).parents("ul");
	var tr = ul.parents("tr");
	tr.find("input[name='proCode']").val($(obj).parent().find("input[name='proCode']").val());
	tr.find("input[name='proName']").val($(obj).parent().find("input[name='proName']").val());
	tr.find("input[name='skuCode']").val($(obj).parent().find("input[name='skuCode']").val());
	tr.find("input[name='skuName']").val($(obj).parent().find("input[name='skuName']").val());
	tr.find("input[name='skuOid']").val($(obj).parent().find("input[name='skuOid']").val());
	tr.find("input[name='price']").val($(obj).parent().find("input[name='price']").val());
	tr.find("td:eq(2) select").val($(obj).parent().find("input[name='unitId']").val());
	tr.find("td:eq(4)").html($(obj).parent().find("input[name='price']").val());
	tr.find("input[name='proNameText']").val($(obj).attr("title"));
	ul.remove();
}
function deleteTr(obj){
	$(obj).parents("tr").remove();
	summaryNum();
}
//向供应商下单，新增一行
function addOrderTr(obj){
	var trObj = $("<tr>");
	var tdObj1 = $("<td>");
	$("<b onClick='deleteTr(this)'>").appendTo(tdObj1);
	$("<input type='hidden' name='proName'>").appendTo(tdObj1);
	$("<input type='hidden' name='proCode'>").appendTo(tdObj1);
	$("<input type='hidden' name='skuCode'>").appendTo(tdObj1);
	$("<input type='hidden' name='skuName'>").appendTo(tdObj1);
	$("<input type='hidden' name='skuOid'>").appendTo(tdObj1);
	$("<input type='hidden' name='price'>").appendTo(tdObj1);
	var tdObj2 = $("<td>");
	tdObj2.css("text-align","left");
	$("<input type='text' name='proNameText' placeholder='输入商品名称/货号/条形码等关键字' onkeyup='selectGoods(this);'>").appendTo(tdObj2);
	var id = new Date().getTime();
	var tdObj3 = $("<td>");
	$("<div>",{id:"unitDiv_"+id}).appendTo(tdObj3);
	var tdObj4 = $("<td>");
	$("<input type='number' value='0' min='0'>").appendTo(tdObj4);
	var tdObj5 = $("<td>");
	$("<div>",{id:"warehouseDiv_"+id}).appendTo(tdObj5);
	var tdObj6 = $("<td>");
	var selectObj = $("<select class='isInvoice'>",{name:"isNeedInvoice"});
	$('<option>',{val:"N",text:"否"}).appendTo(selectObj);
	$('<option>',{val:"Y",text:"是"}).appendTo(selectObj);
	selectObj.appendTo(tdObj6);
	var tdObj7 = $("<td>");
	$("<textarea placeholder='备注内容'>",{name:"remarks"}).appendTo(tdObj7);
	
	tdObj1.appendTo(trObj);
	tdObj2.appendTo(trObj);
	tdObj3.appendTo(trObj);
	tdObj4.appendTo(trObj);
	$("<td>0</td>").appendTo(trObj);
	$("<td>0</td>").appendTo(trObj);
	tdObj5.appendTo(trObj);
	tdObj6.appendTo(trObj);
	tdObj7.appendTo(trObj);
	$(obj).parent().find("table tbody").append(trObj);
	
	//单位下拉
	var objUnit={};
	objUnit.divId="unitDiv_"+id;
	objUnit.selectName="unitName";
	objUnit.selectValue="17070710044995394265";
	unitSelect(objUnit);
	//仓库下拉
	var objWare={};
	objWare.divId="warehouseDiv_"+id;
	objWare.selectName="warehouseName";
	objWare.selectValue="0005";
	warehouseSelect(objWare);
	$("input[type='number']").bind("change",function(){
		validateInput($(this));
	}).bind("keyup",function(){
		validateInput($(this));
	});
}
function validateInput(obj){
	if(!objValidate(obj,2)){
		layer.msg("必须为整数！",{icon:2});
		$(obj).val(0);
		return;
	}else{
		summaryNum();
	}
}
function summaryNum(){
	var numSum=0;
	var totalPrice=0;
	$("#addOrderDiv table>tbody>tr").each(function(i){
		var skuOid = $(this).find("input[name='skuOid']").val();
		if(skuOid != ''){
			//数量
			var num = parseInt($(this).find("td:eq(3) input").val());
			if(objValidate($(this).find("td:eq(3) input"),2)){
				numSum += num;
				//价格
				var price = $(this).find("input[name='price']").val();
				if(price!='' && !isNaN(price)){
					var priceNew = parseFloat(price) * 100;
					priceNew = new Number(priceNew+1).toFixed(2);
					priceNew = new Number(priceNew - 1).toFixed(2);
					//为了消除多余小数
					var priceSum = (num * priceNew)/100;
					priceSum = new Number(priceSum+1).toFixed(2);
					priceSum = new Number(priceSum - 1).toFixed(2);
					totalPrice = new Number(totalPrice) + new Number(priceSum);
					$(this).find("td:eq(5)").html(priceSum);
				}
			}
		}
	});
	$("#addOrderDiv table>tfoot .total:first").html(numSum);
	$("#addOrderDiv table>tfoot .total:eq(1)").html(totalPrice);
}
//验证合法性
function objValidate(obj,type){
	var result = false;
	if(type==1){
		//金额
		var re = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d{1,4})?$/;
		return re.test($(obj).val());
	}else if(type==2){
		//数量
		var re = /^\d+$/;
		return re.test($(obj).val());
	}
}
//加载省
function loadProvince(selectId,selectValue){
	$("#"+selectId).empty();
	$.ajax({
		type : "post",
		url : basePath+"platform/reg/loadProvince",
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			var select = "";
			$.each(result, function(index, element) {
				var id = element.provinceId;
                var name = element.province;
                if(selectValue == id){
                	select = "selected='selected'";
                }else{
                	select = ""
                }
                var opt = "<option value='" + id + "' " + select + ">" + name + "</option>";
                $("#"+selectId).append(opt);
		    });
		},
		error : function() {
			swal({title:"操作提示",text: "加载省出现错误！", type:"error" });
		}
	});
}
//加载市
function loadCity(provinceId,selectId,selectValue){
	$("#"+selectId).empty();
	$.ajax({
		type : "post",
		url : basePath+"platform/reg/loadCityByProvinceId",
		data : {
			"provinceId" : provinceId
		},
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			var select = "";
			$.each(result.citiesList, function(index, element) {
				var id = element.cityId;
                var name = element.city;
                if(selectValue == id){
                	select = "selected='selected'";
                }else{
                	select = ""
                }
                var opt = "<option value='" + id + "' " + select + ">" + name + "</option>";  
                $("#"+selectId).append(opt);
		    });
		},
		error : function() {
			swal({title:"操作提示",text: "加载市出现错误！", type:"error" });
		}
	});
}
//加载地区
function loadArea(cityId,selectId,selectValue){
	$("#"+selectId).empty();
	$.ajax({
		type : "post",
		url : basePath+"platform/reg/loadAreaByCityId",
		data : {
			"cityId" : cityId
		},
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			var select = "";
			$.each(result.areasList, function(index, element) {
				var id = element.areaId;
                var name = element.area;
                if(selectValue == id){
                	select = "selected='selected'";
                }else{
                	select = ""
                }
                var opt = "<option value='" + id + "' " + select + ">" + name + "</option>";  
                $("#"+selectId).append(opt);
		    });
		},
		error : function() {
			swal({title:"操作提示",text: "加载地区出现错误！", type:"error" });
		}
	});
}
function setApprovalUser(url,oldData,successFun){
	$.ajax({
  		url: basePath+"platform/sysVerify/setApprovalUser",
  		success: function(data){
  			layer.open({
				type : 1,
				title : '设置审批流程',
				skin : 'pop',
				area : [ '950px', '550px' ],
				btn:['确认'],
				//zIndex: layer.zIndex,
				content : data,
				yes : function(index, layero) {
					var processUsers = "";
					//流程选择人员
					$("#rightUser #add li.person").each(function(){
						if($(this).find("input[name='userId']").length>0){
							var userId = $(this).find("input[name='userId']").val();
							var userName = $(this).find("input[name='userName']").val();
							processUsers += userId+","+userName+"@";
						}
					});
					if(processUsers==''){
						layer.msg("请选择审批人员！", {icon : 2});
						return false;
					}
					oldData.approval_staff_automatic=processUsers;
					var formDate = new FormData();
					$.each(oldData,function(i,d){
						if(i.lastIndexOf('importExcelTrade_')>-1){
							var objName = i.split("importExcelTrade_")[1];
							var fileObj = document.getElementById(d).files[0];
							formDate.append(objName, fileObj);
						}else{
							formDate.append(i, d);
						}
					});
					layer.load();
					var xhr = new XMLHttpRequest();
					xhr.open("post", basePath+url, true);
					xhr.send(formDate);
					xhr.onreadystatechange = function() {
						if (xhr.readyState == 4 && xhr.status == 200) {
							if(b!=''){
								var b = xhr.responseText;
								var result = eval('(' + b + ')');
								var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
								var res = resultObj.res;
								if(res.code==40000){
									layer.closeAll();
									if(typeof(successFun) != 'undefind'){
										successFun(res.data);
									}
								}else if(res.code==40010){
									//调用失败
									layer.closeAll('loading');
									layer.close(index);
									layer.msg(res.msg,{icon:2});
									return false;
								}
							}
						}
					}
					/*$.ajax({
						type:"POST",
				  		url: basePath+url,
				  		data:oldData,
				  		success: function(data1){
				  			if(data1.flag){
								var res = data1.res;
								if(res.code==40000){
									layer.closeAll();
									if(typeof(successFun) != 'undefind'){
										successFun(res.data);
									}
								}else if(res.code==40010){
									//调用失败
									layer.closeAll();
									layer.msg(res.msg,{icon:2});
									return false;
								}
				  			}
				  		}
					});*/
				},
				cancel : function() {
					//layer.msg("请选择完审批人员，点击【确认】按钮！", {icon : 2});
					//return false;
				}
			});
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
 	});
}

//不需要审批
function notNeedApproval(oldData,successFun){
	var successUrl = oldData.verifySuccess;
	var dataIdArray = oldData.idList;
	if(dataIdArray != null && dataIdArray.length>0){
		$.each(dataIdArray,function(i,data){
			$.ajax({
				url:basePath+successUrl,
				async:false,
				data:{"id":data},
				success:function(data){
				},
				error:function(){
				}
			});
		});
	}
	if(typeof(successFun) != 'undefind'){
		successFun(dataIdArray);
	}
}
function downLoad(url){
	window.open(url);
}