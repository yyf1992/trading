/**
 * 新增商品关联JS
 * liuhui
 * 2017-7-24 13:10:45
 */
$(function() {
    //新增行
    $('.btn_new').click(function(e){
        showProduct();
        // e.preventDefault();
        // var str='<td>' +
        //     '<input type="hidden" name="productId" value=""> ' +
        //     '<input type="hidden" name="productCode" value=""> ' +
        //     '<input type="hidden" name="productName" value=""> '+
        //     '<input type="hidden" name="barcode" value=""> '+
        //     '<input type="hidden" name="skuCode" value="">'+
        //     '<input type="hidden" name="skuName" value="">'+
        //     '<input type="hidden" name="unitId" value="">'+
        //     '<a href="javascript:void(0);" onclick="showProduct(this);">选择商品</a>' +
        //     '</td>' +
        //     '<td><input type="number" name="price" placeholder="点击输入" min="0"></td>'+
        //     '<td><input type="hidden" name="linkCompanyID" value=""><a href="javascript:void(0);" onclick="showCompany(this);">请选择</a></td>'+
        //     // '<td><input type="text" name="linkCompanyBarcode"  placeholder="点击输入"></td>'+
        //     '<td><input type="text" name="note" placeholder="点击输入"></td>'+
        //     '<td> ' +
        //     // '<span class="table_edit"><b></b>修改</span>&nbsp;'+
        //     '<span class="table_del"><b></b>删除</span>'+
        //     '</td>';
        // var tbl=$('#linkBody');
        // addOneRow(str,tbl);
    });
    //删除行
    $('#linkBody').on('click','.table_del',function(){
        layer.confirm('确定删除该行？', {
            icon:3,
            title:'提醒',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(){
            delRow(this);
            layer.msg('删除成功',{time:200});
        }.bind(this));
    });
});


//表格增加和删除一行函数
function addOneRow(str,tbl){
    var addTr=document.createElement('tr');
    addTr.innerHTML=str;
    tbl.append(addTr);
}
function delRow(tr){
    $(tr).parent().parent().remove();
}



//商品弹出框显示
function showProduct(obj) {
    var checkObj;
    findProduct();
    layer.open({
        type: 1,
        title: '选择商品',
        area: ['750px', 'auto'],
        skin:'pop',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#addProductLinkSkuPop'),
        yes : function(index){
            var inputs=$('#productSkuPopTab input[type="checkbox"]');
            var count = 0;
            inputs.each(function(){
                if($(this).prop('checked')==true){
                    count++;
                    checkObj = $(this);
                    var str='<td><input type="hidden" name="productId" value="'+$(checkObj).val()+'"> ' +
                                '<input type="text" name="productName" value="'+$(checkObj).parent().parent().parent().find("td")[1].innerText+'" readonly>'+
                            '</td>'+
                            '<td><input type="text" name="productCode" value="'+$(checkObj).parent().parent().parent().find("td")[2].innerText+'" readonly> </td>' +
                            '<td><input type="text" name="barcode" value="'+$(checkObj).parent().parent().parent().find("td")[3].innerText+'" readonly></td> '+
                            '<td><input type="text" name="skuCode" value="'+$(checkObj).parent().parent().parent().find("td")[4].innerText+'" readonly></td>'+
                            '<td><input type="text" name="skuName" value="'+$(checkObj).parent().parent().parent().find("td")[5].innerText+'" readonly></td>'+
                            '<td><input type="text" name="unitName" value="'+$(checkObj).parent().parent().parent().find("td")[6].innerText+'" readonly>'+
                            '<td><input type="number" name="price" placeholder="点击输入" min="0"></td>'+
                            '<td><input type="number" name="dailyOutput" placeholder="点击输入" min="0"></td>'+
                            '<td><input type="number" name="storageDay" placeholder="点击输入" min="0"></td>'+
                            '<td><input type="hidden" name="linkCompanyID" value=""><a href="javascript:void(0);" onclick="showCompany(this);">请选择</a></td>'+
                            '<td><input type="text" name="note" placeholder="点击输入"></td>'+
                            '<td> <span class="table_del"><b></b>删除</span></td>';
                    var tbl=$('#linkBody');
                    addOneRow(str,tbl);
                }
            });
            layer.close(index);
        },
        no : function(index){
            layer.close(index);
        }
    });
}
//查找商品
function findProduct() {
    $.ajax({
        url:"platform/product/sku/showProductSkuPop",
        data:{
            "searchProductType":'',
            "page.divId":'addProductLinkSkuPop'

        },
        async:false,
        success:function(data){
            $("#addProductLinkSkuPop").empty();
            var str = data.toString();
            $("#addProductLinkSkuPop").html(str);
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//关联客户弹出框
function showCompany(obj) {
    var checkObj;
    findCompany();
    layer.open({
        type: 1,
        title: '选择供应商',
        area: ['710px', 'auto'],
        skin:'pop',
        btns :2,
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#linkcompanyPop'),
        yes : function(index){
            var inputs=$('.supp_list tbody input');
            var count = 0;
            inputs.each(function(){
                if($(this).prop('checked')==true){
                    count++;
                    checkObj = $(this);
                }
            });
            if(count>1||count<1){
                layer.msg("请选择一条数据！",{icon:7});
                return;
            }else {
                var trObj = checkObj;
                var linkcompanyid=$(trObj).val();
                var linkcompanyname=$(trObj).parent().parent().find("td")[1].innerText;
                var linkcompanyperson=$(trObj).parent().parent().find("td")[2].innerText;
                var linkcompanyphone=$(trObj).parent().parent().find("td")[3].innerText;
                $(obj).parent().find("[name='linkCompanyID']").val(linkcompanyid);
                $(obj).html(linkcompanyname);
                layer.close(index);
            }
        }
    });
}

function findCompany() {
    $("#companyBody").empty();
    var searchCompanyName=$("#searchCompanyName").val();
    var searchLinkman=$("#searchLinkman").val();
    var searchTelno=$("#searchTelno").val();
    var url="";
    var data;
    if(linktype=='0'){//与供应商互通
        url= basePath+"platform/common/getSupplierSelect";
        data={
            "sellerName":searchCompanyName,
            "sellerPerson":searchLinkman,
            "sellerPhone":searchTelno
        };
    }else {//1:与客户互通
        url= basePath+"platform/common/getBuyerSelect";
        data={
            "buyersName":searchCompanyName,
            "buyersPerson":searchLinkman,
            "buyersPhone":searchTelno
        };
    }
    $.ajax({
        url : url,
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var innerHtml = $("<tr></tr>");
            $.each(resultObj,function(i,o){
                //关联客户ID
                var linkcompanyid = "";
                //关联客户名称
                var linkcompanyname = "";
                //关联客户联系人
                var linkcompanyperson = "";
                //关联客户号码
                var linkcompanyphone = "";


                if(linktype=='0'){//与供应商互通
                    linkcompanyid = o.sellerId;
                    linkcompanyname = (o.sellerName==null||o.sellerName==undefined)?"":o.sellerName;
                    linkcompanyperson = (o.sellerPerson==null||o.sellerPerson==undefined)?"":o.sellerPerson;
                    linkcompanyphone = (o.sellerPhone==null||o.sellerPhone==undefined)?"":o.sellerPhone;
                }else {//1:与客户互通
                    linkcompanyid = o.buyersId;
                    linkcompanyname = o.buyersName;
                    linkcompanyperson = o.buyersPerson;
                    linkcompanyphone = o.buyersPhone;
                }

                var trObj = $("<tr>");
                $("<td><input type='radio'  value='"+linkcompanyid+"' name='linkCompanyID'/></td>").appendTo(trObj);
                $("<td>"+linkcompanyname+"</td>").appendTo(trObj);
                $("<td>"+linkcompanyperson+"</td>").appendTo(trObj);
                $("<td>"+linkcompanyphone+"</td>").appendTo(trObj);
                $("#companyBody").append(trObj);
            });
            //清除选中项
        //   $('.supp_list thead input').prop('checked',false).parent().removeClass('chk');
      //      supp_sellAll();

        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}



//保存关联
function saveProductLink() {
    var jsonArray = new Array();
    var flag = true;
    var i=1;
    var msg = "";
    $("#linkBody tr").each(function() {
        var productId = $(this).find("input[name='productId']").val();
        if(productId==null||productId==""||productId==undefined){
            flag = false;
            msg = "第"+i+"行，商品不能为空！";
            return false;

        }
        var barcode = $(this).find("input[name='barcode']").val();
        var price = $(this).find("input[name='price']").val();
        if(price==null||price==""||price==undefined){
            flag = false;
            msg = "第"+i+"行，采购价不能为空！";
            return false;
        }
        var dailyOutput = $(this).find("input[name='dailyOutput']").val();
        if(dailyOutput==null||dailyOutput==""||dailyOutput==undefined){
            flag = false;
            msg = "第"+i+"行，日产能不能为空！";
            return false;
        }
        var storageDay = $(this).find("input[name='storageDay']").val();
        if(storageDay==null||storageDay==""||storageDay==undefined){
            flag = false;
            msg = "第"+i+"行，到货天数不能为空！";
            return false;
        }
        var linkCompanyID = $(this).find("input[name='linkCompanyID']").val();
        if(linkCompanyID==null||linkCompanyID==""||linkCompanyID==undefined){
            flag = false;
            msg = "第"+i+"行，供应商不能为空！";
            return false;
        }
        // var linkCompanyBarcode = $(this).find("td:eq(3)").find("input").val();
        // if(linkCompanyBarcode==null||linkCompanyBarcode==""||linkCompanyBarcode==undefined){
        //     flag = false;
        //     msg = "第"+i+"行，条形码不能为空！";
        //     return false;
        // }

        var note = $(this).find("input[name='note']").val();
        var o = new Object();
        o.productId = productId;
        o.barcode = barcode;
        o.price = price;
        o.dailyOutput = dailyOutput;
        o.storageDay = storageDay;
        o.linkCompanyID = linkCompanyID;
        o.linkCompanyBarcode = "";
        o.note = note;
        jsonArray.push(o);
        i++;
    });
    //判断必输项
    if(!flag){
        layer.msg(msg,{icon:2});
        return;
    }
    if(jsonArray.length<=0){
        layer.msg("没有可提交的数据！",{icon:2});
        return;
    }
    var checkRst = checkProductLink(JSON.stringify(jsonArray));
    if(checkRst!=""){
        layer.msg(checkRst,{icon:2});
        return;
    }
    var url = "platform/product/saveProductLink";
    $.ajax({
        type : "POST",
        url : "platform/product/saveProductLink",
        data: {
            "linktype":linktype,
            "insList": JSON.stringify(jsonArray),
            "menuName":'17112910033928502412'
        },
        async:false,
        success:function(data){
            if(data.flag){
                var res = data.res;
                if(res.code==40000){
                    //调用成功
                	sucessSave(res.data);
                }else if(res.code==40010){
                    //调用失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40011){
                    //需要设置审批流程
                    layer.msg(res.msg,{time:500,icon:2},function(){
                        setApprovalUser(url,res.data,function(data){
                        	sucessSave(data);
                        });
                    });
                    return false;
                }else if(res.code==40012){
                    //对应菜单必填
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40099){
                    //合同保存失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40013){
                    //不需要审批
                    notNeedApproval(res.data,function(data){
                    	sucessSave(data);
                    });
                    return false;
                }
            }else{
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
                return false;
            }
        },
        error:function(){
            layer.msg("提交失败，请稍后重试！",{icon:2});
        }
    });
}

//检查是否已存在互通
function checkProductLink(data) {
    var rst = "";
    $.ajax({
        url : "platform/product/checkProductLink",
        data: {
            "data": data
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if (resultObj.success) {
                rst = "";
            }else {
                rst = resultObj.msg;
            }
        },
        error:function(){
            rst = "操作失败";
        }
    });
    return rst;
}

//保存成功
function sucessSave(data) {
    layer.msg("提交成功！",{icon:1});
    leftMenuClick(this,'platform/product/loadProductList?linktype='+linktype,linktype=="0"?"buyer":"sellers")
}