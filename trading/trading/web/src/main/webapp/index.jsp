<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<% 
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>钉钉登录</title>
    <meta charset="utf-8">
    <meta name="keywords" content="诺泰，诺泰买卖，买卖系统，nuotai">
    <meta name="description" content=" 诺泰买卖系统是用于商家向供应商采购商品。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script src="//cdn.bootcss.com/jquery/1.12.3/jquery.min.js"></script>
	<!-- <script src="//res.layui.com/layui/release/layer/dist/layer.js?v=3111"></script>
	<link rel="stylesheet" href="http://res.layui.com/layui/release/layer/dist/theme/default/layer.css?v=3.1.1" id="layuicss-layer"> -->
  </head>
  <script type="text/javascript">
var basePath = "<%=basePath %>";
function dingDingLogin(){
	var login_name = "<%=request.getParameter("login_name")%>";
	var userid = "<%=request.getParameter("userid")%>";
	var dingid = "<%=request.getParameter("dingid")%>";
	var username = "<%=request.getParameter("username")%>";
	if (login_name != '' && login_name != 'null' && userid != ''
			&& userid != 'null' && dingid != '' && dingid != 'null'
			&& username != '' && username != 'null') {
		$.ajax({
			type : "get",
			url : basePath + "platform/dingDingLogin",
			data : {
				"login_name" : login_name,
				"userid" : userid,
				"username" : username
			},
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					/* layer.load('加载中'); */
					document.location.href = basePath+ "platform/loadIndexHtml";
				} else {
					document.location.href = basePath+ "platform/loadLoginHtml";
				}
			},
			error : function() {
				document.location.href = basePath+ "platform/loadLoginHtml";
			}
		});
	} else {
	}
}
function isJSONObject(obj) {
	var isjson = typeof (obj) == "object"
			&& Object.prototype.toString.call(obj).toLowerCase() == "[object object]"
			&& !obj.length;
	return isjson;
}
</script>
<body onload="dingDingLogin();"></body>
</html>
