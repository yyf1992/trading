package com.nuotai.trading.seller.service.buyer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.buyer.SBuyBillCycleManagementMapper;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagement;
import com.nuotai.trading.seller.service.SellerBillCycleInterestService;
import com.nuotai.trading.seller.service.SellerBillCycleOldService;
import com.nuotai.trading.seller.service.SellerBillInterestOldService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class SBuyBillCycleService {
	@Autowired
	private SBuyBillCycleManagementMapper sBuybillMapper;

	public int deleteByPrimaryKey(String id) {
		return sBuybillMapper.deleteByPrimaryKey(id);
	}

	public int insert(SBuyBillCycleManagement record) {
		return 0;
	}

	public int insertSelective(SBuyBillCycleManagement record) {
		return 0;
	}

	// 根据id编号查询账单信息
	public SBuyBillCycleManagement selectByPrimaryKey(String id) {
		return sBuybillMapper.selectByPrimaryKey(id);
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(SBuyBillCycleManagement record) {
		return sBuybillMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(SBuyBillCycleManagement record) {
		return 0;
	}

	//查询账单周期列表信息
	public List<Map<String, Object>> getBillCycleList(
			SearchPageUtil searchPageUtil) {
		return sBuybillMapper.getBillCycleList(searchPageUtil);
	}

	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return sBuybillMapper.queryBillStatusCount(map);
	}
}
