package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.seller.model.SellerBillCycleManagement;
import com.nuotai.trading.seller.model.SellerBillCycleManagementOld;
import com.nuotai.trading.seller.model.SellerBillInterest;
import com.nuotai.trading.seller.model.SellerBillInterestOld;
import com.nuotai.trading.seller.service.SellerBillCycleInterestService;
import com.nuotai.trading.seller.service.SellerBillCycleOldService;
import com.nuotai.trading.seller.service.SellerBillCycleService;
import com.nuotai.trading.seller.service.SellerBillInterestOldService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.Msg;
import com.nuotai.trading.utils.json.Response;
import com.nuotai.trading.utils.json.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */

@Controller
@RequestMapping("platform/seller/billCycle")
public class SellerBillCycleController extends BaseController {
	@Autowired
	private SysUserService sysUserService;//用户信息
	@Autowired
	private SellerBillCycleService sellerBillCycleService;//账单周期
	@Autowired
	private SellerBillCycleOldService sellerBillCycleOldService;//历史账单周期
	@Autowired
	private SellerBillCycleInterestService sellerBillCycleInterestService;//关联利息
	@Autowired
	private SellerBillInterestOldService sellerBillInterestOldService;//关联历史利息
	@Autowired
	private BuyCompanyService buyCompanyService; //公司管理
	
	/**
	 * 账单周期管理列表查询
	 * @return
	 */
	@RequestMapping("billCycleList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("billDealStatus")){
			params.put("billDealStatus","0");
		}
		params.put("selCompanyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<Map<String, Object>> billCycleList = sellerBillCycleService.getSellerBillCycleList(searchPageUtil);
		sellerBillCycleService.getBillCountByStatus(params);
		searchPageUtil.getPage().setList(billCycleList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/sellers/billcycles/billcyclemanagements/billCycleManagement";
	}

	/**
	 * 修改账单周期及关联利息信息
	 * @param updateMap
	 * @return
	 */
	@RequestMapping("updateSaveBillCycleInfo")
	@ResponseBody
	public String updateSaveBillCycleInfo(@RequestParam Map<String, Object> updateMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillCycleService.updateSaveBillCycleInfo(updateMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "账单周期修改成功！");
			}else {
				json.put("error", false);
				json.put("msg", "账单周期修改失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 审批通过 (注意备注都在审批表中)
	 * @param id
	 */
	/*@RequestMapping(value = "/vSellerBillCycleSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String vSellerBillCycleSuccess(String id){
		JSONObject json  = sellerBillCycleService.vSellerBillCycleSuccess(id);
		return json.toString();
	}*/

	/**
	 * 审批拒绝
	 * @param id
	 */
	/*@RequestMapping(value = "/vSellerBillCycleError", method = RequestMethod.GET)
	@ResponseBody
	public String vSellerBillCycleError(String id){
		JSONObject json = sellerBillCycleService.vSellerBillCycleError(id);
		return json.toString();
	}*/

	/**
	 * 审批查看详情
	 * @return
	 */
	/*@RequestMapping(value ="vSellerBillCycleDetail", method = RequestMethod.GET)
	public String vSellerBillCycleDetail(String id){
		//账单结算周期详情
		SellerBillCycleManagement sellerBillCycleManagement = sellerBillCycleService.selectByPrimaryKey(id);
		model.addAttribute("sellerBillCycleManagement",sellerBillCycleManagement);
		//账单结算周期关联利息
		List<Map<String, Object>> interestList = sellerBillCycleInterestService.getBillInteresInfo(id);
		model.addAttribute("interestList",interestList);
		//历史账单周期数据查询
		SellerBillCycleManagementOld sellerCycleManagementOld = sellerBillCycleOldService.queryLastDateBillCycleOld(id);
		model.addAttribute("sellerCycleManagementOld",sellerCycleManagementOld);
		if(null != sellerCycleManagementOld){
			//历史账单周期关联利息查询
			if(null != sellerCycleManagementOld.getId() && !"".equals(sellerCycleManagementOld.getId())){
				List<SellerBillInterestOld> billInterestOldList = sellerBillInterestOldService.queryBillInterestOld(sellerCycleManagementOld.getId());
				model.addAttribute("billInterestOldList",billInterestOldList);
			}
		}
		//卖家信息
		return "platform/seller/billcycles/billcyclemanagements/verifyDetail";
	}*/


	/**
	 * 根据账单周期编号查询关联利息
	 * @param interestMap
	 * @return
	 */
	@RequestMapping("queryInterestList")
	@ResponseBody
	public String queryInterestList(@RequestParam Map<String,Object> interestMap){
		String billCycleId = (String) interestMap.get("billCycleId");
		List<SellerBillInterest> interestList = sellerBillCycleInterestService.getBillInteresInfo(billCycleId);
		return JSONObject.toJSONString(interestList);
	}
	
}
