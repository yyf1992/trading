package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.BuyContract;


/**
 * 
 * 
 * @author liuhui
 * @date 2017-08-07 11:01:28
 */
public interface SBuyContractMapper extends BaseDao<BuyContract> {
}
