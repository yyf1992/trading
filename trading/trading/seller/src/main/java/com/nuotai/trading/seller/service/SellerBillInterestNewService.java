package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerBillInterestNewMapper;
import com.nuotai.trading.seller.model.SellerBillInterestNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class SellerBillInterestNewService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillInterestNewService.class);

	@Autowired
	private SellerBillInterestNewMapper sellerBillInterestNewMapper;

	//根据申请周期编号查询关联利息
	public List<SellerBillInterestNew> queryNewInterestList(String id){
		return sellerBillInterestNewMapper.queryNewInterest(id);
	}
	
	public List<SellerBillInterestNew> queryList(String id){
		return sellerBillInterestNewMapper.queryList(id);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerBillInterestNewMapper.queryCount(map);
	}

	//添加申请关联利息
	public void addNewInterest(SellerBillInterestNew buyBillInterestNew){
		sellerBillInterestNewMapper.addNewInterest(buyBillInterestNew);
	}
	
	public void update(SellerBillInterestNew buyBillInterestNew){
		sellerBillInterestNewMapper.update(buyBillInterestNew);
	}
	
	public void delete(String id){
		sellerBillInterestNewMapper.delete(id);
	}
	

}
