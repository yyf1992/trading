package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerBillInterestOldMapper;
import com.nuotai.trading.seller.model.SellerBillInterestOld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class SellerBillInterestOldService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillInterestOldService.class);

	@Autowired
	private SellerBillInterestOldMapper sellerBillInterestOldMapper;
	
	public SellerBillInterestOld get(String id){
		return sellerBillInterestOldMapper.get(id);
	}
	
	public List<SellerBillInterestOld> queryList(Map<String, Object> map){
		return sellerBillInterestOldMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerBillInterestOldMapper.queryCount(map);
	}

	//添加新历史关联利息
	public int addNewInterestOld(SellerBillInterestOld buyBillInterestOld){
		return sellerBillInterestOldMapper.addNewInterestOld(buyBillInterestOld);
	}
	//查询最近历史纪录
	public List<SellerBillInterestOld> queryBillInterestOld(String billNewCycleOldId){
		return sellerBillInterestOldMapper.queryBillInterestOld(billNewCycleOldId);
	}
	public void update(SellerBillInterestOld buyBillInterestOld){
		sellerBillInterestOldMapper.update(buyBillInterestOld);
	}

	//删除历史账单关联利息
	public int deleteNewInterestOld(String id){
		return sellerBillInterestOldMapper.deleteNewInterestOld(id);
	}
	

}
