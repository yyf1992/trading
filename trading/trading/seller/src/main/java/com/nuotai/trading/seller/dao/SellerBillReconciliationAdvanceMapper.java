package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillReconciliationAdvance;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author "
 * @date 2018-02-24 13:04:37
 */
@Component
public interface SellerBillReconciliationAdvanceMapper extends BaseDao<SellerBillReconciliationAdvance> {
    //列表数据查询
    List<SellerBillReconciliationAdvance> queryAdvanceList(SearchPageUtil searchPageUtil);
    //根据条件查询预付款数据
    List<SellerBillReconciliationAdvance> selAdvanceList(Map<String,Object> selMap);
    //根据状态查询数量
    int getAdvanceCountByStatus(Map<String, Object> map);
    //查询下单人是否已经存在
    int isOrNoBillUser(Map<String,Object> isOrNoMap);
    //根据编号修改数据
    int updateAdvance(SellerBillReconciliationAdvance sellerBillReconciliationAdvance);
    //根据条件查询预付款信息
    SellerBillReconciliationAdvance queryAdvanceBuyMap(Map<String,Object> selMap);
    //根据条件查询预付款详情
    Map<String,Object> queryAdvanceEditInfo(Map<String,Object> map);
    //根据新增账单添加预付款记录
    int addAdvanceEdit(Map<String,Object> addMap);
    //根据驳回账单删除预付款修改记录
    int deleteAdvanceEdit(Map<String,Object> editMap);
    //修改预修改记录付款状态
    int updateAdvanceEdit(Map<String,Object> editMap);
    //根据条件查询预付款修改记录
    List<Map<String,Object>> queryAdvanceEditList(Map<String,Object> editMap);

}
