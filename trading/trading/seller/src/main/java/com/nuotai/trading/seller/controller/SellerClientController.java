package com.nuotai.trading.seller.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.DicProvinces;
import com.nuotai.trading.seller.model.SellerClient;
import com.nuotai.trading.seller.model.SellerClientLinkman;
import com.nuotai.trading.seller.service.SellerClientImplService;
import com.nuotai.trading.seller.service.SellerClientLinkmanImplService;
import com.nuotai.trading.service.DicProvincesService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 我是卖家-我的客户
 * @author wangli
 * @date 2017-07-21 13:58:14
 */
@Controller
@RequestMapping("platform/sellers/client")
public class SellerClientController extends BaseController{
	
	@Autowired
	private SellerClientImplService sellerClientService;
	
	@Autowired
	private DicProvincesService dicProvincesService;

	
	@Autowired
	private SellerClientLinkmanImplService sellerClientLinkmanService;
	
	/**
	 * 跳转至添加客户页面
	 */	
	@RequestMapping("/addClient")
	public String addClient(HttpServletRequest request,@RequestParam Map<String,Object> map){
			
		// 加载省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(map);
        model.addAttribute("provinceList", provinceList);
		return "platform/sellers/client/addClient";
	}
	 
	/**
	 * 添加成功页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/addClientSuccess")
	public String addSucc(HttpServletRequest request){
		return "platform/sellers/client/addClientSuccess";
	}
	
	/**
	 * 跳转至客户管理页面
	 */
	@RequestMapping("/clientList")
	public String ClientList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId",ShiroUtils.getCompId());
		String clientName = (String) (map.get("clientName") == null ? "" : map.get("clientName").toString().trim());
		String clientPerson = (String) (map.get("clientPerson") == null ? "" : map.get("clientPerson").toString().trim());
		String shipAddress = (String) (map.get("shipAddress") == null ? "" : map.get("shipAddress").toString().trim());
		String clientPhone = (String) (map.get("clientPhone") == null ? "" : map.get("clientPhone").toString().trim());
		map.put("clientName", clientName);
		map.put("clientPerson", clientPerson);
		map.put("shipAddress", shipAddress);
		map.put("clientPhone", clientPhone);
		searchPageUtil.setObject(map);
		List<Map<String,Object>> sellerClientList = sellerClientService.selectSellerClientByPage(searchPageUtil);
		searchPageUtil.getPage().setList(sellerClientList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		
		return "platform/sellers/client/clientList";
	}
	
	/**
	 * 保存客户
	 */
	@ResponseBody
	@RequestMapping("/saveSellerClient")
	public String saveClient(SellerClient sellerClient,@RequestParam Map<String,Object> mapLinkman){
		JSONObject json = new JSONObject();
		if (sellerClient != null && mapLinkman.size() >0){
			
			try {
				sellerClientService.saveSellerClient(sellerClient,mapLinkman);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
	
	/**
	 * 手工订单保存客户
	 * @param sellerClient
	 * @param mapLinkman
	 * @return
	 */

	@ResponseBody
	@RequestMapping("/manualSaveSellerClient")
	public String manualSaveSellerClient(SellerClient sellerClient,@RequestParam Map<String,Object> mapLinkman){
		JSONObject json = new JSONObject();
		if (sellerClient != null && mapLinkman.size() >0){
			
			try {
				sellerClientService.manualSaveSellerClient(sellerClient,mapLinkman);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
	
	
	/**
	 * 跳转到修改客户页面
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping("/updateClientDetails")
	public String updateSupplier(String id,@RequestParam Map<String,Object> map){
		
		// 加载省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(map);
        model.addAttribute("provinceList", provinceList);

        // 客户信息
        SellerClient sellerClient = sellerClientService.selectSellerClientById(id);
		model.addAttribute("sellerClient", sellerClient);
		
        // 客户联系人信息
 		List<SellerClientLinkman> linkmanList = sellerClientLinkmanService.selectBySellerClientId(id);
 		model.addAttribute("linkmanList", linkmanList);
 		
		return "platform/sellers/client/updateClientDetails";
	}
	
	/**
	 * 保存修改客户
	 * @param sellerClient
	 * @param mapLinkman
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveUpdateClientDetails")
	public String update(SellerClient sellerClient,@RequestParam Map<String,Object> mapLinkman){
		
		JSONObject json = new JSONObject();
		if (sellerClient != null){
			try {
				sellerClientService.updateSellerClientById(sellerClient, mapLinkman);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}
			
		return json.toString();
	}
	
	/**
	 * 删除客户
	 */
	@ResponseBody
	@RequestMapping("/deleteSellerClient")
	public String deleteSellerClient( String id){

		JSONObject json = new JSONObject();
		if (id != null){
			try {
				sellerClientService.deleteSellerClient(id);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}
			
		return json.toString();
	}
	
	/**
	 * 批量删除客户
	 */
	@ResponseBody
	@RequestMapping("/deleteBeatchSellerClient")
	public String deleteBeatchSellerClient(String ids){
		JSONObject json = new JSONObject();
		if (ids != null){
			try {
				sellerClientService.deleteBeatchSellerClient(ids);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}
			
		return json.toString();
	}
	
	/**
	 * 加载客户联系人详情页
	 * @param id
	 * @return
	 */
	@RequestMapping("/loadClientDetails")
	public String loadSellerClientDetails(String id){
		
		if (id != null && id != ""){
			//加载客户
			SellerClient sellerClient = sellerClientService.selectSellerClientById(id);
			model.addAttribute("sellerClient", sellerClient);
			//加载客户联系人
			List<SellerClientLinkman> sellerClinentLinkmanList = sellerClientLinkmanService.selectBySellerClientId(id);
			model.addAttribute("sellerClinentLinkmanList", sellerClinentLinkmanList);
		}
		
		return "platform/sellers/client/loadClientDetails";
	}
	
	/**
	 * 根据客户id查询联系人信息
	 * @param id
	 * @return
	 */
	@RequestMapping("/loadClientLinkman")
	public String loadSellerClientLinkman(String sellerClientId){
		Map<String,Object> mapPare = new HashMap<String, Object>();
		if (sellerClientId != null && sellerClientId != ""){
			//加载客户联系人
			List<SellerClientLinkman> sellerClinentLinkmanList = sellerClientLinkmanService.selectBySellerClientId(sellerClientId);
			mapPare.put("sellerClinentLinkmanList", sellerClinentLinkmanList);
		}
		
		
		return JSON.toJSONString(mapPare);
	}
	
	/**
	 * 修改时删除客户联系人
	 * @param linkmanId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delClientLinakman")
	public String deleteClientLinakman(String id){
		JSONObject json = new JSONObject();
		if (!ObjectUtil.isEmpty(id)){
			try {
				sellerClientLinkmanService.deleteSellerClientLinkman(id);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}
			
		return json.toString();
	}
	
	/**
	 * 获得采购商下拉
	 * @return
	 */
	@RequestMapping("getSellerSelect")
	@ResponseBody
	public String getBuyerSelect(@RequestParam Map<String,Object> params){
		params.put("companyId", ShiroUtils.getCompId());
		params.put("clientName", params.get("selectValue").toString());
		List<Map<String,Object>> sellerList = sellerClientService.selectByMap(params);
		return JSONObject.toJSONString(sellerList);
	}
	
	/**
	 * 加载修改收货地址页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAddress", method = RequestMethod.POST)
	public String updateAddress(String id,@RequestParam Map<String,Object> map) throws Exception{
		// 加载省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(map);
        model.addAttribute("provinceList", provinceList);
        //查询联系人的收货地址信息
		SellerClientLinkman sa = sellerClientLinkmanService.selectByPrimaryKey(id);
		model.addAttribute("updateAddress", sa);
		return "platform/sellers/salesmanagement/updateAddressManualOrder";
	}
	
	/**
	 * 修改收货地址保存
	 * @param Address
	 * @return
	 */
	@RequestMapping(value = "/saveUpdateAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdateAddress(SellerClientLinkman address){
		JSONObject json = sellerClientLinkmanService.saveUpdate(address);
		return json.toString();
	}
	
	/**
	 * 加载新增收货地址页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addAddressManualOrder", method = RequestMethod.POST)
	public String addAddress(String sellerClientId,@RequestParam Map<String,Object> map) throws Exception{
		// 加载省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(map);
        model.addAttribute("provinceList", provinceList);
        model.addAttribute("sellerClientId", sellerClientId);
		return "platform/sellers/salesmanagement/addAddressManualOrder";
	}
	
	/**
	 * 添加收货地址保存
	 * @param Address
	 * @return
	 */
	@RequestMapping(value = "/saveAddAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveAddAddress(SellerClientLinkman address,@RequestParam Map<String,Object> map){
		JSONObject json = sellerClientLinkmanService.saveAdd(address,map);
		return json.toString();
	}
	
	
}
