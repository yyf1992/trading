package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerBillInterestMapper;
import com.nuotai.trading.seller.model.SellerBillInterest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 账单结算周期关联利息
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
public class SellerBillCycleInterestService {
	@Autowired
	private SellerBillInterestMapper sellerBillInterestMapper;

	//保存账单关联利息
	public int saveInterestInfo(Map<String, Object> map) {
		return sellerBillInterestMapper.saveInterestInfo(map);
	}

	//获取账单关联利息
	public List<SellerBillInterest> getBillInteresInfo(String billCycleId) {
		return sellerBillInterestMapper.getBillInteresInfo(billCycleId);
	}

	//根据账单周期编号删除关联利息
	public int deleteByPrimaryKey(String billCycleId) {
		return sellerBillInterestMapper.deleteByPrimaryKey(billCycleId);
	}

	public int insert(SellerBillInterest record) {
		return 0;
	}

	public int insertSelective(SellerBillInterest record) {
		return 0;
	}

	public SellerBillInterest selectByPrimaryKey(String id) {
		return null;
	}

	public int updateByPrimaryKeySelective(SellerBillInterest record) {
		return 0;
	}

	public int updateByPrimaryKey(SellerBillInterest record) {
		return 0;
	}

}
