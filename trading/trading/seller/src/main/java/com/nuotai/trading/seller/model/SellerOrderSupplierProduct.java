package com.nuotai.trading.seller.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class SellerOrderSupplierProduct {

    //主键
    private String id;
    //订单主键Id
    private String orderId;
    //訂單編號
    private String orderCode;
    //訂單日期（外部）
    private Date orderDate;
    //买家订单ID
    private String buyOrderId;
    //买家订单明细ID
    private String buyOrderItemId;
    //采购计划单号
    private String applyCode;
    //货号
    private String productCode;
    //商品名称
    private String productName;
    //规格代码
    private String skuCode;
    //规格名称
    private String skuName;
    //条形码
    private String barcode;
    //单价
    private BigDecimal price;
    //采购数量
    private Integer orderNum;
    //优惠金额
    private BigDecimal discountMoney;
    //总金额
    private BigDecimal totalMoney;
    //仓库
    private String warehouseId;
    //已发货数量
    private Integer deliveredNum;
    //待发货数量
    private Integer waitDeliveryNum;
    //到货数量
    private Integer arrivalNum;
    //未到货数量
    private Integer unArrivalNum;
    //备注
    private String remark;
    //单位ID
    private String unitId;
    //是否需要发票Y:是N:否
    private String isNeedInvoice;
    //要求到货日期（外部）
    private Date predictArred;
    //单位名称
    private String unitName;
    //訂單狀態（外部）
    private String orderStatus;
    //采购商（外部）
    private String buyerName;
    private String orderRemark;
    private List<Map<String,Object>> deliveryList;

}