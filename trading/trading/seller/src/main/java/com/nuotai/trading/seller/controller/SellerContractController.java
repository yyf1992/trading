package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.seller.model.SellerClient;
import com.nuotai.trading.seller.model.SellerContract;
import com.nuotai.trading.seller.model.buyer.BuyContract;
import com.nuotai.trading.seller.service.SellerClientImplService;
import com.nuotai.trading.seller.service.SellerContractService;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.utils.*;
import com.nuotai.trading.utils.json.Msg;
import com.nuotai.trading.utils.json.Response;
import com.nuotai.trading.utils.json.ServiceException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuhui
 * @description 合同管理
 * @create 2017-08-18 15:29
 **/
@Controller
@RequestMapping("platform/seller/contract")
@SuppressWarnings("all")
public class SellerContractController extends BaseController {
    @Autowired
    private SellerContractService contractService;
    @Autowired
    private SellerClientImplService sellerClientService;
    @Autowired
    private BuyCompanyService companyService;
    @Autowired
    private AttachmentService attachmentService;

    /**
     * 我的合同列表
     * @param searchPageUtil
     * @param params
     * @return
     */
    @RequestMapping(value = "contractList", method = RequestMethod.GET)
    public String contractList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
        //根据role判断是从我是买家还是我是卖家
        String role = "seller";
        model.addAttribute("role",role);
        Map<String,Object> map = contractService.queryContractList(role,searchPageUtil,params);
        model.addAttribute("totalCount",String.valueOf(map.get("total")));
        model.addAttribute("draftCount",String.valueOf(map.get("draftCount")));
        model.addAttribute("myApprovalCount",String.valueOf(map.get("myApprovalCount")));
        model.addAttribute("customerApprovalCount",String.valueOf(map.get("customerApprovalCount")));
        model.addAttribute("contractPhotoCount",String.valueOf(map.get("contractPhotoCount")));
        model.addAttribute("okCount",String.valueOf(map.get("okCount")));
        model.addAttribute("rejectCount",String.valueOf(map.get("rejectCount")));
        List<SellerContract> contractList = (List<SellerContract>) map.get("contractList");
        searchPageUtil.getPage().setList(contractList);
        model.addAttribute("searchPageUtil", searchPageUtil);

        return "platform/buyer/contract/sellerContractList";
    }

    /**
     * 添加合同
     * @return
     */
    /*@RequestMapping("addContract")
    public String addContract(){
        //客户信息
        Map<String,Object> roleMap = new HashMap<String,Object>();
        roleMap.put("companyId", ShiroUtils.getCompId());
        List<SellerClient> customerList = sellerClientService.queryList(roleMap);
        model.addAttribute("customerList",customerList);
        BuyCompany company = companyService.selectByPrimaryKey(ShiroUtils.getCompId());
        model.addAttribute("createrName",company.getCompanyName());
        model.addAttribute("role","seller");
        return "platform/buyer/contract/addContract";
    }*/


    /**
     * 修改合同
     * @param request
     * @return
     */
    /*@RequestMapping("updContract")
    public String updContract(String id){
        //客户信息
        Map<String,Object> roleMap = new HashMap<String,Object>();
        roleMap.put("companyId", ShiroUtils.getCompId());
        List<SellerClient> customerList =sellerClientService.queryList(roleMap);
        model.addAttribute("customerList",customerList);
        SellerContract contract = contractService.get(id);
        //合同内容
        String contentAddress = contract.getContentAddress();
        String htmlContent = "";
        if(!ObjectUtil.isEmpty(contentAddress)){
            String prefix=contentAddress.substring(contentAddress.lastIndexOf(".")+1);
            if(prefix.equalsIgnoreCase("DOCX")){
                htmlContent = PoiWordToHtml.docxToHtml(contentAddress);
            }else {
                htmlContent = PoiWordToHtml.docToHtml(contentAddress);
            }
        }
        model.addAttribute("htmlContent",htmlContent);
        model.addAttribute("contract",contract);
        model.addAttribute("role","seller");
        return "platform/buyer/contract/updContract";
    }*/

    /**
     * 合同明细
     * @param id
     * @return
     */
    @RequestMapping("contractDetail")
    public String contractDetail(String id){

        //合同信息
        SellerContract contract = contractService.get(id);
        //合同内容
        String contentAddress = contract.getContentAddress();
        String htmlContent = "";
        if(!ObjectUtil.isEmpty(contentAddress)){
            String prefix=contentAddress.substring(contentAddress.lastIndexOf(".")+1);
            if(prefix.equalsIgnoreCase("DOCX")){
                htmlContent = PoiWordToHtml.docxToHtml(contentAddress);
            }else {
                htmlContent = PoiWordToHtml.docToHtml(contentAddress);
            }
        }
        //合同原件
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("relatedId",contract.getId());
        List<BuyAttachment> attachmentList = attachmentService.selectByMap(map);
        model.addAttribute("contract",contract);
        model.addAttribute("htmlContent",htmlContent);
        model.addAttribute("attachmentList",attachmentList);
        model.addAttribute("role", "seller");
        model.addAttribute("approvePop","");//是否是审批弹出详情
        return "platform/buyer/contract/contractDetail";
    }

    /**
     * 合同打印
     * @param contractId
     * @param contentAddress
     * @return
     */
    @RequestMapping("printContract")
    public String printContract(String contractId,String contentAddress){

        String htmlContent = "";
        if(!ObjectUtil.isEmpty(contentAddress)){
            String prefix=contentAddress.substring(contentAddress.lastIndexOf(".")+1);
            if(prefix.equalsIgnoreCase("DOCX")){
                htmlContent = PoiWordToHtml.docxToHtml(contentAddress);
            }else {
                htmlContent = PoiWordToHtml.docToHtml(contentAddress);
            }
        }
        //合同原件
        model.addAttribute("htmlContent",htmlContent);
        return "platform/buyer/contract/contractPrint";
    }
    /**
     * 上传合同内容附件
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadContent",method=RequestMethod.POST)
    @ResponseBody
    public String uploadContent(@RequestParam(value = "file", required = false) MultipartFile file)
            throws Exception{

        JSONObject json = new JSONObject();
        try {
            //获取文件名
            String fileName = file.getOriginalFilename();
            json.put("fileName", fileName);
            //获取文件后缀名
            String fileExtensionName = FilenameUtils.getExtension(fileName);
            if(!fileExtensionName.equalsIgnoreCase("DOC") && !fileExtensionName.equalsIgnoreCase("DOCX")){
                json.put("result", "fail");
                json.put("msg", "上传文件格式不正确！");
                return json.toString();
            }
            json.put("fileExtensionName", fileExtensionName);
            //获取文件大小
            String fileSize = ShiroUtils.convertFileSize(file.getSize());
            json.put("fileSize", fileSize);
            UplaodUtil uplaodUtil = new UplaodUtil();
            String url = uplaodUtil.uploadFile(file,null,true);
            String htmlContent = "";
            if(fileExtensionName.equalsIgnoreCase("DOCX")){
                htmlContent = PoiWordToHtml.docxToHtml(url);
            }else {
                htmlContent = PoiWordToHtml.docToHtml(url);
            }
            json.put("htmlContent",htmlContent);
            json.put("filePath", url);
            json.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "fail");
            json.put("msg", "上传失败！请联系管理员！");
            return json.toString();
        }
        return json.toString();
    }

    /**
     * 保存合同
     * @param SellerContract
     * @return
     */
    @RequestMapping(value = "/saveContract", method = RequestMethod.POST)
    @ResponseBody
    public String saveContract(SellerContract sellerContract){
        JSONObject json = new JSONObject();
        try {
            Msg saveMsg = contractService.saveContract(sellerContract);
            if(saveMsg.isFlag()){
                json.put("success", true);
                json.put("msg", "保存成功！");
            }else {
                json.put("success", false);
                json.put("msg", saveMsg.getMsg());
            }
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "保存失败！");
        }
        return json.toString();
    }

    /**
     * 上传合同原件
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadOriContract",method=RequestMethod.POST)
    @ResponseBody
    public String uploadOriContract(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
            throws Exception{
        JSONObject json = new JSONObject();
        try {
            List urlList = new ArrayList();
            for (MultipartFile file : files) {
                //获取文件名
                String fileName = file.getOriginalFilename();
                json.put("fileName", fileName);
                //获取文件后缀名
                String fileExtensionName = FilenameUtils.getExtension(fileName);
                if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
                        && !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
//					json.put("result", "fail");
//					json.put("msg", "上传文件格式不正确！");
//					return json.toString();
                    continue;
                }
                //获取文件大小
                String fileSize = ShiroUtils.convertFileSize(file.getSize());
                UplaodUtil uplaodUtil = new UplaodUtil();
                String url = uplaodUtil.uploadFile(file,null,true);
                urlList.add(url);
            }
            json.put("urlList", urlList);
            json.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "fail");
            json.put("msg", "上传失败！请联系管理员！");
            return json.toString();
        }
        return json.toString();
    }

    /**
     * 加载合同原件
     * @param relatedId
     * @return
     */
    @RequestMapping("loadContractOriginal")
    @ResponseBody
    public String loadContractOriginal(String relatedId){
        JSONArray array = new JSONArray();
        Map<String,Object> map = new HashMap<>();
        map.put("relatedId",relatedId);
//		map.put("type",0);
        List<BuyAttachment> list = attachmentService.selectByMap(map);
        return JSONObject.toJSONString(list);
    }

    /**
     *协议达成
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "agreementReached", method = RequestMethod.POST)
    @ResponseBody
    public String agreementReached(String contractId,String validPeriodStart,String validPeriodEnd,String oriAttachments){
        JSONObject json = new JSONObject();
        try {

            contractService.agreementReached(contractId,validPeriodStart,validPeriodEnd,oriAttachments);
            json.put("success", true);
            json.put("msg", "保存成功！");
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "保存失败！");
        }
        return json.toString();
    }

    /**
     * 合同保存并提交审批
     * @param params
     */
    @RequestMapping(value = "/saveContractApprove", method = RequestMethod.POST)
    public void saveContractApprove(@RequestParam Map<String,Object> params){
        SellerContract sellerContract = JSONObject.parseObject(String.valueOf(params.get("sellerContract")),SellerContract.class);
        Msg msg = new Msg();
        Response res = new Response();
        final List<String> idList = new ArrayList<>();
        //1.保存合同
        Msg saveMsg = contractService.saveContract(sellerContract);
        if(saveMsg.isFlag()){//保存成功，然后做审批
            idList.add(saveMsg.getMsg());
            //2.审批
            insert(params, new IService(){
                @Override
                public JSONObject init(Map<String,Object> params)
                        throws ServiceException {
                    JSONObject json = new JSONObject();
                    json.put("verifySuccess", "platform/seller/contract/approveSuccess");//审批成功调用的方法
                    json.put("verifyError", "platform/seller/contract/approveError");//审批失败调用的方法
                    json.put("relatedUrl", "platform/buyer/contract/approveDetail");//审批详细信息地址
                    json.put("idList", idList);
                    return json;
                }
            });
        }else {//保存失败
            msg.setFlag(state_success);
            res.setCode(40099);
            res.setMsg(saveMsg.getMsg());
            msg.setRes(res);
            out2html(response, msg.toJsonString());
        }
    }

    /**
     * 提交审批按钮
     * @param params
     */
    @RequestMapping(value = "/subAprove")
    @ResponseBody
    public String subAprove(@RequestParam Map<String,Object> params){
//        String contractId = String.valueOf(params.get("contractId"));
//        final List<String> idList = new ArrayList<>();
//        idList.add(contractId);
//        insert(params, new IService(){
//            @Override
//            public JSONObject init(Map<String,Object> params)
//                    throws ServiceException {
//                JSONObject json = new JSONObject();
//                json.put("verifySuccess", "platform/seller/contract/approveSuccess");//审批成功调用的方法
//                json.put("verifyError", "platform/seller/contract/approveError");//审批失败调用的方法
//                json.put("relatedUrl", "platform/seller/contract/approveDetail");//审批详细信息地址
//                json.put("idList", idList);
//                return json;
//            }
//        });
        String contractId = String.valueOf(params.get("contractId"));
        JSONObject json = contractService.approveSuccess(contractId);
        return json.toString();
    }
    /**
     * 下单成功页面
     * @return
     */
    @RequestMapping("success")
    public String success(@RequestParam Map<String,Object> params){
        List<String> idList = (List<String>) params.get("ids");
        String id = "";
        if(!ObjectUtil.isEmpty(idList)&&idList.size()>0){
            id = idList.get(0);
        }
        model.addAttribute("role","seller");
        model.addAttribute("id",id);
        return "platform/buyer/contract/success";
    }
    /**
     * 审批通过
     * @param id
     */
    @RequestMapping(value = "/approveSuccess", method = RequestMethod.GET)
    @ResponseBody
    public String approveSuccess(String id){
        JSONObject json = contractService.approveSuccess(id);
        return json.toString();
    }

    /**
     * 审批拒绝
     * @param id
     */
    @RequestMapping(value = "/approveError", method = RequestMethod.GET)
    @ResponseBody
    public String approveError(String id){
        JSONObject json = contractService.approveError(id);
        return json.toString();
    }

    /**
     * 审批查看详情
     * @return
     */
    @RequestMapping("approveDetail")
    public String approveDetail(String id){
        //合同信息
        SellerContract contract = contractService.get(id);
        //合同内容
        String contentAddress = contract.getContentAddress();
        String htmlContent = "";
        if(!ObjectUtil.isEmpty(contentAddress)){
            String prefix=contentAddress.substring(contentAddress.lastIndexOf(".")+1);
            if(prefix.equalsIgnoreCase("DOCX")){
                htmlContent = PoiWordToHtml.docxToHtml(contentAddress);
            }else {
                htmlContent = PoiWordToHtml.docToHtml(contentAddress);
            }
        }
        //合同原件
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("relatedId",contract.getId());
        List<BuyAttachment> attachmentList = attachmentService.selectByMap(map);
        model.addAttribute("contract",contract);
        model.addAttribute("htmlContent",htmlContent);
        model.addAttribute("attachmentList",attachmentList);
        model.addAttribute("role", request.getParameter("role"));
        model.addAttribute("approvePop","approvePop");//是否是审批弹出详情
        return "platform/buyer/contract/contractDetail";
    }
}
