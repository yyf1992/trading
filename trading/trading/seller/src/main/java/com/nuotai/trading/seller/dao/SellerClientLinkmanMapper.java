package com.nuotai.trading.seller.dao;


import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerClientLinkman;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 客户联系人
 * 
 * @author "
 * @date 2017-07-26 14:39:11
 */
@Component("sellerClientLinkmanMapper")
public interface SellerClientLinkmanMapper extends BaseDao<SellerClientLinkman> {
	
	/**
	 * 保存客户联系人
	 * @param sellerClientLinkman
	 * @return
	 */
	int saveSellerClientLinkman(SellerClientLinkman sellerClientLinkman);
	
	/**
	 * 根据客户ID查询联系人
	 * @param sellerClientId
	 * @return
	 */
	List<SellerClientLinkman> selectBySellerClientId(String sellerClientId);
	
	/**
	 * 更新客户联系人
	 * @param
	 * @return
	 */
	void updateLinkmanById(SellerClientLinkman sellerClientLinkman);
	
	/**
	 * 删除联系人
	 * @param id
	 */
	void deleteSellerClientLinkman(String id);
	
	/**
	 * 查询联系人
	 * @param id
	 * @return
	 */
	SellerClientLinkman selectByPrimaryKey(String id);
	
	/**
	 * 更新联系人的收货信息
	 * @param sellerClientLinkman
	 * @return
	 */
	int updateLinkmanAddressById(SellerClientLinkman sellerClientLinkman);
	
	int cancelDefault(@Param("compId")String compId);
}
