package com.nuotai.trading.seller.model.buyer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 
 * 售后服务表
 * @author "dxl
 * @date 2017-09-12 16:49:24
 */
@Data
public class SBuyCustomer implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后编号
	private String customerCode;
	//公司id
	private String companyId;
	//供应商id
	private String supplierId;
	//供应商名称
	private String supplierName;
	// 供应商负责人
    private String person;
    // 手机号码
    private String phone;
	//售后类型0-换货，1-退款退货
	private String type;
	//是否删除0-未删除；1-已删除
	private String isDel;
	//状态0-待内部审核；1-待对方确认，2-已驳回，3-已取消，4-对方已确认
	private String status;
	//申请原因
	private String reason;
	//商品图片
	private String proof;
	// 收货地址信息
	private String addressId;
	private String province;
	private String city;
	private String area;
	private String addrName;
	private String zone;
	private String telNo;
	private String linkman;
	private String linkmanPhone;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人名称
	private String delName;
	//删除时间
	private Date delDate;
	//
	private List<SBuyCustomerItem> itemList;
	//退款金额
	private BigDecimal sumPrice;
	//审核意见
	private String verifyRemark;
	//审核时间
	private Date verifyDate;
	//出库标题
	private String title;
	//出库仓库
	private String warehouseId;
	private String pushType;
	private String arrivalType;
}
