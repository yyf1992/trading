package com.nuotai.trading.seller.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
@Data
public class SellerCustomerItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后id
	private String customerId;
	//采购计划单号
	private String applyCode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String skuOid;
	//单位
	private String unitId;
	//发货号
	private String deliveryCode;
	//发货明细id
	private String deliveryItemId;
	//售后类型  0-换货，1-退款退货
	private String type;
	//转换类型[0-未转换, 1-换货转退货]
	private String transformType;
	//换货/退款退货数量
	private Integer goodsNumber;
	//换货到货数量
	private Integer exchangeArrivalNum;
	//退货数量
	private Integer returnNum;
	//价格
	private BigDecimal price;
	//收货数量
	private Integer receiveNumber;
	//仓库id
	private String wareHouseId;
	//是否索要发票
	private String isNeedInvoice;
	//备注
	private String remark;
	//返修金额
	private BigDecimal repairPrice;
	//卖家确认数量
	private Integer confirmNumber;
	//买家itemid
	private String buyCustomerItemId;
	private Integer deliverNum;
	//换货发货列表
	private List<Map<String,Object>> deliveryList;
	//已发货数量（外部）
	private Integer deliveryNum;
	//到货数
	private Integer arrivalNum;
	//下单人
	private String createId;
	//下单人名称
	private String createName;
	//售后编号
	private String customerCode;
	//完成时间
	private Date verifyDate;
	//更新时间
	private Date updateDate;
	//换货单价
	private BigDecimal exchangePrice;
}
