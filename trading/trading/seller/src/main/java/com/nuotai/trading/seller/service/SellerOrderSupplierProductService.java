package com.nuotai.trading.seller.service;


import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * The type Seller order supplier product service.
 */
@Service
public class SellerOrderSupplierProductService {
	@Autowired
	private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;

	/**
	 * Delete by primary key int.
	 *
	 * @param id the id
	 * @return the int
	 */
	public int deleteByPrimaryKey(String id) {
		return sellerOrderSupplierProductMapper.deleteByPrimaryKey(id);
	}

	/**
	 * Insert int.
	 *
	 * @param record the record
	 * @return the int
	 */
	public int insert(SellerOrderSupplierProduct record) {
		return sellerOrderSupplierProductMapper.insert(record);
	}

	/**
	 * Insert manual order int.
	 *
	 * @param record the record
	 * @return the int
	 */
	public int insertManualOrder(SellerOrderSupplierProduct record) {
		return sellerOrderSupplierProductMapper.insert(record);
	}

	/**
	 * Select by primary key seller order supplier product.
	 *
	 * @param id the id
	 * @return the seller order supplier product
	 */
	public SellerOrderSupplierProduct selectByPrimaryKey(String id) {
		return sellerOrderSupplierProductMapper.selectByPrimaryKey(id);
	}

	/**
	 * Update by primary key selective int.
	 *
	 * @param record the record
	 * @return the int
	 */
	public int updateByPrimaryKeySelective(SellerOrderSupplierProduct record) {
		return sellerOrderSupplierProductMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * Update by primary key int.
	 *
	 * @param record the record
	 * @return the int
	 */
	public int updateByPrimaryKey(SellerOrderSupplierProduct record) {
		return sellerOrderSupplierProductMapper.updateByPrimaryKey(record);
	}

	/**
	 * Select supplier product by order id list.
	 * 根据订单获取明细
	 * @param orderId the order id
	 * @return the list
	 */
	public List<SellerOrderSupplierProduct> selectSupplierProductByOrderId(String orderId) {
		return sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
	}

	/**
	 * Select supplier product by order id merge list.
	 * 根据订单获取明细，并且按照条形码合并
	 * @param orderId the order id
	 * @return the list
	 */
	public List<SellerOrderSupplierProduct> selectSupplierProductByOrderIdMerge(String orderId) {
		List<SellerOrderSupplierProduct> list = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
		List<SellerOrderSupplierProduct> returnList = new ArrayList<>();
		Map<String,SellerOrderSupplierProduct> productMap = new HashMap<>();
		Iterator it = list.iterator();
		while (it.hasNext()){
			SellerOrderSupplierProduct soItem = (SellerOrderSupplierProduct)it.next();
			if(soItem.getOrderNum()-soItem.getDeliveredNum()-soItem.getWaitDeliveryNum()<=0){
				it.remove();
			}else {
				if(productMap.containsKey(soItem.getBarcode())){
					SellerOrderSupplierProduct oldProduct = productMap.get(soItem.getBarcode());
					soItem.setOrderNum(soItem.getOrderNum()+oldProduct.getOrderNum());
					soItem.setDeliveredNum(soItem.getDeliveredNum()+oldProduct.getDeliveredNum());
					soItem.setWaitDeliveryNum(soItem.getWaitDeliveryNum()+oldProduct.getWaitDeliveryNum());
				}
				productMap.put(soItem.getBarcode(),soItem);
			}

		}
		for (Map.Entry<String,SellerOrderSupplierProduct> entry : productMap.entrySet()){
			returnList.add(entry.getValue());
		}
		return returnList;
	}
}
