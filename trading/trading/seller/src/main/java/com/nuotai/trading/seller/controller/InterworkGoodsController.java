package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.*;
import com.nuotai.trading.seller.dao.SellerCustomerItemMapper;
import com.nuotai.trading.seller.dao.SellerCustomerMapper;
import com.nuotai.trading.seller.model.SellerCustomer;
import com.nuotai.trading.seller.model.SellerCustomerItem;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.seller.model.SellerOrderExportData;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import com.nuotai.trading.seller.service.SellerDeliveryRecordService;
import com.nuotai.trading.seller.service.SellerLogisticsService;
import com.nuotai.trading.seller.service.SellerOrderService;
import com.nuotai.trading.seller.service.SellerOrderSupplierProductService;
import com.nuotai.trading.service.*;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * 互通客户购买的商品
 * @author gsf
 * @date 2017-08-01
 */

@Controller
@RequestMapping("platform/seller/interworkGoods")
public class InterworkGoodsController extends BaseController {
	@Autowired
	private SellerOrderService sellerOrderService;  //订单信息
	@Autowired
	private SellerOrderSupplierProductService orderProductService;  //订单商品信息

	@Autowired
	private BuyCompanyService buyCompanyService;  //公司信息
	
	@Autowired
	private SellerLogisticsService sellerLogisticsService;  //物流信息
	
	@Autowired
	private SellerOrderSupplierProductService sellerOrderSupplierProductService;  //订单互通商品信息
	
	@Autowired
	private SellerDeliveryRecordService delieryService;  //发货单信息

	@Autowired
	private DicProvincesService dicProvincesService; //省

	@Autowired
	private DicCitiesService dicCitiesService; //市

	@Autowired
	private DicAreasService dicAreasService; //区
	
	//售后订单
	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;
	@Autowired
	private SellerCustomerItemMapper sellerCustomerItemMapper;
	
	
	/**
	 * 加载页面
	 * @author gsf
	 * @date 2017-08-01
	 * @return String
	 */
	@RequestMapping("interworkGoodsList.html")
	public String buyOrderList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("tabId")||params.get("tabId")==null){
			params.put("tabId", "99");
		}
		//获得不同状态(互通客户购买商品)订单数
		sellerOrderService.getInterworkGoodsOrderNum(params);
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sellers/interwork/interworkGoodsList";
	}
	
	/**
	 * 获取数据
	 * @author gsf
	 * @date 2017-08-01
	 * @param searchPageUtil
	 * @param params
	 * @return String
	 */
	@RequestMapping(value = "interworkGoodsData.html", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil = sellerOrderService.loadInterworkGoodsData(searchPageUtil, params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("interest", params.containsKey("interest")?params.get("interest").toString():"0");
		return "platform/sellers/interwork/interworkGoodsData";
	}
	
	
	/**
	 * 订单详情（卖家）
	 * @author gsf
	 * @date 2017-08-03
	 * @param params
	 * @return String
	 */
	@RequestMapping(value = "interworkGoodsDetail.html", method = RequestMethod.GET)
	public String interworkGoodsDetail(@RequestParam Map<String,Object> params){
		boolean status = false;
		if(!ObjectUtil.isEmpty(params.get("orderCode"))){
			status = params.get("orderCode").toString().contains("SH");
			//status=true表示订单是售后
			if (status){
				//订单详情
				SellerCustomer customerBean = sellerCustomerMapper.getByCustomerCode(params.get("orderCode").toString());
				//送货地址
				DicProvinces province = new DicProvinces();
				if(!ObjectUtil.isEmpty(customerBean.getProvince())){
					province = dicProvincesService.getProvinceById(Integer.parseInt(customerBean.getProvince()));
				}
				DicCities city = new DicCities();
				if(!ObjectUtil.isEmpty(customerBean.getCity())){
					city = dicCitiesService.getCityById(customerBean.getCity());
				}
				DicAreas area = new DicAreas();
				if(!ObjectUtil.isEmpty(customerBean.getArea())){
					area = dicAreasService.getAreaById(customerBean.getArea());
				}
				customerBean.setProvinceName(province.getProvince());
				customerBean.setCityName(city.getCity());
				customerBean.setAreaName(area.getArea());
				//订单商品
				List<SellerCustomerItem> itemList = sellerCustomerItemMapper.selectItemByCustomerId(customerBean.getId());
				customerBean.setItemList(itemList);
				model.addAttribute("orderBean", customerBean);
				//商品总金额
				BigDecimal totalMoney = new BigDecimal(0.00);
				for(SellerCustomerItem product:itemList){
					totalMoney = totalMoney.add(ObjectUtil.isEmpty(product.getRepairPrice())?new BigDecimal(0.00):product.getRepairPrice());
				}
				model.addAttribute("totalMoney",totalMoney);
				model.addAttribute("otherMoney",0);
				//采购商
				BuyCompany buyerBean = buyCompanyService.selectByPrimaryKey(customerBean.getCompanyId());
				model.addAttribute("buyerBean", buyerBean);
			}else{
				//订单详情
				SellerOrder orderBean = sellerOrderService.getByOrderCode(params.get("orderCode").toString());
				//送货地址
				DicProvinces province = new DicProvinces();
				if(!ObjectUtil.isEmpty(orderBean.getProvince())){
					province = dicProvincesService.getProvinceById(Integer.parseInt(orderBean.getProvince()));
				}
				DicCities city = new DicCities();
				if(!ObjectUtil.isEmpty(orderBean.getCity())){
					city = dicCitiesService.getCityById(orderBean.getCity());
				}
				DicAreas area = new DicAreas();
				if(!ObjectUtil.isEmpty(orderBean.getArea())){
					area = dicAreasService.getAreaById(orderBean.getArea());
				}
				orderBean.setProvinceName(province.getProvince());
				orderBean.setCityName(city.getCity());
				orderBean.setAreaName(area.getArea());
				//订单商品
				List<SellerOrderSupplierProduct> orderProductList = orderProductService.selectSupplierProductByOrderId(orderBean.getId());
				orderBean.setSupplierProductList(orderProductList);
				model.addAttribute("orderBean", orderBean);
				//商品总金额
				BigDecimal totalMoney = new BigDecimal(0.00);
				for(SellerOrderSupplierProduct product:orderProductList){
					totalMoney = totalMoney.add(ObjectUtil.isEmpty(product.getTotalMoney())?new BigDecimal(0.00):product.getTotalMoney());
				}
				model.addAttribute("totalMoney",totalMoney);
				model.addAttribute("otherMoney",0);
				//采购商
				BuyCompany buyerBean = buyCompanyService.selectByPrimaryKey(orderBean.getCompanyId());
				model.addAttribute("buyerBean", buyerBean);
			}
		
		}
		String returnUrl = (String) params.get("returnUrl");
		String menuId = (String) params.get("menuId");
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("menuId", menuId);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		if (status){
			return "platform/sellers/interwork/customerGoodsDetail";
		}
		return "platform/sellers/interwork/interworkGoodsDetail";
		
	}

	/**
	 * 卖家接单
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "approveLinkOrder", method = RequestMethod.POST)
	@ResponseBody
	public String approveLinkOrder(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			String orderId = String.valueOf(params.get("orderId"));
			sellerOrderService.approveLinkOrder(orderId);
			json.put("success", true);
			json.put("msg", "审批成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "审批失败！");
		}
		return json.toString();
	}
	
	/**
	 * 加载客户换货代发货订单页面（卖家）
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("exchangeGoodsList")
	public String exchangeGoodsList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil = sellerOrderService.loadExchangeGoodsData(searchPageUtil, params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sellers/interwork/exchangeGoodsList";
	}


	/**
	 * Cancel link order string.
	 * 卖家驳回订单
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping(value = "cancelLinkOrder", method = RequestMethod.POST)
	@ResponseBody
	public String cancelLinkOrder(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			sellerOrderService.cancelLinkOrder(params);
			json.put("success", true);
			json.put("msg", "操作成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "操作失败！");
		}
		return json.toString();
	}
	
	/**
	 * Export order.
	 * 订单导出
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportOrder")
	public void exportOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<SellerOrderExportData> exportDataList = sellerOrderService.getExportData(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		normalStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		normalStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		normalStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		normalStyle.setBorderTop(BorderStyle.THIN);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"采购单号","订单日期","采购商",
				"商品货号","商品名称","规格名称","条形码","采购数量","采购未到货数量","采购单价","采购金额","采购要求到货日期",
				"发货单号","发货数量","到货数量","最近到货日期","采购商留言","商品备注"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i=0;i<exportDataList.size();i++) {
				SellerOrderExportData data = exportDataList.get(i);
				//因为第0行已经设置标题了,所以从行2开始写入数据
				row=sheet.createRow(i+1);
				//1.采购单号
				cell = row.createCell(0);
				cell.setCellValue(data.getOrderCode());
				cell.setCellStyle(normalStyle);
				//2.订单日期
				cell = row.createCell(1);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
				cell.setCellStyle(normalStyle);
				//3.采购商
				cell = row.createCell(2);
				cell.setCellValue(data.getBuyerName());
				cell.setCellStyle(normalStyle);
				//4.商品货号
				cell = row.createCell(3);
				cell.setCellValue(data.getProductCode());
				cell.setCellStyle(normalStyle);
				//5.商品名称
				cell = row.createCell(4);
				cell.setCellValue(data.getProductName());
				cell.setCellStyle(normalStyle);
				//6.规格名称
				cell = row.createCell(5);
				cell.setCellValue(data.getSkuName());
				cell.setCellStyle(normalStyle);
				//7.条形码
				cell = row.createCell(6);
				cell.setCellValue(data.getBarcode());
				cell.setCellStyle(normalStyle);
				//8.采购数量
				cell = row.createCell(7);
				cell.setCellValue(data.getOrderNum());
				cell.setCellStyle(normalStyle);
				//9.采购未到货数量
				cell = row.createCell(8);
				cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
				cell.setCellStyle(normalStyle);
				//10.采购单价
				cell = row.createCell(9);
				cell.setCellValue(fnum.format(data.getPrice()));
				cell.setCellStyle(normalStyle);
				//11.采购金额
				cell = row.createCell(10);
				cell.setCellValue(fnum.format(data.getTotalPrice()));
				cell.setCellStyle(normalStyle);
				//12.采购要求到货日期
				cell = row.createCell(11);
				cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
				cell.setCellStyle(normalStyle);
				if(!ObjectUtil.isEmpty(data.getDeliverNo())){
					//13.发货单号
					cell = row.createCell(12);
					cell.setCellValue(data.getDeliverNo());
					cell.setCellStyle(normalStyle);
					//14.发货数量
					cell = row.createCell(13);
					cell.setCellValue(ObjectUtil.isEmpty(data.getDeliveryNum())?0:data.getDeliveryNum());
					cell.setCellStyle(normalStyle);
					if(!ObjectUtil.isEmpty(data.getArrivalDate())){
						//15.到货数量
						cell = row.createCell(14);
						cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//16.到货日期
						cell = row.createCell(15);
						cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalDate())?"":sdf.format(data.getArrivalDate()));
						cell.setCellStyle(normalStyle);
					}else{
						//15.到货数量
						cell = row.createCell(14);
						cell.setCellValue("未到货");
						cell.setCellStyle(normalStyle);
						//16.到货日期
						cell = row.createCell(15);
						cell.setCellValue("未到货");
						cell.setCellStyle(normalStyle);
					}
				}else{
					//13.发货单号
					cell = row.createCell(12);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//14.发货数量
					cell = row.createCell(13);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//15.到货数量
					cell = row.createCell(14);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//16.到货日期
					cell = row.createCell(15);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
				}
				//17.采购商留言
				cell = row.createCell(16);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderRemark())?"":data.getOrderRemark());
				cell.setCellStyle(normalStyle);
				//18.商品备注
				cell = row.createCell(17);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderItemRemark())?"":data.getOrderItemRemark());
				cell.setCellStyle(normalStyle);
			}
		}
		ExcelUtil.preExport("订单导出", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * Export not arrival order.
	 * 未到货订单导出
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportNotArrivalOrder")
	public void exportNotArrivalOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<SellerOrderExportData> exportDataList = sellerOrderService.getNotArrivalExportData(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("未到货订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);

		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		normalStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		normalStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		normalStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		normalStyle.setBorderTop(BorderStyle.THIN);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"采购单号","产品名称",
				"货号","条形码","规格名称","采购单价","采购数量","实际到货数量","未到货数量","未到货金额","采购商",
				"采购日期","要求到货日期","采购商留言","商品备注"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i=0;i<exportDataList.size();i++) {
				SellerOrderExportData data = exportDataList.get(i);
				//因为第0行已经设置标题了,所以从行2开始写入数据
				row=sheet.createRow(i+1);
				//1.采购单号
				cell = row.createCell(0);
				cell.setCellValue(data.getOrderCode());
				cell.setCellStyle(normalStyle);
				//2.产品名称
				cell = row.createCell(1);
				cell.setCellValue(data.getProductName());
				cell.setCellStyle(normalStyle);
				//3.货号
				cell = row.createCell(2);
				cell.setCellValue(data.getProductCode());
				cell.setCellStyle(normalStyle);
				//4.条形码
				cell = row.createCell(3);
				cell.setCellValue(data.getBarcode());
				cell.setCellStyle(normalStyle);
				//5.规格名称
				cell = row.createCell(4);
				cell.setCellValue(data.getSkuName());
				cell.setCellStyle(normalStyle);
				//6.采购单价
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(data.getPrice()));
				cell.setCellStyle(normalStyle);
				//7.采购数量
				cell = row.createCell(6);
				cell.setCellValue(data.getOrderNum());
				cell.setCellStyle(normalStyle);
				//8.实际到货数量
				cell = row.createCell(7);
				cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
				cell.setCellStyle(normalStyle);
				//9.未到货数量
				cell = row.createCell(8);
				cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
				cell.setCellStyle(normalStyle);
				//10.未到货金额
				cell = row.createCell(9);
				cell.setCellValue(fnum.format(data.getUnArrivalTotalPrice()));
				cell.setCellStyle(normalStyle);
				//11.采购商
				cell = row.createCell(10);
				cell.setCellValue(data.getBuyerName());
				cell.setCellStyle(normalStyle);
				//12.采购日期
				cell = row.createCell(11);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
				cell.setCellStyle(normalStyle);
				//13.要求到货日期
				cell = row.createCell(12);
				cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
				cell.setCellStyle(normalStyle);
				//14.訂單商品备注
				cell = row.createCell(13);
				cell.setCellValue(data.getOrderRemark());
				cell.setCellStyle(normalStyle);
				cell = row.createCell(14);
				cell.setCellValue(data.getOrderItemRemark());
				cell.setCellStyle(normalStyle);
			}
		}
		ExcelUtil.preExport("未到货订单", response);
		ExcelUtil.export(wb, response);
	}
}
