package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerManualOrder;

/**
 * 
 * 
 * @author "
 * @date 2017-08-21 09:17:46
 */
public interface SellerManualOrderMapper extends BaseDao<SellerManualOrder> {
	
	/**
	 * 保存手工添加订单
	 * @param sellerOrder
	 */
	void insertSellerManualOrder(SellerManualOrder sellerManualOrder);
	
	SellerManualOrder selectByOrderId(String orderId);
}
