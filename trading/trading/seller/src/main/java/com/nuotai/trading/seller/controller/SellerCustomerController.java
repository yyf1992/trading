package com.nuotai.trading.seller.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.SellerCustomer;
import com.nuotai.trading.seller.model.SellerCustomerItem;
import com.nuotai.trading.seller.service.SellerCustomerItemService;
import com.nuotai.trading.seller.service.SellerCustomerService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 售后服务
 * @author dxl
 *
 */
@Controller
@RequestMapping("seller/customer")
public class SellerCustomerController extends BaseController {
	@Autowired
	private SellerCustomerService customerService;
	@Autowired
	private SellerCustomerItemService customerItemService;
	/**
	 * 互通售后服务管理
	 */
	@RequestMapping("customerList")
	public String customerList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "99");
		}
		//获得不同状态数量
		customerService.getCustomerListNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sellers/customer/customerList";
	}
	
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = customerService.loadCustomerData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 获取创建发货单页面
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "loadCreateDeliveryHtml", method = RequestMethod.GET)
	public String loadCreateDeliveryHtml(String customerId){
		SellerCustomer sellerCustomer = customerService.get(customerId);
		List<SellerCustomerItem> itemResultList = new ArrayList<>();
		List<SellerCustomerItem>  itemList= customerItemService.getItemByCustomerId(customerId);
		if(itemList != null && !itemList.isEmpty()){
			for(SellerCustomerItem sci : itemList){
				int confirmNumber = sci.getConfirmNumber();
				int deliverNum = sci.getDeliverNum();
				if(confirmNumber > deliverNum && "0".equals(sci.getType())){
					itemResultList.add(sci);
				}
			}
		}
		model.addAttribute("itemList", itemResultList);
		model.addAttribute("sellerCustomer", sellerCustomer);
		return "platform/sellers/customer/singleSendGoods";
	}
	
	/**
	 * 查看售后明细页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadCustomerDetails")
	public String loadCustomerDetails(String id) throws Exception {
		SellerCustomer customer = customerService.get(id);
		model.addAttribute("customer", customer);
		List<SellerCustomerItem> itemList = customerItemService.selectCustomerItemByCustomerId(id);
		model.addAttribute("itemList", itemList);
		return "platform/sellers/customer/customerDetails";
	}
	
	/**
	 * 加载审核页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loadVerifyCustomer", method = RequestMethod.POST)
	public String loadVerifyCustomer(String id) throws Exception {
		model.addAttribute("id", id);
		return "platform/sellers/customer/verifyCustomer";
	}
	
	/**
	 * 同意售后
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveAgreeCustomer", method = RequestMethod.POST)
	@ResponseBody
	public String saveAgreeCustomer(String id){
		JSONObject json = new JSONObject();
		try {
			customerService.saveAgreeCustomer(id);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 确认售后数量
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveVerifyCustomer", method = RequestMethod.POST)
	@ResponseBody
	public String saveVerifyCustomer(@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			customerService.saveVerifyCustomer(map);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
}
