package com.nuotai.trading.seller.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.utils.ShiroUtils;

@Controller
@RequestMapping("platform/seller/supplier")
public class SellerSupplierController extends BaseController {
	@Autowired
	private SellerOrderSupplierProductMapper productMapper;
	
	/**
	 * 加载供应商到货及时率界面
	 * @return
	 */
	@RequestMapping(value="getArrivalRateHtml")
	public String getArrivalRate(@RequestParam Map<String,Object> map){
		map.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(map.get("startDate")==null && map.get("endDate")==null){
			map.put("startDate",weekAgo);
			map.put("endDate",now);
		}
		Map<String,net.sf.json.JSONObject> arrivalList=getSupplierArrivalDate(map);
		model.addAttribute("map",map);
		model.addAttribute("arrivalList",net.sf.json.JSONObject.fromObject(arrivalList));
		return "platform/sellers/supplier/supplierArrivalRate";
	}

	/**
	 * 加载计划完成率界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getComplitionRateHtml")
	public String getComplitionRate(@RequestParam Map<String, Object> map) {
		map.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if (map.get("startDate") == null && map.get("endDate") == null) {
			map.put("startDate", weekAgo);
			map.put("endDate", now);
		}
		List<Map<String, Object>> complitionList = productMapper.getCompletionRate(map);
		model.addAttribute("map", map);
		model.addAttribute("complitionList",net.sf.json.JSONArray.fromObject(complitionList));
		return "platform/sellers/supplier/supplierCompletionRate";
	}
	
	/**
	 * 及时率数据
	 * @param map
	 * @return
	 */
	public Map<String,net.sf.json.JSONObject> getSupplierArrivalDate(Map<String,Object> map){
		List<Map<String,Object>> supplierList=productMapper.getArrivalRate(map);
		Map<String,net.sf.json.JSONObject> suppMap =new LinkedMap<String,net.sf.json.JSONObject>();
		for (Map<String, Object> supplier : supplierList) {
			String companyId = supplier.get("company_id").toString();
			String companyName = supplier.get("company_name").toString();
			String predictArred = supplier.containsKey("predict_arred") ? supplier.get("predict_arred").toString() : "";//要求到货时间
			String arrivalDate = supplier.containsKey("arrival_date") ? supplier.get("arrival_date").toString() : "";//到货时间
			// 汇总过得数据
			if (suppMap.containsKey(companyId)) {
				net.sf.json.JSONObject old = suppMap.get(companyId);
				old.put("orderSum",old.getInt("orderSum")+1);
				if(arrivalDate.isEmpty()){//未到货
					old.put("failNum",old.getInt("failNum")+1);
				}else{//已到货
					if(predictArred.compareTo(arrivalDate)>=0){
						old.put("successNum",old.getInt("successNum")+1);
					}else{
						old.put("failNum",old.getInt("failNum")+1);
					}
				}
				suppMap.put(companyId, old);
			} else {// 没有汇总过得数据
				net.sf.json.JSONObject newJson = new net.sf.json.JSONObject();
				newJson.put("company_name",companyName);
				newJson.put("supp_id",companyId);
				newJson.put("orderSum",1);
				newJson.put("failNum",0);
				newJson.put("successNum",0);
				if(arrivalDate.isEmpty()){//未到货
					newJson.put("failNum",1);
				}else{//已到货
					if(predictArred.compareTo(arrivalDate)>=0){
						newJson.put("successNum",1);
					}else{
						newJson.put("failNum",1);
					}
				}
				suppMap.put(companyId, newJson);
			}
		}
		return suppMap;
	}
}
