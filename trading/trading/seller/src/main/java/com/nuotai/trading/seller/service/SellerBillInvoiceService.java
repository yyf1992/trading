package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.*;
import com.nuotai.trading.seller.dao.buyer.SBuyBillInvoiceCommodityMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyBillInvoiceMapper;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.SBuyBillInvoice;
import com.nuotai.trading.seller.model.buyer.SBuyBillInvoiceCommodity;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SellerBillInvoiceService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillInvoiceService.class);

	@Autowired
	private SellerBillInvoiceMapper sellerBillInvoiceMapper;
	@Autowired
	private SBuyBillInvoiceMapper sBuyBillInvoiceMapper;
	@Autowired
	private SBuyBillInvoiceCommodityMapper sBuyBillInvoiceCommodityMapper;
	@Autowired
	private SellerBillReconciliationMapper sellerBillReconciliationMapper;
	@Autowired
	private SellerBillReconciliationItemMapper sellerBillReconciliationItemMapper;
	@Autowired
	private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;//发货信息查询
	@Autowired
	private SellerDeliveryRecordItemService sellerDeliveryRecordItemService;//发货明细
	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;//退货信息查询
	@Autowired
	private SellerCustomerItemService sellerCustomerItemService;//退货详情

	//卖家查询发货、退货信息到发票列表信息
	/*public List<Map<String, Object>> getCommodityBatchList(
			SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> commodityBatchList = new ArrayList<>();//合并返回的集合
		//List<Map<String, Object>> deliveryBatchList = new ArrayList<>();//发货返回的集合
		//List<Map<String, Object>> customerBatchList = new ArrayList<>();//退货返回的集合
		//Map<String,Object> deliveryAndCustomerMap = new HashMap<String,Object>();
		Map<String,Object> deliveryMap = new HashMap<String,Object>();
		deliveryMap.put("billDealStatus","3");//已经对账

		List<SellerBillReconciliation> deliveryRecList = sellerBillReconciliationMapper.queryRecByDealStatus(deliveryMap);
		if(null != deliveryRecList && deliveryRecList.size()>0){
			for(SellerBillReconciliation deliveryRec : deliveryRecList){
				String reconciliationId = deliveryRec.getId();
				Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
				deliveryItemMap.put("reconciliationId",reconciliationId);//已经对账
				deliveryItemMap.put("isNeedInvoice","Y");
				deliveryItemMap.put("isInvoiceStatus","2");
				List<SellerBillReconciliationItem> deliveryRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(deliveryItemMap);
				for(se){}

			}
		}



		if(null != sellerDeliveryRecord){
			List<Map<String, Object>> deliveryItemBatchList = new ArrayList<>();//发货详情返回的集合
			Map<String,Object> deliveryMap = new HashMap<String,Object>();//发货数据集合
			String recordId = sellerDeliveryRecord.getId();
			String deliveryNo = sellerDeliveryRecord.getDeliverNo();//批次号
			String buycompanyId = sellerDeliveryRecord.getBuycompanyId();//客户编号
			String buycompanyName = sellerDeliveryRecord.getBuycompanyName();//客户名称
			String arrivalDateStr = sellerDeliveryRecord.getArrivalDateStr();//账单完结时间
			String isInvoiceStatus = sellerDeliveryRecord.getIsInvoiceStatus();//发票确认状态

			Map<String,Object> queryItemMap = new HashMap<String,Object>();
			queryItemMap.put("recordId",recordId);
			List<SellerDeliveryRecordItem> sellerDeliveryRecordItem = sellerDeliveryRecordItemService.queryDeliveryRecordItem(queryItemMap);

			for(SellerDeliveryRecordItem sellerDeliveryRecordItemInfo : sellerDeliveryRecordItem){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				int arrivalNum = sellerDeliveryRecordItemInfo.getArrivalNum();//数量
				BigDecimal salePrice = sellerDeliveryRecordItemInfo.getSalePrice();//单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

				itemMap.put("sellerDeliveryRecordItemInfo",sellerDeliveryRecordItemInfo);
				itemMap.put("salePriceSum",salePriceSum);
				deliveryItemBatchList.add(itemMap);
			}
			deliveryMap.put("commodityFlag",recordId+","+"1");//1 发货数据，2 退货数据
			deliveryMap.put("typeFlag","1");
			deliveryMap.put("recordId",recordId);
			deliveryMap.put("deliveryNo",deliveryNo);
			deliveryMap.put("buycompanyId",buycompanyId);
			deliveryMap.put("buycompanyName",buycompanyName);
			deliveryMap.put("arrivalDateStr",arrivalDateStr);
			deliveryMap.put("isInvoiceStatus",isInvoiceStatus);
			deliveryMap.put("deliveryItemBatchList",deliveryItemBatchList);

			commodityBatchList.add(deliveryMap);
		}

		commodityMap.put("isInvoiceStatus","2");//已经开票
		searchPageUtil.setObject(commodityMap);
		//发货拼装
		List<SellerDeliveryRecord> deliveryRecordList = sellerDeliveryRecordMapper.queryDeliveryListByInvoice(searchPageUtil);
		if(deliveryRecordList.size()>0){
			for(SellerDeliveryRecord sellerDeliveryRecord : deliveryRecordList){
				commodityBatchList = queryDeliveryItemList(sellerDeliveryRecord,commodityBatchList);
			}
		}
		//退货拼装
		List<SellerCustomer> customerList = sellerCustomerMapper.queryCustomerByInvoice(searchPageUtil);
		if(customerList.size()>0){
			for(SellerCustomer sellerCustomer : customerList){
				commodityBatchList = queryCustomerItemList(sellerCustomer,commodityBatchList);
			}
		}

		//组装发货退货
		//deliveryAndCustomerMap.put("deliveryBatchList",deliveryBatchList);
		//deliveryAndCustomerMap.put("customerBatchList",customerBatchList);
		//commodityBatchList.add(deliveryAndCustomerMap);
		return commodityBatchList;
	}*/

	//待开票发票列表查询
	public List<Map<String, Object>> getCommodityBatchList(SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> deliveryItemBatchList = new ArrayList<>();//发货详情返回的集合
		List<SellerBillReconciliationItem> deliveryRecItemList = sellerBillReconciliationItemMapper.queryRecDecliveryItemList(searchPageUtil);
		for(SellerBillReconciliationItem deliveryRecItem : deliveryRecItemList){
			Map<String,Object> itemMap = new HashMap<String,Object>();
			String isUpdateSale = deliveryRecItem.getIsUpdateSale();
			BigDecimal salePrice = new BigDecimal(0);//单价
			if("1".equals(isUpdateSale)){
				salePrice = deliveryRecItem.getUpdateSalePrice();//单价
			}else {
				salePrice = deliveryRecItem.getSalePrice();//单价
			}
			int arrivalNum = deliveryRecItem.getArrivalNum();//数量

			BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
			BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

			itemMap.put("deliveryRecItem",deliveryRecItem);
			itemMap.put("salePrice",salePrice);
			itemMap.put("salePriceSum",salePriceSum);
			deliveryItemBatchList.add(itemMap);
		}
		return deliveryItemBatchList;
	}

	//待开票发货数据导出列表查询
	public List<Map<String, Object>> getCommodityBatchExport(Map<String, Object> paramMap) {
		List<Map<String, Object>> deliveryItemBatchList = new ArrayList<>();//发货详情返回的集合
				Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
		       /* Map<String,Object>   searchPageUtilMap = (Map<String, Object>) searchPageUtil.getObject();
		        //deliveryItemMap.putAll(searchPageUtilMap);
				deliveryItemMap.put("recordId",searchPageUtilMap.get("recordId").toString());//已经对账
		deliveryItemMap.put("buyCompanyName",searchPageUtilMap.get("buyCompanyName").toString());//已经对账
		deliveryItemMap.put("productName",searchPageUtilMap.get("productName").toString());//已经对账
		deliveryItemMap.put("orderId",searchPageUtilMap.get("orderId").toString());//已经对账
		deliveryItemMap.put("startArrivalDate",searchPageUtilMap.get("startArrivalDate").toString());//已经对账
		deliveryItemMap.put("endArrivalDate",searchPageUtilMap.get("endArrivalDate").toString());//已经对账*/

				deliveryItemMap.put("isNeedInvoice","Y");
		        //deliveryItemMap.put("isInvoiceStatus","'"+"1"+"'"+","+"'"+"2"+"'");
				deliveryItemMap.put("isInvoiceStatus1","1");
		        deliveryItemMap.put("isInvoiceStatus2","2");
				deliveryItemMap.put("reconciliationDealStatus","3");
		paramMap.putAll(deliveryItemMap);
				//searchPageUtil.setObject(deliveryItemMap);


				List<SellerBillReconciliationItem> deliveryRecItemList = sellerBillReconciliationItemMapper.queryRecDecliveryItemExport(paramMap);
				for(SellerBillReconciliationItem deliveryRecItem : deliveryRecItemList){
					Map<String,Object> itemMap = new HashMap<String,Object>();
					String isUpdateSale = deliveryRecItem.getIsUpdateSale();
					BigDecimal salePrice = new BigDecimal(0);//单价
					if("1".equals(isUpdateSale)){
						salePrice = deliveryRecItem.getUpdateSalePrice();//单价
					}else {
						salePrice = deliveryRecItem.getSalePrice();//单价
					}
					int arrivalNum = deliveryRecItem.getArrivalNum();//数量

					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

					itemMap.put("deliveryRecItem",deliveryRecItem);
					/*itemMap.put("reconciliationId",reconciliationId);
					itemMap.put("buycompanyName",buycompanyName);*/
					itemMap.put("salePrice",salePrice);
					itemMap.put("salePriceSum",salePriceSum);
					deliveryItemBatchList.add(itemMap);
				}
			//}
		//}
		return deliveryItemBatchList;
	}

	//卖家查询发票列表信息
	public List<Map<String, Object>> getInvoiceList(
			SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> billInvoiceList = sellerBillInvoiceMapper.getBillInvoiceList(searchPageUtil);
		return billInvoiceList;
	}

	//根据审批状态查询数量
	public int getInvoiceCountByStatus(Map<String, Object> map) {
		return sellerBillInvoiceMapper.getInvoiceCountByStatus(map);
	}
	//查询不同状态下数量
	public void queryInvoiceCountByStatus(Map<String, Object> invoiceMap){
		//查询所有状态数量
		invoiceMap.put("billInvoiceStatus", "0");
		//invoiceMap.put("reconciliationDealStatus", "3");
		//int deliveryCount = sellerDeliveryRecordMapper.queryCountByInvoice(invoiceMap);
		//int customerCount = sellerCustomerMapper.queryCountByInvoice(invoiceMap);
		int deliveryCount = sellerBillReconciliationItemMapper.queryCountByInvoice(invoiceMap);

		//查询待对方确认
		invoiceMap.put("billInvoiceStatus", "1");
		int approvalBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//查询对方已确认
		invoiceMap.put("billInvoiceStatus", "2");
		int acceptBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//查询已驳回
		invoiceMap.put("billInvoiceStatus", "3");
		int stopBillCycleCount = getInvoiceCountByStatus(invoiceMap);
		//取消票据 closeBillCycleCount
		invoiceMap.put("billInvoiceStatus", "4");
		int closeBillCycleCount = getInvoiceCountByStatus(invoiceMap);

		invoiceMap.put("noConfirmInvoiceCount", deliveryCount);
		invoiceMap.put("approvalBillCycleCount", approvalBillCycleCount);
		invoiceMap.put("acceptBillCycleCount", acceptBillCycleCount);
		invoiceMap.put("stopBillCycleCount", stopBillCycleCount);
		invoiceMap.put("closeBillCycleCount", closeBillCycleCount);

	}

	public List<SellerBillInvoice> queryBillInvoiceList(Map<String,Object> invoiceMap){
		return sellerBillInvoiceMapper.queryBillInvoiceList(invoiceMap);
	}
	//回显选中的退发货批次发票信息
	public Map<String,Object> drawUpInvoiceToCommodity(Map<String,Object> recItemIdMap){
		Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
		List<Map<String,Object>> deliveryItemList = new ArrayList<>();//合并返回的集合
		String deliveryItemIds = recItemIdMap.get("deliveryItemIds").toString();
		String deliveryItemId = "";

		if(null != deliveryItemIds && !"".equals(deliveryItemIds)){
			String[] deliveryItemIdList = deliveryItemIds.split(",");
			int count = deliveryItemIdList.length;
			Map<String, Object> itemIdsMap = new HashMap<String,Object>();
			if(deliveryItemIdList.length > 1){
				deliveryItemIds = deliveryItemIds.replaceAll(",","','");
				deliveryItemIds = "'"+deliveryItemIds+"'";
				itemIdsMap.put("deliveryItemIds",deliveryItemIds);
			}else {
				deliveryItemId = deliveryItemIds;
				itemIdsMap.put("deliveryItemId",deliveryItemId);
			}
			itemIdsMap.put("isInvoiceStatus",'0');
			List<SellerBillReconciliationItem> deliveryRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(itemIdsMap);
			BigDecimal priceSum = new BigDecimal(0);
			for(SellerBillReconciliationItem deliveryRecItem : deliveryRecItemList){

				String deliverNo = deliveryRecItem.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString()+",";

					}
					attachmentAddrStr = attachmentAddrStr.substring(0,attachmentAddrStr.length()-1);
				}

				String isUpdateSale = deliveryRecItem.getIsUpdateSale();
				Map<String,Object> itemMap = new HashMap<String,Object>();
				int arrivalNum = deliveryRecItem.getArrivalNum();//数量
				BigDecimal salePrice = new BigDecimal(0);//单价
				if("1".equals(isUpdateSale)){
					salePrice = deliveryRecItem.getUpdateSalePrice();
				}else {
					salePrice = deliveryRecItem.getSalePrice();
				}
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额
				priceSum = priceSum.add(salePriceSum);//发票商品总额

				itemMap.put("deliveryRecItem",deliveryRecItem);
				itemMap.put("attachmentAddrStr",attachmentAddrStr);
					/*itemMap.put("reconciliationId",reconciliationId);
					itemMap.put("buycompanyName",buycompanyName);*/
				itemMap.put("salePriceSum",salePriceSum);
				deliveryItemList.add(itemMap);
			}
			deliveryItemMap.put("priceSum",priceSum);
			deliveryItemMap.put("deliveryItemList",deliveryItemList);
		}

		return deliveryItemMap;
	}

	//添加
	public int saveInvoiceInfo(Map<String,Object> saveInvoiceMap){
		int saveInvoiceCount = 0;
		if(null != saveInvoiceMap && saveInvoiceMap.size()>0){
			String invoiceId  = ShiroUtils.getUid();
			String settlementNo = ShiroUtils.getUid();
			String buyCompanyIds	= saveInvoiceMap.get("buyCompanyIds").toString().trim();
			String[] buyCompanyIdList = buyCompanyIds.split(",");
			String buyCompanyId = "";
			for(int i = 0;i<buyCompanyIdList.length;i++){
				if(null != buyCompanyIdList[i].toString() && !"".equals(buyCompanyIdList[i].toString())){
					buyCompanyId = buyCompanyIdList[i].toString();
				}
			}
			//String buyCompanyName	= saveInvoiceMap.get("buyCompanyName").toString();
			BigDecimal salePriceSum	= new BigDecimal(saveInvoiceMap.get("salePriceSum").toString());
			String invoiceType	= saveInvoiceMap.get("invoiceType").toString();
			String invoiceHeader	= saveInvoiceMap.get("invoiceHeader").toString();
			String totalInvoiceValue	= saveInvoiceMap.get("totalInvoiceValue").toString();
			String invoiceNo	= saveInvoiceMap.get("invoiceNo").toString();
			String invoiceFileAddr = saveInvoiceMap.get("invoiceFileAddr").toString();
			String recItemIds = saveInvoiceMap.get("recItemIdList").toString();

			/*String[] salePriceSumList = salePriceSum.split(",");
			BigDecimal totalCommodityAmount = new BigDecimal(0);
			for(String salePriceSums : salePriceSumList){
				BigDecimal salePriceSums1 = new BigDecimal(salePriceSums);
				totalCommodityAmount = totalCommodityAmount.add(salePriceSums1);
			}*/
			BigDecimal totalInvoiceValues = new BigDecimal(totalInvoiceValue);

			SellerBillInvoice sellerBillInvoice = new SellerBillInvoice();
			sellerBillInvoice.setId(invoiceId);
			sellerBillInvoice.setSettlementNo(settlementNo);
			sellerBillInvoice.setCompanyId(ShiroUtils.getCompId());
			sellerBillInvoice.setBuyerCompanyId(buyCompanyId);
			sellerBillInvoice.setInvoiceNo(invoiceNo);
			sellerBillInvoice.setInvoiceFileAddr(invoiceFileAddr);
			sellerBillInvoice.setInvoiceType(invoiceType);
			sellerBillInvoice.setInvoiceHeader(invoiceHeader);
			sellerBillInvoice.setTotalCommodityAmount(salePriceSum);
			sellerBillInvoice.setTotalInvoiceValue(totalInvoiceValues);
			sellerBillInvoice.setAcceptInvoiceStatus("1");
			Date createDate = new Date();//创建时间
			sellerBillInvoice.setCreateDate(createDate);

			saveInvoiceCount = sellerBillInvoiceMapper.addInvoiceInfo(sellerBillInvoice);
			if(saveInvoiceCount > 0){
				//同步到买家模块
				SBuyBillInvoice sBuyBillInvoice = new SBuyBillInvoice();
				sBuyBillInvoice.setId(invoiceId);
				sBuyBillInvoice.setSettlementNo(settlementNo);
				sBuyBillInvoice.setCompanyId(buyCompanyId);
				sBuyBillInvoice.setSupplierId(ShiroUtils.getCompId());
				sBuyBillInvoice.setInvoiceNo(invoiceNo);
				sBuyBillInvoice.setInvoiceFileAddr(invoiceFileAddr);
				sBuyBillInvoice.setInvoiceType(invoiceType);
				sBuyBillInvoice.setInvoiceHeader(invoiceHeader);
				sBuyBillInvoice.setTotalCommodityAmount(salePriceSum);
				sBuyBillInvoice.setTotalInvoiceValue(totalInvoiceValues);
				sBuyBillInvoice.setAcceptInvoiceStatus("1");
				sBuyBillInvoice.setCreateDate(createDate);
				sBuyBillInvoiceMapper.addBuyInvoice(sBuyBillInvoice);
				//添加发票关联
				String[] recItemIdList = recItemIds.split(",");
				for(String recItemId : recItemIdList){
					if(null != recItemIdList &&!"".equals(recItemIdList)){
						SBuyBillInvoiceCommodity sBuyBillInvoiceCommodity = new SBuyBillInvoiceCommodity();
						sBuyBillInvoiceCommodity.setId(ShiroUtils.getUid());
						sBuyBillInvoiceCommodity.setInvoiceId(invoiceId);
						sBuyBillInvoiceCommodity.setReconciliationItemId(recItemId);
						sBuyBillInvoiceCommodity.setAcceptInvoiceStatus("1");
						sBuyBillInvoiceCommodityMapper.addInvoiceToRecItem(sBuyBillInvoiceCommodity);

						//同步对账详情状态
						Map<String,Object> map = new HashMap<String,Object>();
						map.put("id",recItemId);
						//map.put("isInvoiceStatus",billDealStatus);
						map.put("isInvoiceStatus","1");
						sellerBillReconciliationItemMapper.updateRecItemPrice(map);
					}
				}
			}
		}
		return saveInvoiceCount;
	}

	//查询修改发票回显
	public Map<String,Object> queryUpdateInvoice(String invoiceId){
		Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
		List<Map<String,Object>> deliveryItemList = new ArrayList<>();//合并返回的集合
		Map<String,Object> invoiceMap = new HashMap<String,Object>();
		invoiceMap.put("invoiceId",invoiceId);
		SellerBillInvoice sellerBillInvoice = sellerBillInvoiceMapper.queryInvoiceById(invoiceMap);
		deliveryItemMap.put("sellerBillInvoice",sellerBillInvoice);
		//根据发票编号查询发票详情
		List<SBuyBillInvoiceCommodity> invoiceItemlist = sBuyBillInvoiceCommodityMapper.queryInvoiceItemList(invoiceMap);
		BigDecimal priceSum = new BigDecimal(0);
		if(null != invoiceItemlist && invoiceItemlist.size()>0){
			for(SBuyBillInvoiceCommodity invoiceItem: invoiceItemlist){
				String recItemId = invoiceItem.getReconciliationItemId();
				Map<String,Object> recItemMap = new HashMap<String,Object>();
				recItemMap.put("deliveryItemId",recItemId);
				SellerBillReconciliationItem deliveryRecItem = sellerBillReconciliationItemMapper.queryRecItemBuyId(recItemMap);

				String deliverNo = deliveryRecItem.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}

				String isUpdateSale = deliveryRecItem.getIsUpdateSale();
				Map<String,Object> itemMap = new HashMap<String,Object>();
				int arrivalNum = deliveryRecItem.getArrivalNum();//数量
				BigDecimal salePrice = new BigDecimal(0);//单价
				if("1".equals(isUpdateSale)){
					salePrice = deliveryRecItem.getUpdateSalePrice();
				}else {
					salePrice = deliveryRecItem.getSalePrice();
				}
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额
				priceSum = priceSum.add(salePriceSum);//发票商品总额

				itemMap.put("deliveryRecItem",deliveryRecItem);
				itemMap.put("attachmentAddrStr",attachmentAddrStr);
					/*itemMap.put("reconciliationId",reconciliationId);
					itemMap.put("buycompanyName",buycompanyName);*/
				itemMap.put("salePriceSum",salePriceSum);
				deliveryItemList.add(itemMap);

			}
			deliveryItemMap.put("priceSum",priceSum);
			deliveryItemMap.put("deliveryItemList",deliveryItemList);
		}

		return deliveryItemMap;
	}

	//修改保存
	public int updateInvoiceInfo(Map<String,Object> saveInvoiceMap){
		int updateInvoiceCount = 0;
		if(null != saveInvoiceMap && saveInvoiceMap.size()>0){
			//String invoiceId  = ShiroUtils.getUid();
			//String settlementNo = ShiroUtils.getUid();
			/*String buyCompanyIds	= saveInvoiceMap.get("buyCompanyIds").toString().trim();
			String[] buyCompanyIdList = buyCompanyIds.split(",");
			String buyCompanyId = "";
			for(int i = 0;i<buyCompanyIdList.length;i++){
				if(null != buyCompanyIdList[i].toString() && !"".equals(buyCompanyIdList[i].toString())){
					buyCompanyId = buyCompanyIdList[i].toString();
				}
			}*/
			//String buyCompanyName	= saveInvoiceMap.get("buyCompanyName").toString();
			BigDecimal salePriceSum	= new BigDecimal(saveInvoiceMap.get("salePriceSum").toString());
			String invoiceId = saveInvoiceMap.get("invoiceId").toString();
			String invoiceType	= saveInvoiceMap.get("invoiceType").toString();
			String invoiceHeader	= saveInvoiceMap.get("invoiceHeader").toString();
			String totalInvoiceValue	= saveInvoiceMap.get("totalInvoiceValue").toString();
			String invoiceNo	= saveInvoiceMap.get("invoiceNo").toString();
			String invoiceFileAddr = saveInvoiceMap.get("invoiceFileAddr").toString();
			//String recItemIds = saveInvoiceMap.get("recItemIdList").toString();

			BigDecimal totalInvoiceValues = new BigDecimal(totalInvoiceValue);

			SellerBillInvoice sellerBillInvoice = new SellerBillInvoice();
			sellerBillInvoice.setId(invoiceId);
			//sellerBillInvoice.setSettlementNo(settlementNo);
			//sellerBillInvoice.setCompanyId(ShiroUtils.getCompId());
			//sellerBillInvoice.setBuyerCompanyId(buyCompanyId);
			sellerBillInvoice.setInvoiceNo(invoiceNo);
			sellerBillInvoice.setInvoiceFileAddr(invoiceFileAddr);
			sellerBillInvoice.setInvoiceType(invoiceType);
			sellerBillInvoice.setInvoiceHeader(invoiceHeader);
			sellerBillInvoice.setTotalCommodityAmount(salePriceSum);
			sellerBillInvoice.setTotalInvoiceValue(totalInvoiceValues);
			sellerBillInvoice.setAcceptInvoiceStatus("1");
			Date createDate = new Date();//创建时间
			sellerBillInvoice.setCreateDate(createDate);

			updateInvoiceCount = sellerBillInvoiceMapper.updateInvoiceInfo(sellerBillInvoice);
			if(updateInvoiceCount > 0){
				//同步到买家模块
				SBuyBillInvoice sBuyBillInvoice = new SBuyBillInvoice();
				sBuyBillInvoice.setId(invoiceId);
				//sBuyBillInvoice.setSettlementNo(settlementNo);
				//sBuyBillInvoice.setCompanyId(buyCompanyId);
				//sBuyBillInvoice.setSupplierId(ShiroUtils.getCompId());
				sBuyBillInvoice.setInvoiceNo(invoiceNo);
				sBuyBillInvoice.setInvoiceFileAddr(invoiceFileAddr);
				sBuyBillInvoice.setInvoiceType(invoiceType);
				sBuyBillInvoice.setInvoiceHeader(invoiceHeader);
				sBuyBillInvoice.setTotalCommodityAmount(salePriceSum);
				sBuyBillInvoice.setTotalInvoiceValue(totalInvoiceValues);
				sBuyBillInvoice.setAcceptInvoiceStatus("1");
				sBuyBillInvoice.setCreateDate(createDate);
				sBuyBillInvoiceMapper.updateInvoiceInfo(sBuyBillInvoice);
				//添加发票关联
				/*String[] recItemIdList = recItemIds.split(",");
				for(String recItemId : recItemIdList){
					if(null != recItemIdList &&!"".equals(recItemIdList)){
						SBuyBillInvoiceCommodity sBuyBillInvoiceCommodity = new SBuyBillInvoiceCommodity();
						sBuyBillInvoiceCommodity.setId(ShiroUtils.getUid());
						sBuyBillInvoiceCommodity.setInvoiceId(invoiceId);
						sBuyBillInvoiceCommodity.setReconciliationItemId(recItemId);
						sBuyBillInvoiceCommodity.setAcceptInvoiceStatus("1");
						sBuyBillInvoiceCommodityMapper.addInvoiceToRecItem(sBuyBillInvoiceCommodity);

						//同步对账详情状态
						Map<String,Object> map = new HashMap<String,Object>();
						map.put("id",recItemId);
						//map.put("isInvoiceStatus",billDealStatus);
						map.put("isInvoiceStatus","1");
						sellerBillReconciliationItemMapper.updateRecItemPrice(map);
					}
				}*/
			}
		}
		return updateInvoiceCount;
	}

	//发票取消
	public JSONObject closeInvoice(String id){
		JSONObject json = new JSONObject();

		//同步发票详情状态
		Map<String,Object> invoiceMap = new HashMap<String,Object>();
		invoiceMap.put("id",id);
		invoiceMap.put("acceptInvoiceStatus","4");
		int sellerInvoiceCount = sellerBillInvoiceMapper.updateSellerInvoice(invoiceMap);

		if(sellerInvoiceCount > 0){
			sBuyBillInvoiceMapper.updateBillInvoice(invoiceMap);
			//同步对账详情状态
			Map<String,Object> recItemMap = new HashMap<String,Object>();
			recItemMap.put("invoiceId",id);
			recItemMap.put("isInvoiceStatus","0");
			sellerBillReconciliationItemMapper.updateRecItemPrice(recItemMap);

				json.put("success", true);
				json.put("msg", "发票取消成功！");
		}else {
			json.put("success", false);
			json.put("msg", "发票取消失败！");
		}
		return  json;
	}

	//发货信息公共查询调用
	public List<Map<String, Object>> queryDeliveryItemList(SellerDeliveryRecord sellerDeliveryRecord
			,List<Map<String, Object>> commodityBatchList){
		if(null != sellerDeliveryRecord){
			List<Map<String, Object>> deliveryItemBatchList = new ArrayList<>();//发货详情返回的集合
			Map<String,Object> deliveryMap = new HashMap<String,Object>();//发货数据集合
			String recordId = sellerDeliveryRecord.getId();
			String deliveryNo = sellerDeliveryRecord.getDeliverNo();//批次号
			String buycompanyId = sellerDeliveryRecord.getBuycompanyId();//客户编号
			String buycompanyName = sellerDeliveryRecord.getBuycompanyName();//客户名称
			String arrivalDateStr = sellerDeliveryRecord.getArrivalDateStr();//账单完结时间
			String isInvoiceStatus = sellerDeliveryRecord.getIsInvoiceStatus();//发票确认状态

			Map<String,Object> queryItemMap = new HashMap<String,Object>();
			queryItemMap.put("recordId",recordId);
			List<SellerDeliveryRecordItem> sellerDeliveryRecordItem = sellerDeliveryRecordItemService.queryDeliveryRecordItem(queryItemMap);

			for(SellerDeliveryRecordItem sellerDeliveryRecordItemInfo : sellerDeliveryRecordItem){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				int arrivalNum = sellerDeliveryRecordItemInfo.getArrivalNum();//数量
				BigDecimal salePrice = sellerDeliveryRecordItemInfo.getSalePrice();//单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

				itemMap.put("sellerDeliveryRecordItemInfo",sellerDeliveryRecordItemInfo);
				itemMap.put("salePriceSum",salePriceSum);
				deliveryItemBatchList.add(itemMap);
			}
			deliveryMap.put("commodityFlag",recordId+","+"1");//1 发货数据，2 退货数据
			deliveryMap.put("typeFlag","1");
			deliveryMap.put("recordId",recordId);
			deliveryMap.put("deliveryNo",deliveryNo);
			deliveryMap.put("buycompanyId",buycompanyId);
			deliveryMap.put("buycompanyName",buycompanyName);
			deliveryMap.put("arrivalDateStr",arrivalDateStr);
			deliveryMap.put("isInvoiceStatus",isInvoiceStatus);
			deliveryMap.put("deliveryItemBatchList",deliveryItemBatchList);

			commodityBatchList.add(deliveryMap);
		}
		return  commodityBatchList;
	}

	//退货信息公共调用
	public List<Map<String, Object>> queryCustomerItemList(SellerCustomer sellerCustomer
			,List<Map<String, Object>> commodityBatchList){
		if(null != sellerCustomer){
			List<Map<String, Object>> customerItemBatchList = new ArrayList<>();//退货详情返回的集合
			Map<String,Object> customerMap = new HashMap<String,Object>();//退货数据集合
			String customerId = sellerCustomer.getId();
			String customerCode = sellerCustomer.getCustomerCode();//退货批次
			String buycompanyId = sellerCustomer.getCompanyId();//卖家编号
			String buycompanyName = sellerCustomer.getBuyCompanyName();//买家名称
			String verifyDate = sellerCustomer.getVerifyDateStr();//批次完成时间
			String isInvoiceStatus = sellerCustomer.getIsInvoiceStatus();//发票状态

			Map<String,Object> queryItemMap = new HashMap<String,Object>();
			queryItemMap.put("recordId",customerId);
			List<SellerCustomerItem> sellerCustomerItemList = sellerCustomerItemService.queryCustomerItem(queryItemMap);
			for(SellerCustomerItem sellerCustomerItem : sellerCustomerItemList){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				//int goodsNum = sellerCustomerItem.getGoodsNumber();//数量
				int goodsNum = sellerCustomerItem.getConfirmNumber();//数量
				BigDecimal goodsPrice = sellerCustomerItem.getPrice();//单价
				BigDecimal goodsNumDecimal = new BigDecimal(goodsNum);//转换类型
				BigDecimal goodsPriceSum = goodsPrice.multiply(goodsNumDecimal);//退货金额

				itemMap.put("sellerCustomerItem",sellerCustomerItem);
				itemMap.put("goodsPriceSum",goodsPriceSum);
				customerItemBatchList.add(itemMap);
			}
			customerMap.put("commodityFlag",customerId+","+"2");
			customerMap.put("typeFlag","2");
			customerMap.put("customerId",customerId);
			customerMap.put("customerCode",customerCode);
			customerMap.put("buycompanyId",buycompanyId);
			customerMap.put("buycompanyName",buycompanyName);
			customerMap.put("verifyDate",verifyDate);
			customerMap.put("isInvoiceStatus",isInvoiceStatus);
			customerMap.put("customerItemBatchList",customerItemBatchList);

			commodityBatchList.add(customerMap);
		}
		return commodityBatchList;
	}

	//根据发票编号查询订单详情
	public Map<String,Object> queryRecItemByInvoiceId(Map<String,Object> recItemMap){
		//String invoiceId = recItemMap.get("invoiceId").toString();
		Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
		List<Map<String,Object>> deliveryItemList = new ArrayList<>();//合并返回的集合
		List<SellerBillReconciliationItem> deliveryRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		int arrivalNumCount = 0;
		BigDecimal priceSum = new BigDecimal(0);
		for(SellerBillReconciliationItem deliveryRecItem : deliveryRecItemList){
			Map<String,Object> itemMap = new HashMap<String,Object>();
			String isUpdateSale = deliveryRecItem.getIsUpdateSale();
			int arrivalNum = deliveryRecItem.getArrivalNum();//数量
			BigDecimal salePrice = new BigDecimal(0);//单价
			if("1".equals(isUpdateSale)){
				salePrice = deliveryRecItem.getUpdateSalePrice();//单价
			}else {
				salePrice = deliveryRecItem.getSalePrice();//单价
			}
			BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
			BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

			arrivalNumCount = arrivalNumCount + arrivalNum;
			priceSum = priceSum.add(salePriceSum);//发票商品总额

			itemMap.put("deliveryRecItem",deliveryRecItem);
					/*itemMap.put("reconciliationId",reconciliationId);
					itemMap.put("buycompanyName",buycompanyName);*/
			itemMap.put("salePrice",salePrice);
			itemMap.put("salePriceSum",salePriceSum);
			deliveryItemList.add(itemMap);
		}
		deliveryItemMap.put("arrivalNumCount",arrivalNumCount);
		deliveryItemMap.put("priceSum",priceSum);
		deliveryItemMap.put("deliveryItemList",deliveryItemList);

		return deliveryItemMap;
	}

	public SellerBillInvoice get(String id){
		return sellerBillInvoiceMapper.get(id);
	}
	
	public List<SellerBillInvoice> queryList(Map<String, Object> map){
		return sellerBillInvoiceMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerBillInvoiceMapper.queryCount(map);
	}
	
	public void add(SellerBillInvoice sellerBillInvoice){
		sellerBillInvoiceMapper.add(sellerBillInvoice);
	}
	
	public void update(SellerBillInvoice sellerBillInvoice){
		sellerBillInvoiceMapper.update(sellerBillInvoice);
	}
	
	public void delete(String id){
		sellerBillInvoiceMapper.delete(id);
	}
	

}
