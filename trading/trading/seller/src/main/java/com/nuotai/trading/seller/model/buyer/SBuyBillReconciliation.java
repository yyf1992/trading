package com.nuotai.trading.seller.model.buyer;

import java.math.BigDecimal;
import java.util.Date;

public class SBuyBillReconciliation {
    private String id;

    private String paymentNo;

    private String companyId;
    
    private String buyCompanyName;

    private String sellerCompanyId;

    private String sellerCompanyName;

    private String createBillUserId;

    private String createBillUserName;
    
    private Date startBillStatementDate;

    private Date endBillStatementDate;

    private Date createDate;

    private String startBillStatementDateStr;

    private String endBillStatementDateStr;

    private int recordConfirmCount;
    
    private BigDecimal recordConfirmTotal;

    private int recordReplaceCount;

    private BigDecimal recordReplaceTotal;

    private int replaceConfirmCount;

    private BigDecimal replaceConfirmTotal;

    private BigDecimal freightSumCount;

    private int returnGoodsCount;
    
    private BigDecimal returnGoodsTotal;

    private BigDecimal totalAmount;

    private BigDecimal customAmount;//奖惩金额

    private BigDecimal advanceDeductTotal;//预付款金额

    private int startTotalAmount;

    private int endTotalAmount;

    private String status;

    private String bankAccount;

    private String paymentType;

    private String billDealStatus;

    private String sellerDealStatus;

    private int totalSumCount;

    private String isFullInvoice;

    private String paymentStatus;

    private String paymentFileAddr;

    private String buyerReconciliationRemarks;

    private String sellerReconciliationRemarks;

    private String buyerPaymentRemarks;

    private String sellerPaymentRemarks;

    private BigDecimal actualPaymentAmount;

    private BigDecimal residualPaymentAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getBuyCompanyName() {
        return buyCompanyName;
    }

    public void setBuyCompanyName(String buyCompanyName) {
        this.buyCompanyName = buyCompanyName;
    }

    public String getSellerCompanyId() {
        return sellerCompanyId;
    }

    public void setSellerCompanyId(String sellerCompanyId) {
        this.sellerCompanyId = sellerCompanyId;
    }

    public String getSellerCompanyName() {
        return sellerCompanyName;
    }

    public void setSellerCompanyName(String sellerCompanyName) {
        this.sellerCompanyName = sellerCompanyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRecordConfirmCount() {
        return recordConfirmCount;
    }

    public void setRecordConfirmCount(int recordConfirmCount) {
        this.recordConfirmCount = recordConfirmCount;
    }

    public int getReturnGoodsCount() {
        return returnGoodsCount;
    }

    public void setReturnGoodsCount(int returnGoodsCount) {
        this.returnGoodsCount = returnGoodsCount;
    }

    public BigDecimal getRecordConfirmTotal() {
        return recordConfirmTotal;
    }

    public void setRecordConfirmTotal(BigDecimal recordConfirmTotal) {
        this.recordConfirmTotal = recordConfirmTotal;
    }

    public BigDecimal getReturnGoodsTotal() {
        return returnGoodsTotal;
    }

    public void setReturnGoodsTotal(BigDecimal returnGoodsTotal) {
        this.returnGoodsTotal = returnGoodsTotal;
    }

    public Date getStartBillStatementDate() {
        return startBillStatementDate;
    }

    public void setStartBillStatementDate(Date startBillStatementDate) {
        this.startBillStatementDate = startBillStatementDate;
    }

    public Date getEndBillStatementDate() {
        return endBillStatementDate;
    }

    public void setEndBillStatementDate(Date endBillStatementDate) {
        this.endBillStatementDate = endBillStatementDate;
    }

    public String getBillDealStatus() {
        return billDealStatus;
    }

    public void setBillDealStatus(String billDealStatus) {
        this.billDealStatus = billDealStatus;
    }

    public String getSellerDealStatus() {
        return sellerDealStatus;
    }

    public void setSellerDealStatus(String sellerDealStatus) {
        this.sellerDealStatus = sellerDealStatus;
    }

    public int getStartTotalAmount() {
        return startTotalAmount;
    }

    public void setStartTotalAmount(int startTotalAmount) {
        this.startTotalAmount = startTotalAmount;
    }

    public int getEndTotalAmount() {
        return endTotalAmount;
    }

    public void setEndTotalAmount(int endTotalAmount) {
        this.endTotalAmount = endTotalAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStartBillStatementDateStr() {
        return startBillStatementDateStr;
    }

    public void setStartBillStatementDateStr(String startBillStatementDateStr) {
        this.startBillStatementDateStr = startBillStatementDateStr;
    }

    public String getEndBillStatementDateStr() {
        return endBillStatementDateStr;
    }

    public void setEndBillStatementDateStr(String endBillStatementDateStr) {
        this.endBillStatementDateStr = endBillStatementDateStr;
    }

    public int getReplaceConfirmCount() {
        return replaceConfirmCount;
    }

    public void setReplaceConfirmCount(int replaceConfirmCount) {
        this.replaceConfirmCount = replaceConfirmCount;
    }

    public BigDecimal getReplaceConfirmTotal() {
        return replaceConfirmTotal;
    }

    public void setReplaceConfirmTotal(BigDecimal replaceConfirmTotal) {
        this.replaceConfirmTotal = replaceConfirmTotal;
    }

    public int getTotalSumCount() {
        return totalSumCount;
    }

    public void setTotalSumCount(int totalSumCount) {
        this.totalSumCount = totalSumCount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentFileAddr() {
        return paymentFileAddr;
    }

    public void setPaymentFileAddr(String paymentFileAddr) {
        this.paymentFileAddr = paymentFileAddr;
    }

    public BigDecimal getResidualPaymentAmount() {
        return residualPaymentAmount;
    }

    public void setResidualPaymentAmount(BigDecimal residualPaymentAmount) {
        this.residualPaymentAmount = residualPaymentAmount;
    }

    public String getIsFullInvoice() {
        return isFullInvoice;
    }

    public void setIsFullInvoice(String isFullInvoice) {
        this.isFullInvoice = isFullInvoice;
    }

    public String getBuyerPaymentRemarks() {
        return buyerPaymentRemarks;
    }

    public void setBuyerPaymentRemarks(String buyerPaymentRemarks) {
        this.buyerPaymentRemarks = buyerPaymentRemarks;
    }

    public String getSellerPaymentRemarks() {
        return sellerPaymentRemarks;
    }

    public void setSellerPaymentRemarks(String sellerPaymentRemarks) {
        this.sellerPaymentRemarks = sellerPaymentRemarks;
    }

    public BigDecimal getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(BigDecimal actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public BigDecimal getFreightSumCount() {
        return freightSumCount;
    }

    public void setFreightSumCount(BigDecimal freightSumCount) {
        this.freightSumCount = freightSumCount;
    }

    public String getBuyerReconciliationRemarks() {
        return buyerReconciliationRemarks;
    }

    public void setBuyerReconciliationRemarks(String buyerReconciliationRemarks) {
        this.buyerReconciliationRemarks = buyerReconciliationRemarks;
    }

    public String getSellerReconciliationRemarks() {
        return sellerReconciliationRemarks;
    }

    public void setSellerReconciliationRemarks(String sellerReconciliationRemarks) {
        this.sellerReconciliationRemarks = sellerReconciliationRemarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBillUserId() {
        return createBillUserId;
    }

    public void setCreateBillUserId(String createBillUserId) {
        this.createBillUserId = createBillUserId;
    }

    public String getCreateBillUserName() {
        return createBillUserName;
    }

    public void setCreateBillUserName(String createBillUserName) {
        this.createBillUserName = createBillUserName;
    }

    public int getRecordReplaceCount() {
        return recordReplaceCount;
    }

    public void setRecordReplaceCount(int recordReplaceCount) {
        this.recordReplaceCount = recordReplaceCount;
    }

    public BigDecimal getRecordReplaceTotal() {
        return recordReplaceTotal;
    }

    public void setRecordReplaceTotal(BigDecimal recordReplaceTotal) {
        this.recordReplaceTotal = recordReplaceTotal;
    }

    public BigDecimal getAdvanceDeductTotal() {
        return advanceDeductTotal;
    }

    public void setAdvanceDeductTotal(BigDecimal advanceDeductTotal) {
        this.advanceDeductTotal = advanceDeductTotal;
    }

    public BigDecimal getCustomAmount() {
        return customAmount;
    }

    public void setCustomAmount(BigDecimal customAmount) {
        this.customAmount = customAmount;
    }
}