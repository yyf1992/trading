package com.nuotai.trading.seller.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 
 * 
 * @author "
 * @date 2017-10-18 11:06:21
 */
@Data
public class SellerBillReconciliationItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//预付款抵扣编号
	private String advanceEditId;
	//对账编号
	private String reconciliationId;
	//发货单号
	private String recordId;
	//买家编号
	private String buyCompanyId;
	//买家名称
	private String buyCompanyName;
	//卖家编号
	private String sellerCompanyId;
	//发货详情编号
	private String itemId;
	//发货单号
	private String deliverNo;
	//入库单号
	private String storageNo;
	//采购订单号
	private String orderCode;
	//售后编号
	private String customerCode;
	//发货账单详情
	private String deliveryAddr;
	//退货账单详情
	private String proof;
	//订单id
	private String orderId;
	//订单类型： 1 发货，2 换货，3 退货
	private String billReconciliationType;
	//是否需要开发票：Y 是，N 否
	private String isNeedInvoice;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//单位
	private String unitId;
	//单位名称
	private String unitName;
	//销售单价
	private BigDecimal salePrice;
	//修改单价
	private BigDecimal updateSalePrice;
	//上次修改的单价
	private BigDecimal updateSalePriceOld;
	//是否修改过单价
	private String isUpdateSale;
	//到货数
	private Integer arrivalNum;
	//物流编号
	private String logisticsId;
	//运费
	private BigDecimal freight;
	//物流单号
	private String waybillNo;
	//物流公司
	private String logisticsCompany;
	//司机名称
	private String driverName;
	//到货时间
	private Date arrivalDate;
	//到货时间
	private String arrivalDateStr;
	//开票状态
	private String isInvoiceStatus;
	//对账状态
	private String reconciliationDealStatus;
	//单据创建人编号
	private String createBillId;
	//单据创建人名称
	private String createBillName;
}
