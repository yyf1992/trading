package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * The interface Seller order supplier product mapper.
 */
public interface SellerOrderSupplierProductMapper extends BaseDao<SellerOrderSupplierProduct>{
    /**
     * Delete by primary key int.
     *
     * @param id the id
     * @return the int
     */
    int deleteByPrimaryKey(String id);

    /**
     * Insert int.
     *
     * @param record the record
     * @return the int
     */
    int insert(SellerOrderSupplierProduct record);

    /**
     * Insert selective int.
     *
     * @param record the record
     * @return the int
     */
    int insertSelective(SellerOrderSupplierProduct record);

    /**
     * Select by primary key seller order supplier product.
     *
     * @param id the id
     * @return the seller order supplier product
     */
    SellerOrderSupplierProduct selectByPrimaryKey(String id);

    /**
     * Update by primary key selective int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKeySelective(SellerOrderSupplierProduct record);

    /**
     * Update by primary key int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKey(SellerOrderSupplierProduct record);

    /**
     * Select supplier product by order id list.
     *
     * @param orderId the order id
     * @return the list
     */
    List<SellerOrderSupplierProduct> selectSupplierProductByOrderId(String orderId);  //根据订单Id获取信息

    /**
     * Select by map seller order supplier product.
     *
     * @param map the map
     * @return the seller order supplier product
     */
    SellerOrderSupplierProduct selectByMap(Map<String, Object> map);

    /**
     * Update delivered num.
     *
     * @param id          the id
     * @param deliveryNum the delivery num
     */
    void updateDeliveredNum(@Param("id") String id,@Param("deliveryNum") int deliveryNum);

    /**
     * Add arrival num.
     * 增加到货数量
     *
     * @param orderItemId the sellerOrderItemId item id
     * @param addNum      the addNum num
     */
    void addArrivalNum(@Param("orderItemId") String orderItemId,@Param("addNum") int addNum);

    /**
     * Gets not arrival export data.
     * 获取未到货数据
     * @param map the map
     * @return the not arrival export data
     */
    List<SellerOrderSupplierProduct> getNotArrivalExportData(Map<String, Object> map);
    
    List<Map<String,Object>> getArrivalRate(Map<String, Object> map);
    List<Map<String,Object>> getCompletionRate(Map<String, Object> map);
}