package com.nuotai.trading.seller.dao.buyer;

import java.util.List;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyCustomerItem;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-12 16:49:24
 */
public interface SBuyCustomerItemMapper extends BaseDao<SBuyCustomerItem> {
	
	List<SBuyCustomerItem> selectCustomerItemByCustomerId(String customerId);
	
	void deleteByCustomerId(String customerId);
}
