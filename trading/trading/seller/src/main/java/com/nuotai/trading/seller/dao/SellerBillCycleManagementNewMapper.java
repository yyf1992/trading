package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillCycleManagementNew;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 16:01:52
 */
@Component
public interface SellerBillCycleManagementNewMapper extends BaseDao<SellerBillCycleManagementNew> {
    //查询修改申请列表
	List<SellerBillCycleManagementNew> querySellerBillCycleNewList(SearchPageUtil searchPageUtil);
	//根据申请编号查询申请数据
    SellerBillCycleManagementNew querySellerBillCycleNew(String id);
	//根据审批状态查询申请数量
    int getBillNewCountByStatus(Map<String, Object> map);
    //卖家审批修改申请
    int updateBillCycleNew(SellerBillCycleManagementNew sellerBillCycleManagementNew);
}
