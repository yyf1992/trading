package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.SellerContractMapper;
import com.nuotai.trading.seller.model.SellerContract;
import com.nuotai.trading.seller.service.interfaces.ISellerContract;
import com.nuotai.trading.utils.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liuhui
 * @date 2017-08-28 14:01
 * @description
 **/
public class ISellerContractService implements ISellerContract {
    @Autowired
    private SellerContractMapper sellerContractMapper;

    @Override
    public String addContractFromBuyer(String buyContractJson) {
        JSONObject json = new JSONObject();
        try {
            if (!ObjectUtil.isEmpty(buyContractJson)) {
                //将传过来的买方合同转换成卖方合同
                SellerContract contract = JSONObject.parseObject(buyContractJson,SellerContract.class);
                String contractId = contract.getId();
                Map<String,Object> map = new HashMap<>();
                map.put("contractId",contractId);
                int count = sellerContractMapper.queryCount(map);
                if (count > 0) {//如果存在， 则更新原来合同数据，否则新增
                    contract.setStatus(2);
                    contract.setIsDel(0);
                    contract.setDelId(null);
                    contract.setDelName(null);
                    contract.setDelDate(null);
                } else {
                    sellerContractMapper.add(contract);
                }
            }
            json.put("success", true);
            json.put("msg", "保存成功！");
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "保存失败！");
        }
        return json.toString();
    }
}
