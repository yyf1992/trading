package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerBillReconciliationAdvanceMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyBillReconciliationAdvanceMapper;
import com.nuotai.trading.seller.model.SellerBillReconciliationAdvance;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliationAdvance;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SellerBillReconciliationAdvanceService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillReconciliationAdvanceService.class);

	@Autowired
	private SellerBillReconciliationAdvanceMapper sellerBillReconciliationAdvanceMapper;
	@Autowired
	private SBuyBillReconciliationAdvanceMapper buyBillRecAdvanceMapper;

	//列表数据查询
	public List<SellerBillReconciliationAdvance> queryAdvanceList(SearchPageUtil searchPageUtil){
		return sellerBillReconciliationAdvanceMapper.queryAdvanceList(searchPageUtil);
	}

	//根据审批状态查询数量
	public int getAdvanceCountByStatus(Map<String, Object> map) {
		return sellerBillReconciliationAdvanceMapper.getAdvanceCountByStatus(map);
	}
	//根据状态查询预付款数量
	public void queryAdvanceCountByStatus(Map<String, Object> advanceMap){
		//查询所有状态数量
		advanceMap.put("advanceStatusCount", "0");
		int advanceCount = getAdvanceCountByStatus(advanceMap);

		//待卖家审批中数量
		advanceMap.put("advanceStatusCount", "5");
		int advanceApproveCount = getAdvanceCountByStatus(advanceMap);
		//卖家审批通过数量
		advanceMap.put("advanceStatusCount", "6");
		int advancePassCount = getAdvanceCountByStatus(advanceMap);
		//卖家审批驳回数量
		advanceMap.put("advanceStatusCount", "7");
		int advanceRejectCount = getAdvanceCountByStatus(advanceMap);

		advanceMap.put("advanceCount", advanceCount);
		advanceMap.put("advanceApproveCount", advanceApproveCount);
		advanceMap.put("advancePassCount",advancePassCount);
		advanceMap.put("advanceRejectCount",advanceRejectCount);
	}

	//预付款审批
	/*public int updateAdvance(Map<String, Object> updateSavemap){
		String id = updateSavemap.get("id").toString();
		String acceptStatus = updateSavemap.get("acceptStatus").toString();
		String remarks = updateSavemap.get("remarks").toString();
		Date updateDate = new Date();
		String updateUserId = ShiroUtils.getUserId();
		String updateUserName = ShiroUtils.getUserName();

		SellerBillReconciliationAdvance sellerBillRecAdvance = new SellerBillReconciliationAdvance();
		sellerBillRecAdvance.setId(id);
		sellerBillRecAdvance.setStatus(acceptStatus);
		sellerBillRecAdvance.setUpdateUserId(updateUserId);
		sellerBillRecAdvance.setUpdateUserName(updateUserName);
		sellerBillRecAdvance.setUpdateCreateTime(updateDate);
		sellerBillRecAdvance.setUpdateRemarks(remarks);

		int updateCount = sellerBillReconciliationAdvanceMapper.updateAdvance(sellerBillRecAdvance);
		if(updateCount > 0){
			//同步到买家数据
			SBuyBillReconciliationAdvance buyBillRecAdvance = new SBuyBillReconciliationAdvance();
			buyBillRecAdvance.setId(id);
			buyBillRecAdvance.setStatus(acceptStatus);
			buyBillRecAdvance.setUpdateUserId(updateUserId);
			buyBillRecAdvance.setUpdateUserName(updateUserName);
			buyBillRecAdvance.setUpdateCreateTime(updateDate);
			buyBillRecAdvance.setUpdateRemarks(remarks);
			buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);

			Map<String,Object> map = new HashMap<String,Object>();
			//map.put("sellerCompanyId",ShiroUtils.getCompId());
			map.put("id",id);
			//账单对账详情
			SellerBillReconciliationAdvance sellerAdvanceOld = sellerBillReconciliationAdvanceMapper.queryAdvanceBuyMap(map);
			//修改驳回的修改记录
			Map<String,Object> updateAdvanceEditMap = new HashMap<String,Object>();
			updateAdvanceEditMap.put("advanceId",id);
			updateAdvanceEditMap.put("createBillUserId",sellerAdvanceOld.getCreateBillUserId());
			if(acceptStatus.equals("6")){
				updateAdvanceEditMap.put("editStatus","2");
			}else if(acceptStatus.equals("7")){
				updateAdvanceEditMap.put("editStatus","3");
			}
			sellerBillReconciliationAdvanceMapper.updateAdvanceEdit(updateAdvanceEditMap);
		}
		return updateCount;
	}*/

	//根据对账单号查询自定义付款附件信息
	public List<Map<String,Object>> queryAdvanceEditList(Map<String,Object> editMap){
		List<Map<String,Object>> advanceEditList = sellerBillReconciliationAdvanceMapper.queryAdvanceEditList(editMap);
		return advanceEditList;
	}

	//根据条件查询预付款
	public List<SellerBillReconciliationAdvance> selAdvanceList(Map<String,Object> selMap){
		//根据条件查询预付款
		List<SellerBillReconciliationAdvance> advanceList = sellerBillReconciliationAdvanceMapper.selAdvanceList(selMap);
		return  advanceList;
	}

}
