package com.nuotai.trading.seller.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.model.DicProvinces;
import com.nuotai.trading.seller.dao.SellerDeliveryRecordMapper;
import com.nuotai.trading.seller.dao.SellerManualOrderMapper;
import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.seller.model.SellerClientLinkman;
import com.nuotai.trading.seller.model.SellerDeliveryRecord;
import com.nuotai.trading.seller.model.SellerManualOrder;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import com.nuotai.trading.seller.service.SellerClientImplService;
import com.nuotai.trading.seller.service.SellerManualOrderService;
import com.nuotai.trading.seller.service.SellerOrderService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.service.DicProvincesService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 我是卖家-手工添加订单
 * @author wangli
 *
 */
@Controller
@RequestMapping("platform/sellers/salesmanagement")
public class SellerManualOrderController extends BaseController {

	@Autowired
	private SellerClientImplService sellerClientService;

	@Autowired
	private SellerManualOrderService sellerManualOrderService;
	
	@Autowired
	private DicProvincesService dicProvincesService;
	
	@Autowired
	private SellerOrderService sellerOrderService;
	
	@Autowired
	private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;  //卖家订单商品信息
	
	@Autowired
	private SellerManualOrderMapper sellerManualOrderMapper;
	
	@Autowired
	private BuyUnitService unitService;//商品单位
	
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;//公司信息
	
	@Autowired
	private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;
	/**
	 * 加载手工添加订单页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadManualOrder")
	public String loadManualOrder(@RequestParam Map<String,String> params){
		JSONObject json  = null;
		//单位管理
		List<BuyUnit> unitList = unitService.getBuyUnitList();
		model.addAttribute("unitList", unitList);
		model.addAttribute("num", params.containsKey("num")?params.get("num"):"0");
		model.addAttribute("discount_sum", params.containsKey("discount_sum")?params.get("discount_sum"):"0");
		model.addAttribute("total", params.containsKey("total")?params.get("total"):"0");
		
		if(params==null||params.isEmpty()){
			model.addAttribute("orderData", json);
			return "platform/sellers/salesmanagement/loadSellerManualOrder";
		}
		json = new JSONObject();
//		json.put("sellerId", params.get("sellerId"));
		json.put("suppName", params.get("suppName"));
		json.put("contact_person", params.get("contact_person"));
		json.put("clientPhone", params.get("clientPhone"));
		json.put("otherFee", params.get("otherFee"));
		json.put("paymentPrice", params.get("paymentPrice"));
		json.put("date", params.get("date"));
		json.put("paymentType", params.get("paymentType"));
		json.put("paymentPerson", params.get("paymentPerson"));
		json.put("handler", params.get("handler"));
		json.put("annexVoucher", params.get("annexVoucher"));
		
		JSONArray itemArray = new JSONArray();
		String goodsStrArr = params.get("productInfor");
		String[] goodsArr = goodsStrArr.split("&");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split("/");
			JSONObject itemJson = new JSONObject();
			itemJson.put("proName", goodsItem[0]);
			itemJson.put("unitName", goodsItem[1]);
			itemJson.put("price", goodsItem[2]);
			itemJson.put("goodsNumber", goodsItem[3]);
			itemJson.put("updatePrice", goodsItem[4]);
			itemJson.put("priceSum", goodsItem[5]);
			itemJson.put("colorCode", goodsItem[6]);
			itemJson.put("proCode", goodsItem[7]);
			itemJson.put("skuCode", goodsItem[8]);
			itemJson.put("skuOid", goodsItem[9]);
			itemJson.put("unitId", goodsItem[10]);
			itemJson.put("remark", goodsItem.length == 12?goodsItem[11]:"");
			itemArray.add(itemJson);
		}
		json.put("itemArray", itemArray);
		model.addAttribute("orderData", json);
		
		return "platform/sellers/salesmanagement/loadSellerManualOrder";
	
	}
	
	/**
	 * 手工添加订单提交页面
	 * @return
	 */
	@RequestMapping("/addSellerManualOrder")
	public String addManualOrder(String manualOrder,@RequestParam Map<String,Object> map){
		Map<String, Object> manualOrderMap = sellerManualOrderService.toAddManualOrderMapper(manualOrder);
	    model.addAttribute("manualOrderMap", manualOrderMap);
	    
		//查询收获地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue());
		//addressParams.put("compId", ShiroUtils.getCompId());
		addressParams.put("clientName", manualOrderMap.get("buyers"));
		List<SellerClientLinkman> addressList = sellerClientService.selectaddress(addressParams);
		model.addAttribute("addressList", addressList);
		
		// 加载省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(map);
        model.addAttribute("provinceList", provinceList);
        
		return "platform/sellers/salesmanagement/addSellerManualOrder";
	}
	
	/**
	 * 向采购商下单
	 */
	@ResponseBody
	@RequestMapping("saveManualOrderInsert")
	public void saveManualOrderInsert(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/sellers/salesmanagement/approvalMOSuccess");//审批成功调用的方法
				json.put("verifyError", "platform/sellers/salesmanagement/approvalMOError");//审批失败调用的方法
				json.put("relatedUrl", "platform/sellers/salesmanagement/approvalM0Detail");//审批详细信息地址
				List<String> idList = sellerManualOrderService.saveApprovalOrder(params);			
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过
	 * @param id
	 */
	@RequestMapping(value = "/approvalMOSuccess", method = RequestMethod.GET)
	@ResponseBody
	public String approvalMOSuccess(String id){
		JSONObject json = sellerOrderService.approvalMOSuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/approvalMOError", method = RequestMethod.GET)
	@ResponseBody
	public String approvalMOError(String id){
		JSONObject json = sellerOrderService.approvalMOError(id);
		return json.toString();
	}
	
	/**
	 * 审批查看详情
	 * @return
	 */
	@RequestMapping("approvalM0Detail")
	public String approvalMODetail(String id){
		//订单详情
		SellerOrder orderBean = sellerOrderService.get(id);
		//手工订单详情
		SellerManualOrder sellerManualOrder = sellerManualOrderMapper.selectByOrderId(orderBean.getId());
		model.addAttribute("sellerManualOrder", sellerManualOrder);
//		orderBean.setSellerManualOrder(sellerManualOrder);
		//订单商品
		List<SellerOrderSupplierProduct> orderProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderBean.getId());
		orderBean.setSupplierProductList(orderProductList);
		model.addAttribute("orderBean", orderBean);
		//供应商
		BuyCompany buyerBean = buyCompanyMapper.selectByPrimaryKey(orderBean.getCompanyId());
		model.addAttribute("buyerBean", buyerBean);
		//发货信息
		SellerDeliveryRecord  sellerDeliveryRecord  = sellerDeliveryRecordMapper.get(id);
		model.addAttribute("sellerDeliveryRecord", sellerDeliveryRecord);
		
		return "platform/sellers/salesmanagement/sellerManualOrderDetail";
	}
	
	/**
	 * 手工订单保存入库
	 * @param params
	 * @return
	 */
	@RequestMapping("saveManualOrde")
	@ResponseBody
	public String saveManualOrde(@RequestParam Map<String,Object> params){
		
		JSONObject json = sellerManualOrderService.saveOrder(params);
		return json.toString();
		
	}
	
	/**
	 * 添加成功页面
	 * @return
	 */
	@RequestMapping("/addManualOrderSucc")
	public String addManualOrdeSucc(@RequestParam Map<String,Object> params){
		
		model.addAttribute("orderId", params.containsKey("orderId")?params.get("orderId"):"");
		return "platform/sellers/salesmanagement/addSellerManualOrderSucc";
	}
	
	/**
	 * 手工添加订单列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("sellerManualOrderList")
	public String manualOrderList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map) throws Exception {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "99");
		}
		//获得不同状态数量
		Map<String,Object> params = sellerOrderService.getManualOrderNum(map);
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sellers/salesmanagement/sellerManualOrderList";
	}
	
	/**
	 * 获取订单数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadManualOrderData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil = sellerOrderService.loadManualData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		String returnPath = "platform/sellers/salesmanagement/";
		switch (tabId) {
		case 99:
			returnPath += "allManualOrder";//所有的手工订单
			break;
		case 1:
			returnPath += "deliveredOrder";//未发货的手工订单
			break;
		case 5:
			returnPath += "overOrder";//已完成的手工订单
			break;
		default:
			returnPath += "allManualOrder";
			break;
		}
		return returnPath;
	}
	
	/**
	 * 订单详情（卖家）
	 * @param params
	 * @return String
	 */
	@RequestMapping(value = "sellerManualOrderDetails", method = RequestMethod.GET)
	public String manualOrderDetail(@RequestParam Map<String,Object> params){
		if (params.size() == 0){
			return "platform/sellers/salesmanagement/sellerManualOrderDetails";
		}
		if(!ObjectUtil.isEmpty(params.get("orderId"))){
			//订单详情
			SellerOrder orderBean = sellerOrderService.get(params.get("orderId").toString());
			//手工订单详情
			SellerManualOrder sellerManualOrder = sellerManualOrderMapper.selectByOrderId(orderBean.getId());
			model.addAttribute("sellerManualOrder", sellerManualOrder);
//			orderBean.setSellerManualOrder(sellerManualOrder);
			//订单商品
			List<SellerOrderSupplierProduct> orderProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderBean.getId());
			orderBean.setSupplierProductList(orderProductList);
			model.addAttribute("orderBean", orderBean);

			//获得审批数据
			
			//供应商
			BuyCompany buyerBean = buyCompanyMapper.selectByPrimaryKey(orderBean.getCompanyId());
			model.addAttribute("buyerBean", buyerBean);
			//发货信息
			SellerDeliveryRecord  sellerDeliveryRecord  = sellerDeliveryRecordMapper.get(params.get("orderId").toString());
			model.addAttribute("sellerDeliveryRecord", sellerDeliveryRecord);
			//获取物流
//			SellerLogistics logisticsBean = sellerLogisticsService.getSellerLogisticsByDeliveryId(params.get("orderId").toString());
//			model.addAttribute("logisticsBean", logisticsBean);
		}
		return "platform/sellers/salesmanagement/sellerManualOrderDetails";
	}
	
	/**
	 * 打印订单页面
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("printSellerOrder")
	public String printOrder(@RequestParam Map<String,Object> params) throws Exception {
		if(!ObjectUtil.isEmpty(params.get("orderId"))){
		//订单详情
		SellerOrder order = sellerOrderService.get(params.get("orderId").toString());
		//手工订单详情
		SellerManualOrder sellerManualOrder = sellerManualOrderMapper.selectByOrderId(order.getId());
		model.addAttribute("sellerManualOrder", sellerManualOrder);
		//订单商品
		List<SellerOrderSupplierProduct> orderProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(order.getId());
		order.setSupplierProductList(orderProductList);
		model.addAttribute("order", order);
		
		}
		return "platform/sellers/salesmanagement/printSellerOrder";
	}
	
	/**
	 * 手工添加订单页面新增商品
	 * @param params
	 * @return
	 */
	@RequestMapping("saveManualOrderProduct")
	public String saveManualOrderProduct(@RequestParam Map<String,Object> params){
		
		JSONObject jsonObject= sellerManualOrderService.saveNewProduct(params);
		
		return jsonObject.toJSONString();
		
	}
		
}
