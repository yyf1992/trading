package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillReconciliationPayment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-11-01 11:02:22
 */
@Component
public interface SellerBillReconciliationPaymentMapper extends BaseDao<SellerBillReconciliationPayment> {
    //根据账单号查询付款记录
    List<SellerBillReconciliationPayment> getPaymentListByRecId(Map<String,Object> map);
	//根据付款详情单号修改状态
    int updatePaymentItem(Map<String,Object> map);
}
