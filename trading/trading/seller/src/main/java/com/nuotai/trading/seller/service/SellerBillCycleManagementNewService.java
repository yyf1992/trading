package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagement;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagementNew;
import com.nuotai.trading.seller.service.buyer.SBuyBillCycleNewService;
import com.nuotai.trading.seller.service.buyer.SBuyBillCycleService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.seller.dao.SellerBillCycleManagementNewMapper;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SellerBillCycleManagementNewService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillCycleManagementNewService.class);

	@Autowired
	private SellerBillCycleManagementNewMapper sellerBillCycleManagementNewMapper;
	@Autowired
	private SellerBillCycleService sellerBillCycleService;
	@Autowired
	private SellerBillCycleOldService sellerBillCycleOldService;
	@Autowired
	private SellerBillCycleInterestService sellerBillCycleInterestService;
	@Autowired
	private SellerBillInterestNewService sellerBillInterestNewService;
	@Autowired
	private SellerBillInterestOldService sellerBillInterestOldService;
	@Autowired
	private SBuyBillCycleService sBuyBillCycleService;
	@Autowired
	private SBuyBillCycleNewService sBuyBillCycleNewService;

	//根据申请编号查询申请数据
	public SellerBillCycleManagementNew querySellerBillCycleNew(String id){
		return sellerBillCycleManagementNewMapper.querySellerBillCycleNew(id);
	}

	//修改申请列表查询
	public List<SellerBillCycleManagementNew> querySellerBillCycleNewList(SearchPageUtil searchPageUtil){
		return sellerBillCycleManagementNewMapper.querySellerBillCycleNewList(searchPageUtil);
	}

	//根据审批状态查询申请数量
	public int getBillNewCountByStatus(Map<String, Object> map){
		return sellerBillCycleManagementNewMapper.getBillNewCountByStatus(map);
	}

	//向账单周期管理页面提供不同状态下的数量
	public void queryBillCycleNewCountByStatus(Map<String, Object> billMap){
		//查询待内部审批周期数
		billMap.put("billStatusToCount", "2");
		int approvalBillCycleCount = getBillNewCountByStatus(billMap);
		//查询待对方审批周期数
		billMap.put("billStatusToCount", "3");
		int acceptBillCycleCount = getBillNewCountByStatus(billMap);
		//查询审批已通过周期数
		billMap.put("billStatusToCount", "5");
		int apprEndBillCycleCount = getBillNewCountByStatus(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillCycleCount", approvalBillCycleCount);
		billMap.put("acceptBillCycleCount", acceptBillCycleCount);
		billMap.put("apprEndBillCycleCount", apprEndBillCycleCount);

	}

	public void add(SellerBillCycleManagementNew sellerBillCycleManagementNew){
		sellerBillCycleManagementNewMapper.add(sellerBillCycleManagementNew);
	}
	
	public int updateBillCycleNew(Map<String, Object> updateMap){
		String billDealStatus = (String) updateMap.get("billDealStatus");//修改状态
		String billCycleNewId = (String) updateMap.get("billCycleId");//账单周期编号
		//String dealRemarks = (String) updateMap.get("dealRemarks");
		SellerBillCycleManagementNew sellerBillCycleManagementNew = new SellerBillCycleManagementNew();
		sellerBillCycleManagementNew.setId(billCycleNewId);
		sellerBillCycleManagementNew.setBillDealStatus(billDealStatus);
		//修改卖家修改申请状态
		int updateBillCycleNewCount = sellerBillCycleManagementNewMapper.updateBillCycleNew(sellerBillCycleManagementNew);
		//获取申请数据
		SellerBillCycleManagementNew sellerBillCycleNew = querySellerBillCycleNew(billCycleNewId);
		String billCycleId  = sellerBillCycleNew.getNewBillCycleId();
		//获取正式数据
		SellerBillCycleManagement sellerBillCycle = sellerBillCycleService.selectByPrimaryKey(billCycleId);

		if("3".equals(billDealStatus)){
			//拼装并添加历史数据
			SellerBillCycleManagementOld sellerBillCycleOld = new SellerBillCycleManagementOld();
			String billCycleOldId = ShiroUtils.getUid();
			sellerBillCycleOld.setId(billCycleOldId);
			sellerBillCycleOld.setOldBillCycleId(sellerBillCycle.getId());
			sellerBillCycleOld.setBuyCompanyId(sellerBillCycle.getBuyCompanyId());
			sellerBillCycleOld.setSellerCompanyId(sellerBillCycle.getSellerCompanyId());
			sellerBillCycleOld.setCycleStartDate(sellerBillCycle.getCycleStartDate());
			sellerBillCycleOld.setCycleEndDate(sellerBillCycle.getCycleEndDate());
			sellerBillCycleOld.setCheckoutCycle(sellerBillCycle.getCheckoutCycle());
			sellerBillCycleOld.setBillStatementDate(sellerBillCycle.getBillStatementDate());
			sellerBillCycleOld.setOutStatementDate(sellerBillCycle.getOutStatementDate());
			sellerBillCycleOld.setBillDealStatus(sellerBillCycle.getBillDealStatus());
			sellerBillCycleOld.setCreateUser(sellerBillCycle.getCreateUser());
			sellerBillCycleOld.setCreateTime(sellerBillCycle.getCreateTime());
			int addBillCycleOldCount = sellerBillCycleOldService.addNewBillCycleOld(sellerBillCycleOld);
			//获取正式关联利息
			List<SellerBillInterest> billInterestList = sellerBillCycleInterestService.getBillInteresInfo(billCycleId);
			//拼装并添加历史关联利息
			for(SellerBillInterest billInterest : billInterestList){
				SellerBillInterestOld billInterestOld = new SellerBillInterestOld();
				billInterestOld.setId(ShiroUtils.getUid());
				billInterestOld.setBillCycleIdOld(billCycleOldId);
				billInterestOld.setOverdueDate(billInterest.getOverdueDate());
				billInterestOld.setOverdueInterest(billInterest.getOverdueInterest());
				billInterestOld.setInterestCalculationMethod(billInterest.getInterestCalculationMethod());
				int addBillInterestOldCount = sellerBillInterestOldService.addNewInterestOld(billInterestOld);
			}
			//删除正式关联利息
			int deleteInterestCount = sellerBillCycleInterestService.deleteByPrimaryKey(billCycleId);
			//拼装并修改卖家正式数据
			SellerBillCycleManagement sellerBillCycleBean = new SellerBillCycleManagement();
			sellerBillCycleBean.setId(billCycleId);
			sellerBillCycleBean.setBuyCompanyId(sellerBillCycleNew.getBuyCompanyId());
			sellerBillCycleBean.setSellerCompanyId(sellerBillCycleNew.getSellerCompanyId());
			sellerBillCycleBean.setCycleStartDate(sellerBillCycleNew.getCycleStartDate());
			sellerBillCycleBean.setCycleEndDate(sellerBillCycleNew.getCycleEndDate());
			sellerBillCycleBean.setCheckoutCycle(sellerBillCycleNew.getCheckoutCycle());
			sellerBillCycleBean.setBillStatementDate(sellerBillCycleNew.getBillStatementDate());
			sellerBillCycleBean.setOutStatementDate(sellerBillCycleNew.getOutStatementDate());
			sellerBillCycleBean.setBillDealStatus(sellerBillCycleNew.getBillDealStatus());
			sellerBillCycleBean.setCreateUser(sellerBillCycleNew.getCreateUser());
			sellerBillCycleBean.setCreateTime(sellerBillCycleNew.getCreateTime());
			int updateSellerBillCycleCount = sellerBillCycleService.updateByPrimaryKeySelective(sellerBillCycleBean);
			//拼装并修改买家正式数据
			SBuyBillCycleManagement buyBillCycleBean = new SBuyBillCycleManagement();
			buyBillCycleBean.setId(billCycleId);
			buyBillCycleBean.setBuyCompanyId(sellerBillCycleNew.getBuyCompanyId());
			buyBillCycleBean.setSellerCompanyId(sellerBillCycleNew.getSellerCompanyId());
			buyBillCycleBean.setCycleStartDate(sellerBillCycleNew.getCycleStartDate());
			buyBillCycleBean.setCycleEndDate(sellerBillCycleNew.getCycleEndDate());
			buyBillCycleBean.setCheckoutCycle(sellerBillCycleNew.getCheckoutCycle());
			buyBillCycleBean.setBillStatementDate(sellerBillCycleNew.getBillStatementDate());
			buyBillCycleBean.setOutStatementDate(sellerBillCycleNew.getOutStatementDate());
			buyBillCycleBean.setBillDealStatus(sellerBillCycleNew.getBillDealStatus());
			buyBillCycleBean.setCreateUser(sellerBillCycleNew.getCreateUser());
			buyBillCycleBean.setCreateTime(sellerBillCycleNew.getCreateTime());
			int updateBuyBillCycleCount = sBuyBillCycleService.updateByPrimaryKeySelective(buyBillCycleBean);
			//获取申请数据关联利息
			List<SellerBillInterestNew> billInterestNewList = sellerBillInterestNewService.queryNewInterestList(billCycleNewId);
			//添加正式数据关联利息
			for(SellerBillInterestNew billInterestNew : billInterestNewList){
				Map<String, Object> interInterestMap = new HashMap<String, Object>();
				interInterestMap.put("id", ShiroUtils.getUid());
				interInterestMap.put("billCycleId", billCycleId);
				interInterestMap.put("overdueDate", billInterestNew.getOverdueDate());
				interInterestMap.put("overdueInterest", billInterestNew.getOverdueInterest());
				interInterestMap.put("interestCalculationMethod", billInterestNew.getInterestCalculationMethod());
				int addBillInterestCount = sellerBillCycleInterestService.saveInterestInfo(interInterestMap);
			}
		}else if("5".equals(billDealStatus)){
			//驳回买家修改申请状态
			SBuyBillCycleManagementNew sBuyBillCycleNew = new SBuyBillCycleManagementNew();
			sBuyBillCycleNew.setId(billCycleNewId);
			sBuyBillCycleNew.setBillDealStatus(billDealStatus);
			int updateBuyBillCycleNewCount = sBuyBillCycleNewService.updateNewBillCycle(sBuyBillCycleNew);
			//回复买家正式数据状态
			SBuyBillCycleManagement sBuyBillCycle = new SBuyBillCycleManagement();
			sBuyBillCycle.setId(billCycleId);
			sBuyBillCycle.setBillDealStatus(sellerBillCycle.getBillDealStatus());
			int updateBuyBillCycleCount = sBuyBillCycleService.updateByPrimaryKeySelective(sBuyBillCycle);
		}
		return  updateBillCycleNewCount;
	}
	
	public void delete(String id){
		sellerBillCycleManagementNewMapper.delete(id);
	}
	

}
