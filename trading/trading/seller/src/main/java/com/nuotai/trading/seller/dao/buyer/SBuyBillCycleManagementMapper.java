package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagement;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("sBuybillMapper")
public interface SBuyBillCycleManagementMapper extends BaseDao<SBuyBillCycleManagement>{
	//查询账单周期列表
	List<Map<String, Object>> getBillCycleList(SearchPageUtil searchPageUtil);
	
	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);
	
	//添加账单周期数据
	int saveBillCycleInfo(Map<String, Object> map);
	
	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);
	
	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);
	
    int deleteByPrimaryKey(String id);

    int insert(SBuyBillCycleManagement record);

    int insertSelective(SBuyBillCycleManagement record);

    SBuyBillCycleManagement selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SBuyBillCycleManagement record);

   // int updateByPrimaryKey(SBuyBillCycleManagement record);
}