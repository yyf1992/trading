package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillInvoice;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-10-12 17:45:26
 */
@Component
public interface SellerBillInvoiceMapper extends BaseDao<SellerBillInvoice> {
    //查询账单周期列表
    List<Map<String, Object>> getBillInvoiceList(SearchPageUtil searchPageUtil);
    //导出账单周期列表
    List<SellerBillInvoice> queryBillInvoiceList(Map<String,Object> invoiceMap);
    //查询数量
    int getInvoiceCountByStatus(Map<String, Object> map);
    //添加发票信息
    int addInvoiceInfo(SellerBillInvoice sellerBillInvoice);
    //修改发票信息
    int updateInvoiceInfo(SellerBillInvoice sellerBillInvoice);
    //修改发票状态
    int updateSellerInvoice(Map<String,Object> map);
    //根据账单详情查询发票详情
    SellerBillInvoice queryInvoiceByRecItem(Map<String, Object> updateSavemap);
    //根据账单详情查询发票详情
    SellerBillInvoice queryInvoiceById(Map<String, Object> updateSavemap);
}
