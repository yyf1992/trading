package com.nuotai.trading.seller.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 发货单详情展示类
 * @author liuhui
 * @date 2017-09-19 10:03:15
 */
@Data
public class SellerDeliveryRecordDetailView implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//卖家订单ID
	private String sellerOrderId;
	//买家订单ID
	private String buyerOrderId;
	//订单号
	private String orderCode;
	//订单日期
	private Date orderDate;
	//订单类型 0:采购订单 1:其他订单
	private String orderType;
	//供货商id
	private String supplierId;
	//供货商名称
	private String supplierName;
	//采购商公司ID
	private String buycompanyId;
	//采购商公司Name
	private String buycompanyName;
	//发货单号
	private String deliverNo;
	//发货日期
	private Date deliverDate;
	//发货明细
	List<SellerDeliveryRecordItem> itemList;
	//物流单号ID
	private String logisticsId;
	//产品总量
	private Integer orderNum;
}
