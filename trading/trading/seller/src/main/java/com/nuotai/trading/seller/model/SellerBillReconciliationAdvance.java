package com.nuotai.trading.seller.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-02-24 13:04:37
 */
@Data
public class SellerBillReconciliationAdvance implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//预付款编号
	private String id;
	//买家编号
	private String buyCompanyId;
	//采购上名称
	private String buyCompanyName;
	//卖家编号
	private String sellerCompanyId;
	//供应商名称
	private String sellerCompanyName;
	//下单人编号
	private String createBillUserId;
	//
	private String createBillUserName;
	//预付款创建人编号
	private String createUserId;
	//预付款创建人名称
	private String createUserName;
	//创建时间
	private Date createTime;
	//创建时间
	private String createTimeStr;
	//开始时间
	private String startAdvanceDate;
	//结束时间
	private String endAdvanceDate;
	//创建备注
	private String createRemarks;
	//锁定金额
	private BigDecimal lockTotal;
	//预付款可用金额
	private BigDecimal usableTotal;
	//预付款总金额
	private BigDecimal advanceTotal;
	//预付款状态：1 待内部审批，2 内部通过/待卖家确认，3 内部审批驳回，4卖家审批中，5 卖家审批通过，6 卖家审批驳回
	private String status;
	//审批人编号
	private String updateUserId;
	//审批人名称
	private String updateUserName;
	//审批时间
	private Date updateCreateTime;
	//修改审批备注
	private String updateRemarks;
	//附件路径
	private String filesAddress;
}
