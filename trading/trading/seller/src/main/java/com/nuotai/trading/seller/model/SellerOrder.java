package com.nuotai.trading.seller.model;

import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * 
 * 
 * @author "
 * @date 2017-08-17 10:03:15
 */
@Data
public class SellerOrder implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//订单号
	private String orderCode;
	//供应商Id
	private String suppId;
	//供应商名称
	private String suppName;
	//供应商负责人
	private String person;
	//手机号码
	private String phone;
	//产品总量
	private Integer goodsNum;
	//产品总价格
	private BigDecimal goodsPrice;
	//0、待接单；1、待确认协议；2、待对方发货；3、待收货；4、已收货；5、待审批；6、买家取消；7、卖家驳回8:终止
	private Integer status;
	//是否审核 0,待审核；1，已审核
	private Integer isCheck;
	//是否自提 0，不自提；1，自提
	private Integer isSince;
	//类型  0，采购下单；1，转化订单
	private Integer orderKind;
	//备注
	private String remark;
	//
	private String personName;
	//省
	private String province;
	private String provinceName;
	//市
	private String city;
	private String cityName;
	//区
	private String area;
	private String areaName;
	//收货地址
	private String addrName;
	//区号
	private String areaCode;
	//座机号
	private String planeNumber;
	//收货手机号码
	private String receiptPhone;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//修改时间
	private Date updateDate;
	//
	private String delId;
	//
	private String delName;
	//
	private Date delDate;
	//卖家驳回原因
	private String sellerRejectReason;
	//终止原因
	private String stopReason;
	//终止人
	private String stopPerson;
	//终止时间
	private Date stopDate;
	//客户公司id
	private String companyId;
	//客户公司名称
	private String companyName;
	//订单类型（0:采购发货 1:换货发货）
	private String orderType;
	//客户订单ID
	private String buyerOrderId;

	private List<SellerOrderSupplierProduct> supplierProductList;  //商品信息
	private SellerManualOrder sellerManualOrder;//手动添加的订单
	//发货单总量总计
	private Integer deliveryTotalNum;
	//发货中的数量总计
	private Integer deliveringTotalNum;
	//已到货的数量总计
	private Integer arrivalTotalNum;
	// 要求到货时间
	private Date predictArred;
	private boolean showDeliveryButton;
	//是否转化0未转化1已转化
	private Integer isChange;
	//是否已对账（0：未对账， 1：正在对帐，2：对账完成，3：部分对账)
	private String reconciliationStatus;
	//是否已付款（0：未付款， 1：正在付款，2：已付款，3：部分付款）
	private String isPaymentStatus;
}
