package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyBillInvoice;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-10-12 17:45:09
 */
@Component
public interface SBuyBillInvoiceMapper extends BaseDao<SBuyBillInvoice> {
    //同步发票数据
	int addBuyInvoice(SBuyBillInvoice sBuyBillInvoice);
    //修改发票信息
    int updateInvoiceInfo(SBuyBillInvoice sellerBillInvoice);
	//同步修改买家
    int updateBillInvoice(Map<String,Object> map);
}
