package com.nuotai.trading.seller.service.buyer;

import com.nuotai.trading.seller.dao.buyer.SBuyBillInvoiceMapper;
import com.nuotai.trading.seller.model.buyer.SBuyBillInvoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SBuyBillInvoiceService {

    private static final Logger LOG = LoggerFactory.getLogger(SBuyBillInvoiceService.class);

	@Autowired
	private SBuyBillInvoiceMapper sBuyBillInvoiceMapper;
	
	public SBuyBillInvoice get(String id){
		return sBuyBillInvoiceMapper.get(id);
	}
	
	public List<SBuyBillInvoice> queryList(Map<String, Object> map){
		return sBuyBillInvoiceMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sBuyBillInvoiceMapper.queryCount(map);
	}
	
	public void add(SBuyBillInvoice buyBillInvoice){
		sBuyBillInvoiceMapper.add(buyBillInvoice);
	}
	
	public void update(SBuyBillInvoice buyBillInvoice){
		sBuyBillInvoiceMapper.update(buyBillInvoice);
	}
	
	public void delete(String id){
		sBuyBillInvoiceMapper.delete(id);
	}
	

}
