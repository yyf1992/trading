package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.SellerBillInvoice;
import com.nuotai.trading.seller.model.SellerBillReconciliationItem;
import com.nuotai.trading.seller.service.SellerBillInvoiceService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import com.nuotai.trading.seller.service.SellerBillCycleService;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-8-28
 */

@Controller
@RequestMapping("platform/seller/billInvoice")
public class SellerBillInvoiceController extends BaseController {
	@Autowired
	private SysUserService sysUserService;//用户信息
	@Autowired
	private BuyCompanyService buyCompanyService; //公司管理
	@Autowired
	private SellerBillInvoiceService sellerBillInvoiceService;//卖家发票列表
	
	/**
	 * 发票列表查询
	 * @return
	 */
	@RequestMapping("billInvoiceList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("acceptInvoiceStatus")){
			params.put("acceptInvoiceStatus","0");
		}

		String selCompanyId = ShiroUtils.getCompId();
		params.put("selCompanyId",selCompanyId);
		String acceptInvoiceStatus = params.get("acceptInvoiceStatus").toString();

		//列表查询
		List<Map<String,Object>> commodityBatchList = new ArrayList<Map<String,Object>>();
		String returnPath = "platform/sellers/billcycles/billinvoice";
		if("0".equals(acceptInvoiceStatus)){
			params.put("isNeedInvoice","Y");
			params.put("isInvoiceStatus1","1");
			params.put("isInvoiceStatus2","2");
			params.put("reconciliationDealStatus","3");
			searchPageUtil.setObject(params);
			commodityBatchList = sellerBillInvoiceService.getCommodityBatchList(searchPageUtil);
			returnPath = returnPath + "/commodityBatchList";
		}else {
			searchPageUtil.setObject(params);
			commodityBatchList = sellerBillInvoiceService.getInvoiceList(searchPageUtil);
			returnPath = returnPath + "/billInvoiceList";
		}
		sellerBillInvoiceService.queryInvoiceCountByStatus(params);
		searchPageUtil.getPage().setList(commodityBatchList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return returnPath;
	}

	/**
	 * 卖家开具发票
	 * @param
	 * @return
	 */
	@RequestMapping("drawUpInvoice")
	public String drawUpInvoice(@RequestParam Map<String,Object> itemIdsMap){
		//发货、退货发票信息
		Map<String,Object> drawUpInvoiceToCommodity = sellerBillInvoiceService.drawUpInvoiceToCommodity(itemIdsMap);
		model.addAttribute("drawUpInvoiceToCommodity",drawUpInvoiceToCommodity);
		return "platform/sellers/billcycles/billinvoice/addDrawUpInvoice";
	}

	/**
	 * 保存发票数据
	 * @return
	 */
	@RequestMapping("saveInvoiceInfo")
	@ResponseBody
	public String saveInvoiceInfo(@RequestParam Map<String,Object> saveInvoiceMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillInvoiceService.saveInvoiceInfo(saveInvoiceMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "发票信息添加成功！");
			}else {
				json.put("error", false);
				json.put("msg", "发票信息添加失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 卖家开具发票
	 * @param
	 * @return
	 */
	@RequestMapping("updateInvoiceJump")
	public String updateInvoiceJump(String invoiceId){
		//发货、退货发票信息
		Map<String,Object> drawUpInvoiceToCommodity = sellerBillInvoiceService.queryUpdateInvoice(invoiceId);
		model.addAttribute("drawUpInvoiceToCommodity",drawUpInvoiceToCommodity);
		return "platform/sellers/billcycles/billinvoice/updateDrawUpInvoice";
	}

	/**
	 * 保存修改
	 * @param updateInvoiceMap
	 * @return
	 */
	@RequestMapping("updateInvoiceInfo")
	@ResponseBody
	public String updateInvoiceInfo(@RequestParam Map<String,Object> updateInvoiceMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillInvoiceService.updateInvoiceInfo(updateInvoiceMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "发票信息添加成功！");
			}else {
				json.put("error", false);
				json.put("msg", "发票信息添加失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 取消发票
	 * @param id
	 * @return
	 */
	@RequestMapping("closeInvoice")
	@ResponseBody
	public String closeInvoice(String id){
		JSONObject json = sellerBillInvoiceService.closeInvoice(id);
		return json.toString();
	}



	/**
	 * 根据发票编号查询关联订单
	 * @param recItemMap
	 * @return
	 */
	@RequestMapping("queryRecItemList")
	@ResponseBody
	public String queryRecItemList(@RequestParam Map<String,Object> recItemMap){
		Map<String,Object> recItemList = sellerBillInvoiceService.queryRecItemByInvoiceId(recItemMap);
		return JSONObject.toJSONString(recItemList);
	}

	/**
	 * Upload receipt attachment string.
	 * 上传付款票据
	 * @param files the files
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value="/uploadInvoiceFile",method= RequestMethod.POST)
	@ResponseBody
	public String uploadInvoiceFile(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
			throws Exception{
		JSONObject json = new JSONObject();
		try {
			List urlList = new ArrayList();
			for (MultipartFile file : files) {
				//获取文件名
				String fileName = file.getOriginalFilename();
				json.put("fileName", fileName);
				//获取文件后缀名
				String fileExtensionName = FilenameUtils.getExtension(fileName);
				if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
						&& !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
					json.put("result", "fail");
					json.put("msg", "上传文件格式不正确！");
					return json.toString();
				}
				//获取文件大小
				String fileSize = ShiroUtils.convertFileSize(file.getSize());
				UplaodUtil uplaodUtil = new UplaodUtil();
				String url = uplaodUtil.uploadFile(file,null,true);
				urlList.add(url);
			}
			json.put("urlList", urlList);
			json.put("result", "success");
		} catch (Exception e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		}
		return json.toString();
	}

	/**
	 * 待开票信息导出
	 * @param commodityMap
	 */
	@RequestMapping("sellerCommodityExport")
	public void sellerCommodityExport(@RequestParam Map<String,Object> commodityMap){
		commodityMap.put("selCompanyId",ShiroUtils.getCompId());
		List<Map<String,Object>> commodityMapList = sellerBillInvoiceService.getCommodityBatchExport(commodityMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单待开票报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"对账单号","发货单号","采购员","采购商名称","商品","单位","单价","到货数量","总金额","采购订单号","到货日期","状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("账单待开票报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != commodityMapList && commodityMapList.size()>0){
			for(Map<String,Object> commodityMapInfo : commodityMapList){
				SellerBillReconciliationItem deliveryRecItem = (SellerBillReconciliationItem) commodityMapInfo.get("deliveryRecItem");
				String salePrice = commodityMapInfo.get("salePrice").toString();
				String salePriceSum = commodityMapInfo.get("salePriceSum").toString();

				//for(SellerBillReconciliationItem deliveryRecItem : deliveryRecItemList){
					row=sheet.createRow(rowCount);
					//1.对账单号
					cell = row.createCell(0);
					cell.setCellValue(deliveryRecItem.getReconciliationId());
					cell.setCellStyle(normalStyle);
					//2.发货单号
					cell = row.createCell(1);
					cell.setCellValue(deliveryRecItem.getDeliverNo());
					cell.setCellStyle(normalStyle);
					//3.下单人
					cell = row.createCell(2);
					cell.setCellValue(deliveryRecItem.getCreateBillName());
					cell.setCellStyle(normalStyle);
					//3.采购商名称
					cell = row.createCell(3);
					cell.setCellValue(deliveryRecItem.getBuyCompanyName());
					cell.setCellStyle(normalStyle);
					//4.商品
					cell = row.createCell(4);
					cell.setCellValue(deliveryRecItem.getProductName());
					cell.setCellStyle(normalStyle);
					//5.单位
					cell = row.createCell(5);
					cell.setCellValue(deliveryRecItem.getUnitName());
					cell.setCellStyle(normalStyle);
					//6.单价
					cell = row.createCell(6);
					cell.setCellValue(salePrice);
					cell.setCellStyle(normalStyle);
					//7.到货数量
					cell = row.createCell(7);
					cell.setCellValue(deliveryRecItem.getArrivalNum());
					cell.setCellStyle(normalStyle);
					//8.总金额
					cell = row.createCell(8);
					cell.setCellValue(salePriceSum);
					cell.setCellStyle(normalStyle);
					//9.采购订单号
					cell = row.createCell(9);
					cell.setCellValue(deliveryRecItem.getOrderCode());
					cell.setCellStyle(normalStyle);
					//10.到货日期
					cell = row.createCell(10);
					cell.setCellValue(deliveryRecItem.getArrivalDateStr());
					cell.setCellStyle(normalStyle);
					//11.状态
					cell = row.createCell(11);
					String isInvoiceStatus = deliveryRecItem.getIsInvoiceStatus();
					String statusStr = "";
					if("0".equals(isInvoiceStatus)){
						statusStr = "待开票据";
					}else if("1".equals(isInvoiceStatus)){
						statusStr = "待确认票据";
					}else if("2".equals(isInvoiceStatus)){
						statusStr = "已确认票据";
					}else if("3".equals(isInvoiceStatus)){
						statusStr = "已驳回票据";
					}
					cell.setCellValue(statusStr);
					cell.setCellStyle(normalStyle);
					rowCount++;


			}
		}
		ExcelUtil.preExport("账单待开票报表", response);
		ExcelUtil.export(wb, response);
	}


	/**
	 * 导出发票
	 * @param invoiceMap
	 */
	@RequestMapping("sellerInvoiceExport")
	public void sellerInvoiceExport(@RequestParam Map<String,Object> invoiceMap){
		invoiceMap.put("selCompanyId",ShiroUtils.getCompId());
		List<SellerBillInvoice> sellerBillInvoice = sellerBillInvoiceService.queryBillInvoiceList(invoiceMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单发票报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"开票编号","供应商名称","商品总金额","票据号","票据类型","票据金额","开票日期","状态"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("账单发票报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != sellerBillInvoice && sellerBillInvoice.size()>0){
			for(SellerBillInvoice buyBillInvoice : sellerBillInvoice){
				row=sheet.createRow(rowCount);
				//1.开票编号
				cell = row.createCell(0);
				cell.setCellValue(buyBillInvoice.getSettlementNo());
				cell.setCellStyle(normalStyle);
				//2.供应商名称
				cell = row.createCell(1);
				cell.setCellValue(buyBillInvoice.getBuyCompanyName());
				cell.setCellStyle(normalStyle);
				//3.商品总金额
				cell = row.createCell(2);
				cell.setCellValue(fnum.format(buyBillInvoice.getTotalCommodityAmount()));
				cell.setCellStyle(normalStyle);
				//4.票据号
				cell = row.createCell(3);
				cell.setCellValue(buyBillInvoice.getInvoiceNo());
				cell.setCellStyle(normalStyle);
				//5.票据类型
				cell = row.createCell(4);
				String invoiceType = buyBillInvoice.getInvoiceType();
				String typeStr = "";
				if("1".equals(invoiceType)){
					typeStr = "专用发票";
				}else if("2".equals(invoiceType)){
					typeStr = "普通发票";
				}else if("3".equals(invoiceType)){
					typeStr = "其他发票";
				}
				cell.setCellValue(typeStr);
				cell.setCellStyle(normalStyle);
				//6.票据金额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(buyBillInvoice.getTotalInvoiceValue()));
				cell.setCellStyle(normalStyle);
				//7.开票日期
				cell = row.createCell(6);
				cell.setCellValue(buyBillInvoice.getCreateDateStr());
				cell.setCellStyle(normalStyle);
				//8.状态
				cell = row.createCell(7);
				String acceptStatus = buyBillInvoice.getAcceptInvoiceStatus();
				String statusStr = "";
				if("1".equals(acceptStatus)){
					statusStr = "待确认票据";
				}else if("2".equals(acceptStatus)){
					statusStr = "已确认票据";
				}else if("3".equals(acceptStatus)){
					statusStr = "已驳回票据";
				}else if("4".equals(acceptStatus)){
					statusStr = "已取消票据";
				}
				cell.setCellValue(statusStr);
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("账单发票信息", response);
		ExcelUtil.export(wb, response);
	}

}
