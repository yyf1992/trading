package com.nuotai.trading.seller.service.interfaces;
/**
 * @author liuhui
 * @date 2017-08-23 8:56
 * @description 合同接口
 **/


/**
 *  合同接口
 * @author liuhui
 * @create 2017-08-23 8:56
 **/
public interface ISellerContract {

    /**
     * 买家合同审核通过后添加到卖家合同里
     * @param buyContractJson
     * @return
     */
    String addContractFromBuyer(String buyContractJson);
}
