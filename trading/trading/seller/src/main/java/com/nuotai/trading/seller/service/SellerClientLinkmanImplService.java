package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.SellerClientLinkmanMapper;
import com.nuotai.trading.seller.model.SellerClient;
import com.nuotai.trading.seller.model.SellerClientLinkman;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("sellerClientLinkService")
public class SellerClientLinkmanImplService {

   // private static final Log LOG = Log.getLog(${table.className}Service.class);

	@Autowired
	private SellerClientLinkmanMapper sellerClientLinkmanMapper;

	/**
	 * 保存客户联系人
	 * @param sellerClient
	 * @param mapLinkman
	 */
	public void saveSellerClientLinkman(SellerClient sellerClient, Map<String,Object> mapLinkman) {
		if (sellerClient == null){
			return;
		}
		
	    List<SellerClientLinkman> sellerClientLinkmanList = JSONObject.parseArray(String.valueOf(mapLinkman.get("ContactSettings")),SellerClientLinkman.class);
	    
		// 添加联系人表
		if(!ObjectUtil.isEmpty(sellerClientLinkmanList)&&sellerClientLinkmanList.size()>0){
			for (SellerClientLinkman sellerClientLinkman : sellerClientLinkmanList) {
				sellerClientLinkman.setId(ShiroUtils.getUid());
				sellerClientLinkman.setSellerClientId(sellerClient.getId());
				sellerClientLinkman.setIsdel(Constant.IsDel.NODEL.getValue());
				Date date = new Date();
				sellerClientLinkman.setCreateDate(date);
				sellerClientLinkmanMapper.saveSellerClientLinkman(sellerClientLinkman);
			}
		}

	}	
	
	/**
	 * 查询客户联系人详情
	 * @param sellerClientId
	 * @return
	 */
	public List<SellerClientLinkman> selectBySellerClientId(String sellerClientId){
		
		if (sellerClientId != null && sellerClientId != ""){
			List<SellerClientLinkman> sellerClientLinkmanList = sellerClientLinkmanMapper.selectBySellerClientId(sellerClientId);
			return sellerClientLinkmanList;
		}
		return null;		
	}
	
	/**
	 * 根据Id更新客户联系的信息
	 * @param sellerClient
	 */
	public void updateLinkmanById(SellerClient sellerClient,Map<String,Object> mapLinkman){
		
		if (mapLinkman == null){
			return;
		}
		
	    List<SellerClientLinkman> sellerClientLinkmanList = JSONObject.parseArray(String.valueOf(mapLinkman.get("ContactSettings")),SellerClientLinkman.class);
	    
		// 添加联系人表
		if(!ObjectUtil.isEmpty(sellerClientLinkmanList)&&sellerClientLinkmanList.size()>0){
			for (SellerClientLinkman sellerClientLinkman : sellerClientLinkmanList) {
				sellerClientLinkman.setSellerClientId(sellerClient.getId());
				sellerClientLinkman.setIsdel(Constant.IsDel.NODEL.getValue());
				Date date = new Date();
				sellerClientLinkman.setCreateDate(date);
				
				if (sellerClientLinkman.getId().matches("[0-9]{1,}")){
					sellerClientLinkmanMapper.updateLinkmanById(sellerClientLinkman);
				}else{
					sellerClientLinkman.setId(ShiroUtils.getUid());
					sellerClientLinkmanMapper.saveSellerClientLinkman(sellerClientLinkman);
				}
			}
		}
		
	}

	/**
	 * 删除客户联系人
	 * @param
	 */
	public void deleteSellerClientLinkman(String linkmanId){
		if (linkmanId != null){
	
			sellerClientLinkmanMapper.deleteSellerClientLinkman(linkmanId);
		}
	}
	
	/**
	 * 查询联系人详情
	 * @param id
	 * @return
	 */
	public SellerClientLinkman selectByPrimaryKey(String id){
		return sellerClientLinkmanMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 修改保存收货地址
	 * @param shippingAddress
	 */
	public JSONObject saveUpdate(SellerClientLinkman address) {
		JSONObject json = new JSONObject();
		int result = 0;	
		result = sellerClientLinkmanMapper.updateLinkmanAddressById(address);
		if(result > 0){
			json.put("success", true);
			json.put("msg", "保存成功！");
			address = selectByPrimaryKey(address.getId());
			json.put("address", address);
		}else{
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}


	public JSONObject saveAdd(SellerClientLinkman address,Map<String,Object> map) {
		JSONObject json = new JSONObject();
		int result = 0;
		Date date = new Date();
		address.setId(ShiroUtils.getUid());
		address.setSellerClientId(map.get("sellerClientId").toString());
		address.setIsdel(Constant.IsDel.NODEL.getValue());
		address.setCreateDate(date);
		result = sellerClientLinkmanMapper.saveSellerClientLinkman(address);
		if(result > 0){
			json.put("success", true);
			json.put("msg", "保存成功！");
			address = selectByPrimaryKey(address.getId());
			json.put("address", address);
		}else{
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}
}
