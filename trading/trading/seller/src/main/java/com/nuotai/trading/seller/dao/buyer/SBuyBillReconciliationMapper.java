package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliation;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface SBuyBillReconciliationMapper extends BaseDao<SBuyBillReconciliation>{
	//查询账单周期列表
	List<SBuyBillReconciliation> getBillReconciliationList(SearchPageUtil searchPageUtil);

	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);

	//添加账单对账数据
	int saveBillReconciliationInfo(SBuyBillReconciliation buyBillReconciliation);

	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);

	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);

    int deleteByPrimaryKey(String id);

    int insert(SBuyBillReconciliation record);

    int insertSelective(SBuyBillReconciliation record);

    //根据条件查询账单详细
    SBuyBillReconciliation selectByPrimaryKey(Map<String, Object> map);

    //根据编号修改账单状态
    int updateByPrimaryKeySelective(SBuyBillReconciliation record);

    int updateByPrimaryKey(SBuyBillReconciliation record);
}