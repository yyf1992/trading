package com.nuotai.trading.seller.model.buyer;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;


/**
 * 
 * 售后服务明细表
 * @author "dxl
 * @date 2017-09-12 16:49:24
 */
@Data
public class SBuyCustomerItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后id
	private String customerId;
	//采购计划申请单号
	private String applyCode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String skuOid;
	//单位
	private String unitId;
	//发货号
	private String deliveryCode;
	//发货明细id
	private String deliveryItemId;
	//换货/退款退货数量
	private Integer goodsNumber;
	//卖家确认数量
	private Integer confirmNumber;
	//价格
	private BigDecimal price;
	//收货数量
	private Integer receiveNumber;
	private String wareHouseId;
	private String isNeedInvoice;
	private String remark;
	//返修金额
	private BigDecimal repairPrice;
	//换货到货数量
	private Integer exchangeArrivalNum;
	//换货单价
	private BigDecimal exchangePrice;
}
