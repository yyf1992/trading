package com.nuotai.trading.seller.service.buyer;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.buyer.SBuyBillCycleManagementNewMapper;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagementNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class SBuyBillCycleNewService {

    private static final Logger LOG = LoggerFactory.getLogger(SBuyBillCycleNewService.class);

	@Autowired
	private SBuyBillCycleManagementNewMapper buyBillCycleManagementNewMapper;
	//@Autowired
	//private SBuyBillInterestNewService buyBillInterestNewService;
	//@Autowired
	//private BSellerBillCycleManagementNewService bSellerBillCycleManagementNewService;

	//根据编号查询修改数据
	public SBuyBillCycleManagementNew queryBillCycleByNewId(String id){
		return buyBillCycleManagementNewMapper.queryBillCycleByNewId(id);
	}

	//修改申请数据列表查询
	public List<SBuyBillCycleManagementNew> queryListNewBillCycle(Map<String, Object> map){
		return buyBillCycleManagementNewMapper.queryListNewBillCycle(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillCycleManagementNewMapper.queryCount(map);
	}

	//添加修改申请
	public int addNewBillCycle(SBuyBillCycleManagementNew buyBillCycleManagementNew){
		return buyBillCycleManagementNewMapper.addNewBillCycle(buyBillCycleManagementNew);
	}

	//修改申请数据状态
	public int updateNewBillCycle(SBuyBillCycleManagementNew buyBillCycleManagementNew){
		return buyBillCycleManagementNewMapper.updateNewBillCycle(buyBillCycleManagementNew);
	}

	public void delete(String id){
		buyBillCycleManagementNewMapper.delete(id);
	}

}
