package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillInterest;
import com.nuotai.trading.seller.model.SellerBillInterestOld;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author "
 * @date 2017-09-08 20:28:40
 */
@Component("sellerBillInterestOldMapper")
public interface SellerBillInterestOldMapper extends BaseDao<SellerBillInterestOld> {
	//添加数据新关联利息
    int addNewInterestOld(SellerBillInterestOld buyBillInterestOld);
    //查询最近历史数据
    List<SellerBillInterestOld> queryBillInterestOld(String id);
    //根据账单编号删除关联历史利息
    int deleteNewInterestOld(String id);
}
