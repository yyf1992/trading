package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyAttachmentMapper;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.dao.WmsInputplanLogMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.model.wms.WmsInputplanLog;
import com.nuotai.trading.seller.dao.*;
import com.nuotai.trading.seller.dao.buyer.SBuyCustomerMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordItemMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderProductMapper;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecord;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.BuyOrder;
import com.nuotai.trading.seller.model.buyer.BuyOrderProduct;
import com.nuotai.trading.seller.model.buyer.SBuyCustomer;
import com.nuotai.trading.utils.*;
import com.nuotai.trading.utils.qrcode.QRCodeUtil;
import com.oms.client.OMSResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.*;


/**
 * The type Seller delivery record service.
 */
@Service
@Transactional
public class SellerDeliveryRecordService {

	private static final Logger log = LoggerFactory.getLogger(SellerDeliveryRecordService.class);

	@Autowired
	private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;
	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;
	@Autowired
	private SellerOrderMapper sellerOrderMapper;
	@Autowired
	private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;
	@Autowired
	private SellerLogisticsMapper sellerLogisticsMapper;
	@Autowired
	private SBuyDeliveryRecordMapper buyDeliveryRecordMapper;
	@Autowired
	private SBuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;
	@Autowired
	private SBuyOrderMapper sBuyOrderMapper;
	@Autowired
	private SBuyOrderProductMapper sBuyOrderProductMapper;
	@Autowired
	private TBusinessWhareaMapper tBusinessWhareaMapper;
	@Autowired
	private BuyAttachmentMapper buyAttachmentMapper;
	@Autowired
	private WmsInputplanLogMapper wmsInputplanLogMapper;
	@Autowired
	private SBuyCustomerMapper sBuyCustomerMapper;
	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;
	@Autowired
	private SellerCustomerItemMapper sellerCustomerItemMapper;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	
	/**
	 * Get seller delivery record.
	 *
	 * @param id the id
	 * @return the seller delivery record
	 */
	public SellerDeliveryRecord get(String id){
		return sellerDeliveryRecordMapper.get(id);
	}

	/**
	 * Query list list.
	 *
	 * @param map the map
	 * @return the list
	 */
	public List<SellerDeliveryRecord> queryList(Map<String, Object> map){
		return sellerDeliveryRecordMapper.queryList(map);
	}

	/**
	 * Query count int.
	 *
	 * @param map the map
	 * @return the int
	 */
	public int queryCount(Map<String, Object> map){
		return sellerDeliveryRecordMapper.queryCount(map);
	}

	/**
	 * Add.
	 *
	 * @param sellerDeliveryRecord the seller delivery record
	 */
	public void add(SellerDeliveryRecord sellerDeliveryRecord){
		sellerDeliveryRecordMapper.add(sellerDeliveryRecord);
	}

	/**
	 * Update.
	 *
	 * @param sellerDeliveryRecord the seller delivery record
	 */
	public void update(SellerDeliveryRecord sellerDeliveryRecord){
		sellerDeliveryRecordMapper.update(sellerDeliveryRecord);
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(String id){
		sellerDeliveryRecordMapper.delete(id);
	}

	/**
	 * 发货单保存，返回发货单号
	 *
	 * @param params the params
	 * @throws Exception the exception
	 */
	public void saveDeliveryRecord(Map<String, Object> params) throws Exception{
		//解析发货分录信息
		List<SellerDeliveryRecordItem> deliveryItemList = JSONObject.parseArray(String.valueOf(params.get("deliveryItems")),SellerDeliveryRecordItem.class);
		//代发货方式
		String dropshipType = String.valueOf(params.get("dropshipType"));
		//收货地址
		String receiveAddr = String.valueOf(params.get("receiveAddr"));
		//发货类型：0-采购发货，1-换货发货
		String deliverType = String.valueOf(params.get("deliverType"));
		
		//根据不同的仓库，分类数据
		Map<String,List<SellerDeliveryRecordItem>> mapCK = new HashMap<>();
		//生成的发货单号存放，用于生成发货单二维码用
		List<String> deliverNoList = new ArrayList<>();
		for (SellerDeliveryRecordItem item:deliveryItemList) {
			String warehouseId = item.getWarehouseId();
			if(mapCK.containsKey(warehouseId)){
				List<SellerDeliveryRecordItem> list = mapCK.get(warehouseId);
				list.add(item);
				mapCK.put(warehouseId,list);
			}else {
				List<SellerDeliveryRecordItem> list = new ArrayList<>();
				list.add(item);
				mapCK.put(warehouseId,list);
			}
		}
		//1.先保存物流信息（多个发货单可能对应一个物流信息，因此就对应同一个运费）
		String logisticsId = this.saveLogistics(params);
		//循环分类数据，生成不同的发货单
		int count = 1;
		String deliverNo = "";
		if("1".equals(deliverType)){
			//换货发货
			deliverNo = ShiroUtils.getHuanHuoDeliveryNum();
		}else{
			//采购发货
			if(!ObjectUtil.isEmpty(dropshipType)&&!"0".equals(dropshipType)){
				//代发货
				deliverNo = ShiroUtils.getDaiDeliveryNum();
			}else{
				deliverNo = ShiroUtils.getDeliveryNum();
			}
		}
		for(String key:mapCK.keySet()){
			/****************保存发货单主表 begin****************/
			SellerDeliveryRecord deliveryRecord = new SellerDeliveryRecord();
			deliveryRecord.setId(ShiroUtils.getUid());
			deliveryRecord.setCompanyId(ShiroUtils.getCompId());
			//如果有不同仓库，则发货单号增加后缀"_count",否则不增加后缀
			if(mapCK.size()>1){
				deliveryRecord.setDeliverNo(deliverNo+"_"+String.valueOf(count));
				deliverNoList.add(deliverNo+"_"+String.valueOf(count));
				count++;
			}else {
				deliveryRecord.setDeliverNo(deliverNo);
				deliverNoList.add(deliverNo);
			}
			deliveryRecord.setDeliverType(Integer.parseInt(String.valueOf(params.get("deliverType"))));
			deliveryRecord.setSupplierId(ShiroUtils.getCompId());
			deliveryRecord.setSupplierName(ShiroUtils.getCompanyName());
			deliveryRecord.setBuycompanyId(String.valueOf(params.get("buyCompanyId")));
			//待发货
			deliveryRecord.setRecordstatus(Constant.DeliveryRecordStatus.WAITDELIVERY.getValue());
			//未对账
			deliveryRecord.setReconciliationStatus("0");
			//未开票
			deliveryRecord.setIsInvoiceStatus("0");
			//未付款
			deliveryRecord.setIsPaymentStatus("0");
			deliveryRecord.setIsDel(Constant.IsDel.NODEL.getValue());
			deliveryRecord.setCreateDate(new Date());
			deliveryRecord.setCreaterId(ShiroUtils.getUserId());
			deliveryRecord.setCreaterName(ShiroUtils.getUserName());
			deliveryRecord.setLogisticsId(logisticsId);
			deliveryRecord.setDropshipType(dropshipType);
			deliveryRecord.setReceiveAddr(receiveAddr);
			this.add(deliveryRecord);
			/****************保存发货单主表 end****************/
			/****************保存发货单明细表 begin****************/
			List<SellerDeliveryRecordItem> mapCKItemList = mapCK.get(key);
			if("0".equals(deliverType)){
				for (SellerDeliveryRecordItem deliveryItem : mapCKItemList) {
					//根据deliveryItem查询订单商品，如果订单商品是多个，则要分摊发货数量
					String sellerOrderId = deliveryItem.getOrderId();
					String barcode = deliveryItem.getBarcode();
					Map<String,Object> productMap = new HashMap<>(2);
					productMap.put("orderId",sellerOrderId);
					productMap.put("barcode",barcode);
					List<SellerOrderSupplierProduct> productList = sellerOrderSupplierProductMapper.queryList(productMap);
					if(!ObjectUtil.isEmpty(productList)&&productList.size()==1){
						deliveryItem.setId(ShiroUtils.getUid());
						deliveryItem.setRecordId(deliveryRecord.getId());
						//如果是代发仓，则发货明细的仓库改变
						if(!ObjectUtil.isEmpty(dropshipType)&&!"0".equals(dropshipType)){
							deliveryItem.setWarehouseId("52");//代发仓，这里写死
						}
						sellerDeliveryRecordItemMapper.add(deliveryItem);
					}else {
						int fahuoNum = deliveryItem.getDeliveryNum();
						for (SellerOrderSupplierProduct product:productList) {
							int lastNum = product.getOrderNum()-product.getDeliveredNum()-product.getWaitDeliveryNum();
							if(lastNum>0&&fahuoNum>0){
								SellerDeliveryRecordItem addItem = new SellerDeliveryRecordItem();
								addItem.setId(ShiroUtils.getUid());
								addItem.setRecordId(deliveryRecord.getId());
								addItem.setOrderId(product.getOrderId());
								addItem.setOrderItemId(product.getId());
								addItem.setBuyOrderId(product.getBuyOrderId());
								addItem.setBuyOrderItemId(product.getBuyOrderItemId());
								addItem.setApplyCode(product.getApplyCode());
								addItem.setProductCode(product.getProductCode());
								addItem.setProductName(product.getProductName());
								addItem.setBarcode(product.getBarcode());
								addItem.setSkuCode(product.getSkuCode());
								addItem.setSkuName(product.getSkuName());
								addItem.setUnitId(product.getUnitId());
								addItem.setSalePrice(product.getPrice());
								addItem.setWarehouseId(product.getWarehouseId());
								if(fahuoNum-lastNum<0){
									addItem.setDeliveryNum(fahuoNum);
									addItem.setStatus(1);
									fahuoNum = 0;
								}else {
									addItem.setDeliveryNum(lastNum);
									addItem.setStatus(0);
									fahuoNum = fahuoNum - lastNum;
								}
								addItem.setIsNeedInvoice(product.getIsNeedInvoice());
								addItem.setRemark(deliveryItem.getRemark());
								//如果是代发仓，则发货明细的仓库改变
								if(!ObjectUtil.isEmpty(dropshipType)&&!"0".equals(dropshipType)){
									addItem.setWarehouseId("52");//代发仓，这里写死
								}
								sellerDeliveryRecordItemMapper.add(addItem);
							}
						}
					}
					//修改采购订单的发货状态
					SellerOrder order = new SellerOrder();
					order.setId(sellerOrderId);
					order.setStatus(2);
					sellerOrderMapper.updateByPrimaryKeySelective(order);
				}
			}else{
				//换货发货
				for (SellerDeliveryRecordItem deliveryItem : mapCKItemList) {
					SellerDeliveryRecordItem addItem = new SellerDeliveryRecordItem();
					addItem.setId(ShiroUtils.getUid());
					addItem.setDeliverNo(deliveryRecord.getDeliverNo());
					addItem.setRecordId(deliveryRecord.getId());
					addItem.setOrderId(deliveryItem.getOrderId());
					addItem.setOrderItemId(deliveryItem.getOrderItemId());
					addItem.setBuyOrderId(deliveryItem.getBuyOrderId());
					addItem.setBuyOrderItemId(deliveryItem.getBuyOrderItemId());
					addItem.setApplyCode(deliveryItem.getApplyCode());
					addItem.setProductCode(deliveryItem.getProductCode());
					addItem.setProductName(deliveryItem.getProductName());
					addItem.setBarcode(deliveryItem.getBarcode());
					addItem.setSkuCode(deliveryItem.getSkuCode());
					addItem.setSkuName(deliveryItem.getSkuName());
					addItem.setUnitId(deliveryItem.getUnitId());
					addItem.setSalePrice(deliveryItem.getSalePrice());
					addItem.setWarehouseId("53");//质检仓在买卖系统中id是53
					addItem.setDeliveryNum(deliveryItem.getDeliveryNum());
					addItem.setIsNeedInvoice("0");
					addItem.setRemark(deliveryItem.getRemark());
					//如果是代发仓，则发货明细的仓库改变
					if(!ObjectUtil.isEmpty(dropshipType)&&!"0".equals(dropshipType)){
						addItem.setWarehouseId("52");//代发仓，这里写死
					}
					sellerDeliveryRecordItemMapper.add(addItem);
				}
			}
			/****************保存发货单明细表 end****************/
		}
		/**********生成发货单二维码***********/
		String content = Constant.TRADE_URL+"platform/buyer/receive/openQRcode?deliverNo=";
		//目标地址
		String destPath = Constant.QRCODE_SAVE_URL;
		for (String fhNo:deliverNoList) {
			try{
				System.out.println("content=["+content+fhNo+"]");
				String fineName = QRCodeUtil.encode(content+fhNo,"",destPath,true);
				String qrcodeAddr = Constant.QRCODE_VIEW_URL+fineName;
				if(!ObjectUtil.isEmpty(qrcodeAddr)){
					sellerDeliveryRecordMapper.addQrcodeAddr(fhNo,qrcodeAddr);
				}
			}catch (Exception e) {
				System.out.println("发货单号："+fhNo+"创建二维码失败！");
				log.error("发货单号："+fhNo+"创建二维码失败！"+e.getMessage());
			}
		}
	}

	public static void main(String[] args) {
		//测试往WMS推送入库计划
		/*List<String> deliverNoList = new ArrayList<>();
		deliverNoList.add("FH3472620155894491");
		ExecutorService exec = Executors.newSingleThreadExecutor();
		// 向wms推送入库申请单
		Future<String> future = exec.submit(new createQRCode(deliverNoList));
		try{
			//任务处理超时时间设为 5
			String rst = future.get(1000 * 10, TimeUnit.MILLISECONDS);
			System.out.println(rst);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}*/
	}

	/**
	 * 保存物流信息
	 *
	 * @param params     the params
	 */
	public String saveLogistics(Map<String, Object> params){
		SellerLogistics logistics = JSONObject.parseObject(String.valueOf(params.get("logistics")),SellerLogistics.class);
		logistics.setId(ShiroUtils.getUid());
		//由于存在多个发货单对应一个物流信息，因此将物流信息保存在了发货单里，这里的发货单ID不再保存
		logistics.setDeliveryId("");
		logistics.setIsDel(Constant.IsDel.NODEL.getValue());
		logistics.setCreateDate(new Date());
		logistics.setCreateId(ShiroUtils.getUserId());
		logistics.setCreateName(ShiroUtils.getUserName());
		sellerLogisticsMapper.add(logistics);
		return logistics.getId();
	}


	/**
	 * Gets delivery record num.
	 * 获得不同状态发货单数
	 *
	 * @param params the params
	 */
	public void getDeliveryRecordNum(Map<String, Object> params) {
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("supplierId", ShiroUtils.getCompId());//供应商ID
		selectMap.put("isDel", Constant.IsDel.NODEL.getValue());
		//所有
		selectMap.put("recordstatus", "");
		int allNum = sellerDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("allNum", allNum);
		//待发货
		selectMap.put("recordstatus", "0");
		int waitDeliveryNum = sellerDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("waitDeliveryNum", waitDeliveryNum);
		//发货中货物
		selectMap.put("recordstatus", "1");
		int deliveringNum = sellerDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("deliveringNum", deliveringNum);
		//已到货
		selectMap.put("recordstatus", "2");
		int receivedNum = sellerDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("receivedNum", receivedNum);
		//已作废
		selectMap.put("recordstatus", "3");
		int invalidNum = sellerDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("invalidNum", invalidNum);

		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			default:
				params.put("interest", "0");
				break;
		}
	}

	/**
	 * Load delivery record data search page util.
	 * 加载发货单列表数据
	 *
	 * @param searchPageUtil the search page util
	 * @param params         the params
	 * @return the search page util
	 */
	public SearchPageUtil loadDeliveryRecordData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("supplierId", ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			default:
				params.put("interest", "0");
				break;
		}
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待发货
			params.put("recordstatus", "0");
		}else if("2".equals(interest)){
			//发货中货物
			params.put("recordstatus", "1");
		}else if("3".equals(interest)){
			//已到货
			params.put("recordstatus", "2");
		}else if("4".equals(interest)){
			//已作废
			params.put("recordstatus", "3");
		}
		searchPageUtil.setObject(params);
		List<SellerDeliveryRecord> deliveryList = sellerDeliveryRecordMapper.selectByPage(searchPageUtil);
		if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
			for(SellerDeliveryRecord delivery : deliveryList){
				String deliveryId = delivery.getId();
				Map<String,Object> map = new HashMap<>();
				map.put("deliveryId",deliveryId);
				List<SellerDeliveryRecordItem> itemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(map);
				/********发货明细按照条形码合并 begin***********/
				Map<String,SellerDeliveryRecordItem> sdItemMap = new HashMap<>();
				Map<String,Integer> orderNumMap = new HashMap<>();
				List<SellerDeliveryRecordItem> addList = new ArrayList<>();
				for (SellerDeliveryRecordItem item:itemList) {
					if(sdItemMap.containsKey(item.getBarcode())){
						SellerDeliveryRecordItem oldItem = sdItemMap.get(item.getBarcode());
						//发货数量合计
						int oldDeliveryNum = ObjectUtil.isEmpty(oldItem.getDeliveryNum())?0:oldItem.getDeliveryNum();
						int deliveryNum = ObjectUtil.isEmpty(item.getDeliveryNum())?0:item.getDeliveryNum();
						item.setDeliveryNum(oldDeliveryNum+deliveryNum);
						//到货数量合计
						int oldArrivalNum = ObjectUtil.isEmpty(oldItem.getArrivalNum())?0:oldItem.getArrivalNum();
						int arrivalNum = ObjectUtil.isEmpty(item.getArrivalNum())?0:item.getArrivalNum();
						item.setArrivalNum(oldArrivalNum+arrivalNum);
						//判断部分发货还是全部发货,如果有部分发货，则该发货明细合并之后也是部分发货
						if(oldItem.getStatus()==1){
							item.setStatus(1);
						}
						//订单号拼接
						String orderCode = oldItem.getOrderCode();
						if(!orderCode.contains(item.getOrderCode())){
							orderCode=orderCode+","+item.getOrderCode();
						}
						item.setOrderCode(orderCode);
					}
					//获取该发货单下相同订单号的商品，用于计算订单数量
					Map<String,Object> orderItemMap = new HashMap<>(2);
					orderItemMap.put("barcode",item.getBarcode());
					orderItemMap.put("orderId",item.getOrderId());
					List<SellerOrderSupplierProduct> orderItemList = sellerOrderSupplierProductMapper.queryList(orderItemMap);
					int orderNumSum = 0;
					for (SellerOrderSupplierProduct orderItem:orderItemList) {
						orderNumSum += orderItem.getOrderNum()==null?0:orderItem.getOrderNum();
					}
					item.setOrderNum(orderNumSum);
					sdItemMap.put(item.getBarcode(),item);
				}
				for (Map.Entry<String,SellerDeliveryRecordItem> entry : sdItemMap.entrySet()){
					addList.add(entry.getValue());
				}
				delivery.setItemList(addList);
				/********发货明细按照条形码合并 end***********/
				//设置收货凭证信息
				Map<String,Object> attachmentMap = new HashMap<>(1);
				attachmentMap.put("relatedId",delivery.getDeliverNo());
				List<BuyAttachment> attachmentList = buyAttachmentMapper.selectByMap(attachmentMap);
				delivery.setReceiptVoucherList(attachmentList);
				/********判断是否已经推送到WMS***********/
				WmsInputplanLog log = wmsInputplanLogMapper.getByDeliverNo(delivery.getDeliverNo());
				if(!ObjectUtil.isEmpty(log)){
					delivery.setIsSendWms(log.getResult());
				}else {
					delivery.setIsSendWms("Y");
				}
			}
		}
		searchPageUtil.getPage().setList(deliveryList);
		return searchPageUtil;
	}


	/**
	 * Confirm delivery.
	 * 确认发货
	 *
	 * @param deliveryId the delivery id
	 */
	public void confirmDelivery(String deliveryId){
		ExecutorService exec = Executors.newSingleThreadExecutor();
		//更新发货单发货状态
		SellerDeliveryRecord deliveryRecord = new SellerDeliveryRecord();
		deliveryRecord.setId(deliveryId);
		//已发货
		deliveryRecord.setRecordstatus(1);
		deliveryRecord.setSendDate(new Date());
		sellerDeliveryRecordMapper.updateByPrimaryKeySelective(deliveryRecord);
		//发货之后确认订单是否发货完成
		deliveryRecord = sellerDeliveryRecordMapper.get(deliveryId);
		Map<String,Object> itemMap = new HashMap<>(1);
		itemMap.put("deliveryId",deliveryId);
		List<SellerDeliveryRecordItem> deliveryItemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(itemMap);
		if(deliveryRecord.getDeliverType()==0){
			if(!ObjectUtil.isEmpty(deliveryItemList)&&deliveryItemList.size()>0){
				for (SellerDeliveryRecordItem deliveryItem : deliveryItemList) {
					//获取订单商品信息
					SellerOrderSupplierProduct orderProduct = sellerOrderSupplierProductMapper.selectByPrimaryKey(deliveryItem.getOrderItemId());
					//本次发货数量
					int deliveryNum = ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum();
					//更新卖家订单详情中的已发货数量
					sellerOrderSupplierProductMapper.updateDeliveredNum(orderProduct.getId(),deliveryNum);
					//更新买家订单详情中的已发货数量
					BuyOrderProduct buyOrderProduct = sBuyOrderProductMapper.selectByPrimaryKey(orderProduct.getBuyOrderItemId());
					if(!ObjectUtil.isEmpty(buyOrderProduct)){
						sBuyOrderProductMapper.updateDeliveredNum(buyOrderProduct.getId(),deliveryNum);
					}
				}
			}
			//更新订单主表中的状态
			this.updateSellerOrderStatus(deliveryItemList,deliveryRecord);
		}
		//保存发货信息到采购商
		this.saveBuyDeliveryRecord(deliveryRecord,deliveryItemList);
		try{
			// 向wms推送入库申请单
			deliveryRecord.setItemList(deliveryItemList);
			Future<String> future = exec.submit(new PutInputPlanToWms(deliveryRecord));
			//任务处理超时时间设为 10s
			String rst = future.get(1000 * 10, TimeUnit.MILLISECONDS);
			System.out.println("向wms推送入库申请单结果"+rst);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			log.error("入库计划推送到WMS超时["+deliveryRecord.getDeliverNo()+"]");
		}
	}



	/**
	 * Gets export data.
	 * 获取发货导出数据
	 * @param params the params
	 * @return the export data
	 */
	public List<SellerDeliveryRecord> getExportData(Map<String, Object> params) {
		params.put("supplierId", ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			default:
				params.put("interest", "0");
				break;
		}
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待发货
			params.put("recordstatus", "0");
		}else if("2".equals(interest)){
			//发货中货物
			params.put("recordstatus", "1");
		}else if("3".equals(interest)){
			//已到货
			params.put("recordstatus", "2");
		}else if("4".equals(interest)){
			//已作废
			params.put("recordstatus", "3");
		}
		List<SellerDeliveryRecord> deliveryList = sellerDeliveryRecordMapper.selectByMap(params);
		if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
			for(SellerDeliveryRecord delivery : deliveryList){
				String deliveryId = delivery.getId();
				Map<String,Object> map = new HashMap<>();
				map.put("deliveryId",deliveryId);
				List<SellerDeliveryRecordItem> itemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(map);
				/********发货明细按照条形码合并 begin***********/
				Map<String,SellerDeliveryRecordItem> sdItemMap = new HashMap<>();
				Map<String,Integer> orderNumMap = new HashMap<>();
				List<SellerDeliveryRecordItem> addList = new ArrayList<>();
				for (SellerDeliveryRecordItem item:itemList) {
					if(sdItemMap.containsKey(item.getBarcode())){
						SellerDeliveryRecordItem oldItem = sdItemMap.get(item.getBarcode());
						item.setDeliveryNum(oldItem.getDeliveryNum()+item.getDeliveryNum());
					}
					//获取该发货单下相同订单号的商品，用于计算订单数量
					Map<String,Object> orderItemMap = new HashMap<>(2);
					orderItemMap.put("barcode",item.getBarcode());
					orderItemMap.put("orderId",item.getOrderId());
					List<SellerOrderSupplierProduct> orderItemList = sellerOrderSupplierProductMapper.queryList(orderItemMap);
					int orderNumSum = 0;
					for (SellerOrderSupplierProduct orderItem:orderItemList) {
						orderNumSum += orderItem.getOrderNum()==null?0:orderItem.getOrderNum();
					}
					item.setOrderNum(orderNumSum);
					sdItemMap.put(item.getBarcode(),item);
				}
				for (Map.Entry<String,SellerDeliveryRecordItem> entry : sdItemMap.entrySet()){
					addList.add(entry.getValue());
				}
				delivery.setItemList(addList);
			}
		}
		return deliveryList;
    }


	/**
	 * Send wms input plan.
	 * 推送WMS入库计划，用于确认发货时推送失败的数据
	 * @param deliverNo the delivery no.
	 */
	public void sendWmsInputPlan (String deliverNo) throws Exception {
		Map<String,Object> map = new HashMap<>(2);
		WmsInputplanLog log = wmsInputplanLogMapper.getByDeliverNo(deliverNo);
		if(!ObjectUtil.isEmpty(log)){
			OMSResponse result = InterfaceUtil.NuotaiWmsClient(log.getMethod(),log.getToken(), net.sf.json.JSONObject.fromObject(log.getContent()));
			System.out.println("result="+result.isSuccess());
			SellerDeliveryRecord record = sellerDeliveryRecordMapper.getByDeliverNo(deliverNo);
			if (result.isSuccess()) {
				log.setResult("Y");
				log.setErrorMsg("");
				record.setPushType("T");
				sellerDeliveryRecordMapper.updateByPrimaryKeySelective(record);
			}else {
				log.setInterfaceUrl(Constant.WMS_INTERFACE_URL);
				log.setErrorMsg(result.getMsg());
			}
			wmsInputplanLogMapper.update(log);
		}
	}

	static class TestCall implements Callable<String>{

		@Override
		public String call() throws Exception {
			Map<String,Object> map = new HashMap<>();
			net.sf.json.JSONObject header = new net.sf.json.JSONObject();
			header.put("title","采购入库");
			header.put("deliverycode","FHtest2");
			header.put("suppliername","liuhui1");
			map.put("header",header);
			map.put("whcode","49");
			map.put("creater", "liuhui");
			// 设置json中date类型转换格式
			List<net.sf.json.JSONObject> list = new ArrayList<>();
			net.sf.json.JSONObject detail = new net.sf.json.JSONObject();
			detail.put("procode", "001");
			detail.put("proname", "护膝");
			detail.put("skucode", "XL");
			detail.put("skuname", "大号");
			detail.put("skuoid", "bar0001");
			detail.put("warehouseCode", "49");
			list.add(detail);
			map.put("iteamList", list );
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(map);
			net.sf.json.JSONObject headerJson = jsonObj.getJSONObject("header");
			net.sf.json.JSONArray itemListJson = jsonObj.getJSONArray("iteamList");
			// 调用接口
			OMSResponse result = InterfaceUtil.NuotaiWmsClient("client/addDeliveryrecord","123", jsonObj);
			System.out.println("result:"+result);
			System.out.println("result.isSuccess()="+result.isSuccess());
			return result.toString();
		}
	}
	/**
	 * The type Put input plan to wms.
	 * 推送发货单到WMS
	 * 如果发货单明细中存在京东仓和菜鸟仓，则入库计划要去除掉该明细
	 */
	class PutInputPlanToWms implements Callable<String>{
		private SellerDeliveryRecord deliveryRecord;
		private String rst="";
		public PutInputPlanToWms(SellerDeliveryRecord deliveryRecord) {
			this.deliveryRecord = deliveryRecord;
		}
		@Override
		public String call() {
			//日志
			WmsInputplanLog log = new WmsInputplanLog();
			log.setId(ShiroUtils.getUid());
			//总Map
			Map<String, Object> map = new HashMap<String, Object>(4);
			//创建人
			String creater = "";
			//仓库编号
			String whcode = "";
			//组装入库申请单表头
			net.sf.json.JSONObject header = new net.sf.json.JSONObject();
			if(!ObjectUtil.isEmpty(deliveryRecord)){
				header.put("title",deliveryRecord.getDeliverType()==0?"采购入库":"买卖系统换货入库");
				header.put("deliverycode",deliveryRecord.getDeliverNo());
				header.put("suppliername",deliveryRecord.getSupplierName());
			}
			map.put("header", header);
			//发货详情
			List<SellerDeliveryRecordItem> itemList = deliveryRecord.getItemList();
			/***************判断代发货类型,正常发货根据发货明细的仓库处理，否则推送到代发仓********************/
			if(!ObjectUtil.isEmpty(itemList)&&itemList.size()>0){
				String dropshipType = ObjectUtil.isEmpty(deliveryRecord.getDropshipType())?"0":deliveryRecord.getDropshipType();
				if("0".equals(dropshipType)){
					//判断仓库是外仓，无需推送
					boolean isSend = true;
					for (SellerDeliveryRecordItem item:itemList){
						String warehouseId = item.getWarehouseId();
						TBusinessWharea warehouse = tBusinessWhareaMapper.getWharea(warehouseId);
						String warehouseCode = "";
						if(!ObjectUtil.isEmpty(warehouse)&&!ObjectUtil.isEmpty(warehouse.getWhareaCode())){
							warehouseCode = warehouse.getWhareaCode();
						}
						whcode = warehouseCode;
						if(ObjectUtil.isEmpty(warehouseCode)||"CN0001".equalsIgnoreCase(warehouseCode)||"JD0001".equalsIgnoreCase(warehouseCode)){
							isSend = false;
							break;
						}
					}
					if(!isSend){
						return "仓库是外仓，无需推送WMS...";
					}
				}else {
					whcode = "0004";//代发仓编号，这里写死
				}

			}
			Map<String,net.sf.json.JSONObject> goodsMap = new HashMap<String,net.sf.json.JSONObject>();
			//移除掉京东仓和菜鸟仓
			for (SellerDeliveryRecordItem sdItem : itemList) {
				//获取采购订单，组装创建人
				if(deliveryRecord.getDeliverType()==0){
					//采购
					BuyOrder bo = sBuyOrderMapper.selectByPrimaryKey(sdItem.getBuyOrderId());
					if(!ObjectUtil.isEmpty(bo)&&!ObjectUtil.isEmpty(bo.getCreateName())&&!creater.contains(bo.getCreateName())){
						creater += bo.getCreateName()+",";
					}
				}else if(deliveryRecord.getDeliverType()==1){
					//换货
					SBuyCustomer sbc = sBuyCustomerMapper.get(sdItem.getBuyOrderId());
					if(!ObjectUtil.isEmpty(sbc)&&!ObjectUtil.isEmpty(sbc.getCreateName())&&!creater.contains(sbc.getCreateName())){
						creater += sbc.getCreateName()+",";
					}
				}
				//组装入库计划明细
				String skuoid = sdItem.getBarcode();
				long skucount = sdItem.getDeliveryNum();
				net.sf.json.JSONObject detail = new net.sf.json.JSONObject();
				if(goodsMap.containsKey(skuoid)){
					detail = goodsMap.get(skuoid);
					skucount += detail.getLong("skucount");
				}else{
					detail.put("procode", sdItem.getProductCode());
					detail.put("proname", sdItem.getProductName());
					detail.put("skucode", sdItem.getSkuCode());
					detail.put("skuname", sdItem.getSkuName());
					detail.put("skuoid", sdItem.getBarcode());
				}
				detail.put("skucount", skucount);
				goodsMap.put(skuoid, detail);
			}
			if(creater.length()>0){
				creater = creater.substring(0,creater.length()-1);
			}
			map.put("creater", creater);
			map.put("whcode",whcode);
			List<net.sf.json.JSONObject> itemJsonList = new ArrayList<>();
			for(Map.Entry<String,net.sf.json.JSONObject> goods : goodsMap.entrySet()){
				itemJsonList.add(goods.getValue());
			}
			map.put("iteamList", itemJsonList);
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(map);
			// 调用接口
			System.out.println("向WMS推送数据：["+jsonObj.toString()+"]");
			OMSResponse result = null;
			try {
				result = InterfaceUtil.NuotaiWmsClient("client/addDeliveryrecord",String.valueOf(deliveryRecord.getSupplierId()), jsonObj);
				if (result.isSuccess()) {
					rst = "推送入库申请单成功！";
					log.setResult("Y");
					deliveryRecord.setPushType("T");
					sellerDeliveryRecordMapper.updateByPrimaryKeySelective(deliveryRecord);
				}else {
					rst = "推送失败！";
					log.setResult("N");
					log.setErrorMsg(result.getMsg());
					deliveryRecord.setPushType("F");
					sellerDeliveryRecordMapper.updateByPrimaryKeySelective(deliveryRecord);
				}
			}catch (TimeoutException e){
				log.setResult("N");
				log.setErrorMsg("连接超时");
			}catch (Exception e) {
				log.setResult("N");
				log.setErrorMsg(e.getMessage());
			}finally {
				log.setDeliverNo(deliveryRecord.getDeliverNo());
				log.setInterfaceUrl(Constant.WMS_INTERFACE_URL);
				log.setMethod("client/addDeliveryrecord");
				log.setToken(String.valueOf(deliveryRecord.getSupplierId()));
				log.setContent(jsonObj.toString());
				wmsInputplanLogMapper.deleteByDeliverNo(deliveryRecord.getDeliverNo());
				wmsInputplanLogMapper.add(log);
			}
			System.out.println("result:"+result);
			System.out.println("result.isSuccess()="+result.isSuccess());
			return rst;
		}
	}


	/**
	 * 更新订单主表中的状态
	 *
	 * @param deliveryItemList the delivery item list
	 */
	public void updateSellerOrderStatus(List<SellerDeliveryRecordItem> deliveryItemList,SellerDeliveryRecord delivery){
		if(!ObjectUtil.isEmpty(deliveryItemList)&&deliveryItemList.size()>0) {
			List<String> orderIds = new ArrayList<>();
			//先根据订单分录获取订单主表ID
			for (SellerDeliveryRecordItem deliveryItem : deliveryItemList) {
				if(!orderIds.contains(deliveryItem.getOrderId())){
					orderIds.add(deliveryItem.getOrderId());
				}
			}
			for (String orderId: orderIds) {
				SellerOrder sellerOrder = sellerOrderMapper.get(orderId);
				int status = 2;
				List<SellerOrderSupplierProduct> itemList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
				//判断明细如果有一条未发货完，则整个订单状态为发货中
				for (SellerOrderSupplierProduct item:itemList) {
					if(item.getOrderNum()-item.getDeliveredNum()<=0){
						status = 3;
					}else {
						status = 2;
						break;
					}
				}
				sellerOrder.setStatus(status);
				sellerOrderMapper.update(sellerOrder);
				//更新买家订单为待收货状态
				String buyOrderId = sellerOrder.getBuyerOrderId();
				BuyOrder bo = sBuyOrderMapper.selectByPrimaryKey(buyOrderId);
				//bo.setId(buyOrderId);
				bo.setStatus(Constant.OrderStatus.WAITRECEIVE.getValue());
				sBuyOrderMapper.updateByPrimaryKeySelective(bo);
				SellerLogistics logistics = sellerLogisticsMapper.get(delivery.getLogisticsId());
				//发送钉钉消息
				String message = "您的采购单【"+bo.getOrderCode()+"】已发货，发货单号：【"+delivery.getDeliverNo()+"】，发货日期：【"+DateUtils.format(delivery.getSendDate())+"】,预计到货日期：【"+(logistics.getExpectArrivalDate()==null?"未设置":DateUtils.format(logistics.getExpectArrivalDate()))+"】";
				DingDingUtils.sendDingDingMessage(bo.getCreateId(),message,"订单发货通知");
			}
		}
	}

	/**
	 * 保存发货信息到采购商
	 *
	 * @param sellerRecord   the seller record
	 * @param sellerItemList the seller item list
	 */
	public void saveBuyDeliveryRecord(SellerDeliveryRecord sellerRecord,List<SellerDeliveryRecordItem> sellerItemList){
		//保存收货单主表
		BuyDeliveryRecord buyDeliveryRecord = new BuyDeliveryRecord();
		buyDeliveryRecord.setId(ShiroUtils.getUid());
		buyDeliveryRecord.setCompanyId(sellerRecord.getBuycompanyId());
		buyDeliveryRecord.setDeliverId(sellerRecord.getId());
		buyDeliveryRecord.setDeliverNo(sellerRecord.getDeliverNo());
		buyDeliveryRecord.setDeliverType(sellerRecord.getDeliverType());
		buyDeliveryRecord.setSupplierId(sellerRecord.getSupplierId());
		buyDeliveryRecord.setSupplierCode(sellerRecord.getSupplierCode());
		buyDeliveryRecord.setSupplierName(sellerRecord.getSupplierName());
		buyDeliveryRecord.setStatus(null);
		buyDeliveryRecord.setRecordstatus(0);
		buyDeliveryRecord.setReconciliationStatus(sellerRecord.getReconciliationStatus());
		buyDeliveryRecord.setIsInvoiceStatus(sellerRecord.getIsInvoiceStatus());
		buyDeliveryRecord.setIsPaymentStatus(sellerRecord.getIsPaymentStatus());
		buyDeliveryRecord.setReceiptvoucherAddr(sellerRecord.getReceiptvoucherAddr());
		buyDeliveryRecord.setReceiptvoucherCheck("0");
		buyDeliveryRecord.setRemark(sellerRecord.getRemark());
		buyDeliveryRecord.setLogisticsId(sellerRecord.getLogisticsId());
		buyDeliveryRecord.setQrcodeAddr(sellerRecord.getQrcodeAddr());
		buyDeliveryRecord.setIsDel(0);
		buyDeliveryRecord.setCreaterId(ShiroUtils.getUserId());
		buyDeliveryRecord.setCreateDate(new Date());
		buyDeliveryRecord.setCreaterName(ShiroUtils.getUserName());
		buyDeliveryRecord.setDropshipType(sellerRecord.getDropshipType());
		buyDeliveryRecord.setReceiveAddr(sellerRecord.getReceiveAddr());
		buyDeliveryRecordMapper.add(buyDeliveryRecord);
		//保存收货单分录表
		for (SellerDeliveryRecordItem sellerItem: sellerItemList) {
			BuyDeliveryRecordItem buyDeliveryRecordItem = new BuyDeliveryRecordItem();
			SellerOrderSupplierProduct orderProduct = sellerOrderSupplierProductMapper.selectByPrimaryKey(sellerItem.getOrderItemId());
			buyDeliveryRecordItem.setId(sellerItem.getId());
			buyDeliveryRecordItem.setRecordId(buyDeliveryRecord.getId());
			buyDeliveryRecordItem.setOrderId(sellerItem.getBuyOrderId());
			buyDeliveryRecordItem.setOrderItemId(sellerItem.getBuyOrderItemId());
			buyDeliveryRecordItem.setDeliveryItemId(sellerItem.getId());
			buyDeliveryRecordItem.setApplyCode(sellerItem.getApplyCode());
			buyDeliveryRecordItem.setProductCode(sellerItem.getProductCode());
			buyDeliveryRecordItem.setProductName(sellerItem.getProductName());
			buyDeliveryRecordItem.setSkuCode(sellerItem.getSkuCode());
			buyDeliveryRecordItem.setSkuName(sellerItem.getSkuName());
			buyDeliveryRecordItem.setBarcode(sellerItem.getBarcode());
			buyDeliveryRecordItem.setUnitId(sellerItem.getUnitId());
			buyDeliveryRecordItem.setSalePrice(sellerItem.getSalePrice());
			buyDeliveryRecordItem.setWarehouseId(sellerItem.getWarehouseId());
			buyDeliveryRecordItem.setWarehouseCode("");
			buyDeliveryRecordItem.setWarehouseName("");
			buyDeliveryRecordItem.setDeliveryNum(sellerItem.getDeliveryNum());
			buyDeliveryRecordItem.setDeliveredNum(ObjectUtil.isEmpty(orderProduct)?0:orderProduct.getDeliveredNum());
			buyDeliveryRecordItem.setArrivalNum(0);
			buyDeliveryRecordItem.setStatus(sellerItem.getStatus());
			buyDeliveryRecordItem.setIsNeedInvoice(sellerItem.getIsNeedInvoice());
			buyDeliveryRecordItem.setRemark(sellerItem.getRemark());
			buyDeliveryRecordItemMapper.add(buyDeliveryRecordItem);

		}
	}

	/**
	 * Gets print delivery data.
	 * 获取发货单打印数据
	 *
	 * @param deliveryId the delivery id
	 * @return the print delivery data
	 */
	public Map<String,Object> getPrintDeliveryData(String deliveryId) {
		Map<String,Object> map = new HashMap<>();
		//发货单主表
		SellerDeliveryRecord delivery = sellerDeliveryRecordMapper.get(deliveryId);
		//发货单分录表
		map.put("deliveryId",deliveryId);
		List<SellerDeliveryRecordItem> deliveryItemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(map);
		//根据订单获取收货信息
		String orderId = "";
		if(!ObjectUtil.isEmpty(deliveryItemList)&&deliveryItemList.size()>0){
			orderId = deliveryItemList.get(0).getOrderId();
		}
		SellerOrder order = sellerOrderMapper.getAllContent(orderId);
		//运单信息
		SellerLogistics logistics = sellerLogisticsMapper.get(delivery.getLogisticsId());
		map.put("delivery",delivery);
		map.put("deliveryItemList",deliveryItemList);
		map.put("order",order);
		map.put("logistics",logistics);
		return map;
	}



	/**
	 * Gets delivery record detail.
	 * 获取发货单详情
	 * @param params the params
	 * @return the delivery record detail
	 */
	public Map<String,Object> getDeliveryRecordDetail(Map<String, Object> params) {
		Map<String,Object> map = new HashMap<>();
		List<SellerDeliveryRecordDetailView> deliveryList = new ArrayList<>();
		String deliveryId = String.valueOf(params.get("deliveryId"));
		//发货单信息
		SellerDeliveryRecord deliveryRecord = sellerDeliveryRecordMapper.get(deliveryId);
		//订单ID数组，用于存放发货单下的订单ID
		List<String> orderIds = new ArrayList<>();
		Map<String,Object> itemMap = new HashMap<>(1);
		itemMap.put("deliveryId",deliveryId);
		List<SellerDeliveryRecordItem> deliveryItemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(itemMap);//获取发货单明细
		if(!ObjectUtil.isEmpty(deliveryItemList)&&deliveryItemList.size()>0){
			for (SellerDeliveryRecordItem item: deliveryItemList) {
				String orderId = item.getOrderId();
				if(!orderIds.contains(orderId)&&!ObjectUtil.isEmpty(orderId)){
					orderIds.add(orderId);
				}
			}
		}
		//组装数据
		if(orderIds.size()>0){
			for (String sellerOrderId:orderIds) {
				SellerDeliveryRecordDetailView view = new SellerDeliveryRecordDetailView();
				SellerOrder so = null;
				SellerCustomer sc = null;
				if (deliveryRecord.getDeliverType() == 0){
					so = sellerOrderMapper.get(sellerOrderId);
				}else{
					sc = sellerCustomerMapper.get(sellerOrderId);
				}
				
				if(!ObjectUtil.isEmpty(so)){
					//组装表头
					view.setSellerOrderId(sellerOrderId);
					view.setBuyerOrderId(so.getBuyerOrderId());
					view.setOrderCode(so.getOrderCode());
					view.setOrderDate(so.getCreateDate());
					view.setOrderType(so.getOrderType());
					view.setDeliverDate(deliveryRecord.getSendDate());
					view.setDeliverNo(deliveryRecord.getDeliverNo());
					view.setSupplierId(so.getSuppId());
					view.setSupplierName(so.getSuppName());
					view.setBuycompanyId(so.getCompanyId());
					view.setBuycompanyName(so.getCompanyName());
					view.setLogisticsId(deliveryRecord.getLogisticsId());
					//组装明细
					itemMap.put("orderId",sellerOrderId);
					List<SellerDeliveryRecordItem> itemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(itemMap);
					for (SellerDeliveryRecordItem sellerDeliveryRecordItem : itemList){
						SellerOrderSupplierProduct sellerOrderSupplierProduct = sellerOrderSupplierProductMapper.selectByPrimaryKey(sellerDeliveryRecordItem.getOrderItemId());
						if (!ObjectUtil.isEmpty(sellerOrderSupplierProduct)){
							sellerDeliveryRecordItem.setOrderNum(sellerOrderSupplierProduct.getOrderNum());
							sellerDeliveryRecordItem.setDeliveredNum(sellerOrderSupplierProduct.getDeliveredNum());
						}
					}
					view.setItemList(itemList);
					deliveryList.add(view);
				}else if (!ObjectUtil.isEmpty(sc)){
					//组装表头
					view.setSellerOrderId(sellerOrderId);
					view.setBuyerOrderId(sc.getBuyCustomerId());
					view.setOrderCode(sc.getCustomerCode());
					view.setOrderDate(sc.getCreateDate());
					view.setDeliverDate(deliveryRecord.getSendDate());
					view.setDeliverNo(deliveryRecord.getDeliverNo());
					view.setBuycompanyId(sc.getCompanyId());
					BuyCompany buyCompany = buyCompanyMapper.selectByPrimaryKey(sc.getCompanyId());
					if (!ObjectUtil.isEmpty(buyCompany)){
						view.setBuycompanyName(buyCompany.getCompanyName());
					}
					view.setLogisticsId(deliveryRecord.getLogisticsId());
					//组装明细
					itemMap.put("orderId",sellerOrderId);
					List<SellerDeliveryRecordItem> itemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(itemMap);
					for (SellerDeliveryRecordItem sellerDeliveryRecordItem : itemList){
						SellerCustomerItem sellerCustomerItem = sellerCustomerItemMapper.get(sellerDeliveryRecordItem.getOrderItemId());
						if (!ObjectUtil.isEmpty(sellerCustomerItem)){
							sellerDeliveryRecordItem.setOrderNum(sellerCustomerItem.getGoodsNumber());
//							sellerDeliveryRecordItem.setDeliveredNum();
						}
					}
					view.setItemList(itemList);
					deliveryList.add(view);
				}
			}
		}
		map.put("deliveryList",deliveryList);
		map.put("deliveryRecord",deliveryRecord);
		return map;
	}

	/**
	 * 根据条件查询发货详细
	 */
	public List<SellerDeliveryRecord> queryDeliveryRecordList(Map<String, Object> map){
		return sellerDeliveryRecordMapper.queryDeliveryRecordList(map);
	}

	/**
	 * Update delivery by reconciliation int.
	 * 根据条件修改发货对账状态
	 * @param map the map
	 * @return the int
	 */
	public int updateDeliveryByReconciliation(Map<String, Object> map){
		return sellerDeliveryRecordMapper.updateDeliveryByReconciliation(map);
	}



}

