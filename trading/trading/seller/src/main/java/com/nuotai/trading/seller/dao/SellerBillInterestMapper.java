package com.nuotai.trading.seller.dao;

import com.nuotai.trading.seller.model.SellerBillInterest;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("sellerBillInterestMapper")
public interface SellerBillInterestMapper extends BaseDao<SellerBillInterest>{
	//添加账单周期关联利息数据
	int saveInterestInfo(Map<String, Object> map);
	
	//根据账单周期查询关联利息信息
	List<SellerBillInterest> getBillInteresInfo(String billCycleId);
	
	//根据账单周期编号删除关联利息
    int deleteByPrimaryKey(String billCycleId);

    int insert(SellerBillInterest record);

    int insertSelective(SellerBillInterest record);

    SellerBillInterest selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SellerBillInterest record);

    int updateByPrimaryKey(SellerBillInterest record);
}