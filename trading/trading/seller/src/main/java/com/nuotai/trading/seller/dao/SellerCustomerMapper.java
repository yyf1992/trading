package com.nuotai.trading.seller.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerCustomer;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

/**
 * 
 * 卖家售后
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
@Component
public interface SellerCustomerMapper extends BaseDao<SellerCustomer> {
	
	SellerCustomer getByBuyCustomerId(String buyCustomerId);
	
	int selectCustomerListNumByMap(Map<String, Object> map);
	
	List<SellerCustomer> queryCustomerList(SearchPageUtil searchPageUtil);

	//根据周期公司编号查询建单人
	List<Map<String,Object>> queryCreateBillUser(Map<String,Object> map);

	//根据条件查询售后退货
	List<SellerCustomer> queryCustomerListByMap(Map<String,Object> map);

	//根据条件修改退货对账状态
	int updateCustomerByReconciliation(Map<String,Object> map);

	//根据发票条件查询
	List<SellerCustomer> queryCustomerByInvoice(SearchPageUtil searchPageUtil);

	//根据发票模块售后编号查询售后数据
	SellerCustomer queryCustomerById(String customerId);
	//根据发票确认状态查询数量
	int queryCountByInvoice(Map<String,Object> map);
	
	SellerCustomer getByCustomerCode(String orderCode);
	
}
