package com.nuotai.trading.seller.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 物流信息
 * @author gsf
 * @date 2017-08-10
 */

@Data
public class SellerLogistics implements Serializable {
 private static final long serialVersionUID = 1L;

	//
	private String id;
	//发货单Id
	private String deliveryId;
	//物流单号
	private String waybillNo;
	//物流公司
	private String logisticsCompany;
	//手机号码
	private String mobilePhone;
	//区号
	private String zoneCode;
	//电话号码
	private String fixedPhone;
	//分机号
	private String extPhone;
	//运费
	private BigDecimal freight;
	//司机姓名
	private String driverName;
	//预计送达日期
	private Date expectArrivalDate;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人Id
	private String updateId;
	//修改人姓名
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人Id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
}
