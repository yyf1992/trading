package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 采购商收货单
 *
 * @author liuhui
 * @date 2017 -09-06 10:21:25
 */
@Component
public interface SBuyDeliveryRecordMapper extends BaseDao<BuyDeliveryRecord> {

    /**
     * Save receipt voucher.
     * 保存收货回单
     *
     * @param bdr the bdr
     */
    void saveReceiptVoucher(BuyDeliveryRecord bdr);

    /**
     * Update by deliver id.
     * 根据发货单ID更新
     *
     * @param buyDeliveryRecordUpd the buy delivery record upd
     */
    void updateByDeliverId(BuyDeliveryRecord buyDeliveryRecordUpd);

    /**
     * 根据条件修改发货状态为已对帐
     *
     * @param map the map
     * @return int
     */
    int updateDeliveryByReconciliation(Map<String,Object> map);

    /**
     * Gets by deliver id.
     * 根据卖家发货单ID获取收货单
     * @param deliverId the deliverId
     * @return the by deliver id
     */
    BuyDeliveryRecord getByDeliverId(String deliverId);
    /**
     * Add qrcode addr.
     * 更新发货单二维码
     * @param deliverNo  the deliver no
     * @param qrcodeAddr the qrcode addr
     */
    int addQrcodeAddr(@Param("deliverNo") String deliverNo, @Param("qrcodeAddr") String qrcodeAddr);

	int updatePushType(String deliverNo);
}
