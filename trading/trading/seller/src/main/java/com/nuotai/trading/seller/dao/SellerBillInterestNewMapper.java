package com.nuotai.trading.seller.dao;

import com.nuotai.trading.seller.model.SellerBillInterestNew;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 14:52:16
 */
@Component
public interface SellerBillInterestNewMapper extends BaseDao<SellerBillInterestNew> {
	//添加申请关联利息
    int addNewInterest(SellerBillInterestNew sellerBillInterestNew);
    //根据申请周期编号查询关联利息
    List<SellerBillInterestNew> queryNewInterest(String id);
}
