package com.nuotai.trading.seller.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-08-21 09:17:46
 */
@Data
public class SellerManualOrder implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司Id
	private String companyId;
	//订单主键Id
	private String orderId;
	//其他费用
	private BigDecimal otherFee;
	//销售金额
	private BigDecimal purchasePrice;
	//本次费用
	private BigDecimal thisPrice;
	//收款金额
	private BigDecimal paymentPrice;
	//收款方式
	private Integer paymentType;
	//收款日期
	private Date paymentDate;
	//收款人
	private String paymentPerson;
	//附件(多个，分割)
	private String annexVoucher;
	//经办人
	private String handler;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//
	private String createId;
	//
	private String createName;
	//
	private Date createDate;
	//
	private String updateId;
	//
	private String updateName;
	//
	private Date updateDate;
	//
	private String delId;
	//
	private String delName;
	//
	private Date delDate;
}
