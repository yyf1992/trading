package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 用户与角色关联表
 * 
 * @author "
 * @date 2017-09-14 09:23:06
 */
@Data
public class TradeRoleUser implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//用户主键
	private String userId;
	//角色主键
	private String roleId;
	//创建时间
	private Date createTime;
	//
	private String companyId;
}
