package com.nuotai.trading.dao;

import java.util.List;
import com.nuotai.trading.model.BuyWarehouseBacklog;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 库存积压表
 * 
 * @author "
 * @date 2017-09-15 16:07:33
 */
public interface BuyWarehouseBacklogMapper extends BaseDao<BuyWarehouseBacklog> {
	
	List<BuyWarehouseBacklog> selectBacklogByPage(SearchPageUtil searchPageUtil);
	
	List<BuyWarehouseBacklog> selectBacklog();
	
	int selectById(String barcode);
	
	BuyWarehouseBacklog selectByBarcode(String barcode);
	
}

