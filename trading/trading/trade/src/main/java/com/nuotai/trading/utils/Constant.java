package com.nuotai.trading.utils;

import java.util.Locale;

/**
 * 常量
 *
 * @author liujie
 */
public class Constant {
	public static final Locale locale = Locale.CHINESE;
	// session中获取当前用户
	public static final String CURRENT_USER = "CURRENT_USER";
	// 项目路径
	public static final String BASE_PATH = "/trade";
	// 分页初始化页面显示数量10
	public static final String INIT_PAGE_SIZE = "10";
	// 分页初始化页面显示第一页
	public static final String INIT_PAGE_NO = "1";
	// 分页初始化oldpagesize为0
	public static final String INIT_OLD_PAGE_SIZE = "0";
	// session中获取权限菜单
	public static final String AUTHORITYSYSMENU = "AUTHORITYSYSMENU";
	public static final String MENU_ID = "menuId";
	//内部审批提醒时间，小时
	public static final int TRADEVERIFYTIME = 5;
	//内部审批天数
	public static final int TRADEVERIFYDAY = 2;
	
	//文件服务器地址
	//public static String FILE_SERVER_URL = "http://trade.nuotai.biz:8080/";//阿里云
	public static String FILE_SERVER_URL = "http://172.16.3.77:8080/";//测试环境
	//public static String FILE_SERVER_URL = "http://139.224.209.149:8080/";//演示环境
	
	public static String QRCODE_VIEW_URL = FILE_SERVER_URL+"qrcode/";//二维码查看地址
	public static String QRCODE_SAVE_URL = "/usr/local/uploadfiles/qrcode/";//二维码存放地址

	public static final String PLATFORM = "platform";
	public static final String SYSVERIFY = PLATFORM+"/sysVerify";
	public static String DINGDING_URL = "http://trade.nuotai.biz:9090/datacenter/";
	
	//public static String TRADE_URL = "http://trade.nuotai.biz:8080/trade/";//阿里云
	public static String TRADE_URL = "http://cs.nuotai.biz:8989/trade/";//测试环境
	//public static String TRADE_URL = "http://139.224.209.149:8080/trade/";//演示环境

	// wms接口地址
	public static String WMS_INTERFACE_URL = "http://cs.nuotai.biz:8989/wms_interface/";//测试环境
	//public static String WMS_INTERFACE_URL = "http://139.224.209.149:8081/wms_interface/";//演示环境
	//public static String WMS_INTERFACE_URL = "http://trade.nuotai.biz:8989/wms_interface/";//阿里云
	   
	// oms接口地址
	public static String OMS_INTERFACE_URL = "http://139.224.209.149:8686/oms_interface/";//演示环境
	//public static String OMS_INTERFACE_URL = "http://121.199.179.23:30003/oms_interface/";//正式
	
	// 采购系统接口地址
	public static String PURCHASE_INTERFACE_URL = "http://cs.nuotai.biz:8086/purchase/client/";
	
	// 诺泰公司ID
	public static final String COMPANY_ID = "17060909542281121440";
	/**
	 * 菜单位置
	 * @author wxx
	 *
	 */
	public enum MenuPosition {
        TOP("top"),SYSTEM("system"), LEFT("left"), BUTTON("button");
        // 成员变量
        private String value;

        // 构造方法
        private MenuPosition(String value) {
            this.value = value;
        }
		public String getValue() {
			return value;
		}
    }

	/**
	 * 物流类型
	 * @author Administrator
	 *
	 */
	public enum LogisticsType{
		/**
		 * 自提
		 */
		self(1),
		/**
		 * 需要物流
		 */
		logistis(0);
		private int value;
		private LogisticsType(int value){
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	
	/**
	 * 菜单类型
	 */
    public enum MenuType {
    	
        /**
         * 菜单
         */
        MENU(0),
        /**
         * 按钮
         */
        BUTTON(1);

        private int value;

        private MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 状态
     */
    public enum CommonStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 禁止
         */
    	FORBID(1);

        private int value;

        private CommonStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 是否删除
     */
    public enum IsDel {
        /**
         * 未删除
         */
    	NODEL(0),
        /**
         * 已删除
         */
    	YESDEL(1);

        private int value;

        private IsDel(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 订单状态
     */
    public enum OrderStatus{
    	/**
    	 * 0、待接单；
    	 */
    	WAITORDER(0),
    	/**
    	 *  1、待确认协议；
    	 */
    	WAITCONFIRMAGREEMENT(1),
    	/**
    	 *  2、待对方发货；
    	 */
    	WAITDELIVERY(2),
    	/**
    	 *  3、待收货；
    	 */
    	WAITRECEIVE(3),
    	/**
    	 *  4、已收货；
    	 */
		RECEIVED(4),
    	/**
    	 *  5、已完结；
    	 */
    	FINISHED(5),
    	/**
    	 *  6、买家取消；
    	 */
		CANCEL(6),
    	/**
    	 *  7、卖家驳回
    	 */
		REJECT(7),
    	/**
    	 *  8、已终止
    	 */
    	STOP(8);
    	
    	private int value;

        private OrderStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }

	/**
	 * 供应商订单状态
	 */
	public enum SellerOrderStatus{
		/**
		 * 0、待接单(审核中)；
		 */
		WAITORDER(0),
		/**
		 *  1、待发货；
		 */
		WAITDELIVERY(1),
		/**
		 *  2、发货中；
		 */
		DELIVERING(2),
		/**
		 *  3、发货完成；
		 */
		DELIVERED(3),
		/**
		 *  4、已收货；
		 */
		RECEIVED(4),
		/**
		 *  5、交易完成；
		 */
		FINISH(5),
		/**
		 *  6、买家取消；
		 */
		CANCEL(6),
		/**
		 *  卖家、驳回
		 */
		REJECT(7),
		/**
		 *  8、已终止
		 */
		STOP(8);

		private int value;

		SellerOrderStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	/**
     * 是否审核
     */
    public enum IsCheck{
    	/**
    	 *  0,待审核；
    	 */
    	WAITVERIFY(0),
    	/**
    	 * 1，已审核
    	 */
    	ALREADYVERIFY(1),
    	/**
    	 * 2,审核不通过
    	 */
    	VERIFYNOTPASSED(2);
    	
    	private int value;
    	
    	private IsCheck(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 订单类型
     */
    public enum OrderKind{
    	/**
    	 * 0,采购订单
    	 */
    	AT(0),
    	/**
    	 * 1,转化订单
    	 */
    	MT(1);

    	private int value;
    	
    	private OrderKind(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 发货单类型
     */
    public enum DeliveryRecordStatus{
    	/**
    	 * 0,待发货
    	 */
    	WAITDELIVERY(0),
    	/**
    	 * 1,已发货
    	 */
		DELIVERED(1),
    	/**
    	 * 2,已收货
    	 */
    	RECEIVED(2),
		/**
		 * 3,已取消
		 */
		CANCELED(3);

    	private int value;
    	
    	DeliveryRecordStatus(int value) {
    		this.value = value;
    	}
    	public int getValue() {
    		return value;
    	}
    }
    
    /**
     * 店铺采购计划状态
     */
    public enum PurchaseStatus{
    	/**
    	 * 0,等待审核
    	 */
    	WAITVERIFY(0),
    	/**
    	 * 1,审核通过
    	 */
    	VERIFYPASS(1),
    	/**
    	 * 2,审核不通过
    	 */
    	VERIFYNOPASS(2),
    	/**
    	 * 3,已取消
    	 */
    	CANCEL(3);

    	private int value;
    	
    	private PurchaseStatus(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 售后服务状态
     */
    public enum CustomerStatus{
    	/**
    	 * 0,等待审核
    	 */
    	WAITVERIFY(0),
    	/**
    	 * 1,待对方确认
    	 */
    	VERIFYPASS(1),
    	/**
    	 * 2,审核不通过
    	 */
    	VERIFYNOPASS(2),
    	/**
    	 * 3,已取消
    	 */
    	CANCEL(3),
    	/**
    	 * 4,对方已确认
    	 */
    	ALREADYCONFIRM(4);

    	private int value;
    	
    	private CustomerStatus(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    /**
     * 商品类型
     */
    public enum ProductType{
    	/**
    	 * 0,成品
    	 */
    	PRODUCT(0),
    	/**
    	 * 1,原材料
    	 */
    	MATERIAL(1),
    	/**
    	 * 2,辅料
    	 */
    	ACCESSORIES(2),
    	/**
    	 * 3,虚拟产品
    	 */
    	VIRTUALPRODUCTS(3);
    	
    	private int value;
    	
    	private ProductType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

	
}
