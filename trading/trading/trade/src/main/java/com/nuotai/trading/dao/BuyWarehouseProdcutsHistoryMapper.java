package com.nuotai.trading.dao;

import java.util.List;
import com.nuotai.trading.model.BuyWarehouseProdcutsHistory;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-09-20 14:50:15
 */
public interface BuyWarehouseProdcutsHistoryMapper extends BaseDao<BuyWarehouseProdcutsHistory> {
	
	List<BuyWarehouseProdcutsHistory> selectByDate(SearchPageUtil searchPageUtil);
}
