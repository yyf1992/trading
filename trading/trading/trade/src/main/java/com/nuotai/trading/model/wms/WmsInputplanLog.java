package com.nuotai.trading.model.wms;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 推送WMS入库计划日志表
 * @author liuhui
 * @date 2017-11-20 09:59:37
 */
@Data
public class WmsInputplanLog implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//发货单号
	private String deliverNo;
	//接口地址
	private String interfaceUrl;
	//接口方法
	private String method;
	//token
	private String token;
	//传输内容
	private String content;
	//结果 Y:成功 N:失败
	private String result;
	//失败原因
	private String errorMsg;
}
