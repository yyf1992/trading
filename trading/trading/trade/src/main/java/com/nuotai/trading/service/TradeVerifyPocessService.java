package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.TradeVerifyPocess;



@Service
@Transactional
public class TradeVerifyPocessService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeVerifyPocessService.class);

	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	
	public TradeVerifyPocess get(String id){
		return tradeVerifyPocessMapper.get(id);
	}
	
	public List<TradeVerifyPocess> queryList(Map<String, Object> map){
		return tradeVerifyPocessMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeVerifyPocessMapper.queryCount(map);
	}
	
	public void add(TradeVerifyPocess tradeVerifyPocess){
		tradeVerifyPocessMapper.add(tradeVerifyPocess);
	}
	
	public void update(TradeVerifyPocess tradeVerifyPocess){
		tradeVerifyPocessMapper.update(tradeVerifyPocess);
	}
	
	public void delete(String id){
		tradeVerifyPocessMapper.delete(id);
	}

}
