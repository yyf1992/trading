package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.OutWhareaLogMapper;
import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class OutWhareaLogService {

    private static final Logger LOG = LoggerFactory.getLogger(OutWhareaLogService.class);

	@Autowired
	private OutWhareaLogMapper outWhareaLogMapper;
	
	public OutWhareaLog get(String id){
		return outWhareaLogMapper.get(id);
	}
	
	public List<OutWhareaLog> queryList(Map<String, Object> map){
		return outWhareaLogMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return outWhareaLogMapper.queryCount(map);
	}
	
	public void add(OutWhareaLog outWhareaLog){
		outWhareaLogMapper.add(outWhareaLog);
	}
	
	public void update(OutWhareaLog outWhareaLog){
		outWhareaLogMapper.update(outWhareaLog);
	}
	
	public void delete(String id){
		outWhareaLogMapper.delete(id);
	}
	
	public List<OutWhareaLog> queryAllList(SearchPageUtil searchPageUtil){
		return outWhareaLogMapper.queryAllList(searchPageUtil);
	}
	
	/**
	 * 保存导入的外仓数据
	 * @param outWhareaLogList
	 * @return
	 */
	public JSONObject insertOutWhareaLog(List <OutWhareaLog> outWhareaLogList) {
		JSONObject json = new JSONObject();
		for (OutWhareaLog outWhareaLog : outWhareaLogList) {			
			int result = 0;
			outWhareaLog.setCompanyId(ShiroUtils.getCompId());

			if (!ObjectUtil.isEmpty(outWhareaLog.getItems())){
				for (OutWhareaLog item : outWhareaLog.getItems()){
					if (!ObjectUtil.isEmpty(item.getItems())){
						for (OutWhareaLog it : item.getItems()){
							outWhareaLog.setShopName(item.getShopName());
							outWhareaLog.setProductCode(it.getProductCode());
							outWhareaLog.setBarcode(it.getBarcode());
							outWhareaLog.setPrice(it.getPrice());
							outWhareaLog.setOutWhareaStock(it.getOutWhareaStock());
							outWhareaLog.setOutWhareaWayStock(it.getOutWhareaWayStock());
							outWhareaLog.setOutWhareaTotalPrice(it.getOutWhareaTotalPrice());
							//根据条件查询已导入的商品信息，已存在的更新，不存在的插入
							result = queryListByOutWhareaLog(outWhareaLog);
						}
					}else{
						outWhareaLog.setShopName(item.getShopName());
						outWhareaLog.setProductCode(item.getProductCode());
						outWhareaLog.setBarcode(item.getBarcode());
						outWhareaLog.setPrice(item.getPrice());
						outWhareaLog.setOutWhareaStock(item.getOutWhareaStock());
						outWhareaLog.setOutWhareaWayStock(item.getOutWhareaWayStock());
						outWhareaLog.setOutWhareaTotalPrice(item.getOutWhareaTotalPrice());
						//根据条件查询已导入的商品信息，已存在的更新，不存在的插入
						result = queryListByOutWhareaLog(outWhareaLog);
					}
					
					if(result>0){
						continue;
					}else{
						json.put("status", "fail");
						json.put("msg", "外仓数据导入失败！");
						return json;
					}
				}
			}else{
				//根据条件查询已导入的商品信息，已存在的更新，不存在的插入
				result = queryListByOutWhareaLog(outWhareaLog);
			}
			
			if(result>0){
				continue;
			}else{
				json.put("status", "fail");
				json.put("msg", "外仓数据导入失败！");
				return json;
			}
		}
		
		json.put("status", "success");
		json.put("msg", "外仓数据导入成功！");
		return json;
	}

	/**
	 * 根据条件查询已导入的商品信息，已存在的更新，不存在的插入
	 * @param outWhareaLog
	 * @return
	 */
	private int queryListByOutWhareaLog(OutWhareaLog outWhareaLog) {
		int result;
		List<OutWhareaLog> queryOutWhareaLogList =  outWhareaLogMapper.queryListByOutWhareaLog(outWhareaLog);
		if (!ObjectUtil.isEmpty(queryOutWhareaLogList) && queryOutWhareaLogList.size() == 1){
			outWhareaLog.setId(queryOutWhareaLogList.get(0).getId());
			outWhareaLog.setCreateId(ShiroUtils.getUserId());
			outWhareaLog.setCreateName(ShiroUtils.getUserName());
			outWhareaLog.setCreateDate(new Date());
			outWhareaLog.setUpdateDate(new Date());
			outWhareaLog.setUpdateId(ShiroUtils.getUserId());
			outWhareaLog.setUpdateName(ShiroUtils.getUserName());
			result = outWhareaLogMapper.updateByOutWhareaLog(outWhareaLog);
		}else{
			outWhareaLog.setId(ShiroUtils.getUid());
			outWhareaLog.setCreateId(ShiroUtils.getUserId());
			outWhareaLog.setCreateName(ShiroUtils.getUserName());
			outWhareaLog.setCreateDate(new Date());
			result = outWhareaLogMapper.saveOutWhareaLog(outWhareaLog);
		}
		return result;
	}

	public List<OutWhareaLog> queryByMap(Map<String,Object> map){
		return outWhareaLogMapper.queryByMap(map);
	}
}
