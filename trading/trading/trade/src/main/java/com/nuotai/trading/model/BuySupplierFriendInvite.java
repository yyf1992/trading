package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 好友互通申请信息表
 * 
 * @author liuhui
 * @date 2017-08-02 13:45:01
 */
@Data
public class BuySupplierFriendInvite implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//邀请者公司Id
	private String inviterId;
	//邀请者公司名称
	private String inviterName;
	//邀请者公司负责人
	private String inviterPerson;
	//邀请者公司手机号
	private String inviterPhone;
	//邀请者角色（0：我是买家1：我是卖家）
	private String inviterRole;
	//受邀者公司Id
	private String inviteeId;
	//受邀者公司名称
	private String inviteeName;
	//受邀者公司负责人
	private String inviteePerson;
	//受邀者公司号码
	private String inviteePhone;
	//附加信息
	private String note;
	//状态(0: 未同意 1：同意 2：忽略)
	private String status;
	//
	private Integer isDel;
	//
	private String createId;
	//
	private String createName;
	//
	private Date createDate;

	//
	private String delId;
	//
	private String delName;
	//
	private Date delDate;
}
