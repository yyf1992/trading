package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.SysVerifyPocessMapper;
import com.nuotai.trading.model.SysVerifyPocess;



@Service
@Transactional
public class SysVerifyPocessService {

    private static final Logger LOG = LoggerFactory.getLogger(SysVerifyPocessService.class);

	@Autowired
	private SysVerifyPocessMapper sysVerifyPocessMapper;
	
	public SysVerifyPocess get(String id){
		return sysVerifyPocessMapper.get(id);
	}
	
	public List<SysVerifyPocess> queryList(Map<String, Object> map){
		return sysVerifyPocessMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysVerifyPocessMapper.queryCount(map);
	}
	
	public void add(SysVerifyPocess sysVerifyPocess){
		sysVerifyPocessMapper.add(sysVerifyPocess);
	}
	
	public void update(SysVerifyPocess sysVerifyPocess){
		sysVerifyPocessMapper.update(sysVerifyPocess);
	}
	
	public void delete(String id){
		sysVerifyPocessMapper.delete(id);
	}

	public List<SysVerifyPocess> getListByHeaderId(String headerId) {
		return sysVerifyPocessMapper.getListByHeaderId(headerId);
	}
	

}
