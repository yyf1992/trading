package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.BuyProductSkuCompose;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 16:15:53
 */
public interface BuyProductSkuBomMapper extends BaseDao<BuyProductSkuBom> {

	List<BuyProductSkuCompose> selectByPrimaryKey(Map<String, Object> map);
	
	int insertSelective(BuyProductSkuBom buyProductSkuBom);

	List<BuyProductSkuBom> selectList(SearchPageUtil searchPageUtil);
	 
	List<String> selectExitProduct(String companyId); 
	
	void setStatus(Map map);

	BuyProductSkuBom selectById(String id);
	
	BuyProductSkuBom selectByMap(Map<String, Object> map);

	void updateBom(BuyProductSkuBom bom);

	int getStatsCout(Map<String, Object> map);

	BuyProductSkuBom selectByBomId(String id);
	
	BuyProductSkuBom selectByBarcode(Map<String, Object> map);
	
	void deleteById(Map<String, Object> map);
	
	BuyProductSkuBom selectByBarcodeAndCompanyId(Map<String, Object> map);
}
