package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyCategory;

public interface BuyCategoryMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyCategory record);

    int insertSelective(BuyCategory record);

    BuyCategory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyCategory record);

    int updateByPrimaryKey(BuyCategory record);
    
    List<BuyCategory> getAllCategory(Map<String,Object> map);
}