package com.nuotai.trading.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.SysRoleMenuMapper;
import com.nuotai.trading.model.SysRoleMenu;

@Service
public class SysRoleMenuService implements SysRoleMenuMapper {
	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		return sysRoleMenuMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysRoleMenu record) {
		return sysRoleMenuMapper.insert(record);
	}

	@Override
	public int insertSelective(SysRoleMenu record) {
		return sysRoleMenuMapper.insertSelective(record);
	}

	@Override
	public SysRoleMenu selectByPrimaryKey(String id) {
		return sysRoleMenuMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysRoleMenu record) {
		return sysRoleMenuMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysRoleMenu record) {
		return sysRoleMenuMapper.updateByPrimaryKey(record);
	}

	@Override
	public int deleteByRoleId(String[] idList) {
		return sysRoleMenuMapper.deleteByRoleId(idList);
	}

	@Override
	public int deleteByMenuId(String[] idList) {
		return sysRoleMenuMapper.deleteByMenuId(idList);
	}

	public List<String> getMenuByRoleId(String roleId) {
		return sysRoleMenuMapper.getMenuByRoleId(roleId);
	}
}
