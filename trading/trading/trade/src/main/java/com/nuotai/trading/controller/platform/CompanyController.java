package com.nuotai.trading.controller.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.service.BuyCompanyService;

@Controller
@RequestMapping("platform/company")
public class CompanyController extends BaseController{
	
	@Autowired
	private BuyCompanyService buyCompanyService;
	
	/**
	 * 修改保存
	 * @param pos
	 * @return
	 */
	@RequestMapping(value="/saveUpdateCompany",method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdateCompany(BuyCompany company,@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			buyCompanyService.saveUpdateCompany(company,map);
			json.put("success", true);
			json.put("msg", "保存成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "保存失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
}
