package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-08-14 09:32:48
 */
public interface SysVerifyHeaderMapper extends BaseDao<SysVerifyHeader> {

	List<SysVerifyHeader> getPageList(SearchPageUtil searchPageUtil);
	SysVerifyHeader getApprovalByMenuId(Map<String,Object> params);
	List<Map<String, String>> getVerifyType(Map<String, Object> map);
}
