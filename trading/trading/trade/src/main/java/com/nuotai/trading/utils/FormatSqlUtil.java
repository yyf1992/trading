package com.nuotai.trading.utils;

import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.session.defaults.DefaultSqlSession;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author liuhui
 * @date 2017-10-12 11:14
 * PageInterceptor格式化sql
 **/
public class FormatSqlUtil {
    /**
     * 格式化sql，用于控制台输出
     * @param printSql
     * @param parameterObj
     * @param parameterMappingList
     * @return
     */
    public static String formatSql(String printSql, Object parameterObj, List<ParameterMapping> parameterMappingList) {
        //sql判空
        if(printSql==null||printSql.length()<=0){
            return "";
        }
        //定义一个没有替换过占位符的sql，用于出异常时返回
        String sqlWithoutReplacePlaceholder = printSql;
        //美化sql
        printSql = beautifySql(printSql);
        //不传参数的场景，直接把Sql美化一下返回出去
        if(parameterObj==null||parameterMappingList==null||parameterMappingList.size()<=0){
            return printSql;
        }

        try {
            if (parameterMappingList != null) {
                Class<?> parameterObjectClass = parameterObj.getClass();
                // 如果参数是StrictMap且Value类型为Collection，获取key="list"的属性，这里主要是为了处理<foreach>循环时传入List这种参数的占位符替换
                // 例如select * from xxx where id in <foreach collection="list">...</foreach>
                if (FormatSqlUtil.isMap(parameterObjectClass)) {
                    // 如果参数是Map则直接强转，通过map.get(key)方法获取真正的属性值
                    // 这里主要是为了处理<insert>、<delete>、<update>、<select>时传入parameterType为map的场景
                    Map<?, ?> paramMap = (Map<?, ?>) parameterObj;
                    printSql = FormatSqlUtil.handleMapParameter(printSql, paramMap, parameterMappingList);
                }else if (FormatSqlUtil.isStrictMap(parameterObjectClass)) {
                    DefaultSqlSession.StrictMap<Collection<?>> strictMap = (DefaultSqlSession.StrictMap<Collection<?>>)parameterObj;
                    if (FormatSqlUtil.isList(strictMap.get("list").getClass())) {
                        printSql = FormatSqlUtil.handleListParameter(printSql, strictMap.get("list"));
                    }
                }  else {
                    // 通用场景，比如传的是一个自定义的对象或者八种基本数据类型之一或者String
                    printSql = FormatSqlUtil.handleCommonParameter(printSql, parameterMappingList, parameterObjectClass, parameterObj);
                }
            }
        } catch (Exception e) {
            // 占位符替换过程中出现异常，则返回没有替换过占位符但是格式美化过的sql，这样至少保证sql语句比BoundSql中的sql更好看
            return sqlWithoutReplacePlaceholder;
        }

        return printSql;
    }

    /**
     * 美化Sql
     */
    private static String beautifySql(String sql) {
        sql = sql.replace("\n", " ").replace("\t", " ").replace("\r", " ").
                replace("( ", "(").replace(" )", ")").
                replace(" ,", ",").replaceAll(" +"," ");
        return sql;
    }
    /**
     * 处理参数为List的场景
     */
    private static String handleListParameter(String sql, Collection<?> col) {
        if (col != null && col.size() != 0) {
            for (Object obj : col) {
                String value = null;
                Class<?> objClass = obj.getClass();

                // 只处理基本数据类型、基本数据类型的包装类、String这三种
                // 如果是复合类型也是可以的，不过复杂点且这种场景较少，写代码的时候要判断一下要拿到的是复合类型中的哪个属性
                if (isPrimitiveOrPrimitiveWrapper(objClass)) {
                    value = obj.toString();
                } else if (objClass.isAssignableFrom(String.class)) {
                    value = "\"" + obj.toString() + "\"";
                }

                sql = sql.replaceFirst("\\?", value);
            }
        }

        return sql;
    }

    /**
     * 处理参数为Map的场景
     */
    private static String handleMapParameter(String sql, Map<?, ?> paramMap, List<ParameterMapping> parameterMappingList) {
        for (ParameterMapping parameterMapping : parameterMappingList) {
            Object propertyName = parameterMapping.getProperty();
            Object propertyValue = paramMap.get(propertyName);
            if (propertyValue != null) {
                if (propertyValue.getClass().isAssignableFrom(String.class)) {
                    propertyValue = "\'" + propertyValue + "\'";
                }

                sql = sql.replaceFirst("\\?", propertyValue.toString());
            }
        }

        return sql;
    }

    /**
     * 处理通用的场景
     */
    private static String handleCommonParameter(String sql, List<ParameterMapping> parameterMappingList, Class<?> parameterObjectClass,
                                         Object parameterObject) throws Exception {
        for (ParameterMapping parameterMapping : parameterMappingList) {
            String propertyValue = null;
            // 基本数据类型或者基本数据类型的包装类，直接toString即可获取其真正的参数值，其余直接取paramterMapping中的property属性即可
            if (isPrimitiveOrPrimitiveWrapper(parameterObjectClass)) {
                propertyValue = parameterObject.toString();
            }else if(parameterObjectClass.isAssignableFrom(String.class)){
                propertyValue = "\'" + parameterObject.toString() + "\'";
            }else if(parameterObjectClass.isAssignableFrom(SearchPageUtil.class)){//SearchPageUtil类型要单独处理
                SearchPageUtil searchPageUtil = (SearchPageUtil) parameterObject;
                Map<?,?> map = (Map<?, ?>) searchPageUtil.getObject();
                Object propertyName = parameterMapping.getProperty().substring(parameterMapping.getProperty().lastIndexOf(".")+1);
                if (parameterMapping.getJavaType().isAssignableFrom(String.class)) {
                    propertyValue = "\'" + String.valueOf(map.get(propertyName)) + "\'";
                }else{
                    propertyValue = String.valueOf(map.get(propertyName));
                }
            }else {
                String propertyName = parameterMapping.getProperty();
                Field field = parameterObjectClass.getDeclaredField(propertyName);
                // 要获取Field中的属性值，这里必须将私有属性的accessible设置为true
                field.setAccessible(true);
                propertyValue = String.valueOf(field.get(parameterObject));
                if (parameterMapping.getJavaType().isAssignableFrom(String.class)) {
                    propertyValue = "\'" + propertyValue + "\'";
                }
            }

            sql = sql.replaceFirst("\\?", propertyValue);
        }

        return sql;
    }

    /**
     * 是否基本数据类型或者基本数据类型的包装类
     */
    private static boolean isPrimitiveOrPrimitiveWrapper(Class<?> parameterObjectClass) {
        return parameterObjectClass.isPrimitive() ||
                (parameterObjectClass.isAssignableFrom(Byte.class) || parameterObjectClass.isAssignableFrom(Short.class) ||
                        parameterObjectClass.isAssignableFrom(Integer.class) || parameterObjectClass.isAssignableFrom(Long.class) ||
                        parameterObjectClass.isAssignableFrom(Double.class) || parameterObjectClass.isAssignableFrom(Float.class) ||
                        parameterObjectClass.isAssignableFrom(Character.class) || parameterObjectClass.isAssignableFrom(Boolean.class));
    }

    /**
     * 是否DefaultSqlSession的内部类StrictMap
     */
    public static boolean isStrictMap(Class<?> parameterObjectClass) {
        return parameterObjectClass.isAssignableFrom(DefaultSqlSession.StrictMap.class);
    }

    /**
     * 是否List的实现类
     */
    private static boolean isList(Class<?> clazz) {
        Class<?>[] interfaceClasses = clazz.getInterfaces();
        for (Class<?> interfaceClass : interfaceClasses) {
            if (interfaceClass.isAssignableFrom(List.class)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 是否Map的实现类
     */
    private static boolean isMap(Class<?> parameterObjectClass) {
        Class<?>[] interfaceClasses = parameterObjectClass.getInterfaces();
        for (Class<?> interfaceClass : interfaceClasses) {
            if (interfaceClass.isAssignableFrom(Map.class)) {
                return true;
            }
        }

        return false;
    }


}
