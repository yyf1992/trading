package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyStockBacklogDetails;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 库存积压明细表
 * 
 * @author "
 * @date 2017-12-19 13:30:54
 */
public interface BuyStockBacklogDetailsMapper extends BaseDao<BuyStockBacklogDetails> {
	
	//分页查询
	List<BuyStockBacklogDetails> selectProdcutsByPage(SearchPageUtil searchPageUtil);
	
	//查询所有符合条件的数据
	List<BuyStockBacklogDetails> selectProdcutsByParams(Map<String,Object> params);
	
	int selectById(String id);
	
	//同步之前清除表内数据
    void deleteAllDetails();
}
