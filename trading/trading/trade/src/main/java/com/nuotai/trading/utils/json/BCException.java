package com.nuotai.trading.utils.json;

public class BCException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BCException() {
    }

    public BCException(String message) {
        super(message);
    }

    public BCException(String message, Throwable cause) {
        super(message, cause);
    }

    public BCException(Throwable cause) {
        super(cause);
    }
}
