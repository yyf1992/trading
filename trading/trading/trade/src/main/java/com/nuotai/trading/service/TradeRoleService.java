package com.nuotai.trading.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.TradeRoleMapper;
import com.nuotai.trading.dao.TradeRoleMenuMapper;
import com.nuotai.trading.dao.TradeRoleUserMapper;
import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class TradeRoleService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeRoleService.class);

	@Autowired
	private TradeRoleMapper tradeRoleMapper;
	@Autowired
	private TradeRoleUserMapper tradeRoleUserMapper;
	@Autowired
	private TradeRoleMenuMapper tradeRoleMenuMapper;
	
	public TradeRole get(String id){
		return tradeRoleMapper.get(id);
	}
	
	public List<TradeRole> queryList(Map<String, Object> map){
		return tradeRoleMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeRoleMapper.queryCount(map);
	}
	
	public void add(TradeRole tradeRole){
		tradeRoleMapper.add(tradeRole);
	}
	
	public void update(TradeRole tradeRole){
		tradeRoleMapper.update(tradeRole);
	}
	
	public void delete(String id){
		tradeRoleMapper.delete(id);
	}

	public List<TradeRole> getPageList(SearchPageUtil searchPageUtil) {
		return tradeRoleMapper.getPageList(searchPageUtil);
	}

	/**
	 * 保存新增
	 * @param role
	 * @return
	 */
	public JSONObject saveAdd(TradeRole role) {
		JSONObject json = new JSONObject();
		//角色代码重复判断
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("roleCode", role.getRoleCode());
		List<TradeRole> oldList = tradeRoleMapper.getRoleByMap(params);
		if(!oldList.isEmpty()){
			json.put("flag",false);
			json.put("msg","角色代码【"+role.getRoleCode()+"】已经存在！");
			return json;
		}
		role.setId(ShiroUtils.getUid());
		role.setCompanyId(ShiroUtils.getCompId());
		role.setStatus("0");
		tradeRoleMapper.add(role);
		json.put("flag",true);
		json.put("msg","成功！");
		return json;
	}

	public JSONObject saveDelete(String id) {
		JSONObject json = new JSONObject();
		//删除角色人员表
		tradeRoleUserMapper.deleteByRoleId(id);
		//删除角色菜单表
		tradeRoleMenuMapper.deleteByRoleId(id);
		//删除本身
		tradeRoleMapper.delete(id);
		json.put("flag",true);
		json.put("msg","成功！");
		return json;
	}

	/**
	 * 切换状态
	 * @param id
	 * @param status
	 * @return
	 */
	public JSONObject switchStatus(String id, String status) {
		JSONObject json = new JSONObject();
		TradeRole role = tradeRoleMapper.get(id);
		role.setStatus(status);
		tradeRoleMapper.update(role);
		json.put("flag",true);
		json.put("msg","成功！");
		return json;
	}

	/**
	 * 获得公司下菜单列表
	 * @return
	 */
	public List<Map<String, Object>> getMenuByCompanyId() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
