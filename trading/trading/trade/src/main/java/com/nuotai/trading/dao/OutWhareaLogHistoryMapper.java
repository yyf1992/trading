package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.OutWhareaLogHistory;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2018-02-09 10:07:32
 */
public interface OutWhareaLogHistoryMapper extends BaseDao<OutWhareaLogHistory> {
	
	List<OutWhareaLogHistory> queryAllList(SearchPageUtil searchPageUtil);
	
	List<OutWhareaLogHistory> queryByOutWhareaLogId(OutWhareaLogHistory outWhareaLogHistory);
	
	//根据条件查询用于导出
	List<OutWhareaLogHistory> selectByParams(Map<String,Object> params);
}
