package com.nuotai.trading.controller.platform.baseDate;


import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyProductPricealter;
import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.json.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.service.BuyProductPricealterService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 
 * 关联商品价格变更Controller
 * @author liuhui
 * @date 2017-11-20 13:26:35
 */
@Controller
@RequestMapping("platform/productlink/pricealter")
public class BuyProductPricealterController extends BaseController {
	@Autowired
	private BuyProductPricealterService buyProductPricealterService;
	@Autowired
	private BuyShopProductService buyShopProductService;

	@RequestMapping("updBuyShopProductPrice")
	public void updBuyShopProductPrice(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params) throws ServiceException {
				JSONObject json = new JSONObject();
				//审批成功调用的方法
				json.put("verifySuccess", "platform/productlink/pricealter/verifySuccess");
				//审批失败调用的方法
				json.put("verifyError", "platform/productlink/pricealter/verifyError");
				//审批详细信息地址
				json.put("relatedUrl", "platform/productlink/pricealter/verifyDetail");
				String relatedId = (String) params.get("buyShopProductId");
				BigDecimal updPrice = new BigDecimal(params.get("updPrice").toString());
				//修改价格理由
				String modifyReason = params.get("modifyReason").toString();
				List<String> idList = buyProductPricealterService.addGetId(relatedId,updPrice,modifyReason);
				json.put("idList", idList);
				return json;
			}
		});
	}


	/**
	 * 采购商审批通过
	 * @param id
	 */
	@RequestMapping(value = "verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = buyProductPricealterService.verifySuccess(id);
		return json.toString();
	}

	/**
	 * 采购商审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = buyProductPricealterService.verifyError(id);
		return json.toString();
	}

	/**
	 * Verify detail string.
	 * 审批详情
	 * @param id the id
	 * @return the string
	 */
	@RequestMapping(value = "verifyDetail")
	public String verifyDetail(String id){
		BuyProductPricealter buyProductPricealter = buyProductPricealterService.get(id);
		BuyShopProduct buyShopProduct = new BuyShopProduct();
		if(!ObjectUtil.isEmpty(buyProductPricealter)){
			buyShopProduct = buyShopProductService.selectByPrimaryKey(buyProductPricealter.getRelatedId());
		}
		model.addAttribute("buyProductPricealter",buyProductPricealter);
		model.addAttribute("buyShopProduct",buyShopProduct);
		return "platform/baseInfo/product/productLinkAlterDetail";
	}
	
	/**
	 * 取消商品关联
	 * @param id
	 */
	@RequestMapping(value = "/cancelProductLink", method = RequestMethod.POST)
	@ResponseBody
	public String cancelProductLink(String id){
		JSONObject json = new JSONObject();
		try {
			buyShopProductService.cancelProductLink(id);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 查询商品关联供应商价格修改历史
	 * @param searchPageUtil
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/selectAlterPriceHistory", method = RequestMethod.POST)
	public String selectAlterPriceByRelatedId(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> param){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil.setObject(param);
		List<BuyProductPricealter> buyProductPricealterList = buyProductPricealterService.selectAlterPriceHistory(searchPageUtil);
		searchPageUtil.getPage().setList(buyProductPricealterList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/baseInfo/product/selectAlterPriceHistory";
	}
	
}
