package com.nuotai.trading.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysRole;
import com.nuotai.trading.model.SysRoleUser;
import com.nuotai.trading.service.SysRoleService;
import com.nuotai.trading.service.SysRoleUserService;
import com.nuotai.trading.utils.PageAdmin;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 后台管理-权限用户控制
 * @author wxx
 *
 */
@Controller
@RequestMapping("admin/sysRoleUser")
public class AdminSysRoleUserController extends BaseController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleUserService sysRoleUserService;
	
	/**
	 * 左侧角色信息一览
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadLeftRoleList.html", method = RequestMethod.GET)
	public String loadLeftRoleList(SearchPageUtil searchPageUtil,
			String userId, String role_code, String role_name) throws Exception {
		if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);
		map.put("role_code", role_code);
		map.put("role_name", role_name);
		searchPageUtil.setObject(map);
		List<SysRole> sysRoleList = sysRoleService
				.selectLeftRoleByPageUser(searchPageUtil);
		searchPageUtil.getPageAdmin().setList(sysRoleList);
		model.addAttribute("searchPageUtilLeft", searchPageUtil);
		return "admin/sysuser/roleLeft";
	}
	
	/**
	 * 右侧角色信息一览
	 * 
	 * @param searchPageUtil
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadRightRoleList.html", method = RequestMethod.GET)
	public String loadRightRoleList(SearchPageUtil searchPageUtil, String userId)
			throws Exception {
		if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);
		searchPageUtil.setObject(map);
		List<SysRoleUser> sysUserList = sysRoleUserService
				.selectRightByPageUser(searchPageUtil);
		searchPageUtil.getPageAdmin().setList(sysUserList);
		model.addAttribute("searchPageUtilRight", searchPageUtil);
		return "admin/sysuser/roleRight";
	}
	
	/**
	 * 人员设置角色
	 * 
	 * @param roleIds
	 * @param userId
	 */
	@RequestMapping(value = "/addUserRole", method = RequestMethod.POST)
	@ResponseBody
	public String addUserRole(String roleIds, String userId) {
		JSONObject json = new JSONObject();
		try {
			sysRoleUserService.addUserRole(roleIds, userId);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
	
	/**
	 * 删除
	 * 
	 * @param roleUserIds
	 */
	@RequestMapping(value = "/deleteRoleUser", method = RequestMethod.POST)
	@ResponseBody
	public String deleteRoleUser(String roleUserIds) {
		JSONObject json = new JSONObject();
		try {
			sysRoleUserService.deleteRoleUser(roleUserIds);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
}
