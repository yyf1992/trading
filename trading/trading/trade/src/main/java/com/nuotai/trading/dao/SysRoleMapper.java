package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.SysRole;
import com.nuotai.trading.utils.SearchPageUtil;

public interface SysRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

	List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil);

	List<SysRole> selectByMap(Map<String, Object> codemap);
	List<SysRole> selectByRoleCode(String role_code);

	int saveDelete(@Param("idList")String[] id);

	List<SysRole> selectLeftRoleByPageUser(SearchPageUtil searchPageUtil);
}