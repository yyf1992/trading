package com.nuotai.trading.utils.json;

public interface BeanValueFilter {
	Object process(Object value);
}
