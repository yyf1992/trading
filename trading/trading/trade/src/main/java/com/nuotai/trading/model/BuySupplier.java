package com.nuotai.trading.model;

import lombok.Data;

import java.util.Date;
@Data
public class BuySupplier {
    // 主键id
    private String id;
    // 公司id
    private String companyId;
    // 供应商名称
    private String suppName;
    // 省份
    private String province;
    // 城市
    private String city;
    // 地区
    private String area;
    // 详细地址
    private String address;
    // 银行账号
    private String bankNo;
    // 一般纳税人证件号
    private String taxpayerNo;
    // 添加类型0手工录入；1添加好友
    private String addType;
    // 添加类型0互通；1非互通
    private String type;
    //互通好友ID
	private String friendId;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 创建时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人姓名
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人名称
    private String delName;
    // 删除时间
    private Date delDate;
    // 默认联系人
    private String person;
    private String phone;
    //是否冻结
    private int isFrozen;
    //冻结原因
    private String frozenReason;
    //冻结日期
    private Date frozenDate;
    //冻结人
    private String frozenName;

}