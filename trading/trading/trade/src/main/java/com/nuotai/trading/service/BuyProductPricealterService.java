package com.nuotai.trading.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyShopProductMapper;
import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyProductPricealterMapper;
import com.nuotai.trading.model.BuyProductPricealter;
import org.springframework.transaction.annotation.Transactional;


/**
 * The type Buy product pricealter service.
 * 关联商品价格变更Service
 *
 * @author liuhui
 */
@Service
@Transactional
public class BuyProductPricealterService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyProductPricealterService.class);

	@Autowired
	private BuyProductPricealterMapper buyProductPricealterMapper;
	@Autowired
	private BuyShopProductMapper buyShopProductMapper;

	/**
	 * Get buy product pricealter.
	 *
	 * @param id the id
	 * @return the buy product pricealter
	 */
	public BuyProductPricealter get(String id){
		return buyProductPricealterMapper.get(id);
	}
	
	public List<BuyProductPricealter> selectAlterPriceHistory(SearchPageUtil searchPageUtil){
		return buyProductPricealterMapper.selectAlterPriceHistory(searchPageUtil);
	}

	/**
	 * Query list list.
	 *
	 * @param map the map
	 * @return the list
	 */
	public List<BuyProductPricealter> queryList(Map<String, Object> map){
		return buyProductPricealterMapper.queryList(map);
	}


	/**
	 * Add.
	 *
	 * @param buyProductPricealter the buy product pricealter
	 */
	public void add(BuyProductPricealter buyProductPricealter){
		buyProductPricealterMapper.add(buyProductPricealter);
	}

	/**
	 * Add get id list.
	 * 保存修改申请并返回ID
	 * @param relatedId the related id
	 * @param updPrice  the upd price
	 * @return the list
	 */
	public List<String> addGetId(String relatedId,BigDecimal updPrice,String modifyReason) {
		List<String> list = new ArrayList<>();
		BuyShopProduct buyShopProduct = buyShopProductMapper.selectByPrimaryKey(relatedId);
		if(!ObjectUtil.isEmpty(buyShopProduct)){
			BuyProductPricealter buyProductPricealter = new BuyProductPricealter();
			buyProductPricealter.setId(ShiroUtils.getUid());
			buyProductPricealter.setRelatedId(buyShopProduct.getId());
			buyProductPricealter.setShopId("");
			buyProductPricealter.setShopCode("");
			buyProductPricealter.setShopName("");
			//状态 0、主账号审批；1，供应商审批；2通过，3未通过
			buyProductPricealter.setStatus(0);
			buyProductPricealter.setProductCode(buyShopProduct.getProductCode());
			buyProductPricealter.setProductName(buyShopProduct.getProductName());
			buyProductPricealter.setSkuCode(buyShopProduct.getSkuCode());
			buyProductPricealter.setSkuName(buyShopProduct.getSkuName());
			buyProductPricealter.setBarcode(buyShopProduct.getBarcode());
			buyProductPricealter.setPrice(buyShopProduct.getPrice());
			buyProductPricealter.setAlterPrice(updPrice);
			buyProductPricealter.setModifyReason(modifyReason);
			buyProductPricealter.setSuppId(buyShopProduct.getSupplierId());
			buyProductPricealter.setSuppBarcode("");
			buyProductPricealter.setIsDel(0);
			buyProductPricealter.setCreaterId(ShiroUtils.getUserId());
			buyProductPricealter.setCreateDate(new Date());
			buyProductPricealter.setCreaterName(ShiroUtils.getUserName());
			buyProductPricealterMapper.add(buyProductPricealter);
			buyShopProduct = new BuyShopProduct();
			buyShopProduct.setId(buyProductPricealter.getRelatedId());
			buyShopProduct.setIsPriceUpd(1);
			buyShopProduct.setPriceUpdDate(null);
			buyShopProduct.setPriceUpdPerson(ShiroUtils.getUserName());
			buyShopProduct.setIsPriceUpdConfirm(null);
			buyShopProductMapper.setPriceUpdStatus(buyShopProduct);
			list.add(buyProductPricealter.getId());
		}
		return list;
	}


	/**
	 * Update.
	 *
	 * @param buyProductPricealter the buy product pricealter
	 */
	public void update(BuyProductPricealter buyProductPricealter){
		buyProductPricealterMapper.update(buyProductPricealter);
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(String id){
		buyProductPricealterMapper.delete(id);
	}


	/**
	 * Verify success json object.
	 * 价格申请修改成功
	 * @param id the id
	 * @return the json object
	 */
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		try{
			BuyProductPricealter buyProductPricealter = new BuyProductPricealter();
			buyProductPricealter.setId(id);
			buyProductPricealter.setStatus(2);
			buyProductPricealterMapper.updateByPrimaryKeySelective(buyProductPricealter);

			buyProductPricealter = buyProductPricealterMapper.get(id);
			if(!ObjectUtil.isEmpty(buyProductPricealter)&&!ObjectUtil.isEmpty(buyProductPricealter.getRelatedId())){
				BuyShopProduct buyShopProduct = new BuyShopProduct();
				buyShopProduct.setId(buyProductPricealter.getRelatedId());
				buyShopProduct.setPrice(buyProductPricealter.getAlterPrice());
				buyShopProduct.setIsPriceUpd(1);
				buyShopProduct.setPriceUpdDate(new Date());
				buyShopProduct.setIsPriceUpdConfirm(0);
				buyShopProductMapper.updateByPrimaryKeySelective(buyShopProduct);
			}
			json.put("success", true);
			json.put("msg", "成功！");
		}catch (Exception e){
			json.put("success", false);
			json.put("msg", "失败！");
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * Verify error json object.
	 * 价格申请修改失败
	 * @param id the id
	 * @return the json object
	 */
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		try{
			BuyProductPricealter buyProductPricealter = new BuyProductPricealter();
			buyProductPricealter.setId(id);
			buyProductPricealter.setStatus(3);
			buyProductPricealterMapper.updateByPrimaryKeySelective(buyProductPricealter);
			buyProductPricealter = buyProductPricealterMapper.get(id);
			if(!ObjectUtil.isEmpty(buyProductPricealter)&&!ObjectUtil.isEmpty(buyProductPricealter.getRelatedId())){
				BuyShopProduct buyShopProduct = new BuyShopProduct();
				buyShopProduct.setId(buyProductPricealter.getRelatedId());
				//修改价格审批拒绝后价格不修改
			//	buyShopProduct.setPrice(buyProductPricealter.getAlterPrice());
				buyShopProduct.setIsPriceUpdConfirm(1);
				buyShopProductMapper.updateByPrimaryKeySelective(buyShopProduct);
			}
		}catch (Exception e){
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	
	
}
