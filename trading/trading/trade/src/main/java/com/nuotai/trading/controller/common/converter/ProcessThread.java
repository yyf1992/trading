package com.nuotai.trading.controller.common.converter;

/**
 * 转换器线程类
 * 
 * @ClassName ProcessThread
 * @author dxl
 * @Date 2017-7-25
 * @version 1.0.0
 */
public class ProcessThread implements Runnable {

	public static Integer threadNum = 0;

	@Override
	public void run() {
		try {
			threadNum+=1;
			QueueConverter.getInstants().startConvert();
		} finally  {
			if(threadNum>0){
				threadNum-=1;
			}
		}
	}

	public static void main(String[] args) {

		ProcessThread pt = new ProcessThread();
		Thread t = new Thread(pt);
		t.start();
		System.out.println("主线程结束");
	}

}
