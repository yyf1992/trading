package com.nuotai.trading.model;

import java.util.Date;

import lombok.Data;

@Data
public class BuyAddress {
    // 主键id
    private String id;
    // 收货人姓名
    private String personName;
    // 省
    private String province;
    private String provinceName;
    // 市
    private String city;
    private String cityName;
    // 区
    private String area;
    private String areaName;
    // 区号
    private String zone;
    // 电话号
    private String telNo;
    // 手机号
    private String phone;
    // 店铺Id
    private String companyId;
    // 是否默认地址 0表示是；1表示否
    private Integer isDefault;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    //
    private String createId;
    //
    private String createName;
    //
    private Date createDate;
    //
    private String updateId;
    //
    private String updateName;
    //
    private Date updateDate;
    //
    private String delId;
    //
    private String delName;
    //
    private Date delDate;
    // 收货地址
    private String addrName;
}