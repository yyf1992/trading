package com.nuotai.trading.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.DicCitiesMapper;
import com.nuotai.trading.model.DicCities;

@Service
public class DicCitiesService implements DicCitiesMapper {
	@Autowired
	private DicCitiesMapper mapper;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(DicCities record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(DicCities record) {
		return mapper.insertSelective(record);
	}

	@Override
	public DicCities selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(DicCities record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DicCities record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<DicCities> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	@Override
	public DicCities getCityById(String cityId) {
		return mapper.getCityById(cityId);
	}

}
