package com.nuotai.trading.controller.platform.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysPlatform;
import com.nuotai.trading.service.SysPlatformService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 平台表
 * 
 * @author zyn
 * @date 2017-08-03 13:04:48
 */
@Controller
@RequestMapping("platform/sysplatform")
public class SysPlatformController extends BaseController {
	@Autowired
	private SysPlatformService sysPlatformService;

	/**
	 * 列表
	 */
	@RequestMapping("/loadSysPlatformList")
	public String list(SearchPageUtil searchPageUtil,
			@RequestParam Map<String, Object> param) {
		if (searchPageUtil.getPage() == null) {
			searchPageUtil.setPage(new Page());
			param.put("status","-1");
		}
		param.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		// 查询列表数据
		List<Map<String, Object>> sysPlatformList = sysPlatformService
				.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(sysPlatformList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/system/platform/sysPlatformList";
	}

	@RequestMapping(value = "loadAddSysPlatform", method = RequestMethod.POST)
	public String loadAddSysPlatform() {
		return "platform/system/platform/addSysPlatform";
	}

	/**
	 * 保存新增平台
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveInsert")
	public String save(SysPlatform sysPlatform) {
		JSONObject json = new JSONObject();
		json = sysPlatformService.saveAdd(sysPlatform);
		return json.toString();
	}

	/**
	 * 加载修改平台界面
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@RequestMapping(value = "/loadEditPlatform", method = RequestMethod.POST)
	public String loadEditPlatform(String id) {
		SysPlatform sysPlatform = sysPlatformService.selectByPrimaryKey(id);
		model.addAttribute("SysPlatform", sysPlatform);
		return "platform/system/platform/editPlatform";
	}

	/**
	 * 保存修改平台
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateSysPlatform")
	public String updateSysPlatform(SysPlatform sysPlatform) {
		JSONObject json =sysPlatformService.saveUpdate(sysPlatform);
		return json.toString();
	}
	/**
	 * 删除平台
	 */
	@ResponseBody
	@RequestMapping("deleteSysPlatform")
	public String deleteSysPlatform(String id){
		SysPlatform sysPlatform=sysPlatformService.selectByPrimaryKey(id);
		JSONObject json =sysPlatformService.delete(sysPlatform);
		return json.toString();
	}

}
