package com.nuotai.trading.controller.common.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Component;

import com.nuotai.trading.model.ResQueue;
import com.nuotai.trading.service.ResQueueService;
import com.nuotai.trading.service.ResourcesService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.UplaodUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * 转换队列
 * 
 * @ClassName QueueConverter
 * @author dxl
 * @Date 2017-7-25
 * @version 1.0.0
 */
public class QueueConverter {

	/*@Autowired
	private ResQueueService rqService;
	@Autowired
	private ResourcesService resourcesService;*/
	
	private static  QueueConverter thread;

	public static QueueConverter getInstants() {

		if (thread == null) {
			thread = new QueueConverter();
		}
		return thread;

	}

	private QueueConverter() {
		super();
	}

	public synchronized void startConvert() {

		/*
		 * List<ResQueue> resCList = new ArrayList<ResQueue>();
		 * 
		 * ResQueue rq = new ResQueue(); rq.setUrl(
		 * "http://172.16.3.69:8880//file_server//upload//2017021016364359075720.doc"
		 * ); rq.setFileName("2017021016364359075720");
		 * rq.setToTypeFlag(UplaodUtil.CONVERTER_IMG); rq.setTxtType("doc");
		 * resCList.add(rq);
		 */

		// 获取所有
		ResQueueService rqService = Springfactory.getBean(ResQueueService.class);
		ResQueue rq = new ResQueue();
		rq.setBool(0);//处理失败的，不再继续处理
		List<ResQueue> resCList = rqService.getBeanList(rq);

		
		ResourcesService resourcesService = Springfactory.getBean(ResourcesService.class);
		for (ResQueue resQueue : resCList) {
			
			BaseConverter bc = null;

			/*if ("xls".equals(resQueue.getTxtType())||"xlsx".equals(resQueue.getTxtType())) {
//				bc = new PoiExcelConverter();
				bc = new PoiExcelConverter();
			} else if ("doc".equals(resQueue.getTxtType())) {
				bc = new PoiWordConverter();
			} else if ("docx".equals(resQueue.getTxtType())) {
				bc = new PoiWordXConverter();
			}*/ /*else if("xlsx".equals(resQueue.getTxtType())){
				
				bc = new PoiExcelXConverter();
			}*/
			if(bc==null){
				
				resQueue.setBool(1);
				resQueue.setResLog("当前不支持此类型文件转换！");
				rqService.updateBean(resQueue);
				continue;
			}
			try {
				bc.converter(resQueue);
			}  catch (Exception e1) {
				resQueue.setBool(1);
				resQueue.setResLog(e1.getMessage());
				rqService.updateBean(resQueue);
				e1.printStackTrace();
				continue;
			}
			String path = null;
			// 另外一台服务器地址
			String url = Constant.FILE_SERVER_URL;
			// 实例化jersey
			Client c = new Client();
			
			try {
				if (UplaodUtil.CONVERTER_IMG == resQueue.getToTypeFlag()) {
				
					resQueue.setImgUrl(bc.imageGeneratorHtml2Img(resQueue.getHtmlUrl()));
					// 图片保存数据库路径
					path = "upload/" + resQueue.getFileName() + ".png";
					url = url + path;
					// 设置请求路径
					WebResource wr = c.resource(url);
					wr.put(String.class, getContent(resQueue.getImgUrl()));
					resQueue.setImgUrl(url);
				}
				if (UplaodUtil.CONVERTER_HTML == resQueue.getToTypeFlag()){
					
					path = "upload/" + resQueue.getFileName() + ".html";
					url = url + path;
					// 设置请求路径
					WebResource wr = c.resource(url);
					wr.put(String.class, getContent(resQueue.getHtmlUrl()));
					resQueue.setHtmlUrl(url);
				}
			} catch (Exception e1) {
				resQueue.setBool(1);
				resQueue.setResLog(e1.getMessage());
				rqService.updateBean(resQueue);
				e1.printStackTrace();
				continue;
			} 
				if(resQueue.getBool()==0){
					
					try {
						//删除临时文件
						BaseConverter.deleteDir(new File(BaseConverter.path));
						//资源表添加信息
						//队列表删除信息
						resourcesService.addResoucesAndDeleteResQueue(resQueue);
					} catch (Exception e) {
						resQueue.setBool(1);
						resQueue.setResLog(e.getMessage());
						rqService.updateBean(resQueue);
						e.printStackTrace();
						continue;
					}
				}
			
		}

	}

	public byte[] getContent(String filePath) throws IOException {
		File file = new File(filePath);
		long fileSize = file.length();
		if (fileSize > Integer.MAX_VALUE) {
			System.out.println("file too big...");
			return null;
		}
		FileInputStream fi = new FileInputStream(file);
		byte[] buffer = new byte[(int) fileSize];
		int offset = 0;
		int numRead = 0;
		while (offset < buffer.length && (numRead = fi.read(buffer, offset, buffer.length - offset)) >= 0) {
			offset += numRead;
		}
		// 确保所有数据均被读取
		if (offset != buffer.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}
		fi.close();
		return buffer;
	}

}
