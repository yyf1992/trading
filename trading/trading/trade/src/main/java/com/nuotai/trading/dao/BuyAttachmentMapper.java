package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyAttachment;

public interface BuyAttachmentMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyAttachment record);

    int insertSelective(BuyAttachment record);

    BuyAttachment selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyAttachment record);

    int updateByPrimaryKey(BuyAttachment record);
    
    void deleteByRelatedId(String relatedId);
    
    List<BuyAttachment> selectByMap(Map<String, Object> map);
}