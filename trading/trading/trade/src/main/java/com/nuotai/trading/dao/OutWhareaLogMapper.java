package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2018-01-19 14:42:11
 */
public interface OutWhareaLogMapper extends BaseDao<OutWhareaLog> {
	
	List<OutWhareaLog> queryAllList(SearchPageUtil searchPageUtil);
	
	int saveOutWhareaLog(OutWhareaLog outWhareaLog);
	
	List<OutWhareaLog> queryByMap(Map<String,Object> map);
	
	//根据条件查询用于导出
    List<OutWhareaLog> selectOutWhareaLogByParams(Map<String,Object> params);
    
    List<OutWhareaLog> selectAll(Map<String,Object> params);
    
    void deleteByTime(Map<String,Object> params);
    
    List<OutWhareaLog> queryListByOutWhareaLog(OutWhareaLog outWhareaLog);
    
    int updateByOutWhareaLog(OutWhareaLog outWhareaLog);
}
