package com.nuotai.trading.model;


import java.util.Date;

import lombok.Data;

/*
 * 资源处理队列类
 * 1. 视频资源 加帧转换
 * 2. 文档资源 转换为swf
 */
@Data
public class ResQueue {
	
	private String id;
	
	private String resId;	//资源来源表id
	
	private String resCId;	//资源表id	
	
	private String txtType;	//资源类型 
	
	private Integer txtTypeFlag;//资源类型标识
	private Integer bool;	//视频处理成功标示 0：未处理  1：处理失败
	
	private String resLog;	//
	
	private Date addDateTime;	//添加时间
	
	private Integer addPersonId;	//添加人id

	private String url;
	
	private String htmlUrl;
	
	private String fileName;//文件名称
	
	private String imgUrl; //文件转换为图片的位置
	private Boolean isToImg;//是否转换为图片
	private Integer toTypeFlag;//转换类型标识 0：html 1：img
}
