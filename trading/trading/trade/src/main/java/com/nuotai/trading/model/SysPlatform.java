package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 平台表
 * 
 * @author zyn
 * @date 2017-08-03 13:04:48
 */
@Data
public class SysPlatform implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//平台代码
	private String platformCode;
	//平台名称
	private String platformName;
	//公司id
	private String companyId;
	//是否删除  0表示未删除 1表示已删除
	private Integer isDel;
	//创建时间
	private Date createDate;
	//创建人id
	private String createId;
	//创建人姓名
	private String createName;
	//修改时间
	private Date updateDate;
	//修改人id
	private String updateId;
	//修改人名字
	private String updateName;
	//删除时间
	private Date delDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
}
