package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.TradeVerifyFileMapper;
import com.nuotai.trading.model.TradeVerifyFile;



@Service
@Transactional
public class TradeVerifyFileService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeVerifyFileService.class);

	@Autowired
	private TradeVerifyFileMapper tradeVerifyFileMapper;
	
	public TradeVerifyFile get(String id){
		return tradeVerifyFileMapper.get(id);
	}
	
	public List<TradeVerifyFile> queryList(Map<String, Object> map){
		return tradeVerifyFileMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeVerifyFileMapper.queryCount(map);
	}
	
	public void add(TradeVerifyFile tradeVerifyFile){
		tradeVerifyFileMapper.add(tradeVerifyFile);
	}
	
	public void update(TradeVerifyFile tradeVerifyFile){
		tradeVerifyFileMapper.update(tradeVerifyFile);
	}
	
	public void delete(String id){
		tradeVerifyFileMapper.delete(id);
	}
	

}
