package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
@Data
public class TradeVerifyPocess implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//主表id
	private String headerId;
	//审批人员Id
	private String userId;
	//审批人员姓名
	private String userName;
	//状态：0-待审批；1-通过；2-拒接；3-已转交；4-已撤销
	private String status;
	//审批意见
	private String remark;
	//审批序列
	private Integer verifyIndex;
	//开始时间
	private Date startDate;
	//开始时间
	private String startDateFormat;
	//结束时间
	private Date endDate;
	//开始时间
	private boolean startIndext;
	private String fileUrl;
	private Date lastDate;
	private List<TradeVerifyFile> fileList;
}
