package com.nuotai.trading.model;

import lombok.Data;

/**
 * 钉钉消息
 * @author Administrator
 *
 */
@Data
public class DingDing {
	//消息内容
	private String content;
	//发送消息应用ID（显示是谁说话，显示谁的头像）。默认为【公告】发送
	private String agentid;
	//发送对象，用|分割
	private String touser;
	//发送消息类型（目前仅支持“text”类型）
	private String msgtype;
}
