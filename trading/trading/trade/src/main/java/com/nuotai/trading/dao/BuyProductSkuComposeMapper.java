package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyProductSkuCompose;

/**
 * 
 * 
 * @author "
 * @date 2017-09-11 17:48:54
 */
public interface BuyProductSkuComposeMapper extends BaseDao<BuyProductSkuCompose> {

	List<BuyProductSkuCompose> selectByPrimaryKey(String id);

	int saveCompose(BuyProductSkuCompose buCompose);

	List<Map<String, Object>> selectByBomId(Map<String,Object> map);

	void deleteByMap(Map<String,Object> map);
	
	void deleteComposeByMap(Map<String,Object> map);
	
	List<Map<String, Object>> selectByMap(Map<String,Object> map);
	
	BuyProductSkuCompose selectByParams(Map<String,Object> map);
	
	int updateByParams(BuyProductSkuCompose buCompose);
	
}
