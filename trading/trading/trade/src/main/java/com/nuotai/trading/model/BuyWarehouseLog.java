package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-11-03 10:28:12
 */
@Data
public class BuyWarehouseLog implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//发货批次单号
	private String batchNo;
	//采购订单ID
	private String orderId;
	//采购订单编号
	private String orderCode;
	//收货单明细ID
	private String buyDeliveryItemId;
	//采购计划单号
	private String applyCode;
	//公司ID
	private String companyId;
	//仓库Id
	private String warehouseId;
	//店铺Id
	private String shopId;
	//店铺Code
	private String shopCode;
	//店铺名称
	private String shopName;
	//仓库类型 0出库，1入库
	private Integer changeType;
	//一、出库：0:售出1:调货出库2:退货出库（次品退给供应商）二、入库：0:采购入库1:调货入库2:退货入库（顾客退货）
	private Integer stockType;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//价格
	private BigDecimal price;
	//数量
	private Integer number;
	//实际数量
	private Integer factNumber;
	//库存状态 0 已完结，1未完结，2已取消
	private Integer status;
	//出入库日期
	private Date storageDate;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人Id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
	
	
	// 供应商Id
    private String suppId;
    // 供应商名称
    private String suppName;
    //要求到货日期
    private Date predictArred;
    //仓库名称
    private String whareaName;
    //发货类型
    private String dropshipType;
    //入库单号
    private String storageNo;
}
