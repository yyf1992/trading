package com.nuotai.trading.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import com.nuotai.trading.controller.common.converter.ProcessThread;
import com.nuotai.trading.controller.common.exception.CommException;
import com.nuotai.trading.model.ResQueue;
import com.nuotai.trading.service.ResQueueService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

public class UplaodUtil {

	public static final Integer CONVERTER_HTML = 0;
	public static final Integer CONVERTER_IMG = 1;

	public String uploadFile(MultipartFile file, String resId, boolean isConverter)
			throws CommException, UniformInterfaceException, ClientHandlerException, IOException {
		if (file == null) {
			throw new CommException("不能传入空文件");
		}
		// 获取文件名
		String fileName = file.getOriginalFilename();
		// 获取文件后缀名
		String fileExtensionName = FilenameUtils.getExtension(fileName);
		// 获取文件大小
		String fileSize = ShiroUtils.convertFileSize(file.getSize());
		// 文件名称生成策略
		String url = null;
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String format = df.format(new Date());
		// 防止同一时间上传图片，不能用时间作为区分
		// 用随机数
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			// 产生100以内的随机数
			format += r.nextInt(10);
		}
		// 图片保存数据库路径
		String path = "upload/" + format + "." + fileExtensionName;

		// 另外一台服务器地址
		url = Constant.FILE_SERVER_URL;
		url = url + path;
		// 实例化jersey
		Client c = new Client();
		// 设置请求路径
		WebResource wr = c.resource(url);
		wr.put(String.class, file.getBytes());

		if (isConverter) {

			// 图片类型不进行转换
			String allowExt = "xlsx|xls|doc|docx";
			if (-1 != allowExt.indexOf(fileExtensionName)) {

				// 判断文件大小，大于3M不进行转换
				if (file.getSize() < (1048576 * 3)) {

					ResQueue resQueue = new ResQueue();
					resQueue.setId(ShiroUtils.getUid());
					resQueue.setResId(resId);
					resQueue.setUrl(Constant.FILE_SERVER_URL + "/" + path);
					resQueue.setTxtType(fileExtensionName.toLowerCase());
					resQueue.setFileName(format);
					resQueue.setToTypeFlag(("xlsx".equals(resQueue.getTxtType()) || "xls".equals(resQueue.getTxtType())
							? CONVERTER_HTML : CONVERTER_IMG));
					resQueue.setAddDateTime(new Date());
					resQueue.setBool(0);
					ResQueueService rqService = ElFunction.getBean(ResQueueService.class);

					rqService.addBean(resQueue);
					// resQueue.setToTypeFlag(converterType);
					// 如果正有一个线程等待转换，则不再创建新线程；
					// 因为第二个线程执行时会
					if (ProcessThread.threadNum < 2) {

						// 开线程转换
						ProcessThread pt = new ProcessThread();
						Thread t = new Thread(pt);
						t.start();
					}
				}
			}
		}
		return url;
	}

}
