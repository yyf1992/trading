package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 09:33:15
 */
public interface BuyWarehouseLogMapper extends BaseDao<BuyWarehouseLog> {
	
	List<Map<String,Object>> selectProdcutsLogByPage(SearchPageUtil searchPageUtil);
	
	BuyWarehouseLog selectByOrderId(String orderId);
	
	List<BuyWarehouseLog> selectProdcutsLog();
	
	//根据条件查询用于导出
	List<BuyWarehouseLog> selectProdcutsLogByParams(Map<String,Object> params);
	
	//根据时间段汇总出入明细
	List<BuyWarehouseLog> selectByDate(Map<String,Object> map);
	
	//根据时间段计算互通和手工出入库的数量和成本
	BuyWarehouseLog selectNumAndPriceByDate(Map<String,Object> map);
	
	//根据时间段计算其他出入库数量和成本
	BuyWarehouseLog selectOtherOutAndInByDate(Map<String,Object> map);
	
//	//根据时间段计算手工入库
//	BuyWarehouseLog selectBuyManualByDate(Map<String,Object> map);
//	//根据时间段计算其他入库
//	BuyWarehouseLog selectOtherInByDate(Map<String,Object> map);
//	
//	//根据时间段计算互通出库
//	BuyWarehouseLog selectSellerByDate(Map<String,Object> map);
//	//根据时间段计算手工出库
//	BuyWarehouseLog selectSellerManualByDate(Map<String,Object> map);
//	//根据时间段计算其他出库
//	BuyWarehouseLog selectOtherOutByDate(Map<String,Object> map);
}
