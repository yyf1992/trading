package com.nuotai.trading.dao;

import java.util.List;

import com.nuotai.trading.model.BuyProductPricealter;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author liuhui
 * @date 2017-11-20 13:26:35
 */
public interface BuyProductPricealterMapper extends BaseDao<BuyProductPricealter> {

    int updateByPrimaryKeySelective(BuyProductPricealter buyProductPricealter);
    
    List<BuyProductPricealter> selectByRelatedId(String relatedId);
    
    List<BuyProductPricealter> selectAlterPriceHistory(SearchPageUtil searchPageUtil);
}
