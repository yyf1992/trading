package com.nuotai.trading.controller.common.base;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.nuotai.trading.utils.json.ServiceException;

public interface IExcelService {
	Object init(MultipartFile excel, Map<String, Object> params)throws ServiceException;
}
