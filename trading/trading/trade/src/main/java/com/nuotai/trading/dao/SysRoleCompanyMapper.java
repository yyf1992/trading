package com.nuotai.trading.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.SysRoleCompany;
import com.nuotai.trading.utils.SearchPageUtil;

public interface SysRoleCompanyMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRoleCompany record);

    int insertSelective(SysRoleCompany record);

    SysRoleCompany selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoleCompany record);

    int updateByPrimaryKey(SysRoleCompany record);
    
    List<SysRoleCompany> selectRightByPageCompany(SearchPageUtil searchPageUtil);
    
    int deleteByIdList(@Param("idList")String[] idStr);

	List<String> getMenuIdListByCompanyId(String companyId);
}