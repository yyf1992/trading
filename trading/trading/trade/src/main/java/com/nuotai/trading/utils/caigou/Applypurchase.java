package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 同步采购系统中的店铺采购单到买卖系统中的销售计划
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class Applypurchase {
	SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Applypurchase applypurchase = new Applypurchase();
		applypurchase.dealWith();
	}
	
	public void dealWith(){
		try {
			Connection purchaseConn = DBUtils.getTradeConnection();
			//获得数据
			List<Map<String,Object>> dataList = getPurchaseData();
			int length = dataList.size();
			for(Map<String,Object> datalMap : dataList){
				System.out.println("剩余："+length+"条数据,总计："+dataList.size());
				insertData(datalMap,purchaseConn);
				length--;
			}
			DBUtils.closeConnection(purchaseConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void insertData(Map<String, Object> datalMap,Connection conn) throws Exception{
		String apply_code = datalMap.get("apply_code").toString();
		String countSql = "select count(apply_code) num from buy_applypurchase_header where apply_code='"+apply_code+"'";
		PreparedStatement ps = conn.prepareStatement(countSql);
		ResultSet rs = ps.executeQuery();
		int result = 0;
		while (rs.next()) {
			result = rs.getInt("num");
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		if(result == 0){
			StringBuffer insertHeaderSql = new StringBuffer("INSERT INTO `buy_applypurchase_header` VALUES (");
			insertHeaderSql.append("'"+datalMap.get("id")+"',");//id
			insertHeaderSql.append("'17060909542281121440',");//company_id
			insertHeaderSql.append("'"+datalMap.get("apply_code")+"',");//apply_code
			insertHeaderSql.append("'"+datalMap.get("title")+"',");//title
			String remark = datalMap.get("remark")==null?"":datalMap.get("remark").toString();
			insertHeaderSql.append("'"+remark+"',");//remark
			insertHeaderSql.append(datalMap.get("goods_count")+",");//goods_count
			insertHeaderSql.append(datalMap.get("arrival_count")+",");//arrival_count
			//status状态
			int status = Integer.parseInt(datalMap.get("status").toString());
			switch (status) {
			case 0:
				//等待平台审核
				insertHeaderSql.append("'0',");
				break;
			case 1:
				//等待采购审核
				insertHeaderSql.append("'0',");
				break;
			case 2:
				//平台审核不通过
				insertHeaderSql.append("'2',");
				break;
			case 3:
				//采购审核不通过
				insertHeaderSql.append("'2',");
				break;
			case 4:
				//完结
				insertHeaderSql.append("'1',");
				break;
			case 5:
				//关闭
				insertHeaderSql.append("'1',");
				break;
			default:
				//
				insertHeaderSql.append("'3',");
				break;
			}
			insertHeaderSql.append("'"+datalMap.get("shop_id")+"',");//shop_id
			insertHeaderSql.append("'"+datalMap.get("shop_code")+"',");//shop_code
			insertHeaderSql.append("'"+datalMap.get("shop_name")+"',");//shop_name
			insertHeaderSql.append("'0',");//is_del
			insertHeaderSql.append("'"+datalMap.get("createName")+"',");//create_name
			insertHeaderSql.append("'"+datalMap.get("creater")+"',");//creater
			insertHeaderSql.append("'"+datalMap.get("create_date")+"',");//create_date
			insertHeaderSql.append("null,");//update_id
			insertHeaderSql.append("null,");//update_name
			insertHeaderSql.append("null,");//update_date
			insertHeaderSql.append("null,");//del_id
			insertHeaderSql.append("null,");//del_name
			insertHeaderSql.append("null,");//del_date
			String proof = datalMap.get("proof")==null?"":datalMap.get("proof").toString();
			insertHeaderSql.append("'"+proof+"',");//proof
			insertHeaderSql.append(datalMap.get("predictArred")==null?"null,":"'"+datalMap.get("predictArred")+"',");//predictArred
			insertHeaderSql.append("null");//sale_plan_id_str
			insertHeaderSql.append(");");
			PreparedStatement insertPs = conn.prepareStatement(insertHeaderSql.toString());
			int result2 = insertPs.executeUpdate();
			// 关闭声明
			DBUtils.closeStatement(insertPs);
			if(result2 > 0){
				//新增子信息
				List<Map<String,Object>> childList = (List<Map<String,Object>>)datalMap.get("itemList");
				if(childList != null && !childList.isEmpty()){
					for(Map<String,Object> item : childList){
						insertItem(item,conn,datalMap);
					}
				}
				//插入审批信息
				List<Map<String,Object>> verifyList = (List<Map<String,Object>>)datalMap.get("verifyList");
				if(verifyList != null && !verifyList.isEmpty()){
					//新增审批主表
					insertVerifyHeader(datalMap,conn);
					for(int i = 0; i < verifyList.size(); i++){
						Map<String,Object> verify = verifyList.get(i);
						insertVerifyPocess(verify,conn,i);
					}
				}
			}
		}
	}

	/**
	 * 新增审批主表
	 * @param datalMap
	 * @param conn
	 */
	private void insertVerifyHeader(Map<String, Object> datalMap,
			Connection conn) throws Exception{
		StringBuffer insertVerifyHeaderSql = new StringBuffer("INSERT INTO trade_verify_header (");
		insertVerifyHeaderSql.append("id,title,related_id,verify_success,verify_error,related_url,site_id,company_id,");
		insertVerifyHeaderSql.append("status,remark,create_id,create_name,create_date,");
		insertVerifyHeaderSql.append("update_user_id,update_user_name,update_date)");
		insertVerifyHeaderSql.append(" values (");
		insertVerifyHeaderSql.append("'"+datalMap.get("id")+"',");//id
		insertVerifyHeaderSql.append("'原采购系统店铺采购计划',");//title
		insertVerifyHeaderSql.append("'"+datalMap.get("id")+"',");//related_id
		insertVerifyHeaderSql.append("'buyer/applyPurchaseHeader/verifySuccess',");//verify_success
		insertVerifyHeaderSql.append("'buyer/applyPurchaseHeader/verifyError',");//verify_error
		insertVerifyHeaderSql.append("'buyer/applyPurchaseHeader/loadApplyPurchaseDetails?verifyDetails=1',");//related_url
		insertVerifyHeaderSql.append("'17111514043383846004',");//site_id
		insertVerifyHeaderSql.append("'17060909542281121440',");//company_id
		//status状态
		int status = Integer.parseInt(datalMap.get("status").toString());
		switch (status) {
		case 0:
			//等待平台审核
			insertVerifyHeaderSql.append("'0',");
			break;
		case 1:
			//等待采购审核
			insertVerifyHeaderSql.append("'0',");
			break;
		case 2:
			//平台审核不通过
			insertVerifyHeaderSql.append("'2',");
			break;
		case 3:
			//采购审核不通过
			insertVerifyHeaderSql.append("'2',");
			break;
		case 4:
			//完结
			insertVerifyHeaderSql.append("'1',");
			break;
		case 5:
			//关闭
			insertVerifyHeaderSql.append("'1',");
			break;
		default:
			//
			insertVerifyHeaderSql.append("'3',");
			break;
		}
		insertVerifyHeaderSql.append("'原采购系统数据导入',");//remark
		insertVerifyHeaderSql.append("'"+datalMap.get("creater")+"',");//create_id
		insertVerifyHeaderSql.append("'"+datalMap.get("createName")+"',");//create_name
		insertVerifyHeaderSql.append("'"+datalMap.get("create_date")+"',");//create_date
		insertVerifyHeaderSql.append("null,");//update_user_id
		insertVerifyHeaderSql.append("null,");//update_user_name
		insertVerifyHeaderSql.append("null");//update_date
		insertVerifyHeaderSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertVerifyHeaderSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 新增审批数据
	 * @param verify
	 * @param conn
	 * @param i
	 */
	private void insertVerifyPocess(Map<String, Object> verify, Connection conn,
			int i) throws Exception{
		StringBuffer insertVerifyPocessSql = new StringBuffer("INSERT INTO trade_verify_pocess (");
		insertVerifyPocessSql.append("id,header_id,user_id,user_name,status,remark,");
		insertVerifyPocessSql.append("verify_index,start_date,end_date)");
		insertVerifyPocessSql.append(" values (");
		insertVerifyPocessSql.append("'"+verify.get("id")+"',");//id
		insertVerifyPocessSql.append("'"+verify.get("relatedid")+"',");//header_id
		insertVerifyPocessSql.append("'"+verify.get("verifyuser")+"',");//user_id
		insertVerifyPocessSql.append("'"+verify.get("verifyusername")+"',");//user_name
		//status状态
		int status = Integer.parseInt(verify.get("status").toString());
		if(status==0){
			//同意
			insertVerifyPocessSql.append("'1',");
		}else{
			//驳回
			insertVerifyPocessSql.append("'2',");
		}
		String reason = verify.get("reason")==null?"":verify.get("reason").toString();
		int verifytype = Integer.parseInt(verify.get("verifytype").toString());
		if(verifytype==0){
			insertVerifyPocessSql.append("'平台审批 "+reason+"',");//remark
		}else{
			insertVerifyPocessSql.append("'采购审批 "+reason+"',");//remark
		}
		insertVerifyPocessSql.append(i+",");//verify_index
		insertVerifyPocessSql.append("'"+verify.get("verifytime")+"',");//start_date
		insertVerifyPocessSql.append("'"+verify.get("verifytime")+"'");//end_date
		insertVerifyPocessSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertVerifyPocessSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 获得审批信息
	 * @param Connection
	 * @return
	 */
	private List<Map<String, Object>> getVerify(Connection purchaseConn) throws Exception{
		System.out.println("正在获取审批数据。。。。。");
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT id,relatedid,verifyuser,verifyusername,verifytime,`status`,reason,verifytype FROM verify ORDER BY verifytime";
		PreparedStatement ps = purchaseConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>(8);
			map.put("id", rs.getString("id"));
			map.put("relatedid", rs.getString("relatedid"));
			map.put("verifyuser", rs.getString("verifyuser"));
			map.put("verifyusername", rs.getString("verifyusername"));
			map.put("verifytime", rs.getString("verifytime"));
			map.put("status", rs.getString("status"));
			map.put("reason", rs.getString("reason"));
			map.put("verifytype", rs.getString("verifytype"));
			System.out.println(map);
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		System.out.println("获取审批系统数据结束");
		return dataList;
	}

	/**
	 * 新增商品信息
	 * @param item
	 * @param conn
	 * @param headerMap
	 * @throws Exception
	 */
	private void insertItem(Map<String, Object> item, Connection conn,Map<String, Object> headerMap) throws Exception{
		StringBuffer insertItemSql = new StringBuffer("INSERT INTO buy_applypurchase_item (");
		insertItemSql.append("id,apply_id,product_code,product_name,sku_code,sku_name,barcode,unit_id,unit_name,category_id,category_code,category_name,");
		insertItemSql.append("apply_count,sale_plan,confirm_sale_plan,type)");
		insertItemSql.append(" values (");
		insertItemSql.append("'"+item.get("id")+"',");//id
		insertItemSql.append("'"+item.get("apply_id")+"',");//apply_id
		insertItemSql.append("'"+item.get("pro_code")+"',");//pro_code
		insertItemSql.append("'"+item.get("pro_name")+"',");//pro_name
		insertItemSql.append("'"+item.get("sku_code")+"',");//sku_code
		insertItemSql.append("'"+item.get("sku_name")+"',");//sku_name
		insertItemSql.append("'"+item.get("sku_id")+"',");//sku_id
		insertItemSql.append("'17070710044995394265',");//unit_id
		insertItemSql.append("'件',");//unit_name
		insertItemSql.append("'"+item.get("category_id")+"',");//category_id
		insertItemSql.append("'"+item.get("category_code")+"',");//category_code
		insertItemSql.append("'"+item.get("category_name")+"',");//category_name
		insertItemSql.append(item.get("already_count")+",");//apply_count
		insertItemSql.append(item.get("apply_count")+",");//sale_plan
		insertItemSql.append(item.get("firm_count")+",");//confirm_sale_plan
		insertItemSql.append("0");//type
		insertItemSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertItemSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
		StringBuffer insertShopSql = new StringBuffer("INSERT INTO buy_applypurchase_shop (");
		insertShopSql.append("id,item_id,plan_code,shop_id,shop_code,shop_name,sales_num,confirm_sales_num,put_storage_num,confirm_put_storage_num)");
		insertShopSql.append(" values (");
		insertShopSql.append("'"+item.get("id")+"',");//id
		insertShopSql.append("'"+item.get("id")+"',");//apply_id
		insertShopSql.append("'"+headerMap.get("apply_code")+"',");//pro_code
		insertShopSql.append("'"+headerMap.get("shop_id")+"',");//pro_name
		insertShopSql.append("'"+headerMap.get("shop_code")+"',");//sku_code
		insertShopSql.append("'"+headerMap.get("shop_name")+"',");//sku_name
		insertShopSql.append(item.get("apply_count")+",");//sale_plan
		insertShopSql.append(item.get("firm_count")+",");//confirm_sale_plan
		insertShopSql.append("0,");//put_storage_num
		insertShopSql.append("0");//confirm_put_storage_num
		insertShopSql.append(")");
		PreparedStatement insertShopPs = conn.prepareStatement(insertShopSql.toString());
		insertShopPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertShopPs);
	}
	
	/**
	 * 获得采购系统中的数据
	 * @return
	 * @throws Exception
	 */
	private List<Map<String,Object>> getPurchaseData() throws Exception{
		System.out.println("正在获取采购系统数据。。。。。");
		Connection purchaseConn = DBUtils.getPurchaseConnection();
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT * FROM applypurchase_header";
		PreparedStatement ps = purchaseConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = ps.getMetaData();
		// 取得结果集列数
        int columnCount = rsmd.getColumnCount();
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>();
			for (int i = 1; i < columnCount; i++) {
				map.put(rsmd.getColumnLabel(i), rs.getObject(rsmd.getColumnLabel(i)));
            }
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		
		if(dataList != null && !dataList.isEmpty()){
			//获得子信息
			List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
			String selectItemSql = "SELECT * FROM applypurchase_item";
			PreparedStatement ps2 = purchaseConn.prepareStatement(selectItemSql);
			ResultSet rs2 = ps2.executeQuery();
			ResultSetMetaData rsmd2 = ps2.getMetaData();
			// 取得结果集列数
			int columnCount2 = rsmd2.getColumnCount();
			while (rs2.next()) {
				Map<String,Object> map = new HashMap<String,Object>();
				for (int i = 1; i < columnCount2; i++) {
					map.put(rsmd2.getColumnLabel(i), rs2.getObject(rsmd2.getColumnLabel(i)));
				}
				itemList.add(map);
			}
			// 关闭记录集
			DBUtils.closeResultSet(rs2);
			// 关闭声明
			DBUtils.closeStatement(ps2);
			//获得审批信息
			List<Map<String,Object>> verifyList = getVerify(purchaseConn);
			for(Map<String,Object> data : dataList){
				String id = data.get("id").toString();
				List<Map<String,Object>> childList = new ArrayList<Map<String,Object>>();
				if(itemList != null && !itemList.isEmpty()){
					for(Map<String,Object> item : itemList){
						String applyId = item.get("apply_id").toString();
						if(id.equals(applyId)){
							childList.add(item);
						}
					}
				}
				data.put("itemList", childList);
				List<Map<String,Object>> verifyChildList = new ArrayList<Map<String,Object>>();
				if(verifyList != null && !verifyList.isEmpty()){
					for(Map<String,Object> verify : verifyList){
						String relatedid = verify.get("relatedid").toString();
						if(id.equals(relatedid)){
							verifyChildList.add(verify);
						}
					}
				}
				data.put("verifyList", verifyChildList);
			}
		}
		// 关闭链接对象
		DBUtils.closeConnection(purchaseConn);
		System.out.println("获取采购系统数据结束");
		return dataList;
	}
}
