package com.nuotai.trading.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyProductMapper;
import com.nuotai.trading.dao.BuyProductSkuMapper;
import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017\7\27 0027.
 */

@Service
@Transactional
public class BuyProductSkuService {
	private static final Logger LOG = LoggerFactory
			.getLogger(BuyProductSkuService.class);

	@Autowired
	private BuyProductSkuMapper buyProductSkuMapper;
	@Autowired
	private BuyProductMapper buyProductMapper;

	public BuyProductSku get(String id) {
		return buyProductSkuMapper.get(id);
	}

	public List<BuyProductSku> queryList(Map<String, Object> map) {
		return buyProductSkuMapper.queryList(map);
	}

	public int queryCount(Map<String, Object> map) {
		return buyProductSkuMapper.queryCount(map);
	}

	public void add(BuyProductSku productSku) {
		buyProductSkuMapper.add(productSku);
	}

	public void update(BuyProductSku productSku) {
		buyProductSkuMapper.update(productSku);
	}

	public void delete(String id) {
		buyProductSkuMapper.delete(id);
	}

	public List<BuyProductSku> querySkuList(Map<String, Object> map) {
		return buyProductSkuMapper.querySkuList(map);
	}

	public List<Map<String, Object>> selectProductByMap(
			Map<String, Object> params) {
		return buyProductSkuMapper.selectProductByMap(params);
	}

	public JSONObject insertSelective(BuyProductSku buyProductSku) {
		JSONObject object = new JSONObject();
		object = validateSku(buyProductSku);
		if (!object.getBooleanValue("success")) {
			return object;
		}
		int result = 0;
		buyProductSku.setId(ShiroUtils.getUid());
		buyProductSku.setCreateDate(new Date());
		buyProductSku.setCreateId(ShiroUtils.getUserId());
		buyProductSku.setCreateName(ShiroUtils.getUserName());
		buyProductSku.setCompanyId(ShiroUtils.getCompId());
		result = buyProductSkuMapper.insertSelective(buyProductSku);
		if (result > 0) {
			object.put("success", true);
			object.put("msg", "添加成功！");
		} else {
			object.put("success", false);
			object.put("msg", "添加失败！");
		}
		return object;
	}

	public JSONObject validateSku(BuyProductSku buyProductSku) {
		JSONObject object = new JSONObject();
		if ("".equals(buyProductSku.getSkuCode())) {
			object.put("success", false);
			object.put("msg", "商品规格代码不能为空！");
			return object;
		}
		if ("".equals(buyProductSku.getBarcode())) {
			object.put("success", false);
			object.put("msg", "商品条形码不能为空！");
			return object;
		}
		String standardStock = String.valueOf(buyProductSku.getStandardStock());
		String minStock = String.valueOf(buyProductSku.getMinStock());
		String price = String.valueOf(buyProductSku.getPrice());
		String planSalesDays = String.valueOf(buyProductSku.getPlanSalesDays());
		if (standardStock == "null" || "".equals(standardStock)) {
			object.put("success", false);
			object.put("msg", "标准库存不能为空！");
			return object;
		}
		if (minStock == "null" || "".equals(minStock)) {
			object.put("success", false);
			object.put("msg", "库存下限不能为空！");
			return object;
		}
		if (price == "null" || "".equals(price)) {
			object.put("success", false);
			object.put("msg", "商品销售单价不能为空！");
			return object;
		}
		if (planSalesDays == "null" || "".equals(planSalesDays)) {
			object.put("success", false);
			object.put("msg", "商品销售天数不能为空！");
			return object;
		}

		List<BuyProductSku> buyProductSkus = buyProductSkuMapper
				.selectProductByBarCode(buyProductSku);
		if (buyProductSkus.size() > 0) {
			object.put("success", false);
			object.put("msg", "商品条形码已存在！");
			return object;
		}

		object.put("success", true);
		object.put("msg", "");
		return object;
	}
	
	/**
	 * 修改时校验信息
	 * @param buyProductSku
	 * @return
	 */
	public JSONObject updateValidateSku(BuyProductSku buyProductSku) {
		JSONObject object = new JSONObject();
		if ("".equals(buyProductSku.getSkuCode())) {
			object.put("success", false);
			object.put("msg", "商品规格代码不能为空！");
			return object;
		}
		if ("".equals(buyProductSku.getColorCode())) {
			object.put("success", false);
			object.put("msg", "商品颜色不能为空！");
			return object;
		}
		if ("".equals(buyProductSku.getBarcode())) {
			object.put("success", false);
			object.put("msg", "商品条形码不能为空！");
			return object;
		}
		String standardStock = String.valueOf(buyProductSku.getStandardStock());
		String minStock = String.valueOf(buyProductSku.getMinStock());
		String price = String.valueOf(buyProductSku.getPrice());
		String planSalesDays = String.valueOf(buyProductSku.getPlanSalesDays());
		if (standardStock == "null" || "".equals(standardStock)) {
			object.put("success", false);
			object.put("msg", "标准库存不能为空！");
			return object;
		}
		if (minStock == "null" || "".equals(minStock)) {
			object.put("success", false);
			object.put("msg", "库存下限不能为空！");
			return object;
		}
		if (price == "null" || "".equals(price)) {
			object.put("success", false);
			object.put("msg", "商品销售单价不能为空！");
			return object;
		}
		if (planSalesDays == "null" || "".equals(planSalesDays)) {
			object.put("success", false);
			object.put("msg", "商品销售天数不能为空！");
			return object;
		}

		object.put("success", true);
		object.put("msg", "");
		return object;
	}

	public JSONObject deleteSku(BuyProductSku buyProductSku) {
		JSONObject object = new JSONObject();
		int result = 0;
		buyProductSku.setDelDate(new Date());
		buyProductSku.setDelId(ShiroUtils.getUserId());
		buyProductSku.setDelName(ShiroUtils.getUserName());
		result = buyProductSkuMapper.deleteByPrimaryKey(buyProductSku);
		if (result > 0) {
			object.put("success", true);
			object.put("msg", "删除成功！");
		} else {
			object.put("success", false);
			object.put("msg", "删除失败！");
		}
		return object;
	}

	public BuyProductSku selectByPrimaryKey(String id) {
		return buyProductSkuMapper.selectByPrimaryKey(id);
	}

	public JSONObject updateSku(BuyProductSku buyProductSku) {
		JSONObject object = updateValidateSku(buyProductSku);
		if (!object.getBooleanValue("success")) {
			return object;
		}
		int result = 0;
		buyProductSku.setUpdateDate(new Date());
		buyProductSku.setUpdateId(ShiroUtils.getUserId());
		buyProductSku.setUpdateName(ShiroUtils.getUserName());
		result = buyProductSkuMapper.updateByPrimaryKey(buyProductSku);
		if (result > 0) {
			object.put("success", true);
			object.put("msg", "修改成功！");
		} else {
			object.put("success", false);
			object.put("msg", "修改失败！");
		}
		return object;
	}

	public List getColor(String companyId) {
		return buyProductSkuMapper.getColor(companyId);
	}

	public List getSkuCode(String companyId) {
		return buyProductSkuMapper.getSkuCode(companyId);
	}

	public List<Map<String, Object>> selectList(SearchPageUtil searchPageUtil) {
		return buyProductSkuMapper.selectList(searchPageUtil);
	}

	public List<Map<String, Object>> inputSelect(SearchPageUtil searchPageUtil) {
		return buyProductSkuMapper.inputSelect(searchPageUtil);
	}
	
	public List<BuyProductSku> selectByBarCode(String barcode){
		return buyProductSkuMapper.selectByBarCode(barcode);
	}

	public List<BuyProductSku> selectByPage(SearchPageUtil searchPageUtil) {
		return buyProductSkuMapper.selectByPage(searchPageUtil);
	}
	/*
	 * 查询所有的商品
	 */
	public List<Map<String, Object>> selectAllProduct(Map<String, Object> params) {
		return buyProductSkuMapper.selectAllProduct(params);
	}
	/**
	 * 保存同步OMS商品信息
	 * @param mapLinkman
	 * @return
	 * @throws Exception
	 */
	public int saveAddOMSProduct(net.sf.json.JSONObject mapLinkman) throws Exception {
		int num = 0;
		//将商品信息插入商品表
		BuyProduct buyProduct = new BuyProduct();
		buyProduct.setProductCode(mapLinkman.getString("procode"));
		BuyProduct oldProduct = buyProductMapper.selectByProductCode(buyProduct.getProductCode());
		if (ObjectUtil.isEmpty(oldProduct)){
			buyProduct.setId(ShiroUtils.getUid());
			buyProduct.setCompanyId(ShiroUtils.getCompId());
			if (mapLinkman.containsKey("goodsName")){
				buyProduct.setProductName(mapLinkman.getString("goodsName"));
			}else{
				buyProduct.setProductName(mapLinkman.getString("skuname"));
			}
			//商品单位默认为：件
			buyProduct.setUnitId("17070710044995394265");
			String categoryName = "";
			if (mapLinkman.containsKey("categoryName")){
				categoryName = mapLinkman.getString("categoryName").toString();
			}			
			if (categoryName.equals("虚拟商品")){
				buyProduct.setProductType(3);
			}else if (categoryName.equals("辅料")){
				buyProduct.setProductType(2);
			}else if (categoryName.equals("面料") || categoryName.equals("原材料")){
				buyProduct.setProductType(1);
			}else{
				buyProduct.setProductType(0);
			}
			buyProduct.setCreateId(ShiroUtils.getUserId());
			buyProduct.setCreateName(ShiroUtils.getUserName());
			buyProduct.setCreateDate(new Date());
			num = buyProductMapper.insertSelective(buyProduct);
		}else{
			oldProduct.setCompanyId(ShiroUtils.getCompId());
			if (mapLinkman.containsKey("goodsName")){
				oldProduct.setProductName(mapLinkman.getString("goodsName"));
			}else{
				oldProduct.setProductName(mapLinkman.getString("skuname"));
			}
			String categoryName = "";
			if (mapLinkman.containsKey("categoryName")){
				categoryName = mapLinkman.getString("categoryName").toString();
			}			
			if (categoryName.equals("虚拟商品")){
				oldProduct.setProductType(3);
			}else if (categoryName.equals("辅料")){
				oldProduct.setProductType(2);
			}else if (categoryName.equals("面料") || categoryName.equals("原材料")){
				oldProduct.setProductType(1);
			}else{
				oldProduct.setProductType(0);
			}
			oldProduct.setUpdateId(ShiroUtils.getUserId());
			oldProduct.setUpdateName(ShiroUtils.getUserName());
			oldProduct.setUpdateDate(new Date());
			num = buyProductMapper.updateByPrimaryKeySelective(oldProduct);
		}
		//将商品信息插入商品SKU表
		BuyProductSku  buyProductSku = new BuyProductSku();
		buyProductSku.setBarcode(mapLinkman.getString("skuoid"));
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("barcode", buyProductSku.getBarcode());
	//	map.put("productCode", buyProduct.getProductCode());
		BuyProductSku  oldProductSku = buyProductSkuMapper.getByBarcode(map);
		if (ObjectUtil.isEmpty(oldProductSku)){
			buyProductSku.setId(ShiroUtils.getUid());
			buyProductSku.setCompanyId(ShiroUtils.getCompId());
			buyProductSku.setProductCode(mapLinkman.getString("procode"));
			if (mapLinkman.containsKey("goodsName")){
				buyProductSku.setProductName(mapLinkman.getString("goodsName"));
			}else{
				buyProductSku.setProductName(mapLinkman.getString("skuname"));
			}
			buyProductSku.setSkuCode(mapLinkman.getString("skucode"));
			buyProductSku.setSkuName(mapLinkman.getString("skuname"));
			//商品单位默认为：件
			buyProductSku.setUnitId("17070710044995394265");
			String categoryName = "";
			if (mapLinkman.containsKey("categoryName")){
				categoryName = mapLinkman.getString("categoryName").toString();
			}			
			if (categoryName.equals("虚拟商品")){
				buyProductSku.setProductType(3);
			}else if (categoryName.equals("辅料")){
				buyProductSku.setProductType(2);
			}else if (categoryName.equals("面料") || categoryName.equals("原材料")){
				buyProductSku.setProductType(1);
			}else{
				buyProductSku.setProductType(0);
			}
			//默认计划销售天数:1天
			buyProductSku.setPlanSalesDays(1);
			buyProductSku.setPrice(new BigDecimal(mapLinkman.getString("cost")));
			buyProductSku.setCreateId(ShiroUtils.getUserId());
			buyProductSku.setCreateName(ShiroUtils.getUserName());
			buyProductSku.setCreateDate(new Date());
			num = buyProductSkuMapper.insertSelective(buyProductSku);
		}else{
			oldProductSku.setBarcode(mapLinkman.getString("skuoid"));
			oldProductSku.setCompanyId(ShiroUtils.getCompId());
			oldProductSku.setProductCode(mapLinkman.getString("procode"));
			if (mapLinkman.containsKey("goodsName")){
				oldProductSku.setProductName(mapLinkman.getString("goodsName"));
			}else{
				oldProductSku.setProductName(mapLinkman.getString("skuname"));
			}
			oldProductSku.setSkuCode(mapLinkman.getString("skucode"));
			oldProductSku.setSkuName(mapLinkman.getString("skuname"));
			//商品单位默认为：件
			oldProductSku.setUnitId("17070710044995394265");
			String categoryName = "";
			if (mapLinkman.containsKey("categoryName")){
				categoryName = mapLinkman.getString("categoryName").toString();
			}			
			if (categoryName.equals("虚拟商品")){
				oldProductSku.setProductType(3);
			}else if (categoryName.equals("辅料")){
				oldProductSku.setProductType(2);
			}else if (categoryName.equals("面料") || categoryName.equals("原材料")){
				oldProductSku.setProductType(1);
			}else{
				oldProductSku.setProductType(0);
			}
			//默认计划销售天数:1天
			oldProductSku.setPlanSalesDays(1);
			oldProductSku.setPrice(new BigDecimal(mapLinkman.getString("cost")));
			oldProductSku.setUpdateId(ShiroUtils.getUserId());
			oldProductSku.setUpdateName(ShiroUtils.getUserName());
			oldProductSku.setUpdateDate(new Date());
			num = buyProductSkuMapper.updateByPrimaryKey(oldProductSku);
		}
		return num;
		
	}

	public List<Map<String, Object>> getImportAllProduct(String compId) {
		return buyProductSkuMapper.getImportAllProduct(compId);
	}
	
	public BuyProductSku getByBarcode(Map<String, Object> map){
		return buyProductSkuMapper.getByBarcode(map);
	}
}
