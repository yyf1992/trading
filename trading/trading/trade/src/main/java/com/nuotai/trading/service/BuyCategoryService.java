package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.BuyCategoryMapper;
import com.nuotai.trading.model.BuyCategory;

@Service
public class BuyCategoryService implements BuyCategoryMapper{

	@Autowired
	private BuyCategoryMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuyCategory record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyCategory record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuyCategory selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyCategory record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BuyCategory record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<BuyCategory> getAllCategory(Map<String,Object> map) {
		return mapper.getAllCategory(map);
	}

}
