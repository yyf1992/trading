package com.nuotai.trading.model.oms;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 商品信息
 * 
 * @author Administrator
 * 
 */
public class ScmProductOldsku {
	// 主键
	private Long id;
	// 商品主表id
	private Long productId;
	// 商品名称
	private String goodsName;
	// 类目id
	private String categoryId;
	// 类目code
	private String categoryCode;
	// 类目名称
	private String categoryName;
	// 规格码
	private String skuCode;
	// 规格名称
	private String skuName;
	// 条形码
	private String barcode;
	// 成本
	private BigDecimal cost;
	// 库存数
	private Double stockNumber;
	// 最近一次成本价
	private Double lastCost;
	// 第一次成本价
	private Double firstCost;
	// 状态
	private String status;
	// 货号
	private String productCode;
	//
	private Double skuweight;

	private Long parentId;

	private Long warncount;

	private String material;
	// 颜色
	private String color;

	private String sticker;
	// 尺寸名称
	private String productname;

	private BigDecimal wholePrice;

	private String pcs;

	private BigDecimal slength;

	private BigDecimal swidth;

	private BigDecimal sheight;

	private BigDecimal svolume;
	
	//截止20160701时间
	private Integer remark;
		
	private Map<String,Long[]> stockMap;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public Double getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(Double stockNumber) {
		this.stockNumber = stockNumber;
	}

	public Double getLastCost() {
		return lastCost;
	}

	public void setLastCost(Double lastCost) {
		this.lastCost = lastCost;
	}

	public Double getFirstCost() {
		return firstCost;
	}

	public void setFirstCost(Double firstCost) {
		this.firstCost = firstCost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Double getSkuweight() {
		return skuweight;
	}

	public void setSkuweight(Double skuweight) {
		this.skuweight = skuweight;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getWarncount() {
		return warncount;
	}

	public void setWarncount(Long warncount) {
		this.warncount = warncount;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSticker() {
		return sticker;
	}

	public void setSticker(String sticker) {
		this.sticker = sticker;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public BigDecimal getWholePrice() {
		return wholePrice;
	}

	public void setWholePrice(BigDecimal wholePrice) {
		this.wholePrice = wholePrice;
	}

	public String getPcs() {
		return pcs;
	}

	public void setPcs(String pcs) {
		this.pcs = pcs;
	}

	public BigDecimal getSlength() {
		return slength;
	}

	public void setSlength(BigDecimal slength) {
		this.slength = slength;
	}

	public BigDecimal getSwidth() {
		return swidth;
	}

	public void setSwidth(BigDecimal swidth) {
		this.swidth = swidth;
	}

	public BigDecimal getSheight() {
		return sheight;
	}

	public void setSheight(BigDecimal sheight) {
		this.sheight = sheight;
	}

	public BigDecimal getSvolume() {
		return svolume;
	}

	public void setSvolume(BigDecimal svolume) {
		this.svolume = svolume;
	}

	public Map<String, Long[]> getStockMap() {
		return stockMap;
	}

	public void setStockMap(Map<String, Long[]> stockMap) {
		this.stockMap = stockMap;
	}

	public Integer getRemark() {
		return remark;
	}

	public void setRemark(Integer remark) {
		this.remark = remark;
	}
}