package com.nuotai.trading.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nuotai.trading.dao.SysBankMapper;
import com.nuotai.trading.model.SysBank;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
@Transactional
public class SysBankService {

    private static final Logger LOG = LoggerFactory.getLogger(SysBankService.class);

	@Autowired
	private SysBankMapper sysBankMapper;
	
	public SysBank get(String id){
		return sysBankMapper.get(id);
	}
	
	public List<SysBank> queryList(SearchPageUtil searchPageUtil){
		return sysBankMapper.queryList(searchPageUtil);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysBankMapper.queryCount(map);
	}
	
	public void add(SysBank sysBank){
		sysBankMapper.add(sysBank);
	}
	
	public void update(SysBank sysBank){
		sysBankMapper.update(sysBank);
	}
	
	public void delete(String id){
		sysBankMapper.delete(id);
	}
	
	public JSONObject validateBank(SysBank sysBank) {
		JSONObject json=new JSONObject();
		sysBank.setCompanyId(ShiroUtils.getCompId());
		if("".equals(sysBank.getAccountName())){
			json.put("success", false);
			json.put("msg", "账户名不能为空！");
			return json;
		}
		if("".equals(sysBank.getBankAccount())){
			json.put("success", false);
			json.put("msg", "账户不能为空！");
			return json;
		}
		if("".equals(sysBank.getOpenBank())){
			json.put("success", false);
			json.put("msg", "开户行不能为空！");
			return json;
		}
		
		SysBank bankAccount=sysBankMapper.get(sysBank);
		if(bankAccount != null){
			json.put("success", false);
			json.put("msg", "账号已存在！");
			return json;
		}
		
		Pattern pattern = Pattern.compile("[0-9]{1,}");
		Matcher matcher = pattern.matcher((CharSequence)sysBank.getBankAccount());
		boolean result=matcher.matches();
		if(!result){
			json.put("success", false);
			json.put("msg", "账号仅支持数字格式！");
			return json;
		}
		
		json.put("success", true);
		json.put("msg", "");
		return json;
	}
	

	public JSONObject saveBankInsert(SysBank sysBank) {
		// TODO Auto-generated method stub
		JSONObject json =validateBank(sysBank);
		if(!json.getBoolean("success")){
			return json;
		}
		sysBank.setId(ShiroUtils.getUid());
		sysBank.setCompanyId(ShiroUtils.getCompId());
		sysBank.setAccountName(sysBank.getAccountName());
		sysBank.setBankAccount(sysBank.getBankAccount());
		sysBank.setOpenBank(sysBank.getOpenBank());
		sysBank.setCreateDate(new Date());
		sysBank.setIsDel(0);
		sysBank.setCreateId(ShiroUtils.getUserId());
		sysBank.setCreateName(ShiroUtils.getUserName());
		sysBank.setUpdateDate(new Date());
		sysBank.setUpdateId(ShiroUtils.getUserId());
		sysBank.setUpdateName(ShiroUtils.getUserName());
		int result = 0;
		result=sysBankMapper.insert(sysBank);
		if(result>0){
			json.put("success", true);
			json.put("msg", "添加成功！");
		}else{
			json.put("success", false);
			json.put("msg", "添加失败！");	
		}
		return json;
	}

	public JSONObject saveBankEdit(SysBank sysBank) {
		// TODO Auto-generated method stub
		JSONObject json =validateBank(sysBank);
		if(!json.getBooleanValue("success")){
			return json;
		}
		sysBank.setUpdateDate(new Date());
		sysBank.setAccountName(sysBank.getAccountName());
		sysBank.setOpenBank(sysBank.getOpenBank());
		sysBank.setBankAccount(sysBank.getBankAccount());
		sysBank.setUpdateId(ShiroUtils.getUserId());
		sysBank.setUpdateName(ShiroUtils.getUserName());
		int result = 0;
		result=sysBankMapper.update(sysBank);
		if(result>0){
			json.put("success", true);
			json.put("msg", "修改成功！");
		}else{
			json.put("success", false);
			json.put("msg", "修改失败！");
		}
		return json;
	}

	public SysBank selectByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		return sysBankMapper.selectByPrimaryKey(id);
	}

	//查询银行账户数据下拉
	public List<SysBank> getBankAccountBuyMap(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("companyId",ShiroUtils.getCompId());
		return  sysBankMapper.getBankAccountBuyMap(map);
	}

}
