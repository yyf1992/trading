package com.nuotai.trading.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyProductSkuBomMapper;
import com.nuotai.trading.dao.BuyProductSkuComposeMapper;
import com.nuotai.trading.dao.BuyProductSkuMapper;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.BuyProductSkuCompose;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
@Transactional
public class BuyProductSkuComposeService {

	@Autowired
	private BuyProductSkuComposeMapper buyProductSkuComposeMapper;
	@Autowired
	private BuyProductSkuMapper buyProductSkuMapper;
	@Autowired
	private BuyProductSkuBomMapper buyProductSkuBomMapper;
	@Autowired
	private BuyUnitService buyUnitService;

	public BuyProductSkuCompose get(String id) {
		return buyProductSkuComposeMapper.get(id);
	}

	public List<BuyProductSkuCompose> queryList(Map<String, Object> map) {
		return buyProductSkuComposeMapper.queryList(map);
	}

	public int queryCount(Map<String, Object> map) {
		return buyProductSkuComposeMapper.queryCount(map);
	}

	public void add(BuyProductSkuCompose buyProductSkuCompose) {
		buyProductSkuComposeMapper.add(buyProductSkuCompose);
	}

	public void update(BuyProductSkuCompose buyProductSkuCompose) {
		buyProductSkuComposeMapper.update(buyProductSkuCompose);
	}

	public void delete(String id) {
		buyProductSkuComposeMapper.delete(id);
	}

	public JSONObject saveCompose(Map<String, Object> map){
		JSONObject json = new JSONObject();
	    List<BuyProductSkuCompose> composeStrList = JSONObject.parseArray(String.valueOf(map.get("composeStr")),BuyProductSkuCompose.class);
		String productId = map.containsKey("productId") ? map.get("productId").toString() : "";
		String remark = map.containsKey("remark") ? map.get("remark").toString() : "";
		if (remark.length() > 50) {
			json.put("msg", "备注不能超过50字，请重新输入！");
			return json;
			//throw new ServiceException("备注不能超过50字，请重新输入！");
		}
		String ischange = map.containsKey("ischange") ? map.get("ischange").toString() : "";
		
		if(!ObjectUtil.isEmpty(composeStrList)&&composeStrList.size()>0){
			// 修改物料配置
			if (ischange != null && ischange.length() > 0) {
				BuyProductSkuBom bom = new BuyProductSkuBom();
				bom.setCompanyId(ShiroUtils.getCompId());
				bom.setRemark(remark);
				bom.setUpdateDate(new Date());
				bom.setUpdateId(ShiroUtils.getUserId());
				bom.setUpdateName(ShiroUtils.getUserName());
				bom.setProductId(productId);
				buyProductSkuBomMapper.updateBom(bom);
				map.put("companyId", ShiroUtils.getCompId());
				buyProductSkuComposeMapper.deleteComposeByMap(map);
				for (BuyProductSkuCompose buCompose : composeStrList) {
					buCompose.setId(ShiroUtils.getUid());
					buCompose.setCompanyId(ShiroUtils.getCompId());
					buCompose.setProductId(productId);
					buCompose.setCreateDate(new Date());
					buCompose.setCreateName(ShiroUtils.getUserName());
					buCompose.setCreateId(ShiroUtils.getUserId());
					buyProductSkuComposeMapper.saveCompose(buCompose);
				}
			} else {
				map.put("companyId", ShiroUtils.getCompId());
				// 新增物料配置
				List<BuyProductSkuCompose> oldList = buyProductSkuBomMapper.selectByPrimaryKey(map);
				if (oldList.size() > 0) {
					// 有重复数据
					json.put("msg", "该商品已经有物料配置数据");
					return json;
					//throw new ServiceException("该商品已经有物料配置数据");
				} else {
					BuyProductSku sku = buyProductSkuMapper.selectByPrimaryKey(productId);
					BuyProductSkuBom bom = new BuyProductSkuBom();
					bom.setId(ShiroUtils.getUid());
					bom.setCompanyId(ShiroUtils.getCompId());
					bom.setBomCode(ShiroUtils.getUid());
					bom.setProductCode(sku.getProductCode());
					bom.setProductName(sku.getProductName());
					bom.setProductId(sku.getId());
					bom.setSkuCode(sku.getSkuCode());
					bom.setSkuName(sku.getSkuName());
					bom.setBarcode(sku.getBarcode());
					bom.setUnitId(sku.getUnitId());
					bom.setUnitName(buyUnitService.selectByPrimaryKey(sku.getUnitId()).getUnitName());
					bom.setStatus("0");
					bom.setCreateDate(new Date());
					bom.setCreateId(ShiroUtils.getUserId());
					bom.setCreateName(ShiroUtils.getUserName());
					bom.setRemark(remark);
					buyProductSkuBomMapper.insertSelective(bom);

					// 没有重复数据
					for (BuyProductSkuCompose buCompose : composeStrList) {
						buCompose.setId(ShiroUtils.getUid());
						buCompose.setCompanyId(ShiroUtils.getCompId());
						buCompose.setProductId(sku.getId());
						buCompose.setCreateDate(new Date());
						buCompose.setCreateId(ShiroUtils.getUserId());
						buCompose.setCreateName(ShiroUtils.getUserName());
						buyProductSkuComposeMapper.saveCompose(buCompose);
					}
				}
			}
		}

		return json;
	}

	public List<Map<String, Object>> selectByBomId(Map<String,Object> map) {
		return buyProductSkuComposeMapper.selectByBomId(map);
	}
	
	public List<Map<String, Object>> selectByMap(Map<String,Object> map) {
		return buyProductSkuComposeMapper.selectByMap(map);
	}

}
