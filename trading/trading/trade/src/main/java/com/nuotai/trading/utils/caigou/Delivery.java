package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.druid.util.StringUtils;

/**
 * 同步采购系统发货数据
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class Delivery {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Delivery delivery = new Delivery();
		delivery.dealWith();
	}
	
	public void dealWith(){
		try {
			Connection tradeConn = DBUtils.getTradeConnection();
			//获得数据
			List<Map<String,Object>> dataList = getPurchaseDeliveryData();
			int length = dataList.size();
			for(Map<String,Object> datalMap : dataList){
				System.out.println("剩余："+length+"条数据,总计："+dataList.size());
				insertData(datalMap,tradeConn);
				length--;
			}
			DBUtils.closeConnection(tradeConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 新增发货主表
	 * @param datalMap
	 * @param tradeConn
	 */
	private void insertData(Map<String, Object> datalMap,
			Connection conn) throws Exception{
		String deliverycode = datalMap.get("deliverycode").toString();
		String countSql = "select count(deliver_no) num from buy_delivery_record where deliver_no='"+deliverycode+"'";
		PreparedStatement ps = conn.prepareStatement(countSql);
		ResultSet rs = ps.executeQuery();
		int result = 0;
		while (rs.next()) {
			result = rs.getInt("num");
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		if(result == 0){
			StringBuffer insertHeaderSql = new StringBuffer("INSERT INTO buy_delivery_record (");
			insertHeaderSql.append("id,company_id,deliver_id,deliver_no,deliver_type,supplier_id,supplier_code,supplier_name,");
			insertHeaderSql.append("status,recordstatus,reconciliation_status,is_invoice_status,is_payment_status,arrival_date,");
			insertHeaderSql.append("remark,is_del,creater_id,creater_name,create_date,dropship_type)");
			insertHeaderSql.append(" values (");
			insertHeaderSql.append("'"+datalMap.get("id")+"',");//id
			insertHeaderSql.append("'17060909542281121440',");//company_id
			insertHeaderSql.append("'"+datalMap.get("id")+"',");//deliver_id
			insertHeaderSql.append("'"+datalMap.get("deliverycode")+"',");//deliver_no
			insertHeaderSql.append("'"+datalMap.get("type")+"',");//deliver_type
			insertHeaderSql.append("'"+datalMap.get("supplierid")+"',");//supplier_id
			insertHeaderSql.append("'"+datalMap.get("suppliercode")+"',");//supplier_code
			insertHeaderSql.append("'"+datalMap.get("suppliername")+"',");//supplier_name
			insertHeaderSql.append("'0',");//status
			//recordstatus收货单状态
			int status = Integer.parseInt(datalMap.get("status").toString());
			if(status == 0){
				insertHeaderSql.append("'0',");//recordstatus
			}else{
				insertHeaderSql.append("'1',");//recordstatus
			}
			insertHeaderSql.append("'0',");//reconciliation_status
			insertHeaderSql.append("'0',");//is_invoice_status
			insertHeaderSql.append("'0',");//is_payment_status
			String goodsreceiptdate = datalMap.get("goodsreceiptdate")==null?null:datalMap.get("goodsreceiptdate").toString();
			if(StringUtils.isEmpty(goodsreceiptdate)){
				insertHeaderSql.append("null,");//predict_arred
			}else{
				insertHeaderSql.append("'"+goodsreceiptdate+"',");//predict_arred
			}
			insertHeaderSql.append("'【原采购系统导入】',");//remark
			insertHeaderSql.append("'0',");//is_del
			insertHeaderSql.append("'"+datalMap.get("supplierid")+"',");//creater_id
			insertHeaderSql.append("'"+datalMap.get("suppliername")+"',");//creater_name
			insertHeaderSql.append("'"+datalMap.get("created")+"',");//create_date
			insertHeaderSql.append("'0'");//dropship_type
			insertHeaderSql.append(");");
			PreparedStatement insertPs = conn.prepareStatement(insertHeaderSql.toString());
			int result2 = insertPs.executeUpdate();
			// 关闭声明
			DBUtils.closeStatement(insertPs);
			if(result2 > 0){
				//新增子信息
				List<Map<String,Object>> childList = (List<Map<String,Object>>)datalMap.get("itemList");
				if(childList != null && !childList.isEmpty()){
					for(Map<String,Object> item : childList){
						insertItem(item,conn,datalMap);
					}
				}
			}
		}
	}

	
	private void insertItem(Map<String, Object> item, Connection conn,
			Map<String, Object> datalMap) throws Exception{
		StringBuffer insertItemSql = new StringBuffer("INSERT INTO buy_delivery_record_item (");
		insertItemSql.append("id,record_id,order_id,order_item_id,delivery_item_id,apply_code,product_code,product_name,");
		insertItemSql.append("sku_code,sku_name,barcode,unit_id,");
		insertItemSql.append("delivery_num,arrival_num,arrival_date,warehouse_holder,is_need_invoice,remark)");
		insertItemSql.append(" values (");
		insertItemSql.append("'"+item.get("id")+"',");//id
		insertItemSql.append("'"+item.get("deliveryid")+"',");//record_id
		insertItemSql.append("'"+item.get("relatedcode")+"',");//order_id
		insertItemSql.append("'"+item.get("purchase_item_id")+"',");//order_item_id
		insertItemSql.append("'"+item.get("id")+"',");//delivery_item_id
		insertItemSql.append("'"+item.get("apply_code")+"',");//apply_code
		insertItemSql.append("'"+item.get("procode")+"',");//pro_code
		insertItemSql.append("'"+item.get("proname")+"',");//pro_name
		insertItemSql.append("'"+item.get("skucode")+"',");//sku_code
		insertItemSql.append("'"+item.get("skuname")+"',");//sku_name
		insertItemSql.append("'"+item.get("skuoid")+"',");//sku_id
		insertItemSql.append("'17070710044995394265',");//unit_id
		insertItemSql.append(item.get("deliverycount")+",");//priceSum
		insertItemSql.append(item.get("alreadyarrival")+",");//priceSum
		String goodsreceiptdate = datalMap.get("goodsreceiptdate")==null?null:datalMap.get("goodsreceiptdate").toString();
		if(StringUtils.isEmpty(goodsreceiptdate)){
			insertItemSql.append("null,");//predict_arred
		}else{
			insertItemSql.append("'"+goodsreceiptdate+"',");//predict_arred
		}
		String goodsreceiptuser = datalMap.get("goodsreceiptuser")==null?"":datalMap.get("goodsreceiptuser").toString();
		insertItemSql.append("'"+goodsreceiptuser+"',");//ware_house_id
		insertItemSql.append("'N',");//is_need_invoice
		insertItemSql.append("'【原采购系统导入】'");//remark
		insertItemSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertItemSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 获得采购系统发货数据
	 * @return
	 */
	private List<Map<String, Object>> getPurchaseDeliveryData() throws Exception{
		System.out.println("正在获取采购系统发货数据。。。。。");
		Connection purchaseConn = DBUtils.getPurchaseConnection();
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT * FROM deliveryrecord_header limit 1";
		PreparedStatement ps = purchaseConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = ps.getMetaData();
		// 取得结果集列数
        int columnCount = rsmd.getColumnCount();
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>();
			for (int i = 1; i < columnCount; i++) {
				map.put(rsmd.getColumnLabel(i), rs.getObject(rsmd.getColumnLabel(i)));
            }
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		
		if(dataList != null && !dataList.isEmpty()){
			//获得子信息
			List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
			String selectItemSql = "SELECT d.*,a.id AS purchase_item_id,a.apply_code FROM deliveryrecord_item d "
				+ " LEFT JOIN ("
				+ " SELECT i.purchaseid,i.skuoid,a1.id,ah.apply_code FROM purchase_item i"
				+ " LEFT JOIN applypurchase_item a1 ON a1.id=i.applyItemId"
				+ " LEFT JOIN applypurchase_header ah ON ah.id=a1.apply_id"
				+ " )a ON (a.purchaseid=d.relatedcode AND a.skuoid=d.skuoid)";
			PreparedStatement ps2 = purchaseConn.prepareStatement(selectItemSql);
			ResultSet rs2 = ps2.executeQuery();
			// 取得结果集列数
			while (rs2.next()) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("id", rs2.getString("id"));
				map.put("deliveryid", rs2.getString("deliveryid"));
				map.put("relatedcode", rs2.getString("relatedcode"));
				map.put("procode", rs2.getString("procode"));
				map.put("proname", rs2.getString("proname"));
				map.put("skucode", rs2.getString("skucode"));
				map.put("skuname", rs2.getString("skuname"));
				map.put("skuoid", rs2.getString("skuoid"));
				map.put("purchasecount", rs2.getInt("purchasecount"));
				map.put("deliverycount", rs2.getInt("deliverycount"));
				map.put("alreadyarrival", rs2.getInt("alreadyarrival"));
				map.put("purchase_item_id", rs2.getObject("purchase_item_id"));
				map.put("apply_code", rs2.getObject("apply_code"));
				itemList.add(map);
			}
			// 关闭记录集
			DBUtils.closeResultSet(rs2);
			// 关闭声明
			DBUtils.closeStatement(ps2);
			Map<String,List<Map<String,Object>>> itemMap = new HashMap<>();
			if(itemList != null && !itemList.isEmpty()){
				System.out.println("正在处理item数据");
				int length = itemList.size();
				for(Map<String,Object> item : itemList){
					System.out.println("剩余item数："+length+"条");
					String applyId = item.get("deliveryid").toString();
					List<Map<String,Object>> childList = new ArrayList<>();
					if(itemMap.containsKey(applyId)){
						childList = itemMap.get(applyId);
					}
					childList.add(item);
					itemMap.put(applyId, childList);
					length--;
				}
			}
			for(Map<String,Object> data : dataList){
				String id = data.get("id").toString();
				List<Map<String,Object>> childList = itemMap.get(id);
				data.put("itemList", childList);
			}
		}
		// 关闭链接对象
		DBUtils.closeConnection(purchaseConn);
		System.out.println("获取采购系统数据结束");
		return dataList;
	}
}
