package com.nuotai.trading.controller.platform.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysPlatform;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysPlatformService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import net.sf.json.JSONArray;

/**
 * 店铺表
 * 
 * @author "
 * @date 2017-08-07 08:59:32
 */
@Controller
@RequestMapping("platform/sysshop")
public class SysShopController extends BaseController{
	@Autowired
	private SysShopService sysShopService;
	@Autowired
	private SysPlatformService sysPlatformService;

	/**
	 * 加载店铺列表
	 */
	@RequestMapping("/loadSysShopList")
	public String list(SearchPageUtil searchPageUtil,
			@RequestParam Map<String, Object> param) {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
			param.put("status", "-1");
		}
		param.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		// 查询列表数据
		List<Map<String, Object>> sysShopList = sysShopService.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(sysShopList);
        model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/system/shop/shopList";
	}

	/**
	 * 加载新增店铺界面
	 * @return
	 */
	@RequestMapping(value="/loadShopInsert",method=RequestMethod.GET)
	public String loadShopInsert(){
		List<SysPlatform>  sysPlatformList=sysPlatformService.selectByCompanyId(ShiroUtils.getCompId());
        model.addAttribute("sysPlatformList",sysPlatformList);
		return "platform/system/shop/addShop";
	}
	
	/**
	 * 加载平台
	 */
	@ResponseBody
	@RequestMapping("loadPlatform")
	public String loadPlatform(){
		List<SysPlatform>  sysPlatformList=sysPlatformService.selectByCompanyId(ShiroUtils.getCompId());
		return JSON.toJSONString(sysPlatformList);
	}
	
	/**
	 * 保存新增店铺
	 * @param sysShop
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="saveInsertShop",method=RequestMethod.POST)
	public JSONObject saveInsertShop(SysShop sysShop){
		JSONObject json = sysShopService.saveInsertShop(sysShop);
		return json;
	}
	
	/**
	 * 加载修改店铺界面
	 */
	@RequestMapping(value="/loadEditShop",method=RequestMethod.POST)
	public String loadEditShop(String id){
		SysShop sysShop=sysShopService.selectByPrimaryKey(id);
		model.addAttribute("SysShop",sysShop);
		return "platform/system/shop/editShop";
	}
	
	/**
	 * 保存修改界面
	 */
	@ResponseBody
	@RequestMapping(value="/saveEditShop",method=RequestMethod.POST)
	public JSONObject saveEditShop(SysShop sysShop){
		JSONObject json=sysShopService.saveEditShop(sysShop);
		return json;
	}
	
	/**
	 * 加载人员设置关联店铺页面
	 * @param id
	 * @return
	 */
	@RequestMapping("instalShopUser")
	public String instalShopUser(String userId){
		model.addAttribute("instalShopUserId", userId);
		return "platform/system/shop/instalShopUser";
	}
	
	/**
	 * 获得左侧没有的店铺数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getLeftNotShop")
	@ResponseBody
	public String getLeftNotShop(String userId,String shopName){
		List<SysShop> shopList = sysShopService.getLeftNotRole(userId,shopName);
		return JSONArray.fromObject(shopList).toString();
	}
	
	/**
	 * 获得右侧有的店铺数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getRightShop")
	@ResponseBody
	public String getRightShop(String userId,String shopName){
		List<SysShop> shopList = sysShopService.getRightRole(userId,shopName);
		return JSONArray.fromObject(shopList).toString();
	}
	
	/**
	 * 人员添加店铺
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("addUserShop")
	@ResponseBody
	public String addUserShop(String userId,String shopCodeStr){
		JSONObject json = sysShopService.addUserRole(userId,shopCodeStr);
		return json.toJSONString();
	}

	/**
	 * 人员删除店铺
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("deleteUserShop")
	@ResponseBody
	public String deleteUserShop(String userId,String shopCodeStr){
		JSONObject json = sysShopService.deleteUserShop(userId,shopCodeStr);
		return json.toJSONString();
	}
}
