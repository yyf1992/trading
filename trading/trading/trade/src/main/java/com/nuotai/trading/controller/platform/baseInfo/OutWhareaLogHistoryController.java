package com.nuotai.trading.controller.platform.baseInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.model.OutWhareaLogHistory;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.OutWhareaLogHistoryService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


/**
 * 
 * 外仓数据历史记录
 * @author wl
 * @date 2018-02-09 10:07:32
 */
@Controller
@RequestMapping("platform/baseInfo/outwharealog")
public class OutWhareaLogHistoryController extends BaseController{
	@Autowired
	private OutWhareaLogHistoryService outWhareaLogHistoryService;
	@Autowired
	private SysShopService shopService;	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	@RequestMapping("/loadHistoryOutwharealogList")
	public String loadOutwharealogList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(!map.containsKey("minDate")){
			map.put("minDate", yesterday);
		}
		if(!map.containsKey("maxDate")){
			map.put("maxDate", yesterday);
		}
		//获取部门的名称
		List<SysShop> shopList = shopService.selectByMap(map);
		model.addAttribute("shopList", shopList);
		//获取创建人
		List<SysUser> companyUserList = sysUserMapper.getCompanyUser(map);
		model.addAttribute("companyUserList", companyUserList);
		searchPageUtil.setObject(map);
		//查询列表数据
		List<OutWhareaLogHistory> outWhareaLogList = outWhareaLogHistoryService.queryAllList(searchPageUtil);
		searchPageUtil.getPage().setList(outWhareaLogList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/baseInfo/outwharealog/outwharealogListHistory";
	}


}
