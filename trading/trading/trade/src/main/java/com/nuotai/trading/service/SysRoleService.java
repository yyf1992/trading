package com.nuotai.trading.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.nuotai.trading.dao.SysRoleMapper;
import com.nuotai.trading.model.SysRole;
import com.nuotai.trading.model.SysRoleMenu;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class SysRoleService implements SysRoleMapper {
	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Autowired
	private SysRoleUserService sysRoleUserService;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		return sysRoleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysRole record) {
		return sysRoleMapper.insert(record);
	}

	@Override
	public int insertSelective(SysRole record) {
		return sysRoleMapper.insertSelective(record);
	}

	@Override
	public SysRole selectByPrimaryKey(String id) {
		return sysRoleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysRole record) {
		return sysRoleMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysRole record) {
		return sysRoleMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil) {
		return sysRoleMapper.selectByPage(searchPageUtil);
	}

	@Override
	public List<SysRole> selectByMap(Map<String, Object> codemap) {
		return sysRoleMapper.selectByMap(codemap);
	}

	@Override
	public List<SysRole> selectByRoleCode(String role_code) {
		return sysRoleMapper.selectByRoleCode(role_code);
	}

	@Override
	public int saveDelete(String[] idList) {
		sysRoleUserService.deleteByRoleId(idList);
		sysRoleMenuService.deleteByRoleId(idList);
		return sysRoleMapper.saveDelete(idList);
	}

	@Override
	public List<SysRole> selectLeftRoleByPageUser(SearchPageUtil searchPageUtil) {
		return sysRoleMapper.selectLeftRoleByPageUser(searchPageUtil);
	}

	public void saveRoleMenu(String menuIds, String roleId) {
		String[] rolrIdList = {roleId};
		sysRoleMenuService.deleteByRoleId(rolrIdList);
		if(!StringUtils.isEmpty(menuIds)){
			String[] menuIdList = menuIds.split(",");
			for(String menuId : menuIdList){
				SysRoleMenu roleMenu = new SysRoleMenu();
				roleMenu.setId(ShiroUtils.getUid());
				roleMenu.setRoleId(roleId);
				roleMenu.setMenuId(menuId);
				roleMenu.setCreateTime(new Date());
				sysRoleMenuService.insertSelective(roleMenu);
			}
		}
	}

}
