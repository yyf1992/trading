package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
@Data
public class SysUser implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 主键id
    private String id;
    // 公司id
    private String companyId;
    // 用户姓名
    private String userName;
    // 密码
    private String password;
    // 手机号码/登录账号
    private String loginName;
    // 父类Id
    private String parentId;
    // 是否是父类 0不是父类，1是父类
    private Integer isParent;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
	//钉钉id
	private String dingdingId;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 创建时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人名称
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人名称
    private String delName;
    // 删除时间
    private Date delDate;
    // 公司名称
    private String companyName;

}