package com.nuotai.trading.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.SysMenuMapper;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.SysVerifyPocessMapper;
import com.nuotai.trading.dao.TradeRoleMapper;
import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.SysVerifyPocess;
import com.nuotai.trading.model.TradeVerifyCopy;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.PingYinUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

@Service
@Transactional
public class SysApprovalService {
	@Autowired
	private SysMenuMapper sysMenuMapper;
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyPocessMapper sysVerifyPocessMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	@Autowired
	private TradeRoleMapper tradeRoleMapper;

	public SysMenu getMenuByMenuName(String menuName) {
		return sysMenuMapper.getMenuByMenuName(menuName);
	}

	public SysVerifyHeader getApprovalByMenuId(String menuId) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", Constant.COMPANY_ID);
		params.put("menuId", menuId);
		return sysVerifyHeaderMapper.getApprovalByMenuId(params);
	}

	/**
	 * 增加固定审批流程数据
	 * @param verifyheader
	 * @param json
	 */
	public void addFixed(SysVerifyHeader verifyHeader, JSONObject json) throws ServiceException{
		String verifySuccess = json.containsKey("verifySuccess")?json.getString("verifySuccess"):"";
		/*if(StringUtils.isEmpty(verifySuccess)){
			throw new ServiceException("verifySuccess不能为空,请联系开发人员处理");
		}*/
		String verifyError = json.containsKey("verifyError")?json.getString("verifyError"):"";
		/*if(StringUtils.isEmpty(verifyError)){
			throw new ServiceException("verifyError不能为空,请联系开发人员处理");
		}*/
		String relatedUrl = json.containsKey("relatedUrl")?json.getString("relatedUrl"):"";
		if(StringUtils.isEmpty(relatedUrl)){
			throw new ServiceException("relatedUrl不能为空,请联系开发人员处理");
		}
		List<Object> idList = JSONArray.parseArray(json.containsKey("idList")?json.getString("idList"):"[]");
		if(idList.isEmpty()){
			throw new ServiceException("关联数据主键没有返回,请联系开发人员处理");
		}
		for(Object id : idList){
			//主表
			TradeVerifyHeader header = new TradeVerifyHeader();
			header.setId(ShiroUtils.getUid());
			header.setTitle(verifyHeader.getTitle());
			header.setRelatedId(id.toString());
			header.setVerifySuccess(verifySuccess);
			header.setVerifyError(verifyError);
			header.setRelatedUrl(relatedUrl);
			header.setSiteId(verifyHeader.getId());
			header.setCompanyId(ShiroUtils.getCompId());
			header.setStatus("0");
			header.setCreateId(ShiroUtils.getUserId());
			header.setCreateName(ShiroUtils.getUserName());
			header.setCreateDate(new Date());
			tradeVerifyHeaderMapper.add(header);
			//审批流程
			List<SysVerifyPocess> verifyPocessList = sysVerifyPocessMapper.getListByHeaderId(verifyHeader.getId());
			if(!verifyPocessList.isEmpty()){
				for(int i = 0; i < verifyPocessList.size(); i++){
					SysVerifyPocess verifyPocess = verifyPocessList.get(i);
					TradeVerifyPocess pocess = new TradeVerifyPocess();
					pocess.setId(ShiroUtils.getUid());
					pocess.setHeaderId(header.getId());
					pocess.setUserId(verifyPocess.getUserId());
					pocess.setUserName(verifyPocess.getUserName());
					pocess.setStatus("0");
					pocess.setRemark(null);
					pocess.setVerifyIndex(verifyPocess.getVerifyIndex());
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.add(pocess);
					if(i == 0){
						//待审批人员发送钉钉消息
						DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
					}
				}
			}
			//抄送人
			//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
			if("1".equals(verifyHeader.getCopyType())||"2".equals(verifyHeader.getCopyType())){
				List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
				if(!verifyCopyList.isEmpty()){
					for(SysVerifyCopy verifyCopy : verifyCopyList){
						TradeVerifyCopy copy = new TradeVerifyCopy();
						copy.setId(ShiroUtils.getUid());
						copy.setHeaderId(header.getId());
						copy.setUserId(verifyCopy.getUserId());
						copy.setUserName(verifyCopy.getUserName());
						tradeVerifyCopyMapper.add(copy);
						//发送钉钉消息
						DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
					}
				}
			}
		}
	}

	/**
	 * 自动审批人员
	 * @param verifyheader
	 * @param json
	 * @param approvalUsersStr
	 */
	public void addAutomatic(SysVerifyHeader verifyHeader, JSONObject json,
			String approvalUsersStr) throws ServiceException{
		String verifySuccess = json.containsKey("verifySuccess")?json.getString("verifySuccess"):"";
		String verifyError = json.containsKey("verifyError")?json.getString("verifyError"):"";
		String relatedUrl = json.containsKey("relatedUrl")?json.getString("relatedUrl"):"";
		List<Object> idList = JSONArray.parseArray(json.containsKey("idList")?json.getString("idList"):"[]");
		if(idList.isEmpty()){
			throw new ServiceException("关联数据主键没有返回,请联系开发人员处理");
		}
		for(Object id : idList){
			//主表
			TradeVerifyHeader header = new TradeVerifyHeader();
			header.setId(ShiroUtils.getUid());
			header.setTitle(verifyHeader.getTitle());
			header.setRelatedId(id.toString());
			header.setVerifySuccess(verifySuccess);
			header.setVerifyError(verifyError);
			header.setRelatedUrl(relatedUrl);
			header.setSiteId(verifyHeader.getId());
			header.setCompanyId(ShiroUtils.getCompId());
			header.setStatus("0");
			header.setCreateId(ShiroUtils.getUserId());
			header.setCreateName(ShiroUtils.getUserName());
			header.setCreateDate(new Date());
			tradeVerifyHeaderMapper.add(header);
			//审批流程
			String[] userArray = approvalUsersStr.split("@");
			if(userArray != null && userArray.length > 0){
				for(int i = 0; i < userArray.length; i++){
					String userStr = userArray[i];
					TradeVerifyPocess pocess = new TradeVerifyPocess();
					pocess.setId(ShiroUtils.getUid());
					pocess.setHeaderId(header.getId());
					pocess.setUserId(userStr.split(",")[0]);
					pocess.setUserName(userStr.split(",")[1]);
					pocess.setStatus("0");
					pocess.setRemark(null);
					pocess.setVerifyIndex(i+1);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.add(pocess);
					if(i == 0){
						//待审批人员发送钉钉消息
						DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
					}
				}
			}
			//抄送人
			if("1".equals(verifyHeader.getCopyType())||"2".equals(verifyHeader.getCopyType())){
				List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
				if(!verifyCopyList.isEmpty()){
					for(SysVerifyCopy verifyCopy : verifyCopyList){
						TradeVerifyCopy copy = new TradeVerifyCopy();
						copy.setId(ShiroUtils.getUid());
						copy.setHeaderId(header.getId());
						copy.setUserId(verifyCopy.getUserId());
						copy.setUserName(verifyCopy.getUserName());
						tradeVerifyCopyMapper.add(copy);
						//发送钉钉消息
						DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
					}
				}
			}
		}
	}

	
	public void monitorUpdate(String id) throws ServiceException{
		//取得审批主信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("relatedId", id);
		List<TradeVerifyHeader> headerList = tradeVerifyHeaderMapper.queryList(params);
		if(!headerList.isEmpty()){
			TradeVerifyHeader header = headerList.get(0);
			//状态：0-审批中；1-通过；2-拒绝；3-撤销
			if("1".equals(header.getStatus())){
				//已审批过
				throw new ServiceException("已审批的数据不允许修改");
			}else if("2".equals(header.getStatus())){
				//已拒绝的数据
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(int i = 0; i < pocessList.size(); i++){
						TradeVerifyPocess pocess = pocessList.get(i);
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark(null);
						tradeVerifyPocessMapper.update(pocess);
						if(i == 0){
							//待审批人员发送钉钉消息
							DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
						}
					}
				}
			}else if("3".equals(header.getStatus())){
				//已撤销的数据
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark("");
						tradeVerifyPocessMapper.update(pocess);
					}
				}
			}else if("0".equals(header.getStatus())){
				//未审批的数据，获得审批流程
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						if("1".equals(pocess.getStatus()) || "2".equals(pocess.getStatus())){
							throw new ServiceException("已审批的数据不允许修改");
						}
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark("");
						tradeVerifyPocessMapper.update(pocess);
					}
				}
			}
		}
	}
	
	public void monitorUpdatePurchase(String id) throws ServiceException{
		//取得审批主信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("relatedId", id);
		List<TradeVerifyHeader> headerList = tradeVerifyHeaderMapper.queryList(params);
		if(!headerList.isEmpty()){
			TradeVerifyHeader header = headerList.get(0);
			//状态：0-审批中；1-通过；2-拒绝；3-撤销
			if("1".equals(header.getStatus())){
				//已审批过
				//没有设置审批流程未下单可修改
				params.put("companyId", ShiroUtils.getCompId());
				params.put("menuId", "17070718133683994602");
				SysVerifyHeader verifyheader = sysVerifyHeaderMapper.getApprovalByMenuId(params);//销售计划
	        	if(verifyheader == null || verifyheader.getId() == null){//没有设置审批流程
	        		header.setStatus("1");
					tradeVerifyHeaderMapper.update(header);
					List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
					if(!pocessList.isEmpty()){
						for(TradeVerifyPocess pocess : pocessList){
							pocess.setStatus("1");
							pocess.setEndDate(null);
							tradeVerifyPocessMapper.update(pocess);
						}
					}
	        	}else{
	        		throw new ServiceException("已审批的数据不允许修改");
	        	}
			}else if("2".equals(header.getStatus())){
				//已拒绝的数据
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(int i = 0; i < pocessList.size(); i++){
						TradeVerifyPocess pocess = pocessList.get(i);
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark(null);
						tradeVerifyPocessMapper.update(pocess);
						if(i == 0){
							//待审批人员发送钉钉消息
							DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
						}
					}
				}
			}else if("3".equals(header.getStatus())){
				//已撤销的数据
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark("");
						tradeVerifyPocessMapper.update(pocess);
					}
				}
			}else if("0".equals(header.getStatus())){
				//未审批的数据，获得审批流程
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						if("1".equals(pocess.getStatus()) || "2".equals(pocess.getStatus())){
							throw new ServiceException("已审批的数据不允许修改");
						}
						pocess.setStatus("0");
						pocess.setEndDate(null);
						pocess.setRemark("");
						tradeVerifyPocessMapper.update(pocess);
					}
				}
			}
		}
	}

	public TradeVerifyHeader monitorDelete(String id) throws ServiceException{
		//取得审批主信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("relatedId", id);
		List<TradeVerifyHeader> headerList = tradeVerifyHeaderMapper.queryList(params);
		if(!headerList.isEmpty()){
			TradeVerifyHeader header = headerList.get(0);
			//状态：0-审批中；1-通过；2-拒绝；3-撤销
			if("1".equals(header.getStatus()) || "2".equals(header.getStatus())){
				//已审批过
				throw new ServiceException("已审批的数据不允许修改");
			}
			if(!header.getCreateId().equals(ShiroUtils.getUserId())){
				throw new ServiceException("不是自己创建的数据无权删除");
			}
			return header;
		}
		return null;
	}

	/**
	 * 撤销
	 * @param header
	 * @throws ServiceException
	 */
	public void repealVerify(TradeVerifyHeader header) throws ServiceException{
		if(header != null && !"".equals(header.getId())){
			//审批流程结束
			header.setStatus("3");
			header.setUpdateDate(new Date());
			tradeVerifyHeaderMapper.update(header);
			//删除所有未审批的流程
			tradeVerifyPocessMapper.deleteNotVerify(header.getId());
			//添加撤销流程
			TradeVerifyPocess pocess = new TradeVerifyPocess();
			pocess.setId(ShiroUtils.getUid());
			pocess.setHeaderId(header.getId());
			pocess.setUserId(ShiroUtils.getUserId());
			pocess.setUserName(ShiroUtils.getUserName());
			pocess.setStatus("4");
			int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
			pocess.setVerifyIndex(maxIndex+1);
			pocess.setStartDate(new Date());
			tradeVerifyPocessMapper.add(pocess);
			//通知发起人
			DingDingUtils.verifyDingDingOwn(header.getId());
		}
	}

	public SysMenu getMenuByMenuNameOrId(String menuName) {
		return sysMenuMapper.getMenuByMenuNameOrId(menuName);
	}

	/**
	 * 转交人员
	 * @param model
	 */
	public void setReferralUser(Model model) {
		Map<String,List<SysUser>> resultMap = new TreeMap<String,List<SysUser>>();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("notLoginUserId", ShiroUtils.getUserId());
		List<SysUser> userList = sysUserMapper.getCompanyUser(params);
		if(!userList.isEmpty()){
			for(SysUser user : userList){
				String firstChar = PingYinUtil.getFirstCapital(user.getUserName());
				List<SysUser> list = null;
				if(resultMap.containsKey(firstChar)){
					list = resultMap.get(firstChar);
				}else{
					list = new ArrayList<SysUser>();
				}
				list.add(user);
				resultMap.put(firstChar, list);
			}
		}
		model.addAttribute("userList", resultMap);
	}

	/**
	 * 判断是否需要审批
	 * @param menuId
	 * @return true-需要审批，false-不需要审批
	 */
	public boolean judgmentUserVerify(String menuId) {
		boolean result = true;
		//判断人员是否是管理员
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("userId", ShiroUtils.getUserId());
		params.put("companyId", ShiroUtils.getCompId());
		int isAdmin = tradeRoleMapper.isAdminRoleByUserId(params);
		if(isAdmin > 0){
			//是管理员，不需要审批
			result = false;
			return result;
		}
		//取得菜单权限
		params.put("menuId", menuId);
		int isVerify = tradeRoleMapper.isVerifyByMap(params);
		if(isVerify > 0){
			//不需要审批
			result = false;
			return result;
		}
		return result;
	}

	/**
	 * 没有设置审批流程，默认审批通过
	 * @param json
	 * @param menuName 
	 * @throws ServiceException
	 */
	public void addAutomaticNot(JSONObject json, String menuName) throws ServiceException{
		String verifySuccess = json.containsKey("verifySuccess")?json.getString("verifySuccess"):"";
		String verifyError = json.containsKey("verifyError")?json.getString("verifyError"):"";
		String relatedUrl = json.containsKey("relatedUrl")?json.getString("relatedUrl"):"";
		List<Object> idList = JSONArray.parseArray(json.containsKey("idList")?json.getString("idList"):"[]");
		if(idList.isEmpty()){
			throw new ServiceException("关联数据主键没有返回,请联系开发人员处理");
		}
		for(Object id : idList){
			//主表
			TradeVerifyHeader header = new TradeVerifyHeader();
			header.setId(ShiroUtils.getUid());
			header.setTitle(menuName);
			header.setRelatedId(id.toString());
			header.setVerifySuccess(verifySuccess);
			header.setVerifyError(verifyError);
			header.setRelatedUrl(relatedUrl);
			header.setSiteId(null);
			header.setCompanyId(ShiroUtils.getCompId());
			header.setStatus("1");
			header.setCreateId(ShiroUtils.getUserId());
			header.setCreateName(ShiroUtils.getUserName());
			header.setCreateDate(new Date());
			tradeVerifyHeaderMapper.add(header);
			//审批流程
			TradeVerifyPocess pocess = new TradeVerifyPocess();
			pocess.setId(ShiroUtils.getUid());
			pocess.setHeaderId(header.getId());
			pocess.setUserId(ShiroUtils.getUserId());
			pocess.setUserName("买卖系统");
			pocess.setStatus("1");
			pocess.setRemark("没有设置审批流程，系统默认审批通过");
			pocess.setVerifyIndex(1);
			pocess.setStartDate(new Date());
			tradeVerifyPocessMapper.add(pocess);
			//发送钉钉消息
			DingDingUtils.verifyDingDingCopy(ShiroUtils.getUserId(), header.getId());
		}
	}
}
