package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 用户与店铺关联表
 * 
 * @author "
 * @date 2018-03-21 15:29:56
 */
@Data
public class SysShopUser implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//用户主键
	private String userId;
	//店铺ID
	private String shopId;
	//店铺代码
	private String shopCode;
	//创建时间
	private Date createTime;
	//
	private String companyId;
}
