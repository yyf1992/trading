package com.nuotai.trading.controller.platform.interwork;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.BuySupplierFriendInvite;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.service.InviteFriendService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


/**
 * 好友互通申请信息表
 * 
 * @author "
 * @date 2017-08-02 13:45:01
 */
@Controller
@RequestMapping("interwork/applyFriend")
public class InviteFriendController {
	@Autowired
	private InviteFriendService applyFriendService;

	/**
	 * 获取好友请求列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/queryInviteList")
	@ResponseBody
	public String queryInviteList(@RequestParam Map<String,Object> params){
		params.put("inviteeId", ShiroUtils.getCompId());
		List<BuySupplierFriendInvite> list = applyFriendService.queryList(params);
		return JSONObject.toJSONString(list);
	}

	/**
	 * 保存好友请求
	 * @param buySupplierFriendInvite
	 * @return
	 */
	@RequestMapping(value = "/saveInviteFriend", method = RequestMethod.POST)
	@ResponseBody
	public String saveInviteFriend(BuySupplierFriendInvite buySupplierFriendInvite){
		JSONObject json = new JSONObject();
		try {
			json= applyFriendService.add(buySupplierFriendInvite);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "发送好友申请错误！");
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 好友申请状态设置
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/setInviteFriendStatus", method = RequestMethod.POST)
	@ResponseBody
	public String setInviteFriendStatus(String id,String status){
		JSONObject json = new JSONObject();
		try {
			applyFriendService.setInviteFriendStatus(id,status);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
}
