package com.nuotai.trading.model.wms;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 入库计划主表
 * @author Administrator
 *
 */
@Data
public class WmsInputPlan {
    //主键
    private Long id;
    //标题
    private String title;
    //单号
    private String planno;
    //来源单号
    private String sourceno;
    //库位
    private String whcode;
    //类型
    private String type;
    //状态״̬
    private String status;
    //创建人
    private String creater;
    //创建日期
    private Date created;
    //入库人
    private String opter;
    //入库日期
    private Date optdate;
    //
    private String memo;
    //
    private String mark;
    //
    private String supplier;
    private String ownercode;
    private String createtype;
    
    private List<WmsInputPlanDetail> itemList;
}