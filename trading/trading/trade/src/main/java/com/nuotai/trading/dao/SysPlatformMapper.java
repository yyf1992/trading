package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.model.SysPlatform;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 平台表
 * 
 * @author zyn
 * @date 2017-08-03 13:04:48
 */
public interface SysPlatformMapper extends BaseDao<SysPlatform> {
	
	List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil);
	
	int insert(SysPlatform sysPlatform);
	
	int updateByPrimaryKey(SysPlatform sysPlatform);
	
	SysPlatform selectByPrimaryKey(String id);

	SysPlatform selectByPlatformCode(SysPlatform sysPlatform);

	SysPlatform selectByPlatformName(SysPlatform sysPlatform);

	List<SysPlatform> selectByCompanyId(String compId);
	
}
