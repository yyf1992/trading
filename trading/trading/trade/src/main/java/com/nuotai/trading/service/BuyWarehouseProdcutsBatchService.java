package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseProdcutsBatchMapper;
import com.nuotai.trading.model.BuyWarehouseProdcutsBatch;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyWarehouseProdcutsBatchService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseProdcutsBatchService.class);

	@Autowired
	private BuyWarehouseProdcutsBatchMapper buyWarehouseProdcutsBatchMapper;
	
	public BuyWarehouseProdcutsBatch get(String id){
		return buyWarehouseProdcutsBatchMapper.get(id);
	}
	
	public List<BuyWarehouseProdcutsBatch> queryList(Map<String, Object> map){
		return buyWarehouseProdcutsBatchMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseProdcutsBatchMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseProdcutsBatch buyWarehouseProdcutsBatch){
		buyWarehouseProdcutsBatchMapper.add(buyWarehouseProdcutsBatch);
	}
	
	public void update(BuyWarehouseProdcutsBatch buyWarehouseProdcutsBatch){
		buyWarehouseProdcutsBatchMapper.update(buyWarehouseProdcutsBatch);
	}
	
	public void delete(String id){
		buyWarehouseProdcutsBatchMapper.delete(id);
	}
	
	/**
	 * 分页查询商品库存余额
	 * @param searchPageUtil
	 * @return
	 */
	public List<Map<String, Object>> selectProdcutsByPage(SearchPageUtil searchPageUtil) {

		return buyWarehouseProdcutsBatchMapper.selectProdcutsByPage(searchPageUtil);
	}
}
