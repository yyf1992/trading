package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-09-12 11:30:51
 */
public interface BuyWarehouseProdcutsMapper extends BaseDao<BuyWarehouseProdcuts> {
	
	List<Map<String,Object>> selectProdcutsByPage(SearchPageUtil searchPageUtil);
	
	BuyWarehouseProdcuts selectProdcutByid(String id);
	
	List<Map<String,Object>> selectProdcutsDetailsByPage(SearchPageUtil searchPageUtil);
	
	List<BuyWarehouseProdcuts> selectProdcuts();
	
	List<BuyWarehouseProdcuts> selectAllProdcuts();
}
