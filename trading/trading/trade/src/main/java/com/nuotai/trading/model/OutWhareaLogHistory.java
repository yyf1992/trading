package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-02-09 10:07:32
 */
@Data
public class OutWhareaLogHistory implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//外仓导入历史ID
	private String historyId;
	//外仓数据同步时间
	private Date historyDate;
	//公司Id
	private String companyId;
	//子公司名称
	private String subcompanyName;
	//仓库名称
	private String whareaName;
	//店铺
	private String shopName;
	//货号
	private String productCode;
	//条形码
	private String barcode;
	//成本价
	private BigDecimal price;
	//外仓在仓数量
	private Integer outWhareaStock;
	//外仓在途数量
	private Integer outWhareaWayStock;
	//外仓库存金额
	private BigDecimal outWhareaTotalPrice;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//商品名称
	private String productName;
	//单位名称
	private String unitName;
	//商品类型 0成品1原材料2辅料3虚拟产品
	private Integer productType;
	//标准库存
	private Integer standardStock;
	//库存下限
	private Integer minStock;
	//计划销售天数
	private Integer planSalesDays;
	//颜色
	private String colorCode;
	//重量
	private BigDecimal weight;
	//0启用 1停用
	private String status;
	//单位Id
	private String unitId;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//删除时间
	private Date updateDate;
	//删除人
	private String delId;
	//删除人名称
	private String delName;
	//删除日期
	private Date delDate;
}
