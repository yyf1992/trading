package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.InviteFriendMapper;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.model.BuySupplierFriendInvite;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
@Service
public class SysUserService implements SysUserMapper {
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private InviteFriendMapper inviteFriendMapper;
	@Override
	public int deleteByPrimaryKey(String id) {
		return sysUserMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysUser record) {
		return sysUserMapper.insert(record);
	}

	@Override
	public int insertSelective(SysUser record) {
		return sysUserMapper.insertSelective(record);
	}

	@Override
	public SysUser selectByPrimaryKey(String id) {
		return sysUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysUser record) {
		return sysUserMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysUser record) {
		return sysUserMapper.updateByPrimaryKey(record);
	}

	@Override
	public SysUser queryByLoginName(String loginName) {
		return sysUserMapper.queryByLoginName(loginName);
	}

	@Override
	public List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil) {
		return sysUserMapper.selectByPage(searchPageUtil);
	}

	public void deleteUser(String ids) {
		SysUser loginUser = (SysUser)ShiroUtils.getSession().getAttribute(Constant.CURRENT_USER);
		String[] idList = ids.split(",");
		for(String id : idList){
			SysUser user = new SysUser();
			user.setId(id);
			user.setIsDel(1);
			user.setUpdateId(loginUser.getId());
			user.setUpdateName(loginUser.getUserName());
			user.setUpdateDate(new Date());
			user.setDelId(loginUser.getId());
			user.setDelName(loginUser.getUserName());
			user.setDelDate(new Date());
			sysUserMapper.updateByPrimaryKeySelective(user);
		}
	}
	
	public JSONObject validateSys(SysUser sysUser){
		JSONObject json=new JSONObject();
		sysUser.setCompanyId(ShiroUtils.getCompId());
		if(sysUser.getLoginName()==null||"".equals(sysUser.getLoginName())){
			json.put("success", false);
			json.put("msg", "登录账号不能为空！");
			return json;
		}
		/*if(!PhoneFormatCheckUtils.isPhoneLegal(sysUser.getLoginName())){
			json.put("success", false);
			json.put("msg", "请输入正确的手机号！");
			return json;
		}
		if(sysUser.getUserName()==null||"".equals(sysUser.getUserName())){
			json.put("success",false);
			json.put("msg", "联系人不能为空！");
			return json;
		}*/
		SysUser existUser=sysUserMapper.queryExistUser(sysUser);
		if(existUser!=null){
			json.put("success",false);
			json.put("msg", "用户已存在！");
			return json;
		}
		json.put("success",true);
		json.put("msg", "");
		return json;
	}
	
	/**
	 * 校验同步供应商，创建登录信息
	 * @param sysUser
	 * @return
	 */
	public JSONObject validateSupplierSys(SysUser sysUser){
		JSONObject json=new JSONObject();
		if(sysUser.getLoginName()==null||"".equals(sysUser.getLoginName())){
			json.put("success", false);
			json.put("msg", "登录账号不能为空！");
			return json;
		}
		/*if(!PhoneFormatCheckUtils.isPhoneLegal(sysUser.getLoginName())){
			json.put("success", false);
			json.put("msg", "请输入正确的手机号！");
			return json;
		}
		if(sysUser.getUserName()==null||"".equals(sysUser.getUserName())){
			json.put("success",false);
			json.put("msg", "联系人不能为空！");
			return json;
		}*/
		SysUser existUser=sysUserMapper.queryExistUser(sysUser);
		if(existUser!=null){
			json.put("success",false);
			json.put("msg", "用户已存在！");
			return json;
		}
		json.put("success",true);
		json.put("msg", "");
		return json;
	}
	
	public JSONObject saveAdd(SysUser sysUser){
		JSONObject json=new JSONObject();
		json=validateSys(sysUser);
		if(!json.getBooleanValue("success")){
			return json;
		}
		int result=0;
		Date date=new Date();
		sysUser.setPassword(ShiroUtils.getMD5("123456", "UTF-8", 0).toLowerCase());
		sysUser.setCompanyId(ShiroUtils.getCompId());
		sysUser.setCreateDate(date);
		sysUser.setCreateId(ShiroUtils.getUserId());
		sysUser.setCreateName(ShiroUtils.getUserName());
		sysUser.setId(ShiroUtils.getUid());
		sysUser.setUpdateDate(new Date());
		sysUser.setUpdateId(ShiroUtils.getUserId());
		sysUser.setUpdateName(ShiroUtils.getUserName());
		sysUser.setIsDel(0);
		result=sysUserMapper.insertSelective(sysUser);
		if(result>0){
			json.put("success", true);
			json.put("msg", "添加成功,初始密码为123456！");
		}else{
			json.put("success", false);
			json.put("msg", "添加失败！");
		}
		return json;
	}
	
	public JSONObject saveInsert(SysUser sysUser){
		JSONObject json=new JSONObject();
		json=validateSys(sysUser);
		if(!json.getBooleanValue("success")){
			return json;
		}
		int result=0;
		result=sysUserMapper.insertSelective(sysUser);
		if(result>0){
			json.put("success", true);
			json.put("msg", "添加成功,初始密码为123456！");
		}else{
			json.put("success", false);
			json.put("msg", "添加失败！");
		}
		return json;
	}
	/**
	 * 保存同步OMS供应商，创建登录信息
	 * @param sysUser
	 * @return
	 */
	public JSONObject saveSupplierInsert(SysUser sysUser){
		JSONObject json=new JSONObject();
		json=validateSupplierSys(sysUser);
		if(!json.getBooleanValue("success")){
			return json;
		}
		int result=0;
		result=sysUserMapper.insertSelective(sysUser);
		if(result>0){
			json.put("success", true);
			json.put("msg", "添加成功,初始密码为123456！");
		}else{
			json.put("success", false);
			json.put("msg", "添加失败！");
		}
		return json;
	}
	
	public JSONObject saveEdit(SysUser sysUser){
		JSONObject json=new JSONObject();
		int result=0;
		Date date=new Date();
/*		if(sysUser.getIsDel()==1){
			sysUser.setDelDate(date);
			sysUser.setDelId(ShiroUtils.getUserId());
			sysUser.setDelName(ShiroUtils.getUserName());
		}else{
			sysUser.setUpdateDate(date);
			sysUser.setUpdateId(ShiroUtils.getUserId());
			sysUser.setUpdateName(ShiroUtils.getUserName());
		}*/
		sysUser.setUpdateDate(date);
		sysUser.setUpdateId(ShiroUtils.getUserId());
		sysUser.setUpdateName(ShiroUtils.getUserName());
		result=sysUserMapper.updateByPrimaryKeySelective(sysUser);
		if(result>0){
			json.put("success", true);
			json.put("msg", "修改成功！");
		}else{
			json.put("success", false);
			json.put("msg", "修改失败！");
		}
		return json;
	}

	@Override
	public List<Map<String, Object>> queryByPage(SearchPageUtil searchPageUtil) {
		// TODO Auto-generated method stub
		return sysUserMapper.queryByPage(searchPageUtil);
	}

	@Override
	public List<SysUser> selectByCompanyId(String companyId) {
		return sysUserMapper.selectByCompanyId(companyId);
	}
	
	/**
	 * 验证密码是否和原密码一样
	 * @param sysUserId
	 * @param password
	 * @return
	 */
	public JSONObject checkPassword(String sysUserId,String password){
		JSONObject json = new JSONObject();
		json.put("result", false);
		String md5Passwrod = ShiroUtils.getInstance().getMD5(password, "UTF-8", 0).toLowerCase();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		//如果未登入，则查询后判断
		if(user == null){
			SysUser userByPhone = sysUserMapper.selectByPrimaryKey(sysUserId);
			if(userByPhone != null){
				if(!userByPhone.getPassword().equals(md5Passwrod)){
					json.put("result", true);
					return json;
				}
			}
		}
		
		//如果已登入，则在session中获取判断
		if(user != null && !user.getPassword().equals(md5Passwrod)){
			json.put("result", true);
		}
		return json;
	}

	/**
	 * 获取系统消息和好友申请条数
	 * @param params
	 */
	public void getMessageListParams(Map<String, Object> params) {
		//系统消息，暂时没做
		params.put("sysCount",0);
		//好友申请消息
		Map<String,Object> friendMap = new HashMap<String,Object>();
		friendMap.put("inviteeId",ShiroUtils.getCompId());
		int friendCount = inviteFriendMapper.queryInviteCount(friendMap);
		params.put("friendCount",friendCount);
	}

	/**
	 * 获取消息列表数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public String loadMessageData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/system/";
		switch (tabId) {
			case 0:
//				params.put("interest", "0");
				returnPath += "sysMsgList";
				break;
			case 1:
				params.put("inviteeId",ShiroUtils.getCompId());
				searchPageUtil.setObject(params);
				List<BuySupplierFriendInvite> list = inviteFriendMapper.queryInvitePage(searchPageUtil);
				searchPageUtil.getPage().setList(list);
				returnPath += "friendMsgList";
				break;
			default:
				returnPath += "sysMsgList";
				break;
		}

		return returnPath;
	}

	public List<Map<String, Object>> getCompanyUser(SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> userList = sysUserMapper.queryByPage(searchPageUtil);
		return userList;
	}

	@Override
	public List<SysUser> getCompanyUser(Map<String, Object> map) {
		return sysUserMapper.getCompanyUser(map);
	}

	@Override
	public SysUser queryExistUser(SysUser sysUser) {
		return sysUserMapper.queryExistUser(sysUser);
	}

	@Override
	public int updateByCompanyId(SysUser record) {
		return sysUserMapper.updateByCompanyId(record);
	}
}
