package com.nuotai.trading.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.alibaba.druid.util.StringUtils;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.oms.client.OMSClient;
import com.oms.client.OMSRequest;
import com.oms.client.OMSResponse;

/**
 * 钉钉工具类
 * @author Administrator
 *
 */
public class DingDingUtils {
	private static SysUserService sysUserService;
	private static TradeVerifyHeaderService tradeVerifyHeaderService;
	/**
     * 发送钉钉消息
     * @param jsob
     * @return
     */
    public static boolean sendDingDingMessage(JSONObject jsob) {
    	System.out.println("******进入sendDingDingMessage*******");
    	System.out.println("发送程序地址："+Constant.DINGDING_URL+"dingDing/sendMsgNew");
		
    	boolean resultBoolean = false;
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 消息时间
		String notifytime = sdf.format(new Date());
		//获得sign
		String sign;
		try {
			sign = DesUtil.encrypt("nuotai");
			OMSRequest request = new OMSRequest();
			//方法名
			request.setMethodName("dingDing/sendMsgNew");
			//消息时间
			request.setNotifytime(notifytime);
			//签名
			request.setSign(sign);
			//内容
			request.setContent(jsob);
			//密钥
			request.setToken("nuotai");
			OMSClient client = new OMSClient(Constant.DINGDING_URL);
			OMSResponse response = client.execute(request);
			if (response.isSuccess()) {
				resultBoolean = true;
			}
			System.out.println(resultBoolean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("******************resultBoolean**********");
		return resultBoolean;
    }
    
    /**
     * 审批发送钉钉消息-待审批人员
     * @param userId 要发送消息的用户id
     * @param verifyId 审批id
     */
    public static void verifyDingDingMessage(String userId,String verifyId){
    	sysUserService = ShiroUtils.getBean(SysUserService.class);
		tradeVerifyHeaderService = ShiroUtils.getBean(TradeVerifyHeaderService.class);
    	SysUser user = sysUserService.selectByPrimaryKey(userId);
    	if(user != null && !StringUtils.isEmpty(user.getDingdingId())){
    		TradeVerifyHeader verifyHeader = tradeVerifyHeaderService.get(verifyId);
    		JSONObject json = new JSONObject();
    		json.put("agentid", "3838");
    		json.put("touser", user.getDingdingId());
    		json.put("msgtype", "oa");
    		json.put("head", "买卖系统审批");
    		json.put("bgcolor", "FFF7B55E");
    		json.put("message_url", Constant.TRADE_URL + "DDS_verify/getDetailMobile?userId="+userId+"&verifyId="+verifyId);
    		json.put("pc_message_url", Constant.TRADE_URL + "DDS_verify/getDetail?userId="+userId+"&verifyId="+verifyId);
    		json.put("author", "买卖系统");
    		json.put("title", verifyHeader.getCreateName()+"的【"+verifyHeader.getTitle()+"】需要您审批");
    		json.put("text", "");
    		json.put("content", "");
    		JSONArray form = new JSONArray();
    		JSONObject item = new JSONObject();
    		item.put("key", "审批编号:");
    		item.put("value", verifyHeader.getId());
    		form.add(item);
    		JSONObject item1 = new JSONObject();
    		item1.put("key", "申请人:");
    		item1.put("value", verifyHeader.getCreateName());
    		form.add(item1);
    		JSONObject item2 = new JSONObject();
    		item2.put("key", "申请类型:");
    		item2.put("value", verifyHeader.getTitle());
    		form.add(item2);
    		JSONObject item3 = new JSONObject();
    		item3.put("key", "到达时间:");
    		item3.put("value", DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
    		form.add(item3);
    		json.put("form", form);
    		JSONObject rich = new JSONObject();
    		rich.put("key", "审批状态:");
    		rich.put("value", "审批中");
    		json.put("rich", rich);
    		System.out.println("********钉钉消息开始发送**********");
    		sendDingDingMessage(json);
    		System.out.println("********钉钉消息结束发送**********");
    	}
	}
    
    /**
     * 审批发送钉钉消息-审批结束通知发起人
     * @param verifyId
     */
    public static void verifyDingDingOwn(String verifyId){
    	sysUserService = ShiroUtils.getBean(SysUserService.class);
		tradeVerifyHeaderService = ShiroUtils.getBean(TradeVerifyHeaderService.class);
		
		TradeVerifyHeader verifyHeader = tradeVerifyHeaderService.get(verifyId);
    	SysUser user = sysUserService.selectByPrimaryKey(verifyHeader.getCreateId());
    	if(user != null && !StringUtils.isEmpty(user.getDingdingId())){
    		if(!"0".equals(verifyHeader.getStatus())){
    			JSONObject json = new JSONObject();
    			json.put("agentid", "3838");
    			json.put("touser", user.getDingdingId());
    			json.put("msgtype", "oa");
    			json.put("head", "买卖系统审批");
    			json.put("bgcolor", "FFF7B55E");
    			json.put("message_url", Constant.TRADE_URL + "DDS_verify/getDetailMobile?userId="+ShiroUtils.getUserId()+"&verifyId="+verifyId);
    			json.put("pc_message_url", Constant.TRADE_URL + "DDS_verify/getDetail?userId="+ShiroUtils.getUserId()+"&verifyId="+verifyId);
    			json.put("author", "买卖系统");
    			String status = "审批通过";
    			switch (verifyHeader.getStatus()) {
				case "1":
					status = "审批通过";
					break;
				case "2":
					status = "被拒绝";
					break;
				case "3":
					status = "被撤销";
					break;
				default:
					break;
				}
    			json.put("title", "你的【"+verifyHeader.getTitle()+"】"+status);
    			json.put("text", "");
    			json.put("content", "");
    			JSONArray form = new JSONArray();
    			JSONObject item = new JSONObject();
        		item.put("key", "审批编号:");
        		item.put("value", verifyHeader.getId());
        		form.add(item);
        		JSONObject item1 = new JSONObject();
        		item1.put("key", "申请人:");
        		item1.put("value", verifyHeader.getCreateName());
        		form.add(item1);
    			JSONObject item2 = new JSONObject();
    			item2.put("key", "申请类型:");
    			item2.put("value", verifyHeader.getTitle());
    			form.add(item2);
    			JSONObject item3 = new JSONObject();
        		item3.put("key", "完结时间:");
        		item3.put("value", DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        		form.add(item3);
    			json.put("form", form);
    			JSONObject rich = new JSONObject();
    			rich.put("key", "审批状态:");
    			rich.put("value", status);
    			json.put("rich", rich);
    			System.out.println("********钉钉消息开始发送**********");
    			sendDingDingMessage(json);
    			System.out.println("********钉钉消息结束发送**********");
    		}
    	}
	}
    
    /**
     * 审批发送钉钉消息--抄送人员
     * @param verifyId
     */
    public static void verifyDingDingCopy(String userId,String verifyId){
    	sysUserService = ShiroUtils.getBean(SysUserService.class);
		tradeVerifyHeaderService = ShiroUtils.getBean(TradeVerifyHeaderService.class);
    	SysUser user = sysUserService.selectByPrimaryKey(userId);
    	if(user != null && !StringUtils.isEmpty(user.getDingdingId())){
    		TradeVerifyHeader verifyHeader = tradeVerifyHeaderService.get(verifyId);
			JSONObject json = new JSONObject();
			json.put("agentid", "3838");
			json.put("touser", user.getDingdingId());
			json.put("msgtype", "oa");
			json.put("head", "买卖系统审批");
			json.put("bgcolor", "FFF7B55E");
			json.put("message_url", Constant.TRADE_URL + "DDS_verify/getDetailMobile?userId="+userId+"&verifyId="+verifyId);
			json.put("pc_message_url", Constant.TRADE_URL + "DDS_verify/getDetail?userId="+userId+"&verifyId="+verifyId);
			json.put("author", "买卖系统");
			String status = "审批通过";
			switch (verifyHeader.getStatus()) {
			case "0":
				status = "审批中";
				break;
			case "1":
				status = "审批通过";
				break;
			case "2":
				status = "被拒绝";
				break;
			case "3":
				status = "被撤销";
				break;
			default:
				break;
			}
			json.put("title", verifyHeader.getCreateName()+"的【"+verifyHeader.getTitle()+"】"+status);
			json.put("text", "");
			json.put("content", "");
			JSONArray form = new JSONArray();
			JSONObject item = new JSONObject();
    		item.put("key", "审批编号:");
    		item.put("value", verifyHeader.getId());
    		form.add(item);
    		JSONObject item1 = new JSONObject();
    		item1.put("key", "申请人:");
    		item1.put("value", verifyHeader.getCreateName());
    		form.add(item1);
			JSONObject item2 = new JSONObject();
			item2.put("key", "申请类型:");
			item2.put("value", verifyHeader.getTitle());
			form.add(item2);
			JSONObject item3 = new JSONObject();
    		item3.put("key", "到达时间:");
    		item3.put("value", DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
    		form.add(item3);
			json.put("form", form);
			JSONObject rich = new JSONObject();
			rich.put("key", "审批状态:");
			rich.put("value", status);
			json.put("rich", rich);
			System.out.println("********钉钉消息开始发送**********");
			sendDingDingMessage(json);
			System.out.println("********钉钉消息结束发送**********");
    	}
	}
    
    /**
     * 到货通知
     * @param userId
     * @param message
     */
    public static void daoHuoDingDingMessage(String userId,String message){
    	JSONObject json = new JSONObject();
		json.put("agentid", "159746380");
		json.put("touser", userId);
		json.put("msgtype", "oa");
		json.put("head", "");
		json.put("bgcolor", "FFF7B55E");
		json.put("message_url", "");
		json.put("pc_message_url", "");
		json.put("author", "买卖系统");
		json.put("title", "到货通知");
		json.put("text", message);
		json.put("content", message);
		System.out.println("********钉钉消息开始发送**********");
		sendDingDingMessage(json);
		System.out.println("********钉钉消息结束发送**********");
    }
    
    /**
     * 发送订单消息
     * @param userId 接收人id
     * @param message 通知内容
     * @param title 通知标题
     */
    public static void sendDingDingMessage(String userId,String message,String title){
    	sysUserService = ShiroUtils.getBean(SysUserService.class);
    	SysUser user = sysUserService.selectByPrimaryKey(userId);
    	if(user != null && !StringUtils.isEmpty(user.getDingdingId())){
    		JSONObject json = new JSONObject();
    		json.put("agentid", "159746380");
    		json.put("touser", user.getDingdingId());
    		json.put("msgtype", "oa");
    		json.put("head", "");
    		json.put("bgcolor", "FFF7B55E");
    		json.put("message_url", "");
    		json.put("pc_message_url", "");
    		json.put("author", "买卖系统");
    		json.put("title", title);
    		json.put("text", message);
    		json.put("content", message);
    		System.out.println("********钉钉消息开始发送**********");
    		sendDingDingMessage(json);
    		System.out.println("********钉钉消息结束发送**********");
    	}
    }
    
    /**
     * 定时任务发送钉钉消息
     * @param user
     * @param verifyHeader
     */
    public static void verifyDingDingMessageTime(SysUser user,TradeVerifyHeader verifyHeader){
    	if(user != null && !StringUtils.isEmpty(user.getDingdingId())){
    		JSONObject json = new JSONObject();
    		json.put("agentid", "3838");
    		json.put("touser", user.getDingdingId());
    		json.put("msgtype", "oa");
    		json.put("head", "买卖系统审批");
    		json.put("bgcolor", "FFF7B55E");
    		json.put("message_url", Constant.TRADE_URL + "DDS_verify/getDetailMobile?userId="+user.getId()+"&verifyId="+verifyHeader.getId());
    		json.put("pc_message_url", Constant.TRADE_URL + "DDS_verify/getDetail?userId="+user.getId()+"&verifyId="+verifyHeader.getId());
    		json.put("author", "买卖系统");
    		json.put("title", verifyHeader.getCreateName()+"的【"+verifyHeader.getTitle()+"】需要您审批");
    		json.put("text", "");
    		json.put("content", "");
    		JSONArray form = new JSONArray();
    		JSONObject item = new JSONObject();
    		item.put("key", "审批编号:");
    		item.put("value", verifyHeader.getId());
    		form.add(item);
    		JSONObject item1 = new JSONObject();
    		item1.put("key", "申请人:");
    		item1.put("value", verifyHeader.getCreateName());
    		form.add(item1);
    		JSONObject item2 = new JSONObject();
    		item2.put("key", "申请类型:");
    		item2.put("value", verifyHeader.getTitle());
    		form.add(item2);
    		JSONObject item3 = new JSONObject();
    		item3.put("key", "到达时间:");
    		item3.put("value", DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
    		form.add(item3);
    		json.put("form", form);
    		JSONObject rich = new JSONObject();
    		rich.put("key", "审批状态:");
    		rich.put("value", "审批中");
    		json.put("rich", rich);
    		System.out.println("********钉钉消息开始发送**********");
    		sendDingDingMessage(json);
    		System.out.println("********钉钉消息结束发送**********");
    	}
	}
    
    public static void main(String[] args) {
    	String userId = "050342098972";
		String message = "您的采购单【64011383148592951706】有到货入库，发货单号【FH20180119145504212】,入库单号【RK1516344904378】,入库日期：2018-01-19";
		DingDingUtils.daoHuoDingDingMessage(userId, message);
	}
}
