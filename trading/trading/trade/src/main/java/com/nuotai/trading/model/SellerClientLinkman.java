package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 客户联系人
 * 
 * @author "
 * @date 2017-07-26 14:39:11
 */
@Data
public class SellerClientLinkman implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//客户ID
	private String sellerClientId;
	//客户联系人
	private String clientPerson;
	//手机号码
	private String clientPhone;
	//收货地址
	private String shipAddress;
	//电话区号
	private String zone;
	//电话号
	private String telno;
	//是否删除：0表示未删除，-1表示已删除
	private Integer isdel;
	//创建时间
	private Date createDate;
	//传真
	private String fax;
	//qq号
	private String qq;
	//旺旺号
	private String wangNo;
	//邮箱号
	private String email;
	//是否默认联系人：1表示是；0表示否
	private Integer isDefault;
}
