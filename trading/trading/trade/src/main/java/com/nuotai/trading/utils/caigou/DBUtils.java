package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * JDBC连接
 * @author Administrator
 *
 */
public class DBUtils {
	private static String driver = "com.mysql.jdbc.Driver";
	private static String purchaseUrl = "jdbc:mysql://58.58.116.214:3306/nuotai_order?useUnicode=true&characterEncoding=GBK";
	private static String supplierUrl = "jdbc:mysql://58.58.116.214:3306/nuotai_order_supplier?useUnicode=true&characterEncoding=GBK";
	private static String purchaseDBUserName = "root";
	private static String purchaseDBPasswrod = "NuoTai2015_11";
	private static String tradeUrl = "jdbc:mysql://localhost:3306/tradingdb?useUnicode=true&characterEncoding=GBK";
	private static String tradeDBUserName = "root";
	private static String tradeDBPasswrod = "123456";
	
	private DBUtils() {
    }
	
	// 加载驱动
    static {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            System.out.println("驱动加载出错!");
        }
    }
    
    /**
     * 获得采购系统数据库连接
     * @return
     * @throws SQLException
     */
    public static Connection getPurchaseConnection() throws SQLException {
        return DriverManager.getConnection(purchaseUrl, purchaseDBUserName,purchaseDBPasswrod);
    }
    
    /**
     * 获得供应商系统数据库连接
     * @return
     * @throws SQLException
     */
    public static Connection getSupplierConnection() throws SQLException {
        return DriverManager.getConnection(supplierUrl, purchaseDBUserName,purchaseDBPasswrod);
    }
    
    /**
     * 获得买卖系统数据库连接
     * @return
     * @throws SQLException
     */
    public static Connection getTradeConnection() throws SQLException {
        return DriverManager.getConnection(tradeUrl, tradeDBUserName,tradeDBPasswrod);
    }
    
    /**
     * 关闭结果集
     * @param rs
     */
    public static void closeResultSet(ResultSet rs){
    	if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    /**
     * 关闭Statement
     * @param st
     */
    public static void closeStatement(PreparedStatement ps){
    	if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    /**
     * 释放连接
     * @param conn
     */
	public static void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				conn.close(); // 关闭连接
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
