package com.nuotai.trading.controller.platform.baseDate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.service.BuyProductSkuBomService;
import com.nuotai.trading.service.BuyProductSkuComposeService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

@Controller
@RequestMapping("platform/baseDate/productCompose")
public class ProductComposeController extends BaseController{
	
	@Autowired 
	private BuyProductSkuService buyProductSkuService;
	@Autowired 
	private BuyProductSkuComposeService buyProductSkuComposeService;
	@Autowired 
	private BuyProductSkuBomService  bomService;
	@Autowired
	private BuyProductSkuComposeService  composeService;
	@Autowired
	private TradeVerifyHeaderService  tradeVerifyHeaderService;
	
	
	//加载商品物料配置界面
	@RequestMapping("/loadProductComposeHtml")
	public String  loadProductComposeHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String, Object> param) {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		param.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		List<Map<String,Object>> skuList=buyProductSkuService.selectList(searchPageUtil);
		searchPageUtil.getPage().setList(skuList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/baseDate/materialManagement/productCompose";
	}
	
	/**
	 * 加载  商品/原材料
	 */
	@RequestMapping("/loadProductHtml")
	public String loadProductHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		String which="";
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		//物料配置供应商可以查询所有成品
		//params.put("companyId", ShiroUtils.getCompId());
		
		//获取已经选择的原材料barcode
		Object barcodesObj=params.get("barcodes");
		if(barcodesObj!=null && barcodesObj!=""){
			String[] barcodeArr = barcodesObj.toString().split(",");
			params.put("barcodeArr", barcodeArr);
		}
		List<String> exitList=bomService.selectExitProduct(ShiroUtils.getCompId());
		params.put("exitList", exitList);
		searchPageUtil.setObject(params);
		List<Map<String,Object>> productList =buyProductSkuService.inputSelect(searchPageUtil);
		searchPageUtil.getPage().setList(productList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		if("0".equals(params.get("productType"))){
			which+="platform/baseDate/materialManagement/product";
		}else{
			which+="platform/baseDate/materialManagement/rawMaterial";
		}
		return which;
	}
	
	/**
	 * 保存物料配置
	 * @param param
	 * @return
	 * @throws ServiceException 
	 */
	@RequestMapping(value="/saveCompose",method=RequestMethod.GET)
	@ResponseBody
	public String saveCompose(@RequestParam Map<String,Object> params){
		//物料配置去掉审批流程
//		insert(params, new IService(){
//			@Override
//			public JSONObject init(Map<String,Object> params)
//					throws ServiceException {
//				JSONObject json = new JSONObject();
//				json.put("verifySuccess", "platform/baseDate/productCompose/verifySuccess");//审批成功调用的方法
//				json.put("verifyError", "platform/baseDate/productCompose/verifyError");//审批失败调用的方法
//				json.put("relatedUrl", "platform/baseDate/productCompose/verifyDetail");//审批详细信息地址
//				List<String> idList = buyProductSkuComposeService.saveCompose(params);
//				json.put("idList", idList);
//				return json;
//			}
//		});
		JSONObject json = new JSONObject();
		json = buyProductSkuComposeService.saveCompose(params);
		
		if (json.get("msg") != null){
			json.put("success", false);
		}else{
			json.put("success", true);
			json.put("msg", "物料配置设置成功！");
		}	
	
		return json.toString();
	}
	
	//修改物料配置审批
	@RequestMapping(value="/saveUpdateCompose",method=RequestMethod.GET)
	@ResponseBody
	public String saveUpdateCompose(String id,@RequestParam Map<String,Object> params){
//		update(id,params, new IService(){
//			@Override
//			public JSONObject init(Map<String,Object> params)
//					throws ServiceException {
//				JSONObject json = new JSONObject();
//				json.put("verifySuccess", "platform/baseDate/productCompose/verifySuccess");//审批成功调用的方法
//				json.put("verifyError", "platform/baseDate/productCompose/verifyError");//审批失败调用的方法
//				json.put("relatedUrl", "platform/baseDate/productCompose/verifyDetail");//审批详细信息地址
//				List<String> idList = buyProductSkuComposeService.saveCompose(params);
//				json.put("idList", idList);
//				return json;
//			}
//		});
		JSONObject json = new JSONObject();
		json = buyProductSkuComposeService.saveCompose(params);
		
		if (json.get("msg") != null){
			json.put("success", false);
		}else{
			json.put("success", true);
			json.put("msg", "物料配置设置成功！");
		}
		
		return json.toString();
	}
	
	//加载商品物料配置成功界面
	@RequestMapping("/success")
	public String  success() {
		return "platform/baseDate/materialManagement/success";
	}
	
	//审批通过
	@RequestMapping("/verifySuccess")
	public void  verifySuccess(String id) {
		Map<String,String> map=new HashMap<>();
		map.put("msg", "success");
		map.put("id", id);
		bomService.setStatus(map);
	}
	
	//审批不通过
	@RequestMapping("/verifyError")
	public void  verifyError(String id) {
		Map<String,String> map=new HashMap<>();
		map.put("msg", "fail");
		map.put("id", id);
		bomService.setStatus(map);
	}
	
	/**
	 * 查看采购计划详情页面 
	 * @return
	 */
	@RequestMapping("/verifyDetail")
	public String verifyDetail(String id){
		BuyProductSkuBom bom = bomService.selectByBomId(id);
		Map<String, Object> bomMap = new HashMap<>();
		bomMap.put("companyId", bom.getCompanyId());
		bomMap.put("productId", bom.getProductId());
		List<Map<String,Object>>  materialList=composeService.selectByBomId(bomMap);
		//获得审批数据
		TradeVerifyHeader verifyDetail = tradeVerifyHeaderService.getVerifyHeaderByRelatedId(id);
		model.addAttribute("bom", bom);
		model.addAttribute("materialList",materialList);
		model.addAttribute("verifyDetail",verifyDetail);
		return "platform/baseDate/materialManagement/composeDetail";
	}
	
	
}
