package com.nuotai.trading.controller.platform.baseInfo;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.dao.BuyProductMapper;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.model.BuyStockBacklogDetails;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.BuyWarehouseBacklog;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.model.oms.ScmProductOldsku;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.BuyStockBacklogDetailsService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.service.BuyWarehouseBacklogDetailsService;
import com.nuotai.trading.service.BuyWarehouseBacklogService;
import com.nuotai.trading.service.BuyWarehouseLogService;
import com.nuotai.trading.service.BuyWarehouseProdcutsService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;


/**
 * 商品库存/库存积压
 * 
 * @author "
 * @date 2017-09-11 15:00:46
 */
@Controller
@RequestMapping("platform/baseInfo/inventorymanage")
public class BuyWarehouseProdcutsController extends BaseController{ 
	
	@Autowired
	private BuyWarehouseProdcutsService buyWarehouseProdcutsService;	
	@Autowired
	private BuyWarehouseLogService buyWarehouseLogService;	
	@Autowired
	private BuyWarehouseBacklogService buyWarehouseBacklogService;	
	@Autowired
	private BuyWarehouseBacklogDetailsService buyWarehouseBacklogDetailsService;	
	@Autowired
	private BuyProductMapper buyProductMapper;	
	@Autowired
	private SysShopService shopService;	
	@Autowired
	private BuySupplierFriendService buySupplierFriendService;
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private BuyStockBacklogDetailsService buyStockBacklogDetailsService;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	
	/**
	 * 商品库存余额(调用oms库存数据)
	 * @param map
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/prodcutsInventory")
	public String selectProdcutsInventory(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String goodsname = (String) (map.get("goodsName") == null ? "" : map.get("goodsName").toString().replaceAll("\\s*", ""));
		String productCode = (String) (map.get("productCode") == null ? "" : map.get("productCode").toString().replaceAll("\\s*", ""));
		String barcode = (String) (map.get("barcode") == null ? "" : map.get("barcode").toString().replaceAll("\\s*", ""));
		String levelUrl = null;
		try {
			levelUrl = Constant.OMS_INTERFACE_URL + "getGoods?goodsName="
					+ java.net.URLEncoder.encode(goodsname, "UTF-8")
					+ "&productCode=" + productCode + "&skuoid="
					+ barcode + "&pageSize="
					+ searchPageUtil.getPage().getPageSize() + "&pageNo="
					+ searchPageUtil.getPage().getPageNo();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    

		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		JSONObject json = JSONObject.fromObject(arrayString);
		JSONArray productArray = JSONArray.fromObject(json.get("list"));
		
		searchPageUtil.setObject(map);
		List<ScmProductOldsku> list = (List<ScmProductOldsku>) JSONArray.toCollection(productArray, ScmProductOldsku.class);
		for (ScmProductOldsku scmProductOldsku : list){
			
			Long[] stockNum = new Long[2];
			stockNum[0] = Long.valueOf(0);
			stockNum[1] = Long.valueOf(0);
			if (scmProductOldsku.getStockMap().isEmpty()){			
				Map<String, Long[]> stockMap = scmProductOldsku.getStockMap();
				stockMap.put("泰安大仓",stockNum);
				stockMap.put("质检仓",stockNum);
				stockMap.put("退换货仓",stockNum);	
				scmProductOldsku.setStockMap(stockMap);
			}
			if (scmProductOldsku.getStockMap().size() == 1){
				Map<String, Long[]> stockMap = scmProductOldsku.getStockMap();
				stockMap.put("质检仓",stockNum);
				stockMap.put("退换货仓1",stockNum);
				scmProductOldsku.setStockMap(stockMap);
			}
			if (scmProductOldsku.getStockMap().size() == 2){
				Map<String, Long[]> stockMap = scmProductOldsku.getStockMap();
				stockMap.put("退换货仓2",stockNum);
				scmProductOldsku.setStockMap(stockMap);
			}					
		}
		searchPageUtil.getPage().setList(list);
		searchPageUtil.getPage().setCount(json.getInt("total"));
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/baseInfo/inventorymanage/prodcutsInventory";
	}
	/**
	 * 库存余额（买卖系统）
	 */
//	public String selectProdcutsInventory(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
//		
//		int sku = params.containsKey("skuCode")?Integer.parseInt(params.get("skuCode").toString()):99;
//		switch (sku) {
//		case 0:
//			params.put("skuCode", "M");
//			break;
//		case 1:
//			params.put("skuCode", "L");
//			break;
//		case 2:
//			params.put("skuCode", "XL");
//			break;
//		case 3:
//			params.put("skuCode", "XXL");
//			break;
//		case 4:
//			params.put("skuCode", "XXXL");
//			break;
//		case 99:
//			params.put("skuCode", "");
//			break;
//		default:
//			params.put("skuCode", "");
//			break;
//		}
//		
//		int statusId = params.containsKey("status")?Integer.parseInt(params.get("status").toString()):99;
//		switch (statusId) {
//		case 0:
//			params.put("status", "0");
//			break;
//		case 1:
//			params.put("status", "1");
//			break;
//		case 99:
//			params.put("status", "");
//			break;
//		default:
//			params.put("status", "");
//			break;
//		}
//		
//		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
//		if(searchPageUtil.getPage()==null){
//			searchPageUtil.setPage(new Page());
//		}
//		searchPageUtil.setObject(params);
//		List<Map<String,Object>> prodcutsList = buyWarehouseProdcutsService.selectProdcutsByPage(searchPageUtil);
//		searchPageUtil.getPage().setList(prodcutsList);
//		model.addAttribute("searchPageUtil", searchPageUtil);
//			
//		return "platform/baseInfo/inventorymanage/prodcutsInventory";
//	}
	
	/**
	 * 商品库存明细
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("prodcutsDetails")
	public String selectProdcutsDetails(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		
		String id = params.get("id").toString();
		BuyWarehouseProdcuts buyWarehouseProdcuts = buyWarehouseProdcutsService.selectProdcutByid(id);
		model.addAttribute("buyWarehouseProdcuts", buyWarehouseProdcuts);
		params.put("companyId",ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil.setObject(params);
		List<Map<String,Object>> prodcutsList = buyWarehouseProdcutsService.selectProdcutsDetailsByPage(searchPageUtil);
		searchPageUtil.getPage().setList(prodcutsList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		
		return "platform/baseInfo/inventorymanage/prodcutsDetails";
	}
	
	/**
	 * 商品出入明细
	 * @param map
	 * @return
	 */
	@RequestMapping("/prodcutsRecording")
	public String selectprodcutsRecording(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("companyId",ShiroUtils.getCompId());
		List<SysUser> companyUserList = sysUserMapper.getCompanyUser(params);
		model.addAttribute("companyUserList", companyUserList);
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		params.put("buyersId", ShiroUtils.getCompId());
		//获取互通供应商
		List<BuySupplierFriend> supplierList = buySupplierFriendService.selectByMap(params);
		model.addAttribute("supplierList", supplierList);
		//获取部门的名称
		List<SysShop> shopList = shopService.selectByMap(params);
		model.addAttribute("shopList", shopList);
		//获取仓库
		List<TBusinessWharea> tBusinessWharea = tBusinessWhareaService.getCompanyId();
		model.addAttribute("tBusinessWharea", tBusinessWharea);
		String productCode = (String) (params.get("productCode") == null ? "" : params.get("productCode").toString().replaceAll("\\s*", ""));
		String productName = (String) (params.get("productName") == null ? "" : params.get("productName").toString().replaceAll("\\s*", ""));
		String skuCode = (String) (params.get("skuCode") == null ? "" : params.get("skuCode").toString().replaceAll("\\s*", ""));
		String skuName = (String) (params.get("skuName") == null ? "" : params.get("skuName").toString().replaceAll("\\s*", ""));
		String barcode = (String) (params.get("barcode") == null ? "" : params.get("barcode").toString().replaceAll("\\s*", ""));
		String shopName = (String) (params.get("shopName") == null ? "" : params.get("shopName").toString().replaceAll("\\s*", ""));
		String batchNo = (String) (params.get("batchNo") == null ? "" : params.get("batchNo").toString().replaceAll("\\s*", ""));
		String orderCode = (String) (params.get("orderCode") == null ? "" : params.get("orderCode").toString().replaceAll("\\s*", ""));
		params.put("productCode",productCode);
		params.put("productName",productName);
		params.put("barcode",barcode);
		params.put("shopName",shopName);
		params.put("skuCode",skuCode);
		params.put("skuName",skuName);
		params.put("batchNo",batchNo);
		params.put("orderCode",orderCode);
		
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil.setObject(params);
		//获取商品入库数据
		List<Map<String,Object>> prodcutsLogList = buyWarehouseLogService.selectProdcutsByPage(searchPageUtil);
		searchPageUtil.getPage().setList(prodcutsLogList);
		model.addAttribute("searchPageUtil", searchPageUtil);
			
		return "platform/baseInfo/inventorymanage/prodcutsRecording";
	}
	
	/**
	 * 库零分析
	 * @param map
	 * @return
	 */
	@RequestMapping("/backlogWarning")
	public String selectprodcutsBacklogWarning(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		params.put("companyId",ShiroUtils.getCompId());
		String productCode = (String) (params.get("productCode") == null ? "" : params.get("productCode").toString().replaceAll("\\s*", ""));
		String productName = (String) (params.get("productName") == null ? "" : params.get("productName").toString().replaceAll("\\s*", ""));
		String barcode = (String) (params.get("barcode") == null ? "" : params.get("barcode").toString().replaceAll("\\s*", ""));
		String minBacklogdaynum = (String) (params.get("minBacklogdaynum") == null ? "" : params.get("minBacklogdaynum").toString().replaceAll("\\s*", ""));
		String maxBacklogdaynum = (String) (params.get("maxBacklogdaynum") == null ? "" : params.get("maxBacklogdaynum").toString().replaceAll("\\s*", ""));
		String skuCode = (String) (params.get("skuCode") == null ? "" : params.get("skuCode").toString().replaceAll("\\s*", ""));
		String skuName = (String) (params.get("skuName") == null ? "" : params.get("skuName").toString().replaceAll("\\s*", ""));
		String batchNo = (String) (params.get("batchNo") == null ? "" : params.get("batchNo").toString().replaceAll("\\s*", ""));
		params.put("productCode",productCode);
		params.put("productName",productName);
		params.put("barcode",barcode);
		params.put("minBacklogdaynum",minBacklogdaynum);
		params.put("maxBacklogdaynum",maxBacklogdaynum);
		params.put("skuCode",skuCode);
		params.put("skuName",skuName);
		params.put("batchNo",batchNo);

		searchPageUtil.setObject(params);
		//分页查询
		SearchPageUtil prodcuts = buyWarehouseBacklogDetailsService.selectProdcutsByPage(searchPageUtil);

		model.addAttribute("searchPageUtil", prodcuts);
			
		return "platform/baseInfo/inventorymanage/backlogWarning";
	}
	
	/**
	 *  库零分析明细
	 * @param searchPageUtil
	 * @param params
	 * @return  
	 */
	@RequestMapping("backlogWarningDetails")
	public String selectBacklogWarningDetails(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("companyId",ShiroUtils.getCompId());
		String id = params.get("id").toString();
		BuyWarehouseBacklog buyWarehouseBacklog = buyWarehouseBacklogService.get(id);
		model.addAttribute("buyWarehouseBacklog", buyWarehouseBacklog);
		BuyProduct buyProduct = buyProductMapper.selectByProductCode(buyWarehouseBacklog.getProductCode());
		model.addAttribute("buyProduct", buyProduct);
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		searchPageUtil.setObject(params);
		SearchPageUtil prodcutsList = buyWarehouseBacklogDetailsService.selectProdcutsByPage(searchPageUtil);
		
		model.addAttribute("searchPageUtil", prodcutsList);
		
		return "platform/baseInfo/inventorymanage/backlogWarningDetails";
	}
	 
	/**
	 * 库存积压预警
	 * @param map
	 * @return
	 */
	@RequestMapping("/stockBacklogWarning")
	public String selectProdcutsStockBacklog(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		params.put("companyId",ShiroUtils.getCompId());
		String productCode = (String) (params.get("productCode") == null ? "" : params.get("productCode").toString().replaceAll("\\s*", ""));
		String productName = (String) (params.get("productName") == null ? "" : params.get("productName").toString().replaceAll("\\s*", ""));
		String barcode = (String) (params.get("barcode") == null ? "" : params.get("barcode").toString().replaceAll("\\s*", ""));
		String minBacklogdaynum = (String) (params.get("minBacklogdaynum") == null ? "" : params.get("minBacklogdaynum").toString().replaceAll("\\s*", ""));
		String maxBacklogdaynum = (String) (params.get("maxBacklogdaynum") == null ? "" : params.get("maxBacklogdaynum").toString().replaceAll("\\s*", ""));
		String skuCode = (String) (params.get("skuCode") == null ? "" : params.get("skuCode").toString().replaceAll("\\s*", ""));
		String skuName = (String) (params.get("skuName") == null ? "" : params.get("skuName").toString().replaceAll("\\s*", ""));
		String batchNo = (String) (params.get("batchNo") == null ? "" : params.get("batchNo").toString().replaceAll("\\s*", ""));
		params.put("productCode",productCode);
		params.put("productName",productName);
		params.put("barcode",barcode);
		params.put("minBacklogdaynum",minBacklogdaynum);
		params.put("maxBacklogdaynum",maxBacklogdaynum);
		params.put("skuCode",skuCode);
		params.put("skuName",skuName);
		params.put("batchNo",batchNo);

		searchPageUtil.setObject(params);
		//分页查询
		List<BuyStockBacklogDetails> stockBacklogList = buyStockBacklogDetailsService.selectProdcutsByPage(searchPageUtil);
		searchPageUtil.getPage().setList(stockBacklogList);
		model.addAttribute("searchPageUtil", searchPageUtil);
			
		return "platform/baseInfo/inventorymanage/stockBacklogWarning";
	}
}
