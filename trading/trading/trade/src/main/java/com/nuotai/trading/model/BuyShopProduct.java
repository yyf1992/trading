package com.nuotai.trading.model;

import lombok.Data;
import org.apache.poi.util.Internal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 店铺商品供应商对应表
 * @author Administrator
 *
 */
@Data
public class BuyShopProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    //
    private String id;
    //采购商id
    private String companyId;
    //采购商名称
    private String companyName;
    //采购商条形码
    private String barcode;
    //供应商Id
    private String supplierId;
    //供应商名称
    private String supplierName;
    //供应商条形码
    private String supplierBarcode;
    //货号
    private String productCode;
    //商品名称
    private String productName;
    //规格代码
    private String skuCode;
    //规格名称
    private String skuName;
    //价格
    private BigDecimal price;
    //单位ID
    private String unitId;
    //单位名称
    private String unitName;
	//日产能
	private Integer dailyOutput;
	//到货天数
	private Integer storageDay;
    //备注
    private String note;
    //状态 (0:草稿 1:内部审批中 2:对方审批中 3:审批通过)
    private String status;
    //状态名称
    private String statusName;
    //是否价格变更
    private Integer isPriceUpd;
    //价格变更时间
    private Date priceUpdDate;
    //价格变更人
    private String priceUpdPerson;
    //价格变更内部审批状态(0:通过 1:未通过)
    private Integer isPriceUpdConfirm;
    //是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    //创建人id
    private String createId;
    //创建人名称
    private String createName;
    //创建时间
    private Date createDate;
    //修改人ID
    private String updateId;
    //修改人名称
    private String updateName;
    //修改时间
    private Date updateDate;
    //删除人id
    private String delId;
    //删除人姓名
    private String delName;
    //删除时间
    private Date delDate;
    //要插入的产品ID
    private String productId;
    //关联公司ID
    private String linkCompanyID;
    //关联公司产品条形码
    private String linkCompanyBarcode;
    //内部审批状态 0-审批中；1-通过；2-拒绝；3-撤销
    private String internalVerifyStatus;
    //外部审批状态 0-审批中；1-通过；2-拒绝；3-撤销
    private String externalVerifyStatus;

}