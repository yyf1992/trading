package com.nuotai.trading.controller.platform.baseInfo;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyWarehouseProdcutsHistory;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.BuyWarehouseProdcutsHistoryService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 商品汇总表
 * @author wangli
 * @date 2017-09-20 14:50:15
 */
@Controller
@RequestMapping("platform/baseInfo/inventorymanage")
public class BuyWarehouseProdcutsHistoryController extends BaseController {
	
	@Autowired
	private BuyWarehouseProdcutsHistoryService buyWarehouseProdcutsHistoryService;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	
	/**
	 * 商品汇总查询界面
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("/prodcutshistory")
	public String selectProdcutshistory(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		// 仓库管理
		TBusinessWharea tBusinessWharea = new TBusinessWharea();
		List<TBusinessWharea> warehouseList = tBusinessWhareaService.selectWhareaByMap(tBusinessWharea, map);
		model.addAttribute("warehouseList", warehouseList);
		if (map.get("startDate") != null ){		
			model.addAttribute("map", map);
			searchPageUtil.setObject(map);			
			//查询商品库存历史表
			List<BuyWarehouseProdcutsHistory> prodcutshistoryList = buyWarehouseProdcutsHistoryService.selectByDate(searchPageUtil,map);
			model.addAttribute("params", prodcutshistoryList);
			model.addAttribute("searchPageUtil", searchPageUtil);
			model.addAttribute("warehouseList", warehouseList);
			return "platform/baseInfo/inventorymanage/loadProdcutshistory";
		}
		
		return "platform/baseInfo/inventorymanage/prodcutshistory";
	}
	
	/**
	 * 商品汇总展示界面
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("/loadProdcutshistory")
	public String loadProdcutshistory(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){

		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		// 仓库管理
		TBusinessWharea tBusinessWharea = new TBusinessWharea();
		List<TBusinessWharea> warehouseList = tBusinessWhareaService.selectWhareaByMap(tBusinessWharea, map);
	
		if (map.get("startDate") != null ){
			model.addAttribute("map", map);
			searchPageUtil.setObject(map);
			//查询商品库存历史表
			List<BuyWarehouseProdcutsHistory> prodcutshistoryList = buyWarehouseProdcutsHistoryService.selectByDate(searchPageUtil,map);
			model.addAttribute("params", prodcutshistoryList);
			model.addAttribute("searchPageUtil", searchPageUtil);
			model.addAttribute("warehouseList", warehouseList);
		}
		return "platform/baseInfo/inventorymanage/loadProdcutshistory";
	}
	
	/**
	 * 商品汇总历史表
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("/selectProdcuts")
	public String selectProdcuts(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){

		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		// 仓库管理
		TBusinessWharea tBusinessWharea = new TBusinessWharea();
		List<TBusinessWharea> warehouseList = tBusinessWhareaService.selectWhareaByMap(tBusinessWharea, map);
		model.addAttribute("warehouseList", warehouseList);
		if (map.get("startDate") != null ){
			model.addAttribute("map", map);
			searchPageUtil.setObject(map);
			//查询商品库存历史表
			List<BuyWarehouseProdcutsHistory> prodcutshistoryList = buyWarehouseProdcutsHistoryService.selectProdcutsByDate(searchPageUtil,map);
			model.addAttribute("params", prodcutshistoryList);
			model.addAttribute("searchPageUtil", searchPageUtil);
			model.addAttribute("warehouseList", warehouseList);
			return "platform/baseInfo/inventorymanage/loadProdcuts";
		}
		return "platform/baseInfo/inventorymanage/selectProdcuts";
	}
	
}
