package com.nuotai.trading.model.wms;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库计划子表
 * @author Administrator
 *
 */
@Data
public class WmsInputPlanDetail {
    //主键
    private Long id;
    //计划id
    private Long planid;
    //商品货号
    private String procode;
    //商品名称
    private String proname;
    //
    private String skucode;
    //
    private String skuname;
    //条形码
    private String skuoid;
    //计划数
    private Long skucount;
    //计划金额
    private BigDecimal costprice;
    //实际到货量
    private Long realcount;
    //创建日期
    private Date created;

    private String memo;

    private String opter;
    private String batchno;
    private Long checkcount;
    private String ownercode;
    
}