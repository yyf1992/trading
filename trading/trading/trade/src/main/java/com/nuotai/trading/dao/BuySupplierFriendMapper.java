package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierFriend;

public interface BuySupplierFriendMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuySupplierFriend record);

    int insertSelective(BuySupplierFriend record);

    BuySupplierFriend selectByPrimaryKey(String id);
    
    BuySupplierFriend selectBySupplierId(String supplierId);
    
    BuySupplierFriend selectBySellerName(String sellerName);

    int updateByPrimaryKeySelective(BuySupplierFriend record);
    
    int updateBySupplierId(BuySupplierFriend record);

    int updateByPrimaryKey(BuySupplierFriend record);

	List<BuySupplierFriend> selectByMap(Map<String, Object> map);

    /**
     * 检查是否已经是好友
     * @param map
     * @return
     */
    List<BuySupplierFriend> friendCheck(Map<String, Object> map);

    /**
     * 更新卖家客户表关联
     * @param map2
     */
    void updateSellerClientFriendLink(Map<String, Object> map2);

    /**
     * 更新买家供应商表关联
     * @param map2
     */
    int updateBuySupplierFriendLink(Map<String, Object> map2);

    /**
     * 将同意过的好友信息插入到buy_supplier
     * @param buySupplier
     */
    void insBuySupplier(BuySupplier buySupplier);
    
    /**
     * 查询供应商id  code
     * @param map
     * @return
     */
	List<Map<String, Object>> getSupplier(Map<String, Object> map);
}