package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.TradeRoleMenuMapper;
import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.model.TradeRoleMenu;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class TradeRoleMenuService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeRoleMenuService.class);

	@Autowired
	private TradeRoleMenuMapper tradeRoleMenuMapper;
	
	public TradeRoleMenu get(String id){
		return tradeRoleMenuMapper.get(id);
	}
	
	public List<TradeRoleMenu> queryList(Map<String, Object> map){
		return tradeRoleMenuMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeRoleMenuMapper.queryCount(map);
	}
	
	public void add(TradeRoleMenu tradeRoleMenu){
		tradeRoleMenuMapper.add(tradeRoleMenu);
	}
	
	public void update(TradeRoleMenu tradeRoleMenu){
		tradeRoleMenuMapper.update(tradeRoleMenu);
	}
	
	public void delete(String id){
		tradeRoleMenuMapper.delete(id);
	}

	/**
	 * 获得角色下所有菜单
	 * @param roleId
	 * @return
	 */
	public List<TradeRoleMenu> getMenuByRoleId(String roleId) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("roleId", roleId);
		return tradeRoleMenuMapper.queryList(params);
	}

	/**
	 * 保存角色菜单关联信息
	 * @param params
	 * @return
	 */
	public JSONObject saveRoleMenu(Map<String, Object> params) throws Exception{
		JSONObject json = new JSONObject();
		//角色id
		String roleId = params.containsKey("roleId")?
			params.get("roleId")!=null?
			params.get("roleId").toString():"":"";
		//菜单信息
		String dataStr = params.containsKey("dataStr")?
			params.get("dataStr")!=null?
			params.get("dataStr").toString():"":"";
		//先删掉原有数据
		tradeRoleMenuMapper.deleteByRoleId(roleId);
		if(!dataStr.isEmpty()){
			String[] dataArray = dataStr.split("@");
			if(dataArray != null && dataArray.length > 0){
				for(String data : dataArray){
					String[] dataItem = data.split(",");
					TradeRoleMenu roleMenu = new TradeRoleMenu();
					roleMenu.setId(ShiroUtils.getUid());
					roleMenu.setRoleId(roleId);
					roleMenu.setMenuId(dataItem[0]);
					roleMenu.setVerifyType(dataItem[1]);
					roleMenu.setCreateTime(new Date());
					roleMenu.setCompanyId(ShiroUtils.getCompId());
					tradeRoleMenuMapper.add(roleMenu);
				}
			}
		}
		json.put("flag",true);
		json.put("msg","成功");
		return json;
	}
}
