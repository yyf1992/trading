package com.nuotai.trading.controller.platform.traderole;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.model.TradeRoleMenu;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.TradeRoleMenuService;
import com.nuotai.trading.service.TradeRoleService;
import com.nuotai.trading.service.TradeRoleUserService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


/**
 * 权限角色
 * 
 * @author "
 * @date 2017-09-13 16:40:54
 */
@Controller
@RequestMapping("platform/tradeRole")
public class TradeRoleController extends BaseController{
	@Autowired
	private TradeRoleService tradeRoleService;
	@Autowired
	private TradeRoleMenuService tradeRoleMenuService;
	@Autowired
	private SysMenuService sysMenuService;
	@Autowired
	private TradeRoleUserService tradeRoleUserService;
	
	/**
	 * 列表页
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("tradeRoleList")
	public String tradeRoleList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(map);
		List<TradeRole> tradeRoleList = tradeRoleService.getPageList(searchPageUtil);
		searchPageUtil.getPage().setList(tradeRoleList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/traderole/tradeRoleList";
	}
	
	/**
	 * 加载新增页面
	 * @return
	 */
	@RequestMapping("loadAddHtml")
	public String loadAddHtml(){
		return "platform/traderole/add";
	}
	
	/**
	 * 保存新增
	 * @return
	 */
	@RequestMapping("saveAdd")
	@ResponseBody
	public String saveAdd(TradeRole role){
		JSONObject json = tradeRoleService.saveAdd(role);
		return json.toJSONString();
	}
	
	/**
	 * 保存删除
	 * @param role
	 * @return
	 */
	@RequestMapping("saveDelete")
	@ResponseBody
	public String saveDelete(String id){
		JSONObject json = tradeRoleService.saveDelete(id);
		return json.toJSONString();
	}
	
	/**
	 * 切换状态
	 * @param id
	 * @return
	 */
	@RequestMapping("switchStatus")
	@ResponseBody
	public String switchStatus(String id,String status){
		JSONObject json = tradeRoleService.switchStatus(id,status);
		return json.toJSONString();
	}
	
	/**
	 * 加载权限设置页面
	 * @param id
	 * @return
	 */
	@RequestMapping("instalMenu")
	public String instalMenu(String id){
		TradeRole role = tradeRoleService.get(id);
		model.addAttribute("tradeRole", net.sf.json.JSONObject.fromObject(role));
		return "platform/traderole/instalMenu";
	}
	
	/**
	 * 获得公司下的菜单信息
	 * @param tradeId
	 * @return
	 */
	@RequestMapping("loadCompanyMenu")
	@ResponseBody
	public String loadCompanyMenu(String tradeId){
		JSONObject json = new JSONObject();
		// 取得公司下菜单信息
		List<SysMenu> sysMenuList = sysMenuService.getCompanyMenu(null);
		//取得本角色的所有菜单
		List<TradeRoleMenu> oldMenuList = tradeRoleMenuService.getMenuByRoleId(tradeId);
		json.put("sysMenuList", sysMenuList);
		json.put("oldMenuList", oldMenuList);
		return json.toJSONString();
	}
	
	/**
	 * 保存角色菜单关联信息
	 * @param params
	 * @return
	 */
	@RequestMapping("saveRoleMenu")
	@ResponseBody
	public String saveRoleMenu(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			json = tradeRoleMenuService.saveRoleMenu(params);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("flag",false);
			json.put("msg","失败");
		}
		return json.toJSONString();
	}
	
	/**
	 * 加载人员设置角色页面
	 * @param id
	 * @return
	 */
	@RequestMapping("instalRoleUser")
	public String instalRoleUser(String userId){
		model.addAttribute("instalRoleUserId", userId);
		return "platform/traderole/instalRoleUser";
	}
	
	/**
	 * 获得左侧没有的角色数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getLeftNotRole")
	@ResponseBody
	public String getLeftNotRole(String userId){
		List<TradeRole> tradeRoleList = tradeRoleUserService.getLeftNotRole(userId);
		return JSONArray.fromObject(tradeRoleList).toString();
	}
	
	/**
	 * 获得右侧有的角色数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getRightRole")
	@ResponseBody
	public String getRightRole(String userId){
		List<TradeRole> tradeRoleList = tradeRoleUserService.getRightRole(userId);
		return JSONArray.fromObject(tradeRoleList).toString();
	}
	
	/**
	 * 人员添加角色
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("addUserRole")
	@ResponseBody
	public String addUserRole(String userId,String roleIdStr){
		JSONObject json = tradeRoleUserService.addUserRole(userId,roleIdStr);
		return json.toJSONString();
	}
	
	/**
	 * 人员删除角色
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("deleteUserRole")
	@ResponseBody
	public String deleteUserRole(String userId,String roleIdStr){
		JSONObject json = tradeRoleUserService.deleteUserRole(userId,roleIdStr);
		return json.toJSONString();
	}
}
