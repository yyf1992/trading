package com.nuotai.trading.utils.json;

public interface Code {
	/**
     * 执行成功
     */
    int code_success = 40000;

    /**
     * 执行失败
     */
    int code_error = 40010;

    /**
     * 需要设置审批流程
     */
    int code_install_approval = 40011;
    
    /**
     * 对应菜单属性必填
     */
    int code_menu = 40012;
    
    /**
     * 不需要审批流程
     */
    int not_need_approval = 40013;
}
