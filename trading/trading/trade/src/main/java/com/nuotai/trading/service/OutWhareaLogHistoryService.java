package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.OutWhareaLogHistoryMapper;
import com.nuotai.trading.model.OutWhareaLogHistory;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class OutWhareaLogHistoryService {

	@Autowired
	private OutWhareaLogHistoryMapper outWhareaLogHistoryMapper;
	
	public OutWhareaLogHistory get(String id){
		return outWhareaLogHistoryMapper.get(id);
	}
	
	public List<OutWhareaLogHistory> queryList(Map<String, Object> map){
		return outWhareaLogHistoryMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return outWhareaLogHistoryMapper.queryCount(map);
	}
	
	public void add(OutWhareaLogHistory outWhareaLogHistory){
		outWhareaLogHistoryMapper.add(outWhareaLogHistory);
	}
	
	public void update(OutWhareaLogHistory outWhareaLogHistory){
		outWhareaLogHistoryMapper.update(outWhareaLogHistory);
	}
	
	public void delete(String id){
		outWhareaLogHistoryMapper.delete(id);
	}
	
	public List<OutWhareaLogHistory> queryAllList(SearchPageUtil searchPageUtil){
		return outWhareaLogHistoryMapper.queryAllList(searchPageUtil);
	}

}
