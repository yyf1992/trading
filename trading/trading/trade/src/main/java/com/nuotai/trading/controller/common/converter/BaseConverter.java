package com.nuotai.trading.controller.common.converter;

import gui.ava.html.image.generator.HtmlImageGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.poi.EncryptedDocumentException;
import org.openxml4j.exceptions.InvalidFormatException;

import com.nuotai.trading.model.ResQueue;


/**
 * 转换工具接口
 * @ClassName BaseConverter
 * @author huohz
 * @Date 2017年2月8日 上午9:26:23
 * @version 1.0.0
 */
public abstract class BaseConverter {
	final static String path = "D:\\test\\html\\test";
	
	abstract String converter(ResQueue resQueue) throws FileNotFoundException, IOException, ParserConfigurationException, TransformerException, EncryptedDocumentException, InvalidFormatException;
	
	
	/**
	 * html转img
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @author:     huohz
	 * @param htmlUrl
	 * @return
	 */
   public static String imageGeneratorHtml2Img(String htmlUrl){
    	
    	htmlUrl = htmlUrl.replace("/", "\\");
		String fileName = htmlUrl.substring(htmlUrl.lastIndexOf("\\")+1,htmlUrl.lastIndexOf(".")); 
		String resultImgPath = path+"\\"+fileName+".png";
    	
    	 HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
    	 imageGenerator.loadUrl("file:///"+htmlUrl);

//         imageGenerator.getBufferedImage();//java.lang.NegativeArraySizeException
//    	 imageGenerator.setSize(new Dimension(330403,15728));
         imageGenerator.saveAsImage(resultImgPath);

//	         imageGenerator.saveAsHtmlWithMap("hello-world.html", "hello-world.png");
         
         
         
         
         if(new File(resultImgPath).exists()){
        	 return resultImgPath;
         }
		return null;
    	
    }
   
   /**
    * 判断文件夹是否存在，不存在则创建
    * @author:     huohz
    * @param path
    */
	public static void judeDirExistsAndCreater(String path) {
		File filePath = new File(path);
		if (filePath.exists()) {
	         if (!filePath.isDirectory()) {
	        	 filePath.mkdirs();
	         } 
	     } else {
	    	 filePath.mkdirs();
	     }
	}
	
    /**
     * 递归删除目录下的所有文件及子目录下所有文件
     * @param dir 将要删除的文件目录
     * @return boolean Returns "true" if all deletions were successful.
     *                 If a deletion fails, the method stops attempting to
     *                 delete and returns "false".
     */
	public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }
}
