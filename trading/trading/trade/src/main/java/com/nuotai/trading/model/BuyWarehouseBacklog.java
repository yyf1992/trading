package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-11-03 11:01:22
 */
@Data
public class BuyWarehouseBacklog implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司Id
	private String companyId;
	//仓库Id
	private String warehouseId;
	//店铺Id
	private String shopId;
	//店铺Code
	private String shopCode;
	//店铺名称
	private String shopName;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//库存积压预警天数
	private Integer backlogWarning;
	//库存积压天数
	private Integer backlogdaynum;
	//库存积压最长天数
	private Integer backlogmaxday;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//数量
	private Integer number;
	//总库存
	private Integer stocks;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人Id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改人时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
	//截止20160701时间
	private Integer remark;
}
