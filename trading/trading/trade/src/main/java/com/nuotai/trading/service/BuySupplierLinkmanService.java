package com.nuotai.trading.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.BuySupplierLinkmanMapper;
import com.nuotai.trading.model.BuySupplierLinkman;
@Service
public class BuySupplierLinkmanService implements BuySupplierLinkmanMapper {
	@Autowired
	private BuySupplierLinkmanMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuySupplierLinkman record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuySupplierLinkman record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuySupplierLinkman selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuySupplierLinkman record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BuySupplierLinkman record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<BuySupplierLinkman> selectBySupplierId(String supplierId) {
		return mapper.selectBySupplierId(supplierId);
	}

	/**
	 * 查询默认联系人
	 */
	@Override
	public BuySupplierLinkman selectDefaultlinkMan(String supplierId) {
		return mapper.selectDefaultlinkMan(supplierId);
	}

	
}
