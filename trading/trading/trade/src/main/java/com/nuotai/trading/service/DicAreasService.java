package com.nuotai.trading.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.DicAreasMapper;
import com.nuotai.trading.model.DicAreas;

@Service
public class DicAreasService implements DicAreasMapper {
	@Autowired
	private DicAreasMapper mapper;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(DicAreas record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(DicAreas record) {
		return mapper.insertSelective(record);
	}

	@Override
	public DicAreas selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(DicAreas record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DicAreas record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<DicAreas> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	@Override
	public DicAreas getAreaById(String areaId) {
		return mapper.getAreaById(areaId);
	}


}
