package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-09-01 09:23:10
 */
@Data
public class BuyProduct implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司Id
	private String companyId;
	//商品名称
	private String productName;
	//货号
	private String productCode;
	//商品简称
	private String shortName;
	//单位id
	private String unitId;
	//商品类型 0成品1原材料
	private Integer productType;
	//
	private String categoryId;
	//
	private String categoryName;
	//商品季节
	private String season;
	//品牌
	private String brand;
	//年份
	private String year;
	//商品主图
	private String mainPictureUrl;
	//0启用，1禁用
	private String status;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//修改人id
	private String updateId;
	//创建时间
	private Date createDate;
	//修改人姓名
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人Id
	private String delId;
	//删除人名称
	private String delName;
	//删除日期
	private Date delDate;
}
