package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.BuyAttachmentMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.utils.ShiroUtils;


@Service
public class AttachmentService {

	@Autowired
	private BuyAttachmentMapper attachmentMapper;

	public int deleteByPrimaryKey(String id) {
		return attachmentMapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyAttachment record) {
		return attachmentMapper.insert(record);
	}

	public int insertSelective(BuyAttachment record) {
		return attachmentMapper.insertSelective(record);
	}

	public BuyAttachment selectByPrimaryKey(String id) {
		return attachmentMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuyAttachment record) {
		return attachmentMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuyAttachment record) {
		return attachmentMapper.updateByPrimaryKey(record);
	}

	public void deleteByRelatedId(String relatedId) {
		attachmentMapper.deleteByRelatedId(relatedId);
	}

	public List<BuyAttachment> selectByMap(Map<String, Object> map) {
		return attachmentMapper.selectByMap(map);
	}
	
	/**
	 * 附件表添加
	 */
	public void addAttachment(Map<String,Object> map,String companyId){
		// 附件表添加
		BuyAttachment attachment = new BuyAttachment();
		String a1StrPare = map.get("a1Str") == null ? "" : map.get("a1Str").toString();
		if(a1StrPare != null && !a1StrPare.equals("")){
			String[] a1Str = map.get("a1Str").toString().split("@");
			for(int i = 0;i < a1Str.length;i++){
				attachment.setId(ShiroUtils.getUid());
				attachment.setRelatedId(companyId);
				attachment.setUrl(a1Str[i]);
				attachment.setType(1);
				attachmentMapper.insert(attachment);
			}
		}
		String a2StrPare = map.get("a2Str") == null ? "" : map.get("a2Str").toString();
		if(a2StrPare != null && !a2StrPare.equals("")){
			String[] a2Str = map.get("a2Str").toString().split("@");
			for(int i = 0;i < a2Str.length;i++){
				attachment.setId(ShiroUtils.getUid());
				attachment.setRelatedId(companyId);
				attachment.setUrl(a2Str[i]);
				attachment.setType(2);
				attachmentMapper.insert(attachment);
			}
		}
	}

}
