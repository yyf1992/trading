package com.nuotai.trading.service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyProductMapper;
import com.nuotai.trading.dao.BuyProductSkuMapper;
import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyProductService  {
	@Autowired
	private BuyProductMapper mapper;
	@Autowired
	private BuyProductSkuMapper buyProductSkuMapper;
	@Autowired
	private BuyProductSkuService productSkuService;

	
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}


	public int insert(BuyProduct record) {
		return mapper.insert(record);
	}


	public int insertSelective(BuyProduct record) {
		return mapper.insertSelective(record);
	}


	public BuyProduct selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}


	public int updateByPrimaryKeySelective(BuyProduct record) {
		return mapper.updateByPrimaryKeySelective(record);
	}


	public int updateByPrimaryKey(BuyProduct record) {
		return mapper.updateByPrimaryKey(record);
	}


	public List<BuyProduct> selectByPage(SearchPageUtil searchPageUtil) {
		return mapper.selectByPage(searchPageUtil);
	}


	public List<BuyProduct> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	public List<Map<String, Object>> queryByPage(SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> queryByPageList =  mapper.queryByPage(searchPageUtil);
		for (Map<String, Object> map : queryByPageList){
			map.put("productCode", map.get("product_code"));
			List<BuyProductSku>  skuList= productSkuService.queryList(map);
			map.put("skuList", skuList);
		}
		return queryByPageList;
	}

	public JSONObject validateProduct(BuyProduct buyProduct) {
		JSONObject jsonObject=new JSONObject();
		if("".equals(buyProduct.getProductCode())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品货号不能为空！");
			return jsonObject;
		}
		if("".equals(buyProduct.getProductName())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品名称不能为空！");
			return jsonObject;
		}
		if("".equals(buyProduct.getShortName())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品简称不能为空！");
			return jsonObject;
		}
		Map<String, Object> map=new HashMap();
		map.put("productCode", buyProduct.getProductCode());
		map.put("companyId", ShiroUtils.getCompId());
		List<BuyProduct> buyProducts=mapper.selectByMap(map);
		if(buyProducts.size()>0){
			jsonObject.put("success", false);
			jsonObject.put("msg", "该货号已存在！");
			return jsonObject;
		}
		jsonObject.put("success", true);
		jsonObject.put("msg", "");
		return jsonObject;
	}
	public JSONObject insertProduct(BuyProduct buyProduct) {
		// TODO Auto-generated method stub
		JSONObject jsonObject=new JSONObject();
		jsonObject=validateProduct(buyProduct);
		if(!jsonObject.getBooleanValue("success")){
			return jsonObject;
		}
		buyProduct.setId(ShiroUtils.getUid());
		buyProduct.setCreateDate(new Date());
		buyProduct.setCreateId(ShiroUtils.getUserId());
		buyProduct.setCreateName(ShiroUtils.getUserName());
		buyProduct.setCompanyId(ShiroUtils.getCompId());
		buyProduct.setUpdateDate(new Date());
		int result=0;
		result=mapper.insertSelective(buyProduct);
		if(result>0){
			jsonObject.put("success", true);
			jsonObject.put("msg", "新增商品成功！");
		}else{
			jsonObject.put("success", false);
			jsonObject.put("msg", "新增商品失败！");
		}
		return jsonObject;
	}
	
	/**
	 * 修改商品时校验信息
	 * @param buyProduct
	 * @return
	 */
	public JSONObject updateValidateProduct(BuyProduct buyProduct) {
		JSONObject jsonObject=new JSONObject();
		if("".equals(buyProduct.getProductCode())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品货号不能为空！");
			return jsonObject;
		}
		if("".equals(buyProduct.getProductName())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品名称不能为空！");
			return jsonObject;
		}
		if("".equals(buyProduct.getShortName())){
			jsonObject.put("success", false);
			jsonObject.put("msg", "商品简称不能为空！");
			return jsonObject;
		}
		BuyProduct product = mapper.selectByPrimaryKey(buyProduct.getId());
		if (!buyProduct.getProductCode().equals( product.getProductCode())){
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("productCode", buyProduct.getProductCode());
			List<BuyProduct> buyProducts=mapper.selectByMap(map);
			if(buyProducts.size()>0){
				jsonObject.put("success", false);
				jsonObject.put("msg", "该货号已存在,请重新输入！");
				return jsonObject;
			}
		}
		jsonObject.put("success", true);
		jsonObject.put("msg", "");
		return jsonObject;
	}
	
	/**
	 * 修改保存商品
	 * @param buyProduct
	 * @return
	 */
	public JSONObject updateProduct(BuyProduct buyProduct) {
		JSONObject jsonObject=new JSONObject();
		jsonObject=updateValidateProduct(buyProduct);
		if(!jsonObject.getBooleanValue("success")){
			return jsonObject;
		}
		buyProduct.setUpdateId(ShiroUtils.getUid());
		buyProduct.setUpdateDate(new Date());
		buyProduct.setUpdateName(ShiroUtils.getUserName());
		int result=0;
		result=mapper.updateByPrimaryKeySelective(buyProduct);
		Map<String,Object> map =  new HashMap<String, Object>();
		map.put("productCode", buyProduct.getProductCode());
		map.put("productName", buyProduct.getProductName());
		map.put("unitId", buyProduct.getUnitId());
		map.put("updateId", buyProduct.getUnitId());
		map.put("updateName", buyProduct.getUpdateName());
		map.put("updateDate", buyProduct.getUpdateDate());
		result = buyProductSkuMapper.updateByProductCode(map);
		if(result>0){
			jsonObject.put("success", true);
			jsonObject.put("msg", "修改商品成功！");
		}else{
			jsonObject.put("success", false);
			jsonObject.put("msg", "修改商品失败！");
		}
		return jsonObject;
	}
	

	/**
	 * 查询所有的商品
	 */
	public List<Map<String, Object>> selectProductByMap(Map<String, Object> params) {
		
		return mapper.selectProductByMap(params);
	}
	
	public List<Map<String, Object>> selectMore(SearchPageUtil searchPageUtil) {
		
		return mapper.selectMore(searchPageUtil);
	}

}
