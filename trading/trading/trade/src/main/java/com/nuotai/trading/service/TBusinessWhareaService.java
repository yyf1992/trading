package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 
 * @author wangli
 *
 */
@Service
public class TBusinessWhareaService {

    //private static final Logger LOG = LoggerFactory.getLogger(TBusinessWhareaService.class);

	@Autowired
	private TBusinessWhareaMapper tBusinessWhareaMapper;
	
	/**
	 * 根据ID查询仓库
	 * @param id
	 * @return
	 */
	public TBusinessWharea getWharea(String id){
		if (id != null){
			return tBusinessWhareaMapper.getWharea(id);
		}
		return null;
	}
	
	/**
	 * 修改仓库
	 * @param tBusinessWharea
	 */
	public void updateWharea(TBusinessWharea tBusinessWharea){
		
		if (tBusinessWharea != null){
			tBusinessWhareaMapper.updateWharea(tBusinessWharea);
		}
		
	}
	
	/**
	 * 根据Id删除仓库
	 * @param id
	 */
	public void deleteWharea(String id){
		if (id != null){
			tBusinessWhareaMapper.deleteWharea(id);
		}	
	}
	
	/**
	 * 根据Id批量删除仓库
	 * @param id
	 */
	public void deleteBeatchWharea(String ids){
		if (ids != null){
			String[] idStr = ids.split(",");
			for (String id : idStr){
				tBusinessWhareaMapper.deleteWharea(id);
			}
			
		}	
	}
	
	/**
	 * 查询仓库信息
	 * @param map
	 * @return
	 */
	public List<TBusinessWharea> selectWhareaByMap(TBusinessWharea tBusinessWharea,Map<String,Object> map){
		
		if (tBusinessWharea.getWhareaCode() != null && ObjectUtil.isEmpty(tBusinessWharea.getWhareaCode())){
			map.put("whareaCode",tBusinessWharea.getWhareaCode().substring(0,tBusinessWharea.getWhareaCode().indexOf(",")));
		}
		if (tBusinessWharea.getWhareaName() != null && ObjectUtil.isEmpty(tBusinessWharea.getWhareaName())){
			map.put("whareaName",tBusinessWharea.getWhareaName().substring(0,tBusinessWharea.getWhareaName().indexOf(",") ));
		}
		if (tBusinessWharea.getDefectWare() != null){
			map.put("defectWare",tBusinessWharea.getDefectWare());
		}			
		List<TBusinessWharea> tBusinessWhareaList  = tBusinessWhareaMapper.selectWhareaByMap(map);
		
		return tBusinessWhareaList;
		
	}
	
	/**
	 * 添加仓库信息
	 * @param tBusinessWharea
	 */
	public void addWharea(TBusinessWharea tBusinessWharea){
		
		if (tBusinessWharea == null ){
			return;
		}
		

		SysUser sysUser = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		
		tBusinessWharea.setIsdel(Constant.IsDel.NODEL.getValue());
		tBusinessWharea.setId(ShiroUtils.getUid());
		tBusinessWharea.setCreateName(sysUser.getUserName());
		tBusinessWharea.setCreateId(sysUser.getId());
		tBusinessWharea.setCompanyId(ShiroUtils.getCompId());
		Date date = new Date();
		tBusinessWharea.setCreateDate(date);
		
		tBusinessWhareaMapper.addWharea(tBusinessWharea);
		
		
	}
	
	/**
	 * 获得本公司下所有的仓库
	 * @return
	 */
	public List<TBusinessWharea> getCompanyId(){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", Constant.COMPANY_ID);
		params.put("status", "Y");
		return tBusinessWhareaMapper.selectWhareaByMap(params);
	}
	
	/**
	 * 根据code查询仓库
	 * @param code
	 * @return
	 */
	public TBusinessWharea selectByCode(String code){
		return tBusinessWhareaMapper.selectByCode(code);
	}
}