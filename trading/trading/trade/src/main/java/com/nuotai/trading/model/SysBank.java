package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 银行账户表
 * 
 * @author "
 * @date 2017-08-11 09:05:08
 */
@Data
public class SysBank implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司id
	private String companyId;
	//户名
	private String accountName;
	//开户行
	private String openBank;
	//账号
	private String bankAccount;
	//备注
	private String remark;
	//是否删除 0未删除 1已删除
	private Integer isDel;
	//创建时间
	private Date createDate;
	//创建人id
	private String createId;
	//创建人姓名
	private String createName;
	//修改时间
	private Date updateDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//删除时间
	private Date delDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
}
