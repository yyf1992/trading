package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.TradeRoleMenu;

/**
 * 角色与菜单关联表
 * 
 * @author "
 * @date 2017-09-14 09:23:05
 */
public interface TradeRoleMenuMapper extends BaseDao<TradeRoleMenu> {
	int deleteByRoleId(String roleId);

	List<String> getMenuIdByRoleList(Map<String, Object> params);
}
