package com.nuotai.trading.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * session过滤
 * @author Administrator
 *
 */
@Repository
public class SystemInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {

		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");

		// 后台session控制
		String[] noFilters = new String[] { "captcha.jpg", "loadLoginHtml","login","logout","loadLoginHtml","dingDingLogin",
											"loginPlatform","loadOut","404.html","toRegisterPage","checkUserPhone",
											"agreement","subRegister","loadCityByProvinceId","loadAreaByCityId",
											"openQRcode","uploadReceiptAttachment","saveReceiptAttachment",
											"uploadRegAttachment","loadAttachment","loadProvince","common.js"};
		String uri = request.getRequestURI();

		if (!uri.contains("static")&& !uri.contains("client")&& !uri.contains("DDS_")) {
			boolean beFilter = true;
			for (String s : noFilters) {
				if (uri.indexOf(s) != -1) {
					beFilter = false;
					break;
				}
			}
			if (beFilter) {
				Object obj = ShiroUtils.getSession().getAttribute(Constant.CURRENT_USER);
				if (null == obj) {
					// 未登录
					PrintWriter out = response.getWriter();
					StringBuilder builder = new StringBuilder();
					builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
					builder.append("alert(\"页面过期，请重新登录\");");
					builder.append("window.top.location.href=\"");
					builder.append(Constant.BASE_PATH);
					if(uri.contains("admin/")){
						builder.append("/admin/loadLoginHtml\";</script>");
					}else{
						builder.append("/platform/loadOut\";</script>");
					}
					out.print(builder.toString());
					out.close();
					return false;
				}else{
					if(uri.split("trade/").length==1){
						PrintWriter out = response.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(Constant.BASE_PATH);
						builder.append("/platform/loadIndexHtml\";</script>");
						out.print(builder.toString());
						out.close();
						return false;
					}
					if(uri.contains("admin/")){
						SysUser user = (SysUser)obj;
						if(!"admin".equals(user.getLoginName())){
							// 未登录
							PrintWriter out = response.getWriter();
							StringBuilder builder = new StringBuilder();
							builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
							builder.append("alert(\"页面过期，请重新登录\");");
							builder.append("window.top.location.href=\"");
							builder.append(Constant.BASE_PATH);
							builder.append("/admin/loadLoginHtml\";</script>");
							out.print(builder.toString());
							out.close();
							return false;
						}
					}
				}
			}
		}
		return super.preHandle(request, response, handler);
	}
}
