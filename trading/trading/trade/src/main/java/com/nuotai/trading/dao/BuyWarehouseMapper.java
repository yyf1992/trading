package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyWarehouse;

public interface BuyWarehouseMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyWarehouse record);

    int insertSelective(BuyWarehouse record);

    BuyWarehouse selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyWarehouse record);

    int updateByPrimaryKeyWithBLOBs(BuyWarehouse record);

    int updateByPrimaryKey(BuyWarehouse record);
    
    List<BuyWarehouse> selectByMap(Map<String, Object> map);
    
    BuyWarehouse selectByCode(String code);
}