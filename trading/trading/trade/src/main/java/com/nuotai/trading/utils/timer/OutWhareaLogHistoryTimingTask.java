package com.nuotai.trading.utils.timer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.dao.OutWhareaLogHistoryMapper;
import com.nuotai.trading.dao.OutWhareaLogMapper;
import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.model.OutWhareaLogHistory;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 外仓导入数据历史记录定时任务
 * @author wl
 *
 */
@Component
public class OutWhareaLogHistoryTimingTask {
	
	private static final Logger LOG = LoggerFactory.getLogger(OutWhareaLogHistoryTimingTask.class);
	
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private OutWhareaLogHistoryMapper outWhareaLogHistoryMapper;
	@Autowired
	private OutWhareaLogMapper outWhareaLogMapper;
	
	@Scheduled(cron="0 0 23 * * ?")
	//0 */1 * * * ?
	//0 0 23 * * ?
	public void catchBuyWarehouseoutWhareaLogHistory(){
		TimeTask timeTask = timeTaskService.selectByCode("OUTWHAREALOG_HISTORY");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					//外仓导入数据历史记录
					outWhareaLogHistory();
			
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					timeTask.setCurrentStatus("2");//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	/**
	 * 外仓导入数据历史记录
	 */
	public void outWhareaLogHistory(){
		Map<String,Object> map = new HashMap<String, Object>();
		Calendar cal = Calendar.getInstance();
		String yesterday=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		map.put("minDate", yesterday);
		map.put("maxDate", yesterday);
		//获取外仓导入的数据
		 List<OutWhareaLog>  outWhareaLogList = outWhareaLogMapper.selectAll(map);
		 LOG.debug("********外仓导入数据历史记录同步开始**********");
		Date date = new Date();
		if (outWhareaLogList != null && outWhareaLogList.size()>0){
			for (OutWhareaLog outWhareaLog : outWhareaLogList){
				OutWhareaLogHistory outWhareaLogHistory = new OutWhareaLogHistory();
				outWhareaLogHistory.setHistoryId(outWhareaLog.getId());
				outWhareaLogHistory.setId(ShiroUtils.getUid());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				
				Date historyDate = null;
				try {
					historyDate = sdf.parse(sdf.format(date));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				outWhareaLogHistory.setHistoryDate(historyDate);
				outWhareaLogHistory.setCompanyId(outWhareaLog.getCompanyId());
				outWhareaLogHistory.setSubcompanyName(outWhareaLog.getSubcompanyName());
				outWhareaLogHistory.setWhareaName(outWhareaLog.getWhareaName());
				outWhareaLogHistory.setShopName(outWhareaLog.getShopName());			
				outWhareaLogHistory.setProductCode(outWhareaLog.getProductCode());
				outWhareaLogHistory.setBarcode(outWhareaLog.getBarcode());
				outWhareaLogHistory.setPrice(outWhareaLog.getPrice());
				outWhareaLogHistory.setOutWhareaStock(outWhareaLog.getOutWhareaStock());
				outWhareaLogHistory.setOutWhareaWayStock(outWhareaLog.getOutWhareaWayStock());
				outWhareaLogHistory.setOutWhareaTotalPrice(outWhareaLog.getOutWhareaTotalPrice());
				outWhareaLogHistory.setCreateId(outWhareaLog.getCreateId());
				outWhareaLogHistory.setCreateName(outWhareaLog.getCreateName());
				outWhareaLogHistory.setCreateDate(outWhareaLog.getCreateDate());
				outWhareaLogHistory.setUpdateId(outWhareaLog.getUpdateId());
				outWhareaLogHistory.setUpdateName(outWhareaLog.getUpdateName());
				outWhareaLogHistory.setUpdateDate(outWhareaLog.getUpdateDate());
				outWhareaLogHistory.setDelId(outWhareaLog.getDelId());
				outWhareaLogHistory.setDelName(outWhareaLog.getDelName());
				outWhareaLogHistory.setDelDate(outWhareaLog.getDelDate());

				//添加外仓导入数据到历史记录表	
				outWhareaLogHistoryMapper.add(outWhareaLogHistory);
	
			}
		}
		LOG.debug("********外仓导入数据历史记录结束**********");
	}
}
