package com.nuotai.trading.dao;

import java.util.Map;

import com.nuotai.trading.model.SysShopUser;

/**
 * 用户与店铺关联表
 * 
 * @author "
 * @date 2018-03-21 15:29:56
 */
public interface SysShopUserMapper extends BaseDao<SysShopUser> {
	
	int deleteByShopCodeUserId(Map<String, Object> params);
	
}
