package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
@Data
public class SysMenu implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String parentId;

    private String name;

    private Integer isMenu;

    private String sortOrder;

    private String url;

    private String button;

    private String icon;

    private Integer status;

    private Date createTime;

    private Integer level;

    private String position;
    
    private List<SysMenu> childrenMenuList;

}