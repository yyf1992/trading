package com.nuotai.trading.model;

import java.io.Serializable;

import lombok.Data;


/**
 * 
 * 审批流程设置表
 * @author "
 * @date 2017-08-21 11:48:57
 */
@Data
public class SysVerifyPocess implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//主表id
	private String headerId;
	//审批人员Id
	private String userId;
	//审批人员姓名
	private String userName;
	//审批序列
	private Integer verifyIndex;
}
