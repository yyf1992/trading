package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.SysShopMapper;
import com.nuotai.trading.dao.SysShopUserMapper;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.model.SysShopUser;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
@Transactional
public class SysShopService {

	private static final Logger LOG = LoggerFactory
			.getLogger(SysShopService.class);

	@Autowired
	private SysShopMapper sysShopMapper;
	@Autowired
	private SysShopUserMapper sysShopUserMapper;

	public SysShop get(String id) {
		return sysShopMapper.get(id);
	}

	public List<SysShop> queryList(Map<String, Object> map) {
		return sysShopMapper.queryList(map);
	}

	public int queryCount(Map<String, Object> map) {
		return sysShopMapper.queryCount(map);
	}

	public void add(SysShop sysShop) {
		sysShopMapper.add(sysShop);
	}

	public void update(SysShop sysShop) {
		sysShopMapper.update(sysShop);
	}

	public void delete(String id) {
		sysShopMapper.delete(id);
	}

	public List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil) {
		return sysShopMapper.selectByPage(searchPageUtil);
	}

	public SysShop selectByPrimaryKey(String id) {
		return sysShopMapper.selectByPrimaryKey(id);
	}

	public List selectByCompanyId(String id) {
		return sysShopMapper.selectByCompanyId(id);
	}

	public int insert(SysShop sysShop) {
		return sysShopMapper.insert(sysShop);
	}

	public JSONObject validateShop(SysShop sysShop) {
		JSONObject json = new JSONObject();
		sysShop.setCompanyId(ShiroUtils.getCompId());
		if (sysShop.getShopCode() == null || sysShop.getShopCode().equals("")) {
			json.put("success", false);
			json.put("msg", "店铺代码不能为空！");
			return json;
		}
		if (sysShop.getShopName() == null || sysShop.getShopName().equals("")) {
			json.put("success", false);
			json.put("msg", "店铺名称不能为空！");
			return json;
		}

		SysShop shopCode = sysShopMapper.selectByShopCode(sysShop);
		if (shopCode != null) {
			json.put("success", false);
			json.put("msg", "店铺代码已存在！");
			return json;
		}

		SysShop shopName = sysShopMapper.selectByShopName(sysShop);
		if (shopName != null) {
			json.put("success", false);
			json.put("msg", "店铺名称已存在！");
			return json;
		}

		json.put("success", true);
		json.put("msg", "");
		return json;
	}

	public JSONObject saveInsertShop(SysShop sysShop) {
		JSONObject json = validateShop(sysShop);
		if (!json.getBooleanValue("success")) {
			return json;
		}
		if (sysShop.getPlatformId().equals("-1")) {
			json.put("success", false);
			json.put("msg", "请选择平台名称！");
			return json;
		}
		int result = 0;
		Date date = new Date();
		sysShop.setId(ShiroUtils.getUid());
		sysShop.setCompanyId(ShiroUtils.getCompId());
		sysShop.setCreateDate(date);
		sysShop.setCreateId(ShiroUtils.getUserId());
		sysShop.setCreateName(ShiroUtils.getUserName());
		sysShop.setUpdateDate(new Date());
		sysShop.setUpdateId(ShiroUtils.getUserId());
		sysShop.setUpdateName(ShiroUtils.getUserName());
		result = sysShopMapper.insert(sysShop);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "添加成功!");
		} else {
			json.put("success", false);
			json.put("msg", "添加失败!");
		}
		return json;
	}

	public JSONObject saveEditShop(SysShop sysShop) {
		// TODO Auto-generated method stub
		JSONObject json = validateShop(sysShop);
		if (!json.getBooleanValue("success")) {
			return json;
		}
		int result = 0;
		Date date = new Date();
		sysShop.setUpdateDate(date);
		sysShop.setUpdateId(ShiroUtils.getUserId());
		sysShop.setUpdateName(ShiroUtils.getUserName());
		result = sysShopMapper.update(sysShop);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "修改成功!");
		} else {
			json.put("success", false);
			json.put("msg", "修改失败!");
		}
		return json;
	}

	public List<SysShop> selectByMap(Map<String,Object> map) {
		return sysShopMapper.selectByMap(map);
	}
	
	/**
	 * 获得左侧没有的店铺数据
	 * @param model
	 * @param userId
	 */
	public List<SysShop> getLeftNotRole(String userId,String shopName) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("notUserId", userId);
		params.put("shopName", shopName.trim());
		//获得没有的店铺
		List<SysShop> shopList = sysShopMapper.getShopByMap(params);
		return shopList;
	}
	
	/**
	 * 获得右侧有的店铺数据
	 * @param model
	 * @param userId
	 */
	public List<SysShop> getRightRole(String userId,String shopName) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("userId", userId);
		params.put("shopName", shopName.trim());
		//获得已有的店铺
		List<SysShop> shopList = sysShopMapper.getShopByMap(params);
		return shopList;
	}

	/**
	 * 人员设置店铺
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject addUserRole(String userId, String shopCodeStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(shopCodeStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "店铺信息不能为空");
			return json;
		}
		String[] shopCodeArray = shopCodeStr.split(",");
		for(String shopCode : shopCodeArray){
			SysShopUser sysShopUser = new SysShopUser();
			sysShopUser.setId(ShiroUtils.getUid());
			sysShopUser.setCompanyId(ShiroUtils.getCompId());
			sysShopUser.setShopCode(shopCode);
			sysShopUser.setUserId(userId);
			sysShopUser.setCreateTime(new Date());
			sysShopUserMapper.add(sysShopUser);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}
	
	/**
	 * 人员删除店铺
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject deleteUserShop(String userId, String shopCodeStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(shopCodeStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "店铺信息不能为空");
			return json;
		}
		String[] shopCodeArray = shopCodeStr.split(",");
		for(String shopCode : shopCodeArray){
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("shopCode", shopCode);
			params.put("userId", userId);
			sysShopUserMapper.deleteByShopCodeUserId(params);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}

}
