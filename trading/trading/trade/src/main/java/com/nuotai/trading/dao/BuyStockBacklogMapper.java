package com.nuotai.trading.dao;

import com.nuotai.trading.model.BuyStockBacklog;

/**
 * 
 * 
 * @author "
 * @date 2017-12-19 13:30:49
 */
public interface BuyStockBacklogMapper extends BaseDao<BuyStockBacklog> {
	
	int selectById(String barcode);
}
