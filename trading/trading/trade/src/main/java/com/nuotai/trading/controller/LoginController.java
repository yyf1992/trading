package com.nuotai.trading.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.service.TradeRoleUserService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.R;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
/**
 * 登录控制器
 * @author wxx
 *
 */
@Controller
public class LoginController extends BaseController {
    @Autowired
    private SysMenuService sysMenuService;
	@Autowired
	private TradeVerifyHeaderService tradeVerifyHeaderService;
	@Autowired
	private TradeRoleUserService  roleUserService;
	@Autowired
    private SysUserService sysUserService;
	
    /**
     * 加载登录页面
     * @return
     */
    @RequestMapping("platform/loadLoginHtml")
    public String loginJsp() {
        return "platform/login";
    }

    /**
     * 登录方法
     */
    @RequestMapping(value = "platform/loginPlatform", method = RequestMethod.POST)
    @ResponseBody
    public R submit(SysUser user) throws Exception {
    	try{
			Subject subject = ShiroUtils.getSubject();
			//md5加密
			String password = ShiroUtils.getMD5(user.getPassword(), "UTF-8", 0).toLowerCase();
			UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), password);
			subject.login(token);
			setSysMenuSession();
		}catch (UnknownAccountException e) {
			return R.error(e.getMessage());
		}catch (IncorrectCredentialsException e) {
			return R.error(e.getMessage());
		}catch (LockedAccountException e) {
			return R.error(e.getMessage());
		}catch (AuthenticationException e) {
			return R.error("账户验证失败");
		}
		return R.ok();
    }

    /**
     * 首页
     */
    @RequestMapping("platform/loadIndexHtml")
    public String loadIndex(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		/*map.put("companyId", ShiroUtils.getCompId());
		map.put("userId", ShiroUtils.getUserId());
		String[] statusArr = {"0"};
		map.put("statusArr", statusArr);//待审批
		searchPageUtil.setObject(map);
		List<Map<String,Object>> verifyList = tradeVerifyHeaderService.getWaitForMeList(searchPageUtil);
		searchPageUtil.getPage().setList(verifyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("user",ShiroUtils.getUserEntity());
		model.addAttribute("userRole",roleUserService.selectRoleCode(map));*/
		String returnUrl = "newpage/index";
		//BuyCompany company = (BuyCompany)ShiroUtils.getSessionAttribute("company");
		//returnUrl = "platform/platformIndex";
        return returnUrl;
    }
    
    /**
     * 操作指南
     */
    @RequestMapping("platform/loadOperationGuide")
    public String loadOperationGuide(){
        return "platform/operationGuide";
    }
    
    /**
     * 退出
     * @return
     */
    @RequestMapping("platform/loadOut")
    public String loadOut(){
    	ShiroUtils.logout();
		return "redirect:login.html";
    }
    
    public void setSysMenuSession(){
    	List<SysMenu> parentMenu = new ArrayList<>();
    	Map<String,List<SysMenu>> menuMap = new HashMap<>();
    	List<SysMenu> menuList = sysMenuService.tradeLogin(null,null);
    	if(!ObjectUtil.isEmpty(menuList)){
    		for(SysMenu menu : menuList){
    			if(!ObjectUtil.isEmpty(menu.getParentId())){
    				//有父
    				String parentId = menu.getParentId();
    				List<SysMenu> childMenuList = new ArrayList<>();
    				if(menuMap.containsKey(parentId)){
    					childMenuList = menuMap.get(parentId);
    				}
    				childMenuList.add(menu);
    				menuMap.put(menu.getParentId(), childMenuList);
    			}else{
    				parentMenu.add(menu);
    			}
    		}
    	}
    	if(!ObjectUtil.isEmpty(parentMenu)){
    		for(SysMenu menu : menuList){
    			setChildMenu(menu,menuMap);
    		}
		}
    	/*String position = Constant.MenuPosition.TOP.getValue();
		List<SysMenu> parentMenu = sysMenuService.tradeLogin(position,null);
		String position2 = Constant.MenuPosition.SYSTEM.getValue();
		List<SysMenu> parentMenu2 = sysMenuService.tradeLogin(position2,null);
		parentMenu.addAll(parentMenu2);
		*/
		ShiroUtils.setSessionAttribute(Constant.AUTHORITYSYSMENU, JSONArray.fromObject(parentMenu));
    }
    
    public void setChildMenu(SysMenu menu,Map<String, List<SysMenu>> menuMap){
    	if(!ObjectUtil.isEmpty(menuMap)){
    		List<SysMenu> childMenuList = menuMap.get(menu.getId());
    		if(!ObjectUtil.isEmpty(childMenuList)){
    			for(SysMenu childMenu : childMenuList){
        			setChildMenu(childMenu,menuMap);
        		}
    			menu.setChildrenMenuList(childMenuList);
    		}
    	}
    }
    
    /**
	 * 递归取得菜单信息
	 * @param parentMenu
	 */
	public void getSysMenu(List<SysMenu> parentMenu,Map<String, Object> map){
		if (parentMenu != null && parentMenu.size() > 0) {
			for (SysMenu menu : parentMenu) {
				map.put("position", Constant.MenuPosition.LEFT.getValue());
				map.put("parent_id", menu.getId());
				map.put("is_menu", Constant.MenuType.MENU.getValue());
				// 取得二级菜单
				List<SysMenu> childMenu = sysMenuService.getMenuByMap(map);
				if(childMenu != null && childMenu.size() > 0){
					getSysMenu(childMenu,map);
				}
				menu.setChildrenMenuList(childMenu);
			}
		}
	}
	
	/**
	 * 钉钉登录
	 * 
	 * @param login_name
	 * @param userid
	 * @param username
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "platform/dingDingLogin", method = RequestMethod.GET)
	@ResponseBody
	public String dingDingLogin(@RequestParam Map<String, Object> map)
			throws Exception {
		JSONObject json = new JSONObject();
		// 判断用户是否存在
		SysUser user = sysUserService.queryByLoginName(map.get("login_name").toString());
		if (user == null || "".equals(user.getId())) {
			// 不存在用户自动创建
			user.setId(ShiroUtils.getUid());
			user.setCompanyId("17060909542281121440");
			user.setUserName(map.get("username").toString());
			user.setPassword(ShiroUtils.getMD5("123456", "UTF-8", 0).toLowerCase());
			user.setLoginName(map.get("login_name").toString());
			user.setIsDel(0);
			user.setDingdingId(map.get("userid").toString());
			user.setCreateDate(new Date());
			user.setCreateId("admin");
			user.setCreateName("admin");
			user.setId(ShiroUtils.getUid());
			user.setUpdateDate(new Date());
			user.setUpdateId("admin");
			user.setUpdateName("admin");
			sysUserService.insertSelective(user);
		} else {
			if (user.getDingdingId() == null || "".equals(user.getDingdingId())) {
				user.setDingdingId(map.get("userid").toString());
				sysUserService.updateByPrimaryKeySelective(user);
			}
		}
		Subject subject = ShiroUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getPassword());
		subject.login(token);
		setSysMenuSession();
		json.put("success", true);
		json.put("msg", null);
		json.put("state", 0);
		return json.toString();
	}
	
	/**
	 * 加载内容框架
	 * @param map
	 * @return
	 */
    @RequestMapping("platform/iframeIndex")
    public String iframeIndex(@RequestParam Map<String, Object> paramsMap) {
    	model.addAttribute("params", paramsMap);
        return "newpage/iframeIndex";
    }
}
