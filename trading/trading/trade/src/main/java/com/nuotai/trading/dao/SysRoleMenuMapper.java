package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.SysRoleMenu;

public interface SysRoleMenuMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRoleMenu record);

    int insertSelective(SysRoleMenu record);

    SysRoleMenu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoleMenu record);

    int updateByPrimaryKey(SysRoleMenu record);

	int deleteByRoleId(@Param("idList")String[] idList);

	int deleteByMenuId(@Param("idList")String[] idList);

	List<String> getMenuByRoleId(@Param("roleId")String roleId);
}