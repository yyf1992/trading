package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-12-19 13:30:54
 */
@Data
public class BuyStockBacklogDetails implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//批次编号
	private String batchNo;
	//订单号
	private String orderId;
	//订单类型
	private String businessType;
	//下单时间
	private Date orderDate;
	//公司Id
	private String companyId;
	//仓库Id
	private String warehouseId;
	//店铺Id
	private String shopId;
	//店铺Code
	private String shopCode;
	//店铺名称
	private String shopName;
	//出入库时间
	private Date storageDate;
	//数量
	private Integer number;
	//积压库存
	private Integer remainingStock;
	//库零天数
	private Integer backlogdaynum;
	//库存积压最长天数
	private Integer backlogmaxday;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人Id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
}
