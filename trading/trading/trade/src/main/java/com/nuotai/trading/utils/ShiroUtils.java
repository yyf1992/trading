package com.nuotai.trading.utils;

import java.security.Key;
import java.security.MessageDigest;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.nuotai.trading.model.SysUser;


/**
 * Shiro工具类
 * 
 * @author liujie
 */
public class ShiroUtils {
	
	/**
	 * 获取订单号
	 * @return
	 */
	public static String getOrderNum(){
		String orderNum = "CG"+DateUtils.format(new Date(),"yyyyMMddHHmmss");
		orderNum += getRandom(3);
		return orderNum;
	}
	/**
	 * 获取售后号
	 * @return
	 */
	public static String getCustomerNum(){
		String customerNum = "SH"+DateUtils.format(new Date(),"yyyyMMddHHmmss");
		customerNum += getRandom(3);
		return customerNum;
	}
	public static void main(String[] args) {
		System.out.println(getOrderNum());
	}
	public static Session getSession() {
		return SecurityUtils.getSubject().getSession();
	}

	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}

	public static SysUser getUserEntity() {
		return (SysUser)SecurityUtils.getSubject().getPrincipal();
	}
	
	public static String getUserName() {
		return getUserEntity().getUserName();
	}
	public static String getCompanyName() {
		return getUserEntity().getCompanyName();
	}

	public static String getUserId() {
		return getUserEntity().getId();
	}
	public static String getCompId() {
		return getUserEntity().getCompanyId();
	}
	
	public static void setSessionAttribute(Object key, Object value) {
		getSession().setAttribute(key, value);
	}

	public static Object getSessionAttribute(Object key) {
		return getSession().getAttribute(key);
	}

	public static boolean isLogin() {
		return SecurityUtils.getSubject().getPrincipal() != null;
	}

	public static void logout() {
		SecurityUtils.getSubject().logout();
	}
	
	public static String getKaptcha(String key) {
		String kaptcha = getSessionAttribute(key).toString();
		getSession().removeAttribute(key);
		return kaptcha;
	}
	
	/**
	 * MD5加密方法
	 * @param str
	 * @param encoding
	 * @param no_Lower_Upper
	 * @return
	 */
	public static String getMD5(String str, String encoding, int no_Lower_Upper) {
		if (null == encoding)
			encoding = "utf-8";
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(str.getBytes(encoding));
			for (int i = 0; i < array.length; i++) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.toUpperCase().substring(1, 3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (no_Lower_Upper == 0) {
			return sb.toString();
		}
		if (no_Lower_Upper == 1) {
			return sb.toString().toLowerCase();
		}
		if (no_Lower_Upper == 2) {
			return sb.toString().toUpperCase();
		}
		return null;
	}
	/**
	 * 生成密钥
	 * <br>
	 * <b>功能：</b>DES<br>
	 *
	 * @param arrBTmp
	 * @return
	 * @throws Exception
	 */
	private Key getKey(byte[] arrBTmp) throws Exception {
		byte[] arrB = new byte[8];// 创建一个空的8位字节数组（默认值为0）
		for (int i = 0; i < arrBTmp.length && i < arrB.length; i++) { // 将原始字节数组转换为8位
			arrB[i] = arrBTmp[i];
		}
		Key key = new javax.crypto.spec.SecretKeySpec(arrB, "DES");// 生成密钥
		return key;
	}

	/**
	 *
	 * <br>
	 * <b>功能：</b>DES<br>
	 *
	 * @param strIn
	 * @return
	 * @throws Exception
	 */
	private static byte[] hexStr2ByteArr(String strIn) throws Exception {
		byte[] arrB = strIn.getBytes();
		int iLen = arrB.length;

		// 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
		byte[] arrOut = new byte[iLen / 2];
		for (int i = 0; i < iLen; i = i + 2) {
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}
		return arrOut;
	}

	/**
	 *
	 * <br>
	 * <b>功能：</b>DES<br>
	 *
	 * @param arrB
	 * @return
	 * @throws Exception
	 */
	private static String byteArr2HexStr(byte[] arrB) throws Exception {
		int iLen = arrB.length;
		// 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
		StringBuffer sb = new StringBuffer(iLen * 2);
		for (int i = 0; i < iLen; i++) {
			int intTmp = arrB[i];
			// 把负数转换为正数
			while (intTmp < 0) {
				intTmp = intTmp + 256;
			}
			// 小于0F的数需要在前面补0
			if (intTmp < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(intTmp, 16));
		}
		return sb.toString();
	}

	/**
	 *
	 * <br>
	 * <b>功能：</b>DES方法 0为加密,1为解密<br>
	 *
	 * @param deskey
	 *            密钥
	 * @param str
	 *            内容
	 * @param type
	 *            0为加密,1为解密
	 * @return
	 */
	public String getDES(String deskey, String str, int type) {
		Cipher encryptCipher = null;
		Cipher decryptCipher = null;
		Security.addProvider(new com.sun.crypto.provider.SunJCE());
		try {
			Key key = getKey(deskey.getBytes());
			encryptCipher = Cipher.getInstance("DES");
			encryptCipher.init(Cipher.ENCRYPT_MODE, key);
			decryptCipher = Cipher.getInstance("DES");
			decryptCipher.init(Cipher.DECRYPT_MODE, key);
			if (type == 0) { // 0为加密
				return byteArr2HexStr(encryptCipher.doFinal(str.getBytes()));
			} else {
				return new String(decryptCipher.doFinal(hexStr2ByteArr(str)));
			}
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获得主键
	 * @return
	 */
	public static String getUid() {
		return new SimpleDateFormat("yyMMddHHmmss").format(new Date())
				+ getRandom(8);
	}
	
	/*********************************************************************************/
	/**
	 * 
	 * <br>
	 * <b>功能：</b>静态方法<br>
	 * 
	 * @return
	 */
	public static ShiroUtils getInstance() {
		return new ShiroUtils();
	}
	
	/**
	 * 
	 * <br>
	 * <b>功能：</b>获取随机数从1开始,格式10000000-99999999<br>
	 * 
	 * @param number
	 *            1
	 * @return
	 */
	public static int getRandom(int number) {
		int max = 9;
		int min = 1;
		for (int i = 1; i < number; i++) {
			min = min * 10;
			max = max * 10 + 9;
		}
		return getRandom(min, max);
	}
	
	public static int getRandom(int min, int max) {
		// int a = (int) (Math.random() * (44) + 23); //产生的[23,67)的随机数
		return (int) (Math.random() * (max - min) + min);
	}
	
	/**
	 * 获得request
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(); 
		return request;
	}
	
	//获取文件大小
	public static String convertFileSize(long size) {
		long kb = 1024;
		long mb = kb * 1024;
		long gb = mb * 1024;

		if (size >= gb) {
			return String.format("%.1f GB", (float) size / gb);
		} else if (size >= mb) {
			float f = (float) size / mb;
			return String.format(f > 100 ?"%.0f MB":"%.1f MB", f);
		} else if (size >= kb) {
			float f = (float) size / kb;
			return String.format(f > 100 ?"%.0f KB":"%.1f KB", f);
		} else
			return String.format("%d B", size);
	}
	
	/**
	 * 获取采购发货单号
	 * @return String
	 */
	public static String getDeliveryNum(){
		String deliveryNum = "FH"+DateUtils.format(new Date(),"yyyyMMddHHmmss");
		deliveryNum += getRandom(3);
		return deliveryNum;
	}
	
	/**
	 * 获取换货发货单号
	 * @return
	 */
	public static String getHuanHuoDeliveryNum(){
		String deliveryNum = "CPFH"+DateUtils.format(new Date(),"yyyyMMddHHmmss");
		deliveryNum += getRandom(3);
		return deliveryNum;
	}
	
	/**
	 * 代发货单号
	 * @return
	 */
	public static String getDaiDeliveryNum() {
		String deliveryNum = "DFH"+DateUtils.format(new Date(),"yyyyMMddHHmmss");
		deliveryNum += getRandom(3);
		return deliveryNum;
	}
	
	public static <T> T getBean(Class<T> clazz) {
		ApplicationContext ac2 = WebApplicationContextUtils.getWebApplicationContext(ShiroUtils.getRequest().getServletContext());
		return ac2.getBean(clazz);
	}

	/**
	 * 获取登录用户的IP地址
	 *
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip.equals("0:0:0:0:0:0:0:1")) {
			ip = "本地";
		}
		return ip;
	}
	
	/**
	 * 批次号
	 * @return
	 */
	public static String RandomBatchNum(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String format = dateFormat.format(new Date());
         int max=24;
         int min=3;
         Random random = new Random();
          int s = random.nextInt(max)%(max-min+1) + min;
          StringBuffer buffer =new StringBuffer();
          for(int i=0;i<s;i++){
              Integer val = (int)(Math.random()*9+1);
              buffer.append(val.toString());
          }
        return format+buffer.toString();
    }
	
	/**
	 * 返回时回填搜索条件
	 * @return
	 */
	public static String returnSearchCondition(){
		Object o = ShiroUtils.getSessionAttribute("form");
		if(!ObjectUtil.isEmpty(o)){
			String[] s1 = o.toString().split(",");
			String[] s2 = s1[0].split("form=",2);
			String condition = s2[1].replace("}", "");
			return condition;
		}else{
			return null;
		}
    }
}
