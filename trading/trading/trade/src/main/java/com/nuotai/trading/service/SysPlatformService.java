package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.SysPlatformMapper;
import com.nuotai.trading.model.SysPlatform;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class SysPlatformService {

    private static final Logger LOG = LoggerFactory.getLogger(SysPlatformService.class);

	@Autowired
	private SysPlatformMapper sysPlatformMapper;
	
	public SysPlatform get(String id){
		return sysPlatformMapper.get(id);
	}
	
	public List<SysPlatform> queryList(Map<String, Object> map){
		return sysPlatformMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysPlatformMapper.queryCount(map);
	}
	
	public JSONObject saveAdd(SysPlatform sysPlatform){
		JSONObject json = new JSONObject();
		json=validateSysPlatform(sysPlatform);
		if(!json.getBooleanValue("success")){
			return json;
		}
		int result=0;
		Date date=new Date();
		sysPlatform.setId(ShiroUtils.getUid());
		sysPlatform.setCreateDate(date);
		sysPlatform.setCreateId(ShiroUtils.getUserId());
		sysPlatform.setCreateName(ShiroUtils.getUserName());
		sysPlatform.setCompanyId(ShiroUtils.getCompId());
		sysPlatform.setUpdateDate(new Date());
		sysPlatform.setUpdateId(ShiroUtils.getUserId());
		sysPlatform.setUpdateName(ShiroUtils.getUserName());
		result=sysPlatformMapper.insert(sysPlatform);
		if(result>0){
			json.put("success", true);
			json.put("msg", "添加成功！");
		}else{
			json.put("success", false);
			json.put("msg", "添加失败！");	
		}
		return json;
	}
	
	public int update(SysPlatform sysPlatform){
		return sysPlatformMapper.update(sysPlatform);
	}
	
	public JSONObject saveUpdate(SysPlatform sysPlatform){
		JSONObject json = validateSysPlatform(sysPlatform);
		int result=0;
		if(!json.getBooleanValue("success")){
			return json;
		}
		Date date = new Date();
		sysPlatform.setUpdateDate(date);
		sysPlatform.setUpdateId(ShiroUtils.getUserId());
		sysPlatform.setUpdateName(ShiroUtils.getUserName());
		result = sysPlatformMapper.updateByPrimaryKey(sysPlatform);
		if(result>0){
			json.put("success", true);
			json.put("msg", "修改成功！");
		}else{
			json.put("success", false);
			json.put("msg", "修改失败！");	
		}
		return json;
	}
	
	
	public JSONObject delete(SysPlatform sysPlatform){
		JSONObject json = validateSysPlatform(sysPlatform);
		sysPlatform.setIsDel(1);
		sysPlatform.setDelDate(new Date());
		sysPlatform.setDelId(ShiroUtils.getUserId());
		sysPlatform.setDelName(ShiroUtils.getUserName());
		int result=sysPlatformMapper.updateByPrimaryKey(sysPlatform);
		if(result>0){
			json.put("success", true);
			json.put("msg", "删除成功！");
		}else {
			json.put("success", false);
			json.put("msg", "删除失败！");
		}
		return json;
	}
	
	public JSONObject validateSysPlatform(SysPlatform sysPlatform){
		JSONObject json = new JSONObject();
		sysPlatform.setCompanyId(ShiroUtils.getCompId());
		if(sysPlatform.getPlatformCode()==null||sysPlatform.getPlatformCode().equals("")){
			json.put("success", false);
			json.put("msg", "平台代码不能为空！");
			return json;
		}

		if(sysPlatform.getPlatformName()==null||sysPlatform.getPlatformName().equals("")){
			json.put("success", false);
			json.put("msg", "平台名称不能为空！");
			return json;
		}
		
		SysPlatform isExistCode=sysPlatformMapper.selectByPlatformCode(sysPlatform);
		if(isExistCode != null){
			json.put("success", false);
			json.put("msg", "平台代码已存在！");
			return json;
		}
		
		SysPlatform isExistName=sysPlatformMapper.selectByPlatformName(sysPlatform);
		if(isExistName != null){
			json.put("success", false);
			json.put("msg", "平台名称已存在！");
			return json;
		}
		
		json.put("success", true);
		json.put("msg", "");
		return json;
	}
	
	public List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil){
		return sysPlatformMapper.selectByPage(searchPageUtil);
	}

	public int updateByPrimaryKey(SysPlatform sysPlatform) {
		// TODO Auto-generated method stub
		return sysPlatformMapper.updateByPrimaryKey(sysPlatform);
	}

	public SysPlatform selectByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		return sysPlatformMapper.selectByPrimaryKey(id);
	}

	public List<SysPlatform> selectByCompanyId(String compId) {
		// TODO Auto-generated method stub
		return sysPlatformMapper.selectByCompanyId(compId);
	}
}
