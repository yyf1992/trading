package com.nuotai.trading.dao;


import com.nuotai.trading.model.wms.WmsInputplanLog;

/**
 * 
 * 
 * @author liuhui
 * @date 2017-11-20 09:59:37
 */
public interface WmsInputplanLogMapper extends BaseDao<WmsInputplanLog> {

    void deleteByDeliverNo(String deliverNo);

    WmsInputplanLog getByDeliverNo(String deliverNo);
}
