package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-02-10 16:18:34
 */
@Data
public class BuySupplierFriend implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//供应商id
	private String supplierId;
	//买家Id
	private String buyersId;
	//买家名称
	private String buyersName;
	//买家负责人
	private String buyersPerson;
	//买家手机号码
	private String buyersPhone;
	//卖家Id
	private String sellerId;
	//卖家名称
	private String sellerName;
	//卖家负责人
	private String sellerPerson;
	//卖家号码
	private String sellerPhone;
	//
	private Integer isDel;
	//
	private String createId;
	//
	private String createName;
	//
	private Date createDate;
	//
	private String updateId;
	//
	private String updateName;
	//
	private Date updateDate;
	//
	private String delId;
	//
	private String delName;
	//
	private Date delDate;
}
