package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.druid.util.StringUtils;

/**
 * 同步采购系统采购订单信息
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class Purchase {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Purchase purchase = new Purchase();
		purchase.dealWith();
	}
	
	public void dealWith(){
		try {
			Connection purchaseConn = DBUtils.getTradeConnection();
			//获得数据
			List<Map<String,Object>> dataList = getPurchaseData();
			int length = dataList.size();
			for(Map<String,Object> datalMap : dataList){
				System.out.println("剩余："+length+"条数据,总计："+dataList.size());
				insertData(datalMap,purchaseConn);
				length--;
			}
			DBUtils.closeConnection(purchaseConn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void insertData(Map<String, Object> datalMap,Connection conn) throws Exception{
		String purchasecode = datalMap.get("purchasecode").toString();
		String countSql = "select count(order_code) num from buy_order where order_code='"+purchasecode+"'";
		PreparedStatement ps = conn.prepareStatement(countSql);
		ResultSet rs = ps.executeQuery();
		int result = 0;
		while (rs.next()) {
			result = rs.getInt("num");
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		if(result == 0){
			StringBuffer insertHeaderSql = new StringBuffer("INSERT INTO buy_order (");
			insertHeaderSql.append("id,order_code,supp_id,supp_name,goods_num,goods_price,is_check,status,");
			insertHeaderSql.append("is_since,order_kind,remark,is_del,create_id,create_name,create_date,");
			insertHeaderSql.append("company_id,order_type,predict_arred)");
			insertHeaderSql.append(" values (");
			insertHeaderSql.append("'"+datalMap.get("id")+"',");//id
			insertHeaderSql.append("'"+datalMap.get("purchasecode")+"',");//order_code
			insertHeaderSql.append("'"+datalMap.get("supplierid")+"',");//supp_id
			insertHeaderSql.append("'"+datalMap.get("suppliername")+"',");//supp_name
			insertHeaderSql.append("'"+datalMap.get("goodscount")+"',");//goods_num
			insertHeaderSql.append("'"+datalMap.get("goodsprice")+"',");//goods_price
			//status状态
			int status = Integer.parseInt(datalMap.get("status").toString());
			switch (status) {
			case 0:
				//等待财审
				insertHeaderSql.append("'0',");//is_check
				insertHeaderSql.append("'0',");//status
				break;
			case 2:
				//财审不通过
				insertHeaderSql.append("'2',");//is_check
				insertHeaderSql.append("'0',");//status
				break;
			case 3:
				//等待确认
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'0',");//status
				break;
			case 4:
				//已确认
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 5:
				//采购单驳回
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'7',");//status
				break;
			case 6:
				//等待发起对账
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 7:
				//等待对账
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 8:
				//完结
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 9:
				//已调价
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 10:
				//已记账
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'4',");//status
				break;
			case 11:
				//已终止
				insertHeaderSql.append("'1',");//is_check
				insertHeaderSql.append("'8',");//status
				break;
			case 12:
				//等待采购总监审核
				insertHeaderSql.append("'0',");//is_check
				insertHeaderSql.append("'0',");//status
				break;
			default:
				//
				insertHeaderSql.append("'0',");//is_check
				insertHeaderSql.append("'0',");//status
				break;
			}
			insertHeaderSql.append("'0',");//is_since
			insertHeaderSql.append("'0',");//order_kind
			insertHeaderSql.append("'【原采购系统导入】"+datalMap.get("remark")+"',");//remark
			insertHeaderSql.append("'0',");//is_del
			insertHeaderSql.append("'"+datalMap.get("creater")+"',");//creater
			insertHeaderSql.append("'"+datalMap.get("createrName")+"',");//create_name
			insertHeaderSql.append("'"+datalMap.get("created")+"',");//create_date
			insertHeaderSql.append("'17060909542281121440',");//company_id
			insertHeaderSql.append("'0',");//order_type
			String predictArred = datalMap.get("predictArred")==null?null:datalMap.get("predictArred").toString();
			if(StringUtils.isEmpty(predictArred)){
				insertHeaderSql.append("null");//predict_arred
			}else{
				insertHeaderSql.append("'"+predictArred+"'");//predict_arred
			}
			insertHeaderSql.append(");");
			PreparedStatement insertPs = conn.prepareStatement(insertHeaderSql.toString());
			int result2 = insertPs.executeUpdate();
			// 关闭声明
			DBUtils.closeStatement(insertPs);
			if(result2 > 0){
				//新增子信息
				List<Map<String,Object>> childList = (List<Map<String,Object>>)datalMap.get("itemList");
				if(childList != null && !childList.isEmpty()){
					for(Map<String,Object> item : childList){
						insertItem(item,conn,datalMap);
					}
				}
				//插入审批信息
				List<Map<String,Object>> verifyList = (List<Map<String,Object>>)datalMap.get("verifyList");
				if(verifyList != null && !verifyList.isEmpty()){
					//新增审批主表
					insertVerifyHeader(datalMap,conn);
					for(int i = 0; i < verifyList.size(); i++){
						Map<String,Object> verify = verifyList.get(i);
						insertVerifyPocess(verify,conn,i);
					}
				}
			}
			//推送状态
			int pushStatus = Integer.parseInt(datalMap.get("isPushSuccess")==null?"0":datalMap.get("isPushSuccess").toString());
			if(pushStatus == 1){
				//推送成功，向供应商系统插入采购单数据
				insertSeller(datalMap,conn);
			}
		}
	}

	/**
	 * 向卖家添加采购单信息
	 * @param datalMap
	 * @param conn
	 */
	private void insertSeller(Map<String, Object> datalMap, Connection conn) throws Exception{
		String purchasecode = datalMap.get("purchasecode").toString();
		String countSql = "select count(order_code) num from seller_order where order_code='"+purchasecode+"'";
		PreparedStatement ps = conn.prepareStatement(countSql);
		ResultSet rs = ps.executeQuery();
		int result = 0;
		while (rs.next()) {
			result = rs.getInt("num");
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		if(result == 0){
			StringBuffer insertHeaderSql = new StringBuffer("INSERT INTO seller_order (");
			insertHeaderSql.append("id,order_code,supp_id,supp_name,goods_num,goods_price,status,");
			insertHeaderSql.append("is_since,order_kind,remark,is_del,create_id,create_name,create_date,");
			insertHeaderSql.append("company_id,company_name,order_type,predict_arred,buyer_order_id)");
			insertHeaderSql.append(" values (");
			insertHeaderSql.append("'"+datalMap.get("id")+"',");//id
			insertHeaderSql.append("'"+datalMap.get("purchasecode")+"',");//order_code
			insertHeaderSql.append("'"+datalMap.get("supplierid")+"',");//supp_id
			insertHeaderSql.append("'"+datalMap.get("suppliername")+"',");//supp_name
			insertHeaderSql.append("'"+datalMap.get("goodscount")+"',");//goods_num
			insertHeaderSql.append("'"+datalMap.get("goodsprice")+"',");//goods_price
			//status状态
			int status = Integer.parseInt(datalMap.get("status").toString());
			switch (status) {
			case 0:
				//等待财审
				insertHeaderSql.append("'0',");//status
				break;
			case 2:
				//财审不通过
				insertHeaderSql.append("'0',");//status
				break;
			case 3:
				//等待确认
				insertHeaderSql.append("'0',");//status
				break;
			case 4:
				//已确认
				insertHeaderSql.append("'4',");//status
				break;
			case 5:
				//采购单驳回
				insertHeaderSql.append("'7',");//status
				break;
			case 6:
				//等待发起对账
				insertHeaderSql.append("'4',");//status
				break;
			case 7:
				//等待对账
				insertHeaderSql.append("'4',");//status
				break;
			case 8:
				//完结
				insertHeaderSql.append("'4',");//status
				break;
			case 9:
				//已调价
				insertHeaderSql.append("'4',");//status
				break;
			case 10:
				//已记账
				insertHeaderSql.append("'4',");//status
				break;
			case 11:
				//已终止
				insertHeaderSql.append("'8',");//status
				break;
			case 12:
				//等待采购总监审核
				insertHeaderSql.append("'0',");//status
				break;
			default:
				//
				insertHeaderSql.append("'0',");//status
				break;
			}
			insertHeaderSql.append("'0',");//is_since
			insertHeaderSql.append("'0',");//order_kind
			insertHeaderSql.append("'【原采购系统导入】"+datalMap.get("remark")+"',");//remark
			insertHeaderSql.append("'0',");//is_del
			insertHeaderSql.append("'"+datalMap.get("creater")+"',");//creater
			insertHeaderSql.append("'"+datalMap.get("createrName")+"',");//create_name
			insertHeaderSql.append("'"+datalMap.get("created")+"',");//create_date
			insertHeaderSql.append("'17060909542281121440',");//company_id
			insertHeaderSql.append("'山东诺泰健康科技有限公司',");//company_id
			insertHeaderSql.append("'0',");//order_type
			String predictArred = datalMap.get("predictArred")==null?null:datalMap.get("predictArred").toString();
			if(StringUtils.isEmpty(predictArred)){
				insertHeaderSql.append("null,");//predict_arred
			}else{
				insertHeaderSql.append("'"+predictArred+"',");//predict_arred
			}
			insertHeaderSql.append("'"+datalMap.get("id")+"'");//id
			insertHeaderSql.append(");");
			PreparedStatement insertPs = conn.prepareStatement(insertHeaderSql.toString());
			int result2 = insertPs.executeUpdate();
			// 关闭声明
			DBUtils.closeStatement(insertPs);
			if(result2 > 0){
				//新增子信息
				List<Map<String,Object>> childList = (List<Map<String,Object>>)datalMap.get("itemList");
				if(childList != null && !childList.isEmpty()){
					for(Map<String,Object> item : childList){
						insertSellerItem(item,conn,datalMap);
					}
				}
			}
		}
	}

	private void insertSellerItem(Map<String, Object> item, Connection conn,
			Map<String, Object> headerMap) throws Exception{
		StringBuffer insertItemSql = new StringBuffer("INSERT INTO seller_order_supplier_product (");
		insertItemSql.append("id,order_id,buy_order_item_id,apply_code,product_code,product_name,sku_code,sku_name,barcode,price,order_num,");
		insertItemSql.append("total_money,warehouse_id,delivered_num,arrival_num,remark,unit_id,is_need_invoice,predict_arred)");
		insertItemSql.append(" values (");
		insertItemSql.append("'"+item.get("id")+"',");//id
		insertItemSql.append("'"+item.get("purchaseid")+"',");//order_id
		insertItemSql.append("'"+item.get("id")+"',");//buy_order_item_id
		insertItemSql.append("'"+item.get("apply_code")+"',");//apply_code
		insertItemSql.append("'"+item.get("procode")+"',");//pro_code
		insertItemSql.append("'"+item.get("proname")+"',");//pro_name
		insertItemSql.append("'"+item.get("skucode")+"',");//sku_code
		insertItemSql.append("'"+item.get("skuname")+"',");//sku_name
		insertItemSql.append("'"+item.get("skuoid")+"',");//sku_id
		insertItemSql.append(item.get("costprice")+",");//price
		insertItemSql.append(item.get("realcount")+",");//goods_number
		insertItemSql.append(item.get("totalprice")+",");//priceSum
		String warehouseCode = headerMap.get("warehouseCode")==null?"0005":headerMap.get("warehouseCode").toString();
		insertItemSql.append("'"+warehouseCode+"',");//ware_house_id
		insertItemSql.append(item.get("deliverycount")+",");//delivered_num
		insertItemSql.append(item.get("arrivalcount")+",");//arrival_num
		insertItemSql.append("'【原采购系统导入】',");//remark
		insertItemSql.append("'17070710044995394265',");//unit_id
		insertItemSql.append("'N',");//is_need_invoice
		String predictArred = headerMap.get("predictArred")==null?null:headerMap.get("predictArred").toString();
		if(StringUtils.isEmpty(predictArred)){
			insertItemSql.append("null");//predict_arred
		}else{
			insertItemSql.append("'"+predictArred+"'");//predict_arred
		}
		insertItemSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertItemSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 新增审批主表
	 * @param datalMap
	 * @param conn
	 */
	private void insertVerifyHeader(Map<String, Object> datalMap,
			Connection conn) throws Exception{
		StringBuffer insertVerifyHeaderSql = new StringBuffer("INSERT INTO trade_verify_header (");
		insertVerifyHeaderSql.append("id,title,related_id,verify_success,verify_error,related_url,site_id,company_id,");
		insertVerifyHeaderSql.append("status,remark,create_id,create_name,create_date,");
		insertVerifyHeaderSql.append("update_user_id,update_user_name,update_date)");
		insertVerifyHeaderSql.append(" values (");
		insertVerifyHeaderSql.append("'"+datalMap.get("id")+"',");//id
		insertVerifyHeaderSql.append("'原采购系统采购订单',");//title
		insertVerifyHeaderSql.append("'"+datalMap.get("id")+"',");//related_id
		insertVerifyHeaderSql.append("'platform/buyer/purchase/verifySuccess',");//verify_success
		insertVerifyHeaderSql.append("'platform/buyer/purchase/verifyError',");//verify_error
		insertVerifyHeaderSql.append("'platform/buyer/purchase/verifyDetail',");//related_url
		insertVerifyHeaderSql.append("'17111514041191591091',");//site_id
		insertVerifyHeaderSql.append("'17060909542281121440',");//company_id
		//status状态
		int status = Integer.parseInt(datalMap.get("status").toString());
		switch (status) {
		case 0:
			//等待财审
			insertVerifyHeaderSql.append("'0',");
			break;
		case 2:
			//财审不通过
			insertVerifyHeaderSql.append("'2',");
			break;
		case 3:
			//等待确认
			insertVerifyHeaderSql.append("'1',");
			break;
		case 4:
			//已确认
			insertVerifyHeaderSql.append("'1',");
			break;
		case 5:
			//采购单驳回
			insertVerifyHeaderSql.append("'1',");
			break;
		case 6:
			//等待发起对账
			insertVerifyHeaderSql.append("'1',");
			break;
		case 7:
			//等待对账
			insertVerifyHeaderSql.append("'1',");
			break;
		case 8:
			//完结
			insertVerifyHeaderSql.append("'1',");
			break;
		case 9:
			//已调价
			insertVerifyHeaderSql.append("'1',");
			break;
		case 10:
			//已记账
			insertVerifyHeaderSql.append("'1',");
			break;
		case 11:
			//已终止
			insertVerifyHeaderSql.append("'1',");
			break;
		case 12:
			//等待采购总监审核
			insertVerifyHeaderSql.append("'0',");
			break;
		default:
			//
			insertVerifyHeaderSql.append("'0',");
			break;
		}
		insertVerifyHeaderSql.append("'原采购系统数据导入',");//remark
		insertVerifyHeaderSql.append("'"+datalMap.get("creater")+"',");//creater
		insertVerifyHeaderSql.append("'"+datalMap.get("createrName")+"',");//create_name
		insertVerifyHeaderSql.append("'"+datalMap.get("created")+"',");//create_date
		insertVerifyHeaderSql.append("null,");//update_user_id
		insertVerifyHeaderSql.append("null,");//update_user_name
		insertVerifyHeaderSql.append("null");//update_date
		insertVerifyHeaderSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertVerifyHeaderSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 新增审批数据
	 * @param verify
	 * @param conn
	 * @param i
	 */
	private void insertVerifyPocess(Map<String, Object> verify, Connection conn,
			int i) throws Exception{
		StringBuffer insertVerifyPocessSql = new StringBuffer("INSERT INTO trade_verify_pocess (");
		insertVerifyPocessSql.append("id,header_id,user_id,user_name,status,remark,");
		insertVerifyPocessSql.append("verify_index,start_date,end_date)");
		insertVerifyPocessSql.append(" values (");
		insertVerifyPocessSql.append("'"+verify.get("id")+"',");//id
		insertVerifyPocessSql.append("'"+verify.get("relatedid")+"',");//header_id
		insertVerifyPocessSql.append("'"+verify.get("verifyuser")+"',");//user_id
		insertVerifyPocessSql.append("'"+verify.get("verifyusername")+"',");//user_name
		//status状态
		int status = Integer.parseInt(verify.get("status").toString());
		if(status==0){
			//同意
			insertVerifyPocessSql.append("'1',");
		}else{
			//驳回
			insertVerifyPocessSql.append("'2',");
		}
		String reason = verify.get("reason")==null?"":verify.get("reason").toString();
		int verifytype = Integer.parseInt(verify.get("verifytype").toString());
		if(verifytype==2){
			insertVerifyPocessSql.append("'采购单财审 "+reason+"',");//remark
		}else if(verifytype==4){
			insertVerifyPocessSql.append("'采购部长审核 "+reason+"',");//remark
		}else if(verifytype==5){
			insertVerifyPocessSql.append("'总经理审核 "+reason+"',");//remark
		}else{
			insertVerifyPocessSql.append("'"+reason+"',");//remark
		}
		insertVerifyPocessSql.append(i+",");//verify_index
		insertVerifyPocessSql.append("'"+verify.get("verifytime")+"',");//start_date
		insertVerifyPocessSql.append("'"+verify.get("verifytime")+"'");//end_date
		insertVerifyPocessSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertVerifyPocessSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}

	/**
	 * 获得审批信息
	 * @param Connection
	 * @return
	 */
	private List<Map<String, Object>> getVerify(Connection purchaseConn) throws Exception{
		System.out.println("正在获取审批数据。。。。。");
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT id,relatedid,verifyuser,verifyusername,verifytime,`status`,reason,verifytype FROM verify ORDER BY verifytime";
		PreparedStatement ps = purchaseConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>(8);
			map.put("id", rs.getString("id"));
			map.put("relatedid", rs.getString("relatedid"));
			map.put("verifyuser", rs.getString("verifyuser"));
			map.put("verifyusername", rs.getString("verifyusername"));
			map.put("verifytime", rs.getString("verifytime"));
			map.put("status", rs.getString("status"));
			map.put("reason", rs.getString("reason"));
			map.put("verifytype", rs.getString("verifytype"));
			System.out.println(map);
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		System.out.println("获取审批系统数据结束");
		return dataList;
	}

	/**
	 * 新增商品信息
	 * @param item
	 * @param conn
	 * @param headerMap
	 * @throws Exception
	 */
	private void insertItem(Map<String, Object> item, Connection conn,Map<String, Object> headerMap) throws Exception{
		StringBuffer insertItemSql = new StringBuffer("INSERT INTO buy_order_product (");
		insertItemSql.append("id,order_id,apply_code,pro_code,pro_name,sku_code,sku_name,sku_oid,priceSum,price,goods_number,");
		insertItemSql.append("delivered_num,arrival_num,unit_id,ware_house_id,remark,is_need_invoice,predict_arred)");
		insertItemSql.append(" values (");
		insertItemSql.append("'"+item.get("id")+"',");//id
		insertItemSql.append("'"+item.get("purchaseid")+"',");//apply_id
		insertItemSql.append("'"+item.get("apply_code")+"',");//apply_code
		insertItemSql.append("'"+item.get("procode")+"',");//pro_code
		insertItemSql.append("'"+item.get("proname")+"',");//pro_name
		insertItemSql.append("'"+item.get("skucode")+"',");//sku_code
		insertItemSql.append("'"+item.get("skuname")+"',");//sku_name
		insertItemSql.append("'"+item.get("skuoid")+"',");//sku_id
		insertItemSql.append(item.get("totalprice")+",");//priceSum
		insertItemSql.append(item.get("costprice")+",");//price
		insertItemSql.append(item.get("realcount")+",");//goods_number
		insertItemSql.append(item.get("deliverycount")+",");//delivered_num
		insertItemSql.append(item.get("arrivalcount")+",");//arrival_num
		insertItemSql.append("'17070710044995394265',");//unit_id
		String warehouseCode = headerMap.get("warehouseCode")==null?"0005":headerMap.get("warehouseCode").toString();
		insertItemSql.append("'"+warehouseCode+"',");//ware_house_id
		insertItemSql.append("'【原采购系统导入】',");//remark
		insertItemSql.append("'N',");//is_need_invoice
		String predictArred = headerMap.get("predictArred")==null?null:headerMap.get("predictArred").toString();
		if(StringUtils.isEmpty(predictArred)){
			insertItemSql.append("null");//predict_arred
		}else{
			insertItemSql.append("'"+predictArred+"'");//predict_arred
		}
		insertItemSql.append(")");
		PreparedStatement insertPs = conn.prepareStatement(insertItemSql.toString());
		insertPs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(insertPs);
	}
	
	/**
	 * 获得采购系统中的数据
	 * @return
	 * @throws Exception
	 */
	private List<Map<String,Object>> getPurchaseData() throws Exception{
		System.out.println("正在获取采购系统数据。。。。。");
		Connection purchaseConn = DBUtils.getPurchaseConnection();
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT * FROM purchase_header";
		PreparedStatement ps = purchaseConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = ps.getMetaData();
		// 取得结果集列数
        int columnCount = rsmd.getColumnCount();
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>();
			for (int i = 1; i < columnCount; i++) {
				map.put(rsmd.getColumnLabel(i), rs.getObject(rsmd.getColumnLabel(i)));
            }
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		
		if(dataList != null && !dataList.isEmpty()){
			//获得子信息
			List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
			String selectItemSql = "SELECT i.*,h.apply_code FROM purchase_item i"
				+ " LEFT JOIN applypurchase_item a1 ON a1.id=i.applyItemId"
				+ " LEFT JOIN applypurchase_header h ON h.id=a1.apply_id";
			PreparedStatement ps2 = purchaseConn.prepareStatement(selectItemSql);
			ResultSet rs2 = ps2.executeQuery();
			// 取得结果集列数
			while (rs2.next()) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("id", rs2.getString("id"));
				map.put("purchaseid", rs2.getString("purchaseid"));
				map.put("applyItemId", rs2.getString("applyItemId"));
				map.put("shopid", rs2.getString("shopid"));
				map.put("shopcode", rs2.getString("shopcode"));
				map.put("shopname", rs2.getString("shopname"));
				map.put("procode", rs2.getString("procode"));
				map.put("proname", rs2.getString("proname"));
				map.put("skucode", rs2.getString("skucode"));
				map.put("skuname", rs2.getString("skuname"));
				map.put("skuoid", rs2.getString("skuoid"));
				map.put("realcount", rs2.getInt("realcount"));
				map.put("arrivalcount", rs2.getInt("arrivalcount"));
				map.put("costprice", rs2.getObject("costprice"));
				map.put("lasttimeprice", rs2.getObject("lasttimeprice"));
				map.put("totalprice", rs2.getObject("totalprice"));
				map.put("saleprice", rs2.getObject("saleprice"));
				map.put("end_type", rs2.getObject("end_type"));
				map.put("arrivalDate", rs2.getObject("arrivalDate"));
				map.put("assignDate", rs2.getObject("assignDate"));
				map.put("deliverycode", rs2.getObject("deliverycode"));
				map.put("apply_code", rs2.getObject("apply_code"));
				itemList.add(map);
			}
			// 关闭记录集
			DBUtils.closeResultSet(rs2);
			// 关闭声明
			DBUtils.closeStatement(ps2);
			Map<String,List<Map<String,Object>>> itemMap = new HashMap<>();
			if(itemList != null && !itemList.isEmpty()){
				System.out.println("正在处理item数据");
				int length = itemList.size();
				for(Map<String,Object> item : itemList){
					System.out.println("剩余item数："+length+"条");
					String applyId = item.get("purchaseid").toString();
					//获得已发货数量
					String skuoid = item.get("skuoid").toString();
					String selectDeliverycountSql = "SELECT IFNULL(sum(deliverycount),0) deliverycount FROM deliveryrecord_item "
							+ " WHERE relatedcode='"+applyId+"' AND skuoid='"+skuoid+"'";
					PreparedStatement ps3 = purchaseConn.prepareStatement(selectDeliverycountSql);
					ResultSet rs3 = ps3.executeQuery();
					while (rs3.next()) {
						int deliverycount = rs3.getInt("deliverycount");
						item.put("deliverycount", deliverycount);
					}
					System.out.println("applyId="+applyId);
					// 关闭记录集
					DBUtils.closeResultSet(rs3);
					// 关闭声明
					DBUtils.closeStatement(ps3);
					List<Map<String,Object>> childList = new ArrayList<>();
					if(itemMap.containsKey(applyId)){
						childList = itemMap.get(applyId);
					}
					childList.add(item);
					itemMap.put(applyId, childList);
					length--;
				}
			}
			//获得审批信息
			List<Map<String,Object>> verifyList = getVerify(purchaseConn);
			for(Map<String,Object> data : dataList){
				String id = data.get("id").toString();
				List<Map<String,Object>> childList = itemMap.get(id);
				data.put("itemList", childList);
				List<Map<String,Object>> verifyChildList = new ArrayList<Map<String,Object>>();
				if(verifyList != null && !verifyList.isEmpty()){
					for(Map<String,Object> verify : verifyList){
						String relatedid = verify.get("relatedid").toString();
						if(id.equals(relatedid)){
							verifyChildList.add(verify);
						}
					}
				}
				data.put("verifyList", verifyChildList);
			}
		}
		// 关闭链接对象
		DBUtils.closeConnection(purchaseConn);
		System.out.println("获取采购系统数据结束");
		return dataList;
	}
}
