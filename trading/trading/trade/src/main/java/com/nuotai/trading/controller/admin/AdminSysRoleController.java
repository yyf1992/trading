package com.nuotai.trading.controller.admin;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysRole;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.SysRoleMenuService;
import com.nuotai.trading.service.SysRoleService;
import com.nuotai.trading.utils.PageAdmin;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 后台管理-角色管理
 * @author wxx
 *
 */
@Controller
@RequestMapping("admin/adminSysRole")
public class AdminSysRoleController extends BaseController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysMenuService sysMenuService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	
	/**
	 * 加载列表页面
	 * @return
	 */
	@RequestMapping(value = "/loadSysRoleList.html", method = RequestMethod.GET)
	public String loadUserList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		searchPageUtil.setObject(params);
		List<Map<String, Object>> sysRoleList = sysRoleService.selectByPage(searchPageUtil);
		searchPageUtil.getPageAdmin().setList(sysRoleList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "admin/sysrole/sysRoleList";
	}
	
	/**
	 * 加载新增页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadAddRole.html", method = RequestMethod.GET)
	public String loadAdd() {
		return "admin/sysrole/addSysRole";
	}

	/**
	 * 加载修改页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadUpdateRole.html", method = RequestMethod.GET)
	public String loadUpdate(String id) {
		SysRole sysRole = sysRoleService.selectByPrimaryKey(id);
		model.addAttribute("sysRole", sysRole);
		return "admin/sysrole/updateSysRole";
	}
	
	/**
	 * 新增保存
	 * 
	 * @param sysRole
	 * @return
	 */
	@RequestMapping(value = "/saveAdd", method = RequestMethod.POST)
	@ResponseBody
	public String saveAdd(SysRole sysRole) {
		JSONObject json = new JSONObject();
		// 判断代码是否占用
		List<SysRole> coderoleList = sysRoleService.selectByRoleCode(sysRole.getRoleCode());
		if (coderoleList != null && coderoleList.size() > 0) {
			// 名字存在
			json.put("success", false);
			json.put("msg", "角色代码已存在！");
			return json.toString();
		}
		int result = 0;
		sysRole.setId(ShiroUtils.getUid());
		result = sysRoleService.insertSelective(sysRole);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "成功！");
		} else {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}

	/**
	 * 修改
	 * 
	 * @param sysRole
	 * @return
	 */
	@RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdate(SysRole sysRole) {
		JSONObject json = new JSONObject();
		int result = sysRoleService.updateByPrimaryKeySelective(sysRole);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "成功！");
		} else {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	@RequestMapping(value = "/deleteSysRole.html", method = RequestMethod.GET)
	@ResponseBody
	public String deleteSysRole(String ids) {
		JSONObject json = new JSONObject();
		try {
			String[] idList = ids.split(",");
			sysRoleService.saveDelete(idList);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
	
	/**
	 * 加载权限设置页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadInstalSysRoleMenu.html", method = RequestMethod.GET)
	public String loadInstalSysRoleMenu(String roleId) {
		SysRole sysRole = sysRoleService.selectByPrimaryKey(roleId);
		// 取得菜单信息
		List<SysMenu> sysMenuList = sysMenuService.getMenuByMap(null);
		JSONArray zTreeList = new JSONArray();
		if (sysMenuList != null && sysMenuList.size() > 0) {
			for (SysMenu menu : sysMenuList) {
				JSONObject zTreeMap = new JSONObject();
				zTreeMap.put("id", menu.getId());
				zTreeMap.put("pId", menu.getParentId()==null?"0":menu.getParentId());
				zTreeMap.put("name", menu.getName());
				zTreeMap.put("open", true);
				zTreeList.add(zTreeMap);
			}
		}else{
			JSONObject newJson = new JSONObject();
			newJson.put("id", "-1");
			newJson.put("pId", "0");
			newJson.put("name", "根节点");
			zTreeList.add(newJson);
		}
		List<String> oldMenuIdList = sysRoleMenuService.getMenuByRoleId(roleId);
		model.addAttribute("oldMenuIdList", net.sf.json.JSONArray.fromObject(oldMenuIdList));
		model.addAttribute("sysRole", sysRole);
		model.addAttribute("zTreeList", zTreeList);
		return "admin/sysrole/instalSysRoleMenu";
	}
	
	/**
	 * 保存权限设置
	 * @param roleId
	 * @return
	 */
	@RequestMapping(value = "/saveRoleMenu", method = RequestMethod.POST)
	@ResponseBody
	public String saveRoleMenu(String menuIds,String roleId) {
		JSONObject json = new JSONObject();
		try {
			sysRoleService.saveRoleMenu(menuIds,roleId);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
	
}
