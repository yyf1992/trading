package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyProductSkuBomMapper;
import com.nuotai.trading.dao.BuyProductSkuComposeMapper;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class BuyProductSkuBomService {

	@Autowired
	private BuyProductSkuBomMapper buyProductSkuBomMapper;
	@Autowired
	private BuyProductSkuComposeMapper buyProductSkuComposeMapper;
	
	public BuyProductSkuBom get(String id){
		return buyProductSkuBomMapper.get(id);
	}
	
	public List<BuyProductSkuBom> queryList(Map<String, Object> map){
		return buyProductSkuBomMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyProductSkuBomMapper.queryCount(map);
	}
	
	public void add(BuyProductSkuBom buyProductSkuBom){
		buyProductSkuBomMapper.add(buyProductSkuBom);
	}
	
	public void update(BuyProductSkuBom buyProductSkuBom){
		buyProductSkuBomMapper.update(buyProductSkuBom);
	}
	
	/**
	 * 删除物料配置
	 * @param id
	 */
	public void delete(String id){
		Map<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("companyId", ShiroUtils.getCompId());
		map.put("delDate", new Date());
		map.put("delName", ShiroUtils.getUserName());
		map.put("delId", ShiroUtils.getUserId());
		BuyProductSkuBom buyProductSkuBom = buyProductSkuBomMapper.get(id);
		if (!ObjectUtil.isEmpty(buyProductSkuBom)){
			map.put("productId", buyProductSkuBom.getProductId());
			buyProductSkuComposeMapper.deleteByMap(map);
		}
		buyProductSkuBomMapper.deleteById(map);
	}

	public List<BuyProductSkuBom> selectList(SearchPageUtil searchPageUtil) {
		return buyProductSkuBomMapper.selectList(searchPageUtil);
	}
	
	public List<String> selectExitProduct(String  companyId) {
		return buyProductSkuBomMapper.selectExitProduct(companyId);
	}
	
	public void setStatus(Map<String,String>  map) {
		 buyProductSkuBomMapper.setStatus(map);
	}

	public BuyProductSkuBom selectById(String id) {
		return buyProductSkuBomMapper.selectById(id);
	}
	
	public BuyProductSkuBom selectByMap(Map<String, Object> map) {
		return buyProductSkuBomMapper.selectByMap(map);
	}
	
	public BuyProductSkuBom selectByBomId(String id) {
		return buyProductSkuBomMapper.selectByBomId(id);
	}

	//获取不同状态bom表的条数
	public Map<String, Object> getBomStatsCout(Map<String, Object> map) {
		Map<String,Object>  num =new HashMap<String,Object>();
		map.put("status", 0);
		int allCount=buyProductSkuBomMapper.getStatsCout(map);
		map.put("status", 1);
		int waitApproval=buyProductSkuBomMapper.getStatsCout(map);
		map.put("status", 2);
		int passApproval=buyProductSkuBomMapper.getStatsCout(map);
		map.put("status", 3);
		int refuseApproval=buyProductSkuBomMapper.getStatsCout(map);
		num.put("allCount", allCount);
		num.put("waitApproval", waitApproval);
		num.put("passApproval", passApproval);
		num.put("refuseApproval", refuseApproval);
		return num;
	}
	
	public BuyProductSkuBom selectByBarcode(Map<String, Object> map) {
		BuyProductSkuBom buyProductSkuBom = buyProductSkuBomMapper.selectByBarcode(map);
		
		if (!ObjectUtil.isEmpty(buyProductSkuBom)){
			Map<String, Object> bomMap = new HashMap<>();
			bomMap.put("companyId", buyProductSkuBom.getCompanyId());
			bomMap.put("productId", buyProductSkuBom.getProductId());
			List<Map<String, Object>> skuComposeList = buyProductSkuComposeMapper.selectByBomId(bomMap);
			buyProductSkuBom.setSkuComposeList(skuComposeList);
		}
		
		return buyProductSkuBom;
	}

	public BuyProductSkuBom selectByBarcodeAndCompanyId(Map<String, Object> map) {
		return buyProductSkuBomMapper.selectByBarcodeAndCompanyId(map);
	}
}
