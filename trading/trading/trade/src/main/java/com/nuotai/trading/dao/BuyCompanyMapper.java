package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.utils.SearchPageUtil;

public interface BuyCompanyMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyCompany record);

    int insertSelective(BuyCompany record);

    BuyCompany selectByPrimaryKey(String id);
    
    int updateByPrimaryKeySelective(BuyCompany record);

    int updateByPrimaryKey(BuyCompany record);
    
    List<BuyCompany> selectByMap(Map<String,Object> map);
    
    List<BuyCompany> selectByPage(SearchPageUtil searchPageUtil);

    List<BuyCompany> checkCompany(Map<String, Object> map);
}