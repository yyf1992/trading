package com.nuotai.trading.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.nuotai.trading.dao.SysMenuMapper;
import com.nuotai.trading.dao.SysRoleCompanyMapper;
import com.nuotai.trading.dao.SysRoleMenuMapper;
import com.nuotai.trading.dao.TradeRoleMapper;
import com.nuotai.trading.dao.TradeRoleMenuMapper;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class SysMenuService  {
	@Autowired
	private SysMenuMapper sysMenuMapper;
	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
	@Autowired
	private SysRoleCompanyMapper sysRoleCompanyMapper;
	@Autowired
	private TradeRoleMapper tradeRoleMapper;
	@Autowired
	private TradeRoleMenuMapper tradeRoleMenuMapper;
		
	public int deleteByPrimaryKey(String id) {
		return sysMenuMapper.deleteByPrimaryKey(id);
	}

	public int insert(SysMenu record) {
		return sysMenuMapper.insert(record);
	}

	public int insertSelective(SysMenu record) {
		return sysMenuMapper.insertSelective(record);
	}

	public SysMenu selectByPrimaryKey(String id) {
		return sysMenuMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(SysMenu record) {
		return sysMenuMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(SysMenu record) {
		return sysMenuMapper.updateByPrimaryKey(record);
	}

	public List<SysMenu> getAllMenuPage(SearchPageUtil searchPageUtil) {
		return sysMenuMapper.getAllMenuPage(searchPageUtil);
	}

	public List<SysMenu> getMenuByMap(Map<String, Object> map) {
		return sysMenuMapper.getMenuByMap(map);
	}

	public int saveAdd(SysMenu sysMenu) {
		sysMenu.setId(ShiroUtils.getUid());
		if(!StringUtils.isEmpty(sysMenu.getParentId())){
			//有父菜单
			SysMenu parentMenu = sysMenuMapper.selectByPrimaryKey(sysMenu.getParentId());
			sysMenu.setSortOrder(parentMenu.getSortOrder()+"_"+sysMenu.getId());
			if(sysMenu.getIsMenu()==Constant.MenuType.MENU.getValue()){
				//是菜单
				sysMenu.setPosition(Constant.MenuPosition.LEFT.getValue());
			}else{
				//按钮
				sysMenu.setPosition(Constant.MenuPosition.BUTTON.getValue());
			}
		}else{
			sysMenu.setSortOrder(sysMenu.getId());
			if(sysMenu.getIsMenu()==Constant.MenuType.MENU.getValue()){
				sysMenu.setPosition(Constant.MenuPosition.TOP.getValue());
			}
		}
		int level = sysMenu.getSortOrder().split("_").length;
		sysMenu.setLevel(level);
		sysMenu.setCreateTime(new Date());
		return sysMenuMapper.insertSelective(sysMenu);
	}

	public int saveUpdate(SysMenu sysMenu) {
		if(!StringUtils.isEmpty(sysMenu.getParentId())){
			SysMenu parentMenu = sysMenuMapper.selectByPrimaryKey(sysMenu.getParentId());
			sysMenu.setSortOrder(parentMenu.getSortOrder()+"_"+sysMenu.getId());
			if(sysMenu.getIsMenu()==Constant.MenuType.MENU.getValue()){
				sysMenu.setPosition(Constant.MenuPosition.LEFT.getValue());
			}else{
				//按钮
				sysMenu.setPosition(Constant.MenuPosition.BUTTON.getValue());
			}
		}else{
			sysMenu.setSortOrder(sysMenu.getId());
			if(sysMenu.getIsMenu()==Constant.MenuType.MENU.getValue()){
				sysMenu.setPosition(Constant.MenuPosition.TOP.getValue());
			}
		}
		int level = sysMenu.getSortOrder().split("_").length;
		sysMenu.setLevel(level);
		sysMenu.setCreateTime(new Date());
		return sysMenuMapper.updateByPrimaryKeySelective(sysMenu);
	}

	public void deleteSysMenu(String ids) {
		String[] idList = ids.split(",");
		//删除菜单角色关联信息
		sysRoleMenuMapper.deleteByMenuId(idList);
		//删除
		sysMenuMapper.deleteByMenuId(idList);
	}

	public int deleteByMenuId(String[] idList) {
		return sysMenuMapper.deleteByMenuId(idList);
	}

	public List<String> getMenuIdByRoleId(List<String> roleIdList) {
		return sysMenuMapper.getMenuIdByRoleId(roleIdList);
	}

	/**
	 * 获得公司所有的菜单信息
	 * @return
	 */
	public List<SysMenu> getCompanyMenu(String isMenu) {
		// 取得菜单信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		//params.put("isParent", true);
		if(!StringUtils.isEmpty(isMenu)){
			params.put("isMenu", "0");
		}
		List<SysMenu> sysMenuList = sysMenuMapper.getCompanyMenu(params);
		//递归获得子数据
		//recursively(sysMenuList,params);
		return sysMenuList;
	}
	
	/**
	 * 递归获得子数据
	 * @param sysMenuList
	 * @param params
	 */
	private void recursively(List<SysMenu> sysMenuList, Map<String, Object> params){
		if(!sysMenuList.isEmpty()){
			for(SysMenu sysMenu : sysMenuList){
				params.put("parentId", sysMenu.getId());
				List<SysMenu> childList = sysMenuMapper.getCompanyMenu(params);
				if(!childList.isEmpty()){
					sysMenu.setChildrenMenuList(childList);
					recursively(childList,params);
				}
			}
		}
	}
	public List<SysMenu> getCompanyParentMenu() {
		String companyId = ShiroUtils.getCompId();
		List<String> menuIdList = sysRoleCompanyMapper.getMenuIdListByCompanyId(companyId);
		// 取得顶部菜单信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("menuIdList", menuIdList);
		params.put("is_menu", Constant.MenuType.MENU);
		params.put("position", Constant.MenuPosition.TOP);
		params.put("menuIdSelect", "T");
		List<SysMenu> sysMenuList = getMenuByMap(params);
		if (!sysMenuList.isEmpty()) {
			for (SysMenu menu : sysMenuList) {
				//获得左侧一级菜单
				params.put("position", Constant.MenuPosition.LEFT);
				params.put("parent_id", menu.getId());
				List<SysMenu> chirldList = getMenuByMap(params);
				if(!chirldList.isEmpty()){
					for (SysMenu menu_1 : chirldList) {
						//获得左侧二级菜单
						params.put("position", Constant.MenuPosition.LEFT);
						params.put("parent_id", menu_1.getId());
						List<SysMenu> chirldList_2 = getMenuByMap(params);
						menu_1.setChildrenMenuList(chirldList_2);
					}
				}
				menu.setChildrenMenuList(chirldList);
			}
		}
		return sysMenuList;
	}

	/**
	 * 获得登录人的菜单
	 * @return
	 */
	public List<String> getMenuIdByLoginUser() {
		List<String> menuIsList = new ArrayList<String>();
		String companyId = ShiroUtils.getCompId();
		//取得登录人的角色信息
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", companyId);
		params.put("userId", ShiroUtils.getUserId());
		List<TradeRole> roleList = tradeRoleMapper.getRoleByMap(params);
		if(roleList.isEmpty()){
			//没有设置角色
			menuIsList.add("-1");
		}else{
			//先获得所属公司下所有菜单
			List<String> menuIdList = sysRoleCompanyMapper.getMenuIdListByCompanyId(companyId);
			//是否有超级权限
			boolean isAdmin = false;
			List<String> roleIdList = new ArrayList<String>();
			for(TradeRole tradeRole : roleList){
				roleIdList.add(tradeRole.getId());
				if("1".equals(tradeRole.getIsAdmin())){
					//是
					isAdmin = true;
					break;
				}
			}
			if(isAdmin){
				return menuIdList;
			}else{
				//没有超级权限
				params.put("roleIdList", roleIdList);
				List<String> menuIdList_role = tradeRoleMenuMapper.getMenuIdByRoleList(params);
				if(!menuIdList_role.isEmpty()){
					for(String roleId : menuIdList_role){
						boolean inCompany = false;
						for(String comRoleId : menuIdList){
							if(roleId.equals(comRoleId)){
								inCompany = true;
								break;
							}
						}
						if(!inCompany){
							menuIdList_role.remove(roleId);
						}
					}
				}
				return menuIdList_role;
			}
		}
		return menuIsList;
	}

	/**
	 * 登陆时获取菜单
	 * @param position
	 * @param parentId
	 * @return
	 */
	public List<SysMenu> tradeLogin(String position,String parentId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", ShiroUtils.getUserId());
		map.put("companyId", ShiroUtils.getCompId());
		if(position != null && !position.isEmpty()){
			map.put("position", position);
		}else{
			map.put("position", "");
		}
		if(parentId != null && !parentId.isEmpty()){
			map.put("parentId", parentId);
		}else{
			map.put("parentId", "");
		}
		return sysMenuMapper.tradeLogin(map);
	}

	/**
	 * 获得按钮权限
	 * @param menuId
	 * @return
	 */
	public List<SysMenu> getButtonList(String menuId) {
		if(!StringUtils.isEmpty(menuId)){
			String companyId = ShiroUtils.getCompId();
			//取得登录人的角色信息
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("companyId", companyId);
			params.put("userId", ShiroUtils.getUserId());
			if("admin".equals(ShiroUtils.getUserId())){
				params.put("isAdmin", true);
				params.put("isMenu", "1");
				params.put("isParent", true);
				params.put("parentId", menuId);
				List<SysMenu> buttonList = sysMenuMapper.getCompanyMenu(params);
				return buttonList;
			}
			List<TradeRole> roleList = tradeRoleMapper.getRoleByMap(params);
			if(roleList.isEmpty()){
				//没有设置角色
				return null;
			}else{
				//是否有超级权限
				boolean isAdmin = false;
				List<String> roleIdList = new ArrayList<String>();
				for(TradeRole tradeRole : roleList){
					roleIdList.add(tradeRole.getId());
					if("1".equals(tradeRole.getIsAdmin())){
						//是
						isAdmin = true;
						break;
					}
				}
				if(isAdmin){
					//是管理员
					params.put("isAdmin", true);
					params.put("isMenu", "1");
					params.put("isParent", true);
					params.put("parentId", menuId);
					List<SysMenu> buttonList = sysMenuMapper.getCompanyMenu(params);
					return buttonList;
				}else{
					//没有超级权限
					params.put("isAdmin", false);
					params.put("isMenu", "1");
					params.put("isParent", true);
					params.put("parentId", menuId);
					List<SysMenu> buttonList = sysMenuMapper.getCompanyMenu(params);
					return buttonList;
				}
			}
		}
		return null;
	}
}
