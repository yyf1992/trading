package com.nuotai.trading.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.ResQueueMapper;
import com.nuotai.trading.model.ResQueue;

/**
 * 文件转换队列
 * @ClassName ResQueueService
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
@Service
public class ResQueueService implements ResQueueMapper {

	@Autowired
	private ResQueueMapper resQueueMapper;

	@Override
	public List<ResQueue> getBeanList(ResQueue queue) {
		return resQueueMapper.getBeanList(queue);
	}

	@Override
	public void addBean(ResQueue queue) {
		resQueueMapper.addBean(queue);
	}

	@Override
	public void updateBean(ResQueue resQueue) {
		resQueueMapper.updateBean(resQueue);
	}

	@Override
	public void deleteBean(String id) {
		resQueueMapper.deleteBean(id);
	}
}
