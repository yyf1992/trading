package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.TBusinessWharea;

/**
 * 仓库区域
 * 
 * @author "
 * @date 2017-08-10 13:26:09
 */
public interface TBusinessWhareaMapper extends BaseDao<TBusinessWharea> {
	
	/**
	 * 查询仓库信息
	 * @param map
	 * @return
	 */
	List<TBusinessWharea> selectWhareaByMap(Map<String, Object> map);
	
	/**
	 * 添加仓库
	 * @param tBusinessWharea
	 */
	void addWharea(TBusinessWharea tBusinessWharea);
	
	/**
	 * 修改仓库
	 * @param id
	 */
	void updateWharea(TBusinessWharea tBusinessWharea);
	
	/**
	 * 根据Id查询仓库
	 * @param id
	 * @return
	 */
	TBusinessWharea getWharea(String id);
	
	/**
	 * 根据Id删除仓库
	 * @param id
	 */
	void deleteWharea(String id);
	
	/**
	 * 根据code查询仓库
	 * @param code
	 * @return
	 */
	TBusinessWharea selectByCode(String code);
}
