package com.nuotai.trading.controller.common.exception;

/**
 * 通用异常类
 * @ClassName CommException
 * @Description TODO(不需要打印异常时使用)
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
public class CommException extends Exception{

	public CommException() {
		super();
	}

	public CommException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CommException(String message, Throwable cause) {
		super(message, cause);
	}

	public CommException(String message) {
		super(message);
	}

	public CommException(Throwable cause) {
		super(cause);
	}
	
	

}
