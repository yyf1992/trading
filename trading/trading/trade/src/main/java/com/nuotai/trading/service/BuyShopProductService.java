package com.nuotai.trading.service;

import java.math.BigDecimal;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.*;
import com.nuotai.trading.model.*;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BuyShopProductService  {
	@Autowired
	private BuyShopProductMapper buyShopProductMapper;
	@Autowired
	private BuyProductSkuMapper buyProductSkuMapper;
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private BuyProductSkuService buyProductSkuService;
	@Autowired
	private BuyProductPricealterMapper buyProductPricealterMapper;
	
	public int deleteByPrimaryKey(String id) {
		return buyShopProductMapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyShopProduct record) {
		return buyShopProductMapper.insert(record);
	}

	public int insertSelective(BuyShopProduct record) {
		return buyShopProductMapper.insertSelective(record);
	}

	public BuyShopProduct selectByPrimaryKey(String id) {
		return buyShopProductMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuyShopProduct record) {
		return buyShopProductMapper.updateByPrimaryKeySelective(record);
	}
	
	public int updateBySupplierId(BuyShopProduct record) {
		return buyShopProductMapper.updateBySupplierId(record);
	}

	public int updateByPrimaryKey(BuyShopProduct record) {
		return buyShopProductMapper.updateByPrimaryKey(record);
	}

	public List<Map<String,Object>> loadProductLinks(SearchPageUtil searchPageUtil,String linktype) {
		List<Map<String,Object>> list = new ArrayList<>();
		if("0".equalsIgnoreCase(linktype)){
			list = buyShopProductMapper.loadBuyerProductLinks(searchPageUtil);
			if(list != null && list.size() > 0){
				for(Map<String,Object> mapResult : list){
					List<BuyProductSku> psList = buyProductSkuService.selectByBarCode(mapResult.get("barcode").toString());
					if(psList != null && psList.size() > 0){
						BuyProductSku ps = psList.get(0);
						mapResult.put("productType", ps.getProductType());//商品类型
					}
					
					List<BuyProductPricealter> buyProductPricealterList = buyProductPricealterMapper.selectByRelatedId(mapResult.get("id").toString());
					if (buyProductPricealterList != null && buyProductPricealterList.size() > 0){
						//查看修改价格审批ID
						mapResult.put("relatedId", buyProductPricealterList.get(0).getId());
					}
				}
			}
			return list;
		}else if("1".equalsIgnoreCase(linktype)){
//			Map<String,Object> params = (Map<String,Object>)searchPageUtil.getObject();
//			String status = String.valueOf(params.get("status"));
//			if("1".equalsIgnoreCase(status)){
//				params.put("status","2");
//			}
//			searchPageUtil.setObject(params);
			return buyShopProductMapper.loadSellerProductLinks(searchPageUtil);
		}
		return list;
	}
	
	public List<Map<String,Object>> loadProductLinksByMap(Map<String, Object> params) {
		return buyShopProductMapper.loadBuyerProductLinksByMap(params);
	}

	public List<Map<String, Object>> selectProductByMap(Map<String, Object> params) {
		return buyShopProductMapper.selectProductByMap(params);
	}

	public List<Map<String, Object>> getSupplierProductByClient(Map<String, Object> goodsMap) {
		return buyShopProductMapper.getSupplierProductByClient(goodsMap);
	}

	/**
	 * 保存商品关联
	 * @param linktype
	 * @param insList
	 */
	public void saveProductLink(String linktype, String insList) {
		List<BuyShopProduct> list = JSONArray.parseArray(insList,BuyShopProduct.class);
		for (BuyShopProduct shopProduct:list ) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id",shopProduct.getProductId());
			//获取商品信息
			BuyProductSku productSku = new BuyProductSku();
			List<BuyProductSku> productSkuList = buyProductSkuMapper.querySkuList(map);
			if(productSkuList!=null&&productSkuList.size()>0){
				productSku = productSkuList.get(0);
			}
			shopProduct.setId(ShiroUtils.getUid());
			//linktype 互通类型  0:与供应商互通,1:与客户互通
			if("0".equals(linktype)){
				shopProduct.setCompanyId(ShiroUtils.getCompId());
				shopProduct.setSupplierId(shopProduct.getLinkCompanyID());
				shopProduct.setBarcode(productSku.getBarcode());
				shopProduct.setSupplierBarcode(shopProduct.getLinkCompanyBarcode());
			}else {
				shopProduct.setCompanyId(shopProduct.getLinkCompanyID());
				shopProduct.setSupplierId(ShiroUtils.getCompId());
				shopProduct.setBarcode(shopProduct.getLinkCompanyBarcode());
				shopProduct.setSupplierBarcode(productSku.getBarcode());
			}
			shopProduct.setProductCode(productSku.getProductCode());
			shopProduct.setProductName(productSku.getProductName());
			shopProduct.setSkuCode(productSku.getSkuCode());
			shopProduct.setSkuName(productSku.getSkuName());
			shopProduct.setStatus("0");
			shopProduct.setIsDel(0);
			shopProduct.setCreateId(ShiroUtils.getUserId());
			shopProduct.setCreateDate(new Date());
			shopProduct.setCreateName(ShiroUtils.getUserName());
		}
		buyShopProductMapper.saveProductLink(list);
	}
	
	/**
	 * 保存商品关联并返回ID
	 * @param linktype
	 * @param insList
	 * @return
	 */
	public List<String> saveProductLinkGetIds(String linktype, String insList) {
		List<String> ids = new ArrayList<>();
		List<BuyShopProduct> list = JSONArray.parseArray(insList,BuyShopProduct.class);
		for (BuyShopProduct shopProduct:list ) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id",shopProduct.getProductId());
			//获取商品信息
			BuyProductSku productSku = new BuyProductSku();
			List<BuyProductSku> productSkuList = buyProductSkuMapper.querySkuList(map);
			if(productSkuList!=null&&productSkuList.size()>0){
				productSku = productSkuList.get(0);
			}
			shopProduct.setId(ShiroUtils.getUid());
			//linktype 互通类型  0:与供应商互通,1:与客户互通
			if("0".equals(linktype)){
				shopProduct.setCompanyId(ShiroUtils.getCompId());
				shopProduct.setSupplierId(shopProduct.getLinkCompanyID());
				shopProduct.setBarcode(productSku.getBarcode());
				shopProduct.setSupplierBarcode(shopProduct.getLinkCompanyBarcode());
			}else {
				shopProduct.setCompanyId(shopProduct.getLinkCompanyID());
				shopProduct.setSupplierId(ShiroUtils.getCompId());
				shopProduct.setBarcode(shopProduct.getLinkCompanyBarcode());
				shopProduct.setSupplierBarcode(productSku.getBarcode());
			}
			shopProduct.setProductCode(productSku.getProductCode());
			shopProduct.setProductName(productSku.getProductName());
			shopProduct.setSkuCode(productSku.getSkuCode());
			shopProduct.setSkuName(productSku.getSkuName());
			shopProduct.setUnitId(productSku.getUnitId());
			shopProduct.setStatus("1");
			shopProduct.setIsDel(0);
			shopProduct.setCreateId(ShiroUtils.getUserId());
			shopProduct.setCreateDate(new Date());
			shopProduct.setCreateName(ShiroUtils.getUserName());
			ids.add(shopProduct.getId());
		}
		buyShopProductMapper.saveProductLink(list);
		return ids;
	}
	/**
	 * 关联商品价格修改
	 * @param buyShopProductId
	 * @param updPrice
	 */
	public void updBuyShopProductPrice(String buyShopProductId, BigDecimal updPrice, String priceUpdPerson) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("buyShopProductId",buyShopProductId);
		map.put("updPrice",updPrice);
//		map.put("isPriceUpd",1);
		map.put("priceUpdDate",new Date());
		map.put("priceUpdPerson",priceUpdPerson);
		buyShopProductMapper.updBuyShopProductPrice(map);
	}

	/**
	 * 互通商品价格变更确认
	 * @param buyShopProductId
	 */
	public void comfirmShopProductPriceUpd(String buyShopProductId) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("buyShopProductId",buyShopProductId);
//		map.put("isPriceUpdConfirm",1);
		map.put("priceUpdComfirmDate",new Date());
		map.put("priceUpdComfirmPerson",ShiroUtils.getUserId());
		buyShopProductMapper.comfirmShopProductPriceUpd(map);
	}

	/**
	 * 采购商审批通过
	 * @param id
	 */
	public JSONObject buyerVerifySuccess(String id) {
		JSONObject json = new JSONObject();
//		BuyShopProduct shopProduct = buyShopProductMapper.selectByPrimaryKey(id);
		BuyShopProduct shopProductUpd = new BuyShopProduct();
		shopProductUpd.setId(id);
		//内部审批通过直接通过，已屏蔽待对方审批
		shopProductUpd.setStatus("3");
//		if(!ObjectUtil.isEmpty(shopProduct)){
//			SysUser creator = sysUserMapper.selectByPrimaryKey(shopProduct.getCreateId());//创建人
//			String creatorCompanyId = "";
//			if(!ObjectUtil.isEmpty(creator)){
//				creatorCompanyId = creator.getCompanyId();//创建人公司ID
//			}
//			if(!ObjectUtil.isEmpty(creatorCompanyId)){
//				if(creatorCompanyId==shopProduct.getCompanyId()){//创建人公司与采购商一致，则是内部审批通过，否则是对方审批通过
//					shopProductUpd.setStatus("2");
//				}else{
//					shopProductUpd.setStatus("3");
//				}
//
//			}else{
//				shopProductUpd.setStatus("1");
//			}
//		}else{
//			shopProductUpd.setStatus("1");
//		}
		int result = buyShopProductMapper.updateByPrimaryKeySelective(shopProductUpd);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	/**
	 * 采购商审批拒绝
	 * @param id
	 */
	public JSONObject buyerVerifyError(String id) {
		JSONObject json = new JSONObject();
		BuyShopProduct shopProductUpd = new BuyShopProduct();
		shopProductUpd.setId(id);
		//内部审批拒绝，设置状态为2
		shopProductUpd.setStatus("2");
		int result = buyShopProductMapper.updateByPrimaryKeySelective(shopProductUpd);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 供应商审批通过
	 * @param id
	 */
	public JSONObject sellerVerifySuccess(String id) {
		JSONObject json = new JSONObject();
		BuyShopProduct shopProduct = buyShopProductMapper.selectByPrimaryKey(id);
		BuyShopProduct shopProductUpd = new BuyShopProduct();
		shopProductUpd.setId(shopProduct.getId());
		if(!ObjectUtil.isEmpty(shopProduct)){
			SysUser creator = sysUserMapper.selectByPrimaryKey(shopProduct.getCreateId());//创建人
			String creatorCompanyId = "";
			if(!ObjectUtil.isEmpty(creator)){
				creatorCompanyId = creator.getCompanyId();//创建人公司ID
			}
			if(!ObjectUtil.isEmpty(creatorCompanyId)){
				if(creatorCompanyId==shopProduct.getSupplierId()){//创建人公司与供应商一致，则是内部审批通过，否则是对方审批通过
					shopProductUpd.setStatus("2");
				}else{
					shopProductUpd.setStatus("3");
				}

			}else{
				shopProductUpd.setStatus("1");
			}
		}else{
			shopProductUpd.setStatus("1");
		}
		int result = buyShopProductMapper.updateByPrimaryKeySelective(shopProductUpd);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	/**
	 * 供应商审批拒绝
	 * @param id
	 */
	public JSONObject sellerVerifyError(String id) {
		JSONObject json = new JSONObject();
		json.put("success", true);
		json.put("msg", "成功！");
		return json;
	}

	public Integer[] getCount() {
		Map<String,Object> map = new HashMap<>();
		map.put("companyId",ShiroUtils.getCompId());
		int allCount = buyShopProductMapper.getProductCount(map);
		map.put("status","1");
		int internalCount = buyShopProductMapper.getProductCount(map);
		map.put("status","2");
		int externalCount = buyShopProductMapper.getProductCount(map);
		map.put("status","3");
		int okCount =buyShopProductMapper.getProductCount(map);
		map.put("status","4");
		int cancelCount =buyShopProductMapper.getProductCount(map);
		return new Integer[]{allCount,internalCount,externalCount,okCount,cancelCount};
	}

	/**
	 * Check product link json object.
	 * 检查新增的关联是否已存在
	 * @param params the params
	 * @return the json object
	 */
	public JSONObject checkProductLink(Map<String, Object> params) {
		JSONObject jsonObject = new JSONObject();
		List<BuyShopProduct> list = JSONArray.parseArray((String) params.get("data"),BuyShopProduct.class);
		for (BuyShopProduct shopProduct:list ) {
			Map<String,Object> map = new HashMap<>(3);
			map.put("companyId",ShiroUtils.getCompId());
			map.put("supplierId",shopProduct.getLinkCompanyID());
			map.put("barcode",shopProduct.getBarcode());
			List<BuyShopProduct> shopProductList = buyShopProductMapper.queryList(map);
			if(!ObjectUtil.isEmpty(shopProductList)&&shopProductList.size()>0){
				jsonObject.put("success",false);
				jsonObject.put("msg","条形码为:"+shopProduct.getBarcode()+"的商品已于该供应商关联！");
				return jsonObject;
			}
		}
		jsonObject.put("success",true);
		jsonObject.put("msg","");
		return jsonObject;
	}
	
	/**
	 * 取消商品关联
	 * @param id
	 * @return
	 */
	public void cancelProductLink(String id) {
		BuyShopProduct shopProduct = buyShopProductMapper.get(id);
		//设置为取消状态
		shopProduct.setStatus("4");
		shopProduct.setIsDel(Constant.IsDel.YESDEL.getValue());
		shopProduct.setUpdateId(ShiroUtils.getUserId());
		shopProduct.setUpdateName(ShiroUtils.getUserName());
		shopProduct.setUpdateDate(new Date());
		buyShopProductMapper.updateByPrimaryKey(shopProduct);
	}
}
