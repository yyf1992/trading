package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.model.SysVerifyCopy;



@Service
@Transactional
public class SysVerifyCopyService {

    private static final Logger LOG = LoggerFactory.getLogger(SysVerifyCopyService.class);

	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	
	public SysVerifyCopy get(String id){
		return sysVerifyCopyMapper.get(id);
	}
	
	public List<SysVerifyCopy> queryList(Map<String, Object> map){
		return sysVerifyCopyMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysVerifyCopyMapper.queryCount(map);
	}
	
	public void add(SysVerifyCopy sysVerifyCopy){
		sysVerifyCopyMapper.add(sysVerifyCopy);
	}
	
	public void update(SysVerifyCopy sysVerifyCopy){
		sysVerifyCopyMapper.update(sysVerifyCopy);
	}
	
	public void delete(String id){
		sysVerifyCopyMapper.delete(id);
	}

	public List<SysVerifyCopy> getListByHeaderId(String headerId) {
		return sysVerifyCopyMapper.getListByHeaderId(headerId);
	}
	

}
