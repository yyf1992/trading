package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.model.BuyUnit;

public interface BuyUnitMapper {
    int deleteByPrimaryKey(String id);
    
    int insert(BuyUnit record);

    int insertSelective(BuyUnit record);

    BuyUnit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyUnit record);

    int updateByPrimaryKey(BuyUnit record);
    
    //获取所有的单位
  	public List<BuyUnit> getBuyUnitList();

	List<BuyUnit> selectByMap(Map<String, Object> map);

	int delUnit(String id);
}