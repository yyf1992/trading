package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 权限角色
 * 
 * @author "
 * @date 2017-09-14 09:23:05
 */
public interface TradeRoleMapper extends BaseDao<TradeRole> {

	List<TradeRole> getPageList(SearchPageUtil searchPageUtil);
	List<TradeRole> getRoleByMap(Map<String,Object> params);
	int isAdminRoleByUserId(Map<String, Object> params);
	int isVerifyByMap(Map<String, Object> params);
}
