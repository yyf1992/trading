package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.SellerBillCycleManagementMapper;
import com.nuotai.trading.seller.model.SellerBillCycleManagement;
import com.nuotai.trading.seller.model.SellerBillCycleManagementOld;
import com.nuotai.trading.seller.model.SellerBillInterestOld;
import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagement;
import com.nuotai.trading.seller.service.buyer.SBuyBillCycleService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 账单周期卖家
 * @author yuyafei
 * @date 2017-8-17
 *
 */
@Service
public class SellerBillCycleService {
	@Autowired
	private SellerBillCycleManagementMapper sellerBillCycleMapper;
	@Autowired
	private SBuyBillCycleService SBuyBillCycleService;
	@Autowired
	private SellerBillCycleOldService sellerBillCycleOldService;
	@Autowired
	private SellerBillCycleInterestService sellerBillCycleInterestService;
	@Autowired
	private SellerBillInterestOldService sellerBillInterestOldService;

	//查询账单周期列表信息
	public List<Map<String, Object>> getSellerBillCycleList(
			SearchPageUtil searchPageUtil) {
		return sellerBillCycleMapper.getSellerBillCycleList(searchPageUtil);
	}

	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return sellerBillCycleMapper.queryBillStatusCount(map);
	}
	//向账单周期管理页面提供不同状态下的数量
	public void getBillCountByStatus(Map<String, Object> billMap){
		//Map<String, Object> billCycleCountMap = new HashMap<String, Object>();
		//查询所有状态数量
		/*billMap.put("billDealStatus", "");
		int allBillCycleCount = queryBillStatusCount(billMap);*/
		//查询待内部审批周期数
		billMap.put("billStatusToCount", "2");
		int approvalBillCycleCount = queryBillStatusCount(billMap);
		//查询待对方审批周期数
		billMap.put("billStatusToCount", "3");
		int acceptBillCycleCount = queryBillStatusCount(billMap);
		//查询审批已通过周期数
		billMap.put("billStatusToCount", "5");
		int apprEndBillCycleCount = queryBillStatusCount(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillCycleCount", approvalBillCycleCount);
		billMap.put("acceptBillCycleCount", acceptBillCycleCount);
		billMap.put("apprEndBillCycleCount", apprEndBillCycleCount);

	}

	//根据编号修改账单及关联利息表信息
	public int updateSaveBillCycleInfo(Map<String, Object> updateSavemap) {
		int updateCount = 0;
		if(null != updateSavemap){
			SellerBillCycleManagement sellerBillCycle = new SellerBillCycleManagement();//卖家实体bean
			SBuyBillCycleManagement sBuyBillCycleManagement = new SBuyBillCycleManagement();//买家实体bean
			String billDealStatus = (String) updateSavemap.get("billDealStatus");//修改状态
			String billCycleId = (String) updateSavemap.get("billCycleId");//账单周期编号
			String dealRemarks = (String) updateSavemap.get("dealRemarks");
			String onselfCompId = ShiroUtils.getCompId();//操作人所在公司
			String onselfId = ShiroUtils.getUserId();//操作人id
			//判断审批状态
			if("3".equals(billDealStatus)){//审批通过
				//修改卖家状态
				sellerBillCycle.setId(billCycleId);
				sellerBillCycle.setBillDealStatus(billDealStatus);
				sellerBillCycle.setAcceptRemarks(dealRemarks);
				updateCount =  updateByPrimaryKeySelective(sellerBillCycle);//修改卖家

				sBuyBillCycleManagement.setId(billCycleId);
				sBuyBillCycleManagement.setBillDealStatus(billDealStatus);
				sBuyBillCycleManagement.setAcceptRemarks(dealRemarks);
				SBuyBillCycleService.updateByPrimaryKeySelective(sBuyBillCycleManagement);//修改买家
			}else if("5".equals(billDealStatus)){//审批驳回
				//根据编号查询历史账单周期
				SellerBillCycleManagementOld sellerBillCycleManagementOld = sellerBillCycleOldService.queryLastDateBillCycleOld(billCycleId);
				//判断是否有历史账单信息
				if(null != sellerBillCycleManagementOld){
					//回复卖家历史数据
					sellerBillCycle.setId(billCycleId);
					sellerBillCycle.setBuyCompanyId(sellerBillCycleManagementOld.getBuyCompanyId());
					sellerBillCycle.setSellerCompanyId(sellerBillCycleManagementOld.getSellerCompanyId());
					sellerBillCycle.setCycleStartDate(sellerBillCycleManagementOld.getCycleStartDate());
					sellerBillCycle.setCycleEndDate(sellerBillCycleManagementOld.getCycleEndDate());
					sellerBillCycle.setCheckoutCycle(sellerBillCycleManagementOld.getCheckoutCycle());
					sellerBillCycle.setBillStatementDate(sellerBillCycleManagementOld.getBillStatementDate());
					sellerBillCycle.setBillDealStatus(sellerBillCycleManagementOld.getBillDealStatus());
					sellerBillCycle.setOutStatementDate(sellerBillCycleManagementOld.getOutStatementDate());
					sellerBillCycle.setCreateUser(sellerBillCycleManagementOld.getCreateUser());
					sellerBillCycle.setCreateTime(sellerBillCycleManagementOld.getCreateTime());
					//恢复卖家账单周期
					updateCount = updateByPrimaryKeySelective(sellerBillCycle);
					//回退更新申请表历史数据
					sBuyBillCycleManagement.setId(billCycleId);
					sBuyBillCycleManagement.setBuyCompanyId(sellerBillCycleManagementOld.getBuyCompanyId());
					sBuyBillCycleManagement.setSellerCompanyId(sellerBillCycleManagementOld.getSellerCompanyId());
					sBuyBillCycleManagement.setCycleStartDate(sellerBillCycleManagementOld.getCycleStartDate());
					sBuyBillCycleManagement.setCycleEndDate(sellerBillCycleManagementOld.getCycleEndDate());
					sBuyBillCycleManagement.setCheckoutCycle(sellerBillCycleManagementOld.getCheckoutCycle());
					sBuyBillCycleManagement.setBillStatementDate(sellerBillCycleManagementOld.getBillStatementDate());
					sBuyBillCycleManagement.setBillDealStatus(sellerBillCycleManagementOld.getBillDealStatus());
					sBuyBillCycleManagement.setOutStatementDate(sellerBillCycleManagementOld.getOutStatementDate());
					sBuyBillCycleManagement.setCreateUser(sellerBillCycleManagementOld.getCreateUser());
					sBuyBillCycleManagement.setCreateTime(sellerBillCycleManagementOld.getCreateTime());
					//驳回后恢复历史账单周期数据
					SBuyBillCycleService.updateByPrimaryKeySelective(sBuyBillCycleManagement);

					if(updateCount >0){
						//删除申请表中关联的利息
						sellerBillCycleInterestService.deleteByPrimaryKey(billCycleId);

						String lasteDateBillCycleOldId = sellerBillCycleManagementOld.getId();
						//根据历史最近账单周期编号查询关联利息
						List<SellerBillInterestOld> billInterestOldList = sellerBillInterestOldService.queryBillInterestOld(lasteDateBillCycleOldId);

						//驳回后恢复历史账单关联利息
						for(SellerBillInterestOld buyBillInterestOld : billInterestOldList){
							Map<String, Object> interMap = new HashMap<String, Object>();
							interMap.put("id", ShiroUtils.getUid());
							interMap.put("billCycleId", billCycleId);
							interMap.put("overdueDate", buyBillInterestOld.getOverdueDate());
							interMap.put("overdueInterest", buyBillInterestOld.getOverdueInterest());
							interMap.put("interestCalculationMethod", buyBillInterestOld.getInterestCalculationMethod());
							sellerBillCycleInterestService.saveInterestInfo(interMap);
						}
						//根据账单周期编号删除历史最近周期数据
						int deleteOldCycleCount = sellerBillCycleOldService.deleteNewBillCycleOld(lasteDateBillCycleOldId);
						//根据账单周期编号删除历史账单周期关联历史利息
						int deleteInterestOldCount = sellerBillInterestOldService.deleteNewInterestOld(lasteDateBillCycleOldId);
					}
				}else {
					//修改卖家状态
					sellerBillCycle.setId(billCycleId);
					sellerBillCycle.setBillDealStatus(billDealStatus);
					sellerBillCycle.setAcceptRemarks(dealRemarks);
					updateCount =  updateByPrimaryKeySelective(sellerBillCycle);//修改卖家

					sBuyBillCycleManagement.setId(billCycleId);
					sBuyBillCycleManagement.setBillDealStatus(billDealStatus);
					sBuyBillCycleManagement.setAcceptRemarks(dealRemarks);
					SBuyBillCycleService.updateByPrimaryKeySelective(sBuyBillCycleManagement);//修改买家
				}
			}
		}
		return updateCount;
	}

	//卖家审批通过
	/*public JSONObject vSellerBillCycleSuccess(String id) {
		JSONObject json = new JSONObject();
		Map<String, Object> map = new HashMap<String,Object>();
		SellerBillCycleManagement sellerBillCycleManagement = new SellerBillCycleManagement();
		sellerBillCycleManagement.setId(id);
		sellerBillCycleManagement.setBillDealStatus("3");
		int result =  updateByPrimaryKeySelective(sellerBillCycleManagement);

		//同步到买家
		SBuyBillCycleManagement sBuyBillCycleManagement = new SBuyBillCycleManagement();
		sBuyBillCycleManagement.setId(id);
		sBuyBillCycleManagement.setBillDealStatus("3");
		SBuyBillCycleService.updateByPrimaryKeySelective(sBuyBillCycleManagement);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}*/

	//买家内部驳回
	/*public JSONObject vSellerBillCycleError(String id) {
		JSONObject json = new JSONObject();
		SellerBillCycleManagement sellerBillCycle = new SellerBillCycleManagement();//卖家账单周期
		SBuyBillCycleManagement buyBillCycleManagement = new SBuyBillCycleManagement();//买家账单周期

		//根据编号查询历史账单周期
		SellerBillCycleManagementOld sellerBillCycleManagementOld = sellerBillCycleOldService.queryLastDateBillCycleOld(id);
		int sellerBillCycleCount = 0;
		if(null != sellerBillCycleManagementOld){
			//回复卖家历史数据
			sellerBillCycle.setId(id);
			sellerBillCycle.setBuyCompanyId(sellerBillCycleManagementOld.getBuyCompanyId());
			sellerBillCycle.setSellerCompanyId(sellerBillCycleManagementOld.getSellerCompanyId());
			sellerBillCycle.setCycleStartDate(sellerBillCycleManagementOld.getCycleStartDate());
			sellerBillCycle.setCycleEndDate(sellerBillCycleManagementOld.getCycleEndDate());
			sellerBillCycle.setCheckoutCycle(sellerBillCycleManagementOld.getCheckoutCycle());
			sellerBillCycle.setBillStatementDate(sellerBillCycleManagementOld.getBillStatementDate());
			sellerBillCycle.setBillDealStatus(sellerBillCycleManagementOld.getBillDealStatus());
			sellerBillCycle.setCreateUser(sellerBillCycleManagementOld.getCreateUser());
			sellerBillCycle.setCreateTime(sellerBillCycleManagementOld.getCreateTime());
			//恢复卖家账单周期
			sellerBillCycleCount = updateByPrimaryKeySelective(sellerBillCycle);

			//回退更新申请表历史数据
			buyBillCycleManagement.setId(id);
			buyBillCycleManagement.setBuyCompanyId(sellerBillCycleManagementOld.getBuyCompanyId());
			buyBillCycleManagement.setSellerCompanyId(sellerBillCycleManagementOld.getSellerCompanyId());
			buyBillCycleManagement.setCycleStartDate(sellerBillCycleManagementOld.getCycleStartDate());
			buyBillCycleManagement.setCycleEndDate(sellerBillCycleManagementOld.getCycleEndDate());
			buyBillCycleManagement.setCheckoutCycle(sellerBillCycleManagementOld.getCheckoutCycle());
			buyBillCycleManagement.setBillStatementDate(sellerBillCycleManagementOld.getBillStatementDate());
			buyBillCycleManagement.setBillDealStatus(sellerBillCycleManagementOld.getBillDealStatus());
			buyBillCycleManagement.setCreateUser(sellerBillCycleManagementOld.getCreateUser());
			buyBillCycleManagement.setCreateTime(sellerBillCycleManagementOld.getCreateTime());
			//驳回后恢复历史账单周期数据
			SBuyBillCycleService.updateByPrimaryKeySelective(buyBillCycleManagement);

			if(sellerBillCycleCount >0){
				//删除申请表中关联的利息
				sellerBillCycleInterestService.deleteByPrimaryKey(id);

				String lasteDateBillCycleOldId = sellerBillCycleManagementOld.getId();
				//根据历史最近账单周期编号查询关联利息
				List<SellerBillInterestOld> billInterestOldList = sellerBillInterestOldService.queryBillInterestOld(lasteDateBillCycleOldId);

				//驳回后恢复历史账单关联利息
				for(SellerBillInterestOld buyBillInterestOld : billInterestOldList){
					Map<String, Object> interMap = new HashMap<String, Object>();
					interMap.put("id", ShiroUtils.getUid());
					interMap.put("billCycleId", id);
					interMap.put("overdueDate", buyBillInterestOld.getOverdueDate());
					interMap.put("overdueInterest", buyBillInterestOld.getOverdueInterest());
					interMap.put("interestCalculationMethod", buyBillInterestOld.getInterestCalculationMethod());
					sellerBillCycleInterestService.saveInterestInfo(interMap);
				}
				//根据账单周期编号删除历史最近周期数据
				int deleteOldCycleCount = sellerBillCycleOldService.deleteNewBillCycleOld(lasteDateBillCycleOldId);
				//根据账单周期编号删除历史账单周期关联历史利息
				int deleteInterestOldCount = sellerBillInterestOldService.deleteNewInterestOld(lasteDateBillCycleOldId);
				if(deleteInterestOldCount>0){
					json.put("success", true);
					json.put("msg", "成功！");
				}else{
					json.put("success", false);
					json.put("msg", "失败！");
				}
			}
		}else{
			//修改卖家状态
			sellerBillCycle.setId(id);
			sellerBillCycle.setBillDealStatus("5");
			int result =  updateByPrimaryKeySelective(sellerBillCycle);

			//同步到买家
			//SBuyBillCycleManagement sBuyBillCycleManagement = new SBuyBillCycleManagement();
			buyBillCycleManagement.setId(id);
			buyBillCycleManagement.setBillDealStatus("5");
			SBuyBillCycleService.updateByPrimaryKeySelective(buyBillCycleManagement);

			if(result>0){
				json.put("success", true);
				json.put("msg", "成功！");
			}else{
				json.put("success", false);
				json.put("msg", "失败！");
			}
		}
		return json;
	}*/

	public int deleteByPrimaryKey(String id) {
		return sellerBillCycleMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(SellerBillCycleManagement record) {
		return 0;
	}

	//根据编号查询数据
	public SellerBillCycleManagement selectByPrimaryKey(String id) {
		return sellerBillCycleMapper.selectByPrimaryKey(id);
	}

	//根据买家修改的数据同步到卖家数据库中
	public int updateByPrimaryKeySelective(SellerBillCycleManagement record) {
		return sellerBillCycleMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(SellerBillCycleManagement record) {
		return 0;
	}

	
}
