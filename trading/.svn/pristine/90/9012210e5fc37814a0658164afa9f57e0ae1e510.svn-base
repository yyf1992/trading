<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordItemMapper">
	<sql id="all">
		  id , 	 
		  record_id AS recordId , 	  
		  order_id AS orderId ,
		  order_item_id AS orderItemId ,
		  delivery_item_id AS deliveryItemId ,
		  apply_code AS applyCode ,
		  product_code AS productCode , 	  
		  product_name AS productName , 	  
		  sku_code AS skuCode , 	  
		  sku_name AS skuName , 	  
		  barcode , 	 
		  unit_id AS unitId ,
		  sale_price AS salePrice ,
		  warehouse_id AS warehouseId , 	  
		  warehouse_code AS warehouseCode , 	  
		  warehouse_name AS warehouseName , 	  
		  delivery_num AS deliveryNum , 	  
		  delivered_num AS deliveredNum ,
		  arrival_num AS arrivalNum ,
		  arrival_date AS arrivalDate , 	  
		  warehouse_holder AS warehouseHolder , 	  
		  status ,
		  is_need_invoice AS isNeedInvoice,
		  remark 	 
	   </sql>

	<sql id="allNoAs">
			id , 
			record_id , 
			order_id ,
			order_item_id ,
			delivery_item_id ,
			apply_code ,
			product_code , 
			product_name , 
			sku_code , 
			sku_name , 
			barcode ,
			unit_id ,
			sale_price ,
			warehouse_id , 
			warehouse_code , 
			warehouse_name , 
			delivery_num , 
			delivered_num ,
			arrival_num ,
			arrival_date , 
			warehouse_holder , 
			status ,
			is_need_invoice ,
			remark 
	</sql>

	<sql id="BuyDeliveryRecordItem_condition">
   	<if test="recordId != null and recordId != ''">
		  AND record_id = #{recordId}  
	 </if>
   	<if test="orderId != null and orderId != ''">
		  AND order_id = #{orderId}  
	 </if>
	<if test="deliveryItemId != null and deliveryItemId != ''">
		AND delivery_item_id = #{deliveryItemId}
	</if>
	<if test="applyCode != null and applyCode != ''">
		AND apply_code = #{applyCode}
	</if>
   	<if test="productCode != null and productCode != ''">
		  AND product_code = #{productCode}  
	 </if>
   	<if test="productName != null and productName != ''">
		  AND product_name = #{productName}  
	 </if>
   	<if test="skuCode != null and skuCode != ''">
		  AND sku_code = #{skuCode}  
	 </if>
   	<if test="skuName != null and skuName != ''">
		  AND sku_name = #{skuName}  
	 </if>
   	<if test="barcode != null and barcode != ''">
		  AND barcode = #{barcode}  
	 </if>
   	<if test="salePrice != null and salePrice != 0 ">
		  AND sale_price = #{salePrice}  
	 </if>
   	<if test="warehouseId != null and warehouseId != ''">
		  AND warehouse_id = #{warehouseId}  
	 </if>
   	<if test="warehouseCode != null and warehouseCode != ''">
		  AND warehouse_code = #{warehouseCode}  
	 </if>
   	<if test="warehouseName != null and warehouseName != ''">
		  AND warehouse_name = #{warehouseName}  
	 </if>
   	<if test="deliveryNum != null and deliveryNum != 0 ">
		  AND delivery_num = #{deliveryNum}  
	 </if>
   	<if test="deliveredNum != null and deliveredNum != 0 ">
		  AND delivered_num = #{deliveredNum}
	 </if>
   	<if test="arrivalNum != null and arrivalNum != 0 ">
		  AND arrival_num = #{arrivalNum}  
	 </if>
   	<if test="arrivalDate != null">
		  AND arrival_date = #{arrivalDate}  
	 </if>
   	<if test="warehouseHolder != null and warehouseHolder != ''">
		  AND warehouse_holder = #{warehouseHolder}  
	 </if>
   	<if test="status != null and status != 0 ">
		  AND status = #{status}  
	 </if>
	<if test="isNeedInvoice != null ">
		AND is_need_invoice = #{isNeedInvoice}
	</if>
   	<if test="remark != null and remark != ''">
		  AND remark = #{remark}
	 </if>
  	</sql>

	<select id="get" resultType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem">
		SELECT
			item.id ,
			item.record_id AS recordId ,
			item.order_id AS orderId ,
			bo.order_code AS orderCode ,
			item.order_item_id AS orderItemId ,
			item.delivery_item_id AS deliveryItemId ,
			item.apply_code AS applyCode ,
			boItem.goods_number AS orderNum,
			item.product_code AS productCode ,
			item.product_name AS productName ,
			item.sku_code AS skuCode ,
			item.sku_name AS skuName ,
			item.barcode ,
			item.unit_id AS unitId ,
			unit.unit_name AS unitName,
			IFNULL(item.sale_price,0) AS salePrice ,
			item.warehouse_id AS warehouseId ,
			warehouse.wharea_code AS warehouseCode ,
			warehouse.wharea_name AS warehouseName ,
			IFNULL(item.delivery_num,0) AS deliveryNum ,
			item.arrival_date AS arrivalDate ,
			IFNULL(item.arrival_num,0) AS arrivalNum ,
			item.warehouse_holder AS warehouseHolder ,
			item.status ,
			item.is_need_invoice AS isNeedInvoice,
			item.remark,
			IFNULL(boItem.arrival_num,0) AS orderArrivalNum,
			plan.shop_id AS shopId,
			plan.shop_code AS shopCode,
			plan.shop_name AS  shopName,
			bo.order_type AS orderType
		FROM buy_delivery_record_item item
		LEFT JOIN buy_order bo     ON bo.id = item.order_id
		LEFT JOIN buy_order_product boItem     ON boItem.id = item.order_item_id
		LEFT JOIN t_business_wharea warehouse on warehouse.id = item.warehouse_id
		LEFT JOIN buy_unit unit on unit.id = item.unit_id
		LEFT JOIN buy_applypurchase_header plan on plan.apply_code = boItem.apply_code
		WHERE
		item.id = #{value}
	</select>


	<select id="queryList" parameterType = "map" resultType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem">
		SELECT
		item.id ,
		item.record_id AS recordId ,
		item.order_id AS orderId ,
		bo.order_code AS  orderCode ,
		item.order_item_id AS orderItemId ,
		item.delivery_item_id AS deliveryItemId ,
		item.apply_code AS applyCode ,
		boItem.goods_number AS orderNum,
		item.product_code AS productCode ,
		item.product_name AS productName ,
		item.sku_code AS skuCode ,
		item.sku_name AS skuName ,
		item.barcode ,
		item.unit_id AS unitId ,
		unit.unit_name AS unitName,
		IFNULL(item.sale_price,0) AS salePrice ,
		item.warehouse_id AS warehouseId ,
		warehouse.wharea_code AS warehouseCode ,
		warehouse.wharea_name AS warehouseName ,
		IFNULL(item.delivery_num,0) AS deliveryNum ,
		item.arrival_date AS arrivalDate ,
		IFNULL(item.arrival_num,0) AS arrivalNum ,
		item.warehouse_holder AS warehouseHolder ,
		item.status ,
		item.is_need_invoice AS isNeedInvoice,
		item.remark,
		IFNULL(boItem.arrival_num,0) AS orderArrivalNum
		FROM buy_delivery_record_item item
		LEFT JOIN buy_order bo     ON bo.id = item.order_id
		LEFT JOIN buy_order_product boItem     ON boItem.id = item.order_item_id
		LEFT JOIN buy_unit unit on unit.id = item.unit_id
		LEFT JOIN t_business_wharea warehouse on warehouse.id = item.warehouse_id
		<where>
			<if test="recordId != null and recordId != ''">
				AND item.record_id = #{recordId}
			</if>
			<if test="orderId != null and orderId != ''">
				AND item.order_id = #{orderId}
			</if>
			<if test="orderItemId != null and orderItemId != ''">
				AND item.order_item_id = #{orderItemId}
			</if>
			<if test="deliveryItemId != null and deliveryItemId != ''">
				AND item.delivery_item_id = #{deliveryItemId}
			</if>
			<if test="applyCode != null and applyCode != ''">
				AND item.apply_code = #{applyCode}
			</if>
			<if test="productCode != null and productCode != ''">
				AND item.product_code = #{productCode}
			</if>
			<if test="productName != null and productName != ''">
				AND item.product_name = #{productName}
			</if>
			<if test="skuCode != null and skuCode != ''">
				AND item.sku_code = #{skuCode}
			</if>
			<if test="skuName != null and skuName != ''">
				AND item.sku_name = #{skuName}
			</if>
			<if test="barcode != null and barcode != ''">
				AND item.barcode = #{barcode}
			</if>
			<if test="salePrice != null and salePrice != 0 ">
				AND item.sale_price = #{salePrice}
			</if>
			<if test="warehouseId != null and warehouseId != ''">
				AND item.warehouse_id = #{warehouseId}
			</if>
			<if test="warehouseCode != null and warehouseCode != ''">
				AND item.warehouse_code = #{warehouseCode}
			</if>
			<if test="warehouseName != null and warehouseName != ''">
				AND item.warehouse_name = #{warehouseName}
			</if>
			<if test="recordNumber != null and recordNumber != 0 ">
				AND item.record_number = #{recordNumber}
			</if>
			<if test="sendNumber != null and sendNumber != 0 ">
				AND item.send_number = #{sendNumber}
			</if>
			<if test="arrivalNumber != null and arrivalNumber != 0 ">
				AND item.arrival_number = #{arrivalNumber}
			</if>
			<if test="arrivalDate != null">
				AND item.arrival_date = #{arrivalDate}
			</if>
			<if test="warehouseHolder != null and warehouseHolder != ''">
				AND item.warehouse_holder = #{warehouseHolder}
			</if>
			<if test="status != null and status != 0 ">
				AND item.status = #{status}
			</if>
			<if test="isNeedInvoice != null ">
				AND item.is_need_invoice = #{isNeedInvoice}
			</if>
			<if test="remark != null and remark != ''">
				AND item.remark = #{remark}
			</if>
		</where>
	</select>

 	<select id="queryCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_delivery_record_item
		WHERE
		del_date is null
		<include refid="BuyDeliveryRecordItem_condition"/>
	</select>
	 
	<insert id="add" parameterType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem">
		INSERT into buy_delivery_record_item
		(
         <include refid="allNoAs" />
		)
		VALUES
		(
	    #{id}, 
	    #{recordId}, 
	    #{orderId},
	    #{orderItemId},
		#{deliveryItemId},
		#{applyCode},
	    #{productCode}, 
	    #{productName}, 
	    #{skuCode}, 
	    #{skuName}, 
	    #{barcode}, 
	    #{unitId},
	    #{salePrice},
	    #{warehouseId}, 
	    #{warehouseCode}, 
	    #{warehouseName}, 
	    #{deliveryNum}, 
	    #{deliveredNum},
	    #{arrivalNum},
	    #{arrivalDate}, 
	    #{warehouseHolder}, 
	    #{status}, 
	    #{isNeedInvoice},
	    #{remark}
		)
	</insert>
	 
	<update id="update" parameterType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem">
		UPDATE buy_delivery_record_item  SET
				record_id = #{recordId}, 			
				order_id = #{orderId},
				order_item_id = #{orderItemId},
				delivery_item_id = #{deliveryItemId},
				apply_code = #{applyCode},
				product_code = #{productCode}, 			
				product_name = #{productName}, 			
				sku_code = #{skuCode}, 			
				sku_name = #{skuName}, 			
				barcode = #{barcode}, 			
				unit_id = #{unitId},
				sale_price = #{salePrice},
				warehouse_id = #{warehouseId}, 			
				warehouse_code = #{warehouseCode}, 			
				warehouse_name = #{warehouseName}, 			
				delivery_num = #{deliveryNum}, 			
				arrival_num = #{arrivalNum},
				arrival_date = #{arrivalDate}, 			
				warehouse_holder = #{warehouseHolder}, 			
				status = #{status}, 			
				is_need_invoice = #{isNeedInvoice},
				remark = #{remark}
		WHERE id = #{id}
	</update>

	<update id="updateByPrimaryKeySelective" parameterType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem" >
		update buy_delivery_record_item
		<set >
			<if test="arrivalNum != null" >
				arrival_num = #{arrivalNum},
			</if>
			<if test="arrivalDate != null" >
				arrival_date = #{arrivalDate},
			</if>
		</set>
		WHERE id = #{id}
	</update>
	
	<delete id="delete">
		UPDATE buy_delivery_record_item  SET
           del_id = #{delId},
           del_name  = #{delName},
           del_date = #{delDate}
		WHERE id = #{id}
	</delete>

	<select id="getItemByDeliveryItemId" resultType="com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem">
		SELECT
		<include refid="all" />
		FROM buy_delivery_record_item
		WHERE
		delivery_item_id = #{value}
	</select>
	<select id="getItemByRecordId" resultType="map" parameterType="java.lang.String">
		SELECT a.*,u.dingding_id FROM (
			SELECT o.order_code,o.create_id,o.create_name,o.create_date,
			r.product_code,r.product_name,r.sku_code,r.sku_name,r.barcode,r.delivery_num,
			r.arrival_num,r.arrival_date 
			FROM buy_delivery_record_item r
			LEFT JOIN buy_delivery_record d ON d.id=r.record_id
			LEFT JOIN buy_order o ON o.id=r.order_id
			WHERE d.deliver_no=#{deliveryNo}
		)a LEFT JOIN sys_user u ON u.id=a.create_id
	</select>
</mapper>