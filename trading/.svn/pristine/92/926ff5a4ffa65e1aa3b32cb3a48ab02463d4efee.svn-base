<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.seller.dao.buyer.SBuyCustomerItemMapper">
	<sql id="all">
		  id , 	 
		  customer_id AS customerId ,
		  apply_code AS applyCode ,
		  product_code AS productCode ,
		  product_name AS productName , 	  
		  sku_code AS skuCode , 	  
		  sku_name AS skuName , 	  
		  sku_oid AS skuOid , 	  
		  unit_id AS unitId , 	  
		  delivery_code AS deliveryCode , 	  
		  delivery_item_id AS deliveryItemId,
		  goods_number AS goodsNumber ,
		  price AS price,
		  receive_number AS receiveNumber,
		  ware_house_id AS wareHouseId,
		  is_need_invoice AS isNeedInvoice,
		  remark AS remark,
		  repair_price AS repairPrice,
		  confirm_number AS confirmNumber ,
		  exchange_price AS exchangePrice
	   </sql>

	<sql id="allNoAs">
			id , 
			customer_id , 
			apply_code ,
			product_code ,
			product_name , 
			sku_code , 
			sku_name , 
			sku_oid , 
			unit_id , 
			delivery_code , 
			delivery_item_id,
			goods_number ,
			price,
			receive_number,
			ware_house_id ,
		    is_need_invoice,
		    remark,
		    repair_price,
		    confirm_number ,
		    exchange_price
	</sql>

	<sql id="BuyCustomerServiceItem_condition">
   	<if test="customerId != null and customerId != ''">
		  AND customer_id = #{customerId}  
	 </if>
	<if test="applyCode != null and applyCode != ''">
		AND apply_code = #{applyCode}
	</if>
   	<if test="productCode != null and productCode != ''">
		  AND product_code = #{productCode}  
	 </if>
   	<if test="productName != null and productName != ''">
		  AND product_name = #{productName}  
	 </if>
   	<if test="skuCode != null and skuCode != ''">
		  AND sku_code = #{skuCode}  
	 </if>
   	<if test="skuName != null and skuName != ''">
		  AND sku_name = #{skuName}  
	 </if>
   	<if test="skuOid != null and skuOid != ''">
		  AND sku_oid = #{skuOid}  
	 </if>
   	<if test="unitId != null and unitId != ''">
		  AND unit_id = #{unitId}  
	 </if>
   	<if test="deliveryCode != null and deliveryCode != ''">
		  AND delivery_code = #{deliveryCode}  
	 </if>
	 <if test="deliveryItemId != null and deliveryItemId != ''">
		  AND delivery_item_id = #{deliveryItemId}  
	 </if>
   	<if test="goodsNumber != null and goodsNumber != 0 ">
		  AND goods_number = #{goodsNumber}
	 </if>
	 <if test="price != null and price != '' ">
		  AND price = #{price}
	 </if>
	 <if test="receiveNumber != null and receiveNumber != 0">
	 	AND receive_number = #{receiveNumber}
	 </if>
	 <if test="wareHouseId != null and wareHouseId != ''">
		  AND ware_house_id = #{wareHouseId}  
	 </if>
	<if test="isNeedInvoice != null and isNeedInvoice != ''">
		  AND is_need_invoice = #{isNeedInvoice}  
	 </if>
	<if test="remark != null and remark != ''">
		  AND remark = #{remark}  
	 </if>
	 <if test="repairPrice != null and repairPrice != ''">
		  AND repair_price = #{repairPrice}  
	 </if>
	 <if test="confirmNumber != null and confirmNumber != ''">
		  AND confirm_number = #{confirmNumber}  
	 </if>
	 <if test="exchangePrice != null and repairPrice != 0 ">
		  AND exchange_price = #{exchangePrice}  
	 </if>
  	</sql>

	<select id="get" resultType="com.nuotai.trading.seller.model.buyer.SBuyCustomerItem">
		SELECT 
		<include refid="all" />
		FROM buy_customer_item 
		WHERE
		id = #{value}
	</select>

	<select id="queryList" parameterType = "map" resultType="com.nuotai.trading.seller.model.buyer.SBuyCustomerItem">
		SELECT
		<include refid="all" />
		FROM buy_customer_item
		WHERE
		del_date is null
		<include refid="BuyCustomerServiceItem_condition"/>
	</select>

 	<select id="queryCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_customer_item
		WHERE
		del_date is null
		<include refid="BuyCustomerServiceItem_condition"/>
	</select>
	 
	<insert id="add" parameterType="com.nuotai.trading.seller.model.buyer.SBuyCustomerItem">
		INSERT into buy_customer_item
		(
         <include refid="allNoAs" />
		)
		VALUES
		(
	    #{id}, 
	    #{customerId}, 
	    #{applyCode},
	    #{productCode},
	    #{productName}, 
	    #{skuCode}, 
	    #{skuName}, 
	    #{skuOid}, 
	    #{unitId}, 
	    #{deliveryCode}, 
	    #{deliveryItemId},
	    #{goodsNumber},
	    #{price},
	    #{receiveNumber},
	    #{wareHouseId},
		#{isNeedInvoice},
		#{remark},
		#{repairPrice},
		#{confirmNumber} ,
		#{exchangePrice}
		)
	</insert>
	 
	<update id="update" parameterType="com.nuotai.trading.seller.model.buyer.SBuyCustomerItem">
		UPDATE buy_customer_item  SET
				customer_id = #{customerId},
				apply_code = #{applyCode},
				product_code = #{productCode}, 			
				product_name = #{productName}, 			
				sku_code = #{skuCode}, 			
				sku_name = #{skuName}, 			
				sku_oid = #{skuOid}, 			
				unit_id = #{unitId}, 			
				delivery_code = #{deliveryCode}, 	
				delivery_item_id = #{deliveryItemId},		
				goods_number = #{goodsNumber},
				price = #{price},
				receive_number = #{receiveNumber},
				ware_house_id = #{wareHouseId},
			    is_need_invoice = #{isNeedInvoice},
			    remark = #{remark},
			    repair_price = #{repairPrice},
			    confirm_number = #{confirmNumber} ,
			    exchange_price = #{exchangePrice}
		WHERE id = #{id}
	</update>
	
	<delete id="delete">
		UPDATE buy_customer_item  SET
           del_id = #{delId},
           del_name  = #{delName},
           del_date = #{delDate}
		WHERE id = #{id}
	</delete>

	<select id="selectCustomerItemByCustomerId" resultType="com.nuotai.trading.seller.model.buyer.SBuyCustomerItem" parameterType="java.lang.String">
		SELECT
		<include refid="all"/>
		FROM buy_customer_item
		where customer_id = #{customerId}
	</select>
	
	<delete id="deleteByCustomerId" parameterType="java.lang.String">
	    delete from buy_customer_item
	    where customer_id = #{customerId}
	</delete>
</mapper>