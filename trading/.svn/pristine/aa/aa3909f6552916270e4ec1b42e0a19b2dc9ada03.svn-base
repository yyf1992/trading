<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.buyer.dao.SysPurchasePriceMapper">
	<sql id="all">
		  id , 	 
		  company_id AS companyId , 	  
		  product_id AS productId , 	  
		  product_code AS productCode , 	  
		  product_name AS productName , 	  
		  sku_code AS skuCode , 	  
		  sku_name AS skuName , 	  
		  barcode , 	 
		  unit_id AS unitId , 	  
		  price_status AS priceStatus , 	  
		  price_latitude AS priceLatitude , 	  
		  create_date AS createDate , 	  
		  update_date AS updateDate , 	  
		  del_date AS delDate 	  
	   </sql>

	<sql id="allNoAs">
			id , 
			company_id , 
			product_id , 
			product_code , 
			product_name , 
			sku_code , 
			sku_name , 
			barcode , 
			unit_id , 
			price_status , 
			price_latitude , 
			create_date , 
			update_date , 
			del_date 
	</sql>

	<sql id="SysPurchasePrice_condition">
   	<if test="companyId != null and companyId != ''">
		  AND company_id = #{companyId}  
	 </if>
   	<if test="productId != null and productId != ''">
		  AND product_id = #{productId}  
	 </if>
   	<if test="productCode != null and productCode != ''">
		  AND product_code = #{productCode}  
	 </if>
   	<if test="productName != null and productName != ''">
		  AND product_name = #{productName}  
	 </if>
   	<if test="skuCode != null and skuCode != ''">
		  AND sku_code = #{skuCode}  
	 </if>
   	<if test="skuName != null and skuName != ''">
		  AND sku_name = #{skuName}  
	 </if>
   	<if test="barcode != null and barcode != ''">
		  AND barcode = #{barcode}  
	 </if>
   	<if test="unitId != null and unitId != ''">
		  AND unit_id = #{unitId}  
	 </if>
   	<if test="priceStatus != null and priceStatus != ''">
		  AND price_status = #{priceStatus}  
	 </if>
   	<if test="priceLatitude != null and priceLatitude != 0 ">
		  AND price_latitude = #{priceLatitude}  
	 </if>
   	<if test="createDate != null">
		  AND create_date = #{createDate}  
	 </if>
   	<if test="updateDate != null">
		  AND update_date = #{updateDate}  
	 </if>
   	<if test="delDate != null">
		  AND del_date = #{delDate}
	 </if>
  	</sql>

	<select id="get" resultType="com.nuotai.trading.buyer.model.SysPurchasePrice">
		SELECT 
		<include refid="all" />
		FROM sys_purchase_price 
		WHERE
		id = #{value}
	</select>

	<select id="queryList" parameterType = "map" resultType="com.nuotai.trading.buyer.model.SysPurchasePrice">
		SELECT
		<include refid="all" />
		FROM sys_purchase_price
		WHERE
		del_date is null
		<if test="object.companyId != null and object.companyId != '' ">
			AND company_id = #{object.companyId}
		</if>
		<if test="object.productCode != null and object.productCode != '' ">
			AND product_code LIKE '%${object.productCode}%'
		</if>
		<if test="object.barcode!= null and object.barcode != '' ">
			AND barcode LIKE '%${object.barcode}%'
		</if>
		ORDER BY update_date DESC, create_date DESC
	</select>

 	<select id="queryCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM sys_purchase_price
		WHERE
		del_date is null
		<include refid="SysPurchasePrice_condition"/>
	</select>
	 
	 
	<update id="update" parameterType="com.nuotai.trading.buyer.model.SysPurchasePrice">
		UPDATE sys_purchase_price  SET
				company_id = #{companyId}, 			
				product_id = #{productId}, 			
				product_code = #{productCode}, 			
				product_name = #{productName}, 			
				sku_code = #{skuCode}, 			
				sku_name = #{skuName}, 			
				barcode = #{barcode}, 			
				unit_id = #{unitId}, 			
				price_status = #{priceStatus}, 			
				price_latitude = #{priceLatitude}, 			
				create_date = #{createDate}, 			
				update_date = #{updateDate}, 			
				del_date = #{delDate}			
		WHERE id = #{id}
	</update>
	
	<delete id="deletePurchase" parameterType="java.lang.String">
		delete FROM sys_purchase_price WHERE product_id = #{productId}
	</delete>

	<select id="selectExitProductId" parameterType="java.lang.String" resultType="java.lang.String">
		SELECT product_id FROM sys_purchase_price 
		WHERE company_id = #{comId}
	</select>
	
	<insert id="insertPurchase" parameterType="com.nuotai.trading.buyer.model.SysPurchasePrice">
		INSERT INTO  sys_purchase_price
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="id != null and id != '' ">
				id,
			</if>
			<if test="companyId != null and companyId != '' ">
				company_id,
			</if>
			<if test="productId != null and productId != '' ">
				product_id,
			</if>
			<if test="productCode != null and productCode != '' ">
				product_code,
			</if>
			<if test="productName != null and productName != '' ">
				product_name,
			</if>
			<if test="skuCode != null and skuCode != '' ">
				sku_code,
			</if>
			<if test="skuName != null and skuName != '' ">
				sku_name,
			</if>
			<if test="barcode != null and barcode != '' ">
				barcode,
			</if>
			<if test="unitId != null">
				unit_id,
			</if>
			<if test="priceStatus != null">
				price_status,
			</if>
			<if test="priceLatitude != null">
				price_latitude,
			</if>
			<if test="createDate != null">
				create_date,
			</if>
			<if test="updateDate != null">
				update_date,
			</if>
			<if test="delDate != null">
				del_date,
			</if>
		</trim>
		<trim prefix="values ("  suffix=")" suffixOverrides=",">
			<if test="id != null and id != '' ">
				#{id}, 
			</if>
			<if test="companyId != null and companyId != '' ">
				#{companyId}, 
			</if>
			<if test="productId != null and productId != '' ">
				#{productId}, 
			</if>
			<if test="productCode != null and productCode != '' ">
				#{productCode}, 
			</if>
			<if test="productName != null and productName != '' ">
				#{productName}, 
			</if>
			<if test="skuCode != null and skuCode != '' ">
				#{skuCode}, 
			</if>
			<if test="skuName != null and skuName != '' ">
				#{skuName}, 
			</if>
			<if test="barcode != null and barcode != '' ">
				#{barcode}, 
			</if>
			<if test="unitId != null">
				#{unitId}, 
			</if>
			<if test="priceStatus != null">
				#{priceStatus}, 
			</if>
			<if test="priceLatitude != null">
				#{priceLatitude}, 
			</if>
			<if test="createDate != null">
				#{createDate}, 
			</if>
			<if test="updateDate != null">
				#{updateDate}, 
			</if>
			<if test="delDate != null">
				#{delDate},
			</if>
		</trim>
	</insert>
	
	
	<select  id="getPurchaseData" parameterType="map" resultType="java.util.Map">
		SELECT
		    log.batch_no,
		    log.order_id,
  			log.order_code,
			log.barcode,
			log.price,
			log.product_code,
			log.product_name,
			log.fact_number,
			log.create_date,
			price.price_status,
			price.price_latitude
		FROM
			buy_warehouse_log log
		LEFT JOIN sys_purchase_price price ON log.barcode = price.barcode
		WHERE
			log.change_type = 1
		AND log.stock_type = 0
		AND log.fact_number &gt; 0
		<if test="companyId != null and companyId != '' ">
			AND log.company_id = #{companyId}
		</if>
		<if test="startDate != null and startDate != '' and endDate != null and endDate != '' ">
			AND log.create_date &gt;= '${startDate} 00:00:00' AND log.create_date &lt;= '${endDate} 23:59:59'
		</if>
		<if test="batchNo != null and batchNo != '' ">
			AND log.batch_no LIKE '%${batchNo}%'
		</if>
		<if test="orderCode != null and orderCode != '' ">
			AND log.order_code LIKE '%${orderCode}%'
		</if>
	</select>
	
	
	<update id="updatePurchase" parameterType="com.nuotai.trading.buyer.model.SysPurchasePrice"> 
		UPDATE sys_purchase_price 
		<set>
			<if test="priceStatus != null and priceStatus != '' ">
				price_status = #{priceStatus},
			</if>
			<if test="priceLatitude != null">
				price_latitude = #{priceLatitude},
			</if>
			<if test="updateDate != null">
				update_date = #{updateDate}
			</if>
			<if test="companyId != null and companyId != '' ">
				company_id= #{companyId}
			</if>
		</set>
		WHERE  product_id= #{productId}
	</update>
	
		<select id="selectByMap" parameterType = "map" resultType="com.nuotai.trading.buyer.model.SysPurchasePrice">
		SELECT
		<include refid="all" />
		FROM sys_purchase_price
		WHERE
		del_date is null
		<if test="barcode != null and barcode != '' ">
			AND barcode = #{barcode}
		</if>
		<if test="productId != null and productId != '' ">
			AND product_id = #{productId}
		</if>
		<if test="companyId != null and companyId != '' ">
			AND company_id = #{companyId}
		</if>
	</select>
</mapper>