package com.nuotai.trading.seller.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.seller.dao.SellerCustomerItemMapper;
import com.nuotai.trading.seller.model.SellerCustomerItem;



@Service
@Transactional
public class SellerCustomerItemService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerCustomerItemService.class);

	@Autowired
	private SellerCustomerItemMapper sellerCustomerItemMapper;
	
	public SellerCustomerItem get(String id){
		return sellerCustomerItemMapper.get(id);
	}
	
	public List<SellerCustomerItem> queryList(Map<String, Object> map){
		return sellerCustomerItemMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerCustomerItemMapper.queryCount(map);
	}
	
	public void add(SellerCustomerItem sellerCustomerItem){
		sellerCustomerItemMapper.add(sellerCustomerItem);
	}
	
	public void update(SellerCustomerItem sellerCustomerItem){
		sellerCustomerItemMapper.update(sellerCustomerItem);
	}
	
	public void delete(String id){
		sellerCustomerItemMapper.delete(id);
	}
	
	public List<SellerCustomerItem> selectCustomerItemByCustomerId(String customerId){
		return sellerCustomerItemMapper.selectCustomerItemByCustomerId(customerId);
	}

	//根据账单条件查询详细
	public List<SellerCustomerItem> queryCustomerItem(Map<String,Object> map){
		return sellerCustomerItemMapper.queryCustomerItem(map);
	}

	//根据对账过程修改状态
	public int updateCustomerByReconciliation(Map<String,Object> map){
		return sellerCustomerItemMapper.updateCustomerByReconciliation(map);
	}
	
	public List<SellerCustomerItem> getItemByCustomerId(String customerId){
		return sellerCustomerItemMapper.getItemByCustomerId(customerId);
	}
}
