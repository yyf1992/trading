<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.seller.dao.SellerBillReconciliationItemMapper">
	<sql id="all">
		  id ,
		  advance_edit_id AS advanceEditId,
		  reconciliation_id AS reconciliationId ,
		  create_bill_id AS createBillId ,
		  record_id AS recordId ,
		  delivery_no AS deliverNo,
	      buy_company_id AS buyCompanyId,
	      seller_company_id AS sellerCompanyId,
		  item_id AS itemId ,
		  order_id AS orderId,
		  bill_reconciliation_type AS billReconciliationType ,
		  is_need_invoice AS isNeedInvoice ,
		  product_code AS productCode ,
		  product_name AS productName ,
		  sku_code AS skuCode ,
		  sku_name AS skuName ,
		  barcode ,
		  unit_id AS unitId ,
		  sale_price AS salePrice ,
		  update_sale_price AS updateSalePrice ,
		   update_sale_price_old AS updateSalePriceOld ,
		   is_update_sale AS isUpdateSale,
		  arrival_num AS arrivalNum ,
		  arrival_date AS arrivalDate,
		  logistics_id AS logisticsId,
		  freight,
		  waybill_no AS waybillNo,
		  logistics_company AS logisticsCompany,
		  driver_name AS driverName,
		  is_invoice_status AS isInvoiceStatus,
		  reconciliation_deal_status AS reconciliationDealStatus
	   </sql>

	<sql id="allNoAs">
		id ,
		advance_edit_id,
		buy_company_id ,
		seller_company_id ,
		create_bill_id,
		create_bill_name,
		reconciliation_id ,
		record_id ,
		delivery_no,
		item_id ,
		order_id ,
		bill_reconciliation_type ,
		is_need_invoice ,
		product_code ,
		product_name ,
		sku_code ,
		sku_name ,
		barcode ,
		unit_id ,
		sale_price ,
		arrival_num ,
		arrival_date ,
		logistics_id ,
		freight ,
		waybill_no ,
		logistics_company ,
		driver_name ,
		is_invoice_status ,
		reconciliation_deal_status
	</sql>

	<sql id="BuyBillReconciliationItem_condition">
		<if test="reconciliationId != null and reconciliationId != ''">
			AND reconciliation_id = #{reconciliationId}
		</if>
		<if test="recordId != null and recordId != ''">
			AND record_id = #{recordId}
		</if>
		<if test="itemId != null and itemId != ''">
			AND item_id = #{itemId}
		</if>
		<if test="billReconciliationType != null and billReconciliationType != ''">
			AND bill_reconciliation_type = #{billReconciliationType}
		</if>
		<if test="isNeedInvoice != null and isNeedInvoice != ''">
			AND is_need_invoice = #{isNeedInvoice}
		</if>
		<if test="productCode != null and productCode != ''">
			AND product_code = #{productCode}
		</if>
		<if test="productName != null and productName != ''">
			AND product_name = #{productName}
		</if>
		<if test="skuCode != null and skuCode != ''">
			AND sku_code = #{skuCode}
		</if>
		<if test="skuName != null and skuName != ''">
			AND sku_name = #{skuName}
		</if>
		<if test="barcode != null and barcode != ''">
			AND barcode = #{barcode}
		</if>
		<if test="unitId != null and unitId != ''">
			AND unit_id = #{unitId}
		</if>
		<if test="salePrice != null and salePrice != 0 ">
			AND sale_price = #{salePrice}
		</if>
		<if test="updateSalePrice != null and updateSalePrice != 0 ">
			AND update_sale_price = #{updateSalePrice}
		</if>
		<if test="arrivalNum != null and arrivalNum != 0 ">
			AND arrival_num = #{arrivalNum}
		</if>
		<if test="arrivalDate != null">
			AND arrival_date = #{arrivalDate}
		</if>
	</sql>

	<select id="get" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		SELECT
		<include refid="all" />

		FROM buy_bill_reconciliation_item
		WHERE
		id = #{value}
	</select>

	<!--根据发货、退货编号查询对账详情中数据量-->
	<select id="queryRecItemCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_bill_reconciliation_item
		WHERE 1=1
		<if test="reconciliationId != null and reconciliationId != ''">
			and reconciliation_id = #{reconciliationId}
		</if>
		<if test="itemId != null and itemId != ''">
			and item_id = #{itemId}
		</if>
		<if test="isNeedInvoice != null and isNeedInvoice != ''">
			and is_need_invoice = #{isNeedInvoice}
		</if>
	</select>

	<!--根据发货单号查询票据附件-->
	<select id="queryRecItemAddr" parameterType="java.lang.String" resultType="map">
		SELECT ba.url from buy_attachment ba WHERE ba.related_id = #{deliveryNo}
	</select>
	<!--根据发货单号查询票据附件 -->
	<select id="queryCustomerProof" parameterType="java.lang.String" resultType="map">
		SELECT bc.proof from seller_customer bc WHERE bc.customer_code = #{deliveryNo}
	</select>

	<!--根据发票条件查询发货详情-->
	<select id="queryRecDecliveryItemList" parameterType = "map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		SELECT bbr.id ,bbr.buy_company_id AS buyCompanyId,bbr.seller_company_id AS sellerCompanyId,
		bbr.create_bill_id AS createBillId,bbr.create_bill_name AS createBillName,
		bbr.reconciliation_id AS reconciliationId ,bbr.record_id AS recordId ,bbr.delivery_no AS deliverNo,
		bbr.item_id AS itemId ,bbr.order_id AS orderId,bbr.bill_reconciliation_type AS billReconciliationType ,
		bbr.is_need_invoice AS isNeedInvoice ,bbr.product_code AS productCode ,bbr.product_name AS productName ,
		bbr.sku_code AS skuCode ,bbr.sku_name AS skuName ,bbr.barcode ,bbr.unit_id AS unitId ,
		bbr.sale_price AS salePrice ,bbr.update_sale_price AS updateSalePrice ,bbr.is_update_sale AS isUpdateSale,bbr.arrival_num AS arrivalNum ,
		bbr.arrival_date AS arrivalDate,bbr.logistics_id AS logisticsId,bbr.freight,bbr.waybill_no AS waybillNo,bbr.logistics_company AS logisticsCompany,
		bbr.driver_name AS driverName,bbr.is_invoice_status AS isInvoiceStatus,bbr.reconciliation_deal_status AS reconciliationDealStatus
		,IFNULL(bc.company_name ,'') AS buyCompanyName
		,IFNULL(unit.unit_name,'') AS unitName
		,IFNULL(bdr.receiptvoucher_addr,'') deliveryAddr
		,IFNULL(bcu.proof,'') AS proof
		,IFNULL(bcu.customer_code,'') customerCode
		,DATE_FORMAT(bbr.arrival_date,'%Y-%c-%d') AS arrivalDateStr
		,o.order_code orderCode
		FROM buy_bill_reconciliation_item bbr
		LEFT JOIN seller_order o ON o.id = bbr.order_id
		LEFT JOIN buy_company bc ON bc.id = bbr.buy_company_id
		LEFT JOIN seller_delivery_record bdr ON bbr.record_id = bdr.id
		LEFT JOIN buy_unit unit ON bbr.unit_id = unit.id
		LEFT JOIN seller_customer bcu ON bbr.record_id = bcu.id
		WHERE 1=1
		AND bbr.seller_company_id = #{object.selCompanyId}
		<if test="object.reconciliationDealStatus != null and object.reconciliationDealStatus != ''">
			AND bbr.reconciliation_deal_status =#{object.reconciliationDealStatus}
		</if>
		<if test="object.isNeedInvoice != null and object.isNeedInvoice != ''">
			AND bbr.is_need_invoice = #{object.isNeedInvoice}
		</if>
		<if test="object.isInvoiceStatus1 != null and object.isInvoiceStatus1 != ''">
			AND bbr.is_invoice_status != #{object.isInvoiceStatus1}
		</if>
		<if test="object.isInvoiceStatus2 != null and object.isInvoiceStatus2 != ''">
			AND bbr.is_invoice_status != #{object.isInvoiceStatus2}
		</if>
		<if test="object.deliverNo != null and object.deliverNo != ''">
			AND bbr.delivery_no = #{object.deliverNo}
		</if>
		<if test="object.recordId != null and object.recordId != ''">
			AND bbr.record_id = #{object.recordId}
		</if>
		<if test="object.createBillName != null and object.createBillName != ''">
			AND bbr.create_bill_name LIKE '%${object.createBillName}%'
		</if>
		<if test="object.buyCompanyName != null and object.buyCompanyName != ''">
			AND bc.company_name LIKE '%${object.buyCompanyName}%'
		</if>
		<if test="object.productName != null and object.productName != ''">
			AND bbr.product_name LIKE '%${object.productName}%'
		</if>
		<if test="object.orderId != null and object.orderId != ''">
			AND bbr.order_id = #{object.orderId}
		</if>
		<if test="object.startArrivalDate != null">
			AND bbr.arrival_date &gt;= '${object.startArrivalDate} 00:00:00'
		</if>
		<if test="object.endArrivalDate != null">
			AND bbr.arrival_date &lt;='${object.endArrivalDate} 24:00:00'
		</if>
	</select>

	<!--根据发票条件查询发货详情-->
	<select id="queryRecDecliveryItemExport" parameterType = "map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		<!--CALL queryRecDecliveryItemList(#{selCompanyId},#{reconciliationDealStatus},#{isNeedInvoice},#{isInvoiceStatus1},#{isInvoiceStatus2},#{recordId},#{buyCompanyName},#{productName},#{orderId},#{startArrivalDate},#{endArrivalDate});-->
		SELECT bbr.id ,bbr.buy_company_id AS buyCompanyId,bbr.seller_company_id AS sellerCompanyId,
		bbr.create_bill_id AS createBillId,bbr.create_bill_name AS createBillName,
		bbr.reconciliation_id AS reconciliationId ,bbr.record_id AS recordId ,bbr.delivery_no AS deliverNo,
		bbr.item_id AS itemId ,bbr.order_id AS orderId,bbr.bill_reconciliation_type AS billReconciliationType ,
		bbr.is_need_invoice AS isNeedInvoice ,bbr.product_code AS productCode ,bbr.product_name AS productName ,
		bbr.sku_code AS skuCode ,bbr.sku_name AS skuName ,bbr.barcode ,bbr.unit_id AS unitId ,
		bbr.sale_price AS salePrice ,bbr.update_sale_price AS updateSalePrice ,bbr.is_update_sale AS isUpdateSale,bbr.arrival_num AS arrivalNum ,
		bbr.arrival_date AS arrivalDate,bbr.logistics_id AS logisticsId,bbr.freight,bbr.waybill_no AS waybillNo,bbr.logistics_company AS logisticsCompany,
		bbr.driver_name AS driverName,bbr.is_invoice_status AS isInvoiceStatus,bbr.reconciliation_deal_status AS reconciliationDealStatus
		,IFNULL(bc.company_name ,'') AS buyCompanyName
		,IFNULL(unit.unit_name,'') AS unitName
		,IFNULL(bdr.receiptvoucher_addr,'') deliveryAddr
		,IFNULL(bcu.proof,'') AS proof
		,IFNULL(bcu.customer_code,'') customerCode
		,DATE_FORMAT(bbr.arrival_date,'%Y-%c-%d') AS arrivalDateStr
		,o.order_code orderCode
		FROM buy_bill_reconciliation_item bbr
		LEFT JOIN seller_order o ON o.id = bbr.order_id
		LEFT JOIN buy_company bc ON bc.id = bbr.buy_company_id
		LEFT JOIN seller_delivery_record bdr ON bbr.record_id = bdr.id
		LEFT JOIN buy_unit unit ON bbr.unit_id = unit.id
		LEFT JOIN seller_customer bcu ON bbr.record_id = bcu.id
		WHERE 1=1
		AND bbr.seller_company_id = #{selCompanyId}
		<if test="reconciliationDealStatus != null and reconciliationDealStatus != ''">
			AND bbr.reconciliation_deal_status =#{reconciliationDealStatus}
		</if>
		<if test="isNeedInvoice != null and isNeedInvoice != ''">
			AND bbr.is_need_invoice = #{isNeedInvoice}
		</if>
		<if test="isInvoiceStatus1 != null and isInvoiceStatus1 != ''">
			AND bbr.is_invoice_status != #{isInvoiceStatus1}
		</if>
		<if test="isInvoiceStatus2 != null and isInvoiceStatus2 != ''">
			AND bbr.is_invoice_status != #{isInvoiceStatus2}
		</if>
		<if test="deliverNo != null and deliverNo != ''">
			AND bbr.delivery_no = #{deliverNo}
		</if>
		<if test="recordId != null and recordId != ''">
			AND bbr.record_id = #{recordId}
		</if>
		<if test="createBillName != null and createBillName != ''">
			AND bbr.create_bill_name LIKE '%${createBillName}%'
		</if>
		<if test="buyCompanyName != null and buyCompanyName != ''">
			AND bc.company_name LIKE '%${buyCompanyName}%'
		</if>
		<if test="productName != null and productName != ''">
			AND bbr.product_name LIKE '%${productName}%'
		</if>
		<if test="orderId != null and orderId != ''">
			AND bbr.order_id = #{orderId}
		</if>
		<if test="startArrivalDate != null">
			AND bbr.arrival_date &gt;= '${startArrivalDate} 00:00:00'
		</if>
		<if test="endArrivalDate != null">
			AND bbr.arrival_date &lt;='${endArrivalDate} 24:00:00'
		</if>
	</select>

	<!--根据发票条件查询数量-->
	<select id="queryCountByInvoice"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_bill_reconciliation_item bbr
		LEFT JOIN buy_company bc ON bbr.buy_company_id = bc.id
		WHERE 1=1
		AND bbr.seller_company_id = #{selCompanyId}
		<if test="billInvoiceStatus != null and billInvoiceStatus != '' and billInvoiceStatus == 0">
			AND bbr.reconciliation_deal_status = '3' AND bbr.is_need_invoice = 'Y' AND bbr.is_invoice_status NOT IN ('1','2')
		</if>
		<if test="deliverNo != null and deliverNo != ''">
			AND delivery_no = #{deliverNo}
		</if>
		<if test="recordId != null and recordId != ''">
			AND record_id = #{recordId}
		</if>
		<if test="createBillName != null and createBillName != ''">
			AND bbr.create_bill_name LIKE '%${createBillName}%'
		</if>
		<if test="buyCompanyName !=null and buyCompanyName != ''">
			and buy_company_id = (SELECT bc.id from buy_company bc where bc.company_name = #{buyCompanyName})
		</if>
		<if test="productName != null and productName != ''">
			AND product_name = #{productName}
		</if>
		<if test="orderId != null and orderId != ''">
			AND order_id = #{orderId}
		</if>
		<if test="startArrivalDate != null and startArrivalDate != ''">
			and arrival_date&gt;='${startArrivalDate} 00:00:00'
		</if>
		<if test="endArrivalDate != null and endArrivalDate != ''">
			and arrival_date&lt;='${endArrivalDate} 24:00:00'
		</if>
	</select>

	<!--根据对账编号查询对账详情-->
	<select id="queryRecItemList" parameterType="map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		SELECT bbri.id ,bbri.reconciliation_id AS reconciliationId ,bbri.record_id AS recordId ,bbri.delivery_no AS deliverNo,
		IFNULL((SELECT DISTINCT bwlog.storage_no from buy_warehouse_log bwlog where bbri.delivery_no = bwlog.batch_no),'') storageNo,
		bbri.create_bill_id AS createBillId,bbri.create_bill_name AS createBillName,
		bbri.buy_company_id AS buyCompanyId,bbri.seller_company_id AS sellerCompanyId,bbri.item_id AS itemId ,
		IFNULL((SELECT bc.company_name from buy_company bc where bc.id = bbri.buy_company_id),'') AS buyCompanyName ,
		IFNULL((SELECT bc.company_name from buy_company bc where bc.id = bbri.seller_company_id),'') AS sellerCompanyName ,
		bbri.order_id AS orderId,bbri.bill_reconciliation_type AS billReconciliationType ,
		bbri.is_need_invoice AS isNeedInvoice ,bbri.product_code AS productCode ,bbri.product_name AS productName ,
		bbri.sku_code AS skuCode ,bbri.sku_name AS skuName ,bbri.barcode ,bbri.unit_id AS unitId ,
		bbri.sale_price AS salePrice ,bbri.update_sale_price AS updateSalePrice ,bbri.is_update_sale AS isUpdateSale ,bbri.arrival_num AS arrivalNum ,
		bbri.arrival_date AS arrivalDate,bbri.logistics_id AS logisticsId,bbri.freight,bbri.waybill_no AS waybillNo,bbri.logistics_company AS logisticsCompany,
		bbri.driver_name AS driverName,bbri.is_invoice_status AS isInvoiceStatus,bbri.reconciliation_deal_status AS reconciliationDealStatus
		,IFNULL(unit.unit_name,'') AS unitName
		,IFNULL(bdr.receiptvoucher_addr,'') deliveryAddr
		,IFNULL(bc.proof,'') AS proof
		,IFNULL(bdr.deliver_no,'') deliverNo
		,IFNULL(bc.customer_code,'') customerCode
		,DATE_FORMAT(bbri.arrival_date,'%Y-%c-%d') AS arrivalDateStr,
		IFNULL(o.order_code,'') orderCode
		FROM buy_bill_reconciliation_item bbri
		LEFT JOIN seller_order o ON o.id = bbri.order_id
		LEFT JOIN buy_unit unit ON bbri.unit_id = unit.id
		LEFT JOIN seller_delivery_record bdr ON bbri.record_id = bdr.id
		LEFT JOIN seller_customer bc ON bbri.record_id = bc.id
		WHERE 1=1
		<if test="recDealStatusPass != null and recDealStatusPass != ''">
			AND bbri.reconciliation_deal_status != #{recDealStatusPass}
		</if>
		<if test="reconciliationDealStatus != null and reconciliationDealStatus != ''">
			AND bbri.reconciliation_deal_status = #{reconciliationDealStatus}
		</if>
		<if test="deliveryItemIds != null and deliveryItemIds != ''">
			AND bbri.id IN (${deliveryItemIds})
		</if>
		<if test="deliveryItemId != null and deliveryItemId != ''">
			AND bbri.id = #{deliveryItemId}
		</if>
		<if test="reconciliationId != null and reconciliationId != ''">
			AND bbri.reconciliation_id = #{reconciliationId}
		</if>
		<if test="isNeedInvoice != null and isNeedInvoice != ''">
			AND bbri.is_need_invoice = #{isNeedInvoice}
		</if>
		<if test="isInvoiceStatus != null and isInvoiceStatus != ''">
			AND bbri.is_invoice_status = #{isInvoiceStatus}
		</if>
		<if test="invoiceId != null and invoiceId != ''">
			AND bbri.id IN (SELECT bic.reconciliation_item_id from buy_bill_invoice_commodity bic where bic.invoice_id=#{invoiceId})
		</if>
	</select>

	<!--根据对账编号查询对账详情-->
	<select id="queryRecItemBuyId" parameterType="map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		<!--CALL queryRecItemList(#{recDealStatusPass},#{reconciliationDealStatus},#{deliveryItemIds},#{deliveryItemId},#{reconciliationId},#{isNeedInvoice},#{isInvoiceStatus},#{invoiceId});-->
		SELECT bbri.id ,bbri.reconciliation_id AS reconciliationId ,bbri.record_id AS recordId ,bbri.delivery_no AS deliverNo,
		bbri.buy_company_id AS buyCompanyId,bbri.seller_company_id AS sellerCompanyId,bbri.item_id AS itemId ,
		IFNULL((SELECT bc.company_name from buy_company bc where bc.id = bbri.buy_company_id),'') AS buyCompanyName ,
		IFNULL((SELECT bc.company_name from buy_company bc where bc.id = bbri.seller_company_id),'') AS sellerCompanyName ,
		bbri.order_id AS orderId,bbri.bill_reconciliation_type AS billReconciliationType ,
		bbri.is_need_invoice AS isNeedInvoice ,bbri.product_code AS productCode ,bbri.product_name AS productName ,
		bbri.sku_code AS skuCode ,bbri.sku_name AS skuName ,bbri.barcode ,bbri.unit_id AS unitId ,
		bbri.sale_price AS salePrice ,bbri.update_sale_price AS updateSalePrice ,bbri.is_update_sale AS isUpdateSale ,bbri.arrival_num AS arrivalNum ,
		bbri.arrival_date AS arrivalDate,bbri.logistics_id AS logisticsId,bbri.freight,bbri.waybill_no AS waybillNo,bbri.logistics_company AS logisticsCompany,
		bbri.driver_name AS driverName,bbri.is_invoice_status AS isInvoiceStatus,bbri.reconciliation_deal_status AS reconciliationDealStatus
		,IFNULL(unit.unit_name,'') AS unitName
		,IFNULL(bdr.receiptvoucher_addr,'') deliveryAddr
		,IFNULL(bc.proof,'') AS proof
		,IFNULL(bdr.deliver_no,'') deliverNo
		,IFNULL(bc.customer_code,'') customerCode
		,DATE_FORMAT(bbri.arrival_date,'%Y-%c-%d') AS arrivalDateStr,
		IFNULL(o.order_code,'') orderCode
		FROM buy_bill_reconciliation_item bbri
		LEFT JOIN seller_order o ON o.id = bbri.order_id
		LEFT JOIN buy_unit unit ON bbri.unit_id = unit.id
		LEFT JOIN seller_delivery_record bdr ON bbri.record_id = bdr.id
		LEFT JOIN seller_customer bc ON bbri.record_id = bc.id
		WHERE 1=1
		<if test="recDealStatusPass != null and recDealStatusPass != ''">
			AND bbri.reconciliation_deal_status != #{recDealStatusPass}
		</if>
		<if test="reconciliationDealStatus != null and reconciliationDealStatus != ''">
			AND bbri.reconciliation_deal_status = #{reconciliationDealStatus}
		</if>
		<if test="deliveryItemIds != null and deliveryItemIds != ''">
			AND bbri.id IN (${deliveryItemIds})
		</if>
		<if test="deliveryItemId != null and deliveryItemId != ''">
			AND bbri.id = #{deliveryItemId}
		</if>
		<if test="reconciliationId != null and reconciliationId != ''">
			AND bbri.reconciliation_id = #{reconciliationId}
		</if>
		<if test="isNeedInvoice != null and isNeedInvoice != ''">
			AND bbri.is_need_invoice = #{isNeedInvoice}
		</if>
		<if test="isInvoiceStatus != null and isInvoiceStatus != ''">
			AND bbri.is_invoice_status = #{isInvoiceStatus}
		</if>
		<if test="invoiceId != null and invoiceId != ''">
			AND bbri.id IN (SELECT bic.reconciliation_item_id from buy_bill_invoice_commodity bic where bic.invoice_id=#{invoiceId})
		</if>
	</select>

	<!--根据对账编号查询对账详情-->
	<select id="queryRecItem" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		SELECT
		<include refid="all" />
		,IFNULL((SELECT unit.unit_name from buy_unit unit where unit.id = unit_id),'') AS unitName
		,IFNULL((SELECT bdr.receiptvoucher_addr from seller_delivery_record bdr where bdr.id = record_id),'') deliveryAddr
		,IFNULL((SELECT bc.proof from seller_customer bc where bc.id = record_id),'') AS proof
		,IFNULL((SELECT bdr.deliver_no from seller_delivery_record bdr where bdr.id = record_id),'') deliverNo
		,IFNULL((SELECT bc.customer_code from seller_customer bc WHERE bc.id = record_id),'') customerCode
		,DATE_FORMAT(arrival_date,'%Y-%c-%d') AS arrivalDateStr
		FROM buy_bill_reconciliation_item
		WHERE id = #{id}
	</select>

	<!--添加对账详情-->
	<insert id="addRecItem" parameterType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		INSERT into buy_bill_reconciliation_item
		(
		<include refid="allNoAs" />
		)
		VALUES
		(
		#{id},
		#{advanceEditId},
		#{buyCompanyId},
		#{sellerCompanyId},
		#{createBillId},
		#{createBillName},
		#{reconciliationId},
		#{recordId},
		#{deliverNo},
		#{itemId},
		#{orderId},
		#{billReconciliationType},
		#{isNeedInvoice},
		#{productCode},
		#{productName},
		#{skuCode},
		#{skuName},
		#{barcode},
		#{unitId},
		#{salePrice},
		#{arrivalNum},
		#{arrivalDate},
		#{logisticsId},
		#{freight},
		#{waybillNo},
		#{logisticsCompany},
		#{driverName},
		#{isInvoiceStatus},
		#{reconciliationDealStatus}
		)
	</insert>

	<!--根据订单编号修改单价-->
	<update id="updateRecItemPrice" parameterType="map">
		UPDATE buy_bill_reconciliation_item
		<trim prefix="set" suffixOverrides=",">
			<if test="updateSalePrice != null">
				update_sale_price = #{updateSalePrice},
			</if>
			<if test="updateSalePriceOld != null">
				update_sale_price_old = #{updateSalePriceOld},
			</if>
			<if test="isUpdateSale != null and isUpdateSale != ''">
				is_update_sale = #{isUpdateSale},
			</if>
			<if test="isInvoiceStatus != null and isInvoiceStatus != ''">
				is_invoice_status = #{isInvoiceStatus},
			</if>
			<if test="reconciliationDealStatus != null and reconciliationDealStatus != ''">
				reconciliation_deal_status = #{reconciliationDealStatus},
			</if>
		</trim>
		WHERE 1=1
		<if test="id != null and id != ''">
			AND id = #{id}
		</if>
		<if test="reconciliationId != null and reconciliationId != ''">
			AND reconciliation_id = #{reconciliationId}
		</if>
		<if test="invoiceId != null and invoiceId != ''">
			AND id IN
			(SELECT bic.reconciliation_item_id from buy_bill_invoice_commodity bic where bic.invoice_id  = #{invoiceId})
		</if>
	</update>
	<update id="update" parameterType="com.nuotai.trading.seller.model.SellerBillReconciliationItem">
		UPDATE buy_bill_reconciliation_item  SET
				reconciliation_id = #{reconciliationId}, 			
				record_id = #{recordId}, 			
				item_id = #{itemId}, 			
				bill_reconciliation_type = #{billReconciliationType}, 			
				is_need_invoice = #{isNeedInvoice}, 			
				product_code = #{productCode}, 			
				product_name = #{productName}, 			
				sku_code = #{skuCode}, 			
				sku_name = #{skuName}, 			
				barcode = #{barcode}, 			
				unit_id = #{unitId}, 			
				sale_price = #{salePrice}, 			
				update_sale_price = #{updateSalePrice}, 			
				arrival_num = #{arrivalNum}, 			
				arrival_date = #{arrivalDate}			
		WHERE id = #{id}
	</update>

	<delete id="deleteRecItem" parameterType="map">
		DELETE FROM buy_bill_reconciliation_item
		WHERE 1=1
		<if test="id != null and id != ''">
			AND id = #{id}
		</if>
		<if test="reconciliationId != null and reconciliationId != ''">
			AND reconciliation_id = #{reconciliationId}
		</if>
		<if test="billReconciliationStatus != null and billReconciliationStatus != ''">
			AND reconciliation_deal_status = #{billReconciliationStatus}
		</if>
	</delete>

	<delete id="delete">
		UPDATE buy_bill_reconciliation_item  SET
           del_id = #{delId},
           del_name  = #{delName},
           del_date = #{delDate}
		WHERE id = #{id}
	</delete>

</mapper>