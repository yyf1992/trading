<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.seller.dao.SellerBillReconciliationPaymentMapper">
	<sql id="all">
		  id , 	 
		  reconciliation_id AS reconciliationId , 	  
		  actual_payment_amount AS actualPaymentAmount , 	  
		  bank_account AS bankAccount , 	  
		  payment_type AS paymentType , 	  
		  payment_addr AS paymentAddr , 	  
		  receivables_addr AS receivablesAddr , 	  
		  payment_date AS paymentDate , 	  
		  receivables_date AS receivablesDate ,
		  payment_status AS paymentStatus,
		  payment_remarks AS paymentRemarks , 	  
		  receivables_remarks AS receivablesRemarks 	  
	   </sql>

	<sql id="allNoAs">
			id , 
			reconciliation_id , 
			actual_payment_amount , 
			bank_account , 
			payment_type , 
			payment_addr , 
			receivables_addr , 
			payment_date , 
			receivables_date ,
			payment_status ,
			payment_remarks , 
			receivables_remarks 
	</sql>

	<sql id="BuyBillReconciliationPayment_condition">
   	<if test="reconciliationId != null and reconciliationId != ''">
		  AND reconciliation_id = #{reconciliationId}  
	 </if>
   	<if test="actualPaymentAmount != null and actualPaymentAmount != 0 ">
		  AND actual_payment_amount = #{actualPaymentAmount}  
	 </if>
   	<if test="bankAccount != null and bankAccount != ''">
		  AND bank_account = #{bankAccount}  
	 </if>
   	<if test="paymentType != null and paymentType != ''">
		  AND payment_type = #{paymentType}  
	 </if>
   	<if test="paymentAddr != null and paymentAddr != ''">
		  AND payment_addr = #{paymentAddr}  
	 </if>
   	<if test="receivablesAddr != null and receivablesAddr != ''">
		  AND receivables_addr = #{receivablesAddr}  
	 </if>
   	<if test="paymentDate != null">
		  AND payment_date = #{paymentDate}  
	 </if>
   	<if test="receivablesDate != null">
		  AND receivables_date = #{receivablesDate}  
	 </if>
   	<if test="paymentRemarks != null and paymentRemarks != ''">
		  AND payment_remarks = #{paymentRemarks}  
	 </if>
   	<if test="receivablesRemarks != null and receivablesRemarks != ''">
		  AND receivables_remarks = #{receivablesRemarks}
	 </if>
  	</sql>

	<!--根据账单编号查询付款-->
	<select id="getPaymentListByRecId" parameterType="map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationPayment">
		SELECT
		bbp.id ,
		bbp.reconciliation_id AS reconciliationId,
		bbp.actual_payment_amount AS actualPaymentAmount,
		bbp.bank_account AS bankAccount,
		bbp.payment_type AS paymentType,
		bbp.payment_addr AS paymentAddr,
		bbp.receivables_addr ,
		bbp.payment_date AS paymentDate,
		bbp.receivables_date ,
		bbp.payment_status AS paymentStatus,
		bbp.payment_remarks AS paymentRemarks,
		bbp.receivables_remarks
		, DATE_FORMAT(bbp.payment_date,'%Y-%c-%d') AS paymentDateStr
		,(SELECT sb.bank_account FROM sys_bank sb where bbp.bank_account = sb.id) AS bankAccountNum
		FROM buy_bill_reconciliation_payment bbp
		WHERE 1=1 AND bbp.payment_status != '2'
		<if test="reconciliationId != null and reconciliationId != ''">
			AND bbp.reconciliation_id = #{reconciliationId}
		</if>
	</select>

	<select id="get" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationPayment">
		SELECT 
		<include refid="all" />
		FROM buy_bill_reconciliation_payment 
		WHERE
		id = #{value}
	</select>

	<select id="queryList" parameterType = "map" resultType="com.nuotai.trading.seller.model.SellerBillReconciliationPayment">
		SELECT
		<include refid="all" />
		FROM buy_bill_reconciliation_payment
		WHERE
		del_date is null
		<include refid="BuyBillReconciliationPayment_condition"/>
	</select>

 	<select id="queryCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_bill_reconciliation_payment
		WHERE
		del_date is null
		<include refid="BuyBillReconciliationPayment_condition"/>
	</select>
	 
	<insert id="add" parameterType="com.nuotai.trading.seller.model.SellerBillReconciliationPayment">
		INSERT into buy_bill_reconciliation_payment
		(
         <include refid="allNoAs" />
		)
		VALUES
		(
	    #{id}, 
	    #{reconciliationId}, 
	    #{actualPaymentAmount}, 
	    #{bankAccount}, 
	    #{paymentType}, 
	    #{paymentAddr}, 
	    #{receivablesAddr}, 
	    #{paymentDate}, 
	    #{receivablesDate},
		#{paymentStatus},
	    #{paymentRemarks}, 
	    #{receivablesRemarks}
		)
	</insert>

	<!--根据编号修改付款状态-->
	<update id="updatePaymentItem" parameterType="com.nuotai.trading.seller.model.SellerBillReconciliationPayment">
		UPDATE buy_bill_reconciliation_payment
		<trim prefix="set" suffixOverrides=",">
			<if test="actualPaymentAmount != null and actualPaymentAmount !=''">
				actual_payment_amount = #{actualPaymentAmount},
			</if>
			<if test="bankAccount != null and bankAccount != ''">
				bank_account = #{bankAccount},
			</if>
			<if test="paymentType != null and paymentType != ''">
				payment_type = #{paymentType},
			</if>
			<if test="paymentAddr != nul and paymentAddr != ''">
				payment_addr = #{paymentAddr},
			</if>
			<if test="receivablesAddr != null and receivablesAddr != ''">
				receivables_addr = #{receivablesAddr},
			</if>
			<if test="paymentDate != null and paymentDate != ''">
				payment_date = #{paymentDate},
			</if>
			<if test="receivablesDate != null and receivablesDate != ''">
				receivables_date = #{receivablesDate},
			</if>
			<if test="paymentStatus != null and paymentStatus != ''">
				payment_status = #{paymentStatus},
			</if>
			<if test="paymentRemarks != null and paymentRemarks != ''">
				payment_remarks = #{paymentRemarks},
			</if>
			<if test="receivablesRemarks != null and receivablesRemarks != ''">
				receivables_remarks = #{receivablesRemarks},
			</if>
		</trim>
		WHERE 1=1
		<if test="id != null and id != ''">
			AND id = #{id}
		</if>
		<if test="reconciliationId != null and reconciliationId != ''">
			AND reconciliation_id = #{reconciliationId}
		</if>
		<if test="returnStatus != null and returnStatus != ''">
			AND payment_status = #{returnStatus}
		</if>
	</update>
	
	<delete id="delete">
		UPDATE buy_bill_reconciliation_payment  SET
           del_id = #{delId},
           del_name  = #{delName},
           del_date = #{delDate}
		WHERE id = #{id}
	</delete>

</mapper>