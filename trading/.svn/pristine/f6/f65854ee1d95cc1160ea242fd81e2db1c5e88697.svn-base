package com.nuotai.trading.service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyAddressMapper;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyAddressService implements BuyAddressMapper {
	@Autowired
	private BuyAddressMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuyAddress record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyAddress record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuyAddress selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyAddress record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKeyWithBLOBs(BuyAddress record) {
		return mapper.updateByPrimaryKeyWithBLOBs(record);
	}

	@Override
	public int updateByPrimaryKey(BuyAddress record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<BuyAddress> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}
	
	/**
	 * 默认地址查询
	 * @param map
	 * @return
	 */
	public BuyAddress getDefaultAddress() {
		BuyAddress shippingAddress = new BuyAddress();
		Map<String, Object> map = new HashMap<String, Object>();
		// 默认地址
		map.put("isDel", Constant.IsDel.NODEL.getValue());// 未删除
		map.put("isDefault", "0"); // 默认地址
		List<BuyAddress> defaultShippingAddressList = mapper.selectByMap(map);
		if(defaultShippingAddressList != null && defaultShippingAddressList.size() > 0){
			shippingAddress = defaultShippingAddressList.get(0);
		}
		return shippingAddress;
	}
	
	public List<BuyAddress> getAllAddress(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("compId", ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue());// 未删除
		List<BuyAddress> addressList = mapper.selectByMap(map);
		return addressList;
	}

	/**
	 * 修改保存收货地址
	 * @param shippingAddress
	 */
	public JSONObject saveUpdate(BuyAddress address) {
		JSONObject json = verification(address);
		if(!json.getBoolean("success")){
			return json;
		}
		// 判断是否设置默认地址
		if(address.getIsDefault() == 0){
			cancelDefault(ShiroUtils.getCompId());
		}
		int result = 0;
		Date date = new Date();
		address.setUpdateId(ShiroUtils.getUserId());
		address.setUpdateName(ShiroUtils.getUserName());
		address.setUpdateDate(date);
		result = mapper.updateByPrimaryKeySelective(address);
		if(result > 0){
			json.put("success", true);
			json.put("msg", "保存成功！");
			address = selectById(address.getId());
			json.put("address", address);
		}else{
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}

	@Override
	public int cancelDefault(String compId) {
		return mapper.cancelDefault(compId);
	}
	
	/**
	 * 新增、修改保存时数据验证
	 * @param address
	 * @return
	 */
	public JSONObject verification(BuyAddress address){
		JSONObject json = new JSONObject();
		if(address.getPersonName() == null || "".equals(address.getPersonName())){
			json.put("success", false);
			json.put("msg", "收货人不能为空！");
			return json;
		}
		if(address.getProvince() == null || "".equals(address.getProvince())){
			json.put("success", false);
			json.put("msg", "收货地址-省份不能为空！");
			return json;
		}
		if(address.getCity() == null || "".equals(address.getCity())){
			json.put("success", false);
			json.put("msg", "收货地址-市不能为空！");
			return json;
		}
		if(address.getArea() == null || "".equals(address.getArea())){
			json.put("success", false);
			json.put("msg", "收货地址-区不能为空！");
			return json;
		}
		if(address.getAddrName() == null || "".equals(address.getAddrName())){
			json.put("success", false);
			json.put("msg", "详细地址不能为空！");
			return json;
		}
		if(address.getPhone() == null || "".equals(address.getPhone())){
			json.put("success", false);
			json.put("msg", "手机号不能为空！");
			return json;
		}
		json.put("success", true);
		json.put("msg", "");
		return json;
	}

	public JSONObject saveAdd(BuyAddress address) {
		JSONObject json = verification(address);
		if(!json.getBoolean("success")){
			return json;
		}
		// 判断是否设置默认地址
		if(address.getIsDefault() == 0){
			cancelDefault(ShiroUtils.getCompId());
		}
		int result = 0;
		Date date = new Date();
		address.setId(ShiroUtils.getUid());
		address.setCompanyId(ShiroUtils.getCompId());
		address.setIsDel(Constant.IsDel.NODEL.getValue());
		address.setCreateId(ShiroUtils.getUserId());
		address.setCreateName(ShiroUtils.getUserName());
		address.setCreateDate(date);
		result = mapper.insertSelective(address);
		if(result > 0){
			json.put("success", true);
			json.put("msg", "保存成功！");
			address = selectById(address.getId());
			json.put("address", address);
		}else{
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}

	@Override
	public BuyAddress selectById(String id) {
		return mapper.selectById(id);
	}

	public JSONObject saveDelete(String id) {
		JSONObject json = new JSONObject();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		int result = 0;
		Date date = new Date();
		BuyAddress address = new BuyAddress();
		address.setId(id);
		address.setIsDel(Constant.IsDel.YESDEL.getValue());// 删除
		address.setDelId(user.getId());
		address.setDelName(user.getUserName());
		address.setDelDate(date);
		result = updateByPrimaryKeySelective(address);
		if(result > 0){
			json.put("success", true);
		}else{
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}
}
