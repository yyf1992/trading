package com.nuotai.trading.buyer.timer;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.dao.BuyProductSkuMapper;
import com.nuotai.trading.dao.BuyStockBacklogDetailsMapper;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuyStockBacklogDetails;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.model.oms.ScmProductOldsku;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 库存积压预警定时任务
 * @author
 *
 */
@Component
public class StockBacklogReport {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private BuyStockBacklogDetailsMapper buyStockBacklogDetailsMapper;
	@Autowired
	private BuyProductSkuMapper buyProductSkuMapper;
	
	//0 */1 * * * ? 每隔一分钟
	//0 0 21 * * ? 每天21点
	@Scheduled(cron="0 0 21 * * ?")
	public void catchBuyWarehouseBacklog(){
		TimeTask timeTask = timeTaskService.selectByCode("BUY_STOCK_BACKLOG");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					SearchPageUtil searchPageUtil = new SearchPageUtil();
					
					try {
						//计算库存积压时间
						warehouseBacklog(searchPageUtil);
					} catch (UnsupportedEncodingException e1) {
						e1.printStackTrace();
					}
					
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setCurrentStatus("2");
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	/**
	 * 商品库存同步积压表
	 * @throws UnsupportedEncodingException 
	 */
	@SuppressWarnings({ "unchecked" })
	public void warehouseBacklog(SearchPageUtil searchPageUtil) throws UnsupportedEncodingException{
		
		//获取商品库存
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
			searchPageUtil.getPage().setPageNo(1);
			searchPageUtil.getPage().setPageSize(2147483646);
		}

		//获取oms库存数据
		String levelUrl =  Constant.OMS_INTERFACE_URL + "getGoods?pageSize="
							+ searchPageUtil.getPage().getPageSize() + "&pageNo="
							+ searchPageUtil.getPage().getPageNo();

		
		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		JSONObject json = JSONObject.fromObject(arrayString);
		JSONArray productArray = JSONArray.fromObject(json.get("list"));
		List<ScmProductOldsku> list = (List<ScmProductOldsku>) JSONArray.toCollection(productArray, ScmProductOldsku.class);
		
		System.out.println("********库存同步开始**********");
		if (list != null && list.size()>0){
			//同步前清空库存子表数据
			buyStockBacklogDetailsMapper.deleteAllDetails();
			for (ScmProductOldsku prodcuts : list){				
//				Long num = prodcuts.getWarncount();
				Date date = new Date();
				//计算库存预警天数
				int result = 0;
				try {					
					result = calcBacklogDay(date,prodcuts);				
//					prodcuts.setRemark(result);
//					prodcuts.setWarncount(num);
					//将oms的商品库存加入买卖系统库存预警表
					//addBuyWarehouseBacklog(prodcuts);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("********库存同步结束**********");
		
	}
	
	/**
	 * 计算库存预警天数
	 * @param date
	 * @param prodcuts
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	private int calcBacklogDay(Date date,ScmProductOldsku prodcuts)  throws ParseException{
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd000000");
		String modifiedStr = sdf1.format(date);
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd235959");
		String modifiedStr2 = sdf2.format(date);
		if (prodcuts.getWarncount()>0){
			String skuoid = prodcuts.getBarcode().replaceAll("\\s*", "");
			
			//获取oms商品入库数据
			String storageUrl =  Constant.OMS_INTERFACE_URL + "getOmsRuKu?modifiedStr="+modifiedStr+"&modifiedStr2="+modifiedStr2+"&skuoid="+skuoid;
			String storageString = InterfaceUtil.searchLoginService(storageUrl);
			if (!ObjectUtil.isEmpty(storageString) && storageString.charAt(0) == '['){
				JSONArray jsonStorage = JSONArray.fromObject(storageString);
				List<Map<String, Object>> storageList = (List<Map<String, Object>>) JSONArray.toCollection(jsonStorage, Map.class);
				
				//获取该条形码对应的SKU信息
				List<BuyProductSku> buyProductSku = buyProductSkuMapper.getByBarcodeList(skuoid);
				
				for (Map<String, Object> mapRuku:storageList){
					BuyStockBacklogDetails backlogDetails = new BuyStockBacklogDetails();
					try {
						//将map转化为BuyWarehouseBacklogDetails对象
						backlogDetails = changeBacklogDetails(mapRuku);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					
					// 计算后的值重新存入BacklogNum
					int changeNum = backlogDetails.getNumber();
					int oldNumber = (int) (prodcuts.getWarncount()-changeNum);
					prodcuts.setWarncount((long)oldNumber);
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date newDate = new Date();
					try {
						newDate = sdf.parse(sdf.format(newDate));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					if (oldNumber>0){						
						backlogDetails.setRemainingStock((int)changeNum);		
						backlogDetails.setBacklogdaynum((int) ((newDate.getTime() - backlogDetails.getCreateDate().getTime()) / (24 * 60 * 60 * 1000)+1));
						//积压时间 = 当前时间-入库时间 - 商品自定义的销售时间
						if (ObjectUtil.isEmpty(buyProductSku)){
							backlogDetails.setBacklogmaxday(backlogDetails.getBacklogdaynum()-1);
						}else{
							backlogDetails.setBacklogmaxday(backlogDetails.getBacklogdaynum()-buyProductSku.get(0).getPlanSalesDays());
						}
						//将OMS采购入库数据中有积压库存的插入买卖系统库存积压明细表
						addBacklogDetails(backlogDetails);						
					}
					
					if (oldNumber<=0){						
						backlogDetails.setRemainingStock((int)(changeNum+prodcuts.getWarncount()));
						backlogDetails.setBacklogdaynum((int) ((newDate.getTime() - backlogDetails.getCreateDate().getTime()) / (24 * 60 * 60 * 1000)+1));
						//积压时间 = 当前时间-入库时间 - 商品自定义的销售时间
						if (ObjectUtil.isEmpty(buyProductSku)){
							backlogDetails.setBacklogmaxday(backlogDetails.getBacklogdaynum()-1);
						}else{
							backlogDetails.setBacklogmaxday(backlogDetails.getBacklogdaynum()-buyProductSku.get(0).getPlanSalesDays());
						}
						//将OMS采购入库数据中有积压库存的插入买卖系统库存积压明细表
						addBacklogDetails(backlogDetails);
					}
					//库存小于等于0时停止查询
					if (oldNumber<=0) {
						break;
					}
				}
			}
			
			Calendar calendar = new GregorianCalendar();
		    calendar.setTime(date);
		    calendar.add(Calendar.DATE,-1);
		    date=calendar.getTime();
		    //查询入库时间截止到20160701
		    if (date.getTime()<1467302400000L){
		    	return -1;
		    }
			calcBacklogDay(date,prodcuts);
		}
		
		return 1;
		
	}
	
	/**
	 * 将map转化为BuyWarehouseBacklogDetails对象
	 * @param mapRuku
	 * @return
	 * @throws ParseException 
	 */
	private BuyStockBacklogDetails changeBacklogDetails(Map<String, Object> mapRuku) throws ParseException{
		
		BuyStockBacklogDetails backlogDetails = new BuyStockBacklogDetails();
		backlogDetails.setId(mapRuku.get("id").toString());
		backlogDetails.setCompanyId(Constant.COMPANY_ID);
		backlogDetails.setBatchNo(mapRuku.get("sourcemaincode").toString());
		backlogDetails.setBusinessType(mapRuku.get("businesstype").toString());
		backlogDetails.setSkuCode(mapRuku.get("skucode").toString());
		backlogDetails.setSkuName(mapRuku.get("skuname").toString());
		backlogDetails.setProductCode(mapRuku.get("procode").toString());
		backlogDetails.setProductName(mapRuku.get("proname").toString());
		backlogDetails.setBarcode(mapRuku.get("skuoid").toString());
		backlogDetails.setNumber((Integer)mapRuku.get("usecount"));

		if (mapRuku.containsKey("created")){
			Date createDate = new Date((long) (Long.valueOf(mapRuku.get("created").toString())));
			backlogDetails.setCreateDate(createDate);
		}
		if (mapRuku.containsKey("modified")){
			Date updateDate = new Date((long) (Long.valueOf(mapRuku.get("modified").toString())));
			backlogDetails.setUpdateDate(updateDate);
		}
		if (mapRuku.containsKey("finishdate")){
			Date storageDate = new Date((long) (Long.valueOf(mapRuku.get("finishdate").toString())));
			backlogDetails.setStorageDate(storageDate);
		}else{
			if (mapRuku.containsKey("modified")){
				Date updateDate = new Date((long) (Long.valueOf(mapRuku.get("modified").toString())));
				backlogDetails.setStorageDate(updateDate);
			}
		}
	
		return backlogDetails;
		
	}
	
//	/**
//	 * 将oms的商品库存加入买卖系统库存预警表
//	 * @param prodcuts
//	 */
//	private void addBuyWarehouseBacklog(ScmProductOldsku prodcuts){
//		
//		BuyStockBacklog backlog = new BuyStockBacklog();
//		backlog.setId(prodcuts.getId().toString());
//		backlog.setCompanyId("17060909542281121440");
////		backlog.setBacklogWarning(prodcuts.getBacklogWarning());
//		backlog.setStocks(Integer.parseInt(prodcuts.getWarncount().toString()));
//		backlog.setProductCode(prodcuts.getProductCode());
//		backlog.setProductName(prodcuts.getGoodsName());
//		backlog.setSkuCode(prodcuts.getSkuCode());
//		backlog.setSkuName(prodcuts.getSkuName());
//		backlog.setBarcode(prodcuts.getBarcode());
//		backlog.setCreateDate(new Date());
//		backlog.setRemark(prodcuts.getRemark());
//		int count = buyStockBacklogMapper.selectById(prodcuts.getBarcode());
//		if (count >0){
//			buyStockBacklogMapper.update(backlog);
//		}else{
//			//将商品库存同步到库存积压表		
//			buyStockBacklogMapper.add(backlog);
//		}			
//		
//	}
	
	/**
	 * 将oms采购入库数据中有积压库存的插入买卖系统库存积压明细表
	 * @param backlogDetails
	 */
	private void addBacklogDetails(BuyStockBacklogDetails buyStockBacklogDetails){

		System.out.println("********入库同步开始**********");
		int count = buyStockBacklogDetailsMapper.selectById(buyStockBacklogDetails.getId());
		if (count > 0){
			buyStockBacklogDetailsMapper.update(buyStockBacklogDetails);
		}else{
			//将入库明细加入积压明细表
			buyStockBacklogDetailsMapper.add(buyStockBacklogDetails);
		}				
		System.out.println("********入库同步结束**********");		
		
	}

}
