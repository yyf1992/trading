<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.nuotai.trading.seller.dao.buyer.SBuyBillInvoiceMapper">
	<sql id="all">
		  id , 	 
		  settlement_no AS settlementNo , 	  
		  company_id AS companyId , 	  
		  supplier_id AS supplierId , 	  
		  invoice_no AS invoiceNo , 	  
		  invoice_type AS invoiceType , 	  
		  invoice_header AS invoiceHeader , 	  
		  total_commodity_amount AS totalCommodityAmount , 	  
		  total_invoice_value AS totalInvoiceValue ,
		  invoice_file_addr AS  invoiceFileAddr,
		  freight , 	 
		  overdue_interest AS overdueInterest , 	  
		  amount_payable AS amountPayable , 	  
		  actual_payment_amount AS actualPaymentAmount , 	  
		  residual_payment_amount AS residualPaymentAmount , 	  
		  create_date AS createDate , 	  
		  accept_invoice_status AS acceptInvoiceStatus , 	  
		  payment_status AS paymentStatus 	  
	   </sql>

	<sql id="allNoAs">
			id , 
			settlement_no , 
			company_id , 
			supplier_id , 
			invoice_no , 
			invoice_type , 
			invoice_header , 
			total_commodity_amount , 
			total_invoice_value ,
			invoice_file_addr,
			create_date ,
			accept_invoice_status
	</sql>

	<sql id="BuyBillInvoice_condition">
   	<if test="settlementNo != null and settlementNo != ''">
		  AND settlement_no = #{settlementNo}  
	 </if>
   	<if test="companyId != null and companyId != ''">
		  AND company_id = #{companyId}  
	 </if>
   	<if test="supplierId != null and supplierId != ''">
		  AND supplier_id = #{supplierId}  
	 </if>
   	<if test="invoiceNo != null and invoiceNo != ''">
		  AND invoice_no = #{invoiceNo}  
	 </if>
   	<if test="invoiceType != null and invoiceType != ''">
		  AND invoice_type = #{invoiceType}  
	 </if>
   	<if test="invoiceHeader != null and invoiceHeader != ''">
		  AND invoice_header = #{invoiceHeader}  
	 </if>
   	<if test="totalCommodityAmount != null and totalCommodityAmount != 0 ">
		  AND total_commodity_amount = #{totalCommodityAmount}  
	 </if>
   	<if test="totalInvoiceValue != null and totalInvoiceValue != 0 ">
		  AND total_invoice_value = #{totalInvoiceValue}  
	 </if>
   	<if test="freight != null and freight != 0 ">
		  AND freight = #{freight}  
	 </if>
   	<if test="overdueInterest != null and overdueInterest != 0 ">
		  AND overdue_interest = #{overdueInterest}  
	 </if>
   	<if test="amountPayable != null and amountPayable != 0 ">
		  AND amount_payable = #{amountPayable}  
	 </if>
   	<if test="actualPaymentAmount != null and actualPaymentAmount != 0 ">
		  AND actual_payment_amount = #{actualPaymentAmount}  
	 </if>
   	<if test="residualPaymentAmount != null and residualPaymentAmount != 0 ">
		  AND residual_payment_amount = #{residualPaymentAmount}  
	 </if>
   	<if test="createDate != null">
		  AND create_date = #{createDate}  
	 </if>
   	<if test="acceptInvoiceStatus != null and acceptInvoiceStatus != ''">
		  AND accept_invoice_status = #{acceptInvoiceStatus}  
	 </if>
   	<if test="paymentStatus != null and paymentStatus != ''">
		  AND payment_status = #{paymentStatus}
	 </if>
  	</sql>

	<select id="get" resultType="com.nuotai.trading.seller.model.buyer.SBuyBillInvoice">
		SELECT 
		<include refid="all" />
		FROM buy_bill_invoice 
		WHERE
		id = #{value}
	</select>

	<select id="queryList" parameterType = "map" resultType="com.nuotai.trading.seller.model.buyer.SBuyBillInvoice">
		SELECT
		<include refid="all" />
		FROM buy_bill_invoice
		WHERE
		del_date is null
		<include refid="BuyBillInvoice_condition"/>
	</select>

 	<select id="queryCount"  parameterType = "map" resultType="int">
		SELECT
		COUNT(1)
		FROM buy_bill_invoice
		WHERE
		del_date is null
		<include refid="BuyBillInvoice_condition"/>
	</select>

	<!--同步发票数据信息-->
	<insert id="addBuyInvoice" parameterType="com.nuotai.trading.seller.model.buyer.SBuyBillInvoice">
		INSERT into buy_bill_invoice
		(
         <include refid="allNoAs" />
		)
		VALUES
		(
	    #{id},
	    #{settlementNo}, 
	    #{companyId}, 
	    #{supplierId}, 
	    #{invoiceNo}, 
	    #{invoiceType}, 
	    #{invoiceHeader}, 
	    #{totalCommodityAmount}, 
	    #{totalInvoiceValue},
		#{invoiceFileAddr},
		#{createDate},
	    #{acceptInvoiceStatus}
		)
	</insert>
	 
	<update id="updateInvoiceInfo" parameterType="com.nuotai.trading.seller.model.buyer.SBuyBillInvoice">
		UPDATE buy_bill_invoice
		<trim prefix="set" suffixOverrides=",">
			<if test="settlementNo != null and settlementNo != ''">
				settlement_no = #{settlementNo},
			</if>
			<if test="companyId != null and companyId != ''">
				company_id = #{companyId},
			</if>
			<if test="supplierId != null and supplierId != ''">
				supplier_id = #{supplierId},
			</if>
			<if test="invoiceNo != null and invoiceNo != ''">
				invoice_no = #{invoiceNo},
			</if>
			<if test="invoiceType != null and invoiceType != ''">
				invoice_type = #{invoiceType},
			</if>
			<if test="invoiceHeader != null and invoiceHeader != ''">
				invoice_header = #{invoiceHeader},
			</if>
			<if test="totalCommodityAmount != null and totalCommodityAmount != ''">
				total_commodity_amount = #{totalCommodityAmount},
			</if>
			<if test="totalInvoiceValue != null and totalInvoiceValue != ''">
				total_invoice_value = #{totalInvoiceValue},
			</if>
			<if test="invoiceFileAddr != null and invoiceFileAddr != ''">
				invoice_file_addr = #{invoiceFileAddr},
			</if>
			<if test="freight != null and freight != ''">
				freight = #{freight},
			</if>
			<if test="overdueInterest != null and overdueInterest != ''">
				overdue_interest = #{overdueInterest},
			</if>
			<if test="amountPayable != null and amountPayable != ''">
				amount_payable = #{amountPayable},
			</if>
			<if test="actualPaymentAmount != null and actualPaymentAmount != ''">
				actual_payment_amount = #{actualPaymentAmount},
			</if>
			<if test="residualPaymentAmount != null and residualPaymentAmount != ''">
				residual_payment_amount = #{residualPaymentAmount},
			</if>
			<if test="createDate != null">
				create_date = #{createDate,jdbcType=TIMESTAMP},
			</if>
			<if test="acceptInvoiceStatus != null and acceptInvoiceStatus != ''">
				accept_invoice_status = #{acceptInvoiceStatus},
			</if>
			<if test="paymentStatus != null and paymentStatus != ''">
				payment_status = #{paymentStatus},
			</if>
		</trim>
		WHERE id = #{id}
	</update>
	
	<delete id="delete">
		UPDATE buy_bill_invoice  SET
           del_id = #{delId},
           del_name  = #{delName},
           del_date = #{delDate}
		WHERE id = #{id}
	</delete>

	<!--发票核实-->
	<update id="updateBillInvoice" parameterType="map">
		UPDATE buy_bill_invoice
		<trim prefix="set" suffixOverrides=",">
			<if test="acceptInvoiceStatus != null and acceptInvoiceStatus != ''">
				accept_invoice_status = #{acceptInvoiceStatus},
			</if>
			<if test="remarks != null and remarks != ''">
				remarks = #{remarks},
			</if>
		</trim>
		WHERE 1=1
		<if test="id != null and id != ''">
			AND id = #{id}
		</if>
	</update>

</mapper>