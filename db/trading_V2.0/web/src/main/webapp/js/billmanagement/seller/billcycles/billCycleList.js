//关联利息弹出框
function showBillCycleInterest(billCycleId) {
    //var buyCompanyName = buyCompanyName;
   // alert(buyCompanyName);
    findInterest(billCycleId);
    layer.open({
        type: 1,
        title: '关联利息明细',
        area: ['420px', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkInterest'),
        yes : function(index){
            layer.close(index);
        }/*,
        no : function(index){
            layer.close(index);
        }*/
    });
}

//利息数据查询
function findInterest(billCycleId) {
    $("#interestBody").empty();
    data={
            "billCycleId":billCycleId
        };
    $.ajax({
        url : basePath+"platform/seller/billCycle/queryInterestList",
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var innerHtml = $("<tr></tr>");
            $.each(resultObj,function(i,o){
                //逾期天
                var overdueDate = o.overdueDate;
                //月利率
                var overdueInterest = o.overdueInterest;
                //利息计算方式
                //var interestMethod = o.interestCalculationMethod;
                var interestMethod
                if(o.interestCalculationMethod == '1'){
                	 interestMethod = '单利';
                }else if(o.interestCalculationMethod == '2'){
                	 interestMethod = '复利';
                }

                var trObj = $("<tr>");
               // $("<td><span class='comm_sel'><input type='checkbox'  value='"+linkcompanyid+"'/></span></td>").appendTo(trObj);
                $("<td>"+overdueDate+"</td>").appendTo(trObj);
                $("<td>"+overdueInterest+"</td>").appendTo(trObj);
                $("<td>"+interestMethod+"</td>").appendTo(trObj);
                
                
                $("#interestBody").append(trObj);
            });

        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}



//账单结算周期审批
//$('.bill_approval').click(function(){
function showBillCycleAgree(billCycleId) {
  layer.open({
    type:1,
    title:'账单审批',
    area:['380px','auto'],
    skin:'popBarcode',
    closeBtn:2,
    content:$('.bill_exam'),
    btn:['确定','取消'],
    yes:function(index){
    	var	agreeStatus = $("input[name='agreeStatus']:checked").val();
    	//alert("审批选择"+agreeStatus)
    	var	approvalRemarks = $("#approvalRemarks").val();
    	if(!agreeStatus){
    		layer.msg("请选择审批状态！",{icon:2});
    	}else if(approvalRemarks.length >300){
           layer.msg("备注内容不超过300个字符！",{icon:2});
    	}else{
    		$.ajax({
                type : "POST",
                url : "platform/seller/billCycle/updateSaveBillCycleInfo",
                data: {
                	"billCycleId": billCycleId,
                    "billDealStatus": agreeStatus,
                    "dealRemarks": approvalRemarks
                },
                async:false,
                success:function(data){
                    layer.close(index);
                    layer.msg("保存成功！",{icon:1});
                    loadPlatformData();
                },
                error:function(){
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
    		});
    	}
    },
    no : function(index){
        layer.close(index);
    }
  });
}  
//});

//删除供应商
function deleteBillCycle(id){
	layer.confirm('确定删除该账单周期吗？', {
	      icon:3,
	      title:'提示',
	      closeBtn:2,
          skin:'popBarcode',
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/buyer/billCycle/deleteBillCycle",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1},function(){
							leftMenuClick(this,"platform/buyer/billCycle/billCycleList?queryType=0","buyer");
						});
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}