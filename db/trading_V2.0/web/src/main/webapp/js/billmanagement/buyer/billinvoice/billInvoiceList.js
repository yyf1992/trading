/**
 * 导出
 */
function buyInvoiceExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/buyer/billInvoice/buyInvoiceExport?"+ formData;
    window.open(url);
}

//商品明细
function recItemList(settlementNo,invoiceId) {
    findRecItem(invoiceId);
    layer.open({
        type: 1,
        title: '结算单号'+settlementNo+'的商品明细',
        area: ['900px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#recItemList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//商品明细数据查询
function findRecItem(invoiceId) {
    $("#recItemBody").empty();
    data={
        "invoiceId":invoiceId
    };
    $.ajax({
        url : basePath+"platform/buyer/billInvoice/queryRecItemList",
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var arrivalNumCount = resultObj.arrivalNumCount;
            var priceSum = resultObj.priceSum;
            var deliveryItemList = resultObj.deliveryItemList;
            var innerHtml = $("<tr></tr>");
            $.each(deliveryItemList,function(i,o){
                //发货单号
                var orderCode = o.deliveryRecItem.orderCode;
                //发货单号
                var createBillName = o.deliveryRecItem.createBillName;
                //订单号
                var deliverNo = o.deliveryRecItem.deliverNo;
                //商品
                var productName = o.deliveryRecItem.productName;
                //条形码
                var barcode = o.deliveryRecItem.barcode;
                //单位
                var unitName = o.deliveryRecItem.unitName;
                //单价
                var salePrice = o.salePrice;
                //到货数量
                var arrivalNum = o.deliveryRecItem.arrivalNum;
                //总金额
                var salePriceSum = o.salePriceSum;

                var trObj = $("<tr>");
                $("<td>"+deliverNo+"</td>").appendTo(trObj);
                $("<td>"+createBillName+"</td>").appendTo(trObj);
                $("<td>"+orderCode+"</td>").appendTo(trObj);
                $("<td>"+productName+"</td>").appendTo(trObj);
                $("<td>"+barcode+"</td>").appendTo(trObj);
                $("<td>"+unitName+"</td>").appendTo(trObj);
                $("<td>"+salePrice+"</td>").appendTo(trObj);
                $("<td>"+arrivalNum+"</td>").appendTo(trObj);
                $("<td title='"+salePriceSum+"'>"+salePriceSum+"</td>").appendTo(trObj);

                $("#recItemBody").append(trObj);
            });

            var trSumObj = $("<tr>");
            var heji = "合计";
            $("<td>"+heji+"</td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td></td>").appendTo(trSumObj);
            $("<td>"+arrivalNumCount+"</td>").appendTo(trSumObj);
            $("<td>"+priceSum+"</td>").appendTo(trSumObj);

            $("#recItemBody").append(trSumObj);

        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//发票票据
function showInvoiceReceipt(settlementNo,invoiceFileAddr) {
    //var buyCompanyName = buyCompanyName;
    // alert(buyCompanyName);
    findInvoiceReceipt(invoiceFileAddr);
    layer.open({
        type: 1,
        title: '结算单号：'+settlementNo+"的票据",
        area: ['auto', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#invoiceReceipt'),
        yes : function(index){
            layer.close(index);
        }/*,
         no : function(index){
         layer.close(index);
         }*/
    });
}

//收据发票
function findInvoiceReceipt(invoiceFileAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(invoiceFileAddr != null || invoiceFileAddr != ''){
        var bigImg = '';
        var receiptAddrList = invoiceFileAddr.split(',');
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src='"+receiptAddrList[i]+"'></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

//核实票据
function showBillInvoiceAgree(billInvoiceId) {
    layer.open({
        type:1,
        title:'票据核实意见',
        area:['380px','auto'],
        skin:'popBuyer',
        closeBtn:2,
        content:$('.bill_exam'),
        btn:['确定','取消'],
        yes:function(index){
            var	agreeStatus = $("input[name='agreeStatus']:checked").val();
            //alert("审批选择"+agreeStatus)
            var	approvalRemarks = $("#approvalRemarks").val();
            if(!agreeStatus){
                layer.msg("请选择审批状态！",{icon:2});
            }else if(approvalRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
            }else{
                $.ajax({
                    type : "POST",
                    url : basePath+"platform/buyer/billInvoice/updateBillInvoice",
                    data: {
                        "id": billInvoiceId,
                        "acceptInvoiceStatus": agreeStatus,
                        "remarks": approvalRemarks
                    },
                    async:false,
                    success:function(data){
                        /*layer.close(index);
                        layer.msg("保存成功！",{icon:1});*/
                        var result = eval('(' + data + ')');
                        var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                        if(resultObj.success){
                            layer.close(index);
                            layer.msg(resultObj.msg,{icon:1});
                            loadPlatformData();
                            /*layer.msg(resultObj.msg,{icon:1});
                            leftMenuClick(this,'platform/buyer/billInvoice/billInvoiceList?acceptInvoiceStatus=0','buyer')*/
                            //loadPlatformData();
                        }else{
                            layer.msg(resultObj.msg,{icon:2});
                        }
                    },
                    error:function(){
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}