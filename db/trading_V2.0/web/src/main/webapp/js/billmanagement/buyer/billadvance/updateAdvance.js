$(function(){
    /*var urlStrOld = $("#urlStrOld").val();
    var urlList = new Array();
    urlList = urlStrOld.split(",");
    for(var i=0;i<urlList.length;i++){
        var url = urlList[i];
        var a = "<span>"+
            "<b onclick='deletePhoto();'></b>"+
            "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#advanceFileDiv\");'>"+
            "<input type='hidden' name='fileUrl' value='"+url+"'>"+
            "</span>";
        $("#advanceFileDiv").append(a);
    }
    //重新设置input按钮，防止无法重复提交文件
    $("#advanceFileAddr").replaceWith('<input type="file" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadInvoice();" title="'+new Date()+'"/>');*/
});

//提交账单周期信息
$('#billCycleInfo_add').click(function(){
    debugger
    //预付款编号
    var advanceId = $("#advanceId").val()
    //供应商
    var companyId= $("#sellerCompanyId").val();
    //下单人
    var userId=$("#createBillUserId").val();

    var usableTotal = $("#usableTotal").val();
    if(!usableTotal){
        layer.msg("请填写预付款金额！",{icon:2});
        return;
    }else if(usableTotal < 0){
        layer.msg("充值金额不得小于0！",{icon:2});
        return;
    }
    var taskFileStr = ""
    var attachments = $("input[name='fileUrl']");
    /*if(attachments==undefined||attachments.length<=0){
        layer.msg("请上传附件！",{icon:2});
        return;
    }*/
    if(attachments!=undefined&&attachments.length>0){
        for(var i=0;i<attachments.length;i++){
            //attachmentsArr.push(attachments[i].value);
            var fileAddr = attachments[i].value;
            taskFileStr += fileAddr + ",";
        }
    }
    taskFileStr = taskFileStr.substring(0,taskFileStr.length-1);

    var	taskRemarks = $("#taskRemarks").val();
    if(taskRemarks.length >300){
        layer.msg("备注内容不超过300个字符！",{icon:2});
        return;
    }
    //保存数据
    var url = basePath+"platform/buyer/billAdvance/updateAdvanceInfo";
    $.ajax({
        type : "POST",
        url : url,
        async:false,
        data: {
            "advanceId":advanceId,
            "sellerCompanyId":companyId,
            "userId":userId,
            "usableTotal":usableTotal,
            "advanceFileStr":taskFileStr,
            "advanceRemarks":taskRemarks,
        },
        success:function(data){
            var result = eval("(" + data + ")");
            var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
            if(resultObj.success){
                layer.msg(resultObj.msg,{icon:1});
                leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer')
            }else{
                layer.msg(resultObj.msg,{icon:2});
            }
        },
        error:function(){
            layer.msg("保存失败，请稍后重试！",{icon:2});
        }
    });
});

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("advanceFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billPaymentDetail/uploadPaymentBill";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#advanceFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#advanceFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#advanceFileAddr").replaceWith('<input type="file" style="opacity:1" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }
}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}