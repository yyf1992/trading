//商品明细
function showInventory() {
    findInventory();
    layer.open({
        type: 1,
        title: '结算单号'+'xxx'+'的商品明细',
        area: ['900px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkInventory'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//商品明细数据查询
function findInventory() {
    $("#inventoryBody").html("");
    var trObj1 = $("<tr>");
    var trObj2 = $("<tr>");
    var trObj3 = $("<tr>");

    $("<td>"+"588525666526666"+"</td>").appendTo(trObj1);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj1);
    $("<td>"+"细跟高跟女鞋2017春季新品尖头浅口低帮鞋欧美单鞋四季职业工作鞋"+"</td>").appendTo(trObj1);
    $("<td>"+"15646453464"+"</td>").appendTo(trObj1);
    $("<td>"+"件"+"</td>").appendTo(trObj1);
    $("<td>"+"85.00"+"</td>").appendTo(trObj1);
    $("<td>"+"0.00"+"</td>").appendTo(trObj1);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj1);
    $("<td>"+"8500.00"+"</td>").appendTo(trObj1);

    $("<td>"+"588525666526666"+"</td>").appendTo(trObj2);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj2);
    $("<td>"+"细跟高跟女鞋2017春季新品"+"</td>").appendTo(trObj2);
    $("<td>"+"15646453464"+"</td>").appendTo(trObj2);
    $("<td>"+"件"+"</td>").appendTo(trObj2);
    $("<td>"+"85.00"+"</td>").appendTo(trObj2);
    $("<td>"+"0.00"+"</td>").appendTo(trObj2);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj2);
    $("<td>"+"8500.00"+"</td>").appendTo(trObj2);

    $("<td>"+"588525666526666"+"</td>").appendTo(trObj3);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj3);
    $("<td>"+"女鞋2017春季新品"+"</td>").appendTo(trObj3);
    $("<td>"+"15646453464"+"</td>").appendTo(trObj3);
    $("<td>"+"件"+"</td>").appendTo(trObj3);
    $("<td>"+"85.00"+"</td>").appendTo(trObj3);
    $("<td>"+"0.00"+"</td>").appendTo(trObj3);
    $("<td>"+"3237384684674795"+"</td>").appendTo(trObj3);
    $("<td>"+"8500.00"+"</td>").appendTo(trObj3);

    $("#inventoryBody").append(trObj1);
    $("#inventoryBody").append(trObj2);
    $("#inventoryBody").append(trObj3);
}


//收款票据
function showBillReceipt() {
    //var buyCompanyName = buyCompanyName;
   // alert(buyCompanyName);
    findBillReceipt();
    layer.open({
        type: 1,
        title: '收获回单',
        area: ['420px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkReceipt'),
        yes : function(index){
            layer.close(index);
        }/*,
        no : function(index){
            layer.close(index);
        }*/
    });
}

//收据发票
function findBillReceipt() {
    $("#icon_list").html("");
    var trObj1 = $("<li>");
    var trObj2 = $("<li>");
    var trObj3 = $("<li>");
    var trObj4 = $("<li>");
    var trObj5 = $("<li>");
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj1);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj2);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj3);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj4);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj5);

    $("#icon_list").append(trObj1);
    $("#icon_list").append(trObj2);
    $("#icon_list").append(trObj3);
    $("#icon_list").append(trObj4);
    $("#icon_list").append(trObj5);
}

//收款凭据
function showVoucher() {
    //var buyCompanyName = buyCompanyName;
    // alert(buyCompanyName);
    findVoucher();
    layer.open({
        type: 1,
        title: '收获回单',
        area: ['420px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkVoucher'),
        yes : function(index){
            layer.close(index);
        }/*,
         no : function(index){
         layer.close(index);
         }*/
    });
}

//收据凭据
function findVoucher() {
    $("#voucherList").html("");
    var trObj1 = $("<li>");
    var trObj2 = $("<li>");
    var trObj3 = $("<li>");
    var trObj4 = $("<li>");
    var trObj5 = $("<li>");
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj1);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj2);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj3);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj4);
    $("<img src='<%=basePath%>/statics/platform/images/01.png'/>").appendTo(trObj5);

    $("#voucherList").append(trObj1);
    $("#voucherList").append(trObj2);
    $("#voucherList").append(trObj3);
    $("#voucherList").append(trObj4);
    $("#voucherList").append(trObj5);
}

//票据核实
function showPaymentAgree() {
    layer.open({
        type:1,
        title:'票据核实意见',
        area:['500px','auto'],
        skin:'popBuyer',
        closeBtn:2,
        content:$('#paymentAgree'),
        btn:['确定','取消'],
        yes:function(index){
            layer.close(index);
            /*var	agreeStatus = $("input[name='agreeStatus']:checked").val();
            //alert("审批选择"+agreeStatus)
            var	approvalRemarks = $("#approvalRemarks").val();
            if(!agreeStatus){
                layer.msg("请选择审批状态！",{icon:2});
            }else if(approvalRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
            }else{
                $.ajax({
                    type : "POST",
                    url : "platform/buyer/billCycle/updateSaveBillCycleInfo",
                    data: {
                        "billCycleId": billCycleId,
                        "billDealStatus": agreeStatus,
                        "dealRemarks": approvalRemarks
                    },
                    async:false,
                    success:function(data){
                        layer.close(index);
                        layer.msg("保存成功！",{icon:1});
                        loadPlatformData();
                    },
                    error:function(){
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                });
            }*/
        },
        no : function(index){
            layer.close(index);
        }
    });
}

//删除供应商
function beginAccept(){
	layer.confirm('确定发起对账吗？', {
	      icon:3,
	      title:'提示',
	      closeBtn:2,
          skin:'popBuyer',
	      btn: ['确定','取消']
	    },
        function(index){
        layer.close(index);
    });
    /*function(){
	    	$.ajax({
				type : "post",
				url : "platform/buyer/billCycle/deleteBillCycle",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1},function(){
							leftMenuClick(this,"platform/buyer/billCycle/billCycleList?queryType=0","buyer");
						});
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });*/
}