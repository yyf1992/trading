//关联利息弹出框
function showBillCycleInterest(billCycleId) {
    //var buyCompanyName = buyCompanyName;
   // alert(buyCompanyName);
    findInterest(billCycleId);
    layer.open({
        type: 1,
        title: '关联利息明细',
        area: ['420px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkInterest'),
        yes : function(index){
            layer.close(index);
        }/*,
        no : function(index){
            layer.close(index);
        }*/
    });
}

//利息数据查询
function findInterest(billCycleId) {
    $("#interestBody").empty();
    data={
            "billCycleId":billCycleId
        };
    $.ajax({
        url : basePath+"platform/seller/billCycleNew/queryInterestNewList",
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var innerHtml = $("<tr></tr>");
            $.each(resultObj,function(i,o){
                //逾期天
                var overdueDate = o.overdueDate;
                //月利率
                var overdueInterest = o.overdueInterest;
                //利息计算方式
                //var interestMethod = o.interestCalculationMethod;
                var interestMethod
                if(o.interestCalculationMethod == '1'){
                	 interestMethod = '单利';
                }else if(o.interestCalculationMethod == '2'){
                	 interestMethod = '复利';
                }

                var trObj = $("<tr>");
               // $("<td><span class='comm_sel'><input type='checkbox'  value='"+linkcompanyid+"'/></span></td>").appendTo(trObj);
                $("<td>"+overdueDate+"</td>").appendTo(trObj);
                $("<td>"+overdueInterest+"</td>").appendTo(trObj);
                $("<td>"+interestMethod+"</td>").appendTo(trObj);
                
                
                $("#interestBody").append(trObj);
            });

        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//利息显示或隐藏
var hideOrShow = true;
function applyHidOrShowInterst() {
    if(hideOrShow){
        $("#applyInterstHidOrShow").show();
        hideOrShow = false;
    }else {
        $("#applyInterstHidOrShow").hide();
        hideOrShow = true
    }
}

//账单结算周期审批
//$('.bill_approval').click(function(){
function showBillCycleAgree(billCycleNewId) {
    findContrast(billCycleNewId);
  layer.open({
    type:1,
    title:'账单修改申请审批',
    area:['880px','728px'],
    skin:'popBarcode',
    closeBtn:2,
    content:$('.bill_exam'),
    btn:['确定','取消'],
    yes:function(index){
    	var	agreeStatus = $("input[name='agreeStatus']:checked").val();
    	//alert("审批选择"+agreeStatus)
    	var	approvalRemarks = $("#approvalRemarks").val();
    	if(!agreeStatus){
    		layer.msg("请选择审批状态！",{icon:2});
    	}else if(approvalRemarks.length >300){
           layer.msg("备注内容不超过300个字符！",{icon:2});
    	}else{
    		$.ajax({
                type : "POST",
                url : "platform/seller/billCycleNew/updateBillCycleNew",
                data: {
                	"billCycleId": billCycleNewId,
                    "billDealStatus": agreeStatus,
                    "dealRemarks": approvalRemarks
                },
                async:false,
                success:function(data){
                    layer.close(index);
                    layer.msg("保存成功！",{icon:1});
                    loadPlatformData();
                },
                error:function(){
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
    		});
    	}
    },
    no : function(index){
        layer.close(index);
    }
  });
}

//利息数据查询
function findContrast(billCycleNewId) {
    $("#contrastData").empty();
    $("#interestNewBody").empty();
    $("#interestOldBody").empty();

    data={
        "billCycleId":billCycleNewId
    };
    $.ajax({
        url : basePath+"platform/seller/billCycleNew/queryContrastList",
        data: data,
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            var sellerBillCycleNew = resultObj.sellerBillCycleManagementNew;//申请数据
            var sellerBillCycle = resultObj.sellerBillCycleManagement;//正式数据
            var sellerBillInterestNewList = resultObj.sellerBillInterestNewList;//申请关联利息
            var sellerBillInterestList = resultObj.sellerBillInterestList;//正式关联利息

            /*var checkOutCycleHtml = $("<tr>");
            $("<td>"+"申请结账周期（天）:"+"<span>"+sellerBillCycleNew.checkoutCycle+"</span>"+"</td>").appendTo(checkOutCycleHtml);
            $("<td>"+"正式结账周期（天）:"+"<span>"+sellerBillCycle.checkoutCycle+"</span>"+"</td>").appendTo(checkOutCycleHtml);
            $("#contrastData").append(checkOutCycleHtml)*/
            var statementDateHtml = $("<tr>");
            $("<td>"+"申请出账日期（日）: "+"<span>"+sellerBillCycleNew.billStatementDate+"</span>"+"</td>").appendTo(statementDateHtml);
            $("<td>"+"正式出账日期（日）: "+"<span>"+sellerBillCycle.billStatementDate+"</span>"+"</td>").appendTo(statementDateHtml);
            $("#contrastData").append(statementDateHtml)
            var createUserHtml = $("<tr>");
            $("<td>"+"申请创建人 : "+"<span>"+sellerBillCycleNew.createUserName+"</span>"+"</td>").appendTo(createUserHtml);
            $("<td>"+"正式创建人 : "+"<span>"+sellerBillCycle.createUserName+"</span>"+"</td>").appendTo(createUserHtml);
            $("#contrastData").append(createUserHtml)
            var createDateHtml = $("<tr>");
            $("<td>"+"申请创建日期（日）: "+"<span>"+sellerBillCycleNew.createDate+"</span>"+"</td>").appendTo(createDateHtml);
            $("<td>"+"正式创建日期（日）: "+"<span>"+sellerBillCycle.createDate+"</span>"+"</td>").appendTo(createDateHtml);
            $("#contrastData").append(createDateHtml)

            $.each(sellerBillInterestNewList,function(i,o){
                //逾期天
                var overdueDate = o.overdueDate;
                //月利率
                var overdueInterest = o.overdueInterest;
                //利息计算方式
                //var interestMethod = o.interestCalculationMethod;
                var interestMethod
                if(o.interestCalculationMethod == '1'){
                    interestMethod = '单利';
                }else if(o.interestCalculationMethod == '2'){
                    interestMethod = '复利';
                }
                var trObj = $("<tr>");
                $("<td>"+overdueDate+"</td>").appendTo(trObj);
                $("<td>"+overdueInterest+"</td>").appendTo(trObj);
                $("<td>"+interestMethod+"</td>").appendTo(trObj);
                $("#interestNewBody").append(trObj);
            });

            $.each(sellerBillInterestList,function(i,o){
                //逾期天
                var overdueDate = o.overdueDate;
                //月利率
                var overdueInterest = o.overdueInterest;
                //利息计算方式
                //var interestMethod = o.interestCalculationMethod;
                var interestMethod
                if(o.interestCalculationMethod == '1'){
                    interestMethod = '单利';
                }else if(o.interestCalculationMethod == '2'){
                    interestMethod = '复利';
                }
                var trObj = $("<tr>");
                $("<td>"+overdueDate+"</td>").appendTo(trObj);
                $("<td>"+overdueInterest+"</td>").appendTo(trObj);
                $("<td>"+interestMethod+"</td>").appendTo(trObj);
                $("#interestOldBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}