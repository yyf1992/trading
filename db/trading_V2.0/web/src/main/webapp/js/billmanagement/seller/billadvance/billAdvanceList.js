/**
 * 导出
 */
function sellerAdvanceExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/seller/billAdvance/sellerAdvanceExport?"+ formData;
    window.open(url);
}

//发票票据
function showAdvanceReceipt(advanceId,fileAddress,createBillUserName,createTimeStr) {
    findAdvanceReceipt(fileAddress);
    layer.open({
        type: 1,
        title: '采购员：'+createBillUserName+"在"+createTimeStr+"日创建预付款的票据",
        area: ['auto', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#advanceReceipt'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//收据发票
function findAdvanceReceipt(advanceFileAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(advanceFileAddr != null || advanceFileAddr != ''){
        var bigImg = '';
        var receiptAddrList = advanceFileAddr.split(',');
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src='"+receiptAddrList[i]+"'></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

//核实票据
function showAdvanceAgree(advanceId) {
    layer.open({
        type:1,
        title:'预付款审核',
        area:['380px','auto'],
        skin:'popBarcode',
        closeBtn:2,
        content:$('#advanceApproval'),
        btn:['确定','取消'],
        yes:function(index){
            var	agreeStatus = $("input[name='approvalStatus']:checked").val();
            var	approvalRemarks = $("#approvalRemarks").val();
            if(!agreeStatus){
                layer.msg("请选择审批状态！",{icon:2});
                return;
            }else if(approvalRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else{
                $.ajax({
                    type : "POST",
                    url : basePath+"platform/seller/billAdvance/updateAdvance",
                    data: {
                        "id": advanceId,
                        "acceptStatus": agreeStatus,
                        "remarks": approvalRemarks
                    },
                    async:false,
                    success:function(data){
                        var result = eval('(' + data + ')');
                        var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                        if(resultObj.success){
                            layer.close(index);
                            layer.msg(resultObj.msg,{icon:1});
                            loadPlatformData();
                            /*layer.msg(resultObj.msg,{icon:1});
                             leftMenuClick(this,'platform/buyer/billInvoice/billInvoiceList?acceptInvoiceStatus=0','buyer')*/
                            //loadPlatformData();
                        }else{
                            layer.msg(resultObj.msg,{icon:2});
                        }
                    },
                    error:function(){
                        layer.msg("提交失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}

//查看充值记录
function showAdvanceEditList(advanceId,createBillUserId,createBillUserName,createTimeStr) {
    showUpdateEditInfo(advanceId,createBillUserId,createBillUserName);
    layer.open({
        type: 1,
        title: '预付款充值记录',
        area: ['900px', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#advanceEditDiv'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//自定义附件信息查询
function showUpdateEditInfo(advanceId,createBillUserId,createBillUserName) {
    $("#advanceEditBody").empty();
    $.ajax({
        url : basePath+"platform/seller/billAdvance/queryAdvanceEditList",
        data:  {
            "advanceId":advanceId,
            "createBillUserId":createBillUserId,
            "editStatus":'2'
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                //充值人
                var createUserName = o.createUserName;
                //充值时间
                var createTimeStr = o.createTimeStr;
                //充值金额
                var updateTotal = o.updateTotal;
                //附件路径
                var fileAddress = o.fileAddress;
                //充值备注
                var advanceRemarks = o.advanceRemarks;
                //充值审批
                /*var editStatus = o.editStatus;
                var editStatusStr = "";
                if(editStatus == "1"){
                    editStatusStr = "待审批";
                }else if(editStatus == "2"){
                    editStatusStr = "审核通过";
                }else if(editStatus == "3"){
                    editStatusStr = "审核驳回";
                }*/

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createBillUserName+"</td>").appendTo(trObj);
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTimeStr+"</td>").appendTo(trObj);

                $("<td>"+updateTotal+"</td>").appendTo(trObj);
                $("<td>"+advanceRemarks+"</td>").appendTo(trObj);

                /*$("<td>"+editStatusStr+"</td>").appendTo(trObj);*/
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showEditFileList('"+fileAddress+"');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#advanceEditBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//附件显示隐藏
var hideOrShow = true;
function showEditFileList(receiptAddr) {
    $("#showAdvanceBigImg").empty();
    $("#fileAdvanceList").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $("#fileAdvanceList").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $("#showAdvanceBigImg").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
    if(hideOrShow){
        $("#showAdvenceFiles").show();
        hideOrShow = false;
    }else {
        $("#showAdvenceFiles").hide();
        hideOrShow = true
    }
}
