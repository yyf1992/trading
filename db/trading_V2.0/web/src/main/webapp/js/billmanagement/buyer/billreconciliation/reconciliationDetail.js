/**
 * 导出
 */
function buyRecItemExport(exportType) {
   // var formData = $("#searchForm").serialize();
    var reconciliationId = $("#reconciliationId").val();
    if(exportType == "detail"){
        //var reconciliationId = $("#reconciliationId").val();
        var url = "platform/buyer/billReconciliation/buyRecItemExport?reconciliationId="+ reconciliationId;
        window.open(url);
    }else if(exportType == "rough"){
        //var reconciliationId = $("#reconciliationId").val();
        var url = "platform/buyer/billReconciliation/buyRecItemRoughExport?reconciliationId="+ reconciliationId;
        window.open(url);
    }

}

//发货、换货详情界面
function openConfirmReceipt(id,isFromWms,recordstatus,reconciliationId) {
    alert(reconciliationId);
    leftMenuClick(this,"platform/buyer/billReconciliation/confirmReceiveData?id="+id+"&isFromWms="+isFromWms+"&recordstatus="+recordstatus+"&reconciliationId="+reconciliationId,"buyer");
}

//收款凭据
function showBillReceiptNew(deliverNo,itemTtpe) {
    $.ajax({
        type : "POST",
        url : "platform/buyer/billReconciliation/queryFileUrlList",
        async:false,
        data: {
            "deliverNo":deliverNo,
            "itemTtpe":itemTtpe
        },
        success:function(data) {
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
            if (resultObj.success) {
                //layer.msg(resultObj.msg, {icon: 1});
                var urlList = resultObj.urlList;
                findBillReceipt(urlList);
                layer.open({
                    type: 1,
                    title: '收货回单',
                    area: ['auto', 'auto'],
                    skin:'popBuyer',
                    //btns :2,
                    closeBtn:2,
                    btn:['关闭窗口'],
                    content: $('#linkReceipt'),
                    yes : function(index){
                        layer.close(index);
                    }
                });
            }else {
                layer.msg(resultObj.msg, {icon: 2});
            }
        },
        error:function(){
            layer.msg("获取失败，请稍后重试！",{icon:2});
        }
    });
}

//收据发票(退货)
function findBillReceipt(receiptAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

//修改单价
function updatePrice() {
    var reconciliationId = $("#reconciliationId").val();
    var updateSalePrice = "";
    //var reconciliationIds = "";
    $("#reconciliationData tbody>tr").each(function(i){
        var recItemId = $(this).find("input[name='recItemId']").val();
        //var reconciliationId = $(this).find("input[name='reconciliationId']").val();
        var salePrice = $(this).find("input[name='salePrice']").val();
        var productName = $(this).find("input[name='productName']").val();
        if(salePrice < 0){
            layer.msg("商品："+productName+"的单价不得小于0！",{icon:2});
            return;
        }
        updateSalePrice += recItemId +","+salePrice+";";
        //reconciliationIds +=  reconciliationId+",";
    })
    updateSalePrice = updateSalePrice.substring(0,updateSalePrice.length-1);
    //reconciliationIds = reconciliationIds.substring(0,reconciliationIds.length-1);

    $.ajax({
        url:"platform/buyer/billReconciliation/updateRecItemPrice",
        data:{
            "updateSalePrice":updateSalePrice,
            "reconciliationId":reconciliationId
        },
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if(resultObj.success){
                layer.msg(resultObj.msg,{icon:1});
                leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer')
            }else{
                layer.msg(resultObj.msg,{icon:2});
            }
        },
        error:function(){
            //layer.msg("获取数据失败，请稍后重试！",{icon:2});
            layer.msg("修改失败，请稍后重试！",{icon:2});
        }
    });
}

//修改价格
function updPrice(productName,buyShopProductId,reconciliationId){
    layer.open({
        type: 1,
        title: '修改商品价格',
        area: ['420px', 'auto'],
        skin:'popBuyer',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#updPriceDiv'),
        yes : function(index){
            //$("#updPrice").val(oldSalePrice);
            //var reconciliationId = $("#reconciliationId").val();
           // var reconciliationId = ${buyBillReconciliation.sellerCompanyName}.toString();
            var	updPrice = $("#updPrice").val();
            if(!updPrice){
                layer.msg("商品："+productName+"的单价不得为空！",{icon:2});
                return;
            }/*else if(updPrice < 0){
                layer.msg("商品："+productName+"的单价不得小于0！",{icon:2});
                return;
            }*/
            $.ajax({
                type : "POST",
                url : "platform/buyer/billReconciliation/updateRecItemPrice",
                data: {
                    "reconciliationId":reconciliationId,
                    "recItemId": buyShopProductId,
                    "updateSalePrice": updPrice
                },
                async:false,
                success:function(data){
                    layer.close(index);
                    layer.msg("修改成功",{icon:1},function(){
                        leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetailsNew?reconciliationId='+reconciliationId,'buyer')
                    });
                },
                error:function(){
                    layer.msg("修改失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}

//删除自动生的无用行
function removeUselessLine() {
    $(".JCLRgrips").remove();
}

//查看自定义付款详情
function showCustomPayment(reconciliationId) {
    showBillCustomInfo(reconciliationId);
    layer.open({
        type: 1,
        title: '奖惩详情',
        area: ['900px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showBillCustomList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//自定义附件信息查询
function showBillCustomInfo(reconciliationId) {
    $("#billRecCustomFileBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/buyer/billReconciliation/queryBillCustomList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            /*var createUserId = resultObj.createUserId;
             var createUserName = resultObj.createUserName;
             var deliveryItemList = resultObj.deliveryItemList;*/
            $.each(resultObj,function(i,o){
                //创建人编号
                var createUserId = o.createCustomUserId;
                //上传人名称
                var createCustomUserName = o.createCustomUserName;
                //创建时间
                var createCustomTime = o.createCustomTime;
                //附件路径
                var customPhotoAddress = o.customPhotoAddress;
                //奖惩金额
                var customPaymentMoney = o.customPaymentMoney;
                //奖惩类型
                var customType = o.customType;
                var customTypeStr = "";
                if(customType == "1"){
                    customTypeStr = "奖励";
                }else if(customType == "2"){
                    customTypeStr = "处罚";
                }
                //添加备注
                var createRemarks = o.customRemarks;
                //审批状态
                var customStatus = o.customStatus;
                var customStatusStr = "";
                if(customStatus == "9"){
                    customStatusStr = "卖家待确认";
                }else if(customStatus == "10"){
                    customStatusStr = "卖家已确认";
                }else if(customStatus == "11"){
                    customStatusStr = "卖家已驳回";
                }
                //审批时间
                var updateCustomTime = o.updateCustomTime;
                //审批人编号
                var updateCustomUserId = o.updateCustomUserId;
                //审批人名称
                var updateCustomUserName = o.updateCustomUserName;
                //审批人备注
                var updateRemarks = o.updateRemarks;

                var testTitle = "查看票据";
                var trObj = $("<tr>");
                $("<td>"+createCustomUserName+"</td>").appendTo(trObj);
                $("<td>"+createCustomTime+"</td>").appendTo(trObj);
                $("<td>"+customTypeStr+"</td>").appendTo(trObj);

                $("<td>"+customPaymentMoney+"</td>").appendTo(trObj);
                $("<td>"+createRemarks+"</td>").appendTo(trObj);
                $("<td>"+customStatusStr+"</td>").appendTo(trObj);
                $("<td>"+updateCustomUserName+"</td>").appendTo(trObj);

                $("<td>"+updateCustomTime+"</td>").appendTo(trObj);
                $("<td>"+updateRemarks+"</td>").appendTo(trObj);
                $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+customPhotoAddress+"','custom');\">"+testTitle+"</span></td>").appendTo(trObj);

                $("#billRecCustomFileBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}

//查看预付款修改详情
function showAdvanceEdit(reconciliationId,sellerCompanyName) {
    showAdvanceEditInfo(reconciliationId,sellerCompanyName);
    layer.open({
        type: 1,
        title: '预付款抵扣详情',
        area: ['900px', 'auto'],
        skin:'popBuyer',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#showAdvanceEditList'),
        yes : function(index){
            layer.close(index);
        }
    });
}

//预付款抵扣信息查询
function showAdvanceEditInfo(reconciliationId,sellerCompanyName) {
    $("#billAdvanceEditBody").empty();
    // $("#showFiles").empty();
    $.ajax({
        url : basePath+"platform/buyer/billAdvance/queryAdvanceEditList",
        data:  {
            "reconciliationId":reconciliationId
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                //下单人名称
                var createBillUserName = o.createBillUserName;
                //创建人
                var createUserName = o.createUserName;
                //创建时间
                var createTimeStr = o.createTimeStr;
                //预付款抵扣金额
                var updateTotal = o.updateTotal;

                var trObj = $("<tr>");
                $("<td>"+sellerCompanyName+"</td>").appendTo(trObj);
                $("<td>"+createBillUserName+"</td>").appendTo(trObj);
                $("<td>"+createUserName+"</td>").appendTo(trObj);
                $("<td>"+createTimeStr+"</td>").appendTo(trObj);
                $("<td>"+updateTotal+"</td>").appendTo(trObj);

                $("#billAdvanceEditBody").append(trObj);
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}