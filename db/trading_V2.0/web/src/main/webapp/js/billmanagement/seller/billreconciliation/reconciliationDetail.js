/**
 * 导出
 */
function sellerRecItemExport(exportType) {
    // var formData = $("#searchForm").serialize();
    /*var reconciliationId = $("#reconciliationId").val();
    var url = "platform/seller/billReconciliation/sellerRecItemExport?reconciliationId="+ reconciliationId;
    window.open(url);*/
    var reconciliationId = $("#reconciliationId").val();
    if(exportType == "detail"){
        var url = "platform/seller/billReconciliation/sellerRecItemExport?reconciliationId="+ reconciliationId;
        window.open(url);
    }else if(exportType == "rough"){
        var url = "platform/seller/billReconciliation/sellerRecItemRoughExport?reconciliationId="+ reconciliationId;
        window.open(url);
    }
}

//修改价格
function updPrice(productName,billRecItemId,reconciliationId){
    layer.open({
        type: 1,
        title: '修改商品价格',
        area: ['420px', 'auto'],
        skin:'popBarcode',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#updPriceDiv'),
        yes : function(index){
            var	updPrice = $("#updPrice").val();
            if(!updPrice){
                layer.msg("商品："+productName+"的单价不得为空！",{icon:2});
                return;
            }
            $.ajax({
                type : "POST",
                url : "platform/seller/billReconciliation/updateRecItemPrice",
                data: {
                    "reconciliationId":reconciliationId,
                    "recItemId": billRecItemId,
                    "updateSalePrice": updPrice
                },
                async:false,
                success:function(data){
                    layer.close(index);
                    layer.msg("修改成功",{icon:1},function(){
                        leftMenuClick(this,'platform/seller/billReconciliation/getReconciliationDetailsNew?reconciliationId='+reconciliationId,'sellers')
                    });
                },
                error:function(){
                    layer.msg("修改失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}

//发货详情
function deliveryDetail(deliveryId) {
    leftMenuClick(this,"platform/seller/billReconciliation/deliveryRecordDetail?deliveryId="+deliveryId,"sellers");
}

//收款凭据
function showBillReceiptNew(deliverNo,itemTtpe) {
    $.ajax({
        type : "POST",
        url : "platform/seller/billReconciliation/queryFileUrlList",
        async:false,
        data: {
            "deliverNo":deliverNo,
            "itemTtpe":itemTtpe
        },
        success:function(data) {
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');

            if (resultObj.success) {
                //layer.msg(resultObj.msg, {icon: 1});
                var urlList = resultObj.urlList;
                findBillReceipt(urlList);
                layer.open({
                    type: 1,
                    title: '收货回单',
                    area: ['auto', 'auto'],
                    skin:'popBarcode',
                    //btns :2,
                    closeBtn:2,
                    btn:['关闭窗口'],
                    content: $('#linkReceipt'),
                    yes : function(index){
                        layer.close(index);
                    }
                });
            }else {
                layer.msg(resultObj.msg, {icon: 2});
            }
        },
        error:function(){
            layer.msg("获取失败，请稍后重试！",{icon:2});
        }
    });
}
/*//收款凭据
function showBillReceipt(receiptAddr) {
    findBillReceipt(receiptAddr);
    layer.open({
        type: 1,
        title: '收货回单',
        area: ['auto', 'auto'],
        skin:'popBarcode',
        //btns :2,
        closeBtn:2,
        btn:['关闭窗口'],
        content: $('#linkReceipt'),
        yes : function(index){
            layer.close(index);
        }
    });
}*/

//收据发票(退货)
function findBillReceipt(receiptAddr) {
    $(".big_img").empty();
    $("#icon_list").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        var bigImg = '';
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
            $("#icon_list").append(liObj);
            bigImg = receiptAddrList[0];
        }
        //大图
        $(".big_img").append($("<img id='bigImg' src='"+bigImg+"'></img>"));
    }
}

//收据发票
/*function findBillReceipt(receiptAddr) {
    $("#icon_list").empty();
    if(receiptAddr != null || receiptAddr != ''){
        var receiptAddrList = receiptAddr.split(',');
        for(i = 0;i<receiptAddrList.length;i++){
            var liObj = $("<li>");
            $("<img>"+receiptAddrList[i]+"</img>").appendTo(liObj);
            $("#icon_list").append(liObj);
        }
    }
}*/
