/**
 * 导出
 */
function buyPaymentExport() {
    var formData = $("#searchForm").serialize();
    var url = "platform/buyer/billPaymentDetail/buyPaymentExport?"+ formData;
    window.open(url);
}

//付款审批
function acceptBillPayment(reconciliationId,paymentStatus,acceptPaymentId){
    layer.confirm('确定申请付款？', {
        icon:3,
        title:'提示',
        closeBtn:2,
        skin:'popBuyer',
        btn: ['确定','取消']
    },
    function () {
        var url = "platform/buyer/billPaymentDetail/acceptBillPayment"
        $.ajax({
            type : "POST",
            url : url,
            data: {
                "reconciliationId":reconciliationId,
                "acceptPaymentId":acceptPaymentId,
                "paymentStatus":paymentStatus,
                "menuName":"17071814432171364171"
            },
            async:false,
            /*success:function(data){ 17112713043198538104
             var result = eval('(' + data + ')');
             var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
             if(resultObj.success){
             layer.close(index);
             layer.msg(resultObj.msg,{icon:1});
             loadPlatformData();
             //leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList?paymentStatus=1','buyer')
             }else{
             layer.msg(resultObj.msg,{icon:2});
             }
             }*/
            success:function(data){
                if(data.flag){
                    var res = data.res;
                    if(res.code==40000){
                        //调用成功
                        //saveSucess();
                        layer.msg("添加成功！",{icon:1});
                        //leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                        leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer')
                    }else if(res.code==40010){
                        //调用失败
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40011){
                        //需要设置审批流程
                        layer.msg(res.msg,{time:500,icon:2},function(){
                            /*setApprovalUser(url,res.data,function(data){
                             alert("成功了吗")
                             saveSucess(data);
                             });*/
                            setApprovalUser(url,res.data,function(){
                                //saveSucess();
                                layer.msg("付款待审批！",{icon:1});
                                leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer');
                            });
                        });
                        return false;
                    }else if(res.code==40012){
                        //对应菜单必填
                        layer.msg(res.msg,{icon:2});
                        return false;
                    }else if(res.code==40013){
                        //不需要审批
                        notNeedApproval(res.data,function(data){
                            layer.msg("付款成功！",{icon:1});
                            layer.close(index);
                            leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer');
                        });
                    }
                }else{
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                    return false;
                }
            },
            error:function(){
                layer.msg("付款失败，请稍后重试！",{icon:2});
            }
        });
    });
}

//付款
function showBillPayment(reconciliationId,paymentStatus,acceptPaymentId,paymentNo,actualPaymentAmount,residualPaymentAmount,sellerCompanyName){
    findPaymentInfo(paymentNo,residualPaymentAmount);
    getBankAccount();
    layer.open({
        type:1,
        title:'确认向:'+sellerCompanyName+' 付款',
        area:['auto','auto'],
        skin:'popBuyer',
        closeBtn:2,
        content:$('#billPayment'),
        btn:['确定','取消'],
        yes:function(index){
           // var residualPaymentAmount = $("#eResidualPaymentAmount").val()+"";
           // var residualPaymentAmountF = parseFloat($("#eResidualPaymentAmount").val());
            var residualPaymentAmountF = parseFloat(residualPaymentAmount);
            var actualPaymentAmountNewF = parseFloat($("#eActualPaymentAmount").val());

            //var bankAccount = $("#bankAccount").val();
            var paymentType = $("#paymentType").val();

            /*var paymentFileStr = ""
            var attachments = $("input[name='fileUrl']");
            if(attachments==undefined||attachments.length<=0){
                layer.msg("请上传附件！",{icon:2});
                return;
            }
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    //attachmentsArr.push(attachments[i].value);
                    var fileAddr = attachments[i].value;
                    paymentFileStr += fileAddr + ",";
                }
            }
            paymentFileStr = paymentFileStr.substring(0,paymentFileStr.length-1);*/

            var	paymentRemarks = $("#paymentRemarks").val();
            if(paymentRemarks.length >300){
                layer.msg("备注内容不超过300个字符！",{icon:2});
                return;
            }else if(actualPaymentAmountNewF != residualPaymentAmountF){
                layer.msg("付款金额不等于应付款余额！",{icon:2});
                return;
            }else{
               // var url = "platform/buyer/billPaymentDetail/updateSavePaymentInfo"
                var url = "platform/buyer/billPaymentDetail/acceptBillPayment"
                $.ajax({
                    type : "POST",
                    url : url,
                    data: {
                        "reconciliationId":reconciliationId,
                        "acceptPaymentId":acceptPaymentId,
                        "actualPaymentAmount": actualPaymentAmount,
                        "residualPaymentAmount":residualPaymentAmountF,
                        "actualPaymentAmountNew":actualPaymentAmountNewF,
                        "bankAccount":"",
                        "paymentType":paymentType,
                       // "paymentFileStr":paymentFileStr,
                        "paymentRemarks": paymentRemarks,
                        "menuName":"17071814432171364171"
                    },
                    async:false,
                    success:function(data){
                        if(data.flag){
                            var res = data.res;
                            if(res.code==40000){
                                //调用成功
                                //saveSucess();
                                layer.msg("添加成功！",{icon:1});
                                //leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                                leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer')
                            }else if(res.code==40010){
                                //调用失败
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40011){
                                //需要设置审批流程
                                layer.msg(res.msg,{time:500,icon:2},function(){
                                    /*setApprovalUser(url,res.data,function(data){
                                     alert("成功了吗")
                                     saveSucess(data);
                                     });*/
                                    setApprovalUser(url,res.data,function(){
                                        //saveSucess();
                                        layer.msg("付款待审批！",{icon:1});
                                        leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer');
                                    });
                                });
                                return false;
                            }else if(res.code==40012){
                                //对应菜单必填
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40013){
                                //不需要审批
                                notNeedApproval(res.data,function(data){
                                    layer.msg("付款成功！",{icon:1});
                                    layer.close(index);
                                    leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer');
                                });
                            }
                        }else{
                            layer.msg("获取数据失败，请稍后重试！",{icon:2});
                            return false;
                        }
                        /*var result = eval('(' + data + ')');
                        var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                        if(resultObj.success){
                            layer.close(index);
                            layer.msg(resultObj.msg,{icon:1});
                            //loadPlatformData();
                            leftMenuClick(this,'platform/buyer/billPaymentDetail/billPaymentDetailList','buyer')
                        }else{
                            layer.msg(resultObj.msg,{icon:2});
                        }*/
                    },
                    error:function(){
                        layer.msg("付款失败，请稍后重试！",{icon:2});
                    }
                });
            }
        },
        no : function(index){
            layer.close(index);
        }
    });
}



//付款明细
function findPaymentInfo(paymentNo,residualPaymentAmount) {
    $("#billPaymentBody").empty();
    var liObj1 = $("<li>");

    $("<span style='white-space: nowrap;'>"+"付款单号："+paymentNo+"</span>").appendTo(liObj1);
    $("<input type='hidden' name='paymentNo' id='ePaymentNo' value='"+paymentNo+"'>").appendTo(liObj1);
    $("#billPaymentBody").append(liObj1);

    var liObj2 = $("<li>");
    $("<span style='white-space: nowrap;'>"+"应付余额："+residualPaymentAmount+"</span>").appendTo(liObj2);
    $("<input type='hidden' name='residualPaymentAmount' id='eResidualPaymentAmount' value='"+residualPaymentAmount+"'>").appendTo(liObj2);
    $("#billPaymentBody").append(liObj2);

    var liObj3 = $("<li>");
    $("<span style='width: 80px'>"+"实付金额："+"</span>").appendTo(liObj3);
    $("<input type='number' name='actualPaymentAmount' id='eActualPaymentAmount'>").appendTo(liObj3);
    $("#billPaymentBody").append(liObj3);

   /* var liObj3 = $("<li>");
    $("<span>"+"实付金额："+"</span>").appendTo(liObj3);
    $("<input type='text' name='actualPaymentAmount' id='eActualPaymentAmount'>").appendTo(liObj3);
    $("#billPaymentBody").append(liObj3);*/

   /* var liObj4 = $("<li>");
    $("<span>"+"付款账户："+"</span>").appendTo(liObj4);
    $("<select id='bankAccount' name='bankAccount'></select>").appendTo(liObj4);
    $("#billPaymentBody").append(liObj4);*/

    var liObj5 = $("<li>");
    $("<span style='width: 80px'>"+"付款类型："+"</span>").appendTo(liObj5);
    $("<select id='paymentType' name='paymentType'>"+
        "<option value='0'>"+"现金"+"</option>"+
        "<option value='1'>"+"银行转账"+"</option>"+
        "<option value='2'>"+"支付宝"+"</option>"+
        "<option value='3'>"+"微信"+"</option>"+
        "</select>").appendTo(liObj5);
    $("#billPaymentBody").append(liObj5);

    /*var liObj6 = $("<li>");
    $("<span>"+"付款凭证："+"</span>").appendTo(liObj6);
    $("<div class='upload_license' style='width: 70px;height: 20px'>"+"<input type='file' multiple name='paymentFileAddr' id='paymentFileAddr'onchange='uploadPayment();' style='opacity: 1'>"+"</div>").appendTo(liObj6);
   // $("<div class='voucherImg' id='paymentFileDiv'></div>").appendTo(liObj6);
    $("<p></p>").appendTo(liObj6);
    $("<span style='white-space: nowrap'>"+"仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M"+"</span>").appendTo(liObj6);
    $("<div class='scanning_copy original' id='paymentFileDiv'></div>").appendTo(liObj6);
    $("#billPaymentBody").append(liObj6);*/

    var liObj7 = $("<li>");
    $("<span >"+"备注："+"</span>").appendTo(liObj7);
    $("<textarea type='text' name='paymentRemarks' id='paymentRemarks'>").appendTo(liObj7);
    $("#billPaymentBody").append(liObj7);
}

//获取银行账户
function getBankAccount() {
    $.ajax({
        type : "post",
        url : "platform/sysbank/getBankAccountBuyMap",    //url根据自己的项目实际定义
        async : false,
        success : function(data) {
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $("#bankAccount").empty();
            var expressCompany = $("#bankAccount");
            var str = '';
            for(var o in resultObj) {
                str += '<option value="'+resultObj[o].id+'">'+resultObj[o].bankAccount+'</option>';
                //alert("str123"+str);
            }
            expressCompany.append(str);
        },
        error : function() {
            alert("系统忙，请稍后再试！");
            return false;
        }
    });
}

//上传操作
function uploadPayment() {
    var formData = new FormData();
    var files = document.getElementById("paymentFileAddr").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/billPaymentDetail/uploadPaymentBill";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b onclick='deletePhoto();'></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#paymentFileDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#paymentFileDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#paymentFileAddr").replaceWith('<input type="file" style="opacity:1" multiple name="paymentFileAddr" id="paymentFileAddr" onchange="uploadPayment();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//删除图片
function deletePhoto(){
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
}

//确认付款
function trueBillPayment(paymentId){
    layer.confirm('确定付款吗？', {
            icon:3,
            title:'提示',
            closeBtn:2,
            skin:'popBarcode',
            btn: ['确定','取消']
        },
        function(){
            var url = "platform/buyer/billPaymentDetail/trueBillPayment";
            $.ajax({
                type : "post",
                url : url,
                data : {
                    "paymentId" : paymentId
                },
                async : false,
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if (resultObj.success) {
                        layer.msg(resultObj.msg,{icon:1},function(){
                            leftMenuClick(this,"platform/buyer/billPaymentDetail/billPaymentDetailList","buyer");
                        });
                    } else {
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("付款确认失败，请稍后重试！",{icon:2});
                }
            });
        });
}
