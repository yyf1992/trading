layui.use('table', function() {
	var table = layui.table
	,form = layui.form;
	table.on('tool(tradeRoleTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'edit'){
	    	//设置权限
	    	edit(data.id);
	    }else if(obj.event === 'switchStatus'){
	    	//禁用启用
	    	var type = data.status==0?1:0;
	    	switchStatus(data.id,type);
	    }else if(obj.event === 'deleteTradeRole'){
	    	//删除
	    	deleteTradeRole(data.id);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('tradeRoleTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	roleCode     : $("#roleCode").val(),
	        	roleName      : $("#roleName").val()
	        }
	      });
	    },
	    addRole: function(){
	    	$.ajax({
	    		url:basePath+"platform/tradeRole/loadAddHtml",
	    		success:function(data){
	    			layer.open({
	    				type:1,
	    				title:"新增角色",
	    				skin: 'layui-layer-rim',
	    	  		    area: ['450px', 'auto'],
	    	  		    btn:['保存','取消'],
	    	  		    content:data,
	    	  		    yes:function(index,layero){
	    	  		    	var formObj = $(layero).find("form");
	    	  		    	//角色代码判空
	    	  		    	var roleCode = formObj.find("input[name='roleCode']").val();
	    	  		    	if(roleCode == ''){
	    	  		    		layer.msg("角色代码不能为空！",{icon:2});
	    	  		    		return false;
	    	  		    	}
	    	  		    	//角色名称判空
	    	  		    	var roleName = formObj.find("input[name='roleName']").val();
	    	  		    	if(roleName == ''){
	    	  		    		layer.msg("角色名称不能为空！",{icon:2});
	    	  		    		return false;
	    	  		    	}
	    	  		    	var formData = formObj.serializeArray();
	    	  		    	var url = formObj.attr("action");
	    	  		    	layer.load();
	    	  		    	$.ajax({
	    	  		    		url:url,
	    	  		    		data:formData,
	    	  		    		success:function(data){
	    	  		    			var result = eval('(' + data + ')');
	    	  		    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	    	  		    			if(resultObj.flag){
	    	  		    				//保存成功
	    	  		    				layer.closeAll();
	    	  		    				active["reload"].call(this);
	    	  		    			}else{
	    	  		    				layer.closeAll('loading');
	    		  		    			layer.msg(resultObj.msg,{icon:2});
	    	  		    			}
	    	  		    		},
	    	  		    		error:function(){
	    	  		    			layer.closeAll('loading');
	    	  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
	    	  		    		}
	    	  		    	});
	    	  		    }
	    			});
	    		},
	    		error:function(){
	    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
	    		}
	    	});
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
	//管理员切换
	form.on('switch(isAdmin)', function(obj){
	    layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);
    });
});
//设置权限
function edit(id){
	var url = 'platform/tradeRole/instalMenu?id='+id;
	leftMenuClick(this,url,'system','17100920503219724288');
}
//删除
function deleteTradeRole(id){
	layer.msg('确定删除吗？', {
	  time: 0 //不自动关闭
	  ,btn: ['确定', '取消']
	  ,yes: function(index){
	    layer.close(index);
	    layer.load();
    	$.ajax({
    		url:"platform/tradeRole/saveDelete",
    		data:{"id":id},
    		success:function(data){
    			var result = eval('(' + data + ')');
    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    			if(resultObj.flag){
    				//保存成功
    				layer.closeAll();
    				$('#select').click();
    			}else{
    				layer.closeAll('loading');
	    			layer.msg(resultObj.msg,{icon:2});
    			}
    		},
    		error:function(){
    			layer.closeAll('loading');
    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
    		}
    	});
	  }
	});
}
//切换状态
function switchStatus(id,type){
	var typeName = type=='0'?'启用':'禁用';
	layer.msg('确定'+typeName+'吗？', {
	  time: 0 //不自动关闭
	  ,btn: ['确定', '取消']
	  ,yes: function(index){
	    layer.close(index);
	    layer.load();
    	$.ajax({
    		url:"platform/tradeRole/switchStatus",
    		data:{
				"id":id,
				"status":type
			},
    		success:function(data){
    			var result = eval('(' + data + ')');
    			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    			if(resultObj.flag){
    				//保存成功
    				layer.closeAll();
    				$('#select').click();
    			}else{
    				layer.closeAll('loading');
	    			layer.msg(resultObj.msg,{icon:2});
    			}
    		},
    		error:function(){
    			layer.closeAll('loading');
    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
    		}
    	});
	  }
	});
}
