$(function () {
//删除附件
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
});
//发货详情
function deliveryDetail(deliveryId) {
    leftMenuClick(this,"platform/seller/delivery/deliveryRecordDetail?deliveryId="+deliveryId,"sellers");
}
//上传收货单
function openUploadReceipt(deliverNo) {
    $("#uploadDeliverNo").html(deliverNo);
    layer.open({
        type: 1,
        title: '上传收货单',
        area: ['728px', 'auto'],
        skin: 'pop',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#uploadReceiptDiv'),
        yes : function(index){
            var attachmentsArr = new Array();
            var attachments = $("input[name='fileUrl']");
            //验证日期和附件必输
            if(attachments==undefined||attachments.length<=0){
                layer.msg("请上传附件！",{icon:2});
                return;
            }
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    attachmentsArr.push(attachments[i].value);
                }
            }
            $.ajax({
                type:"POST",
                url : "platform/buyer/receive/saveReceiptAttachment",
                async:false,
                data: {
                    "deliverNo": deliverNo,
                    "attachments": JSON.stringify(attachmentsArr)
                },
                success:function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("保存成功！",{icon:1});
                        layer.close(index);
                        leftMenuClick(this,"platform/seller/delivery/deliveryRecordList","sellers");
                    }
                },
                error:function(){
                    layer.msg("操作失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}
//上传操作
function uploadReceipt() {
    var formData = new FormData();
    var files = document.getElementById("uploadReceipt").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/receive/uploadReceiptAttachment";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#addAttachmentDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#addAttachmentDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#uploadReceipt").replaceWith('<input type="file" multiple name="uploadReceipt" id="uploadReceipt" onchange="uploadReceipt();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//查看收货单凭证
function receiptAttachmentPreview(receiptVoucherList) {
    loadAttachment(receiptVoucherList);
    layer.open({
        type: 1,
        title: '预览收货单',
        area: ['720px', '500px'],
        skin: 'pop',
        closeBtn:2,
        content: $('#viewReceiptDiv')
    });
}

//加载附件
function loadAttachment(deliverNo) {
    $("#viewReceiptDiv").empty();
    $.ajax({
        url : basePath+"platform/buyer/receive/loadAttachment",
        data: {
            "relatedId": deliverNo
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                var url = o.url;
                var a = "<a title='点击查看大图'>"+
                    "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#viewReceiptDiv\");'>"+
                    "</a>";
                $("#viewReceiptDiv").append(a);
            });
        }
    });
}
//大图
function openPhoto(div) {
    layer.photos({
        photos: div
        ,anim: 5
    });
}
//打印收货回单
function printReceipt(deliveryId) {
    $.ajax({
        type:"get",
        async:false,
        url:"platform/seller/delivery/printDelivery",
        data:{
            deliveryId:deliveryId
        },
        success:function(data){
            var str = data.toString();
            $("#receiptDiv").html(str);
            layer.open({
                type: 1,
                title: '',
                area: ['1400px', '700px'],
                skin:'',
                //btns :2,
                closeBtn:2,
                // btn:['关闭窗口'],
                content: $('#receiptDiv'),
                yes : function(index){
                    layer.close(index);
                }/*,
                 no : function(index){
                 layer.close(index);
                 }*/
            });
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });

}
//确认发货
function confirmDelivery(deliveryId) {
    layer.confirm('确认发货?', {
        icon:3,
        title:'发货',
        skin:'pop',
        closeBtn:2,
        btn: ['确定','取消']
    }, function(index){
        layer.close(index);
        var lodIndex;
        $.ajax({
            type : "POST",
            url : "platform/seller/delivery/confirmDelivery",
            data : {
                "deliveryId" : deliveryId
            },
            beforeSend: function () {
                lodIndex = layer.load(1);
            },
            complete: function () {
                layer.close(lodIndex);
            },
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("发货成功", {icon: 1},function(){
                        leftMenuClick(this,"platform/seller/delivery/deliveryRecordList","sellers");
                    });
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error : function() {
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    });
}
//推送WMS
function sendToWms(deliverNo) {
    var lodIndex;
    $.ajax({
        type : "POST",
        url : "platform/seller/delivery/sendWmsInputPlan",
        data : {
            "deliverNo": deliverNo
        },
        beforeSend: function () {
            lodIndex = layer.load(1);
        },
        complete: function () {
            layer.close(lodIndex);
        },
        success : function(data) {
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if(resultObj.success){
                layer.msg("推送成功", {icon: 1},function(){
                    leftMenuClick(this,"platform/seller/delivery/deliveryRecordList","sellers");
                });
            }else{
                layer.msg(resultObj.msg,{icon:2});
            }
        },
        error : function() {
            layer.msg("推送失败，请稍后重试！",{icon:2});
        }
    });
}