$(function () {
    //删除附件
    $('.original').on('click','b',function(){
        $(this).parent().remove();
    });
});
//确认收货界面
function openConfirmReceipt(id,isFromWms,recordstatus) {
    leftMenuClick(this,"platform/buyer/receive/confirmReceiveData?id="+id+"&isFromWms="+isFromWms+"&recordstatus="+recordstatus,"buyer");
}

//上传收货单
function openUploadReceipt(deliverNo) {
    $("#uploadDeliverNo").html(deliverNo);
    $("#addAttachmentDiv").empty();
    $.ajax({
        url : basePath+"platform/buyer/receive/loadAttachment",
        data: {
            "relatedId": deliverNo
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                var url = o.url;
                var a = "<span>"+
                    "<b></b>"+
                    "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#addAttachmentDiv\");'>"+
                    "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                    "</span>";
                $("#addAttachmentDiv").append(a);
            });
        }
    });
    layer.open({
        type: 1,
        title: '上传收货单',
        area: ['500px', 'auto'],
        skin: 'pop',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#uploadReceiptDiv'),
        yes : function(index){
            var attachmentsArr = new Array();
            var attachments = $("input[name='fileUrl']");
            if(attachments==undefined||attachments.length<=0){
                layer.msg("请上传附件！",{icon:2});
                return;
            }
            if(attachments!=undefined&&attachments.length>0){
                for(var i=0;i<attachments.length;i++){
                    attachmentsArr.push(attachments[i].value);
                }
            }
            $.ajax({
                type:"POST",
                url : "platform/buyer/receive/saveReceiptAttachment",
                async:false,
                data: {
                    "deliverNo": deliverNo,
                    "attachments": JSON.stringify(attachmentsArr)
                },
                success:function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("保存成功！",{icon:1});
                        layer.close(index);
                        leftMenuClick(this,"platform/buyer/receive/receiveList","buyer");
                    }
                },
                error:function(){
                    layer.msg("操作失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}
//上传操作
function uploadReceipt() {
    var formData = new FormData();
    var files = document.getElementById("uploadReceipt").files;
    //追加文件数据
    for(var i=0;i<files.length;i++){
        formData.append("file[]", files[i]);
    }
    var url = "platform/buyer/receive/uploadReceiptAttachment";
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                for(var i=0;i<resultObj.urlList.length;i++){
                    var url = resultObj.urlList[i];
                    var a = "<span>"+
                        "<b></b>"+
                        "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#addAttachmentDiv\");'>"+
                        "<input type='hidden' name='fileUrl' value='"+url+"'>"+
                        "</span>";
                    $("#addAttachmentDiv").append(a);
                }
                //重新设置input按钮，防止无法重复提交文件
                $("#uploadReceipt").replaceWith('<input type="file" multiple name="uploadReceipt" id="uploadReceipt" onchange="uploadReceipt();" title="'+new Date()+'"/>');
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//打开二维码
function showQrcode(url) {
    $("#qrcodeImg").attr("src",url);
    layer.open({
        type: 1,
        title: false,
        closeBtn: 1,
        area: '320px',
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: $("#qrcodeDiv")
    });
}

//查看收货单凭证
function receiptAttachmentPreview(deliverNo) {
    loadAttachment(deliverNo);
    layer.open({
        type: 1,
        title: '预览收货单',
        area: ['720px', '500px'],
        skin: 'popBuyer',
        closeBtn:2,
        content: $('#viewReceiptDiv')
    });
}

//加载附件
function loadAttachment(deliverNo) {
    $("#viewReceiptDiv").empty();
    $.ajax({
        url : basePath+"platform/buyer/receive/loadAttachment",
        data: {
            "relatedId": deliverNo
        },
        async:false,
        success:function(data){
            $("#icon_list").empty();
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(resultObj,function(i,o){
                var url = o.url;
                var a = "<a title='点击查看大图'>"+
                    "<img layer-src='"+url+"' src='"+url+"' onclick='openPhoto(\"#viewReceiptDiv\");'>"+
                    "</a>";
                $("#viewReceiptDiv").append(a);
            });
        }
    });
}
//大图
function openPhoto(div) {
    layer.photos({
        photos: div
        ,anim: 5
    });
}
//签收回单
function signInReceipt(id) {
    checkReceipt(id,"1");
}
//拒绝回单
function refusesignIn(id) {
    checkReceipt(id,"2");
}
//确认，拒绝回单操作
function checkReceipt(id,result) {
    var title = (result=='1')?'确认签收?':'拒绝签收?';
    layer.confirm(title, {
        icon:3,
        title:'',
        skin:'pop',
        closeBtn:2,
        btn: ['确定','取消']
    }, function(index){
        layer.close(index);
        $.ajax({
            type : "POST",
            url : "platform/buyer/receive/checkReceipt",
            async: false,
            data : {
                "id" : id,
                "result":result
            },
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("操作成功", {icon: 1},function(){
                        queryReceiveData();
                    });
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error : function() {
                layer.msg("操作失败，请稍后重试！",{icon:2});
            }
        });
    });
}