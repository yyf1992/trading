$(function(){
	//iframe自适应
	$(window).on('resize', function() {
		var $content = $('.content');
		$content.height($(this).height() - 120);
		$content.find('iframe').each(function() {
			$(this).height($content.height());
		});
	}).resize();
	$('.submenu > a').click(function(e){
		e.preventDefault();
		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');
		var submenus = $('.sidebar-menu li.submenu ul');
		var submenus_parents = $('.sidebar-menu li.submenu');
		if (li.hasClass('open')) {
			if (($(window).width() > 768) || ($(window).width() < 479)) {
				submenu.slideUp();
			} else {
				submenu.fadeOut(250);
			}
			li.removeClass('open');
			li.find(".fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-left");
		} else {
			if (($(window).width() > 768) || ($(window).width() < 479)) {
				submenus.slideUp();
				submenu.slideDown();
			} else {
				submenus.fadeOut(250);
				submenu.fadeIn(250);
			}
			submenus_parents.removeClass('open');
			li.addClass('open');
			li.find(".fa-angle-left").removeClass("fa-angle-left").addClass("fa-angle-down");
		}
	});
});
function openMenu(obj,url,menuName){
	$(obj).parent().siblings('li').removeClass("active");
	$(obj).parent().addClass("active");
	$(".content-header").find("li:last").html(menuName);
	$(".content").find("iframe").attr("src",url);
}