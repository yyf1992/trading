$(function () {

});
//上传合同原件
layui.use(['upload','form','element'], function(){
    var layer = layui.layer;
    //合同管理页面的审批
    $('.contract_list td:last-child div.approval_contract').click(function(){
        layer.open({
            type: 1,
            title: '合同审批管理',
            area: ['420px', 'auto'],
            skin: 'contractSubmit',
            closeBtn:2,
            btn:['确定','关闭'],
            content: $('.contract_approval')
        })
    });
    //上传图片
    // layui.upload({
    //     url: '' //上传接口
    //     ,success: function(res){ //上传成功后的回调
    //         console.log(res);
    //     }
    // });
    //提交草稿箱
    $('.contract_list div.submit').click(function(){
        layer.confirm('提交此合同后，将不能再修改合同内容，确定要提交吗？',{
            skin:'contractSubmit',
            title:'提醒'
        },function(index){
            layer.close(index);
            layer.msg('恭喜您修改成功！', {icon:1});
        })
    })
});

//合同内容上传
function uploadContent(obj) {
    var formData = new FormData();
    var id = $(obj).attr("id");
    var file = document.getElementById(id).files[0];
    formData.append("file",file);
    var url = "platform/buyer/contract/uploadContent";
    console.log(file);
    var xhr = new XMLHttpRequest();
    xhr.open("post", url, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var b = xhr.responseText;
            var re = eval('(' + b + ')');
            var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
                //获得文件路径
                var url = resultObj.filePath;
                $("#contentAddress").val(url);
                //加载
                // $("#contentArea").text(resultObj.htmlcontent);
                // document.getElementById("contentArea").value=resultObj.htmlcontent;
                var content = document.getElementById("contentArea");
                content.innerHTML =resultObj.htmlContent;
                content.style.display="";
            }else {
                layer.msg(resultObj.msg,{icon:2});
                return;
            }
        }
    }

}

//新增客户
function addCustomer(role) {
    if(role=="seller"){
        leftMenuClick(this,'platform/sellers/client/addClient','sellers');
    }else {
        leftMenuClick(this,'platform/buyer/supplier/addSupplier','buyer');
    }

}
//确认提交
function submitContract() {
    var url = "platform/buyer/contract/saveContractApprove";
    //合同实体类
    var buyContract = new Object();
    buyContract.id = $("#contractId").val();//ID
    buyContract.customerId = $("#customerId").val();//客户ID
    buyContract.customerName = $("#customerId").find("option:selected").text();//客户名称
    buyContract.customerLinkman = $("#customerLinkman").val();//联系人
    buyContract.customerPhone = $("#customerPhone").val();//手机号
    buyContract.contractName = $("#contractName").val();//合同名称
    buyContract.contractNo = $("#contractNo").val();//合同编号
    buyContract.contentAddress = $("#contentAddress").val();//合同内容地址
    buyContract.status = $("#status").val();//状态
    buyContract.remark = $("#remark").val();//留言
    $.ajax({
        url : url,
        async : false,
        data : {
            "buyContract" : JSON.stringify(buyContract),
            "menuName":"17071814440268433504"
        },
        success : function(data) {
            if(data.flag){
                var res = data.res;
                if(res.code==40000){
                    //调用成功
                    saveSucess(res.data);
                }else if(res.code==40010){
                    //调用失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40011){
                    //需要设置审批流程
                    layer.msg(res.msg,{time:500,icon:2},function(){
                        setApprovalUser(url,res.data,function(data){
                            saveSucess(data);
                        });
                    });
                    return false;
                }else if(res.code==40012){
                    //对应菜单必填
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40099){
                    //合同保存失败
                    layer.msg(res.msg,{icon:2});
                    return false;
                }else if(res.code==40013){
					//不需要审批
					notNeedApproval(res.data,function(data){
						saveSucess(data);
					});
					return false;
				}
            }else{
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
                return false;
            }
        },
        error : function() {
            layer.msg("获取数据失败，请稍后重试！", {icon : 2});
        }
    });
}
//保存成功
function saveSucess(ids){
    layer.msg('提交成功！',{icon : 1},function() {
        leftMenuClick(this,'platform/buyer/contract/contractList','buyer','17112414010932041195');
    });
}
//保存草稿
function saveDraft() {
    var url = "platform/buyer/contract/saveContract";
    $.ajax({
        type : "POST",
        url : url,
        async : false,
        data : {
            "id" : $("#contractId").val(),//ID
            "customerId" : $("#customerId").val(),//客户ID
            "customerName" : $("#customerId").find("option:selected").text(),//客户名称
            "customerLinkman" : $("#customerLinkman").val(),//联系人
            "customerPhone" : $("#customerPhone").val(),//手机号
            "contractName" : $("#contractName").val(),//合同名称
            "contractNo" : $("#contractNo").val(),//合同编号
            "contentAddress" : $("#contentAddress").val(),//合同内容地址
            "status" : $("#status").val(),//状态
            "remark" : $("#remark").val()//留言
        },
        success : function(data) {
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if (resultObj.success) {
                layer.msg(resultObj.msg,{icon : 1},function() {
                    leftMenuClick(this,'platform/buyer/contract/contractList','buyer','17112414010932041195');
                });
            }else {
                layer.msg(resultObj.msg,{icon : 2});
            }
        },
        error : function() {
            layer.msg("获取数据失败，请稍后重试！", {icon : 2});
        }
    });
}

