$(function(){
	$('.menu a').on('click',function(){
		if (!$('.menu').hasClass('menu-mini')) {
			var li = $(this).parent();
			if ($(li).hasClass('menu-show') == false) {
				//展开
				$(li).siblings('li').find('ul').slideUp(300);
				$(li).children('ul').slideDown(300);
				$(li).addClass("menu-show").siblings('li').removeClass('menu-show');
			}else{
				$(li).find('ul').slideUp(300);
				//收缩
				$(li).removeClass("menu-show");
				//收缩子
				$(li).find(".menu-show").removeClass("menu-show");
			}
		}
	});
    //menu-mini切换
    $('#mini').on('click',function(){
        if (!$('.menu').hasClass('menu-mini')) {
            $('.menu-item.menu-show').removeClass('menu-show');
            $('.menu-item').children('ul').removeAttr('style');
            $('.menu').addClass('menu-mini');
            $('.rt_detail').css('margin-left','50px');
        }else{
            $('.menu').removeClass('menu-mini');
            $('.rt_detail').css('margin-left','210px');
        }
    });
    
});