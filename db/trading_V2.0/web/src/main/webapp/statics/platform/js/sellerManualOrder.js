layui.use(['layer','form','laydate'], function(){
  var layer = layui.layer;
//删除表格某一行
$('.purchase_list').on('click','b',function(){
  layer.confirm('确定要删除该商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});

  //邀请供应商互通
  $('.invite').click(function(){
    layer.open({
      type:1,
      title:'邀请互通好友',
      area:['330px', 'auto'],
      skin:'pop',
      closeBtn:2,
      content:$('.supplier_invite'),
      btn:['确定发送短信','取消'],
      yes:function(index){
        layer.close(index);
        layer.msg('发送成功',{time:800})
      }
    });
  });
});
//功能点12:新增商品弹出框表格增加、删除一行
$('.new_goods>button').click(function(){
  var str='<td><i></i></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td><td>'+
	  $("#addUnitSelect").html()
	  +'</td><td><input type="text"></td><td><input type="text"></td><td><input type="text"></td>';
  var tbl=$('.new_goods table tr:eq(-1)');
  addRow(str,tbl);
});
$('.new_goods table').on('click','i',function(){
  layer.confirm('确定要删除该商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});
//功能点14：数量求和
function sum(){
  var sum=0;
  var total=$('.supplier .total');
  $('.supplier input[type="number"]').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
$('.supplier').on('click','input[type="number"]',function(){
  sum();
});
//失去焦点时求和
$('.supplier').on('blur','input[type="number"]',function(){
  sum();
});
//删除一行时求和
$('.supplier').on('click','b',function(){
  sum();
});
//功能点6:手工录入采购宝贝页面，点击不含运费，弹出输入框
$('.voucher>li input.no_postage').click(function(){
  $('.franking').css('display','inline-block');
});
$('.voucher>li input.postage').click(function(){
  $('.franking').css('display','none');
});
//表格增加和删除一行函数
function addRow(str,tbl){
var addTr=document.createElement('tr');
  addTr.innerHTML=str;
  tbl.after(addTr);
}
function delRow(tr){
  $(tr).parent().parent().remove();
}
//手工录入采购商品页面
//功能点15:点击增加项，表格增加一行
$('.manual+button').click(function(){
	var id = new Date().getTime();
	var str='<td><b></b>'+
				'<input type="hidden" name="proName">'+
				'<input type="hidden" name="proCode">'+
				'<input type="hidden" name="skuCode">'+
				'<input type="hidden" name="skuOid">'+
				'<input type="hidden" name="price">'+
				'<input type="hidden" name="unitName"">'+
			'</td>'+
		    '<td style="text-align: left;">'+
		      '<input type="text" name="proInfo" id="proInfo" placeholder="输入商品名称/货号/条形码/规格等关键字" onkeyup="selectGoodsSeller(this);">'+
		      '<span class="increase_goods"><i></i>新增商品</span>'+
		     '</td>'+
		      '<td>'+
		      $("#addUnitSelect").html()
		      +'</td>'+
		    '<td><input type="number" value="0" min="0" id="unit_price" class="unit_price"></td>'+
		    '<td><input type="number" value="0" min="0" class="number"></td>'+
		    '<td><input type="number" value="0" min="0" class="discount"></td>'+
		    '<td class="subtotal lineH30">0.00</td>'+
		    '<td><textarea name="" placeholder="备注内容"></textarea></td>';
  var tbl=$('.manual tr:eq(-2)');
  addRow(str,tbl);
});
//数量求和
function calSum(){
  var sum=0;
  var total=$('.manual .num');
  $('.manual input.number').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
//单击时求和
$('.manual').on('click','input.number',function(){
  calSum();
});
//失去焦点时求和
$('.manual').on('blur','input.number',function(){
  calSum();
});
//删除一行时求和
$('.manual').on('click','b',function(){
  calSum();
});
//优惠金额求和
function discount(){
  var sum=0;
  var total=$('.manual .discount_sum');
  $('.manual input.discount').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum.toFixed(2));
}
//单击时求和
$('.manual').on('click','input.discount',function(){
  discount();
});
//失去焦点时求和
$('.manual').on('blur','input.discount',function(){
  discount();
});
//删除一行时求和
$('.manual').on('click','b',function(){
  discount();
});
//采购金额小计
//求总金额
function calTotal(){
  var sum=0;
  $('.manual .subtotal').each(function(i,val){
    var p=$(this).html();
    sum+=parseFloat(p);
  });
  $('.manual .total').html(sum.toFixed(2));
  $('#receivables').html(sum.toFixed(2));

}
//单击单价时求和
$('.manual').on('click','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('click','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//失去焦点时求和
$('.manual').on('blur','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('blur','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//点击优惠金额时求和
$('.manual').on('click','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//失去焦点时
$('.manual').on('blur','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//不需要物流，自提货
$(function(){
	//切换收货地址
	$('.addr>div').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$("input[name='is_since']").change(function(){
		if($(this).is(":checked")){
			$(".addr").hide();
			$(".addr_add").hide();
		}else{
			$(".addr").show();
			$(".addr_add").show();
		}
	});
});
//增加一行
$('#planAdd').click(function(){
	var unitDiv_num = $("#unitDiv_num").val();
	var str='<td><b></b></td>'+
			'<td><input type="text" placeholder="请选择商品" readonly="readonly">'+
				'<ul>'+$("#productSelectDiv").html()+
				'</ul>'+
				'<input type="hidden" name="productCode" value=""/><!-- 货号 -->'+
				'<input type="hidden" name="productName" value=""/><!-- 名称 -->'+
				'<input type="hidden" name="skuCode" value=""/><!-- 规格代码 -->'+
				'<input type="hidden" name="skuName" value=""/><!-- 规格名称 -->'+
				'<input type="hidden" name="barcode" value=""/><!-- 条形码 -->'+
			'</td>'+
			'<td class="lineH30">'+
				'<div id="unitDiv_'+(Number(unitDiv_num) + 1)+'"></div>'+
			'</td><td>'+
			'<input type="number" value="0" min="0" class="text-center" onchange="sumNumber();"></td>'+ 
			//'<td><input type="text" name="date" lay-verify="date" placeholder="年/月/日" class="layui-input" ></td>'+
			'<td><textarea placeholder="备注内容"></textarea></td>';
  var tbl=$('.purchasePlan tr:eq(-2)');
  addRow(str,tbl);
  $("#unitDiv_num").val(Number(unitDiv_num) + 1);
});

//删除附件
//$('#planEnclosure').on('click','span b',function(){
//  layer.confirm('您确定要删除此附件吗？</p>',{
//    icon:3,
//    skin:'pop',
//    title:'提醒',
//    closeBtn:2
//  },function(index){
//    layer.close(index);
//    $(this).parent().remove();
//  }.bind(this));
//});

$('.planList').on('click','tbody td:first-child b',function(){
  layer.confirm('您确定要删除此商品吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    delRow(this);
  }.bind(this));
});

//新增地址加载省市区
function addcity(){
	// 清空城市和区
	$("#addr2 option[value!='']").remove();
	$("#addr3 option[value!='']").remove();
	// 根据省份加载市
	var provinceId = $("#addr1").children('option:selected').val();
	$.ajax({
		type : "post",
		url : 'platform/reg/loadCityByProvinceId',
		data : {
			"provinceId" : provinceId
		},
		
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			$.each(result.citiesList, function(index, element) {
				var id = element.cityId;
                var name = element.city;
                var opt = "<option value='" + id + "'>" + name + "</option>";  
				$("#addr2").append(opt);
		    });
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

function addarea(){
// 清空区
	$("#addr3 option[value!='']").remove();
	// 根据市加载区
	var cityId = $("#addr2").children('option:selected').val();
	$.ajax({
		type : "post",
		url : 'platform/reg/loadAreaByCityId.html',
		data : {
			"cityId" : cityId
		},
		async : false,
		dataType: "json",
		success : function(data) {
			var result = eval('(' + data + ')');
			$.each(result.areasList, function(index, element) {
				var id = element.areaId;
                var name = element.area;
                var opt = "<option value='" + id + "'>" + name + "</option>";  
				$("#addr3").append(opt);
		    });
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
////加载采购商
//$(function(){
//	
//	loadSupplier("${orderData.sellerId}");
//	//重新渲染select控件
//	var form = layui.form;
//	form.render("select");
//	//给select添加监听事件
//	form.on("select(changeSeller)", function(data){
//		var selectValue = data.value;
//		if(selectValue==''){
//			$("input[name='contact_person']").val("");
//			$("input[name='clientPhone']").val("");
//		}else{
//			$("input[name='contact_person']").val(selectValue.split(",")[2]);
//			$("input[name='clientPhone']").val(selectValue.split(",")[3]);
//		}
//	});
//});
////加载采购商下拉
//function loadSupplier(sellerId){
//	
//	var obj={};
//	obj.divId="addOrderSellerDiv";
//	obj.selectName="clientName";
//	obj.selectId="sellerId";
//	obj.filter="changeSeller";
//	obj.selectValue=sellerId;
//	sellerSelect(obj);
//}
////采购商下拉
//function sellerSelect(obj){
//	
//	var url = basePath+"platform/sellers/client/getSellerSelect"
//	$.ajax({
//		url : url,
//		async:false,
//		success:function(data){
//			var result = eval('(' + data + ')');
//			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
//			var selectObj = $("<select lay-filter='"+obj.filter+"'></select>");
//			if(obj.selectName != null && obj.selectName != ''){
//				selectObj.attr("name",obj.selectName);
//			}
//			if(obj.selectId != null && obj.selectId != ''){
//				selectObj.attr("id",obj.selectId);
//			}
//			$('<option>',{val:"",text:"请选择"}).appendTo(selectObj);
//			var selectValue = obj.selectValue;
//			$.each(resultObj,function(i){
//				var id = resultObj[i].seller_client_id;
//				var clientName = resultObj[i].client_Name;
//				var clientPerson = resultObj[i].client_Phone;
//				var clientPhone = resultObj[i].client_Person;
//				if(selectValue != null && selectValue != ''){
//					if(selectValue==id){
//						$('<option>',{val:id+","+clientName+","+clientPhone+","+clientPerson,text:clientName,selected:"selected"}).appendTo(selectObj);
//						return;
//					}
//				}
//				$('<option>',{val:id+","+clientName+","+clientPhone+","+clientPerson,text:clientName}).appendTo(selectObj);
//			});
//			$("#"+obj.divId).append(selectObj);
//		},
//		error:function(){
//			layer.msg("获取数据失败，请稍后重试！",{icon:2});
//		}
//	});
//}
//下一步
function nextAdd(){
	
	var suppName=$("#suppName").val();
	if(suppName == ""){
		layer.msg("请选择采购商！",{icon:2});
		return;
	}
	var sellerId = $("#sellerId").val();
//	var sellerId=buyersArr.split(",")[0];
//	var buyers=buyersArr.split(",")[1];
//	var buyers = "山东";
//	var contact_person=$("#loadSellerManualOrder input[name='contact_person']").val();
//	var clientPhone=$("#loadSellerManualOrder input[name='clientPhone']").val();
	var contact_person = $("#contact_person").val();
	if(contact_person == ""){
		layer.msg("请填写联系人！",{icon:2});
		return;
	}
	var clientPhone = $("#clientPhone").val();
	if(clientPhone == ""){
		layer.msg("请填写手机号！",{icon:2});
		return;
	}
	var productInfor = "";
	var proNameError = "";
	var unitError = "";
	var goodsNumberError = "";
	$("#product_information tr").each(function(i) {
		var proCode = $(this).find("input[name='proCode']").val();
		var proName = $(this).find("input[name='proName']").val();
		if(proName==''){
			proNameError = "第"+(i+1)+"行没有选择商品！";
			return false; 
		}
		var skuCode = $(this).find("input[name='skuCode']").val();
		var colorCode = $(this).find("input[name='colorCode']").val();
		var skuOid = $(this).find("input[name='skuOid']").val();
	//	var proName = $(this).find("td:eq(1)").find("input").val();
		var unitId = $(this).find("td:eq(2)").find("option:selected").val();
		if(unitId==''){
			unitError="第"+(i+1)+"行没有设置单位！";
			return false;
		}
		var unitName = $(this).find("td:eq(2) select option:selected").text();
		if(unitName==''){
			unitError="第"+(i+1)+"行没有设置单位！";
			return false;
		}
		if(unitName=='请选择'){
			unitError="第"+(i+1)+"行没有设置单位！";
			return false;
		}
		var price = $(this).find("td:eq(3)").find("input").val();
		var goodsNumber  = $(this).find("td:eq(4)").find("input").val();
		if(goodsNumber==0){
			goodsNumberError="第"+(i+1)+"行没有选择商品数量！";
			return false;
		}
		var discount = $(this).find("td:eq(5)").find("input").val();
		var priceSum = $(this).find("td:eq(6)").html();
		var remark = $(this).find("td:eq(7)").find("textarea").val();
		
		productInfor = productInfor+proName + "/" + unitName + "/" + price + "/" + 
		goodsNumber + "/" + discount + "/" + priceSum + "/" + colorCode + "/" + proCode+"/" + skuCode+"/" + skuOid+"/" + unitId +"/" + remark+"&";
	});
	productInfor = productInfor.substring(0, productInfor.length - 1);
	if(proNameError!=''){
		layer.msg(proNameError,{icon:2});
		return;
	}
	if(unitError!=''){
		layer.msg(unitError,{icon:2});
		return;
	}
	if(goodsNumberError!=''){
		layer.msg(goodsNumberError,{icon:2});
		return;
	}
	var num = $(".num").html();
	if(num == 0){
		layer.msg("请选择商品数量！",{icon:2});
		return;
	}
	var discount_sum = $(".discount_sum").html();
	var total = $(".total").html();
	var otherFee = $("#otherFee").val();
	if (otherFee ==''){
		otherFee = 0;
	}
	var purchasePrice = $("#receivables").text();
	var paymentPrice = $("#paymentPrice").val();
	if (paymentPrice <= 0){
		layer.tips('请填写收款金额', '#paymentPrice',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	var date = $("#date").val();
	if (date == ''){
		layer.tips('请填写日期', '#date',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	var paymentType = $("#paymentType").children('option:selected').val();
	if (paymentType == '0'){
		layer.tips('请选择收款方式', '#paymentType',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	var paymentPerson = $("#paymentPerson").val();
	var handler = $("#handler").val();
	if (paymentPerson == ''){
		layer.tips('请填写收款人', '#paymentPerson',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	// 附件
	var a3 = $("#attachment3Div").find("span").length;
	var a3Str = ""
	if(a3 != 0){
		for(var i = 0;i < a3;i++){
			var url3 = $("#attachment3Div").find("span:eq("+i+")").find("input").val();
			a3Str = a3Str + url3 + ",";
		}
		a3Str = a3Str.substring(0, a3Str.length - 1);
	}
	if (handler == ''){
		layer.tips('请填写经办人', '#handler',{
			tips: [1, '#f00'],
  			time: 4000
		});
		return;
	}
	var manualOrder = suppName + ";" + contact_person + ";"+ clientPhone + ";"+purchasePrice  + ";"+ otherFee + ";" 
		+ paymentPrice + ";" + date + ";"+paymentType + ";" + paymentPerson + ";"+ handler + ";"+productInfor + ";" 
		+ sellerId+";"+ num + ";" + discount_sum + ";" + total + ";" + a3Str;
	console.log(manualOrder);
	var ContactSettings = contact_person + "," + clientPhone + "," + "" + "," + 
	"" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + ","+ 1 + "&";
	$.ajax({
		type : "post",
		url : "platform/sellers/client/manualSaveSellerClient",
		async: false,
		data : {
			"clientName" : suppName,
			"bankAccount" : "",
			"openBank" : "",
			"accountName" : "",
			"taxidenNum" : "",
			"ContactSettings" : ContactSettings
		},
		success : function(data) {
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				
			}else{
				layer.msg(resultObj.msg,{icon:2});
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
	
	$.ajax({
		type:'POST',
		data : {
			"manualOrder" : manualOrder
		},
		url :'platform/sellers/salesmanagement/addSellerManualOrder',
		success :function(data) {
			var str = data.toString();
			$(".content").html(str);
		},
		error :function(e) {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});	
}
//返回购物车
//返回购物车修改
function backAddManualOrder(){
	
	$.ajax({
		type:'POST',
		url:"platform/sellers/salesmanagement/loadManualOrder",
		data:{
			"sellerId":$("#sellerId").val(),
			"suppName":$("#buyers").val(),
			"contact_person":$("#contact_person").val(),
			"clientPhone":$("#client_Phone").val(),		
			"productInfor":$("#productInfor").val(),		
			"num":$("#num").val(),
			"discount_sum":$("#discount_sum").val(),
			"total":$("#total").val(),		
			"otherFee":$("#otherFee").val(),
			"paymentPrice":$("#paymentPrice").val(),
			"date":$("#date").val(),
			"paymentType":$("#paymentType").val(),
			"paymentPerson":$("#paymentPerson").val(),
			"handler":$("#handler").val()
			//"annexVoucher":$("#annexVoucher").val
		},	
		success:function(data){
			
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

//采购商查询
function selectSeller(obj){
	

	var goodsUlObj = $(obj).parent().find("ul");
	if(goodsUlObj.length == 0){
		goodsUlObj = $("<ul></ul>");
		goodsUlObj.attr("class","menu ui-menu ui-widget ui-widget-contents");
		$(obj).parent().append(goodsUlObj);
	}
	goodsUlObj.empty();
	goodsUlObj.css("display","block");
	
	var selectValue = $(obj).val();

		var url = basePath+"platform/sellers/client/getSellerSelect"
		$.ajax({
			url : url,
			data:{
				"selectValue":selectValue
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				$.each(resultObj,function(i){
					var liObj = $('<li>');
					$("<input>",{name:"clientName",value:resultObj[i].client_Name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"clientPerson",value:resultObj[i].client_Person,hidden:true}).appendTo(liObj);
					$("<input>",{name:"clientPhone",value:resultObj[i].client_Phone,hidden:true}).appendTo(liObj);
					$("<input>",{name:"sellerId",value:resultObj[i].id,hidden:true}).appendTo(liObj);
					var showText = resultObj[i].client_Name;
					var abridgeText = (showText.length)>25?showText.substring(0,25)+"...":showText;
					$("<a href='javascript:void(0)' onclick='chooseSeller(this)' protitle='"+showText+"'>"+abridgeText+"</a>").appendTo(liObj);
					liObj.appendTo(goodsUlObj);
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
}

//选择采购商
function chooseSeller(obj){
	
	var ul = $(obj).parents("ul");
	var form = ul.parents("form");
//	form.find("input[name='contact_person']").val($(obj).parent().find("input[name='clientPerson']").val());
//	form.find("input[name='clientPhone']").val($(obj).parent().find("input[name='clientPhone']").val());
//	form.find("input[name='sellerId']").val($(obj).parent().find("input[name='sellerId']").val());

	form.find("#contact_person").val($(obj).parent().find("input[name='clientPerson']").val());
	form.find("#clientPhone").val($(obj).parent().find("input[name='clientPhone']").val());
	form.find("#sellerId").val($(obj).parent().find("input[name='sellerId']").val());
	var protitle = $(obj).attr("protitle");
	form.find("#suppName").val(protitle);
	ul.remove();
}


//商品查询
function selectGoodsSeller(obj){
	
	//客户判空
	var suppName=$("#suppName").val();
	if(suppName === ""){
		layer.msg("请选择采购商！",{icon:2});
		return;
	}
//	var supplierId=supplierIdArr.split(",")[0];
	var goodsUlObj = $(obj).parent().find("ul");
	if(goodsUlObj.length == 0){
		goodsUlObj = $("<ul></ul>");
		goodsUlObj.attr("class","menu ui-menu ui-widget ui-widget-contents");
		$(obj).parent().append(goodsUlObj);
	}
	goodsUlObj.empty();
	goodsUlObj.css("display","block");
	
	var selectValue = $(obj).val();

		var url = basePath+"platform/product/selectGoods"
		$.ajax({
			url : url,
			data:{
				"selectValue":selectValue
				//"supplierId":supplierId
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				$.each(resultObj,function(i){
					var liObj = $('<li>');
					$("<input>",{name:"proCode",value:resultObj[i].product_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"proName",value:resultObj[i].product_name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuCode",value:resultObj[i].sku_code,hidden:true}).appendTo(liObj);
//					$("<input>",{name:"colorCode",value:resultObj[i].color_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuOid",value:resultObj[i].barcode,hidden:true}).appendTo(liObj);
					$("<input>",{name:"unitId",value:resultObj[i].unit_id,hidden:true}).appendTo(liObj);
					$("<input>",{name:"price",value:resultObj[i].price,hidden:true}).appendTo(liObj);
					var showText = resultObj[i].product_code+"|"
						+resultObj[i].product_name+"|"
						+resultObj[i].sku_code+"|"
//						+resultObj[i].color_code+"|"
						+resultObj[i].barcode;
					var abridgeText = (showText.length)>25?showText.substring(0,25)+"...":showText;
					$("<a href='javascript:void(0)' onclick='chooseGood(this)' protitle='"+showText+"'>"+abridgeText+"</a>").appendTo(liObj);
					liObj.appendTo(goodsUlObj);
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
}

//选择商品
function chooseGood(obj){
	
	var ul = $(obj).parents("ul");
	var tr = ul.parents("tr");
	tr.find("input[name='proCode']").val($(obj).parent().find("input[name='proCode']").val());
	tr.find("input[name='proName']").val($(obj).parent().find("input[name='proName']").val());
	tr.find("input[name='skuCode']").val($(obj).parent().find("input[name='skuCode']").val());
//	tr.find("input[name='colorCode']").val($(obj).parent().find("input[name='colorCode']").val());
	tr.find("input[name='skuOid']").val($(obj).parent().find("input[name='skuOid']").val());
	tr.find("input[name='price']").val($(obj).parent().find("input[name='price']").val());
	tr.find("td:eq(2) select").val($(obj).parent().find("input[name='unitId']").val());
	tr.find("#unit_price").val($(obj).parent().find("input[name='price']").val());
	var protitle = $(obj).attr("protitle");
	tr.find("#proInfo").val(protitle);
	ul.remove();
}


//保存手工添加订单
function saveManualOrder(){
	
	//收货地址判空
	var is_since = "1";
	if(!$("input[name='is_since']").is(":checked")){
		is_since = "0";
		if($('.addr>div.selected').length!=1){
			layer.msg("请选择收货地址！",{icon:2});
			return;
		}
	}
	var addrName = $(".addr>div.selected #shipAddress").text();
	var personName = $(".addr>div.selected #personName").text();
	var receiptPhone = $(".addr>div.selected #receiptPhone").text();
	var url = "platform/sellers/salesmanagement/saveManualOrderInsert";
	$.ajax({
		url:url,
		async:false,
		data:{
			"sellerId":$("#sellerId").val(),
			"buyers":$("#buyers").val(),
			"contact_person":$("#contact_person").val(),
			"clientPhone":$("#client_Phone").val(),		
			"productInfor":$("#productInfor").val(),		
			"num":$("#num").val(),
			"discount_sum":$("#discount_sum").val(),
			"total":$("#total").val(),
			"purchasePrice":$("#purchasePrice").val(),
			"otherFee":$("#otherFee").val(),
			"paymentPrice":$("#paymentPrice").val(),
			"date":$("#date").val(),
			"paymentType":$("#paymentType").val(),
			"paymentPerson":$("#paymentPerson").val(),
			"handler":$("#handler").val(),
			"supplierRemark":$("#supplierRemark").val(),
			"addrName":addrName,
			"personName":personName,
			"receiptPhone":receiptPhone,
			"is_since":is_since,
			"menuName":"17070718450023263056"
		},
		success:function(data){
			
			if(data.flag){
				var res = data.res;
				if(res.code==40000){
					//调用成功
				    saveSucess(res.data);			
				}else if(res.code==40010){
					//调用失败
					layer.msg(res.msg,{icon:2});
					return false;
				}else if(res.code==40011){
					//需要设置审批流程
					layer.msg(res.msg,{time:500,icon:2},function(){
						setApprovalUser(url,res.data,function(data){
							saveSucess(res.data);	
						});
					});
					return false;
				}else if(res.code==40012){
					//对应菜单必填
					layer.msg(res.msg,{icon:2});
					return false;
				}
			}else{
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
				return false;
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

//保存成功页面
function saveSucess(res){
	
	var orderId = "";
	for(var i=0;i<res.length;i++)
	{
	    orderId =res[i];
	}
	$.ajax({
		url:"platform/sellers/salesmanagement/addManualOrderSucc",
		success:function(data){
//			var str = data.toString();
//			$(".content").html(str);
			leftMenuClick(this,'platform/sellers/salesmanagement/addManualOrderSucc?orderId='+orderId,'sellers');
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

//切换收货地址
$('.addr').on('click','.addrItem',function(){
  $(this).addClass('selected').siblings().removeClass('selected',"");
});
$('.addr').on('mouseenter','.addrItem',function(){
  $(this).children('.borderBg').addClass('borderHover');
}).on('mouseleave','.addrItem',function(){
  $(this).children('.borderBg').removeClass('borderHover');
});
//修改收货地址
function updateAddress(obj){
	
	var parentDiv = $(obj).parent().parent();
	var id = parentDiv.find("#addressId").val();
	$.ajax({
		type: "POST",
		url: "platform/sellers/client/updateAddress",
		data:{
			"id": id
		},
		success:function(data){
			var str = data.toString();
			layer.open({
				type: 1,
			    title:'修改收货地址',
			    skin: 'pop btnCenter',
			    closeBtn:2,
			    area: ['668px', '380px'],
			    btn:['保存','关闭'],
			    content:str,
			    yes: function(index, layero){
			  		var isDefault = 0;
	  			  	if($("#updateIsDefault").is(":checked")){//选中  
	  			  		isDefault = 1;
	  			  	}
				    // 确定保存
	  			  	$.ajax({
	  					type : "POST",
	  					url : "platform/sellers/client/saveUpdateAddress",
	  					async: false,
	  					data : {
	  						"id" : $("#updateId").val(),
	  						"clientPerson" : $("#updateClientPerson").val(),
	  						//"province" : $("#updateProvince").val(),
	  						//"city" : $("#updateCity").val(),
	  						//"area" : $("#updateArea").val(),
	  						"shipAddress" : $("#addr1 option:selected").text()+""+$("#addr2 option:selected").text()+""+$("#addr2 option:selected").text()+""+$("#addr4").val(),
	  						"clientPhone" : $("#updateclientPhone").val(),
	  						"zone" : $("#updateZone").val(),
	  						"telno" : $("#updateTelNo").val(),
	  						"isDefault" : isDefault
	  					},
	  					success : function(data) {
	  						var result = eval('(' + data + ')');
	  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  						if(resultObj.success){
	  							layer.close(index);
	  							ResetAddress(resultObj.address,parentDiv);
	  						}else{
	  							layer.msg(resultObj.msg,{icon:2});
	  							return false;
	  						}
	  					},
	  					error : function() {
	  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  					}
	  				});
			  	}
		  	});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//新增收货地址
function addAddress(){
	
	var id= $("#sellerId").val();
	$.ajax({	
		type: "POST",
  		url: "platform/sellers/client/addAddressManualOrder",
  		data:{
			"sellerClientId": $("#sellerId").val()
		},
  		success: function(data){
  			
  			var str = data.toString();
			layer.open({
				type: 1,
			    title:'新增收货地址',
			    skin: 'pop btnCenter',
			    closeBtn:2,
			    area: ['668px', '380px'],
			    btn:['保存','关闭'],
			    content:str,
			    no:function(index){
			    	layer.close(index);
			    },
  			  	yes: function(index, layero){
  			  		var isDefault = 0;
	  			  	if($("#isDefault").is(":checked")){//选中  
	  			  		isDefault = 1
	  			  	}
	  			 var  clientPerson = $("#updateClientPerson").val();
	  			if(clientPerson === ""){
	  				layer.tips('请填写收货人', '#updateClientPerson',{
	  					tips: [1, '#f00'],
	  		  			time: 4000
	  				});
	  				return;
	  			}
	  			  var addr1 = $("#addr1 option:selected").text();
	  			if(addr1 === "省份"){
	  				layer.tips('请选择省份', '#addr1',{
	  					tips: [1, '#f00'],
	  		  			time: 4000
	  				});
	  				return;
	  			}
	  	    	var addr2 = $("#addr2 option:selected").text();
	  			if(addr2 === "地级市"){
	  				layer.tips('请选择市', '#addr2',{
	  					tips: [1, '#f00'],
	  		  			time: 4000
	  				});
	  				return;
	  			}
	  	    	var addr3 = $("#addr3 option:selected").text();
	  			if(addr3 === "县区"){
	  				layer.tips('请选择县区', '#addr3',{
	  					tips: [1, '#f00'],
	  		  			time: 4000
	  				});
	  				return;
	  			}
	  	    	var addr4 = $('#addr4').val();
	  			if(addr4 === ""){
	  				layer.tips('请填写详细地址', '#addr4',{
	  					tips: [2, '#f00'],
	  		  			time: 4000
	  				});
	  				return;
	  			}
  			    	// 确定保存
	  			  	$.ajax({
	  					type : "POST",
	  					url : "platform/sellers/client/saveAddAddress",
	  					async: false,
	  					data : {
	  						"sellerClientId": $("#sellerClientId").val(),
	  						"id" : $("#updateId").val(),
	  						"clientPerson" : $("#updateClientPerson").val(),
	  						//"province" : $("#updateProvince").val(),
	  						//"city" : $("#updateCity").val(),
	  						//"area" : $("#updateArea").val(),
	  						"shipAddress" : $("#addr1 option:selected").text()+""+$("#addr2 option:selected").text()+""+$("#addr2 option:selected").text()+""+$("#addr4").val(),
	  						"clientPhone" : $("#updateclientPhone").val(),
	  						"zone" : $("#updateZone").val(),
	  						"telno" : $("#updateTelNo").val(),
	  						"isDefault" : isDefault
	  					},
	  					success : function(data) {
	  						var result = eval('(' + data + ')');
	  						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	  						if(resultObj.success){
	  							layer.close(index);
	  							ResetAddress(resultObj.address,null);
	  						}else{
	  							layer.msg(resultObj.msg,{icon:2});
	  							return false;
	  						}
	  					},
	  					error : function() {
	  						layer.msg("获取数据失败，请稍后重试！",{icon:2});
	  					}
	  				});
  			  	}
  			});
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
 	});
}
//重置收货地址
function ResetAddress(resultObj,divObj){
	var divStr = "";
	if(resultObj.isDefault==1){
		divStr += "<img src=\"<%=basePath%>statics/platform/images/default_small.jpg\" class=\"defaultAddressImg\">";
	}
	divStr += "<div class='borderBg'></div>";
	divStr += "<div class='addr_hd'>";
	divStr += "<span class='size_md'><b id='personName'>"+resultObj.clientPerson+"</b></span> 收";
	divStr += "</div>";
	divStr += "<div class='addr_bd'>";
	divStr += "<span id='shipAddress'>"+resultObj.shipAddress+"</span>";
	divStr += "<span id='receiptPhone'>"+resultObj.clientPhone+"</span>";
//	divStr += " <span>"+resultObj.phone+"</span>";
	divStr += "</div>";
	divStr += "<div class='addr_md text-right'>";
	divStr += "<span class='addr_modify' onclick='updateAddress(this);'>"
	divStr += "<i class='layui-icon'>&#xe691;</i>修改</span>";
	divStr += "</div>";
	divStr += "<input type='hidden' name='addressId' id='addressId' value='"+resultObj.id+"'>";
	if(divObj==null||divObj.length==0){
		var divId=new Date().getTime();
		divObj = $("<div class='addrItem'>",{id:divId});
		divObj.append(divStr);
		$(".addr>.addrItem:last").after(divObj);
		divObj.addClass('selected').siblings().removeClass('selected');
	}else{
		divObj.empty();
		divObj.append(divStr);
		divObj.addClass('selected').siblings().removeClass('selected');
	}
	$('.addr>div').click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	if(resultObj.isDefault==0){
		$(".isDefault .defaultAddressImg").remove();
		$(".isDefault").removeClass("isDefault");
		divObj.addClass("isDefault");
	}
}

//单位下拉列表共通
function unitSelect(obj){
	var url = basePath+"platform/common/getUnitSelect"
	$.ajax({
		url : url,
		async:false,
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var selectObj = $("<select></select>");
			if(obj.selectName != null && obj.selectName != ''){
				selectObj.attr("name",obj.selectName);
			}
			if(obj.selectId != null && obj.selectId != ''){
				selectObj.attr("id",obj.selectId);
			}
			$('<option>',{val:"",text:"请选择"}).appendTo(selectObj);
			var selectValue = obj.selectValue;
			$.each(resultObj,function(i){
				var id = resultObj[i].id;
				var unitName = resultObj[i].unitName;
				if(selectValue != null && selectValue != ''){
					if(selectValue==id){
						$('<option>',{val:id,text:unitName,selected:"selected"}).appendTo(selectObj);
						return;
					}
				}
				$('<option>',{val:id,text:unitName}).appendTo(selectObj);
			});
			$("#"+obj.divId).append(selectObj);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

//功能点11:弹出增加商品框
$('.purchase_list').on('click','span.increase_goods',function(){
	
  layer.open({
    type:1,
    title:'新增商品',
    area:['900px', '460px'],
    skin:'pop',
    closeBtn:2,
    content:$('.new_goods'),
    btn:['保存','关闭'],
    yes:function(index){
    	
    	var newProductInfor = "";
    	var proNameError = "";
    	var unitError = "";
    	var productNameError = "";
    	var skuCodeError = "";
    	var colorCodeError = "";
    	var barcodeError = "";
    	var minStockError = "";
    	var standardStockError = "";
    	var priceError = "";
  		$("#newGoods tr").each(function(i) {
  			var productCode = $(this).find("td:eq(1)").find("input").val();
  			if(productCode==''){
  				proNameError = "第"+(i+1)+"行没有填写货号！";
  				return false; 
  			}
  			var productName = $(this).find("td:eq(2)").find("input").val();
  			if(productName==''){
  				productNameError = "第"+(i+1)+"行没有填写商品名称！";
  				return false; 
  			}
  			var skuCode = $(this).find("td:eq(3)").find("input").val();
  			if(skuCode==''){
  				skuCodeError = "第"+(i+1)+"行没有填写规格代码！";
  				return false; 
  			}
  			var skuName = $(this).find("td:eq(4)").find("input").val();
  			if(skuName==''){
  				colorCodeError = "第"+(i+1)+"行没有填写规格名称！";
  				return false; 
  			}
  			var barcode = $(this).find("td:eq(5)").find("input").val();
  			if(barcode==''){
  				barcodeError = "第"+(i+1)+"行没有填写条形码！";
  				return false; 
  			}
  			//var unitName = $(this).find("td:eq(6)").find("input").val();
  			var unitId = $(this).find("td:eq(6)").find("option:selected").val();
  			if(unitId==''){
  				unitError="第"+(i+1)+"行没有设置单位！";
  				return false;
  			}
  			var minStock = $(this).find("td:eq(7)").find("input").val();
  			if(minStock==''){
  				minStockError = "第"+(i+1)+"行没有填写库存！";
  				return false; 
  			}
  			var standardStock = $(this).find("td:eq(8)").find("input").val();
  			if(standardStock==''){
  				standardStockError = "第"+(i+1)+"行没有填写标准库存！";
  				return false; 
  			}
  			var price = $(this).find("td:eq(9)").find("input").val();
  			if(price==''){
  				priceError = "第"+(i+1)+"行没有填写单价！";
  				return false; 
  			}
			
  			newProductInfor = newProductInfor+productCode + "/" + productName + "/" + skuCode + "/" + 
  			skuName + "/" + barcode + "/" + unitId + "/" + minStock + "/" + standardStock+"/" + price +"&";
  		});
  		if(proNameError!=''){
  			layer.msg(proNameError,{icon:2});
  			return;
  		}
  		if(unitError!=''){
  			layer.msg(unitError,{icon:2});
  			return;
  		}
  		if(productNameError!=''){
  			layer.msg(productNameError,{icon:2});
  			return;
  		}
  		if(skuCodeError!=''){
  			layer.msg(skuCodeError,{icon:2});
  			return;
  		}
  		if(colorCodeError!=''){
  			layer.msg(colorCodeError,{icon:2});
  			return;
  		}
  		if(barcodeError!=''){
  			layer.msg(barcodeError,{icon:2});
  			return;
  		}
  		if(minStockError!=''){
  			layer.msg(minStockError,{icon:2});
  			return;
  		}
  		if(standardStockError!=''){
  			layer.msg(standardStockError,{icon:2});
  			return;
  		}if(priceError!=''){
  			layer.msg(priceError,{icon:2});
  			return;
  		}
  		
  		newProductInfor = newProductInfor.substring(0, newProductInfor.length - 1);
  		$.ajax({
  			type : "post",
  			url : 'platform/sellers/salesmanagement/saveManualOrderProduct',
  			data : {
  				"newProductInfor" : newProductInfor
  			},
  			
  			async : false,
  			success : function(data) {
  				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					layer.close(index);
				}else{
					layer.msg(resultObj.msg,{icon:2});
					return false;
				}
  			}
//  			error : function() {
//  				layer.msg("获取数据失败，请稍后重试！",{icon:2});
//  			}
  		});
      layer.close(index);
    }
  });
});