var xArr = [""];
var legendArr = [];
var pieSeriesArr = [];
var lineSeriesArr=[0];
$.each(supplierList,function(i,data){
	// 折线图
	//xArr.push(data.supp_name);
	//lineSeriesArr.push(data.order_num);
	// 饼形图
	legendArr.push(data.supp_name);
	pieSeriesArr.push(data.order_num);
});
/*xArr.push("");
lineSeriesArr.push(0);
var lineEcharts = echarts.init(document.getElementById("echartsLine"));
var lineOption = {
    title: {
        text: "供应商使用率"
    },
    tooltip: {
    	// 提示框组件,axis:坐标轴触发，主要在柱状图，折线图等会使用类目轴的图表中使用
        trigger: "axis",
        formatter: "{a} <br/>{b} : {c} 次 "
    },
    legend: {
    	// 图例的数据数组
        data:[]
    },
    grid: {
        x: 40,
        x2: 40,
        y2: 24
    },
    calculable: !0,
    xAxis: [{
        type: "category",
        boundaryGap: !1,
        axisLabel :{  // 解决X轴显示不全
            interval:0   
        },
        data: []
    }],
    yAxis: [{
        type: "value",
        axisLabel: {
            formatter: "{value} 次"
        }
    }],
    series: [{
    	name:"供应商使用率",
    	type:'line',
    	data:[],
    	markPoint: {
            data: [
                {type: 'max', name: '最大值'}
            ]
        }
    }]
};
lineOption.legend.data = ["供应商使用率"];
lineOption.xAxis[0].data = xArr;
lineOption.series[0].data=lineSeriesArr;
lineEcharts.setOption(lineOption);
$(window).resize(lineEcharts.resize);*/
// 饼图
var pieEcharts = echarts.init(document.getElementById("echartsPie"));
var pieOption = {
    title: {
        text: "供应商使用率",
        subtext: "买卖系统统计",
        x: "center"
    },
    toolbox: {
        show : true,
        feature : {
            saveAsImage : {show: true}
        }
    },
    tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} 次 ({d}%)"
    },
    legend: {
        orient: "vertical",
        x: "left",
        data: []
    },
    calculable: !0,
    series: [{
        name: "使用率",
        type: "pie",
        radius: "55%",
        center: ["50%", "60%"],
        data: []
    }]
};
pieOption.legend.data = legendArr;
$.each(pieSeriesArr,function(i){
	var item = {
		name : legendArr[i],
		value : pieSeriesArr[i],
		itemStyle:{
	    	normal:{
	          label:{
	            show: true,
	            formatter: '{b} : {c} 次 ({d}%)'
	          },
	          labelLine :{show:true}
	        }
	    }
	};
	pieOption.series[0].data.push(item);
});
pieEcharts.setOption(pieOption);
$(window).resize(pieEcharts.resize);