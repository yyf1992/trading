
$(function(){
	selAllClient();
});
//功能点1:全选事件
function selAllClient(){
	var inputs=$('.whareaList tbody input');
	$('.whareaList thead input').click(function(){
		inputs.prop('checked',$(this).prop('checked'));
	});
	inputs.click(function(){
		var r=$('.whareaList tbody input:not(:checked)');
		if(r.length==0){
			$('.whareaList thead input').prop('checked',true);
		}else{
			$('.whareaList thead input').prop('checked',false);
		}
	});
}

//批量删除客户
function beatchDeleteWharea(){
	var checked = $("input:checkbox[name='checkones']:checked");
	if (checked.length < 1) {
		layer.msg("请至少选择一条数据！",{icon:1});
		return;
	}
	var ids = "";
	checked.each(function() {
		ids += $(this).val() + ",";
	});
	ids = ids.substring(0, ids.length - 1);
	layer.confirm('确定删除仓库吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){

	    	$.ajax({
				type : "post",
				url : "platform/baseInfo/wharea/deleteBeatchWharea",
				data : {
					"ids" : ids
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1},function(){
							leftMenuClick(this,"platform/baseInfo/wharea/whareaList","baseinfo");
						});
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    
	    });
}

//重置
function recovery(){
	$("#searchForm").find("ul:eq(0)").find("li:eq(0)").find("input").val("");
	$("#searchForm").find("ul:eq(0)").find("li:eq(1)").find("input").val("");
	
	var $defaults = $('input:checkbox[checked]');
	$('input:checkbox').attr('checked', false);
    $defaults.attr('checked', true);
	//$("#searchForm").find("ul:eq(1)").find("li:eq(0)").find("input").val("");
}
//按条件查询
function loadWhArarList(){
	var contentObj;
	if($(".content").length>0) contentObj=$(".content");
	if($(".content_modify").length>0) contentObj=$(".content_modify");
	var formObj = contentObj.find("form");
	$.ajax({
		url : formObj.attr("action"),
		data:formObj.serialize(),
		async:false,
		success:function(data){
			var str = data.toString();
			
			contentObj.html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});


}

//删除仓库
function deleteWharea(id){
	console.log(id);
	 layer.confirm('确定删除该仓库吗？', {
		    icon:3,
		    title:'提醒',
		    closeBtn:2,
		    btn: ['确定','取消']
		  }, function(){
			  $.ajax({
					type : "post",
					url : "platform/baseInfo/wharea/deleteWharea",
					data : {
						"id" : id
					},
					async : false,
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if (resultObj.success) {
							layer.msg(resultObj.msg,{icon:1},function(){
								leftMenuClick(this,"platform/baseInfo/wharea/whareaList","baseinfo");
							});
						} else {
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		  });
}

//弹出新整界面并新增仓库
function addWharea(){
	  layer.open({
      type: 1,
      title: '添加仓库',
      area: ['350px', 'auto'],
      skin: 'pop ',
      closeBtn:2,
      content: $('.add_wharea'),
      btn: ['保存','取消'],
      yes:function(index){
      	var whareaCode = $("#add_Code").val();
      	var whareaName = $("#add_Name").val();
      	
      	var whAreaType = $("#add_whAreaType").children('option:selected').val();
      	var status = $("#add_status").children('option:selected').val();
      	var defectWare = '';
		if ($("#add_defect").is(":checked")){
		 defectWare = '是';
		}
		console.log(whAreaType);
		console.log(status);
		if(whareaCode == ''){
			layer.tips('请输入仓库代码', '#add_Code',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		if(whareaName == ''){
			layer.tips('请输入仓库名称', '#add_Name',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		if(whAreaType == '请选择'){
			layer.tips('请选择仓库类型', '#add_whAreaType',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		
		$.ajax({
			type : "post",
			url : 'platform/baseInfo/wharea/saveWharea',
			data : {
				"whareaCode" : whareaCode,
				"whareaName" : whareaName,
				"whAreaType" : whAreaType,
				"status" : status,
				"defectWare" : defectWare
			},
			
			async : false,
			success : function(data) {
				layer.close(index);
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					leftMenuClick(this,"platform/baseInfo/wharea/whareaList","baseinfo");
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
           
      layer.close(index);
      }
    });
}

//修改仓库
function updateWharea(obj){
	
	var whareaCode = $(obj).parent().parent().find('td:eq(1)').html();
	var whareaName = $(obj).parent().parent().find('td:eq(2)').html();
	var status = $(obj).parent().parent().find('td:eq(3)').html();
	var whAreaType = $(obj).parent().parent().find('td:eq(4)').html();
	var defectWare = $(obj).parent().parent().find('td:eq(6)').html();
	console.log(defectWare);
	$('#update_whareaCode').val(whareaCode);
	$('#update_whareaName').val(whareaName);
	$('#update_whAreaType').val(whAreaType);
	$('#update_status').val(status);
	
	if (defectWare == "是"){
		$('#update_defect').attr('checked',true);
	}else{
		$('#update_defect').attr('checked',false);
	}


	layer.open({
    type: 1,
    title: '修改仓库',
    area: ['345px', 'auto'],
    skin: 'pop ',
    closeBtn:2,
    content: $('.update_wharea'),
    btn: ['保存'],
    yes:function(index){   	
    	var id = $(obj).parent().parent().find('td:eq(0)').find('input').val();
      	var whareaCode = $("#update_whareaCode").val();
      	var whareaName = $("#update_whareaName").val();
      	var whAreaType = $("#update_whAreaType").children('option:selected').val();
      	var status = $("#update_status").children('option:selected').val();
      	var defectWare = '';
		if ($("#update_defect").is(":checked")){
		 defectWare = '是';
		}
		console.log(id);
		if(whareaCode == ''){
			layer.tips('请输入仓库代码', '#update_whareaCode',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		if(whareaName == ''){
			layer.tips('请输入仓库名称', '#update_whareaName',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		if(whAreaType == '请选择'){
			layer.tips('请选择仓库类型', '#update_whAreaType',{
				tips: [2, '#f00'],
	  			time: 4000
			});
			return;
		}
		
		$.ajax({
			type : "post",
			url : 'platform/baseInfo/wharea/updateWharea',
			data : {
				"id" : id,
				"whareaCode" : whareaCode,
				"whareaName" : whareaName,
				"whAreaType" : whAreaType,
				"status" : status,
				"defectWare" : defectWare
			},
			
			async : false,
			success : function(data) {
				layer.close(index);
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					leftMenuClick(this,"platform/baseInfo/wharea/whareaList","baseinfo");
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
		layer.close(index);
    }
	
    });
}


