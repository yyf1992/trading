$(function(){
	//是否删除供应商
	  $('.supplierList').on('click','td:last-child a:nth-child(3)',function(){
	    layer.confirm('确定删除该供应商吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      skin:'pop',
	      btn: ['确定','取消']
	    }, function(){
	      delRow(this);
	      layer.msg('删除成功',{time:500});
	    }.bind(this));
	  });
	  //是否删除客户
	  $('.customerList').on('click','td:last-child a:nth-child(3)',function(){
	    layer.confirm('确定删除该客户吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      skin:'pop',
	      btn: ['确定','取消']
	    }, function(){
	      delRow(this);
	      layer.msg('删除成功',{time:500});
	    }.bind(this));
	  });
	  //批量删除客户
	  $('#batch_d_c').click(function(){
	    layer.confirm('确定批量删除客户吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      skin:'pop',
	      btn: ['确定','取消']
	    }, function(){
	      layer.msg('删除成功',{time:500});
	    });
	  });
	  //联系人删除一行
	  $('.contract_settings').on('click','.table_del',function(){
	    layer.confirm('确定删除该联系人吗？', {
	      icon:3,
	      title:'提醒',
	      skin:'pop',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	      delRow(this);
	      layer.msg('删除成功',{time:500});
	    }.bind(this));
	  });
	  //客户删除一行
	  $('.c_settings').on('click','.table_del',function(){
	    layer.confirm('确定删除该联系人吗？', {
	      icon:3,
	      title:'提醒',
	      skin:'pop',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	      delRow(this);
	      layer.msg('删除成功',{time:500});
	    }.bind(this));
	  });
	  //弹出地址选择框
	  $('.c_settings').on('click','tbody td:nth-child(4)',function(){
	    layer.open({
	      type: 1,
	      title: '收货地址修改',
	      area: ['380px', 'auto'],
	      skin: 'pop ',
	      closeBtn:2,
	      content: $('.edit_addr'),
	      btn: ['保存','取消'],
	      yes:function(index){
	        var str='';
	        str+=$('#addr1').val();
	        str+=$('#addr2').val();
	        str+=$('#addr3').val();
	        str+=$('#addr4').val();
	        $(this).html(str);
	        layer.close(index);
	      }.bind(this)
	    });
	  });
});

//供应商联系人新增一行
$('.contract_a').click(function(e){
  e.preventDefault();
  var str='<tr><td><input type="radio" name="default" onclick="setIsDefault(this)"></td>'
	  +'<td><input type="text" placeholder="联系人"></td>'
	  +'<td><input type="text" placeholder="手机"></td>'
	  +'<td>'
	  +'<input type="text" placeholder="区号" style="width:50px">'
	  +'-<input type="text" style="width:100px" placeholder="电话">'
	  +'</td>'
	  +'<td><input type="text" placeholder="传真"></td>'
	  +'<td><input type="text" placeholder="QQ"></td>'
	  +'<td><input type="text" placeholder="旺旺"></td>'
	  +'<td><input type="text" placeholder="E-mail"></td>'
	  +'<td><span class="table_del"><b></b>删除</span>'
	  +'<input type="hidden" name="isDefault" value="0">'
	  +'</td></tr>';
	  var tby=$('.contract_settings tbody');
	  tby.append(str);
});
//客户联系人新增一行
$('.customer_a').click(function(e){
  e.preventDefault();
  var str='<td><input type="text" placeholder="联系人"></td>'
	  +'<td><input type="text" placeholder="手机"></td>'
	  +'<td>'
	  +'<input type="text" placeholder="区号" style="width:40px">'
	  +'-<input type="text" style="width:70px" placeholder="电话">'
	  +'</td>'
	  +'<td>点击选择收货地址</td>'
	  +'<td><input type="text" placeholder="传真"></td>'
	  +'<td><input type="text" placeholder="QQ"></td>'
	  +'<td><input type="text" placeholder="旺旺"></td>'
	  +'<td><input type="text" placeholder="E-mail"></td>'
	  +'<td><span class="table_del"><b></b>删除</span></td>';
  var tbl=$('.c_settings tr:eq(-1)');
  addRow(str,tbl);
});
//删除营业执照
$('.img_license').on('click','b',function(){
  layer.confirm('确定删除该图片吗？', {
    icon:3,
    title:'提醒',
    skin:'pop',
    closeBtn:2,
    btn: ['确定','取消']
  }, function(){
    $(this).parent().remove();
    layer.msg('删除成功',{time:500});
  }.bind(this));
});
//验证供应商绩效考核数据是否完整
function validateAssesment(){
	var assessmentName=$(".assessmentName").val();
	var score=$(".score").val();
	var remark=$(".remark").val();
	var state=$('input:radio[name="state"]:checked').val();
	var arrivalRateScore=$(".arrivalRateScore").val();
	var completionRateScore=$(".completionRateScore").val();
	var qualificationRateScore=$(".qualificationRateScore").val();
	var afterSaleRateScore=$(".afterSaleRateScore").val();
	var scoreType=$('input:radio[name="scoreType"]:checked').val();
	if(!assessmentName){
		layer.msg("请填写考核名称！",{icon:7});
		return;
	}
	if(!score){
		layer.msg("请填写分值！",{icon:7});
		return;
	}
	if(!arrivalRateScore){
		layer.msg("请填写到货及时率分值！",{icon:7});
		return;
	}
	if(!completionRateScore){
		layer.msg("请填写计划完成率分值！",{icon:7});
		return;
	}
	if(!qualificationRateScore){
		layer.msg("请填写到货合格率分值！",{icon:7});
		return;
	}
	if(!afterSaleRateScore){
		layer.msg("请填写售后响应及时率分值！",{icon:7});
		return;
	}
	var total=parseFloat(arrivalRateScore)+parseFloat(completionRateScore)+parseFloat(qualificationRateScore)+parseFloat(afterSaleRateScore);
	var item = new Object();
	if(scoreType==1){
		if(total!=100){
			layer.msg("分值配比不等于100！",{icon:7});
			return;
		}
	}
	if(scoreType==0){
		var s=parseFloat(score);
		if(s!=total){
			layer.msg("考核项分值之和不等于考核的总分值！",{icon:2});
			return;
		}
	}
	item.assessmentName=assessmentName;
	item.score=score;
	item.remark=remark;
	item.state=state;
	item.scoreType=scoreType;
	item.arrivalRateScore=arrivalRateScore;
	item.completionRateScore=completionRateScore;
	item.qualificationRateScore=qualificationRateScore;
	item.afterSaleRateScore=afterSaleRateScore;
	return item;
}
//保存供应商绩效考核项
function saveInsert(){
	var item =validateAssesment();
	if(!item){
		return;
	}
	$.ajax({
		type:"post",
		url:"platform/buyer/supplier/saveInsertAssesment",
		data:item,
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				layer.msg("保存成功！",{icon:1});
				leftMenuClick(this,"platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml","buyer","18010209272828100705");
			}else{
				layer.msg(resultObj.msg,{icon:2});
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//保存修改供应商绩效考核项
function saveEditSupplierAssesment(){
	var item =validateAssesment();
	if(!item){
		return;
	}
	item.id=$(".id").val();
	$.ajax({
		url:"platform/buyer/supplier/saveEditSupplierAssesment",
		async:false,
		type:"post",
		data:item,
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg("数据修改成功");
				leftMenuClick(this,'platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml','buyer','18010209272828100705');
			}else{
				layer.msg(resultObj.msg);
				leftMenuClick(this,'platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml','buyer','18010209272828100705');
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后再试！",{icon:2});
		}
	})
}
$(".tableSmall tbody tr input").keyup(function(){
	var total=0.0; 
	$(".tableSmall tbody tr input").each(function(i){
		if(i<4){
			var num=$(this).val();
			if(num){
				total+=parseFloat(num);
			}
		}
	})	
	$("#total").html(total);
});