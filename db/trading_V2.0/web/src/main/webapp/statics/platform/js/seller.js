/**
 * Created by Administrator on 2017/5/5.
 */
//功能点1：分页
function paging(){
  function showPages(page, total) {
    var str='<a class="current">'+page+'</a>';
    for (var i=1;i<=3;i++){
      if (page - i > 1) {
        str='<a href="#">'+(page-i)+'</a> '+str;
      }
      if(page+i<total){
        str=str+' <a href="#">'+(page+i)+'</a>';
      }
    }
    if(page-4>1){
      str='... '+str;
    }
    if (page>1){
      str='<a href="#">&lt;上一页</a> <a href="#">1</a>'+ ' '+str;
    }
    if(page+4<total) {
      str=str+ ' ...';
    }
    if(page<total){
      str = str + ' <a href="#">' + total + '</a> <a href="#">下一页&gt;</a>';
    }
    str+=' 共'+total+'页,<span>300</span>个供应商&nbsp;&nbsp;转到 <input> 页&nbsp;&nbsp;<button>确定</button>';
    return str;
  }
  var total=110;
  for (var i=1;i<=total;i++) {
    var ret=showPages(10,total);
    console.log(ret);
    $('.pager').html(ret);
  }
}
paging();
//功能点2:切换采购商
$('.switch>span').click(function(){
  $('#side_pro').css('display','block');
});
//关闭窗口
$('.Pro_head>img').click(function(){
  $('#side_pro').css('display','none');
});
//功能点3:点击合并左侧导航
$('.lf_nav>div>h4').click(function(){
  $(this).next().slideToggle(300,'linear');
  $(this).find('span').toggleClass('fa-caret-up');
});