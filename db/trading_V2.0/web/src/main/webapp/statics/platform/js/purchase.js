//等待审批的商品页面
//功能点1:全选事件
function selAll(){
  var inputs=$('.order_list>p>span.chk_click input');
  function checkedSpan(){
    inputs.each(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('checked')
      }else{
        $(this).parent().removeClass('checked')
      }
    })
  }
  $('.batch input').click(function(){
    inputs.prop('checked',$(this).prop('checked'));
    var inputs_all=$('.batch input');
    if($(this).prop('checked')==true){
      inputs_all.each(function(){
        $(this).parent().addClass('checked')
      })
    }else{
      inputs_all.each(function(){
        $(this).parent().removeClass('checked')
      })
    }
    checkedSpan();
  });
  inputs.click(function(){
    if($(this).prop('checked')==true){
      $(this).parent().addClass('checked')
    }else{
      $(this).parent().removeClass('checked')
    }
    var r=$('.order_list>p>span.chk_click input:not(:checked)');
    if(r.length==0){
      $('.batch input').prop('checked',true).parent().addClass('checked');
    }else{
      $('.batch input').prop('checked',false).parent().removeClass('checked');
    }
  });
}
selAll();
//确认是否取消订
layui.use(['layer','form'], function(){
  var layer = layui.layer,form=layui.form;
  $('.cancel_order').click(function(){
    layer.confirm('您确定要取消订单吗?',{
      skin:'c_order',
      title:'提示'
    },function(index){
      layer.close(index);
      layer.msg('订单取消成功', {icon: 1});
    })
  });
  //手工录入订单页面
  $('.cancelOrder').click(function(){
    layer.confirm('您确定要取消订单吗?',{
      skin:'c_order',
      title:'提示'

    },function(index){
      layer.close(index);
      layer.msg('订单取消成功', {icon: 1});
    })
  });
  //手工录入订单审批
  $('#approvalOrder').click(function(){
      layer.open({
        type: 1,
        title: '审批',
        area: ['400px', 'auto'],
        skin: 'layui-layer-moon', //没有背景色
        closeBtn:2,
        btn:['确定','取消'],
        content: $('.manual_order')
      })
  });
});
//收货页面
function upload(){
  var upload=$('#upload');
  var nameContainer=$('#name');
  upload.onchange=function(){
    console.log(1);
    var name=[];
    for(var i=0;i<this.files.length;i++){
      name[i]= this.files[i].name;
      if(this.files[i].size>=307200){
        alert("文件"+this.files[i].name+"过大，不能超过300kb")
      }
    }
    nameContainer.innerHTML=name;
  }
}
upload();
//互通订单
$(".ystep1").loadStep({
  size: "large",
  color: "blue",
  steps: [{
    title: "提交订单"
  },{
    title: "内部审批"
  },{
    title: "供应商发货"
  },{
    title: "确认收货"
  }]
});
$(".ystep1").setStep(2);
//手工订单
$(".ystep2").loadStep({
  size: "large",
  color: "blue",
  steps: [{
    title: "提交订单"
  },{
    title: "内部审批"
  },{
    title: "采购入库"
  }]
});
$(".ystep2").setStep(2);
