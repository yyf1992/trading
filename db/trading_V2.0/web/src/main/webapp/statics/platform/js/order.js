/**
 * Created by Administrator on 2017/5/3.
 */
//等待供应商接单页面
//功能点1:批量取消订单全选事件
function selAll(){
  var inputs=$('.waiting_order>p>span.chk_click input');
  function checkedSpan(){
    inputs.each(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('checked')
      }else{
        $(this).parent().removeClass('checked')
      }
    })
  }
  $('.batch input').click(function(){
    inputs.prop('checked',$(this).prop('checked'));
    if($(this).prop('checked')==true){
      $(this).parent().addClass('checked')
    }else{
      $(this).parent().removeClass('checked')
    }
    checkedSpan();
  });
  inputs.click(function(){
    if($(this).prop('checked')==true){
      $(this).parent().addClass('checked')
    }else{
      $(this).parent().removeClass('checked')
    }
    var r=$('.waiting_order>p>span.chk_click input:not(:checked)');
    if(r.length==0){
      $('.batch input').prop('checked',true).parent().addClass('checked');
    }else{
      $('.batch input').prop('checked',false).parent().removeClass('checked');
    }
  });
}
selAll();
//功能点2:取消订单，弹出确认框
$('.waiting_order>table').on('click','a.cancel_order',function(e){
  e.preventDefault();
  $('.modal').css('display','block');
});
//关闭确认框
$('.modal_dialog>h2>b').click(function(){
  $('.modal').css('display','none');
});
$('.modal_dialog>div>button.cancel').click(function(){
  $('.modal').css('display','none');
});
//点击确定时
$('.modal_dialog>div>button.confirm').click(function(){
  $('.modal').css('display','none');
});
