//点击添加附件
	function fileUpload(obj){
		var formData = new FormData();
		var id = $(obj).attr("id");
		var file = document.getElementById(id).files[0];
		formData.append("file",file);
		var url = "platform/file/uploadRegAttachment";
		
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var b = xhr.responseText;
				var re = eval('(' + b + ')');
				var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
                if (resultObj.result == "success") {
                	//获得文件路径
                	var url = resultObj.filePath;
                	//获得文件格式
                	/* var fileExtensionName = resultObj.fileExtensionName;
                	//根据文件格式获得文件显示样式
                	var style = getDisplayStyle(fileExtensionName);
                	//获取文件名称
                	var fileName = resultObj.fileName;
                	//获取文件大小
                	var fileSize = resultObj.fileSize; */
                    //加载
                    $.ajax({
                    	url:"platform/file/loadAttachment",
                    	data:{
                    		"url":url
                    	},
                    	success:function(data){
                    		$("#"+id+"Div").append(data);
                    	},
                    	error:function(){
                    		layer.msg("获取数据失败，请稍后重试！",{icon:2});
                    	}
                    });
                }
                if (resultObj.result == "fail") {
                	layer.msg(resultObj.msg,{icon:2});
                	return;
                }
			}
		}
		xhr.open("post", url, true);
		xhr.send(formData);
		//解决上传两次一样的文件第二次没反映问题
		$('input[type=file]').wrap('<form>').closest('form').get(0).reset();
	}