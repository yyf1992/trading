/**
 * Created by wxx on 2017/6/30.
 */
$(function(){
    $('#btn').click(function(e) {
        e.preventDefault();
        $('.login1').css('background',"url('"+basePath+"/statics/platform/images/top1.jpg') no-repeat center top");
        $('.login').addClass('move');
        $('.login1').addClass('top');
    });
});
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    // 兼容FF和IE和Opera
    var theEvent = e || window.event;
    var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        toLogin();
        return false;
    }
    return true;
}
function toLogin() {
    if ($("#loginName").val() == '') {
        layer.msg("用户名不能为空！",{icon:2});
        return;
    }
    if ($("#password").val() == '') {
        layer.msg("密码不能为空！",{icon:2});
        return;
    }
    layer.load();
    var url = basePath+"platform/loginPlatform";
    $.ajax({
        type : "post",
        url : url,
        data : $("#login-form").serialize(),
        success : function(data) {
        	var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if (resultObj.code==0) {
				window.location.href = basePath+'platform/loadIndexHtml';
				return;
			} else {
				layer.closeAll('loading');
				layer.msg(resultObj.msg, {icon : 2});
				return;
			}
        },
        error : function() {
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
