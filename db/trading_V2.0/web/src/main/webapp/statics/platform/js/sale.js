$('.lf_nav>div>h4').click(function(){
  $(this).next().slideToggle(300,'linear');
  $(this).find('span').toggleClass('fa-caret-up');
});
//功能点16:切换供应商窗口
$('.switch>span').click(function(){
  $('#side_pro').toggleClass('show');
});
//关闭窗口
$('.Pro_head>img').click(function(){
  $('#side_pro').toggleClass('show');
});
//功能点4:鼠标悬停在列表上的变化
$('.purchase_list').on('mouseover','tr',function(){
  $(this).find('b').css('background',"url('images/spirit.png') no-repeat -67px -13px");
}).on('mouseout','tr',function(){
  $(this).find('b').css('background',"url('images/spirit.png') no-repeat -159px -13px")
});
//功能点5:点击输入框，弹出下拉商品
$('.purchase_list').on('focus','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','block');
  $(this).siblings('ul').menu();
}).on('blur','td:nth-child(2) input',function(){
  $(this).siblings('ul').css('display','none');
});
//表格增加和删除一行函数
function addRow(str,tbl){
  var addTr=document.createElement('tr');
  addTr.innerHTML=str;
  tbl.after(addTr);
}
function delRow(tr){
  $(tr).parent().parent().remove();
}
//功能点15:点击增加项，表格增加一行
$('.manual+button').click(function(){
  var str=`<td><b></b></td>
                <td>
                  <input type="text" placeholder="输入商品名称/货号/条形码/规格等关键字">
                  <button class="layui-btn layui-btn-mini"><i class="layui-icon">&#xe61f;</i> 新增商品</button>
                  <ul class="menu"">
                    <li><div>护具</div>
                      <ul>
                        <li><div>腿部护具</div></li>
                        <li><div>膝盖护具</div></li>
                        <li><div>腕部护具</div></li>
                      </ul>
                    </li>
                    <li><div>腿部护具 膝盖护具 258585556</div>
                      <ul>
                        <li><div>腿部护具</div></li>
                        <li><div>膝盖护具</div></li>
                        <li><div>腕部护具</div></li>
                      </ul>
                    </li>
                    <li><div>护具 套装 成人</div></li>
                    <li><div>护具 套装 成人</div></li>
                    <li><div>护具 套装 成人</div></li>
                  </ul>
                <td>
                  <select name="">
                    <option value="">请选择</option>
                    <option value="">个</option>
                    <option value="">箱</option>
                    <option value="">套</option>
                  </select>
                </td>
                <td><input type="number" value="0" min="0" class="unit_price"></td>
                <td><input type="number" value="0" min="0" class="number"></td>
                <td><input type="number" value="0" min="0" class="discount"></td>
                <td class="subtotal">0.00</td>
                <td>
                  <select name="">
                    <option value="">请选择</option>
                    <option value="">大仓</option>
                    <option value="">小仓</option>
                    <option value="">质检仓</option>
                  </select>
                </td>
                <td>
                  <select name="" class="isInvoice">
                    <option value="">否</option>
                    <option value="">是</option>
                  </select>
                </td>
                <td><textarea name="" placeholder="备注内容"></textarea></td>`;
  var tbl=$('.manual tr:eq(-2)');
  addRow(str,tbl);
});
//删除表格某一行
$('.purchase_list').on('click','b',function(){
  delRow(this);
});
layui.use(['layer','form'], function(){
  var layer = layui.layer;
  //功能点11:弹出增加商品框
  $('.purchase_list').on('click','button',function(){
    layer.open({
      type: 1,
      title: '新增商品',
      area: ['700px', 'auto'],
      skin: 'add',
      closeBtn:2,
      btn:['保存'],
      content: $('.new_goods'),
      yes:function(index){
        layer.close(index);
        layer.msg('保存成功',{icon:1})
      }
    });
  });
});
//功能点12:新增商品弹出框表格增加、删除一行
$('.new_goods>button').click(function(){
  var str=`<td><b></b></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>
      <td><input type="text"></td>`;
  var tbl=$('.new_goods>table tr:eq(-2)');
  addRow(str,tbl);
});
$('.new_goods>table').on('click','b',function(){
  delRow(this);
});
//功能点14：数量求和
function calSum(){
  var sum=0;
  var total=$('.manual .num');
  $('.manual input.number').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum);
}
//单击时求和
$('.manual').on('click','input.number',function(){
  calSum();
});
//失去焦点时求和
$('.manual').on('blur','input.number',function(){
  calSum();
});
//删除一行时求和
$('.manual').on('click','b',function(){
  calSum();
});
//优惠金额求和
function discount(){
  var sum=0;
  var total=$('.manual .discount_sum');
  $('.manual input.discount').each(function(){
    var p=$(this).val();
    sum+=parseInt(p);
  });
  total.html(sum.toFixed(2));
}
//单击时求和
$('.manual').on('click','input.discount',function(){
  discount();
});
//失去焦点时求和
$('.manual').on('blur','input.discount',function(){
  discount();
});
//删除一行时求和
$('.manual').on('click','b',function(){
  discount();
});
//采购金额小计
//求总金额
function calTotal(){
  var sum=0;
  $('.manual .subtotal').each(function(i,val){
    var p=$(this).html();
    sum+=parseFloat(p);
  });
  $('.manual .total').html(sum.toFixed(2));
}
//单击单价时求和
$('.manual').on('click','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('click','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var discount=$(this).parent().siblings().children('.discount').val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//失去焦点时求和
$('.manual').on('blur','input.unit_price',function(){
  var price=$(this).val();
  var count=$(this).parent().siblings().children('.number').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
$('.manual').on('blur','input.number',function(){
  var count=$(this).val();
  var price=$(this).parent().siblings().children('.unit_price').val();
  var total=$(this).parent().siblings('.subtotal');
  var discount=$(this).parent().siblings().children('.discount').val();
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//点击优惠金额时求和
$('.manual').on('click','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//失去焦点时
$('.manual').on('blur','input.discount',function(){
  var price=$(this).parent().siblings().children('.unit_price').val();
  var count=$(this).parent().siblings().children('.number').val();
  var discount=$(this).val();
  var total=$(this).parent().siblings('.subtotal');
  var amount=parseFloat(price)*count-parseFloat(discount);
  total.html(amount.toFixed(2));
  calTotal();
});
//日期组件
//$("#datepicker").datepicker({
//  dateFormat:"yy-mm-dd",
//  showAnim:'slideDown'
//});
//上传采购凭证
layui.use(['upload','form'], function(){
  layui.upload({
    url: '' //上传接口
    ,success: function(res){ //上传成功后的回调
      console.log(res)
    }
  });
});
//删除采购凭证
$('.voucherImg').on('click','b',function(){
  $(this).parent().remove();
});