//功能点1:全选事件
function selAll(){
  var inputs=$('.order_list>p>span.chk_click input');
  function checkedSpan(){
    inputs.each(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('checked')
      }else{
        $(this).parent().removeClass('checked')
      }
    })
  }
  $('.batch input').click(function(){
    inputs.prop('checked',$(this).prop('checked'));
    var inputs_all=$('.batch input');
    if($(this).prop('checked')==true){
      inputs_all.each(function(){
        $(this).parent().addClass('checked')
      })
    }else{
      inputs_all.each(function(){
        $(this).parent().removeClass('checked')
      })
    }
    checkedSpan();
  });
  inputs.click(function(){
    if($(this).prop('checked')==true){
      $(this).parent().addClass('checked')
    }else{
      $(this).parent().removeClass('checked')
    }
    var r=$('.order_list>p>span.chk_click input:not(:checked)');
    if(r.length==0){
      $('.batch input').prop('checked',true).parent().addClass('checked');
    }else{
      $('.batch input').prop('checked',false).parent().removeClass('checked');
    }
  });
}
selAll();
//仓库管理页面切换页签
layui.use(['layer','element','form'], function(){
  var layer = layui.layer;
  $('.ware_detail').click(function(){
    layer.open({
      type: 1,
      title: '商品库存明细',
      area: ['700px', '400px'],
      skin: 'ware layui-layer-moon', //没有背景色
      closeBtn:2,
      btn:['确定','取消'],
      content: $('.view_detail')
    })
  });
  //商品盘点页面
//功能点2:查看审批流程
  $('.my_approval td:nth-child(11) div.process').hover(
    function(){
      $(this).children('.opinion').toggle();
    }
  );
  //批量提交
  function submission(){
    var inputs=$('.inventory td>span.chk_click input');
    function checkedSpan(){
      inputs.each(function(){
        if($(this).prop('checked')==true){
          $(this).parent().addClass('checked')
        }else{
          $(this).parent().removeClass('checked')
        }
      })
    }
    $('.batch input').click(function(){
      inputs.prop('checked',$(this).prop('checked'));
      var inputs_all=$('.inventory .batch input');
      if($(this).prop('checked')==true){
        inputs_all.each(function(){
          $(this).parent().addClass('checked')
        })
      }else{
        inputs_all.each(function(){
          $(this).parent().removeClass('checked')
        })
      }
      checkedSpan();
    });
    inputs.click(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('checked')
      }else{
        $(this).parent().removeClass('checked')
      }
      var r=$('.inventory td span.chk_click input:not(:checked)');
      if(r.length==0){
        $('.batch input').prop('checked',true).parent().addClass('checked');
      }else{
        $('.batch input').prop('checked',false).parent().removeClass('checked');
      }
    });
  }
  submission();
  //商品管理页面
  //功能点2:查看审批流程
  $('.management td:nth-child(9) div.process').hover(
    function(){
      $(this).children('.opinion').toggle();
    }
  );
//库存管理页面的审批
  $('.my_approval td:last-child button').click(function(){
    layer.open({
      type: 1,
      title: '库存盘点记录审批',
      area: ['420px', 'auto'],
      skin: 'Inventory layui-layer-moon', //没有背景色
      closeBtn:2,
      btn:['确定','驳回'],
      content: $('.manual_order')
    })
  });
  //商品管理页面的审批
  $('.management td:last-child button').click(function(){
    layer.open({
      type: 1,
      title: '商品管理审批',
      area: ['420px', 'auto'],
      skin: 'Inventory layui-layer-moon', //没有背景色
      closeBtn:2,
      btn:['确定','驳回'],
      content: $('.manual_order')
    })
  });
});
//切换页签
$("#my_tabs>ul").on("click","li",function(){
  $(this).addClass("current_tab")
    .siblings().removeClass("current_tab");
  //内容切换
  var $divs=$("#tab_content>div");
  $divs.removeClass("show");
  var i=$(this).index("#my_tabs>ul>li");
  $divs.eq(i).addClass("show");
});
//功能点9:分页
function paging(){
  function showPages(page, total) {
    var str='<a class="current">'+page+'</a>';
    for (var i=1;i<=3;i++){
      if (page - i > 1) {
        str='<a href="#">'+(page-i)+'</a> '+str;
      }
      if(page+i<total){
        str=str+' <a href="#">'+(page+i)+'</a>';
      }
    }
    if(page-4>1){
      str='... '+str;
    }
    if (page>1){
      str='<a href="#">&lt;&lt;</a> <a href="#">1</a>'+ ' '+str;
    }
    if(page+4<total) {
      str=str+ ' ...';
    }
    if(page<total){
      str = str + ' <a href="#">' + total + '</a> <a href="#">&gt;&gt;</a>';
    }
    str+=' 共'+total+'页,转到 <input> 页&nbsp;&nbsp;<button>确定</button>';
    return str;
  }
  var total=110;
  for (var i=1;i<=total;i++) {
    var ret=showPages(10,total);
    $('.pager').html(ret);
  }
}
paging();
