//我已卖出的商品页面
//功能点9:分页
function paging(){
  function showPages(page, total) {
    var str='<a class="current">'+page+'</a>';
    for (var i=1;i<=3;i++){
      if (page - i > 1) {
        str='<a href="#">'+(page-i)+'</a> '+str;
      }
      if(page+i<total){
        str=str+' <a href="#">'+(page+i)+'</a>';
      }
    }
    if(page-4>1){
      str='... '+str;
    }
    if (page>1){
      str='<a href="#">&lt;&lt;</a> <a href="#">1</a>'+ ' '+str;
    }
    if(page+4<total) {
      str=str+ ' ...';
    }
    if(page<total){
      str = str + ' <a href="#">' + total + '</a> <a href="#">&gt;&gt;</a>';
    }
    str+=' 共'+total+'页&nbsp;&nbsp;转到 <input> 页&nbsp;&nbsp;<button>确定</button>';
    return str;
  }
  var total=110;
  for (var i=1;i<=total;i++) {
    var ret=showPages(10,total);
    $('.pager').html(ret);
  }
}
paging();
//功能点2:查看物流信息
$('.inShipment>table td:nth-child(2) div.see_logistics').hover(
  function(){
    $(this).children('.logistics').toggle();
  }
);
//我已卖出的商品页面查看物流信息
$('.examination>table td:nth-child(2) div.see_logistics').hover(
  function(){
    $(this).children('.logistics').toggle();
  }
);
//功能点4:发送地图位置
//弹出地图框
$('.map_position').click(function () {
  $('.map_bg').fadeIn();
});
$('.map_content>b').click(function(){
  $('.map_bg').fadeOut();
});
//功能点5:打印收货回单
$('.delivery').click(function(){
  $('.print_bg').fadeIn();
});
//取消查看
$('.co_print_btn').click(function(){
  $('.print_bg').fadeOut();
});
//关闭收货回单
$('.print_close_i').click(function(){
  $('.print_bg').fadeOut();
});

//功能点8:回单证明大图
var preview={
  LIWIDTH:108,//保存每个li的宽
  $ul:null,//保存小图片列表的ul
  moved:0,//保存左移过的li
  init(){//初始化功能
    this.$ul=$(".view>ul");//查找ul
    $(".view>a").click(function(e){//为两个按钮绑定单击事件
      if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
        if($(e.target).is(".forward")){//如果是向前按钮
          this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
          this.moved++;//移动个数加1
        }
        else{//如果是向后按钮
          this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
          this.moved--;//移动个数减1
        }
        this.checkA();//每次移动完后，调用该方法
      }
    }.bind(this));
    //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
    this.$ul.on("mouseover","li>img",function(){
      var src=$(this).attr("src");//获得当前img的src
      //var i=src.lastIndexOf(".");//找到.的位置
      //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
      $(".big_img>img").attr("src",src);//设置中图片的src
    });
  },
  checkA(){//检查a的状态
    if(this.moved==0){//如果没有移动
      $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
    }
    else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
      $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
    }
    else{//否则，都启用
      $("[class^=backward]").attr("class","backward");
      $("[class^=forward]").attr("class","forward");
    }
  }
};
preview.init();
//查看收获回单
$('.check_receipt').click(function(e){
  e.preventDefault();
  $('.receipt_bg').fadeIn();
});
//关闭收获回单
$('.receipt_content>b').click(function(){
  $('.receipt_bg').fadeOut();
});
layui.use(['layer','form','laydate'], function(){
  var layer = layui.layer,form=layui.form,laydate=layui.laydate;
  //日历组件
  var start = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      end.min = datas; //开始日选好后，重置结束日的最小日期
      end.start = datas //将结束日的初始值设定为开始日
    }
  };
  var end = {
    min: laydate.now()
    ,max: '2099-06-16 23:59:59'
    ,istoday: false
    ,choose: function(datas){
      start.max = datas; //结束日选好后，重置开始日的最大日期
    }
  };
  document.getElementById('LAY_demorange_s').onclick = function(){
    start.elem = this;
    laydate(start);
  };
  document.getElementById('LAY_demorange_e').onclick = function(){
    end.elem = this;
    laydate(end);
  };
  //删除收货回单
  $('.del_receipt>button').click(function(){
    layer.confirm('您确定要将所选中的回单图片删除吗？', {
      icon:0,
      title:'提示',
      btn: ['确定','取消'] //按钮
    }, function(){
      var str=$('.big_img>img').attr('src');
      var li=$('#icon_list li img[src="'+str+'"]').parent();
      li.remove();
      layer.msg('删除成功',{time:800});
    });
  });
  //卖家-去发货页面，点击发货后
  $('#shipment').click(function(){
    layer.confirm('请核对发货数量,确认无误!', {
      icon:3,
      title:'确定发货',
      btn: ['确定','取消']
    }, function(){
      layer.msg('发货成功',{icon:1});
      location.href="../卖家-发货订单管理.html";
    });
  });
  //弹出给司机发送短信页面
  $('.sms_receipt').click(function(){
    layer.open({
      type: 1,
      title: '给司机发送拍照回单短信',
      area: ['400px', 'auto'],
      skin: 'sms',
      closeBtn:2,
      content: $('.short_message'),
      btn:['确定','取消'],
      yes: function(index){
        var driver=$('.short_message input[name="driver"]').val();
        var tel = $('.short_message input[name="driver_tel"]').val();
        var re_tel = $('.short_message input[name="driver_reTel"]').val();
        if(driver==''){
          layer.msg('请输入驾驶员姓名', {icon: 0});
        }else{
          if (tel == re_tel) {
            if(tel==""){
              layer.msg('请输入驾驶员手机', {icon: 0});
            }else{
              layer.close(index);
              layer.msg('发送成功！', {icon: 1});
            }
          }else {
            layer.msg('两次输入号码不一致', {icon: 0});
          }
        }
      }
    })
  });
});
//订单详情的页面
//功能点7:弹出订单预览
$('.preview').click(function(e){
  e.preventDefault();
  $('.print_bg').css('display','block');
});
//关闭预览
$('.print_close_i').click(function(){
  $('.print_bg').fadeOut();
});
$('.co_print_btn').click(function(){
  $('.print_bg').fadeOut();
});
//处理订单
$('.reject_order').click(function(){
  layui.use(['layer','form'], function(){
    var layer=layui.layer;
    layer.open({
      type: 1,
      title: '处理订单',
      area: ['420px', 'auto'],
      skin: 'bold ',
      closeBtn:1,
      btn:['确定','取消'],
      content: $('.reject_reason')
    });
  });
});
