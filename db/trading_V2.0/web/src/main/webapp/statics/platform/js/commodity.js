//批量提交
function submission(){
  var inputs=$('.inventory td>span.chk_click input');
  function checkedSpan(){
    inputs.each(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('checked')
      }else{
        $(this).parent().removeClass('checked')
      }
    })
  }
  $('.batch input').click(function(){
    inputs.prop('checked',$(this).prop('checked'));
    var inputs_all=$('.inventory .batch input');
    if($(this).prop('checked')==true){
      inputs_all.each(function(){
        $(this).parent().addClass('checked')
      })
    }else{
      inputs_all.each(function(){
        $(this).parent().removeClass('checked')
      })
    }
    checkedSpan();
  });
  inputs.click(function(){
    if($(this).prop('checked')==true){
      $(this).parent().addClass('checked')
    }else{
      $(this).parent().removeClass('checked')
    }
    var r=$('.inventory td span.chk_click input:not(:checked)');
    if(r.length==0){
      $('.batch input').prop('checked',true).parent().addClass('checked');
    }else{
      $('.batch input').prop('checked',false).parent().removeClass('checked');
    }
  });
}
submission();
//仓库管理页面切换页签
layui.use(['layer','element','form'], function(){
  var layer = layui.layer;
  //新增单位
  $('.edit_content .unit_add').click(function(){
    layer.open({
      type: 1,
      skin:'unit_new',
      title: '计量单位管理',
      area: ['420px','auto'],
      closeBtn:1,
      btn:['保存','关闭'],
      content: $('.unit'),
      yes:function(index){
        layer.close(index);
        layer.msg('保存成功', {icon: 1,time:800,area:['120px','70px']});
      }
    })
  });
  //添加一项单位
  $('.unit_edit').click(function(){
    var str=$(this).parent().prev().html();
    $('#unit_name').val(str);
  });
  //删除一项单位
  $('.unit_delete').click(function(){
    $(this).parent().parent().remove();
  });
  //保存单位
  //$('.unit_save').click(function(){
  //  layer.msg('保存成功')
  //});
  //重置单位
  $('#reset').click(function(){
    $('#unit_name').val('');
  });
//查看审批流程
  $('.interwork_list div.commodity_approval').hover(
    function(){
      $(this).children('.commodity_opinion').toggle();
    }
  );
//审批
  $('.commodity_examine').click(function(){
    layer.open({
      type: 1,
      title: '商品价格变更审批',
      area: ['420px', 'auto'],
      skin:'change',
      closeBtn:2,
      btn:['确定','关闭'],
      content: $('.price_change')
    })
  });
//修改商品弹出框
  $('.commodity_add .table_edit').click(function(){
    layer.open({
      type: 1,
      title: '选择商品',
      area: ['710px', 'auto'],
      skin:'pop',
      btn:['确定','关闭'],
      content: $('.commodity_sel')
    });
  });
//删除商品弹出框
  $('.commodity_add .table_del').click(function(){
    layer.confirm('确定删除该商品吗？', {
      icon:3,
      title:'提醒',
      closeBtn:2,
      btn: ['确定','取消']
    }, function(){
      layer.msg('删除成功',{icon:1,time:500});
    });
  });
//选择商品弹出框
  $('.commodity_add tbody td:first-child').click(function(){
    layer.open({
      type: 1,
      title: '选择商品',
      area: ['710px', 'auto'],
      skin:'pop',
      btn:['确定','关闭'],
      content: $('.commodity_sel')
    });
  });
//点击新增按钮时，弹出选择框
//   $('.btn_new').click(function(e){
//     e.preventDefault();
//     layer.open({
//       type: 1,
//       title: '选择商品',
//       area: ['710px', 'auto'],
//       skin:'pop',
//       btn:['确定','关闭'],
//       content: $('.commodity_sel')
//     });
//   });
//选择供应商弹出框
  $('.commodity_add tbody td:nth-child(3)').click(function(){
    console.log(11);
    layer.open({
      type: 1,
      title: '选择供应商',
      area: ['710px', 'auto'],
      skin:'pop',
      btn:['确定','关闭'],
      content: $('.supplier_sel')
    });
  });
});
//选择商品事件
function comm_sellAll(){
  var inputs=$('.comm_list tbody input');
  // function checkedSpan(flag){
  //   inputs.each(function(){
  //     if(flag){
  //       $(this).parent().addClass('chk');
  //       $(this).prop('checked',true);
  //     }else{
  //       $(this).parent().removeClass('chk');
  //         $(this).prop('checked',false);
  //     }
  //   });
  //     var r=$('.comm_list tbody input:not(:checked)');
  //     if(r.length==0){
  //         $('.comm_list thead input').prop('checked',true).parent().addClass('chk');
  //     }else{
  //         $('.comm_list thead input').prop('checked',false).parent().removeClass('chk');
  //     }
  // }
  // $('.comm_list thead input').click(function(){
  //   
  //     if($(this).prop('checked')==true){
  //         $(this).parent().addClass('chk');
  //     }else {
  //         $(this).parent().removeClass('chk');
  //     }
  //     inputs.prop('checked',$(this).prop('checked',$(this).prop('checked')));
  //     checkedSpan($(this).prop('checked'));
  // });
  inputs.click(function(){
    if($(this).prop('checked')==true){
        $(this).parent().addClass('chk');
        callBackProducts=new Array();
        var p = new Object();
        console.log($(this));
    }else{
      $(this).parent().removeClass('chk');
    }
    var r=$('.comm_list tbody input:not(:checked)');
    if(r.length==0){
      $('.comm_list thead input').prop('checked',true).parent().addClass('chk');
    }else{
      $('.comm_list thead input').prop('checked',false).parent().removeClass('chk');
    }
  });
}
// comm_sellAll();
//选择供应商事件
function supp_sellAll(){
  var inputs=$('.supp_list>table span.comm_sel input');
  function checkedSpan(){
    inputs.each(function(){
      if($(this).prop('checked')==true){
        $(this).parent().addClass('chk')
      }else{
        $(this).parent().removeClass('chk')
      }
    })
  }
  $('.supp_list thead input').click(function(){
    console.log(11);
    inputs.prop('checked',$(this).prop('checked'));
    checkedSpan();
  });
  inputs.click(function(){
    if($(this).prop('checked')==true){
      $(this).parent().addClass('chk')
    }else{
      $(this).parent().removeClass('chk')
    }
    var r=$('.supp_list tbody input:not(:checked)');
    if(r.length==0){
      $('.supp_list thead input').prop('checked',true).parent().addClass('chk');
    }else{
      $('.supp_list thead input').prop('checked',false).parent().removeClass('chk');
    }
  });
}
supp_sellAll();

//选择商品弹框
$('.layui-form input.product').click(function(){
	$.ajax({
		url:"platform/baseDate/productCompose/loadProductHtml",
		type:"get",
		data:{
			"productType":"0",
//			"page.pageSize":"10",
			"page.divId":"addProduct",
			"id":$(this).parents().find("#id").val()
		},
		async:false,
		success:function(data){
			$("#addProduct").empty();
            var str = data.toString();
            $("#addProduct").html(str);
			  layer.open({
				    type: 1,
				    title:'选择成品',
				    area: ['1200px', 'auto'],
				    skin:'popBarcode btnCenter',
				    closeBtn:0,
				    content: $("#addProduct"),
				    btn:['关闭'],
				    yes:function(index){
				    	layer.close(index);
				    }
				  });
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
});

//为选择的商品赋值
function chooseThis(){
	var checkOb=$("input[name='product']:checked");
	var td=checkOb.parent().parent().find("td");
		$("#productCode").val(td.eq(2).text());
		$("#productName").val(td.eq(3).text());
    	$("#skuCode").val(td.eq(4).text());
    	$("#skuName").val(td.eq(5).text());
    	$("#barcode").val(td.eq(6).text());
    	$("#unitName").val(td.eq(7).html());
    	$("#id").val(checkOb.val());
    	$(".layui-form").find("ul").addClass("checked");
		layer.closeAll();
}

//选择原材料弹框
$('.materialNew').click(function(){
	
	//拿到已选择的原材料的barcode值
	var barcodes="";
	$("#skuTable tbody").find("tr").each(function(){
		barcodes+=$(this).children("td").eq(4).html()+",";
	});
	
	$.ajax({
		url:"platform/baseDate/productCompose/loadProductHtml",
		type:"get",
		data:{
			"productType":"1",
//			"page.pageSize":"20",
			"page.divId":"addRawMaterial",
			"barcodes":barcodes
		},
		async:false,
		success:function(data){
			$("#addRawMaterial").empty();
            var str = data.toString();
            $("#addRawMaterial").html(str);
			  layer.open({
				    type: 1,
				    title:'选择物料',
				    area: ['1200px', ''],
				    skin:'popBarcode btnCenter',
				    closeBtn:0,
				    btn:['批量添加','关闭'],
				    content:  $("#addRawMaterial"),
				    yes:function(index){
				    	$(".table_pure tbody input:checkbox[name='checkOne']:checked").each(function(i){
				    		var productCode=$(this).parents("tr").children('td').eq(2).text();
				    		var productName=$(this).parents("tr").children('td').eq(3).text();
				    		var skuCode=$(this).parents("tr").children('td').eq(4).text();
				    		var skuName=$(this).parents("tr").children('td').eq(5).text();
				    		var barcode=$(this).parents("tr").children('td').eq(6).text();
				    		var unitName=$(this).parents("tr").children('td').eq(7).text();
				    		var tr=$('<tr>');
				    		$("<td>",{name:"productCode",html:productCode}).appendTo(tr);
				    		$("<td>",{name:"productName",html:productName}).appendTo(tr);
				    		$("<td>",{name:"skuCode",html:skuCode}).appendTo(tr);
				    		$("<td>",{name:"skuName",html:skuName}).appendTo(tr);
				    		$("<td>",{name:"barcode",html:barcode}).appendTo(tr);
				    		$("<td>",{name:"unitName",html:unitName}).appendTo(tr);
				    		$("<td><input type='number' placeholder='0'>").appendTo(tr);
				    		$("<td><input type='number' placeholder='0'>").appendTo(tr);
				    		$("<td><input type='number' placeholder='0'>").appendTo(tr);
				    		$("<td><input type='number' placeholder='0'>").appendTo(tr);
				    		$("<td><input type='number' placeholder='0'>").appendTo(tr);
				    		$("<td><input type='number' name='composeNum' placeholder='0'>").appendTo(tr);
				    		$("<td><span class='table_del'><b></b>删除</span></td>").appendTo(tr);
				    		$("#skuTable").append(tr);
				    	});
				    	$("input[name='composeNum']").bind("change",function(){
				    		validateInput($(this));
				    	}).bind("keyup",function(){
				    		validateInput($(this));
				    	});
				    	layer.close(index);
				    },
				  });
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
});

function validateInput(obj){
	if(!objValidate(obj,2)){
		layer.msg("配置数量必须为整数！",{icon:2});
//		$(obj).val(0);
		return;
	}else{
		$(obj).parent().find("span").remove();
		$(obj).parent().removeClass("error");
	}
}

//原材料全选
function metaAll(obj){
	var spanObj = $(obj).parent();
	var tbodyObj = $(obj).parents("table").find("tbody");
	if($(spanObj).hasClass("checked")){
		//选中时点击，设置成未选中
		$(spanObj).removeClass("checked");
		$(obj).prop("checked",false);
		$(tbodyObj).find('input:checkbox').prop("checked",false);
		$(tbodyObj).find('span.barcodeAdd').removeClass("checked");
	}else{
		$(spanObj).addClass("checked");
		$(obj).prop("checked",true);
		$(tbodyObj).find('input:checkbox').prop("checked",true);
		$(tbodyObj).find('span.barcodeAdd').addClass("checked");
	}
}

//查询  成品/原材料  弹框
function selectProduct(obj){
	$.ajax({
		url:"platform/baseDate/productCompose/loadProductHtml",
		type:"get",
		data:$(obj).parents().find("#commPopFrom").serialize(),
		async:false,
		success:function(data){
			var productType=$(obj).parent().find("#productType").val();
			if(productType==0){
				$(".commPop").html(data);
			}else{
				$(".materialpop").html(data);
			}
			
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}


//删除原材料
$('.materialList').on('click','.table_del',function(){
  delRow(this);
});


function submit(obj){
	//判断是否选择商品
	var isChoose=$(".layui-form").find("ul").hasClass("checked");
	if(!isChoose){
		layer.msg("请选择要配置的商品！", {icon : 2});
		return;
	}

	var tr=$("#skuTable tbody").find("tr");
	//判断是否选择原材料
	if(!tr.length>0){
		layer.msg("请选择至少一种原材料！", {icon : 2});
		return;
	}
	
	var adjust =true;
	var repeat =true;
	var repeatStr = "";
	var composeStr = new Array();
	$(tr).each(function(i){
		//验证条形码是否为空
		var barcodeObj=$(this).children('td').eq(4);
		var barcode=barcodeObj.text();
		if(barcode==""){
			adjust=false;
			var span = barcodeObj.find("span");
			if(span.length == 0){
				span = $("<span></span>");
				barcodeObj.append(span);
			}
			span.html("未配置!"); 
			barcodeObj.addClass("error");
		} 
		
		var productCodeObj=$(this).children('td').eq(0);
		var productCode=productCodeObj.text();
		var NameObj=$(this).children('td').eq(1);
		var productName=NameObj.text();
		var skuObjCode=$(this).children('td').eq(2);
		var skuCode=skuObjCode.text();
		var skuNameObj=$(this).children('td').eq(3);
		var skuName=skuNameObj.text();
		var unitObj=$(this).children('td').eq(5);
		var unitName=unitObj.text();
		debugger;
		//验证配置数量是否为空
		var composeNumObj=$(this).children('td').eq(11).find("input");
		var composeNum=composeNumObj.val();
		if(composeNum==""){
			adjust=false;
			var span = composeNumObj.parent().find("span");
			if(span.length == 0){
				span = $("<span></span>");
				composeNumObj.parent().append(span);
			}
			span.html("未配置!"); 
			composeNumObj.parent().addClass("error");
		}else if(composeNum==0){
			adjust=false;
			var span = composeNumObj.parent().find("span");
			if(span.length == 0){
				span = $("<span style='width: 50px;'></span>");
				composeNumObj.parent().append(span);
			}
			span.html("未配置!"); 
			composeNumObj.parent().addClass("error");
		}else if(composeNum<0){
			adjust=false;
			var span = composeNumObj.parent().find("span");
			if(span.length == 0){
				span = $("<span style='width: 50px;'></span>");
				composeNumObj.parent().append(span);
			}
//			span.html("未配置!"); 
			layer.msg("配置数量必须为整数！",{icon:2});
			composeNumObj.parent().addClass("error");
		}
		var o = new Object();
		o.skuBarcode = barcode;
        o.productCode = productCode;       
        o.productName = productName;
        o.skuCode = skuCode;
        o.skuName = skuName;
        o.unitName = unitName;
        o.composeNum = composeNum;
        composeStr.push(o);
        
//		composeStr+=barcode+";"+productCode+";"+productName+";"+skuCode+";"+skuName+";"+unitName+";"+composeNum+"@";		
//		var barcodes=barcode+"@";
		//判断是否有重复数据
//		if(repeatStr.indexOf(barcodes)>-1){
//			//有重复数据
//			repeat = false;
//			$(this).addClass("error");
//		}else{
//			repeatStr += barcodes;
//		}
			
	});
	
	//保存物料配置
	if(adjust){
		if(repeat){
			//判断是否是修改物料配置
			var ischange=$('.layui-form input.product').attr("disabled");
			if($("#id").attr("class")==''){
				var url = "platform/baseDate/productCompose/saveCompose";
			}else{
				var url = "platform/baseDate/productCompose/saveUpdateCompose?id="+$("#id").attr("class");
			}
				var remark =$("#remark").val();
				$.ajax({
					url:url,
					data : {
						"productId":$("#id").val(),
						"composeStr" : JSON.stringify(composeStr),
						"remark"   :remark,
						"ischange" :ischange,
						"menuName":"17090515054792197585"
					},
					async:false,
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.msg(resultObj.msg,{icon:1});
							saveSucess();
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error:function(){
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
					
				});
		}
	}
}

//保存成功
function saveSucess(){
	/*$.ajax({
		url:"platform/baseDate/productCompose/success",
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});*/
//	layer.msg("物料配置设置成功！",{icon:1});
	leftMenuClick(this,'platform/baseDate/buyproductskubom/loadBomListHtml','buyer');
}
//显示物料配置
function showMaterial(id){
	$.ajax({
		url:"platform/baseDate/buyproductskubom/loadMaterialHtml",
		data:{"id":id},
		async:false,
		type:'get',
		success:function(data){
		    layer.open({
		        type: 1,
		        /*skin:'unit_new',*/
		        title: '商品的原材料配置清单',
		        area: ['1200px','auto'],
		        closeBtn:1,
		        btn:['关闭'],
		        content: data
		      })
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//修改物料配置
function changeCompose(id){
	$.ajax({
		url:"platform/baseDate/buyproductskubom/changeCompose",
		data:{"id":id},
		type:'get',
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}