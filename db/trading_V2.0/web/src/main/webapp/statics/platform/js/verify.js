function getHtmlStr(){
	var htmlStr = "<div id='importExcelDiv'>"
		+"<div class='layui-inline' style='padding:10px;'>"
		+"	<div class='layui-input-inline'>"
		+"      <textarea rows='5' cols='40' id='verifyOpinion'></textarea>"
		+"	</div>"
		+"	<input type='file' id='verifyFile' onchange='fileUploadReturnUrl(this,\"verifyFileHidden\");' style='display:none'>"
		+"  <a href='javascript:' onclick='uploadCilck(this);' class='layui-btn layui-btn-normal layui-btn-mini'><i class='layui-icon'>&#xe98a;</i>上传文件</a>"
		+"	<div id='fileShowDiv'>"
		+"  <h4>无附件</h4>"
		+"	</div>"
		+"</div>"
		+"	<input type='hidden' id='verifyFileHidden'>"
		+"</div>";
	return htmlStr;
}
function fileUploadReturnUrl(obj,verifyFileHidden){
	var formData = new FormData();
	var id = $(obj).attr("id");
	var file = document.getElementById(id).files[0];
	formData.append("file",file);
	var url = basePath+"platform/file/uploadRegAttachment";
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var b = xhr.responseText;
			var re = eval('(' + b + ')');
			var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
            	if($('#fileShowDiv>h4').length>0){
            		$('#fileShowDiv>h4').remove();
            	}
            	//获得文件路径
            	var fileUrl = resultObj.filePath;
            	var fileName = fileUrl.substr(fileUrl.lastIndexOf('/')+1);
            	var str = "<p data='"+fileUrl+"'>"+fileName+"<i class='layui-icon' onclick='deleteFile(this)'>&#xe928;</i></p>";
            	$("#fileShowDiv").append(str);
            	reloadFile();
            }
            if (resultObj.result == "fail") {
            	layer.msg(resultObj.msg,{icon:2});
            	return;
            }
		}
	}
	xhr.open("post", url, true);
	xhr.send(formData);
}
function reloadFile(){
	if($('#fileShowDiv p').length>0){
		var fileUrl = '';
		$('#fileShowDiv p').each(function(){
			fileUrl += $(this).attr('data')+',';
		});
		$("#verifyFileHidden").val(fileUrl);
	}else{
		$('#fileShowDiv').append('<h4>无附件</h4>');
	}
}
function deleteFile(obj){
	$(obj).parent().remove();
	reloadFile();
}
function saveVerify(type,id){
	if(type==1){
		//同意
		var htmlStr = getHtmlStr();
		layer.open({
			type:1,
			title:"审批同意意见",
			skin: 'layui-layer-rim',
	    	area: ['310px', 'auto'],
	    	content:htmlStr,
	    	btn:['确定','取消'],
			yes:function(index, layero){
				if($("#verifyOpinion").val() == ""){
					layer.msg('请填写审批意见');
					return false;
				}
				var verifyOpinion = $("#verifyOpinion").val();
				var verifyFileHidden = $("#verifyFileHidden").val();
				verifyTrade(type,id,verifyOpinion,verifyFileHidden);
			}
		});
	}else if(type==2){
		//拒绝
		var htmlStr = getHtmlStr();
		layer.open({
			type:1,
			title:"审批拒绝意见",
			skin: 'layui-layer-rim',
	    	area: ['310px', 'auto'],
	    	content:htmlStr,
	    	btn:['确定','取消'],
			yes:function(index, layero){
				if($("#verifyOpinion").val() == ""){
					layer.msg('请填写审批意见');
					return false;
				}
				var verifyOpinion = $("#verifyOpinion").val();
				var verifyFileHidden = $("#verifyFileHidden").val();
				verifyTrade(type,id,verifyOpinion,verifyFileHidden);
			}
		});
	}else if(type==3){
		//转交
		setReferralUser(type,id);
	}else if(type==4){
		//撤销
		layer.confirm('确认撤销？', {icon: 3, title:'提示'}, function(index){
			verifyTrade(type,id);
		});
	}
}
function setReferralUser(type,id){
	$.ajax({
  		url: basePath+"platform/sysVerify/setReferralUser",
  		data:{
			"page.divId":"layOpen"
		},
  		success: function(data){
  			layer.open({
				type : 1,
				title : '选择转交人员',
				skin : 'pop',
				area : [ '800px', 'auto' ],
				content : data,
				btn:['确认'],
				yes : function(index, layero) {
					var checkedRadio = $('input:radio[name="userId"]:checked');
					if(checkedRadio.length==0){
						layer.msg("请选择转交人员！", {icon : 2});
						return false;
					}
					var htmlStr = getHtmlStr();
					layer.open({
						type:1,
						title:"转交意见",
						skin: 'layui-layer-rim',
				    	area: ['400px', 'auto'],
				    	content:htmlStr,
				    	btn:['确定','取消'],
						yes:function(index, layero){
							if($("#verifyOpinion").val() == ""){
								return false;
							}
							var userId = checkedRadio.val();
							var userName = checkedRadio.parents("tr").find("input[name='userName']").val();
							var verifyOpinion = $("#verifyOpinion").val();
							var remark = userId+","+userName+","+verifyOpinion;
							var verifyFileHidden = $("#verifyFileHidden").val();
							verifyTrade(type,id,remark,verifyFileHidden);
						}
					});
				}
			});
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
 	});
}
function uploadCilck(obj){
	$('#verifyFile').click();
}
function verifyTrade(type,id,remark,fileUrl){
	layer.load();
	$.ajax({
		url:basePath+"platform/tradeVerify/saveVerify",
		data:{
			"saveType":type,
			"id":id,
			"remark":remark,
			"fileUrl":fileUrl
		},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.flag){
				if(type==1||type==2){
					var url = resultObj.url;
					var relatedId = resultObj.id;
					if(url!=''){
						verifyNext(url,relatedId);
					}
				}else{
					layer.closeAll();
					loadPlatformData();
				}
			}
		},
		error:function(){
			layer.closeAll('loading');
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
function verifyNext(url,relatedId){
	$.ajax({
		url:url,
		data:{"id":relatedId},
		success:function(data){
			layer.closeAll('loading');
			layer.closeAll();
			var indexObj = $("input[name='index']:hidden");
			if(indexObj.length > 0 && indexObj.val()=='index'){
				location.reload();
			}else{
				loadPlatformData();
			}
		},
		error:function(){
			layer.closeAll('loading');
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
function loadVerifyType(selectValue){
	$.ajax({
		url:basePath+"platform/tradeVerify/getVerifyType",
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var selectObj = $("<select onchange='changeVerifyType(this);'></select>");
			selectObj.attr("id","verifyType");
			selectObj.attr("name","verifyType");
			$('<option>',{val:"",text:"【全部】"}).appendTo(selectObj);
			$.each(resultObj,function(i){
				var id = resultObj[i].id;
				var title = "【"+resultObj[i].title+"】";
				if(selectValue != null && selectValue != ''){
					if(selectValue==id){
						$('<option>',{val:id,text:title,selected:"selected"}).appendTo(selectObj);
						return;
					}
				}
				$('<option>',{val:id,text:title}).appendTo(selectObj);
			});
			$("#verifyTypeSpan").append(selectObj);
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
function changeVerifyType(obj){
	loadPlatformData();
}
function trClick(obj){
	var id = $(obj).find("td:first").html();
	if($(".verifyDetail").length>0)$(".verifyDetail").remove();
	$.ajax({
		url:basePath+"platform/tradeVerify/verifyDetail",
		data:{"id":id},
		success:function(data){
			var detailDiv = $("<div style='padding:0px'></div>");
			detailDiv.addClass("verifyDetail");
			detailDiv.html(data);
			detailDiv.appendTo($(".verifyDiv"));
			detailDiv.find("#verify_side").addClass("show");
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}
//关闭
function closeVerifyDetail(obj){
	$(".verifyDetail").remove();
}
//打开附件地址
function openAnnex(obj,id){
	var url = $(obj).find("input[name='annexUrl']").val();
	var b = new Base64();
	if(url.indexOf("?") > 0 ){
		url += "&id="+id;
	}else{
		url += "?id="+id;
	}
    var str = b.encode(url);
    var path = basePath+"DDS_verify/viewVerifyDateil?url="+str
	window.open(path);
}
function urge(tradeId){
	$.ajax({
		url:basePath+"platform/tradeVerify/urgeTrade",
		data:{"tradeId":tradeId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			layer.msg(resultObj.msg);
		},
		error:function(){
		}
	});
}