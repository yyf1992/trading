function loadMenuHtml(menuId,menuName){
	if(menuId==''||menuId==null||typeof(menuId)== 'undefined'){
		//取第一
		menuId = $('#parentMenuDl dd:first').attr('dataId');
		menuName = $('#parentMenuDl dd:first').attr('dataName');
	}
	$('#parentMenuShow').html(menuName).append('<span class="layui-nav-more"></span>');
	var liObjStr = new Array();
	$.each(menuArray,function(i,parentMenu){
		if(parentMenu.id==menuId){
			var leftMenuList = parentMenu.childrenMenuList;
			pinJieMenuHtml(leftMenuList,liObjStr);
		}
	});
	$('ul#nav').empty();
	$.each(liObjStr,function(i){
		$('ul#nav').append(liObjStr[i]);
	});
	menuLoadClick();
}
function pinJieMenuHtml(menuList,liObjStr){
	$.each(menuList,function(a,menu){
		var childrenMenuList = menu.childrenMenuList;
		var menuLiObj = '<li data="'+menu.id+'">';
		if(childrenMenuList != null && childrenMenuList.length > 0){
			menuLiObj += '<a href="javascript:;">';
			if(menu.icon != null && menu.icon != ''){
				menuLiObj += '	<i class="layui-icon">'+menu.icon+'</i>';
			}
			menuLiObj += '	<cite>'+menu.name+'</cite>';
			menuLiObj += '	<i class="layui-icon nav_right">&#xeba8;</i>';
			menuLiObj += '</a>';
			menuLiObj += '	<ul class="sub-menu">';
			var liObjStr2 = new Array();
			pinJieMenuHtml(childrenMenuList,liObjStr2);
			$.each(liObjStr2,function(b){
				menuLiObj += liObjStr2[b];
			});
			menuLiObj += '</ul>';
		}else{
			menuLiObj += '<a _href="'+menu.url+'">';
			if(menu.icon != null && menu.icon != ''){
				menuLiObj += '	<i class="layui-icon">'+menu.icon+'</i>';
			}
			menuLiObj += '	<cite>'+menu.name+'</cite>';
			menuLiObj += '</a>';
		}
		menuLiObj += '</li>';
		liObjStr.push(menuLiObj);
	});
}
function menuLoadClick(){
	//左侧菜单效果
    $('.left-nav #nav li').click(function (event) {
        if($(this).children('.sub-menu').length){
            if($(this).hasClass('open')){
                $(this).removeClass('open');
                $(this).find('.nav_right').html('&#xeba8;');
                $(this).children('.sub-menu').stop().slideUp();
                $(this).siblings().children('.sub-menu').slideUp();
            }else{
                $(this).addClass('open');
                $(this).children('a').find('.nav_right').html('&#xeba5;');
                $(this).children('.sub-menu').stop().slideDown();
                $(this).siblings().children('.sub-menu').stop().slideUp();
                $(this).siblings().find('.nav_right').html('&#xeba8;');
                $(this).siblings().removeClass('open');
            }
        }else{
            var urlData = $(this).children('a').attr('_href');
            var menuId = $(this).attr('data');
            var title = $(this).find('cite').html();
            for (var i = 0; i <$('.x-iframe').length; i++) {
                if($('.x-iframe').eq(i).attr('tab-id')==menuId){
                    tab.tabChange(menuId);
                    event.stopPropagation();
                    return;
                }
            };
            var b = new Base64();
            var str = b.encode(urlData);
            var url = basePath+"platform/iframeIndex?urlData="+str+"&menuId="+menuId;
            tab.tabAdd(title,url,menuId);
            tab.tabChange(menuId);
        }
        event.stopPropagation();
    });
}