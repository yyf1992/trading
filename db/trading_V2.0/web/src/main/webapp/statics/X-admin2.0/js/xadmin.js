var form;
//触发事件
var tab = {
	tabAdd: function(title,url,id){
		layer.load();
		//新增一个Tab项
		element.tabAdd('xbs_tab', {
			title: title 
			,content: '<iframe tab-id="'+id+'" frameborder="0" src="'+url+'" scrolling="yes" class="x-iframe"></iframe>'
			,id: id
		})
	}
	,tabDelete: function(othis){
		//删除指定Tab项
		element.tabDelete('xbs_tab', '44');
		othis.addClass('layui-btn-disabled');
	}
	,tabChange: function(id){
		//切换到指定Tab项
		element.tabChange('xbs_tab', id);
	}
};
$(function () {
	initializeLayui();
    //加载弹出层
    tableCheck = {
        init:function  () {
            $(".layui-form-checkbox").click(function(event) {
                if($(this).hasClass('layui-form-checked')){
                    $(this).removeClass('layui-form-checked');
                    if($(this).hasClass('header')){
                        $(".layui-form-checkbox").removeClass('layui-form-checked');
                    }
                }else{
                    $(this).addClass('layui-form-checked');
                    if($(this).hasClass('header')){
                        $(".layui-form-checkbox").addClass('layui-form-checked');
                    }
                }
                
            });
        },
        getData:function  () {
            var obj = $(".layui-form-checked").not('.header');
            var arr=[];
            obj.each(function(index, el) {
                arr.push(obj.eq(index).attr('data-id'));
            });
            return arr;
        }
    }
    //开启表格多选
    tableCheck.init();
    $('.container .left_open i').click(function(event) {
        if($('.left-nav').css('left')=='0px'){
            $('.left-nav').animate({left: '-221px'}, 100);
            $('.page-content').animate({left: '0px'}, 100);
            $('.page-content-bg').hide();
        }else{
            $('.left-nav').animate({left: '0px'}, 100);
            $('.page-content').animate({left: '221px'}, 100);
            if($(window).width()<768){
                $('.page-content-bg').show();
            }
        }

    });

    $('.page-content-bg').click(function(event) {
        $('.left-nav').animate({left: '-221px'}, 100);
        $('.page-content').animate({left: '0px'}, 100);
        $(this).hide();
    });

    $('.layui-tab-close').click(function(event) {
        $('.layui-tab-title li').eq(0).find('i').remove();
    });
});
function initializeLayui(){
	layui.use(['form','element'],
    function() {
        layer = layui.layer,form = layui.form;
        element = layui.element;
    });
	form = layui.form
}

/*弹出层*/
/*
    参数解释：
    title   标题
    url     请求的url
    id      需要操作的数据id
    w       弹出层宽度（缺省调默认值）
    h       弹出层高度（缺省调默认值）
*/
function x_admin_show(title,url,w,h){
    if (title == null || title == '') {
        title=false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '') {
        w=($(window).width()*0.9);
    };
    if (h == null || h == '') {
        h=($(window).height() - 50);
    };
    layer.open({
        type: 2,
        area: [w+'px', h +'px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade:0.4,
        title: title,
        content: url
    });
}
/*关闭弹出框口*/
function x_admin_close(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}
function setSystemTime(){
	// 显示页面时间
	var today = new Date();//定义日期对象   
	var yyyy = today.getFullYear();//通过日期对象的getFullYear()方法返回年    
	var MM = today.getMonth() + 1;//通过日期对象的getMonth()方法返回年    
	var dd = today.getDate();//通过日期对象的getDate()方法返回年     
	var hh = today.getHours();//通过日期对象的getHours方法返回小时   
	var mm = today.getMinutes();//通过日期对象的getMinutes方法返回分钟   
	var ss = today.getSeconds();//通过日期对象的getSeconds方法返回秒   
	// 如果分钟或小时的值小于10，则在其值前加0，比如如果时间是下午3点20分9秒的话，则显示15：20：09   
	MM = checkTime(MM);
	dd = checkTime(dd);
	mm = checkTime(mm);
	ss = checkTime(ss);
	var day; //用于保存星期（getDay()方法得到星期编号）
	if (today.getDay() == 0)
		day = "星期日 "
	if (today.getDay() == 1)
		day = "星期一 "
	if (today.getDay() == 2)
		day = "星期二 "
	if (today.getDay() == 3)
		day = "星期三 "
	if (today.getDay() == 4)
		day = "星期四 "
	if (today.getDay() == 5)
		day = "星期五 "
	if (today.getDay() == 6)
		day = "星期六 "
	document.getElementById('nowDateTimeSpan').innerHTML = yyyy + "年" + MM + "月" + dd + "日 " + day;
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}
//验证操作日期
function verifyTime(type){
	var planTime;
	$.ajax({
		type    : "get",
		url     : basePath+"buyer/planTime/getPlanTime",
		async   : false,
		dataType: "json",
		success : function(data) {
			if(!jQuery.isEmptyObject(data)){
				getAnnouncementHtml(data,type);
				planTime = data;
			}
		},
		error : function() {
		}
	});
	return planTime;
}
//公告
function getAnnouncementHtml(planTime,type){
	var EndTime;
	var EndTimeStr;
	var timeArray;
	var timeArrayStr='';
	var Expired=false;
	if(type=='1'&&planTime.salePlanWeek!=null&&planTime.salePlanWeek!=''){
		//提报销售计划
		timeArray=planTime.salePlanWeek.split("-");
	}else if(type=='2'&&planTime.confirmPlanWeek!=null&&planTime.confirmPlanWeek!=''){
		//权限设置
		if(loginUserId=='admin' || planTime.confirmSaleplanUser.indexOf(loginUserId)>-1){
			return false;
		}
		//确认销售计划
		timeArray=planTime.confirmPlanWeek.split("-");
	}else if(type=='3'&&planTime.applyPlanWeek!=null&&planTime.applyPlanWeek!=''){
		//采购计划提报
		//权限设置
		if(loginUserId=='admin' || planTime.applyPurchaseUser.indexOf(loginUserId)>-1){
			return false;
		}
		timeArray=planTime.applyPlanWeek.split("-");
	}else if(type=='4'&&planTime.purchaseWeek!=null&&planTime.purchaseWeek!=''){
		//采购下单
		//权限设置
		if(loginUserId=='admin' || planTime.purchaseUser.indexOf(loginUserId)>-1){
			return false;
		}
		timeArray=planTime.purchaseWeek.split("-");
	}
	
	if(typeof(timeArray)!='undefined'&&timeArray.length>0){
		var expiredFlage = false;
		$.each(timeArray,function(i){
			if(timeArray[i]=='0')timeArrayStr+='周一'+'、';
			if(timeArray[i]=='1')timeArrayStr+='周二'+'、';
			if(timeArray[i]=='2')timeArrayStr+='周三'+'、';
			if(timeArray[i]=='3')timeArrayStr+='周四'+'、';
			if(timeArray[i]=='4')timeArrayStr+='周五'+'、';
			if(timeArray[i]=='5')timeArrayStr+='周六'+'、';
			if(timeArray[i]=='6')timeArrayStr+='周七'+'、';
			//判断是否过期
			var date = window.util_date.getDate(timeArray[i]);
			var ExpiredType = compareDate(date.pattern('yyyy/MM/dd'),new Date().pattern('yyyy/MM/dd'));
			var EndTime2;
			if(ExpiredType==1){
				//未到
				EndTime2 = date;
			}else if(ExpiredType==2){
				//当天
				Expired = true;
				return false;
			}else if(ExpiredType==3){
				//已经过期
				EndTime2 = getAddDate(date,7);
			}
			if(typeof(EndTime)=='undefined'){
				EndTime=EndTime2;
			}else{
				if(compareDate(EndTime2.pattern('yyyy/MM/dd'),EndTime.pattern('yyyy/MM/dd'))==3)EndTime=EndTime2;
			}
			EndTimeStr = EndTime.pattern('yyyy/MM/dd HH:mm:ss');
		});
		if(!Expired){
			//时间过期，提示公告
			if(timeArrayStr.length>0)timeArrayStr=timeArrayStr.substring(0,timeArrayStr.length-1);
			var htmlStr = '<div class="announcement">';
			htmlStr += '<h2>公告</h2>';
			htmlStr += '本功能'+(type=='1'?'提报':'操作')+'时间为每周的<mark>'+timeArrayStr+'</mark>，本次提交已经结束，敬请下次再来使用，谢谢！';
			htmlStr += '<h3>';
			htmlStr += '	<span id="t_d" class="ptime">00</span>天';
			htmlStr += '	<span id="t_h" class="ptime">00</span>时';
			htmlStr += '	<span id="t_m" class="ptime">00</span>分';
			htmlStr += '	<span id="t_s" class="ptime">00</span>秒';
			htmlStr += '</h3>';
			htmlStr += '</div>';
			htmlStr += '<script>';
			htmlStr += 'var timer = setInterval("GetRTime(\''+EndTimeStr+'\')",0);';
			htmlStr += '</script>';
			//设置了操作日期
			if(type=='1'){
				//权限设置
				if(loginUserId=='admin' || planTime.outPlanUser.indexOf(loginUserId)>-1){
					//拥有计划外提报权限
					layer.open({
						type: 1
						,title: false //不显示标题栏
						,closeBtn: false
						,area: '400px;'
						,shade: [0.5, '#393D49']
						,id: 'LAY_layuipro' //设定一个id，防止重复弹出
						,btn: ['提报计划外销售']
						,btnAlign: 'c'
						,moveType: 1 //拖拽模式，0或者1
						,content: htmlStr
						,yes: function(layero){
							clearInterval(timer);
							layer.closeAll();
							leftMenuClick(this,"buyer/salePlan/addSalePlanOut","buyer",'18041120315315077993','计划外销售计划');
						}
					});
				}else{
					//普通人员
					layer.open({
						type: 1
						,title: false //不显示标题栏
						,closeBtn: false
						,area: '400px;'
						,shade: [0.5, '#393D49']
						,id: 'LAY_layuipro' //设定一个id，防止重复弹出
						,btnAlign: 'c'
						,moveType: 1 //拖拽模式，0或者1
						,content: htmlStr
						,yes: function(layero){
						}
					});
				}
			}else{
				layer.open({
					type: 1
					,title: false //不显示标题栏
					,closeBtn: false
					,area: '400px;'
					,shade: [0.5, '#393D49']
					,id: 'LAY_layuipro' //设定一个id，防止重复弹出
					,btnAlign: 'c'
					,moveType: 1 //拖拽模式，0或者1
					,content: htmlStr
				});
			}
		}
	}
}
function GetRTime(EndTimeStr){
    var minuteOld = document.getElementById("t_m").innerHTML;
    var EndTime= new Date(EndTimeStr);
    var NowTime = new Date();
    var t =EndTime.getTime() - NowTime.getTime();
    var d=Math.floor(t/1000/60/60/24);
    var h=Math.floor(t/1000/60/60%24);
    var m=Math.floor(t/1000/60%60);
    var s=Math.floor(t/1000%60);
    $('.announcement #t_d').html(d);
    $('.announcement #t_h').html(h);
    $('.announcement #t_m').html(m);
    $('.announcement #t_s').html(s);
    if(compareDate(EndTime.pattern('yyyy/MM/dd HH:mm:ss'),NowTime.pattern('yyyy/MM/dd HH:mm:ss'))==2){
    	clearInterval(timer);
    	layer.closeAll();
    }
}
function compareDate(startDate, endDate){
	var result = 0;
	var d1 = new Date(startDate.replace(/\-/g, "\/"));  
	var d2 = new Date(endDate.replace(/\-/g, "\/"));
	if(d1.getTime()>d2.getTime())result=1;
	if(d1.getTime()==d2.getTime())result=2;
	if(d1.getTime()<d2.getTime())result=3;
	return result;
}
function getAddDate(startDate,n){
	var date = new Date();
	date.setTime(startDate.getTime()+24*60*60*1000*n);
	return date;
}
//是否为正整数 
function isPositiveNum(s){ 
    var re = /^[0-9]*[1-9][0-9]*$/ ;  
    return re.test(s)  
}