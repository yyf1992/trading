/**
 * @author zhuyangxing
 */
(function() {
  function util_date() {
    var _today=new Date();
    this.today=_today;
    this.year=_today.getYear()+1900;//当前年份
    this.Month_a=_today.getMonth();
    this.Month=this.Month_a+1;//当前月份
    this.day=_today.getDate();//当前日期
    this.date=_today.getDay()==0?7:_today.getDay();//本周第几天 因系统会把周日作为第0天
    this.Monday="";
    this.Sunday="";
    this.day_one="";
  }
  Date.prototype.pattern=function(fmt) {
    var o = {
    "M+" : this.getMonth()+1, //月份
    "d+" : this.getDate(), //日
    "h+" : this.getHours()%12 == 0 ? 12 : this.getHours()%12, //小时
    "H+" : this.getHours(), //小时
    "m+" : this.getMinutes(), //分
    "s+" : this.getSeconds(), //秒
    "q+" : Math.floor((this.getMonth()+3)/3), //季度
    "S" : this.getMilliseconds() //毫秒
    };
    var week = {
    "0" : "/u65e5",
    "1" : "/u4e00",
    "2" : "/u4e8c",
    "3" : "/u4e09",
    "4" : "/u56db",
    "5" : "/u4e94",
    "6" : "/u516d"
    };
    if(/(y+)/.test(fmt)){
      fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    if(/(E+)/.test(fmt)){
      fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);
    }
    for(var k in o){
      if(new RegExp("("+ k +")").test(fmt)){
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
      }
    }
    return fmt;
  },
  util_date.prototype = {
      newToday : function(_today){
        this.today=_today;
        this.year=_today.getYear()+1900;//当前年份
        this.Month_a=_today.getMonth();
        this.Month=this.Month_a+1;//当前月份
        this.day=_today.getDate();//当前日期
        this.date=_today.getDay()==0?7:_today.getDay();//本周第几天 因系统会把周日作为第0天
        this.Monday="";
        this.Sunday="";
        this.day_one="";
      },
      getDate:function(n){
        var _monday = new Date(this.year,this.Month_a,this.day-this.date+1);
        var date = new Date();
        date.setTime(_monday.getTime()+24*60*60*1000*n);
        return date;
      }
  };
  window.util_date = new util_date();
})();