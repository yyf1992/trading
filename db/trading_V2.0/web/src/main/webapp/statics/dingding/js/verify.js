//判断是否是json对象
function isJSONObject(obj){
	var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
	return isjson;
}
function saveVerify(type,id){
	if(type==1){
		//同意
		var htmlStr = getHtmlStr();
		layer.open({
			type:1,
			title:"审批同意意见",
			skin: 'layui-layer-rim',
	    	area: ['300px', 'auto'],
	    	content:htmlStr,
	    	btn:['确定','取消'],
			yes:function(index, layero){
				if($("#verifyOpinion").val() == ""){
					layer.msg('请填写审批意见');
					return false;
				}
				var verifyOpinion = $("#verifyOpinion").val();
				var verifyFileHidden = $("#verifyFileHidden").val();
				verifyTrade(type,id,verifyOpinion,verifyFileHidden);
			}
		});
	}else if(type==2){
		//拒绝
		var htmlStr = getHtmlStr();
		layer.open({
			type:1,
			title:"审批拒绝意见",
			skin: 'layui-layer-rim',
	    	area: ['300px', 'auto'],
	    	content:htmlStr,
	    	btn:['确定','取消'],
			yes:function(index, layero){
				if($("#verifyOpinion").val() == ""){
					layer.msg('请填写审批意见');
					return false;
				}
				var verifyOpinion = $("#verifyOpinion").val();
				var verifyFileHidden = $("#verifyFileHidden").val();
				verifyTrade(type,id,verifyOpinion,verifyFileHidden);
			}
		});
	}else if(type==3){
		//转交
		setReferralUser(type,id);
	}
}
function setReferralUser(type,id){
	$.ajax({
  		url: basePath+"DDS_verify/setReferralUser",
  		data:{
  			"verifyId":id
  		},
  		success: function(data){
  			layer.open({
  				type : 1,
  				title : '选择转交人员',
  				skin : 'pop',
  				area : [ '300px', '500px' ],
  				content : data,
  				yes : function(index, layero) {
  				}
  			});
  		},
  		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
 	});
}
function verifyTrade(type,id,remark,fileUrl){
	layer.load();
	$.ajax({
		url:basePath+"DDS_verify/saveVerify",
		data:{
			"saveType":type,
			"id":id,
			"remark":remark,
			"userId":$("#userId").val(),
			"fileUrl":fileUrl
		},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.flag){
				if(type==1||type==2){
					var url = resultObj.url;
					var relatedId = resultObj.id;
					if(url != null && url!='' && url != 'undefined'){
						verifyNext(url,relatedId);
					}else{
						//刷新页面
						location.reload();
					}
				}else{
					//刷新页面
					location.reload();
				}
			}
		},
		error:function(){
			layer.msg("审批时获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function verifyNext(url,relatedId){
	$.ajax({
		url:basePath+url,
		async:false,
		data:{"id":relatedId},
		success:function(data){
			//刷新页面
			location.reload();
		},
		error:function(){
			layer.msg("审批成功进行下一步操作时获取数据失败！",{icon:2});
		}
	});
}
//打开附件地址
function openAnnex(obj,id){
	var url = $(obj).find("input[name='annexUrl']").val();
	var b = new Base64();
	if(url.indexOf("?") > 0 ){
		url += "&id="+id;
	}else{
		url += "?id="+id;
	}
    var str = b.encode(url);
	DingTalkPC.biz.util.openLink({
		"url": basePath+"DDS_verify/vieWappendix?userId="+$("#userId").val()+"&url="+str
	});
}
function getHtmlStr(){
	var htmlStr = "<div id='importExcelDiv'>"
		+"<div class='layui-inline' style='padding:10px;'>"
		+"	<div class='layui-input-inline'>"
		+"      <textarea rows='5' style='width: 260px;' id='verifyOpinion'></textarea>"
		+"	</div>"
		+"	<input type='file' id='verifyFile' onchange='fileUploadReturnUrl(this,\"verifyFileHidden\");' style='display:none'>"
		+"  <a href='javascript:' onclick='uploadCilck(this);' class='layui-btn layui-btn-normal layui-btn-mini'><i class='layui-icon'>&#xe98a;</i>上传文件</a>"
		+"	<div id='fileShowDiv'>"
		+"  <h4>无附件</h4>"
		+"	</div>"
		+"</div>"
		+"	<input type='hidden' id='verifyFileHidden'>"
		+"</div>";
	return htmlStr;
}
function uploadCilck(obj){
	$('#verifyFile').click();
}
function fileUploadReturnUrl(obj,verifyFileHidden){
	var formData = new FormData();
	var file = $(obj).get(0).files[0];
	formData.append("file",file);
	var url = basePath+"platform/file/uploadRegAttachment";
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var b = xhr.responseText;
			var re = eval('(' + b + ')');
			var resultObj = isJSONObject(re)?re:eval('(' + re + ')');
            if (resultObj.result == "success") {
            	if($('#fileShowDiv>h4').length>0){
            		$('#fileShowDiv>h4').remove();
            	}
            	//获得文件路径
            	var fileUrl = resultObj.filePath;
            	var fileName = fileUrl.substr(fileUrl.lastIndexOf('/')+1);
            	var str = "<p data='"+fileUrl+"'>"+fileName+"<i class='layui-icon' onclick='deleteFile(this)'>&#xe928;</i></p>";
            	$("#fileShowDiv").append(str);
            	reloadFile();
            }
            if (resultObj.result == "fail") {
            	layer.msg(resultObj.msg,{icon:2});
            	return;
            }
		}
	}
	xhr.open("post", url, true);
	xhr.send(formData);
}
function reloadFile(){
	if($('#fileShowDiv p').length>0){
		var fileUrl = '';
		$('#fileShowDiv p').each(function(){
			fileUrl += $(this).attr('data')+',';
		});
		$("#verifyFileHidden").val(fileUrl);
	}else{
		$('#fileShowDiv').append('<h4>无附件</h4>');
	}
}
function deleteFile(obj){
	$(obj).parent().remove();
	reloadFile();
}
function downLoad(url){
	DingTalkPC.biz.util.openLink({
		"url": url
	});
}
function urge(tradeId){
	$.ajax({
		url:basePath+"platform/tradeVerify/urgeTrade",
		data:{"tradeId":tradeId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			layer.msg(resultObj.msg);
		},
		error:function(){
		}
	});
}