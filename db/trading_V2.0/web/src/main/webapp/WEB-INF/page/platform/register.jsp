<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>注册</title>
<%@ include file="common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/j.js"></script>
<script src="<%=basePath%>statics/platform/js/colResizable-1.6.min.js"></script>
<script src="<%=basePath%>statics/platform/js/common.js"></script>
<script src="<%=basePath%>statics/plugins/layui/layui.js"></script>
<script src="<%=basePath%>statics/platform/js/PCASClass.js"></script>
<script src="<%=basePath%>statics/platform/js/register.js"></script>
<script src="<%=basePath%>statics/platform/js/upload.js"></script>
<script src="<%=basePath%>js/common.js"></script>

<link rel="stylesheet" href="<%=basePath%>statics/plugins/layui/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/register1.css">
<script type="text/javascript">
// 检查是否为空共用方法
	function checkEmpty(name,msg){
		var msgSpan = $("<span id='check"+name+"'></span>").addClass("help_block red");
		var val = $("#" + name).val();
		if(val == ""){
			$("#check" + name).remove();
			msgSpan.append(msg);
			$("#" + name).after(msgSpan);
		}
	}
$(function(){
	//删除采购凭证
	$(".voucherImg").on("click","b",function(){
		$(this).parent().remove();
	});
	// 下一步
	$("#step1").click(function(){
		var obj = $(this).parents("form");
		// 是否填写手机号
		checkEmpty("loginName" , "请填写手机号！");
		checkEmpty("password" , "请填写密码！");
		checkEmpty("rePassword" , "请填写确认密码！");
   		if($(obj).find("span[class='help_block red']").length < 1){
	   		$('.progress>span.company').addClass('current').siblings().removeClass('current');
			$('#re2').addClass('show').siblings().removeClass('show');
   		}
	});
	// 提交
	$('#step2').click(function(){
		var obj = $(this).parents("form");
		// 检查公司名称是否填写
		checkEmpty("companyName" , "请填写公司名称！");
		// 检查企业所在地是否填写
		checkEmpty("area" , "请填写企业所在地！");
		
		checkEmpty("detailAddress" , "请填写详细地址！");
		
		checkEmpty("linkMan" , "请填写联系人！");
   		if($(obj).find("span[class='help_block red']").length < 1){
   			var a1 = $("#attachment1Div").find("span").length;
   			var a1Str = ""
			if(a1 != 0){
				for(var i = 0;i < a1;i++){
					var url1 = $("#attachment1Div").find("span:eq("+i+")").find("input").val();
					a1Str = a1Str + url1 + "@";
				}
				a1Str = a1Str.substring(0, a1Str.length - 1);
			}
			var a2 = $("#attachment2Div").find("span").length;
			var a2Str = ""
			if(a2 != 0){
				for(var i = 0;i < a2;i++){
					var url2 = $("#attachment2Div").find("span:eq("+i+")").find("input").val();
					a2Str = a2Str + url2 + "@";
				}
				a2Str = a2Str.substring(0, a2Str.length - 1)
			}
   			$.ajax({
   				type : "POST",
   				url : "platform/reg/subRegister",
   				async: false,
   				data : {
   					"loginName" : $("#loginName").val(),
					"password" : $("#password").val(),
					"companyName" : $("#companyName").val(),
					"province" : $("#province").val(),
					"city" : $("#city").val(),
					"area" : $("#area").val(),
					"detailAddress" : $("#detailAddress").val(),
					"linkMan" : $("#linkMan").val(),
					"zone" : $("#zone").val(),
					"telNo" : $("#telNo").val(),
					"fax1" : $("#loginName").val(),
					/* "fax2" : $("#fax2").val(), */
					"qq" : $("#qq").val(),
					"wangNo" : $("#wangNo").val(),
					"email" : $("#email").val(),
					"a1Str" : a1Str,
					"a2Str" : a2Str
   				},
   				success : function(data) {
   					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
   					if(resultObj.success){
   						$('.progress>span.success').addClass('current').siblings().removeClass('current');
						$('#re3').addClass('show').siblings().removeClass('show');
   					}else{
   						layer.msg(resultObj.msg,{icon:2});
   					}
   				},
   				error : function() {
   					layer.msg("获取数据失败，请稍后重试！",{icon:2});
   				}
   			});
   		}
	});
	// 上一步
	$('#step2_b').click(function(){
		$('.progress>span.basic').addClass('current').siblings().removeClass('current');
		$('#re1').addClass('show').siblings().removeClass('show');
	});
		loadProvince("province","");
		// 根据省份加载市
		var provinceId = $("#province").val();
		loadCity(provinceId,"city","");
		// 根据市加载区
		var cityId = $("#city").val();
		loadArea(cityId,"area","");
		//省改变
		$("#province").change(function(){
			// 根据省份加载市
			var provinceId = $("#province").val();
			loadCity(provinceId,"city","");
			// 根据市加载区
			var cityId = $("#city").val();
			loadArea(cityId,"area","");
		});
		//市改变
		$("#city").change(function() {
			// 根据市加载区
			var cityId = $("#city").val();
			loadArea(cityId,"area","");
		});
		// 同意协议
		$(".agree").click(function(){
			if ($(this).is(':checked')) {
				$("#subButtonDisable").hide();
				$("#step2").show();
			} else {
				$("#subButtonDisable").show();
				$("#step2").hide();
			}
		});
});
	//检查手机号是否已存在
	function checkPhone(obj) {
		var phone = $(obj).val();
		if (phone != "") {
			$.ajax({
				type : "POST",
				url : "platform/reg/checkUserPhone",
				async : true,
				data : {
					"phone" : phone
				},
				success : function(data) {
					$("#checkloginName").remove();
					var dataObj = eval('(' + data + ')');
					var resultObj = isJSONObject(dataObj)?result:eval('(' + dataObj + ')');
					var msgSpan = $("<span id='checkloginName'></span>").addClass("help_block red");
					if (resultObj.result) {
						msgSpan.append("该手机号已注册本系统，不能再次注册！");
						$(obj).after(msgSpan);
						return;
					}else if(!(/^1[34578]\d{9}$/.test(phone))){//格式是否正确
						msgSpan.append("号码格式不正确！");
						$(obj).after(msgSpan);
						return;
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
		}
	}
	//检查确认密码是否与上次一致
	function checkRePassword(obj){
		var rePassword = $(obj).val();
		var password = $("#password").val();
		$("#checkrePassword").remove();
		if (rePassword != "" && rePassword != password) {
			var msgSpan = $("<span id='checkrePassword'></span>").addClass("help_block red");
			msgSpan.append("输入的密码与上面密码不一致，请重新输入密码！");
			$(obj).after(msgSpan);
		}
	}
	// 检查密码是否为空
	function checkPassword(obj){
		var password = $(obj).val();
		if(password != ""){
			$("#checkpassword").remove();
			if(password.length < 6){
				var msgSpan = $("<span id='checkpassword'></span>").addClass("help_block red");
				msgSpan.append("密码长度不够！");
				$("#password").after(msgSpan);
			}
		}
	}
</script>
<style>
	.sub_disable{
		cursor:not-allowed;
	}
</style>
</head>
<body>
	<div class="top">
		<!--logo部分-->
		<div class="logo">
			<img src="<%=basePath%>statics/platform/images/reg_logo.jpg"> <span>已有账号?&emsp;<a
				href="platform/login">请登录</a>
			</span>
		</div>
	</div>
	<!--主体-->
	<section class="register_main">
		<div>
			<div class="progress">
				<span class="basic current"> <b></b> 基本信息 </span> <span
					class="company"> <b></b> 企业信息 </span> <span class="success">
					<b></b> 注册成功 </span>
			</div>
			<!--注册表单-->
			<div id="form_content">
				<div class="show" id="re1">
					<form action="" class="form_l">
						<div class="form_r">
							<label class="control_l">手 机 号</label> <input type="text"
								class="form_c" placeholder="请输入手机号码" name="loginName" id="loginName" onchange="checkPhone(this)"
								onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
	    						onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
						</div>
						<div class="form_r">
							<label class="control_l">验 证 码</label>
							<div class="verification">
								<input type="text" placeholder="请输入短信验证码" class="form_c"><span>获取验证码</span>
							</div>
						</div>
						<div class="form_r">
							<label class="control_l">设 置 密 码</label> <input type="password"
								class="form_c" placeholder="请输入密码" name="password" id="password" onchange="checkPassword(this);"> <span
								class="help_block">支持英文，字符和数字组成的至少6位的字符</span>
						</div>
						<div class="form_r">
							<label class="control_l">确 认 密 码</label> <input type="password"
								class="form_c" placeholder="请输入确认密码" name="rePassword" id="rePassword" onchange="checkRePassword(this)">
						</div>
						<div class="form_r">
							<label class="control_l">邀 请 码</label> <input type="text"
								class="form_c" placeholder="请输入邀请码"> <span
								class="help_block">用户选填</span>
						</div>
						<div class="form_r">
							<label class="control_l"></label>
							<div class="next" id="step1">下一步</div>
						</div>
					</form>
				</div>
				<div class="hide" id="re2">
					<form action="">
						<div class="main_r">
							<div class="form_r">
								<label class="control_l"><span class="red">*</span> 公 司
									名 称</label> <input type="text" class="form_c" required
									placeholder="请输入公司名称" id="companyName" name="companyName">
							</div>
							<div class="form_r">
								<label class="control_l"><span class="red">*</span> 企 业
									所 在 地</label>
								<div class="re_city">
									<select id="province"></select>
									<select id="city"></select>
									<select id="area"></select>
								</div>
							</div>
							<div class="form_r">
								<label class="control_l"><span class="red">*</span> 详 细
									地 址</label> <input type="text" class="form_c" required
									placeholder="请输入公司详细地址" id="detailAddress" name="detailAddress">
							</div>
							<div class="form_r">
								<label class="control_l"><span class="red">*</span> 联 系
									人</label> <input type="text" class="form_c" required
									placeholder="请输入公司联系人姓名" id="linkMan" name="linkMan">
							</div>
							<div class="form_r">
								<label class="control_l">&emsp;电 话</label> <input type="text"
									class="area_code form_c" placeholder="区号" id="zone" name="zone"
						onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
						onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"> - <input
									type="text" class="phone form_c" placeholder="电话" id="telNo" name="telNo"
						onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
  						onafterpaste="if(this.val ue.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
							</div>
							<div class="form_r">
								<label class="control_l">&emsp;营业证件扫描件</label>
								<span class="uploadImage">
				                	<button class="layui-btn layui-btn-primary layui-btn-small"><i class="layui-icon"></i> 浏览上传</button>
				                	<input type="file" multiple name="attachment1" id="attachment1" onchange="fileUpload(this);">
				                </span>
								<span class="explain">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
								<div class="voucherImg" id="attachment1Div"></div>
							</div>
							<div class="form_r">
								<label class="control_l">&emsp;开户许可证扫描件</label>
								<span class="uploadImage">
				                	<button class="layui-btn layui-btn-primary layui-btn-small"><i class="layui-icon"></i> 浏览上传</button>
				                	<input type="file" multiple name="attachment2" id="attachment2" onchange="fileUpload(this);">
				                </span>
								<span class="explain">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
								<div class="voucherImg" id="attachment2Div"></div>
							</div>
							<!-- <div class="form_r">
								<label class="control_l">&emsp;传 真</label> <input type="text"
									class="area_code form_c" placeholder="区号" id="fax1" name="fax1"> - <input
									type="text" class="phone form_c" placeholder="传真" id="fax2" name="fax2">
							</div> -->
							<div class="form_r">
								<label class="control_l">&emsp;Q Q</label> <input type="text"
									class="form_c" required placeholder="请输入联系QQ" id="qq" name="qq"
						onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
	    				onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
							</div>
							<div class="form_r">
								<label class="control_l">&emsp;旺 旺 号</label> <input type="text"
									class="form_c" required placeholder="请输入联系旺旺号" id="wangNo" name="wangNo">
							</div>
							<div class="form_r">
								<label class="control_l">&emsp;E-mail</label> <input type="text"
									class="form_c" required placeholder="请输入企业邮箱地址" id="email" name="email">
							</div>
							<div class="form_r">
								<label class="control_l"></label> <input type="checkbox"
									class="agree"> 同意诺泰买卖系统的 <a
									href="<%=basePath%>platform/reg/agreement" target="_blank">《软件服务协议》</a>
							</div>
							<div class="form_r">
								<label class="control_l"></label>
								<div class="pre" id="step2_b">上一步</div>
								<input type="button" class="sub_disable" id="subButtonDisable" value="提交">
								<div class="sub" id="step2" style="display: none">提交</div>
							</div>
						</div>
					</form>
				</div>
				<div class="reg_succ" id="re3">
					<img src="<%=basePath%>statics/platform/images/reg_ok.jpg">
					<p>恭喜您注册买卖系统成功！</p>
					<div>审核日期大约为3个工作日，请耐心等待审核结果。</div>
					<a href="<%=basePath%>/platform/loadLoginHtml"><button>返回登录页面</button>
					</a>
				</div>
			</div>
		</div>
	</section>
</body>
</html>