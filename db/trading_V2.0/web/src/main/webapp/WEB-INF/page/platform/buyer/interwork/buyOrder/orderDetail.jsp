<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(function(){
	//var ystepJson = jQuery.parseJSON(${ystep});
	if(${order.status}-6<0){
        $(".ystep1").loadStep({
            size : "large",
            color : "blue",
            steps : ${ystep.stepArray}
        });
        $(".ystep1").setStep(${ystep.length});
    }

});
</script>
<div class="order_top mt">
	<div class="lf order_status">
		<h4 class="mt">当前订单状态</h4>
		<p class="mt text-center">
			<c:choose>
				<c:when test="${ystep.type=='0'}">
					<span class="order_state">${ystep.flag}</span>
				</c:when>
				<c:when test="${ystep.type=='1'}">
					<span class="order_state_cancel">${ystep.flag}</span>
				</c:when>
				<c:when test="${ystep.type=='2'}">
					<span class="order_state_success">${ystep.flag}</span>
				</c:when>
				<c:otherwise>
					<span class="order_state_success">${ystep.flag}</span>
				</c:otherwise>
			</c:choose>
		</p>
		<p class="order_remarks text-center">${ystep.msg}</p>
	</div>
	<div class="lf order_progress">
		<div class="ystep1"></div>
	</div>
</div>
<div>
	<!--订单信息-->
	<h4 class="mt">订单信息</h4>
	<div class="order_d">
		<p class="line mt"></p>
		<p>
			<span class="order_title">供应商信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>供应商名称：<span>${order.suppName}</span>
				</td>
				<td>负责人：<span>${order.person}</span>
				</td>
				<td>手机号：<span>${order.phone}</span>
				</td>
			</tr>
		</table>
		<p class="line mt"></p>
		<p>
			<span class="order_title">订单信息</span>
		</p>
		<table class="table_info">
			<tr>
				<td>订单号：<span>${order.orderCode}</span></td>
				<td>下单日期：<span><fmt:formatDate value="${order.createDate}" type="both"/></span></td>
				<td>下单人：<span>${order.createName}</span></td>
			</tr>
			<%-- <tr>
				<td>要求到货日期：<span><fmt:formatDate value="${order.predictArred}" type="date"/></span></td>
			</tr> --%>
		</table>
		<div class="c66 size_sm orderNote mt">采购商留言：${order.remark}</div>
		<table class="table_pure detailed_list">
			<thead>
				<tr>
					<!-- <td style="width:10%">部门</td> -->
					<td style="width:10%">采购计划编号</td>
					<td style="width:25%">商品</td>
					<td style="width:10%">条形码</td>
					<td style="width:5%">单位</td>
					<td style="width:10%">仓库</td>
					<td style="width:8%">是否含发票</td>
					<td style="width:7%">订单数量</td>
					<td style="width:10%">采购单价</td>
					<td style="width:10%">商品总额</td>
					<td style="width:7%">发货数量</td>
					<td style="width:7%">到货数量</td>
					<td style="width:6%">要求到货日期</td>
					<td style="width:10%">备注</td>

				</tr>
			</thead>
			<tbody>
			<c:forEach var="items" items="${order.orderProductList}">
				<tr>
					<%-- <td>${items.shopName}</td> --%>
					<td>${items.applyCode}</td>
					<td>${items.proCode}|${items.proName }|${items.skuCode}</td>
					<td title="${items.skuOid}">${items.skuOid}</td>
					<td>${items.unitName}</td>
					<td>${items.wareHouseName}</td>
					<td>
						<c:if test="${items.isNeedInvoice=='Y'}">是</c:if>
						<c:if test="${items.isNeedInvoice=='N'}">否</c:if>
					</td>
					<td>${items.goodsNumber}</td>
					<td>${items.price}</td>
					<td>${items.priceSum}</td>
					<td>${items.deliveredNum}</td>
					<td>${items.arrivalNum}</td>
					<td><fmt:formatDate value="${items.predictArred}" type="date"/></td>
					<td>${items.remark}</td>
				</tr>
			</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6">合计</td>
					<td>${order.goodsNum}</td>
					<td></td>
					<td>${order.goodsPrice}</td>
					<td>${order.totalDeliveryNum}</td>
					<td>${order.totalArrivalNum}</td>
					<td></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="text-right" style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'${returnUrl}?${form}','buyer','${menuId}');"><span class="contractBuild">返回</span></a>
	</div>
</div>