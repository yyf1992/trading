<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/contracts.css">
<script type="text/javascript">
$(function(){
	$(".plus-icon").click(function(){
		var selectId = "add";
		var selectType = "1";
		var selectUsers = "";
		if($(this).parent().is("li")){
			//流程选择人员
			$(this).closest("#add").find("input[name='userId']").each(function(){
				selectUsers += $(this).val() + ",";
			});
		}else{
			var selectId = "copyPerson";
			var selectType = "2";
			$(this).parent().find("input[name='userId']").each(function(){
				selectUsers += $(this).val() + ",";
			});
		}
		$.ajax({
			url:"platform/sysVerify/loadPerson",
			data:{
				"page.divId":"layOpen",
				"selectId":selectId,
				"selectType":selectType,
				"selectUsers":selectUsers
			},
			success:function(data){
				layer.open({
					type:1,
					title:"选择人员",
					skin: 'layui-layer-rim',
		  		    area: ['600px', 'auto'],
		  		    content:data,
		  		    yes:function(index,layero){
		  		    }
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
});
function changeVerifyType(obj){
	if($(obj).val()=='0'){
		$("#pocessDiv").show();
	}else if($(obj).val()=='1'){
		$("#pocessDiv").hide();
	}
}
//确认提交
function submitContract(){
	//标题
	var title = $("input[name='title']").val();
	if(title==''){
		layer.msg("审批标题不能为空！",{icon:2});
		return;
	}
	//对应菜单
	var menuId = $("#menuId").val();
	if(menuId==''){
		layer.msg("对应菜单不能为空！",{icon:2});
		return;
	}
	//流程选择人员
	var processUsers = "";
	var verifyType = $("#verifyType").val();
	if(verifyType == '0'){
		$(".size_sm #add li.person").each(function(){
			if($(this).find("input[name='userId']").length>0){
				var userId = $(this).find("input[name='userId']").val();
				var userName = $(this).find("input[name='userName']").val();
				processUsers += userId+","+userName+"@";
			}
		});
		if(processUsers==''){
			layer.msg("请选择审批人员！",{icon:2});
			return;
		}
	}
	//抄送人
	var copyUsers = "";
	$("#copyPerson span.userSpan").each(function(){
		var userId = $(this).find("input[name='userId']").val();
		var userName = $(this).find("input[name='userName']").val();
		copyUsers += userId+","+userName+"@";
	});
	// 确定保存
  	$.ajax({
		type : "POST",
		url : "platform/sysVerify/saveAdd",
		async: false,
		data : {
			"title" : title,
			"menuId" : menuId,
			"verifyType" : verifyType,
			"processUsers" : processUsers,
			"repeatType" : $("#repeatType").val(),
			"copyUsers" : copyUsers,
			"copyType" : $("#copyType").val(),
			"remark" : $("#remark").val()
		},
		success : function(data) {
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				leftMenuClick(this,'platform/sysVerify/sysVerifyList','system','17100920500477275184');
				layer.msg(resultObj.msg,{icon:1});
			}else{
				layer.msg(resultObj.msg,{icon:2});
				return false;
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="content">
    <div class="add_contract">
        <form class="contract_form" action="">
            <div>
                <label><span class="red">*</span> 审批标题:</label>
                <input type="text" value="" name="title">
            </div>
            <div>
                <label><span class="red">*</span> 对应菜单:</label>
                <div class="layui-input-inline contract_input">
                    <select id="menuId" name="menuId">
                        <c:forEach var="menu" items="${sysMenuList}">
                            <option value="${menu.id}">
                            	<c:forEach begin="1" end="${menu.level-1}">&nbsp;&nbsp;&nbsp;&nbsp;</c:forEach>
                            	${menu.name}
                           	</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div>
                <label><span class="red">*</span> 审批类型:</label>
                <div class="layui-input-inline contract_input">
                    <select id="verifyType" name="verifyType" onchange="changeVerifyType(this);">
                        <option value="0">固定审批流程</option>
                        <option value="1">自动审批流程</option>
                    </select>
                </div>
            </div>
            <div class="size_sm" id="pocessDiv">
            	<label><span class="red">*</span> 审批流程:</label>
				<ul id="add">
					<li class="person">
						<span class="userSpan">发起人</span>
					</li>
					<li>
						<img src="${basePath}statics/platform/images/arrow.png">
						<span class="plus-icon"></span>
					</li>
				</ul>
			</div>
			<div>
                <label><span class="red">*</span> 去重规则:</label>
                <div class="layui-input-inline contract_input">
                    <select id="repeatType" name="repeatType">
                        <option value="0">同一个审批人在流程中出现多次时，自动去重</option>
                        <option value="1">同一个审批人仅在连续出现时，自动去重</option>
                    </select>
                </div>
            </div>
            <div class="size_sm" id="copyPerson">
            	<label> 抄送人:</label>
           		<span class="plus-icon"></span>
			</div>
			<div>
                <label> 抄送规则:</label>
                <div class="layui-input-inline contract_input">
                    <select id="copyType" name="copyType">
                        <option value="0">仅全部同意后通知</option>
                        <option value="1">仅发起时通知</option>
                        <option value="2">发起时和全部同意后均通知</option>
                    </select>
                </div>
            </div>
            <div>
                <label>备注:</label>
                <div class="contract_show">
                    <textarea id="remark" name="remark" placeholder="" class="layui-textarea"></textarea>
                </div>
            </div>
        </form>
        <div class="contract_submit">
            <div class="layui-btn layui-btn-normal layui-btn-small"  onclick="submitContract();">确认提交</div>
            <button class="layui-btn layui-btn-primary layui-btn-small" onclick="leftMenuClick(this,'platform/sysVerify/sysVerifyList','system','17100920500477275184');">取消</button>
        </div>
    </div>
</div>