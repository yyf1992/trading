<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
  <form id="editSysForm">
    <div class="account_group">
      <span>登录账户:</span>
     	 ${person.loginName}
    </div>
<!--     <div class="account_group">
      <span>角色:</span>
      <select name="">
        <option value="">普通</option>
        <option value="">财务</option>
        <option value="">业务员</option>
        <option value="">管理员</option>
      </select>
    </div> -->
    <div class="account_group">
      <span>状态:</span>
      <c:if test="${person.isDel==0}">
      	<label><input type="radio" name="isDel" checked value="0"> 正常</label>
      	<label><input type="radio" name="isDel" value="1"> 禁用</label>
      </c:if>
      <c:if test="${person.isDel==1}">
      	<label><input type="radio" name="isDel" value="0"> 正常</label>
      	<label><input type="radio" name="isDel" checked value="1"> 禁用</label>
      </c:if>
    </div>
    <input type="hidden" name="id" value="${person.id}">
  </form>
