<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:76%">
			<ul>
				<li style="width:25%">商品</li>
				<li style="width:10%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">数量</li>
				<li style="width:10%">单价</li>
				<li style="width:10%">总价</li>
				<li style="width:10%">到货数量</li>
				<li style="width:10%">未到货数量</li>
				<li style="width:10%">备注</li>
			</ul>
		</td>
		<td style="width:10%">交易状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list">
		<p>
			<span class="apply_time"><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></span>
			<span class="order_num">订单号:
				<b>${orders.orderCode}</b>
			</span>
			<span>供应商:${orders.suppName}</span>&nbsp;
			<span>下单人:${orders.createName}</span>
		</p>
		<table>
			<tr>
				<td style="width:76%">
				<c:forEach var="product" items="${orders.orderProductList}">
					<ul class="clear">
						<li style="width:25%">
							<span class="defaultImg"></span>
							<div>
									${product.proCode}|${product.proName} <br>
								<span>规格代码:${product.skuCode}</span>
								<span>规格名称:${product.skuName}</span>
							</div></li>
						<li style="width:10%">${product.skuOid}</li>
						<li style="width:5%">${product.unitName}</li>
						<li style="width:10%">${product.goodsNumber}</li>
						<li style="width:10%">${product.price}</li>
						<li style="width:10%">${product.goodsNumber*product.price}</li>
						<li style="width:10%">${product.arrivalNum}</li>
						<li style="width:10%">${product.goodsNumber-product.arrivalNum}</li>
						<li style="width:10%" title="${product.remark}">${product.remark}</li>
					</ul>
				</c:forEach>
				</td>
				<td style="width:10%">
					<div>
						<c:choose>
							<c:when test="${orders.isCheck==0}">待内部审核</c:when>
							<c:when test="${orders.isCheck==1}">
								<c:choose>
									<c:when test="${orders.status==0}">待接单</c:when>
									<c:when test="${orders.status==2}">待对方发货</c:when>
									<c:when test="${orders.status==3}">待收货</c:when>
									<c:when test="${orders.status==4}">已收货</c:when>
									<c:when test="${orders.status==6}">已取消</c:when>
									<c:when test="${orders.status==7}">已驳回</c:when>
								</c:choose>
							</c:when>
							<c:when test="${orders.isCheck==2}">
								<font color="red">内部审核被拒绝</font>
							</c:when>
						</c:choose>
						<br/>
						<div class="opinion_view">
							<span class="orange" data="${orders.id}">查看审批流程</span>
						</div>
					</div>
				</td>
				<td style="width:10%" class="operate">
					<a href="javascript:void(0)"
						onclick="leftMenuClick(this,'platform/buyer/purchase/detail?id=${orders.id}','buyer');"
						class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>详情</a>
					<c:choose>
						<c:when test="${orders.isCheck==0}">
							<br/><a href="javascript:void(0)"
								<%-- onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/loadUpdatePurchaseNew?id=${applyPurchase.id}','buyer');" --%>
								class="layui-btn layui-btn-mini">
								<i class="layui-icon">&#xe8fd;</i>修改</a>
							<br/><a href="javascript:void(0)"
								<%-- onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/loadUpdatePurchaseNew?id=${applyPurchase.id}','buyer');" --%>
								class="layui-btn layui-btn-primary layui-btn-mini">
								<i class="layui-icon">&#xe7ea;</i>删除</a>
						</c:when>
						<c:when test="${orders.isCheck==1}">
							<c:if test="${orders.status==0 || orders.status==7}">
								<br/><a href="javascript:void(0)"
								<%-- onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/loadUpdatePurchaseNew?id=${applyPurchase.id}','buyer');" --%>
								class="layui-btn layui-btn-mini">
								<i class="layui-icon">&#xe8fd;</i>修改</a>
								<br/><a href="javascript:void(0)"
									<%-- onclick="leftMenuClick(this,'buyer/applyPurchaseHeader/loadUpdatePurchaseNew?id=${applyPurchase.id}','buyer');" --%>
									class="layui-btn layui-btn-primary layui-btn-mini">
									<i class="layui-icon">&#xe7ea;</i>取消</a>
							</c:if>
						</c:when>
					</c:choose>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>