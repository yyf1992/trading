<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增预付款</title>
    <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/updateAdvanceEdit.js"></script>
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
</head>

<div id="addAdvanceDiv">
    <%--<div class="layui-row">--%>
        <%--<div class="layui-col-md3 layui-col-md-offset3">--%>

        <%--</div>--%>
    <%--</div>--%>
  <div class="layui-row" style="overflow: hidden">
      <div class="layui-col-md12 layui-col-md-offset1">
          <input type="hidden" id="editId" value="${buyBillRecAdvanceEdit.id}">
          <input type="hidden" id="advanceId" name="advanceId" value="${buyBillRecAdvanceEdit.advanceId}">
          <input type="hidden" id="fileAddressOld" name="fileAddressOld" value="${buyBillRecAdvanceEdit.fileAddress}">
         <%-- <input type="hidden" id="advanceRemarksOld" name="advanceRemarksOld" value="${buyBillRecAdvanceEdit.advanceRemarks}">--%>
          <div class="layui-form-item">
              <label class="labelStyle">预付款账户:</label>
              <input style="width:20%;height: 30px" disabled="disabled" id="sellerCompanyName" name="sellerCompanyName" value="${buyBillRecAdvanceEdit.sellerCompanyName}-${buyBillRecAdvanceEdit.createBillUserName} " >
          </div>
          <div class="layui-form-item">
              <label class="labelStyle">充值金额:</label>
              <input type="number" min="0" name="updateTotal" id="updateTotal"  style="width: 70px;height: 30px;" class="" value="${buyBillRecAdvanceEdit.updateTotal}" placeholder="预付款金额">
          </div>
          <div class="bill_cycle layui-form-item">
              <label class="labelStyle">充值凭证:</label>
              <%--<div class="upload_license" style='width: 150px;height: 20px'>--%>
              <%-- <input type="hidden" id="urlStrOld" value="${buyBillRecAdvance.fileAddress}">--%>
              <input type="file" multiple name="advanceFileAddr" id="advanceFileAddr" onchange="uploadPayment();" style="width: 70px;height: 23px;" >
              <%--</div>--%>
              <p></p>
              <span class="labelStyle" style="white-space: nowrap">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
              <div class="scanning_copy original" id="advanceFileDiv"></div>
          </div>
          <div class="bill_cycle layui-form-item">
              <label class="labelStyle">备注:</label>
              <textarea type="text" id="taskRemarks" name="taskRemarks" style="width: 300px;height: 100px;" class="layui-input">${buyBillRecAdvanceEdit.advanceRemarks}</textarea>
          </div>
          <style>
              .labelStyle{float:left;display:block;padding:9px 15px;width:100px;font-weight:400;text-align:right}
          </style>
      </div>

    </div>
    
  <div class="text-center mp30">
    <a href="javascript:void(0);">
    <button class="layui-btn layui-btn-normal layui-btn-small" id="billCycleInfo_add">确认提交</button>
    <span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/buyer/billAdvanceItem/billAdvanceItemList','buyer');">返回列表</span>
    </a>
  </div>
</div>
 
