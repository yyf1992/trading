<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>买卖系统</title>
	<meta name="keywords" content="诺泰,诺泰买卖,买卖系统,nuotai">
  	<meta name="description" content="诺泰买卖系统是用于商家向供应商采购商品。">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<%@ include file="common/iframeCommon.jsp"%>
</head>
<script>
$(function(){
	var urlData = "${params.urlData}";
	var menuId = "${params.menuId}";
	var b = new Base64();
    var url = b.decode(urlData);
   	loadIframeData(url,menuId);
});
function loadIframeData(url,menuId){
	var contentObj = $(".content");
    var formObj = contentObj.find("form");
	var user = {
		form:formObj.serialize(),
		menuId:menuId
    };
	$.ajax({
		type : "post",
		url : basePath+"platform/common/saveButtonSession",
		data:user,
		async:false,
		success:function(data){
			$.ajax({
				url : basePath+url,
				async:false,
				success:function(data2){
					window.parent.layer.closeAll('loading');
					var str = data2.toString();
					$(".content").empty();
					$(".content").html(str);
				},
				error:function(){
					window.parent.layer.closeAll('loading');
				}
			});
			layuiData();
			loadVerify();
			scroll(0,0);// 显示页面顶部
		},
		error:function(){
			window.parent.layer.closeAll('loading');
		}
	});
	window.parent.layer.closeAll('loading');
}
</script>
<body>
<div class="content"></div>
</body>