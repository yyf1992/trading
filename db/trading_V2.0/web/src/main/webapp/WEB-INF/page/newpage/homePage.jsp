<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
var basePath = "<%=basePath%>";
</script>
<base href="<%=basePath%>"/>
<c:set var="staticsPath" value="${basePath}statics" />
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>买卖系统</title>
	<meta name="keywords" content="诺泰,诺泰买卖,买卖系统,nuotai">
  	<meta name="description" content="诺泰买卖系统是用于商家向供应商采购商品。">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<%-- <%@ include file="common/iframeCommon.jsp"%> --%>
</head>
<script>
</script>
<body>
<div class="">
	<div class="indexRtTitle page_title">我的待审批事项
   		<span class="layui-badge layui-bg-blue"><i class="layui-icon" style="font-size: 12px;">&#xe60e;</i> 待审批</span>
   		<span class="layui-badge layui-bg-blue rt refresh" title="刷新" onclick="window.location.reload();"><i class="layui-icon" style="font-size: 12px;">&#xeac2;</i></span>
 	</div>
 	<div class="pending mp mt">
	   	<div class="pendingCount">
	    	<p>您当前有 <b class="red size_lg">${searchPageUtil.page.count}</b> 个待审批事项</p>
	     	<p class="text-right">
	       		<a href="javascript:void(0)" onclick="viewAll();" class="approval">查看全部 ></a>
	     	</p>
	   	</div>
	   	<div class="pendingItem mp">
	   	<c:forEach var="verifyHeader" items="${searchPageUtil.page.list}" begin="0" end="5" step="1">
	    	<div data-method="offset" data-type="rt" title="审批" onclick="verityThis('${verifyHeader.id}');">
	        	<img src="${staticsPath}/platform/images/approving.png">
	          	<p class="pendItemTitle">
	           		${verifyHeader.title}
	           		<span class="rt size_sm">${verifyHeader.start_date}</span>
	         	</p>
	         	<p class="pendItemNum">审批编号：${verifyHeader.id}</p>
	        	<p class="pendItemName">申 请 人：${verifyHeader.create_name}</p>
	    	</div>
		</c:forEach>
	 	</div>
 	</div>
	<div class="indexRtTitle page_title mt">买卖订单业务流程概览展示
   		<span class="layui-badge layui-bg-green"><i class="layui-icon" style="font-size: 12px;">&#xe641;</i> 流程图</span>
 	</div>
 	<img src="${staticsPath}/platform/images/indexFlow.png">
</div>
</body>