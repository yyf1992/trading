<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--新增sku弹框-->
<form id="skuForm">
	<div class="skuIncrease skuAdd">
		<div>
			<input type="hidden" name="productCode" value="${productCode}">
			<input type="hidden" name="productType" value="${productType}">
			<input type="hidden" name="unitId" value="${unitId}">
			<input type="hidden" name="productName" value="${productName}">
			<span>&emsp;规格代码：</span> <input type="text"
				placeholder="输入规格代码" name="skuCode">
		</div>
		<div>
<!-- 			<span>&emsp;颜&emsp;&emsp;色：</span> <input type="text"
				placeholder="输入商品颜色" name="colorCode"> -->
							<span>&emsp;规格名称：</span> <input type="text"
				placeholder="输入规格代码" name="skuName">
		</div>
		<div>
			<span><span class="red">*</span> 条 形 码：</span> <input type="text"
				placeholder="输入商品条形码" name="barcode" 
				onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
		</div>
		<div>
			<span><span class="red">*</span> 标准库存：</span> <input type="number"
				placeholder="输入商品标准库存" name="standardStock">
		</div>
		<div>
			<span><span class="red">*</span> 库存下限：</span> <input type="number" placeholder="输入商品库存下限"
				name="minStock">
		</div>
		<div>
			<span><span class="red">*</span> 销售单价：</span> <input type="number" placeholder="输入商品销售单价"
				name="price">
		</div>
		<div>
			<span><span class="red">*</span> 销售天数：</span> <input type="number" name="planSalesDays" placeholder="输入销售天数" min="0">
	    </div>
	</div>
</form>