<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
$(function(){
	//加载左侧角色信息
	getLeftNotRole();
	//加载右侧角色信息
	getRightRole();
	$("input:checkbox[name='checkAll']").click(function(){
		var flag = $(this).is(":checked");
		$(this).parents("table").find("tbody input:checkbox").prop("checked",flag);
	});
});
function getRightRole(){
	var userId = $("input[name='instalRoleUserId']").val();
	$("div.rightDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/tradeRole/getRightRole",
		data:{"userId":userId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='rightCheckOne' value='"+data.id+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='roleId' value='"+data.id+"'>";
				trObj += data.roleCode;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.roleName;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.isAdmin=='1'?"是":"否";
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.rightDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取角色数据失败，请稍后重试！",{icon:2});
		}
	});
}
function getLeftNotRole(){
	var userId = $("input[name='instalRoleUserId']").val();
	$("div.leftDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/tradeRole/getLeftNotRole",
		data:{"userId":userId},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='leftCheckOne' value='"+data.id+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='roleId' value='"+data.id+"'>";
				trObj += data.roleCode;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.roleName;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.isAdmin=='1'?"是":"否";
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.leftDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取角色数据失败，请稍后重试！",{icon:2});
		}
	});
}
//添加
function addRole(){
	var userId = $("input[name='instalRoleUserId']").val();
	var checked = $("div.leftDiv table tbody input:checkbox[name='leftCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("左侧角色信息至少选中1条！",{icon:2});
		return;
	}
	var roleIdStr = "";
	checked.each(function(){
		roleIdStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/tradeRole/addUserRole",
		data:{
			"userId":userId,
			"roleIdStr":roleIdStr
		},
		success:function(data){
			//加载左侧角色信息
			getLeftNotRole(userId);
			//加载右侧角色信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取角色数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteRole(){
	var userId = $("input[name='instalRoleUserId']").val();
	var checked = $("div.rightDiv table tbody input:checkbox[name='rightCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("右侧角色信息至少选中1条！",{icon:2});
		return;
	}
	var roleIdStr = "";
	checked.each(function(){
		roleIdStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/tradeRole/deleteUserRole",
		data:{
			"userId":userId,
			"roleIdStr":roleIdStr
		},
		success:function(data){
			//加载左侧角色信息
			getLeftNotRole(userId);
			//加载右侧角色信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取角色数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<div class="roleUser">
	<div class="leftDiv">
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
					<td>角色代码</td>
	                <td>角色名称</td>
	                <td>是否管理员</td>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div class="modelDiv">
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-danger layui-btn-small rt"
			onclick="addRole();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-primary layui-btn-small rt"
			onclick="deleteRole();"><i class="layui-icon">&#xe93e;</i> 删除</a>
	</div>
	<div class="rightDiv">
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
					<td>角色代码</td>
	                <td>角色名称</td>
	                <td>是否管理员</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<input type="hidden" value="${instalRoleUserId}" name="instalRoleUserId">