<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的收款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/billAdvanceItemList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载条件
        $("#id").val("${params.id}");
        $("#reconciliationId").val("${params.reconciliationId}");
        $("#createUserName").val("${params.createUserName}");
        $("#startDate").val("${params.startEditDate}");
        $("#endDate").val("${params.endEditDate}");
        $("#advanceStatus").val("${params.advanceStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    //预付款单据
    var preview={
        LIWIDTH:108,//保存每个li的宽
        $ul:null,//保存小图片列表的ul
        moved:0,//保存左移过的li
        init:function(){//初始化功能
            this.$ul=$(".view>ul");//查找ul
            $(".view>a").click(function(e){//为两个按钮绑定单击事件

                e.preventDefault();
                if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
                    if($(e.target).is(".forward")){//如果是向前按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
                        this.moved++;//移动个数加1
                    }
                    else{//如果是向后按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
                        this.moved--;//移动个数减1
                    }
                    this.checkA();//每次移动完后，调用该方法
                }
            }.bind(this));
            //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
            this.$ul.on("mouseover","li>img",function(){
                var src=$(this).attr("src");//获得当前img的src
                //var i=src.lastIndexOf(".");//找到.的位置
                //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
                $(".big_img>img").attr("src",src);//设置中图片的src
            });
        },
        checkA:function(){//检查a的状态
            if(this.moved==0){//如果没有移动
                $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
            }
           /* else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
                $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
            }*/
            else{//否则，都启用
                $("[class^=backward]").attr("class","backward");
                $("[class^=forward]").attr("class","forward");
            }
        }
    };
    preview.init();

    //重置查询条件
    function resetformData(){
       // alert("进来了吗");
        $("#id").val("");
        $("#reconciliationId").val("");
        $("#createUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#advanceStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<!--内容-->
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/seller/billAdvance/selAdvanceChargeList">
		<ul class="order_search">
			<li class="comm">
				<label>流水单号:</label>
				<input type="text" placeholder="输入充值/对账单号搜索" id="id" name="id" >
			</li>
			<%--<li class="comm">
				<label>对账单号:</label>
				<input type="text" placeholder="请输入对账单号" id="reconciliationId" name="reconciliationId" >
			</li>--%>
			<li class="comm">
				<label>操作人:</label>
				<input type="text" placeholder="请输入申请人名称" id="createUserName" name="createUserName" >
			</li>
			<li class="range nomargin">
				<label>操作日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startEditDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endEditDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<%--<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />--%>
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />


			<%--<div class="rt">
				<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small" onclick="buyerAdvanceExport();">
					<i class="layui-icon">&#xe8bf;</i> 导出
				</a>

			</div>--%>

		</ul>
	</form>
	<div class="aa">
		<table class="table_pure payment_list">
			<thead>
			<tr>
				<td style="width:10%">流水单号</td>
				<%-- <td style="width:10%">抵扣对账单号</td>--%>
				<%--<td style="width:10%">预付款账号</td>--%>
				<%--<td style="width:10%">供应商</td>
                <td style="width:10%">采购员</td>--%>
				<td style="width:10%">流水金额</td>
				<td style="width:10%">操作时间</td>
				<td style="width:8%">操作人</td>
				<td style="width:10%">备注</td>
				<td style="width:10%">凭证</td>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="billAdvanceEdit" items="${advanceEditList}">
				<tr class="breakType">
					<input id="advanceIdOld" type="hidden" value="${billAdvanceEdit.advanceId}">
					<input id="updateTotalOld" type="hidden" value="${billAdvanceEdit.updateTotal}">
					<input id="fileAddressOld" type="hidden" value="${billAdvanceEdit.fileAddress}">
					<input id="advanceRemarksOld" type="hidden" value="${billAdvanceEdit.advanceRemarks}">
					<td style="text-align: center" title="${billAdvanceEdit.id}"> ${billAdvanceEdit.id}</td>
						<%--<td>
                          <c:choose>
                            <c:when test="${billAdvanceEdit.reconciliationId != null && billAdvanceEdit.reconciliationId !=''}">
                                ${billAdvanceEdit.reconciliationId}
                            </c:when>
                            <c:otherwise> -</c:otherwise>
                          </c:choose>
                        </td>--%>
						<%--<td>${billAdvanceEdit.advanceId}</td>
                        <td style="text-align: center" title="${billAdvanceEdit.sellerCompanyName}">
                          ${billAdvanceEdit.sellerCompanyName}</td>
                        <td>${billAdvanceEdit.createBillUserName}</td>--%>
					<td style="text-align: center">
						<c:choose>
							<c:when test="${billAdvanceEdit.reconciliationId != null && billAdvanceEdit.reconciliationId !=''}">
								-${billAdvanceEdit.updateTotal}
							</c:when>
							<c:otherwise>
								${billAdvanceEdit.updateTotal}
							</c:otherwise>
						</c:choose>
					</td>
					<td>${billAdvanceEdit.createTimeStr}</td>
					<td style="text-align: center">${billAdvanceEdit.createUserName}</td>

					<td  style="text-align: center">
						<c:choose>
							<c:when test="${billAdvanceEdit.reconciliationId != null && billAdvanceEdit.reconciliationId !=''}">
								对账单号：${billAdvanceEdit.reconciliationId}
							</c:when>
							<c:otherwise>
								${billAdvanceEdit.advanceRemarks}
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${billAdvanceEdit.reconciliationId == null || billAdvanceEdit.reconciliationId ==''}">
								<span class="layui-btn layui-btn-mini" onclick="showAdvanceEditFile('${billAdvanceEdit.fileAddress}');">充值凭证</span>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="text-center mp30">
		<a href="javascript:void(0);">
			<span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/seller/billAdvance/billAdvanceList','seller');">返回列表</span>
		</a>
	</div>
	<%--<div class="pager">${searchPageUtil.page }</div>--%>
	<%--</form>--%>
</div>
<script>
	function aa() {
        var h = $(window).outerHeight();
        var h1 = h  -  $('.top').outerHeight() - $('.nav').outerHeight() - $('.order_search').outerHeight();
        $('.aa').outerHeight( h1 -150 ).css({overflowX:'hidden',overflowY:'auto'})
    }
	aa();
	$(window).on('resize',function () {
        aa();
    })
</script>


<!--充值票据-->
<div id="advanceEditDiv" class="receipt_content layui-layer-wrap" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<style>
	.view .icon_list {
		height: 92px;
		position:absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
	.view .icon_list li img:hover {
		border: 2px solid #e4393c;
		padding: 0;
	}
</style>