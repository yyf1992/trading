<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>买卖系统</title>
	<meta name="keywords" content="诺泰,诺泰买卖,买卖系统,nuotai">
  	<meta name="description" content="诺泰买卖系统是用于商家向供应商采购商品。">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<%@ include file="common/common.jsp"%>
</head>
<body>
	<!--*******************顶部开始******************* -->
	<%@ include file="common/top.jsp"%>
	<!--*******************顶部结束******************* -->
	<!--*******************左侧菜单开始******************* -->
	<%@ include file="common/menu.jsp"%>
	<!--*******************左侧菜单结束******************* -->
    <!-- 中部开始 -->
    <!-- 左侧菜单开始 -->
    
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
          <ul class="layui-tab-title">
            <li id="homePageLi" lay-id="homePage" class="layui-this">首页</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show" id="homePageDiv">
            	<!-- <iframe tab-id="homePage" frameborder="0" src="homePage" scrolling="yes" class="x-iframe"></iframe>-->
            </div>
          </div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    <!-- 底部开始 -->
    <div class="footer">
        <div class="copyright">Copyright ©2018 山东诺泰健康科技有限公司 v1.0 All Rights Reserved</div>  
    </div>
</body>
</html>