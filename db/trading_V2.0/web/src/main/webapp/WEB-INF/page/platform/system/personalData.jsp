<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../common/path.jsp"%>
<script src="<%=basePath%>statics/plugins/layer/layer.js"></script>
<script src="<%=basePath%>statics/platform/js/toolTip.js"></script>
<script type="text/javascript">
	$(function() {
		loadProvince("updateProvince","${company.province}");
		// 根据省份加载市
		var provinceId = $("#updateProvince").val();
		loadCity(provinceId,"updateCity","${company.city}");
		// 根据市加载区
		var cityId = $("#updateCity").val();
		loadArea(cityId,"updateArea","${company.area}");
		//省改变
		$("#updateProvince").change(function(){
			// 根据省份加载市
			var provinceId = $("#updateProvince").val();
			loadCity(provinceId,"updateCity","");
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		//市改变
		$("#updateCity").change(function() {
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		
		// 修改保存
		$(".data_save").click(function() {
			var area = $("#area").val();
			if(area == ""){
				layer.msg("请填写企业所在地！",{icon:7});
				return;
			}
			var a2 = $("#attachment2Div").find("span").length;
			var a2Str = ""
			if(a2 != 0){
				for(var i = 0;i < a2;i++){
					var url2 = $("#attachment2Div").find("span:eq("+i+")").find("input").val();
					a2Str = a2Str + url2 + "@";
				}
				a2Str = a2Str.substring(0, a2Str.length - 1)
			}
			$.ajax({
				type : "post",
				url : "platform/company/saveUpdateCompany",
				data : {
					"id" : $("#id").val(),
					"a2Str" : a2Str,
					"province" : $("#province").val(),
					"city" : $("#city").val(),
					"area" : $("#area").val(),
					"detailAddress" : $("#detailAddress").val(),
					"zone" : $("#zone").val(),
					"telNo" : $("#telNo").val(),
					"fax1" : $("#fax1").val(),
					"fax2" : $("#fax2").val(),
					"qq" : $("#qq").val(),
					"wangNo" : $("#wangNo").val(),
					"email" : $("#email").val()
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1});
					} else {
						layer.msg(resultObj.msg,{icon:2});
						return false;
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
		});
		//个人资料 删除营业执照
		$(".scanning_copy").on("click","b",function(){
			$(this).parent().remove();
		});
	});
</script>
<div>
	<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
		<ul class="layui-tab-title basic_title">
			<li class="layui-this">基础信息</li>
			<li>个人资料</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<ul class="basic_info">
					<li><label>用户名:</label> <span>${sessionScope.CURRENT_USER.loginName}</span>
					</li>
					<li><label>公司名称:</label> <span>${company.companyName}</span>
					</li>
					<li><label>公司代号:</label> <span>${company.companyCode}</span>
					</li>
					<li><label>营业证件扫描件:</label>
						<c:forEach var="a1" items="${el:getAttachmentByRelatedId(company.id,1)}">
							<div class="scanning">
								<img src="${a1.url}" onMouseOver="toolTip('<img src=${a1.url}>')" onMouseOut="toolTip()">
							</div>
						</c:forEach>
					</li>
					<li><label>联系人:</label> <span>${company.linkMan}</span>
					</li>
				</ul>
			</div>
			<div class="layui-tab-item" style="overflow: hidden;">
                <form action="">
                	<input type="hidden" id="id" name="id" value="${company.id }"/>
                  <ul class="voucher">
                    <li>
                      <label>开户许可证扫描件:</label>
                      <div class="upload_system">
                        <input type="file" multiple name="attachment2" id="attachment2" onchange="fileUpload(this);">
                        <p></p>
                        <span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
                      </div>
                      	<div class="scanning_copy" id="attachment2Div">
		                   	<c:forEach var="a2" items="${el:getAttachmentByRelatedId(company.id,2)}">
								<span><b></b><img src="${a2.url}" onMouseOver="toolTip('<img src=${a2.url}>')" onMouseOut="toolTip()">
		                   		<input type="hidden" name="fileUrl" value="${a2.url}"></span>
							</c:forEach>
		               	</div>
                    </li>
                    <li>
                      <div class="info_city">
                        <label>企业所在地:</label>
						<select id="updateProvince" name="updateProvince"></select>
						<select id="updateCity" name="updatecity"></select>
						<select id="updateArea" name="updateArea"></select>
                      </div>
                    </li>
                    <li>
                      <label>详细地址:</label>
                      <textarea placeholder="请输入详细地址" id="detailAddress">${company.detailAddress}</textarea>
                    </li>
                    <li>
                      <label>电话:</label>
                      	<input type="text" id="zone" value="${company.zone}" placeholder="区号" class="area_code"
							onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
							onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"> - 
						<input type="text" id="telno" value="${company.telNo}" placeholder="座机号" class="phone"
							onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
							onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
                    </li>
                    <li>
                      <label>传真:</label>
                      	<input type="text" id="fax1" value="${company.fax1}" placeholder="区号" class="area_code"
							onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
							onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"> - 
						<input type="text" id="fax2" value="${company.fax2}" placeholder="传真" class="phone"
							onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
							onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
                    </li>
                    <li>
                      <label style="font-family: Arial">QQ:</label>
                      <input type="text" id="qq" value="${company.qq}" placeholder="请输入QQ号"
							onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
							onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}" >
                    </li>
                    <li>
                      <label>旺旺号:</label>
                      <input type="text" id="wangno" value="${company.wangNo}" placeholder="请输入旺旺号">
                    </li>
                    <li>
                      <label>E-mail:</label>
                      <input type="text" id="email" value="${company.email}" placeholder="请输入邮箱">
                    </li>
                  </ul>
                  <div class="btn_save">
	                  <input type="button" class="data_save" value="保存"/>
	              </div>
                </form>
              </div>
		</div>
	</div>
</div>