<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.planList select{
	width: 90%
}
.planList tbody td{
    overflow:visible;
}
</style>
<script type="text/javascript">
$(function(){
	var form = layui.form;
	// 选择供应商获取商品单价
    form.on('select(pageFilter)', function(data){
        var obj = data.elem;
		getProductPrice(obj);
     });  
	$("select[name='supplierName']").trigger("change");//触发供应商下拉的onchange事件
	//仓库下拉
	var wareObj={};
	wareObj.divId="addWarehouseSelect";
	wareObj.selectValue="53";//默认质检仓
	warehouseSelect(wareObj);
	$("#beatchCreateOrderTbody tr").each(function(){
		$(this).find("td:eq(7)").html($("#addWarehouseSelect").html());
	});
	
	// 全部展开
	$("#beatchCreateOrderTbody img[class='rightArrow']").each(function(){
		$(this).hide();
		$(this).next().show();
		var barcode = $(this).parent().parent().find("b").attr("name");
		var tr = document.createElement('tr');
		$(tr).addClass('trInfo');
		$(tr).attr("name","tr_"+barcode);
		tr.innerHTML='<td>计划编号</td><td>部门名称</td><td colspan="2">剩余计划采购数量</td><td>下单日期</td><td>要求到货日期</td><td>备注</td><td>采购审批数量</td><td>删除</td>';
		$(this).parent().parent().after(tr);
		//$(tr).hide();
		$.ajax({
			type : "POST",
			async: false,
			url : "buyer/applyPurchaseHeader/getPurchaseItemList",
			data : {
				"barcode" : barcode
			},
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.list.length > 0){
					var list = resultObj.list;
					for(var i = 0; i < list.length;i++){
						var html = document.createElement('tr');
						$(html).attr("name","trC_"+barcode);
						$(html).addClass('trWarn');
						var time = "";
						var predictArred = "";
						if(typeof(list[i].predictArred) != "undefined"){
							time = list[i].predictArred + "";
							time = time.substring(0, time.length - 3);
							var transTime = new Date( time*1000 );
							predictArred = transTime.getFullYear()+"-"+(transTime.getMonth()+1)+"-"+transTime.getDate();
						}else{
							predictArred = "";
						}
						var time2 = "";
						var createDate = "";
						if(typeof(list[i].create_date) != "undefined"){
							time2 = list[i].create_date + "";
							time2 = time2.substring(0, time2.length - 3);
							var transTime = new Date( time2*1000 );
							createDate = transTime.getFullYear()+"-"+(transTime.getMonth()+1)+"-"+transTime.getDate();
						}else{
							createDate = "";
						}

						$(html).html('<td><input type="hidden" id="orderId_'+list[i].barcode+'_'+i+'" value="'+list[i].id+'">' + list[i].apply_code + '</td>'+
							'<td><input type="hidden" value="'+list[i].shop_id+'">' + list[i].shop_name + '</td>'+
							'<td colspan="2">' + list[i].overplusNum + '</td>'+
							'<td>' + createDate + '</td>'+
							'<td>' + predictArred + '</td>'+
							'<td>' + list[i].remark + '</td>'+
							'<td><input type="number" value="'+ list[i].overplusNum +'" min="0"></td>'+
							'<td><input type="hidden" id="isCustomize" value="'+list[i].is_customize+'"><input type="hidden" id="afterPrice" value="'+list[i].after_price+'">'+
							'<a href="javascript:void(0)" onclick="delRow(this);updateNumber(this);" class="layui-btn layui-btn-primary layui-btn-mini"><i class="layui-icon">&#xe7ea;</i>删除</a></td>');
						$(tr).after(html);
					}
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", { icon : 2 });
			}
		});
	});

	 $("#loadCustom").mouseenter(function(){
		 layer.tips('定制商品单价', '#loadCustom',{
				tips: [3, '#f00'],
	  			time: 1000
			});
	 });
	 $("#restore").mouseenter(function(){
		 layer.tips('恢复商品单价', '#restore',{
				tips: [3, '#f00'],
	  			time: 1000
			});
	 });
	//渲染下拉框
	form.render("select");
	$('.planList').on('click','tbody td:first-child b',function(){
		layer.confirm('<p class="size_sm text-center"><img src="${basePath}statics/platform/images/icon.jpg">&emsp;确定要删除此商品吗？</p>',{
			skin:'popBuyer popB25 btnCenter deleteBuyer',
			title:'提醒',
			closeBtn:0,
			area:['310px','auto']
		},function(index){
			layer.close(index);
			// 删除子集
			var barcode = $(this).attr("name");
			$("tr[name=tr_"+barcode+"]").remove();
			$("tr[name=trC_"+barcode+"]").remove();
			delRow(this);
		}.bind(this));
	});
});
// 更新总数量
function updateNumber(obj){
	var delNumber = $(obj).parents("tr").find("td:eq(2)").text();
	var barcode = $(obj).parents("tr").attr("name");
	barcode = barcode.substring(barcode.indexOf("_")+1,barcode.length);
	var totalNumber = $("b[name="+barcode+"]").parents("tr").find("td:eq(3)").text();
	$("b[name="+barcode+"]").parents("tr").find("td:eq(3)").html(Number(totalNumber) - Number(delNumber));
}
//展开部门采购情况
function showShopPurchase(obj,barcode){
	$(obj).hide();
	$(obj).next().show();
	$("tr[name=tr_"+barcode+"]").show();
	$("tr[name=trC_"+barcode+"]").show();
}
//隐藏表格
function closeShopPurchase(obj,barcode){
	$(obj).hide();
	$(obj).prev().show();
	$("tr[name=tr_"+barcode+"]").hide();
	$("tr[name=trC_"+barcode+"]").hide();
}
// 提交
function submit(){
	var unitError = "";
	var supplierError = "";
	var priceError = "";
	var warehouseError = "";
	var invoiceError = "";
	var infoStr = "";
	var submitError = "";
	$("#beatchCreateOrderTbody b").each(function(i){
		var productCode = $(this).parent().parent().find("td:eq(1)").find("input:eq(0)").val();
		var productName = $(this).parent().parent().find("td:eq(1)").find("input:eq(1)").val();
		var skuCode = $(this).parent().parent().find("td:eq(1)").find("input:eq(2)").val();
		var skuName = $(this).parent().parent().find("td:eq(1)").find("input:eq(3)").val();
		var skuOid = $(this).attr("name");
		//单位
		var unitId = $(this).parent().parent().find("td:eq(2)").find("option:selected").val();
		if(unitId == ''){
			unitError = "第"+(i+1)+"行没有设置单位！";
			return false;
		}
		// 供应商
		var supplier = $(this).parent().parent().find("td:eq(4)").find("option:selected").val();
		var supplierName = $(this).parent().parent().find("td:eq(4)").find("option:selected").text();
		if(supplier == ''){
			supplierError = "第"+(i+1)+"行没有设置供应商！";
			return false;
		}
		var supplierId = supplier.split(",")[0];
		var person = supplier.split(",")[1];
		if(person == ""){
			person = " ";
		}
		var phone = supplier.split(",")[2];
		if(phone == ""){
			phone = " ";
		}
		
		// 采购单价
		var price = $(this).parent().parent().find("td:eq(5)").find("span").html();
		var afterPrice = $(this).parent().parent().find("td:eq(5)").find("input:eq(0)").val();
		var isCustomize = $(this).parent().parent().find("td:eq(5)").find("input:eq(1)").val();
		var customizePrices = $(this).parent().parent().find("td:eq(5)").find("a:eq(0)").text();
		if(price <= 0){
			priceError = "第"+(i+1)+"行没有填写采购单价！";
			return false;
		}
		// 是否要发票
		var invoice = $(this).parent().parent().find("td:eq(6)").find("option:selected").val();
		if(invoice == ''){
			invoiceError = "第"+(i+1)+"行没有设置是否索要发票！";
			return false;
		}
		//仓库
		var warehouseId = $(this).parent().parent().find("td:eq(7)").find("option:selected").val();
		if(warehouseId == ''){
			warehouseError = "第"+(i+1)+"行没有设置仓库！";
			return false;
		}
		var remark = $(this).parent().parent().find("td:eq(8)").find("textarea").val();
		if(remark == ""){
			remark = " ";
		}
		if($("#beatchCreateOrderTbody tr[name^='trC_"+skuOid+"']").length == 0){
			submitError = "提交有误，请重新提交！";
			return false;
		}
		//定义附件数组
		var costItem = {};
		var productInfo = productCode + "," + productName + "," + skuCode + "," + skuName + "," + skuOid
			+ "," + unitId + "," + price + "," + invoice + "," + warehouseId + "," + remark + ","+ afterPrice+","+isCustomize+","+customizePrices;
		costItem["shopList"] = getShopList(productInfo);
		infoStr = infoStr + supplierId + "," + person + "," + phone + "," + supplierName + "," 
				+ JSON.stringify(costItem) + "@";
	});
	infoStr = infoStr.substring(0, infoStr.length - 1);
	if(unitError != ''){
		layer.msg(unitError,{icon:2});
		return;
	}
	if(supplierError != ''){
		layer.msg(supplierError,{icon:2});
		return;
	}
	if(priceError != ''){
		layer.msg(priceError,{icon:2});
		return;
	}
	if(invoiceError != ''){
		layer.msg(invoiceError,{icon:2});
		return;
	}
	if(warehouseError != ''){
		layer.msg(warehouseError,{icon:2});
		return;
	}
	if(submitError != ''){
		layer.msg(submitError,{icon:2});
		return;
	}
	var goodsNumberError = "";
	$("#beatchCreateOrderTbody tr[name^='trC_']").each(function(){
		var goodsNumber = $(this).find("td:eq(6)").find("input").val();
		// 剩余数量
		var surplusNumber = $(this).find("td:eq(2)").text();
		if(goodsNumber <= 0){
			goodsNumberError = "请填写采购审批数量！";
			return false;
		}
		if(Number(goodsNumber) > Number(surplusNumber)){
			goodsNumberError = "采购审批数量不允许大于剩余数量！";
			return false;
		}
	});
	if(goodsNumberError != ''){
		layer.msg(goodsNumberError,{icon:2});
		return;
	}
	$.ajax({
		url:"buyer/applyPurchaseHeader/next",
		data:{
			"infoStr" : infoStr
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function getShopList(productInfo){
	var product = productInfo.split(",");
	var shopList = new Array();
	$("#beatchCreateOrderTbody tr[name='trC_"+product[4]+"']").each(function(){
		var shop = {};
		// 计划单号
		var applyCode = $(this).find("td:eq(0)").text();
		// 部门信息
		var shopId = $(this).find("td:eq(1)").find("input").val();
		// 采购数量
		var goodsNumber = $(this).find("td:eq(6)").find("input").val();
		shop["applyCode"] = applyCode;
		shop["shopId"] = shopId;
		shop["goodsNumber"] = goodsNumber;
		shop["productCode"] = product[0];
		shop["productName"] = product[1];
		shop["skuCode"] = product[2];
		shop["skuName"] = product[3];
		shop["skuOid"] = product[4];
		shop["unitId"] = product[5];
		shop["price"] = product[6];
		shop["invoice"] = product[7];
		shop["warehouseId"] = product[8];
		shop["remark"] = product[9];
		shop["afterPrice"] = product[10];
		shop["isCustomize"] = product[11];
		shop["customizePrices"] = product[12];
		shopList.push(shop);
	});
	return shopList;
}
// 选择供应商获取商品单价
function getProductPrice(obj){
	var skuOid = $(obj).parents("tr").find("td:eq(0) b").attr("name");
	var supplier = $(obj).val();
	var supplierId = supplier.split(",")[0];
	$.ajax({
        url : basePath+"buyer/applyPurchaseHeader/getProductPriceBySupplier",
        data: {
            "supplierId" : supplierId,
            "skuOid" : skuOid
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			$(obj).parents("tr").find("td:eq(5) span").html(resultObj.price);//采购单价
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//定制单价
function loadCustomPrice(barcode){
	var purchasePrices = $("#prices_"+barcode).html();
	 if(purchasePrices <= 0||purchasePrices==""||purchasePrices==undefined||purchasePrices==null){
        layer.msg("请先在供应商商品价格设置处设置价格！",{icon:2});
        return false;
		} 
    layer.open({
        type: 1,
        title: '定制商品价格',
        area: ['420px', 'auto'],
        skin:'change',
        closeBtn:2,
        btn:['确定','关闭'],
        content: $('#customizePricesDiv'),
        yes : function(index){
        	var idStr = "";
            $("#beatchCreateOrderTbody tr[name$='"+barcode+"']").each(function(i){
            	id = $("#orderId_"+barcode+"_"+i).val();
            	if (id != undefined){
            		idStr += "'" + id + "'" + ",";
            	}            });
            var	customizePrices = $("#priceLatitude").val();
            if(customizePrices==""||customizePrices==undefined||customizePrices==null){
                layer.msg("价格不能为空！",{icon:2});
                return false;
			}
            var purchasePrices = $("#prices_"+barcode).html();
            var barcodeStr = "";
            $("#beatchCreateOrderTbody tr[name='apItem']").each(function(i){
            	barcodeStr += "'" + $(this).find("td:eq(0) b").attr("name") + "'" + ",";
            });
            var customizeType = $("#priceStatus").val();
            var url = "buyer/applyPurchaseHeader/addCustomPrice";
            $.ajax({
                type : "POST",
                url : url,
                data: {    
                	"idStr":idStr,
                	"barcodeStr":barcodeStr,
                    "barcode": barcode,
                    "purchasePrices":purchasePrices,
                    "customizePrices": customizePrices,
                    "customizeType":customizeType
                },
                async:false,
                success:function(data){
                	layer.closeAll();
                	layer.msg("价格定制提交成功！",{icon:1});
                	var str = data.toString();
        			$(".content").html(str);
        			
                },
                error:function(){
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
            });
        }
    });
}
//恢复商品原价
function cancelOrder(barcode) {
    layer.confirm('确认恢复商品原价吗?', {
        icon:3,
        title:'提示',
        skin:'pop',
        closeBtn:2,
        btn: ['确定','取消']
    }, function(index){
       
    	var idStr = "";
        var	customizePrices = null;
        var purchasePrices = $("#prices_"+barcode).html();
        var barcodeStr = "";
        $("#beatchCreateOrderTbody tr[name='apItem']").each(function(i){
        	barcodeStr += "'" + $(this).find("td:eq(0) b").attr("name") + "'" + ",";
        });
        var customizeType = 0;
        $.ajax({
            type : "POST",
            url : "buyer/applyPurchaseHeader/addCustomPrice",
            async: false,
            data : {
            	"idStr":idStr,
            	"barcodeStr":barcodeStr,
                "barcode": barcode,
                "purchasePrices":purchasePrices,
                "customizePrices": customizePrices,
                "customizeType":customizeType
            },
            success : function(data) {
            	layer.close(index);
                layer.msg("操作成功", {icon: 1});
               	var str = data.toString();
       			$(".content").html(str);
            },
            error : function() {
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    });
}
</script>
<div id="orderList" style="width: 100%; overflow: auto;">
	<!--采购列表部分-->
	<form class="">
		<table class="planList">
			<thead>
				<tr>
					<td style="width:150px">删除</td>
	                <td style="width:195px">商品</td>
	                <td style="width:80px">单位</td>
	                <td style="width:44px">数量</td>
	                <td style="width:150px">供应商</td>
	                <td style="width:100px">采购单价</td>
	                <td style="width:70px">索要发票</td>
	                <td style="width:130px">仓库</td>
	                <td style="width:88px">备注</td>
				</tr>
			</thead>
			<tbody id="beatchCreateOrderTbody">
				<c:forEach var="apItem" items="${apItemList}" varStatus="status">
					<tr name="apItem">
						<td><b name="${apItem.barcode}"></b></td>
						<td style="white-space: normal;">
							${apItem.productCode}|${apItem.productName}|${apItem.skuCode}|${apItem.skuName}|${apItem.barcode}
							<input type="hidden" value="${apItem.productCode}">
							<input type="hidden" value="${apItem.productName}">
							<input type="hidden" value="${apItem.skuCode}">
							<input type="hidden" value="${apItem.skuName}">
							<img src="${basePath}statics/platform/images/right.png" class="rightArrow" onclick="showShopPurchase(this,'${apItem.barcode}');">
                  			<img src="${basePath}statics/platform/images/down.png" class="downArrow" onclick="closeShopPurchase(this,'${apItem.barcode}');">
						</td>
						<td><div id="unitDiv_${status.count}"></div></td>
						<td>${apItem.applyCount - apItem.alreadyPlanPurchaseNum - apItem.lockGoodsNumber}</td>
						<td>
							<select name="supplierName" id="supplierId" onchange="getProductPrice(this);" lay-search="" lay-filter="pageFilter">
								<c:forEach var="supplier" items="${apItem.supplierLinksList}">
									<option value="${supplier.supplierId},${supplier.person},${supplier.phone}" >${supplier.supplierName}</option>
								</c:forEach>
							</select>
						</td>
						<td>
											
							<c:if test="${apItem.afterPrice != null}">
							<input type="hidden" id="afterPrice" value="${apItem.afterPrice}">	
							<input type="hidden" id="isCustomize" value="${apItem.isCustomize}">
								原价：<span id="prices_${apItem.barcode}"></span><br>
								加价：<a>${apItem.customizePrices}</a>						
								<br>
								<img src="${basePath}statics/platform/images/loadCustomPrice.png" id="loadCustom" onclick="loadCustomPrice('${apItem.barcode}');">
								<a href="javascript:void(0)" onclick="cancelOrder('${apItem.barcode}');" id="restore" class="layui-btn layui-btn-primary layui-btn-mini">
									<i class="layui-icon">&#xe92b;</i>
								</a>
							</c:if>
							<c:if test="${apItem.afterPrice == null}">
								<span id="prices_${apItem.barcode}"></span><br>
								<img src="${basePath}statics/platform/images/loadCustomPrice.png" id="loadCustom" onclick="loadCustomPrice('${apItem.barcode}');">
							</c:if>											
						</td>
						<td><select name="invoice">
								<option value="N">否</option>
								<option value="Y">是</option>
						</select></td>
						<td></td>
						<td><textarea name="remarks" placeholder="备注内容"><c:if test="${apItem.afterPrice != null}">(定制商品)</c:if></textarea></td>
					</tr>
					<script>
						//初始化
						//单位下拉
						var unitObj={};
						unitObj.divId="unitDiv_${status.count}";
						unitObj.selectValue="${apItem.unitId}";
						unitSelect(unitObj);
					</script>
				</c:forEach>
			</tbody>
		</table>
	</form>
	<div class="text-right">
		<a onclick="submit();"><button class="next_step">提交</button></a>
	</div>
</div>
<!-- 仓库下拉 -->
<div id="addWarehouseSelect" hidden="true"></div>
<!--商品价格变更-->
<div class="plain_frame price_change" id="customizePricesDiv" style="display: none;">
 	<div class="account_group">
	     <span>定制方式:</span>
	     <select name="priceStatus" id="priceStatus">
	     	<option value="0">加价</option>
	     	<option value="1">提成</option>
	     </select>
    </div>
    <div class="account_group">
	     <span>金额/百分比:</span>
	     <input type="text" name="priceLatitude" id="priceLatitude">
    </div>
</div>