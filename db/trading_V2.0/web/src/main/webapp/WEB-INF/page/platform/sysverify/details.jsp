<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/contracts.css">
<div class="content">
    <div class="add_contract">
        <form class="contract_form" action="">
            <div>
                <label><span class="red">*</span> 审批标题:</label>
                <input type="text" value="${verifyHeader.title}" name="title" disabled="disabled">
            </div>
            <div>
                <label><span class="red">*</span> 对应菜单:</label>
                <div class="layui-input-inline contract_input">
                    <select id="menuId" name="menuId" disabled="disabled">
                        <c:forEach var="menu" items="${sysMenuList}">
                            <option value="${menu.id}" <c:if test="${verifyHeader.menuId==menu.id}">selected</c:if>>${menu.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div>
                <label><span class="red">*</span> 审批类型:</label>
                <div class="layui-input-inline contract_input">
                	<select id="verifyType" name="verifyType" disabled="disabled">
                        <option value="0" <c:if test="${verifyHeader.verifyType==0}">selected</c:if>>固定审批流程</option>
                        <option value="1" <c:if test="${verifyHeader.verifyType==1}">selected</c:if>>自动审批流程</option>
                    </select>
                </div>
            </div>
           <c:if test="${verifyHeader.verifyType==0}">
	            <div class="size_sm" id="pocessDiv">
	            	<label><span class="red">*</span> 审批流程:</label>
					<ul id="add">
						<li class="person">
							<span class="userSpan">发起人</span>
						</li>
					<c:forEach var="pocess" items="${pocessList }">
						<li class="person">
							<img src="${basePath}statics/platform/images/arrow.png">
							<span class="userSpan">
								<b title="删除" style="display: none;"></b>
								<input type="hidden" name="userId" value="${pocess.userId}">
								<input type="hidden" name="userName" value="${pocess.userName}">
								${pocess.userName}
							</span>
						</li>
					</c:forEach>
					</ul>
				</div>
           </c:if>
			<div>
                <label><span class="red">*</span> 去重规则:</label>
                <div class="layui-input-inline contract_input">
                	<select id="repeatType" name="repeatType" disabled="disabled">
                        <option value="0" <c:if test="${verifyHeader.repeatType==0}">selected</c:if>>同一个审批人在流程中出现多次时，自动去重</option>
                        <option value="1" <c:if test="${verifyHeader.repeatType==1}">selected</c:if>>同一个审批人仅在连续出现时，自动去重</option>
                    </select>
                </div>
            </div>
            <div class="size_sm" id="copyPerson">
            	<label> 抄送人:</label>
            <c:forEach var="copy" items="${copyList }">
					<span class="userSpan">
						<input type="hidden" name="userId" value="${copy.userId}">
						<input type="hidden" name="userName" value="${copy.userName}">
						${copy.userName}
					</span>
				</c:forEach>
			</div>
			<div>
                <label> 抄送规则:</label>
                <div class="layui-input-inline contract_input">
                	<select id="copyType" name="copyType" disabled="disabled">
                        <option value="0" <c:if test="${verifyHeader.copyType==0}">selected</c:if>>仅全部同意后通知</option>
                        <option value="1" <c:if test="${verifyHeader.copyType==1}">selected</c:if>>仅发起时通知</option>
                        <option value="2" <c:if test="${verifyHeader.copyType==2}">selected</c:if>>发起时和全部同意后均通知</option>
                    </select>
                </div>
            </div>
            <div>
                <label>备注:</label>
                <div class="contract_show">
                	<textarea id="remark" name="remark" placeholder="" class="layui-textarea" readonly="readonly">${verifyHeader.remark}</textarea>
                </div>
            </div>
            <input name="id" type="hidden" value="${verifyHeader.id}">
        </form>
        <div class="contract_submit">
            <button class="layui-btn layui-btn-primary layui-btn-small" onclick="leftMenuClick(this,'platform/sysVerify/sysVerifyList','system','17100920500477275184');">取消</button>
        </div>
    </div>
</div>