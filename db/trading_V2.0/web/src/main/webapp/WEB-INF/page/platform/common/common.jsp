<%@ page language="java" import="java.util.*,com.nuotai.trading.utils.ShiroUtils" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="basePath" value="<%=basePath %>" />
<c:set var="staticsPath" value="${basePath}statics" />
<!--当前登录用户ID-->
<c:set var="userId" value="<%=ShiroUtils.getUserId() %>" />
<!--当前登录用户公司ID-->
<c:set var="companyId" value="<%=ShiroUtils.getCompId() %>" />
<script type="text/javascript">
    var basePath = "${basePath}";
    var loginUserId = "${userId}";
    var loginCompanyId = "${companyId}";
</script>
<!-- css -->
<link rel="stylesheet" href="${staticsPath}/menu/css/menu.css">
<link rel="stylesheet" type="text/css" href="${staticsPath}/menu/font/iconfont.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/jquery-ui-1.11.4/jquery-ui.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/common.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/purchase.css">
<link rel="stylesheet" href="${staticsPath}/plugins/layui/css/layui.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/system.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/supplier_add.css">
<link rel="stylesheet" href="${basePath}statics/plugins/ztree/css/zTreeStyle/zTreeStyle.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/ystep/ystep.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/purchase_manual.css">
<link rel="stylesheet" href="${staticsPath}/css/nuotai.css">
<%--<link rel="stylesheet" href="${staticsPath}/platform/css/seller_order.css">--%>
<link rel="stylesheet" href="${staticsPath}/platform/css/print.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/seller.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/index.css">

<!-- js -->
<script src="${staticsPath}/platform/js/j.js"></script>
<script src="${staticsPath}/menu/js/menu.js"></script>
<script src="${staticsPath}/platform/js/html2canvas.js"></script>
<script src="${staticsPath}/platform/js/sweetTitles.js"></script>
<script src="${staticsPath}/plugins/layer/layer.js"></script>
<script src="${staticsPath}/plugins/layui/layui.js"></script>
<script src="${staticsPath}/platform/css/jquery-ui-1.11.4/jquery-ui.js"></script>
<script src="${staticsPath}/platform/js/colResizable-1.6.min.js"></script>
<script src="<%=basePath%>js/common.js"></script>
<script src="${staticsPath}/platform/js/common.js"></script>
<script src="${staticsPath}/platform/js/commodity.js"></script>
<script src="${staticsPath}/platform/js/5-1-1-1.js"></script>
<script src="${staticsPath}/platform/js/system.js"></script>
<script src="${staticsPath}/platform/js/Region.js"></script>
<script src="${staticsPath}/platform/js/PCASClass.js"></script>
<script src="${staticsPath}/platform/js/supplier.js"></script>
<%-- <script src="${staticsPath}/platform/js/approval.js"></script>
<script src="${staticsPath}/platform/js/friend_add.js"></script> --%>
<script src="${staticsPath}/platform/js/supplier_order.js"></script>
<script src="${staticsPath}/platform/js/upload.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.all.min.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.core.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.excheck.js"></script>
<script src="${staticsPath}/platform/js/ystep.js"></script>
<script src="${staticsPath}/platform/js/verify.js"></script>
<script src="${staticsPath}/platform/js/purchase.js"></script>
<script src="${staticsPath}/dingding/js/base64.js"></script>
