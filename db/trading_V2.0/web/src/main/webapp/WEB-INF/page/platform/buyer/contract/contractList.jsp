<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <title>合同列表</title>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/contracts.css">
    <script src="<%=basePath%>/statics/platform/js/contract.js"></script>
    <script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
    <script type="text/javascript">
        var role='${role}';
        var addContractUrl = "";
        if(role=="seller"){
            addContractUrl = "platform/seller/contract/addContract";
        }else {
            addContractUrl = "platform/buyer/contract/addContract";
        }
        layui.use('form', function(){
        	var form = layui.form;
            form.render("select");
        });
        //标签页改变
        function setStatus(obj,status) {
            $("#status").val(status);
            $('.tab a').removeClass("hover");
            $(obj).addClass("hover");
            loadContractList();
        }
        //提交审批
        function buyerAprove(contractId) {
            
            var url = "platform/"+role+"/contract/subAprove";
            $.ajax({
                url : url,
                async : false,
                data : {
                    "contractId" : contractId,
                    "menuName":"17071814440268433504"
                },
                success : function(data) {
                    if(data.flag){
                        var res = data.res;
                        if(res.code==40000){
                            //调用成功
                            saveSucess(res.data);
                        }else if(res.code==40010){
                            //调用失败
                            layer.msg(res.msg,{icon:2});
                            return false;
                        }else if(res.code==40011){
                            //需要设置审批流程
                            layer.msg(res.msg,{time:500,icon:2},function(){
                                setApprovalUser(url,res.data,function(data){
                                    saveSucess(data);
                                });
                            });
                            return false;
                        }else if(res.code==40012){
                            //对应菜单必填
                            layer.msg(res.msg,{icon:2});
                            return false;
                        }else if(res.code==40013){
							//不需要审批
							notNeedApproval(res.data,function(data){
								 saveSucess(data);
							});
							return false;
						}
                    }else{
                        layer.msg("获取数据失败，请稍后重试！",{icon:2});
                        return false;
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！", {icon : 2});
                }
            });
        }
        //提交审批成功页面
        function saveSucess(res){
            layer.msg('提交审批成功！',{icon:1});
            loadContractList();
        }
        //卖家审批
        function sellerAprove(contractId) {
            $.ajax({
                type : "POST",
                url : "platform/seller/contract/subAprove",
                data: {
                    "contractId": contractId
                },
                async:false,
                success:function(data){
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("审批成功！",{icon:1});
                        loadContractList();
                    }else{
                        layer.msg(data.msg,{icon:1});
                    }

                },
                error:function(){
                    layer.msg("操作失败，请稍后重试！",{icon:2});
                }
            });
        }
    </script>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
    <a href="javascript:void(0);" onclick="setStatus(this,'')" <c:if test="${searchPageUtil.object.status eq ''}">class="hover"</c:if>>所有（<span>${totalCount}</span>）</a> <b>|</b>
    <c:if test="${role == 'buyer'}">
        <a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.status eq '0'}">class="hover"</c:if>>草稿（<span>${draftCount}</span>）</a> <b>|</b>
        <a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.status eq '1'}">class="hover"</c:if>>审批中（<span>${myApprovalCount}</span>）</a> <b>|</b>
        <a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.status eq '2'}">class="hover"</c:if>>待对方审批（<span>${customerApprovalCount}</span>）</a> <b>|</b>
    </c:if>
    <c:if test="${role == 'seller'}">
        <a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.status eq '1'}">class="hover"</c:if>>待我审批（<span>${myApprovalCount}</span>）</a> <b>|</b>
    </c:if>
    <a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.status eq '3'}">class="hover"</c:if>>待传合同照（<span>${contractPhotoCount}</span>） </a><b>|</b>
    <a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.status eq '4'}">class="hover"</c:if>>协议达成（<span>${okCount}</span>） </a><b>|</b>
    <%--<a href="javascript:void(0);" onclick="setStatus(this,'5')" <c:if test="${searchPageUtil.object.status eq '5'}">class="hover"</c:if>>已驳回（<span>${rejectCount}</span>） </a>--%>
</div>
<div>
    <!--搜索栏-->
    <form id="searchForm" action="platform/buyer/contract/contractList" >

        <ul class="order_search contract_s">
            <li class="comm">
                <label>&nbsp;发起人:</label>
                <input type="text" placeholder="" name="sponsorName" value="${searchPageUtil.object.sponsorName}"/>
            </li>
            <li class="comm">
                <label>客户名称:</label>
                <input type="text" placeholder="" name="customerName" value="${searchPageUtil.object.customerName}"/>
            </li>
            <li class="comm">
                <label>合同编号:</label>
                <input type="text" placeholder="" name="contractNo" value="${searchPageUtil.object.contractNo}"/>
            </li>

            <li class="comm">
                <label>合同名称:</label>
                <input type="text" placeholder="" name="contractName" value="${searchPageUtil.object.contractName}"/>
            </li>
            <li class="range nomargin">
                <label>状态:</label>
                <div class="layui-input-inline">
                    <select id="status" name="status" lay-filter="aihao" >
                        <option value=""  <c:if test="${searchPageUtil.object.status eq ''}">selected="selected"</c:if>>全部</option>

                        <c:if test="${role == 'buyer'}">
                            <option value="0" <c:if test="${searchPageUtil.object.status eq '0'}">selected="selected"</c:if>>草稿</option>
                            <option value="1" <c:if test="${searchPageUtil.object.status eq '1'}">selected="selected"</c:if>>审批中</option>
                            <option value="2" <c:if test="${searchPageUtil.object.status eq '2'}">selected="selected"</c:if>>待对方审批</option>
                        </c:if>
                        <c:if test="${role == 'seller'}">
                            <option value="1" <c:if test="${searchPageUtil.object.status eq '1'}">selected="selected"</c:if>>待我审批</option>
                        </c:if>
                        <option value="3" <c:if test="${searchPageUtil.object.status eq '3'}">selected="selected"</c:if>>待传合同照</option>
                        <option value="4" <c:if test="${searchPageUtil.object.status eq '4'}">selected="selected"</c:if>>协议达成</option>
                        <%--<option value="5" <c:if test="${searchPageUtil.object.status eq '5'}">selected="selected"</c:if>>已驳回</option>--%>
                    </select>
                </div>
            </li>

            <li class="spec nomargin">
                <label>创建日期:</label>
                <div class="layui-input-inline">
                    <input type="text" name="createDateStart" id="createDateStart" lay-verify="date" value="${searchPageUtil.object.createDateStart}" class="layui-input" placeholder="开始日">
                </div>
                -
                <div class="layui-input-inline">
                    <input type="text" name="createDateEnd" id="createDateEnd" lay-verify="date" value="${searchPageUtil.object.createDateEnd}" class="layui-input" placeholder="截止日">
                </div>
            </li>
            <li class="range">
                <button type="button" class="search" onclick="loadContractList();">搜索</button>
            </li>
            <c:if test="${role == 'buyer'}">
                <a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="leftMenuClick(this,'platform/buyer/contract/addContract','buyer')">
                    <i class="layui-icon">&#xe64c;</i>创建合同
                </a>
            </c:if>
            <!-- 分页隐藏数据 -->
            <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
            <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
        </ul>
        <!--列表区-->
        <table class="table_pure contract_list">
            <thead>
                <tr>
                    <td width="0" style="display: none;">合同ID</td>
                    <td width="10%">合同编号</td>
                    <td width="10%">合同名称</td>
                    <td width="10%">发起人</td>
                    <td width="10%">客户名称</td>
                    <td width="8%">创建日期</td>
                    <td width="8%">备注</td>
                    <c:choose>
                        <c:when test="${searchPageUtil.object.status eq '4' }">
                            <td width="11%">合同原件</td>
                            <td width="10%">有效开始日期</td>
                            <td width="10%">有效截止日期</td>
                        </c:when>
                        <c:otherwise></c:otherwise>
                    </c:choose>
                    <td width="22%">状态</td>
                    <td width="12%">操作</td>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="contract" items="${searchPageUtil.page.list}">
                <tr class="text-center">
                    <td style="display: none;">${contract.id}</td>
                    <td>${contract.contractNo}</td>
                    <td>${contract.contractName}</td>
                    <td>${contract.sponsorName}</td>
                    <td>${contract.customerName}</td>
                    <td><fmt:formatDate value="${contract.createDate}" ></fmt:formatDate></td>
                    <td>${contract.remark}</td>
                    <c:choose>
                        <c:when test="${searchPageUtil.object.status eq '4'}">
                            <td><span class="contract_preview" title="预览" onclick="contractPreview($(this));"></span></td>
                            <td><fmt:formatDate value="${contract.validPeriodStart}" ></fmt:formatDate></td>
                            <td><fmt:formatDate value="${contract.validPeriodEnd}" ></fmt:formatDate></td>
                        </c:when>
                        <c:otherwise></c:otherwise>
                    </c:choose>
                    <td>
                        <c:choose>
                            <c:when test="${contract.status eq '0'}">草稿</c:when>
                            <c:when test="${contract.status eq '1'}">审批中</c:when>
                            <c:when test="${contract.status eq '2'}">待对方审批</c:when>
                            <c:when test="${contract.status eq '3'}">待传合同照</c:when>
                            <c:when test="${contract.status eq '4'}">协议达成</c:when>
                            <%--<c:when test="${contract.status eq '5'}">已驳回</c:when>--%>
                            <c:otherwise>草稿</c:otherwise>
                        </c:choose>
                        <br/>
                        <a href="javascript:void(0);"
                           onclick="leftMenuClick(this,'platform/'+role+'/contract/contractDetail?id='+$(this).parent().parent().find('td')[0].innerText+'&role='+role,'')" target="_blank">合同明细</a>
                        <br/>
                    </td>

                    <td>
                        <c:choose>
                            <c:when test="${role eq 'buyer'}">
                                <c:choose>
                                    <c:when test="${contract.status eq '0'}">
                                        <a href="javascript:void(0)" onclick="buyerAprove($(this).parent().parent().find('td')[0].innerText);" class="layui-btn layui-btn-mini layui-btn-normal">提交审批</a>
                                        <a href="javascript:void(0)" class="layui-btn layui-btn-mini layui-btn-danger"
                                           onclick="leftMenuClick(this,'platform/buyer/contract/updContract?id='+$(this).parent().parent().find('td')[0].innerText,'17112414010932041195')" >修改</a>
                                    </c:when>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                   <%-- 除了买家角色，其他角色的操作--%>
                                    <c:when test="${contract.status eq '1'}">
                                        <a href="javascript:void(0)" onclick="sellerAprove($(this).parent().parent().find('td')[0].innerText);" class="layui-btn layui-btn-mini layui-btn-normal">审批</a>
                                    </c:when>
                                    <c:when test="${contract.status eq '3'}">
                                        <a href="javascript:void(0)" onclick="openUpload($(this));" class="layui-btn layui-btn-mini layui-btn-normal">上传合同照</a>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="pager">${searchPageUtil.page }</div>
    </form>
    <%--合同原件预览--%>
    <div class='contractView layer-photos-demo' id='layer-photos-demo'></div>
    <%--合同原件上传--%>
    <div class='contractView' id='uploadDiv' style="display: none">
        <span style="font-size: 10px;color: red">说明：1. 先在合同明细页中打印本编号的合同内容。--》2. 线下签订合同 --》3. 上传已完成签订的合同照</span>
        <div class="mp">
            <span class="">合同有效日期： </span>
            <div class="layui-input-inline">
                <input type="text" name="validPeriodStart" id="validPeriodStart" lay-verify="date"  class="layui-input" required>
            </div>
            -
            <div class="layui-input-inline">
                <input type="text" name="validPeriodEnd" id="validPeriodEnd" lay-verify="date"  class="layui-input" required>
            </div>

        </div>
        <div class="mp">
            <label>上传合同原件:</label>
            <div class="upload_system layui-inline" style="margin-left: 0">
                <input type="file" multiple name="oriContracts" id="oriContracts" onchange="uploadOriContract(this);">
                <p></p>
                <span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
            </div>
            <div class="scanning_copy original" id="oriContractDiv"></div>
        </div>
    </div>
</div>


