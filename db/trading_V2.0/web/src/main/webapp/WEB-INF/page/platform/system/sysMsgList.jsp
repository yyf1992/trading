<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="layui-tab-item layui-show">
    <div class="system_sel">
        <div>
            <span></span>
            <div>
                <b></b>
                <ul>
                    <li id="msg_empty">清空信息</li>
                    <li id="msg_shield">屏蔽信息</li>
                    <li>全部标记为已读</li>
                </ul>
            </div>
        </div>
        <label><input type="checkbox"> 只看未读</label>
    </div>
    <div class="message_list">
        <div>
            <span>2017-06-12</span>
            <div>
                <b></b>
                <p class="news_title"><span>阿里巴巴集团<i></i></span> <b>18:30:17</b></p>
                <p class="abstract"><a href="#">菜鸟网在杭州举办2017全球智慧物流峰会，本次峰会的主题是“连接升级”,，主要探讨智慧物流新趋势! 在上午的开幕议程中，菜鸟网在杭州举办2017全球智慧物流峰会，本次峰会的主题是“连接升级”,主要探讨智慧物流新趋势!</a></p>
                <p class="recovery"><span></span></p>
            </div>
        </div>
    </div>
</div>