<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<head>
    <title>商品互通明细</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">

    </style>
</head>
<div class="">
    <form action="" class="layui-form">
        <ul class="">
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">商品货号:</label>${buyShopProduct.productCode}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66  text-right">商品名称:</label>${buyShopProduct.productName}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">规格代码:</label>${buyShopProduct.skuCode}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">规格名称:</label>${buyShopProduct.skuName}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">条形码:</label>${buyShopProduct.barcode}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">单位:</label>${buyShopProduct.unitName}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">是否含税:</label>
                <c:if test="${buyShopProduct.isTax == 0}">否</c:if>
				<c:if test="${buyShopProduct.isTax == 1}">是</c:if>
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">是否含运费:</label>
                <c:if test="${buyShopProduct.isFreight == 0}">否</c:if>
				<c:if test="${buyShopProduct.isFreight == 1}">是</c:if>
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">报价有效期:</label>${buyShopProduct.quotationPeriod}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">合作权重:</label>${buyShopProduct.undertakeProportion}%
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">日产能:</label>${buyShopProduct.dailyOutput}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">物流周期:</label>${buyShopProduct.storageDay}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">最小起订量:</label>${buyShopProduct.minimumOrderQuantity}
            </li>
            <c:choose>
				<c:when test="${linktype==0}">
	            	<li class="layui-form-item">
						<label class="layui-col-sm1 c66 text-right">关联供应商:</label>${buyShopProduct.supplierName}
					</li>
					<li class="layui-form-item">
	                	<label class="layui-col-sm1 c66 text-right">原采购单价:</label>${buyShopProduct.price}
		            </li>
		            <li class="layui-form-item">
		                <label class="layui-col-sm1 c66 text-right">调整单价:</label>${buyProductPricealter.alterPrice}
		            </li>
				</c:when>
				<c:when test="${linktype==2}">
					 <li class="layui-form-item">
						<label class="layui-col-sm1 c66 text-right">关联外协:</label>${buyShopProduct.supplierName}
					</li>
					<li class="layui-form-item">
	                	<label class="layui-col-sm1 c66 text-right">原加工费:</label>${buyShopProduct.price}
		            </li>
		            <li class="layui-form-item">
		                <label class="layui-col-sm1 c66 text-right">调整加工费:</label>${buyProductPricealter.alterPrice}
		            </li>
				</c:when>
            </c:choose>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">修改理由:</label>${buyProductPricealter.modifyReason}
            </li>
            <li class="layui-form-item">
                <label class="layui-col-sm1 c66 text-right">备注:</label>${buyShopProduct.note}
            </li>
        </ul>
    </form>
</div>



