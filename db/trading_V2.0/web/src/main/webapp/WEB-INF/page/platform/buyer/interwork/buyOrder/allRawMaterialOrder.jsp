<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
//补发
function deliveryAgain(id){
	$.ajax({
		url:"platform/buyer/buyOrder/deliveryAgain?id="+id,
		type:"get",
		async:false,
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg, {time:5000},{icon:1});
			}else{
				layer.msg(resultObj.msg, {time:5000},{icon:2});
			}
			leftMenuClick(this,'platform/buyer/buyOrder/rawMaterialOrderList','buyer','17112115052543698084');
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}

function autoAcceptOrder(id){
	$.ajax({
		url:"platform/buyer/buyOrder/verifySuccess?id="+id,
		type:"get",
		async:false,
		success:function(data){
			var result=eval('('+data+')');
			var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg, {time:5000},{icon:1});
			}else{
				layer.msg(resultObj.msg, {time:5000},{icon:2});
			}
			leftMenuClick(this,'platform/buyer/buyOrder/rawMaterialOrderList','buyer','17112115052543698084');
		},
		error:function(){
			layer.msg("获取数据失败，稍后重试！",{icon:2});
		}
	});
}

//生成对账单
function startPay(orderId){
	var url="platform/buyer/buyOrder/startReconciliation";
	$.ajax({
		url:url,
		type:"get",
		data:{"orderId":orderId},
		async:false,
		success : function(data) {
			var result=eval('('+data+')');
	    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
		    if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1,time:2000},function(){
					//跳转对账管理页面
					leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer','17071814392298054492','账单列表');
				});
			}else{
				layer.msg(resultObj.msg,{icon:2,time:2000},function(){
					leftMenuClick(this,'platform/buyer/buyOrder/rawMaterialOrderList?tabId=${searchPageUtil.object.tabId}','buyer','17070718432862058337');
				});
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}

</script>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:80%">
			<ul>
				<li style="width:25%">商品</li>
				<li style="width:10%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">订单数量</li>
				<li style="width:5%">单价</li>
				<li style="width:5%">总价</li>
				<li style="width:10%">到货数量</li>
				<li style="width:10%">未到货数量</li>
				<li style="width:10%">要求到货日期</li>
				<li style="width:10%">备注</li>
			</ul></td>
		<td style="width:10%">交易状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list">
		<p>
			<span class="layui-col-sm3">订单日期:<b><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></b></span>
			<span class="layui-col-sm3">订单号:<b>${orders.orderCode}</b></span>
			<span class="layui-col-sm3">供应商:<b>${orders.suppName}</b></span>
			<span class="layui-col-sm3">下单人:<b>${orders.createName}</b></span>
		</p>
		<table>
			<tr>
				<td style="width:80%">
				<c:forEach var="product" items="${orders.orderProductList}">
				<c:set var="deliveriedNum" value="0" scope="page"></c:set>
				<c:set var="arr" value="0" scope="page"></c:set>
				<c:set var="totalNum" value="0" scope="page"></c:set>
					<ul class="clear">
						<li style="width:25%">
							<!-- <span class="defaultImg"></span> -->
							${product.proCode}|${product.proName}|${product.skuCode}|${product.skuName}
						</li>
						<li style="width:10%">${product.skuOid}</li>
						<li style="width:5%">${product.unitName}</li>
						<li style="width:10%">${product.goodsNumber}</li>
						<li style="width:5%;text-align: center;">${product.price}</li>
						<li style="width:5%">${product.goodsNumber*product.price}</li>
						<li style="width:10%">${product.arrivalNum}</li>
						<li style="width:10%">${product.goodsNumber-product.arrivalNum}</li>
						<li style="width:10%"><fmt:formatDate value="${product.predictArred}"/></li>
						<li style="width:10%" title="${product.remark}">${product.remark}</li>
						<c:set var="totalNum" value="${product.goodsNumber}" scope="page"></c:set>
					</ul>
					<c:if test="${product.deliveryList !=null && product.deliveryList.size()>0 }">
						<table style="width: 100%">
							<thead>
								<tr style="background: #F7F7F7;height: 20px;">
									<td style="width:30px;">发货单号</td>
									<td style="width:30px;">发货类型</td>
									<td style="padding-top: 3px;width:30px;">发货数量</td>
									<td style="padding-top: 3px;width:30px;">到货数量</td>
									<td style="width:30px;">发货日期</td>
									<td style="width:30px;">到货日期</td>
								</tr>
							<c:forEach items="${product.deliveryList}" var="deliveryItem">
								<tr>
									<td>${deliveryItem.deliver_no}</td>
									<td>
										<c:choose>
											<c:when test="${deliveryItem.dropship_type==0}">正常发货</c:when>
											<c:when test="${deliveryItem.dropship_type==1}">代发客户</c:when>
											<c:when test="${deliveryItem.dropship_type==2}">代发菜鸟仓</c:when>
											<c:when test="${deliveryItem.dropship_type==3}">代发京东仓</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${deliveryItem.warehouse_id==52}">代发货</c:when>
													<c:otherwise>正常发货</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</td>
									<td style="padding-top: 3px;">${deliveryItem.delivery_num}</td>
									<c:choose>
										<c:when test="${deliveryItem.recordstatus==0}">
											<!-- 待收货 -->
											<td style="padding-top: 3px;">未到货</td>
											<td>
												<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
											</td>
											<td>未到货</td>
											<c:set var="deliveriedNum" value="${deliveriedNum+ deliveryItem.delivery_num}"></c:set>
										</c:when>
										<c:when test="${deliveryItem.recordstatus==1}">
											<!-- 已收货 -->
											<td style="padding-top: 3px;">${deliveryItem.arrival_num}</td>
											<td>
												<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
											</td>
											<td>
												<fmt:formatDate value="${deliveryItem.arrival_date}" type="date"/>
											</td>
											<c:set var="deliveriedNum" value="${deliveriedNum+ deliveryItem.arrival_num}"></c:set>
										</c:when>
									</c:choose>
								</tr>
							</c:forEach>
							</thead>
						</table>
					</c:if>
				</c:forEach>
				</td>
				<td style="width:10%">
					<div>
						<c:choose>
							<c:when test="${orders.isCheck==0}">待内部审批</c:when>
							<c:when test="${orders.isCheck==1}">
								<c:choose>
									<c:when test="${orders.status==0}">待接单</c:when>
									<c:when test="${orders.status==2}">待发货</c:when>
									<c:when test="${orders.status==3}">待收货</c:when>
									<c:when test="${orders.status==4}">已收货</c:when>
								</c:choose>
							</c:when>
							<c:when test="${orders.isCheck==2}">
								<font color="red">内部审核被拒绝</font>
							</c:when>
						</c:choose>
						<br/>
						<c:choose>
							<c:when test="${orders.reconciliationStatus==0}">未对账</c:when>
							<c:when test="${orders.reconciliationStatus==1}">正在对账</c:when>
							<c:when test="${orders.reconciliationStatus==2}">已对账</c:when>
							<c:when test="${orders.reconciliationStatus==3}">对账驳回</c:when>
							<c:when test="${orders.reconciliationStatus==5}">账单待审批</c:when>
						</c:choose>
						<br/>
						<c:choose>
							<c:when test="${orders.isPaymentStatus==0}">未付款</c:when>
							<c:when test="${orders.isPaymentStatus==1}">已付款</c:when>
							<c:when test="${orders.isPaymentStatus==4}">正在付款</c:when>
							<c:when test="${orders.isPaymentStatus==5}">付款驳回</c:when>
						</c:choose>
						<br/>
						<div class="opinion_view">
							<c:if test="${orders.status!=6}">
								<span class="orange" data="${orders.id}">查看审批流程</span>
							</c:if>
						</div>
					</div>
				</td>
				<td style="width:10%" class="operate">
					<a href="javascript:void(0)" button="详情" onclick="leftMenuClick(this,'platform/buyer/buyOrder/showRawMaterialOrderDetail?id=${orders.id}'+'&returnUrl=platform/buyer/buyOrder/rawMaterialOrderList&menuId=17070718432862058337','buyer','18032910091837605030');" class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>详情
					</a>
					<c:if test="${orders.isCheck==1 && orders.status != 0 && deliveriedNum < totalNum}">
						<span class="layui-btn layui-btn-danger layui-btn-mini" button="补发" onclick="deliveryAgain('${orders.id}');">补发</span>
					</c:if>
					<c:if test="${orders.isCheck==1 && orders.status == 0}">
						<span class="layui-btn layui-btn-danger layui-btn-mini" button="发货" onclick="autoAcceptOrder('${orders.id}');">发货</span>
					</c:if>
					<%-- <c:if test="${orders.status==4 && orders.reconciliationStatus==0 }"> --%>
					<c:if test="${orders.status==4 && orders.reconciliationStatus==0 }">
								<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="startPay('${orders.id}');">生成对账单</span>
					</c:if>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
<!--确认付款-->
<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="billPayment">
		<ul id="billPaymentBody">
		</ul>
</div>