<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
$(function(){
	//加载左侧供应商信息
	getLeftNotRole();
	//加载右侧供应商信息
	getRightRole();
	$("input:checkbox[name='checkAll']").click(function(){
		var flag = $(this).is(":checked");
		$(this).parents("table").find("tbody input:checkbox").prop("checked",flag);
	});
});
function getRightRole(){
	var userId = $("input[name='instalSupplierUserId']").val();
	var supplierName = $("input[name='supplierNameRight']").val();
	$("div.rightDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/buyer/supplier/getRightSupplier",
		data:{
			"userId":userId,
			"supplierName":supplierName
			},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='rightCheckOne' value='"+data.supplierId+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='supplierIdRight' value='"+data.companyCode+"'>";
				trObj += data.companyName;
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.rightDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取供应商数据失败，请稍后重试！",{icon:2});
		}
	});
}
function getLeftNotRole(){
	var userId = $("input[name='instalSupplierUserId']").val();
	var supplierName = $("input[name='supplierNameLeft']").val();
	$("div.leftDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/buyer/supplier/getLeftNotSupplier",
		data:{
			"userId":userId,
			"supplierName":supplierName
			},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='leftCheckOne' value='"+data.supplierId+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='supplierIdRight' value='"+data.companyCode+"'>";
				trObj += data.companyName;
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.leftDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取供应商数据失败，请稍后重试！",{icon:2});
		}
	});
}
//添加
function addRole(){
	var userId = $("input[name='instalSupplierUserId']").val();
	var checked = $("div.leftDiv table tbody input:checkbox[name='leftCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("左侧供应商信息至少选中1条！",{icon:2});
		return;
	}
	var supplierIdStr = "";
	checked.each(function(){
		supplierIdStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/buyer/supplier/addUserSupplier",
		data:{
			"userId":userId,
			"supplierIdStr":supplierIdStr
		},
		success:function(data){
			//加载左侧供应商信息
			getLeftNotRole(userId);
			//加载右侧供应商信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取供应商数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteRole(){
	var userId = $("input[name='instalSupplierUserId']").val();
	var checked = $("div.rightDiv table tbody input:checkbox[name='rightCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("右侧供应商信息至少选中1条！",{icon:2});
		return;
	}
	var supplierIdStr = "";
	checked.each(function(){
		supplierIdStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/buyer/supplier/deleteUserSupplier",
		data:{
			"userId":userId,
			"supplierIdStr":supplierIdStr
		},
		success:function(data){
			//加载左侧供应商信息
			getLeftNotRole(userId);
			//加载右侧供应商信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取供应商数据失败，请稍后重试！",{icon:2});
		}
	});
}
function changePopHeight(){
	  var h = $(window).height();
	  $('.leftDiv').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
	  $('.rightDiv').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
	}
	changePopHeight();
	window.onresize = function(){changePopHeight();}
//搜索左侧数据
function loadleftDivData(){
	//加载左侧店铺信息
	getLeftNotRole();
}
//搜索右侧数据
function loadRightDivData(){
	//加载右侧店铺信息
	getRightRole();
}
</script>
<div class="roleUser">
	<div class="leftDiv">
		<ul>
			<li>
				<input type="text" style="width:140px;height: 25px;" placeholder="输入供应商名称" name="supplierNameLeft" value="">   
           		<button class="search rt" style="width:100px;height: 25px;"  onclick="loadleftDivData();">搜索</button>
         	</li>
		</ul>
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
	                <td>供应商名称</td>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div class="modelDiv">
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-danger layui-btn-small rt"
			onclick="addRole();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-primary layui-btn-small rt"
			onclick="deleteRole();"><i class="layui-icon">&#xe93e;</i> 删除</a>
	</div>
	<div class="rightDiv">
		<ul>
			<li>
				<input type="text" style="width:140px;height: 25px;" placeholder="输入供应商名称" name="supplierNameRight" value="">   
           		<button class="search rt" style="width:100px;height: 25px;"  onclick="loadRightDivData();">搜索</button>
         	</li>
		</ul>
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
	                <td>供应商名称</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<input type="hidden" value="${instalSupplierUserId}" name="instalSupplierUserId">