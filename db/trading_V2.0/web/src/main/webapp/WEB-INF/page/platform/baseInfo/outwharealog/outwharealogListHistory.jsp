<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(outwharealogListHistoryTableFilter)', function(obj){
	    var data = obj.data;
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('outwharealogListHistoryTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	productCode: $("#productCode").val(),
	        	barcode: $("#barcode").val(),
	        	skuCode: $("#skuCode").val(),
	        	subcompanyName: $("#subcompanyName").val(),
	        	shopName: $("#shopName").val(),
	        	whareaName: $("#whareaName").val(),
	        	minDate: $("#minDate").val(),
	        	maxDate: $("#maxDate").val(),
	        	createName: $("#createName").val()
	        }
	      });
	    },
	    //导出
	    exportExcel: function(){
			var formData = [
				"subcompanyName="+$("#subcompanyName").val(),
				"&barcode="+$("#barcode").val(),
				"&productCode="+$("#productCode").val(),
				"&skuCode="+$("#skuCode").val(),
				"&shopName="+$("#shopName").val(),
				"&whareaName="+$("#whareaName").val(),
				"&minDate="+$("#minDate").val(),
				"&maxDate="+$("#maxDate").val(),
				"&createName="+$("#createName").val()
			];
			var url = basePath + "outwharealogHistoryListDownload/outwharealogHistoryListData?" + formData.join("");
			window.open(url);
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="subcompanyName" id="subcompanyName" autocomplete="off" placeholder='子公司'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="shopName" id="shopName" autocomplete="off" placeholder='店铺名称'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-input-inline">
    <select id="whareaName" name="whareaName" lay-verify="required" lay-search="">
	 <option value="">所有仓库</option>
	 <option value="菜鸟仓">菜鸟仓</option>
	 <option value="京东仓">京东仓</option>
    </select>
  </div>
  <div class="layui-inline">
  	<div class="layui-input-inline">
      <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
    </div>
    <div class="layui-input-inline">
      <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
    </div>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="createName" id="createName" autocomplete="off" placeholder='创建人'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="exportExcel">导出</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:20,
		id:'outwharealogListHistoryTable',
		height:'full-90',
		url:'<%=basePath %>platform/baseInfo/outwharealog/loadHistoryDataJson'
	}" lay-filter="outwharealogListHistoryTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'subcompanyName',width:120, sort: true,show:true}">子公司</th>
            <th lay-data="{field:'shopName', sort: true,show:true}">店铺</th>
            <th lay-data="{field:'barcode',width:120, sort: true,show:true,showToolbar:true}">条形码</th>
            <th lay-data="{field:'productCode',width:120, sort: true,show:true}">货号</th>
            <th lay-data="{field:'price', sort: true,show:true}">成本价</th>
            <th lay-data="{field:'outWhareaStock', sort: true,show:true}">外仓在仓数量</th>
            <th lay-data="{field:'outWhareaWayStock', sort: true,show:true}">外仓在途数量</th>
            <th lay-data="{field:'outWhareaTotalPrice', sort: true,show:true}">入外仓库存金额</th>
            <th lay-data="{field:'whareaName', sort: true,show:true}">仓库名称</th>
            <th lay-data="{field:'createName', sort: true,show:true}">创建人</th>
            <th lay-data="{field:'historyDate', sort: true,show:true}">历史时间</th>
        </tr>
    </thead>
</table>