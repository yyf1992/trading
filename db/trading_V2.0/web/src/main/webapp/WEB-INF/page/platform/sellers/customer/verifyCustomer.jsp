<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="plain_frame">
	<form action="">
		<input type="hidden" id="id" name="id" value="${id}">
		<ul>
			<li><span>审批意见:</span> 
			<label><input type="radio" name="agree" value="1" checked> 同意</label>&emsp;&emsp; 
			<label><input type="radio" name="agree" value="2"> 驳回</label></li>
			<li><span>填写备注:</span> <textarea placeholder="请输入内容" id="verifyRemark"></textarea>
			</li>
		</ul>
	</form>
</div>