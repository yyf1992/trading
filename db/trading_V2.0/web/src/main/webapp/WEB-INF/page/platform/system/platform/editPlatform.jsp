<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
  <form id="editSysPlatform">
    <div class="account_group">
      <span>平台代码:</span>
     	<input type="text" name="platformCode" value="${SysPlatform.platformCode}">
    </div>
    <div class="account_group">
      <span>平台名称:</span>
      <input type="text" name="platformName" value="${SysPlatform.platformName}">
    </div>
    <div class="account_group">
      <span>状态:</span>
      <c:if test="${SysPlatform.isDel==0}">
      	<label><input type="radio" name="isDel" checked value="0"> 正常</label>
      	<label><input type="radio" name="isDel" value="1"> 禁用</label>
      </c:if>
      <c:if test="${SysPlatform.isDel==1}">
      	<label><input type="radio" name="isDel" value="0"> 正常</label>
      	<label><input type="radio" name="isDel" checked value="1"> 禁用</label>
      </c:if>
    </div>
    <input type="hidden" name="id" value="${SysPlatform.id}">
  </form>
