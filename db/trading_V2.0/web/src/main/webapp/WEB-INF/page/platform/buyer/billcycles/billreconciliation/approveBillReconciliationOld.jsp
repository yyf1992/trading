<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/billReconciliationList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%--<script src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#reconciliationId").val("${params.reconciliationId}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#billDealStatus").val("${params.billDealStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select");

	});

    //标签页改变
    /*function setStatus(obj,status) {
		$("#billDealStatus").val(status);

        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }*/

    //重置查询条件
    function resetformData(){
        $("#reconciliationId").val("");
    	$("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
        $("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
    	$("#billDealStatus").val("0");
    }

    // 审核
    function approveBillReconciliation(id){
        if($(".verifyDetail").length>0)$(".verifyDetail").remove();
        $.ajax({
            url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
            data:{"id":id},
            success:function(data){
                var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
                detailDiv.addClass("verifyDetail");
                detailDiv.html(data);
                detailDiv.appendTo($("#reconciliationContent"));
                detailDiv.find("#verify_side").addClass("show");
            },
            error:function(){
                layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
            }
        });
    }

    //一键通过
    function verifyAll(){
        var checkedObj = $("#reconciliationContent input[name='checkone']:checked");
        if(checkedObj.length == 0){
            layer.msg('至少选择一条数据！');
            return;
        }else{
            layer.msg('你确定一键通过？', {
                time : 0,//不自动关闭
                btn : [ '确定', '取消' ],
                yes : function(index) {
                    layer.close(index);
                    var orderIdArray = "";
                    $.each(checkedObj,function(i,o){
                        orderIdArray += $(this).val() + ",";
                    });
                    layer.load();
                    $.ajax({
                        url:basePath+"platform/buyer/billReconciliation/verifyBillRecAll",
                        async:false,
                        data:{"purchaseId":orderIdArray},
                        success:function(data){
                            //selectData();
                            layer.msg("审批成功！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billReconciliation/approveBillReconciliation','buyer')
                        },
                        error:function(){
                            layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        }
    }
    function selectAll(obj) {
        if ($(obj).is(":checked")) {
            $("#reconciliationContent input[name='checkone']").prop("checked", true);
        } else {
            $("#reconciliationContent input[name='checkone']").prop("checked", false);
        }
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<div id="reconciliationContent">
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billReconciliation/approveBillReconciliation">

		<ul class="order_search">
			<li class="ship nomargin">
				<label>对账单号:</label>
				<input id="reconciliationId" type='text' placeholder='输入对账单号' name="reconciliationId" />
			</li>
			<li class="ship nomargin">
				<label>供应商名称:</label>
				<input id="sellerCompanyName" type='text' placeholder='输入供应商名称' name="sellerCompanyName" />
			</li>
			<li class="ship nomargin">
				<label>采购员:</label>
				<input type="text"  placeholder='输入采购员' id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="spec nomargin">
				<label>出账日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="ship nomargin">
				<label>账期总金额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
				<!--<button class="search">搜索</button>-->
			</li>
			<li class="range nomargin">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select id="billDealStatus" name="billDealStatus" lay-filter="aihao">
						<option value="0" >全部</option>
						<%--<option value="1">内部等待对账</option>--%>
						<option value="2" >待确认对账</option>
						<option value="3" >已确认对账</option>
						<option value="4" >已驳回对账</option>
						<option value="5" >内部待审批</option>
						<%--<option value="6" >财务待审批</option>--%>
						<option value="7" >内部已驳回</option>
						<%--<option value="8" >财务部驳回</option>--%>
						<option value="9">奖惩待确认</option>
						<option value="10">奖惩已确认</option>
						<option value="11">奖惩已驳回</option>
						<%--<option value="5">供应商对账驳回</option>--%>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<%--<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="buyRecExport();">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>--%>
            <a href="javascript:void(0);"
               class="layui-btn layui-btn-normal layui-btn-small" onclick="verifyAll();">
                <i class="layui-icon">&#xe6a3;</i> 一键通过
            </a>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
	</form>
	<!--列表区-->
	<table class="order_detail">
	  <tr>
		<td style="width:100%">
		  <ul>
			<li style="width:5%">
				<input type="checkbox" name="checkAll" onclick="selectAll(this);">
			</li>
			<li style="width:15%">对账单号</li>
			<li style="width:10%">账单周期</li>
			<li style="width:10%">采购员</li>
			<li style="width:10%">供应商</li>
			<li style="width:10%">采购总数量</li>
		    <li style="width:15%">采购总金额</li>
		    <li style="width:15%">售后总数量</li>
		    <li style="width:15%">售后总金额</li>
		    <li style="width:12%">运费</li>
		    <li style="width:12%">奖惩金额</li>
		    <li style="width:15%">预付款抵扣金额</li>
		    <li style="width:15%">总金额</li>
		    <li style="width:15%">状态</li>
		  </ul>
		</td>
		<td style="width:10%">操作</td>
	  </tr>
	</table>
    <c:forEach var="buyBillReconciliation" items="${searchPageUtil.page.list}">
	 <div class="order_list" onclick="approveBillReconciliation('${buyBillReconciliation.purchaseId}');">
	   <table>
	     <tr>
		   <td style="width:100%">
		     <ul class="clear" style="padding: 10px">
			   <li style="width:15%">${buyBillReconciliation.id}</li>
			   <li style="width:10%">${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</li>
			   <li style="width:10%">${buyBillReconciliation.createBillUserName}</li>
			   <li style="width:10%">${buyBillReconciliation.sellerCompanyName}</li>
			   <li style="width:10%">${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</li>
			   <li style="width:10%">${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</li>
			   <li style="width:10%">${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</li>
			   <li style="width:10%">${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</li>
			   <li style="width:10%">${buyBillReconciliation.freightSumCount}</li>
			   <li style="width:10%">
			     <span class="layui-btn layui-btn-mini" onclick="showCustomPayment('${buyBillReconciliation.id}');">
					   ${buyBillReconciliation.customAmount}</span>
			   </li>
			   <li style="width:10%">
			     <span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.sellerCompanyName}','${buyBillReconciliation.billDealStatus}');">
					   ${buyBillReconciliation.advanceDeductTotal}</span>
			   </li>
			   <li style="width:10%">${buyBillReconciliation.totalAmount}</li>

			   <li style="width:10%">
				 <c:choose>
					 <c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==6}">财务待审批</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
					 <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
				 </c:choose>
			   </li>
				<%-- <li style="width:10%">${buyBillReconciliation.totalAmount}</li>
				 <li style="width:10%">${buyBillReconciliation.totalAmount}</li>
				 <li style="width:10%">${buyBillReconciliation.totalAmount}</li>
				 <li style="width:10%">${buyBillReconciliation.totalAmount}</li>
			   <li style="width:10%" title="${product.remark}">${product.remark}</li>--%>
		     </ul>

			   <div class="mp mt size_sm">
				   <c:set var="priceSum" value="0" scope="page"></c:set>
				   <c:forEach var="daohuoItem" items="${buyBillReconciliation.daohuoMap}">
					   <div>
						   <div class="page_title">
							   <table class="table_n">
								   <tr>
									   <td style="width: 20%"><span ><span class="c99">订单类型：</span>采购到货</span></td>
									   <td style="width: 20%"><span ><span class="c99">运单号：</span></span>${daohuoItem.value.waybillNo}</td>
									   <td style="width: 20%"><span ><span class="c99">物流名称：</span>${daohuoItem.value.logisticsCompany}</span></td>
									   <td style="width: 20%"><span ><span class="c99">司机名称：</span>${daohuoItem.value.driverName}</span></td>
									   <td style="width: 20%"><span ><span class="c99">运费：</span>${daohuoItem.value.freight}元</span></td>
									   <td style="width: 20%"><span ><span class="c99">总金额+运费：</span>${daohuoItem.value.itemPriceAndFreightSum + daohuoItem.value.freight}元</span></td>
								   </tr>

							   </table>
						   </div>
						   <table class="layui-table text-center" lay-even="" lay-size="sm"
								  style="margin: 0;">
							   <thead>
							   <tr>
								   <td style="width:15%">货号</td>
								   <td style="width:10%">商品名称</td>
								   <td style="width:10%">规格代码</td>
								   <td style="width:10%">规格名称</td>
								   <td style="width:13%">条形码</td>
								   <td style="width:27%">采购订单号</td>
								   <td style="width:15%">采购员</td>
								   <td style="width:27%">发货单号</td>
								   <td style="width:27%">入库单号</td>
								   <td style="width:10%">到货日期</td>
								   <td style="width:10%">到货总数量</td>
								   <td style="width:10%">是否开票</td>
								   <td style="width:13%">采购单价</td>
								   <td style="width:13%">对账单价</td>
								   <td style="width:15%">总金额</td>
								   <td style="width:17%" class="oprationRe">操作</td>
							   </tr>
							   </thead>
							   <tbody>
							   <c:set var="arrivalNumSum" value="0" scope="page"></c:set>
							   <c:set var="itemPriceSum" value="0" scope="page"></c:set>
							   <c:forEach var="goodsItem" items="${daohuoItem.value.itemList}">
								   <tr class="breakType">
									   <td>${goodsItem.productCode }</td>
									   <td>${goodsItem.productName }</td>
									   <td>${goodsItem.skuCode }</td>
									   <td>${goodsItem.skuName }</td>
									   <td>${goodsItem.barcode }</td>
									   <td>${goodsItem.orderCode }</td>
									   <td>${goodsItem.createBillName }</td>
									   <td>${goodsItem.deliverNo }</td>
									   <td>${goodsItem.storageNo }</td>
									   <td><fmt:formatDate value="${goodsItem.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
									   <td>${goodsItem.arrivalNum }</td>
									   <td>
										   <c:choose>
											   <c:when test="${goodsItem.isNeedInvoice == 'Y'}">是</c:when>
											   <c:when test="${goodsItem.isNeedInvoice != 'Y'}">否</c:when>
										   </c:choose>
									   </td>
									   <td>${goodsItem.salePrice }</td>
									   <td>${goodsItem.updateSalePrice }</td>
									   <td>${goodsItem.updateSalePrice*goodsItem.arrivalNum }</td>
									   <td class="oprationRe">
										   <div>
											   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${goodsItem.deliverNo}','1');">查看收货凭据</span>
												   <%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${daohuoItem.value.attachmentAddrStr}');">查看收据</span>--%>
												   <%--<c:choose>
                                                       <c:when test="${buyBillReconciliation.billDealStatus!=3}">
                                                           <a href="javascript:void(0)" onclick="updPrice('${goodsItem.productName}','${goodsItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
                                                       </c:when>
                                                   </c:choose>--%>
										   </div>
									   </td>
								   </tr>
							   </c:forEach>
							   </tbody>
							   <tfoot>
							   <tr>
								   <td>合计</td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td>${daohuoItem.value.arrivalNumSum }</td>
								   <td></td>
								   <td>-</td>
								   <td>-</td>
								   <td>${daohuoItem.value.itemPriceSum }</td>
									   <%--<td>总金额+运费:${itemPriceSum+daohuoItem.value.freight }</td>--%>
								   <td class="oprationRe"></td>
							   </tr>
							   </tfoot>
						   </table>
					   </div>
					   <%--<c:set var="priceSum" value="${priceSum+itemPriceSum+daohuoItem.value.freight }" scope="page"></c:set>--%>
				   </c:forEach>
				   <!--采购换货-->
				   <c:forEach var="recordReplaceItem" items="${buyBillReconciliation.recordReplaceMap}">
					   <div>
						   <div class="page_title">
							   <table class="table_n">
								   <tr>
									   <td style="width: 20%"><span ><span class="c99">订单类型：</span>采购换货</span></td>
									   <td style="width: 20%"><span ><span class="c99">运单号：</span></span>${recordReplaceItem.value.waybillNo}</td>
									   <td style="width: 20%"><span ><span class="c99">物流名称：</span>${recordReplaceItem.value.logisticsCompany}</span></td>
									   <td style="width: 20%"><span ><span class="c99">司机名称：</span>${recordReplaceItem.value.driverName}</span></td>
									   <td style="width: 20%"><span ><span class="c99">运费：</span>${recordReplaceItem.value.freight}元</span></td>
									   <td style="width: 20%"><span ><span class="c99">总金额+运费：</span>${recordReplaceItem.value.itemPriceAndFreightSum+recordReplaceItem.value.freight}元</span></td>
								   </tr>

							   </table>
						   </div>
						   <table class="layui-table text-center" lay-even="" lay-size="sm"
								  style="margin: 0;">
							   <thead>
							   <tr>
								   <td style="width:15%">货号</td>
								   <td style="width:10%">商品名称</td>
								   <td style="width:10%">规格代码</td>
								   <td style="width:10%">规格名称</td>
								   <td style="width:10%">条形码</td>
								   <td style="width:25%">采购订单号</td>
								   <td style="width:15%">采购员</td>
								   <td style="width:30%">发货单号</td>
								   <td style="width:30%">入库单号</td>
								   <td style="width:10%">到货日期</td>
								   <td style="width:10%">到货总数量</td>
								   <td style="width: 10%">是否开票</td>
								   <td style="width:12%">返修单价</td>
								   <td style="width:12%">对账单价</td>
								   <td style="width:15%">总金额</td>
								   <td style="width:15%" class="oprationRe5">操作</td>
							   </tr>
							   </thead>
							   <tbody>
							   <c:forEach var="recReplaceItem" items="${recordReplaceItem.value.itemList}">
								   <tr class="breakType">
									   <td>${recReplaceItem.productCode }</td>
									   <td>${recReplaceItem.productName }</td>
									   <td>${recReplaceItem.skuCode }</td>
									   <td>${recReplaceItem.skuName }</td>
									   <td>${recReplaceItem.barcode }</td>
									   <td>${recReplaceItem.orderCode }</td>
									   <td>${recReplaceItem.createBillName }</td>
									   <td>${recReplaceItem.deliverNo }</td>
									   <td>${recReplaceItem.storageNo }</td>
									   <td><fmt:formatDate value="${recReplaceItem.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
									   <td>${recReplaceItem.arrivalNum }</td>
									   <td>
										   <c:choose>
											   <c:when test="${recReplaceItem.isNeedInvoice == 'Y'}">是</c:when>
											   <c:when test="${recReplaceItem.isNeedInvoice != 'Y'}">否</c:when>
										   </c:choose>
									   </td>
									   <td>${recReplaceItem.salePrice }</td>
									   <td>${recReplaceItem.updateSalePrice }</td>
									   <td>${recReplaceItem.updateSalePrice*recReplaceItem.arrivalNum }</td>
									   <td class="oprationRe5">
										   <div >
											   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${recReplaceItem.deliverNo}','1');">查看收货凭据</span>
												   <%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${daohuoItem.value.attachmentAddrStr}');">查看收据</span>--%>

										   </div>
										   <c:choose>
											   <c:when test="${buyBillReconciliation.billDealStatus == 1}">
												   <a href="javascript:void(0)" onclick="updPrice('${recReplaceItem.productName}','${recReplaceItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
											   </c:when>
											   <%--<c:when test="${buyBillReconciliation.billDealStatus == 4}">
                                                   <a href="javascript:void(0)" onclick="updPrice('${goodsItem.productName}','${goodsItem.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
                                               </c:when>--%>
										   </c:choose>
									   </td>
								   </tr>
							   </c:forEach>
							   </tbody>
							   <tfoot>
							   <tr>
								   <td>合计</td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td>${recordReplaceItem.value.arrivalNumSum }</td>
								   <td></td>
								   <td>-</td>
								   <td>-</td>
								   <td>${recordReplaceItem.value.itemPriceSum }</td>
									   <%--<td>总金额+运费:${itemPriceSum+daohuoItem.value.freight }</td>--%>
								   <td class="oprationRe5"></td>
							   </tr>
							   </tfoot>
						   </table>
					   </div>
				   </c:forEach>
				   <!--换货数据-->
				   <c:forEach var="replaceItem" items="${buyBillReconciliation.replaceMap}">
					   <div>
						   <div class="page_title">
							   <table class="table_n">
								   <tr>
									   <td style="width: 20%"><span ><span class="c99">订单类型：</span>售后换货</span></td>
									   <td style="width: 20%"><span ><span class="c99">总金额：</span>${replaceItem.value.itemPriceSum}元</span></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
								   </tr>

							   </table>
						   </div>
						   <table class="layui-table text-center" lay-even="" lay-size="sm"
								  style="margin: 0;">
							   <thead>
							   <tr>
								   <td style="width:15%">货号</td>
								   <td style="width:10%">商品名称</td>
								   <td style="width:10%">规格代码</td>
								   <td style="width:10%">规格名称</td>
								   <td style="width:13%">条形码</td>
									   <%--<td style="width:27%">采购订单号</td>--%>
								   <td style="width:10%">创建人</td>
								   <td style="width:27%">换货单号</td>
								   <td style="width:27%">入库单号</td>
								   <td style="width:10%">到货日期</td>
								   <td style="width:10%">换货总数量</td>
									   <%--<td style="width: 10%">是否开票</td>--%>
								   <td style="width:12%">换货单价</td>
								   <td style="width:12%">对账单价</td>
								   <td style="width:10%">总金额</td>
								   <td style="width:15%" class="oprationRe1">操作</td>
							   </tr>
							   </thead>
							   <tbody>
								   <%--<c:set var="arrivalNumSum" value="0" scope="page"></c:set>
                                   <c:set var="itemPriceSum" value="0" scope="page"></c:set>--%>
							   <c:forEach var="replaceItemInfo" items="${replaceItem.value.itemList}">
								   <tr class="breakType">
									   <td>${replaceItemInfo.productCode }</td>
									   <td>${replaceItemInfo.productName }</td>
									   <td>${replaceItemInfo.skuCode }</td>
									   <td>${replaceItemInfo.skuName }</td>
									   <td>${replaceItemInfo.barcode }</td>
										   <%--<td>${replaceItemInfo.orderCode }</td>--%>
									   <td>${replaceItemInfo.createBillName }</td>
									   <td>${replaceItemInfo.deliverNo }</td>
									   <td>-</td>
									   <td><fmt:formatDate value="${replaceItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
									   <td>${replaceItemInfo.arrivalNum }</td>
										   <%--<td>
                                               <c:choose>
                                                   <c:when test="${replaceItemInfo.isNeedInvoice == 'Y'}">是</c:when>
                                                   <c:when test="${replaceItemInfo.isNeedInvoice != 'Y'}">否</c:when>
                                               </c:choose>
                                           </td>--%>
									   <td>${replaceItemInfo.salePrice }</td>
									   <td>${replaceItemInfo.updateSalePrice }</td>
									   <td>${replaceItemInfo.updateSalePrice*replaceItemInfo.arrivalNum }</td>
									   <td class="oprationRe1">
										   <div >
											   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${replaceItemInfo.deliverNo}','2');">查看收货凭据</span>
												   <%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${replaceItem.value.attachmentAddrStr}');">查看收据</span>--%>
												   <%--<c:choose>
                                                       <c:when test="${buyBillReconciliation.billDealStatus!=3}">
                                                           <a href="javascript:void(0)" onclick="updPrice('${replaceItemInfo.productName}','${replaceItemInfo.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
                                                       </c:when>
                                                   </c:choose>--%>
										   </div>
									   </td>
								   </tr>
								   <%--<c:set var="arrivalNumSum" value="${arrivalNumSum+replaceItemInfo.arrivalNum }" scope="page"></c:set>
                                   <c:set var="itemPriceSum" value="${itemPriceSum+replaceItemInfo.updateSalePrice*replaceItemInfo.arrivalNum }" scope="page"></c:set>--%>
							   </c:forEach>
							   </tbody>
							   <tfoot>
							   <tr>
								   <td>合计</td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td>${replaceItem.value.arrivalNumSum}</td>
									   <%--<td></td>--%>
								   <td>-</td>
								   <td>-</td>
								   <td>${replaceItem.value.itemPriceSum }</td>
									   <%--<td class="oprationRe1">总金额+运费:${itemPriceSum+replaceItem.value.freight }</td>--%>
								   <td class="oprationRe1"></td>
							   </tr>
							   </tfoot>
						   </table>
					   </div>
					   <%--<c:set var="priceSum" value="${priceSum+itemPriceSum+replaceItem.value.freight }" scope="page"></c:set>--%>
				   </c:forEach>
				   <!--退货数据-->
				   <c:forEach var="customerItem" items="${buyBillReconciliation.customerMap}">
					   <div>
						   <div class="page_title">
							   <table class="table_n">
								   <tr>
									   <td style="width: 20%"><span ><span class="c99">订单类型：</span>售后退货</span></td>
									   <td style="width: 20%"><span ><span class="c99">总金额：</span>${customerItem.value.itemPriceSum}元</span></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
									   <td style="width: 20%"></td>
								   </tr>

							   </table>
						   </div>
						   <table class="layui-table text-center" lay-even="" lay-size="sm"
								  style="margin: 0;">
							   <thead>
							   <tr>
								   <td style="width:15%">货号</td>
								   <td style="width:10%">商品名称</td>
								   <td style="width:10%">规格代码</td>
								   <td style="width:10%">规格名称</td>
								   <td style="width:15%">条形码</td>
									   <%--<td width="150px">采购单号</td>--%>
								   <td style="width:10%">创建人</td>
								   <td style="width:25%">退货单号</td>
								   <td style="width:25%">入库单号</td>
								   <td style="width:10%">到货日期</td>
								   <td style="width:10%">到货总数量</td>
									   <%--<td style="width: 10%">是否开票</td>--%>
								   <td style="width:15%">采购单价</td>
								   <td style="width:15%">对账单价</td>
								   <td style="width:15%">总金额</td>
								   <td style="width:15%" class="oprationRe2">操作</td>
							   </tr>
							   </thead>
							   <tbody>
								   <%--<c:set var="arrivalNumSum" value="0" scope="page"></c:set>
                                   <c:set var="itemPriceSum" value="0" scope="page"></c:set>--%>
							   <c:forEach var="customerItemInfo" items="${customerItem.value.itemList}">
								   <tr class="breakType">
									   <td>${customerItemInfo.productCode }</td>
									   <td>${customerItemInfo.productName }</td>
									   <td>${customerItemInfo.skuCode }</td>
									   <td>${customerItemInfo.skuName }</td>
									   <td>${customerItemInfo.barcode }</td>
										   <%--<td>${goodsItem.orderCode }</td>--%>
									   <td>${customerItemInfo.createBillName }</td>
									   <td>${customerItemInfo.deliverNo }</td>
									   <td>-</td>
									   <td><fmt:formatDate value="${customerItemInfo.arrivalDate }" pattern="yyyy-MM-dd" type="both"/></td>
									   <td>${customerItemInfo.arrivalNum }</td>
										   <%--<td>
                                               <c:choose>
                                                   <c:when test="${customerItemInfo.isNeedInvoice == 'Y'}">是</c:when>
                                                   <c:when test="${customerItemInfo.isNeedInvoice != 'Y'}">否</c:when>
                                               </c:choose>
                                           </td>--%>
									   <td>${customerItemInfo.salePrice }</td>
									   <td>${customerItemInfo.updateSalePrice }</td>
									   <td>${customerItemInfo.updateSalePrice*customerItemInfo.arrivalNum }</td>
									   <td class="oprationRe2">
										   <div>
											   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceiptNew('${customerItemInfo.deliverNo}','3');">查看收货凭据</span>
												   <%--<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItem.value.attachmentAddrStr}');">查看收据</span>--%>
												   <%--<c:choose>
                                                       <c:when test="${buyBillReconciliation.billDealStatus!=3}">
                                                           <a href="javascript:void(0)" onclick="updPrice('${customerItemInfo.productName}','${customerItemInfo.id}','${buyBillReconciliation.id}');" class="layui-btn layui-btn-mini">修改单价</a>
                                                       </c:when>
                                                   </c:choose>--%>
										   </div>
									   </td>
								   </tr>
								   <%--<c:set var="arrivalNumSum" value="${arrivalNumSum+customerItemInfo.arrivalNum }" scope="page"></c:set>
                                   <c:set var="itemPriceSum" value="${itemPriceSum+customerItemInfo.updateSalePrice*customerItemInfo.arrivalNum }" scope="page"></c:set>--%>
							   </c:forEach>
							   </tbody>
							   <tfoot>
							   <tr>
								   <td>合计</td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td>${customerItem.value.arrivalNumSum}</td>
									   <%--<td></td>--%>
								   <td>-</td>
								   <td>-</td>
								   <td>${customerItem.value.itemPriceSum}</td>
									   <%--<td class="oprationRe2">总金额:${itemPriceSum }</td>--%>
								   <td class="oprationRe2"></td>
							   </tr>
							   </tfoot>
						   </table>
					   </div>
					   <%--<c:set var="priceSum" value="${priceSum-itemPriceSum}" scope="page"></c:set>--%>
				   </c:forEach>
			   </div>

		   </td>
		   <td>
		     <div>
			   <a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
					   '${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
					   '${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
		     </div>
		     <div class="opinion_view">
			   <span class="orange" data="${buyBillReconciliation.purchaseId}">审批流程</span>
		     </div>
			   <span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetailsNew?reconciliationId=${buyBillReconciliation.id}&recType=1','buyer')">账单详情</span><br>
		   </td>
		 </tr>
	   </table>
		<%--<tr class="breakType text-center">
			<td><input type="checkbox" name="checkone" value="${buyBillReconciliation.purchaseId}"></td>
			<td>${buyBillReconciliation.id}</td>
			<td>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</td>
			<td>${buyBillReconciliation.createBillUserName}</td>
			<td>${buyBillReconciliation.sellerCompanyName}</td>
			<td>${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</td>
			<td>${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</td>
			<td>${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</td>
			<td>${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</td>
			<td>${buyBillReconciliation.freightSumCount}</td>
			<td>
			<span class="layui-btn layui-btn-mini" onclick="showCustomPayment('${buyBillReconciliation.id}');">
					${buyBillReconciliation.customAmount}</span>
			</td>
			<td>
			<span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.sellerCompanyName}');">
					${buyBillReconciliation.advanceDeductTotal}</span>
			</td>
			<td>${buyBillReconciliation.totalAmount}</td>

			<td>
				<div>
					<c:choose>
						<c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==6}">财务待审批</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
					</c:choose>
				</div>
				<div>
					<a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
							'${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
							'${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
				</div>
				<div class="opinion_view">
					<span class="orange" data="${buyBillReconciliation.purchaseId}">审批流程</span>
				</div>
			</td>
			<td>
				<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetailsNew?reconciliationId=${buyBillReconciliation.id}&recType=1','buyer')">账单详情</span><br>
			</td>
		</tr>--%>
	 </div>

    </c:forEach>

		<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
	<!--收款凭据-->
	<div id="linkReceipt" class="interest" style="display:none;">
		<div id="receiptBody" class="receipt_content">
			<div class="big_img">
				<img src="">
			</div>
			<div class="del_receipt">
				<button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="icon_list"></ul>
			</div>
		</div>
	</div>

	<!--内部选人审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="insideBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="insideAgree" type="radio" name="insideStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="insideReject" type="radio" name="insideStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="insideRemarks" name="insideRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>

	<!--最终审批-->
	<%--<div class="plain_frame bill_exam" style="display:none;" id="acceptBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="acceptAgree" type="radio" name="agreeStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="acceptReject" type="radio" name="agreeStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="acceptRemarks" name="acceptRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>
	<!--内部审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="approvalBillReconciliation">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="purAgree" type="radio" name="approvalStatus" value="pass"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="purReject" type="radio" name="approvalStatus" value="return"> 不同意对账</label>
				</li>
				<li>
					<span>填写备注:</span>
					<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>--%>

	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillRecApproval">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">审批人</td>
					<td style="width:20%">审批时间</td>
					<td style="width:20%">审批备注</td>
				</tr>
				</thead>
				<tbody id="billRecApprovalShow">
				</tbody>
			</table>
		</div>
	</div>

	<!--自定义付款项-->
	<div class="plain_frame price_change" id="customPaymentDiv" style="display:none;height: 400px;overflow-y: auto">
		<ul id="billCustomBody">
		</ul>
	</div>
	<!--回显自定义付款详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillCustomList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">添加人</td>
					<td style="width:10%">添加时间</td>
					<td style="width:10%">奖惩类型</td>
					<td style="width:10%">奖惩金额</td>
					<td style="width:10%">奖惩添加备注</td>
					<td style="width:10%">审批状态</td>
					<td style="width:10%">审批人</td>
					<td style="width:10%">审批时间</td>
					<td style="width:10%">奖惩审批备注</td>
					<td style="width:10%">奖惩单据</td>
				</tr>
				</thead>
				<tbody id="billRecCustomFileBody">
				</tbody>
			</table>
		</div>
		<!--自定义付款款凭据-->
		<div id="showCustomFiles" class="receipt_content" style="display:none;">
			<div id="showCustomBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileCustomList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--添加账单附件-->
	<div class="plain_frame bill_exam" style="display:none;" id="billFiles">
		<ul id="billFilesBody">
		</ul>
	</div>
	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillFileList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">上传人</td>
					<td style="width:20%">上传时间</td>
					<td style="width:20%">上传备注</td>
					<td style="width:20%">上传单据</td>

				</tr>
				</thead>
				<tbody id="billRecFileBody">
				</tbody>
			</table>
		</div>
		<!--收款凭据-->
		<div id="showFiles" class="receipt_content" style="display:none;">
			<div id="showBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--预付款抵扣详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showAdvanceEditList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">供应商</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">添加人</td>
					<td style="width:10%">添加时间</td>
					<td style="width:10%">预付款抵扣金额</td>
				</tr>
				</thead>
				<tbody id="billAdvanceEditBody">
				</tbody>
			</table>
		</div>
	</div>
</div>



<style>
	.view .icon_list {
		height: 92px;
		position: absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
</style>