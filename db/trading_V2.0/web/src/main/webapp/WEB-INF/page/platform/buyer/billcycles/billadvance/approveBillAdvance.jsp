<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的收款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/billAdvanceList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载条件
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startAdvanceDate}");
        $("#endDate").val("${params.endAdvanceDate}");
        $("#advanceStatus").val("${params.advanceStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    //预付款单据
    var preview={
        LIWIDTH:108,//保存每个li的宽
        $ul:null,//保存小图片列表的ul
        moved:0,//保存左移过的li
        init:function(){//初始化功能
            this.$ul=$(".view>ul");//查找ul
            $(".view>a").click(function(e){//为两个按钮绑定单击事件

                e.preventDefault();
                if(!$(e.target).is("[class$='_disabled']")){//如果按钮不是禁用
                    if($(e.target).is(".forward")){//如果是向前按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))-this.LIWIDTH);//整个ul的left左移
                        this.moved++;//移动个数加1
                    }
                    else{//如果是向后按钮
                        this.$ul.css("left",parseFloat(this.$ul.css("left"))+this.LIWIDTH);//整个ul的left右移
                        this.moved--;//移动个数减1
                    }
                    this.checkA();//每次移动完后，调用该方法
                }
            }.bind(this));
            //为$ul添加鼠标进入事件委托，只允许li下的img响应时间
            this.$ul.on("mouseover","li>img",function(){
                var src=$(this).attr("src");//获得当前img的src
                //var i=src.lastIndexOf(".");//找到.的位置
                //src=src.slice(0,i)+"-m"+src.slice(i);//将src 拼接-m 成新的src
                $(".big_img>img").attr("src",src);//设置中图片的src
            });
        },
        checkA:function(){//检查a的状态
            if(this.moved==0){//如果没有移动
                $("[class^=backward]").attr("class","backward_disabled");//左侧按钮禁用
            }
            else if(this.$ul.children().size()-this.moved==5){//如果总个数减已经移动的个数等于5
                $("[class^=forward]").attr("class","forward_disabled");//右侧按钮禁用
            }
            else{//否则，都启用
                $("[class^=backward]").attr("class","backward");
                $("[class^=forward]").attr("class","forward");
            }
        }
    };
    preview.init();

    //重置查询条件
    function resetformData(){
       // alert("进来了吗");
        $("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#advanceStatus").val("0");
    }

    // 审核
    function approveBillAdvanceEdit(id){
        if($(".verifyDetail").length>0)$(".verifyDetail").remove();
        $.ajax({
            url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
            data:{"id":id},
            success:function(data){
                var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
                detailDiv.addClass("verifyDetail");
                detailDiv.html(data);
                detailDiv.appendTo($("#billAdvanceContent"));
                detailDiv.find("#verify_side").addClass("show");
            },
            error:function(){
                layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
            }
        });
    }

    //一键通过
    function verifyAll(){
        var checkedObj = $("#billAdvanceContent input[name='checkone']:checked");
        if(checkedObj.length == 0){
            layer.msg('至少选择一条数据！');
            return;
        }else{
            layer.msg('你确定一键通过？', {
                time : 0,//不自动关闭
                btn : [ '确定', '取消' ],
                yes : function(index) {
                    layer.close(index);
                    var orderIdArray = "";
                    $.each(checkedObj,function(i,o){
                        orderIdArray += $(this).val() + ",";
                    });
                    layer.load();
                    $.ajax({
                        url:basePath+"platform/buyer/billAdvanceItem/verifyBillAdvanceEditAll",
                        async:false,
                        data:{"acceptId":orderIdArray},
                        success:function(data){
                            //selectData();
                            layer.msg("审批成功！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billAdvanceItem/approveBillAdvanceEdit','buyer')
                        },
                        error:function(){
                            layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        }
    }
    function selectAll(obj) {
        if ($(obj).is(":checked")) {
            $("#billAdvanceContent input[name='checkone']").prop("checked", true);
        } else {
            $("#billAdvanceContent input[name='checkone']").prop("checked", false);
        }
    }

	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<!--内容-->
<div id="billAdvanceContent">
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billAdvanceItem/approveBillAdvanceEdit">
		<ul class="order_search">
			<li class="comm">
				<label>充值编号:</label>
				<input type="text" placeholder="请输入充值编号" id="id" name="id" >
			</li>
			<li class="comm">
				<label>供应商名称:</label>
				<input type="text" placeholder="请输入供应商名称" id="sellerCompanyName" name="sellerCompanyName" >
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="range nomargin">
				<label>充值日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startEditDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endEditDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />

			<div class="rt">
				<a href="javascript:void(0);"
				   class="layui-btn layui-btn-normal layui-btn-small" onclick="verifyAll();">
					<i class="layui-icon">&#xe6a3;</i> 一键通过
				</a>
			</div>
		</ul>
	</form>
    <table class="table_pure payment_list">
	  <thead>
	  <tr>
		  <td style="width:5%"><input type="checkbox" name="checkAll" onclick="selectAll(this);"></td>
		  <td style="width:15%">充值编号</td>
		  <td style="width:10%">预付款账号</td>
		  <td style="width:15%">供应商</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">充值金额</td>
		  <td style="width:10%">充值时间</td>
		  <td style="width:10%">充值人</td>
		  <td style="width:10%">充值备注</td>
		  <td style="width:10%">操作</td>
	  </tr>
	  </thead>
	  <tbody>
	    <c:forEach var="billAdvanceEdit" items="${searchPageUtil.page.list}">
		  <tr class="breakType">
			  <td style="text-align: center"><input type="checkbox" name="checkone" value="${billAdvanceEdit.id}"></td>
			  <td style="text-align: center" title="${billAdvanceEdit.id}">
					  ${billAdvanceEdit.id}</td>
			  <td>${billAdvanceEdit.advanceId}</td>
			  <td style="text-align: center" title="${billAdvanceEdit.sellerCompanyName}">
			    ${billAdvanceEdit.sellerCompanyName}</td>
			  <td>${billAdvanceEdit.createBillUserName}</td>
			  <td>${billAdvanceEdit.updateTotal}</td>
			  <td>${billAdvanceEdit.createTimeStr}</td>
			  <td>${billAdvanceEdit.createUserName}</td>
			  <td style="text-align: center">${billAdvanceEdit.advanceRemarks}</td>
			  <td>
			    <div>
				  <c:choose>
					  <c:when test="${billAdvanceEdit.editStatus==1}">
						  <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="approveBillAdvanceEdit('${billAdvanceEdit.id}');">审批</span>
					  </c:when>
				  </c:choose>
			    </div>
			  </td>
		  </tr>
	    </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
</div>

<!--预付款票据-->
<div id="advanceReceipt" class="receipt_content layui-layer-wrap" style="display:none;">
	<div class="big_img">
		<%--<img id="bigImg">--%>
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<!--回显预付款修改记录-->
<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="advanceEditDiv">
	<div>
		<table class="table_yellow">
			<thead>
			<tr>
				<td style="width:10%">采购员</td>
				<td style="width:10%">充值人</td>
				<td style="width:10%">充值时间</td>
				<td style="width:10%">充值金额</td>
				<td style="width:10%">充值备注</td>
				<td style="width:10%">充值状态</td>
				<td style="width:10%">充值单据</td>
			</tr>
			</thead>
			<tbody id="advanceEditBody">
			</tbody>
		</table>
	</div>
	<!--自定义付款款凭据-->
	<div id="showAdvenceFiles" class="receipt_content" style="display:none;">
		<div id="showAdvanceBigImg" class="big_img">
			<%--<img id="bigImg">--%>
		</div>
		<div class="view">
			<a href="#" class="backward_disabled"></a>
			<a href="#" class="forward"></a>
			<ul id="fileAdvanceList" class="icon_list"></ul>
		</div>
	</div>
</div>
<style>
	.view .icon_list {
		height: 92px;
		position:absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
	.view .icon_list li img:hover {
		border: 2px solid #e4393c;
		padding: 0;
	}
</style>