<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%@ include file="../../platform/common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/toolTip.js"></script>
<style>
.voucherImg {
    margin: 15px 0 0 140px;
}
</style>
<div class="modal-content" style="width: 600px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel">公司信息</h4>
	</div>
	<div class="modal-body row-fluid ">
		<div class=" padding-top-sm row">
			<div class="form-group col-md">
				<label class="label-two">编号：${company.companyCode}</label> 
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">名称：${company.companyName}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">所在地：${el:getProvinceById(company.province).province} ${el:getCityById(company.city).city} ${el:getAreaById(company.area).area}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">详细地址：${company.detailAddress}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">联系人：${company.linkMan}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">电话：${company.zone} - ${company.telNo}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">传真：${company.fax1} - ${company.fax2}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">qq：${company.qq}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">旺旺号：${company.wangNo}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">email：${company.email}</label>
			</div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">营业证件：
					<c:forEach var="a1" items="${a1List}">
						<img src="${a1.url}" style="width: 90px;height: 90px;" 
							onMouseOver="toolTip('<img src=${a1.url}>')" onMouseOut="toolTip()">
					</c:forEach>
				</label>
			</div>
			<div class="form-group col-md padding-top-sm"></div>
			<div class="form-group col-md padding-top-sm"></div>
			<div class="form-group col-md padding-top-sm"></div>
			<div class="form-group col-md padding-top-sm">
				<label class="label-two">开户许可证：
					<c:forEach var="a2" items="${a2List}">
						<img src="${a2.url}" style="width: 90px;height: 90px;"
							onMouseOver="toolTip('<img src=${a2.url}>')" onMouseOut="toolTip()">
					</c:forEach>
				</label>
			</div>
		</div>
	</div>
	<div class="modal-footer"></div>
</div>
