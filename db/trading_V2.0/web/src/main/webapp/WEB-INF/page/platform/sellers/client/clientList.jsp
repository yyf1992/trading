<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<title>管理客户</title>
  <link rel="stylesheet" href="<%=basePath %>css/common.css">
  <link rel="stylesheet" href="<%=basePath %>css/layui/css/layui.css">
  <link rel="stylesheet" href="<%=basePath %>/statics/platform/css/commodity_all.css">
<style type="text/css">
.order_search>li .reset{
display:inline-block;
  margin-left:5px;
  background:#F6F5F5;
  color:#333;
  border:1px solid #BFBFBF;
  border-radius:3px;
  width:128px;
  height:25px;
  line-height:25px;
  font-weight:bold;
  text-align:center;
  cursor: pointer;
  outline:0;
}
</style>
<script src="statics/platform/js/clientList.js"></script>
<div class="tab tab_seller">
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/addClient','sellers')">添加客户</a> <b>|</b>
  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/clientList','sellers')" class="hover">我的客户</a>
</div>
<div>
  <form id="searchForm" action="platform/sellers/client/clientList">
    <ul class="order_search customer_s">
      <li class="comm">
        <label>客户名称:</label>
        <input type="text" placeholder="输入客户名称" id="clientName" name="clientName" value="${searchPageUtil.object.clientName}">
      </li>
      <li class="comm">
        <label>联系人:</label>&nbsp;
        <input type="text" placeholder="输入联系人" id="clientPerson" name="clientPerson" value="${searchPageUtil.object.clientPerson}">
      </li>
      <li class="comm">
        <label>收货地址:</label>
        <input type="text" placeholder="输入收货地址" id="shipAddress"  name="shipAddress" value="${searchPageUtil.object.shipAddress}">
      </li>
      <li class="comm">
        <label>手机号:</label>&nbsp;
        <div class="layui-input-inline">
        <input type="text" id="clientPhone" autocomplete="off" class="layui-input" placeholder="输入手机号" name="clientPhone" value="${searchPageUtil.object.clientPhone}">
      	</div>
      </li>
      <li class="comm nomargin">
       <label>创建日期:</label>
       <div class="layui-input-inline">
<input type="text" name="createDate" id="createDate"  placeholder="请选择日期"  lay-verify="date" class="layui-input" style="width: 200px;height: 25px; display: inline-block;">
                  	              	           
	<!--  <input type="text" id="createDate" name="createDate"  placeholder="请选择日期" autocomplete="off" class="layui-input"  
	onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">  -->
       </div>
      </li>
      <li class="nomargin">     	
      	 <span class="reset" onclick="reset_search();">重置</span>  
      </li>
      <li class="nomargin">	
	  	<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
	  </li>
   </ul>
       <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
	   <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />

       <input type="button" class="layui-btn layui-btn-danger layui-btn-small batch_d" value="批量删除供应商" onclick="beatchDeleteClient();">
  	   <!--  <button class="layui-btn layui-btn-normal layui-btn-small batch_d_c" id="batch_d_c" onclick="beatchDeleteClient();">批量删除客户</button> -->
   <table class="table_pure customerList mt">
    <thead>
      <tr>
        <td style="width:5%"><input  onclick="selAllClient();" type="checkbox"></td>
        <td style="width:20%">客户名称</td>
        <td style="width:8%">联系人</td>
        <td style="width:10%">手机号</td>
        <td style="width:10%">电话</td>
        <td style="width:20%">收货地址</td>
        <td style="width:10%">创建日期</td>
        <td style="width:17%">操作</td>
      </tr>
      <tr>
        <td colspan="8"></td>
      </tr>
    </thead>
    <tbody>
    	<c:forEach var="clientLinkman" items="${searchPageUtil.page.list}">        	
	       <tr>
	         <td><input type="checkbox" name="checkones" value="${clientLinkman.id}"></td>
	         <td style="vertical-align:middle; text-align:center;">${clientLinkman.client_Name}</td> 
	         <td style="vertical-align:middle; text-align:center;">${clientLinkman.client_Person}</td>
	         <td style="vertical-align:middle; text-align:center;">${clientLinkman.client_Phone}</td>
	         <td style="vertical-align:middle; text-align:center;">${clientLinkman.zone}
	         	- ${clientLinkman.telNo}</td>
	         <td style="vertical-align:middle; text-align:center;">${clientLinkman.ship_Address}</td>
	         <td><fmt:formatDate value="${clientLinkman.create_date}" type="both"/></td>
	         <td>
	           <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/updateClientDetails?id=${clientLinkman.id }','sellers')" class="layui-btn layui-btn layui-btn-mini">修改</a>
	           <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/loadClientDetails?id=${clientLinkman.id }','sellers')" class="layui-btn layui-btn-normal layui-btn-mini">查看详情</a>
	           <a onclick="deleteSellerClient('${clientLinkman.id}');" class="customer_d layui-btn layui-btn-danger layui-btn-mini">删除</a>
	         </td>
	       </tr>
        </c:forEach>
    </tbody>
   </table>
   <input type="button" class="layui-btn layui-btn-danger layui-btn-small batch_d" value="批量删除供应商" onclick="beatchDeleteClient();">
   <div class="pager">${searchPageUtil.page}</div> 
   </form> 
</div>