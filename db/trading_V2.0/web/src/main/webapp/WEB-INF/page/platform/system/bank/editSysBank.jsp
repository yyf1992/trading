<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<form id="editSysBankForm">
<div class="plain_frame accountAdd">
  <ul>
    <li>
      <span>银行账号：</span>
      <input type="text"  name="bankAccount" value="${sysBank.bankAccount}" onkeyup="(this.v=function(){this.value=this.value.replace(/[^0-9-]+/,'');}).call(this)" onblur="this.v();" autocomplete="off" title="仅支持数字格式">
    </li>
    <li>
      <span>开户户名：</span>
      <input type="text" name="accountName" value="${sysBank.accountName}">
    </li>
    <li>
      <span>开户行：</span>
      <textarea name="openBank">${sysBank.openBank}</textarea>
    </li>
    <li>
       <span>状态:</span>
      <c:if test="${sysBank.isDel==0}">
      	<label><input type="radio" name="isDel" checked value="0"> 正常</label>
      	<label><input type="radio" name="isDel" value="1"> 禁用</label>
      </c:if>
      <c:if test="${sysBank.isDel==1}">
      	<label><input type="radio" name="isDel" value="0"> 正常</label>
      	<label><input type="radio" name="isDel" checked value="1"> 禁用</label>
      </c:if>
    </li>
  </ul>
      <input type="hidden" name="id" value="${sysBank.id}">
</div>
</form>