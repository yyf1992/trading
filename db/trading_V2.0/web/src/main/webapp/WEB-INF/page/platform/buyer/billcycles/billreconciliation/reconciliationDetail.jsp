<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%--<%@ include file="../../../common/common.jsp"%>--%>
<head>
	<title>账单对账列表</title>
	<%--<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/reconciliationDetail.js"></script>--%>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">--%>

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
	<%--<script type="text/javascript">
        //标签页改变
        function setStatus(obj,status) {
            $("#billDealStatus").val(status);

            $('.tab a').removeClass("hover");
            $(obj).addClass("hover");
            loadPlatformData();
        }
	</script>--%>
</head>
<!--内容-->
<!--页签 -->
<div class="print_m">
	<div id="print_yc">
	<h3 class="print_title">供应商对账账单</h3>
	<table class="table_n">
		<tr>
			<td>供应商：<span>${recordAndReturnMap.sellerCompanyName}</span></td>
			<td>出账周期：<span>${recordAndReturnMap.startBillStatementDateStr}至${recordAndReturnMap.endBillstatementDateStr}</span></td>
			<td>状态：
				<span>
			  <c:choose>
				  <c:when test="${recordAndReturnMap.reconciliationStatus==1}">等待对账</c:when>
				  <c:when test="${recordAndReturnMap.reconciliationStatus==2}">待对方对账</c:when>
				  <c:when test="${recordAndReturnMap.reconciliationStatus==3}">已完成对账</c:when>
				  <c:when test="${recordAndReturnMap.reconciliationStatus==4}">内部审批已驳回</c:when>
				  <c:when test="${recordAndReturnMap.reconciliationStatus==5}">对方审批已驳回</c:when>
			  </c:choose>
			</span></td>
		</tr>
	</table>

		<!--列表区-->
	<table id="reconciliationData" class="layui-table table_c" >
		<thead>
		<tr>
			<td style="width:10%">到货日期</td>
			<td style="width:23%">发货单号</td>
			<td style="width:10%">名称</td>
			<td style="width:10%">规格</td>
			<td style="width:5%">单位</td>
			<td style="width:10%">条形码</td>
			<td style="width:10%">单价</td>
			<td style="width:10%">修改单价</td>
			<td style="width:8%">数量</td>
			<td style="width:15%">物流编号</td>
			<td style="width:9%">运费</td>
			<td style="width:13%">商品金额</td>
			<td class="oprationRe" style="width:10%">操作</td>
		</tr>
		</thead>
		<tbody>

		<tr>
			<c:forEach var="buyBillReconciliation" items="${recordAndReturnMap.recordItemList}">
				<tr class="text-center">
				   <td>${buyBillReconciliation.arrivalDateStr}</td>
				   <td title="${buyBillReconciliation.deliverNo}">${buyBillReconciliation.deliverNo}</td>
				   <td title="${buyBillReconciliation.buyDeliveryRecordItem.productName}">${buyBillReconciliation.buyDeliveryRecordItem.productName}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.skuName}</td>
				   <td>${buyBillReconciliation.buyDeliveryRecordItem.unitName}</td>
				   <td title="${buyBillReconciliation.buyDeliveryRecordItem.barcode}">${buyBillReconciliation.buyDeliveryRecordItem.barcode}</td>
			       <td title="${buyBillReconciliation.buyDeliveryRecordItem.salePrice}">${buyBillReconciliation.buyDeliveryRecordItem.salePrice}</td>
			       <td title="${buyBillReconciliation.buyDeliveryRecordItem.updateSalePrice}">${buyBillReconciliation.buyDeliveryRecordItem.updateSalePrice}</td>
				   <td title="${buyBillReconciliation.buyDeliveryRecordItem.arrivalNum}">${buyBillReconciliation.buyDeliveryRecordItem.arrivalNum}</td>
			       <td title="${buyBillReconciliation.buyDeliveryRecordItem.logisticsId}">${buyBillReconciliation.buyDeliveryRecordItem.logisticsId}</td>
			       <td title="${buyBillReconciliation.buyDeliveryRecordItem.freight}">${buyBillReconciliation.buyDeliveryRecordItem.freight}</td>
				   <td title="${buyBillReconciliation.salePriceSum}">${buyBillReconciliation.salePriceSum}</td>
					<td  class="oprationRe1">
						<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="openConfirmReceipt('${buyBillReconciliation.buyDeliveryRecordItem.recordId}','N','1','${recordAndReturnMap.reconciliationId}');">发货详情</span>
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${buyBillReconciliation.attachmentList}');">查看收据</span>
						<c:choose>
							<c:when test="${recordAndReturnMap.reconciliationStatus!=3}">
								<a href="javascript:void(0)" onclick="updPrice('${buyBillReconciliation.buyDeliveryRecordItem.productName}','${buyBillReconciliation.buyDeliveryRecordItem.id}');" class="layui-btn layui-btn-mini">修改单价</a>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<c:forEach var="recordReplaceItemList" items="${recordAndReturnMap.recordReplaceItemList}">
				<tr class="text-center">
					<td>${recordReplaceItemList.arrivalDateStr}</td>
					<td title="${recordReplaceItemList.deliverNo}">${recordReplaceItemList.deliverNo}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.productName}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.skuName}</td>
					<td>${recordReplaceItemList.buyRecordReplaceItem.unitName}</td>
					<td title="${recordReplaceItemList.buyRecordReplaceItem.barcode}">${recordReplaceItemList.buyRecordReplaceItem.barcode}</td>
					<td title="${recordReplaceItemList.buyRecordReplaceItem.salePrice}">${recordReplaceItemList.buyRecordReplaceItem.salePrice}</td>
			        <td title="${recordReplaceItemList.buyRecordReplaceItem.updateSalePrice}">${recordReplaceItemList.buyRecordReplaceItem.updateSalePrice}</td>
					<td title="${recordReplaceItemList.buyRecordReplaceItem.arrivalNum}">${recordReplaceItemList.buyRecordReplaceItem.arrivalNum}</td>
			        <td title="${recordReplaceItemList.buyRecordReplaceItem.logisticsId}">${recordReplaceItemList.buyRecordReplaceItem.logisticsId}</td>
			        <td title="${recordReplaceItemList.buyRecordReplaceItem.freight}">${recordReplaceItemList.buyRecordReplaceItem.freight}</td>
					<td title="${recordReplaceItemList.replacePriceSum}">${recordReplaceItemList.replacePriceSum}</td>
					<td class="oprationRe2">
						<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="openConfirmReceipt('${recordReplaceItemList.buyRecordReplaceItem.recordId}','N','1','${recordAndReturnMap.reconciliationId}');">换货详情</span>
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${recordReplaceItemList.attachmentList}');">查看收据</span>
					</td>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<c:forEach var="customerItemList" items="${recordAndReturnMap.customerItemList}">
				<tr class="text-center">
					<td>${customerItemList.verifyDateStr}</td>
					<td title="${customerItemList.customerCode}">${customerItemList.customerCode}</td>
					<td>${customerItemList.buyCustomerItem.productName}</td>
					<td>${customerItemList.buyCustomerItem.skuName}</td>
					<td>${customerItemList.buyCustomerItem.unitName}</td>
					<td>${customerItemList.buyCustomerItem.barcode}</td>
					<td title="${customerItemList.buyCustomerItem.salePrice}">${customerItemList.buyCustomerItem.salePrice}</td>
			        <td title="${customerItemList.buyCustomerItem.updateSalePrice}">${customerItemList.buyCustomerItem.updateSalePrice}</td>
					<td title="${customerItemList.buyCustomerItem.arrivalNum}">${customerItemList.buyCustomerItem.arrivalNum}</td>
			        <td title="${customerItemList.buyCustomerItem.logisticsId}">${customerItemList.buyCustomerItem.logisticsId}</td>
			        <td title="${customerItemList.buyCustomerItem.freight}">${customerItemList.buyCustomerItem.freight}</td>
					<td title="${customerItemList.goodsPriceSum}">${customerItemList.goodsPriceSum}</td>
					<td class="oprationRe3">
						<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/loadCustomerDetails?id=${customerItemList.buyCustomerItem.recordId}&reconciliationId=${recordAndReturnMap.reconciliationId}','buyer')">售后详情</span>
						<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItemList.proof}');">查看收据</span>
						<c:choose>
							<c:when test="${recordAndReturnMap.reconciliationStatus!=3}">
								<a href="javascript:void(0)" onclick="updPrice('${customerItemList.buyCustomerItem.productName}','${customerItemList.buyCustomerItem.id}');" class="layui-btn layui-btn-mini">修改单价</a>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</tr>
		<tr>
			<td>合计</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td title="${recordAndReturnMap.totalSumCount}">${recordAndReturnMap.totalSumCount}</td>
			<td></td>
			<td title="${recordAndReturnMap.freightSum}">${recordAndReturnMap.freightSum}</td>
			<td title="${recordAndReturnMap.totalAmount}">${recordAndReturnMap.totalAmount}</td>
			<td class="oprationRe4"></td>
		</tr>
		</tbody>
	</table>
	</div>
	<div class="print_b">
		<input type="hidden" id="reconciliationId" name="reconciliationId" value="${recordAndReturnMap.reconciliationId}">
		<%--<button  class="layui-btn layui-btn-small" onclick="updatePrice();">确认提交</button>--%>
		<%--<span onclick="updatePrice();">确认提交</span>--%>
		<span id="print_sure">打印账单</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer');">返回</span>
		<%--<a href="javascript:void(0)" class="next_step" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer');">返回</a>--%>
	</div>
</div>

<!--商品价格变更审批-->
<div class="plain_frame price_change" id="updPriceDiv" style="display: none;">
	<ul>
		<li>
			<span>商品价格:</span>
			<input type="number" id="updPrice" name="updPrice"  min="0" >
		</li>
	</ul>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content" style="display:none;">
	<div class="big_img">
		<img id="bigImg">
	</div>
	<div class="view">
		<a href="#" class="backward_disabled"></a>
		<a href="#" class="forward"></a>
		<ul id="icon_list"></ul>
	</div>
</div>

<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $(".oprationRe").hide();
        $(".oprationRe1").hide();
        $(".oprationRe2").hide();
        $(".oprationRe3").hide();
        $(".oprationRe4").hide();
        $(".oprationRe").hide();
        $("#print_yc").jqprint();
        $(".oprationRe").show();
        $(".oprationRe1").show();
        $(".oprationRe2").show();
        $(".oprationRe3").show();
        $(".oprationRe4").show();
        $(".oprationRe").show();
    });
</script>