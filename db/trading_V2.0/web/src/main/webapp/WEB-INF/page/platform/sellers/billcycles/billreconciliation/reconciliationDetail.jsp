<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%--<%@ include file="../../../common/common.jsp"%>--%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billreconciliation/reconciliationDetail.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<%--<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">--%>

	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/print.css">
	<script src="<%=basePath%>/statics/platform/js/jquery-migrate-1.1.0.js"></script>
	<script src="<%=basePath%>/statics/platform/js/jquery.jqprint-0.3.js"></script>
</head>
<!--内容-->
<!--页签-->
<div class="print_m">
	    <%--<div class="ax_default label">
		   <div class="text" style="visibility: visible;">
			   <p>
				   <span style="font-family:'Arial Negreta', 'Arial';color:#FF6600;">${recordAndReturnMap.startBillStatementDateStr}-${recordAndReturnMap.endBillstatementDateStr}</span>
				   <span style="font-family:'Arial Negreta', 'Arial';">&nbsp; 期间，与购买方：</span>
				   <span style="font-family:'宋体 Bold', '宋体';color:#FF6600;">${recordAndReturnMap.buyCompanyName}</span>
				   <span style="font-family:'宋体 Bold', '宋体';color:#494949;"> 的对账明细</span>
			   </p>
		   </div>
	    </div>--%>
			<div id="print_yc">
			<h3 class="print_title">客户对账账单</h3>
			<table class="table_n">
				<tr>
					<td>客户：<span>${recordAndReturnMap.buyCompanyName}</span></td>
					<td>出账周期：<span>${recordAndReturnMap.startBillStatementDateStr}-${recordAndReturnMap.endBillstatementDateStr}</span></td>
					<td>状态：
						<span>
					  <c:choose>
						  <c:when test="${recordAndReturnMap.reconciliationStatus==2}">等待对账</c:when>
						  <c:when test="${recordAndReturnMap.reconciliationStatus==3}">已完成对账</c:when>
						  <c:when test="${recordAndReturnMap.reconciliationStatus==5}">审批已驳回</c:when>
					  </c:choose>
			</span></td>
				</tr>
			</table>

	<p ></p>
		<!--列表区-->
		<table id="reconciliationData" class="layui-table table_c" >
			<thead>
			<tr>
				<td style="width:10%">完成日期</td>
				<td style="width:23%">发货单号</td>
				<td style="width:10%">商品名称</td>
				<td style="width:10%">商品规格</td>
				<td style="width:5%">商品单位</td>
				<td style="width:10%">条形码</td>
				<td style="width:10%">商品单价</td>
				<td style="width:10%">修改单价</td>
				<td style="width:8%">商品数量</td>
				<td style="width:15%">物流编号</td>
				<td style="width:9%">运费</td>
				<td style="width:13%">商品金额</td>
				<td class="oprationRe" style="width:10%">操作</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<c:forEach var="sellerBillReconciliation" items="${recordAndReturnMap.recordItemList}">
					<tr class="text-center">
					   <td>${sellerBillReconciliation.arrivalDateStr}</td>
					   <td title="${sellerBillReconciliation.deliverNo}">${sellerBillReconciliation.deliverNo}</td>
					   <td>${sellerBillReconciliation.sellerDeliveryRecordItem.productName}</td>
					   <td>${sellerBillReconciliation.sellerDeliveryRecordItem.skuName}</td>
					   <td>${sellerBillReconciliation.sellerDeliveryRecordItem.unitName}</td>
					   <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.barcode}">${sellerBillReconciliation.sellerDeliveryRecordItem.barcode}</td>
					   <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.salePrice}">${sellerBillReconciliation.sellerDeliveryRecordItem.salePrice}</td>
				       <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.updateSalePrice}">${sellerBillReconciliation.sellerDeliveryRecordItem.updateSalePrice}</td>
					   <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.arrivalNum}">${sellerBillReconciliation.sellerDeliveryRecordItem.arrivalNum}</td>
				       <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.logisticsId}">${sellerBillReconciliation.sellerDeliveryRecordItem.logisticsId}</td>
				       <td title="${sellerBillReconciliation.sellerDeliveryRecordItem.freight}">${sellerBillReconciliation.sellerDeliveryRecordItem.freight}</td>
				       <td title="${sellerBillReconciliation.salePriceSum}">${sellerBillReconciliation.salePriceSum}</td>
						<td class="oprationRe1">
							<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="deliveryDetail('${sellerBillReconciliation.sellerDeliveryRecordItem.recordId}');">发货详情</span>
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${sellerBillReconciliation.attachmentList}');">查看收据</span>
						</td>
					</tr>
				</c:forEach>
            </tr>
			<tr>
				<c:forEach var="recordReplaceItemList" items="${recordAndReturnMap.recordReplaceItemList}">
					<tr class="text-center">
						<td>${recordReplaceItemList.arrivalDateStr}</td>
						<td title="${recordReplaceItemList.deliverNo}">${recordReplaceItemList.deliverNo}</td>
						<td>${recordReplaceItemList.sellerRecordReplaceItem.productName}</td>
						<td>${recordReplaceItemList.sellerRecordReplaceItem.skuName}</td>
						<td>${recordReplaceItemList.sellerRecordReplaceItem.unitName}</td>
						<td title="${recordReplaceItemList.sellerRecordReplaceItem.barcode}">${recordReplaceItemList.sellerRecordReplaceItem.barcode}</td>
						<td title="${recordReplaceItemList.sellerRecordReplaceItem.salePrice}">${recordReplaceItemList.sellerRecordReplaceItem.salePrice}</td>
				        <td title="${recordReplaceItemList.sellerRecordReplaceItem.updateSalePrice}">${recordReplaceItemList.sellerRecordReplaceItem.updateSalePrice}</td>
						<td title="${recordReplaceItemList.sellerRecordReplaceItem.arrivalNum}">${recordReplaceItemList.sellerRecordReplaceItem.arrivalNum}</td>
				        <td title="${recordReplaceItemList.sellerRecordReplaceItem.logisticsId}">${recordReplaceItemList.sellerRecordReplaceItem.logisticsId}</td>
			        	<td title="${recordReplaceItemList.sellerRecordReplaceItem.freight}">${recordReplaceItemList.sellerRecordReplaceItem.freight}</td>
				        <td title="${recordReplaceItemList.replacePriceSum}">${recordReplaceItemList.replacePriceSum}</td>
						<td class="oprationRe2">
							<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="deliveryDetail('${recordReplaceItemList.sellerRecordReplaceItem.recordId}');">换货详情</span>
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${recordReplaceItemList.attachmentList}');">查看收据</span>
						</td>
					</tr>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="customerItemList" items="${recordAndReturnMap.customerItemList}">
					<tr class="text-center">
						<td>${customerItemList.verifyDateStr}</td>
						<td title="${customerItemList.customerCode}">${customerItemList.customerCode}</td>
						<td>${customerItemList.sellerCustomerItem.productName}</td>
						<td>${customerItemList.sellerCustomerItem.skuName}</td>
						<td>${customerItemList.sellerCustomerItem.unitName}</td>
						<td>${customerItemList.sellerCustomerItem.skuOid}</td>
						<td title="${customerItemList.sellerCustomerItem.salePrice}">${customerItemList.sellerCustomerItem.salePrice}</td>
				        <td title="${customerItemList.sellerCustomerItem.updateSalePrice}">${customerItemList.sellerCustomerItem.updateSalePrice}</td>
						<td title="${customerItemList.sellerCustomerItem.arrivalNum}">${customerItemList.sellerCustomerItem.arrivalNum}</td>
				        <td title="${customerItemList.sellerCustomerItem.logisticsId}">${customerItemList.sellerCustomerItem.logisticsId}</td>
				        <td title="${customerItemList.sellerCustomerItem.freight}">${customerItemList.sellerCustomerItem.freight}</td>
			     	    <td title="${customerItemList.goodsPriceSum}">${customerItemList.goodsPriceSum}</td>
						<td class="oprationRe3">
							<span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/loadCustomerDetails?id=${customerItemList.sellerCustomerItem.recordId}','sellers')">售后详情</span>
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillReceipt('${customerItemList.proof}');">查看收据</span>
						</td>
					</tr>
				</c:forEach>
			</tr>
			<tr>
				<td>合计</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td title="${recordAndReturnMap.totalSumCount}">${recordAndReturnMap.totalSumCount}</td>
				<td></td>
				<td title="${recordAndReturnMap.freightSum}">${recordAndReturnMap.freightSum}</td>
				<td title="${recordAndReturnMap.totalAmount}">${recordAndReturnMap.totalAmount}</td>
				<td class="oprationRe4"></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="print_b">
		<span id="print_sure">打印账单</span>
		<span class="order_p" onclick="leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');">返回</span>
	</div>
</div>

<!--收款凭据-->
<div id="linkReceipt" class="receipt_content layui-layer-wrap" style="display:none;">
		<div class="big_img">
			<img id="bigImg">
		</div>
		<div class="view">
			<a href="#" class="backward_disabled"></a>
			<a href="#" class="forward"></a>
			<ul id="icon_list"></ul>
		</div>
</div>

<script type="text/javascript">
    //打印
    $("#print_sure").click(function(){
        $(".oprationRe").hide();
        $(".oprationRe1").hide();
        $(".oprationRe2").hide();
        $(".oprationRe3").hide();
        $(".oprationRe4").hide();
        $("#print_yc").jqprint();
        $(".oprationRe").show();
        $(".oprationRe1").show();
        $(".oprationRe2").show();
        $(".oprationRe3").show();
        $(".oprationRe4").show();
    });
</script>

