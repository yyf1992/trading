<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="el" uri="/elfun"%>
<style>
.cc:after{
    content: '';
    display: block;
    clear: both;
}
.cc input,.cc select{
    width:90px;
    height: 20px;
    margin-bottom: 5px;
}
</style>
<script>
$(function (){
	metaAll();
});
function metaAll(){
    var inputs=$('.table_pure span.barcodeAdd input');
    function checkedSpan(){
      inputs.each(function(){
        if($(this).prop('checked')==true){
          $(this).parent().addClass('checked')
        }else{
          $(this).parent().removeClass('checked')
        }
      })
    }
    $('.table_pure thead input').click(function(){
      inputs.prop('checked',$(this).prop('checked'));
      checkedSpan();
    });
    inputs.click(function(){
      checkedSpan();
      var r=$('.table_pure tbody span.barcodeAdd input:not(:checked)');
      if(r.length==0){
        $('.table_pure thead input').prop('checked',true).parent().addClass('checked');
      }else{
        $('.table_pure thead input').prop('checked',false).parent().removeClass('checked');
      }
    });
  }
  function selectProduct(){
  	$.ajax({
            url:"platform/buyer/customer/loadSelectProduct",
            data:$("#customerProductForm").serialize(),
            async:false,
            success:function(data){
                $("#customerProductDiv").html(data);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！", {icon : 2});
            }
        });
  }
</script>
<div id="customerProductDiv">
	<form action="platform/buyer/customer/loadSelectProduct" id="customerProductForm">
		<div class="cc mt">
			<span>货号：</span><input type="text" placeholder="货号" name="productCode" value="${searchPageUtil.object.productCode}" class="mr">
			<span>商品名称：</span><input type="text" placeholder="商品名称" name="productName" value="${searchPageUtil.object.productName}" class="mr">
			<span>规格代码：</span><input type="text" placeholder="规格代码" name="skuCode" value="${searchPageUtil.object.skuCode}" class="mr">
			<span>规格名称：</span><input type="text" placeholder="规格名称" name="skuName" value="${searchPageUtil.object.skuName}" class="mr">
			<span>条形码：</span><input type="text" placeholder="条形码" name="barcode" value="${searchPageUtil.object.barcode}" class="mr">
			<img src="${basePath }statics/platform/images/find.jpg" class="rt" onclick="selectProduct();" style="cursor: pointer;">
		    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		    <input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
		    <input id="notskuoId" name="notskuoId" type="hidden" value="${searchPageUtil.object.notskuoId}" />
		    <input id="supplierId" name="supplierId" type="hidden" value="${searchPageUtil.object.supplierId}" />
	    </div>
    </form>
	<div class="afterCommContent">
		<table class="table_pure">
			<thead>
				<tr>
					<td style="width:39px">
						<span class="barcodeAdd"><input type="checkbox"></span>
					</td>
					<td style="width:100px">货号</td>
					<td style="width:300px">商品规格</td>
					<td style="width:100px">条形码</td>
					<td style="width:200px">供应商</td>
					<td style="width:120px">最近采购单价</td>
				</tr>
			</thead>
			<tbody id="productTbody">
				<c:forEach var="product" items="${searchPageUtil.page.list}" varStatus="indexNum">
					<tr>
						<td>
							<span class="barcodeAdd"><input type="checkbox"></span>
						</td>
						<td>${product.product_code}</td>
						<td>
							<input type="hidden" name="product_code" value="${product.product_code}">
							<input type="hidden" name="product_name" value="${product.product_name}">
							<input type="hidden" name="sku_code" value="${product.sku_code}">
							<input type="hidden" name="sku_name" value="${product.sku_name}">
							<input type="hidden" name="supplier_id" value="${product.supplier_id}">
							${product.product_name}|${product.sku_code}|${product.sku_name}
						</td>
						<td>${product.barcode}</td>
						<td>${el:getCompanyById(product.supplier_id).companyName}</td>
						<td>${product.price}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="pager" id="page">${searchPageUtil.page}</div>
	</div>
</div>