<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!--列表区-->
<table class="table_pure purchaseOrder" style="width:100%">
	<thead>
		<tr>
			<td style="width:48px"><span class="chk_click"><input
					type="checkbox">
			</span>
			</td>
			<td style="width:90px">货号</td>
			<td style="width:120px">商品名称</td>
			<td style="width:100px">规格代码</td>
			<td style="width:69px">规格名称</td>
			<td style="width:86px">条形码</td>
			<td style="width:86px">计划采购总数量</td>
			<td style="width:53px">已生成采购计划</td>
			<!-- <td style="width:76px">已下单未审核采购量</td> -->
			<td style="width:53px">待生成采购计划</td>
			<td style="width:86px">操作</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="product" items="${searchPageUtil.page.list }"
			varStatus="indexNum">
			<tr>
				<td><span class="chk_click"><input type="checkbox"
						name="checkone" value="${product.barcode}">
				</span>
				</td>
				<td>${product.product_code}</td>
				<td>${product.product_name}</td>
				<td title="${product.sku_code}">${product.sku_code}</td>
				<td>${product.sku_name}</td>
				<td title="${product.barcode}">${product.barcode}</td>
				<td>${product.apply_count}</td>
				<td>${product.order_num}</td>
				<td>${product.apply_count - product.order_num}</td>
				<td><a href="javascript:void(0)"
					onclick="loadPurchaseDetails('${product.barcode}');"
					class="layui-btn layui-btn-normal layui-btn-mini"> <i
						class="layui-icon">&#xe695;</i>采购明细</a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>