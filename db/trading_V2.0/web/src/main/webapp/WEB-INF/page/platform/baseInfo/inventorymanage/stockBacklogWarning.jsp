<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<title>库存积压预警</title>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<script type="text/javascript">
//重置
function recovery(){
	$("#productCode").val("");
	$("#productName").val("");
	$("#skuCode").val("");
	$("#skuName").val("");
	$("#barcode").val("");
	$("#batchNo").val("");
	$("#minBacklogdaynum").val("");
	$("#maxBacklogdaynum").val("");
	$("#minDate").val("");
	$("#maxDate").val("");
	$("#businessType option:first").prop("selected","selected");
}
$(function(){
	$("#businessType option[value='"+"${searchPageUtil.object.businessType}"+"']").attr("selected","selected");
});
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(backlogWarningTableFilter)', function(obj){
	    var data = obj.data;
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('backlogWarningTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	productCode: $("#productCode").val(),
	        	barcode: $("#barcode").val(),
	        	skuCode: $("#skuCode").val(),
	        	batchNo: $("#batchNo").val(),
	        	businessType: $("#businessType").val(),
	        	minDate: $("#minDate").val(),
	        	maxDate: $("#maxDate").val()
	        }
	      });
	    },
	    //导出
	    exportExcel: function(){
			var formData = [
				"batchNo="+$("#batchNo").val(),
				"&barcode="+$("#barcode").val(),
				"&productCode="+$("#productCode").val(),
				"&skuCode="+$("#skuCode").val(),
				"&businessType="+$("#businessType").val(),
				"&minDate="+$("#minDate").val(),
				"&maxDate="+$("#maxDate").val()
			];
			var url = basePath + "stockBacklogWarningDownload/stockBacklogWarningData?" + formData.join("");
			window.open(url);
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="skuCode" id="skuCode" autocomplete="off" placeholder='型号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="batchNo" id="batchNo" autocomplete="off" placeholder='来源单号'>
  </div>
  <div class="layui-inline">
    <select id="businessType" name="businessType" style="width: 200px;">
       <option value="" selected>全部</option>
       <option value="29">采购系统入库</option>
       <option value="21">OMS采购入库</option>
       <option value="31">盘点入库</option>
       <option value="SW">同步WMS入库</option>
     </select>
  </div>
  <div class="layui-inline">
  	<div class="layui-input-inline">
      <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
    </div>
    <div class="layui-input-inline">
      <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
    </div>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="exportExcel">导出</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:20,
		id:'backlogWarningTable',
		height:'full-90',
		url:'<%=basePath %>platform/baseInfo/inventorymanage/loadStockBacklogWarningDataJson'
	}" lay-filter="backlogWarningTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'barcode', sort: true,show:true,showToolbar:true}">条形码</th>
            <th lay-data="{field:'productCode', sort: true,show:true}">货号</th>
            <th lay-data="{field:'skuName', sort: true,show:true}">规格名称</th>
            <th lay-data="{field:'businessType', sort: true,show:true,templet: '#businessTypeTpl', align: 'center'}">订单类型</th>
            <th lay-data="{field:'batchNo', sort: true,show:true}">来源单号</th>            
            <th lay-data="{field:'storageDate', sort: true,show:true}">入库日期</th>
            <th lay-data="{field:'number', sort: true,show:true}">入库数量</th>
            <th lay-data="{field:'remainingStock', sort: true,show:true}">积压库存</th>
            <th lay-data="{field:'backlogdaynum', sort: true,show:true}">库龄时间（天）</th>
        </tr>
    </thead>
</table>
<!-- 确认状态 -->
<script type="text/html" id="businessTypeTpl">
	{{#  if(d.businessType == 29){ }}
		<span>采购系统入库</span>
	{{#  } else if(d.businessType == 21){ }}
		<span>OMS采购入库</span>
	{{#  } else if(d.businessType == 31){ }}
		<span>盘点入库</span>
	{{#  } else if(d.businessType === 'SW'){ }}
		<span>同步WMS入库</span>
	{{#  } }}
</script>