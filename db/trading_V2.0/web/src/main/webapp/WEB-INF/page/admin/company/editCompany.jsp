<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun"%>
<!--外协弹框-->
<ul>
	<li><span>是否需要外协:</span>
	<c:choose>
			<c:when test="${editCompany.isNeedAssist==1}">
				<label><input type="radio" name="insideStatus" value="1" checked="checked">是</label>&nbsp;&nbsp;
				<label><input type="radio" name="insideStatus" value="0">否</label>
			</c:when>
			<c:when test="${editCompany.isNeedAssist==0}">
				<label><input type="radio" name="insideStatus" value="1">是</label>&nbsp;&nbsp;
				<label><input type="radio" name="insideStatus" value="0"checked="checked">否</label>
			</c:when>
	</c:choose>
	</li>
</ul>
