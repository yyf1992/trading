<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>

<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/seller_delivery.css">
<%--发货单管理列表--%>
<script type="text/javascript">
    $(function(){
        //设置选择的tab
        $(".tab>a").removeClass("hover");
        $(".tab #tab_${params.tabId}").addClass("hover");
        //日期
//        loadDate("startDate","endDate");
        //初始化数据
        selectDeliveryRecord();
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#resetButton").click();
            var index = id.split("_")[1];
            $("#deliveryRecordDiv input[name='tabId']").val(index);
            loadPlatformData('content');
        });
    });
    //查询数据
    function selectDeliveryRecord(){
        var formObj = $("#deliveryRecordListForm");
        $.ajax({
            url : basePath+"platform/seller/delivery/loadDeliveryRecordData",
            data:formObj.serialize(),
            async:false,
            success:function(data){
                $("#deliveryRecordContent").empty();
                var str = data.toString();
                $("#deliveryRecordContent").html(str);
                $(".tab>a").removeClass("hover");
                $(".tab #tab_"+$("#interest").val()).addClass("hover");
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
    //状态下拉改变
    function setInterest(value) {
        $("#tabId").val(value);
    }
    /**
     * 导出
     */
    function exportOrder() {
        var formData = $("#deliveryRecordListForm").serialize();
        var url = basePath+"platform/seller/delivery/exportDelivery?"+ formData;
        window.open(url);
    }
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_0">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_1">待发货（<span>${params.waitDeliveryNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_2">发货中（<span>${params.deliveringNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_3">已收货（<span>${params.receivedNum}</span>）</a><b>|</b>
	<a href="javascript:void(0)" id="tab_4">已终止（<span>${params.invalidNum}</span>）</a><b>|</b>
</div>
<div id="deliveryRecordDiv">
	<!--列表区-->
	<form id="deliveryRecordListForm" method="post" action="platform/seller/delivery/deliveryRecordList" >
		<ul class="order_search">
			<li class="comm">
				<label>发货单号:</label>
				<input type="text" placeholder="输入发货单号" id="deliverNo" name="deliverNo" value="${params.deliverNo}">
			</li>
			<li class="comm">
				<label>客户名称:</label>
				<input type="text" placeholder="输入客户名称" id="buycompanyName" name="buycompanyName" value="${params.buycompanyName}">
			</li>
			<li class="comm">
				<label>订单号:</label>
				<input type="text" placeholder="输入订单号" id="orderCode" name="orderCode" value="${params.orderCode}">
			</li>
			<li class="range">
				<label>发货日期:</label>
				<div class="layui-input-inline">
						<input type="text" name="startDate" id="startDate" lay-verify="date" value="${params.startDate}" class="layui-input" placeholder="开始日">
				</div> -
				<div class="layui-input-inline">
					<input type="text" name="endDate" id="endDate" lay-verify="date" value="${params.endDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range">
				<label>到货日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="arrivalDateBegin" id="arrivalDateBegin" lay-verify="date" value="${params.arrivalDateBegin}" class="layui-input" placeholder="开始日">
				</div> -
				<div class="layui-input-inline">
					<input type="text" name="arrivalDateEnd" id="arrivalDateEnd" lay-verify="date" value="${params.arrivalDateEnd}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="state">
				<label>交易状态:</label>
				<div class="layui-input-inline">
					<select name="interest" id="interest" lay-filter="aihao" onchange="setInterest(this.value)">
						<option value="0" <c:if test="${params.interest==0}">selected</c:if>>全部</option>
						<option value="1" <c:if test="${params.interest==1}">selected</c:if>>待发货</option>
						<option value="2" <c:if test="${params.interest==2}">selected</c:if>>发货中</option>
						<option value="3" <c:if test="${params.interest==3}">selected</c:if>>已收货</option>
						<option value="4" <c:if test="${params.interest==4}">selected</c:if>>已终止</option>
					</select>
				</div>
			</li>
			<li class="comm">
				<label>商品名称:</label>
				<input type="text" placeholder="输入商品名称" id="productName" name="productName" value="${params.productName}">
			</li>
			<li class="comm">
				<label>商品货号:</label>
				<input type="text" placeholder="输入商品货号" id="productCode" name="productCode" value="${params.productCode}">
			</li>
			<li class="comm">
				<label>规格代码:</label>
				<input type="text" placeholder="输入规格代码" id="skuCode" name="skuCode" value="${params.skuCode}">
			</li>
			<li class="comm">
				<label>规格名称:</label>
				<input type="text" placeholder="输入规格名称" id="skuName" name="skuName" value="${params.skuName}">
			</li>
			<li class="comm">
				<label>条形码:</label>
				<input type="text" placeholder="输入条形码" id="barcode" name="barcode" value="${params.barcode}">
			</li>
			<li>
				<button type="button" onclick="selectDeliveryRecord()" class="search">搜索</button>
				<button type="reset" id="resetButton" class="search">重置查询条件</button>
			</li>
		</ul>
		<input type="hidden" name="tabId" id="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="exportOrder();">
		<i class="layui-icon">&#xe8bf;</i> 导出
	</a>
	<div id="deliveryRecordContent"></div>
</div>