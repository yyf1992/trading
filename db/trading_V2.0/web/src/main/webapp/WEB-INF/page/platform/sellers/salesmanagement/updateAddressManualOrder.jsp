<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		loadProvince("updateProvince","${address.province}");
		// 根据省份加载市
		var provinceId = $("#updateProvince").val();
		loadCity(provinceId,"updateCity","${address.city}");
		// 根据市加载区
		var cityId = $("#updateCity").val();
		loadArea(cityId,"updateArea","${address.area}");
		//省改变
		$("#updateProvince").change(function(){
			// 根据省份加载市
			var provinceId = $("#updateProvince").val();
			loadCity(provinceId,"updateCity","");
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		//市改变
		$("#updateCity").change(function() {
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
	});
</script>
<!--修改地址弹出框-->
  <form action="">
  <input type="hidden" id="updateId" value="${updateAddress.id}">
    <div class="address_group">
      <span>收货人:</span>
      <input type="text" style="width:207px" value="${updateAddress.clientPerson}" id="updateClientPerson" placeholder="请填写收货人">
    </div>
    <div class="address_group">
      <span>收货地址:</span>
      <select name="province3" id="addr1" onchange="addcity();">
      	<option  value="">省份</option>
      	<c:forEach var="province" items="${provinceList}">
	 	  <option value="${province.provinceId}">${province.province}</option>
	    </c:forEach>
      </select>
      <select  name="city3" id="addr2" onchange="addarea();">
      	<option value="">地级市</option>
      </select>
      <select name="area3" id="addr3">
      	<option value="">县区</option>
      </select>
    </div>
    <div class="address_group">
      <span>详细地址:</span>
      <input type="text" style="width:555px" value="${updateAddress.shipAddress}" id="addr4" placeholder="请如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息">
    </div>
    <div class="address_group">
      <span>手机号码:</span>
      <input type="tel" style="width:207px" value="${updateAddress.clientPhone}" id="updateclientPhone" placeholder="请填写收货人手机号">
      <span>固定电话:</span>
      <input type="text" style="width:65px" value="${updateAddress.zone}" id="updateZone" placeholder="区号">
      -
      <input type="text" style="width:110px" value="${updateAddress.telno}" id="updateTelNo" placeholder="座机号">
    </div>
    <div class="address_group">
      <span></span>
      <label><input type="checkbox" id="updateIsDefault" <c:if test="${updateAddress.isDefault == 1}">checked="checked"</c:if> > 设为默认地址</label>
    </div>
  </form>
