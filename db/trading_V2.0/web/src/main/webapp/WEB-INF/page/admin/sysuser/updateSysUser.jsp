<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$("#updateSysUserForm").validate({
	rules:{
		userName:{
			required: true,
			maxlength:20
		}
	},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.form-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.form-group').removeClass('error');
		$(element).parents('.form-group').addClass('success');
	},
	submitHandler : function(){
		$.ajax({
		  	url : basePath+"admin/adminSysUser/saveUpdate",
		  	type: "post",
		  	data:$("#updateSysUserForm").serialize(),
		  	async:false,
		  	success:function(data){
		  		var result=eval('('+data+')');
		  		var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						loadAdminData();
						//阻止表单提交
						return false;
					} else {
						layer.msg(resultObj.msg,{icon:2});
						//阻止表单提交
						return false;
					}
		  	},
		  	error:function(){
		  		layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  		//阻止表单提交
				return false;
		  	}
		});
	}
});
</script>
<form class="form-inline " id="updateSysUserForm" novalidate="novalidate">
<div class="modal-content" style="width: 600px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel">修改系统用户</h4>
	</div>
	<div class="modal-body row-fluid ">
		<div class=" padding-top-sm row">
				<div class="form-group col-md">
					<label for="exampleInputEmail2" class="label-four">登录名：</label> 
					<input value="${sysUser.loginName}"
						type="text" class="form-control-new" name="loginName" placeholder="登录名" style="width: 60%" readonly="readonly">
				</div>
				<div class="form-group col-md padding-top-sm">
					<label for="exampleInputEmail2" class="label-remark">姓名：</label> 
					<input value="${sysUser.userName}"
						type="text" class="form-control-new" name="userName" placeholder="姓名" style="width: 60%">
				</div>
				<div class="form-group col-md padding-top-sm">
					<label>帐号类型：
					<c:choose>
						<c:when test="${sysUser.isParent == 1}">主账号</c:when>
						<c:otherwise>子账号</c:otherwise>
					</c:choose></label>
				</div>
				<div class="form-group col-md padding-top-sm">
					<label class="label-remark">状态：
					<c:if test="${sysUser.isDel == 0}">
						<input type="radio" name="status" checked value="0"/>正常
						<input type="radio" name="status" value="1"/>已删除
					</c:if>
					<c:if test="${sysUser.isDel == 1}">
						<input type="radio" name="status"  value="0"/>正常
						<input type="radio" name="status" checked value="1"/>已删除
					</c:if></label>
				</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
		<button type="submit" class="btn btn-sm2 btn-00967b" id="saveButton">保存</button>
	</div>
</div>
	<!--隐藏域  -->
	<input type="hidden" name="id" value="${sysUser.id}">
</form>
