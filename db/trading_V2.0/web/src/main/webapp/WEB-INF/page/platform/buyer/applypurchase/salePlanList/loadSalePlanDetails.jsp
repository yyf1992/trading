<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../../common/path.jsp"%>
<link rel="stylesheet" href="${basePath}statics/platform/css/supplier.css">
<script type="text/javascript">
	// 审核
	function verify(id){
		if($(".verifyDetail").length>0)$(".verifyDetail").remove();
		$.ajax({
			url:"platform/tradeVerify/verifyDetailByRelatedId",
			data:{"id":id},
			success:function(data){
				var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
				detailDiv.addClass("verifyDetail");
				detailDiv.html(data);
				detailDiv.appendTo($("#salePlanDetailsDiv"));
				detailDiv.find("#verify_side").addClass("show");
			},
			error:function(){
				layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
			}
		});
	}
</script>
<div id="salePlanDetailsDiv">
	<h4 class="currentTitle">销售计划明细</h4>
	<div class="padding-sm-lr currentContent">
		<div class="mp">
			<span class="currentLabel">当前状态：</span>
			<span class="currentState size_sm">
				<c:choose>
					<c:when test="${salePlanHeader.status==0}">等待审核</c:when>
					<c:when test="${salePlanHeader.status==1}">审核通过</c:when>
					<c:when test="${salePlanHeader.status==2}">审核不通过</c:when>
				</c:choose>
			</span>
		</div>
		<ul class="currentDetail mp30 mt">
			<li><span>编&emsp;&emsp;号</span>： ${salePlanHeader.planCode}</li>
			<li><span>标题</span>： ${salePlanHeader.title}</li>
			<li><span>申 请 人</span>： ${salePlanHeader.createName}</li>
			<li><span>申请日期</span>： <fmt:formatDate value="${salePlanHeader.createDate }" type="both"/></li>
			<li><span>销售周期</span>： 
				<fmt:formatDate value="${salePlanHeader.startDate}" type="date"></fmt:formatDate> 至 <fmt:formatDate
					value="${salePlanHeader.endDate}" type="date"></fmt:formatDate>
			</li>
		</ul>
		<div class="currentExplain">
			<span>说&emsp;&emsp;明：</span>
			<p>
				${salePlanHeader.remark}
			</p>
		</div>
		<p class="line mp30"></p>
		<%-- <div class="text-right mp mt">
			<span id="currentApproval" onclick="verify('${purchaseHeader.id}');"><img src="<%=basePath%>statics/platform/images/planApproval.jpg">
			</span>
		</div> --%>
	</div>
	<h4 class="currentTitle">销售计划清单</h4>
	<div class="padding-sm-lr currentContent" style="width: 100%; overflow: auto;">
		<table class="table_pure mp30 ApprovalList">
			<thead>
				<tr>
					<td width="100px">部门</td>
					<td width="400px">商品</td>
					<td width="50px">单位</td>
					<td width="100px">商品类型</td>
					<td width="60px">销售计划</td>
					<td width="87px">入仓量</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${salePlanHeader.itemList}">
					<tr>
						<td>${item.shopName}</td>
						<td>${item.productCode}|${item.productName}|${item.skuCode}|${item.skuName}|${item.barcode}</td>
						<td>${item.unitName}</td>
						<td>
							<c:choose>
	                            <c:when test="${item.productType=='0'}">成品</c:when>
	                            <c:when test="${item.productType=='1'}">原材料</c:when>
	                            <c:when test="${item.productType=='2'}">辅料</c:when>
	                            <c:when test="${item.productType=='3'}">虚拟产品</c:when>
	                            <c:otherwise></c:otherwise>
	                        </c:choose>
						</td>
						<td>${item.salesNum}</td>
						<td>${item.putStorageNum}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="mp30">
			<div class="size_sm mm30">
				附件：<c:if test="${salePlanHeader.proof != null && salePlanHeader.proof != ''}">
						<span class="c66">${fn:substringAfter(salePlanHeader.proof, "upload/")}</span>
						<%-- <span class="planView"><img src="<%=basePath%>statics/platform/images/planV.jpg" onclick="preview('${purchaseHeader.proof}');"> </span> --%>
						<span class="plandownLoad"><img src="<%=basePath%>statics/platform/images/planD.jpg" onclick="window.open('${salePlanHeader.proof}');"> </span>					
					</c:if>
			</div>
		</div>
	</div>
	<c:if test="${verifyDetails != '1'}"><!--审核查看详情不显示返回按钮-->
		<div class="text-right" style="margin-top: 40px;">
			<a href="javascript:void(0)" onclick="leftMenuClick(this,'buyer/salePlan/salePlanList?${form}','buyer','17070718133683994602');"><span class="contractBuild">返回</span></a>
		</div>
	</c:if>
</div>
