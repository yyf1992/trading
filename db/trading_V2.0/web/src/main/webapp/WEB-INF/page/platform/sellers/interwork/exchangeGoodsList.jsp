<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function(){
        //搜索更多
        $(".search_more").click(function(){
            $(this).toggleClass("clicked");
            $(this).parent().nextAll("ul").toggle();
        });
        //设置选择的tab
        $(".tab>a").removeClass("hover");
        $(".tab #tab_${params.tabId}").addClass("hover");

        //日期
//        loadDate("startDate","endDate");
        //初始化数据
        //selectData();
        $("#orderContent #selectButton").click(function(e){
            e.preventDefault();
            selectData();
        });
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#orderContent #resetButton").click();
            var index = id.split("_")[1];
            $("#orderContent input[name='tabId']").val(index);
            loadPlatformData('content');
        });
    });
    //功能点1:全选事件
    function selAll(){
        var inputs=$('.order_list>p>span.chk_click input');
        function checkedSpan(){
            inputs.each(function(){
                if($(this).prop('checked')==true){
                    $(this).parent().addClass('checked')
                }else{
                    $(this).parent().removeClass('checked')
                }
            })
        }

        inputs.click(function(){
            if($(this).prop('checked')==true){
                $(this).parent().addClass('checked')
            }else{
                $(this).parent().removeClass('checked')
            }
            var r=$('.order_list>p>span.chk_click input:not(:checked)');
            if(r.length==0){
                $('.batch_handle input').prop('checked',true).parent().addClass('checked');
            }else{
                $('.batch_handle input').prop('checked',false).parent().removeClass('checked');
            }
        });
    }
    selAll();
    $('#deliver_b').click(function(){
        var layer = layui.layer,form=layui.form;
        var orderIds = "";//订单ID
        if($("input:checkbox[name='checkone']:checked").length < 1){
            layer.msg("至少选中一项！",{icon:2});
            return;
        }else{
            //判断是否是同一家采购商
			var checkedItems = $("input:checkbox[name='checkone']:checked");
            var buyerIdFirst = $(checkedItems[0]).parent().parent().find("input[name='buyerId']").val();//第一条选中数据的采购商ID
            orderIds = $(checkedItems[0]).val();
			for(var i=1;i<checkedItems.length;i++){
			    var currBuyerId = $(checkedItems[i]).parent().parent().find("input[name='buyerId']").val();
			    var currOrderId = $(checkedItems[i]).val();
				if(buyerIdFirst!=currBuyerId){
					layer.msg("必须同一家采购商才可以合并发货！",{icon:2});
					return;
				}else{
                    orderIds += "," + currOrderId;
				}

			}
            //接单之后才可以合并发货
            for(var i=0;i<checkedItems.length;i++){
                var statusFlag = $(checkedItems[i]).parent().parent().find("input[name='status']").val();
                if(statusFlag=='0'){
                    layer.msg("必须接单之后才可以合并发货！",{icon:2});
                    return;
                }
            }

		}
        layer.confirm('您确定要将选中的商品合并发货吗?', {
            icon:3,
            title:'选中合并发货',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            leftMenuClick(this,'platform/seller/delivery/mergeSendGoods?orderIds='+orderIds+',buyCompanyId='+buyerIdFirst,'sellers');
        });
    });
    function selectData(){
        var formObj = $("#orderContent").find("form");
        $.ajax({
            url : "platform/seller/interworkGoods/exchangeGoodsList",
            data:formObj.serialize(),
            async:false,
            success:function(data){
                $("#orderContent").empty();
                var str = data.toString();
                $("#orderContent").html(str);
                $(".tab>a").removeClass("hover");
                $(".tab #tab_"+$("#interest").val()).addClass("hover");
                loadVerify();
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
</script>
<div id="orderContent">
    <!--搜索栏-->
    <form class="layui-form" action="platform/seller/interworkGoods/exchangeGoodsList" >
        <ul class="order_search seller_order">
            <li class="comm">
                <label>客户名称:</label>
                <input type="text" placeholder="输入客户名称进行搜索" id="companyName" name="companyName" style="width:262px" value="${searchPageUtil.object.companyName}">
            </li>
            <li class="state nomargin">
                <label>交易状态:</label>
                <div class="layui-input-inline">
                    <select name="interest" id="interest" lay-filter="aihao">
                        <option value="99" <c:if test="${searchPageUtil.object.interest==99}">selected</c:if>>全部</option>
                        <option value="0" <c:if test="${searchPageUtil.object.interest==0}">selected</c:if>>待接单</option>
                        <option value="1" <c:if test="${searchPageUtil.object.interest==1}">selected</c:if>>待发货</option>
                        <option value="2" <c:if test="${searchPageUtil.object.interest==2}">selected</c:if>>发货中</option>
                        <option value="3" <c:if test="${searchPageUtil.object.interest==3}">selected</c:if>>发货完成</option>
                        <option value="4" <c:if test="${searchPageUtil.object.interest==4}">selected</c:if>>已收货</option>
                    </select>
                </div>
            </li>
            <li class="range">
                <label>订单时间:</label>
                <div class="layui-input-inline">
                    <input type="text" name="startDate" id="startDate" lay-verify="date" value="${searchPageUtil.object.startDate}" class="layui-input" placeholder="开始日">
                </div>-
                <div class="layui-input-inline">
                    <input type="text" name="endDate" id="endDate" lay-verify="date" value="${searchPageUtil.object.startDate}" class="layui-input" placeholder="截止日">
                </div>
            </li>
            <li class="comm">
                <label>&nbsp;&nbsp;订单号:</label>
                <div class="layui-input-inline">
                	<input type="text" placeholder="订单号" id="orderCode" name="orderCode" value="${searchPageUtil.object.orderCode}">
                </div>
            </li>
            <li class="range"><button type="button" onclick="selectData()"  class="search">搜索</button></li>
            <li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
        </ul>
        <input type="hidden" name="tabId" value="${searchPageUtil.object.tabId}">
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
        <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
    </form>
    <div id="tabContent">
    <table class="order_detail">
	<tr>
		<td style="width:76%">
			<ul>
				<li style="width:30%">商品</li>
				<li style="width:15%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">数量</li>
				<li style="width:10%">单价</li>
				<li style="width:10%">总价</li>
				<li style="width:10%">是否索要发票</li>
				<li style="width:10%">备注</li>
			</ul></td>
		<td style="width:12%">交易状态</td>
		<td style="width:12%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list" id="dataList">
		<p>
			<span class="chk_click"><input type="checkbox" value="${orders.id}" name="checkone"></span>
			<span class="apply_time"><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></span>
			<span class="order_num">订单号: <b>${orders.orderCode}</b></span>
			<span>采购商：${orders.companyName}</span>
			<input type="hidden" value="${orders.companyId}" name="buyerId">
			<input type="hidden" value="${orders.status}" name="status">
		</p>
		<table>
			<tr>
				<td style="width:76%">
					<c:forEach var="product" items="${orders.supplierProductList}" varStatus="i">
						<ul class="clear">
							<li style="width:30%">
								<%--<img src="images/04.jpg">--%>
									<span class="defaultImg"></span>
								<div>${product.productCode} ${product.productName}<br>
									<span>规格代码: ${product.skuCode}</span>
									<span>规格名称: ${product.skuName}</span>
								</div>
							</li>
							<li style="width:15%">${product.barcode}</li>
							<li style="width:5%">${product.unitName}</li>
							<li style="width:10%">${product.orderNum}</li>
							<li style="width:10%">${product.price}</li>
							<li style="width:10%">${product.price*product.orderNum}</li>
							<li style="width:10%">
								<c:choose>
									<c:when test="${product.isNeedInvoice == 'N'}">否</c:when>
									<c:otherwise>是</c:otherwise>
								</c:choose>
							</li>
							<li style="width:10%">${product.remark}</li>
						</ul>
					</c:forEach>
				</td>

				<td style="width:12%">
					<div>
						<c:choose>
							<c:when test="${orders.status==0}">待接单</c:when>
							<c:when test="${orders.status==1}">待发货</c:when>
							<c:when test="${orders.status==2}">发货中</c:when>
							<c:when test="${orders.status==3}">发货完成</c:when>
							<c:when test="${orders.status==4}">已收货</c:when>
							<c:when test="${orders.status==5}">交易完成</c:when>
							<c:when test="${orders.status==6}">已取消订单</c:when>
							<c:when test="${orders.status==7}">已驳回订单</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>
					<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/interworkGoods/interworkGoodsDetail.html?orderCode=${orders.orderCode}'
							+'&returnUrl=platform/seller/interworkGoods/exchangeGoodsList&menuId=17101214335653863179','sellers');" class="approval">订单详情</a>
				</td>
				<td style="width:12%">
					<c:choose>
						<c:when test="${orders.status==0}">
							<a href="javascript:void(0)" onclick="approveOrder('${orders.id}');" class="deliver_btn">接单</a>
							<a href="javascript:void(0)" onclick="cancelOrder('${orders.id}');" class="deliver_btn">取消订单</a>
						</c:when>
						<c:when test="${(orders.status==1||orders.status==2) && (orders.goodsNum-orders.deliveryTotalNum>0)}">
							<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/delivery/singleSendGoods?id=${orders.id}','sellers');" class="deliver_btn">创建发货单</a>
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>

				</td>
			</tr>
		</table>
	</div>
</c:forEach>

<!--合并发货-->
<div class="text-center">
	<button class="layui-btn layui-btn-normal layui-btn-small" id="deliver_b">合并发货</button>
</div>

<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
    </div>
</div>