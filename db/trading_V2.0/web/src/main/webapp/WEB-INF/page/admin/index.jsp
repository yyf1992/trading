<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>诺泰权限系统</title>
<%@ include file="../platform/common/path.jsp"%>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="<%=basePath%>statics/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<%=basePath%>statics/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=basePath%>statics/css/AdminLTE.min.css">
<link rel="stylesheet" href="<%=basePath%>statics/css/all-skins.min.css">
<link rel="stylesheet" href="<%=basePath%>statics/css/main.css">

<script src="<%=basePath%>statics/libs/jquery.min.js"></script>
<script src="<%=basePath%>statics/libs/bootstrap.min.js"></script>
<script src="<%=basePath%>statics/libs/jquery.slimscroll.min.js"></script>
<script src="<%=basePath%>statics/libs/fastclick.min.js"></script>
<script src="<%=basePath%>statics/plugins/layer/layer.js"></script>
<script src="<%=basePath%>js/index.js"></script>

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script>
</script>
</head>
<body class="skin-blue sidebar-mini">
	<div class="wrapper" id="rrapp">
		<header class="main-header">
			<a href="javascript:void(0);" class="logo">
				<span class="logo-mini"><b>诺泰</b> </span> 
				<span class="logo-lg"><b>诺泰权限系统</b></span>
			</a>
			<nav class="navbar navbar-static-top" role="navigation">
				<a href="<%=basePath%>admin/adminIndexHtml" data-toggle="offcanvas"
					role="button" class="sidebar-toggle"><span class="sr-only">Toggle
						navigation</span>
				</a>
				<div
					style="float: left; color: rgb(255, 255, 255); padding: 15px 10px;">欢迎${sessionScope.CURRENT_USER.userName}
				</div>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li><a href="<%=basePath%>admin/logout"><i
								class="fa fa-sign-out"></i> &nbsp;退出系统</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar" style="height: auto;">
				<ul class="sidebar-menu">
					<li class="header">导航菜单</li>
					<li class="submenu">
						<a href="javacript:void(0)">
							<i class="fa fa-cog"></i>
							<span>系统管理</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>admin/sysMenu/loadMenuList.html','菜单管理');"><i class="fa fa-th-list"></i>菜单管理</a>
							</li>
							<li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>admin/adminSysRole/loadSysRoleList.html','角色管理');"><i class="fa fa-user-secret"></i>角色管理</a>
							</li>
							<%-- <li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>admin/adminSysUser/loadUserList.html','人员管理');"><i class="fa fa-user"></i>人员管理</a>
							</li> --%>
							<li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>admin/adminCompany/loadCompanyList.html','公司管理');"><i class="fa fa-list-alt"></i>公司管理</a>
							</li>
							<li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>druid','SQL监控');"><i class="fa fa-bug"></i>SQL监控</a>
							</li>
							<li>
								<a href="javacript:void(0)" onclick="openMenu(this,'<%=basePath%>admin/generator/generator.html','代码生成器');"><i class="fa fa-bug"></i>代码生成器</a>
							</li>
						</ul>
					</li>
				</ul>
			</section>
		</aside>
		<div class="content-wrapper" style="min-height: 100%;">
			<section class="content-header">
				<ol id="nav_title" class="breadcrumb"
					style="position: static; float: none;">
					<li class="active"><i class="fa fa-home"
						style="font-size: 20px; position: relative; top: 2px; left: -3px;"></i>
						&nbsp; 首页</li>
					<li class="active">控制台</li>
				</ol>
			</section>
			<section class="content" style="background:#fff;">
				<iframe scrolling="yes" frameborder="0"
					src="<%=basePath%>admin/adminMainHtml"
					style="width:100%;min-height:200px;overflow:visible;background:#fff;"></iframe>
			</section>
		</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs">Version 1.6.0</div>
			Copyright © 2017 <a href="http://www.nuotai.com/" target="_blank">nuotai.com</a>
			All Rights Reserved
		</footer>
		<div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
	</div>
</body>
</html>