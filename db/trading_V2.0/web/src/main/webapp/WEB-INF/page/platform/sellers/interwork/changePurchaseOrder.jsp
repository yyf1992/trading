<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.planList select{
	width: 90%
}
.planList tbody td{
    overflow:visible;
}
.layui-form-select{
	min-width: 80px;
}
</style>
<script type="text/javascript">
//加载要求到货日期
function loadPredictArredDate(dateId){
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		laydate.render({
			elem: '#'+dateId
		});
    });
}
$(function(){
	var form = layui.form;
	// 选择供应商获取商品单价
    form.on('select(pageFilter)', function(data){
        var obj = data.elem;
		getProductPrice(obj);
    });
	$("select[name='supplierName']").trigger("change");//触发供应商下拉的onchange事件
	//仓库下拉
	var wareObj={};
	wareObj.divId="changeWarehouseSelect";
	wareObj.selectValue="53";//默认质检仓
	warehouseSelect(wareObj);
	$("#beatchCreateOrderTbody tr").each(function(){
		$(this).find("td[name='warehouseSelectTd']").html($("#changeWarehouseSelect").html());
	});
	form.render("select");
	//加载日期
	$("#beatchCreateOrderTbody table[name='tableBig'] tr[name^='supplierTr_']").each(function(){
		var predictArredId = $(this).find("td[name='predictArredTd']").find("input[name='predictArred']").attr("id");
		loadPredictArredDate(predictArredId);
	});
	$('.planList').on('click','tbody td:first-child b',function(){
		layer.confirm('<p class="size_sm text-center"><img src="${basePath}statics/platform/images/icon.jpg">&emsp;确定要删除此商品吗？</p>',{
			skin:'popBuyer popB25 btnCenter deleteBuyer',
			title:'提醒',
			closeBtn:0,
			area:['310px','auto']
		},function(index){
			layer.close(index);
			// 删除子集
			delRow(this);
		}.bind(this));
	});
});
function delSupplierRow(obj,barcode){
	var supplierId = $(obj).parents("tr").find("td:eq(0)").find("input[name='supplierId']").val();
	var addSize = $(obj).parents("tr[name^='product_']").find("td:eq(3)").find("input[name='addSize']").val();
	layer.confirm("您确定要删除此供应商吗？</p>",{
	    icon:3,
	    skin:'popBuyer btnCenter',
	    title:'提醒',
	    closeBtn:0
	  },function(index){
	    layer.close(index);
		var shopRow = $("tr[name^='supplierTr_"+barcode+"_']").length;
		if(shopRow == 1){
			layer.msg("至少保留一个供应商！",{icon:7});
			return;
		}else{
			$(obj).parents("tr[name^='product_']").find("td:eq(3)").find("input[name='addSize']").val(Number(addSize)-1);
			$("tr[name='supplierTr_"+barcode+"_"+supplierId+"']").remove();//删除供应商
		}
	  }.bind(this));
}
//提交
function submit(){
	var unitError = "";
	var supplierError = "";
	var priceError = "";
	var warehouseError = "";
	var invoiceError = "";
	var infoStr = "";
	var submitError = "";
	var predictArredError = "";
	var goodsNumberError = "";
	var supplierError = "";
	$("#beatchCreateOrderTbody table[name='tableBig'] tr[name^='supplierTr_']").each(function(i){
		var supplierId = $(this).find("input[name='supplierId']").val();
		var supplierName = $(this).find("input[name='supplierName']").val();
		var person = $(this).find("input[name='person']").val();
		if(person == ""){
			person = " ";
		}
		var phone = $(this).find("input[name='phone']").val();
		if(phone == ""){
			phone = " ";
		}
		var productCode = $(this).find("input[name='productCode']").val();
		var productName = $(this).find("input[name='productName']").val();
		var skuCode = $(this).find("input[name='skuCode']").val();
		var skuName = $(this).find("input[name='skuName']").val();
		var skuOid = $(this).find("input[name='barcode']").val();
		if(supplierId == ""){
			supplierError = "商品："+skuOid+"，未关联供应商！";
			return false;
		}
		//单位
		var unitId = $("#unit_"+skuOid).find("option:selected").val();
		if(unitId == ''){
			unitError = "商品："+skuOid+"，没有设置单位！";
			return false;
		}
		// 采购单价
		var price = $(this).find("td[name='priceTd']").find("input").val();
		if(price == ""){
			priceError = "第"+(i+1)+"行没有填写采购单价！";
			return false;
		}
		// 采购数量
		var purchaseNum = $(this).find("td[name='purchaseNumTd']").find("input[name='purchaseNum']").val();
		if(purchaseNum == ''){
			goodsNumberError = "商品："+skuOid+"，供应商："+supplierName+"，没有填写采购数量！";
			return false;
		}
		//仓库
		var warehouseId = $(this).find("td[name='warehouseSelectTd']").find("option:selected").val();
		if(warehouseId == ''){
			warehouseError = "商品:"+skuOid+"，供应商："+supplierName+"，没有设置仓库！";
			return false;
		}
		//预计到货日期
		var predictArred = $(this).find("td[name='predictArredTd']").find("input").val();
		if(predictArred==""||predictArred==undefined||predictArred==null){
			predictArredError = "第"+(i+1)+"行要求到货日期不能为空！";
			return false;
		}
		var remark = $(this).find("td[name='remarksTd']").find("textarea").val();
		if(remark == ""){
			remark = " ";
		}
		//定义附件数组
		var costItem = {};
		var productInfo = productCode + ".NTNT." + productName + ".NTNT." + skuCode + ".NTNT." + skuName + ".NTNT." + skuOid
			+ ".NTNT." + unitId + ".NTNT." + price + ".NTNT." + purchaseNum + ".NTNT." + warehouseId + ".NTNT." + remark
			+ ".NTNT." + predictArred;
		//检查填写的数量是否大于采购数量
		var err = getErr(productInfo);
		if(err!=""){
			goodsNumberError = err;
			return false;
		}
		costItem["shopList"] = getShopList(productInfo,supplierId);
		infoStr = infoStr + supplierId + ".NTNT." + person + ".NTNT." + phone + ".NTNT." + supplierName + ".NTNT." 
				+ JSON.stringify(costItem) + "@";
	});
	infoStr = infoStr.substring(0, infoStr.length - 1);
	if(supplierError != ''){
		layer.msg(supplierError,{icon:2});
		return;
	}
	if(unitError != ''){
		layer.msg(unitError,{icon:2});
		return;
	}
	if(supplierError != ''){
		layer.msg(supplierError,{icon:2});
		return;
	}
	if(priceError != ''){
		layer.msg(priceError,{icon:2});
		return;
	}
	if(invoiceError != ''){
		layer.msg(invoiceError,{icon:2});
		return;
	}
	if(warehouseError != ''){
		layer.msg(warehouseError,{icon:2});
		return;
	}
	if(predictArredError != ''){
		layer.msg(predictArredError,{icon:2});
		return;
	}
	if(goodsNumberError != ''){
		layer.msg(goodsNumberError,{icon:2});
		return;
	}
	$.ajax({
		type : "post",
		url:"platform/buyer/buyOrder/changePurchaseOrderNext",
		data:{
			"buyOrderId" : $("#buyOrderId").val(),
			"infoStr" : infoStr
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function getErr(productInfo){
	var product = productInfo.split(".NTNT.");
	var err = "";
	var supplierIdArr = "";
	var totalGoodNum = 0;
	var purchaseNum = 0;
	$("#table_"+product[4]+" tr[name^='supplierTr_"+product[4]+"_']").each(function(){
		var supplierId = $(this).find("td:eq(0)").find("input[name='supplierId']").val();
		var supplierName = $(this).find("td:eq(0)").find("input[name='supplierName']").val();
		if(supplierIdArr.indexOf(supplierId+",")>-1){
			err = "供应商："+supplierName+"，重复！";
		}else{
			supplierIdArr += supplierId+",";
		}
		
		var goodNum = $(this).find("td[name='purchaseNumTd']").find("input[name='purchaseNum']").val();
		purchaseNum = $(this).parents("tr").find("td:eq(2)").text();
		totalGoodNum = Number(totalGoodNum) + Number(goodNum);
	});
	if(Number(totalGoodNum) != Number(purchaseNum)){
		err = "商品："+product[4]+"，采购数量必须等于总数量！";
	}
	return err;
}
function getShopList(productInfo,supplierId){
	var product = productInfo.split(".NTNT.");
	var shopList = new Array();
	var pObj = $("#table_"+product[4]+" tr[name^='supplierTr_"+product[4]+"_"+supplierId+"']");
	var shop = {};
	// 采购数量
	var goodsNumber = $(pObj).find("td[name='purchaseNumTd']").find("input").val();
	shop["goodsNumber"] = goodsNumber;
	shop["productCode"] = product[0];
	shop["productName"] = product[1];
	shop["skuCode"] = product[2];
	shop["skuName"] = product[3];
	shop["skuOid"] = product[4];
	shop["unitId"] = product[5];
	shop["price"] = product[6];
	shop["warehouseId"] = product[8];
	shop["remark"] = product[9];
	shop["predictArred"] = product[10];
	shopList.push(shop);
	return shopList;
}
// 选择供应商获取商品单价
function getProductPrice(obj){
	var skuOid = $(obj).parents("td").find("input[name='barcode']").val();
	var supplier = $(obj).val();
	if(supplier == null){
		layer.msg("商品："+skuOid+"，未关联供应商！",{icon:2});
		return;
	}
	var supplierId = supplier.split(",")[0];
	//tr的name赋值
	$(obj).parents("table[name='tableBig'] tr").attr("name","supplierTr_"+skuOid+"_"+supplierId);
	$(obj).parents("table[name='tableBig'] tr").find("td:eq(4)").find("input[name='predictArred']").attr("id","predictArred_"+skuOid+"_"+supplierId);
	var parentTr = $(obj).parents("table[name='tableBig'] tr[name^='supplierTr_']");
	//下一行tr的name赋值
	var objN =$(parentTr).nextAll();
	$.each(objN,function(){
		var trName = $(this).attr("name");
		if(trName.lastIndexOf("supplierTr_")>0){
			return false;
		}
		if($(this).find("td:first input").length > 0){
			$(this).find("td:eq(0)").find("input[name='supplierId']").val(supplierId);
		}
	});
	$(obj).parents("table[name='tableBig'] tr").find("td:eq(0)").find("input[name='supplierId']").val(supplierId);
	$(obj).parents("table[name='tableBig'] tr").find("td:eq(0)").find("input[name='person']").val(supplier.split(",")[1]);
	$(obj).parents("table[name='tableBig'] tr").find("td:eq(0)").find("input[name='phone']").val(supplier.split(",")[2]);
	$(obj).parents("table[name='tableBig'] tr").find("td:eq(0)").find("input[name='supplierName']").val($(obj).find("option:selected").text());
	$.ajax({
		type : "post",
        url : basePath+"buyer/applyPurchaseHeader/getProductPriceBySupplier",
        data: {
            "supplierId" : supplierId,
            "skuOid" : skuOid
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			$(obj).parents("table[name='tableBig'] tr").find("td[name='priceTd']").find("input").val(resultObj.price);//采购单价
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
//精确的乘法结果
function accMul(arg1,arg2){
	var m=0,s1=arg1.toString(),s2=arg2.toString();
	try{m+=s1.split(".")[1].length}catch(e){}
	try{m+=s2.split(".")[1].length}catch(e){}
	return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
}
//新增供应商
function addSupplier(obj){
	var form = layui.form;
	var skuOid = $(obj).parents("tr").find("td:eq(0)").find("input[name='skuOid']").val();
	//供应商显示与其他不同项
	var result = getNotInSuppStr($(obj).parents("tr").find("td:eq(4)").find("table[name='tableBig'] tr:eq(1)"),skuOid);
	var parentTr = $(obj).parents("tr").find("td:eq(4)").find("table[name='tableBig'] tr[name='tableBigTitle']");
	var objN = $(parentTr).nextAll();
	var addSize = $(obj).parents("tr").find("td:eq(3)").find("input[name='addSize']").val();
	var NumErro = "";
	var supplierId = "";
	var predictArredNumber = (new Date()).valueOf();
	$.each(objN,function(){
		var trName = $(this).attr("name");
		if(trName.lastIndexOf("supplierTr_")==0){
			var optionSize = $(this).find("td:eq(0)").find("select[name='supplierName'] option").size();
			if(addSize >= optionSize){
				NumErro = "供应商不能超过"+optionSize+"个！";
				return false;
			}
		}
		if(trName.lastIndexOf("tableBigTitle")>0){
			return false;
		}
		var tbl=$("#table_"+skuOid+" tr:eq(-1)");
		var supplierObj;
	  	if(trName.lastIndexOf("supplierTr_")==0){
			var addTr=document.createElement('tr');
			addTr.innerHTML = $(this).html();
			tbl.after(addTr);
			//设置供应商默认显示值
			$(addTr).find("td:eq(0)").find("select option[value='"+result+"']").attr("selected",true);
			supplierId = $(addTr).find("td:eq(0)").find("select option:selected").val().split(",")[0];
			$(addTr).attr("name","supplierTr_"+skuOid+"_"+supplierId);
			$(addTr).find("td:eq(4)").find("input[name='predictArred']").attr("id","predictArred"+predictArredNumber);
			$(addTr).find("td:eq(4)").find("input[name='predictArred']").attr("lay-key","");
			loadPredictArredDate("predictArred"+predictArredNumber);
			//采购数量默认为0
			$(addTr).find("td[name='purchaseNumTd']").find("input[name='purchaseNum']").attr("value","0");
			//设置trname和隐藏input值
			getProductPrice($("#table_"+skuOid+" tr[name^='supplierTr_"+skuOid+"_"+supplierId+"']").find("td:eq(0)").find("select"));
	  	}
	  	//添加的数量+1
	  	$(obj).parents("tr").find("td:eq(3)").find("input[name='addSize']").val(Number(addSize)+1)
	  	form.render("select");
	  	var obji = $(this).next();
	  	if($(obji).attr("name").lastIndexOf("supplierTr_")==0){
	  		return false;
	  	}
	});
	if(NumErro != ''){
		layer.msg(NumErro,{icon:2});
		return;
	}
}
//取不在字符中的供应商
function getNotInSuppStr(obj,skuOid){
	var all = ".NTNT.";
	var result = "";
	//所有供应商
	$(obj).find("td:eq(0)").find("select option").each(function() {
	    all += $(this).attr("value")+".NTNT.";
	});
	//已添加的供应商supplierTr_16655527
	$("#beatchCreateOrderTbody table[name='tableBig'] tr[name^='supplierTr_"+skuOid+"']").each(function(){
		var beSupplierId = $(this).find("td:eq(0)").find("select").val();
		all = all.replace(".NTNT."+beSupplierId, "");
	});
	var strArry = all.split(".NTNT.");
	result = strArry[1];
	return result;
}
</script>
<div id="orderList" style="width: 100%; overflow: auto;">
	<!--采购列表部分-->
	<form class="layui-form" style="overflow: auto;">
		<input type="hidden" id="buyOrderId" value="${buyOrderId}">
		<table class="planList">
              <thead>
              <tr>
                <td style="width:10%">商品</td>
                <td style="width:7%">单位</td>
                <td style="width:5%">数量</td>
                <td style="width:3%">添加供应商</td>
                <td style="width:75%">供应商</td>
              </tr>
              </thead>
              <tbody id="beatchCreateOrderTbody">
              	<c:forEach var="apItem" items="${productList}" varStatus="status">
              		<tr name="product_${apItem.skuBarcode}">
              			<td style="vertical-align: middle;">
              				<b name="${apItem.skuBarcode}"></b>
              				${apItem.productCode}<br>${apItem.productName}<br>${apItem.skuCode}<br>${apItem.skuName}<br>${apItem.skuBarcode}
              				<input type="hidden" name="skuOid" value="${apItem.skuBarcode}"/>
              			</td>
              			<td style="vertical-align: middle;" id="unit_${apItem.skuBarcode}">
              				<div id="unitDiv_${status.count}"></div>
              			</td>
              			<td style="vertical-align: middle;">
              				${apItem.cpNum * apItem.composeNum}
              			</td>
              			<td style="vertical-align: middle;">
              				<!-- <a onclick="addSupplier(this);">新增</a> -->
              				<a href="javascript:void();"><img src="${basePath}statics/platform/images/addIcon.png" onclick="addSupplier(this);"></a>
              				<input type="hidden" name="addSize" value="1"/>
              			</td>
              			<td colspan="7" style="padding: 0;">
              				<table name="tableBig" id="table_${apItem.skuBarcode}" class="layui-table" lay-size="sm" 
              					style="margin: 0;width: 100%;border-collapse: collapse;text-align: center;font-size: 12px;height: 100%">
				            	<tr name="tableBigTitle" class="trTitle">
					                <td style="width:280px">名称</td>
					                <td style="width:150px">采购单价</td>
					                <td style="width:150px">采购数量</td>
					                <td style="width:150px">仓库</td>
					                <td style="width:150px">要求到货日期</td>
					                <td style="width:230px">备注</td>
					                <td style="width:110px">删除</td>
				            	</tr>
	              				<tr name="supplierTr_${apItem.skuBarcode}_">
	              					<td name="supplierTd">
	              						<select name="supplierName" onchange="getProductPrice(this);" lay-search="" lay-filter="pageFilter">
		              						<c:forEach var="supplier" items="${apItem.supplierLinksList}">
	              								<option value="${supplier.supplierId},${supplier.person},${supplier.phone}" >${supplier.supplierName}</option>
		              						</c:forEach>
	              						</select>
		              					<input type="hidden" name="supplierId" value=""/>
		              					<input type="hidden" name="person" value=""/>
		              					<input type="hidden" name="phone" value=""/>
		              					<input type="hidden" name="supplierName" value=""/>
		              					<input type="hidden" name="productCode" value="${apItem.productCode}">
										<input type="hidden" name="productName" value="${apItem.productName}">
										<input type="hidden" name="skuCode" value="${apItem.skuCode}">
										<input type="hidden" name="skuName" value="${apItem.skuName}">
										<input type="hidden" name="barcode" value="${apItem.skuBarcode}">
		              				</td>
					                <td name="priceTd" id="priceTd">
										<input type="number" name="price" value="" min="0">
									</td>
					                <td name="purchaseNumTd">
					                	<input type="number" name="purchaseNum" value="${apItem.cpNum * apItem.composeNum}" min="0">
					                </td>
					                <td name="warehouseSelectTd"></td><!-- 仓库下拉 -->
					                <td name="predictArredTd">
					                	<input type="text" name="predictArred" lay-verify="date" placeholder="年/月/日" class="layui-input" style="width: 130px" >
					                </td>
					                <td name='remarksTd'>
					                  <textarea name="remarks" placeholder="备注内容"></textarea>
					                </td>
					                <td>
					                	<a href="javascript:void(0)" onclick="delSupplierRow(this,${apItem.skuBarcode})" class="layui-btn layui-btn-primary layui-btn-mini" ><i class="layui-icon">&#xe640;</i>删除</a></td>
	              				</tr>
              				</table>
              			</td>
              		</tr>
              		<script>
						//初始化
						//单位下拉						
						var unitObj={};
						unitObj.divId="unitDiv_${status.count}";
						unitObj.selectValue="${apItem.unitId}";
						unitSelect(unitObj);
					</script>
              	</c:forEach>
              </tbody>
            </table>
	</form>
	<div class="text-right">
		<a onclick="submit();"><button class="next_step">提交</button></a>
	</div>
</div>
<!-- 仓库下拉 -->
<div id="changeWarehouseSelect" hidden="true"></div>
<!--商品价格变更-->
<div class="plain_frame price_change" id="customizePricesDiv" style="display: none;">
   <div class="account_group">
     <span>定制方式:</span>
     <select name="priceStatus" id="priceStatus">
     	<option value="0">加价</option>
     	<option value="1">提成</option>
     </select>
   </div>
   <div class="account_group">
     <span>金额/百分比:</span>
     <input type="text" name="priceLatitude" id="priceLatitude">
   </div>
</div>