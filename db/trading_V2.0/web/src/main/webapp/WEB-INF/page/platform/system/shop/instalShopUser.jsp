<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
$(function(){
	//加载左侧店铺信息
	getLeftNotRole();
	//加载右侧店铺信息
	getRightRole();
	$("input:checkbox[name='checkAll']").click(function(){
		var flag = $(this).is(":checked");
		$(this).parents("table").find("tbody input:checkbox").prop("checked",flag);
	});
});
function getRightRole(){
	var userId = $("input[name='instalShopUserId']").val();
	var shopName = $("input[name='shopNameRight']").val();
	$("div.rightDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/sysshop/getRightShop",
		data:{
			"userId":userId,
			"shopName":shopName
			},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='rightCheckOne' value='"+data.shopCode+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='shopIdRight' value='"+data.id+"'>";
				trObj += data.shopCode;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.shopName;
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.rightDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取店铺数据失败，请稍后重试！",{icon:2});
		}
	});
}
function getLeftNotRole(){
	var userId = $("input[name='instalShopUserId']").val();
	var shopName = $("input[name='shopNameLeft']").val();
	$("div.leftDiv table tbody").empty();
	$.ajax({
		url:basePath+"platform/sysshop/getLeftNotShop",
		data:{
			"userId":userId,
			"shopName":shopName
			},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			var trObj = "";
			$.each(resultObj,function(i,data){
				trObj += "<tr>";
				trObj += "<td>";
				trObj += "<input type='checkbox' name='leftCheckOne' value='"+data.shopCode+"'>";
				trObj += "</td>";
				trObj += "<td>";
				trObj += "<input type='hidden' name='shopIdLeft' value='"+data.id+"'>";
				trObj += data.shopCode;
				trObj += "</td>";
				trObj += "<td>";
				trObj += data.shopName;
				trObj += "</td>";
				trObj += "</tr>";
			});
			$("div.leftDiv table tbody").append(trObj);
		},
		error:function(){
			layer.msg("获取店铺数据失败，请稍后重试！",{icon:2});
		}
	});
}
//添加
function addRole(){
	var userId = $("input[name='instalShopUserId']").val();
	var checked = $("div.leftDiv table tbody input:checkbox[name='leftCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("左侧店铺信息至少选中1条！",{icon:2});
		return;
	}
	var shopCodeStr = "";
	checked.each(function(){
		shopCodeStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/sysshop/addUserShop",
		data:{
			"userId":userId,
			"shopCodeStr":shopCodeStr
		},
		success:function(data){
			//加载左侧店铺信息
			getLeftNotRole(userId);
			//加载右侧店铺信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取店铺数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteRole(){
	var userId = $("input[name='instalShopUserId']").val();
	var checked = $("div.rightDiv table tbody input:checkbox[name='rightCheckOne']:checked");
	if(checked.length == 0){
		layer.msg("右侧店铺信息至少选中1条！",{icon:2});
		return;
	}
	var shopCodeStr = "";
	checked.each(function(){
		shopCodeStr += $(this).val() + ",";
	});
	$.ajax({
		url:basePath+"platform/sysshop/deleteUserShop",
		data:{
			"userId":userId,
			"shopCodeStr":shopCodeStr
		},
		success:function(data){
			//加载左侧店铺信息
			getLeftNotRole(userId);
			//加载右侧店铺信息
			getRightRole(userId);
		},
		error:function(){
			layer.msg("获取店铺数据失败，请稍后重试！",{icon:2});
		}
	});
}
function changePopHeight(){
	  var h = $(window).height();
	  $('.leftDiv').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
	  $('.rightDiv').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
	}
	changePopHeight();
	window.onresize = function(){changePopHeight();}
//搜索左侧数据
function loadleftDivData(){
	//加载左侧店铺信息
	getLeftNotRole();
}
//搜索右侧数据
function loadRightDivData(){
	//加载右侧店铺信息
	getRightRole();
}
</script>
<div class="roleUser">
	<div class="leftDiv">
		<ul>
			<li>
				<input type="text" style="width:140px;height: 25px;" placeholder="输入店铺名称" name="shopNameLeft" value="">   
           		<button class="search rt" style="width:100px;height: 25px;"  onclick="loadleftDivData();">搜索</button>
         	</li>
		</ul>
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
					<td>店铺代码</td>
	                <td>店铺名称</td>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div class="modelDiv">
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-danger layui-btn-small rt"
			onclick="addRole();"><i class="layui-icon">&#xebaa;</i> 添加</a>
		<a href="javascript:void(0)"
			class="layui-btn layui-btn-primary layui-btn-small rt"
			onclick="deleteRole();"><i class="layui-icon">&#xe93e;</i> 删除</a>
	</div>
	<div class="rightDiv">
		<ul>
			<li>
				<input type="text" style="width:140px;height: 25px;" placeholder="输入店铺名称" name="shopNameRight" value="">   
           		<button class="search rt" style="width:100px;height: 25px;"  onclick="loadRightDivData();">搜索</button>
         	</li>
		</ul>
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td><input type="checkbox" name="checkAll"></td>
					<td>店铺代码</td>
	                <td>店铺名称</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<input type="hidden" value="${instalShopUserId}" name="instalShopUserId">