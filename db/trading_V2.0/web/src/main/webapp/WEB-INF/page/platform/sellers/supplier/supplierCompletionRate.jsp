<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script> 
<script src="<%=basePath%>statics/platform/js/echarts.min.js"></script>
<script type="text/javascript" src="<%=basePath%>statics/platform/js/setPrice.js"></script>
<script type="text/javascript">
var supplierList = <%=request.getAttribute("complitionList")%>;
var xArr = [];
var succArr=[];
var totalArr=[];
$.each(supplierList,function(i,data){
	xArr.push(data.company_name);
	succArr.push(data.arrival_num);
	totalArr.push(data.goods_number);
});

 // 基于准备好的dom，初始化echarts实例
 var columnEcharts = echarts.init(document.getElementById('echartsColumn'));

 // 指定图表的配置项和数据
 var columnOption = {
 	    title : {
 	        text: '完成到货数量和接单总数量',
 	        subtext: '买卖系统'
 	    },
 	    tooltip : {
 	        trigger: 'axis'
 	    },
 	    legend: {
 	        data:['到货总数','接单总数']
 	    },
 	    toolbox: {
 	        show : true,
 	        feature : {
 	            //dataView : {show: true, readOnly: false},		//数据视图
 	            magicType : {show: true, type: ['line', 'bar','stack','tiled']},
 	            restore : {show: true},
 	            saveAsImage : {show: true}
 	        }
 	    },
 	   	grid: {  
	  		bottom:'70'
	  	}, 
 	    calculable : true,
 	    xAxis : [
 	        {
 	            type : 'category',
 	            axisLabel :{   // 解决X轴显示不全
 	              interval:'auto',  //0强制显示所有标签，如果设置为1，表示隔一个标签显示一个标签，如果为3，表示隔3个标签显示一个标签
 	              //rotate:40, //旋转角度  
 	              formatter : function(value) {
						var ret = "";//拼接加\n返回的类目项  
						var maxLength = 6;//每项显示文字个数  
						var valLength = value.length;//X轴类目项的文字个数  
						var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
						if (rowN > 1)//如果类目项的文字大于6,  
						{
							for ( var i = 0; i < rowN; i++) {
								var temp = "";//每次截取的字符串  
								var start = i * maxLength;//开始截取的位置  
								var end = start + maxLength;//结束截取的位置  
								//这里也可以加一个是否是最后一行的判断，但是不加也没有影响，那就不加吧  
								temp = value.substring(start, end) + "\n";
								ret += temp; //凭借最终的字符串  
							}
							return ret;
						} else {
							return value;
						}
					}
 	            },
 	            data : []
 	        }
 	    ],
 	    yAxis : [
 	        {
 	            type : 'value',
 	            axisLabel: {
 	            formatter: "{value} 数"
 	            }
 	        }
 	    ],
 	    series : [
 	        {
 	            name:'完成到货数量',
 	            type:'bar',
 	            data:[],
 	            markPoint : {
 	                data : [
 	                    {type : 'max', name: '最大值'},
 	                    {type : 'min', name: '最小值'}
 	                ]
 	            },
 	            markLine : {
 	                data : [
 	                    {type : 'average', name: '平均值'}
 	                ]
 	            }
 	        },
 	        {
 	            name:'接单总数量',
 	            type:'bar',
 	            data:[],
 	            markPoint : {
					data : [
	                    {type : 'max', name: '最大值'},
	                    {type : 'min', name: '最小值'}
					]
 	            },
 	            markLine : {
 	                data : [
 	                    {type : 'average', name : '平均值'}
 	                ]
 	            }
 	        }
 	    ]
 	};

 columnOption.xAxis[0].data = xArr;
 columnOption.series[0].data = succArr;
 columnOption.series[1].data = totalArr;
 columnEcharts.setOption(columnOption);
 // 使用刚指定的配置项和数据显示图表。
 columnEcharts.setOption(columnOption);
</script>
<div>
	<form class="layui-form" action="platform/seller/supplier/getComplitionRateHtml">
		<ul class="order_search">
			<input type="hidden" name="byDate" value="${map.byDate }">
			<li class="state"><label>起始时间：</label>
				<div class="layui-input-inline">
					<input type="text" name="startDate" value="${map.startDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
					-
				<div class="layui-input-inline">
					<input type="text" name="endDate" value="${map.endDate}" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input">
				</div>
			</li>
			<li class="range"><label>采购商名称：</label> 
				<input type="text" placeholder="输入采购商名称" name="companyame" value="${map.companyame}">
			</li>
			<li class="range"><label>商品名称：</label> 
				<input type="text" placeholder="输入商品名称" name="productName" value="${map.productName}">
			</li>
			<li class="range"><label>货号：</label> 
				<input type="text" placeholder="输入货号" name="productCode" value="${map.productCode}">
			</li>
			<li class="range"><label>条形码：</label> 
				<input type="text" placeholder="输入条形码" name="barcode" value="${map.barcode}">
			</li>
			<li class="range"><label>规格名称：</label> 
				<input type="text" placeholder="输入规格名称" name="skuName" value="${map.skuName}">
			</li>
			<li class="range"><label>规格代码：</label> 
				<input type="text" placeholder="输入规格代码" name="skuCode" value="${map.skuCode}">
			</li>
			<li class="range"><label>创建人：</label> 
				<input type="text" placeholder="输入创建人" name="createName" value="${map.createName}">
			</li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="exportData('supplierCompletionTable','供应商到货及时率');">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
		</ul>
	</form>
	<table class="table_pure supplierList" id="supplierCompletionTable">
		<thead>
			<tr>
				<td>采购商名称</td>
				<td>接单总数量</td>
				<td>完成到货数量</td>
				<td>未完成到货数量</td>
				<td>计划完成率</td>
			</tr>
		</thead>
		<tbody>
		    <c:set var="total" value="0" scope="page"></c:set>
			<c:set var="successTotal" value="0" scope="page"></c:set>
			<c:set var="failTotal" value="0" scope="page"></c:set>
			<c:forEach var="item" items="${complitionList}" varStatus="status">
				<tr>
					<c:set var="total" value="${item.goods_number + total }"></c:set>
				    <c:set var="successTotal" value="${item.arrival_num + successTotal }"  scope="page"></c:set>
				    <c:set var="failTotal" value="${item.order_num - item.arrival_num + failTotal }" scope="page"></c:set>
					<td>${item.company_name}</td>
					<td>${item.order_num}</td>
					<td>${item.arrival_num}</td>
					<td>${item.order_num - item.arrival_num}</td>
					<td><fmt:formatNumber value="${item.rate}" pattern="#.##" type="number"/>%</td>
				</tr>
			</c:forEach>
			<tr style="color: blue">
					<td>合计：</td>
					<td>${total}</td>
					<td>${successTotal}</td>
					<td>${failTotal}</td>
					<td>
						<c:choose>
							<c:when test="${total==0 }">
								0%
							</c:when>
							<c:otherwise>
								<script>
									var successTotal = parseFloat("${successTotal}");
									var total = parseFloat("${total}");
									$("#rateTotal").html(Math.round((successTotal*100/total)*100)/100);
								</script>
								<span id="rateTotal"></span>%
							</c:otherwise>
						</c:choose>
					</td>
			</tr>
		</tbody>
	</table>
	 <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	<div id="echartsColumn" style="width: 100%;height:500px; margin-top: 50px;"></div>
</div>