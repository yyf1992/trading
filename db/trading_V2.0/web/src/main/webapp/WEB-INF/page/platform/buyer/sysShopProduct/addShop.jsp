<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="basePath" value="<%=basePath%>" />
<c:set var="staticsPath" value="${basePath}/statics" />
<link rel="stylesheet" href="${staticsPath}/platform/css/setPrice.css">
<script src="${staticsPath}/platform/js/setPrice.js"></script>

<script type="text/javascript">
//  增加一行
$('.rowNew').click(function(){
  var id= (new Date()).valueOf();
  var tr='<tr id="firstTr">'
		+'<td>'
		+'<select id="shop" name="shopId" onchange="removeError(this);">'
		+'<c:forEach var="SysShop" items="${sysShopList}">'
		+'<option value="${SysShop.shopCode}" id="${SysShop.id}">${SysShop.shopName}</option>'
		+'</c:forEach>'
		+'</select>'
		+'</td>'
		+'<td><input class="goodsIpt" type="text" placeholder="商品名称" name="productName" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="货号" name="productCode" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="条形码" name="barcode" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="规格名称" name="skuName" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="规格代码" name="skuCode" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="成本价" name="costPrice" onclick="selectGood(this);"></td>'
		+'<td><input class="goodsIpt" type="text" placeholder="出货价" name="price" onkeyup="applyPurchaseKeyUp(this)"></td>'
		+'<td><button class="commodityDel">删除商品</button></td>'
		+'<input type="hidden" name="skuId">'
		+'</tr>';
  var trObj=$(tr).attr("id",id);
  var tby=$('.commodityNew tbody');
  tby.append(trObj);
});

//  删除一行
$('.commodityNew').on('click','.commodityDel',function(){
  $(this).parent().parent().remove();
});

function removeError(obj){
	var trObj = $(obj).parent().parent("tr");
	if($(trObj).hasClass("error")){
		$(trObj).removeClass("error");
	}
}
</script>
<body>
	<div id="tableForm">
		<table class="commodityNew">
			<thead>
				<tr>
					<td style="width:20%">店铺</td>
					<td style="width:10%">商品名称</td>
					<td style="width:10%">货号</td>
					<td style="width:10%">条形码</td>
					<td style="width:10%">规格名称</td>
					<td style="width:10%">规格代码</td>
					<td style="width:10%">成本价</td>
					<td style="width:10%">品牌中心出货价</td>
					<td style="width:10%"><button class="rowNew">新增一行</button></td>
				</tr>
			</thead>
			<tbody>
				<tr id="firstTr">
					<td>
						<select id="shop" name="shopId" onchange="removeError(this);">
								<c:forEach var="SysShop" items="${sysShopList}">
									<option value="${SysShop.shopCode}" id="${SysShop.id}">${SysShop.shopName}</option>
								</c:forEach>
						</select>
					</td>
					<td><input class="goodsIpt" type="text" placeholder="商品名称" name="productName" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="货号" name="productCode" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="条形码" name="barcode" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="规格名称" name="skuName" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="规格代码" name="skuCode" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="成本价" name="costPrice" onclick="selectGood(this);"></td>
					<td><input class="goodsIpt" type="text" placeholder="出货价" name="price" onkeyup="applyPurchaseKeyUp(this)"></td>
					<td><button disabled="disabled" class="commodityDel">删除商品</button></td>
					<input type="hidden" name="skuId">
				</tr>
			</tbody>
		</table>
		<div class="commoditySubmit">
			<button class="commodityCancel"onclick="leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385')">取消</button>
			<button class="commoditySave" onclick="saveSetPrice(this);">保存</button>
		</div>
	</div>
</body>