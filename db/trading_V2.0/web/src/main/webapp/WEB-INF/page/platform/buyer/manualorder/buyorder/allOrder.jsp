<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop dealTop">
  <tr>
    <td>
      <ul>
        <li>商品</li>
        <li>条形码</li>
        <li>单位</li>
        <li>数量</li>
        <li>备注</li>
      </ul>
    </td>
    <td>交易状态</td>
    <td>操作</td>
  </tr>
</table>
<!-- <div class="batch_handle mt">
	<div>
		<span class="chk_click"><input type="checkbox"> </span>
	</div>
	<span id="batch_c">批量取消订单</span>
</div> -->
<div class="orderList orderDeal">
	<c:forEach var="order" items="${searchPageUtil.page.list }">
		<div >
			<p>
				<span class="chk_click"><input type="checkbox" name="checkone"> </span> <span
					class="apply_time"><fmt:formatDate
						value="${order.createDate }" type="both" />
				</span> <span class="order_num">订单号: <b>${order.orderCode}</b> </span> <span>${order.suppName}</span>
			</p>
			<table>
				<c:if test="${order.orderProductList != null}">
					<tr>
						<td style="width:76%">
							<c:forEach var="orderProduct" items="${order.orderProductList}">
								<ul class="clear">
									<li style="width:50%"><img
										src="${basePath}statics/platform/images/01.jpg">
										<div>
											${orderProduct.proCode} ${orderProduct.proName} <br> 
											<span>规格代码: ${orderProduct.skuCode}</span>
											<span>规格名称: ${orderProduct.skuName}</span>
										</div></li>
									<li style="width:15%">${orderProduct.skuOid}</li>
									<li style="width:10%">${el:getUnitById(orderProduct.unitId).unitName}</li>
									<li style="width:10%">${orderProduct.goodsNumber}</li>
									<li style="width:15%">${orderProduct.remark}</li>
								</ul>
							</c:forEach>
						</td>
						<td style="width:12%">
							<div>
								<c:choose>
									<c:when test="${order.isCheck==0}">待内部审批</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${order.status==0}">待接单</c:when>
											<c:when test="${order.status==2}">待对方发货</c:when>
											<c:when test="${order.status==3}">待收货</c:when>
											<c:when test="${order.status==4}">已收货</c:when>
											<c:when test="${order.status==6}">已取消</c:when>
											<c:when test="${order.status==7}">已驳回</c:when>
											<c:when test="${order.status==8}">已完成</c:when>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
							<a href="javascript:void(0)" onclick="leftMenuClick(this,'buyer/manualOrder/manualOrderDetails?id=${order.id}','buyer')" class="approval">订单详情</a>
							<div class='opinion_view'>
			                    <span class='orange'>查看审批流程</span>
			                    <!--流程弹出框-->
			                    <div class='opinion'>
			                      <b></b>
			                      <h3>审批流程</h3>
			                      <div>
			                        <p>发起申请<span></span></p>
			                        <div class='clear'>
			                          <h4>${order.tradeHeader.startName}</h4>
			                          <span><fmt:formatDate value="${order.tradeHeader.createDate}" type="both" /></span>
			                        </div>
			                      </div>
			                      <c:forEach items="${order.pocessList}" var="pocess">
									<div>
										<p>
											<c:choose>
												<c:when test="${pocess.startIndext && pocess.status=='0'}">审批中</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='0'}">等待审批</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='1'}">已通过</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='2'}">已驳回</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='3'}">已转交</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='4'}">已撤销</c:when>
											</c:choose>
										<span></span></p>
										<div class='clear'>
											<h4>${pocess.userName}</h4>
											<c:if test="${pocess.remark != null}">
												<p>(${pocess.remark})</p>
											</c:if>
											<span><fmt:formatDate value="${pocess.endDate}" type="both" /></span>
										</div>
									</div>
			                      </c:forEach>
			                    </div>
			                  </div>
							</td>
							<td style="width:12%">
							<c:if test="${order.isCheck == 0 && order.status == 5}">
								<div class="approvalOrder green_btn" onclick="verify('${order.id}');">审批</div>
								<div class="cancelOrder" onclick="cancelOrder('${order.id}');">取消订单</div>
							</c:if>
						</td>
					</tr>
				</c:if>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
