<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<title>商品出入明细</title>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<script type="text/javascript">
//重置
function recovery(){
	leftMenuClick(this,'platform/baseInfo/inventorymanage/prodcutsRecording',"baseInfo");
}
$(function(){
	$("#shopId option[value='"+"${searchPageUtil.object.shopId}"+"']").attr("selected","selected");
	var supplierId = '${searchPageUtil.object.supplierId}';
	$("#supplierId").val(supplierId);
	$("#createName option[value='"+"${searchPageUtil.object.createName}"+"']").attr("selected","selected");
	$("#warehouseId option[value='"+"${searchPageUtil.object.warehouseId}"+"']").attr("selected","selected");
	$("#dropshipType option[value='"+"${searchPageUtil.object.dropshipType}"+"']").attr("selected","selected");
});
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(prodcutsRecordingTableFilter)', function(obj){
	    var data = obj.data;
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('prodcutsRecordingTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	productCode: $("#productCode").val(),
	        	barcode: $("#barcode").val(),
	        	skuCode: $("#skuCode").val(),
	        	batchNo: $("#batchNo").val(),
	        	orderCode: $("#orderCode").val(),
	        	applyCode: $("#applyCode").val(),
	        	storageNo: $("#storageNo").val(),
	        	dropshipType: $("#dropshipType").val(),
	        	minDate: $("#minDate").val(),
	        	maxDate: $("#maxDate").val(),
	        	createName: $("#createName").val(),
	        	suppName: $("#suppName").val()
	        }
	      });
	    },
	    //导出
	    exportExcel: function(){
			var formData = [
				"batchNo="+$("#batchNo").val(),
				"&barcode="+$("#barcode").val(),
				"&productCode="+$("#productCode").val(),
				"&skuCode="+$("#skuCode").val(),
				"&orderCode="+$("#orderCode").val(),
				"&applyCode="+$("#applyCode").val(),
				"&storageNo="+$("#storageNo").val(),
				"&dropshipType="+$("#dropshipType").val(),
				"&minDate="+$("#minDate").val(),
				"&maxDate="+$("#maxDate").val(),
				"&createName="+$("#createName").val(),
				"&suppName="+$("#suppName").val()
			];
			var url = basePath + "recordingDownload/recordingData?" + formData.join("");
			window.open(url);
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="skuCode" id="skuCode" autocomplete="off" placeholder='型号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="batchNo" id="batchNo" autocomplete="off" placeholder='发货单号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="orderCode" id="orderCode" autocomplete="off" placeholder='采购单号'>
  </div>
    <div class="layui-inline">
    <input class="layui-input" name="applyCode" id="applyCode" autocomplete="off" placeholder='采购计划单号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="storageNo" id="storageNo" autocomplete="off" placeholder='入库单号'>
  </div>
  <div class="layui-inline">
    <select id="dropshipType" name="dropshipType" lay-verify="required" lay-search="">
       	<option value="">所有发货类型</option>
		<option value="0">正常发货</option>
		<option value="1">代发客户</option>
		<option value="2">代发菜鸟</option>
		<option value="3">代发京东</option>
    </select>
  </div>
  <div class="layui-inline">
  	<div class="layui-input-inline">
      <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
    </div>
    <div class="layui-input-inline">
      <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
    </div>
  </div>
  <div class="layui-inline">
	<input class="layui-input" name="suppName" id="suppName" autocomplete="off" placeholder='供应商'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="createName" id="createName" autocomplete="off" placeholder='下单人'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="exportExcel">导出</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:20,
		id:'prodcutsRecordingTable',
		height:'full-90',
		url:'<%=basePath %>platform/baseInfo/inventorymanage/loadProdcutsRecordingDataJson'
	}" lay-filter="prodcutsRecordingTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'barcode',width:120, sort: true,show:true,showToolbar:true}">条形码</th>
            <th lay-data="{field:'productCode',width:120, sort: true,show:true}">货号</th>
            <th lay-data="{field:'skuName',width:120, sort: true,show:true}">规格名称</th>
            <th lay-data="{field:'batchNo', sort: true,show:true}">发货单号</th>
            <th lay-data="{field:'orderCode', sort: true,show:true}">采购单号</th>
            <th lay-data="{field:'applyCode', sort: true,show:true}">采购计划单号</th>
            <th lay-data="{field:'suppName', sort: true,show:true}">供应商</th>
            <th lay-data="{field:'number', sort: true,show:true}">入库数量</th>
            <th lay-data="{field:'price', sort: true,show:true}">采购金额</th>
            <th lay-data="{field:'whareaName', sort: true,show:true}">入库名称</th>
            <th lay-data="{field:'dropshipType', sort: true,show:true,templet: '#dropshipTypeTpl', align: 'center'}">发货类型</th>
            <th lay-data="{field:'createName', sort: true,show:true}">下单人</th>
            <th lay-data="{field:'storageDate', sort: true,show:true}">入库时间</th>
            <th lay-data="{field:'storageNo', sort: true,show:true}">入库单号</th>
        </tr>
    </thead>
</table>
<!-- 确认状态 -->
<script type="text/html" id="dropshipTypeTpl">
	{{#  if(d.dropshipType == 0){ }}
		<span>正常发货</span>
	{{#  } else if(d.dropshipType == 1){ }}
		<span>代发客户</span>
	{{#  } else if(d.dropshipType == 2){ }}
		<span>代发菜鸟</span>
	{{#  } else if(d.dropshipType == 3){ }}
		<span>代发京东</span>
	{{#  } }}
</script>