<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
$(function(){
	//切换右侧好友窗口
	$('.switch>span').click(function(){
	  $('.switch>span.open').toggle();
	  $('.switch>span.close').toggle();
	  $('#side_pro').toggleClass('show');
	});
});
</script>
<!--侧边栏-->
<div class="aside">
	<div class="switch">
		<span class="open"> 
			<img src="<%=basePath%>statics/platform/images/friend_list.jpg"> 打开我的好友 <i></i>
		</span> 
		<span class="close"> 
			<img src="<%=basePath%>statics/platform/images/friend_list.jpg"> 关闭我的好友 <i></i>
		</span>
		<div id="side_pro">
			<div class="Pro_main">
				<div class="Pro_mT clear">
					<input type="text" name="" value="" placeholder="输入供应商名称">
					<button></button>
				</div>
				<div class="Pro_list">
					<div title="杭州丽人女装股份有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>杭州丽人女装股份有限公司</h4>
							<p>大连办事1处-张先生 座机：0511-88773216 手机： 15305312019/13466893214
								每周二进行审核</p>
						</div>
					</div>
					<div title="广州南凯投资移民咨询服务有限公司青岛分公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>广州南凯投资移民咨询服务有限公司青岛分公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="中科盛创(青岛)电气股份有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>中科盛创(青岛)电气股份有限公司</h4>
							<p>
								<span class="fa fa-pencil-square-o"></span> 张先生 1333333333
							</p>
						</div>
					</div>
					<div title="联程旅游发展(大连)有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>联程旅游发展(大连)有限公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="中科盛创(青岛)电气股份有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>中科盛创(青岛)电气股份有限公司</h4>
							<p>每周五进行电话会议</p>
						</div>
					</div>
					<div title="山东海易筹商务信息咨询有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>山东海易筹商务信息咨询有限公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="金斯达投资管理咨询有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>金斯达投资管理咨询有限公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="中企动力科技股份有限公司山东济南分公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>中企动力科技股份有限公司山东济南分公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="联程旅游发展(大连)有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>联程旅游发展(大连)有限公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
					<div title="中科盛创(青岛)电气股份有限公司">
						<img src="<%=basePath%>statics/platform/images/2-2-01.png">
						<div>
							<h4>中科盛创(青岛)电气股份有限公司</h4>
							<p>张先生 1333333333</p>
						</div>
					</div>
				</div>
				<div class="last">
					<b></b>
				</div>
			</div>
		</div>
	</div>
</div>
