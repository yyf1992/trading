<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="<%=basePath%>statics/dingding/css/app.css" />
<script>
function chooseUser(obj){
	var verifyId = $(obj).parents(".picker-box").find("input[name='verifyId']").val();
	var userId = $(obj).find("input[name='userId']").val();
	var userName = $(obj).find("input[name='userName']").val();
	var htmlStr = getHtmlStr();
	layer.open({
		type:1,
		title:"转交意见",
		skin: 'layui-layer-rim',
    	area: ['300px', 'auto'],
    	content:htmlStr,
    	btn:['确定','取消'],
		yes:function(index, layero){
			if($("#verifyOpinion").val() == ""){
				return false;
			}
			var verifyOpinion = $("#verifyOpinion").val();
			var remark = userId+","+userName+","+verifyOpinion;
			var verifyFileHidden = $("#verifyFileHidden").val();
			verifyTrade(3,verifyId,remark,verifyFileHidden);
		}
	});
}
</script>
<div class="picker-box">
	<section class="pro-picker">
	<c:forEach items="${userList}" var="userDL">
		<dl>
			<dt id="${userDL.key}">${userDL.key}</dt>
			<c:forEach items="${userDL.value}" var="userDD">
				<dd data-letter="${userDL.key}" onclick="chooseUser(this);">
					${userDD.userName}
					<input type="hidden" name="userId" value="${userDD.id}">
					<input type="hidden" name="userName" value="${userDD.userName}">
				</dd>
			</c:forEach>
		</dl>
	</c:forEach>
	</section>
	<div class="navbar">
		<c:forEach items="${userList}" var="userDL">
			<a href="#${userDL.key}">${userDL.key}</a>
		</c:forEach>
	</div>
	<input type="hidden" name="verifyId" value="${verifyId}">
</div>