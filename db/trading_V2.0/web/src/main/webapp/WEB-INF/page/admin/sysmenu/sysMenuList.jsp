<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>菜单管理</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ include file="../../platform/common/path.jsp"%>
<%@ include file="../resources.jsp"%>
<script type="text/javascript">
$(function(){
	//全选
	$("#checkAll").change(function(){
		var status = $(this).is(":checked");
		if(status){
			$("input:checkbox[name='checkone']").prop("checked",status);
		}else{
			$("input:checkbox[name='checkone']").prop("checked",status);
		}
	});
});
//创建
function addSysMenu(obj){
	$.ajax({
		url : basePath+"admin/sysMenu/loadAddMenu.html",
		success:function(data){
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//修改
function updateSysMenu(obj){
	var checked = $("input:checkbox[name='checkone']:checked");
	if(checked.length != 1){
		layer.msg("请选择一条数据！",{icon:7});
		return;
	}
	$.ajax({
		url : basePath+"admin/sysMenu/loadUpdateMenu.html",
		data:{"id":checked.val()},
		success:function(data){
			var str = data.toString();
			$("#modalDiv .modal-dialog").html(str);
			$("#modalDiv").modal({ show: true});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteSysMenu(obj){
	if($("input:checkbox[name='checkone']:checked").length < 1){
		layer.msg("请至少选择一条数据！",{icon:7});
		return;
	}
	layer.confirm('确认删除？删除后关联信息也会删除！', {
	  btn: ['确定','取消'] //按钮
	}, function(){
		var ids = "";
		$("input:checkbox[name='checkone']:checked").each(function(){
			ids += $(this).val() + ",";
		});
		$.ajax({
			url : basePath+"admin/sysMenu/deleteSysMenu.html",
			data:{"ids":ids},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if (resultObj.success) {
					loadAdminData();
				} else {
					alert(resultObj.msg);
				}
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}, function(){
	});
}
</script>
</head>
<body style="background-color: white;">
	<!--内容-->
<div id="content">
	<div class="container-fluid">
		<div class="row-fluid">
			<!--搜索条件 start-->
			<div class="form-serch">
				<div class="widget-box">
					<div class="widget-content" style="padding-bottom:5px">
						<form class="form-inline clearfix" id="selectForm"
							action="<%=basePath %>admin/sysMenu/loadMenuList.html">
							<div class="form-group">
								<label for="exampleInputName2">菜单名称：</label> <input type="text"
									class="form-control" id="name" name="name" placeholder="菜单名称"
									value="${object.name}">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-sm2 btn-success " id="selectButton"
									onclick="loadAdminData();">查询</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--搜索条件 end-->
			<!--tableys start-->
			<div class="widget-box">
				<div class="widget-title title-lg">
					<span class="icon"> <i class="icon-th"></i> </span>
					<h5>对系统菜单进行操作</h5>
					<div class="pull-left" style="margin-top: 8px;">
						<button class="btn btn-success" type="button" data-toggle="modal" onclick="addSysMenu(this)">创建</button>
						<button class="btn btn-info" onclick="updateSysMenu(this)">修改</button>
						<button class="btn btn-warning" onclick="deleteSysMenu(this)">删除</button>
					</div>
				</div>
				<div class="widget-content nopadding" id="dataList">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
						<div style=" width: 100%; overflow: auto;">
						<table class="table table-bordered table-striped with-check">
							<thead>
								<tr>
									<th width="5px;">
										<input type="checkbox" id="checkAll" name="title-table-checkbox" /></th>
									<th width="300px;">菜单名称</th>
									<th width="40px;">菜单ID</th>
									<th width="80px;">菜单位置</th>
									<th width="50px;">类型</th>
									<th width="250px;">地址</th>
									<th width="50px;">状态</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="sysMenu" items="${sysMenuList}" varStatus="status">
									<tr>
										<td>
											<input type="checkbox" name="checkone"value="${sysMenu.id}" />
										</td>
									<c:if test="${sysMenu.position=='top' || sysMenu.position=='system'}">
										<td class="text-left">
									</c:if>
									<c:if test="${sysMenu.position != 'top' && sysMenu.position!='system'}">
										<c:set var="marginLeft" scope="session" value="${sysMenu.level*60}"/>
										<td style="text-align:inherit;padding-left: ${marginLeft}px;">
									</c:if>
											<c:if test="${sysMenu.isMenu==0}">
												<!-- 菜单 -->
												<i class="icon icon-th-large"></i>
											</c:if>
											<c:if test="${sysMenu.isMenu==1}">
												<!-- 按钮 -->
												<i class="icon icon-edit"></i>
											</c:if>
											${sysMenu.name}
										</td>
										<td>${sysMenu.id}</td>
										<td>
											<c:choose>
												<c:when test="${sysMenu.position == 'top'}">上部</c:when>
												<c:when test="${sysMenu.position == 'left'}">左侧</c:when>
												<c:when test="${sysMenu.position == 'system'}">系统设置</c:when>
												<c:otherwise>按钮</c:otherwise>
											</c:choose>
										</td>
										<td>
											<c:choose>
												<c:when test="${sysMenu.isMenu == 0}">菜单</c:when>
												<c:when test="${sysMenu.isMenu == 1}">按钮</c:when>
											</c:choose>
										</td>
										<td>${sysMenu.url}</td>
										<%-- <td>
											<input id="buttonSpan_${status.index}" type="text" style="width: 100%;border: none;background-color: #F9F9F9;">
											<span id="buttonHideSpan_${status.index}" style="display: none;">${sysMenu.button}</span>
											<script type="text/javascript">
												var width = $("#buttonHideSpan_${status.index}").parent().width();
												var length = width/10 + 15;
												var buttonStr = $("#buttonHideSpan_${status.index}").html();
												$("#buttonSpan_${status.index}").val(buttonStr);
											</script>
										</td> --%>
										<td><c:choose>
												<c:when test="${sysMenu.status == 0}">启用</c:when>
												<c:when test="${sysMenu.status == 1}">禁用</c:when>
											</c:choose></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal fade bs-example-modal-lg" id="modalDiv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document" id="modalDialogDiv"></div>
	</div>
</body>
</html>