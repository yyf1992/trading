<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<!--供应商详情-->
<div class="add_supplier">
	<h3 class="page_title mt">供应商详情</h3>
	<div class="form_group">
		<label class="control_label">供 应 商 名 称:</label> ${supplier.suppName }
	</div>
	<div class="form_group">
		<label class="control_label">企 业 所 在 地:</label> ${el:getProvinceById(supplier.province).province} ${el:getCityById(supplier.city).city} ${el:getAreaById(supplier.area).area}
	</div>
	<div class="form_group">
		<label class="control_label">&nbsp;详 细 地 址:</label> ${supplier.address }
	</div>
	<div class="form_group">
		<label class="control_label">&nbsp;银 行 账 号:</label>${supplier.bankNo }
	</div>
	<div class="form_group">
		<label class="control_label">&nbsp;纳税人识别号:</label>${supplier.taxpayerNo }
	</div>
	<div class="form_group">
		<label class="control_label">&nbsp;营业证件扫描件:</label>
		<div class="img_license">
			<c:forEach var="attachment" items="${el:getAttachmentByRelatedId(supplier.id,1)}">
				<span><img src="${attachment.url}"></span>
			</c:forEach>
		</div>
	</div>
	<div class="setting">
		<h4>联系方式:</h4>
		<div class="btn_add mt"></div>
		<table class="table_blue contract_settings">
			<thead>
				<tr>
					<td style="width:10%">联系人</td>
					<td style="width:10%">手机号</td>
					<td style="width:15%">固话</td>
					<td style="width:13%">传真</td>
					<td style="width:10%">QQ</td>
					<td style="width:12%">旺旺</td>
					<td style="width:12%">E-mail</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="linkman" items="${linkmanList}">
					<tr>
					<td>${linkman.person}</td>
					<td>${linkman.phone}</td>
					<td>${linkman.zone}-${linkman.telNo}</td>
					<td>${linkman.fax}</td>
					<td>${linkman.qq}</td>
					<td>${linkman.wangNo}</td>
					<td>${linkman.email}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="text-right"  style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/supplierList?${form}','buyer','17071815040317888127')">
			<span class="contractBuild">返回</span></a>
		<%--<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/updateSupplier?id=${supplier.id }','buyer','17071815040317888127')" ><button class="layui-btn layui-btn-danger layui-btn-small">修改</button>
		</a>--%>
	</div>
</div>
