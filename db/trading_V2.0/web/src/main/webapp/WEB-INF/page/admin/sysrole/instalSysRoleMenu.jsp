<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
var zTreeList = <%=request.getAttribute("zTreeList")%>;
var json = JSON.stringify(zTreeList);
json = isJSONObject(json)?result:eval('(' + json + ')');
var oldMenuIdList = <%=request.getAttribute("oldMenuIdList")%>;

var setting = {
	check: {
		enable: true
	},
	view: {
		showLine: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {  
        onClick: function(treeId, treeNode) {  
            /* var treeObj = $.fn.zTree.getZTreeObj(treeNode);  
            var selectedNode = treeObj.getSelectedNodes()[0];  
            $("#preNodeId").val(selectedNode.id);
            $("#loadDaoXueType").val("1");
            shareBaseInfoSearch_p(curPage); */
        }
    }
};
$.fn.zTree.init($("#treeDemo"), setting, json);
//保存
function saveRoleMenu(){
	var menuIds="";
	var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
    var nodes = treeObj.getCheckedNodes(true);
    $.each(nodes,function(i){
    	menuIds+=nodes[i].id+",";
    });
    $.ajax({
		url : basePath+"admin/adminSysRole/saveRoleMenu",
		type : "post",
		data : {
			"menuIds":menuIds,
			"roleId" :$("#roleId").val()
		},
		async : false,
		success : function(data) {
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if (resultObj.success) {
				loadAdminData();
				//阻止表单提交
				return false;
			} else {
				layer.msg(resultObj.msg,{icon:2});
				//阻止表单提交
				return false;
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
			//阻止表单提交
			return false;
		}
	});
}
//默认选中
setChecked();
function setChecked(){
	var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
	$.each(oldMenuIdList,function(i){
		//根据id获取树的某个节点
		var node = treeObj.getNodeByParam("id", oldMenuIdList[i]);
		//设置node节点选中状态
		if(node != null)node.checked=true;
		//注：设置checked属性之后，一定要更新该节点，否则会出现只有鼠标滑过的时候节点才被选中的情况
		treeObj.updateNode(node);
	});
}
</script>
<form class="form-inline " id="updateSysRoleForm"
	novalidate="novalidate">
	<div class="modal-content" style="width: 600px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">修改系统角色权限</h4>
		</div>
		<div class="modal-body row-fluid ">
			<div class="zTreeDemoBackground left">
				<ul id="treeDemo" class="ztree"></ul>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
			<button type="button" class="btn btn-sm2 btn-00967b" id="saveButton" onclick="saveRoleMenu();">保存</button>
		</div>
	</div>
	<!--隐藏域  -->
	<input type="hidden" id="roleId" name="id" value="${sysRole.id}">
</form>