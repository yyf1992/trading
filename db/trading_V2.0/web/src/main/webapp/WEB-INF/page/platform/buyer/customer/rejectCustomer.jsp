<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td>
			<ul>
				<li>商品</li>
				<li>条形码</li>
				<li>退/换货数量</li>
			</ul></td>
		<td>申请原因</td>
		<td>状态</td>
		<td>操作</td>
	</tr>
</table>
<div class="orderList serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
		<div>
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> <span class="order_num"><span>售后单号:</span>
					${customer.customerCode}</span> <span class="order_name">${customer.supplierName}</span>
			</p>
			<table>
				<tr>
					<td><c:forEach var="customerItem" items="${customer.itemList}">
							<ul class="clear">
								<li><span class="defaultImg"></span>
									<div>
										${customerItem.productCode}|${customerItem.productName} <br>
										<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
											${customerItem.skuName}</span>
									</div></li>
								<li>${customerItem.skuOid}</li>
								<li>${customerItem.goodsNumber}</li>
							</ul>
						</c:forEach></td>
					<td>${customer.reason}</td>
					<td>
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待内部审批</c:when>
								<c:when test="${customer.status==1}">待对方确认</c:when>
								<c:when test="${customer.status==2}">已驳回</c:when>
								<c:when test="${customer.status==3}">已取消</c:when>
								<c:when test="${customer.status==4}">对方已确认</c:when>
							</c:choose>
						</div>
						<div class="opinion_view">
							<span class="orange" data="${customer.id}">查看审批流程</span>
						</div>	
					</td>
					<td>
						<a href="javascript:void(0)" button="详情"onclick="leftMenuClick(this,'platform/buyer/customer/loadCustomerDetails?id=${customer.id}'+'&returnUrl=platform/buyer/customer/customerList&menuId=17091216212559741744','buyer','17112215171918654466')"class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe695;</i>详情</a>
						<a href="javascript:void(0)" button="修改"onclick="leftMenuClick(this,'platform/buyer/customer/updateCustomer?id=${customer.id}','buyer','17112214564049463774');"class="layui-btn layui-btn-mini">
							<i class="layui-icon">&#xe8fd;</i>修改</a>
						<a href="javascript:void(0)" button="取消" onclick="cancelApply('${customer.id}');"class="layui-btn layui-btn-primary layui-btn-mini">
							<i class="layui-icon">&#xe7ea;</i>取消</a>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>