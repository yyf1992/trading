<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<script type="text/javascript">
var planTime = <%=request.getAttribute("planTime")%>;
$(function(){
	if(planTime != null){
		loadDate();
	}
	$(".plus-icon").click(function(){
		var plusIcon = $(this)
		,p = plusIcon.parent()
		,a = p.find("a")
		,inputUserName = p.find("input.multiUserInput").attr("name")
		,selectUsers = [];
		$.each(a,function(i,u){
			selectUsers.push($(this).find("input[name='userId']").val());
		});
		var selectId = inputUserName;
		var selectType = "3";
		$.ajax({
			url : basePath+"platform/sysVerify/loadPerson",
			data: {
				"page.divId":"layOpen",
				"selectId":selectId,
				"selectType":selectType,
				"selectUsers":selectUsers.join(",")
			},
			success:function(data){
				layer.open({
					type:1,
					title:"选择人员",
					skin: 'layui-layer-rim',
		  		    area: ['600px', 'auto'],
		  		    content:data,
		  		    yes:function(index,layero){
		  		    }
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
});
function loadDate(){
	$("select[name='salePlanWeek']").val(planTime.salePlanWeek.split("-"));
	$("select[name='confirmPlanWeek']").val(planTime.confirmPlanWeek.split("-"));
	$("select[name='applyPlanWeek']").val(planTime.applyPlanWeek.split("-"));
	$("select[name='purchaseWeek']").val(planTime.purchaseWeek.split("-"));
	var salePlanInterval = planTime.salePlanInterval;
	var salePlanArea = planTime.salePlanArea;
	$("input[name='salePlanInterval']").val(salePlanInterval);
	$("input[name='salePlanArea']").val(salePlanArea);
	$(".multiUser").each(function(){
		var multiUser = $(this)
			,plusIcon = multiUser.find(".plus-icon")
			,userId = $(this).find("input.multiUserInput").val();
		if(userId=='')return;
		$.ajax({
			type : "GET",
			url  : basePath+"platform/sysUser/multiUser",
			data : {"userId" : userId},
			dataType: "json",
			success:function(data){
				if(data.flag){
					$.each(data.userList,function(i,user){
						var aObj = ['<a href="javascript:;">','<input type="hidden" value="'+user.id+'" name="userId">','<span>'+user.userName+'</span>','<i></i></a>'].join('');
						plusIcon.before(aObj);
						multiUser.find("i").click(function(){deleteUser($(this))});
					});
				}
			}
		});
	});
}
//删除人员
function deleteUser(iObj){
	var a = $(iObj).parent()
	, d = a.parent()
	, inputUser = d.find("input.multiUserInput")
	,userIdP = [];
	a.remove();
	d.find("a").each(function(){
		var userIdI = $(this).find("input[name='userId']").val();
		userIdP.push(userIdI);
	});
	inputUser.val(userIdP.join("-"));
}
// 保存修改
function savePlanTime(){
	if($("select[name='salePlanWeek']").val()==''){
		layer.msg("请选择销售计划提报日期！",{icon:2});
		return;
	}
	if($("input[name='salePlanInterval']").val()==''){
		layer.msg("请填写销售计划间隔！",{icon:2});
		return;
	}
	if($("input[name='salePlanArea']").val()==''){
		layer.msg("请填写销售周期范围！",{icon:2});
		return;
	}
	if($("select[name='confirmPlanWeek']").val()==''){
		layer.msg("请选择确认销售计划日期！",{icon:2});
		return;
	}
	if($("select[name='applyPlanWeek']").val()==''){
		layer.msg("请选择采购计划提报日期！",{icon:2});
		return;
	}
	if($("select[name='purchaseWeek']").val()==''){
		layer.msg("请选择采购下单日期！",{icon:2});
		return;
	}
	$.ajax({
		type : "POST",
		url : "buyer/planTime/savePlanTimeSet",
		async: false,
		data : {
			"salePlanWeek":$("select[name='salePlanWeek']").val().join("-"),
			"confirmPlanWeek":$("select[name='confirmPlanWeek']").val().join("-"),
			"applyPlanWeek":$("select[name='applyPlanWeek']").val().join("-"),
			"purchaseWeek":$("select[name='purchaseWeek']").val().join("-"),
			"salePlanInterval":$("input[name='salePlanInterval']").val(),
			"salePlanArea":$("input[name='salePlanArea']").val(),
			"outPlanUser":$("input[name='outPlanUser']").val(),
			"confirmSaleplanUser":$("input[name='confirmSaleplanUser']").val(),
			"applyPurchaseUser":$("input[name='applyPurchaseUser']").val(),
			"purchaseUser":$("input[name='purchaseUser']").val()
		},
		success:function(data){
               var result = eval('(' + data + ')');
               var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
               if(resultObj.success){
                   layer.msg(resultObj.msg,{icon:1});
                   location.reload();
               }else{
                   layer.msg(resultObj.msg,{icon:2});
               }
           },
		error : function() {
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<form class="layui-form layui-form-pane" action="" id="planTimeForm">
	<div class="layui-form-item">
    	<label class="layui-form-label">销售计划提报</label>
    	<div class="layui-input-block">
      		<select name="salePlanWeek" lay-filter="salePlanWeek" multiple lay-verify="required" >
		        <option value="0">周一</option>
		        <option value="1">周二</option>
		        <option value="2">周三</option>
		        <option value="3">周四</option>
		        <option value="4">周五</option>
		        <option value="5">周六</option>
		        <option value="6">周日</option>
	      	</select>
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">计划外人员</label>
    	<div class="layui-input-block multiUser">
			<span class="plus-icon"></span>
			<input type="hidden" value="${planTime.outPlanUser}" class="multiUserInput" name="outPlanUser">
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">销售计划间隔</label>
    	<div class="layui-input-inline">
      		<input type="number" name="salePlanInterval" lay-verify="required" placeholder="请输入" class="layui-input" value="30">
    	</div>
    	<div class="layui-form-mid layui-word-aux">天</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">销售周期范围</label>
    	<div class="layui-input-inline">
      		<input type="number" name="salePlanArea" lay-verify="required" placeholder="请输入" class="layui-input" value="7">
    	</div>
    	<div class="layui-form-mid layui-word-aux">
    		天
   		</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">确认销售计划</label>
    	<div class="layui-input-block">
      		<select name="confirmPlanWeek" lay-filter="confirmPlanWeek" multiple lay-verify="required" >
		        <option value="0">周一</option>
		        <option value="1">周二</option>
		        <option value="2">周三</option>
		        <option value="3">周四</option>
		        <option value="4">周五</option>
		        <option value="5">周六</option>
		        <option value="6">周日</option>
	      	</select>
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">确认人员设置</label>
    	<div class="layui-input-block multiUser">
			<span class="plus-icon"></span>
			<input type="hidden" value="${planTime.confirmSaleplanUser}" class="multiUserInput" name="confirmSaleplanUser">
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">采购计划提报</label>
    	<div class="layui-input-block">
      		<select name="applyPlanWeek" lay-filter="applyPlanWeek" multiple lay-verify="required" >
		       	<option value="0">周一</option>
		        <option value="1">周二</option>
		        <option value="2">周三</option>
		        <option value="3">周四</option>
		        <option value="4">周五</option>
		        <option value="5">周六</option>
		        <option value="6">周日</option>
	      	</select>
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">采购计划人员设置</label>
    	<div class="layui-input-block multiUser">
			<span class="plus-icon"></span>
			<input type="hidden" value="${planTime.applyPurchaseUser}" class="multiUserInput" name="applyPurchaseUser">
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">采购下单</label>
    	<div class="layui-input-block">
      		<select name="purchaseWeek" lay-filter="purchaseWeek" multiple lay-verify="required" >
		        <option value="0">周一</option>
		        <option value="1">周二</option>
		        <option value="2">周三</option>
		        <option value="3">周四</option>
		        <option value="4">周五</option>
		        <option value="5">周六</option>
		        <option value="6">周日</option>
	      	</select>
    	</div>
  	</div>
  	<div class="layui-form-item">
    	<label class="layui-form-label">采购下单人员设置</label>
    	<div class="layui-input-block multiUser">
			<span class="plus-icon"></span>
			<input type="hidden" value="${planTime.purchaseUser}" class="multiUserInput" name="purchaseUser">
    	</div>
  	</div>
</form>
<div class="submitButton">
     <button class="layui-btn layui-btn-normal" onclick="savePlanTime();">确认提交</button>
</div>