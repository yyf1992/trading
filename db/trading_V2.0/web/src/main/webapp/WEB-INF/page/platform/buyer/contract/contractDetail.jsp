<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<head>
    <title>修改合同</title>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/contracts.css">
    <%--<script src="<%=basePath%>/js/contract/addContract.js"></script>--%>
    <script type="text/javascript">
        var role = '${role}';
        $(function () {
        });
        //返回列表
        function returnList() {
            leftMenuClick(this,'platform/'+role+'/contract/contractList?role='+role,role=='buyer'?'buyer':'sellers');
        }
        //合同下载
        function runSave(){
            if (this.imgFrame.document.readyState == "complete") {
                this.imgFrame.document.execCommand("SaveAs",true);
            } else {
                window.setTimeout("runSave()", 10);
            }
        }
        function downloadContract(url) {
            
            this.imgFrame.document.location = url;
            runSave();
        }
        //打印
        function printContract(contractId,contentAddress) {
            var url = basePath + "platform/"+role+"/contract/printContract?contractId="+contractId+"&contentAddress="+contentAddress;
            window.open(url);
        }
    </script>
</head>
<div class="content">
    <div class="add_contract">
        <form class="contract_form" action="">
            <div>
                <label>合同编号:</label>${contract.contractNo}
            </div>
            <div>
                <label>合同名称:</label>${contract.contractName}
            </div>
            <div>

                    <c:choose>
                        <c:when test="${role=='seller'}">
                        <label>客户名称:</label>${contract.sponsorName}
                        </c:when>
                        <c:otherwise>
                            <label>供应商名称:</label>${contract.customerName}
                        </c:otherwise>
                    </c:choose>

            </div>
            <div>
                <label>联系人:</label>${contract.customerLinkman}
            </div>
            <div>
                <label>手机号:</label>${contract.customerPhone}
            </div>

            <div>
                <label>合同内容:</label>
                <a href="javascript:void(0);" class="rt" style="color: blue" onclick="printContract('${contract.id}','${contract.contentAddress}');">
                    打印合同
                </a>
                <span  class="rt">&nbsp;&nbsp;</span>
                <a href="javascript:void(0);" class="rt" style="color: blue" onclick="downloadContract('${contract.contentAddress}');">
                    下载合同
                </a>
                <iframe name="imgFrame" style="display:none;"></iframe>
                <div class="contract_show mp">
                    <div placeholder="发起方（单方）的合同文件电子版内容详情展示区" class="layui-textarea" id="contentArea">
                        ${htmlContent}
                    </div>
                </div>
            </div>
            <div>
                <label>留言:</label>
                <div class="contract_show">
                    <textarea id="remark" name="remark" placeholder="" class="layui-textarea" readonly>${contract.remark}</textarea>
                </div>
            </div>
            <div>
                <label>创建日期:</label>
                <fmt:formatDate value="${contract.createDate}" ></fmt:formatDate>
            </div>
            <div>
                <label>发起单位:</label>${contract.sponsorName}
            </div>
            <%--<div>--%>
                <%--<label>发起人:</label>${contract.createrName}--%>
            <%--</div>--%>
            <%--<div>
                <label>合同状态:</label>
                <c:choose>
                    <c:when test="${contract.status eq '0'}">草稿</c:when>
                    <c:when test="${contract.status eq '1'}">待我审批</c:when>
                    <c:when test="${contract.status eq '2'}">待对方审批</c:when>
                    <c:when test="${contract.status eq '3'}">待传合同照</c:when>
                    <c:when test="${contract.status eq '4'}">协议达成</c:when>
                    <c:when test="${contract.status eq '5'}">已驳回</c:when>
                    <c:otherwise>草稿</c:otherwise>
                </c:choose>
            </div>--%>
            <c:choose>
                <c:when test="${contract.status == '4'}">
                    <div class="scanning_copy original" id="oriContractDetailDiv">
                        <label>合同原件照:</label>
                        <br/>
                        <label></label>
                        <c:forEach  var="attachment" items="${attachmentList}">
                        <span>
                            <img layer-src="${attachment.url}" src="${attachment.url}" onclick="openPhoto('#oriContractDetailDiv');">
                        </span>
                        </c:forEach>
                    </div>
                </c:when>
            </c:choose>

        </form>
        <c:if test="${approvePop != null && approvePop == 'approvePop'}">
        </c:if>
        <c:choose>
            <c:when test="${capprovePop != null && approvePop == 'approvePop'}"></c:when>
            <c:otherwise>
                <div class="contract_submit">
                			<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/'+role+'/contract/contractList?${form}&role='+role,role=='buyer'?'buyer':'sellers');">
                			<span class="contractBuild">返回</span></a>
                
<!--                     <div class="layui-btn layui-btn-normal layui-btn-small"  onclick="returnList();">返回</div> -->
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
