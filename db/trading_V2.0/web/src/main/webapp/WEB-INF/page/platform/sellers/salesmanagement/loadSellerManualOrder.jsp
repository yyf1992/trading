<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>手工添加订单</title>
<script src="<%=basePath%>statics/platform/js/upload.js"></script>
<script src="<%=basePath %>/statics/platform/js/sellerManualOrder.js"></script>
<script type="text/javascript">
layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //限定可选日期
	  var ins22 = laydate.render({
	    elem: '#date'
	    ,min: 0
	    ,btns: ['clear']
	  });
});
$(function(){
	var type = ${orderData.paymentType};
	$("#paymentType option[value='"+type+"']").attr("select","selected");
	var unitId = ${unit.id};
	$("#unit option[value='"+unitId+"']").attr("select","selected");
});

</script>
<style type="text/css">
#suppName {
    display: block;
    width: 100%;
    padding-left: 10px;
    height: 30px;
}
.invitation{
  width:92px;
  height:25px;
  background: url('../images/spirit.png') no-repeat -450px -537px;
  float: right;
  border:0;
  cursor: pointer;
}
</style>
</head>
<body>
<div class="loadSellerManualOrder">
<!--搜索部分-->
<div class="search_seller mt">
	<form class="layui-form" action="">
		<input type="hidden" name="clientPerson" value="${orderData.contact_person}">
		<input type="hidden" name="clientPhone" value="${orderData.clientPhone}">
		<input type="hidden" name="sellerId" value="${orderData.sellerId}">
		<div class="layui-inline">
			<label class="layui-form-label">
				<span class="red">*</span>采购商:
			</label>
			<div class="layui-input-inline">
				<input type="hidden" id="sellerId" name="sellerId" value="${orderData.sellerId}">
				<input type="text" id= "suppName" name="suppName" placeholder="请输入公司名称" onkeyup="selectSeller(this);" value="${orderData.suppName}">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">
				<span class="red">*</span>联系人:</label>
			<div class="layui-input-inline">
				<input type="text" id="contact_person" name="contact_person" placeholder="请输入联系人" class="layui-input " value="${orderData.contact_person}"  >
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">
				<span class="red">*</span>手机号:</label>
			<div class="layui-input-inline">
				<input type="tel" id="clientPhone" name="clientPhone" placeholder="请输入手机号" class="layui-input " value="${orderData.clientPhone}"  >
			</div>
		</div>
	<!--  <button class="invite"></button>	   -->	 
	</form>
</div>
<!--采购列表部分-->
<table class="table_pure purchase_list manual mt">
  <thead>
    <tr>
      <td style="width:4%">删除</td>
      <td style="width:35%">商品</td>
      <td style="width:9%">单位</td>
      <td style="width:8%">销售单价</td>
      <td style="width:6%">数量</td>
      <td style="width:7%">优惠金额</td>
      <td style="width:10%">销售金额</td>
      <td style="width:11%">备注</td>
    </tr>
    <tr>
      <td colspan="8"></td>
    </tr>
  </thead>
  <tbody id="product_information">
  	<c:choose>
  		<c:when test="${orderData != null}">
  			<c:forEach var="dataItem" items="${orderData.itemArray}" varStatus="status">
	  			<tr>
			      <td><b></b>
			      	<input type="hidden" name="proName" value="${dataItem.proName}">
					<input type="hidden" name="proCode" value="${dataItem.proCode}">
					<input type="hidden" name="skuCode" value="${dataItem.skuCode}">
				    <input type="hidden" name="colorCode" value="${dataItem.colorCode}">
					<input type="hidden" name="skuOid" value="${dataItem.skuOid}">
					<input type="hidden" name="price" value="${dataItem.price}">
					<input type="hidden" name="unitName" value="${dataItem.unitName}">
			      </td>
			      <td style="text-align: left;">
			        <input type="text" name="proInfo" placeholder="输入商品名称/货号/条形码等关键字" onkeyup="selectGoodsSeller(this);" 
			        value="${dataItem.proCode}|${dataItem.proName}|${dataItem.skuCode}/${dataItem.skuOid}">
			        <span class="increase_goods"><i></i>新增商品</span>			
			      </td>
			      <td>
			        <select name="unit" id="unit">
						<option value="">请选择</option>
						<c:forEach var="unit" items="${unitList}">
							<option value="${unit.id}">${unit.unitName}</option>
						</c:forEach>
				    </select>
			      </td>
			      <td><input type="number"  min="0" class="unit_price" id="unit_price"  name="unit_price" value="${dataItem.price }"></td>
			      <td><input type="number"  min="0" class="number" value="${dataItem.goodsNumber }"></td>
			      <td><input type="number"  min="0" class="discount" value="${dataItem.updatePrice }"></td>
			      <td class="subtotal">${dataItem.priceSum}</td>
			      <td><textarea name="" placeholder="备注内容">${dataItem.remark}</textarea></td>
			    </tr>
  			</c:forEach>
  		</c:when>
  		
  		<c:otherwise>
  			<tr>
		      <td><b></b>
		      	<input type="hidden" name="proName">
				<input type="hidden" name="proCode">
				<input type="hidden" name="skuCode">
				<input type="hidden" name="colorCode">
				<input type="hidden" name="skuOid">
				<input type="hidden" name="price">
		      </td>
		      <td style="text-align: left;">
		        <input type="text" name="proInfo" id="proInfo" placeholder="输入商品名称/货号/条形码等关键字" onkeyup="selectGoodsSeller(this);" value="">
		        <span class="increase_goods"><i></i>新增商品</span>			
		      </td>
		      <td>
		        <select name="unit" id="unit">
						<option value="">请选择</option>
						<c:forEach var="unit" items="${unitList}">
							<option value="${unit.id}">${unit.unitName}</option>
						</c:forEach>
				</select>
		      </td>
		      <td><input type="number" value="0" min="0" class="unit_price" id="unit_price" name="unit_price"></td>
		      <td><input type="number" value="0" min="0" class="number"></td>
		      <td><input type="number" value="0" min="0" class="discount"></td>
		      <td class="subtotal">0.00</td>
		      <td><textarea name="" placeholder="备注内容"></textarea></td>
		    </tr>
  		</c:otherwise>
  	</c:choose>
  </tbody>
  <tfoot>
	  <tr>
	    <td></td>
	    <td>合计</td>
	    <td></td>
	    <td></td>
	    <td class="num">${num}</td>
	    <td class="discount_sum">${discount_sum}</td>
	    <td class="total">${total}</td>
	    <td></td>
	  </tr>
  </tfoot>
</table>
<button class="tr_add"></button>
<!--上传凭证-->
<form action="">
  <ul class="voucher">
    <li>
      <p>
        <span>其他费用：</span>
        <input type="number" placeholder="请输入其他费用" id="otherFee" value="${orderData.otherFee}">
      </p>
      <p>
        <span>应收款：</span>￥<b class="red" id="receivables">${total}</b>
      </p>
    </li>
    <li>
      <p>
        <span>收款金额：</span>
        <input type="number" placeholder="请输入收款金额" id="paymentPrice" value="${orderData.paymentPrice}"> 元
      </p>
      <p>
        <span>收款日期：</span>
   <!-- <input type="text" name="date" id="date"  placeholder="请选择日期" autocomplete="off" class="layui-input" style="width: 233px;height: 30px; display: inline-block;">  -->           
        <input type="text" name="date" id="date"  placeholder="请选择日期" lay-verify="date" class="layui-input" style="width: 230px;height: 30px; display: inline-block;">
      </p>
    </li>
    <li>
        <p>
          <span>收款方式：</span>
          <select name="" style="width: 54%;height: 30px;" id="paymentType">        
            <option value="0">请选择</option>
            <option value="1">银行卡转账</option>
            <option value="2">现金</option>
            <option value="3">网银支付</option>
          </select>
          <script>
          var type = ${orderData.paymentType};
          $("#paymentType option[value='"+type+"']").attr("select","selected"); 
          </script>
        </p>
        <p>
          <span>收款人：</span>
          <input type="text" placeholder="请输入付款人姓名" style="width: 230px;height: 30px;" id="paymentPerson" value="${orderData.paymentPerson}">
        </p>
     </li>
  <!--    <c:choose>
      	<c:when test="${orderData.annexVoucher != null && orderData.annexVoucher != ''}">
     		<li>
		     	<span>附件：</span>
				<div class="upload_license">
					<input type="file" multiple name="attachment3" id="attachment3" onchange="fileUpload(this);">
					<p></p>
					<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
				</div>
				<div class="voucherImg" id="attachment3Div">
					<c:forEach var="annexVoucher" items="${orderData.annexVoucher.split(',')}">
					<span>
						<img src="${annexVoucher}" onMouseOver="toolTip('<img src=${purchVoucher}>')" onMouseOut="toolTip()">
					</span>
				</c:forEach>
				</div>
		  </li>
     	</c:when>
     	<c:otherwise>
     		<li>
     			<span>附件：</span>
				<div class="upload_license">
					<input type="file" multiple name="attachment3" id="attachment3" onchange="fileUpload(this);">
					<p></p>
					<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
				</div>
				<div class="voucherImg" id="attachment3Div">
				</div>
	 		</li>
     	</c:otherwise>
     </c:choose>-->
     <li>
  	    <span>附件：</span>
		<div class="upload_license">
			<input type="file" multiple name="attachment3" id="attachment3" onchange="fileUpload(this);">
			<p></p>
			<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
		</div>
		<div class="voucherImg" id="attachment3Div">
		</div>
	</li>
     <li>
      	<p>
        	<span>经办人：</span>
        	<input type="text" placeholder="请输入经办人姓名" 
        	style="width: 54%;height: 30px;border: 1px solid #D5D5D5;font-size: 12px;padding-left: 5px;" 
        	id="handler" value="${orderData.handler}">
        </p>
     </li>
    </ul>
  </form>
  <div class="text-center">
    <a href="javascript:void(0)" onclick="nextAdd();"><button class="next_step">下一步</button></a>
  </div>
<!--邀请互通好友弹框-->
<!-- <div class="friends_add supplier_invite">
  <div>
    <label>采购商:</label>
    <input type="text" id="" value="" >
  </div>
  <div>
    <label>联系人:</label>
    <input type="text" value="">
  </div>
  <div>
    <label>手机号:</label>
    <input type="text" value="">
  </div>
</div>  -->
<!--新增商品弹出框-->
<div class="new_goods">
  <div class="goodsNew mt">
    <table class="table_blue">
      <thead>
      <tr>
        <td style="width:4%">删除</td>
        <td style="width:8%">货号</td>
        <td style="width:14%">商品名称</td>
        <td style="width:6%">规格代码</td>
        <td style="width:6%">规格名称</td>
        <td style="width:8%">条形码</td>
        <td style="width:8%">单位</td>
        <td style="width:8%">库存下限</td>
        <td style="width:8%">标准库存</td>
        <td style="width:8%">销售单价</td>
        <td style="width:10%">库存积压预警天数</td>
      </tr>
      </thead>
      <tbody id="newGoods">
      <tr>
        <td><i></i></td>
        <td><input type="text" id="new_productCode"></td>
        <td><input type="text" id="new_productName"></td>
        <td><input type="text" id="new_skuCode"></td>
        <td><input type="text" id="new_skuName"></td>
        <td><input type="text" id="new_barcode"></td>
        <td>
        <!--  <input type="text" id="new_unitName">-->
        	<select name="unit" id="unit">
				<option value="">请选择</option>
				<c:forEach var="unit" items="${unitList}">
					<option value="${unit.id}">${unit.unitName}</option>
				</c:forEach>
		   </select>
        </td>
        <td><input type="text" id="new_minStock"></td>
        <td><input type="text" id="new_standardStock"></td>
        <td><input type="text" id="new_price"></td>
        <td><input type="text" id=""></td>
      </tr>
      </tbody>
    </table>
  </div>
  <button class="layui-btn layui-btn-normal layui-btn-small"><i class="layui-icon">&#xe61f;</i> 增加项</button>
</div>
<div id="addUnitSelect" hidden="true">
	<select name="unit" id="unit">
		<option value="">请选择</option>
		<c:forEach var="unit" items="${unitList}">
			<option value="${unit.id}">${unit.unitName}</option>
		</c:forEach>
	</select>
</div>
</div>
</body>
</html>