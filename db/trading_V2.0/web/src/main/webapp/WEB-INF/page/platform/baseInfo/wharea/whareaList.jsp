<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<title>仓库区域管理</title>
<script src="statics/platform/js/wharea.js"></script>
<script type="text/javascript">
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(whareaListTableFilter)', function(obj){
	    var data = obj.data;
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('whareaListTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	whareaCode: $("#whareaCode").val(),
	        	whareaName: $("#whareaName").val()
	        }
	      });
	    },
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<!-- 仓库列表 -->
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="whareaCode" id="whareaCode" autocomplete="off" placeholder='仓库代码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="whareaName" id="whareaName" autocomplete="off" placeholder='仓库名称'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:20,
		id:'whareaListTable',
		height:'full-90',
		url:'<%=basePath %>platform/baseInfo/wharea/loadWhareaListDataJson'
	}" lay-filter="whareaListTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'whareaCode', sort: true,show:true}">仓库代码</th>
            <th lay-data="{field:'whareaName', sort: true,show:true}">仓库名称</th>
            <th lay-data="{field:'status', sort: true,show:true,templet: '#statusTpl', align: 'center'}">状态</th>
            <th lay-data="{field:'whAreaType', sort: true,show:true,templet: '#whAreaTypeTpl', align: 'center'}">仓库类型</th>            
            <th lay-data="{field:'createDate', sort: true,show:true}">创建日期</th>
            <th lay-data="{field:'defectWare', sort: true,show:true}">残次品仓</th>
        </tr>
    </thead>
</table>
<!-- 状态 -->
<script type="text/html" id="statusTpl">
	{{#  if(d.status == 'Y'){ }}
		<span>正常</span>
	{{#  } else if(d.status == 'N'){ }}
		<span>停用</span>
	{{#  } }}
</script>
<!-- 仓库类型 -->
<script type="text/html" id="whAreaTypeTpl">
	{{#  if(d.whAreaType == '1'){ }}
		<span>业务仓</span>
	{{#  } else if(d.whAreaType == '2'){ }}
		<span>管理仓</span>
	{{#  } }}
</script>