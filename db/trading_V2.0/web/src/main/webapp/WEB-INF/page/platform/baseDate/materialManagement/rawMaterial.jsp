<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script>
function checkedThis(obj){
 		if($(obj).parent().hasClass("checked")){
			$(obj).prop("checked",false);
			$(obj).parent().removeClass("checked");
		}else{
			$(obj).prop("checked",true);
			$(obj).parent().addClass("checked");
		}
	}
function pageChange(a,value,obj){
	$("#pageSize").val(value);
	$.ajax({
		url:"platform/baseDate/productCompose/loadProductHtml",
		type:"get",
		data:$(obj).parents().find("form").serialize(),
		async:false,
		success:function(data){
			var productType=$("#productType").val();
			if(productType==0){
				$(".commPop").html(data);
			}else{
				$(".materialpop").html(data);
				$(" select option[value='"+value+"']").attr("selected","selected"); 
 				changePopHeight();
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}
function changePopHeight(){
	  var h = $(window).height();
	  $('.popHeight').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
// 	    if(h >= 900){
// 	        $('.popHeight').css({
// 	        height:500,overflow:'auto'
// 	      })
// 	    }else if (h >= 680 && h < 900){
// 	      $('.popHeight').css({
// 	        height:500,overflow:'auto'
// 	      })
// 	    }
// 	    else if (h >= 580 && h < 680){
// 	      $('.popHeight').css({
// 	        height:400,overflow:'auto'
// 	      })
// 	    }
// 	    else if (h >= 480 && h < 580){
// 	      $('.popHeight').css({
// 	        height:300,overflow:'auto'
// 	      })
// 	    }
// 	    else if (h >= 100 && h < 480){
// 	      $('.popHeight').css({
// 	        height:200,overflow:'auto'
// 	      })
// 	    }
	}
	changePopHeight();
	window.onresize = function(){changePopHeight();
	}
	$(function(){
		var value = $("#pageSize").val();
		$(" select option[value='"+value+"']").attr("selected","selected"); 
	});
</script>
<!--原材料弹框-->
<div class="materialpop">
<form action="platform/baseDate/productCompose/loadProductHtml">
  <div class="materialQuery mt">
    <input type="text" placeholder="货号/商品名称/条形码" id="inputSelect" name="inputSelect" value="${searchPageUtil.object.inputSelect}">&emsp;
    <img src="statics/platform/images/find.jpg" class="" onclick="selectProduct(this);">
    <input id="productType" name="productType" type="hidden" value="1" />
    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
  	<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
  	<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
  	<input id="barcodes" name="barcodes" type="hidden" value="${searchPageUtil.object.barcodes}" />
  </div>
</form>
<div>&nbsp;</div>
<div>&nbsp;</div>
  <div class="materialContent">
  	<div class="popHeight">
  		<table class="table_pure">
		      <thead>
		      <tr>
		        <td style="width:5%"><span class="barcodeAdd"><input type="checkbox" onclick="metaAll(this);"></span></td>
		        <td style="width:7%">序号</td>
		        <td style="width:11%">货号</td>
		        <td style="width:35%">商品名称</td>
		        <td style="width:9%">规格代码</td>
		        <td style="width:9%">规格名称</td>
		        <td style="width:15%">条形码</td>
		        <td style="width:9%">单位</td>
		      </tr>
		      </thead>
		      <tbody id="childGoodsTbody">
		      <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
			      <tr>
			        <td><span class="barcodeAdd"><input type="checkbox" value="${item.id}" name="checkOne" onclick="checkedThis(this);"></span></td>
			        <td>${status.count}</td>
			        <td>${item.product_code}</td>
			        <td>${item.product_name}</td>
			        <td>${item.sku_code}</td>
			        <td>${item.sku_name}</td>
			        <td>${item.barcode}</td>
			        <td>${item.unit_name}</td>
			      </tr>
		      </c:forEach>
		      </tbody>
    	</table>
  	</div>
  </div>  
  <div class="pager">
  	<span class="lf size_sm">
	  	每页显示
	  	<select style="width:50px;height:25px;border-radius: 3px" onchange="pageChange(1,this.value,this);"> 
		  	<option value="10">10</option> 
		  	<option value="25">25</option> 
		  	<option value="50">50</option> 
	  	</select>
	  	条
  	</span>
  		${searchPageUtil.page}
   </div>
</div>