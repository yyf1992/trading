<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../../common/path.jsp"%>
<script src="${basePath}statics/platform/js/5-1-1-1.js"></script>
<style>
.purchasePlanOut select{
	width: 90%
}
.unAllowUpdate{
	cursor:not-allowed;
	background:#EBEBE4;
	height:25px;
}
.allowUpdate{
	height:25px;
}
.upload-group{
  line-height: 28px;
}
.upload-file{
  position: relative;
  vertical-align: baseline;
}
.upload-input{
  position: absolute;
  width:100%;
  height: 100%;
  opacity: 0;
  z-index: 9999;
}
@media only screen and (min-width: 1680px) {
  		#tableDiv_height{
  			height: 650px;
  		}
  		
  	}
  	@media only screen and (min-width: 1367px) and (max-width: 1679px) {
  		#tableDiv_height{
  			height: 480px;
  		}
  		
  	}
  	@media only screen and (max-width: 1366px){
  		#tableDiv_height{
  			height: 350px;
  		}
  		
  	}
  .trInfo {
  font-family: '微软雅黑';
  color: #3a87ad !important;
  background-color: #d9edf7 !important;
}
.trWarn{
  background-color: #fcf8e3 !important;
  border-color: #fbeed5 !important;
}
.table_pure tbody{
	border: 0;
}
.table_pure tbody td{
	height: 57px;
	border: 1px solid #E5E5E5;
	overflow: hidden;
}
.icon_delete{
    display: inline-block;
    width: 15px;
    height: 15px;
    cursor: pointer;
    background: url(${basePath}statics/platform/images/spirit.png) no-repeat -600px -122px;
    margin-top: 10px;
}
.td_delete{
	left:0; 
	width:60px; 
	overflow:hidden;
	position:absolute;
	z-index:1;
	padding: 0 !important;
	border: 0 !important;
}
.tr_commodity>i{
	 margin-top: 10px;
}
</style>
<script type="text/javascript">
// 汇总数量
function sumNumber() {
	var sumNumber = "";
	$("tr[name^='productTr_']").each(function() {
		var number = $(this).find("input[name='applyCount']").val();
		sumNumber = Number(sumNumber) + Number(number);
	});
	$(".total").html(sumNumber);
}
$(function(){
	var form = layui.form;
	form.render("select");
	$("tr[name^='productTr_']").each(function(){
		var trObj = $(this);
		var barcode = trObj.find("input[name='barcode']").val();
		countSalesNumAndPutStorageNum(barcode);
		
		countConfirmSalesNum(barcode);
		countConfirmPutStorageNum(barcode);
		//显示本月销量
                $.ajax({
			        url : "buyer/applyPurchaseHeader/productInterfaceQuerying",
			        data: {
			            "skuoid" : barcode
			        },
			        async:false,
			        success:function(data){
			            var result = eval('(' + data + ')');
			            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						trObj.find("input[name='monthSaleNum']").val(resultObj.monthSaleNum);//本月销量
						trObj.find("input[name='monthDms']").val(resultObj.monthDms);//本月日均销量
						trObj.find("input[name='scStock']").val(resultObj.warncount);//杉橙库存
						trObj.find("input[name='transitNum']").val(resultObj.transitNum);//在途订单数
						trObj.find("input[name='outStock']").val(resultObj.outWhareaNum);//外仓数量
						formulaCalculator1(trObj.find("input[name='transitNum']"));
						sumNumber();
			        },
			        error:function(){
			            layer.msg("获取数据失败，请稍后重试！",{icon:2});
			        }
			    });
	});
	// 删除商品
	$('.trInfo').on('click','td:first-child b',function(){
	  layer.confirm('您确定要删除此商品吗？</p>',{
	    icon:3,
	    skin:'popBuyer btnCenter',
	    title:'提醒',
	    closeBtn:0
	  },function(index){
	    layer.close(index);
	    // 删除子集
		var barcode = $(this).attr("name");
		$("tr[name=productTr_"+barcode+"]").remove();
		$("tr[name=productItemTr_"+barcode+"]").remove();
		delRow(this);
	    sumNumber();
	  }.bind(this));
	});
	//删除部门操作
	$('.trWarn').on('click','td:first-child b',function(){
	  layer.confirm('您确定要删除此部门吗？</p>',{
	    icon:3,
	    skin:'popBuyer btnCenter',
	    title:'提醒',
	    closeBtn:0
	  },function(index){
	    layer.close(index);
	    // 删除子集
		var barcode = $(this).attr("name");
		var shopRow = $("tr[name=productItemTr_"+barcode+"]").length;
		if(shopRow == 1){
			layer.msg("至少保留一个部门！",{icon:7});
			return;
		}
		delRow(this);
		// 汇总销售计划/汇总入仓量
		countSalesNumAndPutStorageNum(barcode);
		countConfirmPutStorageNum(barcode);
		countConfirmSalesNum(barcode);
	    sumNumber();
	  }.bind(this));
	});
	// 提交保存
	$(".next_step").click(function(){
		var title = $("#title").val();
		if(title == ''){
			layer.msg("请填写标题！",{icon:7});
			return;
		}else if(title.length > 30){
			layer.msg("标题长度不能大于30个字符！",{icon:7});
			return;
		}
		var numError="";
		var goodsStr="";
		var confirmSaleNumError="";
		var confirmPutStorageNumError="";
		$("tr[name^='productTr_']").each(function(i){
			var skuOid = $(this).find("input[name='barcode']").val();
			if(skuOid != ""){
				var proCode = $(this).find("input[name='productCode']").val();
				var proName = $(this).find("input[name='productName']").val();
				var skuCode = $(this).find("input[name='skuCode']").val();
				var skuName = $(this).find("input[name='skuName']").val();
				var unitId = $(this).find("input[name='unitId']").val();
				var unitName = $(this).find("input[name='unitName']").val();
				//商品类型
				var productType = $(this).find("input[name='productType']").val();
				//数量
				var num = $(this).find("input[name='applyCount']").val();
				if(parseInt(num)<=0){
					numError="第"+(i+1)+"行数量必须大于0！";
					return false;
				}
				//产品状态
				var productStatus = $(this).find("input[name='productStatus']").val();
				//采购周期
				var purchasCycle = $(this).find("input[name='purchasCycle']").val();
				//本月销量
				var monthSaleNum = $(this).find("input[name='monthSaleNum']").val();
				//本月日均销量
				var monthDms = $(this).find("input[name='monthDms']").val();
				//杉橙库存数量
				var scStock = $(this).find("input[name='scStock']").val();
				//外仓库存数量
				var outStock = $(this).find("input[name='outStock']").val();
				//在途订单数量
				var transitNum = $(this).find("input[name='transitNum']").val();
				//合计库存数量
				var totalStock = $(this).find("input[name='totalStock']").val();
				//销售计划
				var salePlan = $(this).find("td[class='salePlan']").text();
				//确认销售计划
				var confirmSalePlan = $(this).find("td[class='confirmSalePlan']").text();
				if(parseInt(confirmSalePlan)<=0){
					confirmSaleNumError="确认销售计划必须大于0！";
					return false;
				}
				//入仓量
				var totalPutStorageNum = $(this).find("td[class='totalPutStorageNum']").text();
				//确认入仓量
				var confirmPutStorageNum = $(this).find("td[class='confirmPutStorageNum']").text();
				if(parseInt(confirmPutStorageNum)<0 || confirmPutStorageNum == ""){
					confirmPutStorageNumError="确认入仓量不能为空或负数！";
					return false;
				}
				//现需求差异量
				var differenceNum = $(this).find("input[name='differenceNum']").val();
				//预估下月到货
				var predictNextMonthArrival = $(this).find("input[name='predictNextMonthArrival']").val();
				//预估下月底库存
				var predictNextMonthStock = $(this).find("input[name='predictNextMonthStock']").val();
				//备注
				var remark = $(this).find("textarea[name='productRemark']").val();
				//定义附件数组
				var costItem = {};
				costItem["shopList"] = getShopList(skuOid);
				goodsStr += skuOid+".NTNT."+proCode+".NTNT."+proName+".NTNT."+skuCode+".NTNT."+skuName+".NTNT."+unitId+".NTNT."+unitName+".NTNT."+productType+".NTNT."
						+num+".NTNT."+productStatus+".NTNT."+purchasCycle+".NTNT."+monthSaleNum+".NTNT."+monthDms+".NTNT."+scStock+".NTNT."+outStock+".NTNT."
						+transitNum+".NTNT."+totalStock+".NTNT."+salePlan+".NTNT."+confirmSalePlan+".NTNT."+differenceNum+".NTNT."+predictNextMonthArrival+".NTNT."
						+predictNextMonthStock+".NTNT."+remark+".NTNT."+totalPutStorageNum+".NTNT."+confirmPutStorageNum+".NTNT."+JSON.stringify(costItem) + "@NTNT@";
			}
		});
		goodsStr = goodsStr.substring(0, goodsStr.length - 6);
		//填写销售计划校验
		var inputSaleNumError = "";
		$("tr[name^='productItemTr_']").each(function(){
			//剩余销售计划
			var salesNum = $(this).find("td[class='salesNum']").text();
			//确认销售计划
			var confirmSalesNum = $(this).find("input[name='confirmSalesNum']").val();
			if(confirmSalesNum == "" || parseInt(confirmSalesNum)<=0 || parseInt(confirmSalesNum) > parseInt(salesNum)){
				inputSaleNumError="确认销售计划必须大于0且小于等于剩余销售计划！";
				return false;
			}
		});
		if(inputSaleNumError!=''){
			layer.msg(inputSaleNumError,{icon:7});
			return;
		}
		if(numError!=''){
			layer.msg(numError,{icon:7});
			return;
		}
		if(confirmSaleNumError!=''){
			layer.msg(confirmSaleNumError,{icon:7});
			return;
		}
		if(confirmPutStorageNumError!=''){
			layer.msg(confirmPutStorageNumError,{icon:7});
			return;
		}
		sumNumber();
		var numSum = $(".total").html();
		// 附件信息
		var a3 = $("#attachment3Div").find("span").length;
		if(a3 > 1){
			layer.msg("附件只允许上传一个！",{icon:7});
			return;
		}
		var url3 = $("#attachment3Div").find("span").find("input").val();
		var url = "buyer/applyPurchaseHeader/saveAddPurchase";
		$.ajax({
			type : "POST",
			url : url,
			async: false,
			data : {
				"title" : title,
				"goodsStr" : goodsStr,
				"numSum" : numSum,
				"predictArred" : $("#predictArred").val(),
				"remark" : $("#remark").val(),
				"url3" : url3,
				"menuName" : "17070718133683994602"
			},
			success : function(data) {
				if(data.flag){
					var res = data.res;
					if(res.code==40000){
						//调用成功
						saveSucess();
					}else if(res.code==40010){
						//调用失败
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40011){
						//需要设置审批流程
						layer.msg(res.msg,{time:500,icon:2},function(){
							setApprovalUser(url,res.data,function(data){
								saveSucess(data);
							});
						});
						return false;
					}else if(res.code==40012){
						//对应菜单必填
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40013){
						//不需要审批
						notNeedApproval(res.data,function(data){
							saveSucess(data);
						});
						return false;
					}
				}else{
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
					return false;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	});
});
//保存成功
function saveSucess(ids){
	layer.msg("保存成功！",{icon:1});
	leftMenuClick(this,"buyer/applyPurchaseHeader/applyPurchaseList","buyer","17070718133683994603");
}
function getShopList(skuOid){
	var shopList = new Array();
	$("tr[name^='productItemTr_"+skuOid+"']").each(function(){
		var shop = {};
		var shopId = $(this).find("input[name='shopId']").val();
		var shopCode = $(this).find("input[name='shopCode']").val();
		var shopName = $(this).find("input[name='shopName']").val();
		var planCode = $(this).find("input[name='planCode']").val();
		//销售开始日期
		var saleStartDate = $(this).find("input[name='startDate']").val();
		//销售结束日期
		var saleEndDate = $(this).find("input[name='endDate']").val();
		//销售天数
		var saleDays = $(this).find("input[name='saleDays']").val();
		//剩余销售计划
		var salesNum = $(this).find("td[class='salesNum']").text();
		//确认销售计划
		var confirmSalesNum = $(this).find("input[name='confirmSalesNum']").val();
		//入仓量
		var putStorageNum = $(this).find("td[class='putStorageNum']").text();
		//确认入仓量
		var confirmPutStorageNum = $(this).find("input[name='confirmPutStorageNum']").val();
		shop["shopId"] = shopId;
		shop["shopCode"] = shopCode;
		shop["shopName"] = shopName;
		shop["planCode"] = planCode;
		shop["saleStartDate"] = saleStartDate;
		shop["saleEndDate"] = saleEndDate;
		shop["saleDays"] = saleDays;
		shop["salesNum"] = salesNum;
		shop["confirmSalesNum"] = confirmSalesNum;
		shop["putStorageNum"] = putStorageNum;
		shop["confirmPutStorageNum"] = confirmPutStorageNum;
		shopList.push(shop);
	});
	return shopList;
}
// 公式计算
function formulaCalculator1(obj){
	var scStock = $(obj).parents("tr[name^='productTr_']").find("input[name='scStock']").val();//杉橙库存数量
	var outStock = $(obj).parents("tr[name^='productTr_']").find("input[name='outStock']").val();//外仓库存数量
	var transitNum = $(obj).parents("tr[name^='productTr_']").find("input[name='transitNum']").val();//在途数量
	var totalStock = Number(scStock)+Number(outStock)+Number(transitNum);
	$(obj).parents("tr[name^='productTr_']").find("input[name='totalStock']").val(totalStock);//合计库存（衫橙库存数量+外仓库存数量+在途订单数量）
	var salePlan = $(obj).parents("tr[name^='productTr_']").find("td[class='confirmSalePlan']").text();//销售计划
	var putStorageNum = $(obj).parents("tr[name^='productTr_']").find("td[class='confirmPutStorageNum']").text();//入仓量
	$(obj).parents("tr[name^='productTr_']").find("input[name='differenceNum']").val(Number(salePlan)-Number(totalStock)+Number(putStorageNum));//现需求差异量（销售计划-合计库存数量+入仓量）
	var predictNextMonthArrival = Number(salePlan)-Number(totalStock)+Number(putStorageNum);
	$(obj).parents("tr[name^='productTr_']").find("input[name='predictNextMonthArrival']").val(predictNextMonthArrival);//预估下月到货
	$(obj).parents("tr[name^='productTr_']").find("input[name='applyCount']").val(predictNextMonthArrival);//下单数量
	//$(obj).parents("tr").find("td:eq(19) input").val(Number(totalStock)+Number(predictNextMonthArrival)-Number(salePlan));//预估月底库存（合计库存数量+预估下月到货-销售计划）
	formulaCalculator2(obj);
	sumNumber();
}
function formulaCalculator2(obj){
	var predictNextMonthArrival = $(obj).parents("tr[name^='productTr_']").find("input[name='predictNextMonthArrival']").val();//预估下月到货
	var totalStock = $(obj).parents("tr[name^='productTr_']").find("input[name='totalStock']").val();//合计库存
	var salePlan = $(obj).parents("tr[name^='productTr_']").find("td[class='confirmSalePlan']").text();//销售计划
	$(obj).parents("tr[name^='productTr_']").find("input[name='predictNextMonthStock']").val(Number(totalStock)+Number(predictNextMonthArrival)-Number(salePlan));//预估月底库存（合计库存数量+预估下月到货-销售计划）
	$(obj).parents("tr[name^='productTr_']").find("input[name='applyCount']").val(predictNextMonthArrival);//下单数量
	sumNumber();
}
// 汇总销售计划/汇总入仓量
function countSalesNumAndPutStorageNum(barcode){
	var totalSalePlan = 0;
	var totalPutStorageNum = 0;
	$("tr[name='productItemTr_"+barcode+"']").each(function(){
		var salePlan = $(this).find("td[class='salesNum']").text();
		totalSalePlan = Number(totalSalePlan) + Number(salePlan);
		var putStorageNum = $(this).find("td[class='putStorageNum']").text();
		totalPutStorageNum = Number(totalPutStorageNum) + Number(putStorageNum);
	});
	$("tr[name='productTr_"+barcode+"']").find("td[class='salePlan']").text(totalSalePlan);
	$("tr[name='productTr_"+barcode+"']").find("td[class='totalPutStorageNum']").text(totalPutStorageNum);
	formulaCalculator1($("tr[name='productTr_"+barcode+"']").find("td[class='confirmSalePlan']"));
}
// 汇总确认销售计划
function countConfirmSalesNum(barcode){
	var totalSalePlan = 0;
	$("tr[name='productItemTr_"+barcode+"']").each(function(){
		var salePlan = $(this).find("input[name='confirmSalesNum']").val();
		totalSalePlan = Number(totalSalePlan) + Number(salePlan);
	});
	$("tr[name='productTr_"+barcode+"']").find("td[class='confirmSalePlan']").text(totalSalePlan);
	formulaCalculator1($("tr[name='productTr_"+barcode+"']").find("td[class='confirmSalePlan']"));
}
// 汇总确认入仓量
function countConfirmPutStorageNum(barcode){
	var totalPutStorageNum = 0;
	$("tr[name='productItemTr_"+barcode+"']").each(function(){
		var putStorageNum = $(this).find("input[name='confirmPutStorageNum']").val();
		totalPutStorageNum = Number(totalPutStorageNum) + Number(putStorageNum);
	});
	$("tr[name='productTr_"+barcode+"']").find("td[class='confirmPutStorageNum']").text(totalPutStorageNum);
	formulaCalculator1($("tr[name='productTr_"+barcode+"']").find("td[class='confirmSalePlan']"));
}
$('#tableDiv_y').height($('#tableDiv_height').height() - 50);
	$('#scrollDiv_x>div').width($('table.tableMain').width());
	$('#scrollDiv_y').height($('#tableDiv_height').height() - 50);
	$('#scrollDiv_y>div').height($('table.tableMain').height());
	
	$('.td_delete').each(function(){
		var h = $(this).parent().height();
		console.log($(this).parent());
		$(this).height(h);
	})
function divScroll(scrollDiv){
  var scrollLeft = scrollDiv.scrollLeft;
  document.getElementById("tableDiv_title").scrollLeft = scrollLeft;
  document.getElementById("tableDiv_body").scrollLeft = scrollLeft;    
}
function divYScroll(scrollYDiv){
  var scrollTop = scrollYDiv.scrollTop;
  document.getElementById("tableDiv_y").scrollTop = scrollTop;  
}
</script>
<div id="addPurchaseDiv">
	<!--搜索部分-->
	<div class="search_supplier mt">
		<form class="layui-form" action="">
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>标题:</label>
				<div class="layui-input-inline">
					<input type="text" id="title" name="title" class="layui-input" value="">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">要求到货日期：</label>
				<div class="layui-inline">
					<input type="text" name="predictArred" id="predictArred" lay-verify="date" placeholder="年/月/日" class="layui-input" style="width: 130px" value="">
				</div>
			</div>
		</form>
	</div>
	<br>
	<!--采购列表部分-->
	<form class="layui-form" style="overflow: auto;">
		<div style="position:relative; padding-right:18px;">
		  <div style="position:relative;height:;overflow:hidden;width:100%" id="tableDiv_height">
		  <div style="padding-left:460px; width:auto; overflow:hidden;" id="tableDiv_title" >
		  <table class="layui-table" style="margin: 0; text-align: center;" lay-size="sm">
		  	<thead>
		  		<tr>
			    <td style="left:0; top:0; width:60px; overflow:hidden;background:#f2f2f2;position:absolute;z-index:1;">删除</td>
			    <td style="min-width:400px;max-width:400px;width: 400px;left:60px; top:0;overflow:hidden;background:#f2f2f2;position:absolute;z-index:1;" >商品</td>
			    <td style="min-width:80px;max-width:80px;width: 80px;" >单位</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >商品类型</td>
			    <td style="min-width:150px;max-width:150px;width: 150px;" >本次合计下单数量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >产品状态</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >采购周期</td>
			    <td style="min-width:230px;max-width:230px;width: 230px;" >销售开始日期 至 销售结束日期</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >销售天数</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >本月销量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >本月日均销量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >杉橙库存数量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >外仓库存数量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >在途订单数量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >合计库存数量</td>
			    <td style="min-width:100px;max-width:100px;width: 100px;" >剩余销售计划</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >确认销售计划</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >入仓量</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >确认入仓量</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >最近销售达成率</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >现需求差异量</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >预估下月到货</td>
	            <td style="min-width:110px;max-width:110px;width: 110px;" >预估下月底库存</td>
	            <td style="min-width:100px;max-width:100px;width: 100px;" >备注</td>
		   </tr>
		  	</thead>
		   </table>
		   </div> 
		  <div style="overflow:hidden; position:absolute;height:; width:100%;" id="tableDiv_y">
		    <div style="padding-left:460px; width:auto;overflow:hidden;" id="tableDiv_body">
		    <table class="table_pure tableMain" style="margin: 0;text-align: center;">
		    	<c:forEach var="product" items="${productList}">
		    		<tr class="trInfo" name="productTr_${product.barcode}" style="overflow: hidden;">
		    			<td style="left:0; width:60px; overflow:hidden;position:absolute;z-index:1;padding: 0;border: 0" class="trInfo td_delete">
		    				<b class="icon_delete" name="${product.barcode}"></b></b>
		    			</td>
				      	<td style="min-width:400px;max-width:400px;width: 400px;left:60px;overflow:hidden;position:absolute;z-index:1;" class="trInfo td_commodity">
				      		${product.productCode}|${product.productName}|${product.skuCode}|${product.skuName}|${product.barcode}
	          				<input type="hidden" name="productCode" value="${product.productCode}"/>
	          				<input type="hidden" name="productName" value="${product.productName}"/>
	          				<input type="hidden" name="skuCode" value="${product.skuCode}"/>
	          				<input type="hidden" name="skuName" value="${product.skuName}"/>
	          				<input type="hidden" name="barcode" value="${product.barcode}"/>
				      	</td>
				      	<td style="min-width:80px;max-width:80px;width: 80px;" >
				      		${product.unitName}
	          				<input type="hidden" name="unitId" value="${product.unitId}"/>
	          				<input type="hidden" name="unitName" value="${product.unitName}"/>
				      	</td>
				      	<td style="min-width:100px;max-width:100px;width: 100px;" >
				      		<c:choose>
	                            <c:when test="${product.productType=='0'}">成品</c:when>
	                            <c:when test="${product.productType=='1'}">原材料</c:when>
	                            <c:when test="${product.productType=='2'}">辅料</c:when>
	                            <c:when test="${product.productType=='3'}">虚拟产品</c:when>
	                            <c:otherwise></c:otherwise>
	                        </c:choose>
	                        <input type="hidden" name="productType" value="${product.productType}">
				      	</td>
				      	<td style="min-width:150px;max-width:150px;width: 150px;" >
				      		<input type="number" name="applyCount" value="0" min="0" class="unAllowUpdate" onchange="sumNumber();" style="width: 50px;border: 0" readonly="readonly">
				      	</td>
				      	<td style="min-width:100px;max-width:100px;width: 100px;" ><input type="text" class="allowUpdate" name="productStatus" placeholder="输入产品状态" size="12" style="border: 0"></td>
				      	<td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="purchasCycle" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0"></td>
				      	<td style="min-width:230px;max-width:230px;width: 230px;" >-</td>
					    <td style="min-width:100px;max-width:100px;width: 100px;" >-</td>
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="monthSaleNum" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 本月销量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="monthDms" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 本月日均销量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="scStock" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 杉橙库存 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="outStock" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0" onkeyup="formulaCalculator1(this);"></td><!-- 外仓库存 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="transitNum" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0" onkeyup="formulaCalculator1(this);"></td><!-- 在途数量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="totalStock" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 合计库存 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" class="salePlan"></td><!-- 剩余销售计划 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" class="confirmSalePlan"></td><!-- 确认销售计划 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" class="totalPutStorageNum"></td><!-- 入仓量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" class="confirmPutStorageNum"></td><!-- 确认入仓量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" >-</td><!-- 最近销售达成率 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="differenceNum" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 现需求差异量 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><input type="number" name="predictNextMonthArrival" value="0" min="0" class="allowUpdate" style="width: 50px;border: 0" onkeyup="formulaCalculator2(this);"></td><!-- 预估下月到货 -->
					    <td style="min-width:110px;max-width:110px;width: 110px;" ><input type="number" name="predictNextMonthStock" value="0" min="0" class="unAllowUpdate" style="width: 50px;border: 0" readonly="readonly"></td><!-- 预估月底库存 -->
					    <td style="min-width:100px;max-width:100px;width: 100px;" ><textarea placeholder="备注内容" name='productRemark' style="height: 31px"></textarea></td>
		    		</tr>
		    		<c:forEach var="shop" items="${product.shopItemList}">
		    			<tr name="productItemTr_${product.barcode}" class="trWarn">
		    				<td style="left:0; width:60px; overflow:hidden;position:absolute;z-index:1;padding: 0;border: 0" class="trWarn td_delete">
		    					<b class="icon_delete" name="${product.barcode}"></b>
		    				</td>
					      	<td style="min-width:400px;max-width:400px;width: 400px;left:60px;overflow:hidden;position:absolute;z-index:1;" class="trWarn td_commodity" >
					      		${shop.shop_name}
	          					<input type="hidden" name="shopId" value="${shop.shop_id}"/>
	          					<input type="hidden" name="shopCode" value="${shop.shop_code}"/>
	          					<input type="hidden" name="shopName" value="${shop.shop_name}"/>
					      	</td>
					      	<td colspan="2" style="min-width:180px;max-width:180px;width: 180px;">
					      		${shop.plan_code}
          						<input type="hidden" name="planCode" value="${shop.plan_code}"/>
          					</td>
					      	<td style="min-width:150px;max-width:150px;width: 150px;">-</td><!-- 下单数量 -->
	          				<td style="min-width:100px;max-width:100px;width: 100px;">-</td>
	          				<td style="min-width:100px;max-width:100px;width: 100px;">-</td>
					      	<td style="min-width:230px;max-width:230px;width: 230px;"><fmt:formatDate value="${shop.start_date}" type="date"></fmt:formatDate> 至 <fmt:formatDate value="${shop.end_date}" type="date"></fmt:formatDate>
	          					<input type="hidden" name="startDate" value="<fmt:formatDate value="${shop.start_date}" type="date"></fmt:formatDate>"/>
	          					<input type="hidden" name="endDate" value="<fmt:formatDate value="${shop.end_date}" type="date"></fmt:formatDate>"/>
	          				</td>
	          				<td style="min-width:100px;max-width:100px;width: 100px;">${shop.sale_days}
	          					<input type="hidden" name="saleDays" value="${shop.sale_days}"/>
	          				</td>
	          				<td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td class="salesNum" style="min-width:100px;max-width:100px;width: 100px;">${shop.overplusNum}</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;"><input type="number" name="confirmSalesNum" value="${shop.overplusNum}" min="0" class="allowUpdate" style="width: 50px;border: 0" onkeyup="countConfirmSalesNum('${product.barcode}');"></td>
		                    <td class="putStorageNum" style="min-width:100px;max-width:100px;width: 100px;">${shop.put_storage_num}</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;"><input type="number" name="confirmPutStorageNum" value="${shop.put_storage_num}" min="0" class="allowUpdate" style="width: 50px;border: 0" onkeyup="countConfirmPutStorageNum('${product.barcode}');"></td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">${shop.salesCompletionRate*100}%</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		                    <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
				            <td style="min-width:110px;max-width:110px;width: 110px;">-</td>
				            <td style="min-width:100px;max-width:100px;width: 100px;">-</td>
		    			</tr>
		    		</c:forEach>
		    	</c:forEach>
		    	<tfoot>
          <tr>
            <td style="left:0; width:60px; overflow:hidden;position:absolute;z-index:1;padding: 0;background: #ffffff;"></td>
            <td style="min-width:400px;max-width:400px;width: 400px;left:60px;overflow:hidden;position:absolute;z-index:1;background: #ffffff;">合计</td>
            <td></td>
            <td></td>
            <td class="total">0</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          </tfoot>
		    </table>
		  </div>     
		  </div> 
		   <div style="background-color:#eee;overflow:hidden;bottom: 0; width:100%; z-index:2;position:absolute;">
		    <div style="margin-left:460px; width:auto;overflow-x:scroll;overflow-y:hidden;" onscroll='divScroll(this);' id="scrollDiv_x">
		      <div style="width:; height:1px;"></div>
		    </div>
		  </div>
		  </div>
		    <div id="scrollDiv_y" style="display:block; overflow-x:hidden; overflow-y:scroll; position:absolute; top:22px; right:0px; padding-bottom:10px;" onscroll='divYScroll(this);'>
		      <div style="width:1px; height:;"></div>
		     </div>
		  </div>
		  </form>
	<br><br><br>
	<div class="size_sm">
		<p class="mt">说明：</p>
		<textarea placeholder="请输入说明内容" class="orderRemark" id="remark">${updatePurchase.remark}</textarea>
	</div>
	<div class="size_sm mp mt upload-group">
            <span class="mt">附件:</span>
            <div class="upload-file size_sm layui-inline">
              <input type="file" class="upload-input" name="attachment3" id="attachment3" onchange="fileUpload(this);">
              <button type="button" class="layui-btn layui-btn-primary layui-btn-small">上传文件</button>
            </div>
            <span class="c99">仅支持JPG,GIF,PNG,JPEG,XLS,XLSX格式，且文件小于4M。</span>
            <div class="voucherImg" id="attachment3Div" style="margin-left: 35px">
            	<c:if test="${updatePurchase.proof != null && updatePurchase.proof != ''}">
					<span><b></b><img src="${updatePurchase.proof }" onMouseOver="toolTip('<img src=${updatePurchase.proof }>')" onMouseOut="toolTip()">
					<input type="hidden" name="fileUrl" value="${updatePurchase.proof }"></span>
				</c:if>
            </div>
    </div>
	<div class="text-right">
		<a><input type="button" class="next_step" value="提交"></a>
	</div>
	<!--选择商品弹出框-->
    <div class="commSelPlan" id="commSelPlan"></div>
</div>
