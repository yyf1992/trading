<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/setPrice.js" type="text/javascript"></script>
<script src="<%=basePath%>statics/platform/js/upload.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%=basePath%>statics/css/nuotai.css"></link>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/warehouse.css">
<script type="text/javascript">
$(function(){
	var unitId = '${unitId}';
	//$("#unitId").find("option[value = '"+unitId+"']").attr("selected","selected");
	$("#unitId").val(unitId);
});
//删除图片
$('.voucherImg').on('click','b',function(){
  layer.confirm('确定要删除该图片吗？</p>',{
    icon:3,
    skin:'pop',
    title:'提醒',
    closeBtn:2
  },function(index){
    layer.close(index);
    $(this).parent().remove();
  }.bind(this));
});
</script>
<style>

#attachment3{
position:absolute;
left:580px;
top:320px;
}
</style>
<!-- 内容 -->
<div  class="content_modify">
    <h3 class="edit_title">修改商品</h3>
	<form class="" id="productForm">
	    <div class="stock_explain">
	    <div class="voucherImg" id="attachment3Div">
	    </div>
	    <c:if test="${buyProduct.mainPictureUrl !=null }">
	    	<img src="${buyProduct.mainPictureUrl }">
	    </c:if>
	    <c:if test="${buyProduct.mainPictureUrl ==null }">
	    	 <span class="defaultImg" id="defaultImg"></span>
	    </c:if>
	    
	 <!-- <input type="file" name="mainPictureUrl" id="attachment3" onchange="fileUpload(this);" style="width: 180px;">  -->   
	    	    	   
	    <ul>
	  	  <li>
	        <span>商品名称:</span>
	        <input type="text"  name="productName" id="productName" placeholder="请输入商品名称" value="${productName}" style="height: 30px;width: 308px">
	      </li>
	      <li>
	        <span>商品货号:</span>
	        <input type="text" placeholder="请输入商品货号" name="productCode" id="productCode" value="${productCode}"
	             style='border:none;height:30px;width:308px;' readonly>
	      </li>
	      <li>
			 <span>单&emsp;&emsp;位:</span>
			 <select name="unitId" id="unitId" lay-filter="aihao" style="display: inline-block;width: 308px">
				<c:forEach var="BuyUnit" items="${buyUnitList}">
					<option value="${BuyUnit.id}" id="${BuyUnit.id}">${BuyUnit.unitName}</option>
				</c:forEach>
			 </select>
	      </li>
	    </ul>
	    </div>
	    <input type="hidden" value="${productType}" id="productTypeNum">
	    <input type="hidden" value="${id}" id="id" name="id">
	 </form>

	<div class="text-right mp30">
		<button id="skuNew" class="skuNew c66"
			onclick="newSku('${id}','${productCode}','${productType}','${unitId}','${productName}');">新增商品SKU</button>
	</div>
	<c:choose>
		<c:when test="${buyProductSkus.size()==0}">
		<!--  	<div class="noneData">
				<p>您还没有数据请添加！</p>
			</div> -->
			<table class="table_blue skuList mp30">
				<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:8%">规格代码</td>
						<!-- <td style="width:7%">颜色</td> -->
						<td style="width:7%">规格名称</td>
						<td style="width:18%">条形码</td>
						<td style="width:12%">销售单价</td>
						<td style="width:10%">标准库存</td>
						<td style="width:10%">库存下限</td>
						<td style="width:20%">操作</td>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<table class="table_blue skuList mp30">
				<thead>
					<tr>
						<td style="width:15%">货号</td>
						<td style="width:8%">规格代码</td>
						<!-- <td style="width:7%">颜色</td> -->
						<td style="width:7%">规格名称</td>
						<td style="width:18%">条形码</td>
						<td style="width:12%">销售单价</td>
						<td style="width:10%">标准库存</td>
						<td style="width:10%">库存下限</td>
						<td style="width:10%">销售天数</td>
						<td style="width:20%">操作</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="BuyProductSku" items="${buyProductSkus}">
						<tr>
							<td>${BuyProductSku.productCode}</td>
							<td>${BuyProductSku.skuCode}</td>
							<%-- <td>${BuyProductSku.colorCode}</td> --%>
							<td>${BuyProductSku.skuName}</td>
							<td>${BuyProductSku.barcode}</td>
							<td>${BuyProductSku.price}</td>
							<td>${BuyProductSku.standardStock}</td>
							<td>${BuyProductSku.minStock}</td>
							<td>${BuyProductSku.planSalesDays}</td>
							<td><span class="table_edit skuEdit"
								onclick="editSku('${BuyProductSku.id}','${id}','${productCode}','${productType}','${unitId}','${productName}');"><b></b>修改</span>&emsp;
								<span class="table_del skuDel"
								onclick="deleteSku('${BuyProductSku.id}','${id}','${productCode}','${productType}','${unitId}','${productName}');"><b></b>删除</span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	<div class="text-center">
 		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/allProduct?productType=${productType}&${form}','baseInfo')">
			<input type="button" class="next_step" value="返回">
		</a>
		<button class="skuSave" id="commSubmit" onclick="updateProduct(this);">保存</button>
	</div>
</div>
