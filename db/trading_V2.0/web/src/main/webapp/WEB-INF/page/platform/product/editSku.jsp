<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--修改sku弹框-->
<form id="editSkuForm">
	<div class="skuIncrease skuCompile">
	  <div>
	  		<input type="hidden" name="productName" value="${buyProductSku.productName}">
	    	<input type="hidden" name="productCode" value="${buyProductSku.productCode}">
			<input type="hidden" name="productType" value="${buyProductSku.productType}">
			<input type="hidden" name="unitId" value="${buyProductSku.unitId}">
	    <span>&emsp;规格代码：</span>
	    <input type="text" value="${buyProductSku.skuCode}" name="skuCode">
	  </div>
	  <div>
	    <%-- <span>&emsp;颜&emsp;&emsp;色：</span>
	    <input type="text" value="${buyProductSku.colorCode}" name="colorCode"> --%>
	    <span>&emsp;规格名称：</span>
	    <input type="text" value="${buyProductSku.skuName}" name="skuName">
	  </div>
	  <div>
	    <span><span class="red">*</span> 条 形 码：</span>
	    <input type="text" value="${buyProductSku.barcode}" name="barcode">
	  </div>
	  <div>
	    <span><span class="red">*</span> 标准库存：</span>
	    <input type="number" value="${buyProductSku.standardStock}" name="standardStock">
	  </div>
	  <div>
	    <span><span class="red">*</span> 库存下限：</span>
	    <input type="number" value="${buyProductSku.minStock}" name="minStock">
	  </div>
	  <div>
	    <span><span class="red">*</span> 销售单价：</span>
	    <input type="number" value="${buyProductSku.price}" name="price">
	  </div>
	  <div>
		<span><span class="red">*</span> 销售天数：</span> 
		<input type="number" name="planSalesDays" value="${buyProductSku.planSalesDays}" min="0">
	  </div>
	  <input type="hidden" value="${buyProductSku.id}" name="id">
	</div>
</form>
