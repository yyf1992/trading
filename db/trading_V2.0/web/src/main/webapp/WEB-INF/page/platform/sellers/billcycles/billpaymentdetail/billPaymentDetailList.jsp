<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单我的收款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billpaymentdetail/billPaymentDetailList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	//$(function(){
    var form;
    layui.use('form', function() {
        form = layui.form;
        form.render("select");
	    //预加载条件
        $("#queryBillRecId").val("${params.billRecId}");
        $("#buyCompanyName").val("${params.buyCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#paymentStatus").val("${params.paymentStatus}");
		//重新渲染select控件
		/*var form = layui.form;
		form.render("select"); */
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		loadDate("startDate","endDate");
	});

    //标签页改变
    function setStatus(obj,status) {
        $("#paymentStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }
    //重置查询条件
    function resetformData(){
       // alert("进来了吗");
        $("#queryBillRecId").val("");
        $("#buyCompanyName").val("");
        $("#createBillUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
        $("#paymentStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>

<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.paymentStatus eq '0'}">class="hover"</c:if>>所有（<span>${params.approvalBillReconciliationCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.paymentStatus eq '1'}">class="hover"</c:if>>收款待确认（<span>${params.acceptBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.paymentStatus eq '2'}">class="hover"</c:if>>收款已确认（<span>${params.apprEndBillReconciliationCount}</span>）</a> <b>|</b>
</div>
<!--内容-->
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/seller/billPaymentDetail/billPaymentDetailList">
		<ul class="order_search">
			<li  class="comm">
				<label>对账单号:</label>
				<input type="text" placeholder="输入结算单号" id="queryBillRecId" name="billRecId" />
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="comm">
				<label>客户名称:</label>
				<input type="text" placeholder="请输入客户名称" id="buyCompanyName" name="buyCompanyName" >
			</li>
			<li class="range">
				<label>应付款总额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
			</li>
			<li class="range nomargin">
				<label>账单日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date"  class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range nomargin">
				<label>收款确认状态:</label>
				<div class="layui-input-inline">
					<select name="paymentStatus" id="paymentStatus" lay-filter="aihao">
						<option value="0" >所有</option>
						<option value="1" >收款待确认</option>
						<option value="2" >收款已确认</option>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />
				<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small rt" onclick="sellerPaymentExport();">
					<i class="layui-icon">&#xe8bf;</i> 导出
				</a>
		</ul>
	</form>
    <table class="table_pure payment_list">
	  <thead>
	  <tr>
		  <td style="width:15%">对账单号</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">客户名称</td>
		  <td style="width:10%">应收款总额</td>
		  <td style="width:10%">已收款金额</td>
		  <td style="width:10%">应收款余额</td>
		  <td style="width:10%">出账开始日期</td>
		  <td style="width:10%">出账截止日期</td>
		  <td style="width:10%">确认状态</td>
		  <td style="width:10%">操作</td>
	  </tr>
	  </thead>
	  <tbody>
	    <c:forEach var="billPayment" items="${searchPageUtil.page.list}">
		  <tr class="breakType">
			  <td style="text-align: center" title="${billPayment.id}">
			    ${billPayment.id}</td>
			  <td>${billPayment.createBillUserName}</td>
			  <td>${billPayment.buyCompanyName}</td>
			  <td>${billPayment.totalAmount}</td>
			  <td>${billPayment.actualPaymentAmount}</td>
			  <td>${billPayment.residualPaymentAmount}</td>
			  <%--<td>${billPayment.id}</td>--%>
			  <td>${billPayment.startBillStatementDateStr}</td>
			  <td>${billPayment.endBillStatementDateStr}</td>
			  <td style="text-align: center">
				  <c:choose>
					  <c:when test="${billPayment.paymentStatus==1}">收款待确认</c:when>
					  <c:when test="${billPayment.paymentStatus==2}">收款已确认</c:when>
				  </c:choose>

			  </td>
			  <td>
				  <div>
				  <c:choose>
					  <c:when test="${billPayment.paymentStatus==1}">
					<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showPaymentAgree('${billPayment.id}');">收款确认</span>
					  </c:when>
				  </c:choose>
				  </div>
				  <div>
					  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/billPaymentDetail/showPaymentInfo?reconciliationId=${billPayment.id}','buyer')" class="layui-btn layui-btn-mini">收款详情</a>
				  </div>
				  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/billPaymentDetail/showRecItemListNew?reconciliationId=${billPayment.id}','buyer')" class="layui-btn layui-btn-normal layui-btn-mini">账单详情</a>
			  </td>
		  </tr>
	    </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
</div>

<!--确认付款-->
<!--结算周期审批-->
<div class="plain_frame bill_exam" style="display:none;" id="billApproval">
	<form action="">
		<ul>
			<li>
				<span>审批意见:</span>
				<label><input id="agree" type="radio" name="agreeStatus" value="2"> 确认收款</label>&nbsp;&nbsp;
				<%--<label><input id="reject" type="radio" name="agreeStatus" value="4"> 驳回</label>--%>
			</li>
			<li>
				<span>填写备注:</span>
				<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
			</li>
		</ul>
	</form>
</div>