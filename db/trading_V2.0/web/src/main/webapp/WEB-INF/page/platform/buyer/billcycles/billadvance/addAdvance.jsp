<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增账单周期</title>
    <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billadvance/addAdvance.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/bill.css">
</head>
<div id="addAdvanceDiv">
  <div>
    <form class="layui-form">

    <div class="layui-inline">
        <label class="labelStyle">供应商:</label>
        <div class="layui-input-inline" style="width:200px" id="sellerCompanyDiv">
        </div>
    </div>
    <div class="bill_cycle">
      <label class="labelStyle">采购员:</label>
        <div class="layui-input-inline" style="width:200px" id="buyUserDiv">
        </div>
    </div>
    <%--<div class="bill_cycle">
        <label class="labelStyle">预付款可用金额:</label>
        <div class="layui-input-inline">
            <input type="number" min="0" name="usableTotal" id="usableTotal" value="${params.usableTotal}" style="width: 200px;height: 30px;" class="layui-input" placeholder="预付款金额">
        </div>
    </div>
    <div class="bill_cycle">
        <label class="labelStyle">预付款凭证:</label>
        &lt;%&ndash;<div class="upload_license" style='width: 150px;height: 20px'>&ndash;%&gt;
            <input type="file" multiple name="paymentFileAddr" id="paymentFileAddr" onchange="uploadPayment();" style="width: 70px;height: 23px;" >
        &lt;%&ndash;</div>&ndash;%&gt;
        <p></p>
        <span class="labelStyle" style="white-space: nowrap">仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
        <div class="scanning_copy original" id="paymentFileDiv"></div>
    </div>--%>
    <div class="bill_cycle">
        <label class="labelStyle">备注:</label>
        <textarea type="text" id="taskRemarks" name="taskRemarks" style="width: 300px;height: 100px;" class="layui-input"></textarea>
    </div>
      </form>
      <style>
          .labelStyle{float:left;display:block;padding:9px 15px;width:200px;font-weight:400;text-align:right}
      </style>
    </div>
    
  <div class="text-center mp30">
    <a href="javascript:void(0);">
    <button class="layui-btn layui-btn-normal layui-btn-small" id="billCycleInfo_add">确认提交</button>
    <span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/buyer/billAdvance/billAdvanceList','buyer');">返回列表</span>
    </a>
  </div>
</div>
 
