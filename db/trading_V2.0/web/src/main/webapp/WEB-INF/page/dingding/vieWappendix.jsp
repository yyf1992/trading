<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>诺泰买卖系统</title>
  <%@ include file="./../platform/common/common.jsp"%>
  <script src="<%=basePath%>statics/dingding/js/base64.js"></script>
</head>
<body>
<script>
$(function(){
	layer.msg('加载中', {icon: 16,shade: 0.01});
	var str = "${url}";
	//解密
	var b = new Base64();
    var url = b.decode(str);
    $.ajax({
    	url:"<%=basePath%>"+url,
    	success:function(data){
    		layer.closeAll();
    		$("#wappendixDiv").html(data);
    		$("table").colResizable();
    	},
    	error:function(){
    		layer.closeAll();
    		layer.msg("获取数据失败，请稍后重试！",{icon:2});
    	}
    });
});
</script>
<body>
	<div id="wappendixDiv" style="margin: 40px;"></div>
</body>