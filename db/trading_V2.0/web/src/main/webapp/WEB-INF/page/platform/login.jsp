<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
	<meta http-equiv="content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, 
		maximum-scale=1, minimum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta name="keywords" content="诺泰,买卖系统,买卖">
	<title>买卖系统</title>
    <%@ include file="common/path.jsp"%>
	<script src="<%=basePath%>statics/platform/js/jquery-1.8.3.min.js"></script>
	<script src="<%=basePath%>statics/plugins/layui/layui.js"></script>
	<script src="<%=basePath%>statics/platform/js/system.js"></script>
	<script src="<%=basePath%>statics/platform/js/colResizable-1.6.min.js"></script>
	<script src="<%=basePath%>js/common.js"></script>
	<script src="<%=basePath%>statics/platform/js/common.js"></script>
	<script src="<%=basePath%>statics/platform/login.js"></script>

	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/login.css">
	<link rel="stylesheet" href="<%=basePath%>statics/plugins/layui/css/layui.css">
<script>
	if (window.top !== window.self) {
		window.top.location = window.location;
	}
</script>
</head>
<body>
	<div class="login1">
		<img src="<%=basePath%>statics/platform/images/login_logo.png" /><br/>
		<img src="<%=basePath%>statics/platform/images/title.png" />
	</div>
	<div class="login">
		<div>
			<p>输入账号密码登录</p>
			<form id="login-form">
				<input type="text" placeholder="请输入用户名" name="loginName" id="loginName" />
					<input type="password" placeholder="请输入用户密码" name="password" id="password" />
						<div class="forgot">
							<input type="checkbox"> 记住用户名 <a href="#">忘记密码？</a>
						</div>
						<input type="button" id="bt-login" onclick="toLogin();" value="登录"/>
			</form>
		</div>
	</div>
	<div class="login2">
		<a href="#" id="btn">登录</a> <a href="<%=basePath%>platform/reg/toRegisterPage" >注册</a>
	</div>
</body>
</html>
