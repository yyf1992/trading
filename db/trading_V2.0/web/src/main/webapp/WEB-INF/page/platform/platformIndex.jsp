<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="keywords" content="诺泰，诺泰买卖，买卖系统，nuotai">
  <meta name="description" content=" 诺泰买卖系统是用于商家向供应商采购商品。">
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>买卖</title>
  <%@ include file="common/common.jsp"%>
  <link rel="stylesheet" href="${basePath}/statics/platform/css/verify.css">
  <script src="${staticsPath}/platform/js/verify.js"></script>
</head>
<script>
$(function(){
	//loadMenu();
	$(".indexLeftNav li").click(function(){
		$(".indexLeftNav li").removeClass("on");
		$(this).addClass("on");
		var className=$(this).attr("class");
		if("indexLeft02 on"==className){
				$.ajax({
					url : basePath+"platform/common/titleMenuClick",
					data:{
						"menuId":"17100919541427382982"
					},
					async:false,
					success:function(data){
						var data = {
							"url":"platform/sysUser/updatePassword",
							"type":"system"
						};
						var b = new Base64();
			    		var str = b.encode(JSON.stringify(data));
						window.location.href = basePath+"platform/sysUser/systemIndex?jump="+str;
						return;
					},
					error:function(){
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		}else if("indexLeft03 on"==className){
			window.open("platform/loadOperationGuide");
		}
	});
	homePage();
});
function loadMenu(){
	layer.load();
	$.ajax({
		url : basePath+"platform/common/loadMenu",
		success:function(data){
			layer.closeAll('loading');
		},
		error:function(){
			layer.closeAll('loading');
		}
	});
}

function verityThis(id){
	$.ajax({
		url:basePath+"platform/tradeVerify/verifyDetail",
		data:{"id":id},
		success:function(data){
			if($(".verifyDetail").length > 0){
				$(".verifyDetail").remove();
			}
			var detailDiv = $("<div style='padding:0px'></div>");
			detailDiv.addClass("verifyDetail");
			detailDiv.html(data);
			var inputObj = $("<input type='hidden' name='index' value='index'>");
			detailDiv.append(inputObj);
			detailDiv.appendTo($("body"));
			detailDiv.find("#verify_side").addClass("show");
		},
		error:function(){
			layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
		}
	});
}

function viewAll(){
	$.ajax({
		url : basePath+"platform/common/titleMenuClick",
		data:{
			"menuId":"17070718122379536910"
		},
		async:false,
		success:function(data){
			var data = {
				"url":"platform/tradeVerify/waitForMeList",
				"type":"baseInfo",
				"id":"17082811062290245539"
			};
			var b = new Base64();
    		var str = b.encode(JSON.stringify(data));
			window.location.href = basePath+"platform/common/baseInfoIndexHtml?jump="+str;
			return;
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

function homePage(){
	layer.load();
	$.ajax({
		url : basePath+"platform/homePage",
		success:function(data){
			$(".right_index").empty();
			$(".right_index").html(data);
			layer.closeAll('loading');
		},
		error:function(){
			layer.closeAll('loading');
		}
	});
}
</script>
<body>
<!-- 顶部 -->
<%@ include file="common/top.jsp"%>
<!-- 顶部菜单 -->
<jsp:include page="common/title.jsp" >
	<jsp:param name="menuId" value="1" />
</jsp:include>
<div class="container">
  <!--二、主体-->
  <section class="section">
  <div class="content">
  <!--左侧导航-->
    <div class="left_index">
      <div class="text-center">
        <div class="indexPhoto">
          <img src="${staticsPath}/platform/images/indexPhoto.png">
          <div class="indexPhotoBg"><a href="系统-个人资料.html"><span class="fa fa-pencil-square-o"></span> 完善个人资料</a></div>
        </div>
        <p class="mp30 indexUser">用户名：${user.loginName }</p>
        <div class="indexPosition mp30">
        	<c:choose>
        		<c:when test="${userRole.is_admin==1}">
        			<c:out value="管理员" default="暂无角色信息"/>
        		</c:when>
        		<c:otherwise><c:out value="${userRole.role_name}" default="暂无角色信息"/></c:otherwise>
        	</c:choose>
        </div>
        <div class="indexCompany mp">
          <b></b>
          ${user.companyName }
        </div>
      </div>
      <ul class="mp30 indexLeftNav">
        <li class="indexLeft01 on">
          <b></b>我的待审批事项
        </li>
        <li class="indexLeft02">
          <b></b>修改密码
        </li>
        <li class="indexLeft03">
          <b></b>用户操作指南
        </li>
      </ul>
    </div>
    <!--二、算账右侧详情-->
    <div class="right_index">
      <!--内容-->
      <div class="">
        <div class="indexRtTitle page_title">我的待审批事项
          <span class="layui-badge layui-bg-blue"><i class="layui-icon" style="font-size: 12px;">&#xe60e;</i> 待审批</span>
          <span class="layui-badge layui-bg-blue rt refresh" title="刷新" onclick="window.location.reload();"><i class="layui-icon" style="font-size: 12px;">&#xeac2;</i></span>
        </div>
        <div class="pending mp mt">
          <div class="pendingCount">
            <p>您当前有 <b class="red size_lg">${searchPageUtil.page.count}</b> 个待审批事项</p>
            <p class="text-right">
              <a href="javascript:void(0)" onclick="viewAll();" class="approval">查看全部 ></a>
            </p>
          </div>
          <div class="pendingItem mp">
          <c:forEach var="verifyHeader" items="${searchPageUtil.page.list}" begin="0" end="5" step="1">
	             <div data-method="offset" data-type="rt" title="审批" onclick="verityThis('${verifyHeader.id}');">
	               	   <img src="${staticsPath}/platform/images/approving.png">
		               <p class="pendItemTitle">
		                ${verifyHeader.title}
		                <span class="rt size_sm">${verifyHeader.start_date}</span>
	               	</p>
	               	<p class="pendItemNum">审批编号：${verifyHeader.id}</p>
	              	 <p class="pendItemName">申 请 人：${verifyHeader.create_name}</p>
              	 </div>
			</c:forEach>
        	</div>
        </div>
        <div class="indexRtTitle page_title mt">买卖订单业务流程概览展示
          <span class="layui-badge layui-bg-green"><i class="layui-icon" style="font-size: 12px;">&#xe641;</i> 流程图</span>
        </div>
        <img src="${staticsPath}/platform/images/indexFlow.png">
      </div>
      </div>
    </div>
  </section>
  <!--侧边栏-->
</div>
<!--返回顶部按钮-->
<div class="toTop">
  <a href="#"><img src="<%=basePath%>/statics/platform/images/sidebar_4.png"></a>
</div>
</body>
</html>