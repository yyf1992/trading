<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/billReconciliationList.js"></script>
<script type="text/javascript" src="<%=basePath%>/statics/platform/js/common.js"></script>
<script type="text/javascript">
    $(function(){
        if ('${buyBillReconciliation.billDealStatus}'<7){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "待内部审批"
                },{
                    title: "对账已完成"
                }/*,{
                    title: "内部审批驳回"
                },{
                    title: "对方审批驳回"
                }*/]
            });
            
            var step = parseInt('${buyBillReconciliation.billDealStatus}')+4;
            $(".ystep1").setStep(step);
        }
    });

    //账单附件回显
    function openBillFile(reconciliationId,createBillUserName,startDateStr,endDateStr) {
        showBillFiles(reconciliationId);
        layer.open({
            type: 1,
            title: createBillUserName+''+startDateStr+'-'+endDateStr+'周期账单附件',
            area: ['900px', 'auto'],
            skin:'popBuyer',
            //btns :2,
            closeBtn:2,
            btn:['关闭窗口'],
            content: $('#showBillFileList'),
            yes : function(index){
                layer.close(index);
            }
        });
    }

    //账单附件信息查询
    function showBillFiles(reconciliationId) {
        $("#billRecFileBody").empty();
        // $("#showFiles").empty();
        $.ajax({
            url : basePath+"platform/buyer/billReconciliation/queryBillFileList",
            data:  {
                "reconciliationId":reconciliationId
            },
            async:false,
            success:function(data){
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				/*var createUserId = resultObj.createUserId;
				 var createUserName = resultObj.createUserName;
				 var deliveryItemList = resultObj.deliveryItemList;*/
                $.each(resultObj,function(i,o){
                    //创建人编号
                    var createUserId = o.createUserId;
                    //上传人名称
                    var createUserName = o.createUserName;
                    //创建时间
                    var createTime = o.createTime;
                    //附件路径
                    var filesAddress = o.filesAddress;
                    //备注
                    var remarks = o.remarks;

                    var testTitle = "查看票据";
                    var trObj = $("<tr>");
                    $("<td>"+createUserName+"</td>").appendTo(trObj);
                    $("<td>"+createTime+"</td>").appendTo(trObj);
                    $("<td>"+remarks+"</td>").appendTo(trObj);
                    $("<td> <span class='layui-btn layui-btn-mini layui-btn-warm' onclick=\"showBillRecFileList('"+filesAddress+"','recFile');\">"+testTitle+"</span></td>").appendTo(trObj);

                    $("#billRecFileBody").append(trObj);
                });
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }

    //附件显示隐藏
    var hideOrShow = true;
    function showBillRecFileList(receiptAddr,fileType) {
        if(receiptAddr != null || receiptAddr != ''){
            var bigImgShow = "";
            var smallImg = "";
            var showOrHidden = "";
            if(fileType == "custom"){
                $("#showCustomBigImg").empty();
                $("#fileCustomList").empty();
                bigImgShow = "#showCustomBigImg";
                smallImg = "#fileCustomList";
                showOrHidden = "#showCustomFiles";
            }else if(fileType == "recFile"){
                $("#showBigImg").empty();
                $("#fileList").empty();
                bigImgShow = "#showBigImg";
                smallImg = "#fileList";
                showOrHidden = "#showFiles";
            }
            var receiptAddrList = receiptAddr.split(',');
            var bigImg = '';
            for(i = 0;i<receiptAddrList.length;i++){
                var liObj = $("<li>");
                $("<img src="+receiptAddrList[i]+"></img>").appendTo(liObj);
                $(smallImg).append(liObj);
                bigImg = receiptAddrList[0];
            }
            //大图
            $(bigImgShow).append($("<img id='bigImg' src='"+bigImg+"'></img>"));

            if(hideOrShow){
                $(showOrHidden).show();
                hideOrShow = false;
            }else {
                $(showOrHidden).hide();
                hideOrShow = true
            }
        }
    }
</script>
<%--<div >--%>
	<div class="order_top mt">
		<div class="lf order_status" >
			<h4 class="mt">当前订单状态</h4>
			<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
			  <%--<c:when test="${buyBillReconciliation.billDealStatus==5}">采购待审批</c:when>--%>
			  <c:when test="${buyBillReconciliation.billDealStatus==6}">审批通过</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
			  <%--<c:when test="${buyBillReconciliation.billDealStatus==7}">采购部驳回</c:when>--%>
			  <c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
		  </c:choose>
        </span>
			</p>
			<p class="order_remarks text-center"></p>
		</div>
		<div class="lf order_progress">
			<div class="ystep1"></div>
		</div>
	</div>
	<div>
		<h4 class="mt">账单对账信息</h4>
		<div class="order_d">
			<p class="line mt"></p>
			<p><span class="order_title">供应商信息</span></p>
			<table class="table_info">
				<tr>
					<td>供应商名称：<span>${buyBillReconciliation.sellerCompanyName}</span></td>
					<%--<td>负责人：<span>萌萌</span></td>
					<td>手机号：<span>12234325</span></td>--%>
				</tr>
			</table>

			<p class="line mt"></p>
			<p><span class="order_title">账单对账信息</span></p>
			<table class="table_info">
			  <thead>
			   <tr>
				<td style="width:15%">账单周期</td>
				<td style="width:10%">采购员</td>
				<td style="width:10%">到货总数量</td>
				<td style="width:10%">到货总金额</td>
				<td style="width:10%">售后总数量</td>
				<td style="width:10%">售后总金额</td>


			   <td style="width:10%">运费</td>
			   <td style="width:10%">奖惩总金额</td>
			   <td style="width:10%">预付款抵扣金额</td>
			   <td style="width:10%">账期总金额</td>
			   <td style="width:10%">审批状态</td>
				<%--<td style="width:10%">备注</td>--%>
			   <td style="width:10%">付款凭证</td>
			   </tr>
			  </thead>
			  <tbody>
			   <tr>
				   <td>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</td>
				   <td>${buyBillReconciliation.createBillUserName}</td>
				   <td>${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</td>
				   <td>${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</td>
				   <td>${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</td>
				   <td>${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</td>
				   <td>${buyBillReconciliation.freightSumCount}</td>
				  <%-- <td>${buyBillReconciliation.customAmount}</td>
				   <td>${buyBillReconciliation.advanceDeductTotal}</td>--%>
				   <td>
					    <span class="layui-btn layui-btn-mini" onclick="showCustomPayment('${buyBillReconciliation.id}');">
							${buyBillReconciliation.customAmount}</span>
				   </td>
				   <td>
					    <span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.sellerCompanyName}','${buyBillReconciliation.billDealStatus}');">
							${buyBillReconciliation.advanceDeductTotal}</span>
				   </td>
				   <td>${buyBillReconciliation.totalAmount}</td>
				   <td>
					   <c:choose>
						   <c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
						   <%--<c:when test="${buyBillReconciliation.billDealStatus==5}">采购待审批</c:when>--%>
						   <c:when test="${buyBillReconciliation.billDealStatus==6}">财务待审批</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
						   <%--<c:when test="${buyBillReconciliation.billDealStatus==7}">采购部驳回</c:when>--%>
						   <c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
						   <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
					   </c:choose>
				   </td>
				   <td>
					   <a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
							   '${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
							   '${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
				   </td>
			   </tr>
			  </tbody>

			</table>

		</div>
		<!--收款凭据-->
		<!--回显账单附件-->
		<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillFileList">
			<div>
				<table class="table_yellow">
					<thead>
					<tr>
						<td style="width:20%">上传人</td>
						<td style="width:20%">上传时间</td>
						<td style="width:20%">上传备注</td>
						<td style="width:20%">上传单据</td>

					</tr>
					</thead>
					<tbody id="billRecFileBody">
					</tbody>
				</table>
			</div>
			<!--收款凭据-->
			<div id="showFiles" class="receipt_content" style="display:none;">
				<div id="showBigImg" class="big_img">
					<%--<img id="bigImg">--%>
				</div>
				<div class="view">
					<a href="#" class="backward_disabled"></a>
					<a href="#" class="forward"></a>
					<ul id="fileList" class="icon_list"></ul>
				</div>
			</div>
		</div>

		<!--回显自定义付款详情-->
		<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillCustomList">
			<div>
				<table class="table_yellow">
					<thead>
					<tr>
						<td style="width:10%">添加人</td>
						<td style="width:10%">添加时间</td>
						<td style="width:10%">奖惩类型</td>
						<td style="width:10%">奖惩金额</td>
						<td style="width:10%">奖惩添加备注</td>
						<td style="width:10%">审批状态</td>
						<td style="width:10%">审批人</td>
						<td style="width:10%">审批时间</td>
						<td style="width:10%">奖惩审批备注</td>
						<td style="width:10%">奖惩单据</td>
					</tr>
					</thead>
					<tbody id="billRecCustomFileBody">
					</tbody>
				</table>
			</div>
			<!--自定义付款款凭据-->
			<div id="showCustomFiles" class="receipt_content" style="display:none;">
				<div id="showCustomBigImg" class="big_img">
					<%--<img id="bigImg">--%>
				</div>
				<div class="view">
					<a href="#" class="backward_disabled"></a>
					<a href="#" class="forward"></a>
					<ul id="fileCustomList" class="icon_list"></ul>
				</div>
			</div>
		</div>

		<!--预付款抵扣详情-->
		<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showAdvanceEditList">
			<div>
				<table class="table_yellow">
					<thead>
					<tr>
						<td style="width:10%">供应商</td>
						<td style="width:10%">采购员</td>
						<td style="width:10%">添加人</td>
						<td style="width:10%">添加时间</td>
						<td style="width:10%">预付款抵扣金额</td>
					</tr>
					</thead>
					<tbody id="billAdvanceEditBody">
					</tbody>
				</table>
			</div>
		</div>
	</div>
<%--
</div>--%>
