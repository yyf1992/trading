<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
Date.prototype.format = function(fmt) { 
     var o = { 
        "M+" : this.getMonth()+1,                 //月份 
        "d+" : this.getDate(),                    //日 
        "h+" : this.getHours(),                   //小时 
        "m+" : this.getMinutes(),                 //分 
        "s+" : this.getSeconds(),                 //秒 
        "q+" : Math.floor((this.getMonth()+3)/3), //季度 
        "S"  : this.getMilliseconds()             //毫秒 
    }; 
    if(/(y+)/.test(fmt)) {
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
    }
	for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)){
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
		}
	}
    return fmt; 
}
function getDate(date){
	if(date != null && date != ''){
		var oldDate = (new Date(date)).getTime();
	    var curDate = new Date(oldDate).format("yyyy-MM-dd");
	    return curDate;
	}else{
		return "";
	}
}  
</script>
<script type="text/html" id="stimeTpl2">
	{{  getDate(d.sale_start_date) }} 
</script>
<script type="text/html" id="etimeTpl2">
	{{  getDate(d.sale_end_date) }} 
</script>
<script type="text/html" id="predictSalesNumTpl2">
	<input type="number" name="predictSalesNum" value="{{d.predict_sales_num}}" min="0" style="width:80px">
</script>
<!-- 材料计划 -->
<div class="demoTable2">
  	采购单号：
	<div class="layui-inline">
		<input class="layui-input" name="applyCode" id="applyCode" autocomplete="off">
	</div>
	产品名称：
	<div class="layui-inline">
		<input class="layui-input" name="productName" id="productName" autocomplete="off" >
	</div>
	条形码：
	<div class="layui-inline">
		<input class="layui-input" name="barcode" id="barcode" autocomplete="off" >
	</div>
  <button class="layui-btn" data-type="reload">搜索</button>
</div>
<table class="layui-hide" id="materialProductTable" lay-filter="user"></table> 
<script>
layui.use('table', function(){
  var table = layui.table;
  
  //方法级渲染
  table.render({
    elem: '#materialProductTable'
    ,url: '<%=basePath %>buyer/applyPurchaseHeader/loadDataJson?productType=1'
    ,cols: [[
      {checkbox: true,rowspan: 2}
      ,{field:'apply_code', title: '采购单号', show:true,rowspan: 2}
      ,{field:'create_date', title: '计划时间',show:true,rowspan: 2}
      ,{field:'product_name', title: '产品名称',show:true,rowspan: 2}
      ,{field:'barcode', title: '条形码',show:true,rowspan: 2}
      ,{field:'purchaseNum', title: '采购计划数',show:true,rowspan: 2}
      ,{field:'unit_name', title: '单位',show:true,rowspan: 2}
      ,{field:'cost', title: '单价',show:true,rowspan: 2}
      ,{field:'price', title: '金额',show:true,rowspan: 2}
      ,{field:'requiredTime', title: '需求时间',show:true,rowspan: 2}
      ,{field:'', title: '生产需求数',show:true,rowspan: 2}
      ,{field:'transitNum', title: '在途数量',show:true,rowspan: 2}
      ,{field:'', title: '护具原材料仓库存',show:true,align:'center',colspan: 2}
      ,{field:'stockTotal', title: '合计库存总数',show:true,rowspan: 2}
      ,{field:'safeStock', title: '安全库存数',show:true,rowspan: 2}
      ,{field:'stockUsable', title: '可用库存数',show:true,rowspan: 2}
      ,{field:'create_name', title: '计划专员',show:true,rowspan: 2}
    ],[
    	{field:'stockZZC', title: '质检仓',show:true}
    	,{field:'stockDC', title: '大仓',show:true}
    ]]
    ,id: 'testReload2'
    ,page: true
    ,height: 255
    ,cellMinWidth : 80
    ,height:'full-90'
    ,limit:20
    ,total:true
  });
  
  var $ = layui.$, active = {
    reload: function(){
      
      //执行重载
      table.reload('testReload2', {
        page: {
          curr: 1 //重新从第 1 页开始
        }
        ,where: {
			applyCode : $("#applyCode").val(),
			productName :  $("#productName").val(),
			barcode :  $("#barcode").val()
        }
      });
    }
  };
  
  $('.demoTable2 .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
});
</script>