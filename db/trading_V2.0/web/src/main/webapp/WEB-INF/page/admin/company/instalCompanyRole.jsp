<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
loadLeftRole();
loadRightRole();
//加载左侧页面
function loadLeftRole(){
	$.ajax({
		url : "admin/sysRoleCompany/loadLeftRoleList.html",
		data:{
			"pageAdmin.divId"    : "leftRoleDiv",
			"pageAdmin.pageSize" : 15,
			"companyId"        : $("#instalCompanyIdHidden").val()
		},
		async:false,
		success:function(data){
			var str = data.toString();
			$("#leftRoleDiv").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//加载右侧页面
function loadRightRole(){
	$.ajax({
		url : "admin/sysRoleCompany/loadRightRoleList.html",
		data:{
			"pageAdmin.divId"    : "rightRoleDiv",
			"pageAdmin.pageSize" : 15,
			"companyId"        : $("#instalCompanyIdHidden").val()
		},
		async:false,
		success:function(data){
			var str = data.toString();
			$("#rightRoleDiv").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//添加
function addCompanyToRole(){
	if($("#leftRoleDiv input:checkbox[name='checkone']:checked").length < 1){
		layer.msg("左侧角色至少选中一项！",{icon:7});
		return;
	}
	var roleIds = "";
	$("#leftRoleDiv input:checkbox[name='checkone']:checked").each(function(){
		roleIds += $(this).val() + ",";
	});
	$.ajax({
		url : "admin/sysRoleCompany/addCompanyRole",
		type: "POST",
		data:{
			"roleIds" : roleIds,
			"companyId"  : $("#instalCompanyIdHidden").val()
		},
		async:false,
		success:function(data){
			var result=eval('('+data+')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			layer.msg(resultObj.msg,{icon:7});
			loadLeftRole();
			loadRightRole();
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//删除
function deleteRoleCompany(){
	if($("#rightRoleDiv input:checkbox[name='checkone']:checked").length < 1){
		layer.msg("右侧角色至少选中一项！",{icon:7});
		return;
	}
	var roleUserIds = "";
	$("#rightRoleDiv input:checkbox[name='checkone']:checked").each(function(){
		roleUserIds += $(this).val() + ",";
	});
	$.ajax({
		url : "admin/sysRoleCompany/deleteRoleCompany",
		type: "POST",
		data:{
			"roleUserIds" : roleUserIds
		},
		async:false,
		success:function(data){
			var result=eval('('+data+')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			layer.msg(resultObj.msg,{icon:7});
			loadLeftRole();
			loadRightRole();
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">设置公司角色</h4>
		</div>
		<div class="modal-body row-fluid ">
			<div class="modal-two clearfix">
				<!-- 左侧人员 -->
				<div class="modal-two-left pull-left" id="leftRoleDiv"></div>
				<div class="modal-two-middle text-center">
					<div class="btn btn-sm2-wrapper">
						<button class="btn btn-sm2 btn-info" onclick="addCompanyToRole();">添加</button>
					</div>
					<div class="btn btn-sm2-wrapper">
						<button class="btn btn-sm2 btn-warning margin-top-sm" onclick="deleteRoleCompany();">删除</button>
					</div>
				</div>
				<!-- 右侧人员 -->
				<div class="modal-two-right pull-right" id="rightRoleDiv"></div>
			</div>
		</div>
		<div class="modal-footer">
			<!-- <button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
			<button type="button" class="btn btn-sm2 btn-00967b" id="saveButton">保存</button> -->
		</div>
	</div>
	<!--隐藏域  -->
	<input type="hidden" name="id" value="${company.id}" id="instalCompanyIdHidden">