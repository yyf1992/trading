<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>手工添加订单</title>
<link rel="stylesheet" href="css/layui/css/layui.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/purchase.css">

<script type="text/javascript">
//加载采购商
$(function(){
	
	loadSupplier("${orderData.sellerId}");
	//重新渲染select控件
	var form = layui.form;
	form.render("select");
	//给select添加监听事件
	form.on("select(changeSeller)", function(data){
		var selectValue = data.value;
		if(selectValue==''){
			$("input[name='contact_person']").val("");
			$("input[name='clientPhone']").val("");
		}else{
			$("input[name='contact_person']").val(selectValue.split(",")[2]);
			$("input[name='clientPhone']").val(selectValue.split(",")[3]);
		}
		//已选择商品清空
//		$(".table_pure tbody").empty();
//		$("button.tr_add").click();
	});
//	$("input[type='number']").bind("change",function(){
//		validateInput($(this));
//	}).bind("keyup",function(){
//		validateInput($(this));
//	});
});
</script>
</head>
<body>
<div class="content">
    <div>
      <div class="order_success">
        <img src="statics\platform\images\ok.png">
        <h3>恭喜您添加订单成功！</h3>
        <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/sellerManualOrderList','sellers');" class="purchase">转到我手工添加的订单</a>
        <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/sellerManualOrderDetails?orderId=${orderId}','sellers');" class="view_seller">查看详情</a>
        <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/salesmanagement/printSellerOrder?orderId=${orderId}','sellers');" target="_blank" class="print_seller">打印销售单</a>
      </div>
    </div>
</div>
</body>
</html>
