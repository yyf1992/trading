<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/billReconciliationList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%--<script src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#reconciliationId").val("${params.reconciliationId}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#billDealStatus").val("${params.billDealStatus}");
		//重新渲染select控件
		/*var form = layui.form;
		form.render("select");*/

	});

    //标签页改变
    function setStatus(obj,status) {
		$("#billDealStatus").val(status);

        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
//      $("#searchForm").submit();
    }

    //重置查询条件
    function resetformData(){
        $("#reconciliationId").val("");
    	$("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
        $("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
    	$("#billDealStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.billDealStatus eq '0'}">class="hover"</c:if>>所有</a> <b></b>
	<%--<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.billDealStatus eq '1'}">class="hover"</c:if>>等待对账（<span>${params.approvalBillReconciliationCount}</span>）</a> <b>|</b>--%>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.billDealStatus eq '2'}">class="hover"</c:if>>待确认对账（<span>${params.acceptBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.billDealStatus eq '3'}">class="hover"</c:if>>已确认对账（<span>${params.apprEndBillReconciliationCount}</span>） </a>
	<a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.billDealStatus eq '4'}">class="hover"</c:if>>已驳回对账（<span>${params.approvalBillReconciliationCount}</span>）</a> <b>|</b>
</div>
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billReconciliation/billReconciliationList">
		<%--<div class="search_top mt">
			<input id="sellerCompanyName" type='text' placeholder='输入供应商名称' name="sellerCompanyName" />
			<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			<span class="search_more sBuyer"></span>
		</div>

		<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="buyRecExport();">
			<i class="layui-icon">&#xe7a0;</i> 导出</a>--%>

		<ul class="order_search">
			<li class="ship nomargin">
				<label>对账单号:</label>
				<input id="reconciliationId" type='text' placeholder='输入对账单号' name="reconciliationId" />
			</li>
			<li class="ship nomargin">
				<label>供应商名称:</label>
				<input id="sellerCompanyName" type='text' placeholder='输入供应商名称' name="sellerCompanyName" />
			</li>
			<li class="ship nomargin">
				<label>采购员:</label>
				<input type="text"  placeholder='输入采购员' id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="spec nomargin">
				<label>出账日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="ship nomargin">
				<label>账期总金额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
				<!--<button class="search">搜索</button>-->
			</li>
			<li class="range nomargin">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select id="billDealStatus" name="billDealStatus" lay-filter="aihao">
						<option value="0" >全部</option>
						<%--<option value="1">内部等待对账</option>--%>
						<option value="2" >待确认对账</option>
						<option value="3" >已确认对账</option>
						<option value="4" >已驳回对账</option>
						<option value="5" >内部待审批</option>
						<%--<option value="6" >财务待审批</option>--%>
						<option value="7" >内部已驳回</option>
						<%--<option value="8" >财务部驳回</option>--%>
						<option value="9">奖惩待确认</option>
						<option value="10">奖惩已确认</option>
						<option value="11">奖惩已驳回</option>
						<%--<option value="5">供应商对账驳回</option>--%>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="buyRecExport();">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
	</form>
		<!--列表区-->
		<table class="table_pure interwork_list">
			<thead>
			<tr>
				<%--<td style="width: 8%">账单开始日期</td>
				<td style="width: 8%">账单截止日期</td>--%>
				<td style="width: 12%">对账单号</td>
				<td style="width: 8%">账单周期</td>
				<td style="width: 8%">采购员</td>
				<td style="width: 8%">供应商</td>
				<td style="width: 6%">采购总数量</td>
				<td style="width: 8%">采购总金额</td>
				<%--<td style="width: 8%">采购换货数量</td>
				<td style="width: 8%">采购换货金额</td>--%>
				<td style="width: 8%">售后总数量</td>
				<td style="width: 8%">售后总金额</td>
				<%--<td style="width: 6%">售后退货数量</td>
				<td style="width: 8%">售后退货金额</td>--%>
				<td style="width: 8%">运费</td>
				<td style="width: 8%">奖惩金额</td>
				<td style="width: 8%">预付款抵扣金额</td>
				<td style="width: 8%">总金额</td>
				<td style="width: 8%">状态</td>
				<td style="width: 15%">操作</td>

			</tr>
			</thead>
			<tbody>
			  <c:forEach var="buyBillReconciliation" items="${searchPageUtil.page.list}">
				  <tr class="breakType text-center">
					  <%--<td>${buyBillReconciliation.startBillStatementDateStr}</td>
					  <td>${buyBillReconciliation.endBillStatementDateStr}</td>--%>
					  <td>${buyBillReconciliation.id}</td>
					  <td>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</td>
					  <td>${buyBillReconciliation.createBillUserName}</td>
					  <td>${buyBillReconciliation.sellerCompanyName}</td>
					  <td>${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</td>
					  <td>${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</td>
					  <%--<td>${buyBillReconciliation.recordReplaceCount}</td>
					  <td>${buyBillReconciliation.recordReplaceTotal}</td>--%>
					  <td>${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</td>
					  <td>${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</td>
					  <%--<td>${buyBillReconciliation.returnGoodsCount}</td>
					  <td>${buyBillReconciliation.returnGoodsTotal}</td>--%>
					  <td>${buyBillReconciliation.freightSumCount}</td>
					  <td>
					    <span class="layui-btn layui-btn-mini" onclick="showCustomPayment('${buyBillReconciliation.id}');">
								${buyBillReconciliation.customAmount}</span>
					  </td>
					  <td>
					    <span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.sellerCompanyName}','${buyBillReconciliation.billDealStatus}');">
								${buyBillReconciliation.advanceDeductTotal}</span>
					  </td>
					  <td>${buyBillReconciliation.totalAmount}</td>

					  <td>
						  <div>
							  <c:choose>
								 <%-- <c:when test="${buyBillReconciliation.billDealStatus==1}">等待对账</c:when>--%>
								  <c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
								  <%--<c:when test="${buyBillReconciliation.billDealStatus==5}">采购待审批</c:when>--%>
								  <c:when test="${buyBillReconciliation.billDealStatus==6}">财务待审批</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
								  <%--<c:when test="${buyBillReconciliation.billDealStatus==7}">采购部驳回</c:when>--%>
								  <c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
								  <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
							  </c:choose>
						  </div>
						  <div>
						    <a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
							  '${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
							  '${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
						  </div>
						  <div class="opinion_view">
							  <span class="orange" data="${buyBillReconciliation.purchaseId}">审批流程</span>
						  </div>
						  <%--<div>
						    <c:choose>
							  <c:when test="${buyBillReconciliation.billDealStatus != 2}">
								  <span class="orange" onclick="approvalShow('${buyBillReconciliation.purchaseUpdateUserName}','${buyBillReconciliation.purchaseApprovalTimeStr}','${buyBillReconciliation.purchaseApprovalRemarks}','purchase');">采购审批详情</span>
							  </c:when>
						    </c:choose>
						  </div>
						  <div>
							  <c:choose>
								  <c:when test="${buyBillReconciliation.billDealStatus !=2 }">
									  <span class="orange" onclick="approvalShow('${buyBillReconciliation.financeUpdateUserName}','${buyBillReconciliation.financeApprovalTimeStr}','${buyBillReconciliation.financeApprovalRemarks}','finance');">财务审批详情</span>
								  </c:when>
							  </c:choose>
						  </div>--%>
					  </td>
					  <td>
						  <!-- <div> -->
						  <%--<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetails?reconciliationId=${buyBillReconciliation.id}','buyer')" class="approval">对账详情</a>--%>
							   <span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetailsNew?reconciliationId=${buyBillReconciliation.id}&recType=0','buyer')">账单详情</span><br>
						  <c:choose>
							  <c:when test="${buyBillReconciliation.billDealStatus==2 || buyBillReconciliation.billDealStatus==7 || buyBillReconciliation.billDealStatus==10 || buyBillReconciliation.billDealStatus==11}">
								  <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="addBillRecFiles('${buyBillReconciliation.id}',
										  '${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
										  '${buyBillReconciliation.endBillStatementDateStr}');">添加附件</span><br>
								  <span class="layui-btn layui-btn-mini" onclick="customPayment('${buyBillReconciliation.id}');">奖惩设置</span><br>
								  <span class="layui-btn layui-btn-danger layui-btn-mini" onclick="approvalInside('${buyBillReconciliation.id}','${buyBillReconciliation.billDealStatus}','${buyBillReconciliation.purchaseId}');">提交审批</span>
								  <%--<a button="账单审批" href="javascript:approvalMenuClick(this,'${buyBillReconciliation.id}','accept','buyer','18012916331242021979');" class="layui-btn layui-btn-danger layui-btn-mini">
									  <i class="layui-icon">&#xe67d;</i>账单审批</a><br>--%>
                              </c:when>
							  <%--<c:when test="${buyBillReconciliation.billDealStatus==5 || buyBillReconciliation.billDealStatus==8}">
								      <a button="采购审批" href="javascript:approvalMenuClick(this,'${buyBillReconciliation.id}','purchase','buyer','18012916334314721422');" class="layui-btn layui-btn-danger layui-btn-mini">
									  <i class="layui-icon">&#xe67d;</i>采购审批</a><br>
							  </c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==6 }">
								  <a button="财务审批" href="javascript:approvalMenuClick(this,'${buyBillReconciliation.id}','finance','buyer','18012916340227616601');" class="layui-btn layui-btn-danger layui-btn-mini">
									  <i class="layui-icon">&#xe67d;</i>财务审批</a><br>
							  </c:when>--%>
						  </c:choose>
					  </td>
				  </tr>
			  </c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
	<!--收款凭据-->
	<div id="linkReceipt" class="interest" style="display:none;">
		<div id="receiptBody" class="receipt_content">
			<div class="big_img">
				<img src="">
			</div>
			<div class="del_receipt">
				<button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="icon_list"></ul>
			</div>
		</div>
	</div>

	<!--内部选人审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="insideBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="insideAgree" type="radio" name="insideStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="insideReject" type="radio" name="insideStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="insideRemarks" name="insideRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>

	<!--最终审批-->
	<%--<div class="plain_frame bill_exam" style="display:none;" id="acceptBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="acceptAgree" type="radio" name="agreeStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="acceptReject" type="radio" name="agreeStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="acceptRemarks" name="acceptRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>
	<!--内部审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="approvalBillReconciliation">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="purAgree" type="radio" name="approvalStatus" value="pass"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="purReject" type="radio" name="approvalStatus" value="return"> 不同意对账</label>
				</li>
				<li>
					<span>填写备注:</span>
					<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>--%>

	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillRecApproval">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">审批人</td>
					<td style="width:20%">审批时间</td>
					<td style="width:20%">审批备注</td>
				</tr>
				</thead>
				<tbody id="billRecApprovalShow">
				</tbody>
			</table>
		</div>
	</div>

	<!--自定义付款项-->
	<div class="plain_frame price_change" id="customPaymentDiv" style="display:none;height: 400px;overflow-y: auto">
		<ul id="billCustomBody">
		</ul>
	</div>
	<!--回显自定义付款详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillCustomList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">申请人</td>
					<td style="width:10%">申请时间</td>
					<td style="width:10%">类型</td>
					<td style="width:10%">金额</td>
					<td style="width:10%">添加备注</td>
					<td style="width:10%">审批状态</td>
					<td style="width:10%">审批人</td>
					<td style="width:10%">审批时间</td>
					<td style="width:10%">奖惩审批备注</td>
					<td style="width:10%">奖惩单据</td>
				</tr>
				</thead>
				<tbody id="billRecCustomFileBody">
				</tbody>
			</table>
		</div>
		<!--自定义付款款凭据-->
		<div id="showCustomFiles" class="receipt_content" style="display:none;">
			<div id="showCustomBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileCustomList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--添加账单附件-->
	<div class="plain_frame bill_exam" style="display:none;" id="billFiles">
		<ul id="billFilesBody">
		</ul>
	</div>
	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillFileList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">上传人</td>
					<td style="width:20%">上传时间</td>
					<td style="width:20%">上传备注</td>
					<td style="width:20%">上传单据</td>

				</tr>
				</thead>
				<tbody id="billRecFileBody">
				</tbody>
			</table>
		</div>
		<!--收款凭据-->
		<div id="showFiles" class="receipt_content" style="display:none;">
			<div id="showBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--预付款抵扣详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showAdvanceEditList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">供应商</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">申请人</td>
					<td style="width:10%">申请时间</td>
					<td style="width:10%">抵扣金额</td>
				</tr>
				</thead>
				<tbody id="billAdvanceEditBody">
				</tbody>
			</table>
		</div>
	</div>
</div>



<style>
	.view .icon_list {
		height: 92px;
		position: absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
</style>