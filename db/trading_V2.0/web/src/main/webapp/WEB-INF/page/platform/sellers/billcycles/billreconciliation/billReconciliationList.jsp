<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/seller/billreconciliation/billReconciliationList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<script src="<%=basePath%>/statics/platform/js/common.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#reconciliationId").val("${params.reconciliationId}");
        $("#buyCompanyName").val("${params.buyCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#billDealStatus").val("${params.billDealStatus}");
		//重新渲染select控件
        var form;
        layui.use('form', function(){
            form = layui.form;
            form.render("select");
        });
	});

    //标签页改变
    function setStatus(obj,status) {
        $("#billDealStatus").val(status);

        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
//      $("#searchForm").submit();
    }

    //重置查询条件
    function resetformData(){
        $("#reconciliationId").val("");
        $("#buyCompanyName").val("");
        $("#createBillUserName").val("");
        $("#startDate").val("");
        $("#endDate").val("");
        $("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
        $("#billDealStatus").val("0");
    }

	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.billDealStatus eq '0'}">class="hover"</c:if>>所有</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.billDealStatus eq '1'}">class="hover"</c:if>>待发起对账（<span>${params.approvalBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.billDealStatus eq '2'}">class="hover"</c:if>>待确认对账（<span>${params.acceptBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.billDealStatus eq '3'}">class="hover"</c:if>>已确认对账（<span>${params.apprEndBillReconciliationCount}</span>） </a>
	<a href="javascript:void(0);" onclick="setStatus(this,'4')" <c:if test="${searchPageUtil.object.billDealStatus eq '4'}">class="hover"</c:if>>已驳回对账（<span>${params.stopBillReconciliationCount}</span>） </a>
</div>
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/seller/billReconciliation/billReconciliationList">
		<%--<div class="search_top mt">
			<input id="buyCompanyName" type='text' placeholder='输入采购商名称' name="buyCompanyName" />
			<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			<span class="search_more sSeller"></span>
		</div>
		<div class="rt">
		<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="leftMenuClick(this,'platform/seller/billReconciliation/addReconciliationJump','sellers')">
			<i class="layui-icon">&#xebaa;</i>新增账单对账
		</a>
		<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small " onclick="sellerRecExport();">
			<i class="layui-icon">&#xe8bf;</i> 导出
		</a>
		</div>--%>
		<ul class="order_search">
			<li class="ship nomargin">
				<label>对账单号:</label>
				<input id="reconciliationId" type='text' placeholder='输入对账单号' name="reconciliationId" />
			</li>
			<li class="ship nomargin">
				<label>采购商名称:</label>
				<input id="buyCompanyName" type='text' placeholder='输入采购商名称' name="buyCompanyName" />
			</li>
			<li class="ship nomargin">
				<label>采购员:</label>
				<input type="text"  placeholder='输入采购员' id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="spec nomargin">
				<label>出账日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date" class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>

			<li class="ship nomargin">
				<label>总金额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
			</li>

			<li class="range">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select id="billDealStatus" name="billDealStatus" lay-filter="aihao">
						<option value="0">全部</option>
						<option value="1">待发起对账</option>
						<option value="2">待确认对账</option>
						<option value="3">已确认对账</option>
						<option value="4">已驳回对账</option>
						<option value="9">奖惩待确认</option>
						<option value="10">奖惩已确认</option>
						<option value="11">奖惩已驳回</option>
					</select>
				</div>
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
			<div class="rt">
				<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small " onclick="leftMenuClick(this,'platform/seller/billReconciliation/addReconciliationJump','sellers')">
					<i class="layui-icon">&#xebaa;</i>新增账单对账
				</a>
				<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-small " onclick="sellerRecExport();">
					<i class="layui-icon">&#xe8bf;</i> 导出
				</a>
			</div>
	</form>
		<!--列表区-->
		<table class="table_pure payment_list">
			<thead>
			<tr>
				<td style="width: 12%">对账单号</td>
				<td style="width: 8%">账单周期</td>
				<td style="width: 8%">采购员</td>
				<td style="width: 8%">采购商</td>
				<td style="width: 6%">采购总数量</td>
				<td style="width: 8%">采购总金额</td>
				<%--<td style="width: 8%">采购换货数量</td>
				<td style="width: 8%">采购换货金额</td>--%>
				<td style="width: 8%">售后总数量</td>
				<td style="width: 8%">售后总金额</td>
				<%--<td style="width: 6%">售后退货数量</td>
				<td style="width: 8%">售后退货金额</td>--%>
				<td style="width: 8%">运费</td>
				<td style="width: 8%">买家奖惩金额</td>
				<td style="width: 8%">预付款抵扣金额</td>
				<td style="width: 8%">总金额</td>
				<td style="width: 8%">状态</td>
				<td style="width: 10%">审批备注</td>
				<td style="width: 15%">操作</td>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="buyBillReconciliation" items="${searchPageUtil.page.list}">
				<tr class="breakType text-center">
					<td>${buyBillReconciliation.id}</td>
					<td>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</td>
					<%--<td>${buyBillReconciliation.endBillStatementDateStr}</td>--%>
					<td>${buyBillReconciliation.createBillUserName}</td>
					<td>${buyBillReconciliation.buyCompanyName}</td>
					<td>${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</td>
					<td>${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</td>
					<%--<td>${buyBillReconciliation.recordReplaceCount}</td>
					<td>${buyBillReconciliation.recordReplaceTotal}</td>--%>
					<td>${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</td>
					<td>${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</td>
					<%--<td>${buyBillReconciliation.returnGoodsCount}</td>
					<td>${buyBillReconciliation.returnGoodsTotal}</td>--%>
					<td>${buyBillReconciliation.freightSumCount}</td>
					<td>
					    <span class="layui-btn layui-btn-mini" onclick="showBillRecCustom('${buyBillReconciliation.id}');">
								${buyBillReconciliation.customAmount}</span>
					</td>
					<td>
					    <span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.buyCompanyName}','${buyBillReconciliation.billDealStatus}');">
								${buyBillReconciliation.advanceDeductTotal}</span>
					</td>
					<td>${buyBillReconciliation.totalAmount}</td>

					<td>
						<div>
							<c:choose>
								<c:when test="${buyBillReconciliation.billDealStatus==1}">待发起对账</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==2}">待确认对账</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
								<c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
							</c:choose>
							<br/><a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
								'${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
								'${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
						</div>
					</td>
					<td>${buyBillReconciliation.buyerReconciliationRemarks}</td>
					<td>
					  <span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/seller/billReconciliation/getReconciliationDetailsNew?reconciliationId=${buyBillReconciliation.id}','sellers')">账单详情</span><br>

					  <c:choose>
						<c:when test="${buyBillReconciliation.billDealStatus==1}">
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="addBillRecFiles('${buyBillReconciliation.id}',
									'${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
									'${buyBillReconciliation.endBillStatementDateStr}');">添加附件</span><br>
							<span class="layui-btn layui-btn-danger layui-btn-mini" onclick="launchReconciliation('${buyBillReconciliation.id}');">发起对账</span>
						</c:when>
						<c:when test="${buyBillReconciliation.billDealStatus==9}">
							<span class="layui-btn layui-btn-mini" onclick="acceptCustomPayment('${buyBillReconciliation.id}');">奖惩待确认</span><br>
						</c:when>
					  </c:choose>
					  <c:choose>
						<c:when test="${buyBillReconciliation.billDealStatus==4}">
							<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="addBillRecFiles('${buyBillReconciliation.id}',
									'${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
									'${buyBillReconciliation.endBillStatementDateStr}');">添加附件</span><br>
							<%--<span class="layui-btn layui-btn-mini" onclick="launchReconciliation('${buyBillReconciliation.id}');">修改对账</span>--%>
							<a href="javascript:void(0);" class="layui-btn layui-btn-mini" onclick="leftMenuClick(this,'platform/seller/billReconciliation/updateBillRecJump?reconciliationId=${buyBillReconciliation.id}','sellers')">再次修改</a>
						</c:when>
					  </c:choose>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page }</div>

	<!--收款凭据-->
	<div id="linkReceipt" class="interest" style="display:none;">
		<div id="receiptBody" class="receipt_content">
			<div class="big_img">
				<img src="">
			</div>
			<div class="del_receipt">
				<button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--结算周期审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="billApproval">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="agree" type="radio" name="agreeStatus" value="3"> 同意</label>&nbsp;&nbsp;
					<label><input id="reject" type="radio" name="agreeStatus" value="5"> 驳回</label>
				</li>
				<li>
					<span>填写备注:</span>
					<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>

	</div>
	<!--添加账单附件-->
	<div class="plain_frame bill_exam" style="display:none;" id="billFiles">
		<ul id="billFilesBody">
		</ul>
	</div>

	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillFileList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">上传人</td>
					<td style="width:20%">上传时间</td>
					<td style="width:20%">上传备注</td>
					<td style="width:20%">上传单据</td>

				</tr>
				</thead>
				<tbody id="billRecFileBody">
				</tbody>
			</table>
		</div>
		<!--收款凭据-->
		<div id="showFiles" class="receipt_content" style="display:none;">
			<div id="showBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--卖家奖惩金额审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="acceptBillCustom">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="customAgree" type="radio" name="customStatus" value="10"> 同意</label>&nbsp;&nbsp;
					<label><input id="customReject" type="radio" name="customStatus" value="11"> 不同意</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="customRemarks" name="customRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>

	<!--回显自定义付款详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillCustomList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">申请人</td>
					<td style="width:10%">申请时间</td>
					<td style="width:10%">类型</td>
					<td style="width:10%">金额</td>
					<td style="width:10%">备注</td>
					<td style="width:10%">审批状态</td>
					<td style="width:10%">审批人</td>
					<td style="width:10%">审批时间</td>
					<td style="width:10%">审批备注</td>
					<td style="width:10%">单据</td>
				</tr>
				</thead>
				<tbody id="billRecCustomFileBody">
				</tbody>
			</table>
		</div>
		<!--自定义付款款凭据-->
		<div id="showCustomFiles" class="receipt_content" style="display:none;">
			<div id="showCustomBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileCustomList" class="icon_list"></ul>
			</div>
		</div>
	</div>

	<!--预付款抵扣详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showAdvanceEditList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">采购商</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">添加人</td>
					<td style="width:10%">添加时间</td>
					<td style="width:10%">预付款抵扣金额</td>
				</tr>
				</thead>
				<tbody id="billAdvanceEditBody">
				</tbody>
			</table>
		</div>
	</div>
</div>




<style>
	.view .icon_list {
		height: 92px;
		position: absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
</style>