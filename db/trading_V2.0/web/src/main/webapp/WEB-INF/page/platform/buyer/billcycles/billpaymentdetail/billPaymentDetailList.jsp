<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单我的付款明细</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billpaymentdetail/billPaymentDetailList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#billRecId").val("${params.billRecId}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#paymentStatus").val("${params.paymentStatus}");
		//重新渲染select控件
		/*var form = layui.form;
		form.render("select"); */
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
		
		//日期
		//loadDate("startDate","endDate");
	});

    //标签页改变
    function setStatus(obj,status) {
        $("#paymentStatus").val(status);
        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }

    //重置查询条件
    function resetformData(){
        $("#billRecId").val("");
        $("#createBillUserName").val("");
    	$("#sellerCompanyName").val("");
    	$("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
        $("#startTotalAmount").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
    	$("#paymentStatus").val("0");
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.paymentStatus eq '0'}">class="hover"</c:if>>所有（<span>${params.approvalBillReconciliationCount}</span>）</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.paymentStatus eq '3'}">class="hover"</c:if>>待付款开票（<span>${params.needBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.paymentStatus eq '1'}">class="hover"</c:if>>待卖家确认收款（<span>${params.acceptBillReconciliationCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.paymentStatus eq '2'}">class="hover"</c:if>>卖家收款已确认（<span>${params.apprEndBillReconciliationCount}</span>）</a> <b>|</b>

</div>
<div>
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billPaymentDetail/billPaymentDetailList">
		<ul class="order_search">
			<li >
				<label>对账单号:</label>
				<input type="text" placeholder="输入对账单号" id="billRecId" name="billRecId" />
			</li>
			<li class="comm">
				<label>采购员:</label>
				<input type="text" placeholder="请输入采购员名称" id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="comm">
				<label>供应商名称:</label>
				<input type="text" placeholder="请输入供应商名称" id="sellerCompanyName" name="sellerCompanyName" >
			</li>
			<li class="range">
				<label>应付款总额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
			</li>
			<li class="range nomargin">
				<label>出账日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range nomargin">
				<label>确认状态:</label>
				<div class="layui-input-inline">
					<select name="paymentStatus" id="paymentStatus" lay-filter="aihao">
						<option value="0" >所有</option>
						<option value="1" >待卖家确认收款</option>
						<option value="2" >卖家收款已确认</option>
						<option value="3" >待付款开票</option>
						<option value="4" >待付款申请</option>
						<option value="5" >审批已驳回</option>
					</select>
				</div>
			</li>


			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" />

			<li>
			  <li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
				<button type="reset" class="search" onclick="resetformData();">重置</button>

			  </li>
			<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="buyPaymentExport();">
				<i class="layui-icon">&#xe7a0;</i> 导出</a>
			</li>

		</ul>
	</form>
	  <table class="table_pure payment_list">
	  <thead>
	    <tr>
		  <td style="width:13%">对账单号</td>
		  <td style="width:10%">采购员</td>
		  <td style="width:10%">供应商</td>
		  <td style="width:10%">应付款总额</td>
		  <td style="width:10%">已付款金额</td>
		  <td style="width:10%">应付款余额</td>
		  <td style="width:11%">账单周期</td>
		  <%--<td style="width:11%">出账截止日期</td>--%>
		  <td style="width:10%">确认状态</td>
		  <td style="width:15%">操作</td>
	    </tr>
	  </thead>
	  <tbody>
       <c:forEach var="billPayment" items="${searchPageUtil.page.list}">
	    <tr class="breakType">
			<td style="text-align: center">${billPayment.id}</td>
			<td>${billPayment.createBillUserName}</td>
			<td>${billPayment.sellerCompanyName}</td>
			<td>${billPayment.totalAmount}</td>
			<td>${billPayment.actualPaymentAmount}</td>
			<td>${billPayment.residualPaymentAmount}</td>
			<td>${billPayment.startBillStatementDateStr}至${billPayment.endBillStatementDateStr}</td>
			<%--<td>${billPayment.endBillStatementDateStr}</td>--%>
			<td style="text-align: center">
				<div>
					<c:choose>
						<%--<c:when test="${billPayment.paymentStatus==0}">待付款账单</c:when>--%>
						<c:when test="${billPayment.paymentStatus==1}">待卖家确认收款</c:when>
						<c:when test="${billPayment.paymentStatus==2}">卖家收款已确认</c:when>
						<c:when test="${billPayment.paymentStatus==3}">待付款开票</c:when>
						<c:when test="${billPayment.paymentStatus==4}">待付款申请</c:when>
						<c:when test="${billPayment.paymentStatus==5}">付款申请中</c:when>
						<c:when test="${billPayment.paymentStatus==6}">审批已驳回</c:when>
					</c:choose>
				</div>
				<div class="opinion_view">
					<c:choose>
						<c:when test="${billPayment.paymentStatus != 4}">
							<span class="orange" data="${billPayment.acceptPaymentId}">查看审批流程</span>
						</c:when>
					</c:choose>
				</div>
			</td>
			<td style="text-align: center">
			 <%-- <div>--%>
			  <c:choose>
				  <c:when test="${billPayment.paymentStatus==0 ||billPayment.paymentStatus==1 || billPayment.paymentStatus==4 || billPayment.paymentStatus==6}">
					  <c:if test="${billPayment.residualPaymentAmount > 0}">
						   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillPayment('${billPayment.id}',
								   '${billPayment.paymentStatus}','${billPayment.acceptPaymentId}',
								   '${billPayment.paymentNo}','${billPayment.actualPaymentAmount}',
								   '${billPayment.residualPaymentAmount}','${billPayment.sellerCompanyName}');">申请付款</span>
					  </c:if>

				  </c:when>
			  </c:choose>
			 <%-- </div>
			  <div>--%>
				  <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/billPaymentDetail/showPaymentInfo?reconciliationId=${billPayment.id}&paymentType=0','buyer')" class="layui-btn layui-btn-mini">付款详情</a>

			  <%--</div>--%>
				<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/billPaymentDetail/showRecItemListNew?reconciliationId=${billPayment.id}&paymentType=0','buyer')" class="layui-btn layui-btn-normal layui-btn-mini">账单详情</a>
			</td>
		</tr>
	   </c:forEach>
	  </tbody>
    </table>
	<div class="pager">${searchPageUtil.page }</div>
</div>

<!--确认付款-->
<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="billPayment">
		<ul id="billPaymentBody">
		</ul>
</div>