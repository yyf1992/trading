<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
//生成对账单
function startPay(orderId){
	var url="platform/buyer/buyOrder/startReconciliation";
	$.ajax({
		url:url,
		type:"get",
		data:{"orderId":orderId},
		async:false,
		success : function(data) {
			var result=eval('('+data+')');
	    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
			if(resultObj.success){
				layer.msg(resultObj.msg,{icon:1,time:2000},function(){
					//跳转对账管理页面
					leftMenuClick(this,'platform/buyer/billReconciliation/billReconciliationList','buyer','17071814392298054492','账单列表');
				});
			}else{
				layer.msg(resultObj.msg,{icon:2,time:2000},function(){
					leftMenuClick(this,'platform/buyer/buyOrder/buyOrderList?tabId=${searchPageUtil.object.tabId}','buyer','17070718432862058335');
				});
			}
		},
		error : function() {
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}
</script>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:80%">
			<ul>
				<li style="width:25%">商品</li>
				<li style="width:10%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">订单数量</li>
				<li style="width:5%">单价</li>
				<li style="width:5%">总价</li>
				<li style="width:10%">到货数量</li>
				<li style="width:10%">未到货数量</li>
				<li style="width:10%">要求到货日期</li>
				<li style="width:10%">备注</li>
			</ul></td>
		<td style="width:10%">交易状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list">
		<p>
			<span class="layui-col-sm3">订单日期:<b><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></b></span>
			<span class="layui-col-sm3">订单号:<b>${orders.orderCode}</b></span>
			<span class="layui-col-sm3">供应商:<b>${orders.suppName}</b></span>
			<span class="layui-col-sm1">下单人:<b>${orders.createName}</b></span>
			<%-- <span class="layui-col-sm2">要求到货日期:<b><fmt:formatDate value="${orders.predictArred}" type="date"></fmt:formatDate></b></span> --%>
		</p>
		<table>
			<tr>
				<td style="width:80%">
				<c:forEach var="product" items="${orders.orderProductList}">
					<ul class="clear">
						<li style="width:25%">
							<!-- <span class="defaultImg"></span> -->
							${product.proCode}|${product.proName}|${product.skuCode}|${product.skuName}
						</li>
						<li style="width:10%">${product.skuOid}</li>
						<li style="width:5%">${product.unitName}</li>
						<li style="width:10%">${product.goodsNumber}</li>
						<li style="width:5%;text-align: center;">${product.price}</li>
						<li style="width:5%">${product.goodsNumber*product.price}</li>
						<li style="width:10%">${product.arrivalNum}</li>
						<li style="width:10%">${product.goodsNumber-product.arrivalNum}</li>
						<li style="width:10%"><fmt:formatDate value="${product.predictArred}"/></li>
						<li style="width:10%" title="${product.remark}">${product.remark}</li>
					</ul>
					<c:if test="${product.deliveryList !=null && product.deliveryList.size()>0 }">
						<table style="width: 100%">
							<thead>
								<tr style="background: #F7F7F7;height: 20px;">
									<td style="width:30px;">发货单号</td>
									<td style="width:30px;">发货类型</td>
									<td style="padding-top: 3px;width:30px;">发货数量</td>
									<td style="padding-top: 3px;width:30px;">到货数量</td>
									<td style="width:30px;">发货日期</td>
									<td style="width:30px;">到货日期</td>
								</tr>
							<c:forEach items="${product.deliveryList}" var="deliveryItem">
								<tr>
									<td>${deliveryItem.deliver_no}</td>
									<td>
										<c:choose>
											<c:when test="${deliveryItem.dropship_type==0}">正常发货</c:when>
											<c:when test="${deliveryItem.dropship_type==1}">代发客户</c:when>
											<c:when test="${deliveryItem.dropship_type==2}">代发菜鸟仓</c:when>
											<c:when test="${deliveryItem.dropship_type==3}">代发京东仓</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${deliveryItem.warehouse_id==52}">代发货</c:when>
													<c:otherwise>正常发货</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</td>
									<td style="padding-top: 3px;">${deliveryItem.delivery_num}</td>
									<c:choose>
										<c:when test="${deliveryItem.recordstatus==0}">
											<!-- 待收货 -->
											<td style="padding-top: 3px;">未到货</td>
											<td>
												<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
											</td>
											<td>未到货</td>
										</c:when>
										<c:when test="${deliveryItem.recordstatus==1}">
											<!-- 已收货 -->
											<td style="padding-top: 3px;">${deliveryItem.arrival_num}</td>
											<td>
												<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
											</td>
											<td>
												<fmt:formatDate value="${deliveryItem.arrival_date}" type="date"/>
											</td>
										</c:when>
									</c:choose>
								</tr>
							</c:forEach>
							</thead>
						</table>
					</c:if>
				</c:forEach>
				</td>
				<td style="width:10%">
					<div>
						<c:choose>
							<c:when test="${orders.isCheck==0}">
								<c:choose>
									<c:when test="${orders.status==6}">已取消</c:when>
									<%--<c:when test="${orders.status==7}">已驳回</c:when>--%>
									<c:when test="${orders.status==8}">已终止</c:when>
									<c:otherwise>待内部审批</c:otherwise>
								</c:choose>
							</c:when>
							<c:when test="${orders.isCheck==1}">
								<c:choose>
									<c:when test="${orders.status==0}">待接单</c:when>
									<%--<c:when test="${orders.status==1}">待确认协议</c:when>--%>
									<c:when test="${orders.status==2}">待对方发货</c:when>
									<c:when test="${orders.status==3}">待收货</c:when>
									<c:when test="${orders.status==4}">已收货</c:when>
									<%--<c:when test="${orders.status==5}">已完结</c:when>--%>
									<c:when test="${orders.status==6}">已取消</c:when>
									<c:when test="${orders.status==7}">
										<a href="javascript:void(0)" button="终止" onclick="showRejectReason('${orders.sellerRejectReason}');" class="approval" title="${orders.sellerRejectReason}">已驳回</a>
									</c:when>
									<c:when test="${orders.status==8}">
										<a href="javascript:void(0)" button="终止" onclick="showStopReason('${orders.stopReason}');" class="approval" title="${orders.stopReason}">已终止</a>
									</c:when>
								</c:choose>
							</c:when>
							<c:when test="${orders.isCheck==2}">
								<font color="red">内部审核被拒绝</font>
							</c:when>
						</c:choose>
						<br/>
						<div class="opinion_view">
							<c:if test="${orders.status!=6}">
								<span class="orange" data="${orders.id}">查看审批流程</span>
							</c:if>
						</div>
					</div>
				</td>
				<td style="width:10%" class="operate">
					<a href="javascript:void(0)" button="详情" onclick="leftMenuClick(this,'platform/buyer/buyOrder/showOrderDetail?orderCode=${orders.orderCode}'+
							'&returnUrl=platform/buyer/buyOrder/buyOrderList&menuId=17070718432862058335','buyer','17112115052543698084');" class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>详情
					</a>
					<c:choose>
						<%--未审核--%>
						<c:when test="${orders.isCheck==0}">
							<c:if test="${orders.isApproved==false}">
								<c:if test="${orders.isApproved==false && orders.status!=6}">
									<a href="javascript:void(0)" button="修改" onclick="update(this,'${orders.id}','${orders.createId}');" class="layui-btn layui-btn-mini">
										<i class="layui-icon">&#xe8fd;</i>修改
									</a>
								</c:if>
								<c:if test="${orders.isApproved==false && orders.status!=6 && orders.status!=7 && orders.status!=8}">
									<br/>
									<a href="javascript:void(0)" button="取消" onclick="cancelOrder('${orders.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
										<i class="layui-icon">&#xe92b;</i>取消
									</a>
								</c:if>
							</c:if>
						</c:when>
						<%--审核通过--%>
						<c:when test="${orders.isCheck==1}">
							<c:if test="${orders.status==7}"><%-- orders.status==6 ||  --%>
								<a href="javascript:void(0)" button="修改" onclick="update(this,'${orders.id}','${orders.createId}');" class="layui-btn layui-btn-mini">
									<i class="layui-icon">&#xe8fd;</i>修改
								</a><br/>
							</c:if>
							<c:if test="${orders.status!=6 && orders.status!=8}">
								<a href="javascript:void(0)" button="终止" onclick="stopOrder('${orders.id}');" class="layui-btn layui-btn-normal layui-btn-mini">
									<i class="layui-icon">&#xe68d;</i>终止
								</a>
							</c:if>
							<c:if test="${orders.status==4 && orders.reconciliationStatus==0 }">
								<span class="layui-btn layui-btn-mini layui-btn-warm" onclick="startPay('${orders.id}');">生成对账单</span>
							</c:if>
						</c:when>
						<%--审核未通过--%>
						<c:when test="${orders.isCheck==2}">
							<c:if test="${orders.status==0 || orders.status==7}">
								<a href="javascript:void(0)" button="修改" onclick="update(this,'${orders.id}','${orders.createId}');" class="layui-btn layui-btn-mini">
									<i class="layui-icon">&#xe8fd;</i>修改
								</a>
								<br/>
								<a href="javascript:void(0)" button="取消" onclick="this.cancelOrder('${orders.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
									<i class="layui-icon">&#xe92b;</i>取消
								</a>
							</c:if>
						</c:when>
					</c:choose>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>

<script type="text/javascript">
    /**
	 * 取消订单
     * @param id
     */
    function cancelOrder(id) {
        layer.confirm('确认取消订单?', {
            icon:3,
            title:'提示',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            $.ajax({
                type : "POST",
                url : "platform/buyer/buyOrder/cancelOrder",
                async: false,
                data : {
                    "orderId" : id
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("操作成功", {icon: 1},function(){
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });
    }
    /**
     * 终止订单
     * @param id
     */
    function stopOrder(id) {
        layer.prompt({
            formType : 2,
            title : '请输入终止原因',
            area: ['250px', '100px'],
            closeBtn: 0,
            maxlength : 500
        }, function(val, index) {
            layer.close(index);
            $.ajax({
                type : "POST",
                url : "platform/buyer/buyOrder/stopOrder",
                async: false,
                data : {
                    "orderId" : id,
					"stopReason" : val
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("操作成功", {icon: 1},function(){
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });
    }
    //查看订单驳回原因
	function showRejectReason(msg) {
        layer.open({
            title: '驳回原因'
            ,content: msg
        });
    }
    //查看订单终止原因
	function showStopReason(msg) {
        layer.open({
            title: '终止原因'
            ,content: msg
        });
    }
</script>