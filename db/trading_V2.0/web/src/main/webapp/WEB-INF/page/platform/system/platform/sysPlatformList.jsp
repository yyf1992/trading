<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@	taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(sysPlatformTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'edit'){
	    	//修改
	    	editPlatform(data.id);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('sysPlatformTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          platformCode   : $("#platformCode").val(),
	          platformName   : $("#platformName").val(),
	          isDel          : $("#isDel").val()
	        }
	      });
	    },
	    add: function(){
	    	$.ajax({
				url:basePath+"platform/sysplatform/loadAddSysPlatform",
				type:"post",
				async:false,
				success:function(data){
					layer.open({
						type:1,
						title:"添加平台",
						skin: 'layui-layer-rim',
		  		        area: ['400px', 'auto'],
		  		        content:data,
		  		        btn:['确定','取消'],
		  		        yes:function(index,layerio){
		  		        	$.ajax({
		  		        		url:basePath+"platform/sysplatform/saveInsert",
		  		        		type:"post",
		  		        		data:$("#sysPlatform").serialize(),
		  		        		async:false,
		  		        		success:function(data){
		  		        			var result=eval('('+data+')');
							    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
									if(resultObj.success){
										layer.close(index);
										layer.msg(resultObj.msg,{icon:1});
										active["reload"].call(this);
									}else{
										layer.msg(resultObj.msg,{icon:2});
									}
		  		        		},
		  		        		error:function(){
		  		        			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  		        		}
		  		        	});
		  		        }
					});
				},
				error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="platformCode" id="platformCode" autocomplete="off" placeholder='平台代码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="platformName" id="platformName" autocomplete="off" placeholder='平台名称'>
  </div>
  <div class="layui-inline">
  	<select id="isDel" name="isDel" lay-filter="aihao">
		<option value="">全部</option>
		<option value="0">正常</option>
		<option value="1">禁用</option>
	</select>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="add" button="新增">新增</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'sysPlatformTable',
		height:'full-90',
		url:'<%=basePath %>platform/sysplatform/loadDataJson'
	}" lay-filter="sysPlatformTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'id', sort: true,show:false}">id</th>
            <th lay-data="{field:'platformCode', sort: true,show:true}">平台代码</th>
            <th lay-data="{field:'platformName', sort: true,show:true}">平台名称</th>
            <th lay-data="{field:'companyId', sort: true,show:false}">公司id</th>
            <th lay-data="{field:'isDel', sort: true,show:true,templet: '#isDelTpl', align: 'center'}">状态</th>
            <th lay-data="{align:'center', toolbar: '#operate',show:true}">操作</th>
        </tr>
    </thead>
</table>
<!-- 操作 -->
<script type="text/html" id="operate">
	<a class="layui-btn layui-btn-update layui-btn-xs" lay-event="edit" button="修改">修改</a>
</script>
<!-- 状态转换 -->
<script type="text/html" id="isDelTpl">
	{{#  if(d.isDel === 0){ }}
		<span style="color: green;">正常</span>
	{{#  } else if(d.isDel === 1){}}
		<span style="color: red;">禁用</span>
	{{#  } }}
</script>
<script>
function editPlatform(id){
	$.ajax({
		url:basePath+"platform/sysplatform/loadEditPlatform",
		async:false,
		type:"post",
		data:{"id":id},
		success:function(data){
			layer.open({
			type:1,
			title:"修改平台",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:basePath+"platform/sysplatform/updateSysPlatform",
  		    		data:$("#editSysPlatform").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/sysplatform/loadSysPlatformList','system','17100920493416593335');
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/sysplatform/loadSysPlatformList','system','17100920493416593335');
						}
				},
  		    		error:function(){
  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>