<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="keywords" content="诺泰，诺泰买卖，买卖系统，nuotai">
  <meta name="description" content=" 诺泰买卖系统是用于商家向供应商采购商品。">
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>诺泰买卖系统</title>
  <%@ include file="../common/common.jsp"%>
  <script type="text/javascript">
  	$(function(){
  		leftMenuClick(this,'platform/seller/interworkGoods/interworkGoodsList.html','sellers','17070718471195397330');
  	});
  </script>
</head>
<body>
<!-- 顶部 -->
<%@ include file="../common/top.jsp"%>
<!-- 顶部菜单 -->
<jsp:include page="common/sellersTitle.jsp" >
	<jsp:param name="menuId" value="${sessionScope.menuId}" />
</jsp:include>
<div class="menu">
   <div class="menu-top">
       <div id="mini" style="border-bottom:1px solid rgba(255,255,255,.1)">
       	<img src="<%=basePath%>/statics/menu/images/mini.png">
       </div>
    </div>
    <!--左侧标题-->
	<jsp:include page="common/sellersMenu.jsp" >
		<jsp:param name="menuId" value="${sessionScope.menuId}" />
	</jsp:include>
</div>
<div class="container">
	<section class="section">
	    <!--内容-->
		<div class="rt_detail">
	    	<div class="content">内容</div>
	  	</div>
  	</section>
</div>
 <!--右侧好友-->
<%-- <jsp:include page= "../common/myFriend.jsp" flush="true">
	<jsp:param name= "role" value= "seller"/>
</jsp:include> --%>
<!--返回顶部按钮-->
<div class="toTop">
  <a href="javascript:scroll(0,0)"><img src="<%=basePath%>statics/platform/images/sidebar_4.png"></a>
</div>
</body>
</html>