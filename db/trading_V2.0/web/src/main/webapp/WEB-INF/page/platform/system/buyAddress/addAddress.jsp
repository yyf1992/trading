<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function(){
		loadProvince("province","");
		// 根据省份加载市
		var provinceId = $("#province").val();
		loadCity(provinceId,"city","");
		// 根据市加载区
		var cityId = $("#city").val();
		loadArea(cityId,"area","");
		//省改变
		$("#province").change(function(){
			// 根据省份加载市
			var provinceId = $("#province").val();
			loadCity(provinceId,"city","");
			// 根据市加载区
			var cityId = $("#city").val();
			loadArea(cityId,"area","");
		});
		//市改变
		$("#city").change(function() {
			// 根据市加载区
			var cityId = $("#city").val();
			loadArea(cityId,"area","");
		});
	});
</script>
<form action="">
	<div class="address_group">
		<span>收货人:</span> <input type="text" id="personName" style="width: 207px" placeholder="请输入收货人姓名">
	</div>
	<div class="address_group">
		<span>收货地址:</span>
		<select id="province" ></select>
		<select id="city" ></select>
		<select id="area" ></select>
	</div>
	<div class="address_group">
		<span>详细地址:</span> <input type="text" style="width: 90%" id="addrName"
			placeholder="建议您如实填写详细收货地址,例如街道名称,门牌号码,楼层和房间号等信息">
	</div>
	<div class="address_group">
		<span>手机号码:</span>
		<input type="tel" id="phone" style="width: 207px" placeholder="手机号码、电话号码必须填一项">
		<span>固定电话:</span>
		<input type="text" id="zone" style="width: 65px" placeholder="区号"> -
		<input type="text" id="telNo" style="width: 110px" placeholder="电话号码">
	</div>
	<div class="address_group">
		<span></span> <label><input type="checkbox" id="isDefault"> 设为默认地址</label>
	</div>
</form>