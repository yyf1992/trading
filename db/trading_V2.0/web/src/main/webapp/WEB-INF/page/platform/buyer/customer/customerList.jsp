<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<script type="text/javascript">
	$(function(){ 
		var form;
		layui.use('form',function(){
	    form = layui.form;
		form.render("select");
		//搜索更多
        $(".search_more").click(function(){
            $(this).toggleClass("clicked");
            $(this).parent().nextAll("ul").toggle();
        });
        //设置选择的tab
		$(".tab>a").removeClass("hover");
		$(".tab #tab_${params.tabId}").addClass("hover");
        $(".tab a").click(function(){
            var id = $(this).attr("id");
            //清空查询条件
            $("#customContent #resetButton").click();
            var index = id.split("_")[1];
            $("#customContent input[name='tabId']").val(index);
			loadPlatformData();
        });
        //日期
		loadDate("startDate","endDate");
        //初始化数据
		selectData();
		$("#customContent #selectButton").click(function(e){
			e.preventDefault();
			selectData();
		});
	});
	});
	function selectData(){
		var formObj = $("#customContent").find("form");
		$.ajax({
			url : "platform/buyer/customer/loadData",
			data:formObj.serialize(),
			async:false,
			success:function(data){
				var str = data.toString();
				$("#tabContent").html(str);
				loadVerify();
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	
	// 展示售后凭证
	function showProof(proof){
		if(proof == null || proof == ""){
			layer.msg("未上传凭证！",{icon:7});
			return;
		}else{
			window.open("platform/buyer/customer/showProof?proof="+proof);  
		}
	}
	//取消申请
	function cancelApply(id){
		layer.confirm('确定取消本单的售后申请吗?',{
			icon:3,
		      skin:'popBuyer popB25 btnCenter deleteBuyer',
		      title:'提示',
		      closeBtn:0,
		      area:['310px','auto']
		    },function(index){
		      layer.close(index);
		      $.ajax({
				type : "POST",
				url : "platform/buyer/customer/saveCancelCustomer",
				async: false,
				data : {
					"id" : id
				},
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if(resultObj.success){
						layer.msg("保存成功！",{icon:1});
						selectData();
					}else{
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
	}
	// 审核
	function verify(id){
		if($(".verifyDetail").length>0)$(".verifyDetail").remove();
		$.ajax({
			url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
			data:{"id":id},
			success:function(data){
				var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
				detailDiv.addClass("verifyDetail");
				detailDiv.html(data);
				detailDiv.appendTo($("#customContent"));
				detailDiv.find("#verify_side").addClass("show");
			},
			error:function(){
				layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
			}
		});
	}
	//导出
	$("#customerExportButton").click(function(e){
		e.preventDefault();
		var formData = $("form").serialize();
		var url = "platform/buyer/customer/exportCustomer?" + formData;
		window.open(url);
	});
	function pushOms(id){
		$.ajax({
			type : "POST",
			url : "platform/buyer/customer/pushOms",
			async: false,
			data : {
				"id" : id
			},
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
					layer.msg("推送成功！",{icon:1});
					selectData();
				}else{
					layer.msg(resultObj.msg,{icon:2});
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
	function showTips(shengYuNum,yiFaNum){
		var title = "卖家已发"+yiFaNum+",剩余"+shengYuNum+"等待换货";
		layer.msg(title);
	}
	//换货转退货
	function conversionType(itemId,userId){
		if(userId!=loginUserId){
			layer.msg("不是自己申请的售后无法修改！",{icon:2});
			return;
		}
		layer.msg('确认转为退货？', {
			 time: 0,
		    btn: ['确认', '取消'],
		    yes: function(index){
				$.ajax({
					type : "POST",
					url : "platform/buyer/customer/conversionType",
					async: false,
					data : {
						"itemId" : itemId
					},
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if(resultObj.success){
							layer.msg("转化成功！",{icon:1});
							selectData();
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
		    }
		  });
	}
</script>
<!--页签-->
<div class="tab">
	<a href="javascript:void(0)" id="tab_99">所有</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_0">待内部审批（<span>${params.waitVerifyNum}</span>）</a> <b>|</b>
	<%-- <a href="javascript:void(0)" id="tab_2">已驳回（<span>${params.rejectNum}</span>）</a> <b>|</b> --%>
	<a href="javascript:void(0)" id="tab_1">待卖家同意（<span>${params.waitAgreeNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_5">待卖家收货（<span>${params.passNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_4">卖家已收货（<span>${params.overNum}</span>）</a> <b>|</b>
	<a href="javascript:void(0)" id="tab_3">已取消（<span>${params.cancelNum}</span>）</a> <b>|</b>
</div>
<div id="customContent">
	<!--搜索栏-->
	<form class="layui-form" action="platform/buyer/customer/customerList" >
		<div class="search_top mt">
			<input type="text" placeholder="输入商品名称进行搜索" id="productName" name="productName" value="${params.productName}">
			<button id="selectButton">搜索</button>
			<span class="search_more sBuyer"></span>
		</div>
		<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="customerExportButton">
		<i class="layui-icon">&#xe7a0;</i> 导出</a>
		<ul class="order_search seller_order">
            <li class="comm">
                <label>货号:</label>
                <input type="text" placeholder="输入货号进行搜索" id="productCode" name="productCode" style="width:110px" value="${params.productCode}">
            </li>
            <li class="comm">
                <label>规格代码:</label>
                <input type="text" placeholder="输入规格代码进行搜索" id="skuCode" name="skuCode" style="width:130px" value="${params.skuCode}">
            </li>
            <li class="comm">
                <label>规格名称:</label>
                <input type="text" placeholder="输入规格名称进行搜索" id="skuName" name="skuName" style="width:130px" value="${params.skuName}">
            </li>
            <li class="comm">
                <label>条形码:</label>
                <input type="text" placeholder="输入条形码进行搜索" id="skuOid" name="skuOid" style="width:120px" value="${params.skuOid}">
            </li>
            <li class="comm">
                <label>供应商名称:</label>
                <input type="text" placeholder="输入供应商名称进行搜索" id="supplierName" name="supplierName" style="width:140px" value="${params.supplierName}">
            </li>
            <li class="comm">
                <label>售后单号:</label>
                <input type="text" placeholder="输入售后单号进行搜索" id="customerCode" name="customerCode" style="width:130px" value="${params.customerCode}">
            </li>
            <li class="range">
                <label>申请日期:</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="开始日" id="startDate" name="startDate" lay-verify="date" value="${params.startDate}">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="截止日" id="endDate" name="endDate" lay-verify="date" value="${params.endDate}">
                </div>
            </li>
            <li class="range">
                <label>oms出库日期:</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="开始日" id="omsFinishdateStart" name="omsFinishdateStart" lay-verify="date" value="${params.omsFinishdateStart}">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" placeholder="截止日" id="omsFinishdateEnd" name="omsFinishdateEnd" lay-verify="date" value="${params.omsFinishdateEnd}">
                </div>
            </li>
            <li class="state nomargin">
                <label>售后类型:</label>
                <div class="layui-input-inline">
                    <select name="type" id="type" lay-filter="aihao">
                        <option value="" <c:if test="${params.type == ''}">selected</c:if>>全部</option>
                        <option value="0" <c:if test="${params.type == '0'}">selected</c:if>>换货</option>
                        <option value="1" <c:if test="${params.type == '1'}">selected</c:if>>退货</option>
                    </select>
                </div>
            </li>
            <li class="range"><button type="reset" id="resetButton" class="search">重置查询条件</button></li>
        </ul>
        <input type="hidden" name="interest" value="${params.interest}">
		<input type="hidden" name="tabId" value="${params.tabId}">
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	</form>
	<div id="tabContent"></div>
</div>
