<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="${basePath}statics/platform/css/supplier.css">
<script type="text/javascript">
$(function(){
	selAll();
});
//功能点1:全选事件
function selAll(){
	var inputs=$('.supplierList tbody input');
	$('.supplierList thead input').click(function(){
		inputs.prop('checked',$(this).prop('checked'));
	});
	inputs.click(function(){
		var r=$('.supplierList tbody input:not(:checked)');
		if(r.length==0){
			$('.supplierList thead input').prop('checked',true);
		}else{
			$('.supplierList thead input').prop('checked',false);
		}
	});
}
//删除供应商
function deleteSupplier(id){
	layer.confirm('确定删除供应商吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/buyer/supplier/saveDeleteSupplier",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1});
						leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}

function  frozenSupplier(id,suppName,status){
	//冻结供应商
	if(status==1){
		layer.open({
			type:1,
			content:'<textarea placeholder="输入冻结原因" name="frozenReason" id="frozenReason" style="width:360px;height:100px"></textarea>',
			title:'冻结供应商',
			skin:'layui-layer-rim',
			area:['400px','auto'],
			btn:['确定','取消'],
			yes:function(index,layerio){
				$.ajax({
					type : "post",
					url : "platform/buyer/supplier/saveFrozenSupplier",
					data : {
						"id" : id,
						"suppName":suppName,
						"frozenReason":$("#frozenReason").val(),
						"status":status
					},
					async : false,
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if (resultObj.success) {
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
						} else {
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
			}
		});
	}else if(status==0){//取消冻结
		layer.confirm('确定取消冻结供应商吗？', {
		      icon:3,
		      title:'提醒',
		      closeBtn:2,
		      btn: ['确定','取消'],
			  yes:function(index,layerio){
				$.ajax({
					type : "post",
					url : "platform/buyer/supplier/saveFrozenSupplier",
					data : {
						"id" : id,
						"status":status
					},
					async : false,
					success : function(data) {
						var result = eval('(' + data + ')');
						var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
						if (resultObj.success) {
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
						} else {
							layer.msg(resultObj.msg,{icon:2});
						}
					},
					error : function() {
						layer.msg("获取数据失败，请稍后重试！",{icon:2});
					}
				});
			}
		});
	}

}

//取消冻结供应商
function unfrozenSupplier(id){
	layer.confirm('取消冻结供应商吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/buyer/supplier/unFrozenSupplier",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1});
						leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}
// 批量删除供应商
/*
function beatchDeleteSupplier(){
	var checked = $("input:checkbox[name='checkone']:checked");
	if (checked.length < 1) {
		layer.msg("请至少选择一条数据！",{icon:1});
		return;
	}
	var ids = "";
	checked.each(function() {
		ids += $(this).val() + ",";
	});
	ids = ids.substring(0, ids.length - 1);
	layer.confirm('确定删除供应商吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/buyer/supplier/saveBeatchDeleteSupplier",
				data : {
					"ids" : ids
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{time:500},function(){
							leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
						});
					} else {
						layer.msg(resultObj.msg,{time:500});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}*/
//导出
$("#exportSupplierDate").click(function(e){
	e.preventDefault();
	var formData = $("form").serialize();
	var url = "platform/buyer/supplier/exportSupplierData?"+ formData;
	window.open(url);
});
/**
 * 同步OMS供应商数据
 */
function synchronousSupplier() {
    layer.confirm('确认同步供应商吗?', {
        icon:3,
        title:'提示',
        skin:'pop',
        closeBtn:2,
        btn: ['确定','取消']
    }, function(index){
    	layer.close(index);
        $.ajax({     	
            url : "platform/buyer/supplier/synOMSSupplier",
            async: false,
            data : {         	
            },      
            success : function(data) {
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg(resultObj.msg, {icon: 1},function(){
                    	leftMenuClick(this,"platform/buyer/supplier/supplierList","buyer","17071815040317888127");
                    });
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
        });       
    });
}
</script>
<div>
	<form action="platform/buyer/supplier/supplierList" id="searchForm">
		<ul class="order_search supplier_query">
			<li class="comm">
                <label>供应商:</label>
                <input type="text" placeholder="输入供应商名称" name="suppName" value="${searchPageUtil.object.suppName}" >
			</li>
			<%-- <li class="range">
            	<label>区号电话:</label>
            	<input type="text" placeholder="输入区号" name="zone" value="${searchPageUtil.object.zone}" > - 
				<input type="text" placeholder="输入电话" name="telNo" value="${searchPageUtil.object.telNo}" >
            </li> --%>
            <li class="range">
              <label>联系人:</label>
              <input type="text" placeholder="输入联系人" name="person" value="${searchPageUtil.object.person}" >
            </li>
            <li class="comm">
              <label>手机号:</label>
              <input type="text" placeholder="输入手机号" name="phone" value="${searchPageUtil.object.phone}">
            </li>
            <!-- <li class="range nomargin">
              <label>创建日期:</label>
              <div class="layui-input-inline">
                <input type="text" name="createDate" id="createDate" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
              </div>
            </li> -->
			<li>
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
            </ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
	<!--批量删除-->
	<div class="mt" id="newSupplier">
      <%--<input type="button" class="layui-btn layui-btn-danger layui-btn-small batch_d" value="批量删除供应商" onclick="beatchDeleteSupplier();"> --%>
<!--    	 <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/addSupplier','buyer','17071815040317888127')" class="layui-btn layui-btn-danger layui-btn-small rt" button="新建供应商">新建供应商</a>  -->
     <a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" id="exportSupplierDate">
			<i class="layui-icon">&#xe7a0;</i> 导出
	 </a>
	 <a href="javascript:void(0)" onclick="synchronousSupplier();" class="layui-btn layui-btn-danger layui-btn-small rt">从OMS同步供应商</a> 	 
    </div>
	<table class="table_pure supplierList mt">
		<thead>
			<tr>
				<%--<td style="width:5%"><input type="checkbox"></td>--%>
				<td style="width:12%">供应商编码</td>
				<td style="width:30%">供应商名称</td>
				<td style="width:7%">供应商等级</td>
				<td style="width:10%">合作始于</td>
                <td style="width:8%">联系人</td>
                <td style="width:10%">联系电话</td>
                <td style="width:10%">付款方式</td>
                <td style="width:10%">历史绩效</td>
                <td style="width:6%">合作状态</td>
                <td style="width:8%">冻结原因</td>
                <td style="width:20%">工厂地址</td>
                <td style="width:20%">创建日期</td>
                <td style="width:15%">操作</td>
			</tr>
			<tr>
				<td colspan="14"></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="supplier" items="${searchPageUtil.page.list}">
				<tr>
					<%--<td><input type="checkbox" name="checkone" value="${supplier.id }"></td>--%>
					<td>${supplier.company_code }</td>
					<td>${supplier.supp_name }</td>
					<td>
						<c:if test="${supplier.linklevel == 1}">初级</c:if>
						<c:if test="${supplier.linklevel == 2}">核心</c:if>
						<c:if test="${supplier.linklevel == 3}">战略</c:if>
					</td>
					<td><fmt:formatDate value="${supplier.create_date }" type="date"/></td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.person}">${supplier.person}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.phone}">${supplier.phone}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.payment_method}">${supplier.payment_method}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.historical_performance}">${supplier.historical_performance}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${supplier.is_frozen==0}">正常</c:when>
							<c:when test="${supplier.is_frozen==1}">已冻结</c:when>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.frozen_reason}">${supplier.frozen_reason}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty supplier.address}">${supplier.address}</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td><fmt:formatDate value="${supplier.create_date }" type="both"/></td>
					<td>
						<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/updateSupplier?id=${supplier.id }','buyer','17071815040317888127')" class="layui-btn layui-btn-mini" button="修改">修改</a>
						<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/contract/addContract?role=buyer','buyer')" 
							class="layui-btn layui-btn-normal layui-btn-mini">上传合同</a>
<%-- 						<a onclick="deleteSupplier('${supplier.id }');" class="layui-btn layui-btn-danger layui-btn-mini" button="删除">删除</a> --%>
						<c:choose>
							<c:when test="${supplier.is_frozen==0}">
							<a onclick="frozenSupplier('${supplier.id }','${supplier.supp_name }',1);" class="layui-btn layui-btn-jinyong layui-btn-mini" button="冻结">&emsp;冻结&emsp;</a>
							</c:when>
							<c:when test="${supplier.is_frozen==1}">
							<a onclick="frozenSupplier('${supplier.id }','${supplier.supp_name }',0);" class="layui-btn layui-btn-jinyong layui-btn-mini" button="取消冻结">取消冻结</a>
							</c:when>
						</c:choose>
						<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/product/loadSupplierProductLinks?supplierId=${supplier.supplier_id }&suppName=${supplier.supp_name }','buyer')" class="layui-btn layui-btn-normal layui-btn-mini">查看关联</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
	</form>
</div>