<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<%@ include file="path.jsp"%>--%>
<script type="text/javascript">
	//判断我是买家还是我是卖家菜单
	var role = '${param.role}';
    $(function(){
	//切换右侧好友窗口
	$('.switch>span').click(function(){
	    debugger;
	  $('.switch>span.open').toggle();
	  $('.switch>span.close').toggle();
	  $('#side_pro').toggleClass('show');
	});
    layui.use(['layer','form'], function() {
        var layer = layui.layer, form = layui.form;
        //添加好友
        $('.last>b').click(function () {
            layer.open({
                type: 1,
                title: '添加好友',
                area: ['365px', 'auto'],
                skin: 'pop',
                closeBtn: 2,
                shade: 0,
                content: $('.friends_add'),
                btn: ['确认申请', '取消'],
                yes: function (index) {
                    var inviteeName = $("#inviteeName").val();
                    if(inviteeName==null||inviteeName==''||inviteeName==undefined){
                        layer.msg("请输入对方公司名称",{icon:2});
                        return;
                    }
                    $.ajax({
                        type : "POST",
                        url :"interwork/applyFriend/saveInviteFriend",
                        data: {
                            "inviterRole":role=="seller"?"1":"0",
                            "inviteeName":inviteeName,
                            "note": $("#note").val()
                        },
                        async:false,
                        success:function(data){
                            var result = eval('(' + data + ')');
                            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                            if(resultObj.success){
                                layer.msg("操作成功！",{icon:1});
                                layer.close(index);
                            }else{
                                layer.msg(resultObj.msg,{icon:7});
                            }
                        },
                        error:function(){
                            layer.msg("保存失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        });
    });
    loadFriend(role);
});
function loadFriend(role) {
    var opt="";
    var url = "";
    var queryParams;
    if(role=="seller"){
        url = basePath+'platform/common/getBuyerSelect';
        queryParams = {
            buyersName: $("#searchName").val()
        };
    }else{
        url = basePath+'platform/common/getSupplierSelect';
        queryParams = {
            sellerName: $("#searchName").val()
        };
	}
    $.ajax({
        type : "GET",
        url : url,
        data : queryParams,
        async : true,
        dataType: "json",
        success : function(data) {
            var result = eval('(' + data + ')');
//            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            $.each(result, function(index, element) {
                var title = element.sellerName;
                var imgSrc = "${basePath}statics/platform/images/defaulGoods.png";
                var company = "";
                var linkman = "";
                var tel = ""
                if(role=="seller"){//我是买家，公司展示buyersName
                    company = (element.buyersName==null||element.buyersName==undefined)?"":element.buyersName;
                    linkman = (element.buyersPerson==null||element.buyersPerson==undefined)?"":element.buyersPerson;
                    tel = (element.buyersPhone==null||element.buyersPhone==undefined)?"":element.buyersPhone;
				}else {
                    company = (element.sellerName==null||element.sellerName==undefined)?"":element.sellerName;
                    linkman = (element.sellerPerson==null||element.sellerPerson==undefined)?"":element.sellerPerson;
                    tel = (element.sellerPhone==null||element.sellerPhone==undefined)?"":element.sellerPhone;
				}


                opt = opt+"<div title='"+title+"'>"+
							"<img src='"+imgSrc+"'>"+
							"<div>"+
								"<h4>"+company+"</h4>"+
								"<p>"+linkman+"&nbsp;"+tel+"</p>"+
							"</div>"+
						"</div>";
            });
            $(".Pro_list").empty();
            $(".Pro_list").append(opt);
        },
        error : function() {
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
}
</script>
<!--侧边栏-->
<div class="aside">
	<div class="switch">
		<span class="open"> 
			<img src="${basePath}statics/platform/images/friend_list.jpg"> 打开我的好友 <i></i>
		</span> 
		<span class="close"> 
			<img src="${basePath}statics/platform/images/friend_list.jpg"> 关闭我的好友 <i></i>
		</span>
		<div id="side_pro">
			<div class="Pro_main">
					<div class="Pro_mT clear">
						<input type="text" id="searchName" name="searchName"
						<c:choose>
							   <c:when test="${param.role=='seller'}">placeholder="输入采购商名称"</c:when>
							   <c:otherwise>placeholder="输入供应商名称"</c:otherwise>
						</c:choose>
						>
						<button onclick="loadFriend('${param.role}');"></button>
					</div>
					<div class="Pro_list"></div>
					<div class="last">
						<b></b>
					</div>
			</div>
		</div>
	</div>
</div>
<div class='friends_add'  >
	<div>
		<label>公司名称:</label>
		<input type='text' id="inviteeName" name="inviteeName" placeholder='对方公司名称' required>
	</div>
	<div>
		<label>附加信息:</label>
		<textarea id="note" name="note" placeholder='我想与您成为互通好友，方便线上对账，请快同意下吧！'></textarea>
	</div>
</div>