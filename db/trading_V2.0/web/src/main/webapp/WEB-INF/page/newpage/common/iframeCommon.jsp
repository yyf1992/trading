<%@ page language="java" import="java.util.*,com.nuotai.trading.utils.ShiroUtils" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="basePath" value="<%=basePath %>" />
<c:set var="staticsPath" value="${basePath}statics" />
<!--当前登录用户ID-->
<c:set var="userId" value="<%=ShiroUtils.getUserId() %>" />
<!--当前登录用户公司ID-->
<c:set var="companyId" value="<%=ShiroUtils.getCompId() %>" />
<script type="text/javascript">
    var basePath = "${basePath}";
    var loginUserId = "${userId}";
    var loginCompanyId = "${companyId}";
</script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="${basePath}statics/css/layui-icon-font.css">
<link rel="stylesheet" href="${basePath}statics/X-admin2.0/css/iconfont-font.css">
<link rel="stylesheet" href="${basePath}statics/X-admin2.0/css/xadmin.css">
<link rel="stylesheet" href="<%=basePath %>statics/X-admin2.0/XiaLaDuoXuan/layui/css/layui.css" media="all" />
<link rel="stylesheet" href="<%=basePath %>statics/X-admin2.0/XiaLaDuoXuan/css/public.css" media="all" />

<link rel="stylesheet" href="${staticsPath}/platform/css/common.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/purchase.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/system.css">
<link rel="stylesheet" href="${basePath}statics/plugins/ztree/css/zTreeStyle/zTreeStyle.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/ystep/ystep.css">
<link rel="stylesheet" href="${staticsPath}/css/nuotai.css">
<link rel="stylesheet" href="${staticsPath}/platform/css/print.css">
<link rel="stylesheet" href="${basePath}statics/platform/css/verify.css">

<!-- js -->
<script src="${basePath}statics/X-admin2.0/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath %>statics/X-admin2.0/XiaLaDuoXuan/layui/layui.js"></script>
<script src="${basePath}statics/X-admin2.0/js/util_date.js"></script>
<script src="${basePath}statics/X-admin2.0/js/xadmin.js"></script>

<script src="${staticsPath}/platform/js/html2canvas.js"></script>
<script src="${staticsPath}/platform/js/sweetTitles.js"></script>
<script src="${staticsPath}/platform/js/colResizable-1.6.min.js"></script>
<script src="<%=basePath%>js/common.js"></script>
<script src="${staticsPath}/platform/js/common.js"></script>
<script src="${staticsPath}/platform/js/Region.js"></script>
<script src="${staticsPath}/platform/js/upload.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.all.min.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.core.js"></script>
<script src="${basePath}statics/plugins/ztree/jquery.ztree.excheck.js"></script>
<script src="${staticsPath}/platform/js/ystep.js"></script>
<script src="${staticsPath}/platform/js/verify.js"></script>
<script src="${staticsPath}/dingding/js/base64.js"></script>
<script src="${basePath}statics/platform/js/toolTip.js"></script>
