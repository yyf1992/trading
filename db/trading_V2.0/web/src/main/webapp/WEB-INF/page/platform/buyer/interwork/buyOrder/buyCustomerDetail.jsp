<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun"%>
<script type="text/javascript">
	$(function(){
		layer.photos({
			photos: '#layer-photos-demo',
			anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
		});
	});
</script>
<div>
	<ul class="afterSale">
		<li><span>售后编号：</span>
			<p>${order.customerCode}</p></li>
		<li><span>供应商名称：</span>
			<p>${order.supplierName}</p></li>
		<li><span class="mt">售后商品：</span>
			<div class="afterList">
				<table class="table_pure afterDetail">
					<thead>
						<tr>
							<td>货号</td>
							<td>商品名称</td>
							<td>规格代码</td>
							<td>规格名称</td>
							<td>条形码</td>
							<td>价格</td>
							<td>售后类型</td>
							<td>售后数量</td>
							<td>单个返修金额</td>
							<td>售后总金额</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${order.itemList}">
							<tr>
								<td>${item.productCode}</td>
								<td>${item.productName}</td>
								<td>${item.skuCode}</td>
								<td>${item.skuName}</td>
								<td>${item.skuOid}</td>
								<td>${item.price}</td>
								<td>
									<c:choose>
										<c:when test="${item.type==0}">换货</c:when>
										<c:when test="${item.type==1}">退货</c:when>
										<c:when test="${item.type==2}">换货转退货</c:when>
									</c:choose>
								</td>
								<td>${item.goodsNumber}</td>
								<td>${item.exchangePrice}</td>
								<td>${item.repairPrice}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div></li>
		<li><span>申请原因：</span>
			<p>${order.reason}</p>
		</li>
		<c:if test="${order.proof != null && order.proof != ''}">
		<li><span>售后凭证：</span>
			<p class="afterImg layer-photos-demo" id="layer-photos-demo">
				<c:forEach var="proof" items="${order.proof.split(',')}">
					<img src="${proof}" layer-src="${proof}" title="点击查看大图">
				</c:forEach>
			</p>
		</li>
		</c:if>
		<li><span>申请状态：</span>
			<p>
				<c:choose>
					<c:when test="${order.status==0}">待内部审批</c:when>
					<c:when test="${order.status==1}">待对方同意</c:when>
					<c:when test="${order.status==2}">已驳回</c:when>
					<c:when test="${order.status==3}">已取消</c:when>
					<c:when test="${order.status==4}">对方已确认</c:when>
					<c:when test="${order.status==5}">待对方确认</c:when>
				</c:choose>
			</p></li>
		<li><span>申请人：</span>
			<p>${order.createName}</p></li>
		<li><span>申请时间：</span>
			<p><fmt:formatDate value="${order.createDate}" type="both"/></p></li>
		<li><span>收货地址：</span>
			<p>${el:getProvinceById(order.province).province} ${el:getCityById(order.city).city} 
				${el:getAreaById(order.area).area} ${order.addrName}（${order.linkman} 收）${order.linkmanPhone}</p></li>
	</ul>
	<div class="text-center" style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'${returnUrl}?${form}','buyer','${menuId}');"><span class="contractBuild">返回</span></a>
	</div>
</div>