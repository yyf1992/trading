<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../../common/path.jsp"%>
<script>
function toBillCycleList(){
    leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
}
function toBillCycleDetails(){
    leftMenuClick(this,'platform/buyer/billCycle/billCycleVerifyDetail?billCycleId=${billCycleId}','buyer')
}
</script>
<div>
	<div class="order_success">
		<img src="<%=basePath%>statics/platform/images/ok.png">
		<h3>恭喜您已下单成功！</h3>
		<a href="javascript:void(0)" class="purchase" onclick="toBillCycleList();">转到账单结算周期列表</a>
		<a href="javascript:void(0)" onclick="toBillCycleDetails();" class="view_details">查看详情</a>
		<a href="javascript:void(0)" class="print" target="_blank">打印采购单</a>
	</div>
</div>