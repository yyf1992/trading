<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<html>
  <form id="purchasePriceForm">
    <div class="account_group">
      <span>商品名称:</span>
     	<input type="text" name="productCode" value="${purchase[0].productName}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>货号:</span>
     	<input type="text" name="productCode" value="${purchase[0].productCode}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>条形码:</span>
      <input type="text" name="barcode" value="${purchase[0].barcode}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>加价方式:</span>
      <select name="priceStatus">
      	<option value="0" <c:if test="${purchase[0].priceStatus==0}">selected="selected"</c:if>>加价</option>
      	<option value="1" <c:if test="${purchase[0].priceStatus==1}">selected="selected"</c:if>>提成</option>
      </select>
    </div>
    <div class="account_group">
      <span>金额/百分比(%):</span>
      <input type="text" name="priceLatitude" value="${purchase[0].priceLatitude}">
    </div>
    <input type="hidden" name="productId" value="${purchase[0].productId}">
</form>
</html>