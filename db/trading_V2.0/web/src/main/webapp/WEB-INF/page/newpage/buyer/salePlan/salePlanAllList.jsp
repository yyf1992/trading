<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
body{overflow-y: scroll;}
</style>
<script>
var planTime;
layui.use('table', function() {
	var table = layui.table;
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('salePlanTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          shopName     : $("#shopName").val(),
	          barcode      : $("#barcode").val(),
	          productCode  : $("#productCode").val(),
	          skuCode      : $("#skuCode").val(),
	          planCode     : $("#planCode").val()
	        }
	      });
	    },
	    //导出
	    exportExcel: function(){
			var formData = [
				"shopName="+$("#shopName").val(),
				"&barcode="+$("#barcode").val(),
				"&productCode="+$("#productCode").val(),
				"&skuCode="+$("#skuCode").val(),
				"&planCode="+$("#planCode").val()
			];
			var url = basePath + "download/salePlanListStatistic?" + formData.join("");
			window.open(url);
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	    var type = $(this).data('type');
	    active[type] ? active[type].call(this) : '';
	});
});
Date.prototype.format = function(fmt) { 
     var o = { 
        "M+" : this.getMonth()+1,                 //月份 
        "d+" : this.getDate(),                    //日 
        "h+" : this.getHours(),                   //小时 
        "m+" : this.getMinutes(),                 //分 
        "s+" : this.getSeconds(),                 //秒 
        "q+" : Math.floor((this.getMonth()+3)/3), //季度 
        "S"  : this.getMilliseconds()             //毫秒 
    }; 
    if(/(y+)/.test(fmt)) {
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
    }
	for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)){
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
		}
	}
    return fmt; 
}
function getDate(startDate,endDate){
	if(startDate != null && startDate != ''){
		var oldStartDate = (new Date(startDate)).getTime();
	    var curStartDate = new Date(oldStartDate).format("yyyy-MM-dd");
	    var oldEndDate = (new Date(endDate)).getTime();
	    var curEndDate = new Date(oldEndDate).format("yyyy-MM-dd");
	    return curStartDate +" 至 "+ curEndDate;
	}else{
		return "";
	}
}
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="planCode" id="planCode" autocomplete="off" placeholder='销售计划编号' style="width: 170px;">
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="shopName" id="shopName" autocomplete="off" placeholder='部门'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="skuCode" id="skuCode" autocomplete="off" placeholder='型号'>
  </div>
  <button class="layui-btn" data-type="reload" id="select">搜索</button>
  <button class="layui-btn" data-type="exportExcel">导出</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		total:true,
		page:true,
		limit:20,
		id:'salePlanTable',
		height:'full-90',
		url:'<%=basePath %>buyer/salePlan/loadDataJson'
	}" lay-filter="salePlanTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'barcode',width:120, sort: true,show:true,showToolbar:true}">条形码</th>
            <th lay-data="{field:'product_code',width:120, sort: true,show:true}">货号</th>
            <th lay-data="{field:'sku_name',width:120, sort: true,show:true}">规格名称</th>
            <th lay-data="{field:'sales_num',width:90, sort: true,total:true,show:true}">销售计划</th>
            <th lay-data="{field:'confirm_sales_num',width:90, sort: true,total:true,show:true,templet: '#confirmSalesNumTpl'}">确认计划</th>
            <th lay-data="{field:'monthly_forecast',width:110, sort: true,total:true,show:true}">月度预计总量</th>
            <th lay-data="{field:'promotions_remark', sort: true,show:false}">活动说明</th>
            <th lay-data="{field:'id',width:150, sort: true,show:false}">ID</th>
            <th lay-data="{field:'plan_code',width:180, sort: true,show:true}">销售计划编号</th>
            <th lay-data="{field:'shop_name',width:110, sort: true,show:true}">部门</th>
            <th lay-data="{field:'',width:180, sort: true,show:true,templet: '#timeTpl'}">销售周期</th>
            <th lay-data="{field:'create_name', sort: true,show:true}">创建人</th>
            <th lay-data="{field:'create_date',width:150, sort: true,show:true}">创建时间</th>
            <th lay-data="{field:'main_picture_url', show:true,templet: '#mainPictureUrl',img:true}">图片</th>
            <th lay-data="{field:'plan_type', sort: true,show:true,templet: '#planTypeTpl', align: 'center'}">计划类型</th>
            <th lay-data="{field:'is_expired', sort: true,show:true,templet: '#isExpiredTpl', align: 'center'}">是否过期</th>
            <th lay-data="{field:'is_confirm', sort: true,show:true,templet: '#isConfirmTpl', align: 'center'}">是否确认</th>
            <th lay-data="{field:'is_next_step', sort: true,width:120,show:true,templet: '#isNextStepTpl', align: 'center'}">是否提交采购计划</th>
        </tr>
    </thead>
</table>
<!-- 是否提交采购计划 -->
<script type="text/html" id="isNextStepTpl">
	{{#  if(d.is_next_step === 0){ }}
		<span style="color: red;">未转采购计划</span>
	{{#  } else if(d.is_next_step === 1){ }}
		<span style="color: green;">已转采购计划</span>
	{{#  } }}
</script>
<!-- 确认销售计划显示 -->
<script type="text/html" id="confirmSalesNumTpl">
	{{#  if(d.is_confirm === 0){ }}
		<span style="color: red;">未确认</span>
	{{#  } else if(d.is_confirm === 1){ }}
		{{#  if(d.confirm_sales_num === null || d.confirm_sales_num === ""|| d.confirm_sales_num === undefined){ }}{{#  }else{ }}
			{{d.confirm_sales_num}}
		{{#  } }}
	{{#  } }}
</script>
<!-- 图片设置 -->
<script type="text/html" id="mainPictureUrl">
	{{#  if(d.main_picture_url != null && d.main_picture_url != ''){ }}
		<img src="{{d.main_picture_url}}" onmouseover="toolTip('{{d.main_picture_url}}')" onmouseout="toolTip()">
	{{#  } else{ }}
		<img src='<%=basePath%>statics/platform/images/defaulGoods.jpg' onmouseover="toolTip('<%=basePath%>statics/platform/images/defaulGoods.jpg')" onmouseout="toolTip()">
	{{#  } }}
</script>
<!-- 类型转换 -->
<script type="text/html" id="planTypeTpl">
	{{#  if(d.plan_type === 0){ }}
		<span style="color: green;">计划内</span>
	{{#  } else if(d.plan_type === 1){}}
		<span style="color: red;">计划外</span>
	{{#  } }}
</script>
<!-- 是否过期 -->
<script type="text/html" id="isExpiredTpl">
{{#  if(d.is_confirm === 0){ }}
	{{#  if(d.is_expired === 0){ }}
		<span style="color: green;">未过期</span>
	{{#  } else if(d.is_expired === 1){}}
		<span style="color: red;">已过期</span>
	{{#  } }}
{{#  } else {}}
	-
{{#  } }}
</script>
<!-- 确认状态 -->
<script type="text/html" id="isConfirmTpl">
	{{#  if(d.is_confirm === 0){ }}
		<span style="color: red;">未确认</span>
	{{#  } else if(d.is_confirm === 1){ }}
		<span style="color: green;">已确认</span>
	{{#  } }}
</script>
<!-- 格式化日期 -->
<script type="text/html" id="timeTpl">
	{{  getDate(d.start_date,d.end_date) }} 
</script>