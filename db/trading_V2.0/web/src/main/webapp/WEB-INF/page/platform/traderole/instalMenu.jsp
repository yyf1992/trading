<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../common/path.jsp"%>
<script>
var tradeRole = <%=request.getAttribute("tradeRole")%>;
</script>
<script type="text/javascript" src="${basePath}js/traderole/instalMenu.js"></script>
<div>
	<div style="text-align: center;padding: 10px;">
	     <div class="layui-btn layui-btn-normal layui-btn-small <c:if test='${tradeRole.isAdmin==1}'>layui-btn-disabled</c:if>"  onclick="submitSave();" disabled>确认提交</div>
         <button class="layui-btn layui-btn-primary layui-btn-small" onclick="leftMenuClick(this,'platform/tradeRole/tradeRoleList','system','17100920503219724288');">返回</button>
     </div>
	<form action="platform/tradeRole/saveInstalMenu">
		<table class="table_pure menuTable">
			<thead>
				<tr>
					<td style="width:20%">菜单名称</td>
					<td style="width:10%">菜单位置</td>
					<td style="width:10%">类型</td>
					<td style="width:20%">操作数据时是否需要审批</td>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</form>
	 <div style="text-align: center;padding: 10px;">
         <div class="layui-btn layui-btn-normal layui-btn-small <c:if test='${tradeRole.isAdmin==1}'>layui-btn-disabled</c:if>"  onclick="submitSave();">确认提交</div>
         <button class="layui-btn layui-btn-primary layui-btn-small" onclick="leftMenuClick(this,'platform/tradeRole/tradeRoleList','system','17100920503219724288');">返回</button>
     </div>
</div>