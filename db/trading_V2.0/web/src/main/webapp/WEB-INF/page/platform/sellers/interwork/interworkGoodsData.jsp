<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
    layui.use(['form'], function(){
        var form = layui.form;
    });
    preview.init();
    //功能点2:查看审批流程
    $('.opinion_view').hover(
        function(){
            $(this).children('.opinion').toggle();
        }
    );

    //功能点1:全选事件
    function selAll(){
        var inputs=$('.order_list>p>span.chk_click input');
        function checkedSpan(){
            inputs.each(function(){
                if($(this).prop('checked')==true){
                    $(this).parent().addClass('checked')
                }else{
                    $(this).parent().removeClass('checked')
                }
            })
        }

        inputs.click(function(){
            if($(this).prop('checked')==true){
                $(this).parent().addClass('checked')
            }else{
                $(this).parent().removeClass('checked')
            }
            var r=$('.order_list>p>span.chk_click input:not(:checked)');
            if(r.length==0){
                $('.batch_handle input').prop('checked',true).parent().addClass('checked');
            }else{
                $('.batch_handle input').prop('checked',false).parent().removeClass('checked');
            }
        });
    }
    selAll();

    //选中合并发货，必须是同一家采购商才可以合并发货
    $('#deliver_b').click(function(){
        var layer = layui.layer,form=layui.form;
        var orderIds = "";//订单ID
        if($("input:checkbox[name='checkone']:checked").length < 1){
            layer.msg("至少选中一项！",{icon:2});
            return;
        }else{
            //判断是否是同一家采购商
			var checkedItems = $("input:checkbox[name='checkone']:checked");
            var buyerIdFirst = $(checkedItems[0]).parent().parent().find("input[name='buyerId']").val();//第一条选中数据的采购商ID
            orderIds = $(checkedItems[0]).val();
			for(var i=1;i<checkedItems.length;i++){
			    var currBuyerId = $(checkedItems[i]).parent().parent().find("input[name='buyerId']").val();
			    var currOrderId = $(checkedItems[i]).val();
				if(buyerIdFirst!=currBuyerId){
					layer.msg("必须同一家采购商才可以合并发货！",{icon:2});
					return;
				}else{
                    orderIds += "," + currOrderId;
				}

			}
            //接单之后才可以合并发货
            for(var i=0;i<checkedItems.length;i++){
                var statusFlag = $(checkedItems[i]).parent().parent().find("input[name='status']").val();
                if(statusFlag=='0'){
                    layer.msg("必须接单之后才可以合并发货！",{icon:2});
                    return;
                }
            }

		}
        layer.confirm('您确定要将选中的商品合并发货吗?', {
            icon:3,
            title:'选中合并发货',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            leftMenuClick(this,'platform/seller/delivery/mergeSendGoods?orderIds='+orderIds+',buyCompanyId='+buyerIdFirst,'sellers');
        });
    });
    //接单审核
	function approveOrder(id) {
        layer.confirm('确认接单?', {
            icon:3,
            title:'接单',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            $.ajax({
                type : "POST",
                url : "platform/seller/interworkGoods/approveLinkOrder",
                async: false,
                data : {
                    "orderId" : id
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("接单成功", {icon: 1},function(){
//                            leftMenuClick(this,"platform/seller/interworkGoods/interworkGoodsList.html","sellers");
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });
    }
    
    //驳回订单
	function rejectOrder(id) {
        layer.prompt({
            formType : 2,
            title : '请输入驳回原因',
            area: ['250px', '100px'],
            closeBtn: 0,
            maxlength : 500
        }, function(val, index) {
            layer.close(index);
            $.ajax({
                type : "POST",
                url : basePath+"platform/seller/interworkGoods/cancelLinkOrder",
                async: false,
                data : {
                    "orderId" : id,
                    "sellerRejectReason" : val
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("操作成功", {icon: 1},function(){
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });
        /*layer.confirm('确认取消订单?', {
            icon:3,
            title:'提示',
            skin:'pop',
            closeBtn:2,
            btn: ['确定','取消']
        }, function(index){
            layer.close(index);
            $.ajax({
                type : "POST",
                url : "platform/seller/interworkGoods/cancelLinkOrder",
                async: false,
                data : {
                    "orderId" : id
                },
                success : function(data) {
                    var result = eval('(' + data + ')');
                    var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                    if(resultObj.success){
                        layer.msg("操作成功", {icon: 1},function(){
                            selectData();
                        });
                    }else{
                        layer.msg(resultObj.msg,{icon:2});
                    }
                },
                error : function() {
                    layer.msg("获取数据失败，请稍后重试！",{icon:2});
                }
            });
        });*/
    }
</script>
<table class="order_detail">
	<tr>
		<td style="width:76%">
			<ul>
				<li style="width:25%">商品</li>
				<li style="width:15%">条形码</li>
				<li style="width:5%">单位</li>
				<li style="width:10%">订单数量</li>
				<li style="width:5%">单价</li>
				<li style="width:10%">总价</li>
				<li style="width:10%">到货数量</li>
				<li style="width:10%">要求到货日期</li>
				<li style="width:10%">备注</li>
			</ul>
	    </td>
		<td style="width:12%">交易状态</td>
		<td style="width:12%">操作</td>
	</tr>
</table>
<c:forEach var="orders" items="${searchPageUtil.page.list}">
	<div class="order_list" id="dataList">
		<p>
			<span class="layui-col-sm2">
<%-- 				<input type="checkbox" value="${orders.id}" name="checkone"> --%>
				&nbsp;订单日期: <b><fmt:formatDate value="${orders.createDate}" type="both"></fmt:formatDate></b>
			</span>
			<span class="layui-col-sm2">订单号: <b>${orders.orderCode}</b></span>
			<span class="layui-col-sm3">采购商：<b>${orders.companyName}</b></span>
			<span class="layui-col-sm3">下单人：<b>${orders.createName}</b></span>
			<input type="hidden" value="${orders.companyId}" name="buyerId">
			<input type="hidden" value="${orders.status}" name="status">

		</p>
		<table>
			<tr>
				<td style="width:76%">
					<c:forEach var="product" items="${orders.supplierProductList}" varStatus="i">
						<ul class="clear">
							<li style="width:25%">
								<!-- <span class="defaultImg"></span> -->
								${product.productCode}|${product.productName}|${product.skuCode}|${product.skuName}
								<%-- <div>${product.productCode} ${product.productName}<br/>
									<span>规格代码: ${product.skuCode}</span><br/>
									<span>规格名称: ${product.skuName}</span>
								</div> --%>
							</li>
							<li style="width:15%">${product.barcode}</li>
							<li style="width:5%">${product.unitName}</li>
							<li style="width:10%">${product.orderNum}</li>
							<li style="width:5%;text-align: center;">${product.price}</li>
							<li style="width:10%">${product.price*product.orderNum}</li>
							<li style="width:10%">${product.arrivalNum}</li>
							<li style="width:10%">
								<fmt:formatDate value="${product.predictArred}"/>
							</li>
							<li style="width:10%">${product.remark}</li>
						</ul>
						<c:if test="${product.deliveryList !=null && product.deliveryList.size()>0 }">
							<table style="width: 100%">
								<thead>
									<tr style="background: #F7F7F7;height: 20px;">
										<td style="width:30px;">发货单号</td>
										<td style="width:30px;">发货类型</td>
										<td style="padding-top: 3px;width:30px;">发货数量</td>
										<td style="padding-top: 3px;width:30px;">到货数量</td>
										<td style="width:30px;">发货状态</td>
										<td style="width:30px;">创建发货日期</td>
										<td style="width:30px;">发货日期</td>
										<td style="width:30px;">到货日期</td>
									</tr>
								<c:forEach items="${product.deliveryList}" var="deliveryItem">
									<tr>
										<td>${deliveryItem.deliver_no}</td>
										<td>
											<c:choose>
												<c:when test="${deliveryItem.dropship_type==0}">正常发货</c:when>
												<c:when test="${deliveryItem.dropship_type==1}">代发客户</c:when>
												<c:when test="${deliveryItem.dropship_type==2}">代发菜鸟仓</c:when>
												<c:when test="${deliveryItem.dropship_type==3}">代发京东仓</c:when>
											</c:choose>
										</td>
										<td style="padding-top: 3px;">${deliveryItem.delivery_num}</td>
										<c:choose>
											<c:when test="${deliveryItem.recordstatus==0}">
												<!-- 未发货 -->
												<td style="padding-top: 3px;">未发货</td>
												<td>未发货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>未发货</td>
												<td>未发货</td>
											</c:when>
											<c:when test="${deliveryItem.recordstatus==1}">
												<!-- 已发货 -->
												<td style="padding-top: 3px;">买家未收货</td>
												<td>已发货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.send_date}" type="date"/>
												</td>
												<td>买家未收货</td>
											</c:when>
											<c:when test="${deliveryItem.recordstatus==2}">
												<!-- 已收货 -->
												<td style="padding-top: 3px;">${deliveryItem.arrival_num}</td>
												<td>买家已收货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.send_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.arrival_date}" type="date"/>
												</td>
											</c:when>
										</c:choose>
									</tr>
								</c:forEach>
								</thead>
							</table>
						</c:if>
					</c:forEach>
				</td>

				<td style="width:12%">
					<div>
						<c:choose>
							<c:when test="${orders.status==0}">待接单</c:when>
							<c:when test="${orders.status==1}">待发货</c:when>
							<c:when test="${orders.status==2}">发货中</c:when>
							<c:when test="${orders.status==3}">发货完成</c:when>
							<c:when test="${orders.status==4}">已收货</c:when>
							<c:when test="${orders.status==5}">交易完成</c:when>
							<c:when test="${orders.status==6}">已取消</c:when>
							<c:when test="${orders.status==7}">已驳回</c:when>
							<c:when test="${orders.status==8}">已终止</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>
					<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/interworkGoods/interworkGoodsDetail.html?orderCode=${orders.orderCode}'
							+'&returnUrl=platform/seller/interworkGoods/interworkGoodsList.html&menuId=17070718471195397330','sellers');" class="approval">订单详情</a>
				</td>
				<td style="width:12%">
					<c:choose>
						<c:when test="${orders.status==0}">
							<a href="javascript:void(0)" onclick="approveOrder('${orders.id}');" class="deliver_btn">接单</a>
							<a href="javascript:void(0)" onclick="rejectOrder('${orders.id}');" class="deliver_btn">驳回</a>
						</c:when>
						<c:when test="${(orders.status==1||orders.status==2||orders.status==3) && (orders.goodsNum-orders.arrivalTotalNum-orders.deliveringTotalNum>0)}">
							<c:if test="${orders.showDeliveryButton}">
								<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/seller/delivery/singleSendGoods?id=${orders.id}','sellers');" class="deliver_btn">创建发货单</a>
								<c:if test="${orders.isChange == null || orders.isChange == '' || orders.isChange == 0}">
									<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/buyOrder/changePurchaseOrder?id=${orders.buyerOrderId}','sellers');" class="green_btn">转化采购单</a>
								</c:if>
							</c:if>
						</c:when>
					</c:choose>

				</td>
			</tr>
		</table>
	</div>
</c:forEach>

<!--合并发货-->
<div class="text-center">
	<c:choose>
		<c:when test="${interest==1||interest==2}">
			<button class="layui-btn layui-btn-normal layui-btn-small" id="deliver_b">合并发货</button>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>

</div>

<!--分页-->
<div class="pager">${searchPageUtil.page}</div>