<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(sysBankTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'editAccount'){
	    	//修改
	    	editAccount(data.id);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('sysBankTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          accountName: $("#accountName").val(),
	          isDel: $("#isDel").val(),
	          createDate: $("#createDate").val()
	        }
	      });
	    },
	    addBank: function(){
	    	$.ajax({
	    		url:basePath+"platform/sysbank/loadBankInsert",
	    		type:"get",
	    		async:false,
	    		success:function(data){
	    			layer.open({
	    				type:1,
	    				content:data,
	    				title:'新建付款账户',
	    				skin:'layui-layer-rim',
	    				area:['400px','auto'],
	    				btn:['确定','取消'],
	    				yes:function(index,layerio){
	    					$.ajax({
	    						url:basePath+"platform/sysbank/saveBankInsert",
	    						type:"post",
	    						async:false,
	    						data:$("#addSysBankForm").serialize(),
	    						success:function(data){
	    							var result=eval('('+data+')');
	    							var resultObj=isJSONObject(result)?result:eval('('+result+')');
	    							if(resultObj.success){
										layer.close(index);
										layer.msg(resultObj.msg,{icon:1});
										active["reload"].call(this);
									}else{
										layer.msg(resultObj.msg,{icon:2});
									}
	    						},
	    						error:function(){
	    							layer.msg("获取数据失败，稍后重试！",{icon:2});
	    						}
	    					});
	    				}
	    			});
	    		},
	    		error:function(){
	    			layer.msg("获取数据失败，稍后重试！",{icon:2});
	    		}
	    	});
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="accountName" id="accountName" autocomplete="off" placeholder='户名'>
  </div>
  <div class="layui-input-inline">
    <input type="text" name="createDate" id="createDate" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
  </div>
  <div class="layui-inline">
  	<select id="isDel" name="isDel" lay-filter="aihao">
		<option value="">全部</option>
		<option value="0">正常</option>
		<option value="1">禁用</option>
	</select>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="addBank" button="新增">新增</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'sysBankTable',
		height:'full-90',
		url:'<%=basePath %>platform/sysbank/loadDataJson'
	}" lay-filter="sysBankTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'id', sort: true,show:false}">id</th>
            <th lay-data="{field:'openBank', sort: true,show:true}">开户行</th>
            <th lay-data="{field:'bankAccount', sort: true,show:true}">账号</th>
            <th lay-data="{field:'accountName', sort: true,show:true}">户名</th>
            <th lay-data="{field:'isDel', sort: true,show:true,templet: '#isDelTpl', align: 'center'}">状态</th>
            <th lay-data="{field:'createDate', sort: true,show:true}">创建日期</th>
            <th lay-data="{align:'center', toolbar: '#operate',show:true,width:300}">操作</th>
        </tr>
    </thead>
</table>
<!-- 操作 -->
<script type="text/html" id="operate">
	<a class="layui-btn layui-btn-update layui-btn-xs" lay-event="editAccount" button="修改">修改</a>
</script>
<!-- 状态转换 -->
<script type="text/html" id="isDelTpl">
	{{#  if(d.isDel === 0){ }}
		<span style="color: green;">正常</span>
	{{#  } else if(d.isDel === 1){}}
		<span style="color: red;">禁用</span>
	{{#  } }}
</script>
<script>
function editAccount(id){
	$.ajax({
	url:basePath+"platform/sysbank/loadEditbank",
	type:"post",
	data:{"id":id},
	async:false,
	success:function(data){
		layer.open({
			type:1,
			content:data,
			title:'修改账户',
			skin:'layui-layer-rim',
			area:['400px','auto'],
			btn:['确定','取消'],
			yes:function(index,layerio){
				$.ajax({
					url:basePath+"platform/sysbank/saveBankEdit",
					type:"post",
					async:false,
					data:$("#editSysBankForm").serialize(),
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/sysbank/loadSysBankList','system','17100920501973862097');
						}
					},
					error:function(){
						layer.msg("获取数据失败，稍后重试！",{icon:2});
					}
				});
			}
		});
	},
	error:function(){
		layer.msg("获取数据失败，稍后重试！",{icon:2});
	}
 });
}
</script>