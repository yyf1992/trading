<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:70%">
			<ul>
				<li style="width:40%">商品</li>
				<li style="width:15%">条形码</li>
				<li style="width:15%">部门</li>
				<li style="width:15%">销售计划</li>
				<li style="width:15%">入仓量</li>
			</ul>
		</td>
		<td style="width:10%">备注</td>
		<td style="width:10%">状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<div class="salePlanContentDiv">
	<c:forEach var="salePlan" items="${searchPageUtil.page.list}">
		<div class="order_list">
			<p>
				<span class="layui-col-sm2">计划编号:<b>${salePlan.planCode}</b>
				</span>
				<span class="layui-col-sm3">标题:<b>${salePlan.title}</b>
				</span>
				<span class="layui-col-sm2">创建人:<b>${salePlan.createName}</b>
				</span>
				<span class="layui-col-sm2">销售周期: <b> <fmt:formatDate
							value="${salePlan.startDate}" type="date"></fmt:formatDate> 至 <fmt:formatDate
							value="${salePlan.endDate}" type="date"></fmt:formatDate> </b> </span>
				<span class="layui-col-sm2">创建日期:<b><fmt:formatDate
							value="${salePlan.createDate}" type="both"></fmt:formatDate></b>
				</span>
			</p>
			<table>
				<tr>
					<td style="width:70%"><c:forEach var="salePlanItem"
							items="${salePlan.itemList}">
							<ul class="clear">
								<li style="width:40%">
									${salePlanItem.productCode}|${salePlanItem.productName}|${salePlanItem.skuCode}|${salePlanItem.skuName}
								</li>
								<li style="width:15%">${salePlanItem.barcode}</li>
								<li style="width:15%">${salePlanItem.shopName}</li>
								<li style="width:15%">${salePlanItem.salesNum}</li>
								<li style="width:15%">${salePlanItem.putStorageNum}</li>
							</ul>
						</c:forEach>
					</td>
					<td style="width:10%">${salePlan.remark}</td>
					<td style="width:10%">
						<div>
							审核不通过
						</div>
						<div class="opinion_view">
							<span class="orange" data="${salePlan.id}">查看审批流程</span>
						</div>
					</td>
					<td style="width:10%">
						<a href="javascript:void(0)" button="详情"
							onclick="leftMenuClick(this,'buyer/salePlan/loadSalePlanDetails?id=${salePlan.id}','buyer','18011115560680331888');"
							class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe695;</i>详情</a>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>