<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    $(function(){
        if ('${buyBillReconciliation.billDealStatus}'<4){
            $(".ystep1").loadStep({
                size: "large",
                color: "green",
                steps: [{
                    title: "等待对账"
                },{
                    title: "待对方对账"
                },{
                    title: "对账已完成"
                }/*,{
				 title: "内部审批驳回"
				 },{
				 title: "对方审批驳回"
				 }*/]
            });
            debugger;
            var step = parseInt('${buyBillReconciliation.billDealStatus}');
            $(".ystep1").setStep(step);
        }
    });

</script>
<%--<div >--%>
<div class="order_top mt">
	<div class="lf order_status" >
		<h4 class="mt">当前订单状态</h4>
		<p class="mt text-center">
        <span class="order_state">
          <c:choose>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==1}">待内部审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==2}">待对方审批</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==3}">审批已通过</c:when>--%>
			  <%--<c:when test="${billCycleManagementInfo.billDealStatus==4}">内部审批驳回</c:when>
			  <c:when test="${billCycleManagementInfo.billDealStatus==5}">对方审批驳回</c:when>--%>
			  <c:when test="${buyBillReconciliation.billDealStatus == 1}">等待对账</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==4}">内部驳回</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus == 2}">待对方对账</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==3}">对账已完成</c:when>
			  <c:when test="${buyBillReconciliation.billDealStatus==5}">对方驳回</c:when>
		  </c:choose>
        </span>
		</p>
		<p class="order_remarks text-center"></p>
	</div>
	<div class="lf order_progress">
		<div class="ystep1"></div>
	</div>
</div>
<div>
	<h4 class="mt">账单对账信息</h4>
	<div class="order_d">
		<p class="line mt"></p>
		<p><span class="order_title">供应商信息</span></p>
		<table class="table_info">
			<tr>
				<td>供应商名称：<span>${buyBillReconciliation.sellerCompanyName}</span></td>
				<%--<td>负责人：<span>萌萌</span></td>
                <td>手机号：<span>12234325</span></td>--%>
			</tr>
		</table>

		<p class="line mt"></p>
		<p><span class="order_title">账单对账信息</span></p>
		<table class="table_info">
			<tr>
				<%--<td>供货商：<span>${buyBillReconciliation.sellerCompanyName}</span></td>--%>
				<td>出账日期：<span>${buyBillReconciliation.endBillStatementDateStr}</span></td>
				<td>到货数量：<span>${buyBillReconciliation.recordConfirmCount}</span></td>
				<td>到货金额：<span>${buyBillReconciliation.recordConfirmTotal}</span></td>
			</tr>
			<tr>
				<td>退货数量：<span>${buyBillReconciliation.returnGoodsCount}</span></td>
				<td>退货金额：<span>${buyBillReconciliation.returnGoodsTotal}</span></td>
				<td>账期总金额：<span>${buyBillReconciliation.totalAmount}</span></td>
			</tr>

		</table>

	</div>
</div>
<%--
</div>--%>
