<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../common/path.jsp"%>
<script>
	var role = '${role}';
	var id = '${id}';
	function toContractList(){
        leftMenuClick(this,'platform/'+role+'/contract/contractList','');
	}
	function toContractDetail() {
        leftMenuClick(this,'platform/'+role+'/contract/contractDetail?id='+id+'&role='+role,'');
    }
</script>
<div>
	<div class="order_success">
		<img src="<%=basePath%>statics/platform/images/ok.png">
		<h3>恭喜您提交成功！</h3>
		<a href="javascript:void(0)" class="purchase" onclick="toContractList();">转到合同列表</a>
		<a href="javascript:void(0)" class="view_details" onclick="toContractDetail();">查看详情</a>
		<%--<a href="javascript:void(0)" class="print" target="_blank">打印采购单</a>--%>
	</div>
</div>