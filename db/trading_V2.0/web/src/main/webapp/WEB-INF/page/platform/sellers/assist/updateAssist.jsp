<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<script src="<%=basePath%>statics/plugins/layer/layer.js"></script>
<script src="<%=basePath%>statics/platform/js/supplier.js"></script>
<script type="text/javascript">
	$(function() {
		loadProvince("updateProvince","${supplier.province}");
		// 根据省份加载市
		var provinceId = $("#updateProvince").val();
		loadCity(provinceId,"updateCity","${supplier.city}");
		// 根据市加载区
		var cityId = $("#updateCity").val();
		loadArea(cityId,"updateArea","${supplier.area}");
		//省改变
		$("#updateProvince").change(function(){
			// 根据省份加载市
			var provinceId = $("#updateProvince").val();
			loadCity(provinceId,"updateCity","");
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		//市改变
		$("#updateCity").change(function() {
			// 根据市加载区
			var cityId = $("#updateCity").val();
			loadArea(cityId,"updateArea","");
		});
		// 修改保存
		$("#saveUpdate").click(function() {
			var suppName = $("#suppName").val();
			if(suppName == ''){
				layer.msg("请填写公司名称！",{icon:7});
				return;
			}
			var province = $("#province").val();
			if(province == ''){
				layer.msg("请填写所在地！",{icon:7});
				return;
			}
			var address = $("#address").val();
			if(address == ''){
				layer.msg("请填写详细地址！",{icon:7});
				return;
			}
			// 附件信息
			var a1 = $("#attachment1Div").find("span").length;
			var a1Str = ""
			if(a1 != 0){
				for(var i = 0;i < a1;i++){
					var url1 = $("#attachment1Div").find("span:eq("+i+")").find("input").val();
					a1Str = a1Str + url1 + "@";
				}
				a1Str = a1Str.substring(0, a1Str.length - 1);
			}
			// 联系人字符串拼接
			var linkmanStr = new Array();
			$("#linkmanTbody tr").each(function() {
				var person = $(this).find("td:eq(1)").find("input").val();
				var phone = $(this).find("td:eq(2)").find("input").val();
				var zone = $(this).find("td:eq(3)").find("input:eq(0)").val();
				var telNo = $(this).find("td:eq(3)").find("input:eq(1)").val();
				var fax = $(this).find("td:eq(4)").find("input").val();
				var qq = $(this).find("td:eq(5)").find("input").val();
				var wangNo = $(this).find("td:eq(6)").find("input").val();
				var email = $(this).find("td:eq(7)").find("input").val();
				var isDefault = $(this).find("td:eq(8)").find("input").val();
				var item = new Object();
				item.person = person;
				item.phone = phone;
				item.zone = zone;
				item.telNo = telNo;
				item.fax = fax;
				item.qq = qq;
				item.wangNo = wangNo;
				item.email = email;
				item.isDefault = isDefault;
				linkmanStr.push(item);
			});
			$.ajax({
				type : "post",
				url : "platform/seller/assist/saveUpdateSupplier",
				data : {
					"id" : $("#id").val(),
					"suppName" : suppName,
					"province" : province,
					"city" : $("#city").val(),
					"area" : $("#area").val(),
					"address" : address,
					"bankNo" : $("#bankNo").val(),
					"taxpayerNo" : $("#taxpayerNo").val(),
					"a1Str" : a1Str,
					"linkmanStr" : JSON.stringify(linkmanStr)
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1});
						leftMenuClick(this,"platform/seller/assist/loadAssistHtml","seller","18041717432690984729");
					} else {
						layer.msg(resultObj.msg, {icon : 2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！", {icon : 2});
				}
			});
		});
	});
function setIsDefault(obj){
	$("#linkmanTbody tr").each(function() {
		$(this).find("td:eq(8)").find("input").val("0");
	});
	$(obj).parents("tr").find("td:eq(8)").find("input").val("1");
}
</script>
<style>
.error{
border-color: red;
}
</style>
<div class="add_supplier">
	<form action="">
		<input type="hidden" id="id" name="id" value="${supplier.id}"/>
		<div class="form_group">
			<label class="control_label"><span class="red">*</span>外协名称:</label>
			<input type="text" class="form_control" required
				placeholder="请输入公司名称" value="${supplier.suppName}" name="suppName" id="suppName">
		</div>
		<div class="form_group">
			<label class="control_label"><span class="red">*</span>所 在 地:</label>
			<div class="info_city" style="margin-left: 148px;margin-top: -20px;">
				<select id="updateProvince" name="updateProvince"></select>
				<select id="updateCity" name="updatecity"></select>
				<select id="updateArea" name="updateArea"></select>
			</div>
		</div>
		<div class="form_group">
			<label class="control_label"><span class="red">*</span> 详 细 地 址:</label>
			<textarea type="text" class="form_control_t" required name="address" id="address"
				placeholder="请如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息" style="width:313px">${supplier.address}</textarea>
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;营业证件扫描件:</label>
			<div class="upload_license">
				<input type="file" name="attachment1" id="attachment1" onchange="fileUpload(this);">
				<p></p>
				<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
			</div>
			<div class="img_license" id="attachment1Div">
				<c:forEach var="a1" items="${el:getAttachmentByRelatedId(supplier.id,1)}">
					<span><b></b><img src="${a1.url}">
                  	<input type="hidden" name="fileUrl" value="${a1.url}"></span>
				</c:forEach>
			</div>
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;银行账号:</label> <input
				type="text" class="form_control" placeholder="请输入银行账号" name="bankNo" id="bankNo" value="${supplier.bankNo}">
		</div>
		<div class="form_group">
			<label class="control_label">&nbsp;纳税人识别号:</label> <input type="text"
				class="form_control" placeholder="请输入纳税人识别号" name="taxpayerNo" id="taxpayerNo" value="${supplier.taxpayerNo}">
		</div>
		<div class="setting">
			<h4>联系方式设置:</h4>
			<div class="btn_add mt">
				<a href="#" class="btn_new contract_a"></a>
			</div>
			<table class="table_blue contract_settings">
				<thead>
					<tr>
						<td style="width:4%">默认</td>
						<td style="width:10%">联系人</td>
						<td style="width:10%">手机号</td>
						<td style="width:20%">固话</td>
						<td style="width:12%">传真</td>
						<td style="width:10%">QQ</td>
						<td style="width:12%">旺旺</td>
						<td style="width:12%">E-mail</td>
						<td style="width:10%">操作</td>
					</tr>
				</thead>
				<tbody id="linkmanTbody">
			    	<c:forEach var="linkman" items="${linkmanList}" varStatus="indexNum">
						<tr>
							<td><input type="radio" name="default" <c:if test="${linkman.isDefault == 1}">checked</c:if> onclick="setIsDefault(this)"></td><!-- 默认为1 -->
							<td><input type="text" placeholder="联系人" name="person" value="${linkman.person }">
							</td>
							<td><input type="text" placeholder="手机" name="phone" value="${linkman.phone }">
							</td>
							<td><input type="text" placeholder="区号" style="width:50px" name="zone" value="${linkman.zone }">
								- <input type="text" style="width:100px" placeholder="电话" name="telNo" value="${linkman.telNo }">
							</td>
							<td><input type="text" placeholder="传真" name="fax" value="${linkman.fax }">
							</td>
							<td><input type="text" placeholder="QQ" name="qq" value="${linkman.qq }">
							</td>
							<td><input type="text" placeholder="旺旺" name="wangNo" value="${linkman.wangNo }">
							</td>
							<td><input type="text" placeholder="E-mail" name="email" value="${linkman.email }">
							</td>
							<td>
							<c:if test="${indexNum.count != 1}"><span class="table_del"><b></b>删除</span></c:if>
							<input type="hidden" name="isDefault" value="${linkman.isDefault }">
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</form>
	<div class="contract_submit" style="margin-left: 47%;margin-top: 40px;">
            <div class="layui-btn layui-btn-normal layui-btn-small" id="saveUpdate">保存</div>
            <button class="layui-btn layui-btn-primary layui-btn-small" onclick="leftMenuClick(this,'platform/seller/assist/loadAssistHtml','seller','18041717432690984729');">取消</button>
    </div>
</div>
