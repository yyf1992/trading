<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun"%>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td style="width: 70%">
			<ul>
				<li style="width: 50%">商品</li>
				<li style="width: 10%">条形码</li>
				<li style="width: 10%">售后数量</li>
				<li style="width: 10%">oms出库数</li>
				<li style="width: 10%">卖家收货数</li>
				<li style="width: 10%">售后类型</li>
			</ul></td>
		<td style="width: 10%">申请原因</td>
		<td style="width: 10%">状态</td>
		<td style="width: 10%">操作</td>
	</tr>
</table>
		<div class="order_list serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> 
				<span class="order_num"><span>标题:</span>
					${customer.title}</span>
				<span class="order_num"><span>售后单号:</span>
					${customer.customerCode}</span> 
				<span class="order_num"><span>创建人:</span>
					${customer.createName}</span>
				<span class="order_name">${customer.supplierName}</span>
				<c:if test="${customer.arrivalType==1}">
					<span style="margin-left: 30px;">oms出库时间：<fmt:formatDate value="${customer.omsFinishdate}" type="both"/></span>
				</c:if>
				<c:if test="${not empty customer.updateDate && customer.status==4}">
					<span style="margin-left: 30px;">卖家收货时间：<fmt:formatDate value="${customer.updateDate}" type="both"/></span>
				</c:if>
			</p>
			<table>
				<tr>
					<td style="width: 70%;">
				<c:forEach var="customerItem" items="${customer.itemList}">
						<ul class="clear">
							<li style="width: 50%;"><span class="defaultImg"></span>
								<div>
									${customerItem.productCode}|${customerItem.productName} <br>
									<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
										${customerItem.skuName}</span>
								</div></li>
							<li style="width: 10%;">${customerItem.skuOid}</li>
							<li style="width: 10%;">${customerItem.goodsNumber}</li>
							<li style="width: 10%">&nbsp;${customerItem.receiveNumber}</li>
							<li style="width: 10%">&nbsp;${customerItem.confirmNumber}</li>
							<li style="width: 10%;">
								<c:choose>
									<c:when test="${customerItem.type==0}">
										<c:choose>
											<c:when test="${customerItem.updateTypeFlag}">
												<span>换货中</span>
												<i class="layui-icon" style="font-size:1px;cursor:pointer;" 
													onclick="showTips('${customerItem.confirmNumber-customerItem.deliveryNum}','${customerItem.deliveryNum}');">&#xe725;</i>
												 <a href="javascript:void(0)" onclick="conversionType('${customerItem.id}','${customer.createId}');" class="layui-btn layui-btn-normal layui-btn-mini">
													<i class="layui-icon">&#xe669;</i>转为退货</a>
											</c:when>
											<c:otherwise>换货结束</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${customerItem.type==1}">退货</c:when>
									<c:when test="${customerItem.type==2}">换货转退货</c:when>
								</c:choose>
							</li>
						</ul>
						<c:if test="${customerItem.deliveryList !=null && customerItem.deliveryList.size()>0 }">
							<table style="width: 100%;">
								<thead>
									<tr style="background: #F7F7F7;height: 20px;">
										<td style="width:30px;">发货单号</td>
										<td style="width:30px;">发货类型</td>
										<td style="padding-top: 3px;width:30px;">发货数量</td>
										<td style="padding-top: 3px;width:30px;">到货数量</td>
										<td style="width:30px;">发货状态</td>
										<td style="width:30px;">发货日期</td>
										<td style="width:30px;">到货日期</td>
									</tr>
								<c:forEach items="${customerItem.deliveryList}" var="deliveryItem">
									<tr>
										<td>${deliveryItem.deliver_no}</td>
										<td>
											<c:choose>
												<c:when test="${deliveryItem.dropship_type==0}">正常发货</c:when>
												<c:when test="${deliveryItem.dropship_type==1}">代发客户</c:when>
												<c:when test="${deliveryItem.dropship_type==2}">代发菜鸟仓</c:when>
												<c:when test="${deliveryItem.dropship_type==3}">代发京东仓</c:when>
											</c:choose>
										</td>
										<td style="padding-top: 3px;">${deliveryItem.delivery_num}</td>
										<c:choose>
											<c:when test="${deliveryItem.recordstatus==0}">
												<!-- 待收货 -->
												<td style="padding-top: 3px;">待收货</td>
												<td>待收货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>待收货</td>
											</c:when>
											<c:when test="${deliveryItem.recordstatus==1}">
												<!-- 已收货 -->
												<td style="padding-top: 3px;">${deliveryItem.arrival_num}</td>
												<td>已收货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.arrival_date}" type="date"/>
												</td>
											</c:when>
										</c:choose>
									</tr>
								</c:forEach>
								</thead>
							</table>
						</c:if>
					</c:forEach>
					</td>
					<td style="width: 10%">${customer.reason}</td>
					<td style="width: 10%">
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待内部审批</c:when>
								<c:when test="${customer.status==1}">待卖家同意</c:when>
								<c:when test="${customer.status==2}">已驳回</c:when>
								<c:when test="${customer.status==3}">已取消</c:when>
								<c:when test="${customer.status==4}">卖家已收货</c:when>
								<c:when test="${customer.status==5}">待卖家收货</c:when>
							</c:choose>
						</div>
						<div>
							<c:choose>
								<c:when test="${customer.arrivalType==1}">oms已出库</c:when>
								<c:otherwise>oms未出库</c:otherwise>
							</c:choose>
						</div>
						<div class="opinion_view">
							<span class="orange" data="${customer.id}">查看审批流程</span>
						</div>
					</td>
					<td style="width: 10%">
						<a href="javascript:void(0)" button="详情" onclick="leftMenuClick(this,'platform/buyer/customer/loadCustomerDetails?id=${customer.id}'+'&returnUrl=platform/buyer/customer/customerList&menuId=17091216212559741744','buyer','17112215171918654466')" class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe695;</i>详情</a>
						<c:if test="${customer.status == 0 || customer.status == 2}">
							<a href="javascript:void(0)" button="修改" onclick="leftMenuClick(this,'platform/buyer/customer/updateCustomer?id=${customer.id}','buyer','17112214564049463774');" class="layui-btn layui-btn-mini">
								<i class="layui-icon">&#xe8fd;</i>修改</a>
						</c:if>
						<c:if test="${customer.status == 0}">
							<a href="javascript:void(0)" button="审批" onclick="verify('${customer.id}');" class="layui-btn layui-btn-danger layui-btn-mini">
								<i class="layui-icon">&#xe67d;</i>审批</a>
							<a href="javascript:void(0)" button="取消" onclick="cancelApply('${customer.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
								<i class="layui-icon">&#xe7ea;</i>取消</a>
						</c:if>
						<c:if test="${customer.status == 5 && customer.pushType == 0}">
							<!-- 卖家同意售后 -->
							<a href="javascript:void(0)" button="推送OMS" onclick="pushOms('${customer.id}');" class="layui-btn layui-btn-primary layui-btn-mini">
								推送oms出库</a>
						</c:if>
					</td>
				</tr>
			</table>
	</c:forEach>
		</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>