<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<script>
$(function(){
	setSystemTime();
});
</script>
<div class="container">
    <div class="logo">
    	<ul class="layui-nav left fast-add" lay-filter="">
          	<li class="layui-nav-item"><a href="javascript:;" id="parentMenuShow">我是买家</a>
				<dl class="layui-nav-child" id="parentMenuDl">
				<c:forEach var="parentMenu" items="${sessionScope.AUTHORITYSYSMENU}">
	          		<dd dataId="${parentMenu.id}" dataName="${parentMenu.name}"><a href="javascript:;" onclick="loadMenuHtml('${parentMenu.id}','${parentMenu.name}')"><i class="layui-icon">${parentMenu.icon}</i>${parentMenu.name}</a></dd>
				</c:forEach>
	        	</dl>
	       </li>
		</ul>
   	</div>
    <div class="left_open">
        <i title="展开左侧栏" class="iconfont">&#xe699;</i>
    </div>
    <ul class="layui-nav left fast-add" lay-filter="">
      <li>
      	<!-- <iframe width="650" scrolling="no" height="60" frameborder="0" allowtransparency="true"
			src="http://i.tianqi.com/index.php?c=code&id=12&color=%23C6C6C6&icon=1&num=5"></iframe>-->
      </li>
    </ul>
    <ul class="layui-nav right" lay-filter="">
      <li class="layui-nav-item">
        <a href="javascript:;">${sessionScope.CURRENT_USER.userName}</a>
      </li>
      <li class="layui-nav-item to-index">
      	<span id="nowDateTimeSpan">2017年3月3日 星期五</span>
     </li>
     <li class="layui-nav-item to-index">
     	<a href="<%=basePath%>platform/loadOut" class="friend"><b></b>退出</a>
     </li>
    </ul>
</div>