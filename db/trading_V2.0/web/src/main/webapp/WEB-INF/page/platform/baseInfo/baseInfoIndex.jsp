<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="keywords" content="诺泰，诺泰买卖，买卖系统，nuotai">
  <meta name="description" content=" 诺泰买卖系统是用于商家向供应商采购商品。">
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>诺泰买卖系统</title>
  <%@ include file="../common/common.jsp"%>
  <script type="text/javascript">
  	$(function(){
  		var jump = "<%=request.getParameter("jump")%>";
  		if(jump != '' && jump != 'null'){
  			var b = new Base64();
  			jump = b.decode(jump);
  			var jumpObj = JSON.parse(jump);
  			leftMenuClick(this,jumpObj.url,jumpObj.type,jumpObj.id);
  		}else{
			leftMenuClick(this,'platform/tradeVerify/waitForMeList','baseinfo','17082811062290245539');
  		}
  	});
  </script>
</head>
<body>
<!-- 顶部 -->
<%@ include file="../common/top.jsp"%>
<!-- 顶部菜单 -->
<jsp:include page="../common/title.jsp" >
	<jsp:param name="menuId" value="${sessionScope.menuId}" />
</jsp:include>
<div class="menu">
   <div class="menu-top">
       <div id="mini" style="border-bottom:1px solid rgba(255,255,255,.1)">
       	<img src="<%=basePath%>/statics/menu/images/mini.png">
       </div>
    </div>
	<!--左侧标题-->
	<jsp:include page="common/baseInfoMenu.jsp" >
		<jsp:param name="menuId" value="${sessionScope.menuId}" />
	</jsp:include>
</div>
<div class="container">
	<section class="section">
	    <!--内容-->
		<div class="rt_detail">
	    	<div class="content">内容</div>
	  	</div>
  	</section>
</div>
<!--返回顶部按钮-->
<div class="toTop">
  <a href="javascript:scroll(0,0)"><img src="<%=basePath%>statics/platform/images/sidebar_4.png"></a>
</div>
</body>
</html>