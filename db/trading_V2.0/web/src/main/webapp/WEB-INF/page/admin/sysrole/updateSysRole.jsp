<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	$("#updateSysRoleForm").validate({
		rules : {
			roleName : {
				required : true,
				maxlength : 20
			},
			remark : {
				maxlength : 200
			}
		},
		errorClass : "help-inline",
		errorElement : "span",
		highlight : function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('error');
		},
		unhighlight : function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('error');
			$(element).parents('.form-group').addClass('success');
		},
		submitHandler : function() {
			$.ajax({
				url : basePath+"admin/adminSysRole/saveUpdate",
				type : "post",
				data : $("#updateSysRoleForm").serialize(),
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						loadAdminData();
						//阻止表单提交
						return false;
					} else {
						layer.msg(resultObj.msg,{icon:2});
						//阻止表单提交
						return false;
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
					//阻止表单提交
					return false;
				}
			});
		}
	});
</script>
<form class="form-inline " id="updateSysRoleForm"
	novalidate="novalidate">
	<div class="modal-content" style="width: 600px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">修改系统角色</h4>
		</div>
		<div class="modal-body row-fluid ">
			<div class=" padding-top-sm row">
				<div class="form-group col-md">
					<label class="label-four">角色代码：</label> 
					<input type="text" value="${sysRole.roleCode}" readonly="readonly"
						class="form-control-new" name="roleCode" placeholder="角色代码"
						style="width: 60%">
				</div>
				<div class="form-group col-md padding-top-sm">
					<label for="exampleInputEmail2" class="label-four">角色名称：</label> <input
						value="${sysRole.roleName}" type="text" class="form-control-new"
						name="roleName" placeholder="角色名" style="width: 60%">
				</div>
				<div class="form-group col-md padding-top-sm">
					<label class="label-two">状态：
					<c:if test="${sysRole.status == 0}">
						<input type="radio" name="status" checked value="0"/>启用
						<input type="radio" name="status" value="1"/>停用
					</c:if>
					<c:if test="${sysRole.status == 1}">
						<input type="radio" name="status"  value="0"/>启用
						<input type="radio" name="status" checked value="1"/>停用
					</c:if>
					</lable>
				</div>
				<div class="form-group col-md padding-top-sm">
					<label>是否管理员：
					<c:if test="${sysRole.isAdmin == 0}">
						<input type="radio" name="isAdmin" checked value="0"/>否
						<input type="radio" name="isAdmin" value="1"/>是
					</c:if>
					<c:if test="${sysRole.isAdmin == 1}">
						<input type="radio" name="isAdmin"  value="0"/>否
						<input type="radio" name="isAdmin" checked value="1"/>是
					</c:if>
					</lable>
				</div>
				<div class="form-group form-group-textarea col-md-12 padding-top-sm">
					<label class="label-remark">备注：</label>
					<textarea class="form-control" rows="3" name="remark"
						style="width:64% !important">${sysRole.remark}</textarea>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-sm2 btn-default" data-dismiss="modal">取消</button>
			<button type="submit" class="btn btn-sm2 btn-00967b" id="saveButton">保存</button>
		</div>
	</div>
	<!--隐藏域  -->
	<input type="hidden" name="id" value="${sysRole.id}">
</form>