<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增账单周期</title>
    <%--<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billcycles/addBillCycle.js"></script>--%>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/bill.css">
    <script src="<%=basePath%>statics/libs/jquery.min.js"></script>
    <%--<script src="<%=basePath%>statics/plugins/layui/layui.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/jquery-ui-multiselect/jquery.multiselect.css" />
    <link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/jquery-ui-multiselect/jquery.multiselect.filter.css" />
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/jquery-ui-1.11.4/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/jquery-ui-multiselect/style.css" />
    <link rel="stylesheet" type="text/css" href="<%=basePath%>statics/platform/css/jquery-ui-multiselect/prettify.css" />


    <script type="text/javascript" src="<%=basePath%>statics/platform/css/jquery-ui-1.11.4/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=basePath%>statics/platform/css/jquery-ui-multiselect/prettify.js"></script>
    <script type="text/javascript" src="<%=basePath%>statics/platform/js/jquery-ui-multiselect/jquery.multiselect.js"></script>
    <script type="text/javascript" src="<%=basePath%>statics/platform/js/jquery-ui-multiselect/jquery.multiselect.filter.js"></script>
    <script src="<%=basePath%>statics/platform/js/colResizable-1.6.min.js"></script>
    <script type="text/javascript">

     //$(function(){
     var form;
     layui.use('form', function() {
         form = layui.form;
         form.render("select");

         $("#sela").multiselect({
             header:true,
             //height: 175,
             //minWidth:200,
             classes:'',
             noneSelectedText: "==请选择==",
             checkAllText: "选中全部",
             uncheckAllText: '取消全选',
             selecteText:'#选中',
             selectedList:1000000,
             //show:null,
             //hide:null,
             //autoOpen:false,
             //multiple:true,
             //position:{},
            //appendTo:"body",
             //menuWidth:nul  multiselectfilter:"搜索",

         });
    	 //关联商家下拉
    	 loadSupplier("");
    	 
    	 //提交账单周期信息
    	 $('#billCycleInfo_add').click(function(){
    		 var date = new Date();
             var month = date.getMonth() + 1;
             var strDate = date.getDate();
             if (month >= 1 && month <= 9) {
                 month = "0" + month;
             }
             if (strDate >= 0 && strDate <= 9) {
                 strDate = "0" + strDate;
             }
             var nowDate = date.getFullYear() +"-"+ month+"-"+strDate;

    		 var cycleStartDate=$("#startDate").val();
    		 if(cycleStartDate == ''){
    				layer.msg("请选择账期开始日期！",{icon:2});
    				return;
    			}else if(cycleStartDate > nowDate){
                 layer.msg("开始日期不得大于操作日期！",{icon:2});
                 return;
             }
    		 var cycleEndDate=$("#endDate").val();
    		 if(cycleEndDate == ''){
 				layer.msg("请选择账期终止日期！",{icon:2});
 				return;
 			   }else if(cycleEndDate > nowDate){
                 layer.msg("终止日期不得大于操作日期！",{icon:2});
                 return;
             }
    		//供应商判空
             //供应商判空
             var supplierIdArr=$("#addBillCycleDiv #buyersId").val();
   			if(supplierIdArr == ''){
   				layer.msg("请选择采购商！",{icon:2});
   				return;
   			}
   			var companyId=supplierIdArr.split(",")[0];
            var supplierNameArr=$("#addBillCycleDiv #buyersId").text();

             //获取创建人
             var createUserId = $("#sela").val();
             var createUserName = $("#selcetText").text();

             var createUserIdStr = "";
             if(null != createUserId){
                 if( createUserId.length > 0){
                     //if(createUserId.length > 1){
                         for(i = 0;createUserId.length > i;i++){
                             createUserIdStr = createUserIdStr+createUserId[i]+",";
                         }
                         createUserIdStr = createUserIdStr.substring(0,createUserIdStr.length -1).trim();
                     /*}else {
                         createUserIdStr = createUserId;
                     }*/
                 }else {
                     createUserIdStr = "";
                 }
             }else {
                 createUserIdStr = "";
             }
             var createUserNameStr = "";

             //alert("建单名称长度"+createUserNames.length);
             if(null != createUserName){
                 var createUserNames = createUserName.split(",");
                 if(createUserNames.length > 0){
                     //if(createUserNames.length > 1){
                         for(i = 0;createUserNames.length > i;i++){
                             createUserNameStr = createUserNameStr+createUserNames[i]+",";
                         }
                         createUserNameStr = createUserNameStr.substring(0,createUserNameStr.length -1).trim();
                    /* }else {
                         createUserNameStr = createUserName;
                     }*/
                 }else {
                     createUserNameStr = "";
                 }
             }else {
                 createUserNameStr = "";
             }


           //保存数据
         var url = basePath+"platform/seller/billReconciliation/saveReconciliationInfo";
         $.ajax({
             type : "POST",
             url : url,
             async:false,
             data: {
                 "startArrivalDate":cycleStartDate,
                 "endArrivalDate":cycleEndDate,
                 "companyId":companyId,
                 "companyName":supplierNameArr,
                 "createUserId":createUserIdStr,
                 "createUserName":createUserNameStr,
             },
             success:function(data){
                 var result = eval("(" + data + ")");
                 var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
                 if(resultObj.success){
                     layer.msg(resultObj.msg,{icon:1});
                     leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers')
                 }else{
                     layer.msg(resultObj.msg,{icon:2});
                 }
             },
             error:function(){
                 layer.msg("保存失败，请稍后重试！",{icon:2});
             }
         });
        });
     });
    //保存成功
   function saveSucess(){
        $.ajax({
            url:"platform/buyer/billCycle/success",
            success:function(data){
                var str = data.toString();
                $(".content").html(str);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }
   //加载互通卖家好友下拉
     function loadSupplier(supplierId){
     	var obj={};
     	obj.divId="addBillCycleSeller";
     	obj.buyersName="buyersName";
     	obj.buyersId="buyersId";
     	obj.filter="changeSupplier";
     	obj.selectValue=supplierId;
     	recBuyerSelect(obj);
     }

     // 采购商下拉共通
     function recBuyerSelect(obj){
         var url = basePath+"platform/common/getBuyerSelect"
         $.ajax({
             url : url,
             async:false,
             success:function(data){
                 var result = eval('(' + data + ')');
                 var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                 var selectObj = $("<select lay-filter='"+obj.filter+"' lay-search=''></select>");
                 if(obj.buyersName != null && obj.buyersName != ''){
                     selectObj.attr("name",obj.buyersName);
                 }
                 if(obj.buyersId != null && obj.buyersId != ''){
                     selectObj.attr("id",obj.buyersId);
                 }
                 $('<option>',{val:"",text:"请选择或输入关键字搜索"}).appendTo(selectObj);
                 var selectValue = obj.selectValue;
                 $.each(resultObj,function(i){
                     var buyersId = resultObj[i].buyersId;
                     var buyersName = resultObj[i].buyersName;
                     var buyersPerson = resultObj[i].buyersPerson;
                     var buyersPhone = resultObj[i].buyersPhone;
                     if(selectValue != null && selectValue != ''){
                         if(selectValue==buyersId){
                             $('<option>',{val:buyersId+","+buyersPerson+","+buyersPhone,text:buyersName,selected:"selected"}).appendTo(selectObj);
                             return;
                         }
                     }
                     $('<option>',{val:buyersId+","+buyersPerson+","+buyersPhone,text:buyersName}).appendTo(selectObj);
                 });
                 $("#"+obj.divId).append(selectObj);
             },
             error:function(){
                 layer.msg("获取数据失败，请稍后重试！",{icon:2});
             }
         });
     }

     //查询订单创建人
    $('#sela_ms').click(function () {

         var date = new Date();
         var month = date.getMonth() + 1;
         var strDate = date.getDate();
         if (month >= 1 && month <= 9) {
             month = "0" + month;
         }
         if (strDate >= 0 && strDate <= 9) {
             strDate = "0" + strDate;
         }
         var nowDate = date.getFullYear() +"-"+ month+"-"+strDate;

         var cycleStartDate=$("#startDate").val();
         if(cycleStartDate == ''){
             layer.msg("请选择账期开始日期！",{icon:2});
             return;
         }else if(cycleStartDate > nowDate){
             layer.msg("开始日期不得大于操作日期！",{icon:2});
             return;
         }
         var cycleEndDate=$("#endDate").val();
         if(cycleEndDate == ''){
             layer.msg("请选择账期终止日期！",{icon:2});
             return;
         }else if(cycleEndDate > nowDate){
             layer.msg("终止日期不得大于操作日期！",{icon:2});
             return;
         }
         //供应商判空
         var supplierIdArr=$("#addBillCycleDiv #buyersId").val();
         if(supplierIdArr == ''){
             layer.msg("请选择采购商！",{icon:2});
             return;
         }
         var companyId=supplierIdArr.split(",")[0];
         var supplierNameArr=$("#addBillCycleDiv #buyersId").text();
        var supplierNameArrss = supplierNameArr.substring(11,supplierNameArr.length);

         //根据周期和公司查询建单人
         var url = basePath+"platform/seller/billReconciliation/queryCreateBillUser";
         $.ajax({
             type : "POST",
             url : url,
             async:false,
             data: {
                 "startArrivalDate":cycleStartDate,
                 "endArrivalDate":cycleEndDate,
                 "companyId":companyId,
                 "companyName":supplierNameArrss
             },
             success:function(data){
                 var result = eval("(" + data + ")");
                 var resultObj = isJSONObject(result)?result:eval("(" + result + ")");
                 if(resultObj.success){
                     var createUserMap = resultObj.createBillUserLink;
                     for(i = 0;i<createUserMap.length;i++){
                         debugger
                         if(null !=createUserMap[i]){
                             debugger
                             var buyersId = createUserMap[i].createId;
                             var buyersName = createUserMap[i].createName;
                             //alert("编号"+buyersId);
                             //alert("名称"+buyersName);
                             $("#sela").append("<option value='" + buyersId + "'>" + buyersName + "</option>");
                         }
                     }
                     $("#sela").multiselect("destroy").multiselect({
                         // 自定义参数，按自己需求定义
                         height: 175,
                         classes:'',
                         noneSelectedText: "==请选择==",
                         checkAllText: "选中全部",
                         uncheckAllText: '取消全选',
                         selecteText:'#选中',
                         selectedList:1000000,
                         autoOpen:true,
                     });
                     $("#sela").multiselectfilter({
                         label:"搜索:",
                         placeholder:"请输入姓名关键字",
                         width: 150,
                     });
                 }else{
                     layer.msg(resultObj.msg,{icon:2});
                 }
             },
             error:function(){
                 layer.msg("获取失败，请稍后重试！",{icon:2});
             }
         });
     });

    </script>
</head>
<div id="addBillCycleDiv">
 <!--  <form id="addForm" name="addForm" action=""> -->
  <div>
      <form class="layui-form">
    <div class="bill_cycle">
        <label class="labelStyle">账单周期:</label>
            <%--<label >起始日期</label>--%>
            <div class="layui-input-inline">
                <input type="text" name="startBillStatementDate" id="startDate" lay-verify="date" value="${params.startBillStatementDate}" class="layui-input" placeholder="开始日">
            </div>
        至
           <%-- <label >终止日期</label>--%>
            <div class="layui-input-inline">
                 <input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" value="${params.endBillStatementDate}" class="layui-input" placeholder="截止日">
            </div>
    </div>
    <div class="layui-inline">
      <label class="labelStyle">采购商名称:</label>
      <div class="layui-input-inline" style="width:200px" id="addBillCycleSeller">
      </div>
    </div>
    <div class="noselect">
      <label class="labelStyle">采购/售后订单创建人:</label>
        <select id ="sela" multiple="multiple" style="overflow-y: auto">
            <%--<option value="t1">上海</option>
            <option value="t2">武汉</option>
            <option value="t3">成都</option>
            <option value="t4">北京</option>
            <option value="t5">南京</option>--%>
        </select>
    </div>
      </form>
      <style>
          .labelStyle{float:left;display:block;padding:9px 15px;width:200px;font-weight:400;text-align:right}
          .noselect .layui-form-select{
              display: none;
          }
          .ui-multiselect{
              height:30px;
          }
      </style>
    <%--<div>
      <div id="interstHidOrShow" style="display: none">
      <label class="layui-form-label">利息设置:</label>
      <button class="layui-btn layui-btn-danger layui-btn-mini" id="interest_add" ><i class="layui-icon">&#xe6ab;</i> 新增项</button>
      <div class="bill_interest mp">
        <table class="table_pure">
          <thead>
          <tr>
            <td style="width:25%">逾期（天）</td>
            <td style="width:25%">月利率（%）</td>
            <td style="width:30%">利息计算方式</td>
            <td style="width:20%">操作</td>
          </tr>
          </thead>
          <tbody id="interestBody">
          </tbody>
        </table>
      </div>
      </div>
    </div>--%>

    </div>
    
  <div class="text-center mp30">
    <a href="javascript:void(0);">
    <button class="layui-btn layui-btn-normal layui-btn-small" id="billCycleInfo_add">确定生成账单</button>
    <span class="layui-btn layui-btn-small" onclick="leftMenuClick(this,'platform/seller/billReconciliation/billReconciliationList','sellers');">返回账单列表</span>
    <%--<button class="layui-btn layui-btn-small" onclick="hidOrShowInterst()">利息设置</button>--%>
    </a>
  </div>
</div>
 
