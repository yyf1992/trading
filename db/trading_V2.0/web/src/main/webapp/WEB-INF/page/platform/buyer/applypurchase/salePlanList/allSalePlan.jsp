<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!--列表区-->
<table class="order_detail">
	<tr>
		<td style="width:80%">
			<ul>
				<li style="width:36%">商品</li>
				<li style="width:8%">条形码</li>
				<li style="width:10%">部门</li>
				<li style="width:10%">销售计划</li>
				<li style="width:8%">入仓量</li>
				<li style="width:10%">已生成销售计划</li>
				<li style="width:10%">实际销售量</li>
				<li style="width:7%">销售达成率</li>
			</ul>
		</td>
		<td style="width:10%">备注</td>
		<td style="width:10%">状态</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<div class="salePlanContentDiv">
	<c:forEach var="salePlan" items="${searchPageUtil.page.list}">
		<div class="order_list">
			<p>
				<span class="layui-col-sm2">计划编号:<b>${salePlan.planCode}</b>
				</span>
				<span class="layui-col-sm3">标题:<b>${salePlan.title}</b>
				</span>
				<span class="layui-col-sm2">创建人:<b>${salePlan.createName}</b>
				</span>
				<span class="layui-col-sm2">销售周期: <b> <fmt:formatDate
							value="${salePlan.startDate}" type="date"></fmt:formatDate> 至 <fmt:formatDate
							value="${salePlan.endDate}" type="date"></fmt:formatDate> </b> </span>
				<span class="layui-col-sm2">创建日期:<b><fmt:formatDate
							value="${salePlan.createDate}" type="both"></fmt:formatDate></b>
				</span>
			</p>
			<table>
				<tr>
					<td style="width:80%"><c:forEach var="salePlanItem"
							items="${salePlan.itemList}">
							<ul class="clear">
								<li style="width:35%">
										${salePlanItem.productCode}|${salePlanItem.productName}|${salePlanItem.skuCode}|${salePlanItem.skuName}
								</li>
								<li style="width:10%">${salePlanItem.barcode}</li>
								<li style="width:10%">${salePlanItem.shopName}</li>
								<li style="width:10%">${salePlanItem.salesNum}</li>
								<li style="width:8%">${salePlanItem.putStorageNum}</li>
								<li style="width:10%">${salePlanItem.confirmSalesNum}</li>
								<li style="width:10%">${salePlanItem.num}</li>
								<li style="width:7%">
									<c:if test="${salePlanItem.salesCompletionRate != null}">
										${salePlanItem.salesCompletionRate*100}%
										<i class="layui-icon" style="font-size:1px;cursor:pointer;" title="销售率=实际销售/(销售计划+入仓量)">&#xe725;</i>
									</c:if>
								</li>
							</ul>
						</c:forEach>
					</td>
					<td style="width:10%">${salePlan.remark}</td>
					<td style="width:8%">
						<div>
							<c:choose>
								<c:when test="${salePlan.status==0}">等待审核</c:when>
								<c:when test="${salePlan.status==1}">审核通过</c:when>
								<c:when test="${salePlan.status==2}">审核不通过</c:when>
							</c:choose>
						</div>
						<div class="opinion_view">
							<span class="orange" data="${salePlan.id}">查看审批流程</span>
						</div>
					</td>
					<td style="width:12%">
						<a href="javascript:void(0)" button="详情"
							onclick="leftMenuClick(this,'buyer/salePlan/loadSalePlanDetails?id=${salePlan.id}','buyer','18011115560680331888');"
							class="layui-btn layui-btn-normal layui-btn-mini">
							<i class="layui-icon">&#xe695;</i>详情</a>
						<c:if test="${salePlan.status == 0}">
							<a href="javascript:void(0)" button="审批" verifyId="${salePlan.id}"
								<%-- onclick="verify('${salePlan.id}');" --%>
								class="layui-btn layui-btn-danger layui-btn-mini">
								<i class="layui-icon">&#xe67d;</i>审批</a>
						</c:if>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
<script>
$(function(){
	$("a[button='审批']").click(function(){
		var verifyId = $(this).attr("verifyId");
		if($(".verifyDetail").length>0)$(".verifyDetail").remove();
		$.ajax({
			url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
			data:{"id":verifyId},
			success:function(data){
				var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
				detailDiv.addClass("verifyDetail");
				detailDiv.html(data);
				detailDiv.appendTo($("#salePlanContent"));
				detailDiv.find("#verify_side").addClass("show");
			},
			error:function(){
				layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
			}
		});
	});
})
</script>