<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(function(){
	//加载审批类型
	loadVerifyType("${searchPageUtil.object.verifyType}");
	$(".verifyDiv table tbody tr").click(function(e){
		trClick($(this));
	});
});
</script>
<div class="verifyDiv">
	<form action="platform/tradeVerify/waitForMeList" id="searchForm">
		<ul class="order_search supplier_query">
			<li class="comm">
				<label>审批类型:</label>
				<span id="verifyTypeSpan"></span>
			</li>
			<li class="nomargin">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
		</ul>
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		<table class="table_pure supplierList mt">
			<thead>
				<tr>
					<td style="width:15%">审批编号</td>
					<td style="width:15%">审批标题</td>
					<td style="width:10%">申请人</td>
					<td style="width:15%">发起时间</td>
					<td style="width:15%">到达我审批的时间</td>
					<td style="width:10%">状态</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="verifyHeader" items="${searchPageUtil.page.list}">
					<tr>
						<td>${verifyHeader.id}</td>
						<td>${verifyHeader.create_name}的【${verifyHeader.title}】</td>
						<td>${verifyHeader.create_name}</td>
						<td><fmt:formatDate value="${verifyHeader.hCreated}" type="both"/></td>
						<td><fmt:formatDate value="${verifyHeader.start_date}" type="both"/></td>
						<td>${verifyHeader.user_name}审批中</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page}</div>
	</form>
</div>