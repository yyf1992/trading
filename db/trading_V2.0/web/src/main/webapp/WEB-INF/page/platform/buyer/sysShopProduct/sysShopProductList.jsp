<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
function editPrice(id){
	$.ajax({
		url:"platform/buyer/sysshopproduct/loadEditPrice",
		async:false,
		type:"post",
		data:{"id":id},
		success:function(data){
			layer.open({
			type:1,
			title:"修改价格",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:"platform/buyer/sysshopproduct/saveEditPrice",
  		    		data:$("#editPriceForm").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385'); 
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385'); 
						}
				},
  		    		error:function(){
  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//导入店铺价格
function loadImportHtml(){
	$.ajax({
			url:"platform/buyer/sysshopproduct/loadImportExcelHtml",
			type:"post",
			async:false,
			success:function(data){
				layer.open({
					type:1,
					title:"导入数据",
					skin: 'layui-layer-rim',
	  		    	area: ['400px', 'auto'],
	  		    	content:data,
	  		    	btn:['确定','取消','下载模板'],
					yes:function(){
						var formData = new FormData();
						formData.append("excel", document.getElementById("file1").files[0]);
						$.ajax({
							url : "platform/buyer/sysshopproduct/importShopPriceData",
							async : false,
							data : formData,
							type : "post",
							/**
							 *必须false才会自动加上正确的Content-Type
							 */
							contentType : false,
							/**
							 * 必须false才会避开jQuery对 formdata 的默认处理
							 * XMLHttpRequest会对 formdata 进行正确的处理
							 */
							processData : false,
							success : function(data) {
								var result = eval('(' + data + ')');
								var resultObj = isJSONObject(result) ? result: eval('(' + result + ')');
								if(resultObj.success){
									layer.closeAll();
									layer.msg(resultObj.msg, {icon : 1});
									leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385');
								}else{
									layer.msg(resultObj.msg, {icon : 2});
								}
							},
							error: function(data) {
								layer.closeAll();
								layer.msg(resultObj.msg, {icon : 2});
								leftMenuClick(this,'platform/buyer/sysshopproduct/loadGoodsPriceList','buyer','17081511320249044385');
							}
						});
					},btn3:function(){
					    var url ="platform/buyer/sysshopproduct/downloadExcel";  
					    url = encodeURI(url);
					    location.href = url;  
					}
				})
			},error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
	});
	}
</script>
<div class="content_role">
	<form class="layui-form"
		action="platform/buyer/sysshopproduct/loadGoodsPriceList">
		<ul class="order_search platformSearch">
			<li class="state"><label>货号：</label> <input type="text"
				placeholder="输入货号" name="productCode"
				value="${searchPageUtil.object.productCode}"></li>
			<li class="state"><label>条形码：</label> <input type="text"
				placeholder="输入条形码" name="barcode"
				value="${searchPageUtil.object.barcode}"></li>
			<li class="state"><label>店铺：</label>
				<div class="layui-input-inline">
					<select name="shopId">
						<option value="-1">全部</option>
						<c:forEach var="SysShop" items="${sysShopList}">
							<option value="${SysShop.id}" id="${SysShop.id}"
								<c:if test="${param.shopId==SysShop.id}">selected="selected"</c:if>>${SysShop.shopName}</option>
						</c:forEach>
					</select>
				</div></li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
		</ul>
	</form>

	<div class="newBuild mt">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/sysshopproduct/loadAddShopHtml','buyer','17081511320249044385');" button="设置出货价格"><button>设置出货价格</button>
		</a>
		<a href="javascript:void(0)" onclick="loadImportHtml();" button="导入"><button>导入</button>
		</a>
	</div>
	
	<table class="table_pure platformList">
		<thead>
			<tr>
				<td style="width:10%">货号</td>
				<td style="width:10%">规格名称</td>
				<td style="width:10%">条形码</td>
				<td style="width:15%">所属店铺</td>
				<td style="width:10%">商品成本</td>
				<td style="width:10%">品牌中心出货价</td>
				<td style="width:15%">操作日期</td>
				<td style="width:10%">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="SysShopProduct" items="${searchPageUtil.page.list}"
				varStatus="status">
				<tr>
					<td>${SysShopProduct.productCode}</td>
					<td>${SysShopProduct.skuName}</td>
					<td>${SysShopProduct.barcode}</td>
					<td>${SysShopProduct.shopName}</td>
					<td>${SysShopProduct.costPrice}</td>
					<td>${SysShopProduct.sellPrice}</td>
					<td><c:choose>
							<c:when test="${SysShopProduct.updateDate==null}">
								<fmt:formatDate value="${SysShopProduct.createDate}" type="both" />
							</c:when>
							<c:otherwise>
								<fmt:formatDate value="${SysShopProduct.updateDate}" type="both" />
							</c:otherwise>
						</c:choose>
					<td><span class="layui-btn layui-btn-mini" button="修改"
						onclick="editPrice('${SysShopProduct.id}');"><b></b>修改</span></td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
	<div class="pager">${searchPageUtil.page}</div>
</div>
