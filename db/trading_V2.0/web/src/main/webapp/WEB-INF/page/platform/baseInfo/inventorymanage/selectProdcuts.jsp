<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/commodity_all.css">
<title>商品库存历史表</title>
<script type="text/javascript">
function select(){
	var startDate = $("#startDate").val();
	if(startDate == ''){
		/*layer.tips('请选择开始时间！', '#startDate',{
			tips: [1, '#f00'],
  			time: 2000
		});*/
		return;
	}
	var endDate = $("#endDate").val();
	if(endDate == ''){
		/*layer.tips('请选择结束时间！', '#endDate',{
			tips: [1, '#f00'],
  			time: 2000
		});*/
		return;
	}
	loadPlatformData();
}

layui.use('laydate', function(){
	
//	var myDate = new Date();
//	myDate.setTime(myDate.getTime()-24*60*60*1000);
	  var laydate = layui.laydate;
	  //限定可选日期
	  var ins22 = laydate.render({
	    elem: '#startDate'
	    ,min: '1970-10-14'
	    ,max: -1
	    ,btns: ['clear']
	    ,ready: function(){
	  //ins22.hint('日期可选值设定在 <br> 今天之前');
	    }
	  });
	  var ins23 = laydate.render({
		    elem: '#endDate'
		    ,min: '1970-10-14'
		    ,max: -1
		    ,btns: ['clear']
		    ,ready: function(){
		//ins22.hint('日期可选值设定在 <br> 今天之前');
		    }
	  });
	
});
</script>

<div id="prodcutshistoryContent">
  <!--搜索栏-->
  <form class="layui-form" action="platform/baseInfo/inventorymanage/selectProdcuts">
    <ul class="order_search summary_list">
      <li class="range">
         <label>日&nbsp;期:</label>
         <div class="layui-input-inline">
             <!--   <input class="layui-input" placeholder="开始日" id="startDate" name="startDate" value="" required> -->
             <input type="text" name="startDate" id="startDate" value="${params.startDate}"  lay-verify="date" placeholder="开始日" class="layui-input" style="width: 100px" required>
         </div>
         <div class="layui-input-inline">
             <!--  <input class="layui-input" placeholder="截止日" id="endDate" name="endDate" value="" required>  -->
               <input type="text" name="endDate" id="endDate" value="${params.endDate}" lay-verify="date" placeholder="截止日" class="layui-input" style="width: 100px" required>
         </div>
      </li>
      <li>
        <label>商品名称:</label>
        <input type="text" placeholder="输入商品名称进行搜索" id="productName" name="productName" value="${params.productName }">
      </li>
      <li>
        <label>商品货号:</label>
        <input type="text" placeholder="输入商品货号进行搜索" id="productCode" name="productCode" value="${params.productCode }" style="width: 240px;"> 
      </li>
      <li class="">
        <label>条形码:</label>
        <input type="text" placeholder="输入条形码" id="barcode" name="barcode" value="${params.barcode}" style="width: 208px;">
      </li>
      <li>
        <label>仓&nbsp;&nbsp;库:</label>
        <div class="layui-input-inline">
            <select name="warehouseId" id="warehouseId" style="width: 208px">
				<option value="">请选择</option>
				<c:forEach var="warehouse" items="${warehouseList}">
					<option value="${warehouse.id}">${warehouse.whareaName}</option>
				</c:forEach>
			</select>
        </div>
      </li>
      <li class=" rt">
        <button class="search" onclick="select();" >搜索</button>
      </li>
    </ul>
    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
    <h3>说明：请选择开始时间和截止时间，再点击搜索</h3>
  <!--分页-->
  <div class="pager">${searchPageUtil.page}</div>
  </form>
</div>

