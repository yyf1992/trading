<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<!--物料清单弹框-->
<div class="bomTable">
  <div>
    <table class="table_yellow">
      <thead>
      <tr>
        <td width="10%">序号</td>
        <td width="20%">商品名称</td>
        <td width="16%">货号</td>
        <td width="15%">条形码</td>
        <td width="17%">规格代码</td>
        <td width="10%">单位</td>
        <td width="12%">配置数量</td>
      </tr>
      </thead>
      <tbody>
      	<c:forEach var='item' items="${materialList}" varStatus="status">
	      <tr>
	        <td>${status.count}</td>
	        <td>${item.product_name}</td>
	        <td>${item.product_code}</td>
	        <td>${item.sku_barcode}</td>
	        <td>${item.sku_code}</td>
	        <td>${item.unit_name}</td>
	        <td>${item.compose_num}</td>
	      </tr>
	  	</c:forEach>
      </tbody>
    </table>
  </div>
</div>
