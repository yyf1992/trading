<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/path.jsp"%>
<head>
    <title>新增合同</title>

    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/contracts.css">
    <script src="<%=basePath%>/js/contract/addContract.js"></script>
    <script type="text/javascript">
        $(function () {

        });
        var role ='buyer';
    </script>
</head>
<div>
    <div class="add_contract">
        <form class="contract_form layui-form" action="">
            <div>
                <label><span class="red">*</span> 发起人:</label>
                <input type="text" placeholder=""  value="${createrName}" readonly>
            </div>
            <div>
                <label><span class="red">*</span>
                    供应商：
                </label>
                <div class="layui-input-inline contract_input">
                    <select id="customerId" name="customerId" lay-verify="required" lay-search="">
                        <c:forEach var="customer" items="${customerList}">
                            <%--<c:choose>--%>
                                <%--<c:when test="${role=='seller'}">--%>
                                    <%--<option value="${customer.id}">${customer.clientName}</option>--%>
                                <%--</c:when>--%>
                                <%--<c:otherwise>--%>
                                    <%--<option value="${customer.sellerid}">${customer.sellerName}</option>--%>
                                <%--</c:otherwise>--%>
                            <%--</c:choose>--%>
                            <option value="${customer.sellerId}">${customer.sellerName}</option>
                        </c:forEach>
                    </select>
                </div>
                <%--<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small" onclick="addCustomer('${role}');">
                    <i class="layui-icon">&#xe613;</i>
                    <c:choose>
                        <c:when test="${role=='seller'}">
                            新增采购商
                        </c:when>
                        <c:otherwise>
                            新增供应商
                        </c:otherwise>
                    </c:choose>

                </a>--%>
            </div>
            <div>
                <label><span class="red">*</span> 联系人:</label>
                <input type="text" id="customerLinkman" name="customerLinkman" placeholder="" required>
            </div>
            <div>
                <label><span class="red">*</span> 手机号:</label>
                <input type="tel" id="customerPhone" name="customerPhone" placeholder="" required>
            </div>
            <div>
                <label><span class="red">*</span> 合同名称:</label>
                <input type="text" id="contractName" name="contractName" placeholder="" required>
            </div>
            <div>
                <label><span class="red">*</span> 合同编号:</label>
                <input type="text" id="contractNo" name="contractNo" placeholder="" required>
            </div>
            <div>
                <label>合同内容:</label>
                <div class="upload_license uploadPlan">
                    <input type="file" name="attachment1" id="attachment1" onchange="uploadContent(this);">
                    <input type="hidden" id="contentAddress" name="contentAddress" value="">
                    <p></p>
                    <span>支持后缀为doc,docx的word文件</span>
                </div>
                <div class="contract_show mp">
                    <div placeholder="发起方（单方）的合同文件电子版内容详情展示区" class="layui-textarea" id="contentArea" style="display: none;"></div>
                </div>
            </div>
            <div>
                <label>留言:</label>
                <div class="contract_show">
                    <textarea id="remark" name="remark" placeholder="" class="layui-textarea"></textarea>
                </div>
            </div>
            <input type="hidden" id="contractId" name="contractId" value="">
            <input type="hidden" id="createrId" name="createrId" value="">
            <input type="hidden" id="status" name="status" value="0">
        </form>
        <div class="contract_submit">
            <div class="layui-btn layui-btn-normal layui-btn-small"  onclick="submitContract();">确认提交</div>
            <div class="layui-btn layui-btn-danger layui-btn-small"  onclick="saveDraft();">保存草稿</div>
        </div>
    </div>
</div>