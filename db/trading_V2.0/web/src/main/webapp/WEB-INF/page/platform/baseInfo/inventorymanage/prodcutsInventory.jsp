<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/commodity_all.css">
<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/commodity_interwork.css">
<title>商品库存余额</title>
<style>
   .commDetail{
  display: inline-block;
  background: #35B8E9;
  color:#fff;
  padding: 5px;
  border-radius: 3px;
}
.order_search>li .reset{
    margin-left: 5px;
    background: #F6F5F5;
    color: #333;
    border: 1px solid #BFBFBF;
    border-radius: 3px;
    width: 128px;
    height: 25px;
    cursor: pointer;
    outline: 0;
    font-weight: bold;
}
.table_blue tbody tr{
	 height: 50px;
}
.table_blue tbody td:nth-child(2){
  text-align: center;
}
</style>
<script type="text/javascript">
//重置查询条件
function reset_search(){
	$("#searchForm").find("ul").find("li:eq(0)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(1)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(2)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(3)").find("input").val("");
	$("#searchForm").find("ul").find("li:eq(4)").find("input").val("");
}
function prodcutsDetails(obj){

	var parentDiv = $(obj).parent().parent();
	var id = parentDiv.find("#id").val();
	var barcode = parentDiv.find("#barcode").val();
	$.ajax({
		type: "POST",
		url: "platform/baseInfo/inventorymanage/prodcutsDetails",
		data:{
			"id": id,
			"barcode":barcode
		},
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
$(function(){
	$("#prodcuts tr").each(function() {
		
		var tdNum = $(this).find("td").length;
		if (tdNum == 9){
			$(this).append("<td></td")
		}
			
		var numTai1= $(this).find("td:eq(8)").html();
		var numTai2= $(this).find("td:eq(9)").html();
		var numTal = $(this).find("td:eq(7)").html(parseInt(numTai1)+parseInt(numTai2));
		var numZhi1= $(this).find("td:eq(11)").html();
		var numZhi2= $(this).find("td:eq(12)").html();
		var numZhi = $(this).find("td:eq(10)").html(parseInt(numZhi1)+parseInt(numZhi2));
		var numTui1= $(this).find("td:eq(14)").html();
		var numTui2= $(this).find("td:eq(15)").html();
		var numTui = $(this).find("td:eq(13)").html(parseInt(numTui1)+parseInt(numTui2));
	});
})
//重置
function recovery(){

	$("#productCode").val("");
	$("#goodsName").val("");
	$("#barcode").val("");
}
</script>
<div>
      <form id="searchForm" class="layui-form" action="platform/baseInfo/inventorymanage/prodcutsInventory">
       <ul class="order_search ">
         <li>
           <label>商品货号</label>:
           <input type="text" placeholder="输入商品货号" style="width:150px" id="productCode" name="productCode" value="${searchPageUtil.object.productCode}">
         </li>
         <li>
           <label>&nbsp;商品名称</label>:
           <input type="text" placeholder="输入商品名称" style="width:150px" id="goodsName" name="goodsName" value="${searchPageUtil.object.goodsName}">
         </li>
         <li>
           <label>&nbsp;条形码</label>:
           <input type="text" placeholder="输入条形码" style="width:150px" id="barcode" name="barcode" value="${searchPageUtil.object.barcode}">
         </li>
         <li>
           <button class="search" onclick="loadPlatformData();">搜索</button>
     	   <button type="button" class="search" onclick="recovery();">重置</button>
         </li>
       </ul>
      <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
  	  <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
      </form>
      <table class="table_blue" >
       <thead>
       <tr>
         <td style="width:8%" rowspan="2">商品货号</td>
         <td style="width:9%" rowspan="2">商品名称</td>
         <td style="width:10%" rowspan="2">规格代码</td>
         <td style="width:10%" rowspan="2">规格名称</td>
         <td style="width:8%" rowspan="2">条形码</td>
         <td style="width:5%" rowspan="2">成本价</td>
         <td style="width:5%" rowspan="2">总库存</td>
         <td style="width:14%" colspan="3">泰安大仓</td>
         <td style="width:14%" colspan="3">质检仓</td>
         <td style="width:14%" colspan="3">退换货仓</td>
       </tr>
       <tr>
       	<td>总库存</td>
       	<td>可用</td>
       	<td>锁定</td>
        <td>总库存</td>
       	<td>可用</td>
       	<td>锁定</td>
       	<td>总库存</td>
       	<td>可用</td>
       	<td>锁定</td>
       </tr>
       </thead>
       <tbody id="prodcuts">
       	 <c:forEach var="prodcutsList" items="${searchPageUtil.page.list}" varStatus="status">
	       <tr>	              	        
	         <td title='${prodcutsList.productCode}'>${prodcutsList.productCode}</td>
	         <td title='${prodcutsList.goodsName}' align="center">${prodcutsList.goodsName}</td>
	         <td title='${prodcutsList.skuCode}'>${prodcutsList.skuCode}</td>
	         <td title='${prodcutsList.skuName}'>${prodcutsList.skuName}</td>
	         <td title='${prodcutsList.barcode}'>${prodcutsList.barcode}</td>
	         <td title='${prodcutsList.cost}'>${prodcutsList.cost}</td>
	         <td title='${prodcutsList.warncount}'>${prodcutsList.warncount}</td>            
	         <c:forEach var="stock" items="${prodcutsList.stockMap}" varStatus="status">
	         	 <td>
	         	 	 
	         	 </td>
	         	 <td>
			         ${stock.value[1]}   				                 		         	
			     </td>			 
			     <td> 
			     	${stock.value[0]}
			     </td>    
		     </c:forEach>
	       </tr>
	     </c:forEach>
       </tbody>
     </table>
     <div class="pager">${searchPageUtil.page}</div>
   
 </div>