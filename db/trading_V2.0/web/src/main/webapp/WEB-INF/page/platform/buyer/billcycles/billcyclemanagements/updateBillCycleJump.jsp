<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增账单周期</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billcycles/updateBillCycle.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/statics/platform/css/bill.css">
    <script type="text/javascript">
    //var queryType = '${queryType}';
     $(function(){
    	 //关联商家下拉
    	 loadSupplier('${billCycleMapByUpdate.sellerCompanyId}');
    	 
    	 //提交账单周期信息
    	 $('#billCycleInfo_update').click(function(){
    		 
    		 /*var cycleStartDate=$("#updateBillCycleDiv input[name='cycleStartDate']").val();
    		 if(cycleStartDate == ''){
    				layer.msg("请选择账期开始日期！",{icon:2});
    				return;
    			}
    		 var cycleEndDate=$("#updateBillCycleDiv input[name='cycleEndDate']").val();
    		 if(cycleEndDate == ''){
 				layer.msg("请选择账期结束日期！",{icon:2});
 				return;
 			   }*/
    		 /*var billStatementDate=$("#updateBillCycleDiv input[name='billStatementDate']").val();
    		 if(billStatementDate == ''){
  				layer.msg("请选择出账日期！",{icon:2});
  				return;
  			   }*/
             var outStatementDate = $("#outAccount").val();
             var billStatementDate = outStatementDate.toString();
             // billStatementDate = billStatementDate.toString();
             var location = billStatementDate.lastIndexOf("-");
             billStatementDate = billStatementDate.substring(location+1,billStatementDate.length );
             var billStatementNum = parseInt(billStatementDate);

             if(!billStatementDate && billStatementDate == ''){
                 layer.msg("请选择出账日期！",{icon:2});
                 return;
             }else if(billStatementNum > 28){
                 layer.msg("每月出账日期不得大于28号！",{icon:2});
                 return;
             }
  			   var sellerCompanyId = $("#sellerCompanyId").val();
    		//供应商判空
   			/*var supplierIdArr=$("#updateBillCycleDiv #supplierId").val();
   			if(supplierIdArr == ''){
   				layer.msg("请选择供应商！",{icon:2});
   				return;
   			}
   			var supplierId=supplierIdArr.split(",")[0];*/
   			
    		 var jsonArray = new Array();
    		    $("#interestBody tr").each(function() {
    		        var overdueDate = $(this).find("td:eq(0)").find("input[name='overdueDate']").val();
    		        var overdueInterest = $(this).find("td:eq(1)").find("input[name='overdueInterest']").val();
    		        var interestCalculationMethod = $(this).find("td:eq(2)").find("select[name='interestMethod']").val();
    		        var o = new Object();
    		        o.overdueDate = overdueDate;
    		        o.overdueInterest = overdueInterest;
    		        o.interestCalculationMethod = interestCalculationMethod;
    		        jsonArray.push(o);
    		    });
    		   /* if(jsonArray.length<=0){
    		        layer.msg("请设置对应利息！",{icon:2});
    		        return;
    		    }*/
    		    var url = "platform/buyer/billCycle/updateSaveBillCycleInfo";
    		    $.ajax({
    		        type : "POST",
    		        url : url,
    		        data: {
    		        	"billCycleId":'${billCycleMapByUpdate.id}',
    		        	"billDealStatus":'1',
    		            "outStatementDate":outStatementDate,
    		            "billStatementDate":billStatementDate,
    		            "sellerCompanyId":sellerCompanyId,
    		            "interestList": JSON.stringify(jsonArray),
                        "menuName":"17071814370247369019"
    		        },
    		        /*async:false,
    		        success:function(data){
    		            var result = eval('(' + data + ')');
    		            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    		            if(resultObj.success){
    		                layer.msg("提交成功！",{icon:1});
    		                leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=0&billDealStatus=1','buyer')
    		            }else{
    		                layer.msg(resultObj.msg,{icon:2});
    		            }
    		        },*/
                    success:function(data){
                        if(data.flag){
                            var res = data.res;
                            if(res.code==40000){
                                //调用成功
                                //saveSucess();
                                layer.msg("提交成功！",{icon:1});
                                //leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                                leftMenuClick(this,'platform/buyer/billCycle/billCycleList','buyer')
                            }else if(res.code==40010){
                                //调用失败
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40011){
                                //需要设置审批流程
                                layer.msg(res.msg,{time:500,icon:2},function(){
                                    setApprovalUser(url,res.data,function(){
                                        //saveSucess();
                                        layer.msg("提交成功！",{icon:1});
                                        leftMenuClick(this,'platform/buyer/billCycle/billCycleList','buyer')
                                    });
                                });
                                return false;
                            }else if(res.code==40012){
                                //对应菜单必填
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40013){
								//不需要审批
								notNeedApproval(res.data,function(data){
									layer.msg("提交成功！",{icon:1});
                                    leftMenuClick(this,'platform/buyer/billCycle/billCycleList','buyer')
								});
								return false;
							}
                        }else{
                            layer.msg("获取数据失败，请稍后重试！",{icon:2});
                            return false;
                        }
                    },
    		        error:function(){
    		            layer.msg("保存失败，请稍后重试！",{icon:2});
    		        }
    		    });
    		});
     });

   //加载互通卖家好友下拉
     function loadSupplier(supplierId){
     	var obj={};
     	obj.divId="addBillCycleSeller";
     	obj.selectName="supplierName";
     	obj.selectId="supplierId";
     	obj.filter="changeSupplier";
     	obj.selectValue=supplierId;//匹配供应商
     	
     	supplierSelect(obj);
     }
    </script>
</head>
<div id="updateBillCycleDiv">
 <!--  <form id="addForm" name="addForm" action=""> -->
  <div class="bill_cycle">
    <%--<div>
      <label>账期设置:</label>
      &lt;%&ndash;本月 <input type="text" style="width:80px" id="bill_present" name="cycleStartDate" value="${billCycleMapByUpdate.cycleStartDate}"> 日 至下月
      <input type="text" style="width:80px" id="bill_next" name="cycleEndDate" value="${billCycleMapByUpdate.cycleEndDate}"> 日 &emsp;为一个账单周期&ndash;%&gt;
      <!-- <ul id = "tab_ul" class="tab_ul" style="display: none; left: 281px;"> -->
      <ul class = "tab_ul" >
        <div class="tab_ul_div"><span>日历表</span><img src="<%=basePath%>statics/platform/images/rili_close.png"></div>
        <li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>10</li><li>11</li><li>12</li><li>13</li><li>14</li><li>15</li><li>16</li><li>17</li><li>18</li><li>19</li><li>20</li><li>21</li><li>22</li><li>23</li><li>24</li><li>25</li><li>26</li><li>27</li><li>28</li>
      </ul>
    </div>--%>
    <%--<div>
      <label>出账时间:</label>
      每月 <input type="text" style="width:80px" id="out_account" name="billStatementDate" value="${billCycleMapByUpdate.billStatementDate}"> 日
      <ul class = "tab_ul" >
        <div class="tab_ul_div"><span>日历表</span><img src="<%=basePath%>statics/platform/images/rili_close.png"></div>
        <li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>10</li><li>11</li><li>12</li><li>13</li><li>14</li><li>15</li><li>16</li><li>17</li><li>18</li><li>19</li><li>20</li><li>21</li><li>22</li><li>23</li><li>24</li><li>25</li><li>26</li><li>27</li><li>28</li>
      </ul>
    </div>--%>
      <div >
        <label >出账时间:</label>每月
        <div class="layui-input-inline">
          <input type="text" id="outAccount" lay-verify="date" value="${billCycleMapByUpdate.outStatementDateStr}" class="layui-input" placeholder="请选择出账日">
        </div>日
      </div>
    <div>
      <div id="interstHidOrShow" style="display: none">
      <label class="layui-form-label">利息设置:</label>
      <button class="layui-btn layui-btn-danger layui-btn-mini" id="interest_add" ><i class="layui-icon">&#xe6ab;</i> 新增项</button>
      <div class="bill_interest mp">
        <table class="table_pure">
          <thead>
          <tr>
            <td style="width:25%">逾期（天）</td>
            <td style="width:25%">月利率（%）</td>
            <td style="width:30%">利息计算方式</td>
            <td style="width:20%">操作</td>
          </tr>
          </thead>
          <tbody id="interestBody">
            <c:forEach var="interestListUpdate" items="${interestListByUpdate}">
              <tr class="text-center">
                <td>
                  <input type="number" name="overdueDate" value="${interestListUpdate.overdueDate}">
                </td>
                <td>
                  <input type="number" name="overdueInterest" value="${interestListUpdate.overdueInterest}">
                </td>
                <td>
                  <select name="interestMethod" value="${interestListUpdate.interestCalculationMethod}">
                    <option value="">请选择</option>
                    <option value="1" <c:if test="${interestListUpdate.interestCalculationMethod eq 1}">selected</c:if>>单利</option>
                    <option value="2" <c:if test="${interestListUpdate.interestCalculationMethod eq 2}">selected</c:if>>复利</option>
                  </select>
                </td>
                <td><button class="layui-btn layui-btn-mini layui-btn-normal"><i class="layui-icon"></i> 删除</button></td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
      </div>
    </div>
     <div class="layui-inline">
		<label class="layui-form-label">
			供&nbsp;应&nbsp;商:
		</label>
       <input type="hidden" id="sellerCompanyId" name="sellerCompanyId" value="${billCycleMapByUpdate.sellerCompanyId}">
       <input style="width:50%;" disabled="disabled" id="sellerCompanyName" name="sellerCompanyName" value="${billCycleMapByUpdate.sellerCompanyName}">
		<%--<div class="layui-input-inline" style="width:200px" id="addBillCycleSeller">
		</div>--%>
	</div>
    </div> 
    
  <div class="text-center mp30">
    <!-- <a href="买家-结算-待内部审批.html">
      <button  class="layui-btn layui-btn-danger layui-btn-small">确认提交</button>
    </a> -->
    <a href="javascript:void(0);">
    <button  class="layui-btn layui-btn-danger layui-btn-small" id="billCycleInfo_update">确认提交</button>
    <button class="layui-btn layui-btn-small" onclick="hidOrShowInterst()">利息设置</button>
    </a>
  </div>
  <!-- </form> -->
</div>
 
