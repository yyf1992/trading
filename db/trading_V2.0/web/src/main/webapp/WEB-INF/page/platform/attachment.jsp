<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/toolTip.js"></script>
<span>
	<b></b>
	<img src="${url}" onMouseOver="toolTip('${url}')" onMouseOut="toolTip()" 
		onclick="window.open('${url}');">
	<input type="hidden" name="fileUrl" value="${url}" />
</span>
