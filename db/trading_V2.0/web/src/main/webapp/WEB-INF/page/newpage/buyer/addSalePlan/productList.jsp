<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>买卖系统</title>
	<meta name="keywords" content="诺泰,诺泰买卖,买卖系统,nuotai">
  	<meta name="description" content="诺泰买卖系统是用于商家向供应商采购商品。">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<%@ include file="../../../../newpage/common/iframeCommon.jsp"%>
</head>
<script>
$(function(){
layui.use('table', function() {
	var table = layui.table
	,form = layui.form;
	//监听单选
	form.on('radio(radioClick)', function(obj){
		var data = JSON.parse(this.value)
		,selectProductId = $("#selectProductId").val()
		,trObj = $("tr#"+selectProductId, window.parent.document);
		
		trObj.find("[name='productCode']").val(data.productCode);
	    trObj.find("[name='productName']").val(data.productName);
	    trObj.find("[name='skuCode']").val(data.skuCode);
	    trObj.find("[name='skuName']").val(data.skuName);
	    trObj.find("[name='skuOid']").val(data.barcode);
	    trObj.find("[name='productType']").val(data.productType);
	    trObj.find("[name='unitId']").val(data.unitId);
	    trObj.find("[name='unitName']").val(data.unitName);
	    //商品主图
	    $.ajax({
			type : "post",
	        url : basePath + "platform/product/getProductPicByProductCode",
	        data: {"productCode" : data.productCode},
	        success:function(data){
	            var result = eval('(' + data + ')');
	            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
	            var imgUrl = resultObj.pic?resultObj.pic:"<%=basePath %>/statics/platform/images/defaulGoods.jpg"
	            ,imgObj = ["<img style='height: 48px'","onmouseover='toolTip(\""+imgUrl+"\")'","onmouseout='toolTip()'","src='"+imgUrl+"' >"];
	            
            	trObj.find("td[name='mainPictureUrlTd']").html(imgObj.join(""));
	        },
	        error:function(){
	            layer.msg("获取数据失败，请稍后重试！",{icon:2});
	        }
	    });
		parent.layer.closeAll();
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('productTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          productCode  : $("#productCode").val(),
	          productName  : $("#productName").val(),
	          barcode      : $("#barcode").val(),
	          skuCode      : $("#skuCode").val(),
	          productType  : $("#productTypeSelect").val()
	        }
	      });
	    }
	};
	$('#productTableSelect .layui-btn').on('click', function(){
	    var type = $(this).data('type');
	    active[type] ? active[type].call(this) : '';
	});
});
});
</script>
<input type="hidden" id="selectProductId" value="${selectProductId}">
<div class="demoTable" id="productTableSelect">
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="productName" id="productName" autocomplete="off" placeholder='商品名称'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="skuCode" id="skuCode" autocomplete="off" placeholder='规格'>
  </div>
  <div class="layui-inline">
  	<select id="productTypeSelect">
  		<option value="">全部类型</option>
  		<option value="0">成品</option>
  		<option value="1">原材料</option>
  		<option value="2">辅料</option>
  		<option value="3">虚拟产品</option>
  	</select>
  </div>
  <button class="layui-btn" data-type="reload" id="select">搜索</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:480,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'productTable',
		url:'<%=basePath %>buyer/applyPurchaseHeader/loadAllProductDataJson'
	}" lay-filter="productTableFilter">
    <thead>
        <tr>
            <th lay-data="{ align:'center', templet: '#radio',show:true,unresize:true}">选择</th>
            <th lay-data="{field:'productCode', sort: true,show:true}">货号</th>
            <th lay-data="{field:'productName', sort: true,show:true}">商品名称</th>
            <th lay-data="{field:'skuName', sort: true,show:true}">规格</th>
            <th lay-data="{field:'barcode', sort: true,show:true}">条形码</th>
            <th lay-data="{field:'unitName', sort: true,show:true}">单位</th>
            <th lay-data="{field:'productType', sort: true,show:true,templet: '#productType'}">商品类型</th>
            <th lay-data="{field:'id', sort: true,show:false}">ID</th>
        </tr>
    </thead>
</table>
<!-- 单选 -->
<script type="text/html" id="radio">
	<input type='radio' name='checkOne' value='{{JSON.stringify(d)}}' title=' ' lay-filter='radioClick'>
</script>
<!-- 商品类型 0成品1原材料2辅料3虚拟产品-->
<script type="text/html" id="productType">
	{{#  if(d.productType === 0){ }}
		成品
	{{#  } else if(d.productType === 1){ }}
		原材料
	{{#  } else if(d.productType === 2){ }}
		辅料
	{{#  } else if(d.productType === 3){ }}
		虚拟产品
	{{#  } }}
</script>
<%-- <link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/commodity_all.css"/>
<link rel="stylesheet" type="text/css" href="<%=basePath %>statics/platform/css/common.css"/>
<style>
    .cc:after{
        content: '';
        display: block;
        clear: both;
    }
    .cc input,.cc select{
        width:80px;
        height: 20px;
        margin-bottom: 5px;
    }
    .materialContent{
        overflow-y: auto;
    }
}
</style> --%>
<script type="text/javascript">
    function searchProductSku() {
        $.ajax({
            url:"buyer/applyPurchaseHeader/loadAllProductList",
            data:$("#productLinkForm").serialize(),
            async:false,
            success:function(data){
                $(".productLinkDiv").html(data);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！", {icon : 2});
            }
        });
    }
    //为选择的商品赋值
function setProductInfo(obj){
	var productCode=$(obj).parent().parent().find("td")[1].innerText;
    var productName=$(obj).parent().parent().find("td")[2].innerText;
    var skuCode=$(obj).parent().parent().find("td")[3].innerText;
    var skuName=$(obj).parent().parent().find("td")[4].innerText;
    var skuOid=$(obj).parent().parent().find("td")[5].innerText;
    var unitId = $(obj).parent().parent().find("td:eq(6)").find("input").val();
    var unitName = $(obj).parent().parent().find("td")[6].innerText;
    var productType = $(obj).parent().parent().find("td:eq(8)").find("input").val();
    var selectProductId = $("#selectProductId").val();
    $("#"+selectProductId).find("[name='productCode']").val(productCode);
    $("#"+selectProductId).find("[name='productName']").val(productName);
    $("#"+selectProductId).find("[name='skuCode']").val(skuCode);
    $("#"+selectProductId).find("[name='skuName']").val(skuName);
    $("#"+selectProductId).find("[name='skuOid']").val(skuOid);
    $("#"+selectProductId).find("[name='productType']").val(productType);
    $("#"+selectProductId).find("[name='unitId']").val(unitId);
    $("#"+selectProductId).find("[name='unitName']").val(unitName);
    //商品主图
    $.ajax({
		type : "post",
        url : basePath+"platform/product/getProductPicByProductCode",
        data: {
            "productCode" : productCode
        },
        async:false,
        success:function(data){
            var result = eval('(' + data + ')');
            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
            if(resultObj.pic == ""){
            	$("#"+selectProductId).find("td[name='mainPictureUrlTd']").html("<img src='<%=basePath %>/statics/platform/images/defaulGoods.jpg'>");
            }else{
            	$("#"+selectProductId).find("td[name='mainPictureUrlTd']").html("<img style='width: 65px;height: 65px' src='"+resultObj.pic+"'>");
            }
        },
        error:function(){
            layer.msg("获取数据失败，请稍后重试！",{icon:2});
        }
    });
	layer.closeAll();
}
</script>
<!--商品弹框-->
<%-- <div class="productLinkDiv">
    <form action="buyer/applyPurchaseHeader/loadAllProductList" id="productLinkForm">
        <div class="cc mt">
            <span>商品货号:</span> <input type="text" id="searchProductCode" name="searchProductCode" value="${searchPageUtil.object.searchProductCode}" class="mr" placeholder="商品货号">
            <span>商品名称:</span> <input type="text" id="searchProductName" name="searchProductName" value="${searchPageUtil.object.searchProductName}" class="mr" placeholder="商品名称">
            <span>条 形 码:</span> <input type="text" id="searchBarCode" name="searchBarCode" value="${searchPageUtil.object.searchBarCode}" class="" placeholder="条形码">
            <span>规格代码:</span> <input type="text" id="searchSkuCode" name="searchSkuCode" value="${searchPageUtil.object.searchSkuCode}" class="mr" placeholder="规格代码">
            <span>规格名称:</span> <input type="text" id="searchSkuName" name="searchSkuName" value="${searchPageUtil.object.searchSkuName}" class="mr" placeholder="规格名称">
            <img src="<%=basePath %>/statics/platform/images/find.jpg" onclick="searchProductSku();" class="rt">
            <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
            <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
            <input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
            <input type="hidden" id="selectProductId" name="selectProductId" value="${searchPageUtil.object.selectProductId}"/>
        </div>
    </form>
    <div class="materialContent" style="width: 100%; overflow: auto;">
        <table class="table_blue" id="selectProduct">
            <thead>
            <tr>
                    <td style="width:45px;height: "></td>
                    <td style="width:100px">货号</td>
                    <td style="width:150px">商品</td>
                    <td style="width:150px">规格代码</td>
                    <td style="width:150px">规格名称</td>
                    <td style="width:120px">条形码</td>
                    <td style="width:50px">单位</td>
                    <td style="width:80px">价格</td>
                    <td style="width:80px">商品类型</td>
                </tr>
            </thead>
            <tbody id="productBody">
        	<c:forEach var="product" items="${searchPageUtil.page.list}">
        		<tr>
	                <td>
	                	<input type="radio" value='${product.id}' name="checkOne" onclick="setProductInfo(this);">
	                </td>
	                <td>${product.productCode}</td>
	                <td>${product.productName}</td>
	                <td>${product.skuCode}</td>
	                <td style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">${product.skuName}</td>
	                <td title='${product.barcode}'>${product.barcode}</td>
	                <td><input type='hidden' value='${product.unitId}'>${product.unitName}</td>
	                <td>${product.price}</td>
	                <td><c:choose>
                            <c:when test="${product.productType=='0'}">成品</c:when>
                            <c:when test="${product.productType=='1'}">原材料</c:when>
                            <c:when test="${product.productType=='2'}">辅料</c:when>
                            <c:when test="${product.productType=='3'}">虚拟产品</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                        <input type="hidden" value="${product.productType}">
                    </td>
				</tr>
        	</c:forEach>
        </tbody>
        </table>
    </div>
    <div class="pager" id="page">${searchPageUtil.page}</div>
</div> --%>