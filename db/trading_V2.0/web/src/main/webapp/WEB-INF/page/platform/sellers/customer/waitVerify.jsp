<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td style="width: 70%">
			<ul>
				<li style="width: 50%">商品</li>
				<li style="width: 10%">条形码</li>
				<li style="width: 10%">类型</li>
				<li style="width: 10%">退/换货数量</li>
				<li style="width: 10%">买家发货数量</li>
				<li style="width: 10%">实际收货数量</li>
			</ul></td>
		<td style="width: 10%">申请原因</td>
		<td style="width: 10%">状态</td>
		<td style="width: 10%">操作</td>
	</tr>
</table>
<div class="orderList serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
		<div>
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> <span class="order_num"><span>编号:</span>
					${customer.customerCode}</span> <span class="order_name">${el:getCompanyById(customer.companyId).companyName}</span>
			</p>
			<table>
				<tr>
					<td style="width: 70%"><c:forEach var="customerItem" items="${customer.itemList}">
							<ul class="clear">
								<li style="width:50%;"><span class="defaultImg"></span>
									<div>
										${customerItem.productCode}|${customerItem.productName} <br>
										<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
											${customerItem.skuName}</span>
									</div></li>
								<li style="width:10%;">${customerItem.skuOid}</li>
								<li style="width:10%;">
									<c:choose>
										<c:when test="${customerItem.type==0}">换货</c:when>
										<c:when test="${customerItem.type==1}">退货</c:when>
										<c:when test="${customerItem.type==2}">换货转退货</c:when>
									</c:choose>
								</li>
								<li style="width:10%;">${customerItem.goodsNumber}</li>
								<li style="width:10%;"><c:out value="${customerItem.receiveNumber}" default="0"/> </li>
								<li style="width:10%;">
									<input type="number" min="0" name="confirmNum" value="${customerItem.receiveNumber}" style="width:80%;"/>
									<input type="hidden" name="customerItemId" value="${customerItem.id}"/>
								</li>
							</ul>
						</c:forEach></td>
					<td style="width: 10%">${customer.reason}</td>
					<td style="width: 10%">
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待同意</c:when>
								<c:when test="${customer.status==5}">已同意</c:when>
								<c:when test="${customer.status==1}">已收货</c:when>
							</c:choose>
						</div>
						<div>
							<c:choose>
								<c:when test="${customer.arrivalType==1}">买家已发货</c:when>
								<c:otherwise>买家未发货</c:otherwise>
							</c:choose>
						</div>
						<a href="javascript:void(0)"
						onclick="leftMenuClick(this,'seller/customer/loadCustomerDetails?id=${customer.id}','sellers')"
						class="approval">申请明细</a><br> <a href="javascript:void(0)"
						onclick="showProof('${customer.proof}');"
						class="check_receipt orange">查看售后凭证</a>
					</td>
					<td style="width: 10%">
						<c:if test="${customer.status == 5}">
							<div class="afterExam green_btn" onclick="confirmCustomer(this);">确认收货</div>
						</c:if>
						<input type="hidden" name="customerId" value="${customer.id}"/>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>