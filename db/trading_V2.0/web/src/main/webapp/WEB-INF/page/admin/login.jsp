<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>诺泰权限系统</title>
   <%@ include file="../platform/common/path.jsp"%>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<%=basePath%>statics/css/bootstrap.min.css">
  <link rel="stylesheet" href="<%=basePath%>statics/css/font-awesome.min.css">
  <link rel="stylesheet" href="<%=basePath%>statics/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<%=basePath%>statics/css/all-skins.min.css">
  <link rel="stylesheet" href="<%=basePath%>statics/css/main.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" id="rrapp" v-cloak>
  <div class="login-logo">
    <b>诺泰权限系统</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
      <p class="login-box-msg">管理员登录</p>
      <div v-if="error" class="alert alert-danger alert-dismissible">
        <h4 style="margin-bottom: 0px;"><i class="fa fa-exclamation-triangle"></i> {{errorMsg}}</h4>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" v-model="username" placeholder="账号">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" v-model="password" placeholder="密码">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" class="btn btn-primary btn-block btn-flat" @click="login">登录</button>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.social-auth-links -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<script src="<%=basePath%>statics/libs/jquery.min.js"></script>
<script src="<%=basePath%>statics/libs/vue.min.js"></script>
<script src="<%=basePath%>statics/libs/bootstrap.min.js"></script>
<script src="<%=basePath%>statics/libs/jquery.slimscroll.min.js"></script>
<script src="<%=basePath%>statics/libs/fastclick.min.js"></script>
<script src="<%=basePath%>statics/libs/app.js"></script>
<script type="text/javascript">
var vm = new Vue({
	el:'#rrapp',
	data:{
		username: '',
		password: '',
		captcha: '',
		error: false,
		errorMsg: ''
	},
	beforeCreate: function(){
	},
	methods: {
		login: function (event) {
			toLoginAdmin(vm);
		}
	}
});
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    // 兼容FF和IE和Opera
    var theEvent = e || window.event;
    var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        toLoginAdmin(vm);
        return false;
    }
    return true;
}
function toLoginAdmin(vm){
	var data = "username="+vm.username+"&password="+vm.password;
	$.ajax({
		type: "POST",
	    url: basePath+"admin/login",
	    data: data,
	    dataType: "json",
	    success: function(result){
			if(result.code == 0){//登录成功
				parent.location.href =basePath+"admin/adminIndexHtml";
			}else{
				vm.error = true;
				vm.errorMsg = result.msg;
			}
		}
	});
}
</script>
</body>
</html>
