<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.min.js"></script>
<script src="<%=basePath%>statics/platform/js/jquery.table2excel.js"></script> 
<style>
ul li {
	padding: 3px;
}
</style>
<div>
	<form class="layui-form" action="platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml">
		<ul class="order_search">
			<li class="range"><label>考核名称：</label> 
				<input type="text" placeholder="输入考核名称" name="assessmentName" value="${param.assessmentName}">
			</li>
			<li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
		</ul>
	</form>
	<div class="newBuild mt">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/loadAddSupplierAssesmentHtml','buyer','18010209272828100705');" button="新建供应商考核"><button>新建供应商考核方案</button></a>
	</div>
	<table class="table_pure assesmentList">
          <thead>
          <tr>
            <td>考核名称</td>
            <td>总分值</td>
            <td>状态</td>
            <td>创建日期</td>
            <td>考核项</td>
            <td>分值</td>
            <td>操作</td>
          </tr>
          </thead>
          <tbody>
          <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
          	<tr>
              <td>${item.assessmentName}</td>
              <td>${item.score}</td>
              <td>
				<c:choose>
					<c:when test="${item.state==0}">正常</c:when>
					<c:when test="${item.state==1}">禁用</c:when>
				</c:choose>
              </td>
              <td><fmt:formatDate value="${item.createDate}" type="both"/></td>
              <td><ul>
              		<li>到货及时率分值 </li>
              		<li>计划完成率分值</li>
              		<li>到货合格率分值</li>
              		<li>售后响应及时率分值</li>
              	  </ul>
              </td>
              <td>
              	<c:choose>
              		<c:when test="${item.scoreType==0 }">
              		<ul>
	              		<li>${item.arrivalRateScore}</li>
	              		<li>${item.completionRateScore}</li>
	              		<li>${item.qualificationRateScore}</li>
	              		<li>${item.afterSaleRateScore}</li>
              	  	</ul>
              		</c:when>
              		<c:when test="${item.scoreType==1 }">
              		<ul>
	              		<li>${item.arrivalRateScore * item.score /100}</li>
	              		<li>${item.completionRateScore * item.score /100}</li>
	              		<li>${item.qualificationRateScore * item.score /100}</li>
	              		<li>${item.afterSaleRateScore * item.score /100}</li>
              	  	</ul>
              		</c:when>
              	</c:choose>
              </td>
              <td>
              	<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/loadEditSupplierAssesmentHtml?id=${item.id}','buyer','18010209272828100705');" class="layui-btn layui-btn-update layui-btn-mini"  button="修改"><i class="layui-icon">&#xe691;</i>修改</a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
        <div class="pager">${searchPageUtil.page}</div>	
</div>