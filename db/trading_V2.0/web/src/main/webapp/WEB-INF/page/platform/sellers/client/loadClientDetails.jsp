<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../common/path.jsp"%>
<html>
<head>
<!-- 对客户以及联系人的操作 -->
<script src="<%=basePath %>/statics/platform/js/updateClient.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户详情页</title>
</head>
<body>
      <div class="content" style="overflow: hidden;">
        <div class="add_supplier">
          <h3 class="page_title mt">客户详情</h3>
          <div class="form_group">
            <label class="control_label">客 户 编 号：</label>${sellerClient.id }
          </div>
          <div class="form_group">
            <label class="control_label">公 司 名 称：</label>${sellerClient.clientName }
          </div>
          <div class="form_group">
            <label class="control_label">银 行 账 号：</label>${sellerClient.bankAccount }
          </div>
          <div class="form_group">
            <label class="control_label">开 户 行：</label>${sellerClient.openBank }
          </div>
          <div class="form_group">
            <label class="control_label">户 名：</label>${sellerClient.accountName }
          </div>
          <div class="form_group">
            <label class="control_label">纳税人识别号：</label>${sellerClient.taxidenNum }
          </div>
          <div class="setting">
            <h4 class="mt">联系方式</h4>
            <table class="table_blue c_details mt" >
              <thead>
              <tr>
              	<td style="width:5%">默认</td>
                <td style="width:10%">联系人</td>
                <td style="width:10%">手机号</td>
                <td style="width:10%">固话</td>
                <td style="width:20%">收货地址</td>
                <td style="width:12%">传真</td>
                <td style="width:10%">QQ</td>
                <td style="width:13%">旺旺</td>
                <td style="width:15%">E-mail</td>
              </tr>
              </thead>
              <tbody>
                <c:forEach var="sellerClinentLinkman" items="${sellerClinentLinkmanList}">
					<tr>
						<td>
							<c:if test="${sellerClinentLinkman.isDefault == 1}">
								<input type="radio" name="default" checked><!-- 默认为1 --> 
							</c:if> 
						</td>
						<td>${sellerClinentLinkman.clientPerson}</td>
						<td>${sellerClinentLinkman.clientPhone}</td>
						<td>${sellerClinentLinkman.zone}-${sellerClinentLinkman.telno}</td>
						<td style="vertical-align:middle; text-align:center;">${sellerClinentLinkman.shipAddress}</td>
						<td>${sellerClinentLinkman.fax}</td>
						<td>${sellerClinentLinkman.qq}</td>
						<td>${sellerClinentLinkman.wangNo}</td>
						<td>${sellerClinentLinkman.email}</td>
					</tr>
				</c:forEach>
              </tbody>
            </table>
          </div>
          <div class="btn_p text-center mp30">
          	<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/clientList','sellers')">
				<span class="preview order_p_s">返回</span>
			</a>
            <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/sellers/client/updateClientDetails?id=${sellerClient.id }','sellers')"><span class="preview order_p_s">修改</span></a>
          </div>
        </div>
      </div>
</body>
</html>