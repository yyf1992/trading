<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<!--供应商详情-->
<div class="add_supplier">
	<h3 class="page_title mt">${suppName}</h3>
	<a href="javascript:void(0);" class="layui-btn layui-btn-add layui-btn-small rt" 
		onclick="leftMenuClick(this,'platform/product/addProductLink?linktype=${linktype}')">
		<i class="layui-icon">&#xebaa;</i>新增商品关联
	</a>
	<div class="setting">
		<div class="btn_add mt"></div>
		<table class="table_blue contract_settings">
			<thead>
				<tr>
					<td style="width:10%">货号</td>
					<td style="width:10%">规格</td>
					<td style="width:15%">最新报价</td>
					<td style="width:13%">是否含税</td>
					<td style="width:10%">是否含运费</td>
					<td style="width:12%">报价有效期</td>
					<td style="width:12%">合作权重</td>
					<td style="width:12%">单位</td>
					<td style="width:12%">日产能数量</td>
					<td style="width:12%">物流周期</td>
					<td style="width:12%">最小起订量</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="productLinks" items="${buyShopProductList}">
					<tr>
					<td>${productLinks.productCode}</td>
					<td>${productLinks.skuName}</td>
					<td>${productLinks.price}</td>
					<td>
						<c:if test="${productLinks.isTax == 0}">否</c:if>
						<c:if test="${productLinks.isTax == 1}">是</c:if>
					</td>
					<td>
						<c:if test="${productLinks.isFreight == 0}">否</c:if>
						<c:if test="${productLinks.isFreight == 1}">是</c:if>
					</td>
					<td>${productLinks.quotationPeriod}</td>
					<td>${productLinks.undertakeProportion}</td>
					<td>${productLinks.unitName}</td>
					<td>${productLinks.dailyOutput}</td>
					<td>${productLinks.storageDay}</td>
					<td>${productLinks.minimumOrderQuantity}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="text-center"  style="margin-top: 40px;">
		<a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/buyer/supplier/supplierList?${form}&linktype=${linktype}','buyer','17071815040317888127')" class="layui-btn layui-btn-normal layui-btn-mini">
			返回</a>
	</div>
</div>
