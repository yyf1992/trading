<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
$(function(){
	//设置选择的tab
	$(".layui-tab-title>li").removeClass("layui-this");
	$(".layui-tab-title #tab_${params.tabId}").addClass("layui-this");
	$(".layui-tab-title li").click(function(){
		var id = $(this).attr("id");
		//清空查询条件
		//$("#purchasePlanContent #resetButton").click();
		var index = id.split("_")[1];
		$("#purchasePlanContent input[name='tabId']").val(index);
		$("#purchasePlanContent input[name='productType']").val(index);
		loadPlatformData();
	});
    //初始化数据
    selectData();
});
function selectData(){
	var formObj = $("#purchasePlanContent").find("form");
	$.ajax({
		url : basePath+"buyer/applyPurchaseHeader/loadData",
		data:formObj.serialize(),
		async:false,
		success:function(data){
            $("#tabContent").empty();
			var str = data.toString();
			$("#tabContent").html(str);
            $(".layui-tab-title>li").removeClass("layui-this");
            $(".layui-tab-title #tab_${params.tabId}").addClass("layui-this");
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
<!--页签-->
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
  <ul class="layui-tab-title">
    <li id="tab_0">成品计划</li>
    <li id="tab_1">材料计划</li>
  </ul>
  <div class="layui-tab-content" id="purchasePlanContent">
  	<form class="" action="buyer/applyPurchaseHeader/applyPurchaseList">
  		<input type="hidden" name="tabId" id="tabId" value="${params.tabId}">
  		<input type="hidden" name="productType" id="productType" value="${params.tabId}">
  	</form>
  	<div id="tabContent"></div>
    <%-- <div class="layui-tab-item layui-show"><%@ include file="finishedProductPlan.jsp"%></div>
    <div class="layui-tab-item">内容2</div> --%>
  </div>
</div> 