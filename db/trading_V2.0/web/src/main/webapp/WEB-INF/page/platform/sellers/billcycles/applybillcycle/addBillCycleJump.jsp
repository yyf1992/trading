<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
    <title>新增账单周期</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billcycles/addBillCycle.js"></script>
    <link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
    <script type="text/javascript">
    var queryType = '${queryType}';
     $(function(){
    	 //关联商家下拉
    	 loadSupplier("");
    	 
    	 //提交账单周期信息
    	 $('#billCycleInfo_add').click(function(){
    		 
    		 var cycleStartDate=$("#addBillCycleDiv input[name='cycleStartDate']").val();
    		 if(cycleStartDate == ''){
    				layer.msg("请选择账期开始日期！",{icon:2});
    				return;
    			}
    		 var cycleEndDate=$("#addBillCycleDiv input[name='cycleEndDate']").val();
    		 if(cycleEndDate == ''){
 				layer.msg("请选择账期结束日期！",{icon:2});
 				return;
 			   }
    		 var billStatementDate=$("#addBillCycleDiv input[name='billStatementDate']").val();
    		 if(billStatementDate == ''){
  				layer.msg("请选择出账日期！",{icon:2});
  				return;
  			   }
    		//供应商判空
   			var supplierIdArr=$("#addBillCycleDiv #supplierId").val();
   			if(supplierIdArr == ''){
   				layer.msg("请选择供应商！",{icon:2});
   				return;
   			}
   			var supplierId=supplierIdArr.split(",")[0];
   			
    		 var jsonArray = new Array();
    		    $("#interestBody tr").each(function() {
    		        var overdueDate = $(this).find("td:eq(0)").find("input[name='overdueDate']").val();
    		        var overdueInterest = $(this).find("td:eq(1)").find("input[name='overdueInterest']").val();
    		        var interestCalculationMethod = $(this).find("td:eq(2)").find("select[name='interestMethod']").val();
    		        var o = new Object();
    		        o.overdueDate = overdueDate;
    		        o.overdueInterest = overdueInterest;
    		        o.interestCalculationMethod = interestCalculationMethod;
    		        jsonArray.push(o);
    		    });
    		    if(jsonArray.length<=0){
    		        layer.msg("请设置对应利息！",{icon:2});
    		        return;
    		    }
    		    
    		    $.ajax({
    		        type : "POST",
    		        url : "platform/buyer/billCycle/saveBillCycleInfo",
                    async:false,
    		        data: {
    		            "queryType":queryType,
    		            "cycleStartDate":cycleStartDate,
    		            "cycleEndDate":cycleEndDate,
    		            "billStatementDate":billStatementDate,
    		            "supplierId":supplierId,
    		            "interestList": JSON.stringify(jsonArray),
                        "menuName":"17071814370247369019"
    		        },
    		        /*success:function(data){
    		            var result = eval('(' + data + ')');
    		            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
    		            if(resultObj.success){
    		                layer.msg("提交成功！",{icon:1});
    		                leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
    		                //window.location.href="platform/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1";
    		            }else{
    		                layer.msg(resultObj.msg,{icon:2});
    		            }
    		        },*/
                    success:function(data){
                        if(data.flag){
                            var res = data.res;
                            if(res.code==40000){
                                //调用成功
                                //saveSucess();
                                layer.msg("提交成功！",{icon:1});
                                leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                            }else if(res.code==40010){
                                //调用失败
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40011){
                                //需要设置审批流程
                                layer.msg(res.msg,{time:500,icon:2},function(){
                                    setApprovalUser(url,res.data,function(){
                                       //saveSucess();
                                        layer.msg("提交成功！",{icon:1});
                                        leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer')
                                    });
                                });
                                return false;
                            }else if(res.code==40012){
                                //对应菜单必填
                                layer.msg(res.msg,{icon:2});
                                return false;
                            }else if(res.code==40013){
								//不需要审批
								notNeedApproval(res.data,function(data){
									 layer.msg("提交成功！",{icon:1});
                                     leftMenuClick(this,'platform/buyer/billCycle/billCycleList?queryType=${queryType}&billDealStatus=1','buyer');
								});
								return false;
							}
                        }else{
                            layer.msg("获取数据失败，请稍后重试！",{icon:2});
                            return false;
                        }
                    },
    		        error:function(){
    		            layer.msg("保存失败，请稍后重试！",{icon:2});
    		        }
    		    });
    		});
     });
    //保存成功
   /*function saveSucess(){
        $.ajax({
            url:"platform/buyer/billCycle/success",
            success:function(data){
                var str = data.toString();
                $(".content").html(str);
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
    }*/
   //加载互通卖家好友下拉
     function loadSupplier(supplierId){
     	var obj={};
     	obj.divId="addBillCycleSeller";
     	obj.selectName="supplierName";
     	obj.selectId="supplierId";
     	obj.filter="changeSupplier";
     	obj.selectValue=supplierId;
     	supplierSelect(obj);
     }
    </script>
</head>
<div id="addBillCycleDiv">
 <!--  <form id="addForm" name="addForm" action=""> -->
  <div class="bill_cycle">
    <div>
      <label>账期设置:</label>
      本月 <input type="text" style="width:80px" id="bill_present" name="cycleStartDate"> 日 至下月
      <input type="text" style="width:80px" id="bill_next" name="cycleEndDate"> 日 &emsp;为一个账单周期
      <!-- <ul id = "tab_ul" class="tab_ul" style="display: none; left: 281px;"> -->
      <ul class = "tab_ul" >
        <div class="tab_ul_div"><span>日历表</span><img src="<%=basePath%>statics/platform/images/rili_close.png"></div>
        <li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>10</li><li>11</li><li>12</li><li>13</li><li>14</li><li>15</li><li>16</li><li>17</li><li>18</li><li>19</li><li>20</li><li>21</li><li>22</li><li>23</li><li>24</li><li>25</li><li>26</li><li>27</li><li>28</li><li>29</li><li>30</li><li>31</li>
      </ul>
    </div>
    <div>
      <label>出账时间:</label>
      每月 <input type="text" style="width:80px" id="out_account" name="billStatementDate"> 日
    </div>
    <div>
      <label>利息设置:</label>
      <button class="layui-btn layui-btn-danger layui-btn-mini" id="interest_add" ><i class="layui-icon">&#xe6ab;</i> 新增项</button>
      <div class="bill_interest mp">
        <table class="table_pure">
          <thead>
          <tr>
            <td style="width:25%">逾期（天）</td>
            <td style="width:25%">月利率（%）</td>
            <td style="width:30%">利息计算方式</td>
            <td style="width:20%">操作</td>
          </tr>
          </thead>
          <tbody id="interestBody">
          </tbody>
        </table>
      </div>
    </div>
     <div class="layui-inline">
		<label class="layui-form-label">
			供应商:
		</label>
		<div class="layui-input-inline" style="width:200px" id="addBillCycleSeller">
		</div>
	</div>
    </div> 
    
  <div class="text-center mp30">
    <!-- <a href="买家-结算-待内部审批.html">
      <button  class="layui-btn layui-btn-danger layui-btn-small">确认提交</button>
    </a> -->
    <a href="javascript:void(0);">
    <button  class="layui-btn layui-btn-danger layui-btn-small" id="billCycleInfo_add">确认提交</button>
    </a>
  </div>
  <!-- </form> -->
</div>
 
