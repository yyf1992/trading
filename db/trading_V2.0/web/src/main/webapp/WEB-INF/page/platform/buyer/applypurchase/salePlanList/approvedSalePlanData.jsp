<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<table class="order_detail">
	<tr>
		<td style="width:70%">
			<ul>
				<li style="width:37px">
					<input type="checkbox" name="checkAll" onclick="selectAll(this);">
				</li>
				<li style="width:25%">商品</li>
				<li style="width:15%">条形码</li>
				<li style="width:15%">部门</li>
				<li style="width:12%">销售计划</li>
				<li style="width:12%">入仓量</li>
			</ul>
		</td>
		<td style="width:20%">备注</td>
		<td style="width:10%">操作</td>
	</tr>
</table>
<c:forEach var="salePlan" items="${searchPageUtil.page.list}">
	<div class="order_list" onclick="verifySalePlan('${salePlan.id}');">
		<p>
			<span class="layui-col-sm1" style="width: 30px"><input type="checkbox" name="checkone" value="${salePlan.id}"></span>
			<span class="layui-col-sm2" title="${salePlan.title}">销售计划编号:<b>${salePlan.planCode}</b>
			</span>
			<span class="layui-col-sm3">标题:<b>${salePlan.title}</b></span>
			<span class="layui-col-sm2" style="width: 250px">创建人:<b>${salePlan.createName}</b>
			</span>
			<span class="layui-col-sm2">销售周期:
				<b><fmt:formatDate value="${salePlan.startDate}" type="date"/>至 <fmt:formatDate value="${salePlan.endDate}" type="date"/></b> 
			</span>
			<span class="layui-col-sm2">创建日期:<b><fmt:formatDate value="${salePlan.createDate}" type="both"></fmt:formatDate></b>
			</span>
		</p>
		<table>
			<tr>
				<td style="width:70%">
					<c:forEach var="salePlanItem" items="${salePlan.itemList}">
						<ul class="clear">
							<li style="width:27%">
								<!-- <span class="defaultImg"></span> -->
								${salePlanItem.productCode}|${salePlanItem.productName}|${salePlanItem.skuCode}|${salePlanItem.skuName}
							</li>
							<li style="width:15%">${salePlanItem.barcode}</li>
							<li style="width:17%">${salePlanItem.shopName}</li>
							<li style="width:12%">${salePlanItem.salesNum}</li>
							<li style="width:12%">${salePlanItem.putStorageNum}</li>
						</ul>
					</c:forEach>
			   </td>
			   <td style="width:20%">${salePlan.remark}</td>
			   <td style="width:12%">
					<a href="javascript:void(0)" 
						onclick="leftMenuClick(this,'buyer/salePlan/approvedDetails?id=${salePlan.id}','buyer');"
						class="layui-btn layui-btn-normal layui-btn-mini">
						<i class="layui-icon">&#xe695;</i>详情
					</a>
					<div class="opinion_view">
						<span class="orange" data="${salePlan.id}">查看审批流程</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
</c:forEach>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>
<script>
$(function(){
	//美化title提示
	sweetTitles.init();
})
</script>