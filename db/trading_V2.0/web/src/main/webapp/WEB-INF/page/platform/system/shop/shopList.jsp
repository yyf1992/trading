<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(shopTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'editShop'){
	    	//修改
	    	editShop(data.id);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('shopTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          shopName: $("#shopName").val(),
	          shopCode: $("#shopCode").val(),
	          isDel: $("#isDel").val(),
	          createDate: $("#createDate").val()
	        }
	      });
	    },
	    addShop: function(){
	    	$.ajax({
	    		url:basePath+"platform/sysshop/loadShopInsert",
	    		type:"get",
	    		async:false,
	    		success:function(data){
	    			layer.open({
	    				type:1,
	    				content:data,
	    				title:'新建店铺',
	    				skin:'layui-layer-rim',
	    				area:['400px','auto'],
	    				btn:['确定','取消'],
	    				yes:function(index,layerio){
	    					$.ajax({
	    						url:basePath+"platform/sysshop/saveInsertShop",
	    						type:"post",
	    						async:false,
	    						data:$("#addShopForm").serialize(),
	    						success:function(data){
	    							var result=eval('('+data+')');
	    							var resultObj=isJSONObject(result)?result:eval('('+result+')');
	    							if(resultObj.success){
	    								layer.close(index);
										layer.msg(resultObj.msg,{icon:1});
										active["reload"].call(this);
	    							}else{
										layer.msg(resultObj.msg,{icon:2});
									}
	    						},
	    						error:function(){
	    							layer.msg("获取数据失败，稍后重试！",{icon:2});
	    						}
	    					});
	    				}
	    			});
	    		},
	    		error:function(){
	    			layer.msg("获取数据失败，稍后重试！",{icon:2});
	    		}
	    	});
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="shopCode" id="shopCode" autocomplete="off" placeholder='店铺代码'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="shopName" id="shopName" autocomplete="off" placeholder='店铺名称'>
  </div>
  <div class="layui-input-inline">
    <input type="text" name="createDate" id="createDate" lay-verify="date" placeholder="请选择日期" autocomplete="off" class="layui-input" >
  </div>
  <div class="layui-inline">
  	<select id="isDel" name="isDel" lay-filter="aihao">
		<option value="">全部</option>
		<option value="0">正常</option>
		<option value="1">禁用</option>
	</select>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="addShop" button="新增">新增</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'shopTable',
		height:'full-90',
		url:'<%=basePath %>platform/sysshop/loadDataJson'
	}" lay-filter="shopTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'id', sort: true,show:false}">id</th>
            <th lay-data="{field:'shopCode', sort: true,show:true}">店铺代码</th>
            <th lay-data="{field:'shopName', sort: true,show:true}">店铺名称</th>
            <th lay-data="{field:'createDate', sort: true,show:true}">加入日期</th>
            <th lay-data="{field:'isDel', sort: true,show:true,templet: '#isDelTpl', align: 'center'}">状态</th> 
<!--             <th lay-data="{field:'updateDate', sort: true,show:true}">操作日期</th> -->
            <th lay-data="{align:'center', toolbar: '#operate',show:true,width:300}">操作</th>
        </tr>
    </thead>
</table>
<!-- 状态转换 -->
<script type="text/html" id="isDelTpl">
	{{#  if(d.isDel === 0){ }}
		<span style="color: green;">正常</span>
	{{#  } else if(d.isDel === 1){}}
		<span style="color: red;">禁用</span>
	{{#  } }}
</script>
<!-- 操作 -->
<script type="text/html" id="operate">
	<a class="layui-btn layui-btn-update layui-btn-xs" lay-event="editShop" button="修改">修改</a>
</script>
<script>
function editShop(id){
	$.ajax({
	url:basePath+"platform/sysshop/loadEditShop",
	type:"post",
	data:{"id":id},
	async:false,
	success:function(data){
		layer.open({
			type:1,
			content:data,
			title:'修改店铺',
			skin:'layui-layer-rim',
			area:['400px','auto'],
			btn:['确定','取消'],
			yes:function(index,layerio){
				$.ajax({
					url:basePath+"platform/sysshop/saveEditShop",
					type:"post",
					async:false,
					data:$("#editShopForm").serialize(),
					success:function(data){
						var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/sysshop/loadSysShopList?'+$("#shopForm").serialize(),'system','17100920494996594508');
						}else{
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/sysshop/loadSysShopList?'+$("#shopForm").serialize(),'system','17100920494996594508');
						}
					},
					error:function(){
						layer.msg("获取数据失败，稍后重试！",{icon:2});
					}
				});
			}
		});
	},
	error:function(){
		layer.msg("获取数据失败，稍后重试！",{icon:2});
	}
});
}
</script>