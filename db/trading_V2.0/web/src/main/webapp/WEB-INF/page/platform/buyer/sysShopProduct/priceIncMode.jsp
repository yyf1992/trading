<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
function editPrice(productId){
	$.ajax({
		url:"platform/buyer/sysshopproduct/loadEditPriceHtml",
		async:false,
		type:"post",
		data:{"productId":productId},
		success:function(data){
			layer.open({
			type:1,
			title:"修改价格",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:"platform/buyer/sysshopproduct/saveEditPurchasePrice",
  		    		data:$("#purchasePriceForm").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
						}else{
							layer.close(index);
							layer.msg(resultObj.msg,{icon:2});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
						}
					},
  		    		error:function(){
  		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
  		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}

function deletePrice(productId){
	layer.confirm('确定删除?', {icon: 3, title:'提示'}, function(index){
		   $.ajax({
    		url:"platform/buyer/sysshopproduct/deletePurchasePrice",
    		data:{"productId":productId},
    		async:false,
    		type:"post",
    		success:function(data){
    			var result=eval('('+data+')');
				var resultObj=isJSONObject(result)?result:eval('('+result+')');
				if(resultObj.success){
					layer.msg(resultObj.msg,{icon:1});
					leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
				}else{
					layer.msg(resultObj.msg,{icon:2});
					leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350'); 
					}
			},error:function(){
    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
    		}
    	});
	});
}

function loadImportHtml(){
$.ajax({
		url:"platform/buyer/syspurchaseprice/loadImportExcelHtml",
		type:"post",
		async:false,
		success:function(data){
			layer.open({
				type:1,
				title:"导入数据",
				skin: 'layui-layer-rim',
  		    	area: ['400px', 'auto'],
  		    	content:data,
  		    	btn:['确定','取消','下载模板'],
				yes:function(){
					var formData = new FormData();
					formData.append("excel", document.getElementById("file1").files[0]);
					$.ajax({
						url : "platform/buyer/syspurchaseprice/importDate",
						async : false,
						data : formData,
						type : "post",
						/**
						 *必须false才会自动加上正确的Content-Type
						 */
						contentType : false,
						/**
						 * 必须false才会避开jQuery对 formdata 的默认处理
						 * XMLHttpRequest会对 formdata 进行正确的处理
						 */
						processData : false,
						success : function(data) {
							var result = eval('(' + data + ')');
							var resultObj = isJSONObject(result) ? result: eval('(' + result + ')');
							if(resultObj.status=="success"){
								layer.closeAll();
								layer.msg(resultObj.msg, {icon : 1});
								leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350');
							}else{
								layer.msg(resultObj.msg, {icon : 2});
							}
						},
						error: function(data) {
							layer.closeAll();
							layer.msg(resultObj.msg, {icon : 2});
							leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350');
						}
					});
				},btn3:function(){
				    var url ="platform/buyer/syspurchaseprice/downloadExcel";  
				    url = encodeURI(url);
				    location.href = url;  
				}
			})
		},error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
});
}

function excelOut(){
    var url ="platform/buyer/syspurchaseprice/excelOut";  
    url = encodeURI(url);
    location.href = url;  
} 
</script>

<div class="content_role">
        <form class="layui-form" action="platform/buyer/sysshopproduct/loadProductPurchasePriceList">
        <ul class="order_search platformSearch">
          <li class="state">
            <label>货号：</label>
            <input type="text" placeholder="输入货号" name="productCode" value="${searchPageUtil.object.productCode}">
          </li>
          <li class="state">
            <label>条形码：</label>
             <input type="text" placeholder="输入条形码" name="barcode" value="${searchPageUtil.object.barcode}">
          </li>
          <li class="nomargin"><button class="search" onclick="loadPlatformData();">搜索</button></li>
        <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
	    <input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
        </ul>
        </form>
        
        <div class="newBuild mt">
	        <a href="javascript:void(0)" button="设置价格提升方式" onclick="leftMenuClick(this,'platform/buyer/sysshopproduct/loadSetPriceStatusHtml','buyer','17102410494297195350')">
	        <button>设置价格提升方式</button></a>
	        <a href="javascript:void(0)" onclick="loadImportHtml();"  button="导入">
	        <button>导入</button></a>
        </div>

        
        <table class="table_pure platformList">
          <thead>
          <tr>
            <td style="width:10%">商品名称</td>
            <td style="width:10%">货号</td>
            <td style="width:10%">规格代码</td>
            <td style="width:10%">规格名称</td>
            <td style="width:10%">条形码</td>
            <td style="width:10%">加价类型</td>
            <td style="width:10%">加价金额/百分比(%)</td>
            <td style="width:11%">操作</td>
          </tr>
          </thead>
          <tbody>
          	<c:forEach var="purchase" items="${searchPageUtil.page.list}" varStatus="status">
          		<tr>
              		<td>${purchase.productName}</td>
              		<td>${purchase.productCode}</td>
              		<td>${purchase.skuCode}</td>
              		<td>${purchase.skuName}</td>
              		<td>${purchase.barcode}</td>
              		<td>
	              		<c:choose>
	              			<c:when test="${purchase.priceStatus==0}">
	              				加价
	              			</c:when>
	              			<c:otherwise>
	              				提成
	              			</c:otherwise>
	              		</c:choose>
              		</td>
              		<td>${purchase.priceLatitude}</td>
              		<td>
              			<span class="layui-btn layui-btn-mini"  onclick="editPrice('${purchase.productId}');" button="修改"><b></b>修改</span>
              			<span class="layui-btn layui-btn-delete layui-btn-mini"  onclick="deletePrice('${purchase.productId}');" button="删除"><b></b>删除</span>
          			</td>
            	</tr>
          	</c:forEach>

          </tbody>
        </table>
		<div class="pager">${searchPageUtil.page}</div>
</div>