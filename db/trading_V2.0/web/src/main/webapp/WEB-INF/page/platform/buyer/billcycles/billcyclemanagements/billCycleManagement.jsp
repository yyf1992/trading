<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<head>
	<title>账单结算周期</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billcycles/billCycleList.js"></script>
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<script type="text/javascript">
	$(function(){
		//重新渲染select控件
		var form = layui.form;
		form.render("select"); 
		//搜索更多
		 $(".search_more").click(function(){
		  $(this).toggleClass("clicked");
		  $(this).parent().nextAll("ul").toggle();
		});  
	});
	
        //标签页改变
     function setStatus(obj,status) {
	   $("#billDealStatus").val(status);
       $('.tab a').removeClass("hover");
	   $(obj).addClass("hover");
	   loadPlatformData();
//      $("#searchForm").submit();
     }
	</script>
</head>
<!--内容-->
<!--页签-->
<div class="tab">
	<a href="javascript:void(0);" onclick="setStatus(this,'0')" <c:if test="${searchPageUtil.object.billDealStatus eq '0'}">class="hover"</c:if>>所有</a> <b></b>
	<a href="javascript:void(0);" onclick="setStatus(this,'1')" <c:if test="${searchPageUtil.object.billDealStatus eq '1'}">class="hover"</c:if>>待内部审批（<span>${params.approvalBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'2')" <c:if test="${searchPageUtil.object.billDealStatus eq '2'}">class="hover"</c:if>>待对方审批（<span>${params.acceptBillCycleCount}</span>）</a> <b>|</b>
	<a href="javascript:void(0);" onclick="setStatus(this,'6')" <c:if test="${searchPageUtil.object.billDealStatus eq '6'}">class="hover"</c:if>>修改待审批（<span>${params.updateEndBillCycleCount}</span>） </a>
	<a href="javascript:void(0);" onclick="setStatus(this,'3')" <c:if test="${searchPageUtil.object.billDealStatus eq '3'}">class="hover"</c:if>>审批已通过（<span>${params.apprEndBillCycleCount}</span>） </a>
</div>
<div>
    <!-- <div>platform/buyer/billCycle/billCycleList?queryType=0
	  <button type="reset" id="resetButton" onclick="resetformData();">重置</button>
	  <button type="button" class="search" onclick="loadPlatformData();">搜索</button>
	</div> -->
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billCycle/billCycleList">
		<div class="search_top mt">
			<input id="sellerCompanyName" type='text' placeholder='输入合作方名称进行搜索' name="sellerCompanyName" value="${params.sellerCompanyName}"/>
			<!-- <button type="submit">搜索</button> -->
			<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			<!-- <button type="reset" id="resetButton" onclick="resetformData();">重置</button> -->
			<span class="search_more sBuyer"></span>
		</div>
		<%--<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="leftMenuClick(this,'platform/buyer/billCycle/addBillCycleJump?queryType=0','buyer')">--%>
		<a href="javascript:void(0);" class="layui-btn layui-btn-danger layui-btn-small rt" onclick="leftMenuClick(this,'platform/buyer/billCycle/addBillCycleJump','buyer')">
			<i class="layui-icon">&#xe6ab;</i> 新增结算周期</a>
		<ul class="order_search bill_request">
			<li class="comm">
				<label>创建人:</label>
				<input id="createName" type="text" placeholder="输入创建人进行搜索" name="createName" value="${params.createName}"/>
			</li>
			<%--<li class="range">
			    <label>创建日期</label>
			    <div class="layui-input-inline">
			         <input class="layui-input" placeholder="开始日" id="startDate" name="createStartDate" value="${params.createStartDate}">
			    </div>
			    <div class="layui-input-inline">
			         <input class="layui-input" placeholder="截止日" id="endDate" name="createEndDate" value="${params.createEndDate}">
			    </div>
			</li>--%>
			<li class="spec nomargin">
				<label>创建日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="createStartDate" id="startDate" lay-verify="date" value="${params.createStartDate}" class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="createEndDate" id="endDate" lay-verify="date" value="${params.createEndDate}" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="range nomargin">
				<label>状态:</label>
				<div class="layui-input-inline">
					<select id="billDealStatus" name="billDealStatus" lay-filter="aihao" value="${params.billDealStatus}">
						<!-- <option value="" selected>全部</option> -->
						<option value="0" <c:if test="${params.billDealStatus eq 0}">selected="selected"</c:if>>全部</option>
						<option value="1" <c:if test="${params.billDealStatus eq 1}">selected="selected"</c:if>>待我审批</option>
						<option value="2" <c:if test="${params.billDealStatus eq 2}">selected="selected"</c:if>>待对方审批</option>
						<option value="3" <c:if test="${params.billDealStatus eq 3}">selected="selected"</c:if>>审批已通过</option>
						<option value="6" <c:if test="${params.billDealStatus eq 6}">selected="selected"</c:if>>修改待审批</option>
						<option value="4" <c:if test="${params.billDealStatus eq 4}">selected="selected"</c:if>>内部审批已驳回</option>
						<option value="5" <c:if test="${params.billDealStatus eq 5}">selected="selected"</c:if>>对方审批已驳回</option>
					</select>
				</div>
			</li>
			<!-- <li class="range"><button type="reset" id="resetButton" onclick="resetformData();">重置</button></li> -->
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
		<!--列表区-->
		<table class="table_pure interwork_list">
			<thead>
			<tr>
				<td style="width: 25%">供应商</td>
				<%--<td style="width: 9%">结账周期（天）</td>--%>
				<td style="width: 15%">出账日期(日)</td>
				<%--<td style="width: 12%">利息明细</td>--%>
				<td style="width: 15%">创建人</td>
				<td style="width: 15%">创建时间</td>
				<td style="width: 15%">状态</td>
				<td style="width: 15%">操作</td>
			</tr>
			<%--<tr>
				<td colspan="8"></td>
			</tr>--%>
			</thead>
			<tbody>
			<c:forEach var="buyBillCycle" items="${searchPageUtil.page.list}">
				<tr class="text-center">
					<td>${buyBillCycle.sellerCompanyName}</td>
					<%--<td>${buyBillCycle.checkoutCycle}</td>--%>
					<td>${buyBillCycle.billStatementDate}</td>
					<%--<td>
					   <span class="layui-btn layui-btn-mini layui-btn-warm" onclick="showBillCycleInterest('${buyBillCycle.id}');">查看利息明细</span>
					</td>--%>
					<td>${buyBillCycle.createName}</td>
					<td>${buyBillCycle.createDate}</td>
					
					<td>
					   <div>
					      <c:choose>
							<c:when test="${buyBillCycle.billDealStatus==1}">待内部审批</c:when>
							<c:when test="${buyBillCycle.billDealStatus==2}">待对方审批</c:when>
							<c:when test="${buyBillCycle.billDealStatus==3}">审批已通过</c:when>
							<c:when test="${buyBillCycle.billDealStatus==4}">内部审批已驳回</c:when>
							<c:when test="${buyBillCycle.billDealStatus==5}">对方审批已驳回</c:when>
						    <c:when test="${buyBillCycle.billDealStatus==6}">修改待审批</c:when>
						  </c:choose>
					   </div>
						<%--<div class='opinion_view'>
							<span class='orange'>查看审批流程</span>
							<!--流程弹出框-->
							<div class='opinion'>
								<b></b>
								<h3>审批流程</h3>
								<div>
									<p>发起申请<span></span></p>
									<div class='clear'>
										<h4>${buyBillCycle.createName}</h4>
										<span><fmt:formatDate value="${buyBillCycle.createDate}" type="both" /></span>
									</div>
								</div>
								<c:forEach items="${applyPurchase.pocessList}" var="pocess">
									<div>
										<p>
											<c:choose>
												<c:when test="${pocess.startIndext && pocess.status=='0'}">审批中</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='0'}">等待审批</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='1'}">已通过</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='2'}">已驳回</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='3'}">已转交</c:when>
												<c:when test="${!pocess.startIndext && pocess.status=='4'}">已撤销</c:when>
											</c:choose>
											<span></span></p>
										<div class='clear'>
											<h4>${pocess.userName}</h4>
											<c:if test="${pocess.remark != null}">
												<p>(${pocess.remark})</p>
											</c:if>
											<span><fmt:formatDate value="${pocess.endDate}" type="both" /></span>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>--%>
					</td>
					<td>
					  <!-- <div> -->
					    <c:choose>
					    <%--<c:when test="${buyBillCycle.billDealStatus==1}">
					       <!-- <div class="bill_approval layui-btn layui-btn-mini layui-btn-normal">我要审批</div>  -->
					        <span class="layui-btn layui-btn-mini" onclick="showBillCycleAgree('${buyBillCycle.id}');">我要审批</span>
					    </c:when>--%>
						<c:when test="${buyBillCycle.billDealStatus==3}">
						   <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billCycle/updateBillCycleJump?billCycleId=${buyBillCycle.id}','buyer')">再次修改</a>
						   <%-- <a href="platform/billCycle/updateBillCycleJump?billCycleId=${buyBillCycle.id}"  class="layui-btn layui-btn-mini layui-btn-danger">再次修改</a> --%>
						</c:when>
						<c:when test="${buyBillCycle.billDealStatus==4}">
						   <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billCycle/updateBillCycleJump?billCycleId=${buyBillCycle.id}','buyer')">再次修改</a>
						</c:when>
						<c:when test="${buyBillCycle.billDealStatus==5}">
						   <a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billCycle/updateBillCycleJump?billCycleId=${buyBillCycle.id}','buyer')">再次修改</a>
						</c:when>
						<%--<c:when test="${buyBillCycle.billDealStatus==6}">
							<a href="javascript:void(0);" class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billCycle/updateBillCycleJump?queryType=0&billCycleId=${buyBillCycle.id}','buyer')">再次修改</a>
						</c:when>--%>
					  </c:choose>
					  <%--<c:choose>
					    <c:when test="${buyBillCycle.billDealStatus==1}">
					       <a onclick="deleteBillCycle('${buyBillCycle.id}');" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>
					    </c:when>
					    <c:when test="${buyBillCycle.billDealStatus==4}">
					       <a onclick="deleteBillCycle('${buyBillCycle.id}');" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>
					    </c:when>
					  </c:choose> --%>
					    
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="pager">${searchPageUtil.page }</div>
	</form>
</div>
<!--利息明细-->
<div id="linkInterest" class="interest" style="display:none;">
  <div>
    <table class="table_yellow">
      <thead>
      <tr>
        <td style="width:33%">逾期（天）</td>
        <td style="width:33%">月利率（%）</td>
        <td style="width:33%">利息计算方式</td>
      </tr>
      </thead>
      <tbody id="interestBody">
      </tbody>
    </table>
  </div>
</div>
<!--审批流程-->
 <!-- <div id="linkApprovalProcess" class="opinion" style="display:none;">
   <div id="approvalProcess"></div>
</div>  -->
<!--结算周期审批-->
<div class="plain_frame bill_exam" style="display:none;" id="billApproval">
  <form action="">
    <ul>
      <li>
        <span>审批意见:</span>
        <label><input id="agree" type="radio" name="agreeStatus" value="2"> 同意</label>&nbsp;&nbsp;
        <label><input id="reject" type="radio" name="agreeStatus" value="4"> 驳回</label>
      </li>
      <li>
        <span>填写备注:</span>
        <textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
      </li>
    </ul>
  </form>
</div>