<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../../common/path.jsp"%>
<script type="text/javascript">
var shopList = <%=request.getAttribute("shopList")%>;
var shopArrayStr = JSON.parse(JSON.stringify(shopList));
var form;
layui.use('form', function(){
  	form = layui.form;
	form.render("select");
	// 删除采购凭证
	$('.voucherImg').on('click','b',function(){
	  layer.confirm('确定要删除该图片吗？</p>',{
	    icon:3,
	    skin:'pop',
	    title:'提醒',
	    closeBtn:2
	  },function(index){
	    layer.close(index);
	    $(this).parent().remove();
	  }.bind(this));
	});
	$('#planAdd').click(function(){
		var saleDateId = "saleDate_"+(new Date()).valueOf();
		var str="<td><b></b></td>"+
					"<td><input type='text' class='unAllowUpdate' name='skuOid' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly'>" +
					"<input type='hidden' name='productType' value=''>"+
					"<input type='hidden' name='unitId' value=''>"+
					"<input type='hidden' name='unitName' value=''>"+
					"<input type='hidden' name='productName' value=''>"+
	               	"<input type='hidden' name='skuCode' value=''></td>"+
					"<td name='mainPictureUrlTd'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='productCode' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly'></td>"+
					"<td><input type='text' class='unAllowUpdate' name='skuName' onclick='showInterflowProduct(this);' placeholder='选择商品' readonly='readonly'></td>"+
					"<td style='overflow:visible;'>"+$('#addDeptSelect').html()+"</td>"+
					"<td><input type='number' min='0' name='salesNum' class='unAllowUpdate' value='0'></td>"+
					"<td><input type='number' min='0' name='monthlyForecast' class='unAllowUpdate' value='0'></td>"+
					"<td><input type='text' name='promotionsRemark' class='unAllowUpdate' ></td>"+ 
					"<td><input type='text' id='"+saleDateId+"' lay-verify='date' placeholder='开始日期 - 结束日期' class='unAllowUpdate'>"+ 
	               	"<input type='hidden' name='startDate' value=''>"+
	               	"<input type='hidden' name='endDate' value=''>"+
	               	"<input type='hidden' name='saleDays' value=''/><input type='hidden' name='planType' value='1'/></td>";
	  var tbl=$('.purchasePlan tr:eq(-1)');
	  addRow(str,tbl);
	  form.render("select");
	  loadDateScope(saleDateId);
	});
	// 删除采购计划
	$('.purchasePlan').on('click','tbody td:first-child b',function(){
	  layer.confirm('您确定要删除此计划采购的商品吗？</p>',{
	    icon:3,
	    skin:'popBuyer btnCenter',
	    title:'提醒',
	    closeBtn:0
	  },function(index){
	    layer.close(index);
	    delRow(this);
	  }.bind(this));
	});
	// 提交保存
	$("#submitButton").click(function(){
		var skuoIdArr="";
		var shopError = "";
		var goodsArrar=[];
		var pronameErrot="";
		var numError="";
		var saleDateError = "";
		$("#productTbody>tr:gt(0)").each(function(i){
			var skuOid = $(this).find("input[name='skuOid']").val();
			if(skuOid != ''){
				var proCode = $(this).find("input[name='productCode']").val();
				var proName = $(this).find("input[name='productName']").val();
				var skuCode = $(this).find("input[name='skuCode']").val();
				var skuName = $(this).find("input[name='skuName']").val();
				var shopIdArr = $(this).find("td:eq(5)").find("option:selected").val();
				if(shopIdArr==''){
					shopError="第"+(i+1)+"行没有选择部门！";
					return false;
				}
				var shopId=shopIdArr.split(",")[0];
				var shopCode=shopIdArr.split(",")[1];
				var shopName=shopIdArr.split(",")[2];
				if(skuoIdArr.indexOf(skuOid+shopId+",")>-1){
					pronameErrot = "第"+(i+1)+"行商品重复！";
					return false;
				}else{
					skuoIdArr += skuOid+shopId+",";
				}
				//销售计划
				var salesNum = $(this).find("input[name='salesNum']").val();
				if(parseInt(salesNum)<=0){
					numError="第"+(i+1)+"行销售计划必须大于0！";
					return false;
				}
				//商品类型
				var productType = $(this).find("input[name='productType']").val();
				//单位
				var unitId = $(this).find("input[name='unitId']").val();
				var unitName = $(this).find("input[name='unitName']").val();
				// 周期
				var saleDate = $(this).find("input[name='startDate']").val();
				if(saleDate == ''){
					saleDateError="第"+(i+1)+"行没有选择销售周期！";
					return false;
				}
				var startDate = $(this).find("input[name='startDate']").val();
				var endDate = $(this).find("input[name='endDate']").val();
				var saleDays = $(this).find("input[name='saleDays']").val();
				var planType = $(this).find("input[name='planType']").val();
				var item = {};
	            item.shopId = shopId;
	            item.shopCode = shopCode;
	            item.shopName = shopName;
	            item.productCode = proCode;
	            item.productName = proName;
	            item.skuCode = skuCode;
	            item.skuName = skuName;
	            item.barcode = skuOid;
	            item.salesNum = salesNum;
	            item.putStorageNum=0;
	            item.productType = productType;
	            item.unitId = unitId;
	            item.unitName = unitName;
	            item.startDate = new Date(startDate);
	            item.endDate = new Date(endDate);
	            item.saleDays = saleDays;
	            item.planType = 1;
	            item.isNextStep = '0';
	            item.isExpired = '0';
	            item.monthlyForecast = $(this).find("input[name='monthlyForecast']").val();
	            item.promotionsRemark = $(this).find("input[name='promotionsRemark']").val();
	            
	            goodsArrar.push(item);
			}
		});
		if(shopError!=''){
			layer.msg(shopError,{icon:7});
			return;
		}
		if(skuoIdArr==''){
			layer.msg("请添加商品！",{icon:7});
			return;
		}
		if(pronameErrot!=''){
			layer.msg(pronameErrot,{icon:7});
			return;
		}
		if(numError!=''){
			layer.msg(numError,{icon:7});
			return;
		}
		if(saleDateError!=''){
			layer.msg(saleDateError,{icon:7});
			return;
		}
		var url = basePath+"buyer/salePlan/saveAddSalePlan";
		layer.load();
		$.ajax({
			type : "POST",
			url : url,
			async: false,
			data : {
				"goodsStr": JSON.stringify(goodsArrar)
			},
			success:function(data){
				layer.closeAll();
            	var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				if(resultObj.success){
				    layer.msg(resultObj.msg,{icon:1},function(){
					    location.reload();
				    });
				}else{
				    layer.msg(resultObj.msg,{icon:2});
				}
           	},
			error : function() {
				layer.closeAll();
				layer.msg("提交销售计划失败，请稍候重试！",{icon:2});
			}
		});
	});
	//导入
	$("#salePlanBeatchImport").click(function (e){
		e.preventDefault();
		var htmlStr = "<form class='layui-form'><div id='importExcelDiv'>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>销售周期:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<input type='text' id='importSaleDate' lay-verify='date' placeholder='开始日期 - 结束日期' class='layui-input' style='width: 200px' >"
	        +"      <input type='hidden' id='importStartDate'>"
	        +"      <input type='hidden' id='importEndDate'>"
	        +"      <input type='hidden' id='importSaleDays'>"
	        +"      <input type='hidden' id='importPlanType' value='1'>"
			+"	</div>"
			+"</div>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>部门:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<select id='importShop' lay-verify='required' lay-search='' lay-filter='pageFilter'>"
	        +"      	<option value='''>直接选择或搜索选择</option>";
		$.each(shopArrayStr,function(i){
			var shopOptionStr = shopArrayStr[i].id+","+shopArrayStr[i].shopCode+","+shopArrayStr[i].shopName;
			htmlStr +="<option value='"+shopOptionStr+"'>"+shopArrayStr[i].shopName+"</option>";
		});
        htmlStr +="      </select>"
			+"	</div>"
			+"</div>"
			+"<div class='layui-inline' >"
			+"	<label class='layui-form-label'><span class='red'>*</span>选择文件:</label>"
			+"	<div class='layui-input-inline'>"
			+"		<input type='file' name='excel' id='excel' />"
			+"	</div>"
			+"</div>"
			+"</div></form>"
			+"<script>"
			+"	loadDateImportScope('importSaleDate');"
			//+" 	$('#importStartDate').val($('#startDate').val());"
			//+" 	$('#importEndDate').val($('#endDate').val());"
			//+" 	$('#importSaleDays').val($('#saleDays').val());"
			+"<\/script>";
		layer.open({
			type:1,
	    	area: ['400px', 'auto'],
			fix: false, //不固定
			maxmin: true,
			shadeClose: true,
        	shade:0.4,
			title:"导入数据",
	    	content:htmlStr,
	    	btn:['确定','取消','下载模板'],
			yes:function(){
				if($("#importSaleDate").val() == ""){
					layer.msg("请选择销售周期！",{icon:2});
					return false;
				}
				if($("#importShop").val() == ""){
					layer.msg("请选择部门！",{icon:2});
					return false;
				}
				if($("#excel").val() == ""){
					layer.msg("请选择excel文件！",{icon:2});
					return false;
				}else{
					layer.load();
					var shop = $("#importShop").val();
					var fileObj = document.getElementById("excel").files[0];
					var url = basePath+"download/importSalePlanExcelNew";
					var formDate = new FormData();
					formDate.append("excel", fileObj);
					formDate.append("shopId", shop.split(",")[0]);
					formDate.append("shopCode", shop.split(",")[1]);
					formDate.append("shopName", shop.split(",")[2]);
					formDate.append("importStartDate", $("#importStartDate").val());
					formDate.append("importEndDate", $("#importEndDate").val());
					formDate.append("importSaleDays", $("#importSaleDays").val());
					formDate.append("importPlanType", '1');
					formDate.append("importExcelTrade_excel", "excel");
					var xhr = new XMLHttpRequest();
					xhr.open("post", url, true);
					xhr.send(formDate);
					xhr.onreadystatechange = function() {
						if (xhr.readyState == 4 && xhr.status == 200) {
							var b = xhr.responseText;
							var result = eval('(' + b + ')');
				            var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				            if(resultObj.importStatus){
								layer.closeAll();
								layer.msg(resultObj.importMsg,{icon:1},function(){
								    location.reload();
							    });
							}else{
								layer.closeAll('loadding');
							    layer.msg(resultObj.importMsg,{icon:2});
							}
						}else{
							layer.closeAll('loadding');
						}
					}
				}
			},btn3:function(){
			    download('${basePath}statics/file/salePlanModel.xls');
			}
		});
		form.render("select");
	});
});
//销售日期范围
loadDateScope("saleDate_0");
//日期范围
function loadDateScope(saleDateId){
	layui.use('laydate', function(){
	    var laydate = layui.laydate;
	    laydate.render({
		  elem: '#'+saleDateId,
		  range: true,
		  done: function(value, date){//选择后回调
		    var scope = value.replace(/\s+/g,"").split("-");
		    var startDate =  scope[0] + "-" + scope[1] + "-" + scope[2];
		    var endDate =  scope[3] + "-" + scope[4] + "-" + scope[5];
		    var saleDays = DateDiff(startDate,endDate);
		    $('#'+saleDateId).parent().find("input[name='startDate']").val(startDate);
		    $('#'+saleDateId).parent().find("input[name='endDate']").val(endDate);
		    $('#'+saleDateId).parent().find("input[name='saleDays']").val(saleDays);
		  }
		});
	});
}
//计算天数差的函数，通用  
function  DateDiff(sDate1,  sDate2){    //sDate1和sDate2是2006-12-18格式  
    var  aDate,  oDate1,  oDate2,  iDays  
    aDate  =  sDate1.split("-")  
    oDate1  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])    //转换为12-18-2006格式  
    aDate  =  sDate2.split("-")  
    oDate2  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])  
    iDays  =  parseInt(Math.abs(oDate1  -  oDate2)  /  1000  /  60  /  60  /24) + 1;    //把相差的毫秒数转换为天数   + 1
    return  iDays  
}
function loadDateImportScope(saleDate){
	layui.use('laydate', function(){
	    var laydate = layui.laydate;
	    laydate.render({
		  elem: '#'+saleDate,
		  range: true,
		  done: function(value, date){//选择后回调
		    var scope = value.replace(/\s+/g,"").split("-");
		    var startDate =  scope[0] + "-" + scope[1] + "-" + scope[2];
		    var endDate =  scope[3] + "-" + scope[4] + "-" + scope[5];
		    var saleDays = DateDiff(startDate,endDate);
		    $("#importStartDate").val(startDate);
		    $("#importEndDate").val(endDate);
		    $("#importSaleDays").val(saleDays);
		  }
		});
	});
}
// 下载
function download(src) {
	var $a = $("<a></a>").attr("href", src).attr("download", "销售计划导入模版.xls");
    $a[0].click();
}
//商品弹出框显示
function showInterflowProduct(obj) {
	var id=$(obj).parents("tr").attr("id")
	,url = basePath + "buyer/applyPurchaseHeader/loadAllProductList?selectProductId="+id;
	layer.open({
        type: 2,
        title: '选择商品',
        area: ['900px', '600px'],
		fix: false,
		maxmin: true,
		shadeClose: true,
       	shade:0.4,
        content: url,
    });
}
</script>
<div id="addPurchaseDiv">
	<!--搜索部分-->
	<form class="layui-form" action="">
		<input type="hidden" id="addSaleDateNum" value="0">
		<div class="search_supplier">
			<b>提报计划外销售计划</b>
		</div>
		<!--采购列表部分-->
		<div class="purchasePlanOut mt" style="overflow:visible;">
	        <table class="table_pure purchasePlan mt" style="width: 100%">
	          <thead>
	          <tr>
	            <td width="40px">操作</td>
	            <td width="150px">条形码</td>
	            <td width="100px">图片</td>
	            <td width="150px">货号</td>
	            <td width="150px">规格名称</td>
	            <td width="150px">部门</td>
	            <td width="80px">销售计划</td>
	            <td width="80px">月度预计总量</td>
	            <td width="150px">活动说明</td>
	            <td width="150px">销售周期</td>
	          </tr>
	          </thead>
	          <tbody id="productTbody">
	          	<tr style="display: none"><td colspan="7"></td></tr>
	          	<tr id="bbb">
	               <td><b></b></td>
	               <td>
	               		<input type="text" class="unAllowUpdate" name="skuOid" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly"">
	               		<input type="hidden" name="productType" value="">
	               		<input type="hidden" name="unitId" value="">
	               		<input type="hidden" name="unitName" value="">
	               		<input type="hidden" name="productName" value="">
	               		<input type="hidden" name="skuCode" value="">
	               </td>
	               <td name="mainPictureUrlTd"></td>
	               <td><input type="text" class="unAllowUpdate" name="productCode" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly"></td>
	               <td><input type="text" class="unAllowUpdate" name="skuName" onclick="showInterflowProduct(this);" placeholder="选择商品" readonly="readonly"></td>
	               <td style="overflow:visible;">
	               	<select id="shopId" name="shopId" lay-verify="required" lay-search="" lay-filter="pageFilter">
						<option value="">直接选择或搜索选择</option>
						<c:forEach var="shop" items="${shopList2}">
							<option value="${shop.id},${shop.shopCode},${shop.shopName}">${shop.shopName}</option>
						</c:forEach>
					</select>
	               </td>
	               <td><input type="number" value="0" min="0" name="salesNum" class="unAllowUpdate" ></td><!-- 销量 -->
	               <td><input type="number" value="0" min="0" name="monthlyForecast" class="unAllowUpdate"  ></td><!-- 月度预计总量 -->
	               <td><input type="test" class="unAllowUpdate" name="promotionsRemark"></td><!-- 活动说明 -->
	               <td>
		               	<input type="text" id="saleDate_0" lay-verify="date" placeholder="开始日期 - 结束日期" class="unAllowUpdate">
		               	<input type="hidden" name="startDate" value="">
		               	<input type="hidden" name="endDate" value="">
		               	<input type="hidden" name="saleDays" value=""/>
		               	<input type="hidden" name="planType" value="1"/>
	               </td>
	            </tr>
	          </tbody>
	        </table>
	      </div>
	</form>
	<button class="tr_add_buyer" id="planAdd"></button>
	<div class="submitButton">
       	<button class="layui-btn layui-btn-normal" id="submitButton">确认提交</button>
       	<button class="layui-btn layui-btn-danger" id="salePlanBeatchImport">
			<i class="layui-icon">&#xe98e;</i>导入
		</button>
     </div>
	<!--选择商品弹出框-->
    <div class="commSelPlan" id="commSelPlan"></div>
    <div id="addDeptSelect" hidden="true">
		<select name="shopId" lay-verify="required" lay-search="" lay-filter="pageFilter">
			<option value="">直接选择或搜索选择</option>
			<c:forEach var="shop" items="${shopList2}">
				<option value="${shop.id},${shop.shopCode},${shop.shopName}">${shop.shopName}</option>
			</c:forEach>
		</select>
	</div>
</div>