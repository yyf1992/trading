<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
// $("input[name='checkAll']").click(function(){
// 	var checked = $(this).is(":checked");
// 	$("input[name='checkone']").prop("checked",checked);
// });
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(sysVerifyTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'edit'){
	    	//修改
	    	edit(data.id);
	    }else if(obj.event === 'setVerify'){
	    	//详情
	    	setVerify(data.id);
	    }else if(obj.event === 'deleteVerify'){
	    	//是否禁用启用
	    	deleteVerify(data.id,data.status);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('sysVerifyTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	title: $("#title").val()
	        }
	      });
	    },
	    addVerify: function(){
	    	leftMenuClick(this,'platform/sysVerify/loadAddHtml','system','17100920500477275184');
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="title" id="title" autocomplete="off" placeholder='审批标题'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="addVerify" button="新增">新增</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'sysVerifyTable',
		height:'full-90',
		url:'<%=basePath %>platform/sysVerify/loadDataJson'
	}" lay-filter="sysVerifyTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'id', sort: true,show:false}">id</th>
            <th lay-data="{field:'title', sort: true,show:true}">审批标题</th>
            <th lay-data="{field:'menuName', sort: true,show:true}">对应菜单</th>
            <th lay-data="{field:'verifyType',sort: true,show:true,templet: '#verifyTypeTpl', align: 'center'}">审批类型</th>
            <th lay-data="{field:'status', sort: true,show:true,templet: '#isDelTpl', align: 'center'}">状态</th>
            <th lay-data="{field:'createName', sort: true,show:true, align: 'center'}">创建人</th>
            <th lay-data="{field:'createDate', sort: true,show:true}">创建时间</th>
            <th lay-data="{align:'center', toolbar: '#operate',show:true}">操作</th>
        </tr>
    </thead>
</table>
<!-- 操作 -->
<script type="text/html" id="operate">
	<a class="layui-btn layui-btn-update layui-btn-xs" lay-event="edit" button="修改">修改</a>
	<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="setVerify" button="查看详情">查看详情</a>
	{{#  if(d.status == 0){ }}
		<span><a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteVerify" button="禁用">禁用</a></span>
	{{#  } else if(d.status == 1){}}
		<span><a class="layui-btn layui-btn-xs" lay-event="deleteVerify" button="启用">启用</a></span>
	{{#  } }}
	
</script>
<!-- 状态转换 -->
<script type="text/html" id="isDelTpl">
	{{#  if(d.status == 0){ }}
		<span style="color: green;">启用</span>
	{{#  } else if(d.status == 1){}}
		<span style="color: red;">禁用</span>
	{{#  } }}
</script>
<!-- 审批类型 -->
<script type="text/html" id="verifyTypeTpl">
	{{#  if(d.verifyType == 0){}}
		<span>固定审批流程</span>
	{{#  } else if(d.verifyType == 1){}}
		<span>自动审批流程</span>
	{{#  } }}
</script>
<script>
//修改
function edit(id){
	leftMenuClick(this,'platform/sysVerify/loadUpdateHtml?id='+id,'system','17100920500477275184');
}
//查看详情
function setVerify(id){
	leftMenuClick(this,'platform/sysVerify/loadDetails?id='+id,'system','17100920500477275184');
}
function deleteVerify(id,status){
	$.ajax({
		type : "POST",
		url:basePath+"platform/sysVerify/deleteVerify",
		data:{
			"id":id,
			"status":status
		},
		success:function(data){
			var result = eval('(' + data + ')');
			var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
			if(resultObj.success){
				leftMenuClick(this,'platform/sysVerify/sysVerifyList','system','17100920500477275184');
				layer.msg(resultObj.msg,{icon:1});
			}else{
				layer.msg(resultObj.msg,{icon:2});
				return false;
			}
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>
