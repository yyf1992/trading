<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(outwharealogListTableFilter)', function(obj){
	    var data = obj.data;
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('outwharealogListTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	productCode: $("#productCode").val(),
	        	barcode: $("#barcode").val(),
	        	skuCode: $("#skuCode").val(),
	        	subcompanyName: $("#subcompanyName").val(),
	        	shopName: $("#shopName").val(),
	        	whareaName: $("#whareaName").val(),
	        	minDate: $("#minDate").val(),
	        	maxDate: $("#maxDate").val(),
	        	createName: $("#createName").val()
	        }
	      });
	    },
	    //导出
	    exportExcel: function(){
			var formData = [
				"subcompanyName="+$("#subcompanyName").val(),
				"&barcode="+$("#barcode").val(),
				"&productCode="+$("#productCode").val(),
				"&skuCode="+$("#skuCode").val(),
				"&shopName="+$("#shopName").val(),
				"&whareaName="+$("#whareaName").val(),
				"&minDate="+$("#minDate").val(),
				"&maxDate="+$("#maxDate").val(),
				"&createName="+$("#createName").val()
			];
			var url = basePath + "outwharealogListDownload/outwharealogList?" + formData.join("");
			window.open(url);
	    },
	    //导入
	    loadImportHtml: function(){
	    	$.ajax({
				url:basePath+"platform/baseInfo/outwharealog/loadImportExcelHtml",
				type:"post",
				async:false,
				success:function(data){
					layer.open({
						type:1,
						title:"导入数据",
						skin: 'layui-layer-rim',
		  		    	area: ['400px', 'auto'],
		  		    	content:data,
		  		    	btn:['确定','取消','下载模板'],
						yes:function(){
							var formData = new FormData(); 
							var warehouse = $("#warehouse").val();
							if (warehouse == "") {
								layer.msg("请选择导入的仓库名称！",{icon:2});	
								return;
							}
							formData.append("warehouse", warehouse);					
							formData.append("excel", document.getElementById("file1").files[0]);
							$.ajax({
								url : basePath+"platform/baseInfo/outwharealog/importOutWhareaLogData",
								async : false,
								data : formData,
								type : "post",
								/**
								 *必须false才会自动加上正确的Content-Type
								 */
								contentType : false,
								/**
								 * 必须false才会避开jQuery对 formdata 的默认处理
								 * XMLHttpRequest会对 formdata 进行正确的处理
								 */
								processData : false,
								success : function(data) {
									var result = eval('(' + data + ')');
									var resultObj = isJSONObject(result) ? result: eval('(' + result + ')');
									if(resultObj.status  == "success"){
										layer.closeAll();
										layer.msg(resultObj.msg, {icon : 1});
										leftMenuClick(this,'platform/baseInfo/outwharealog/loadOutwharealogList','buyer','18013010144715396017');
									}else{
										layer.msg(resultObj.msg, {icon : 2});
									}
								},
								error: function(data) {
									layer.closeAll();
									layer.msg(resultObj.fail, {icon : 2});
									leftMenuClick(this,'platform/baseInfo/outwharealog/loadOutwharealogList','buyer','18013010144715396017');
								}
							});
						},btn3:function(){
						    var url ="platform/baseInfo/outwharealog/downloadExcel";  
						    url = encodeURI(url);
						    location.href = url;  
						}
					})
				},error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
		    });
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="subcompanyName" id="subcompanyName" autocomplete="off" placeholder='子公司'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="shopName" id="shopName" autocomplete="off" placeholder='店铺名称'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="productCode" id="productCode" autocomplete="off" placeholder='货号'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="barcode" id="barcode" autocomplete="off" placeholder='条形码'>
  </div>
  <div class="layui-input-inline">
    <select id="whareaName" name="whareaName" lay-verify="required" lay-search="">
	 <option value="">所有仓库</option>
	 <option value="菜鸟仓">菜鸟仓</option>
	 <option value="京东仓">京东仓</option>
    </select>
  </div>
  <div class="layui-inline">
  	<div class="layui-input-inline">
      <input type="text" name="minDate" id="minDate"  style="width:79px" lay-verify="date" placeholder="开始日" class="layui-input" value="${searchPageUtil.object.minDate}">
    </div>
    <div class="layui-input-inline">
      <input type="text" name="maxDate" id="maxDate"  style="width:80px" lay-verify="date" placeholder="截止日" class="layui-input" value="${searchPageUtil.object.maxDate}">
    </div>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="createName" id="createName" autocomplete="off" placeholder='创建人'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="exportExcel">导出</button>
  <button class="layui-btn" data-type="loadImportHtml">导入</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:20,
		id:'outwharealogListTable',
		height:'full-90',
		url:'<%=basePath %>platform/baseInfo/outwharealog/loadDataJson'
	}" lay-filter="outwharealogListTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'subcompanyName',width:120, sort: true,show:true}">子公司</th>
            <th lay-data="{field:'shopName', sort: true,show:true}">店铺</th>
            <th lay-data="{field:'barcode',width:120, sort: true,show:true,showToolbar:true}">条形码</th>
            <th lay-data="{field:'productCode',width:120, sort: true,show:true}">货号</th>
            <th lay-data="{field:'price', sort: true,show:true}">成本价</th>
            <th lay-data="{field:'outWhareaStock', sort: true,show:true}">外仓在仓数量</th>
            <th lay-data="{field:'outWhareaWayStock', sort: true,show:true}">外仓在途数量</th>
            <th lay-data="{field:'outWhareaTotalPrice', sort: true,show:true}">入外仓库存金额</th>
            <th lay-data="{field:'whareaName', sort: true,show:true}">仓库名称</th>
            <th lay-data="{field:'createName', sort: true,show:true}">创建人</th>
            <th lay-data="{field:'createDate', sort: true,show:true}">导入时间</th>
        </tr>
    </thead>
</table>