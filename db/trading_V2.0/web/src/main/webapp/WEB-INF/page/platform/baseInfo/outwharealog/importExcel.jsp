<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<div style="text-align: center;margin-top: 16px">
	 <div>
		 <label>仓&emsp;&emsp;库:</label>
		 <select id="warehouse" name="warehouse" lay-verify="required" lay-search="" style="height: 25px;width: 135px;">
			<option value="">请选择</option>
			<option value="菜鸟仓">菜鸟仓</option>
			<option value="京东仓">京东仓</option>
		 </select>
	 </div>
	 <br>
	 <input type="file"id="file1"/>
	 <br>
	 <div class="materialRemark size_sm mp30">
	 	<p>
	 		<span>说明：导入的表格样式请严格符合下载模板中的样式</span>
	 	</p>
	 </div>
</div>