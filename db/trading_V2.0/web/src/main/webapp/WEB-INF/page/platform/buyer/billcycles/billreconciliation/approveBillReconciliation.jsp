<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../common/path.jsp"%>
<%-- <%@ include file="../../../common/common.jsp"%>  --%>
<head>
	<title>账单对账列表</title>
	<script type="text/javascript" src="<%=basePath%>/js/billmanagement/buyer/billreconciliation/billReconciliationList.js"></script>
	<script type="text/javascript" src="<%=basePath%>/statics/platform/js/date.js"></script>
	<%--<script src="<%=basePath%>/statics/platform/js/common.js"></script>--%>
	<%-- <script type="text/javascript" src="<%=basePath%>/js/billCycle/approval.js"></script> --%>
	<link rel="stylesheet" href="<%=basePath %>/statics/platform/css/common.css">
	<link rel="stylesheet" href="<%=basePath%>statics/platform/css/bill.css">
	<link rel="stylesheet" href="<%=basePath%>/statics/platform/css/purchase_manual.css">
	<script type="text/javascript">
	$(function(){
	    //预加载查询条件
        $("#reconciliationId").val("${params.reconciliationId}");
        $("#sellerCompanyName").val("${params.sellerCompanyName}");
        $("#createBillUserName").val("${params.createBillUserName}");
        $("#startDate").val("${params.startBillStatementDate}");
        $("#endDate").val("${params.endBillStatementDate}");
        $("#startTotalAmount").val("${params.startTotalAmount}");
        $("#endTotalAmount").val("${params.endTotalAmount}");
        $("#billDealStatus").val("${params.billDealStatus}");
		//重新渲染select控件
		var form = layui.form;
		form.render("select");

	});

    //标签页改变
    /*function setStatus(obj,status) {
		$("#billDealStatus").val(status);

        $('.tab a').removeClass("hover");
        $(obj).addClass("hover");
        loadPlatformData();
    }*/

    //重置查询条件
    function resetformData(){
        $("#reconciliationId").val("");
    	$("#sellerCompanyName").val("");
        $("#createBillUserName").val("");
    	$("#startDate").val("");
    	$("#endDate").val("");
        $("#startTotalAmount").val("");
        $("#endTotalAmount").val("");
    	$("#billDealStatus").val("0");
    }

    // 审核
    function approveBillReconciliation(id){
        if($(".verifyDetail").length>0)$(".verifyDetail").remove();
        $.ajax({
            url:basePath+"platform/tradeVerify/verifyDetailByRelatedId",
            data:{"id":id},
            success:function(data){
                var detailDiv = $("<div style='padding:0px;z-index:99999'></div>");
                detailDiv.addClass("verifyDetail");
                detailDiv.html(data);
                detailDiv.appendTo($("#reconciliationContent"));
                detailDiv.find("#verify_side").addClass("show");
            },
            error:function(){
                layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
            }
        });
    }

    //一键通过
    function verifyAll(){
        var checkedObj = $("#reconciliationContent input[name='checkone']:checked");
        if(checkedObj.length == 0){
            layer.msg('至少选择一条数据！');
            return;
        }else{
            layer.msg('你确定一键通过？', {
                time : 0,//不自动关闭
                btn : [ '确定', '取消' ],
                yes : function(index) {
                    layer.close(index);
                    var orderIdArray = "";
                    $.each(checkedObj,function(i,o){
                        orderIdArray += $(this).val() + ",";
                    });
                    layer.load();
                    $.ajax({
                        url:basePath+"platform/buyer/billReconciliation/verifyBillRecAll",
                        async:false,
                        data:{"purchaseId":orderIdArray},
                        success:function(data){
                            //selectData();
                            layer.msg("审批成功！",{icon:1});
                            leftMenuClick(this,'platform/buyer/billReconciliation/approveBillReconciliation','buyer')
                        },
                        error:function(){
                            layer.msg("获取审批类型失败，请稍后重试！",{icon:2});
                        }
                    });
                }
            });
        }
    }
    function selectAll(obj) {
        if ($(obj).is(":checked")) {
            $("#reconciliationContent input[name='checkone']").prop("checked", true);
        } else {
            $("#reconciliationContent input[name='checkone']").prop("checked", false);
        }
    }
	</script>
	<style>
		.breakType td{
			word-wrap: break-word;
			white-space: inherit;
			word-break: break-all;
		}
	</style>
</head>
<!--内容-->
<div id="reconciliationContent">
	<!--搜索栏-->
	<form id="searchForm" class="layui-form" action="platform/buyer/billReconciliation/approveBillReconciliation">

		<ul class="order_search">
			<li class="ship nomargin">
				<label>对账单号:</label>
				<input id="reconciliationId" type='text' placeholder='输入对账单号' name="reconciliationId" />
			</li>
			<li class="ship nomargin">
				<label>供应商名称:</label>
				<input id="sellerCompanyName" type='text' placeholder='输入供应商名称' name="sellerCompanyName" />
			</li>
			<li class="ship nomargin">
				<label>采购员:</label>
				<input type="text"  placeholder='输入采购员' id="createBillUserName" name="createBillUserName" >
			</li>
			<li class="spec nomargin">
				<label>出账日期:</label>
				<div class="layui-input-inline">
					<input type="text" name="startBillStatementDate" id="startDate" lay-verify="date"  class="layui-input" placeholder="开始日">
				</div>
				-
				<div class="layui-input-inline">
					<input type="text" name="endBillStatementDate" id="endDate" lay-verify="date" class="layui-input" placeholder="截止日">
				</div>
			</li>
			<li class="ship nomargin">
				<label>账期总金额:</label>
				<input type="number" placeholder="￥" id="startTotalAmount" name="startTotalAmount" >
				-
				<input type="number" placeholder="￥" id="endTotalAmount" name="endTotalAmount" >
			</li>
			<li class="rang">
				<button type="button" class="search" onclick="loadPlatformData();">搜索</button>
			</li>
			<li class="range"><button type="reset" class="search" onclick="resetformData();">重置查询条件</button></li>
            <a href="javascript:void(0);"
               class="layui-btn layui-btn-normal layui-btn-small" onclick="verifyAll();">
                <i class="layui-icon">&#xe6a3;</i> 一键通过
            </a>
			<!-- 分页隐藏数据 -->
			<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			<!--关联类型-->
			<input id="queryType" name="queryType" type="hidden" value="0" /> 
		</ul>
	</form>
	<!--列表区-->
	<table class="table_pure interwork_list">
	  <thead>
	    <tr>
		  <td style="width:5%"><input type="checkbox" name="checkAll" onclick="selectAll(this);"></td>
		  <td style="width: 12%">对账单号</td>
		  <td style="width: 8%">账单周期</td>
		  <td style="width: 8%">采购员</td>
		  <td style="width: 8%">供应商</td>
		  <td style="width: 6%">采购总数量</td>
		  <td style="width: 8%">采购总金额</td>
		  <td style="width: 8%">售后总数量</td>
		  <td style="width: 8%">售后总金额</td>
		  <td style="width: 8%">运费</td>
		  <td style="width: 8%">奖惩金额</td>
		  <td style="width: 8%">预付款抵扣金额</td>
		  <td style="width: 8%">总金额</td>
		  <td style="width: 8%">单据/审批流程</td>
		  <td style="width: 15%">操作</td>

	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach var="buyBillReconciliation" items="${searchPageUtil.page.list}">
		  <%--<div class="order_list" onclick="approveBillReconciliation('${buyBillReconciliation.purchaseId}');">--%>
			  <tr class="breakType text-center">
				  <td><input type="checkbox" name="checkone" value="${buyBillReconciliation.purchaseId}"></td>
				  <td>${buyBillReconciliation.id}</td>
				  <td>${buyBillReconciliation.startBillStatementDateStr}至${buyBillReconciliation.endBillStatementDateStr}</td>
				  <td>${buyBillReconciliation.createBillUserName}</td>
				  <td>${buyBillReconciliation.sellerCompanyName}</td>
				  <td>${buyBillReconciliation.recordConfirmCount + buyBillReconciliation.recordReplaceCount}</td>
				  <td>${buyBillReconciliation.recordConfirmTotal + buyBillReconciliation.recordReplaceTotal}</td>
				  <td>${buyBillReconciliation.replaceConfirmCount + buyBillReconciliation.returnGoodsCount}</td>
				  <td>${buyBillReconciliation.replaceConfirmTotal + buyBillReconciliation.returnGoodsTotal}</td>
				  <td>${buyBillReconciliation.freightSumCount}</td>
				  <td>
			<span class="layui-btn layui-btn-mini" onclick="showCustomPayment('${buyBillReconciliation.id}');">
					${buyBillReconciliation.customAmount}</span>
				  </td>
				  <td>
			<span class="layui-btn layui-btn-mini" onclick="showAdvanceEdit('${buyBillReconciliation.id}','${buyBillReconciliation.sellerCompanyName}','${buyBillReconciliation.billDealStatus}');">
					${buyBillReconciliation.advanceDeductTotal}</span>
				  </td>
				  <td>${buyBillReconciliation.totalAmount}</td>

				  <td>
					 <%-- <div>
						  <c:choose>
							  <c:when test="${buyBillReconciliation.billDealStatus==2}">账单待审批</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==3}">已确认对账</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==4}">已驳回对账</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==5}">内部审批中</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==6}">财务待审批</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==7}">内部驳回</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==8}">财务部驳回</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==9}">奖惩待确认</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==10}">奖惩已确认</c:when>
							  <c:when test="${buyBillReconciliation.billDealStatus==11}">奖惩已驳回</c:when>
						  </c:choose>
					  </div>--%>
					  <div>
						  <a href="javascript:void(0);" onclick="openBillFile('${buyBillReconciliation.id}',
								  '${buyBillReconciliation.createBillUserName}','${buyBillReconciliation.startBillStatementDateStr}',
								  '${buyBillReconciliation.endBillStatementDateStr}');" class="approval">账单票据</a>
					  </div>
					  <div class="opinion_view">
						  <span class="orange" data="${buyBillReconciliation.purchaseId}">审批流程</span>
					  </div>
				  </td>
				  <td>
					  <span class="layui-btn layui-btn-normal layui-btn-mini" onclick="leftMenuClick(this,'platform/buyer/billReconciliation/getReconciliationDetailsNew?reconciliationId=${buyBillReconciliation.id}&recType=1','buyer')">账单详情</span><br>
					  <span class="layui-btn layui-btn-danger layui-btn-mini" onclick="approveBillReconciliation('${buyBillReconciliation.purchaseId}');">账单审批</span>
				  </td>
			  </tr>
		 <%-- </div>--%>

	  </c:forEach>
	  </tbody>
	</table>
		<div class="pager">${searchPageUtil.page }</div>
	<%--</form>--%>
	<!--收款凭据-->
	<div id="linkReceipt" class="interest" style="display:none;">
		<div id="receiptBody" class="receipt_content">
			<div class="big_img">
				<img src="">
			</div>
			<div class="del_receipt">
				<button class="layui-btn layui-btn-danger layui-btn-mini">删除</button>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="icon_list"></ul>
			</div>
		</div>
	</div>

	<!--内部选人审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="insideBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="insideAgree" type="radio" name="insideStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="insideReject" type="radio" name="insideStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="insideRemarks" name="insideRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>

	<!--最终审批-->
	<%--<div class="plain_frame bill_exam" style="display:none;" id="acceptBillRec">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="acceptAgree" type="radio" name="agreeStatus" value="5"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="acceptReject" type="radio" name="agreeStatus" value="4"> 不同意对账</label>
				</li>
				<li>
					<span>审批备注:</span>
					<textarea id="acceptRemarks" name="acceptRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>
	<!--内部审批-->
	<div class="plain_frame bill_exam" style="display:none;" id="approvalBillReconciliation">
		<form action="">
			<ul>
				<li>
					<span>审批意见:</span>
					<label><input id="purAgree" type="radio" name="approvalStatus" value="pass"> 同意对账</label>&nbsp;&nbsp;
					<label><input id="purReject" type="radio" name="approvalStatus" value="return"> 不同意对账</label>
				</li>
				<li>
					<span>填写备注:</span>
					<textarea id="approvalRemarks" name="approvalRemarks" placeholder="请输入内容"></textarea>
				</li>
			</ul>
		</form>
	</div>--%>

	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillRecApproval">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">审批人</td>
					<td style="width:20%">审批时间</td>
					<td style="width:20%">审批备注</td>
				</tr>
				</thead>
				<tbody id="billRecApprovalShow">
				</tbody>
			</table>
		</div>
	</div>

	<!--自定义付款项-->
	<div class="plain_frame price_change" id="customPaymentDiv" style="display:none;height: 400px;overflow-y: auto">
		<ul id="billCustomBody">
		</ul>
	</div>
	<!--回显自定义付款详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillCustomList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">添加人</td>
					<td style="width:10%">添加时间</td>
					<td style="width:10%">奖惩类型</td>
					<td style="width:10%">奖惩金额</td>
					<td style="width:10%">奖惩添加备注</td>
					<td style="width:10%">审批状态</td>
					<td style="width:10%">审批人</td>
					<td style="width:10%">审批时间</td>
					<td style="width:10%">奖惩审批备注</td>
					<td style="width:10%">奖惩单据</td>
				</tr>
				</thead>
				<tbody id="billRecCustomFileBody">
				</tbody>
			</table>
		</div>
		<!--自定义付款款凭据-->
		<div id="showCustomFiles" class="receipt_content" style="display:none;">
			<div id="showCustomBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileCustomList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--添加账单附件-->
	<div class="plain_frame bill_exam" style="display:none;" id="billFiles">
		<ul id="billFilesBody">
		</ul>
	</div>
	<!--回显账单附件-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showBillFileList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:20%">上传人</td>
					<td style="width:20%">上传时间</td>
					<td style="width:20%">上传备注</td>
					<td style="width:20%">上传单据</td>

				</tr>
				</thead>
				<tbody id="billRecFileBody">
				</tbody>
			</table>
		</div>
		<!--收款凭据-->
		<div id="showFiles" class="receipt_content" style="display:none;">
			<div id="showBigImg" class="big_img">
				<%--<img id="bigImg">--%>
			</div>
			<div class="view">
				<a href="#" class="backward_disabled"></a>
				<a href="#" class="forward"></a>
				<ul id="fileList" class="icon_list"></ul>
			</div>
		</div>
	</div>
	<!--预付款抵扣详情-->
	<div class="plain_frame bill_exam" style="display:none;height: 400px;overflow-y: auto" id="showAdvanceEditList">
		<div>
			<table class="table_yellow">
				<thead>
				<tr>
					<td style="width:10%">供应商</td>
					<td style="width:10%">采购员</td>
					<td style="width:10%">添加人</td>
					<td style="width:10%">添加时间</td>
					<td style="width:10%">预付款抵扣金额</td>
				</tr>
				</thead>
				<tbody id="billAdvanceEditBody">
				</tbody>
			</table>
		</div>
	</div>
</div>



<style>
	.view .icon_list {
		height: 92px;
		position: absolute;
		left: 28px;
		top: 0;
		overflow: hidden;
	}
	.view .icon_list li {
		width: 108px;
		text-align: center;
		float: left;
	}
	.view .icon_list li img {
		width: 92px;
		height: 92px;
		padding: 1px;
		border: 1px solid #CECFCE;
	}
</style>