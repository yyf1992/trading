<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="el" uri="/elfun" %>
<!--列表区-->
<table class="orderTop serviceTop">
	<tr>
		<td style="width: 70%">
			<ul>
				<li style="width: 50%">商品</li>
				<li style="width: 10%">条形码</li>
				<li style="width: 10%">类型</li>
				<li style="width: 10%">退/换货数量</li>
				<li style="width: 10%">买家发货数量</li>
				<li style="width: 10%">实际收货数量</li>
			</ul></td>
		<td style="width: 10%">申请原因</td>
		<td style="width: 10%">状态</td>
		<td style="width: 10%">操作</td>
	</tr>
</table>
<div class="orderList serviceList">
	<c:forEach var="customer" items="${searchPageUtil.page.list}">
		<div>
			<p>
				<span class="apply_time"><fmt:formatDate
						value="${customer.createDate}" type="both"></fmt:formatDate>
				</span> <span class="order_num"><span>编号:</span>
					${customer.customerCode}</span> <span class="order_name">${el:getCompanyById(customer.companyId).companyName}</span>
			</p>
			<table>
				<tr><c:set var="flag" value="0" scope="page"></c:set>
					<td><c:forEach var="customerItem" items="${customer.itemList}">
					<c:if test="${customerItem.type==0}">
						<c:set var="flag" value="${flag+1}" scope="page"></c:set>
					</c:if>
						<ul class="clear">
							<li style="width:50%;"><span class="defaultImg"></span>
								<div>
									${customerItem.productCode}|${customerItem.productName} <br>
									<span>规格代码: ${customerItem.skuCode}</span> <span>规格名称:
										${customerItem.skuName}</span>
								</div></li>
							<li style="width:10%;">${customerItem.skuOid}</li>
							<li style="width:10%;">
								<c:choose>
									<c:when test="${customerItem.type==0}">换货</c:when>
									<c:when test="${customerItem.type==1}">退货</c:when>
									<c:when test="${customerItem.type==2}">换货转退货</c:when>
								</c:choose>
							</li>
							<li style="width:10%;">${customerItem.goodsNumber}</li>
							<li style="width:10%;">${customerItem.receiveNumber}</li>
							<li style="width:10%;">
								<c:choose>
									<c:when test="${customer.status==0}"><input type="number" min="0" style="width: 50px" value="${customerItem.goodsNumber}" ></c:when>
									<c:when test="${customer.status==1}"><input type="number" min="0" style="width: 50px;border: 0" value="${customerItem.confirmNumber}" readonly="readonly"></c:when>
								</c:choose>
								<input type="hidden" name="customerItemId" value="${customerItem.id}"/>
							</li>
						</ul>
						<c:if test="${customerItem.deliveryList !=null && customerItem.deliveryList.size()>0 }">
							<table style="width: 100%">
								<thead>
									<tr style="background: #F7F7F7;height: 20px;">
										<td style="width:30px;">发货单号</td>
										<td style="width:30px;">发货类型</td>
										<td style="padding-top: 3px;width:30px;">发货数量</td>
										<td style="padding-top: 3px;width:30px;">到货数量</td>
										<td style="width:30px;">发货状态</td>
										<td style="width:30px;">创建发货日期</td>
										<td style="width:30px;">发货日期</td>
										<td style="width:30px;">到货日期</td>
									</tr>
								<c:forEach items="${customerItem.deliveryList}" var="deliveryItem">
									<tr>
										<td>${deliveryItem.deliver_no}</td>
										<td>
											<c:choose>
												<c:when test="${deliveryItem.dropship_type==0}">正常发货</c:when>
												<c:when test="${deliveryItem.dropship_type==1}">代发客户</c:when>
												<c:when test="${deliveryItem.dropship_type==2}">代发菜鸟仓</c:when>
												<c:when test="${deliveryItem.dropship_type==3}">代发京东仓</c:when>
											</c:choose>
										</td>
										<td style="padding-top: 3px;">${deliveryItem.delivery_num}</td>
										<c:choose>
											<c:when test="${deliveryItem.recordstatus==0}">
												<!-- 未发货 -->
												<td style="padding-top: 3px;">未发货</td>
												<td>未发货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>未发货</td>
												<td>未发货</td>
											</c:when>
											<c:when test="${deliveryItem.recordstatus==1}">
												<!-- 已发货 -->
												<td style="padding-top: 3px;">买家未收货</td>
												<td>已发货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.send_date}" type="date"/>
												</td>
												<td>买家未收货</td>
											</c:when>
											<c:when test="${deliveryItem.recordstatus==2}">
												<!-- 已收货 -->
												<td style="padding-top: 3px;">${deliveryItem.arrival_num}</td>
												<td>买家已收货</td>
												<td>
													<fmt:formatDate value="${deliveryItem.create_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.send_date}" type="date"/>
												</td>
												<td>
													<fmt:formatDate value="${deliveryItem.arrival_date}" type="date"/>
												</td>
											</c:when>
										</c:choose>
									</tr>
								</c:forEach>
								</thead>
							</table>
						</c:if>
					</c:forEach></td>
					<td>${customer.reason}</td>
					<td>
						<div>
							<c:choose>
								<c:when test="${customer.status==0}">待同意</c:when>
								<c:when test="${customer.status==5}">已同意</c:when>
								<c:when test="${customer.status==1}">已收货</c:when>
							</c:choose>
						</div>
						<div>
							<c:choose>
								<c:when test="${customer.arrivalType==1}">买家已发货</c:when>
								<c:otherwise>买家未发货</c:otherwise>
							</c:choose>
						</div>
						<a href="javascript:void(0)"
						onclick="leftMenuClick(this,'seller/customer/loadCustomerDetails?id=${customer.id}','sellers')"
						class="approval">申请明细</a><br> <a href="javascript:void(0)"
						onclick="showProof('${customer.proof}');"
						class="check_receipt orange">查看售后凭证</a></td>
					<td>
						<c:if test="${customer.faHuoFlag}"><div class="afterExam green_btn" onclick="createDelivery('${customer.id}');">创建发货单</div></c:if>
					</td>
				</tr>
			</table>
		</div>
	</c:forEach>
</div>
<!--分页-->
<div class="pager">${searchPageUtil.page}</div>