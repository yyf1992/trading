<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<div>
		<table class="table_yellow schedule">
			<thead>
				<tr>
					<td style="width:5%">序号</td>
			        <td style="width:15%">编号</td>
			        <!-- <td style="width:15%">部门名称</td> -->
			        <td style="width:10%">计划采购数量</td>
			        <td style="width:10%">待生成采购计划</td>
			        <td style="width:12%">要求到货日期</td>
			        <td style="width:15%">备注</td>
			        <td style="width:8%">申请人</td>
			        <td style="width:15%">申请时间</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="purchaseDetails" items="${barcodeList}" varStatus="indexNum">
					<tr>
						<td>${indexNum.count}</td>
						<td title="${purchaseDetails.apply_code}">${purchaseDetails.apply_code}</td>
						<%-- <td>${purchaseDetails.shop_name}</td> --%>
						<td>${purchaseDetails.apply_count}</td>
						<td>${purchaseDetails.overplusNum}</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd" value="${purchaseDetails.predictArred}" /></td>
						<td title="${purchaseDetails.hReamark}">${purchaseDetails.hReamark}</td>
						<td>${purchaseDetails.create_name}</td>
						<td><fmt:formatDate value="${purchaseDetails.create_date}" type="both"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
