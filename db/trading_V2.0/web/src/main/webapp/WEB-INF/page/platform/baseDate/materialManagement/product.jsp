<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../common/path.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script>
function changePopHeight(){
	  var h = $(window).height();
	  $('.table_pure').css({
	    	height:h - 350,
	    	overflow:'auto'
	  })
	}
	changePopHeight();
	window.onresize = function(){changePopHeight();}
</script>
<!--商品弹框-->
<div class="commPop">
	<form action="platform/baseDate/productCompose/loadProductHtml" id="commPopFrom" class="layui-form">
		  <div class="materialQuery mt">
		<%--     <input type="text" placeholder="货号/商品名称/条形码" id="inputSelect" name="inputSelect" value="${searchPageUtil.object.inputSelect}">&emsp; --%>
			    <label>商品货号</label>:
			    <input type="text" placeholder="输入商品货号" style="width:100px" id="productCode" name="productCode" value="${searchPageUtil.object.productCode}">
			    <label>商品名称</label>:
			    <input type="text" placeholder="输入商品名称" style="width:100px" id="productName" name="productName" value="${searchPageUtil.object.productName}">
			    <label>规格代码</label>:
			    <input type="text" placeholder="输入规格代码" style="width:100px" id="skuCode" name="skuCode" value="${searchPageUtil.object.skuCode}">
			    <label>规格名称</label>:
			    <input type="text" placeholder="输入规格名称" style="width:100px" id="skuName" name="skuName" value="${searchPageUtil.object.skuName}">
			    <label>条形码</label>:
			    <input type="text" placeholder="输入条形码" style="width:100px" id="barcode" name="barcode" value="${searchPageUtil.object.barcode}">
			    <img src="statics/platform/images/find.jpg" class="rt" onclick="selectProduct(this);">
			    <input id="productType" name="productType" type="hidden" value="0" />
			    <input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
			  	<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
			  	<input id="divId" name="page.divId" type="hidden" value="${searchPageUtil.page.divId}" />
			  	<input id="id" name="id" type="hidden" value="${searchPageUtil.object.id}" />
		  </div>
	</form>
<div>&emsp;&emsp;</div>
<div>&emsp;&emsp;</div>
  <div class="materialContent">
    <table class="table_pure">
      <thead>
      <tr>
        <td style="width:5%"></td>
        <td style="width:5%">序号</td>
        <td style="width:15%">货号</td>
        <td style="width:20%">商品名称</td>
        <td style="width:17%">规格代码</td>
        <td style="width:17%">规格名称</td>
        <td style="width:15%">条形码</td>
        <td style="width:6%">单位</td>
      </tr>
      </thead>
      <tbody id="productTbody">
	      <c:forEach var="item" items="${searchPageUtil.page.list}" varStatus="status">
	      	 <c:choose>
	      		<c:when test="${item.id != null}">
		      		<tr>
				        <td><input type="radio" value="${item.id}" name="product" onclick="chooseThis();"></td>
				        <td>${status.count}</td>
				        <td>${item.product_code}</td>
				        <td>${item.product_name}</td>
				        <td>${item.sku_code}</td>
				        <td>${item.sku_name}</td>
				        <td>${item.barcode}</td>
				        <td>${item.unit_name}</td>
			        </tr>
	      		</c:when>
	      		<c:otherwise>
	      			该成品已在物料清单(BOM)表中
	      		</c:otherwise>
	      	 </c:choose>
		  </c:forEach>
      </tbody>
    </table>
  </div>
  <div class="pager" id="page">${searchPageUtil.page}</div>
</div>