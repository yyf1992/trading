<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../common/path.jsp"%>
<script type="text/javascript">
    $(function () {
        
    });
    //同意
    function agree(id) {
        setStatus('agree',id);
    }
    //忽略
    function ignore(id) {
        setStatus('ignore',id);
    }
    //删除
    function del(id) {
        if(setStatus('del',id)){
            loadData();
            var count= $('.pager').find('span').html();
            $("#count1").html(count);
        }

    }
    function setStatus(status,id) {
        var result = false;
        $.ajax({
            type:"POST",
            url : "interwork/applyFriend/setInviteFriendStatus",
            data:{
                "id":id,
                "status":status
            },
            async:false,
            success:function(data){
                var result = eval('(' + data + ')');
                var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
                if(resultObj.success){
                    layer.msg("操作成功！",{icon:1});
                    loadData();
                    var count= $('.pager').find('span').html();
                    $("#count1").html(count);
                    result = true;
                }else{
                    layer.msg(resultObj.msg,{icon:2});
                }
            },
            error:function(){
                layer.msg("获取数据失败，请稍后重试！",{icon:2});
            }
        });
        return result;
    }
</script>
<div class="layui-tab-item layui-show">
        <div class="verify_list">
            <c:forEach var="item" items="${searchPageUtil.page.list}">
                <div>
                    <b onclick="del('${item.id}');"></b>
                    <img src="${staticsPath}/platform/images/verify_photo.jpg">
                    <div>
                        <p class="verify_account">
                            <span>${item.inviterPerson}&nbsp;${item.inviterPhone}</span>
                            <b><fmt:formatDate value="${item.createDate}" ></fmt:formatDate></b>
                        </p>
                        <p class="verify_supplier"><i></i>${item.inviterName}</p>
                        <p class="additional">
                            <b>附加消息：</b>${item.note}
                            <button class="btn_agree" onclick="agree('${item.id}');">同意</button>
                            <button class="btn_ignore" onclick="ignore('${item.id}');">忽略</button>
                                <%--<span class="verify_agree">已同意</span>--%>
                                <%--<span class="verify_refuse">已拒绝</span>--%>
                        </p>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="pager">${searchPageUtil.page }</div>
</div>