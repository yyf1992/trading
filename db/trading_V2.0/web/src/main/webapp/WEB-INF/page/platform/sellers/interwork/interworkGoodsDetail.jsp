<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style media="print">
  @page {
    size: auto;  /* auto is the initial value */
    margin: 0mm; /* this affects the margin in the printer settings */
  }
</style>
<script>
    $(function(){
        if(${orderBean.status}-6<0){
            $(".ystep1").loadStep({
                size: "large",
                color: "blue",
                steps: [{
                    title: "待接单"
                },{
                    title: "待发货"
                },{
                    title: "发货中"
                },{
                    title: "发货完成"
                },{
                    title: "已收货"
                }]
            });
            $(".ystep1").setStep(parseInt(${orderBean.status}+1));
        }
    });
    function printDetail() {
        var oldstr = document.body.innerHTML;
        var printData = document.getElementById("orderDiv").innerHTML; //获得 div 里的所有 html 数据
        window.document.body.innerHTML = printData;   //把 html 里的数据 复制给 body 的 html 数据 ，相当于重置了 整个页面的 内容
        window.print(); // 开始打印
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="content">
  <div class="order_top mt">
    <div class="lf order_status" style="width: 30%;">
      <h4 class="mt">当前订单状态</h4>
      <p class="mt text-center">
        <span class="order_state">
          <c:choose>
            <c:when test="${orderBean.status==0}">待接单</c:when>
            <c:when test="${orderBean.status==1}">待发货</c:when>
            <c:when test="${orderBean.status==2}">发货中</c:when>
            <c:when test="${orderBean.status==3}">发货完成</c:when>
            <c:when test="${orderBean.status==4}">已收货</c:when>
            <c:when test="${orderBean.status==5}">交易完成</c:when>
            <%--<c:when test="${orderBean.status==6}">已取消订单</c:when>--%>
            <c:when test="${orderBean.status==7}">已驳回订单</c:when>
            <c:when test="${orderBean.status==8}">已终止订单</c:when>
            <c:otherwise>
              <%--内部审批--%>
            </c:otherwise>
          </c:choose>
        </span>
      </p>
      <p class="order_remarks text-center">${orderBean.stopReason}</p>
    </div>
    <div class="lf order_progress">
      <div class="ystep1"></div>
    </div>
  </div>
  <div id="orderDiv">
    <%--<h4 class="mt">订单信息</h4>--%>
    <div class="order_d">
      <p><span class="order_title">采购商信息</span></p>
      <table class="table_info">
        <tr>
          <td>采购商名称：<span>${buyerBean.companyName}</span></td>
          <td>负责人：<span>${orderBean.personName}</span></td>
          <td>手机号：<span>${orderBean.receiptPhone}</span></td>
        </tr>
        <tr>
          <td colspan="3">收货地址：${orderBean.provinceName}${orderBean.cityName}${orderBean.areaName}${orderBean.addrName}</td>
        </tr>
        <%--<tr>--%>
          <%--<td>运送方式：快递</td>--%>
        <%--</tr>--%>
      </table>
      <p class="line mt"></p>
      <%-- <p><span class="order_title">供应商信息</span></p>
      <table class="table_info">
        <tr>
          <td>供应商名称：<span>${orderBean.suppName}</span></td>
          <td>负责人：<span>${orderBean.person}</span></td>
          <td>手机号：<span>${orderBean.phone}</span></td>
        </tr>
      </table>
      <p class="line mt"></p> --%>
      <p><span class="order_title">订单信息</span></p>
      <table class="table_info">
        <tr>
          <td>订单号：<span>${orderBean.orderCode}</span></td>
          <td>下单人：<span>${orderBean.createName}</span></td>
          <td>创建时间：<span><fmt:formatDate value="${orderBean.createDate}" ></fmt:formatDate></span></td>
          <%-- <td>订单交期：<span><fmt:formatDate value="${orderBean.predictArred}" type="date"/></span></td> --%>
        </tr>
        <%--<tr>--%>
          <%--<td>收款方式：<span>银行卡转账</span></td>--%>
          <%--<td>收款人：<span>李xxx</span></td>--%>
          <%--<td>经办人：<span> 张xxx</span></td>--%>
        <%--</tr>--%>
      </table>
      <div class="c66 size_sm orderNote">备注：${orderBean.remark}</div>
      <table class="table_pure detailed_list_s mt">
        <thead>
        <tr>
          <td style="width:15%">商品</td>
          <td style="width:10%">货号</td>
          <td style="width:10%">条形码</td>
          <td style="width:10%">规格代码</td>
          <td style="width:10%">规格名称</td>
          <td style="width:5%">单位</td>
          <td style="width:10%">销售单价</td>
          <td style="width:6%">数量</td>
          <td style="width:10%">商品总额</td>
          <td style="width:10%">发货数量</td>
          <td style="width:10%">到货数量</td>
          <td style="width:9%">要求到货日期</td>
          <td style="width:15%">备注</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="product" items="${orderBean.supplierProductList}">
          <tr>
            <td>${product.productName}</td>
            <td>${product.productCode}</td>
            <td>${product.barcode}</td>
            <td>${product.skuCode}</td>
            <td>${product.skuName}</td>
            <td>${product.unitName}</td>
            <td>${product.price}</td>
            <td>${product.orderNum}</td>
            <td>${product.totalMoney}</td>
            <td>${product.deliveredNum}</td>
            <td>${product.arrivalNum}</td>
            <td><fmt:formatDate value="${product.predictArred}" type="date"/></td>
            <td>${product.remark}</td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
    <div class="totalOrder">
      <ul class="submit_order seller">
        <li>
          <label>商品总额:</label>
          <span>¥ ${totalMoney}</span>
        </li>
        <li>
          <label>其他费用:</label>
          <span>¥ ${otherMoney}</span>
        </li>
        <li>
          <label>订单总金额:</label>
          <span class="orange">¥ ${totalMoney+otherMoney}</span>
        </li>
        <%--<li>--%>
          <%--<label>已收款金额:</label>--%>
          <%--<span class="orange">¥ 30000.00</span>--%>
        <%--</li>--%>
      </ul>
    </div>
   <%-- <div>
      <p>收款凭证：</p>
      <div class="voucherImg orderVoucher">
        <span><img src="images/timg%20(4).jpg"></span>
        <span><img src="images/timg%20(4).jpg"></span>
        <span><img src="images/timg%20(4).jpg"></span>
      </div>
      <p class="mp">合同原件：</p>
      <div class="voucherImg orderVoucher">
        <span><img src="images/timg%20(4).jpg"></span>
        <span><img src="images/timg%20(4).jpg"></span>
        <span><img src="images/timg%20(4).jpg"></span>
      </div>
    </div>--%>

  </div>
  <div class="btn_p text-center" id="printDiv">
    <a href="javascript:void(0);" onclick="printDetail();"><span class="preview order_p_s">打印订单</span></a>
    &nbsp;&nbsp;<a href="javascript:void(0);" onclick="leftMenuClick(this,'${returnUrl}?${form}','sellers','${menuId}')"><span class="preview order_p_s">返回</span></a>
  </div>
</div>
