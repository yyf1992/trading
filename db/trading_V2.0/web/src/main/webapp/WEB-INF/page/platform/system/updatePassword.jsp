<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<script type="text/javascript">
// 保存修改
$(function() {
	$(".modify_sure").click(function() {
		var passwordOld = $("#y_password").val()
		,password = $("#password").val()
		,rePassword = $("#rePassword").val();
		if(passwordOld==''){
			layer.msg("请输入原密码！");
			return;
		}
		if(password==''){
			layer.msg("请输入新密码！");
			return;
		}
		if(rePassword==''){
			layer.msg("请输入确认密码！");
			return;
		}
		if(password!=rePassword){
			layer.msg("两次密码不一致！");
			return;
		}
		$.ajax({
			type : "post",
			url  : basePath + "platform/sysUser/saveUpdatePassword",
			data : {
				"sysUserId"   : $("#sysUserId").val(),
				"passwordOld" : passwordOld,
				"password"    : password
			},
			async : false,
			success : function(data) {
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result) ? result : eval('(' + result + ')');
				if (resultObj.success) {
					layer.msg(resultObj.msg, {icon : 1});
					return;
				} else {
					layer.msg(resultObj.msg, {icon : 2});
					return;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {icon : 2});
			}
		});
	});
});
</script>
<div>
	<p class="modify_title">
		<span></span>为了您的账户安全使用，您需要设置登录密码保障服务
	</p>
	<p class="account">
		买卖账户: <span>${sysUser.loginName}</span>
	</p>
	<p class="setup">
		设置<span>登录密码</span>
	</p>
	<form action="" class="fill_in">
		<input type="hidden" id="sysUserId" name="sysUserId" value="${sysUser.id}" />
		<div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 原密码:</label>
				<input type="password" id="y_password" class="form_control" value="">
			</div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 新密码:</label>
				<input type="password" id="password" class="form_control" value="">
			</div>
			<div class="form_group">
				<label class="control_label"><i>*</i> 确认密码:</label>
				<input type="password" id="rePassword" class="form_control" value="">
			</div>
			<input type="button" class="modify_sure" value="确定修改" />
		</div>
	</form>
	<br/><br/>
</div>