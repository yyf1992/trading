<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>买卖系统用户操作指南</title>
    <%@ include file="common/common.jsp"%>
    <link rel="stylesheet" href="${basePath}/statics/platform/css/operationGuide.css">
</head>
<body>
<div style="width: 1070px;margin: 20px auto;">
    <div id="sideMenu" class="docs-left">
        <h3 class="menu-title">用户操作指南</h3>
        <ul class="menu-1">
            <li><a href="#a_jump"><strong>一、首页</strong></a></li>
            <li><a href="#a_jump1">&emsp;1、首页</a></li>
            <li><a href="#b_jump"><strong>二、我是买家</strong></a></li>
            <li><a href="#b_jump1">&emsp;1、我的采购计划</a></li>
            <li><a href="#b_jump1_1">&emsp;&emsp;（1）创建采购计划</a></li>
            <li><a href="#b_jump1_2">&emsp;&emsp;（2）采购计划列表</a></li>
            <li><a href="#b_jump1_3">&emsp;&emsp;（3）采购下单</a></li>
            <li><a href="#b_jump2">&emsp;2、订单管理</a></li>
            <li><a href="#b_jump2_1">&emsp;&emsp;（1）向供应商采购商品订单</a></li>
            <li><a href="#b_jump2_2">&emsp;&emsp;（2）收货订单管理</a></li>
            <li><a href="#b_jump2_3">&emsp;&emsp;（3）我的换货待发货订单</a></li>
            <li><a href="#b_jump3">&emsp;3、账单管理</a></li>
            <li><a href="#b_jump3_1">&emsp;&emsp;（1）账单结算周期管理</a></li>
            <li><a href="#b_jump3_2">&emsp;&emsp;（2）账单对账列表</a></li>
            <li><a href="#b_jump3_3">&emsp;&emsp;（3）发票列表</a></li>
            <li><a href="#b_jump3_4">&emsp;&emsp;（4）我的付款明细表</a></li>
            <li><a href="#b_jump4">&emsp;4、合同管理</a></li>
            <li><a href="#b_jump4_1">&emsp;&emsp;（1）添加合同</a></li>
            <li><a href="#b_jump4_2">&emsp;&emsp;（2）我的合同</a></li>
            <li><a href="#b_jump5">&emsp;5、供应商管理</a></li>
            <li><a href="#b_jump5_1">&emsp;&emsp;（1）供应商管理</a></li>
            <li><a href="#b_jump6">&emsp;6、售后服务管理</a></li>
            <li><a href="#b_jump6_1">&emsp;&emsp;（1）创建售后单</a></li>
            <li><a href="#b_jump6_2">&emsp;&emsp;（2）互通售后服务管理</a></li>
            <li><a href="#b_jump7">&emsp;7、内部结算管理</a></li>
            <li><a href="#b_jump7_1">&emsp;&emsp;（1）产品中心结算</a></li>
            <li><a href="#b_jump7_1_1">&emsp;&emsp;&emsp;&emsp;①商品出货价格设置</a></li>
            <li><a href="#b_jump7_1_2">&emsp;&emsp;&emsp;&emsp;②发货取数店铺管理</a></li>
            <li><a href="#b_jump7_1_3">&emsp;&emsp;&emsp;&emsp;③店铺发货数据统计</a></li>
            <li><a href="#b_jump7_1_4">&emsp;&emsp;&emsp;&emsp;④店铺销售数据统计</a></li>
            <li><a href="#b_jump7_2">&emsp;&emsp;（2）采购部结算</a></li>
            <li><a href="#b_jump7_2_1">&emsp;&emsp;&emsp;&emsp;①产品采购价格设置</a></li>
            <li><a href="#b_jump7_2_2">&emsp;&emsp;&emsp;&emsp;②采购数据统计</a></li>
            <li><a href="#c_jump"><strong>三、我是卖家</strong></a></li>
            <li><a href="#c_jump1">&emsp;1、订单管理</a></li>
            <li><a href="#c_jump1_1">&emsp;&emsp;（1）互通客户购买的商品</a></li>
            <li><a href="#c_jump1_2">&emsp;&emsp;（2）发货订单管理</a></li>
            <li><a href="#c_jump1_3">&emsp;&emsp;（3）客户换货待发货订单</a></li>
            <li><a href="#c_jump2">&emsp;2、合同管理</a></li>
            <li><a href="#c_jump2_1">&emsp;&emsp;（1）我的合同</a></li>
            <li><a href="#c_jump3">&emsp;3、账单管理</a></li>
            <li><a href="#c_jump3_1">&emsp;&emsp;（1）账单结算周期审核列表</a></li>
            <li><a href="#c_jump3_2">&emsp;&emsp;（2）账单对账列表</a></li>
            <li><a href="#c_jump3_3">&emsp;&emsp;（3）发票管理列表</a></li>
            <li><a href="#c_jump3_4">&emsp;&emsp;（4）我的收款明细表</a></li>
            <li><a href="#c_jump3_5">&emsp;&emsp;（5）账单结算周期修改审批</a></li>
            <li><a href="#c_jump4">&emsp;4、售后服务管理</a></li>
            <li><a href="#c_jump4_1">&emsp;&emsp;（1）互通客户售后服务</a></li>
            <li><a href="#d_jump"><strong>四、基础资料</strong></a></li>
            <li><a href="#d_jump1">&emsp;1、商品管理</a></li>
            <li><a href="#d_jump1_1">&emsp;&emsp;（1）我的所有商品</a></li>
            <li><a href="#d_jump1_2">&emsp;&emsp;（2）与供应商互通的商品</a></li>
            <li><a href="#d_jump1_3">&emsp;&emsp;（3）与客户互通的商品</a></li>
            <li><a href="#d_jump2">&emsp;2、库存管理</a></li>
            <li><a href="#d_jump2_1">&emsp;&emsp;（1）商品库存统计</a></li>
            <li><a href="#d_jump2_2">&emsp;&emsp;（2）商品出入明细</a></li>
            <li><a href="#d_jump2_3">&emsp;&emsp;（3）商品汇总表</a></li>
            <li><a href="#d_jump2_4">&emsp;&emsp;（4）商品库存历史表</a></li>
            <li><a href="#d_jump2_5">&emsp;&emsp;（5）商品积压预警表</a></li>
            <li><a href="#d_jump2_6">&emsp;&emsp;（6）仓库管理</a></li>
            <li><a href="#d_jump3">&emsp;3、审批管理</a></li>
            <li><a href="#d_jump3_1">&emsp;&emsp;（1）待我审批</a></li>
            <li><a href="#d_jump3_2">&emsp;&emsp;（2）我已审批</a></li>
            <li><a href="#d_jump3_3">&emsp;&emsp;（3）我发起的</a></li>
            <li><a href="#d_jump3_4">&emsp;&emsp;（4）抄送我的</a></li>
            <li><a href="#d_jump4">&emsp;4、原材料商品管理</a></li>
            <li><a href="#d_jump4_1">&emsp;&emsp;（1）商品物料配置管理</a></li>
            <li><a href="#d_jump4_2">&emsp;&emsp;（2）物料清单（BOM）表</a></li>
            <li><a href="#e_jump"><strong>五、系统设置</strong></a></li>
            <li><a href="#e_jump1">&emsp;1、个人资料</a></li>
            <li><a href="#e_jump2">&emsp;2、修改密码</a></li>
            <li><a href="#e_jump3">&emsp;3、消息列表</a></li>
            <li><a href="#e_jump4">&emsp;4、收货地址管理</a></li>
            <li><a href="#e_jump5">&emsp;5、人员管理</a></li>
            <li><a href="#e_jump6">&emsp;6、平台管理</a></li>
            <li><a href="#e_jump7">&emsp;7、店铺管理</a></li>
            <li><a href="#e_jump8">&emsp;8、内部审批管理</a></li>
            <li><a href="#e_jump9">&emsp;9、银行账户管理</a></li>
            <li><a href="#e_jump10">&emsp;10、角色管理</a></li>
        </ul>
    </div>
    <div id="content" style="float: left;" class="content">
        <p id="top"></p>
        <p id="a_jump">
            <strong>一、首页</strong><br />
            <p id="a_jump1">
                &emsp;1、首页<br />
                <img src="${basePath}/statics/platform/guideImg/a_jump1.png" height="335" width="783"/>
                &emsp;首页主要显示用户的基本信息、未审批的事项、以及密码的修改。
            </p>
        </p>
        <p id="b_jump">
            <strong>二、我是买家</strong><br />
            <p id="b_jump1">
                &emsp;1、我的采购计划<br />
                <p id="b_jump1_1">
                    &emsp;&emsp;（1）创建采购计划<br />
                    <img src="${basePath}/statics/platform/guideImg/b_jump1_1.png" height="590" width="783"/>
                    &emsp;&emsp;&nbsp;创建采购计划模块主要是用于店铺向采购部提交采购计划。
                </p>
                <p id="b_jump1_2">
                    &emsp;&emsp;（2）采购计划列表<br />
                    <img src="${basePath}/statics/platform/guideImg/b_jump1_2.png" height="443" width="783"/>&emsp;&emsp;&nbsp;采购计划列表模块用于显示已经创建了的采购计划以及对采购计划查看、审批、详情等操作。
                </p>
                <p id="b_jump1_3">
                    &emsp;&emsp;（3）采购下单<br /><img src="${basePath}/statics/platform/guideImg/b_jump1_3.png" height="236" width="783"/>
                    &emsp;&emsp;&nbsp;采购下单模块用于展示已经审核通过的采购计划，在此界面可以对已经审核通过的采购计划进行生成采购订单操作。
                </p>
            </p>
            <p id="b_jump2">
                &emsp;2、订单管理<br />
                <p id="b_jump2_1">
                    &emsp;&emsp;（1）向供应商采购商品订单<br /><img src="${basePath}/statics/platform/guideImg/b_jump2_1.png" height="360" width="783"/>
                    &emsp;&emsp;&nbsp;向供应商采购的商品订单将会在此模块显示，在此界面还会显示采购单的交易状态、审核状态以及订单的详细信息，可以通过商品的名称、货号、条形码、订单号码等信息进行搜索。
                </p>
                <p id="b_jump2_2">
                    &emsp;&emsp;（2）收货订单管理<br /><img src="${basePath}/statics/platform/guideImg/b_jump2_2.png" height="513" width="783"/>
                    &emsp;&emsp;&nbsp;收货订单管理模块主要显示卖家已经发货的订单，在此模块可以对订单进行确认收货、上传收货单、查看收货明细等操作。
                </p>
                <p id="b_jump2_3">
                    &emsp;&emsp;（3）我的换货待发货订单<br /><img src="${basePath}/statics/platform/guideImg/b_jump2_3.png" height="592" width="783"/>
                    &emsp;&emsp;&nbsp;我的换货待发货订单模块主要展示换货未发货的订单信息，可以通过商品的名称、货号、条形码或者订单号进行搜索。
                </p>
            </p>
            <p id="b_jump3">
                &emsp;3、账单管理<br />
                <p id="b_jump3_1">
                    &emsp;&emsp;（1）账单结算周期管理<br /><img src="${basePath}/statics/platform/guideImg/b_jump3_1.png" height="596" width="783"/>
                    &emsp;&emsp;&nbsp;账单结算周期管理模块用于设置账单的出账日期和利息（设置之后需要双方审核通过之后在下一次出账时被启用）。还可以对已经设置出账日期的账单进行修改操作，支持通过合作方的信息、创建日期、创建人等信息进行查询。
                </p>
                <p id="b_jump3_2">
                    &emsp;&emsp;（2）账单对账列表<br /><img src="${basePath}/statics/platform/guideImg/b_jump3_2.png" height="246" width="783"/>
                    &emsp;&emsp;&nbsp;账单对账列表模块用于发起对账、查看账单详情、打印账单、查看数据等操作。支持通过供应商名称出账日期账期总金额、账期状态等信息进行查询。
                </p>
                <p id="b_jump3_3">
                    &emsp;&emsp;（3）发票列表<br /><img src="${basePath}/statics/platform/guideImg/b_jump3_3.png" height="364" width="783"/>
                    &emsp;&emsp;&nbsp;发票列表模块用于展示本公司所有的发票信息，在此界面可以查看票据信息、商品的明细等信息。支持通过结算单号、供应商名称、票据单号、商品总金额、票据状态、开票日期等信息进行查询。
                </p>
                <p id="b_jump3_4">
                    &emsp;&emsp;（4）我的付款明细表<br /><img src="${basePath}/statics/platform/guideImg/b_jump3_4.png" height="251" width="783"/>
                    &emsp;&emsp;&nbsp;我的付款明细表展示的是所有的付款信息，在此模块可以进行付款、查看付款明细、查看已付款详情等操作。可以根据供应商名称、应付款金额、账单日期、付款确认状态等信息进行查询。
                </p>
            </p>
            <p id="b_jump4">
                &emsp;4、合同管理<br />
                <p id="b_jump4_1">
                    &emsp;&emsp;（1）添加合同<br /><img src="${basePath}/statics/platform/guideImg/b_jump4_1.png" height="435" width="783"/>
                    &emsp;&emsp;&nbsp;添加合同模块的主要功能是与供应商签订合同，如果要签订合同的供应商不在公司的供应商列表中，可以先添加供应商再签订合同或保存合同草稿，合同提交保存之后会走审批流程，待双方审批通过之后该合同才会生效。
                </p>
                <p id="b_jump4_2">
                    &emsp;&emsp;（2）我的合同<br /><img src="${basePath}/statics/platform/guideImg/b_jump4_2.png" height="543" width="783"/>
                    &emsp;&emsp;&nbsp;成功添加的合同或保存的合同草稿会在此模块中显示，合同草稿可在此界面修改合同内容或提交审批，此外也可以在此界面创建新的合同。
                </p>
            </p>
            <p id="b_jump5">
                &emsp;5、供应商管理<br />
                <p id="b_jump5_1">
                    &emsp;&emsp;（1）供应商管理<br /><img src="${basePath}/statics/platform/guideImg/b_jump5_1.png" height="384" width="783"/>
                    &emsp;&emsp;&nbsp;供应商管理模块用于显示与公司互通的供应商信息，可以在此界面查看、修改、新增或删除互通的供应商信息等操作。
                </p>
            </p>
            <p id="b_jump6">
                &emsp;6、售后服务管理<br />
                <p id="b_jump6_1">
                    &emsp;&emsp;（1）创建售后单<br /><img src="${basePath}/statics/platform/guideImg/b_jump6_1.png" height="585" width="783"/>
                    &emsp;&emsp;&nbsp;如有退货或者换货需求，可以在此模块创建新的换货或者退货单。
                </p>
                <p id="b_jump6_2">
                    &emsp;&emsp;（2）互通售后服务管理<br /><img src="${basePath}/statics/platform/guideImg/b_jump6_2.png" height="423" width="783"/>
                    &emsp;&emsp;&nbsp;创建完成的售后单将会在此界面展示。
                </p>
            </p>
            <p id="b_jump7">
                &emsp;7、内部结算管理<br />
                <p id="b_jump7_1">
                    &emsp;&emsp;（1）产品中心结算<br />
                    <p id="b_jump7_1_1">
                        &emsp;&emsp;&emsp;&emsp;①商品出货价格设置<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_1_1.png" height="305" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;商品出货价格设置模块用于给店铺里的商品设置销售单价。
                    </p>
                    <p id="b_jump7_1_2">
                        &emsp;&emsp;&emsp;&emsp;②发货取数店铺管理<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_1_2.png" height="593" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;发货取数店铺管理用于管理取发货数据的店铺，在此界面新增或删除取发货数据的店铺。
                    </p>
                    <p id="b_jump7_1_3">
                        &emsp;&emsp;&emsp;&emsp;③店铺发货数据统计<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_1_3.png" height="376" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;此模块用于显示已经发货的商品，可以根据店铺、货号、条形码、起始时间等条件查询，此模块的数据可以导出为excel表格。
                    </p>
                    <p id="b_jump7_1_4">
                        &emsp;&emsp;&emsp;&emsp;④店铺销售数据统计<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_1_4.png" height="352" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;此模块用于显示已经销售的商品，可以根据店铺、货号、条形码、起始时间等条件查询，此模块的数据可以导出为excel表格。
                    </p>
                </p>
                <p id="b_jump7_2">
                    &emsp;&emsp;（2）采购部结算<br />
                    <p id="b_jump7_2_1">
                        &emsp;&emsp;&emsp;&emsp;①产品采购价格设置<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_2_1.png" height="462" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;产品采购价格设置模块用于设置商品的采购价格。
                    </p>
                    <p id="b_jump7_2_2">
                        &emsp;&emsp;&emsp;&emsp;②采购数据统计<br /><img src="${basePath}/statics/platform/guideImg/b_jump7_2_2.png" height="291" width="783"/>
                        &emsp;&emsp;&emsp;&emsp;采购数据统计模块用于展示采购数据报表，此模块的数据可以导出为excel表格。
                    </p>
                </p>
            </p>
        </p>
        <p id="c_jump">
            <strong>三、我是卖家</strong><br />
            <p id="c_jump1">
                &emsp;1、订单管理<br />
                <p id="c_jump1_1">
                    &emsp;&emsp;（1）互通客户购买的商品<br /><img src="${basePath}/statics/platform/guideImg/c_jump1_1.png" height="216" width="783"/>
                    &emsp;&emsp;互通客户购买的商品模块用于显示互通客户的下单信息，在此界面可以进行发货操作。可以根据客户的名称、交易状态、订单时间、订单号等信息进行查询。
                </p>
                <p id="c_jump1_2">
                    &emsp;&emsp;（2）发货订单管理<br /><img src="${basePath}/statics/platform/guideImg/c_jump1_2.png" height="221" width="783"/>
                    &emsp;&emsp;发货订单管理模块展示了已经发货的订单信息，在此模块可以查看发货详情、查看收货单以及打印收货回单。可以通过发货单号、客户名称、发货时间交易状态等信息进行查询。
                </p>
                <p id="c_jump1_3">
                    &emsp;&emsp;（3）客户换货待发货订单<br /><img src="${basePath}/statics/platform/guideImg/c_jump1_3.png" height="195" width="783"/>
                    &emsp;&emsp;客户换货代发货订单模块用于显示客户的换货未发货数据，在此界面可以进行发货处理，还可以根据商品名称、客户名称、交易状态发货时间、发货类型等信息进行查询。
                </p>
            <p id="c_jump2">
                &emsp;2、合同管理<br />
                <p id="c_jump2_1">
                    &emsp;&emsp;（1）我的合同<br /><img src="${basePath}/statics/platform/guideImg/c_jump2_1.png" height="219" width="783"/>
                    &emsp;&emsp;该模块用于显示买家发起的合同信息，可以通过发起人、客户名称、合同编号、合同名称、创建日期以及合同状态进行查询。
                </p>
            </p>
            <p id="c_jump3">
                &emsp;3、账单管理<br />
                <p id="c_jump3_1">
                    &emsp;&emsp;（1）账单结算周期审核列表<br /><img src="${basePath}/statics/platform/guideImg/c_jump3_1.png" height="419" width="783"/>
                    &emsp;&emsp;账单结算周期审核列表模块用于显示账单核算列表，可以对买家已发起的账单结算周期进行审核或者驳回等操作，通过输入合作方的名称、创建人、创建日期以及状态对账单结算周期列表进行查询。
                </p>
                <p id="c_jump3_2">
                    &emsp;&emsp;（2）账单对账列表<br /><img src="${basePath}/statics/platform/guideImg/c_jump3_2.png" height="181" width="783"/>
                </p>
                <p id="c_jump3_3">
                    &emsp;&emsp;（3）发票管理列表<br /><img src="${basePath}/statics/platform/guideImg/c_jump3_3.png" height="262" width="783"/>
                </p>
                <p id="c_jump3_4">
                    &emsp;&emsp;（4）我的收款明细表<br /><img src="${basePath}/statics/platform/guideImg/c_jump3_4.png" height="247" width="783"/>
                </p>
                <p id="c_jump3_5">
                    &emsp;&emsp;（5）账单结算周期修改审批<br /><img src="${basePath}/statics/platform/guideImg/c_jump3_5.png" height="325" width="783"/>
                </p>
            </p>
            <p id="c_jump4">
                &emsp;4、售后服务管理<br />
                <p id="c_jump4_1">
                    &emsp;&emsp;（1）互通客户售后服务<br /><img src="${basePath}/statics/platform/guideImg/c_jump4_1.png" height="199" width="783"/>
                </p>
            </p>
        </p>
        <p id="d_jump">
            <strong>四、基础资料</strong><br />
            <p id="d_jump1">
                &emsp;1、商品管理<br />
                <p id="d_jump1_1">
                    &emsp;&emsp;（1）我的所有商品<br /><img src="${basePath}/statics/platform/guideImg/d_jump1_1.png" height="343" width="783"/>
                    &emsp;&emsp;我的所有商品模块可以新增商品或原材料操作，也可以对商品和SKU进行修改和删除操作。
                </p>
                <p id="d_jump1_2">
                    &emsp;&emsp;（2）与供应商互通的商品<br /><img src="${basePath}/statics/platform/guideImg/d_jump1_2.png" height="339" width="783"/>
                    &emsp;&emsp;与供应商互通的商品属于买家使用的模块，在此模块可以新增与供应商互通的商品，设置互通商品的采购价，提交要保存的互通商品之后，就会进入审批流程，待双方审批均通过之后，该商品的采购价格即可使用。
                </p>
                <p id="d_jump1_3">
                    &emsp;&emsp;（3）与客户互通的商品<br /><img src="${basePath}/statics/platform/guideImg/d_jump1_3.png" height="188" width="783"/>
                    &emsp;&emsp;与客户互通的商品模块属于卖家使用的模块，在此模块可以看到卖家发起的互通商品信息。
                </p>
            </p>
            <p id="d_jump2">
                &emsp;2、库存管理<br />
                <p id="d_jump2_1">
                    &emsp;&emsp;（1）商品库存统计<br /><img src="${basePath}/statics/platform/guideImg/d_jump2_1.png" height="352" width="783"/>
                    &emsp;&emsp;商品库存统计模块用于显示商品的库存信息，可以通过货号、商品名称、条形码等信息进行库存的查询。

                </p>
                <p id="d_jump2_2">
                    &emsp;&emsp;（2）商品出入明细<br /><img src="${basePath}/statics/platform/guideImg/d_jump2_2.png" height="336" width="783"/>
                    &emsp;&emsp;商品出入明细模块用于展示商品的出入库信息，可以通过商品的关键词、店铺名、变更类型以及操作时间进行查询。
                </p>
                <p id="d_jump2_3"><img src="${basePath}/statics/platform/guideImg/d_jump2_3.png" height="292" width="783"/
                    &emsp;&emsp;（3）商品汇总表<br />
                    &emsp;&emsp;在此模块可以显示所有仓库的商品信息，可以根据商品的时间、商品名称、商品货号、条形码、仓库等信息进行查询。
                </p>
                <p id="d_jump2_4">
                    &emsp;&emsp;（4）商品库存历史表<br /><img src="${basePath}/statics/platform/guideImg/d_jump2_4.png" height="268" width="783"/>
                    &emsp;&emsp;商品库存历史表展示的事商品的历史库存记录信息，可以根据商品的入库时间、商品名称、商品货号、条形码、仓库等信息进行查询。
                </p>
                <p id="d_jump2_5">
                    &emsp;&emsp;（5）商品积压预警表<br /><img src="${basePath}/statics/platform/guideImg/d_jump2_5.png" height="234" width="783"/>
                    &emsp;&emsp;商品积压预警表用于显示商品的积压明细，可以通过商品名称、货号、条形码、积压天数等条件进行查询。
                </p>
                <p id="d_jump2_6">
                    &emsp;&emsp;（6）仓库管理<br /><img src="${basePath}/statics/platform/guideImg/d_jump2_6.png" height="374" width="783"/>
                    &emsp;&emsp;此模块用于展示公司的仓库信息。
                </p>
            </p>
            <p id="d_jump3">
                &emsp;3、审批管理<br />
                <p id="d_jump3_1">
                    &emsp;&emsp;（1）待我审批<br /><img src="${basePath}/statics/platform/guideImg/d_jump3_1.png" height="316" width="783"/>
                    &emsp;&emsp;待我审批模块用于显示等待我审批的信息，点击一条记录可以对该条审批进行同意、拒绝、转交等操作，可以根据审批类型进行查询。
                </p>
                <p id="d_jump3_2">
                    &emsp;&emsp;（2）我已审批<br /><img src="${basePath}/statics/platform/guideImg/d_jump3_2.png" height="285" width="783"/>
                    &emsp;&emsp;我已审批模块用于显示已经审批了的申请信息，点击某条信息即可查看申请的详细信息，可以根据审批类型进行查询。
                </p>
                <p id="d_jump3_3">
                    &emsp;&emsp;（3）我发起的<br /><img src="${basePath}/statics/platform/guideImg/d_jump3_3.png" height="287" width="783"/>
                    &emsp;&emsp;我发起的审批模块用于显示我发起的审批信息以及审批的进度，可以根据审批类型进行查询。
                </p>
                <p id="d_jump3_4">
                    &emsp;&emsp;（4）抄送我的<br /><img src="${basePath}/statics/platform/guideImg/d_jump3_4.png" height="155" width="783"/>
                    &emsp;&emsp;抄送我的模块用于显示抄送给我的审批信息。
                </p>
            </p>
            <p id="d_jump4">
                &emsp;4、原材料商品管理<br />
                <p id="d_jump4_1">
                    &emsp;&emsp;（1）商品物料配置管理<br /><img src="${basePath}/statics/platform/guideImg/d_jump4_1.png" height="575" width="783"/>
                    &emsp;&emsp;商品物料配置管理模块用于配置商品的物料信息，商品物料信息配置提交之后就会进入审批流程，审批通过之后就会出现在商品物料bom表界面。
                </p>
                <p id="d_jump4_2">
                    &emsp;&emsp;（2）物料清单（BOM）表<br /><img src="${basePath}/statics/platform/guideImg/d_jump4_2.png" height="435" width="783"/>
                    &emsp;&emsp;此界面用于显示审批通过之后的商品物料配置信息，可以根据商品名称信息进行查询，对于已驳回的物料配置审批可以进行修改物料配置操作。
                </p>
            </p>
        </p>
        <p id="e_jump">
            <strong>五、系统设置</strong><br />
            <p id="e_jump1">
                &emsp;1、个人资料<br /><img src="${basePath}/statics/platform/guideImg/e_jump1.png" height="245" width="783"/>
                &emsp;个人资料模块用于显示公司的基本信息以及用户的个人资料，在个人资料界面可以修改用户的个人资料。
            <p id="e_jump2">
                &emsp;2、修改密码<br /><img src="${basePath}/statics/platform/guideImg/e_jump2.png" height="424" width="783"/>
                &emsp;修改密码模块用于修改用户的登录密码。
            </p>
            <p id="e_jump3">
                &emsp;3、消息列表<br /><img src="${basePath}/statics/platform/guideImg/e_jump3.png" height="187" width="783"/>
                &emsp;消息列表模块显示推送的系统信息以及用户的验证信息。
            </p>
            <p id="e_jump4">
                &emsp;4、收货地址管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump4.png" height="310" width="783"/>
                &emsp;收货地址管理管理模块用于新增、删除、修改收货地址信息，还可以设置默认收货地址信息。
            </p>
            <p id="e_jump5">
                &emsp;5、人员管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump5.png" height="309" width="783"/>
                &emsp;此模块用于对公司人员进行增删改查等操作，可以根据人员的职位分配不同的角色。可以通过员工的姓名、登录账户、加入日期、状态等信息进行查询。
            </p>
            <p id="e_jump6">
                &emsp;6、平台管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump6.png" height="241" width="783"/>
                &emsp;平台管理模块用于管理公司的平台信息，可以新增、删除、修改平台信息，根据平台的名称、加入日期、状态等信息进行查询。
            </p>
            <p id="e_jump7">
                &emsp;7、店铺管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump7.png" height="310" width="783"/>
                &emsp;店铺管理模块用于管理公司的店铺信息，可以新增、删除、修改店铺信息，根据店铺名称、加入日期、状态等信息进行查询。
            </p>
            <p id="e_jump8">
                &emsp;8、内部审批管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump8.png" height="297" width="783"/>
                &emsp;内部审批管理模块用于管理公司的审批流程，可以创建新的审批事项、修改审批流程设置以及启用或者禁用审批事项，通过输入审批事项的标题可以查询该审批事项的具体信息。
            </p>
            <p id="e_jump9">
                &emsp;9、银行账户管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump9.png" height="274" width="783"/>
                &emsp;银行账户管理模块主要用于管理公司的付款账户，包括新建账户、修改账户、启用或禁用账户等功能，可以根据账户的户名、加入日期、状态等信息对账户进行查询。
            </p>
            <p id="e_jump10">
                &emsp;10、角色管理<br /><img src="${basePath}/statics/platform/guideImg/e_jump10.png" height="212" width="783"/>
                &emsp;角色管理模块用于对公司的角色进行管理操作，可以设置新的角色，为不同的角色设置不同的权限，对角色进行启用、禁用或者删除等操作，可以通过角色代码、角色名称等信息对角色进行查找。
            </p>
        </p>
    </div>
</div>
</body>
</html>