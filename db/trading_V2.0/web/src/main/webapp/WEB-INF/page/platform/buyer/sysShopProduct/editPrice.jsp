<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="el" uri="/elfun" %>
<form id="editPriceForm">
    <div class="account_group">
      <span>货号:</span>
     	<input type="text" name="productCode" value="${SysShopProduct.productCode}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>条形码:</span>
      <input type="text" name="barcode" value="${SysShopProduct.barcode}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>所属店铺:</span>
      <input type="text" name="shopName" value="${SysShopProduct.shopName}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>成本价:</span>
      <input type="text" name="costPrice" value="${SysShopProduct.costPrice}" readonly="readonly">
    </div>
    <div class="account_group">
      <span>出货价:</span>
      <input type="text" name="sellPrice" value="${SysShopProduct.sellPrice}">
    </div>
    <input type="hidden" name="id" value="${SysShopProduct.id}">
</form>