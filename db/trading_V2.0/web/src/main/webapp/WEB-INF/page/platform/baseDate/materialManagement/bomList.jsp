 <%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>   
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../../common/path.jsp"%>
<script src="<%=basePath%>statics/platform/js/commodity.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/commodity_all.css"></link>
<link rel="stylesheet" type="text/css" href="<%= basePath %>statics/platform/css/common.css"></link>
<script type="text/javascript">
 //标签页改变
function setStatus(obj, status) {
	$("#tabId").attr("value",status);
	$('.tab a').removeClass("hover");
	$(obj).addClass("hover");
	loadPlatformData();
}
//重置
function recovery(){
	leftMenuClick(this,'platform/baseDate/buyproductskubom/loadBomListHtml',"baseInfo");
}
//删除物料配置
function deleteProductSkuBom(id){
	layer.confirm('确定删除物料清单吗？', {
	      icon:3,
	      title:'提醒',
	      closeBtn:2,
	      btn: ['确定','取消']
	    }, function(){
	    	$.ajax({
				type : "post",
				url : "platform/baseDate/buyproductskubom/saveDeleteSKUBom",
				data : {
					"id" : id
				},
				async : false,
				success : function(data) {
					var result = eval('(' + data + ')');
					var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
					if (resultObj.success) {
						layer.msg(resultObj.msg,{icon:1});
						leftMenuClick(this,'platform/baseDate/buyproductskubom/loadBomListHtml',"baseInfo");
					} else {
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error : function() {
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    });
}
</script>
<div>
  <!--搜索栏-->
  <form class="layui-form" action="platform/baseDate/buyproductskubom/loadBomListHtml">
  	<input type="hidden" name="tabId" value="${params11.tabId}" id="tabId">
  		<ul class="order_search">
    		<li class="range">
      			<label>商品名称：</label>
      			<input type="text" placeholder="输入商品名称进行搜索" id="productName" name="productName" value="${searchPageUtil.object.productName}" style="width:260px;border-radius:4px;">
    		</li>
			<li class="nomargin">
				<button class="search" onclick="loadPlatformData();">搜索</button>
				<button type="button" class="search" onclick="recovery();">重置</button>
			</li>
  		</ul>
  		<!-- 分页隐藏数据 -->
		<input id="pageNo" name="page.pageNo" type="hidden" value="${searchPageUtil.page.pageNo}" />
		<input id="pageSize" name="page.pageSize" type="hidden" value="${searchPageUtil.page.pageSize}" />
  </form>
  <!--列表区-->
  <table class="orderTop bomTop">
    <tr>
      <td style="width:80%">
        <ul>
          <li style="width:45%">商品</li>
          <li style="width:14%">条形码</li>
          <li style="width:30%">单位</li>
          <li style="width:7%">配置数量</li>
        </ul>
      </td>
      <td>备注</td>
      <td>操作</td>
    </tr>
  </table>
  <div class="orderList bomList ">
  	<c:forEach var="BuyProductSkuBom" items="${searchPageUtil.page.list}">
		<div>
	       <p>
	   	 	 <c:if test="${BuyProductSkuBom.updateDate != null}">
	   	 		<span class="layui-col-sm2" style="width:70%">
	   	 			<b><fmt:formatDate type="date" value="${BuyProductSkuBom.updateDate}" /></b>&emsp;&emsp;
	   	 			BOM编号:<b>${BuyProductSkuBom.bomCode}</b>&emsp;&emsp;&emsp;&emsp;
	   	 			${BuyProductSkuBom.productCode}|${BuyProductSkuBom.productName}|${BuyProductSkuBom.skuName}|${BuyProductSkuBom.barcode}
	   	 		</span>
	   	 	 </c:if>
	   	 	 <c:if test="${BuyProductSkuBom.updateDate == null}">
	   	 		<span class="layui-col-sm2" style="width:70%">
	   	 			<b><fmt:formatDate type="date" value="${BuyProductSkuBom.createDate}" /></b>&emsp;&emsp;
	   	 			BOM编号:<b>${BuyProductSkuBom.bomCode}</b>&emsp;&emsp;&emsp;&emsp;
	   	 			${BuyProductSkuBom.productCode}|${BuyProductSkuBom.productName}|${BuyProductSkuBom.skuName}|${BuyProductSkuBom.barcode}
	   	 		</span>
	   	 	 </c:if>
	         <span>创建人:${BuyProductSkuBom.createName}</span>
	       </p>
	       <table>
	         <tr>
	           <td width="80%">
					<c:forEach var="product" items="${BuyProductSkuBom.skuComposeList}">
						<ul class="clear">
							<li style="width:40%">
								<span class="defaultImg"></span>
								<div>
									${product.productCode}|${product.productName} <br/>
									<span>规格代码:${product.skuCode}</span><br/>
									<span>规格名称:${product.skuName}</span>
								</div>
							</li>
							<li style="width:25%">${product.skuBarcode}</li>
							<li style="width:20%">${product.unitName}</li>
							<li style="width:15%">${product.composeNum}</li>
						</ul>
					</c:forEach>
	           </td>
	           <td width="10%">${BuyProductSkuBom.remark}</td>
	           <td width="10%">
	            <a href="javascript:void(0)" onclick="leftMenuClick(this,'platform/baseDate/buyproductskubom/changeCompose?productId=${BuyProductSkuBom.productId}','baseinfo')"class="layui-btn layui-btn-mini">修改配置</a>
				<a onclick="deleteProductSkuBom('${BuyProductSkuBom.id }');" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>
	           </td>
	         </tr>
	       </table>
	     </div> 
	</c:forEach>
    <!--分页-->
    <div class="pager">${searchPageUtil.page}</div>
  </div>
</div>