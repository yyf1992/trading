<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<c:set var="basePath" value="<%=basePath %>" />
<c:set var="staticsPath" value="${basePath}/statics" />
<link rel="stylesheet" href="${staticsPath}/platform/css/setPrice.css">
<script src="${staticsPath}/platform/js/setPrice.js"></script>

<script type="text/javascript">
//  增加一行
$('.rowNew').click(function(){
var id = (new Date()).valueOf();
  var tr='<tr>'
	    +'<td>'
	    +'<div class="searchList">'
	    +'<input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);">'
	    +'</div>'
	    +'</td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="点击选择	" onclick="chooseGoods(this);"><input type="hidden" class="unitId"/></td>'
	    +'<td>'
	    +'<select name="priceType">'
	    +'<option value="0">加价</option>'
	    +'<option value="1">提成</option>'
	    +'</select>'
	    +'</td>'
	    +'<td><input class="goodsIpt" type="text" placeholder="金额/百分比(%)"  name="price"  onkeyup="checkPrice(this);"></td>'
	    +'<td><button class="commodityDel">删除商品</button></td>'
	    +'<input type="hidden" name="productId">'
	    +'</tr>';
    var trObj = $(tr).attr("id",id);
    var tby=$('.commodityNew tbody');
    tby.append(trObj);
});

//  删除一行
$('.commodityNew').on('click','.commodityDel',function(){
  $(this).parent().parent().remove();
});


function chooseGoods(obj){
	var id=$(obj).parent().parent().parent().attr("id");
	var barcodeStr="";
	$(".commodityNew>tbody>tr").each(function(i){
		var barcode=$(this).find("td:nth-child(3) input").val();
		if(barcode!=null && barcode.length>0){
			barcodeStr+=barcode+",";
		}
	});
	$.ajax({
		url:"platform/buyer/sysshopproduct/loadProductData",
		type:"get",
		data:{
			"page.pageSize":"5",
			"page.divId":"layOpen",
			"productId":id,
			"barcodeStr":barcodeStr
		},
		async:false,
		success:function(data){
			  layer.open({
			      type: 1,
			      skin:'unit_new',
			      title: '商品列表',
			      area: ['1200px','auto'],
			      content:data,
				  });
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！", {icon : 2});
		}
	});
}


function checkPrice(obj){
var price=$(obj).val();
	if(price != null && price.length > 0){
		$(obj).parent().removeClass("error");
	}
}
</script>
<body>
<div id="tableForm">
	<table class="commodityNew">
	  <thead>
	  <tr>
	    <td style="width:15%">商品名称</td>
	    <td style="width:10%">货号</td>
	    <td style="width:10%">条形码</td>
	    <td style="width:10%">规格代码</td>
	    <td style="width:13%">规格名称</td>
	    <td style="width:7%">单位</td>
	    <td style="width:10%">价格设置方式</td>
	    <td style="width:15%">加价金额/提成百分比(%)</td>
	    <td style="width:10%"><button class="rowNew">新增一行</button></td>
	  </tr>
	  </thead>
	  <tbody>
	  <tr id="aaa">
	    <td>
	      <div class="searchList">
	        <input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);">
	      </div>
	    </td>
	    <td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>
	    <td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>
	    <td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>
	    <td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);"></td>
	    <td><input class="goodsIpt" type="text" placeholder="点击选择" onclick="chooseGoods(this);">
	    	<input type="hidden" class="unitId"/>
	    </td>
	    <td>
	        <select name="priceType">
	            <option value="0">加价</option>
	            <option value="1">提成</option>
	        </select>
	    </td>
	    <td><input class="goodsIpt" type="text" placeholder="金额/百分比(%)"  name="price" onkeyup="checkPrice(this);"></td>
	    <td><button disabled="disabled" class="commodityDel">删除商品</button></td>
	     <input type="hidden" name="productId">
	  </tr>
	  </tbody>
	</table>
	<div class="commoditySubmit">
	  <button class="commodityCancel" onclick="leftMenuClick(this,'platform/buyer/sysshopproduct/loadProductPurchasePriceList','buyer','17102410494297195350')">取消</button>
	  <button class="commoditySave" onclick="submitLatitude(this);">保存</button>
	</div>

</div>
</body>
</html>