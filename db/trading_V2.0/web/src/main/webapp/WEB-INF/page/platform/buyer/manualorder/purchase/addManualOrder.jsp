<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {
		//删除表格某一行
		$('.purchase_list').on('click','b',function(){
		  layer.confirm('确定要删除该商品吗？</p>',{
		    icon:3,
		    skin:'pop',
		    title:'提醒',
		    closeBtn:2
		  },function(index){
		    layer.close(index);
		    delRow(this);
		    sumTotalPrice();
		  }.bind(this));
		});
		//功能点5:点击输入框，弹出下拉商品
		/* $('.purchase_list').on('focus', 'td:nth-child(2) input:eq(0)',
				function() {
					$(this).siblings('ul').css('display', 'block');
					$(this).siblings('ul').menu();
				}).on('blur', 'td:nth-child(2) input:eq(0)', function() {
			$(this).siblings('ul').css('display', 'none');
		}); */
		$('.manual+button')
				.click(
						function() {
							var str = '<td><b></b></td>'
									+ '<td align="left">'
									+ '<input type="text" name="productNameText" placeholder="输入商品名称/货号/条形码等关键字" onkeyup="selectManualGoods(this);">'
									+ '<input type="hidden" name="productCode" value=""/><!-- 货号 -->'
									+ '<input type="hidden" name="productName" value=""/><!-- 名称 -->'
									+ '<input type="hidden" name="skuCode" value=""/><!-- 规格代码 -->'
									+ '<input type="hidden" name="skuName" value=""/><!-- 规格名称 -->'
									+ '<input type="hidden" name="barcode" value=""/><!-- 条形码 -->'
									+ '<td>'
									+ $("#addUnitSelect").html()
									+ '</td>'
									+ '<td><input type="number" value="0" min="0" class="unit_price" onchange="sumTotalPrice();" onkeyup="if( ! /^[+]?\\d+\\.\\d{0,4}$/){this.value=\'\';}" /></td>'
									+ '<td><input type="number" value="0" min="0" class="number" onchange="sumNumber();" onkeyup="this.value=this.value.replace(/[^\\- \\d.]/g,\'\')" /></td>'
									+ '<td><input type="number" value="0" min="0" class="discount" onchange="sumPreferentialPrice();" onkeyup= "if( ! /^[+]?\\d+\\.\\d{0,4}$/){this.value=\'\';}" /></td>'
									+ '<td class="subtotal"></td>'
									+ '<td>'
									+ $("#addWarehouseSelect").html()
									+ '</td>'
									+ '<td><textarea id="remark" placeholder="备注内容"></textarea></td>';
							var tbl = $('.manual tr:eq(-2)');
							addRow(str, tbl);
						});
	});
	function addRow(str, tbl) {
		var addTr = document.createElement('tr');
		addTr.innerHTML = str;
		tbl.after(addTr);
	}
	// 汇总数量
	function sumNumber() {
		var sumNumber = "";
		$("#productTbody tr").each(function() {
			var number = $(this).find("td:eq(4)").find("input").val();
			sumNumber = Number(sumNumber) + Number(number);
		});
		$(".num").html(sumNumber);
		sumTotalPrice();
	}
	// 汇总优惠金额
	function sumPreferentialPrice() {
		var sumPreferentialPrice = "";
		$("#productTbody tr").each(function() {
			var preferentialPrice = $(this).find("td:eq(5)").find("input").val();
			sumPreferentialPrice = Number(sumPreferentialPrice) + Number(preferentialPrice);
		});
		$(".discount_sum").html(sumPreferentialPrice);
		sumTotalPrice();
	}
	// 汇总采购金额和总采购价格
	function sumTotalPrice() {
		var sumTotalPrice = "";
		$("#productTbody tr").each(function() {
			var dj = $(this).find("td:eq(3)").find("input").val(); // 单价
			var sl = $(this).find("td:eq(4)").find("input").val(); // 数量
			var yh = $(this).find("td:eq(5)").find("input").val(); // 数量
			var total = (dj * sl - yh).toFixed(4);//保留4位小数
			$(this).find("td:eq(6)").html(total);
			sumTotalPrice = (Number(sumTotalPrice) + Number(total)).toFixed(4);
		});
		$(".total").html(sumTotalPrice);
		$("#thisPrice").html(sumTotalPrice);
	}
	// 提交申请
	function subModify() {
		//供应商判空
		var suppName = $("#suppName").val();
		if(suppName == ''){
			layer.msg("请填写供应商！",{icon:7});
			return;
		}
		var person = $("#person").val();
		if(person == ''){
			layer.msg("请填写联系人！",{icon:7});
			return;
		}
		var phone = $("#phone").val();
		if(phone == ''){
			layer.msg("请填写手机号！",{icon:7});
			return;
		}
		// 商品信息
		var productInfo = "";
		var skuoIdArr = "";
		var pronameErrot = "";
		var unitError="";
		var wareError="";
		var numError="";
		$("#productTbody tr").each(
			function(i) {
				var skuOid = $(this).find("td:eq(1)").find("input:eq(5)").val();
				if(skuOid != ''){
					var proCode = $(this).find("td:eq(1)").find("input:eq(1)").val();
					var proName = $(this).find("td:eq(1)").find("input:eq(2)").val();
					var skuCode = $(this).find("td:eq(1)").find("input:eq(3)").val();
					var skuName = $(this).find("td:eq(1)").find("input:eq(4)").val();
					if(skuoIdArr.indexOf(skuOid+",")>-1){
						pronameErrot = "第"+(i+1)+"行商品重复！";;
						return false;
					}else{
						skuoIdArr += skuOid+",";
					}
					//单位验证
					var unitId = $(this).find("td:eq(2)").find("option:selected").val();
					if(unitId==''){
						unitError="第"+(i+1)+"行没有设置单位！";
						return false;
					}
					//数量
					var num = $(this).find("td:eq(4)").find("input").val();
					if(parseInt(num)<=0){
						numError="第"+(i+1)+"行数量必须大于0！";
						return false;
					}
					if(!objValidate($(this).find("td:eq(4) input"),2)){
						numError="第"+(i+1)+"行数量必须为整数！";
						return false;
					}
					var price = $(this).find("td:eq(3)").find("input").val();
					var preferentialPrice = $(this).find("td:eq(5)").find("input").val();
					var totalPrice = $(this).find("td:eq(6)").html();
					var warehouseId = $(this).find("td:eq(7)").find("option:selected").val();
					//仓库验证
					if(warehouseId==''){
						wareError = "第"+(i+1)+"行没有设置仓库！";
						return false;
					}
					var remark = $(this).find("td:eq(8)").find("textarea").val();
					productInfo = productInfo + proCode + "," + proName + ","
							+ skuCode + "," + skuName + "," + skuOid + ","
							+ unitId + "," + price + "," + num + ","
							+ preferentialPrice + "," + totalPrice + ","
							+ remark + "," + warehouseId + "@";
				}
			});
		if(skuoIdArr == ''){
			layer.msg("请填写商品！",{icon:2});
			return;
		}
		if(pronameErrot!=''){
			layer.msg(pronameErrot,{icon:2});
			return;
		}
		if(unitError!=''){
			layer.msg(unitError,{icon:2});
			return;
		}
		if(wareError!=''){
			layer.msg(wareError,{icon:2});
			return;
		}
		if(numError!=''){
			layer.msg(numError,{icon:2});
			return;
		}
		productInfo = productInfo.substring(0, productInfo.length - 1);
		sumTotalPrice();
		// 附件
		var a3 = $("#attachment3Div").find("span").length;
		var a3Str = ""
		if(a3 != 0){
			for(var i = 0;i < a3;i++){
				var url3 = $("#attachment3Div").find("span:eq("+i+")").find("input").val();
				a3Str = a3Str + url3 + ",";
			}
			a3Str = a3Str.substring(0, a3Str.length - 1);
		}
		var a4 = $("#attachment4Div").find("span").length;
		var a4Str = ""
		if(a4 != 0){
			for(var i = 0;i < a4;i++){
				var url4 = $("#attachment4Div").find("span:eq("+i+")").find("input").val();
				a4Str = a4Str + url4 + ",";
			}
			a4Str = a4Str.substring(0, a4Str.length - 1);
		}
		var a5 = $("#attachment5Div").find("span").length;
		var a5Str = ""
		if(a5 != 0){
			for(var i = 0;i < a5;i++){
				var url5 = $("#attachment5Div").find("span:eq("+i+")").find("input").val();
				a5Str = a5Str + url5 + ",";
			}
			a5Str = a5Str.substring(0, a5Str.length - 1);
		}
		var suppId = $("#suppId").val();
		if(suppId != ""){
			var y_suppName = $("#y_suppName").val();
			var y_person = $("#y_person").val();
			var y_phone = $("#y_phone").val();
			if(y_suppName != suppName || y_person != person || y_phone != phone){
				suppId = "";
			}
		}
		var url = "buyer/manualOrder/saveAddManualOrder";
		$.ajax({
			type : "POST",
			url : url,
			async : false,
			data : {
				"suppId" : suppId,
				"suppName" : suppName,//供应商名称
				"person" : person,//供应商联系人
				"phone" : phone,//手机号
				"goodsNum" : $(".num").html(),//总数量
				"goodsPrice" : $(".total").html(),//商品总价格

				"productInfo" : productInfo,
				"postage" : $("#postage").val(),
				"purchasePrice" : $("#purchasePrice").val(),//采购费用
				"thisPrice" : $("#thisPrice").html(),// 本次应付款
				"paymentPrice" : $("#paymentPrice").val(),//付款金额
				"paymentDate" : $("#paymentDate").val(),//付款日期
				"paymentType" : $("#paymentType").val(),//付款方式
				"paymentPerson" : $("#paymentPerson").val(),//付款人
				"handler" : $("#handler").val(),//经办人
				"a3Str" : a3Str,
				"a4Str" : a4Str,
				"a5Str" : a5Str,
				"menuName" : "17070718421516112012"
			},
			success : function(data) {
				if(data.flag){
					var res = data.res;
					if(res.code==40000){
						//调用成功
						saveSucess();
					}else if(res.code==40010){
						//调用失败
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40011){
						//需要设置审批流程
						layer.msg(res.msg,{time:500,icon:2},function(){
							setApprovalUser(url,res.data,function(data){
								saveSucess(data);
							});
						});
						return false;
					}else if(res.code==40012){
						//对应菜单必填
						layer.msg(res.msg,{icon:2});
						return false;
					}else if(res.code==40013){
						//不需要审批
						notNeedApproval(res.data,function(data){
							saveSucess(data);
						});
						return false;
					}
				}else{
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
					return false;
				}
			},
			error : function() {
				layer.msg("获取数据失败，请稍后重试！", {icon : 2});
			}
		});
	}
	//保存成功
function saveSucess(ids){
	$.ajax({
		url:"buyer/manualOrder/addManualOrderSucc",
		success:function(data){
			var str = data.toString();
			$(".content").html(str);
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
	// 商品查询（所有）
function selectManualGoods(obj){
	var goodsUlObj = $(obj).parent().find("ul");
	if(goodsUlObj.length == 0){
		goodsUlObj = $("<ul></ul>");
		goodsUlObj.attr("class","menu ui-menu ui-widget ui-widget-content");
		$(obj).parent().append(goodsUlObj);
	}
	goodsUlObj.empty();
	goodsUlObj.css("display","block");
	
	var selectValue = $(obj).val();
	if(selectValue != ''){
		var url = basePath+"platform/product/selectGoods";
		$.ajax({
			url : url,
			data:{
				"selectValue":selectValue
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				$.each(resultObj,function(i){
					var liObj = $('<li>');
					$("<input>",{name:"proCode",value:resultObj[i].product_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"proName",value:resultObj[i].product_name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuCode",value:resultObj[i].sku_code,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuName",value:resultObj[i].sku_name,hidden:true}).appendTo(liObj);
					$("<input>",{name:"skuOid",value:resultObj[i].barcode,hidden:true}).appendTo(liObj);
					$("<input>",{name:"unitId",value:resultObj[i].unit_id,hidden:true}).appendTo(liObj);
					//$("<input>",{name:"price",value:resultObj[i].price,hidden:true}).appendTo(liObj);
					var showText = resultObj[i].product_code+"|"
						+resultObj[i].product_name+"|"
						+resultObj[i].sku_code+"|"
						+resultObj[i].sku_name+"|"
						+resultObj[i].barcode;
					var abridgeText = (showText.length)>25?showText.substring(0,25)+"...":showText;
					$("<a href='javascript:void(0)' onclick='chooseManualGoods(this)' title='"+showText+"'>"+abridgeText+"</a>").appendTo(liObj);
					liObj.appendTo(goodsUlObj);
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//选择商品
function chooseManualGoods(obj){
	var ul = $(obj).parents("ul");
	var tr = ul.parents("tr");
	tr.find("input[name='productCode']").val($(obj).parent().find("input[name='proCode']").val());
	tr.find("input[name='productName']").val($(obj).parent().find("input[name='proName']").val());
	tr.find("input[name='skuCode']").val($(obj).parent().find("input[name='skuCode']").val());
	tr.find("input[name='skuName']").val($(obj).parent().find("input[name='skuName']").val());
	tr.find("input[name='barcode']").val($(obj).parent().find("input[name='skuOid']").val());
	tr.find("td:eq(2) select").val($(obj).parent().find("input[name='unitId']").val());
	//tr.find("td:eq(3)").html($(obj).parent().find("input[name='price']").val());
	tr.find("input[name='productNameText']").val($(obj).attr("title"));
	ul.remove();
}
// 供应商查询（非互通）
function selectSuppliers(obj){
	var suppliersUlObj = $(obj).parent().find("ul");
	if(suppliersUlObj.length == 0){
		suppliersUlObj = $("<ul></ul>");
		suppliersUlObj.attr("class","menu ui-menu ui-widget ui-widget-content");
		$(obj).parent().append(suppliersUlObj);
	}
	suppliersUlObj.empty();
	suppliersUlObj.css("display","block");
	
	var selectValue = $(obj).val();
	if(selectValue != ''){
		var url = basePath+"buyer/manualOrder/selectSuppliers";
		$.ajax({
			url : url,
			data:{
				"selectValue":selectValue
			},
			success:function(data){
				var result = eval('(' + data + ')');
				var resultObj = isJSONObject(result)?result:eval('(' + result + ')');
				$.each(resultObj,function(i){
					var liObj = $('<li>');
					$("<input>",{name:"person",value:resultObj[i].person,hidden:true}).appendTo(liObj);
					$("<input>",{name:"phone",value:resultObj[i].phone,hidden:true}).appendTo(liObj);
					$("<input>",{name:"suppId",value:resultObj[i].id,hidden:true}).appendTo(liObj);
					var showText = resultObj[i].suppName;
					var abridgeText = (showText.length)>25?showText.substring(0,25)+"...":showText;
					$("<a href='javascript:void(0)' onclick='chooseSuppliers(this)' title='"+showText+"'>"+abridgeText+"</a>").appendTo(liObj);
					liObj.appendTo(suppliersUlObj);
				});
			},
			error:function(){
				layer.msg("获取数据失败，请稍后重试！",{icon:2});
			}
		});
	}
}
//选择商品
function chooseSuppliers(obj){
	var ul = $(obj).parents("ul");
	var tr = ul.parents("tr");
	$("#person").val($(obj).parent().find("input[name='person']").val());
	$("#y_person").val($(obj).parent().find("input[name='person']").val());
	$("#phone").val($(obj).parent().find("input[name='phone']").val());
	$("#y_phone").val($(obj).parent().find("input[name='phone']").val());
	$("#suppId").val($(obj).parent().find("input[name='suppId']").val());
	$("#suppName").val($(obj).attr("title"));
	$("#y_suppName").val($(obj).attr("title"));
	ul.remove();
}
</script>
<div>
	<!--搜索部分-->
	<div class="search_manual mt">
		<form class="layui-form" action="">
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>
					供应商:</label>
				<div class="layui-input-inline">
					<input type="text" id="suppName" lay-verify="title"
						autocomplete="off" placeholder="请输入供应商名称" class="layui-input"
						style="width: 200px" onkeyup="selectSuppliers(this);" >
					<input type="hidden" id="suppId" value=""/>
					<input type="hidden" id="y_suppName" value=""/>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>
					联系人:</label>
				<div class="layui-input-inline">
					<input type="text" id="person" lay-verify="title"
						autocomplete="off" class="layui-input" placeholder="请输入联系人"
						required>
					<input type="hidden" id="y_person" value=""/>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label"><span class="red">*</span>
					手机号:</label>
				<div class="layui-input-inline">
					<input type="tel" id="phone" lay-verify="title"
						autocomplete="off" class="layui-input" placeholder="请输入手机号"
						required>
					<input type="hidden" id="y_phone" value=""/>
				</div>
			</div>
			<button class="invite"></button>
		</form>
	</div>
	<!--采购列表部分-->
	<table class="table_pure purchase_list manual mt">
		<thead>
			<tr>
				<td style="width: 5%">删除</td>
				<td style="width: 35%">商品</td>
				<td style="width: 8%">单位</td>
				<td style="width: 8%">采购单价</td>
				<td style="width: 6%">数量</td>
				<td style="width: 7%">优惠金额</td>
				<td style="width: 12%">采购金额</td>
				<td style="width: 9%">仓库</td>
				<td style="width: 10%">备注</td>
			</tr>
			<tr>
				<td colspan="9"></td>
			</tr>
		</thead>
		<tbody id="productTbody">
			<tr>
				<td><!-- <b></b> --></td>
				<td align="left">
					<input type="text" name="productNameText" placeholder="输入商品名称/货号/条形码等关键字" onkeyup="selectManualGoods(this);">
					<input type="hidden" name="productCode" value=""/><!-- 货号 -->
					<input type="hidden" name="productName" value=""/><!-- 名称 -->
					<input type="hidden" name="skuCode" value=""/><!-- 规格代码 -->
					<input type="hidden" name="skuName" value=""/><!-- 规格名称 -->
					<input type="hidden" name="barcode" value=""/><!-- 条形码 -->
				<td><select name="unit" id="unit">
						<option value="">请选择</option>
						<c:forEach var="unit" items="${unitList}">
							<option value="${unit.id}">${unit.unitName}</option>
						</c:forEach>
				</select></td>
				<!-- 单价 -->
				<td><input type="number" value="0" min="0" class="unit_price"
					onchange="sumTotalPrice();"
					onkeyup="if( ! /^[+]?\d+\.\d{0,4}$/){this.value='';}" ></td>
				<!-- 数量 -->
				<td><input type="number" value="0" min="0" class="number"
					onchange="sumNumber();"
					onkeyup="this.value=this.value.replace(/[^\- \d.]/g,'')"></td>
				<!-- 优惠金额 -->
				<td><input type="number" value="0" min="0" class="discount"
					onchange="sumPreferentialPrice();"
					onkeyup="if( ! /^[+]?\d+\.\d{0,4}$/){this.value='';}"></td>
				<td class="subtotal"></td>
				<td><select name="warehouse" id="warehouse">
						<option value="">请选择</option>
						<c:forEach var="warehouse" items="${warehouseList}">
							<option value="${warehouse.id}">${warehouse.whareaName}</option>
						</c:forEach>
				</select></td>
				<td><textarea id="remark" placeholder="备注内容"></textarea></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td>合计</td>
				<td></td>
				<td></td>
				<td class="num">0</td>
				<!-- 总数量 -->
				<td class="discount_sum">0</td>
				<td class="total">0.00</td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
	</table>
	<button class="tr_add_buyer"></button>
	<!--上传凭证-->
	<form action="">
		<ul class="voucher">
			<li>
            	<span>商品运费：</span>
            	<input type="number" placeholder="请输入商品运费" name="postage" id="postage">
            </li>
			<li>
				<p>
					<span>采购费用：</span> <input type="number" id="purchasePrice"
						placeholder="请输入采购费用">
				</p>
				<p>
					<span>本次应付款：</span> <b class="red">￥ <b id="thisPrice">0.00</b></b>
				</p>
			</li>
			<li>
				<p>
					<span>付款金额：</span> <input type="number" id="paymentPrice" placeholder="请输入付款金额">元
				</p>
				<p>
					<span>付款日期：</span> <input type="text" name="date" lay-verify="date"
						placeholder="请选择日期" autocomplete="off" class="layui-input" id="paymentDate"
						 style="display: inline-block;">
				</p>
			</li>
			<li>
				<p>
					<span>付款方式：</span> <select name="paymentType" id="paymentType">
						<option value="">请选择</option>
						<option value="0">银行卡转账</option>
						<option value="1">现金</option>
						<option value="2">网银支付</option>
					</select>
				</p>
				<p>
					<span>付款人：</span> <input type="text" id="paymentPerson" placeholder="请输入付款人姓名">
				</p>
			</li>
			<li><span>采购凭证：</span>
				<div class="upload_license">
					<input type="file" multiple name="attachment3" id="attachment3" onchange="fileUpload(this);">
					<p></p>
					<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
				</div>
				<div class="voucherImg" id="attachment3Div"></div></li>
			<li><span>付款凭证：</span>
				<div class="upload_license">
					<input type="file" multiple name="attachment4" id="attachment4" onchange="fileUpload(this);">
					<p></p>
					<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
				</div>
				<div class="voucherImg" id="attachment4Div"></div></li>
			<li><span>合同原件：</span>
				<div class="upload_license">
					<input type="file" multiple name="attachment5" id="attachment5" onchange="fileUpload(this);">
					<p></p>
					<span>仅支持JPG,GIF,PNG,JPEG格式，且文件小于4M</span>
				</div>
				<div class="voucherImg" id="attachment5Div"></div></li>
			<li><span>经办人：</span> <input type="text" id="handler" placeholder="请输入经办人姓名">
			</li>
		</ul>
	</form>
	<div class="text-center">
		<button class="next_step" onclick="subModify();">提交申请</button>
	</div>
</div>
<div id="addUnitSelect" hidden="true">
	<select name="unit" id="unit">
		<option value="">请选择</option>
		<c:forEach var="unit" items="${unitList}">
			<option value="${unit.id}">${unit.unitName}</option>
		</c:forEach>
	</select>
</div>
<div id="addWarehouseSelect" hidden="true">
	<select name="warehouse" id="warehouse">
		<option value="">请选择</option>
		<c:forEach var="warehouse" items="${warehouseList}">
			<option value="${warehouse.id}">${warehouse.whareaName}</option>
		</c:forEach>
	</select>
</div>
