<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
layui.use('table', function() {
	var table = layui.table;
	table.on('tool(sysUserTableFilter)', function(obj){
	    var data = obj.data;
	    if(obj.event === 'edit'){
	    	//修改
	    	edit(data.id);
	    }else if(obj.event === 'setRole'){
	    	//设置角色
	    	setRole(data.id);
	    }else if(obj.event === 'setShop'){
	    	//设置店铺
	    	setShop(data.id);
	    }else if(obj.event === 'setSupplier'){
	    	//设置供应商
	    	setSupplier(data.id);
	    }
	});
	var $ = layui.$, active = {
		//重载
		reload: function(){
	      //执行重载
	      table.reload('sysUserTable', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	          userName     : $("#userName").val(),
	          loginName      : $("#loginName").val()
	        }
	      });
	    },
	    addUser: function(){
	    	$.ajax({
				url:basePath+"platform/sysUser/loadAddSysUser",
				type:"post",
				async:false,
				success:function(data){
					layer.open({
						type:1,
						title:"新增人员",
						skin: 'layui-layer-rim',
		  		        area: ['400px', 'auto'],
		  		        content:data,
		  		        btn:['确定','取消'],
		  		        yes:function(index,layerio){
		  		        	$.ajax({
		  		        		url:basePath+"platform/sysUser/saveAdd",
		  		        		type:"post",
		  		        		data:$("#sysUserForm").serialize(),
		  		        		async:false,
		  		        		success:function(data){
		  		        			var result=eval('('+data+')');
							    	var resultObj=isJSONObject(result)?result:eval('('+result+')');
									if(resultObj.success){
										layer.close(index);
										layer.msg(resultObj.msg,{icon:1});
										active["reload"].call(this);
									}else{
										layer.msg(resultObj.msg,{icon:2});
									}
		  		        		},
		  		        		error:function(){
		  		        			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		  		        		}
		  		        	});
		  		        }
					});
				},
				error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	    }
	};
	$('.demoTable .layui-btn').on('click', function(){
	   var type = $(this).data('type');
	   active[type] ? active[type].call(this) : '';
	});
});
</script>
<div class="demoTable">
  <div class="layui-inline">
    <input class="layui-input" name="userName" id="userName" autocomplete="off" placeholder='姓名'>
  </div>
  <div class="layui-inline">
    <input class="layui-input" name="loginName" id="loginName" autocomplete="off" placeholder='登录帐号'>
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  <button class="layui-btn" data-type="addUser" button="新增">新增</button>
</div>
<table class="layui-table" 
	lay-data="{
		height:255,
		cellMinWidth: 80,
		page:true,
		limit:50,
		id:'sysUserTable',
		height:'full-90',
		url:'<%=basePath %>platform/sysUser/loadDataJson'
	}" lay-filter="sysUserTableFilter">
    <thead>
        <tr>
            <th lay-data="{field:'id', sort: true,show:false}">id</th>
            <th lay-data="{field:'userName', sort: true,show:true}">用户姓名</th>
            <th lay-data="{field:'loginName', sort: true,show:true}">登录账号</th>
            <th lay-data="{field:'companyId', sort: true,show:false}">公司id</th>
            <th lay-data="{field:'companyName', sort: true,show:false}">公司名称</th>
            <th lay-data="{field:'dingdingId', sort: true,show:true}">钉钉ID</th>
            <th lay-data="{field:'isDel', sort: true,show:true,templet: '#isDelTpl', align: 'center'}">状态</th>
            <th lay-data="{align:'center', toolbar: '#operate',show:true,width:300}">操作</th>
        </tr>
    </thead>
</table>
<!-- 操作 -->
<script type="text/html" id="operate">
	<a class="layui-btn layui-btn-update layui-btn-xs" lay-event="edit" button="修改">修改</a>
	<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="setRole" button="设置角色">设置角色</a>
	<a class="layui-btn layui-btn-xs" lay-event="setShop" button="设置店铺">设置店铺</a>
	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="setSupplier" button="设置供应商">设置供应商</a>
</script>
<!-- 状态转换 -->
<script type="text/html" id="isDelTpl">
	{{#  if(d.isDel === 0){ }}
		<span style="color: green;">正常</span>
	{{#  } else if(d.isDel === 1){}}
		<span style="color: red;">禁用</span>
	{{#  } }}
</script>
<script>
function edit(id){
	$.ajax({
		url:basePath+"platform/sysUser/loadEditPerson",
		async:false,
		type:"post",
		data:{"id":id},
		success:function(data){
			layer.open({
			type:1,
			title:"修改人员",
			skin: 'layui-layer-rim',
  		    area: ['400px', 'auto'],
  		    content:data,
  		    btn:['确定','取消'],
  		    yes:function(index,layero){
  		    	$.ajax({
  		    		url:basePath+"platform/sysUser/updateSysUser",
  		    		data:$("#editSysForm").serialize(),
  		    		async:false,
  		    		type:"post",
  		    		success:function(data){
  		    			var result=eval('('+data+')');
						var resultObj=isJSONObject(result)?result:eval('('+result+')');
						if(resultObj.success){
							layer.close(index);
							layer.msg(resultObj.msg,{icon:1});
							$(".demoTable .layui-btn").click();
						}else{
							layer.msg(resultObj.msg,{icon:2});
						}
					},
 		    		error:function(){
 		    			layer.msg("获取数据失败，请稍后重试！",{icon:2});
 		    		}
  		    	});
  		    }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
function deleteSys(id){
	layer.confirm("删除此账户后，该账户将不能再登录此企业的系统进行操作，确定要删除此账户吗？",{skin:"layui-layer-rim",
	      title:"提醒",
	      area: ["320px","auto"]
	      },function(index){
	      		$.ajax({
				url:basePath+"platform/sysUser/deleteSysUser",
				async:false,
				type:"post",
				data:{"id":id},
				success:function(data){
					var result=eval('('+data+')');
					var resultObj=isJSONObject(result)?result:eval('('+result+')');
					if(resultObj.success){
						layer.msg(resultObj.msg,{icon:1});
						active["reload"].call(this);
					}else{
						layer.msg(resultObj.msg,{icon:2});
					}
				},
				error:function(){
					layer.msg("获取数据失败，请稍后重试！",{icon:2});
				}
			});
	      }.bind(id));
}
//设置角色
function setRole(userId){
	$.ajax({
		url:basePath+"platform/tradeRole/instalRoleUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置角色",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//设置店铺
function setShop(userId){
	$.ajax({
		url:basePath+"platform/sysshop/instalShopUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置关联店铺",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
//设置供应商
function setSupplier(userId){
	$.ajax({
		url:basePath+"platform/buyer/supplier/instalSupplierUser",
		data:{"userId":userId},
		success:function(data){
			layer.open({
				type:1,
				title:"设置关联供应商",
				skin: 'layui-layer-rim',
  		        area: ['800px', 'auto'],
  		        content:data,
  		        yes:function(index,layerio){
  		        }
			});
		},
		error:function(){
			layer.msg("获取数据失败，请稍后重试！",{icon:2});
		}
	});
}
</script>