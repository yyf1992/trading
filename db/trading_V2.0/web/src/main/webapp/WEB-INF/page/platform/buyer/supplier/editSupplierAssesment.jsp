<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="<%=basePath%>statics/platform/css/supplier.css">
<script src="<%=basePath%>statics/platform/js/supplier.js" type="text/javascript"></script>
<div>
	<table class="assesmentTable">
		<thead>
			<tr>
				<td>考核名称</td>
				<td>总分值</td>
				<td>备注</td>
				<td>状态</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><input type="text" placeholder="考核名称" class="assessmentName" value="${assesment.assessmentName }"></td>
				<td><input type="number" min="0.0" step="0.1" placeholder="分值" class="score" value="${assesment.score }" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"></td>
				<td><input type="text" placeholder="备注" class="remark" value="${assesment.remark }"></td>
				<td>
				<c:choose>
					<c:when test="${assesment.state==0}">
						<input type="radio" name="state" class="state" value="0" checked="checked">启用
						<input type="radio" name="state" class="state" value="1">禁用
					</c:when>
					<c:when test="${assesment.state==1}">
						<input type="radio" name="state" class="state" value="0">启用
						<input type="radio" name="state" class="state" value="1" checked="checked">禁用
					</c:when>
				</c:choose>
				</td>
			</tr>
		</tbody>
	</table>
	
	<h4 class="page_title" style="margin-top: 20px;">考核项</h4>
	
	<table class="tableSmall">
			<thead>
				<tr>
					<td>考核项</td>
					<td><c:if test="${assesment.scoreType==0 }">
							<b>分值</b><input type="radio" name="scoreType" class="scoreType" value="0" checked="checked">
							<b>百分比</b><input type="radio" name="scoreType" class="scoreType" value="1">
						</c:if>
						<c:if test="${assesment.scoreType==1 }">
							<b>分值</b><input type="radio" name="scoreType" class="scoreType" value="0">
							<b>百分比</b><input type="radio" name="scoreType" class="scoreType" value="1" checked="checked">
						</c:if>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1、到货及时率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="arrivalRateScore" value="${assesment.arrivalRateScore }" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"></td>
				</tr>
				<tr>
					<td>2、计划完成率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="completionRateScore" value="${assesment.completionRateScore }" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"></td>
				</tr>
				<tr>
					<td>3、到货合格率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="qualificationRateScore" value="${assesment.qualificationRateScore }" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"></td>
				</tr>
				<tr>
					<td>4、售后响应及时率</td>
					<td><span>分值/百分比(%)：</span><input type="number" min="0.0" step="0.1" placeholder="分值" class="afterSaleRateScore" value="${assesment.afterSaleRateScore }"  onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}"></td>
				</tr>
				<tr>
					<td>合计：</td>
					<td><span id="total" style="color: red">${assesment.arrivalRateScore + assesment.completionRateScore + assesment.qualificationRateScore + assesment.afterSaleRateScore}</span></td>
				</tr>
				<input type="hidden" value="${assesment.id }" class="id">
			</tbody>
		</table>	
		<div class="submit">
			<button class="cancle"onclick="leftMenuClick(this,'platform/buyer/supplier/loadSupplierPerformanceAppraisalHtml','buyer','18010209272828100705')">取消</button>
			<button class="save" onclick="saveEditSupplierAssesment();">保存</button>
		</div>
</div>