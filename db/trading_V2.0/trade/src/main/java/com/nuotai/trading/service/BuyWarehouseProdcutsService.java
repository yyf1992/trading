package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseProdcutsMapper;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
public class BuyWarehouseProdcutsService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseProdcutsService.class);

	@Autowired
	private BuyWarehouseProdcutsMapper buyWarehouseProdcutsMapper;
	
	public BuyWarehouseProdcuts get(String id){
		return buyWarehouseProdcutsMapper.get(id);
	}
	
	public List<BuyWarehouseProdcuts> queryList(Map<String, Object> map){
		return buyWarehouseProdcutsMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseProdcutsMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseProdcuts buyWarehouseProdcuts){
		buyWarehouseProdcutsMapper.add(buyWarehouseProdcuts);
	}
	
	public void update(BuyWarehouseProdcuts buyWarehouseProdcuts){
		buyWarehouseProdcutsMapper.update(buyWarehouseProdcuts);
	}
	
	public void delete(String id){
		buyWarehouseProdcutsMapper.delete(id);
	}
	
	/**
	 * 分页查询商品库存余额
	 * @param searchPageUtil
	 * @return
	 */
	public List<Map<String, Object>> selectProdcutsByPage(SearchPageUtil searchPageUtil) {

		return buyWarehouseProdcutsMapper.selectProdcutsByPage(searchPageUtil);
	}

	public BuyWarehouseProdcuts selectProdcutByid(String id){
		return buyWarehouseProdcutsMapper.selectProdcutByid(id);
	}
	
	/**
	 * 分页查询商品库存余额
	 * @param searchPageUtil
	 * @return
	 */
	public List<Map<String, Object>> selectProdcutsDetailsByPage(SearchPageUtil searchPageUtil) {

		return buyWarehouseProdcutsMapper.selectProdcutsDetailsByPage(searchPageUtil);
	}
	
	
}
