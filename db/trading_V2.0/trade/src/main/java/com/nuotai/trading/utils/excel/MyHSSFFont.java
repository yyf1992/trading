package com.nuotai.trading.utils.excel;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

@SuppressWarnings("deprecation")
public class MyHSSFFont {
	//加粗字体
	private HSSFFont BOLD;
	//正常字体
	private HSSFFont NORMAL;
	
	public MyHSSFFont(HSSFWorkbook wb){
		this.BOLD = (HSSFFont) wb.createFont();
		BOLD.setFontHeightInPoints((short) 12);
		BOLD.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		
		this.NORMAL = (HSSFFont) wb.createFont();
		NORMAL.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
	}
	
	public HSSFFont getBOLD() {
		return BOLD;
	}
	public void setBOLD(HSSFFont bOLD) {
		BOLD = bOLD;
	}
	public HSSFFont getNORMAL() {
		return NORMAL;
	}
	public void setNORMAL(HSSFFont nORMAL) {
		NORMAL = nORMAL;
	}
}
