package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyStockBacklogDetailsMapper;
import com.nuotai.trading.model.BuyStockBacklogDetails;
import com.nuotai.trading.model.BuyWarehouseBacklogDetails;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyStockBacklogDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyStockBacklogDetailsService.class);

	@Autowired
	private BuyStockBacklogDetailsMapper buyStockBacklogDetailsMapper;
	
	public BuyStockBacklogDetails get(String id){
		return buyStockBacklogDetailsMapper.get(id);
	}
	
	public List<BuyStockBacklogDetails> queryList(Map<String, Object> map){
		return buyStockBacklogDetailsMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyStockBacklogDetailsMapper.queryCount(map);
	}
	
	public void add(BuyStockBacklogDetails buyStockBacklogDetails){
		buyStockBacklogDetailsMapper.add(buyStockBacklogDetails);
	}
	
	public void update(BuyStockBacklogDetails buyStockBacklogDetails){
		buyStockBacklogDetailsMapper.update(buyStockBacklogDetails);
	}
	
	public void delete(String id){
		buyStockBacklogDetailsMapper.delete(id);
	}
	
	/**
	 * 分页查询展示
	 * @param searchPageUtil
	 * @return
	 */
	public List<BuyStockBacklogDetails> selectProdcutsByPage(SearchPageUtil searchPageUtil) {
		
		List<BuyStockBacklogDetails> buyStockBacklogDetailsList = buyStockBacklogDetailsMapper.selectProdcutsByPage(searchPageUtil);

		return buyStockBacklogDetailsList;
	}
	
	/**
	 * 查询所有的符合条件的数据导出
	 * @param searchPageUtil
	 * @return
	 */
	public List<BuyStockBacklogDetails> selectProdcutsByParams(Map<String,Object> params) {
		
		List<BuyStockBacklogDetails> buyStockBacklogDetailsList = buyStockBacklogDetailsMapper.selectProdcutsByParams(params);

		return buyStockBacklogDetailsList;
	}
	

}
