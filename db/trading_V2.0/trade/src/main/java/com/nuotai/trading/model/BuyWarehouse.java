package com.nuotai.trading.model;

import java.util.Date;

/**
 * 仓库表
 * @author dxl
 *
 */
public class BuyWarehouse {
    // 主键id
    private String id;
    // 店铺id
    private String shopId;
    // 店铺code
    private String shopCode;
    // 店铺名称
    private String shopName;
    // 仓库名称
    private String wareHouseName;
    // 仓库code
    private String wareCode;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 创建时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人姓名
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人名称
    private String delName;
    // 删除时间
    private Date delDate;
    // 备注
    private String remark;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getShopId() {
        return shopId;
    }
    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
    public String getShopCode() {
        return shopCode;
    }
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
    public String getShopName() {
        return shopName;
    }
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
    public String getWareHouseName() {
        return wareHouseName;
    }
    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }
    public String getWareCode() {
        return wareCode;
    }
    public void setWareCode(String wareCode) {
        this.wareCode = wareCode;
    }
    public Integer getIsDel() {
        return isDel;
    }
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    public String getCreateId() {
        return createId;
    }
    public void setCreateId(String createId) {
        this.createId = createId;
    }
    public String getCreateName() {
        return createName;
    }
    public void setCreateName(String createName) {
        this.createName = createName;
    }
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getUpdateId() {
        return updateId;
    }
    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }
    public String getUpdateName() {
        return updateName;
    }
    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }
    public Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    public String getDelId() {
        return delId;
    }
    public void setDelId(String delId) {
        this.delId = delId;
    }
    public String getDelName() {
        return delName;
    }
    public void setDelName(String delName) {
        this.delName = delName;
    }
    public Date getDelDate() {
        return delDate;
    }
    public void setDelDate(Date delDate) {
        this.delDate = delDate;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
}