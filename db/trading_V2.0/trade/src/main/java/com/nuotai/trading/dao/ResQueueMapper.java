package com.nuotai.trading.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.ResQueue;

/**
 * 资源队列dao
 * @ClassName ResQueueMapper
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
public interface ResQueueMapper {

	/**
	 * 获取队列列表
	 * @author:     dxl
	 * @param queue
	 * @return
	 */
	List<ResQueue> getBeanList(ResQueue queue);

	/**
	 * 选择性添加
	 * @author:     dxl
	 * @param queue
	 */
	void addBean(ResQueue queue);

	/**
	 * 修改
	 * @author:     dxl
	 * @param resQueue
	 */
	void updateBean(ResQueue resQueue);

	/**
	 * 删除
	 * @author:     dxl
	 * @param id
	 */
	void deleteBean(@Param("id")String id);

}
