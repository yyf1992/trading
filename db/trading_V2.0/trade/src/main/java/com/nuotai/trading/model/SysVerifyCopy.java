package com.nuotai.trading.model;

import java.io.Serializable;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-08-21 11:50:20
 */
@Data
public class SysVerifyCopy implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//主表id
	private String headerId;
	//
	private String userId;
	//抄送人员姓名
	private String userName;
}
