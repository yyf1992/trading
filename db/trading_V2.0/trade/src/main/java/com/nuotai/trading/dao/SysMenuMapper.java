package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.utils.SearchPageUtil;

public interface SysMenuMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

	List<SysMenu> getAllMenuPage(SearchPageUtil searchPageUtil);

	List<SysMenu> getMenuByMap(Map<String, Object> map);

	int deleteByMenuId(@Param("idList")String[] idList);
	
	List<String> getMenuIdByRoleId(@Param("roleIdList") List<String> roleIdList);

	SysMenu getMenuByMenuName(String menuName);
	SysMenu getMenuByMenuNameOrId(String menuName);

	List<SysMenu> tradeLogin(Map<String, Object> map);
	List<SysMenu> getCompanyMenu(Map<String, Object> map);
}