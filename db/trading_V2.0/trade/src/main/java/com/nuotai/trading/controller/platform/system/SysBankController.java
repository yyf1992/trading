package com.nuotai.trading.controller.platform.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.SysBank;
import com.nuotai.trading.service.SysBankService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 银行账户表
 * 
 * @author "
 * @date 2017-08-11 09:05:08
 */
@Controller
@RequestMapping("platform/sysbank")
public class SysBankController extends BaseController{
	@Autowired
	private SysBankService sysBankService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/loadSysBankList")
	public String loadBankList(SearchPageUtil searchPageUtil,@RequestParam Map<String, Object> param){
//		if(searchPageUtil.getPage() == null){
//			searchPageUtil.setPage(new Page());
//		}
//		param.put("companyId",ShiroUtils.getCompId());
//		String accountName = (String) (param.get("accountName") == null ? "" : param.get("accountName").toString().trim());
//		param.put("accountName",accountName);
//		searchPageUtil.setObject(param);
//		//查询列表数据
//		List<SysBank> sysBankList = sysBankService.queryList(searchPageUtil);
//		searchPageUtil.getPage().setList(sysBankList);
//		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/system/bank/sysBankList";
	}
	
	/**
	 * 获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<SysBank> sysBankList = sysBankService.queryDataList(params);
				return sysBankList;
			}
		});
	}
	
	/**
	 * 加载添加银行账户界面
	 * @return
	 */
	@RequestMapping("/loadBankInsert")
	public String loadBankInsert(){
		return "platform/system/bank/addSysBank";
	}
	
	/**
	 * 保存新增账户
	 * @param sysBank
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveBankInsert",method=RequestMethod.POST)
	public JSONObject saveBankInsert(SysBank sysBank){
		JSONObject  json=sysBankService.saveBankInsert(sysBank);
		return json;
	}
	
	/**
	 * 加载修改账户界面
	 */
	@RequestMapping(value="/loadEditbank",method=RequestMethod.POST)
	public String loadEditbank(String id){
		SysBank sysBank=sysBankService.selectByPrimaryKey(id);
		model.addAttribute("sysBank",sysBank);
		return "platform/system/bank/editSysBank";
	}
	
	/**
	 * 保存修改账户
	 * @param sysBank
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveBankEdit",method=RequestMethod.POST)
	public JSONObject saveBankEdit(SysBank sysBank){
		JSONObject  json=sysBankService.saveBankEdit(sysBank);
		return json;
	}

	/**
	 * 根据查询账号列表
	 * @return
	 */
	@RequestMapping("getBankAccountBuyMap")
	@ResponseBody
	public String getBankAccountBuyMap(){
		List<SysBank> sysBankList = sysBankService.getBankAccountBuyMap();
		return JSONObject.toJSONString(sysBankList);
	}
}
