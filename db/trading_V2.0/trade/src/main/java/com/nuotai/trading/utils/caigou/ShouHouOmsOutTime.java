package com.nuotai.trading.utils.caigou;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.utils.ObjectUtil;

public class ShouHouOmsOutTime {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ShouHouOmsOutTime shouHouOmsOutTime = new ShouHouOmsOutTime();
		try {
			shouHouOmsOutTime.dealWith();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void dealWith() throws Exception{
		Connection tradeConn = DBUtils.getTradeConnection();
		List<Map<String,Object>> dataList = getPocessData(tradeConn);
		Map<String,Date> omsDateMap = new HashMap<>();
		if(!ObjectUtil.isEmpty(dataList)){
			String bcCode = "";
			for(Map<String,Object> bc : dataList){
				bcCode += bc.get("customerCode") + ",";
			}
			System.out.println("未出库售后单号=[" + bcCode+"]");
			JSONArray whstockArray = this.getOutFromWms(bcCode);
			if(!ObjectUtil.isEmpty(whstockArray)){
				for (int i = 0; i < whstockArray.size(); i++) {
    				// 循环
    				JSONObject logJson = whstockArray.getJSONObject(i);
    				String sourceno = logJson.get("sourceno") == null ? ""
							: logJson.getString("sourceno");
    				Date finishDate = logJson.get("finishdate") == null ? new Date()
						: new Date(logJson.getLong("finishdate"));
    				omsDateMap.put(sourceno, finishDate);
				}
			}
		}
		int length = dataList.size();
		for(Map<String,Object> datalMap : dataList){
			System.out.println("剩余："+length+"条数据,总计："+dataList.size());
			String id = datalMap.get("id").toString();
			String sourceno = datalMap.get("customerCode").toString();
			Date omsDate = omsDateMap.containsKey(sourceno)?omsDateMap.get(sourceno):null;
			if(omsDate != null){
				int result = updateOmsData(id,omsDate,tradeConn);
				System.out.println(sourceno+"更新oms出库时间="+result);
			}
			length--;
		}
		DBUtils.closeConnection(tradeConn);
	}
	

	private int updateOmsData(String id, Date omsDate,Connection tradeConn) throws Exception{
		StringBuffer updateSql = new StringBuffer("UPDATE buy_customer SET oms_finishdate=");
		updateSql.append("'"+sdf.format(omsDate)+"' ");
		updateSql.append(" where id='" + id + "';");
		PreparedStatement updatePs = tradeConn.prepareStatement(updateSql.toString());
		int result = updatePs.executeUpdate();
		// 关闭声明
		DBUtils.closeStatement(updatePs);
		return result;
	}

	private List<Map<String, Object>> getPocessData(Connection tradeConn) throws Exception{
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT id,customer_code FROM buy_customer WHERE arrival_type='1' AND oms_finishdate IS NULL;";
		PreparedStatement ps = tradeConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		// 取得结果集列数
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("id", rs.getString("id"));
			map.put("customerCode", rs.getString("customer_code"));
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		return dataList;
	}
	
	private JSONArray getOutFromWms(String bcCode) throws Exception {
        System.out.println("**************************从WMS获取出库信息 Begin****************************************");
        String path = "http://121.199.179.23:30003/oms_interface/getTradeAftermarket?sourceno=" + bcCode;
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // 调用
        InputStream inputStream = conn.getInputStream();
        // 接收
        InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
        BufferedReader budr = new BufferedReader(isr);
        String omsResult = budr.readLine();
        JSONArray whstockArray = JSONArray.parseArray(omsResult);
	return whstockArray;
}
}
