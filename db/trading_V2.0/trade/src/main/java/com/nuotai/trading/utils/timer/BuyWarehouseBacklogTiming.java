package com.nuotai.trading.utils.timer;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.dao.BuyWarehouseBacklogDetailsMapper;
import com.nuotai.trading.dao.BuyWarehouseBacklogMapper;
import com.nuotai.trading.model.BuyWarehouseBacklog;
import com.nuotai.trading.model.BuyWarehouseBacklogDetails;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.BuyWarehouseBacklogService;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.ObjectUtil;

/**
 * 商品库存积压最大天数提取记录定时任务
 * @author
 *
 */
@Component
public class BuyWarehouseBacklogTiming {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private BuyWarehouseBacklogDetailsMapper buyWarehouseBacklogDetailsMapper;
	@Autowired
	private BuyWarehouseBacklogMapper buyWarehouseBacklogMapper;
	@Autowired
	private BuyWarehouseBacklogService buyWarehouseBacklogService;
	
	@Scheduled(cron="0 59 23 * * ?")
	//0 */1 * * * ?
	//0 59 23 * * ?
	public void catchBuyWarehouseprodcutsHistory(){
		TimeTask timeTask = timeTaskService.selectByCode("BACKLOG_DAYNUM");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					//提取库存积压最大天数
					selectBacklogMaxday();
			
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					timeTask.setCurrentStatus("2");//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	/**
	 * 提取库存积压最大天数
	 */
	public void selectBacklogMaxday(){
		//获取商品库存积压明细
		List<BuyWarehouseBacklogDetails> prodcutsList = buyWarehouseBacklogDetailsMapper.selectProdcuts();
		System.out.println("********提取库存积压最大天数同步开始**********");
		if (prodcutsList != null && prodcutsList.size()>0){
			for (BuyWarehouseBacklogDetails prodcuts : prodcutsList){
				BuyWarehouseBacklog buyWarehouseBacklog = buyWarehouseBacklogMapper.selectByBarcode(prodcuts.getBarcode());
				if (!ObjectUtil.isEmpty(buyWarehouseBacklog)){
					buyWarehouseBacklog.setBacklogmaxday(prodcuts.getBacklogdaynum());
					buyWarehouseBacklogService.update(buyWarehouseBacklog);
				}			
			}
		}
		System.out.println("********提取库存积压最大天数同步结束**********");
	}
}
