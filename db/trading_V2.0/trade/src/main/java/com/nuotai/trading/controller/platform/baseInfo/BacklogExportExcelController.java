package com.nuotai.trading.controller.platform.baseInfo;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyWarehouseBacklogDetails;
import com.nuotai.trading.service.BuyWarehouseBacklogDetailsService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ShiroUtils;


/**
 * 库龄分析数据统计excel表格导出
 * 
 * @author wl
 * 
 */
@Controller
@RequestMapping("backlogWarningDownload")
@Scope("prototype")
public class BacklogExportExcelController extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(BacklogExportExcelController.class);
	@Autowired
	private BuyWarehouseBacklogDetailsService buyWarehouseBacklogDetailsService;		
	
	/**
	 * 库龄分析数据统计excel表格导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/backlogWarningData")
	public void exportPaymentExtend(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		String reportName = "库龄分析报表";
		long currTime = System.currentTimeMillis();
		// 内存中缓存记录行数
		int rowaccess = 100;
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBold(false);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		titleStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		titleStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		titleStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		titleStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		titleStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		cellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		cellStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		cellStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		cellStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setFont(font2);
		String[] title = {"商品货号","商品名称","规格代码","规格名称","条形码","订单类型","来源单号","到货日期","到货数量","积压库存","库龄时间"};
		// 大标题
		Row row = sheet.createRow(0);
		// 合并单元格
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		// 设置列宽
		sheet.setColumnWidth(0, 4500);
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 1 || i == 3){
				sheet.setColumnWidth(i, 7000);
			}else{
				sheet.setColumnWidth(i, 4500);
			}
		}
		int startRow = 2;
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("companyId", ShiroUtils.getCompId());
		List<BuyWarehouseBacklogDetails> buyWarehouseBacklogDetailsList = buyWarehouseBacklogDetailsService.selectProdcutsByParams(map);
		if (buyWarehouseBacklogDetailsList != null && buyWarehouseBacklogDetailsList.size() > 0) {
			for (int i = 0; i < buyWarehouseBacklogDetailsList.size(); i++) {
				BuyWarehouseBacklogDetails item = buyWarehouseBacklogDetailsList.get(i);
				// 创建行
				row = sheet.createRow(startRow);
				
				// 商品货号
				Cell cell = row.createCell(0);
				cell.setCellValue(item.getProductCode());
				cell.setCellStyle(cellStyle);
				
				// 商品名称
				cell = row.createCell(1);
				cell.setCellValue(item.getProductName());
				cell.setCellStyle(cellStyle);
				
				//规格代码
				cell = row.createCell(2);
				cell.setCellValue(item.getSkuCode());
				cell.setCellStyle(cellStyle);
				
				//规格名称
				cell = row.createCell(3);
				cell.setCellValue(item.getSkuName());
				cell.setCellStyle(cellStyle);
				
				//条形码
				cell = row.createCell(4);
				cell.setCellValue(item.getBarcode());
				cell.setCellStyle(cellStyle);
				
				//订单类型
				cell = row.createCell(5);
				cell.setCellValue(item.getBusinessType());
				cell.setCellStyle(cellStyle);
				
				//来源单号
				cell = row.createCell(6);
				cell.setCellValue(item.getBatchNo());
				cell.setCellStyle(cellStyle);
				
				SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//到货日期
				cell = row.createCell(7);
				cell.setCellValue(sdf.format( item.getStorageDate()));
				cell.setCellStyle(cellStyle);
				
				//到货数量
				cell = row.createCell(8);
				cell.setCellValue(item.getNumber());
				cell.setCellStyle(cellStyle);
				
				//积压库存
				cell = row.createCell(9);
				cell.setCellValue(item.getRemainingStock());
				cell.setCellStyle(cellStyle);
				
				//库龄时间
				cell = row.createCell(10);
				cell.setCellValue(item.getBacklogdaynum());
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
			}
		}
		LOG.debug("耗时:" + (System.currentTimeMillis() - currTime) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
}
