package com.nuotai.trading.utils.timer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.dao.BuyWarehouseProdcutsHistoryMapper;
import com.nuotai.trading.dao.BuyWarehouseProdcutsMapper;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.model.BuyWarehouseProdcutsHistory;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 商品库存历史记录定时任务
 * @author
 *
 */
@Component
public class ProdcutsHistoryTimingTask {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private BuyWarehouseProdcutsMapper buyWarehouseProdcutsMapper;
	@Autowired
	private BuyWarehouseProdcutsHistoryMapper buyWarehouseProdcutsHistoryMapper;
	
	@Scheduled(cron="0 30 23 * * ?")
	//0 */1 * * * ?
	//0 30 23 * * ?
	public void catchBuyWarehouseprodcutsHistory(){
		TimeTask timeTask = timeTaskService.selectByCode("PRODCUTS_HISTORY");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					//商品库存历史记录
					warehouseProdcutsHistory();
			
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					timeTask.setCurrentStatus("2");//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	/**
	 * 商品库存同步积压表
	 */
	public void warehouseProdcutsHistory(){
		//获取商品库存
		List<BuyWarehouseProdcuts> prodcutsList = buyWarehouseProdcutsMapper.selectProdcuts();
		System.out.println("********库存历史记录同步开始**********");
		if (prodcutsList != null && prodcutsList.size()>0){
			for (BuyWarehouseProdcuts prodcuts : prodcutsList){
				BuyWarehouseProdcutsHistory prodcutsHistory = new BuyWarehouseProdcutsHistory();
				prodcutsHistory.setId(ShiroUtils.getUid());
				prodcutsHistory.setHistoryId(prodcuts.getId());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				Date historyDate = null;
				try {
					historyDate = sdf.parse(sdf.format(date));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				prodcutsHistory.setHistoryDate(historyDate);
				prodcutsHistory.setCompanyId(prodcuts.getCompanyId());
				prodcutsHistory.setWarehouseId(prodcuts.getWarehouseId());
				prodcutsHistory.setShopId(prodcuts.getShopId());
				prodcutsHistory.setShopCode(prodcuts.getShopCode());
				prodcutsHistory.setShopName(prodcuts.getShopName());			
				prodcutsHistory.setProductCode(prodcuts.getProductCode());
				prodcutsHistory.setProductName(prodcuts.getProductName());
				prodcutsHistory.setSkuCode(prodcuts.getSkuCode());
				prodcutsHistory.setSkuName(prodcuts.getSkuName());
				prodcutsHistory.setBarcode(prodcuts.getBarcode());
				prodcutsHistory.setTotalCost(prodcuts.getTotalCost());
				prodcutsHistory.setUsableStock(prodcuts.getUsableStock());
				prodcutsHistory.setLockStock(prodcuts.getLockStock());
				prodcutsHistory.setMaxStock(prodcuts.getMaxStock());
				prodcutsHistory.setMinStock(prodcuts.getMinStock());
				prodcutsHistory.setStandardStock(prodcuts.getStandardStock());
				prodcutsHistory.setStocks(prodcuts.getStocks());
				prodcutsHistory.setStatus(prodcuts.getStatus());
				prodcutsHistory.setBacklogWarning(prodcuts.getBacklogWarning());
				prodcutsHistory.setRemark(prodcuts.getRemark());
				prodcutsHistory.setIsDel(prodcuts.getIsDel());
				prodcutsHistory.setCreateId(prodcuts.getCreateId());
				prodcutsHistory.setCreateName(prodcuts.getCreateName());
				prodcutsHistory.setCreateDate(prodcuts.getCreateDate());
				prodcutsHistory.setUpdateId(prodcuts.getUpdateId());
				prodcutsHistory.setUpdateName(prodcuts.getUpdateName());
				if (prodcuts.getUpdateDate()!=null){
					prodcutsHistory.setUpdateDate(prodcuts.getUpdateDate());
				}else{
					prodcutsHistory.setUpdateDate(prodcuts.getCreateDate());
				}			
				prodcutsHistory.setDelId(prodcuts.getDelId());
				prodcutsHistory.setDelName(prodcuts.getDelName());
				prodcutsHistory.setDelDate(prodcuts.getDelDate());

				//将商品库存同步到库存积压表		
				buyWarehouseProdcutsHistoryMapper.add(prodcutsHistory);
	
			}
		}
		System.out.println("********库存历史记录同步开始**********");
	}
}
