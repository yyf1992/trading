package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.DicProvincesMapper;
import com.nuotai.trading.model.DicProvinces;

@Service
public class DicProvincesService implements DicProvincesMapper {
	@Autowired
	private DicProvincesMapper mapper;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(DicProvinces record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(DicProvinces record) {
		return mapper.insertSelective(record);
	}

	@Override
	public DicProvinces selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(DicProvinces record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DicProvinces record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<DicProvinces> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	@Override
	public DicProvinces getProvinceById(Integer provinceId) {
		return mapper.getProvinceById(provinceId);
	}

}
