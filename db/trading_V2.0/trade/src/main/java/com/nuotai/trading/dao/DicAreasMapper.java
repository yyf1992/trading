package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.DicAreas;

public interface DicAreasMapper {
	int deleteByPrimaryKey(Integer id);

    int insert(DicAreas record);

    int insertSelective(DicAreas record);

    DicAreas selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DicAreas record);

    int updateByPrimaryKey(DicAreas record);
    
    List<DicAreas> selectByMap(Map<String, Object> map);
    
    DicAreas getAreaById(String areaId);
}