package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 审批主表
 * @author "
 * @date 2017-08-14 09:32:48
 */
@Data
public class SysVerifyHeader implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//审批标题
	private String title;
	//菜单id
	private String menuId;
	private String menuName;
	//公司id
	private String companyId;
	//备注
	private String remark;
	//审批类型:0-固定审批流程;1-自动审批流程
	private String verifyType;
	//状态:0-启用;1-禁用
	private String status;
	//去重规则：0-同一个审批人在流程中出现多次时，自动去重；1-同一个审批人仅在连续出现时，自动去重
	private String repeatType;
	//抄送人设置：0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
	private String copyType;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//
	private Date createDate;
	//修改人id
	private String updateUserId;
	//修改人姓名
	private String updateUserName;
	//修改时间
	private Date updateDate;
}
