package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-09-13 16:15:53
 */
@Data
public class BuyProductSkuBom implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司Id
	private String companyId;
	//BOM编号
	private String bomCode;
	//商品id
	private String productId;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//单位Id
	private String unitId;
	//单位名称
	private String unitName;
	//商品物料配置备注
	private String remark;
	//0审核未通过  1审核通过
	private String status;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//删除时间
	private Date updateDate;
	//删除人
	private String delId;
	//删除人名称
	private String delName;
	//删除日期
	private Date delDate;
	
	private List<Map<String,Object>> skuComposeList;
}
