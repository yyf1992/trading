package com.nuotai.trading.utils.json;

public class JsonModel {
	public String toJsonString() {
        return JsonFormatUtils.toJsonString(this);
    }

    public String toJsonNoDataString() {
        return toJsonString(new ValueFilter(Response.class, "data"));
    }

    public String toJsonString(ValueFilter... vfs) {
        return JsonFormatUtils.toJsonString(this, vfs);
    }
}
