package com.nuotai.trading.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.model.TradeVerifyCopy;



@Service
@Transactional
public class TradeVerifyCopyService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeVerifyCopyService.class);

	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	
	public TradeVerifyCopy get(String id){
		return tradeVerifyCopyMapper.get(id);
	}
	
	public List<TradeVerifyCopy> queryList(Map<String, Object> map){
		return tradeVerifyCopyMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeVerifyCopyMapper.queryCount(map);
	}
	
	public void add(TradeVerifyCopy tradeVerifyCopy){
		tradeVerifyCopyMapper.add(tradeVerifyCopy);
	}
	
	public void update(TradeVerifyCopy tradeVerifyCopy){
		tradeVerifyCopyMapper.update(tradeVerifyCopy);
	}
	
	public void delete(String id){
		tradeVerifyCopyMapper.delete(id);
	}
	

}
