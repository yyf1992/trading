package com.nuotai.trading.utils.excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

@SuppressWarnings("deprecation")
public class MyHSSFStyle {
	//居中、加粗、全边框
	private HSSFCellStyle BOLD_BORDER_All;
	//居中、全边框
	private HSSFCellStyle BORDER_All;
	//加粗左下边框
	private HSSFCellStyle BOLD_BORDER_BOTTOM_LEFT;
	//加粗左下边框
	private HSSFCellStyle BOLD_BORDER_BOTTOM_RIGHT;
	//加粗下边框
	private HSSFCellStyle BOLD_BORDER_BOTTOM;
	//加粗右上边框
	private HSSFCellStyle BOLD_BORDER_TOP_RIGHT;
	//加粗右上边框
	private HSSFCellStyle BOLD_BORDER_NONE;
	private HSSFCellStyle BOLD_BORDER_NORMAL;
	private HSSFCellStyle patriarchStyle;
	//加粗右上边框
	private MyHSSFFont myFont;
	
	public MyHSSFStyle(HSSFWorkbook wb){
		this.patriarchStyle = (HSSFCellStyle)wb.createCellStyle();
		patriarchStyle.setWrapText(true);
		patriarchStyle.setAlignment((short)2);
		patriarchStyle.setVerticalAlignment((short)1);
		patriarchStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		patriarchStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		patriarchStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		patriarchStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		
		myFont = new MyHSSFFont(wb);
		this.BOLD_BORDER_All = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_All.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		BOLD_BORDER_All.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		BOLD_BORDER_All.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		BOLD_BORDER_All.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		BOLD_BORDER_All.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_All.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		BOLD_BORDER_All.setFont(myFont.getBOLD());
		
		this.BORDER_All = (HSSFCellStyle)wb.createCellStyle();
		BORDER_All.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		BORDER_All.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		BORDER_All.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		BORDER_All.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		BORDER_All.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		BORDER_All.setFont(myFont.getNORMAL());
		
		this.BOLD_BORDER_BOTTOM_LEFT = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_BOTTOM_LEFT.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		BOLD_BORDER_BOTTOM_LEFT.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		BOLD_BORDER_BOTTOM_LEFT.setBorderRight(XSSFCellStyle.BORDER_NONE);// 右边框
		BOLD_BORDER_BOTTOM_LEFT.setBorderTop(XSSFCellStyle.BORDER_NONE);// 上边框
		BOLD_BORDER_BOTTOM_LEFT.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_BOTTOM_LEFT.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		BOLD_BORDER_BOTTOM_LEFT.setFont(myFont.getBOLD());
		
		this.BOLD_BORDER_BOTTOM_RIGHT = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_BOTTOM_RIGHT.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		BOLD_BORDER_BOTTOM_RIGHT.setBorderLeft(XSSFCellStyle.BORDER_NONE);// 左边框
		BOLD_BORDER_BOTTOM_RIGHT.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		BOLD_BORDER_BOTTOM_RIGHT.setBorderTop(XSSFCellStyle.BORDER_NONE);// 上边框
		BOLD_BORDER_BOTTOM_RIGHT.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_BOTTOM_RIGHT.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		BOLD_BORDER_BOTTOM_RIGHT.setFont(myFont.getBOLD());
		
		this.BOLD_BORDER_BOTTOM = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_BOTTOM.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		BOLD_BORDER_BOTTOM.setBorderLeft(XSSFCellStyle.BORDER_NONE);// 左边框
		BOLD_BORDER_BOTTOM.setBorderRight(XSSFCellStyle.BORDER_NONE);// 右边框
		BOLD_BORDER_BOTTOM.setBorderTop(XSSFCellStyle.BORDER_NONE);// 上边框
		BOLD_BORDER_BOTTOM.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_BOTTOM.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		BOLD_BORDER_BOTTOM.setFont(myFont.getBOLD());
		
		this.BOLD_BORDER_TOP_RIGHT = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_TOP_RIGHT.setBorderBottom(XSSFCellStyle.BORDER_NONE);// 下边框
		BOLD_BORDER_TOP_RIGHT.setBorderLeft(XSSFCellStyle.BORDER_NONE);// 左边框
		BOLD_BORDER_TOP_RIGHT.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		BOLD_BORDER_TOP_RIGHT.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		BOLD_BORDER_TOP_RIGHT.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_TOP_RIGHT.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		BOLD_BORDER_TOP_RIGHT.setFont(myFont.getBOLD());
		
		this.BOLD_BORDER_NONE = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_NONE.setBorderBottom(XSSFCellStyle.BORDER_NONE);// 下边框
		BOLD_BORDER_NONE.setBorderLeft(XSSFCellStyle.BORDER_NONE);// 左边框
		BOLD_BORDER_NONE.setBorderRight(XSSFCellStyle.BORDER_NONE);// 右边框
		BOLD_BORDER_NONE.setBorderTop(XSSFCellStyle.BORDER_NONE);// 上边框
		BOLD_BORDER_NONE.setFont(myFont.getBOLD());
		
		this.BOLD_BORDER_NORMAL = (HSSFCellStyle)wb.createCellStyle();
		BOLD_BORDER_NORMAL.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		BOLD_BORDER_NORMAL.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		BOLD_BORDER_NORMAL.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		BOLD_BORDER_NORMAL.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		BOLD_BORDER_NORMAL.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		BOLD_BORDER_NORMAL.setFont(myFont.getNORMAL());
	}

	public HSSFCellStyle getBOLD_BORDER_All() {
		return BOLD_BORDER_All;
	}

	public void setBOLD_BORDER_All(HSSFCellStyle bOLD_BORDER_All) {
		BOLD_BORDER_All = bOLD_BORDER_All;
	}

	public HSSFCellStyle getBORDER_All() {
		return BORDER_All;
	}

	public void setBORDER_All(HSSFCellStyle bORDER_All) {
		BORDER_All = bORDER_All;
	}

	public HSSFCellStyle getBOLD_BORDER_BOTTOM_LEFT() {
		return BOLD_BORDER_BOTTOM_LEFT;
	}

	public void setBOLD_BORDER_BOTTOM_LEFT(HSSFCellStyle bOLD_BORDER_BOTTOM_LEFT) {
		BOLD_BORDER_BOTTOM_LEFT = bOLD_BORDER_BOTTOM_LEFT;
	}

	public HSSFCellStyle getBOLD_BORDER_BOTTOM_RIGHT() {
		return BOLD_BORDER_BOTTOM_RIGHT;
	}

	public void setBOLD_BORDER_BOTTOM_RIGHT(HSSFCellStyle bOLD_BORDER_BOTTOM_RIGHT) {
		BOLD_BORDER_BOTTOM_RIGHT = bOLD_BORDER_BOTTOM_RIGHT;
	}

	public HSSFCellStyle getBOLD_BORDER_BOTTOM() {
		return BOLD_BORDER_BOTTOM;
	}

	public void setBOLD_BORDER_BOTTOM(HSSFCellStyle bOLD_BORDER_BOTTOM) {
		BOLD_BORDER_BOTTOM = bOLD_BORDER_BOTTOM;
	}

	public HSSFCellStyle getBOLD_BORDER_TOP_RIGHT() {
		return BOLD_BORDER_TOP_RIGHT;
	}

	public void setBOLD_BORDER_TOP_RIGHT(HSSFCellStyle bOLD_BORDER_TOP_RIGHT) {
		BOLD_BORDER_TOP_RIGHT = bOLD_BORDER_TOP_RIGHT;
	}

	public HSSFCellStyle getBOLD_BORDER_NONE() {
		return BOLD_BORDER_NONE;
	}

	public void setBOLD_BORDER_NONE(HSSFCellStyle bOLD_BORDER_NONE) {
		BOLD_BORDER_NONE = bOLD_BORDER_NONE;
	}

	public HSSFCellStyle getPatriarchStyle() {
		return patriarchStyle;
	}

	public void setPatriarchStyle(HSSFCellStyle patriarchStyle) {
		this.patriarchStyle = patriarchStyle;
	}

	public MyHSSFFont getMyFont() {
		return myFont;
	}

	public void setMyFont(MyHSSFFont myFont) {
		this.myFont = myFont;
	}
	public HSSFCellStyle getBOLD_BORDER_NORMAL() {
		return BOLD_BORDER_NORMAL;
	}
	public void setBOLD_BORDER_NORMAL(HSSFCellStyle bOLD_BORDER_NORMAL) {
		BOLD_BORDER_NORMAL = bOLD_BORDER_NORMAL;
	}
}
