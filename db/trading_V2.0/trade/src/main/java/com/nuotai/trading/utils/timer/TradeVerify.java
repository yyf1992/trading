package com.nuotai.trading.utils.timer;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.service.TradeVerifyHeaderService;

/**
 * 审批提醒
 * @author Administrator
 *
 */
@Component
public class TradeVerify {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private TradeVerifyHeaderService tradeVerifyHeaderService;
	
	@Scheduled(cron="0 0/5 * * * ?")
	public void sendDingDingMessage(){
		TimeTask timeTask = timeTaskService.selectByCode("TRADEVERIFY");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					try {
						tradeVerifyHeaderService.sendDingDingMessageTime();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setCurrentStatus("2");
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
}
