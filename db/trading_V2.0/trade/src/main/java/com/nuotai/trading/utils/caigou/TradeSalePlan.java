package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 更改销售计划状态
 * @author Administrator
 *
 */
public class TradeSalePlan {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TradeSalePlan salePlan = new TradeSalePlan();
		try {
			salePlan.dealWith();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void dealWith() throws Exception{
		Connection tradeConn = DBUtils.getTradeConnection();
		List<Map<String,String>> dataList = getData(tradeConn);
		int length = dataList.size();
		for(Map<String,String> datalMap : dataList){
			System.out.println("剩余："+length+"条数据,总计："+dataList.size());
			String id = datalMap.get("id");
			String status = datalMap.get("status");
			if("0".equals(status)){
				//等待审批
				update(id,datalMap,0,tradeConn);
			}else if("1".equals(status)){
				//审批通过，获得已生成采购计划的数量
				String plan_code = datalMap.get("plan_code");
				String barcode = datalMap.get("barcode");
				if(getCount(plan_code,barcode,tradeConn)>0){
					//已经转化采购计划
					update(id,datalMap,1,tradeConn);
				}else{
					//没有转化采购计划
					update(id,datalMap,2,tradeConn);
				}
			}else if("2".equals(status)){
				//审批不通过
				update(id,datalMap,3,tradeConn);
			}
			length--;
		}
		DBUtils.closeConnection(tradeConn);
	}
	
	private int getCount(String plan_code,String barcode,Connection tradeConn) throws Exception{
		int count = 0;
		StringBuffer totalSql = new StringBuffer("select count(*) count ");
		totalSql.append("FROM buy_applypurchase_shop s ");
		totalSql.append("LEFT JOIN buy_applypurchase_item i ON i.id=s.item_id ");
		totalSql.append("WHERE s.plan_code='"+plan_code+"' AND i.barcode='"+barcode+"'; ");
		PreparedStatement ps = tradeConn.prepareStatement(totalSql.toString());
		ResultSet rs = ps.executeQuery();
		// 取得结果集列数
		while (rs.next()) {
			count = rs.getInt("count");
		}
		DBUtils.closeResultSet(rs);
		DBUtils.closeStatement(ps);
		return count;
	}
	
	private int getTotalNum(String plan_code,String barcode,Connection tradeConn) throws Exception{
		int num = 0;
		StringBuffer totalSql = new StringBuffer("SELECT (sum(s.confirm_sales_num)+sum(s.confirm_put_storage_num)) num ");
		totalSql.append("FROM buy_applypurchase_shop s ");
		totalSql.append("LEFT JOIN buy_applypurchase_item i ON i.id=s.item_id ");
		totalSql.append("WHERE s.plan_code='"+plan_code+"' AND i.barcode='"+barcode+"'; ");
		PreparedStatement ps = tradeConn.prepareStatement(totalSql.toString());
		ResultSet rs = ps.executeQuery();
		// 取得结果集列数
		while (rs.next()) {
			num = rs.getInt("num");
		}
		DBUtils.closeResultSet(rs);
		DBUtils.closeStatement(ps);
		return num;
	}
	
	private void update(String id, Map<String, String> datalMap, int i,Connection tradeConn) throws Exception{
		String plan_code = datalMap.get("plan_code");
		String barcode = datalMap.get("barcode");
		
		StringBuffer updateHeaderSql = new StringBuffer("update buy_sale_plan_item set ");
		updateHeaderSql.append(" start_date='"+datalMap.get("start_date")+"'");
		updateHeaderSql.append(" ,end_date='"+datalMap.get("end_date")+"'");
		updateHeaderSql.append(" ,sale_days="+datalMap.get("sale_days")+"");
		updateHeaderSql.append(" ,plan_type=0");
		
		if(i==0){
			//未审批
			updateHeaderSql.append(" ,is_confirm=0");
			updateHeaderSql.append(" ,is_next_step=0");
		}else if(i==1){
			//审批通过，已经转化采购计划
			updateHeaderSql.append(" ,is_confirm=1");
			updateHeaderSql.append(" ,is_next_step=1");
			int totalNum = getTotalNum(plan_code,barcode,tradeConn);
			updateHeaderSql.append(" ,confirm_sales_num="+totalNum);
		}else if(i==2){
			//审批通过,未转为采购计划
			updateHeaderSql.append(" ,is_confirm=1");
			updateHeaderSql.append(" ,is_next_step=0");
			updateHeaderSql.append(" ,confirm_sales_num=IFNULL(sales_num,0)+IFNULL(put_storage_num,0)");
		}else if(i==3){
			//审批不通过
			updateHeaderSql.append(" ,is_confirm=1");
			updateHeaderSql.append(" ,confirm_sales_num=0");
			updateHeaderSql.append(" ,is_next_step=1");
		}
		updateHeaderSql.append(" where id='"+id+"' ");
		PreparedStatement updatePs = tradeConn.prepareStatement(updateHeaderSql.toString());
		updatePs.executeUpdate();
	}

	private List<Map<String, String>> getData(Connection tradeConn) throws Exception{
		List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
		String selectSql = "SELECT i.id,i.barcode,h.start_date,h.end_date,h.sale_days,h.status,h.if_order,h.is_del,h.plan_code"
				+ " FROM buy_sale_plan_item i" 
				+ " LEFT JOIN buy_sale_plan_header h ON h.id=i.header_id"
				+ " WHERE i.start_date IS NULL";
		PreparedStatement ps = tradeConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		// 取得结果集列数
		while (rs.next()) {
			Map<String,String> map = new HashMap<String,String>();
			map.put("id", rs.getString("id"));
			map.put("start_date", rs.getString("start_date"));
			map.put("end_date", rs.getString("end_date"));
			map.put("sale_days", rs.getString("sale_days"));
			map.put("status", rs.getString("status"));
			map.put("if_order", rs.getString("if_order"));
			map.put("is_del", rs.getString("is_del"));
			map.put("barcode", rs.getString("barcode"));
			map.put("plan_code", rs.getString("plan_code"));
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		return dataList;
	}

}
