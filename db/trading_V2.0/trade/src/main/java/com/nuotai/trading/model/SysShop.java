package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 店铺表
 * 
 * @author "
 * @date 2017-08-07 08:59:32
 */
@Data
public class SysShop implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//店铺代码
	private String shopCode;
	//店铺名称
	private String shopName;
	//公司id
	private String companyId;
	//所属平台id
	private String platformId;
	//删除状态 0未删除 1已删除
	private Integer isDel;
	//创建时间
	private Date createDate;
	//创建人id
	private String createId;
	//创建人姓名
	private String createName;
	//修改时间
	private Date updateDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//删除时间
	private Date delDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
}
