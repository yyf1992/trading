package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyStockBacklogMapper;
import com.nuotai.trading.model.BuyStockBacklog;



@Service
@Transactional
public class BuyStockBacklogService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyStockBacklogService.class);

	@Autowired
	private BuyStockBacklogMapper buyStockBacklogMapper;
	
	public BuyStockBacklog get(String id){
		return buyStockBacklogMapper.get(id);
	}
	
	public List<BuyStockBacklog> queryList(Map<String, Object> map){
		return buyStockBacklogMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyStockBacklogMapper.queryCount(map);
	}
	
	public void add(BuyStockBacklog buyStockBacklog){
		buyStockBacklogMapper.add(buyStockBacklog);
	}
	
	public void update(BuyStockBacklog buyStockBacklog){
		buyStockBacklogMapper.update(buyStockBacklog);
	}
	
	public void delete(String id){
		buyStockBacklogMapper.delete(id);
	}
	

}
