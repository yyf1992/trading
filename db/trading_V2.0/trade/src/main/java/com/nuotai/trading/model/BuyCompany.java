package com.nuotai.trading.model;

import java.util.Date;

import lombok.Data;

@Data
public class BuyCompany {
    // 主键id
    private String id;
    // 公司代号
    private String companyCode;
    // 公司名称
    private String companyName;
    // 省
    private String province;
    // 市
    private String city;
    // 区
    private String area;
    // 详细地址
    private String detailAddress;
    // 联系人
    private String linkMan;
    // 电话区号
    private String zone;
    // 电话号
    private String telNo;
    // 传真1
    private String fax1;
    // 传真2
    private String fax2;
    // qq
    private String qq;
    // 旺旺号
    private String wangNo;
    // email
    private String email;
    // 状态0待审核1审核通过2审核驳回
    private Integer status;
    // 审核意见
    private String reason;
    // 审核时间
    private Date verifyDate;
    // 0表示启用；1表示停用
    private Integer isDel;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 注册时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人姓名
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人名称
    private String delName;
    // 删除时间
    private Date delDate;
    // 是否需要外协
    private String isNeedAssist;
}