package com.nuotai.trading.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFComment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

/**
 * 利用POI将Word转换为Html,以便在浏览器上预览
 * Created by Administrator on 2017\8\8 0008.
 */
public class PoiWordToHtml {
    public static String docToHtml(String file) {
        HttpURLConnection urlConnection = null;
        String htmlContent = "";
        try {
            URL url = new URL(file);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.connect();
            InputStream input = urlConnection.getInputStream();
            HWPFDocument wordDocument = new HWPFDocument(input);
            WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(
                    DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
            wordToHtmlConverter.setPicturesManager(new PicturesManager() {
                @Override
                public String savePicture(byte[] bytes, PictureType pictureType, String suggestedName,
                                          float widthInches, float heightInches) {
                    return suggestedName;
                }
            });
            wordToHtmlConverter.processDocument(wordDocument);
            List pics = wordDocument.getPicturesTable().getAllPictures();
            //将word中的图片放在临时目录下
            String picPath = System.getProperty("java.io.tmpdir");
            if (pics != null) {
                for (int i = 0; i < pics.size(); i++) {
                    Picture pic = (Picture) pics.get(i);
                    try {
                        pic.writeImageContent(new FileOutputStream(picPath + pic.suggestFullFileName()));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
            Document htmlDocument = wordToHtmlConverter.getDocument();
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            DOMSource domSource = new DOMSource(htmlDocument);
            StreamResult streamResult = new StreamResult(outStream);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.METHOD, "html");
            serializer.transform(domSource, streamResult);
            outStream.close();
            htmlContent = new String(outStream.toByteArray(), "utf-8");
            if (htmlContent.indexOf("<img src=\"") > 0) {
                htmlContent = htmlContent.replaceAll("<img src=\"", "<img src=\"" + picPath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return htmlContent;
    }

    public static String docxToHtml(String file) {
        HttpURLConnection urlConnection = null;
        String htmlContent = "";
        try {
            // 1) Load DOCX into XWPFDocument
            URL url = new URL(file);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.connect();
            InputStream input = urlConnection.getInputStream();
            XWPFDocument doc = new XWPFDocument(input);
            //下面这两行代码只获取文本信息，格式不保留
//            XWPFWordExtractor extractor = new XWPFWordExtractor(doc);
//            htmlContent = extractor.getText();

            // 2) Prepare XHTML options (here we set the IURIResolver to load images from a "word/media" folder)
            String picPath = System.getProperty("java.io.tmpdir");
            File imageFolderFile = new File(picPath);
            XHTMLOptions options = XHTMLOptions.create().URIResolver(new FileURIResolver(imageFolderFile));
            options.setExtractor(new FileImageExtractor(imageFolderFile));
            options.setIgnoreStylesIfUnused(false);
            options.setFragment(true);
            // 3) Convert XWPFDocument to XHTML
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            XHTMLConverter.getInstance().convert(doc, out, options);
            htmlContent = new String(out.toByteArray(), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return htmlContent;
    }

    public static void main(String[] args) {
//        String path = "http://172.16.3.69:8880/file_server/trade/upload/20170808164843351771596.doc";
        String path = "http://172.16.3.69:8880/file_server/trade/upload/20170815102151416564529.docx";
//        String path = "C:\\Users\\Administrator\\Desktop\\Java开发手册v1.00.doc";
        String a = docxToHtml(path);
        System.out.println(a);
    }
}
