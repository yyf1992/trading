package com.nuotai.trading.controller.admin;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.SysRoleUserService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.R;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 后台管理登录
 * @author wxx
 *
 */
@Controller
public class AdminLoginController extends BaseController {
	@Autowired
	private Producer producer;
	@Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private SysMenuService sysMenuService;
	
	/**
	 * 获取验证码
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping("captcha.jpg")
	public void captcha()throws ServletException, IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
	
	/**
	 * 加载登录页
	 * @return
	 */
	@RequestMapping("admin/loadLoginHtml")
	public String loadLoginHtml(){
		return "admin/login";
	}
	
	/**
	 * 登录
	 */
	@ResponseBody
	@RequestMapping(value = "admin/login", method = RequestMethod.POST)
	public R login(String username, String password)throws IOException {
		if("".equals(username)){
			return R.error("请输入帐号");
		}
		if("".equals(password)){
			return R.error("请输入密码");
		}
		if(!"admin".equals(username)){
			return R.error("没有权限登录");
		}
		try{
			Subject subject = ShiroUtils.getSubject();
			//md5加密
			password = ShiroUtils.getMD5(password, "UTF-8", 0);
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
			setSysMenuSession();
		}catch (UnknownAccountException e) {
			return R.error(e.getMessage());
		}catch (IncorrectCredentialsException e) {
			return R.error(e.getMessage());
		}catch (LockedAccountException e) {
			return R.error(e.getMessage());
		}catch (AuthenticationException e) {
			return R.error("账户验证失败");
		}
		return R.ok();
	}
	
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping("admin/adminIndexHtml")
	public String adminIndexHtml(){
		return "admin/index";
	}
	
	/**
	 * 内容首页
	 * @return
	 */
	@RequestMapping("admin/adminMainHtml")
	public String adminMainHtml(){
		return "admin/main";
	}
	
	/**
	 * 退出
	 */
	@RequestMapping(value = "admin/logout", method = RequestMethod.GET)
	public String logout() {
		ShiroUtils.logout();
		return "redirect:admin/loadLoginHtml";
	}
	
	public void setSysMenuSession(){
    	SysUser user = ShiroUtils.getUserEntity();
    	//菜单信息
    	// 权限过滤，admin是超级权限
    	Map<String, Object> map = new HashMap<String, Object>();
		// 取得登录人id
		if (!"admin".equals(user.getLoginName())) {
			map.put("loginUserId", user.getId());
			// 取得登陆人的角色信息
			List<String> roleIdList = sysRoleUserService.selectRoleIdByUserId(user.getId());
			List<String> menuIdList = sysMenuService.getMenuIdByRoleId(roleIdList);
			map.put("menuIdList", menuIdList);
		}
		// 取得权限下的顶部菜单菜单
		map.put("position", Constant.MenuPosition.TOP.getValue());
		List<SysMenu> parentMenu = sysMenuService.getMenuByMap(map);
		getSysMenu(parentMenu,map);
		ShiroUtils.setSessionAttribute(Constant.AUTHORITYSYSMENU, parentMenu);
    }
    
    /**
	 * 递归取得菜单信息
	 * @param parentMenu
	 */
	public void getSysMenu(List<SysMenu> parentMenu,Map<String, Object> map){
		if (parentMenu != null && parentMenu.size() > 0) {
			for (SysMenu menu : parentMenu) {
				map.put("position", Constant.MenuPosition.LEFT.getValue());
				map.put("parent_id", menu.getId());
				map.put("is_menu", Constant.MenuType.MENU.getValue());
				// 取得二级菜单
				List<SysMenu> childMenu = sysMenuService.getMenuByMap(map);
				if(childMenu != null && childMenu.size() > 0){
					getSysMenu(childMenu,map);
				}
				menu.setChildrenMenuList(childMenu);
			}
		}
	}
}
