package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.utils.SearchPageUtil;

public interface BuyShopProductMapper extends BaseDao<BuyShopProduct>{
    int deleteByPrimaryKey(String id);

    int insert(BuyShopProduct record);

    int insertSelective(BuyShopProduct record);

    BuyShopProduct selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyShopProduct record);
    
    int updateBySupplierId(BuyShopProduct record); 

    int updateByPrimaryKey(BuyShopProduct record);

    List<Map<String,Object>> loadBuyerProductLinks(SearchPageUtil searchPageUtil);

    List<Map<String,Object>> loadSellerProductLinks(SearchPageUtil searchPageUtil);

	List<Map<String, Object>> selectProductByMap(Map<String, Object> params);

	List<Map<String, Object>> getSupplierProductByClient(Map<String, Object> goodsMap);

    void saveProductLink(List<BuyShopProduct> list);

    void updBuyShopProductPrice(Map<String,Object> map);

    void comfirmShopProductPriceUpd(Map<String, Object> map);

    List<BuyShopProduct> queryALlList(Map<String, Object> map);
    
    List<Map<String,Object>> loadBuyerProductLinksByMap(Map<String, Object> params);

    int setPriceUpdStatus(BuyShopProduct buyShopProduct);
    
    int getProductCount(Map<String, Object> map);
    
    List<BuyShopProduct> queryListBySupplierId(Map<String, Object> map);

	int getWeightSum(BuyShopProduct pro);
}