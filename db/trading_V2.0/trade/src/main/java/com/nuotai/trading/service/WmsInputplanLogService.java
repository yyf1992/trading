package com.nuotai.trading.service;

import com.nuotai.trading.model.wms.WmsInputplanLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.WmsInputplanLogMapper;


@Service
public class WmsInputplanLogService {

    private static final Logger LOG = LoggerFactory.getLogger(WmsInputplanLogService.class);

	@Autowired
	private WmsInputplanLogMapper wmsInputplanLogMapper;
	
	public WmsInputplanLog get(String id){
		return wmsInputplanLogMapper.get(id);
	}
	
	public List<WmsInputplanLog> queryList(Map<String, Object> map){
		return wmsInputplanLogMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return wmsInputplanLogMapper.queryCount(map);
	}
	
	public void add(WmsInputplanLog wmsInputplanLog){
		wmsInputplanLogMapper.add(wmsInputplanLog);
	}
	
	public void update(WmsInputplanLog wmsInputplanLog){
		wmsInputplanLogMapper.update(wmsInputplanLog);
	}
	
	public void delete(String id){
		wmsInputplanLogMapper.delete(id);
	}

	public void deleteByDeliverNo(String deliverNo){
		wmsInputplanLogMapper.deleteByDeliverNo(deliverNo);
	}


}
