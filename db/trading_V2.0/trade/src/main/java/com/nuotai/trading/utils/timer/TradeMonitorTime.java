package com.nuotai.trading.utils.timer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;

/**
 * 监控定时任务
 * @author Administrator
 *
 */
@Component
public class TradeMonitorTime {
	@Autowired
	private TimeTaskService timeTaskService;
	
	@Scheduled(cron="0 0/5 * * * ?")
	public void monitorTimer(){
		Map<String,Object> params = new HashMap<>();
		params.put("currentStatus", "1");
		List<TimeTask> timeTaskList = timeTaskService.getListByMap(params);
		if(timeTaskList != null && timeTaskList.size() > 0){
			for(TimeTask timeTask : timeTaskList){
				System.out.println(timeTask.getLastExecute().compareTo(timeTask.getLastSynchronous()));
				if(timeTask.getLastExecute().compareTo(timeTask.getLastSynchronous())==0){
					continue;
				}
				timeTask.setCurrentStatus("0");
				timeTask.setPrepStatus("0");
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}
		}
	}
}
