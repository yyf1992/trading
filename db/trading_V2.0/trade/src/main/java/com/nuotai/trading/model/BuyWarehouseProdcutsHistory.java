package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-11-03 10:45:53
 */
@Data
public class BuyWarehouseProdcutsHistory implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//库存历史id
	private String historyId;
	//库存历史时间
	private Date historyDate;
	//公司Id
	private String companyId;
	//仓库Id
	private String warehouseId;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//店铺Id
	private String shopId;
	//店铺Code
	private String shopCode;
	//店铺名称
	private String shopName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//颜色
	private String color;
	//条形码
	private String barcode;
	//成本总额
	private BigDecimal totalCost;
	//可用库存（所有批次可用库存之和）
	private Integer usableStock;
	//锁定库存（所有批次锁定库存之和）
	private Integer lockStock;
	//库存上限
	private Integer maxStock;
	//库存下限
	private Integer minStock;
	//标准库存
	private Integer standardStock;
	//总库存（所有批次实际库存之和）
	private Integer stocks;
	//状态：0，短缺 1，正常
	private String status;
	//库存积压预警天数
	private Integer backlogWarning;
	//备注
	private String remark;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
	
	private List<BuyWarehouseLog> buyWarehouseLog;//商品出入库明细
	
	private Integer outNum;//出库总数量 0
	private BigDecimal outPrice;//出库总成本 0
	private Integer sellerManualNum;//手工出库数量 0 1 
	private BigDecimal sellerManualPrice;//手工出库成本 0 1
	private Integer sellerNum;//互通出库数量 0 0
	private BigDecimal sellerPrice;//互通出库成本 0 0
	private Integer transferOutNum;//调货出库数量
	private BigDecimal transferOutPrice;//调货出库成本
	private Integer returnOutNum;//退货出库数量
	private BigDecimal returnOutPrice;//退货出库成本
	
	
	private Integer inNum;//入库总数量 1
	private BigDecimal inPrice;//入库总成本 1
	private Integer buyNum;//互通入库数量 1 0 
	private BigDecimal buyPrice;//互通入库成本 1 0
	private Integer buyManualNum;//手工入库数量  1 1
	private BigDecimal buyManualPrice;//手工入库成本 1 1
	private Integer transferInNum;//调货入库数量
	private BigDecimal transferInPrice;//调货入库成本
	private Integer returnInNum;//退货入库数量
	private BigDecimal returnInPrice;//退货入库成本
}
