package com.nuotai.trading.dao;

import java.util.List;

import com.nuotai.trading.model.SysVerifyPocess;

/**
 * 
 * 
 * @author "
 * @date 2017-08-21 11:48:57
 */
public interface SysVerifyPocessMapper extends BaseDao<SysVerifyPocess> {

	List<SysVerifyPocess> getListByHeaderId(String headerId);
	int deleteByHeaderId(String headerId);
}
