package com.nuotai.trading.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.utils.PageAdmin;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 公司管理
 * @author dxl
 *
 */
@Controller
@RequestMapping("admin/adminCompany")
public class AdminCompanyController extends BaseController {
	@Autowired
    private BuyCompanyService companyService;
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private BuyCompanyMapper companyMapper;
	
	/**
	 * 加载列表页面
	 * @return
	 */
	@RequestMapping(value = "/loadCompanyList.html", method = RequestMethod.GET)
	public String loadUserList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		searchPageUtil.setObject(params);
		List<BuyCompany> companyList = companyService.selectByPage(searchPageUtil);
		searchPageUtil.getPageAdmin().setList(companyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "admin/company/companyList";
	}
	
	/**
	 * 加载审核页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/verifyCompany.html")
	public String verifyCompany(String id) throws Exception {
		BuyCompany company = companyService.selectByPrimaryKey(id);
		model.addAttribute("company", company);
		return "admin/company/verifyCompany";
	}
	
	/**
	 * 加载详情页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadDetails.html")
	public String loadDetails(String id) throws Exception {
		BuyCompany company = companyService.selectByPrimaryKey(id);
		model.addAttribute("company", company);
		// 附件表信息
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("relatedId", id);
		map.put("type", "1");// 营业执照
		List<BuyAttachment> a1List = attachmentService.selectByMap(map);
		model.addAttribute("a1List", a1List);
		map.put("type", "2");// 开户许可证
		List<BuyAttachment> a2List = attachmentService.selectByMap(map);
		model.addAttribute("a2List", a2List);
		return "admin/company/loadDetails";
	}
	
	/**
	 * 审核保存
	 */
	@RequestMapping(value = "/saveVerifyCompany", method = RequestMethod.POST)
	@ResponseBody
	public String saveVerifyCompany(BuyCompany company) {
		JSONObject json = new JSONObject();
		try{
			companyService.saveVerifyCompany(company);
			json.put("success", true);
			json.put("msg", "成功！");
		}catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 停用公司账号
	 * 
	 * @param ids
	 */
	@RequestMapping(value = "/updateCompanyIsDel.html", method = RequestMethod.GET)
	@ResponseBody
	public String updateCompanyIsDel(String id,String isDel) {
		JSONObject json = new JSONObject();
		try {
			companyService.updateCompanyIsDel(id,isDel);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
	
	/**
	 * 设置是否需要外协
	 * 
	 * @param ids
	 */
	@RequestMapping(value = "/updateCompanyIsNeedAssist.html", method = RequestMethod.GET)
	@ResponseBody
	public String updateCompanyIsNeedAssist(String id,String status) {
		JSONObject json = new JSONObject();
		try {
			BuyCompany company = new BuyCompany();
			company.setId(id);
			company.setIsNeedAssist(status);
			companyMapper.updateByPrimaryKeySelective(company);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
	
	/**
	 * 加载外协排单页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getEditCompany.html", method = RequestMethod.GET)
	public String getEditCompany(String id) {
		BuyCompany company = companyService.selectByPrimaryKey(id);
		model.addAttribute("editCompany", company);
		return "admin/company/editCompany";
	}
	
	/**
	 * 加载设置角色页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/instalCompanyRole.html", method = RequestMethod.GET)
	public String instalCompanyRole(String id) {
		BuyCompany company = companyService.selectByPrimaryKey(id);
		model.addAttribute("company", company);
		return "admin/company/instalCompanyRole";
	}
	
}
