package com.nuotai.trading.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.utils.Constant;

/**
 * 管理后台-菜单管理
 * @author wxx
 *
 */
@Controller
@RequestMapping("admin/sysMenu")
public class AdminSysMenuController extends BaseController {
	@Autowired
	private SysMenuService sysMenuService;
	
	/**
	 * 系统菜单一览
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadMenuList.html", method = RequestMethod.GET)
	public String loadSysMenu(@RequestParam Map<String,Object> params){
		/*if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		searchPageUtil.setObject(params);*/
		// 取得数据
		List<SysMenu> sysMenuList = sysMenuService.getMenuByMap(params);
		model.addAttribute("object", params);
		model.addAttribute("sysMenuList", sysMenuList);
		return "admin/sysmenu/sysMenuList";
	}
	
	/**
	 * 加载新增页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadAddMenu.html", method = RequestMethod.GET)
	public String loadAddMenu() throws Exception {
		// 取得菜单
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("is_menu", Constant.MenuType.MENU);
		List<SysMenu> parentMenuList = sysMenuService.getMenuByMap(map);
		if (parentMenuList != null && parentMenuList.size() > 0) {
			for (int i = 0; i < parentMenuList.size(); i++) {
				SysMenu menu = parentMenuList.get(i);
				String sortorder = menu.getSortOrder() == null ? "" : menu.getSortOrder();
				int level = sortorder.split("_").length;
				menu.setLevel(level);
			}
		}
		model.addAttribute("parentMenuList", parentMenuList);
		return "admin/sysmenu/addSysMenu";
	}

	/**
	 * 加载修改页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadUpdateMenu.html", method = RequestMethod.GET)
	public String loadUpdateMenu(String id) throws Exception {
		SysMenu sysMenu = sysMenuService.selectByPrimaryKey(id);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("is_menu", Constant.MenuType.MENU);
		List<SysMenu> parentMenuList = sysMenuService.getMenuByMap(map);
		if (parentMenuList != null && parentMenuList.size() > 0) {
			for (int i = 0; i < parentMenuList.size(); i++) {
				SysMenu menu = parentMenuList.get(i);
				String sortorder = menu.getSortOrder() == null ? "" : menu.getSortOrder();
				int level = sortorder.split("_").length;
				menu.setLevel(level);
			}
		}
		model.addAttribute("parentMenuList", parentMenuList);
		model.addAttribute("sysMenu", sysMenu);
		return "admin/sysmenu/updateSysMenu";
	}

	/**
	 * 新增
	 */
	@RequestMapping(value = "/saveAdd", method = RequestMethod.POST)
	@ResponseBody
	public String saveAdd(SysMenu sysMenu) {
		JSONObject json = new JSONObject();
		// 判断名称是否占用
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("name", sysMenu.getName());
		map2.put("is_menu", sysMenu.getIsMenu());
		map2.put("parent_id", sysMenu.getParentId());
		List<SysMenu> menuList = sysMenuService.getMenuByMap(map2);
		if (menuList != null && menuList.size() > 0) {
			// 名字存在
			json.put("success", false);
			if (sysMenu.getIsMenu() == 0) {
				json.put("msg", "菜单已存在！");
			} else {
				json.put("msg", "按钮已存在！");
			}
			return json.toString();
		}
		int result = 0;
		result = sysMenuService.saveAdd(sysMenu);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "成功！");
		} else {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}

	/**
	 * 修改
	 */
	@RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdate(SysMenu sysMenu) {
		JSONObject json = new JSONObject();
		// 判断名称是否占用
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("name", sysMenu.getName());
		map2.put("is_menu", sysMenu.getIsMenu());
		map2.put("parent_id", sysMenu.getParentId());
		List<SysMenu> menuList = sysMenuService.getMenuByMap(map2);
		if (menuList != null && menuList.size() > 0) {
			SysMenu menu = menuList.get(0);
			if (!menu.getId().equals(sysMenu.getId())) {
				// 名字存在
				json.put("success", false);
				if (sysMenu.getIsMenu() == 0) {
					json.put("msg", "菜单已存在！");
				} else {
					json.put("msg", "按钮已存在！");
				}
				return json.toString();
			}
		}
		int result = 0;
		result = sysMenuService.saveUpdate(sysMenu);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "成功！");
		} else {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	@RequestMapping(value = "/deleteSysMenu.html", method = RequestMethod.GET)
	@ResponseBody
	public String deleteSysMenu(String ids) {
		JSONObject json = new JSONObject();
		try {
			sysMenuService.deleteSysMenu(ids);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}
}
