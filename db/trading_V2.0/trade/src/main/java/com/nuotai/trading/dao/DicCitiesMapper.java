package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.DicCities;

public interface DicCitiesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DicCities record);

    int insertSelective(DicCities record);

    DicCities selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DicCities record);

    int updateByPrimaryKey(DicCities record);
    
    List<DicCities> selectByMap(Map<String, Object> map);
    
    DicCities getCityById(String cityId);
}