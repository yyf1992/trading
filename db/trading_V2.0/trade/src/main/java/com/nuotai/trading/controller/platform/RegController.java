package com.nuotai.trading.controller.platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.DicAreas;
import com.nuotai.trading.model.DicCities;
import com.nuotai.trading.model.DicProvinces;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.DicAreasService;
import com.nuotai.trading.service.DicCitiesService;
import com.nuotai.trading.service.DicProvincesService;
import com.nuotai.trading.service.SysUserService;

/**
 * 注册
 * @author dxl
 *
 */
@Controller
@RequestMapping("platform/reg")
public class RegController extends BaseController{
	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private DicProvincesService dicProvincesService;
	@Autowired
	private DicCitiesService dicCitiesService;
	@Autowired
	private DicAreasService dicAreasService;
	@Autowired
	private BuyCompanyService buyCompanyService;
	
	/**
	 * 跳转到注册页面
	 * @author:     dxl
	 * @return
	 */
	@RequestMapping("/toRegisterPage")
	public String toRegisterPage(){
		return "platform/register";
		
	}
	
	/**
	 * 验证用户手机号是否已经存在
	 * @author:     dxl
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "checkUserPhone",method = RequestMethod.POST)
	@ResponseBody
	public String checkUserPhone(String phone){
		SysUser userByPhone = sysUserService.queryByLoginName(phone);
		JSONObject json = new JSONObject();
		if(userByPhone != null){
			BuyCompany company = buyCompanyService.selectByPrimaryKey(userByPhone.getCompanyId());
			if(company.getStatus() != 2){// 审核未通过允许重新住测
				json.put("result", true);
			}
		}
		return json.toString();
	}
	
	/**
	 * 获取省份数据
	 * @return
	 */
	@RequestMapping(value = "loadProvince",method = RequestMethod.POST)
	@ResponseBody
	public String loadProvince(){
		// 省份列表
        List<DicProvinces> provinceList = dicProvincesService.selectByMap(null);
		return JSON.toJSONString(provinceList);
	}
	
	/**
	 * 根据省份id加载城市列表
	 * @param provinceId
	 * @return
	 */
	@RequestMapping(value = "loadCityByProvinceId",method = RequestMethod.POST)
	@ResponseBody
	public String loadCityByProvinceId(String provinceId){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("provinceid", provinceId);
		List<DicCities> citiesList = dicCitiesService.selectByMap(map);
		Map<String,Object> mapPare = new HashMap<String, Object>();
		mapPare.put("citiesList", citiesList);
		return JSON.toJSONString(mapPare);
	}
	
	/**
	 * 根据城市id加载区列表
	 * @param cityId
	 * @return
	 */
	@RequestMapping(value = "loadAreaByCityId",method = RequestMethod.POST)
	@ResponseBody
	public String loadAreaByCityId(String cityId){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cityid", cityId);
		List<DicAreas> areasList = dicAreasService.selectByMap(map);
		Map<String,Object> mapPare = new HashMap<String, Object>();
		mapPare.put("areasList", areasList);
		return JSON.toJSONString(mapPare);
	}
	
	/**
	 * 提交注册
	 * @param user
	 * @param company
	 * @return
	 */
	@RequestMapping(value = "/subRegister", method = RequestMethod.POST)
	@ResponseBody
	public String subRegister(SysUser user,BuyCompany company,@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try{
			buyCompanyService.subRegister(user,company,map);
			json.put("success", true);
		}catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 跳转到软件服务协议页面
	 * @author:     dxl
	 * @return
	 */
	@RequestMapping("agreement")
	public String agreement(HttpServletRequest request){
		return "platform/service_agreement";
	}
	
	/**
	 * 验证密码是否和原密码一样
	 * @author:     dxl
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "checkPassword",method = RequestMethod.POST)
	@ResponseBody
	public String checkPassword(String sysUserId,String password){
		JSONObject json = sysUserService.checkPassword(sysUserId, password);
		return json.toString();
	}
}
