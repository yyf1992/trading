package com.nuotai.trading.controller.platform;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 买卖系统共通控制器
 * @author wxx
 *
 */
@Controller
@RequestMapping("platform/common")
public class CommonController extends BaseController {
	@Autowired
	private BuySupplierFriendService buySupplierFriendService;
	@Autowired
	private BuyUnitService buyUnitService;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	@Autowired
	private BuyShopProductService buyShopProductService;
	@Autowired
    private SysMenuService sysMenuService;

	/**
	 * 我是买家首页
	 * @return
	 */
	@RequestMapping("buyerIndexHtml")
	public String BuyerIndexHtml(){
		return "platform/buyer/buyerIndex";
	}

	/**
	 * 我是卖家首页
	 * @return
	 */
	@RequestMapping("sellersIndexHtml")
	public String SellersIndexHtml(){
		return "platform/sellers/sellersIndex";
	}

	/**
	 * 基础资料首页
	 * @return
	 */
	@RequestMapping("baseInfoIndexHtml")
	public String BaseInfoIndexHtml(){
		return "platform/baseInfo/baseInfoIndex";
	}

	/**
	 * 顶部菜单点击跳转设置session
	 * @param menuId
	 * @return
	 */
	@RequestMapping("titleMenuClick")
	@ResponseBody
	public void titleMenuClick(String menuId){
		ShiroUtils.setSessionAttribute(Constant.MENU_ID, menuId);
	}
	
	/**
	 * 设置菜单
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("loadMenu")
	@ResponseBody
	public void loadMenu(){
		List<SysMenu> parentMenu = (List<SysMenu>)ShiroUtils.getSessionAttribute(Constant.AUTHORITYSYSMENU);
		getSysMenu(parentMenu);
		ShiroUtils.setSessionAttribute(Constant.AUTHORITYSYSMENU, parentMenu);
	}
	

	/**
	 * 获得供应商下拉共通
	 * @return
	 */
	@RequestMapping("getSupplierSelect")
	@ResponseBody
	public String getSupplierSelect(@RequestParam Map<String,Object> params){
		params.put("buyersId", ShiroUtils.getCompId());
		List<BuySupplierFriend> supplierFriendList = buySupplierFriendService.selectByMap(params);
		return JSONObject.toJSONString(supplierFriendList);
	}

	/**
	 * 获得采购商下拉共通
	 * @return
	 */
	@RequestMapping("getBuyerSelect")
	@ResponseBody
	public String getBuyerSelect(@RequestParam Map<String,Object> params){
		params.put("sellerId", ShiroUtils.getCompId());
		List<BuySupplierFriend> buyerFriendList = buySupplierFriendService.selectByMap(params);
		return JSONObject.toJSONString(buyerFriendList);
	}
	/**
	 * 单位下拉共通
	 * @param params
	 * @return
	 */
	@RequestMapping("getUnitSelect")
	@ResponseBody
	public String getUnitSelect(@RequestParam Map<String,Object> params){
		List<BuyUnit> unitList = buyUnitService.selectByMap(params);
		return JSONObject.toJSONString(unitList);
	}

	/**
	 * 仓库下拉
	 * @param params
	 * @return
	 */
	@RequestMapping("getWarehouseSelect")
	@ResponseBody
	public String getWarehouseSelect(@RequestParam Map<String,Object> params){
		List<TBusinessWharea> unitList = tBusinessWhareaService.getCompanyId();
		return JSONObject.toJSONString(unitList);
	}

	/**
	 * 商品查询共通
	 * @param params
	 * @return
	 */
	@RequestMapping("selectGoods")
	@ResponseBody
	public String selectGoods(@RequestParam Map<String,Object> params){
		params.put("companyID", ShiroUtils.getCompId());
		List<Map<String,Object>> productList = buyShopProductService.selectProductByMap(params);
		return JSONObject.toJSONString(productList);
	}
	
	/**
	 * 递归取得菜单信息
	 * @param parentMenu
	 */
	public void getSysMenu(List<SysMenu> parentMenu){
		if (parentMenu != null && parentMenu.size() > 0) {
			for (SysMenu menu : parentMenu) {
				// 取得二级菜单
				List<SysMenu> childMenu = sysMenuService.tradeLogin(Constant.MenuPosition.LEFT.getValue(), menu.getId());
				if(childMenu != null && childMenu.size() > 0){
					getSysMenu(childMenu);
				}
				menu.setChildrenMenuList(childMenu);
			}
		}
	}
	
	/**
	 * 按钮权限
	 * @param menuId
	 */
	@RequestMapping("saveButtonSession")
	@ResponseBody
	public void saveButtonSession(String menuId,@RequestParam Map<String,Object> params){
		List<SysMenu> buttonList = sysMenuService.getButtonList(menuId);
		ShiroUtils.setSessionAttribute("buttonList", buttonList);
		ShiroUtils.setSessionAttribute("form", params);
	}
	
	/**
	 * 获得按钮
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("getButtonList")
	@ResponseBody
	public String getButtonList(){
		List<SysMenu> buttonList = (List<SysMenu>)ShiroUtils.getSessionAttribute("buttonList");
		if(buttonList != null && buttonList.size() > 0){
			return JSONArray.fromObject(buttonList).toString();
		}
		return "[]";
	}
}
