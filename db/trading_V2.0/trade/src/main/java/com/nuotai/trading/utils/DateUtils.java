package com.nuotai.trading.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.alibaba.druid.util.StringUtils;

/**
 * 日期处理
 *
 * @author liujie
 */
public class DateUtils {
	/** 时间格式(yyyy-MM-dd) */
	public final static String DATE_PATTERN = "yyyy-MM-dd";
	/** 时间格式(yyyy-MM-dd HH:mm:ss) */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	public static String format(Date date) {
        return format(date, DATE_PATTERN);
    }

    public static String format(Date date, String pattern) {
        if(date != null){
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }
    
    public static Date parse(String dateStr) {
        return parse(dateStr,DATE_PATTERN);
    }
    
    public static Date parse(String dateStr, String pattern) {
        if(!StringUtils.isEmpty(dateStr)){
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            try {
				return df.parse(dateStr);
			} catch (ParseException e) {
				return null;
			}
        }
        return null;
    }
    
    /**获得周一的日期 */
    public static Date getMonday(){
    	Calendar calendar = Calendar.getInstance(Locale.CHINA);
    	calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
    	return calendar.getTime();
    }
    
    /**获得本周的日期 */
    public static Date getWeekDate(int n){
    	Date monday = getMonday();
    	Calendar calendar = Calendar.getInstance(Locale.CHINA);
    	calendar.setTime(monday);
    	int day = calendar.get(Calendar.DATE);
    	calendar.set(Calendar.DATE,day+n);
    	return calendar.getTime();
    }
    
    /**
     * 比较两个日期大小
     * @param DATE1
     * @param DATE2
     * @return 1-大于;0-等于;-1小于
     */
    public static int compareDate(Date DATE1, Date DATE2) {
    	Date dt1 = parse(format(DATE1));
    	Date dt2 = parse(format(DATE2));
        if (dt1.getTime() > dt2.getTime()) {
            return 1;
        } else if (dt1.getTime() < dt2.getTime()) {
            System.out.println("dt1在dt2后");
            return -1;
        } else if (dt1.getTime() == dt2.getTime()) {
            return 0;
        }
        return 0;
    }
}
