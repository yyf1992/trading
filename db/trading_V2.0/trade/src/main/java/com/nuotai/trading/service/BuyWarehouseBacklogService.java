package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseBacklogMapper;
import com.nuotai.trading.model.BuyWarehouseBacklog;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyWarehouseBacklogService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseBacklogService.class);

	@Autowired
	private BuyWarehouseBacklogMapper buyWarehouseBacklogMapper;
	
	public BuyWarehouseBacklog get(String id){
		return buyWarehouseBacklogMapper.get(id);
	}
	
	public List<BuyWarehouseBacklog> queryList(Map<String, Object> map){
		return buyWarehouseBacklogMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseBacklogMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseBacklog buyWarehouseBacklog){
		buyWarehouseBacklogMapper.add(buyWarehouseBacklog);
	}
	
	public void update(BuyWarehouseBacklog buyWarehouseBacklog){
		buyWarehouseBacklogMapper.update(buyWarehouseBacklog);
	}
	
	public void delete(String id){
		buyWarehouseBacklogMapper.delete(id);
	}
	
	public SearchPageUtil selectBacklogByPage(SearchPageUtil searchPageUtil) {
		
		List<BuyWarehouseBacklog> buyWarehouseBacklogList = buyWarehouseBacklogMapper.selectBacklogByPage(searchPageUtil);
		
		searchPageUtil.getPage().setList(buyWarehouseBacklogList);

		return searchPageUtil;
	}

}
