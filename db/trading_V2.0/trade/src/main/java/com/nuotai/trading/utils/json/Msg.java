package com.nuotai.trading.utils.json;

/**
 * 
 * @author Administrator
 *
 */
public class Msg extends  JsonModel{
	private boolean flag;
	private String msg;
	private Object res;

	public Msg() {
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Msg(boolean flag, Object res) {
		super();
		this.flag = flag;
		this.res = res;
	}
	public Msg(boolean flag) {
		super();
		this.flag = flag;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public Object getRes() {
		return res;
	}
	public void setRes(Object res) {
		this.res = res;
	}
}
