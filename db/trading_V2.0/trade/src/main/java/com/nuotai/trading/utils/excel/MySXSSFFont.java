package com.nuotai.trading.utils.excel;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFFont;

@SuppressWarnings("deprecation")
public class MySXSSFFont {
	//加粗字体
	private XSSFFont BOLD;
	//正常字体
	private XSSFFont NORMAL;
	
	public MySXSSFFont(SXSSFWorkbook wb){
		this.BOLD = (XSSFFont) wb.createFont();
		BOLD.setFontHeightInPoints((short) 12);
		BOLD.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		
		this.NORMAL = (XSSFFont) wb.createFont();
		NORMAL.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
	}

	public XSSFFont getBOLD() {
		return BOLD;
	}
	public void setBOLD(XSSFFont bOLD) {
		BOLD = bOLD;
	}
	public XSSFFont getNORMAL() {
		return NORMAL;
	}
	public void setNORMAL(XSSFFont nORMAL) {
		NORMAL = nORMAL;
	}
}
