package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.TradeRoleMapper;
import com.nuotai.trading.dao.TradeRoleUserMapper;
import com.nuotai.trading.model.TradeRole;
import com.nuotai.trading.model.TradeRoleUser;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class TradeRoleUserService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeRoleUserService.class);

	@Autowired
	private TradeRoleUserMapper tradeRoleUserMapper;
	@Autowired
	private TradeRoleMapper tradeRoleMapper;
	
	public TradeRoleUser get(String id){
		return tradeRoleUserMapper.get(id);
	}
	
	public List<TradeRoleUser> queryList(Map<String, Object> map){
		return tradeRoleUserMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeRoleUserMapper.queryCount(map);
	}
	
	public void add(TradeRoleUser tradeRoleUser){
		tradeRoleUserMapper.add(tradeRoleUser);
	}
	
	public void update(TradeRoleUser tradeRoleUser){
		tradeRoleUserMapper.update(tradeRoleUser);
	}
	
	public void delete(String id){
		tradeRoleUserMapper.delete(id);
	}
	
	/**
	 * 获得左侧没有的角色数据
	 * @param model
	 * @param userId
	 */
	public List<TradeRole> getLeftNotRole(String userId) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("notUserId", userId);
		//获得已有的角色
		List<TradeRole> tradeRoleList = tradeRoleMapper.getRoleByMap(params);
		return tradeRoleList;
	}
	
	/**
	 * 获得右侧有的角色数据
	 * @param model
	 * @param userId
	 */
	public List<TradeRole> getRightRole(String userId) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("userId", userId);
		//获得已有的角色
		List<TradeRole> tradeRoleList = tradeRoleMapper.getRoleByMap(params);
		return tradeRoleList;
	}

	/**
	 * 人员设置角色
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject addUserRole(String userId, String roleIdStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(roleIdStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "角色信息不能为空");
			return json;
		}
		String[] roleArray = roleIdStr.split(",");
		for(String roleId : roleArray){
			TradeRoleUser roleUser = new TradeRoleUser();
			roleUser.setId(ShiroUtils.getUid());
			roleUser.setCompanyId(ShiroUtils.getCompId());
			roleUser.setRoleId(roleId);
			roleUser.setUserId(userId);
			roleUser.setCreateTime(new Date());
			tradeRoleUserMapper.add(roleUser);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}
	
	/**
	 * 人员删除角色
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject deleteUserRole(String userId, String roleIdStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(roleIdStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "角色信息不能为空");
			return json;
		}
		String[] roleArray = roleIdStr.split(",");
		for(String roleId : roleArray){
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("roleId", roleId);
			params.put("userId", userId);
			tradeRoleUserMapper.deleteByRoleIdUserId(params);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}

	public Map<String,String> selectRoleCode(Map<String, Object> map) {
		return tradeRoleUserMapper.selectRoleCode(map);
	}
}
