package com.nuotai.trading.controller.common.base;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.AbstractController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.service.SysApprovalService;
import com.nuotai.trading.utils.json.Code;
import com.nuotai.trading.utils.json.LayuiTableData;
import com.nuotai.trading.utils.json.Msg;
import com.nuotai.trading.utils.json.Response;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * Created by wxx on 2017/6/30.
 */
public class BaseController extends AbstractController{
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected Model model;
    @Autowired
	private SysApprovalService sysApprovalService;

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest req, HttpServletResponse res, Model model)throws IOException {
    	req.setCharacterEncoding("utf-8");
        res.setCharacterEncoding("utf-8");
        res.setContentType("text/html; charset=utf-8");
        this.request = req;
        this.response = res;
        this.model = model;
    }
    
    /**
     * 监听新增
     * @param params
     * @param controller
     */
    protected void insert(Map<String,Object> params, IService controller) {
    	Msg m = new Msg();
        Response res = new Response();
        //对应菜单判空
        boolean menuNameFlag = !params.containsKey("menuName")
    		||params.get("menuName")==null
    		||"".equals(params.get("menuName").toString());
        if(menuNameFlag){
        	res.setCode(Code.code_menu);
        	res.setMsg("对应菜单名称不能为空,请联系开发人员处理");
        	m.setFlag(state_success);
        	m.setRes(res);
        	out2html(response, m.toJsonString());
        	return;
        }
        //根据菜单取得对应的菜单id
        String menuName = params.get("menuName").toString();
        SysMenu sysMenu = sysApprovalService.getMenuByMenuNameOrId(menuName);
        if(sysMenu == null || sysMenu.getId() == null){
        	res.setCode(Code.code_menu);
        	res.setMsg("对应菜单名称不正确,请联系开发人员处理");
        	m.setFlag(state_success);
        	m.setRes(res);
        	out2html(response, m.toJsonString());
        	return;
        }
        try {
	        //判断人员是否需要审批
	        /*boolean userVerify = sysApprovalService.judgmentUserVerify(sysMenu.getId());
	        if(!userVerify){
	        	//不需要审批
	    		res.setCode(Code.not_need_approval);
	    		res.setMsg("不需要审批");
	    		JSONObject json = (JSONObject) controller.init(params);
	    		res.setData(json);
	    		m.setFlag(state_success);
	        	m.setRes(res);
	        	out2html(response, m.toJsonString());
	        	return;
	        }*/
        	//获得审批流程设置
        	SysVerifyHeader verifyheader = sysApprovalService.getApprovalByMenuId(sysMenu.getId());
        	if(verifyheader == null || verifyheader.getId() == null){
        		//不需要审批
        		res.setCode(Code.not_need_approval);
        		res.setMsg("不需要审批");
        		JSONObject json = (JSONObject) controller.init(params);
        		sysApprovalService.addAutomaticNot(json,sysMenu.getName());
        		res.setData(json);
        		m.setFlag(state_success);
            	m.setRes(res);
            	out2html(response, m.toJsonString());
            	return;
        	}else{
        		//需要审批
        		if("0".equals(verifyheader.getVerifyType())){
        			//固定审批流程
        			JSONObject json = (JSONObject) controller.init(params);
        			if(!json.isEmpty()){
        				sysApprovalService.addFixed(verifyheader,json);
        				res.setCode(Code.code_success);
        	        	res.setMsg("成功");
        	        	res.setData(json.containsKey("idList")?json.getString("idList"):"[]");
        	        	m.setFlag(state_success);
        	        	m.setRes(res);
        	        	out2html(response, m.toJsonString());
        	        	return;
        			}else{
        				res.setCode(Code.code_error);
        	        	res.setMsg("对应保存方法返回数据不正确,请联系开发人员处理");
        	        	res.setData(params);
        	        	m.setFlag(state_success);
        	        	m.setRes(res);
        	        	out2html(response, m.toJsonString());
        	        	return;
        			}
        		}else if("1".equals(verifyheader.getVerifyType())){
        			//自动审批流程
        			boolean approvalFlag = params.containsKey("approval_staff_automatic")
    					&& params.get("approval_staff_automatic")!=null
    					&& !"".equals(params.get("approval_staff_automatic"));
        			if(approvalFlag){
        				String approvalUsersStr = params.get("approval_staff_automatic").toString();
        				JSONObject json = (JSONObject) controller.init(params);
        				sysApprovalService.addAutomatic(verifyheader,json,approvalUsersStr);
        				res.setCode(Code.code_success);
        	        	res.setMsg("成功");
        	        	res.setData(json.containsKey("idList")?json.getString("idList"):"[]");
        	        	m.setFlag(state_success);
        	        	m.setRes(res);
        	        	out2html(response, m.toJsonString());
        	        	return;
        			}else{
        				res.setCode(Code.code_install_approval);
        				res.setMsg("需要选择审批人员");
        				res.setData(params);
        				m.setFlag(state_success);
        				m.setRes(res);
        				out2html(response, m.toJsonString());
        				return;
        			}
        		}
        	}
            m.setFlag(state_success);
            res.setCode(Code.code_success);
            m.setRes(res);
        } catch (ServiceException e) {
        	res.setCode(e.getCode());
        	res.setMsg(e.getLocalizedMessage());
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
        }catch (Exception e) {
	        String emsg = e.getLocalizedMessage();
	        e.printStackTrace();
	        m = new Msg(state_error);
	        m.setMsg(emsg);
        }
        out2html(response, m.toJsonString());
    }
    
	/**
     * 监听修改
     * @param params
     * @param controller
     */
    protected void update(String id,Map<String,Object> params, IService controller) {
    	Msg m = new Msg();
        Response res = new Response();
        params.put("id", id);
        try {
			//更新的时候判断是否审批，no的话不用走monitorUpdate方法
			/*String checkApprove = String.valueOf(params.get("checkApprove"));
			if(!"no".equalsIgnoreCase(checkApprove)){
				//判断审批数据
				sysApprovalService.monitorUpdate(id);
			}*/
        	res.setData(controller.init(params));
            m.setFlag(state_success);
            res.setCode(Code.code_success);
            m.setRes(res);
        } catch (ServiceException e) {
        	res.setCode(e.getCode());
        	res.setMsg(e.getLocalizedMessage());
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
        }catch (Exception e) {
	        String emsg = e.getLocalizedMessage();
	        e.printStackTrace();
	        m = new Msg(state_error);
	        m.setMsg(emsg);
        }
        out2html(response, m.toJsonString());
    }
    
    protected void updateFuKuan(String id,Map<String,Object> params, IService controller) {
    	Msg m = new Msg();
        Response res = new Response();
        params.put("id", id);
        try {
			//更新的时候判断是否审批，no的话不用走monitorUpdate方法
			String checkApprove = String.valueOf(params.get("checkApprove"));
			if(!"no".equalsIgnoreCase(checkApprove)){
				//判断审批数据
				sysApprovalService.monitorUpdate(id);
			}
        	res.setData(controller.init(params));
            m.setFlag(state_success);
            res.setCode(Code.code_success);
            m.setRes(res);
        } catch (ServiceException e) {
        	res.setCode(e.getCode());
        	res.setMsg(e.getLocalizedMessage());
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
        }catch (Exception e) {
	        String emsg = e.getLocalizedMessage();
	        e.printStackTrace();
	        m = new Msg(state_error);
	        m.setMsg(emsg);
        }
        out2html(response, m.toJsonString());
    }
    
    /**
     * 监听修改销售计划
     * @param params
     * @param controller
     */
    protected void updatePurchase(String id,Map<String,Object> params, IService controller) {
    	Msg m = new Msg();
        Response res = new Response();
        params.put("id", id);
        try {
        		//判断审批数据
        	sysApprovalService.monitorUpdatePurchase(id);
        	res.setData(controller.init(params));
            m.setFlag(state_success);
            res.setCode(Code.code_success);
            m.setRes(res);
        } catch (ServiceException e) {
        	res.setCode(e.getCode());
        	res.setMsg(e.getLocalizedMessage());
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
        }catch (Exception e) {
	        String emsg = e.getLocalizedMessage();
	        e.printStackTrace();
	        m = new Msg(state_error);
	        m.setMsg(emsg);
        }
        out2html(response, m.toJsonString());
    }
    
    /**
     * 监听删除
     * @param params
     * @param controller
     */
    protected void delete(String id,Map<String,Object> params, IService controller) {
    	Msg m = new Msg();
        Response res = new Response();
        params.put("id", id);
        try {
        	//判断审批数据
        	TradeVerifyHeader header = sysApprovalService.monitorDelete(id);
        	boolean result = (boolean) controller.init(params);
        	if(result){
        		//删除成功,撤销申请
        		sysApprovalService.repealVerify(header);
        	}
        	res.setData(controller.init(params));
            m.setFlag(state_success);
            res.setCode(Code.code_success);
            m.setRes(res);
        } catch (ServiceException e) {
        	res.setCode(e.getCode());
        	res.setMsg(e.getLocalizedMessage());
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
        }catch (Exception e) {
	        String emsg = e.getLocalizedMessage();
	        e.printStackTrace();
	        m = new Msg(state_error);
	        m.setMsg(emsg);
        }
        out2html(response, m.toJsonString());
    }
    
    /**
     * 监听导入新增
     * @param params
     * @param controller
     * @throws IOException 
     */
    protected void insertByImport(MultipartFile excel,Map<String,Object> params, 
    		IExcelService controller) throws Exception {
    	Msg m = new Msg();
        Response res = new Response();
		if (excel != null) {
			String fileName = excel.getOriginalFilename();// 获取文件名
			String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
			// 判断文件格式
			if ("xls".equals(ext) || "xlsx".equals(ext)) {
				//对应菜单判空
		        boolean menuNameFlag = !params.containsKey("menuName")
		    		||params.get("menuName")==null
		    		||"".equals(params.get("menuName").toString());
		        if(menuNameFlag){
		        	res.setCode(Code.code_menu);
		        	res.setMsg("对应菜单名称不能为空,请联系开发人员处理");
		        	m.setFlag(state_success);
		        	m.setRes(res);
		        	out2html(response, m.toJsonString());
		        	return;
		        }
		        //根据菜单取得对应的菜单id
		        String menuName = params.get("menuName").toString();
		        SysMenu sysMenu = sysApprovalService.getMenuByMenuNameOrId(menuName);
		        if(sysMenu == null || sysMenu.getId() == null){
		        	res.setCode(Code.code_menu);
		        	res.setMsg("对应菜单名称不正确,请联系开发人员处理");
		        	m.setFlag(state_success);
		        	m.setRes(res);
		        	out2html(response, m.toJsonString());
		        	return;
		        }
		        try {
		        	//获得审批流程设置
		        	SysVerifyHeader verifyheader = sysApprovalService.getApprovalByMenuId(sysMenu.getId());
		        	if(verifyheader == null || verifyheader.getId() == null){
		        		//不需要审批
		        		res.setCode(Code.not_need_approval);
		        		res.setMsg("不需要审批");
		        		JSONObject json = (JSONObject) controller.init(excel,params);
		        		if(json.getBoolean("importStatus")){
		        			sysApprovalService.addAutomaticNot(json,sysMenu.getName());
		        			res.setData(json);
		        			m.setFlag(state_success);
		        			m.setRes(res);
		        		}else{
		        			res.setCode(Code.code_error);
		    	        	res.setMsg(json.getString("importMsg"));
		    	        	m = new Msg();
		    	        	m.setFlag(state_error);
		    	        	m.setRes(res);
		        		}
		            	out2html(response, m.toJsonString());
		            	return;
		        	}else{
		        		//需要审批
		        		if("0".equals(verifyheader.getVerifyType())){
		        			//固定审批流程
		        			JSONObject json = (JSONObject) controller.init(excel,params);
		        			if(!json.isEmpty()){
		        				if(json.getBoolean("importStatus")){
		        					sysApprovalService.addFixed(verifyheader,json);
			        				res.setCode(Code.code_success);
			        	        	res.setMsg("成功");
			        	        	res.setData(json.containsKey("idList")?json.getString("idList"):"[]");
			        	        	m.setFlag(state_success);
			        	        	m.setRes(res);
				        		}else{
				        			res.setCode(Code.code_error);
				    	        	res.setMsg(json.getString("importMsg"));
				    	        	m = new Msg();
				    	        	m.setFlag(state_error);
				    	        	m.setRes(res);
				        		}
		        	        	out2html(response, m.toJsonString());
		        	        	return;
		        			}else{
		        				res.setCode(Code.code_error);
		        	        	res.setMsg("对应保存方法返回数据不正确,请联系开发人员处理");
		        	        	res.setData(params);
		        	        	m.setFlag(state_success);
		        	        	m.setRes(res);
		        	        	out2html(response, m.toJsonString());
		        	        	return;
		        			}
		        		}else if("1".equals(verifyheader.getVerifyType())){
		        			//自动审批流程
		        			boolean approvalFlag = params.containsKey("approval_staff_automatic")
		    					&& params.get("approval_staff_automatic")!=null
		    					&& !"".equals(params.get("approval_staff_automatic"));
		        			if(approvalFlag){
		        				String approvalUsersStr = params.get("approval_staff_automatic").toString();
		        				JSONObject json = (JSONObject) controller.init(excel,params);
		        				if(json.getBoolean("importStatus")){
		        					sysApprovalService.addAutomatic(verifyheader,json,approvalUsersStr);
			        				res.setCode(Code.code_success);
			        	        	res.setMsg("成功");
			        	        	res.setData(json.containsKey("idList")?json.getString("idList"):"[]");
			        	        	m.setFlag(state_success);
			        	        	m.setRes(res);
				        		}else{
				        			res.setCode(Code.code_error);
				    	        	res.setMsg(json.getString("importMsg"));
				    	        	m = new Msg();
				    	        	m.setFlag(state_error);
				    	        	m.setRes(res);
				        		}
		        	        	out2html(response, m.toJsonString());
		        	        	return;
		        			}else{
		        				res.setCode(Code.code_install_approval);
		        				res.setMsg("需要选择审批人员");
		        				res.setData(params);
		        				m.setFlag(state_success);
		        				m.setRes(res);
		        				out2html(response, m.toJsonString());
		        				return;
		        			}
		        		}
		        	}
		            m.setFlag(state_success);
		            res.setCode(Code.code_success);
		            m.setRes(res);
		        } catch (ServiceException e) {
		        	res.setCode(e.getCode());
		        	res.setMsg(e.getLocalizedMessage());
		        	m = new Msg();
		        	m.setFlag(state_error);
		        	m.setRes(res);
		        }catch (Exception e) {
			        String emsg = e.getLocalizedMessage();
			        e.printStackTrace();
			        m = new Msg(state_error);
			        m.setMsg(emsg);
		        }
			} else {
				res.setCode(Code.code_error);
	        	res.setMsg("导入文件格式不正确");
	        	m = new Msg();
	        	m.setFlag(state_error);
	        	m.setRes(res);
			}
		} else {
			res.setCode(Code.code_error);
        	res.setMsg("必须导入一个excel文件");
        	m = new Msg();
        	m.setFlag(state_error);
        	m.setRes(res);
		}
        out2html(response, m.toJsonString());
    }
    
    /**
     * layui前端table数据获取
     * @param params
     * @param controller
     */
    @SuppressWarnings("unchecked")
	protected void layuiTableData(Map<String,Object> params, 
    		IService controller){
    	LayuiTableData ltd = new LayuiTableData();
    	try {
    		List<T> dataList = (List<T>) controller.init(params);
			ltd.setCode(0);
			ltd.setMsg("");
			ltd.setCount(params.containsKey("layuiTableDataCount")?Integer.parseInt(params.get("layuiTableDataCount").toString()):dataList.size());
			ltd.setData(dataList);
		} catch (ServiceException e) {
			e.printStackTrace();
			ltd.setCode(Code.code_error);
			ltd.setMsg(e.getLocalizedMessage());
			ltd.setCount(0);
			ltd.setData(null);
		}
    	out2html(response, ltd.toJsonString());
    }
    
    public void returnJSONObject(JSONObject json)throws Exception{
		PrintWriter out = response.getWriter();
		out.print(json);
		out.flush();
		out.close();
	}
	public void returnJSONArray(JSONArray jsonArray)throws Exception{
		PrintWriter out = response.getWriter();
		out.print(jsonArray);
		out.flush();
		out.close();
	}
}
