package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
public interface TradeVerifyHeaderMapper extends BaseDao<TradeVerifyHeader> {

	List<Map<String, Object>> getVerifyByMapPage(SearchPageUtil searchPageUtil);

	List<Map<String, Object>> mineOriginateList(SearchPageUtil searchPageUtil);

	List<Map<String, Object>> copyToMeList(SearchPageUtil searchPageUtil);

	List<Map<String, Object>> getWaitForMeListPage(SearchPageUtil searchPageUtil);
	
	TradeVerifyHeader getVerifyHeaderByRelatedId(String relatedId);

    void deleteByPrimaryKey(String id);
    
    List<String> getApprovedOrderId(Map<String,String> params);

	List<String> notHeaderIdList(String userId);
}
