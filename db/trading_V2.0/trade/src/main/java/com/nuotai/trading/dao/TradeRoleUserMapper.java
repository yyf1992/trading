package com.nuotai.trading.dao;

import java.util.Map;

import com.nuotai.trading.model.TradeRoleUser;

/**
 * 用户与角色关联表
 * 
 * @author "
 * @date 2017-09-14 09:23:06
 */
public interface TradeRoleUserMapper extends BaseDao<TradeRoleUser> {

	int deleteByRoleId(String roleId);

	int deleteByRoleIdUserId(Map<String, Object> params);
	
	Map<String, String> selectRoleCode(Map <String,Object> map);
}
