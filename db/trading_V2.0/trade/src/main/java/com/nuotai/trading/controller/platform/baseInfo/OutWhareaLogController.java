package com.nuotai.trading.controller.platform.baseInfo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.dao.SysUserMapper;
import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.OutWhareaLogService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 
 * 
 * @author wl
 * @date 2018-01-19 14:42:11
 */
@Controller
@RequestMapping("platform/baseInfo/outwharealog")
public class OutWhareaLogController extends BaseController{
	@Autowired
	private OutWhareaLogService outWhareaLogService;
	@Autowired
	private SysShopService shopService;	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	@RequestMapping("/loadOutwharealogList")
	public String loadOutwharealogList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		//获取部门的名称
		List<SysShop> shopList = shopService.selectByMap(map);
		model.addAttribute("shopList", shopList);
		//获取创建人
		List<SysUser> companyUserList = sysUserMapper.getCompanyUser(map);
		model.addAttribute("companyUserList", companyUserList);
		searchPageUtil.setObject(map);
		//查询列表数据
		List<OutWhareaLog> outWhareaLogList = outWhareaLogService.queryAllList(searchPageUtil);
		searchPageUtil.getPage().setList(outWhareaLogList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/baseInfo/outwharealog/outwharealogList";
	}
	
	/**
	 * 库存积压预警获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<OutWhareaLog> outWhareaLogList = outWhareaLogService.selectOutWhareaLogByParams(params);
				return outWhareaLogList;
			}
		});
	}
	
	/**
	 * 加载导入数据页面
	 * @return
	 */
	@RequestMapping(value="/loadImportExcelHtml",method=RequestMethod.POST)
	public String loadImportExcelHtml(){
		return "platform/baseInfo/outwharealog/importExcel";
	}
	
	/**
	 * 下载模板
	 * @param response
	 * @param request
	 */
	@RequestMapping("/downloadExcel")
	public void downloadExcel(HttpServletResponse response,HttpServletRequest request) {
        try {
            //获取文件的路径
            String excelPath = request.getSession().getServletContext().getRealPath("statics/file/"+"集团关于外仓数据统计.xls");
            //String fileName = "采购价格设置模板.xlsx".toString(); // 文件的默认保存名
            // 读到流中
            //文件的存放路径
            InputStream inStream = new FileInputStream(excelPath);
            // 设置输出的格式
            response.reset();
            response.setContentType("bin");
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode("集团关于外仓数据统计.xls", "UTF-8"));
            OutputStream os = response.getOutputStream();
            // 循环取出流中的数据
            byte[] b = new byte[200];
            int len;

            while ((len = inStream.read(b)) > 0){
            	os.write(b, 0, len);
            }
            // 这里主要关闭。
            os.close();
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * 导入外仓商品数据
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	@RequestMapping(value="/importOutWhareaLogData",method=RequestMethod.POST)
	@ResponseBody
	public String importOutWhareaLogData(@RequestParam(required = false) MultipartFile excel,@RequestParam Map<String,Object> map) throws Exception {
		JSONObject json = new JSONObject();
		//判断是否是excel文件
		if (excel != null) {
			// 获取文件名
			String fileName = excel.getOriginalFilename();
			// 获取扩展名
			String ext = FilenameUtils.getExtension(fileName);
			InputStream is = excel.getInputStream();
			// 判断文件格式
			if ("xls".equals(ext) || "xlsx".equals(ext)) {
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is) : new XSSFWorkbook(is);
				//获得第一个表单
				Sheet sheet = workbook.getSheetAt(0); 
				List<CellRangeAddress> cras = getCombineCell(sheet);
				List<OutWhareaLog> excelData = new ArrayList<OutWhareaLog>();
				// 获取excel行数
				int rowNum = sheet.getLastRowNum();
				int rowIndex = 0;
				for (int i = 1; i <= rowNum; i++) {
					try {
						rowIndex = i;
						//获取每一行的内容
						Row row = sheet.getRow(i);
						if (!ExcelUtil.isBlankRow(row)) {
							//每一行的第一格有值时才导入
							if (!ObjectUtil.isEmpty(row.getCell(0).getStringCellValue())) {
								OutWhareaLog outWhareaLog = new OutWhareaLog();
								outWhareaLog.setWhareaName(map.containsKey("warehouse")?(String)map.get("warehouse"):"");
								outWhareaLog.setSubcompanyName(row.getCell(0).getStringCellValue());
				            	List<OutWhareaLog> items = new ArrayList<>();
				            	//判断单元格是否合并
				            	if(isMergedRegion(sheet,i,0)){
				            		//获取合并单元格的行数
				            		int lastRow = getRowNum(cras,sheet.getRow(i).getCell(0),sheet);
				        			//循环获取同一合并单元格之后的每一行数据
				        			for(;i<=lastRow;i++){
				        				row = sheet.getRow(i);
				        				OutWhareaLog item = new OutWhareaLog();
				        				item.setShopName(getCellValue(row.getCell(1)));
				        				List<OutWhareaLog> its = new ArrayList<>();
				        				if (isMergedRegion(sheet,i,1)){
				        					int lastRow1 = getRowNum(cras,sheet.getRow(i).getCell(1),sheet);
				        					for (;i<=lastRow1;i++){
				        						row = sheet.getRow(i);
				        						OutWhareaLog it = new OutWhareaLog();
				        						it.setProductCode(getCellValue(row.getCell(2)));
				        						it.setBarcode(getCellBarcodeValue(row.getCell(3)));
				        						it.setPrice(new BigDecimal(row.getCell(4).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
				        						it.setOutWhareaStock((int)row.getCell(5).getNumericCellValue());
				        						it.setOutWhareaWayStock((int)row.getCell(6).getNumericCellValue());
				        						it.setOutWhareaTotalPrice(new BigDecimal(row.getCell(7).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
				        						its.add(it);
				        						item.setItems(its);
				        					}
				        					i--;
				        				}else{
				        					item.setProductCode(row.getCell(2).getStringCellValue());
											item.setBarcode(getCellBarcodeValue(row.getCell(3)));
											item.setPrice(new BigDecimal(row.getCell(4).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
											item.setOutWhareaStock((int)row.getCell(5).getNumericCellValue());
											item.setOutWhareaWayStock((int)row.getCell(6).getNumericCellValue());
											item.setOutWhareaTotalPrice(new BigDecimal(row.getCell(7).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
				        				}
				        				items.add(item);
				        			}
				        			i--;
				            	}else{
				        			row = sheet.getRow(i);							
									outWhareaLog.setShopName(getCellValue(row.getCell(1)));
									outWhareaLog.setProductCode(getCellValue(row.getCell(2)));
									outWhareaLog.setBarcode(getCellBarcodeValue(row.getCell(3)));
									outWhareaLog.setPrice(new BigDecimal(row.getCell(4).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
									outWhareaLog.setOutWhareaStock((int)row.getCell(5).getNumericCellValue());
									outWhareaLog.setOutWhareaWayStock((int)row.getCell(6).getNumericCellValue());
									outWhareaLog.setOutWhareaTotalPrice(new BigDecimal(row.getCell(7).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
				            	}
				            	outWhareaLog.setItems(items);
				            	excelData.add(outWhareaLog);
							}
						}
					} catch (Exception e) {
						json.put("status", "fail");
						json.put("msg", e.getMessage());
						json.put("fail", "请检查数据的第 "+rowIndex+"行");
						e.printStackTrace();
						return json.toString();
					}
				}
				if (!ObjectUtil.isEmpty(excelData)){
					json = outWhareaLogService.insertOutWhareaLog(excelData);
				}else{
					json.put("status", "success");
					json.put("msg", "无数据导入！");
					return json.toString();
				}
			} else {
				json.put("status", "fail");
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("status", "fail");
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	
	/**   
	    * 获取单元格的值   
	    * @param cell   
	    * @return   
	    */    
	    @SuppressWarnings("deprecation")
		public String getCellBarcodeValue(Cell cell){    
	    	DecimalFormat df = new DecimalFormat("0");  
	        if(cell == null) return "";    
	        if(cell.getCellType() == Cell.CELL_TYPE_STRING){    
	            return cell.getStringCellValue();    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){    
	            return String.valueOf(cell.getBooleanCellValue());    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){    
	            return cell.getCellFormula() ;    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){    
	            return String.valueOf(df.format(cell.getNumericCellValue()) );    
	        }
	        return "";    
	    }
	    
	/**   
	    * 获取单元格的值   
	    * @param cell   
	    * @return   
	    */    
	    @SuppressWarnings("deprecation")
		public String getCellValue(Cell cell){    
	    
	        if(cell == null) return "";    
	        if(cell.getCellType() == Cell.CELL_TYPE_STRING){    
	            return cell.getStringCellValue();    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){    
	            return String.valueOf(cell.getBooleanCellValue());    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){    
	            return cell.getCellFormula() ;    
	        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){    
	            return String.valueOf(cell.getNumericCellValue());    
	        }
	        return "";    
	    }
	
	/** 
    * 合并单元格处理,获取合并行 
    * @param sheet 
    * @return List<CellRangeAddress> 
	*/  
    public List<CellRangeAddress> getCombineCell(Sheet sheet)  
    {  
        List<CellRangeAddress> list = new ArrayList<CellRangeAddress>();  
        //获得一个 sheet 中合并单元格的数量  
        int sheetmergerCount = sheet.getNumMergedRegions();  
        //遍历所有的合并单元格  
        for(int i = 0; i<sheetmergerCount;i++)   
        {  
            //获得合并单元格保存进list中  
            CellRangeAddress ca = sheet.getMergedRegion(i);  
            list.add(ca);  
        }  
        return list;  
    }
	    
    private int getRowNum(List<CellRangeAddress> listCombineCell,Cell cell,Sheet sheet){
    	int xr = 0;
    	int firstC = 0;  
        int lastC = 0;  
        int firstR = 0;  
        int lastR = 0;  
    	for(CellRangeAddress ca:listCombineCell)  
        {
            //获得合并单元格的起始行, 结束行, 起始列, 结束列  
            firstC = ca.getFirstColumn();  
            lastC = ca.getLastColumn();  
            firstR = ca.getFirstRow();  
            lastR = ca.getLastRow();  
            if(cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR)   
            {  
                if(cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC)   
                {  
                	xr = lastR;
                } 
            }  
            
        }
    	return xr;
    	
    }
    
	/**  
	* 判断指定的单元格是否是合并单元格  
	* @param sheet   
	* @param row 行下标  
	* @param column 列下标  
	* @return  
	*/  
	private boolean isMergedRegion(Sheet sheet,int row ,int column) {  
	  int sheetMergeCount = sheet.getNumMergedRegions();  
	  for (int i = 0; i < sheetMergeCount; i++) {  
		CellRangeAddress range = sheet.getMergedRegion(i);  
		int firstColumn = range.getFirstColumn();  
		int lastColumn = range.getLastColumn();  
		int firstRow = range.getFirstRow();  
		int lastRow = range.getLastRow();  
		if(row >= firstRow && row <= lastRow){  
			if(column >= firstColumn && column <= lastColumn){  
				return true;  
			}  
		}
	  }  
	  return false;  
	}
	
	
}
