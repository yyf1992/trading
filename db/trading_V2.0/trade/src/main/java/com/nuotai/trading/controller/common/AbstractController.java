package com.nuotai.trading.controller.common;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * Controller公共组件
 *
 * @author liujie
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	public static final boolean state_success = true;
    public static final boolean state_error = false;
	
	protected SysUser getUser() {
		return ShiroUtils.getUserEntity();
	}

	protected String getUserId() {
		return getUser().getId();
	}
	
	protected void out2html(HttpServletResponse response, String json) {
        try {
            response.setContentType("text/html;charset=utf-8");
            response
                    .setHeader("Content-type", "application/json;charset=utf-8");
            response.getWriter().print(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}