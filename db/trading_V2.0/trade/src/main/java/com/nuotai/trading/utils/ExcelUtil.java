package com.nuotai.trading.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelUtil {
	
	/**
	 * excel导出前的设置
	 * @param fileName 需要导出的文件名(无需加后缀名)
	 * @param response 
	 */
	public static void preExport (String fileName,HttpServletResponse response){
		String filename = fileName+".xlsx";
		//设置浏览器响应类型为附件下载
		response.setContentType("application/x-excel");
		try {
			response.setHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes(),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过模板获取Workbook对象
	 * @param classpath下的模板文件名
	 * @return workbook
	 */
	public static XSSFWorkbook getWorkbookFromTemplate(String fileName){
		InputStream in = ExcelUtil.class.getResourceAsStream("/"+fileName);
		if(in==null){
			System.out.println("获取模板失败");
		}
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(in);
			return workbook;
		} catch (IOException e) {
			try {
				in.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 导出excel表格
	 * @param workbook
	 * @param response
	 */
	public static void export(Workbook workbook,HttpServletResponse response){
		try {
			ServletOutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 合并单元格功能（样式设置）
	 * @param sheet 需要设置的工作表
	 * @param beginRow 合并单元格开始的行号
	 * @param endRow   合并单元格结束的行号
	 * @param beginColumn 合并单元格开始的列号
	 * @param endColumn   合并单元格结束的列号
	 */
	public static void setRangeStyle(Sheet sheet,int beginRow,int endRow,int beginColumn,int endColumn){
		CellRangeAddress cellRangeAddress = new CellRangeAddress(beginRow-1,endRow-1,beginColumn-1,endColumn-1);
		sheet.addMergedRegion(cellRangeAddress);
	}
	
	/**
	 * 设置单元格格式
	 */
	public static Cell setCellStyle(Workbook workbook,Cell cell){
		if(workbook!=null&&cell!=null){
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);//字体左右居中
			cellStyle.setBorderBottom(CellStyle.BORDER_THIN);//下边框
			cellStyle.setBorderLeft(CellStyle.BORDER_THIN);//左边框
			cellStyle.setBorderRight(CellStyle.BORDER_THIN);//有边框
			cellStyle.setBorderTop(CellStyle.BORDER_THIN);//上边框
			cell.setCellStyle(cellStyle);
		}
		return cell;
	}
	//设置数字格式保留小数点后保留两位
	public static Cell setCellValueStyle(Workbook workbook,Cell cell){
		if(workbook!=null&&cell!=null){
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
			cell.setCellStyle(cellStyle);
		}
		return cell;
	}
	public static boolean isBlankRow(Row row){
		if(row == null) return true;
		boolean result = true;
		for(int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++){
			Cell cell = row.getCell(i, Row.RETURN_BLANK_AS_NULL);
			String value = "";
			if(cell != null){
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					value = String.valueOf((int) cell.getNumericCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					value = String.valueOf(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_FORMULA:
					value = String.valueOf(cell.getCellFormula());
					break;
				//case Cell.CELL_TYPE_BLANK:
				//	break;
				default:
					break;
				}
				
				if(!value.trim().equals("")){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	public static String getCellValue(Cell cell){
		String cellValue = "";  
        DecimalFormat df = new DecimalFormat("#");  
        switch (cell.getCellType()) {  
        case HSSFCell.CELL_TYPE_STRING:  
            cellValue = cell.getRichStringCellValue().getString().trim();  
            break;  
        case HSSFCell.CELL_TYPE_NUMERIC:  
            cellValue = df.format(cell.getNumericCellValue()).toString();  
            break;  
        case HSSFCell.CELL_TYPE_BOOLEAN:  
            cellValue = String.valueOf(cell.getBooleanCellValue()).trim();  
            break;  
        case HSSFCell.CELL_TYPE_FORMULA:  
            cellValue = cell.getCellFormula();  
            break;  
        default:  
            cellValue = "";  
        }  
        return cellValue;  
	}
	
}
