package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseLogMapper;
import com.nuotai.trading.dao.BuyWarehouseProdcutsHistoryMapper;
import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.model.BuyWarehouseProdcutsHistory;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyWarehouseProdcutsHistoryService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseProdcutsHistoryService.class);

	@Autowired
	private BuyWarehouseProdcutsHistoryMapper buyWarehouseProdcutsHistoryMapper;
	@Autowired
	private BuyWarehouseLogMapper buyWarehouseLogMapper;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	
	public BuyWarehouseProdcutsHistory get(String id){
		return buyWarehouseProdcutsHistoryMapper.get(id);
	}
	
	public List<BuyWarehouseProdcutsHistory> queryList(Map<String, Object> map){
		return buyWarehouseProdcutsHistoryMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseProdcutsHistoryMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseProdcutsHistory buyWarehouseProdcutsHistory){
		buyWarehouseProdcutsHistoryMapper.add(buyWarehouseProdcutsHistory);
	}
	
	public void update(BuyWarehouseProdcutsHistory buyWarehouseProdcutsHistory){
		buyWarehouseProdcutsHistoryMapper.update(buyWarehouseProdcutsHistory);
	}
	
	public void delete(String id){
		buyWarehouseProdcutsHistoryMapper.delete(id);
	}
	
	/**
	 * 查询时间段内的商品汇总历史
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	public List<BuyWarehouseProdcutsHistory> selectProdcutsByDate(SearchPageUtil searchPageUtil,Map<String,Object> map){
		
		List<BuyWarehouseProdcutsHistory> historyList = buyWarehouseProdcutsHistoryMapper.selectByDate(searchPageUtil);
		if (historyList != null && historyList.size()>0){
			for (int i=0;i<historyList.size();i++){
				TBusinessWharea tBusinessWharea  = tBusinessWhareaService.getWharea(historyList.get(i).getWarehouseId());
				historyList.get(i).setWarehouseId(tBusinessWharea.getWhareaName());
			}
		}
		return historyList;
	}
	/**
	 * 查询时间段的商品库存明细历史记录
	 * @param map
	 * @return
	 * @throws ParseException 
	 */
	public List<BuyWarehouseProdcutsHistory> selectByDate(SearchPageUtil searchPageUtil,Map<String,Object> map){
		
		List<BuyWarehouseProdcutsHistory> historyList = buyWarehouseProdcutsHistoryMapper.selectByDate(searchPageUtil);
		boolean bar = false;
		String code = "";
		if (map.containsKey("barcode")){
			bar = true;
			code = (String) map.get("barcode");
		}
		if (historyList != null && historyList.size()>0){
			for (int i=0;i<historyList.size();i++){
				TBusinessWharea tBusinessWharea  = tBusinessWhareaService.getWharea(historyList.get(i).getWarehouseId());
				historyList.get(i).setWarehouseId(tBusinessWharea.getWhareaName());
				String barcode = historyList.get(i).getBarcode();
				if (barcode != null){
					map.put("barcode", barcode);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");					
					Calendar calendar = Calendar.getInstance();  
			        try { 
						calendar.setTime(sdf.parse((String) map.get("startDate")));
					} catch (ParseException e) {
						e.printStackTrace();
					}  
			        calendar.add(Calendar.DAY_OF_MONTH, 1);  
			        Date startDate = calendar.getTime();  								
					map.put("startTime", sdf.format(startDate));
					
					try {
						calendar.setTime(sdf.parse((String) map.get("endDate")));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					calendar.add(Calendar.DAY_OF_MONTH, -1); 
					Date endDate = calendar.getTime(); 
					map.put("endTime", sdf.format(endDate));

					//查询时间段内出入库汇总价格和成本
					List<BuyWarehouseLog> buyWarehouseLogList = buyWarehouseLogMapper.selectByDate(map);
					
					for (BuyWarehouseLog buyWarehouseLog : buyWarehouseLogList){
						//统计出库数量和成本
						if (buyWarehouseLog.getChangeType() == 0){
							historyList.get(i).setOutNum(buyWarehouseLog.getNumber());
							historyList.get(i).setOutPrice(buyWarehouseLog.getPrice());
						}
						//统计入库数量和成本
						if (buyWarehouseLog.getChangeType() == 1){
							historyList.get(i).setInNum(buyWarehouseLog.getNumber());
							historyList.get(i).setInPrice(buyWarehouseLog.getPrice());
						}
					}
					
					int changeType = 0;
					int stockType = 0;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询互通出库数量和成本
					BuyWarehouseLog buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setSellerNum(buyWarehouseLog.getNumber());
						historyList.get(i).setSellerPrice(buyWarehouseLog.getPrice());
					}
					stockType = 1;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询手工出库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setSellerManualNum(buyWarehouseLog.getNumber());
						historyList.get(i).setSellerManualPrice(buyWarehouseLog.getPrice());
					}
					stockType = 2;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询调货出库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setTransferOutNum(buyWarehouseLog.getNumber());
						historyList.get(i).setTransferOutPrice(buyWarehouseLog.getPrice());
					}
					stockType = 3;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询退货出库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setReturnOutNum(buyWarehouseLog.getNumber());
						historyList.get(i).setReturnOutPrice(buyWarehouseLog.getPrice());
					}
					changeType = 1;
					stockType = 0;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询互通入库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setBuyNum(buyWarehouseLog.getNumber());
						historyList.get(i).setBuyPrice(buyWarehouseLog.getPrice());
					}
					stockType = 1;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询手工入库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setBuyManualNum(buyWarehouseLog.getNumber());
						historyList.get(i).setBuyManualPrice(buyWarehouseLog.getPrice());
					}
					stockType = 2;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询调货入库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setTransferInNum(buyWarehouseLog.getNumber());
						historyList.get(i).setTransferInPrice(buyWarehouseLog.getPrice());
					}
					stockType = 3;
					map.put("changeType", changeType);
					map.put("stockType", stockType);
					//查询退货入库数量和成本
					buyWarehouseLog = buyWarehouseLogMapper.selectNumAndPriceByDate(map);
					if (buyWarehouseLog != null){
						historyList.get(i).setReturnInNum(buyWarehouseLog.getNumber());
						historyList.get(i).setReturnInPrice(buyWarehouseLog.getPrice());
					}
				}
			}
			
		}
		if (bar == true){
			map.put("barcode", code);		
		}

		return historyList;
	}
}
