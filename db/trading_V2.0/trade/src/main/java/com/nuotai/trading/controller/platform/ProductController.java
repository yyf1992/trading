package com.nuotai.trading.controller.platform;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.dao.BuyShopProductMapper;
import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.service.BuyProductService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.UplaodUtil;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 商品管理
 * @author liuhui
 * @date 2017-07-19
 */

@Controller
@RequestMapping("platform/product")
public class ProductController extends BaseController {
	
	private static final Logger log=LoggerFactory.getLogger(ProductController.class);
	
	/*
	 * 单位管理
	 */
	@Autowired
	private BuyUnitService unitService;  
	@Autowired
	private BuyShopProductService shopProductService;
	@Autowired
	private BuyProductService buyProductService;
	@Autowired
	private BuyProductSkuService productSkuService;
	@Autowired
	private BuyShopProductMapper productMapper;
	
	/**
	 * 加载与供应商互通商品数据
	 * @return
	 */
	@RequestMapping(value = "productLinkList", method = RequestMethod.GET)
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
//		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}//互通类型  0:与供应商互通,1:与客户互通,2.与外协互通
		String linktype = params.containsKey("linktype")?params.get("linktype").toString():"0";
		model.addAttribute("linktype",linktype);
		params.put("searchCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<Map<String,Object>> shopProductList = shopProductService.loadProductLinks(searchPageUtil,linktype);
		searchPageUtil.getPage().setList(shopProductList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/baseInfo/product/productLinkListData";
	}
	
	/**
	 * 加载与供应商互通商品页面
	 * @return
	 */
	@RequestMapping(value = "loadProductList", method = RequestMethod.GET)
	public String loadProductList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("status")){
			params.put("status", "");
		}
		//互通类型  0:与供应商互通,1:与客户互通,2.与外协互通
		String linktype =params.get("linktype").toString();
		model.addAttribute("linktype",linktype);
		searchPageUtil.setObject(params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		//按类型获取条数
		Integer[] totalMap = shopProductService.getCount();
		model.addAttribute("allCount", totalMap[0]);
		model.addAttribute("internalCount", totalMap[1]);
		model.addAttribute("externalCount", totalMap[2]);
		model.addAttribute("okCount", totalMap[3]);
		model.addAttribute("cancelCount", totalMap[4]);
		return "platform/baseInfo/product/productLinkList";
	}

	/**
	 * 新增商品关联界面
	 * @param request
	 * @return
	 */
	@RequestMapping("addProductLink")
	public String addProductLink(HttpServletRequest request){
		String linktype = request.getParameter("linktype");
		model.addAttribute("linktype", linktype);
		return "platform/baseInfo/product/addProductLink";
	}

	/**
	 * 商品sku弹出框列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadProductSkuList")
	@ResponseBody
	public String loadProductSkuList(@RequestParam Map<String,Object> params){
		String companyid = ShiroUtils.getCompId();
		params.put("companyId",companyid);
		List<BuyProductSku> list = productSkuService.querySkuList(params);
		return JSONObject.toJSONString(list);
	}

	/**
	 * Check product link string.
	 * 检查新增的关联是否已存在
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping(value = "/checkProductLink")
	@ResponseBody
	public String checkProductLink(@RequestParam Map<String,Object> params){
		JSONObject json = shopProductService.checkProductLink(params);
		return json.toString();
	}
	
	/**
	 * 商品关联提交
	 * @param params
	 * @return
	 */
	@RequestMapping("saveProductLink")
	public void saveProductLink(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params) throws ServiceException {
				JSONObject json = new JSONObject();
				//linktype 互通类型  0:与供应商互通,1:与客户互通,2.与外协互通
				String linktype = params.get("linktype").toString();
				//String linktype ="0";
				if("0".equalsIgnoreCase(linktype) ||"2".equalsIgnoreCase(linktype)){
					//审批成功调用的方法
					json.put("verifySuccess", "platform/product/buyerVerifySuccess");
					//审批失败调用的方法
					json.put("verifyError", "platform/product/buyerVerifyError");
				}
				//商品关联提交屏蔽待对方审批
//				else {//目前只能由与供应商互通商品提交审批
//					json.put("verifySuccess", "platform/product/sellerVerifySuccess");//审批成功调用的方法
//					json.put("verifyError", "platform/product/sellerVerifyError");//审批失败调用的方法
//				}
				//审批详细信息地址
				json.put("relatedUrl", "platform/product/buyerVerifyDetail?linktype="+linktype);
				String insList = (String) params.get("insList");
				List<String> idList = shopProductService.saveProductLinkGetIds(linktype,insList);
				json.put("idList", idList);
				return json;
			}
		});
	}

	/**
	 * 商品关联供应商审批
	 * @param params
	 */
	@RequestMapping(value = "/productLinkSellerAprove", method = RequestMethod.POST)
	@ResponseBody
	public String productLinkSellerAprove(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			BuyShopProduct product = new BuyShopProduct();
			product.setId(String.valueOf(params.get("id")));
			product.setStatus("3");
			shopProductService.updateByPrimaryKeySelective(product);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 采购商审批通过
	 * @param id
	 */
	@RequestMapping(value = "buyerVerifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String buyerVerifySuccess(String id){
		JSONObject json = shopProductService.buyerVerifySuccess(id);
		return json.toString();
	}

	/**
	 * 采购商审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "buyerVerifyError", method = RequestMethod.GET)
	@ResponseBody
	public String buyerVerifyError(String id){
		JSONObject json = shopProductService.buyerVerifyError(id);
		return json.toString();
	}


	/**
	 * Buyer verify detail string.
	 * 审批详情
	 * @param id the id
	 * @return the string
	 */
	@RequestMapping(value = "buyerVerifyDetail")
	public String buyerVerifyDetail(String id,String linktype){
		BuyShopProduct buyShopProduct = shopProductService.selectByPrimaryKey(id);
		model.addAttribute("buyShopProduct",buyShopProduct);
		model.addAttribute("linktype",linktype);
		return "platform/baseInfo/product/productLinkDetail";
	}
	/**
	 * 供应商审批通过
	 * @param id
	 */
	@RequestMapping(value = "/sellerVerifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String sellerVerifySuccess(String id){
		JSONObject json = shopProductService.sellerVerifySuccess(id);
		return json.toString();
	}

	/**
	 * 供应商审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/sellerVerifyError", method = RequestMethod.GET)
	@ResponseBody
	public String sellerVerifyError(String id){
		JSONObject json = shopProductService.sellerVerifyError(id);
		return json.toString();
	}

	/**
	 * 关联商品价格变更
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/updBuyShopProductPrice", method = RequestMethod.POST)
	@ResponseBody
	public String updBuyShopProductPrice(String buyShopProductId, BigDecimal updPrice){
		JSONObject json = new JSONObject();
		try {
			String userId = ShiroUtils.getUserId();
			shopProductService.updBuyShopProductPrice(buyShopProductId,updPrice,userId);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 获得商品总权重
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getWeightSum", method = RequestMethod.GET)
	@ResponseBody
	public String getWeightSum(String barcode,String buyShopProductId){
		JSONObject json = new JSONObject();
		try {
			int sum=shopProductService.getWeightSum(barcode,buyShopProductId);
			json.put("success", true);
			json.put("msg", sum);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 关联商品合作权重变更
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/updateWeight", method = RequestMethod.POST)
	@ResponseBody
	public String updateWeight(String buyShopProductId, int undertakeProportion){
		JSONObject json = new JSONObject();
		try {
			BuyShopProduct product = new BuyShopProduct();
			product.setId(buyShopProductId);
			product.setUndertakeProportion(undertakeProportion);
			productMapper.updateByPrimaryKeySelective(product);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 关联商品价格变更
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/comfirmShopProductPriceUpd", method = RequestMethod.POST)
	@ResponseBody
	public String comfirmShopProductPriceUpd(String buyShopProductId){
		JSONObject json = new JSONObject();
		try {
//			String userId = ShiroUtils.getUserId();
			shopProductService.comfirmShopProductPriceUpd(buyShopProductId);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 商品查询
	 * @param params
	 * @return
	 */
	@RequestMapping("selectGoods")
	@ResponseBody
	public String selectGoods(@RequestParam Map<String,Object> params){
		params.put("companyID", ShiroUtils.getCompId());
		List<Map<String,Object>> productList = buyProductService.selectProductByMap(params);
		return JSONObject.toJSONString(productList);
	}
	/**
	 * 加载全部商品列表
	 * @param searchPageUtil
	 * @param param
	 * @return
	 */
	@RequestMapping("/allProduct")
	public String allProduct(SearchPageUtil searchPageUtil, @RequestParam Map<String, Object> param){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		//商品的类型
		param.put("productType", param.containsKey("productType")? param.get("productType").toString():0);
		param.put("companyId", ShiroUtils.getCompId());
		param.put("productName",param.containsKey("productName")? param.get("productName").toString().trim():"");
		param.put("productCode", param.containsKey("productCode")? param.get("productCode").toString().trim():"");
		param.put("barcode", param.containsKey("barcode")? param.get("barcode").toString().trim():"");
		param.put("skuCode", param.containsKey("skuCode")? param.get("skuCode").toString().trim():"");
		param.put("skuName", param.containsKey("skuName")? param.get("skuName").toString().trim():"");
		searchPageUtil.setObject(param);
		//查询列表数据
		List<Map<String, Object>> buyProductList = buyProductService.queryByPage(searchPageUtil);
		searchPageUtil.getPage().setList(buyProductList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		model.addAttribute("productPro",param.get("productPro"));
		int type = Integer.parseInt(param.get("productType").toString());
		if ( type == Constant.ProductType.MATERIAL.getValue()){
			return "platform/product/productListMaterial";
		}
		if (type == Constant.ProductType.ACCESSORIES.getValue()){
			return "platform/product/productListAccessories";
		}
		if (type == Constant.ProductType.VIRTUALPRODUCTS.getValue()){
			return "platform/product/productListVirtualProducts";
		}
		return "platform/product/productList";
	}
	
	
	/**
	 * 加载新增商品界面
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/loadAddProductHtml")
	public String loadAddProductHtml( @RequestParam Map<String, Object> param){
		
		//商品的类型
		param.put("productType", param.containsKey("productType")? param.get("productType").toString():0);
		
		int type = Integer.parseInt(param.get("productType").toString());
		if ( type == Constant.ProductType.MATERIAL.getValue()){
			return "platform/product/addMaterial";
		}
		if (type == Constant.ProductType.ACCESSORIES.getValue()){
			return "platform/product/addAccessories";
		}
		if (type == Constant.ProductType.VIRTUALPRODUCTS.getValue()){
			return "platform/product/addVirtualProducts";
		}
		return "platform/product/addProduct";
	}
	
	
	/**
	 * 加载单位
	 * @return
	 */
	@RequestMapping(value="/loadUnitList")
	@ResponseBody
	public String loadUnitList(){
		List<BuyUnit> buyUnitList = unitService.getBuyUnitList();
		return JSONArray.toJSONString(buyUnitList);
	}
	
	/**
	 * 加载新增计量单位界面
	 * @return
	 */
	@RequestMapping(value="/loadAddUnitHtml")
	public String loadAddUnitHtml(){
		List<BuyUnit>  unitList=unitService.getBuyUnitList();
		model.addAttribute("unitList",unitList);
		return "platform/product/addUnit";
	}
	
	/** 
	 * 加载新增商品SKU界面
	 * @return
	 */
	@RequestMapping(value="/loadAddSKU")
	public String loadAddSKU(String id,String productCode,int productType,String unitId,String productName){
		model.addAttribute("id",id);
		model.addAttribute("productCode",productCode);
		model.addAttribute("productType",productType);
		model.addAttribute("unitId",unitId);
		model.addAttribute("productName",productName);
		return "platform/product/addSKU";
	}
	
	/** 
	 * 保存新增商品界面
	 * @return
	 */
	@RequestMapping(value="/saveProduct",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveProduct(BuyProduct buyProduct){
		JSONObject jsonObject=buyProductService.insertProduct(buyProduct);
		return jsonObject;
	}
	
	/** 
	 * 修改保存商品
	 * @return
	 */
	@RequestMapping(value="/updateProduct",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject updateProduct(BuyProduct buyProduct){
		JSONObject jsonObject=buyProductService.updateProduct(buyProduct);
		return jsonObject;
	}
	
	/** 
	 * 加载所有SKU界面
	 * @return
	 */
	@RequestMapping(value="/loadAllSKU")
	public String loadAllSKU(String id,String productCode,int productType,String unitId,String productName,@RequestParam Map<String, Object> param){
		List<BuyUnit> buyUnitList = unitService.getBuyUnitList();
		Map<String, Object> map=new HashMap<String, Object>();
		if(productCode != null){
			map.put("productCode", productCode);
		}
		BuyProduct buyProduct = buyProductService.selectByPrimaryKey(id);
		map.put("companyId", ShiroUtils.getCompId());
		List<BuyProductSku> buyProductSkus=productSkuService.queryList(map);
		model.addAttribute("buyProductSkus", buyProductSkus);
		model.addAttribute("buyProduct", buyProduct);
		model.addAttribute("id", id);
		model.addAttribute("productCode", productCode);
		model.addAttribute("productType", productType);
		model.addAttribute("unitId", unitId);
		model.addAttribute("productName", productName);
		model.addAttribute("buyUnitList", buyUnitList);
		model.addAttribute("form", param.get("form"));
		return "platform/product/allSKU";
	}
	//保存新增商品SKU
	@RequestMapping(value="saveSku",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveSku(BuyProductSku buyProductSku){
		JSONObject jsonObject=productSkuService.insertSelective(buyProductSku);
		return jsonObject;
	}
	//删除SKU
	@RequestMapping(value="deleteSku",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject deleteSku(String id){
		BuyProductSku buyProductSku=productSkuService.selectByPrimaryKey(id);
		JSONObject jsonObject=productSkuService.deleteSku(buyProductSku);
		return jsonObject;
	}
	//加载修改商品SKU界面
	@RequestMapping(value="loadEditSku",method=RequestMethod.POST)
	public String loadEditSku(String id){
		BuyProductSku buyProductSku=productSkuService.selectByPrimaryKey(id);
		model.addAttribute("buyProductSku", buyProductSku);
		return "platform/product/editSku";
	}
	//保存商品SKU修改
	@RequestMapping(value="saveEditSku",method=RequestMethod.POST)
	@ResponseBody
	public JSONObject saveEditSku(BuyProductSku buyProductSku){
		JSONObject jsonObject=productSkuService.updateSku(buyProductSku);
		return jsonObject;
	}
	
	//保存新增单位
	@RequestMapping(value="saveUnit")
	@ResponseBody
	public JSONObject saveUnit(@RequestParam Map<String,Object> param){
		JSONObject json=unitService.insertUnit(param);
		return json;
	}
	
	//删除单位
	@RequestMapping(value="delUnit")
	@ResponseBody
	public JSONObject delUnit(String id){
		JSONObject json=new JSONObject();
		int result=unitService.delUnit(id);
		if(result>0){
			json.put("success",true);
			json.put("msg","单位删除成功！");
		}else{
			json.put("success",false);
			json.put("msg","单位删除失败！");
		}
		return json;
	}
	
	/**
	 * 互通商品弹出框列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadInterflowProductList")
	public String loadInterflowProductList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		params.put("linktype", "0");
		//状态已通过
		params.put("status", "3");
		params.put("searchCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<Map<String,Object>> shopProductList = shopProductService.loadProductLinks(searchPageUtil,"0");
		searchPageUtil.getPage().setList(shopProductList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/addPlan/productList";
	}
	
    /**
     * 
     * 上传商品主图附件
     * @param files the files
     * @return the string
     * @throws Exception the exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/uploadImage",method=RequestMethod.POST)
    @ResponseBody
    public String uploadReceiptAttachment(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
            throws Exception{
        JSONObject json = new JSONObject();
        try {
            List urlList = new ArrayList();
            for (MultipartFile file : files) {
                //获取文件名
                String fileName = file.getOriginalFilename();
                json.put("fileName", fileName);
                //获取文件后缀名
                String fileExtensionName = FilenameUtils.getExtension(fileName);
                if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
                        && !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
					json.put("result", "fail");
					json.put("msg", "上传文件格式不正确！");
					return json.toString();
                }
                //获取文件大小
 //               String fileSize = ShiroUtils.convertFileSize(file.getSize());
                UplaodUtil uplaodUtil = new UplaodUtil();
                String url = uplaodUtil.uploadFile(file,null,true);
                urlList.add(url);
            }
            json.put("urlList", urlList);
            json.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "fail");
            json.put("msg", "上传失败！请联系管理员！");
            return json.toString();
        }
        return json.toString();
    }
    
	/** 
	 * 加载新增商品SKU界面
	 * @return
	 */
	@RequestMapping(value="/loadOMSProduct")
	public String loadOMSProduct(@RequestParam Map<String, Object> params){
		String type = (String) (params.get("type") == null ? "" : params.get("type").toString().replaceAll("\\s*", ""));
		model.addAttribute("type", type);
		return "platform/product/loadOMSProduct";
	}
	
	/**
	 * 同步OMS商品信息
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="synchronousOMSProduct")
	@ResponseBody
	public String synchronousOMSProduct(@RequestParam Map<String, Object> params){
		JSONObject json = new JSONObject();
		//获取买卖系统商品所有数据
		List<Map<String,Object>> productSKUList = productSkuService.selectAllProduct(params);
		net.sf.json.JSONObject jsonString = new  net.sf.json.JSONObject();
		String type = (String) (params.get("type") == null ? "" : params.get("type").toString().replaceAll("\\s*", ""));
		String skuCode = (String) (params.get("skuCode") == null ? "" : params.get("skuCode").toString().replaceAll("\\s*", ""));
		String skuName = (String) (params.get("skuName") == null ? "" : params.get("skuName").toString().replaceAll("\\s*", ""));
		String productCode = (String) (params.get("productCode") == null ? "" : params.get("productCode").toString().replaceAll("\\s*", ""));
		String barcode = (String) (params.get("barcode") == null ? "" : params.get("barcode").toString().replaceAll("\\s*", ""));
		//获取OMS所有商品数据
		String levelUrl = null;
		try {
			levelUrl = Constant.OMS_INTERFACE_URL + "loadProductOldSku?skucode="
					+ java.net.URLEncoder.encode(skuCode, "UTF-8")
					+"&skuname=" + java.net.URLEncoder.encode(skuName, "UTF-8")
					+ "&procode=" + productCode + "&skuoid="+ barcode;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}   
		
		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		jsonString.put("list", arrayString);
		arrayString = jsonString.toString();
		int num = 0;
		if (!ObjectUtil.isEmpty(arrayString)){
			net.sf.json.JSONObject data = net.sf.json.JSONObject.fromObject(arrayString);		
			net.sf.json.JSONArray productSKUArray = net.sf.json.JSONArray.fromObject(data.get("list"));
			
			Iterator<Object> it = productSKUArray.iterator();
	        while (it.hasNext()) {
	        	net.sf.json.JSONObject ob = (net.sf.json.JSONObject) it.next();
            	if (ob.getString("skuoid")!=null){
            		String barcodeOMS = ob.getString("skuoid").toString().replaceAll("\\s*", "");
//            		boolean result = true;
            		
            		if (productSKUList != null && productSKUList.size() > 0) {
            			for (int i = 0; i < productSKUList.size(); i++) {
            				Map<String,Object> item = productSKUList.get(i);
            				
            				if (barcodeOMS.equals(item.get("barcode").toString().replaceAll("\\s*", "")) ){
//            					result = false;
            				}
            			}
	            	}
            		//如果OMS中商品不存在买卖系统中，将商品添加到买卖系统
//            		if (result){  
            			System.out.println(ob.getString("skuoid"));
        				try {
							num = productSkuService.saveAddOMSProduct(ob);
						} catch (Exception e) {
							data.put("success", false);
            				data.put("msg", e.getMessage());
            				json.put("type",type);
							e.printStackTrace();
						}
//            		}

	            }
			}	
		}
		if(num>0){
			json.put("success",true);
			json.put("msg","操作成功！");
			json.put("type",type);
		}else{
			json.put("success",true);
			json.put("msg","无数据同步！");
			json.put("type",type);
		}
		return json.toString();
	}
	
	/**
	 * 加载查询供应商详情页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadSupplierProductLinks")
	public String loadSupplierProductLinks(String supplierId,String suppName,String linktype) throws Exception{
		Map<String,Object> map = new HashMap<>();
		map.put("supplierId", supplierId);
		// 供应商关联商品
		List<BuyShopProduct> buyShopProductList = shopProductService.queryListBySupplierId(map);
		model.addAttribute("buyShopProductList", buyShopProductList);
		model.addAttribute("suppName", suppName);
		model.addAttribute("linktype", linktype);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/supplier/loadSupplierProductLinks";
	}
	
	/**
	 * 根据货号查询商品图片
	 */
	@RequestMapping("getProductPicByProductCode")
	@ResponseBody
	public String getProductPicByProductCode(String productCode){
		JSONObject json = new JSONObject();
		BuyProduct product = buyProductService.selectByProductCode(productCode);
		if(product != null){
			String pic = product.getMainPictureUrl() == null ? "" : product.getMainPictureUrl();
			json.put("pic", pic);
		}else{
			json.put("pic", "");
		}
		return json.toJSONString();
	}
}
