package com.nuotai.trading.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.Resources;

/**
 * 资源表dao
 * @ClassName ResourcesMapper
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
public interface ResourcesMapper {

	/**
	 * 获取列表
	 * @author:     dxl
	 * @param queue
	 * @return
	 */
	List<Resources> getBeanList(Resources resources);

	/**
	 * 选择性添加
	 * @author:     dxl
	 * @param queue
	 */
	void addBean(Resources resources);

	/**
	 * 修改
	 * @author:     dxl
	 * @param resQueue
	 */
	void updateBean(Resources resources);

	/**
	 * 删除
	 * @author:     dxl
	 * @param id
	 */
	void deleteBean(@Param("id")String id);
}
