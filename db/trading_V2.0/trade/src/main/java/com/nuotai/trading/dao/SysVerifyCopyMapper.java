package com.nuotai.trading.dao;

import java.util.List;

import com.nuotai.trading.model.SysVerifyCopy;

/**
 * 
 * 
 * @author "
 * @date 2017-08-21 11:50:20
 */
public interface SysVerifyCopyMapper extends BaseDao<SysVerifyCopy> {

	List<SysVerifyCopy> getListByHeaderId(String headerId);
	int deleteByHeaderId(String headerId);
}
