package com.nuotai.trading.controller.platform.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.SysPlatform;
import com.nuotai.trading.service.SysPlatformService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 平台表
 * 
 * @author zyn
 * @date 2017-08-03 13:04:48
 */
@Controller
@RequestMapping("platform/sysplatform")
public class SysPlatformController extends BaseController {
	@Autowired
	private SysPlatformService sysPlatformService;

	/**
	 * 列表
	 */
	@RequestMapping("/loadSysPlatformList")
	public String list(SearchPageUtil searchPageUtil,
			@RequestParam Map<String, Object> param) {
		return "platform/system/platform/sysPlatformList";
	}
	
	/**
	 * 获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<SysPlatform> sysPlatformList = sysPlatformService.queryList(params);
				return sysPlatformList;
			}
		});
	}

	@RequestMapping(value = "loadAddSysPlatform", method = RequestMethod.POST)
	public String loadAddSysPlatform() {
		return "platform/system/platform/addSysPlatform";
	}

	/**
	 * 保存新增平台
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveInsert")
	public String save(SysPlatform sysPlatform) {
		JSONObject json = new JSONObject();
		json = sysPlatformService.saveAdd(sysPlatform);
		return json.toString();
	}

	/**
	 * 加载修改平台界面
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@RequestMapping(value = "/loadEditPlatform", method = RequestMethod.POST)
	public String loadEditPlatform(String id) {
		SysPlatform sysPlatform = sysPlatformService.selectByPrimaryKey(id);
		model.addAttribute("SysPlatform", sysPlatform);
		return "platform/system/platform/editPlatform";
	}

	/**
	 * 保存修改平台
	 * 
	 * @param sysPlatform
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateSysPlatform")
	public String updateSysPlatform(SysPlatform sysPlatform) {
		JSONObject json =sysPlatformService.saveUpdate(sysPlatform);
		return json.toString();
	}
	/**
	 * 删除平台
	 */
	@ResponseBody
	@RequestMapping("deleteSysPlatform")
	public String deleteSysPlatform(String id){
		SysPlatform sysPlatform=sysPlatformService.selectByPrimaryKey(id);
		JSONObject json =sysPlatformService.delete(sysPlatform);
		return json.toString();
	}

}
