package com.nuotai.trading.model;

import java.io.Serializable;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
@Data
public class TradeVerifyCopy implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//主表id
	private String headerId;
	//
	private String userId;
	//抄送人员姓名
	private String userName;
}
