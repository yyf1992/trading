package com.nuotai.trading.dao;

import java.util.List;

import com.nuotai.trading.model.TradeVerifyCopy;

/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
public interface TradeVerifyCopyMapper extends BaseDao<TradeVerifyCopy> {

	List<TradeVerifyCopy> queryListByHeaderId(String headerId);
	
}
