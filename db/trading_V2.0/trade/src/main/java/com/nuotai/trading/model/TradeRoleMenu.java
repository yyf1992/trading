package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 角色与菜单关联表
 * 
 * @author "
 * @date 2017-09-14 09:23:05
 */
@Data
public class TradeRoleMenu implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//角色主键
	private String roleId;
	//菜单主键
	private String menuId;
	//是否需要审批，0-否；1-是
	private String verifyType;
	//创建时间
	private Date createTime;
	//
	private String companyId;
}
