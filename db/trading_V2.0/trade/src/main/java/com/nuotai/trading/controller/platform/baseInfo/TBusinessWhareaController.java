package com.nuotai.trading.controller.platform.baseInfo;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 仓库区域
 * 
 * @author wangli
 * @date 2017-08-10 13:26:09
 */
@Controller
@RequestMapping("platform/baseInfo/wharea")
public class TBusinessWhareaController extends BaseController{
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	@Autowired
	private TBusinessWhareaMapper tBusinessWhareaMapper;
	/**
	 * 查询仓库列表
	 * @param map
	 * @return
	 */
	@RequestMapping("/whareaList")
	public String selectWharea(TBusinessWharea tBusinessWharea,@RequestParam Map<String,Object> map){
		
//		if (tBusinessWharea != null){
//			String whareaCode = (String) (map.get("whareaCode") == null ? "" : map.get("whareaCode").toString().trim());
//			String whareaName = (String) (map.get("whareaName") == null ? "" : map.get("whareaName").toString().trim());
//			map.put("whareaName", whareaName);
//			map.put("whareaCode", whareaCode);
//			List<TBusinessWharea> tBusinessWhareaList = tBusinessWhareaService.selectWhareaByMap(tBusinessWharea,map);
//			model.addAttribute("tBusinessWhareaList",tBusinessWhareaList);
//		}
			
		return "platform/baseInfo/wharea/whareaList";
	}
	
	/**
	 * 库零分析获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadWhareaListDataJson", method = RequestMethod.GET)
	public void loadWhareaListDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<TBusinessWharea> tBusinessWhareaList = tBusinessWhareaMapper.selectWhareaByMap(params);
				return tBusinessWhareaList;
			}
		});
	}
	
	/**
	 * 根据Id查询仓库
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getWhareaById")
	public void info(String id){
		
		if (id != null){
			TBusinessWharea tBusinessWharea = tBusinessWhareaService.getWharea(id);
			model.addAttribute("tBusinessWharea",tBusinessWharea);
		}
		
	}
	
	/**
	 * 新增仓库
	 * @param tBusinessWharea
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveWharea")
	public String save(TBusinessWharea tBusinessWharea){
		JSONObject json = new JSONObject();
		if (tBusinessWharea != null){
			
			try {
				tBusinessWhareaService.addWharea(tBusinessWharea);
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
	
	/**
	 * 修改仓库
	 * @param tBusinessWharea
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateWharea")
	public String update(TBusinessWharea tBusinessWharea){
		JSONObject json = new JSONObject();
		if (tBusinessWharea != null){
			
			try {
				tBusinessWhareaService.updateWharea(tBusinessWharea); 
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
	
	/**
	 * 删除仓库
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteWharea")
	public String  delete(String id){
		JSONObject json = new JSONObject();
		if (id != null){			
			try {
				tBusinessWhareaService.deleteWharea(id); 
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
	
	/**
	 * 批量删除仓库
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteBeatchWharea")
	public String  deleteBeatchWharea(String ids){
		JSONObject json = new JSONObject();
		if (ids != null){			
			try {
				tBusinessWhareaService.deleteBeatchWharea(ids); 
				json.put("success", true);
			} catch (Exception e) {
				json.put("false", false);
				json.put("msg", e.getMessage());
				e.printStackTrace();
			}
		}	
		return json.toString();
	}
}
