package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyWarehouseProdcutsBatch;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author "
 * @date 2017-09-12 14:47:15
 */
public interface BuyWarehouseProdcutsBatchMapper extends BaseDao<BuyWarehouseProdcutsBatch> {
	
	List<Map<String,Object>> selectProdcutsByPage(SearchPageUtil searchPageUtil);
	
}
