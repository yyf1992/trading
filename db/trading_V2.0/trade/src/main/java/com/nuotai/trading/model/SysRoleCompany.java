package com.nuotai.trading.model;

import java.util.Date;

import lombok.Data;

/**
 * 公司角色关联表
 * @author dxl
 *
 */
@Data
public class SysRoleCompany {
    private String id;

    private String companyId;

    private String roleId;
    
    private String roleName;
    private String roleCode;

    private Date createTime;

}