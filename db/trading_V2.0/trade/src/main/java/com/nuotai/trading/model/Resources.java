package com.nuotai.trading.model;

import java.util.Date;

import lombok.Data;

/**
 * 资源对象
 * @ClassName Resources
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
@Data
public class Resources {
	private String id;
	private String txtType;
	private String sourceUrl;
	private String htmlUrl;
	private String imgUrl;
	private String fileName;
	private String resId;
	private Date addDateTime;
}
