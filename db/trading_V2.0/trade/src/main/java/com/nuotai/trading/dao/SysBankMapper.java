package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.SysBank;

/**
 * 银行账户表
 * 
 * @author "
 * @date 2017-08-11 09:05:08
 */
public interface SysBankMapper extends BaseDao<SysBank> {

	int insert(SysBank sysBank); 
	
	List<SysBank> queryList(String bankAccount);

	SysBank selectByPrimaryKey(String id);

	//根据条件查询银行账户数据下拉
	List<SysBank> getBankAccountBuyMap(Map<String,Object> map);
	
	List<SysBank> queryDataList(Map<String,Object> map);
}
