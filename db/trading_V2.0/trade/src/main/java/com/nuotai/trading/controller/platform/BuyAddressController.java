package com.nuotai.trading.controller.platform;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.service.BuyAddressService;


@Controller
@RequestMapping("platform/address")
public class BuyAddressController extends BaseController{
	
	@Autowired
	private BuyAddressService buyAddressService;
	
	/**
	 * 收货地址列表页面
	 * @param searchPageUtil
	 * @return
	 */
	@RequestMapping("loadAddressList")
	public String supplierList(){
		List<BuyAddress> addressList = buyAddressService.getAllAddress();
		model.addAttribute("addressList", addressList);
		return "platform/system/buyAddress/addressList";
	}
	
	/**
	 * 加载新增收货地址页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addAddress", method = RequestMethod.POST)
	public String addAddress() throws Exception{
		return "platform/system/buyAddress/addAddress";
	}
	
	/**
	 * 添加收货地址保存
	 * @param Address
	 * @return
	 */
	@RequestMapping(value = "/saveAddAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveAddAddress(BuyAddress address){
		JSONObject json = buyAddressService.saveAdd(address);
		return json.toString();
	}
	
	/**
	 * 加载修改收货地址页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAddress", method = RequestMethod.POST)
	public String updateAddress(String id) throws Exception{
		BuyAddress sa = buyAddressService.selectByPrimaryKey(id);
		model.addAttribute("address", sa);
		return "platform/system/buyAddress/updateAddress";
	}
	
	/**
	 * 修改收货地址保存
	 * @param Address
	 * @return
	 */
	@RequestMapping(value = "/saveUpdateAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdateAddress(BuyAddress address){
		JSONObject json = buyAddressService.saveUpdate(address);
		return json.toString();
	}
	
	/**
	 * 删除收货地址保存
	 * @param Address
	 * @return
	 */
	@RequestMapping(value = "/saveDeleteAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveDeleteAddress(String id){
		JSONObject json = buyAddressService.saveDelete(id);
		return json.toString();
	}
}
