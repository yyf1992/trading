package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author liuhui
 * @date 2017-11-20 13:26:35
 */
@Data
public class BuyProductPricealter implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//关联Id
	private String relatedId;
	//店铺id
	private String shopId;
	//店铺code
	private String shopCode;
	//店铺名称
	private String shopName;
	//状态 0、主账号审批；1通过，2未通过
	private Integer status;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//原价格
	private BigDecimal price;
	//修改价格
	private BigDecimal alterPrice;
	//修改价格理由
	private String modifyReason;
	//供应商Id
	private String suppId;
	//供应商条形码
	private String suppBarcode;
	//
	private Integer isDel;
	//
	private String createrId;
	//创建人姓名
	private String createrName;
	//创建时间
	private Date createDate;
	//
	private String updateId;
	//
	private String updateName;
	//
	private Date updateDate;
	//
	private String delId;
	//
	private String delName;
	//
	private Date delDate;
}
