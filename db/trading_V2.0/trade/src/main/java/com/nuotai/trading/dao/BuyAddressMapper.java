package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.BuyAddress;


public interface BuyAddressMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyAddress record);

    int insertSelective(BuyAddress record);

    BuyAddress selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyAddress record);

    int updateByPrimaryKeyWithBLOBs(BuyAddress record);
    
    int updateByPrimaryKey(BuyAddress record);
    
    List<BuyAddress> selectByMap(Map<String,Object> map);

	int cancelDefault(@Param("compId")String compId);

	BuyAddress selectById(@Param("id")String id);
}