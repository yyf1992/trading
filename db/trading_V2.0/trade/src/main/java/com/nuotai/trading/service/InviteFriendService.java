package com.nuotai.trading.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.*;
import com.nuotai.trading.model.*;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class InviteFriendService {

    private static final Logger LOG = LoggerFactory.getLogger(InviteFriendService.class);

	@Autowired
	private InviteFriendMapper inviteFriendMapper;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	@Autowired
	private BuySupplierFriendMapper buySupplierFriendMapper;

	public BuySupplierFriendInvite get(String id){
		return inviteFriendMapper.get(id);
	}
	
	public List<BuySupplierFriendInvite> queryList(Map<String, Object> map){
		return inviteFriendMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return inviteFriendMapper.queryCount(map);
	}
	
	public JSONObject add(BuySupplierFriendInvite buySupplierFriendInvite) throws Exception {
		JSONObject json = new JSONObject();
		//根据被邀请人公司名称查询被邀请人的公司信息
		Map<String ,Object> map = new HashMap<String ,Object>();
		map.put("searchContent",buySupplierFriendInvite.getInviteeName());
		List<BuyCompany> list = buyCompanyMapper.checkCompany(map);
		BuyCompany invitee = new BuyCompany();
		if(list!=null&&list.size()>0){
			invitee = list.get(0);
			//查询是否已经是好友
			map.clear();
			String inviterRole = buySupplierFriendInvite.getInviterRole();
			if("1".equalsIgnoreCase(inviterRole)){//我是卖家发起
				map.put("sellerId", ShiroUtils.getCompId());
				map.put("buyersId",invitee.getId());
			}else {//我是买家发起
				map.put("buyersId", ShiroUtils.getCompId());
				map.put("sellerId",invitee.getId());
			}
			List<BuySupplierFriend> friendList = buySupplierFriendMapper.friendCheck(map);
			if(friendList!=null&&friendList.size()>0){
//				throw new Exception("您与该单位已经是好友啦！");
				json.put("success",false);
				json.put("msg","您与该单位已经是好友啦！");
				return json;
			}
			//如果已经有申请信息，则更新一下申请信息,否则新增
			BuySupplierFriendInvite friendInvite = new BuySupplierFriendInvite();
			map.clear();
			map.put("inviterId", ShiroUtils.getCompId());
			map.put("inviteeId",invitee.getId());
			map.put("inviterRole",buySupplierFriendInvite.getInviterRole());
			List<BuySupplierFriendInvite> invitelist = inviteFriendMapper.queryList(map);
			if(invitelist!=null&&invitelist.size()>0){
				friendInvite = invitelist.get(0);
				friendInvite.setNote(buySupplierFriendInvite.getNote());
				friendInvite.setStatus("0");
				friendInvite.setIsDel(0);
				friendInvite.setDelDate(null);
				friendInvite.setDelId(null);
				friendInvite.setDelName(null);
				friendInvite.setCreateId(ShiroUtils.getUserId());
				friendInvite.setCreateName(ShiroUtils.getUserName());
				friendInvite.setCreateDate(new Date());
				inviteFriendMapper.update(friendInvite);
			}else {
				BuyCompany inviteCompany = buyCompanyMapper.selectByPrimaryKey(ShiroUtils.getCompId());
				friendInvite.setId(ShiroUtils.getUid());
				friendInvite.setInviterId(ShiroUtils.getCompId());
				friendInvite.setInviterName(inviteCompany.getCompanyName());
				friendInvite.setInviterPerson(inviteCompany.getLinkMan());
				friendInvite.setInviterPhone(inviteCompany.getTelNo());
				friendInvite.setInviterRole(buySupplierFriendInvite.getInviterRole());
				friendInvite.setInviteeId(invitee.getId());
				friendInvite.setInviteeName(invitee.getCompanyName());
				friendInvite.setInviteePerson(invitee.getLinkMan());
				friendInvite.setInviteePhone(invitee.getTelNo());
				friendInvite.setNote(buySupplierFriendInvite.getNote());
				friendInvite.setStatus("0");
				friendInvite.setIsDel(0);
				friendInvite.setDelDate(null);
				friendInvite.setDelId(null);
				friendInvite.setDelName(null);
				friendInvite.setCreateId(ShiroUtils.getUserId());
				friendInvite.setCreateName(ShiroUtils.getUserName());
				friendInvite.setCreateDate(new Date());
				inviteFriendMapper.add(friendInvite);
			}
			json.put("success",true);
			json.put("msg","发送好友申请成功！");
		}else {
			json.put("success",false);
			json.put("msg","系统中不存在要添加的客户！");
		}
		return json;
	}
	
	public void update(BuySupplierFriendInvite buySupplierFriendInvite){
		inviteFriendMapper.update(buySupplierFriendInvite);
	}
	
	public void delete(String id){
		inviteFriendMapper.delete(id);
	}


	public List<BuySupplierFriendInvite> selectByPage(SearchPageUtil friendPageUtil) {
		return inviteFriendMapper.selectByPage(friendPageUtil);
	}

	public void setInviteFriendStatus(String id, String status) {
		//获取要更新的申请表中的信息
		BuySupplierFriendInvite invite = inviteFriendMapper.get(id);
		//根据状态来设置更新的内容
		if("agree".equals(status)){
			invite.setStatus("1");
		}else if ("ignore".equals(status)){
			invite.setStatus("2");
		}else if ("del".equals(status)){
			invite.setIsDel(1);
			invite.setDelId(ShiroUtils.getUserId());
			invite.setDelName(ShiroUtils.getUserName());
			invite.setDelDate(new Date());
		}
		//更新申请表
		inviteFriendMapper.update(invite);
		//如果是同意，则插入信息到buy_supplier_friend表
		if("agree".equals(status)){
			//先判断buy_supplier_friend表是否存在,若存在则更新，否则新增
			BuySupplierFriend buySupplierFriend = new BuySupplierFriend();
			Map<String ,Object> map = new HashMap<String ,Object>();
			if("1".equalsIgnoreCase(invite.getInviterRole())){//如果是卖家发起，则卖方是发起人
				map.put("sellerId", invite.getInviterId());
				map.put("buyersId",invite.getInviteeId());
			}else {//如果是买家发起，则卖方是受邀人
				map.put("buyersId", invite.getInviterId());
				map.put("sellerId",invite.getInviteeId());
			}
			List<BuySupplierFriend> buySupplierFriendList  = buySupplierFriendMapper.selectByMap(map);
			if(buySupplierFriendList!=null&&buySupplierFriendList.size()>0){
				buySupplierFriend = buySupplierFriendList.get(0);
				buySupplierFriend.setIsDel(0);
				buySupplierFriend.setDelId("");
				buySupplierFriend.setDelName("");
				buySupplierFriend.setDelDate(null);
				buySupplierFriendMapper.updateByPrimaryKey(buySupplierFriend);
			}else {
				buySupplierFriend.setId(ShiroUtils.getUid());
				if("1".equalsIgnoreCase(invite.getInviterRole())){//如果是卖家发起，则卖方是发起人
					buySupplierFriend.setBuyersId(invite.getInviteeId());
					buySupplierFriend.setBuyersName(invite.getInviteeName());
					buySupplierFriend.setBuyersPerson(invite.getInviteePerson());
					buySupplierFriend.setBuyersPhone(invite.getInviteePhone());
					buySupplierFriend.setSellerId(invite.getInviterId());
					buySupplierFriend.setSellerName(invite.getInviterName());
					buySupplierFriend.setSellerPerson(invite.getInviterPerson());
					buySupplierFriend.setSellerPhone(invite.getInviterPhone());
				}else {//如果是买家发起，则买方是发起人
					buySupplierFriend.setBuyersId(invite.getInviterId());
					buySupplierFriend.setBuyersName(invite.getInviterName());
					buySupplierFriend.setBuyersPerson(invite.getInviterPerson());
					buySupplierFriend.setBuyersPhone(invite.getInviterPhone());
					buySupplierFriend.setSellerId(invite.getInviteeId());
					buySupplierFriend.setSellerName(invite.getInviteeName());
					buySupplierFriend.setSellerPerson(invite.getInviteePerson());
					buySupplierFriend.setSellerPhone(invite.getInviteePhone());
				}

				buySupplierFriend.setIsDel(0);
				buySupplierFriend.setCreateId(ShiroUtils.getUserId());
				buySupplierFriend.setCreateName(ShiroUtils.getUserName());
				buySupplierFriend.setCreateDate(new Date());
				buySupplierFriendMapper.insert(buySupplierFriend);
			}
			//更新我的供应商或者我的客户表
			Map<String,Object> map2 = new HashMap<>();
			if("1".equalsIgnoreCase(invite.getInviterRole())) {//如果是卖家发起，则更新seller_client表，否则更新buy_supplier表
				map2.put("companyId",invite.getInviterId());
				map2.put("clientName",invite.getInviteeName());
				map2.put("friendId",buySupplierFriend.getBuyersId());
				buySupplierFriendMapper.updateSellerClientFriendLink(map2);
			}else {
				map2.put("companyId",invite.getInviterId());
				map2.put("suppName",invite.getInviteeName());
				map2.put("type","0");
				map2.put("friendId",buySupplierFriend.getSellerId());
				int count = buySupplierFriendMapper.updateBuySupplierFriendLink(map2);
				//buy_supplier没更新，则插入一条数据
				if(count<=0){
					BuySupplier buySupplier = new BuySupplier();
					buySupplier.setId(ShiroUtils.getUid());
					buySupplier.setCompanyId(invite.getInviterId());
					buySupplier.setFriendId(invite.getInviteeId());
					buySupplier.setSuppName(invite.getInviteeName());
					buySupplier.setCreateId(invite.getCreateId());
					buySupplier.setCreateDate(new Date());
					buySupplier.setCreateName(invite.getCreateName());
					buySupplier.setAddType("1");
					buySupplier.setType("0");
					buySupplier.setIsDel(0);
					buySupplierFriendMapper.insBuySupplier(buySupplier);
				}
			}

		}
	}
}
