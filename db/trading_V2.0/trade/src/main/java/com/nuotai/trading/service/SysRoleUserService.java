package com.nuotai.trading.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.SysRoleUserMapper;
import com.nuotai.trading.model.SysRoleUser;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class SysRoleUserService implements SysRoleUserMapper {
	@Autowired
	private SysRoleUserMapper sysRoleUserMapper;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		return sysRoleUserMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(com.nuotai.trading.model.SysRoleUser record) {
		return sysRoleUserMapper.insert(record);
	}

	@Override
	public int insertSelective(com.nuotai.trading.model.SysRoleUser record) {
		return sysRoleUserMapper.insertSelective(record);
	}

	@Override
	public SysRoleUser selectByPrimaryKey(String id) {
		return sysRoleUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysRoleUser record) {
		return sysRoleUserMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(com.nuotai.trading.model.SysRoleUser record) {
		return sysRoleUserMapper.updateByPrimaryKey(record);
	}

	@Override
	public int deleteByRoleId(String[] idList) {
		return sysRoleUserMapper.deleteByRoleId(idList);
	}

	@Override
	public List<SysRoleUser> selectRightByPageUser(SearchPageUtil searchPageUtil) {
		return sysRoleUserMapper.selectRightByPageUser(searchPageUtil);
	}
	
	public void addRoleUser(String userIds, String roleId) throws Exception {
		String[] userIdList = userIds.split(",");
		for (String userId : userIdList) {
			SysRoleUser sru = new SysRoleUser();
			sru.setId(ShiroUtils.getUid());
			sru.setRoleId(roleId);
			sru.setUserId(userId);
			sru.setCreateTime(new Date());
			sysRoleUserMapper.insertSelective(sru);
		}
	}
	
	public void addUserRole(String roleIds, String userId) throws Exception {
		String[] roleIdList = roleIds.split(",");
		for (String roleId : roleIdList) {
			SysRoleUser sru = new SysRoleUser();
			sru.setId(ShiroUtils.getUid());
			sru.setRoleId(roleId);
			sru.setUserId(userId);
			sru.setCreateTime(new Date());
			sysRoleUserMapper.insertSelective(sru);
		}
	}
	
	public void deleteRoleUser(String roleUserIds) throws Exception {
		String[] idStr = roleUserIds.split(",");
		deleteByIdList(idStr);
	}

	@Override
	public int deleteByIdList(String[] idStr) {
		return sysRoleUserMapper.deleteByIdList(idStr);
	}
	
	public List<String> selectRoleIdByUserId(String user_id){
		return sysRoleUserMapper.selectRoleIdByUserId(user_id);
	}
}
