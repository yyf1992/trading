package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseLogMapper;
import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyWarehouseLogService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseLogService.class);

	@Autowired
	private BuyWarehouseLogMapper buyWarehouseLogMapper;
	
	public BuyWarehouseLog get(String id){
		return buyWarehouseLogMapper.get(id);
	}
	
	public List<BuyWarehouseLog> queryList(Map<String, Object> map){
		return buyWarehouseLogMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseLogMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseLog buyWarehouseLog){
		buyWarehouseLogMapper.add(buyWarehouseLog);
	}
	
	public void update(BuyWarehouseLog buyWarehouseLog){
		buyWarehouseLogMapper.update(buyWarehouseLog);
	}
	
	public void delete(String id){
		buyWarehouseLogMapper.delete(id);
	}
	
	public List<Map<String, Object>> selectProdcutsByPage(SearchPageUtil searchPageUtil) {

		return buyWarehouseLogMapper.selectProdcutsLogByPage(searchPageUtil);
	}
	
	public List<BuyWarehouseLog> selectProdcutsLogByParams(Map<String, Object> map){
		return buyWarehouseLogMapper.selectProdcutsLogByParams(map);
	}
}
