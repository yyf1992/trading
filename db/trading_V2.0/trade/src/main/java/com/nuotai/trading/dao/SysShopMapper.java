package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 店铺表
 * 
 * @author "
 * @date 2017-08-07 08:59:32
 */
public interface SysShopMapper extends BaseDao<SysShop> {
	int insert(SysShop sysShop);

	SysShop selectByPrimaryKey(String id);

	List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil);

	SysShop selectByShopCode(SysShop sysShop);

	SysShop selectByShopName(SysShop sysShop);

	List selectByCompanyId(String id);
	
	List<SysShop> selectByMap(Map<String,Object> map);
	
	List<SysShop> getShopByMap(Map<String,Object> params);
	
	List<SysShop> queryDataList(Map<String,Object> map); 
}
