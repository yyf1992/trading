package com.nuotai.trading.controller.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.SysApprovalService;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.SysRoleUserService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

@Controller
@RequestMapping("DDS_verify")
public class DingDingController extends BaseController {
	@Autowired
	private TradeVerifyHeaderService tradeVerifyHeaderService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysApprovalService sysApprovalService;
	@Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private SysMenuService sysMenuService;
	
	/**
	 * 获得审批详情
	 * @param userId
	 * @param verifyId
	 * @return
	 */
	@RequestMapping("getDetail")
	public String getDetail(String userId,String verifyId){
		ShiroUtils.logout();
		SysUser user = sysUserService.selectByPrimaryKey(userId);
		//ShiroUtils.setSessionAttribute(Constant.CURRENT_USER, user);
		Subject subject = ShiroUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getPassword());
		subject.login(token);
		//获得审批数据
		tradeVerifyHeaderService.getDingDingVerifyDetail(model,userId,verifyId);
		return "dingding/verifyDetail";
	}
	
	/**
	 * 获得审批详情
	 * @param userId
	 * @param verifyId
	 * @return
	 */
	@RequestMapping("getDetailMobile")
	public String getDetailMobile(String userId,String verifyId){
		ShiroUtils.logout();
		SysUser user = sysUserService.selectByPrimaryKey(userId);
		//ShiroUtils.setSessionAttribute(Constant.CURRENT_USER, user);
		Subject subject = ShiroUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getPassword());
		subject.login(token);
		//获得审批数据
		tradeVerifyHeaderService.getDingDingVerifyDetail(model,userId,verifyId);
		return "dingding/verifyDetailMobile";
	}
	
	/**
	 * 保存审批
	 * @param map
	 * @return
	 */
	@RequestMapping("saveVerify")
	@ResponseBody
	public String saveVerify(@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			json = tradeVerifyHeaderService.saveVerify(map);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("flag", false);
			json.put("msg", "失败");
		}
		return json.toJSONString();
	}
	
	/**
	 * 选择转交人员
	 * @return
	 */
	@RequestMapping("setReferralUser")
	public String setReferralUser(String verifyId){
		sysApprovalService.setReferralUser(model);
		model.addAttribute("verifyId", verifyId);
		return "dingding/setReferralUser";
	}
	
	/**
	 * 察看附件
	 * @param verifyId
	 * @return
	 */
	@RequestMapping("vieWappendix")
	public String vieWappendix(String userId,String url){
		ShiroUtils.logout();
		SysUser user = sysUserService.selectByPrimaryKey(userId);
		Subject subject = ShiroUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getPassword());
		subject.login(token);
		setSysMenuSession();
		model.addAttribute("url", url);
		return "dingding/vieWappendix";
	}
	
	public void setSysMenuSession(){
    	SysUser user = ShiroUtils.getUserEntity();
    	//菜单信息
    	// 权限过滤，admin是超级权限
    	Map<String, Object> map = new HashMap<String, Object>();
		// 取得登录人id
		if (!"admin".equals(user.getLoginName())) {
			map.put("loginUserId", user.getId());
			// 取得登陆人的角色信息
			List<String> roleIdList = sysRoleUserService.selectRoleIdByUserId(user.getId());
			List<String> menuIdList = sysMenuService.getMenuIdByRoleId(roleIdList);
			map.put("menuIdList", menuIdList);
		}
		// 取得权限下的顶部菜单菜单
		map.put("position", Constant.MenuPosition.TOP.getValue());
		List<SysMenu> parentMenu = sysMenuService.getMenuByMap(map);
		ShiroUtils.setSessionAttribute(Constant.AUTHORITYSYSMENU, parentMenu);
    }
    
    /**
	 * 递归取得菜单信息
	 * @param parentMenu
	 */
	public void getSysMenu(List<SysMenu> parentMenu,Map<String, Object> map){
		if (parentMenu != null && parentMenu.size() > 0) {
			for (SysMenu menu : parentMenu) {
				map.put("position", Constant.MenuPosition.LEFT.getValue());
				map.put("parent_id", menu.getId());
				map.put("is_menu", Constant.MenuType.MENU.getValue());
				// 取得二级菜单
				List<SysMenu> childMenu = sysMenuService.getMenuByMap(map);
				if(childMenu != null && childMenu.size() > 0){
					getSysMenu(childMenu,map);
				}
				menu.setChildrenMenuList(childMenu);
			}
		}
	}
	
	/**
	 * 察看附件
	 * @param verifyId
	 * @return
	 */
	@RequestMapping("viewVerifyDateil")
	public String viewVerifyDateil(String url){
		model.addAttribute("url", url);
		return "platform/verifyDateil";
	}
}
