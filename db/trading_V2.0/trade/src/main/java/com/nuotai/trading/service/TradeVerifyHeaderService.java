package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ocpsoft.prettytime.PrettyTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.dao.TradeVerifyFileMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.TradeVerifyCopy;
import com.nuotai.trading.model.TradeVerifyFile;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;



@Service
@Transactional
public class TradeVerifyHeaderService {
    private static final Logger LOG = LoggerFactory.getLogger(TradeVerifyHeaderService.class);

	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyFileMapper tradeVerifyFileMapper;
	@Autowired
	private SysUserService sysUserService;
	
	public TradeVerifyHeader get(String id){
		return tradeVerifyHeaderMapper.get(id);
	}
	
	public List<TradeVerifyHeader> queryList(Map<String, Object> map){
		return tradeVerifyHeaderMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return tradeVerifyHeaderMapper.queryCount(map);
	}
	
	public void add(TradeVerifyHeader tradeVerifyHeader){
		tradeVerifyHeaderMapper.add(tradeVerifyHeader);
	}
	
	public void update(TradeVerifyHeader tradeVerifyHeader){
		tradeVerifyHeaderMapper.update(tradeVerifyHeader);
	}
	
	public void delete(String id){
		tradeVerifyHeaderMapper.delete(id);
	}

	public List<Map<String,String>> getVerifyType(
			Map<String, Object> map) {
		map.put("companyId", ShiroUtils.getCompId());
		map.put("status", "0");
		return sysVerifyHeaderMapper.getVerifyType(map);
	}

	/**
	 * 待我审批的数据
	 * @param searchPageUtil
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getWaitForMeList(
			SearchPageUtil searchPageUtil) {
		//获得还没有到自己审批的数据
		String userId = ShiroUtils.getUserId();
		List<String> notHeaderIdList = tradeVerifyHeaderMapper.notHeaderIdList(userId);
		Map<String,Object> params = (Map<String,Object>)searchPageUtil.getObject();
		params.put("notHeaderIdList", notHeaderIdList);
		return tradeVerifyHeaderMapper.getWaitForMeListPage(searchPageUtil);
	}

	public List<Map<String, Object>> approvedList(SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> list = tradeVerifyHeaderMapper.getVerifyByMapPage(searchPageUtil);
		if(!list.isEmpty()){
			for(Map<String, Object> map : list){
				String id = map.get("id").toString();
				String hStatus = map.containsKey("hStatus")?
					map.get("hStatus")!=null?map.get("hStatus").toString():"0":"0";
				if("0".equals(hStatus)){
					//审批中
					String userName = tradeVerifyPocessMapper.getApprovalUser(id);
					map.put("approvalUser", userName);
				}else{
					map.put("approvalUser", null);
				}
			}
		}
		return list;
	}

	
	public List<Map<String, Object>> mineOriginateList(
			SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> list = tradeVerifyHeaderMapper.mineOriginateList(searchPageUtil);
		if(!list.isEmpty()){
			for(Map<String, Object> map : list){
				String id = map.get("id").toString();
				String hStatus = map.containsKey("hStatus")?
					map.get("hStatus")!=null?map.get("hStatus").toString():"0":"0";
				if("0".equals(hStatus)){
					//审批中
					String userName = tradeVerifyPocessMapper.getApprovalUser(id);
					map.put("approvalUser", userName);
				}else{
					map.put("approvalUser", null);
				}
			}
		}
		return list;
	}

	public List<Map<String, Object>> copyToMeList(SearchPageUtil searchPageUtil) {
		List<Map<String, Object>> list = tradeVerifyHeaderMapper.copyToMeList(searchPageUtil);
		if(!list.isEmpty()){
			for(Map<String, Object> map : list){
				String id = map.get("id").toString();
				String hStatus = map.containsKey("hStatus")?
					map.get("hStatus")!=null?map.get("hStatus").toString():"0":"0";
				if("0".equals(hStatus)){
					//审批中
					String userName = tradeVerifyPocessMapper.getApprovalUser(id);
					map.put("approvalUser", userName);
				}else{
					map.put("approvalUser", null);
				}
			}
		}
		return list;
	}

	/**
	 * 加载审批详情页面
	 * @param model
	 * @param id
	 */
	public void getVerifyDetail(Model model, String id) {
		PrettyTime p = new PrettyTime(Constant.locale);
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(id);
		SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
		//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
		String copyType = "审批通过后，通知抄送人";
		if("0".equals(verifyHeader.getCopyType())){
			copyType = "审批通过后，通知抄送人";
		}else if("1".equals(verifyHeader.getCopyType())){
			copyType = "发起时通知，通知抄送人";
		}else if("2".equals(verifyHeader.getCopyType())){
			copyType = "发起时和全部同意后，均通知抄送人";
		}
		
		header.setCreateDateFormat(p.format(header.getCreateDate()).replaceAll(" ", ""));
		//是否可以撤回
		boolean revoke = false;
		if(header.getCreateId().equals(ShiroUtils.getUserId())){
			header.setStartName(header.getCreateName());
			//自己创建的
			if("0".equals(header.getStatus())){
				//审批中的数据
				//详情页中不展示撤销
				// revoke = true;
			}
		}else{
			header.setStartName(header.getCreateName());
		}
		//获得审批流程
		//是否需要审批
		boolean verify = false;
		boolean verifyOwen = false;
		List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(id);
		if(!pocessList.isEmpty()){
			int n = 0;
			for(TradeVerifyPocess pocess : pocessList){
				if(pocess.getUserId().equals(ShiroUtils.getUserId())){
					if(!"0".equals(pocess.getStatus())){
						verifyOwen = true;
					}
				}
				pocess.setStartIndext(false);
				if("0".equals(pocess.getStatus())){
					n++;
				}else{
					pocess.setStartDateFormat(p.format(pocess.getEndDate()).replaceAll(" ", ""));
				}
				if(n==1){
					//第一个待审批数据
					pocess.setStartDateFormat(p.format(pocess.getStartDate()).replaceAll(" ", ""));
					pocess.setStartIndext(true);
					if(pocess.getUserId().equals(ShiroUtils.getUserId())){
						//可以审批
						verify = true;
					}
				}
				//获得附件
				Map<String,Object> fileMap = new HashMap<>();
				fileMap.put("verifyId", header.getId());
				fileMap.put("pocessId", pocess.getId());
				List<TradeVerifyFile> fileList = tradeVerifyFileMapper.queryList(fileMap);
				pocess.setFileList(fileList);
			}
		}
		//获得抄送数据
		List<TradeVerifyCopy> copyList = tradeVerifyCopyMapper.queryListByHeaderId(id);
		model.addAttribute("tradeHeader", header);
		model.addAttribute("revoke", revoke);
		model.addAttribute("verify", verify);
		model.addAttribute("pocessList", pocessList);
		model.addAttribute("copyList", copyList);
		model.addAttribute("copyType", copyType);
		model.addAttribute("verifyOwen", verifyOwen);
		boolean urge = false;
		if(header.getCreateId().equals(ShiroUtils.getUserId())){
			//自己发起的申请，可以催办
			urge = true;
		}
		model.addAttribute("urge", urge);
	}
	
	/**
	 * 
	 * @param model
	 * @param id
	 */
	public void getRelatedDetail(Model model, String id) {
		PrettyTime p = new PrettyTime(Constant.locale);
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(id);
		SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
		//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
		String copyType = "审批通过后，通知抄送人";
		if("0".equals(verifyHeader.getCopyType())){
			copyType = "审批通过后，通知抄送人";
		}else if("1".equals(verifyHeader.getCopyType())){
			copyType = "发起时通知，通知抄送人";
		}else if("2".equals(verifyHeader.getCopyType())){
			copyType = "发起时和全部同意后，均通知抄送人";
		}
		
		header.setCreateDateFormat(p.format(header.getCreateDate()).replaceAll(" ", ""));
		//是否可以撤回
		boolean revoke = false;
		if(header.getCreateId().equals(ShiroUtils.getUserId())){
			header.setStartName(header.getCreateName());
			//自己创建的
			if("0".equals(header.getStatus())){
				//审批中的数据
				//详情页中不展示撤销
				// revoke = true;
			}
		}else{
			header.setStartName(header.getCreateName());
		}
		//获得审批流程
		//是否需要审批
		boolean verify = false;
		boolean verifyOwen = false;
		List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
		if(!pocessList.isEmpty()){
			int n = 0;
			for(TradeVerifyPocess pocess : pocessList){
				if(pocess.getUserId().equals(ShiroUtils.getUserId())){
					if(!"0".equals(pocess.getStatus())){
						verifyOwen = true;
					}
				}
				pocess.setStartIndext(false);
				if("0".equals(pocess.getStatus())){
					n++;
				}else{
					pocess.setStartDateFormat(p.format(pocess.getEndDate()).replaceAll(" ", ""));
				}
				if(n==1){
					//第一个待审批数据
					pocess.setStartDateFormat(p.format(pocess.getStartDate()).replaceAll(" ", ""));
					pocess.setStartIndext(true);
					if(pocess.getUserId().equals(ShiroUtils.getUserId())){
						//可以审批
						verify = true;
					}
				}
				//获得附件
				Map<String,Object> fileMap = new HashMap<>();
				fileMap.put("verifyId", header.getId());
				fileMap.put("pocessId", pocess.getId());
				List<TradeVerifyFile> fileList = tradeVerifyFileMapper.queryList(fileMap);
				pocess.setFileList(fileList);
			}
		}
		//获得抄送数据
		List<TradeVerifyCopy> copyList = tradeVerifyCopyMapper.queryListByHeaderId(id);
		model.addAttribute("tradeHeader", header);
		model.addAttribute("revoke", revoke);
		model.addAttribute("verify", verify);
		model.addAttribute("pocessList", pocessList);
		model.addAttribute("copyList", copyList);
		model.addAttribute("copyType", copyType);
		model.addAttribute("verifyOwen", verifyOwen);
		boolean urge = false;
		if(header.getCreateId().equals(ShiroUtils.getUserId())){
			//自己发起的申请，可以催办
			urge = true;
		}
		model.addAttribute("urge", urge);
	}

	/**
	 * 保存审批
	 * @param map
	 * @return
	 */
	public JSONObject saveVerify(Map<String, Object> map) throws Exception{
		String userId = ShiroUtils.getUserId();
		JSONObject json = new JSONObject();
		String id = map.get("id").toString();
		String saveType = map.containsKey("saveType")?map.get("saveType")!=null?map.get("saveType").toString():"":"";
		String fileUrl = map.containsKey("fileUrl")?map.get("fileUrl")!=null?map.get("fileUrl").toString():"":"";
		if("1".equals(saveType)){
			//同意
			String remark = map.containsKey("remark")?map.get("remark")!=null?map.get("remark").toString():"":"";
			json = agreeVerify(id,remark,userId,fileUrl);
			json.put("flag", true);
			json.put("msg", "成功");
		}else if("2".equals(saveType)){
			//拒绝
			String remark = map.containsKey("remark")?map.get("remark")!=null?map.get("remark").toString():"":"";
			json = refuse(id,remark,userId,fileUrl);
			json.put("flag", true);
			json.put("msg", "成功");
		}else if("3".equals(saveType)){
			//转交
			String remark = map.containsKey("remark")?map.get("remark")!=null?map.get("remark").toString():"":"";
			json = referralUser(id,remark,userId,fileUrl);
			json.put("flag", true);
			json.put("msg", "成功");
		}else if("4".equals(saveType)){
			//撤销
			json = revoked(id,userId);
		}
		return json;
	}

	/**
	 * 转交
	 * @param id
	 * @param remark
	 * @param fileUrl 
	 * @return
	 */
	private JSONObject referralUser(String id, String remark,String loginUserId, String fileUrl) {
		JSONObject json = new JSONObject();
		String[] referralArray = remark.split(",");
		String userId = referralArray[0];
		String userName = referralArray[1];
		String opinion = referralArray.length>2?referralArray[2]:"";
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", id);
		params.put("userId", loginUserId);
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("3");
			pocessOwn.setEndDate(new Date());
			pocessOwn.setRemark(opinion);
			if(!StringUtils.isEmpty(fileUrl)){
				//pocessOwn.setFileUrl(fileUrl);
				insertVerifyFile(pocessOwn,fileUrl);
			}
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						pocess.setVerifyIndex(pocess.getVerifyIndex()+1);
						tradeVerifyPocessMapper.update(pocess);
					}
				}
				TradeVerifyPocess pocess = new TradeVerifyPocess();
				pocess.setId(ShiroUtils.getUid());
				pocess.setHeaderId(id);
				pocess.setUserId(userId);
				pocess.setUserName(userName);
				pocess.setStatus("0");
				pocess.setVerifyIndex(pocessOwn.getVerifyIndex()+1);
				pocess.setStartDate(new Date());
				tradeVerifyPocessMapper.add(pocess);
				//发送钉钉消息
				DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
			}
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}

	/**
	 * 撤销
	 * @param id
	 * @throws Exception
	 */
	private JSONObject revoked(String id,String loginUserId) throws Exception{
		JSONObject json = new JSONObject();
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(id);
		if(!header.getCreateId().equals(loginUserId)){
			json.put("flag", false);
			json.put("msg", "无权撤销");
			return json;
		}
		//审批流程结束
		header.setStatus("3");
		header.setUpdateDate(new Date());
		tradeVerifyHeaderMapper.update(header);
		//删除所有未审批的流程
		tradeVerifyPocessMapper.deleteNotVerify(id);
		//添加撤销流程
		TradeVerifyPocess pocess = new TradeVerifyPocess();
		pocess.setId(ShiroUtils.getUid());
		pocess.setHeaderId(id);
		pocess.setUserId(loginUserId);
		pocess.setUserName(ShiroUtils.getUserName());
		pocess.setStatus("4");
		int maxIndex = tradeVerifyPocessMapper.getMaxIndex(id);
		pocess.setVerifyIndex(maxIndex+1);
		pocess.setStartDate(new Date());
		tradeVerifyPocessMapper.add(pocess);
		json.put("flag", true);
		json.put("msg", "成功");
		//发送钉钉消息
		DingDingUtils.verifyDingDingOwn(id);
		return json;
	}

	/**
	 * 拒绝审批
	 * @param id
	 * @param remark
	 * @param fileUrl 
	 * @throws Exception
	 */
	private JSONObject refuse(String id, String remark,String loginUserId, String fileUrl) throws Exception{
		JSONObject json = new JSONObject();
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(id);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", id);
		params.put("userId", loginUserId);
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("2");
			pocessOwn.setRemark(remark);
			pocessOwn.setEndDate(new Date());
			if(!StringUtils.isEmpty(fileUrl)){
				//pocessOwn.setFileUrl(fileUrl);
				insertVerifyFile(pocessOwn,fileUrl);
			}
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//审批流程结束
				header.setStatus("2");
				header.setUpdateDate(new Date());
				tradeVerifyHeaderMapper.update(header);
				//删除剩余流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				tradeVerifyPocessMapper.deleteOverVerify(params);
				String errorUrl = header.getVerifyError();
				json.put("url", errorUrl);
				json.put("id", header.getRelatedId());
				//发送钉钉消息
				DingDingUtils.verifyDingDingOwn(id);
			}
		}
		return json;
	}

	/**
	 * 同意审批
	 * @param id
	 * @param remark
	 * @param fileUrl 
	 */
	private JSONObject agreeVerify(String id, String remark,String loginUserId, String fileUrl) throws Exception{
		JSONObject json = new JSONObject();
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(id);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", id);
		params.put("userId", loginUserId);
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark(remark);
			pocessOwn.setEndDate(new Date());
			if(!StringUtils.isEmpty(fileUrl)){
				//pocessOwn.setFileUrl(fileUrl);
				insertVerifyFile(pocessOwn,fileUrl);
			}
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						String successUrl = header.getVerifySuccess();
						json.put("url", successUrl);
						json.put("id", header.getRelatedId());
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
		return json;
	}

	private void insertVerifyFile(TradeVerifyPocess pocessOwn, String fileUrl) {
		if(!StringUtils.isEmpty(fileUrl)){
			String[] fileArray = fileUrl.split(",");
			for(String file : fileArray){
				TradeVerifyFile verifyFile = new TradeVerifyFile();
				verifyFile.setId(ShiroUtils.getUid());
				verifyFile.setVerifyId(pocessOwn.getHeaderId());
				verifyFile.setPocessId(pocessOwn.getId());
				verifyFile.setFileUrl(file);
				verifyFile.setFileName(file.substring(file.lastIndexOf("/")+1));
				tradeVerifyFileMapper.add(verifyFile);
			}
		}
	}

	public void getDingDingVerifyDetail(Model model, String userId,
			String verifyId) {
		PrettyTime p = new PrettyTime(Constant.locale);
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(verifyId);
		SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
		//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
		String copyType = "审批通过后，通知抄送人";
		if("0".equals(verifyHeader.getCopyType())){
			copyType = "审批通过后，通知抄送人";
		}else if("1".equals(verifyHeader.getCopyType())){
			copyType = "发起时通知，通知抄送人";
		}else if("2".equals(verifyHeader.getCopyType())){
			copyType = "发起时和全部同意后，均通知抄送人";
		}
		
		header.setCreateDateFormat(p.format(header.getCreateDate()).replaceAll(" ", ""));
		//是否可以撤回
		boolean revoke = false;
		if(header.getCreateId().equals(userId)){
			header.setStartName(header.getCreateName());
			//自己创建的
			if("0".equals(header.getStatus())){
				//审批中的数据
				//详情页中不展示撤销
				// revoke = true;
			}
		}else{
			header.setStartName(header.getCreateName());
		}
		//获得审批流程
		//是否需要审批
		boolean verify = false;
		boolean verifyOwen = false;
		List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(verifyId);
		if(!pocessList.isEmpty()){
			int n = 0;
			for(TradeVerifyPocess pocess : pocessList){
				if(pocess.getUserId().equals(userId)){
					if(!"0".equals(pocess.getStatus())){
						verifyOwen = true;
					}
				}
				pocess.setStartIndext(false);
				if("0".equals(pocess.getStatus())){
					n++;
				}else{
					pocess.setStartDateFormat(p.format(pocess.getEndDate()).replaceAll(" ", ""));
				}
				if(n==1){
					//第一个待审批数据
					pocess.setStartDateFormat(p.format(pocess.getStartDate()).replaceAll(" ", ""));
					pocess.setStartIndext(true);
					if(pocess.getUserId().equals(userId)){
						//可以审批
						verify = true;
					}
				}
				//获得附件
				Map<String,Object> fileMap = new HashMap<>();
				fileMap.put("verifyId", header.getId());
				fileMap.put("pocessId", pocess.getId());
				List<TradeVerifyFile> fileList = tradeVerifyFileMapper.queryList(fileMap);
				pocess.setFileList(fileList);
			}
		}
		//获得抄送数据
		List<TradeVerifyCopy> copyList = tradeVerifyCopyMapper.queryListByHeaderId(verifyId);
		model.addAttribute("tradeHeader", header);
		model.addAttribute("revoke", revoke);
		model.addAttribute("verify", verify);
		model.addAttribute("pocessList", pocessList);
		model.addAttribute("copyList", copyList);
		model.addAttribute("copyType", copyType);
		model.addAttribute("verifyOwen", verifyOwen);
		model.addAttribute("userId", userId);
		model.addAttribute("verifyId", verifyId);
		boolean urge = false;
		if(header.getCreateId().equals(ShiroUtils.getUserId())){
			//自己发起的申请，可以催办
			urge = true;
		}
		model.addAttribute("urge", urge);
		
	}
	
	/**
	 * 根据关联id查询
	 * @param id
	 * @return
	 */
	public TradeVerifyHeader getVerifyHeaderByRelatedId(String id){
		return tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(id);
	}

	/**
	 * 审批查看共通
	 * @param model
	 * @param relatedId
	 */
	public void verifyDetailCommon(Model model, String relatedId) throws ServiceException{
		PrettyTime p = new PrettyTime(Constant.locale);
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(relatedId);
		if(header == null || "".equals(header.getId())){
			LOG.info("没有设置审批");
//			throw new ServiceException("没有设置审批");
		}else {
			SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
			if(verifyHeader != null){
				//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
				String copyType = "审批通过后，通知抄送人";
				if("0".equals(verifyHeader.getCopyType())){
					copyType = "审批通过后，通知抄送人";
				}else if("1".equals(verifyHeader.getCopyType())){
					copyType = "发起时通知，通知抄送人";
				}else if("2".equals(verifyHeader.getCopyType())){
					copyType = "发起时和全部同意后，均通知抄送人";
				}
				model.addAttribute("copyType", copyType);
			}else{
				model.addAttribute("copyType", "没有设置审批，系统默认通过");
			}
			header.setCreateDateFormat(p.format(header.getCreateDate()).replaceAll(" ", ""));
			if(header.getCreateId().equals(ShiroUtils.getUserId())){
				header.setStartName(header.getCreateName());
			}else{
				header.setStartName(header.getCreateName());
			}
			//获得审批流程
			//是否需要审批
			boolean verify = false;
			boolean verifyOwen = false;
			List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
			if(!pocessList.isEmpty()){
				int n = 0;
				for(TradeVerifyPocess pocess : pocessList){
					if(pocess.getUserId().equals(ShiroUtils.getUserId())){
						if(!"0".equals(pocess.getStatus())){
							verifyOwen = true;
						}
					}
					pocess.setStartIndext(false);
					if("0".equals(pocess.getStatus())){
						n++;
					}else{
						pocess.setStartDateFormat(p.format(pocess.getEndDate()).replaceAll(" ", ""));
					}
					if(n==1){
						//第一个待审批数据
						pocess.setStartDateFormat(p.format(pocess.getStartDate()).replaceAll(" ", ""));
						pocess.setStartIndext(true);
					}
					//获得附件
					Map<String,Object> fileMap = new HashMap<>();
					fileMap.put("verifyId", header.getId());
					fileMap.put("pocessId", pocess.getId());
					List<TradeVerifyFile> fileList = tradeVerifyFileMapper.queryList(fileMap);
					pocess.setFileList(fileList);
				}
			}
			//获得抄送数据
			List<TradeVerifyCopy> copyList = tradeVerifyCopyMapper.queryListByHeaderId(header.getId());
			model.addAttribute("tradeHeader", header);
			model.addAttribute("verify", verify);
			model.addAttribute("pocessList", pocessList);
			model.addAttribute("copyList", copyList);
			model.addAttribute("verifyOwen", verifyOwen);
			boolean urge = false;
			if(header.getCreateId().equals(ShiroUtils.getUserId())){
				//自己发起的申请，可以催办
				urge = true;
			}
			model.addAttribute("urge", urge);
		}
	}

	/**
	 * 未审批的数据，发送钉钉消息提醒
	 */
	public void sendDingDingMessageTime() {
		long nd = 1000 * 24 * 60 * 60;
	    long nh = 1000 * 60 * 60;
		//获得所有未审批的数据
		Map<String,Object> params = new HashMap<>();
		params.put("status", 0);
		List<TradeVerifyHeader> headerList = tradeVerifyHeaderMapper.queryList(params);
		if(headerList != null && headerList.size() > 0){
			Date systemDate = new Date();
			for(TradeVerifyHeader header : headerList){
				//获得待审批人员
				TradeVerifyPocess pocess = tradeVerifyPocessMapper.getApprovalPocess(header.getId());
				if(pocess==null){
					continue;
				}
				Date creadeDate = pocess.getStartDate();
				// 计算差多少小时
				long diff = systemDate.getTime() - (pocess.getLastDate()==null?creadeDate.getTime():pocess.getLastDate().getTime());
				long hour = diff % nd / nh;
				if(hour >= Constant.TRADEVERIFYTIME){
					//发送钉钉消息
					SysUser user = sysUserService.selectByPrimaryKey(pocess.getUserId());
					DingDingUtils.verifyDingDingMessageTime(user, header);
					pocess.setLastDate(systemDate);
					tradeVerifyPocessMapper.update(pocess);
				}
			}
		}
	}

	/**
	 * 催办审批
	 * @param String
	 * @return
	 */
	public JSONObject urgeTrade(String tradeId) {
		JSONObject json = new JSONObject();
		String loginUserId = ShiroUtils.getUserId();
		TradeVerifyHeader header = tradeVerifyHeaderMapper.get(tradeId);
		if(!loginUserId.equals(header.getCreateId())){
			json.put("flag", false);
			json.put("msg", "不是自己发起的申请无法催办");
			return json;
		}
		if(!ObjectUtil.isEmpty(header)){
			//获得待审批人员
			TradeVerifyPocess pocess = tradeVerifyPocessMapper.getApprovalPocess(header.getId());
			if(pocess==null){
				json.put("flag", false);
				json.put("msg", "获取审批数据失败");
				return json;
			}
			//发送钉钉消息
			SysUser user = sysUserService.selectByPrimaryKey(pocess.getUserId());
			DingDingUtils.verifyDingDingMessageTime(user, header);
			pocess.setLastDate(new Date());
			tradeVerifyPocessMapper.update(pocess);
			json.put("flag", true);
			json.put("msg", "已发送钉钉消息");
		}else{
			json.put("flag", false);
			json.put("msg", "获取审批数据失败");
			return json;
		}
		return json;
	}
}
