package com.nuotai.trading.model;

import java.util.Date;

import lombok.Data;

@Data
public class BuyCategory {
    private String id;

    private String companyId;
    //编号，01/0101/010101级别关系
    private String categoryCode;
    //名称
    private String name;
    //级别
    private String level;
    //上级类目id
    private String parentId;
    //状态 N-删除;Y-正常
    private String status;

    private String createId;

    private String createName;

    private String updateId;

    private Date createDate;

    private String updateName;

    private Date updateDate;

    private String delId;

    private String delName;

    private Date delDate;

}