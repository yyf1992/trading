package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-09-13 14:52:26
 */
@Data
public class BuyWarehouseProdcutsBatch implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//订单号
	private String orderId;
	//发货单号
	private String deliveryNo;
	//批次编号
	private String batchNo;
	//仓库Id
	private String warehouseId;
	//店铺id
	private String shopId;
	//店铺code
	private String shopCode;
	//店铺名称
	private String shopName;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//颜色
	private String color;
	//条形码
	private String barcode;
	//价格
	private BigDecimal price;
	//总库存（可用库存+已用库存）
	private Integer stocks;
	//实际库存（可用库存+锁定库存）
	private Integer factStock;
	//可用库存
	private Integer usableStock;
	//已用库存
	private Integer usedStock;
	//锁定库存
	private Integer lockStock;
	//备注
	private String remark;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人名称
	private String delName;
	//删除时间
	private Date delDate;
}
