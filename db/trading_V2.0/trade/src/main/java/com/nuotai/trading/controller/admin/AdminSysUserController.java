package com.nuotai.trading.controller.admin;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.PageAdmin;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 人员管理
 * @author wxx
 *
 */
@Controller
@RequestMapping("admin/adminSysUser")
public class AdminSysUserController extends BaseController {
	@Autowired
    private SysUserService sysUserService;
	
	/**
	 * 加载列表页面
	 * @return
	 */
	@RequestMapping(value = "/loadUserList.html", method = RequestMethod.GET)
	public String loadUserList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPageAdmin()==null){
			searchPageUtil.setPageAdmin(new PageAdmin());
		}
		searchPageUtil.setObject(params);
		List<Map<String, Object>> sysUserList = sysUserService.selectByPage(searchPageUtil);
		searchPageUtil.getPageAdmin().setList(sysUserList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "admin/sysuser/sysUserList";
	}
	
	/**
	 * 加载修改页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSysUser.html", method = RequestMethod.GET)
	public String updateSysUser(String id) throws Exception {
		SysUser sysUser = sysUserService.selectByPrimaryKey(id);
		//角色信息
		model.addAttribute("sysUser", sysUser);
		return "admin/sysuser/updateSysUser";
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdate(SysUser sysUser) {
		JSONObject json = new JSONObject();
		SysUser loginUser = (SysUser)ShiroUtils.getSession().getAttribute(Constant.CURRENT_USER);
		sysUser.setUpdateId(loginUser.getId());
		sysUser.setUpdateName(loginUser.getUserName());
		sysUser.setUpdateDate(new Date());
		int result = sysUserService.updateByPrimaryKeySelective(sysUser);
		if (result > 0) {
			json.put("success", true);
			json.put("msg", "成功！");
		} else {
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json.toString();
	}

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	@RequestMapping(value = "/deleteSysUser.html", method = RequestMethod.GET)
	@ResponseBody
	public String deleteSysUser(String ids) {
		JSONObject json = new JSONObject();
		try {
			sysUserService.deleteUser(ids);
			json.put("success", true);
			json.put("msg", "删除成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "删除失败！");
		}
		return json.toString();
	}
	
	/**
	 * 加载设置角色页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/instalSysUserRole.html", method = RequestMethod.GET)
	public String instalSysUserRole(String id) {
		SysUser sysUser = sysUserService.selectByPrimaryKey(id);
		model.addAttribute("sysUser", sysUser);
		return "admin/sysuser/instalSysUserRole";
	}
}
