package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyCompanyService {
	@Autowired
	private BuyCompanyMapper mapper;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private AttachmentService attachmentService;

	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyCompany record) {
		return mapper.insert(record);
	}

	public int insertSelective(BuyCompany record) {
		return mapper.insertSelective(record);
	}

	public BuyCompany selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuyCompany record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuyCompany record) {
		return mapper.updateByPrimaryKey(record);
	}

	/**
	 * 注册保存
	 * @param user
	 * @param company
	 */
	/**
	 * @param user
	 * @param company
	 * @param mapPare
	 * @throws Exception
	 */
	public void subRegister (SysUser user,BuyCompany company,Map<String ,Object> mapPare) throws Exception{
		Date date = new Date();
		Map<String ,Object> map = new HashMap<String, Object>();
		map.put("companyName", company.getCompanyName());
		map.put("notEqualsStatus", "2");
		List<BuyCompany> coompanyList = mapper.selectByMap(map);
		if(coompanyList != null && coompanyList.size() > 0){
			throw new Exception("该公司名称已存在！");
		}
		//重新注册
		SysUser u = sysUserService.queryByLoginName(user.getLoginName());
		if(u != null){
			BuyCompany c = mapper.selectByPrimaryKey(u.getCompanyId());
			if(c.getStatus() == 2){ // 未通过，做修改操作
				company.setId(c.getId());
				company.setStatus(0);// 待审核
				company.setCreateDate(date);
				mapper.updateByPrimaryKeySelective(company);
				user.setId(u.getId());
				user.setPassword(ShiroUtils.getMD5(user.getPassword(), "UTF-8", 0).toLowerCase());
				sysUserService.updateByPrimaryKeySelective(user);
				attachmentService.deleteByRelatedId(c.getId());
				// 附件表添加
				attachmentService.addAttachment(mapPare, company.getId());
			}else if(c.getStatus() == 1){
				throw new Exception("该手机号已审核通过，不允许再次注册！");
			}
		}else{//新注册
			company.setId(ShiroUtils.getUid());
			company.setStatus(0);// 待审核
			company.setIsDel(Constant.IsDel.NODEL.getValue()); // 未删除
			company.setCreateId("admin");
			company.setCreateName("admin");
			company.setCreateDate(date);
			mapper.insert(company);
			user.setId(ShiroUtils.getUid());
			user.setCompanyId(company.getId());
			user.setUserName(company.getLinkMan());
			user.setPassword(ShiroUtils.getMD5(user.getPassword(), "UTF-8", 0).toLowerCase());
			user.setParentId("");
			user.setIsParent(1);// 是父类
			user.setIsDel(Constant.IsDel.NODEL.getValue());// 未删除
			user.setCreateId("admin");
			user.setCreateName("admin");
			user.setCreateDate(date);
			sysUserService.insert(user);
			// 附件表添加
			attachmentService.addAttachment(mapPare, company.getId());
		}
	}

	public List<BuyCompany> selectByMap(Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	public List<BuyCompany> selectByPage(SearchPageUtil searchPageUtil) {
		return mapper.selectByPage(searchPageUtil);
	}
	
	/**
	 * 审核公司保存
	 * @param company
	 * @throws Exception
	 */
	public void saveVerifyCompany(BuyCompany company) throws Exception{
		Date date = new Date();
		company.setVerifyDate(date);// 审核时间
		mapper.updateByPrimaryKeySelective(company);
	}
	
	/**
	 * 修改公司isdel
	 * @param id
	 * @throws Exception
	 */
	public void updateCompanyIsDel(String id,String isDel) throws Exception{
			BuyCompany company = new BuyCompany();
			company.setId(id);
			company.setIsDel(Integer.parseInt(isDel));
			mapper.updateByPrimaryKeySelective(company);
			// 所属公司下的人员也修改
			List<SysUser> userList = sysUserService.selectByCompanyId(company.getId());
			if(userList != null && userList.size() > 0){
				for(SysUser user : userList){
					user.setIsDel(Integer.parseInt(isDel));
					sysUserService.updateByPrimaryKeySelective(user);
				}
			}
	}
	
	/**
	 * 修改个人资料保存
	 * @param company
	 * @throws Exception
	 */
	public void saveUpdateCompany(BuyCompany company,Map<String,Object> map) throws Exception{
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Date date = new Date();
		company.setUpdateId(user.getId());
		company.setUpdateName(user.getUpdateName());
		company.setUpdateDate(date);
		mapper.updateByPrimaryKeySelective(company);
		Map<String,Object> mapA = new HashMap<String, Object>();
		mapA.put("relatedId", company.getId());
		mapA.put("type", "2");
		List<BuyAttachment> aList = attachmentService.selectByMap(mapA);
		if(aList != null && aList.size() > 0){
			for(BuyAttachment a : aList){
				attachmentService.deleteByPrimaryKey(a.getId());
			}
		}
		// 附件表添加
		attachmentService.addAttachment(map, company.getId());
	}
	
	public void vailidateCompany(BuyCompany buyCompany) throws Exception{
		Map<String ,Object> map = new HashMap<String, Object>();
		map.put("companyName", buyCompany.getCompanyName());
		List<BuyCompany> coompanyList = mapper.selectByMap(map);
		if(coompanyList != null && coompanyList.size() > 0){
			throw new Exception("该公司名称已存在！");
		}
		
	}
	public void saveInsert(BuyCompany buyCompany) throws Exception{
		vailidateCompany(buyCompany);
		mapper.insertSelective(buyCompany);
	}
}
