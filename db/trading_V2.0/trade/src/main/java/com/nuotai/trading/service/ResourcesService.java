package com.nuotai.trading.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.dao.ResourcesMapper;
import com.nuotai.trading.model.ResQueue;
import com.nuotai.trading.model.Resources;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 资源表信息
 * @ClassName ResourcesServiceImpl
 * @author dxl
 * @Date 2017年7月25日
 * @version 1.0.0
 */
@Service
public class ResourcesService implements ResourcesMapper {
	
	@Autowired
	private ResourcesMapper resourcesDao;
	@Autowired
	private ResQueueService resQueueService;

	@Override
	public List<Resources> getBeanList(Resources resources) {
		return resourcesDao.getBeanList(resources);
	}

	@Override
	public void addBean(Resources resources) {
		resourcesDao.addBean(resources);
	}

	@Override
	public void updateBean(Resources resources) {
		resourcesDao.updateBean(resources);
	}

	@Override
	public void deleteBean(String id) {
		resourcesDao.deleteBean(id);
	}

	@Transactional
	public void addResoucesAndDeleteResQueue(ResQueue resQueue) {
		// 资源表添加资源信息
		Resources resources = new Resources();
		resources.setId(new ShiroUtils().getUid());
		resources.setAddDateTime(new Date());
		resources.setFileName(resQueue.getFileName());
		resources.setResId(resQueue.getResId());
		resources.setSourceUrl(resQueue.getUrl());
		resources.setHtmlUrl(resQueue.getHtmlUrl());
		resources.setImgUrl(resQueue.getImgUrl());
		resources.setTxtType(resQueue.getTxtType());
		resourcesDao.addBean(resources);
		// 删除队列表信息
		resQueueService.deleteBean(resQueue.getId());
	}

}
