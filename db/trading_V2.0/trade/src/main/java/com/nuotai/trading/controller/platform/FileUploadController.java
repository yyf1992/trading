package com.nuotai.trading.controller.platform;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import org.json.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.exception.CommException;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.UplaodUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * 异步上传文件
 * @author Administrator
 *
 */
@Controller
@RequestMapping("platform/file")
public class FileUploadController extends BaseController{
	
	@RequestMapping("/upload")
	@ResponseBody
	public String upload(@RequestParam(required=true) MultipartFile file){
		JSONObject json = new JSONObject();
		
		
		if(file==null){
			json.put("result", "fail");
			json.put("msg", "不能传入空文件");
			return json.toString();
		}
		// 文件名称生成策略  
        try {
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");  
			String format = df.format(new Date());  
			// 防止同一时间上传图片，不能用时间作为区分  
			// 用随机数  \\\\\\\\
			Random r = new Random();  
			for (int i = 0; i < 3; i++) {  
			    // 产生100以内的随机数  
			    format += r.nextInt(100);  
			}  
			String fileExtention = FilenameUtils.getExtension(file.getOriginalFilename());
			// 图片保存数据库路径  
			String path = "upload/"+format+"."+fileExtention;  
  
			// 另外一台服务器地址  
			String url = Constant.FILE_SERVER_URL;
			url = url+path;
			// 实例化jersey  
			Client c = new Client();  
			// 设置请求路径  
			WebResource wr = c.resource(url);  
			wr.put(String.class, file.getBytes());  
  
			System.out.println("发送成功!");  
			json.put("result", "success");
		} catch (Exception e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", e.toString());
		}
		return json.toString();
	}
	
	@RequestMapping("/toFileUpload")
	public String toFileUpload(){
		return "fileUpload";
	}
	/**
	 * 日常费用申请为费用明细添加附件
	 * @param MultipartFile file:上传的文件
	 * @return
	 */
	@RequestMapping(value="/uploadRegAttachment",method=RequestMethod.POST)
	@ResponseBody
	public String uploadRegAttachment(HttpServletRequest request,@RequestParam(value = "file", required = false) MultipartFile file)
			throws Exception{
		
		JSONObject json = new JSONObject();
		try {
			//获取文件名
			String fileName = file.getOriginalFilename();
			json.put("fileName", fileName);
			//获取文件后缀名
			String fileExtensionName = FilenameUtils.getExtension(fileName);
			/*if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
					&& !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")
					&& !fileExtensionName.equalsIgnoreCase("XLS") && !fileExtensionName.equalsIgnoreCase("XLSX")){
				json.put("result", "fail");
				json.put("msg", "上传文件格式不正确，请上传JPG、GIF、PNG、JPEG、XLS、XLSX格式！");
				return json.toString();
			}*/
			json.put("fileExtensionName", fileExtensionName);
			//获取文件大小
			String fileSize = ShiroUtils.getInstance().convertFileSize(file.getSize());
			json.put("fileSize", fileSize);
			UplaodUtil uplaodUtil = new UplaodUtil();
			String url = uplaodUtil.uploadFile(file,null,true);
			json.put("filePath", url);
			json.put("result", "success");
		} catch (UniformInterfaceException e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		} catch (ClientHandlerException e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		} catch (CommException e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", e.getMessage());
			return json.toString();
		} catch (IOException e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		}
		return json.toString();
	}
	
	/**
	 * 加载附件div
	 * @author dxl
	 * @param String url : 附件url
	 */
	@RequestMapping(value= "/loadAttachment", method = RequestMethod.GET)
	public String loadAttachment(String url){
		model.addAttribute("url", url);
		return "platform/attachment";
	}

}
