package com.nuotai.trading.utils.caigou;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TradeVerifyFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TradeVerifyFile verifyFile = new TradeVerifyFile();
		try {
			verifyFile.dealWith();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void dealWith() throws Exception{
		Connection tradeConn = DBUtils.getTradeConnection();
		List<Map<String,Object>> dataList = getPocessData(tradeConn);
		int length = dataList.size();
		for(Map<String,Object> datalMap : dataList){
			System.out.println("剩余："+length+"条数据,总计："+dataList.size());
			insertData(datalMap,tradeConn);
			length--;
		}
		DBUtils.closeConnection(tradeConn);
	}
	
	private void insertData(Map<String, Object> datalMap, Connection tradeConn) throws Exception{
		StringBuffer insertHeaderSql = new StringBuffer("INSERT INTO `trade_verify_file` VALUES (");
		insertHeaderSql.append("'"+datalMap.get("id")+"',");//id
		insertHeaderSql.append("'"+datalMap.get("header_id")+"',");
		String fileUrl = datalMap.get("file_url").toString();
		insertHeaderSql.append("'"+fileUrl.substring(fileUrl.lastIndexOf("/")+1)+"',");
		insertHeaderSql.append("'"+fileUrl+"',");
		insertHeaderSql.append("'"+datalMap.get("id")+"'");//id
		insertHeaderSql.append(");");
		PreparedStatement insertPs = tradeConn.prepareStatement(insertHeaderSql.toString());
		insertPs.executeUpdate();
	}
	private List<Map<String, Object>> getPocessData(Connection tradeConn) throws Exception{
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		String selectSql = "SELECT * FROM trade_verify_pocess WHERE file_url IS NOT NULL;";
		PreparedStatement ps = tradeConn.prepareStatement(selectSql);
		ResultSet rs = ps.executeQuery();
		// 取得结果集列数
		while (rs.next()) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("id", rs.getString("id"));
			map.put("header_id", rs.getString("header_id"));
			map.put("file_url", rs.getString("file_url"));
			dataList.add(map);
		}
		// 关闭记录集
		DBUtils.closeResultSet(rs);
		// 关闭声明
		DBUtils.closeStatement(ps);
		return dataList;
	}
}
