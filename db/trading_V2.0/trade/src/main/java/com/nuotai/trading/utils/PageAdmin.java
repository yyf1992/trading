package com.nuotai.trading.utils;

import java.util.ArrayList;
import java.util.List;


/**
 * 管理后台分页类
 * @author wxx
 *
 */
public class PageAdmin {
	private int pageNo = Integer.parseInt(Constant.INIT_PAGE_NO); // 当前页码
	// 页面大小，设置为“-1”表示不进行分页（分页无效）
	private int pageSize = Integer.parseInt(Constant.INIT_PAGE_SIZE);
	private long count;// 总记录数，设置为“-1”表示不查询总数
	private String divId = "content";

	private int first;// 首页索引
	private int last;// 尾页索引
	private int prev;// 上一页索引
	private int next;// 下一页索引

	private boolean firstPage;// 是否是第一页
	private boolean lastPage;// 是否是最后一页

	private List list;

	private String funcName = "pageAdmin"; // 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。

	public PageAdmin() {
		// this.pageSize = -1;
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 */
	public PageAdmin(int pageNo, int pageSize) {
		this(pageNo, pageSize, 0);
	}

	public PageAdmin(int pageNo, int pageSize, String divId) {
		this(pageNo, pageSize, 0, divId);
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 */
	public PageAdmin(int pageNo, int pageSize, long count) {
		this(pageNo, pageSize, count, new ArrayList());
	}

	public PageAdmin(int pageNo, int pageSize, long count, String divId) {
		this(pageNo, pageSize, count, divId, new ArrayList());
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 * @param list
	 *            本页数据对象列表
	 */
	public PageAdmin(int pageNo, int pageSize, long count, List list) {
		this.setCount(count);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.setList(list);
	}

	public PageAdmin(int pageNo, int pageSize, long count, String divId, List list) {
		this.setCount(count);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.divId = divId;
		this.setList(list);
	}

	/**
	 * 初始化参数
	 */
	public void initialize() {

		// 1
		this.first = 1;

		this.last = (int) (count / (this.pageSize < 1 ? 20 : this.pageSize)
				+ first - 1);

		if (this.count % this.pageSize != 0 || this.last == 0) {
			this.last++;
		}

		if (this.last < this.first) {
			this.last = this.first;
		}

		if (this.pageNo <= 1) {
			this.pageNo = this.first;
			this.firstPage = true;
		}

		if (this.pageNo >= this.last) {
			this.pageNo = this.last;
			this.lastPage = true;
		}

		if (this.pageNo < this.last - 1) {
			this.next = this.pageNo + 1;
		} else {
			this.next = this.last;
		}

		if (this.pageNo > 1) {
			this.prev = this.pageNo - 1;
		} else {
			this.prev = this.first;
		}

		// 2
		if (this.pageNo < this.first) {// 如果当前页小于首页
			this.pageNo = this.first;
		}

		if (this.pageNo > this.last) {// 如果当前页大于尾页
			this.pageNo = this.last;
		}

		if (this.divId == null || "".equals(this.divId)) {
			this.divId = "content";
		}

	}

	/**
	 * 默认输出当前分页标签 <div class="page">${page}</div>
	 */
	@Override
	public String toString() {
		String functionName = funcName + divId;
		initialize();
		StringBuilder sb = new StringBuilder();
		// js代码
		sb.append("<script type='text/javascript'>");
		sb.append("function pageAdmin" + divId + "(n, s) {");
		if("content".equals(divId)){
			sb.append("$('#pageNo').val(n);");
			sb.append("$('#pageSize').val(s);");
			sb.append("loadAdminData();}");
		}else{
			sb.append("$('#" + divId + "').find('#pageNo').val(n);");
			sb.append("$('#" + divId + "').find('#pageSize').val(s);");
			sb.append("loadAdminData('" + divId + "');}");
		}
		sb.append("</script>");
		// 分页标签代码
		sb.append("<div class='fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix' style='height: 40px'>");
		if ("content".equals(divId)) {
			// 条数select
			sb.append("<div id='DataTables_Table_0_length' class='dataTables_length pull-left'>");
			sb.append("<label>显示");
			sb.append("<select style='width:60px' onchange='" + functionName
					+ "(" + 1 + ",this.value);'>");
			sb.append(" <option value='10' "
					+ (pageSize == 10 ? "selected='selected'" : "")
					+ ">10</option>");
			sb.append(" <option value='25'"
					+ (pageSize == 25 ? "selected='selected'" : "")
					+ " >25</option>");
			sb.append(" <option value='50'"
					+ (pageSize == 50 ? "selected='selected'" : "")
					+ ">50</option>");
			sb.append(" <option value='100' "
					+ (pageSize == 100 ? "selected='selected'" : "")
					+ ">100</option>");
			sb.append(" </select>");
			sb.append("条 ，共 " + count + " 条</label>");
			sb.append("</div>");
		}
		// 分页内容
		sb.append("<div class='dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers'");
		sb.append("id='DataTables_Table_0_paginate'>");
		if (pageNo != first) {
			// 如果不是第一页,可以点击
			if ("content".equals(divId)) {
				// 第一页按钮
				sb.append("<a tabindex='0' href='javascript:void(0)' class='first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default'");
				sb.append("id='DataTables_Table_0_first' onclick='"
						+ functionName + "(1," + pageSize + ");'>第一页</a>");
			}
			// 上一页
			sb.append("<a tabindex='0' href='javascript:void(0)' class='previous fg-button ui-button ui-state-default'");
			sb.append("id='DataTables_Table_0_previous' onclick='"
					+ functionName + "(" + (pageNo - 1) + "," + pageSize
					+ ");'>上一页</a>");
		} else {
			// 如果是第一页,不可以点击
			if ("content".equals(divId)) {
				// 第一页按钮
				sb.append("<a tabindex='0' href='javascript:void(0)' class='first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled'");
				sb.append("id='DataTables_Table_0_first'>第一页</a>");
			}
			// 上一页
			sb.append("<a tabindex='0' href='javascript:void(0)' class='previous fg-button ui-button ui-state-default ui-state-disabled'");
			sb.append("id='DataTables_Table_0_previous'>上一页</a>");
		}
		sb.append("<span>");
		if ("content".equals(divId)) {
			if (last <= 20) {
				// 中间分页个数
				for (int i = 1; i <= last; i++) {
					if (pageNo == i) {
						sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
								+ i + "</a>");
					} else {
						sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
								+ functionName
								+ "("
								+ i
								+ ","
								+ pageSize
								+ ");'>" + i + "</a>");
					}
				}
			} else {
				if (pageNo > 15) {
					if ((last - pageNo) > 5) {
						for (int i = (pageNo - 14); i <= (pageNo + 5); i++) {
							if (pageNo == i) {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
										+ i + "</a>");
							} else {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
										+ functionName
										+ "("
										+ i
										+ ","
										+ pageSize + ");'>" + i + "</a>");
							}
						}
					} else {
						for (int i = (last - 19); i <= last; i++) {
							if (pageNo == i) {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
										+ i + "</a>");
							} else {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
										+ functionName
										+ "("
										+ i
										+ ","
										+ pageSize + ");'>" + i + "</a>");
							}
						}
					}
				} else {
					// 中间分页个数
					for (int i = 1; i <= 20; i++) {
						if (pageNo == i) {
							sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
									+ i + "</a>");
						} else {
							sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
									+ functionName
									+ "("
									+ i
									+ ","
									+ pageSize
									+ ");'>" + i + "</a>");
						}
					}
				}
			}
		} else {
			if (last <= 5) {
				// 中间分页个数
				for (int i = 1; i <= last; i++) {
					if (pageNo == i) {
						sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
								+ i + "</a>");
					} else {
						sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
								+ functionName
								+ "("
								+ i
								+ ","
								+ pageSize
								+ ");'>" + i + "</a>");
					}
				}
			} else {
				if (pageNo > 3) {
					if ((last - pageNo) > 2) {
						for (int i = (pageNo - 2); i <= (pageNo + 2); i++) {
							if (pageNo == i) {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
										+ i + "</a>");
							} else {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
										+ functionName
										+ "("
										+ i
										+ ","
										+ pageSize + ");'>" + i + "</a>");
							}
						}
					} else {
						for (int i = (last - 4); i <= last; i++) {
							if (pageNo == i) {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
										+ i + "</a>");
							} else {
								sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
										+ functionName
										+ "("
										+ i
										+ ","
										+ pageSize + ");'>" + i + "</a>");
							}
						}
					}
				} else {
					// 中间分页个数
					for (int i = 1; i <= 5; i++) {
						if (pageNo == i) {
							sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default ui-state-disabled'>"
									+ i + "</a>");
						} else {
							sb.append("<a tabindex='0' class='fg-button ui-button ui-state-default' onclick='"
									+ functionName
									+ "("
									+ i
									+ ","
									+ pageSize
									+ ");'>" + i + "</a>");
						}
					}
				}
			}
		}

		sb.append("</span>");
		if (pageNo != last) {
			// 如果不是最后一页,可以点击
			// 下一页
			sb.append("<a tabindex='0' href='javascript:void(0)' class='next fg-button ui-button ui-state-default'");
			sb.append("id='DataTables_Table_0_next' onclick='" + functionName
					+ "(" + (pageNo + 1) + "," + pageSize + ");'>下一页</a>");
			if ("content".equals(divId)) {
				// 最后一页
				sb.append("<a tabindex='0' href='javascript:void(0)' class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default'");
				sb.append("id='DataTables_Table_0_last' onclick='"
						+ functionName + "(" + last + "," + pageSize
						+ ");'>最后一页</a>");
			}
		} else {
			// 如果是最后一页,不可以点击
			// 下一页
			sb.append("<a tabindex='0' href='javascript:void(0)' class='next fg-button ui-button ui-state-default ui-state-disabled'");
			sb.append("id='DataTables_Table_0_next'>下一页</a>");
			if ("content".equals(divId)) {
				// 最后一页
				sb.append("<a tabindex='0' href='javascript:void(0)' class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default ui-state-disabled'");
				sb.append("id='DataTables_Table_0_last'>最后一页</a>");
			}
		}
		sb.append("</div>");
		sb.append("</div>");
		return sb.toString();
	}

	/**
	 * 获取分页HTML代码
	 * 
	 * @return
	 */
	public String getHtml() {
		return toString();
	}

	/**
	 * 获取设置总数
	 * 
	 * @return
	 */
	public long getCount() {
		return count;
	}

	/**
	 * 设置数据总数
	 * 
	 * @param count
	 */
	public void setCount(long count) {
		this.count = count;
		if (pageSize >= count) {
			pageNo = 1;
		}
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getPageNo() {

		return pageNo;
	}

	/**
	 * 设置当前页码
	 * 
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo < 0 ? 1 : pageNo;
	}

	/**
	 * 获取页面大小
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小（最大500）
	 * 
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;// > 500 ? 500 : pageSize;
	}

	/**
	 * 首页索引
	 * 
	 * @return
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * 尾页索引
	 * 
	 * @return
	 */
	
	public int getLast() {
		return last;
	}

	/**
	 * 获取页面总数
	 * 
	 * @return getLast();
	 */
	
	public int getTotalPage() {
		return getLast();
	}

	/**
	 * 是否为第一页
	 * 
	 * @return
	 */
	
	public boolean isFirstPage() {
		return firstPage;
	}

	/**
	 * 是否为最后一页
	 * 
	 * @return
	 */
	
	public boolean isLastPage() {
		return lastPage;
	}

	/**
	 * 上一页索引值
	 * 
	 * @return
	 */
	
	public int getPrev() {
		if (isFirstPage()) {
			return pageNo;
		} else {
			return pageNo - 1;
		}
	}

	/**
	 * 下一页索引值
	 * 
	 * @return
	 */
	
	public int getNext() {
		if (isLastPage()) {
			return pageNo;
		} else {
			return pageNo + 1;
		}
	}

	/**
	 * 获取本页数据对象列表
	 * 
	 * @return List<T>
	 */
	public List getList() {
		return list;
	}

	/**
	 * 设置本页数据对象列表
	 * 
	 * @param list
	 */

	public PageAdmin setList(List list) {
		this.list = list;
		return this;
	}

	/**
	 * 获取点击页码调用的js函数名称 function ${page.funcName}(pageNo){location=
	 * "${ctx}/list-${category.id}${urlSuffix}?pageNo="+i;}
	 * 
	 * @return
	 */
	
	public String getFuncName() {
		return funcName;
	}

	/**
	 * 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。
	 * 
	 * @param funcName
	 *            默认为page
	 */
	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	/**
	 * 分页是否有效
	 * 
	 * @return this.pageSize==-1
	 */
	
	public boolean isDisabled() {
		return this.pageSize == -1;
	}

	/**
	 * 是否进行总数统计
	 * 
	 * @return this.count==-1
	 */
	
	public boolean isNotCount() {
		return this.count == -1;
	}

	/**
	 * 获取 Hibernate FirstResult
	 */
	public int getFirstResult() {
		int firstResult = (getPageNo() - 1) * getPageSize();
		if (firstResult >= getCount()) {
			firstResult = 0;
		}
		return firstResult;
	}

	public int getLastResult() {
		int lastResult = getFirstResult() + getMaxResults();
		if (lastResult > getCount()) {
			lastResult = (int) getCount();
		}
		return lastResult;
	}

	/**
	 * 获取 Hibernate MaxResults
	 */
	public int getMaxResults() {
		return getPageSize();
	}

	public String getDivId() {
		return divId;
	}

	public void setDivId(String divId) {
		this.divId = divId;
	}
}
