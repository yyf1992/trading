package com.nuotai.trading.controller.platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 用户管理
 * @author dxl
 * 
 */

@Controller
@RequestMapping("platform/sysUser")
public class SysUserController extends BaseController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private BuyCompanyService buyCompanyService;

	/**
	 * 首页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "systemIndex")
	public String systemIndex() throws Exception {
		return "platform/system/systemIndex";
	}
	
	/**
	 * 加载修改密码页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "updatePassword")
	public String updateSysUser() throws Exception {
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		model.addAttribute("sysUser", user);
		return "platform/system/updatePassword";
	}

	/**
	 * 修改密码保存
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/saveUpdatePassword", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdate(String sysUserId,String passwordOld,String password) {
		JSONObject json = new JSONObject();
		SysUser sysUser = sysUserService.selectByPrimaryKey(sysUserId);
		if(!sysUser.getPassword().equals(ShiroUtils.getMD5(passwordOld, "UTF-8", 0).toLowerCase())){
			json.put("success", false);
			json.put("msg", "原密码不正确！");
		}else{
			int result = 0;
			sysUser.setPassword(ShiroUtils.getMD5(password, "UTF-8", 0).toLowerCase());
			result = sysUserService.updateByPrimaryKeySelective(sysUser);
			if (result > 0) {
				json.put("success", true);
				json.put("msg", "修改成功！");
			} else {
				json.put("success", false);
				json.put("msg", "修改失败！");
			}
		}
		return json.toString();
	}
	
	/**
	 * 个人资料
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "personalData")
	public String personalData() throws Exception {
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		// 公司信息
		BuyCompany company = buyCompanyService.selectByPrimaryKey(user.getCompanyId());
		model.addAttribute("company", company);
		return "platform/system/personalData";
	}
	
	/**
	 * 加载人员管理页面
	 * @param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loadPersonList")
	public String getPersonList(SearchPageUtil searchPageUtil,@RequestParam Map<String, Object> param) throws Exception {
		return "platform/system/person/personList";
	}
	
	/**
	 * 获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<SysUser> salePlanList = sysUserService.getCompanyUser(params);
				return salePlanList;
			}
		});
	}
	
	/**
	 * 加载修改人员界面
	 * @return
	 */
	@RequestMapping(value="loadEditPerson",method=RequestMethod.POST)
	public String editSysUser(String id){
		SysUser user=sysUserService.selectByPrimaryKey(id);
		model.addAttribute("person",user);
		return "platform/system/person/editPerson";
	}
	/**
	 * 保存修改人员
	 * @return
	 */
	@RequestMapping(value="updateSysUser",method=RequestMethod.POST)
	@ResponseBody
	public String saveEdit(SysUser sysUser){
		JSONObject json=sysUserService.saveEdit(sysUser);
		return json.toString();
	}
	
	/**
	 * 加载新增人员界面
	 * @return
	 */
	@RequestMapping(value="loadAddSysUser",method=RequestMethod.POST)
	public String loadAddSysUser(){
		return "platform/system/person/addPerson";
	}
	
	/**
	 * 保存新增人员
	 * @param sysUser
	 * @return
	 */
	@RequestMapping(value="saveAdd",method=RequestMethod.POST)
	@ResponseBody
	public String saveAdd(SysUser sysUser){
		JSONObject json=sysUserService.saveAdd(sysUser);
		return json.toString();
	}
	/**
	 * 删除账号
	 * @return
	 */
	@RequestMapping(value="deleteSysUser",method=RequestMethod.POST)
	@ResponseBody
	public String deleteSysUser(String id){
		JSONObject json=new JSONObject();
		int result=0;
		result=sysUserService.deleteByPrimaryKey(id);
		if(result>0){
			json.put("success", true);
			json.put("msg", "删除成功!");
		}else{
			json.put("success", false);
			json.put("msg", "删除失败!");
		}
		return json.toString();
	}
	/**
	 *消息列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "messageList")
	public String messageList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params)  {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("tabId")||params.get("tabId")==null){
			params.put("tabId", "0");
		}
		//获取系统消息和好友申请条数
		sysUserService.getMessageListParams(params);
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/system/messageList";
	}

	/**
	 * 加载消息列表数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadMessageData")
	public String loadMessageData(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = sysUserService.loadMessageData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 多选获得人员
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="multiUser",method=RequestMethod.GET)
	@ResponseBody
	public JSONObject multiUser(String userId){
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(userId)){
			String[] userIdArray = userId.split("-");
			Map<String,Object> params = new HashMap<>();
			params.put("companyId", ShiroUtils.getCompId());
			params.put("userIdArray", userIdArray);
			List<SysUser> userList = sysUserService.getCompanyUser(params);
			json.put("flag", true);
			json.put("userList", userList);
		}else{
			json.put("flag", false);
			json.put("userList", null);
		}
		return json;
	}
}
