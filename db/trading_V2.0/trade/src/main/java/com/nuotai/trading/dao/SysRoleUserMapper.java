package com.nuotai.trading.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.model.SysRoleUser;
import com.nuotai.trading.utils.SearchPageUtil;

public interface SysRoleUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);
    
    int deleteByRoleId(@Param("idList")String[] idList);

	List<SysRoleUser> selectRightByPageUser(SearchPageUtil searchPageUtil);

	int deleteByIdList(@Param("idList")String[] idStr);
	
	List<String> selectRoleIdByUserId(String user_id);
}