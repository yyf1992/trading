package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyWarehouseBacklogDetails;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 库零分析明细表
 * 
 * @author "
 * @date 2017-09-15 17:01:03
 */
public interface BuyWarehouseBacklogDetailsMapper extends BaseDao<BuyWarehouseBacklogDetails> {
	
	//分页查询
	List<BuyWarehouseBacklogDetails> selectProdcutsByPage(SearchPageUtil searchPageUtil);
	
	//查询所有符合条件的数据
	List<BuyWarehouseBacklogDetails> selectProdcutsByParams(Map<String,Object> params);
	
	List<BuyWarehouseBacklogDetails> selectPurchaseNum(Map<String, Object> paraMap);
	
	int selectById(String id);
	
	List<BuyWarehouseBacklogDetails> selectProdcuts();
	
	//同步之前清除表内数据
	void deleteAllDetails();
}
