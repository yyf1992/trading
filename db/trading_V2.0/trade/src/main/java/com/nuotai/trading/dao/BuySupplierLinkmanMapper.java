package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuySupplierLinkman;

public interface BuySupplierLinkmanMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuySupplierLinkman record);

    int insertSelective(BuySupplierLinkman record);

    BuySupplierLinkman selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuySupplierLinkman record);

    int updateByPrimaryKey(BuySupplierLinkman record);
    
    List<BuySupplierLinkman> selectBySupplierId(String supplierId);
    
    /**
     * 查询供应商默认联系人
     * @param supplierId
     * @return
     */
    BuySupplierLinkman selectDefaultlinkMan(String supplierId);
    
    BuySupplierLinkman selectByMap(Map<String, Object> map);
}