package com.nuotai.trading.utils;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Page {
	private int pageNo = Integer.parseInt(Constant.INIT_PAGE_NO); // 当前页码
	// 页面大小，设置为“-1”表示不进行分页（分页无效）
	private int pageSize = Integer.parseInt(Constant.INIT_PAGE_SIZE);
	private long count;// 总记录数，设置为“-1”表示不查询总数
	private String divId = "content";

	private int first;// 首页索引
	private int last;// 尾页索引
	private int prev;// 上一页索引
	private int next;// 下一页索引

	private boolean firstPage;// 是否是第一页
	private boolean lastPage;// 是否是最后一页

	private List list;

	private String funcName = "page"; // 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。

	public Page() {
		this.pageSize = 10;
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 */
	public Page(int pageNo, int pageSize) {
		this(pageNo, pageSize, 0);
	}

	public Page(int pageNo, int pageSize, String divId) {
		this(pageNo, pageSize, 0, divId);
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 */
	public Page(int pageNo, int pageSize, long count) {
		this(pageNo, pageSize, count, new ArrayList());
	}

	public Page(int pageNo, int pageSize, long count, String divId) {
		this(pageNo, pageSize, count, divId, new ArrayList());
	}

	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 * @param list
	 *            本页数据对象列表
	 */
	public Page(int pageNo, int pageSize, long count, List list) {
		this.setCount(count);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.setList(list);
	}

	public Page(int pageNo, int pageSize, long count, String divId, List list) {
		this.setCount(count);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.divId = divId;
		this.setList(list);
	}

	/**
	 * 初始化参数
	 */
	public void initialize() {

		// 1
		this.first = 1;

		this.last = (int) (count / (this.pageSize < 1 ? 20 : this.pageSize)
				+ first - 1);

		if (this.count % this.pageSize != 0 || this.last == 0) {
			this.last++;
		}

		if (this.last < this.first) {
			this.last = this.first;
		}

		if (this.pageNo <= 1) {
			this.pageNo = this.first;
			this.firstPage = true;
		}

		if (this.pageNo >= this.last) {
			this.pageNo = this.last;
			this.lastPage = true;
		}

		if (this.pageNo < this.last - 1) {
			this.next = this.pageNo + 1;
		} else {
			this.next = this.last;
		}

		if (this.pageNo > 1) {
			this.prev = this.pageNo - 1;
		} else {
			this.prev = this.first;
		}

		// 2
		if (this.pageNo < this.first) {// 如果当前页小于首页
			this.pageNo = this.first;
		}

		if (this.pageNo > this.last) {// 如果当前页大于尾页
			this.pageNo = this.last;
		}

		if (this.divId == null || "".equals(this.divId)) {
			this.divId = "content";
		}

	}

	/**
	 * 默认输出当前分页标签 <div class="page">${page}</div>
	 */
	@Override
	public String toString() {
		String functionName = funcName + divId;
		initialize();
        StringBuilder sb = new StringBuilder();
        //js代码
        sb.append("<script type='text/javascript'>");
        sb.append("function page" + divId + "(n, s) {");
        sb.append(" var contentObj;");
        if("layOpen".equals(divId)){
        	sb.append(" contentObj = $('.layui-layer-content');");
        }else if("content".equals(divId)){
        	sb.append(" if($('.content').length>0) contentObj=$('.content');");
        	sb.append(" if($('.content_modify').length>0) contentObj=$('.content_modify');");
        	sb.append(" if($('.content_system').length>0) contentObj=$('.content_system');");
        }else{
        	sb.append(" contentObj = $('#"+divId+"');");
        }
        sb.append("$(contentObj).find('#pageNo').val(n);");
        sb.append("$(contentObj).find('#pageSize').val(s);");
        sb.append("loadPlatformData('"+divId+"');");
        sb.append("return false;}");
        sb.append("</script>");
        if("content".equals(divId)){
	        sb.append("<span class='lf size_sm'>");
	        sb.append("每页显示");
	        sb.append("<select style='width:50px;height:25px;border-radius: 3px' onchange='" + functionName
					+ "(" + 1 + ",this.value);'>");
			sb.append(" <option value='10' "
					+ (pageSize == 10 ? "selected='selected'" : "")
					+ ">10</option>");
			sb.append(" <option value='25'"
					+ (pageSize == 25 ? "selected='selected'" : "")
					+ " >25</option>");
			sb.append(" <option value='50'"
					+ (pageSize == 50 ? "selected='selected'" : "")
					+ ">50</option>");
			sb.append(" <option value='100' "
					+ (pageSize == 100 ? "selected='selected'" : "")
					+ ">100</option>");
			sb.append(" <option value='200' "
					+ (pageSize == 200 ? "selected='selected'" : "")
					+ ">200</option>");
			sb.append(" </select>");
	        sb.append("条，共<span>"+count+"</span>条");
	        sb.append("</span>");
        }
        if (pageNo != first) {
			// 如果不是第一页,有<<按钮
        	sb.append("<a href='javascript:void(0)' onclick="+functionName+"("+prev+","+pageSize+")>&lt;&lt;</a>");
		}
        if(pageNo <= 4){
        	for(int i = 1; i <= pageNo;i++){
            	if (pageNo == i) {
            		sb.append(" <a class='current'>"+i+"</a>");
            	} else {
            		sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"("+i+","+pageSize+")'>"+i+"</a>");
            	}
            }
        }else{
        	sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"(1,"+pageSize+")'>1</a>");
        	sb.append(" ...");
        	for(int i = (pageNo - 2); i <= pageNo; i++){
        		if (pageNo == i) {
            		sb.append(" <a class='current'>"+i+"</a>");
            	} else {
            		sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"("+i+","+pageSize+")'>"+i+"</a>");
            	}
        	}
        }
        if(last-pageNo<=4){
        	for(int i = (pageNo+1); i <= last; i++){
        		sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"("+i+","+pageSize+")'>"+i+"</a>");
        	}
        }else{
        	for(int i = (pageNo+1); i <= pageNo+2; i++){
        		sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"("+i+","+pageSize+")'>"+i+"</a>");
        	}
        	sb.append(" ...");
        	sb.append(" <a href='javascript:void(0)' onclick='"+functionName+"("+last+","+pageSize+")'>"+last+"</a>");
        }
        if (pageNo != last) {
        	//不是最后一页
        	sb.append(" <a href='javascript:void(0)' onclick="+functionName+"("+next+","+pageSize+")>&gt;&gt;</a>");
        }
        if("content".equals(divId)){
        	sb.append(" 共"+last+"页&nbsp;转到 <input id='inputPageNo' value='"+pageNo+"'> 页&nbsp;&nbsp;<button type='button' onclick=\""+functionName+"($('#inputPageNo').val(),"+pageSize+");\">跳转</button>");
        }else{
        	sb.append(" 共"+last+"页,<span>"+count+"</span>条记录");
        }
		return sb.toString();
	}

	/**
	 * 获取分页HTML代码
	 * 
	 * @return
	 */
	public String getHtml() {
		return toString();
	}

	/**
	 * 获取设置总数
	 * 
	 * @return
	 */
	public long getCount() {
		return count;
	}

	/**
	 * 设置数据总数
	 * 
	 * @param count
	 */
	public void setCount(long count) {
		this.count = count;
		if (pageSize >= count) {
			pageNo = 1;
		}
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getPageNo() {

		return pageNo;
	}

	/**
	 * 设置当前页码
	 * 
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo < 0 ? 1 : pageNo;
	}

	/**
	 * 获取页面大小
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小（最大500）
	 * 
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;// > 500 ? 500 : pageSize;
	}

	/**
	 * 首页索引
	 * 
	 * @return
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * 尾页索引
	 * 
	 * @return
	 */
	public int getLast() {
		return last;
	}

	/**
	 * 获取页面总数
	 * 
	 * @return getLast();
	 */
	public int getTotalPage() {
		return getLast();
	}

	/**
	 * 是否为第一页
	 * 
	 * @return
	 */
	public boolean isFirstPage() {
		return firstPage;
	}

	/**
	 * 是否为最后一页
	 * 
	 * @return
	 */
	public boolean isLastPage() {
		return lastPage;
	}

	/**
	 * 上一页索引值
	 * 
	 * @return
	 */
	public int getPrev() {
		if (isFirstPage()) {
			return pageNo;
		} else {
			return pageNo - 1;
		}
	}

	/**
	 * 下一页索引值
	 * 
	 * @return
	 */
	public int getNext() {
		if (isLastPage()) {
			return pageNo;
		} else {
			return pageNo + 1;
		}
	}

	/**
	 * 获取本页数据对象列表
	 * 
	 * @return List<T>
	 */
	public List getList() {
		return list;
	}

	/**
	 * 设置本页数据对象列表
	 * 
	 * @param list
	 */

	public Page setList(List list) {
		this.list = list;
		return this;
	}

	/**
	 * 获取点击页码调用的js函数名称 function ${page.funcName}(pageNo){location=
	 * "${ctx}/list-${category.id}${urlSuffix}?pageNo="+i;}
	 * 
	 * @return
	 */
	public String getFuncName() {
		return funcName;
	}

	/**
	 * 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。
	 * 
	 * @param funcName
	 *            默认为page
	 */
	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	/**
	 * 分页是否有效
	 * 
	 * @return this.pageSize==-1
	 */
	public boolean isDisabled() {
		return this.pageSize == -1;
	}

	/**
	 * 是否进行总数统计
	 * 
	 * @return this.count==-1
	 */
	public boolean isNotCount() {
		return this.count == -1;
	}

	/**
	 * 获取 Hibernate FirstResult
	 */
	public int getFirstResult() {
		int firstResult = (getPageNo() - 1) * getPageSize();
		if (firstResult >= getCount()) {
			firstResult = 0;
		}
		return firstResult;
	}

	public int getLastResult() {
		int lastResult = getFirstResult() + getMaxResults();
		if (lastResult > getCount()) {
			lastResult = (int) getCount();
		}
		return lastResult;
	}

	/**
	 * 获取 Hibernate MaxResults
	 */
	public int getMaxResults() {
		return getPageSize();
	}

	public String getDivId() {
		return divId;
	}

	public void setDivId(String divId) {
		this.divId = divId;
	}
}
