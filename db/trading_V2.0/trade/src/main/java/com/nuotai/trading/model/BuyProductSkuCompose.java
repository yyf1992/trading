package com.nuotai.trading.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * bom子表
 * 
 * @author "
 * @date 2017-09-18 11:29:45
 */
@Data
public class BuyProductSkuCompose implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//id
	private String id;
	//公司id
	private String companyId;
	//成品id
	private String productId;
	//原材料条形码
	private String skuBarcode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//单位名称
	private String unitName;
	//配置数量
	private Integer composeNum;
	//创建日期
	private Date createDate;
	//创建人id
	private String createId;
	//创建人姓名
	private String createName;
	//修改日期
	private Date updateDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//删除日期
	private Date delDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//理论损耗
	private BigDecimal theoreticalLoss;
	//标准用量
	private BigDecimal standardDosage;
	//实际用量
	private BigDecimal actualDosage;
	//单价
	private BigDecimal price;
	//实际单价
	private BigDecimal actualPrice;
}
