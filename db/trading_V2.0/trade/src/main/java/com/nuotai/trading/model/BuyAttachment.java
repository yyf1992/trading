package com.nuotai.trading.model;

import lombok.Data;

@Data
public class BuyAttachment {
	//
	private String id;
	//地址
	private String url;
	//关联Id
	private String relatedId;
	//所属类型（0合同，1营业证件附件，2开户许可证附件）
	private Integer type;
}