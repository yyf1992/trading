package com.nuotai.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BuyWarehouseBacklogDetailsMapper;
import com.nuotai.trading.dao.BuyWarehouseBacklogMapper;
import com.nuotai.trading.model.BuyWarehouseBacklog;
import com.nuotai.trading.model.BuyWarehouseBacklogDetails;
import com.nuotai.trading.utils.SearchPageUtil;



@Service
@Transactional
public class BuyWarehouseBacklogDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyWarehouseBacklogDetailsService.class);

	@Autowired
	private BuyWarehouseBacklogDetailsMapper buyWarehouseBacklogDetailsMapper;
	@Autowired
	private BuyWarehouseBacklogMapper buyWarehouseBacklogMapper;
	
	public BuyWarehouseBacklogDetails get(String id){
		return buyWarehouseBacklogDetailsMapper.get(id);
	}
	
	public List<BuyWarehouseBacklogDetails> queryList(Map<String, Object> map){
		return buyWarehouseBacklogDetailsMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyWarehouseBacklogDetailsMapper.queryCount(map);
	}
	
	public void add(BuyWarehouseBacklogDetails buyWarehouseBacklogDetails){
		buyWarehouseBacklogDetailsMapper.add(buyWarehouseBacklogDetails);
	}
	
	public void update(BuyWarehouseBacklogDetails buyWarehouseBacklogDetails){
		buyWarehouseBacklogDetailsMapper.update(buyWarehouseBacklogDetails);
	}
	
	public void delete(String id){
		buyWarehouseBacklogDetailsMapper.delete(id);
	}
	
	/**
	 * 分页查询
	 * @param searchPageUtil
	 * @return
	 */
	public SearchPageUtil selectProdcutsByPage(SearchPageUtil searchPageUtil) {
		
		List<BuyWarehouseBacklogDetails> buyWarehouseBacklogDetailsList = buyWarehouseBacklogDetailsMapper.selectProdcutsByPage(searchPageUtil);

		searchPageUtil.getPage().setList(buyWarehouseBacklogDetailsList);

		return searchPageUtil;
	}
	
	/**
	 * 查询所有的符合条件的数据
	 * @param searchPageUtil
	 * @return
	 */
	public List<BuyWarehouseBacklogDetails> selectProdcutsByParams(Map<String,Object> params) {
		
		List<BuyWarehouseBacklogDetails> buyWarehouseBacklogDetailsList = buyWarehouseBacklogDetailsMapper.selectProdcutsByParams(params);

		return buyWarehouseBacklogDetailsList;
	}

	/**
	 * 计算库存积压预警的时间
	 * @throws Exception
	 */
	public void backlogInventory() throws Exception{
		List<BuyWarehouseBacklog> owshList = buyWarehouseBacklogMapper.selectBacklog();
		int count = owshList.size();
		for(int x = 0 ; x<owshList.size();x++){
			
			Date sysDate = owshList.get(x).getCreateDate();
			// 计算积压库存
			int y = 0;
//			owshList.get(x).setBacklogdaynum(0);
//			owshList.get(x).setLockingnum(owshList.get(x).getLockingnum()==null?0L:owshList.get(x).getLockingnum());
//			owshList.get(x).setStocknum(owshList.get(x).getStocknum()==null?0L:owshList.get(x).getStocknum());
			List<BuyWarehouseBacklogDetails> warehouseLogList = new ArrayList<BuyWarehouseBacklogDetails>(0);
			// 如果库存数为0则不查询入库记录,以优化效率
			if (owshList.get(x).getStocks() > 0) {

				Map<String, Object> paraMap = new HashMap<String, Object>();
				paraMap.put("barcode", owshList.get(x).getBarcode());
				paraMap.put("created", sysDate);
				// 取所有仓库的总库存数，所以这个仓库的查询条件未使用
//				paraMap.put("warehouseid", owsh.getWarehouseid());
				// 获取当前之前的所有入库记录，根据日期降序
				warehouseLogList = buyWarehouseBacklogDetailsMapper.selectPurchaseNum(paraMap);
			}
			// 初始化积压天数为y
			owshList.get(x).setBacklogmaxday(y);
			// 计算总库存(可用库存+锁定库存)
			owshList.get(x).setStocks(owshList.get(x).getStocks());
			try {
				Map<String, Object> map = new HashMap<String, Object>();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				map.put("startData", sdf.format(date));
				// 执行计算积压库存迭代器
				owshList.get(x).setBacklogmaxday(calcBacklogDay(map, owshList.get(x), y, warehouseLogList));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
//			mapper.updateByPrimaryKeySelective(owshList.get(x));
			System.out.println("剩余执行:"+count--);
		}
	}
	
	private int calcBacklogDay(Map<String, Object> map, BuyWarehouseBacklog osh, Integer y,
			List<BuyWarehouseBacklogDetails> warehouseLogList) throws ParseException {

		// 如果库存本身为0则积压日期为0
		if (osh.getStocks() > 0) {

			// 如果入库记录都减完后剩余库存依然大于等于0则库存存在问题，返回-1
			if (y >= warehouseLogList.size()) {

				return -1;
			}

			// 计算后的值重新存入BacklogNum
			int changeNum = warehouseLogList.get(y).getNumber();
			int oldNumber = osh.getStocks() - changeNum;
			osh.setStocks(oldNumber);
			if (oldNumber>0){
				warehouseLogList.get(y).setRemainingStock(changeNum);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date newDate = sdf.parse(map.get("startData").toString());
				warehouseLogList.get(y).setBacklogdaynum((int) ((newDate.getTime() - warehouseLogList.get(y).getCreateDate().getTime()) / (24 * 60 * 60 * 1000)));
				buyWarehouseBacklogDetailsMapper.update(warehouseLogList.get(y));
			}

			// 如果临时变量中的库存小于等于0，则计算完毕，返回积压天数
			if (oldNumber <= 0) {
				warehouseLogList.get(y).setRemainingStock(changeNum+osh.getStocks());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date newDate = sdf.parse(map.get("startData").toString());
				warehouseLogList.get(y).setBacklogdaynum((int) ((newDate.getTime() - warehouseLogList.get(y).getCreateDate().getTime()) / (24 * 60 * 60 * 1000)+1));
				buyWarehouseBacklogDetailsMapper.update(warehouseLogList.get(y));
				//日期因为是在0：00开始计算，选择的那天会缺少，因此最后许加上一天
				return (int) ((newDate.getTime() - warehouseLogList.get(y).getCreateDate().getTime()) / (24 * 60 * 60 * 1000));
			}
			// 临时变量中的库存不小于等于0，则继续迭代执行
			return calcBacklogDay(map, osh, ++y, warehouseLogList);

		}
		return 0;
	}
}
