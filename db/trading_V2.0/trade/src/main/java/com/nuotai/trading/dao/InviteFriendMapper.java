package com.nuotai.trading.dao;

import com.nuotai.trading.model.BuySupplierFriendInvite;
import com.nuotai.trading.utils.SearchPageUtil;

import java.util.List;
import java.util.Map;

/**
 * 好友互通申请信息表
 * 
 * @author "
 * @date 2017-08-02 13:45:01
 */
public interface InviteFriendMapper extends BaseDao<BuySupplierFriendInvite> {

    List<BuySupplierFriendInvite> selectByPage(SearchPageUtil friendPageUtil);

    /**
     * 获取好友申请列表条数
     * @param friendMap
     * @return
     */
    int queryInviteCount(Map<String, Object> friendMap);

    /**
     * 获取好友申请列表数据
     * @param searchPageUtil
     * @return
     */
    List<BuySupplierFriendInvite> queryInvitePage(SearchPageUtil searchPageUtil);
}
