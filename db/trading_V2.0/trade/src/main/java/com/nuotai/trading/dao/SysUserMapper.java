package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.utils.SearchPageUtil;

public interface SysUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);
    
    int updateByCompanyId(SysUser record);

    int updateByPrimaryKey(SysUser record);
    
    SysUser queryByLoginName(String loginName);
    
    SysUser queryExistUser(SysUser sysUser);

	List<Map<String, Object>> selectByPage(SearchPageUtil searchPageUtil);
	
	List<Map<String, Object>> queryByPage(SearchPageUtil searchPageUtil);
	
	List<SysUser> selectByCompanyId(String companyId);
	
	List<SysUser> getCompanyUser(Map<String, Object> map);
}