package com.nuotai.trading.model;

import java.io.Serializable;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-03-14 18:50:16
 */
@Data
public class TradeVerifyFile implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//
	private String verifyId;
	//
	private String fileUrl;
	//
	private String pocessId;
	
	private String fileName;
}
