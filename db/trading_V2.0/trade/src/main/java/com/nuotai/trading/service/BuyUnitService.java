package com.nuotai.trading.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyUnitMapper;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyUnitService implements BuyUnitMapper {
	@Autowired
	private BuyUnitMapper mapper;

	@Override
	public int insert(BuyUnit record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyUnit record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(BuyUnit record) {
		return mapper.updateByPrimaryKey(record);
	}

	/*
	 * 获取所有的单位
	 * @author gsf
	 * @date 2017-06-12
	 * @see com.nuotai.service.custom.BuyUnitService#getBuyUnitList()
	 */
	@Override
	public List<BuyUnit> getBuyUnitList() {
		return mapper.getBuyUnitList();
	}

	@Override
	public BuyUnit selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyUnit record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	public List<BuyUnit> selectByMap(
			Map<String, Object> map) {
		return mapper.selectByMap(map);
	}

	public JSONObject insertUnit(Map<String, Object> param) {
		// TODO Auto-generated method stub
		JSONObject json=new JSONObject();
		String units=param.containsKey("unitStr")?param.get("unitStr").toString():"";
		if(units!=null && units.length()>0){
			String [] unitArr=units.split("@");
			int result=0;
			String repeat="";
			for(String unit:unitArr){
				Map map=new HashMap<String, Object>();
				map.put("unit_name", unit);
				List<BuyUnit> unitList=mapper.selectByMap(map);
				if(unitList.size()>0){
					repeat+=unit+",";
				}else{
					BuyUnit buyUnit=new BuyUnit();
					buyUnit.setId(ShiroUtils.getUid());
					buyUnit.setCompanyId(ShiroUtils.getCompId());
					buyUnit.setUnitName(unit);
					mapper.insertSelective(buyUnit);
				}
			}
			if(repeat==""){
				json.put("success", true);
				json.put("msg", "新增成功！");
			}else{
				json.put("success", false);
				json.put("msg", "【"+repeat+"】"+"已存在！");
			}
		}
		else{
			json.put("success", false);
			json.put("msg", "关闭");
		}
		return json;
	}

	@Override
	public int delUnit(String id) {
		// TODO Auto-generated method stub
		return mapper.delUnit(id);
	}
}
