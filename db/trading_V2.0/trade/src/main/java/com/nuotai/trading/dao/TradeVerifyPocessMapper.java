package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.TradeVerifyPocess;

/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
public interface TradeVerifyPocessMapper extends BaseDao<TradeVerifyPocess> {

	String getApprovalUser(String headerId);

	List<TradeVerifyPocess> queryListByHeaderId(String headerId);

	TradeVerifyPocess getPocessOwn(Map<String, Object> params);

	List<TradeVerifyPocess> getNextVerify(Map<String, Object> params);

	int deleteOverVerify(Map<String, Object> params);

	int deleteNotVerify(String headerId);

	int getMaxIndex(String headerId);

    void deleteByHeaderId(String id);

	TradeVerifyPocess getApprovalPocess(String headerId);
}
