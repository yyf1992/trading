package com.nuotai.trading.utils.json;

public class DBException extends ServiceException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DBException() {
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(Throwable cause) {
        super(cause);
    }
}
