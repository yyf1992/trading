package com.nuotai.trading.controller.platform.baseDate;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.BuyProductSkuCompose;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.service.BuyProductSkuBomService;
import com.nuotai.trading.service.BuyProductSkuComposeService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

@Controller
@RequestMapping("platform/baseDate/productCompose")
public class ProductComposeController extends BaseController{
	
	@Autowired 
	private BuyProductSkuService buyProductSkuService;
	@Autowired 
	private BuyProductSkuComposeService buyProductSkuComposeService;
	@Autowired 
	private BuyProductSkuBomService  bomService;
	@Autowired
	private BuyProductSkuComposeService  composeService;
	@Autowired
	private TradeVerifyHeaderService  tradeVerifyHeaderService;
	
	
	//加载商品物料配置界面
	@RequestMapping("/loadProductComposeHtml")
	public String  loadProductComposeHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String, Object> param) {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		param.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		List<Map<String,Object>> skuList=buyProductSkuService.selectList(searchPageUtil);
		searchPageUtil.getPage().setList(skuList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/baseDate/materialManagement/productCompose";
	}
	
	/**
	 * 加载  商品/原材料
	 */
	@RequestMapping("/loadProductHtml")
	public String loadProductHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		String which="";
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		//物料配置供应商可以查询所有成品
		//params.put("companyId", ShiroUtils.getCompId());
		
		//获取已经选择的原材料barcode
		Object barcodesObj=params.get("barcodes");
		if(barcodesObj!=null && barcodesObj!=""){
			String[] barcodeArr = barcodesObj.toString().split(",");
			params.put("barcodeArr", barcodeArr);
		}
		List<String> exitList=bomService.selectExitProduct(ShiroUtils.getCompId());
		params.put("exitList", exitList);
		searchPageUtil.setObject(params);
		List<Map<String,Object>> productList =buyProductSkuService.inputSelect(searchPageUtil);
		searchPageUtil.getPage().setList(productList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		if("0".equals(params.get("productType"))){
			which+="platform/baseDate/materialManagement/product";
		}else{
			which+="platform/baseDate/materialManagement/rawMaterial";
		}
		return which;
	}
	
	/**
	 * 保存物料配置
	 * @param param
	 * @return
	 * @throws ServiceException 
	 */
	@RequestMapping(value="/saveCompose",method=RequestMethod.GET)
	@ResponseBody
	public String saveCompose(@RequestParam Map<String,Object> params){
		//物料配置去掉审批流程
//		insert(params, new IService(){
//			@Override
//			public JSONObject init(Map<String,Object> params)
//					throws ServiceException {
//				JSONObject json = new JSONObject();
//				json.put("verifySuccess", "platform/baseDate/productCompose/verifySuccess");//审批成功调用的方法
//				json.put("verifyError", "platform/baseDate/productCompose/verifyError");//审批失败调用的方法
//				json.put("relatedUrl", "platform/baseDate/productCompose/verifyDetail");//审批详细信息地址
//				List<String> idList = buyProductSkuComposeService.saveCompose(params);
//				json.put("idList", idList);
//				return json;
//			}
//		});
		JSONObject json = new JSONObject();
		json = buyProductSkuComposeService.saveCompose(params);
		
		if (json.get("msg") != null){
			json.put("success", false);
		}else{
			json.put("success", true);
			json.put("msg", "物料配置设置成功！");
		}	
	
		return json.toString();
	}
	
	//修改物料配置审批
	@RequestMapping(value="/saveUpdateCompose",method=RequestMethod.GET)
	@ResponseBody
	public String saveUpdateCompose(String id,@RequestParam Map<String,Object> params){
//		update(id,params, new IService(){
//			@Override
//			public JSONObject init(Map<String,Object> params)
//					throws ServiceException {
//				JSONObject json = new JSONObject();
//				json.put("verifySuccess", "platform/baseDate/productCompose/verifySuccess");//审批成功调用的方法
//				json.put("verifyError", "platform/baseDate/productCompose/verifyError");//审批失败调用的方法
//				json.put("relatedUrl", "platform/baseDate/productCompose/verifyDetail");//审批详细信息地址
//				List<String> idList = buyProductSkuComposeService.saveCompose(params);
//				json.put("idList", idList);
//				return json;
//			}
//		});
		JSONObject json = new JSONObject();
		json = buyProductSkuComposeService.saveCompose(params);
		
		if (json.get("msg") != null){
			json.put("success", false);
		}else{
			json.put("success", true);
			json.put("msg", "物料配置设置成功！");
		}
		
		return json.toString();
	}
	
	//加载商品物料配置成功界面
	@RequestMapping("/success")
	public String  success() {
		return "platform/baseDate/materialManagement/success";
	}
	
	//审批通过
	@RequestMapping("/verifySuccess")
	public void  verifySuccess(String id) {
		Map<String,String> map=new HashMap<>();
		map.put("msg", "success");
		map.put("id", id);
		bomService.setStatus(map);
	}
	
	//审批不通过
	@RequestMapping("/verifyError")
	public void  verifyError(String id) {
		Map<String,String> map=new HashMap<>();
		map.put("msg", "fail");
		map.put("id", id);
		bomService.setStatus(map);
	}
	
	/**
	 * 查看采购计划详情页面 
	 * @return
	 */
	@RequestMapping("/verifyDetail")
	public String verifyDetail(String id){
		BuyProductSkuBom bom = bomService.selectByBomId(id);
		Map<String, Object> bomMap = new HashMap<>();
		bomMap.put("companyId", bom.getCompanyId());
		bomMap.put("productId", bom.getProductId());
		List<Map<String,Object>>  materialList=composeService.selectByBomId(bomMap);
		//获得审批数据
		TradeVerifyHeader verifyDetail = tradeVerifyHeaderService.getVerifyHeaderByRelatedId(id);
		model.addAttribute("bom", bom);
		model.addAttribute("materialList",materialList);
		model.addAttribute("verifyDetail",verifyDetail);
		return "platform/baseDate/materialManagement/composeDetail";
	}
	
	/**
	 * 导入BOM数据
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	@RequestMapping(value="/importBOMData",method=RequestMethod.POST)
	@ResponseBody
	public String importBOMData(@RequestParam(required = false) MultipartFile excel,@RequestParam Map<String,Object> map) throws Exception {
		JSONObject json = new JSONObject();
		//判断是否是excel文件
		if (excel != null) {
			// 获取文件名
			String fileName = excel.getOriginalFilename();
			// 获取扩展名
			String ext = FilenameUtils.getExtension(fileName);
			InputStream is = excel.getInputStream();
			// 判断文件格式
			if ("xls".equals(ext) || "xlsx".equals(ext)) {
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is) : new XSSFWorkbook(is);
				//获得第一个表单
				Sheet sheet = workbook.getSheetAt(0); 
//				List<CellRangeAddress> cras = getCombineCell(sheet);
				List<BuyProductSkuCompose> excelData = new ArrayList<BuyProductSkuCompose>();
				// 获取excel行数
				int rowNum = sheet.getLastRowNum();
				int rowIndex = 0;
				for (int i = 1; i <= rowNum; i++) {
					try {
						rowIndex = i;
						//获取每一行的内容
						Row row = sheet.getRow(i);
						if (!ExcelUtil.isBlankRow(row)) {
							//每一行的第一格有值时才导入
							if (!ObjectUtil.isEmpty(getCellValue(row.getCell(0)))) {
								BuyProductSkuCompose buyProductSkuCompose = new BuyProductSkuCompose();
								buyProductSkuCompose.setProductId(map.containsKey("productId")?(String)map.get("productId"):"");
								buyProductSkuCompose.setSkuBarcode(getCellBarcodeValue(row.getCell(0)));
								buyProductSkuCompose.setUnitName(getCellValue(row.getCell(2)));
								buyProductSkuCompose.setTheoreticalLoss(new BigDecimal(row.getCell(3).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
								buyProductSkuCompose.setStandardDosage(new BigDecimal(row.getCell(4).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
								buyProductSkuCompose.setActualDosage(new BigDecimal(row.getCell(5).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
								buyProductSkuCompose.setPrice(new BigDecimal(row.getCell(6).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
								buyProductSkuCompose.setActualPrice(new BigDecimal(row.getCell(7).getNumericCellValue()).setScale(0, BigDecimal.ROUND_HALF_UP));
								buyProductSkuCompose.setComposeNum((int)row.getCell(8).getNumericCellValue());
				            	excelData.add(buyProductSkuCompose);
							}
						}
					} catch (Exception e) {
						json.put("status", "fail");
						json.put("msg", e.getMessage());
						json.put("fail", "请检查数据的第 "+rowIndex+"行");
						e.printStackTrace();
						return json.toString();
					}
				}
				if (!ObjectUtil.isEmpty(excelData)){
					json = buyProductSkuComposeService.insertBOMData(excelData);
				}else{
					json.put("status", "success");
					json.put("msg", "无数据导入！");
					return json.toString();
				}
			} else {
				json.put("status", "fail");
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("status", "fail");
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	
	/**   
    * 获取单元格的值   
    * @param cell   
    * @return   
    */    
    @SuppressWarnings("deprecation")
	public String getCellBarcodeValue(Cell cell){    
    	DecimalFormat df = new DecimalFormat("0");  
        if(cell == null) return "";    
        if(cell.getCellType() == Cell.CELL_TYPE_STRING){    
            return cell.getStringCellValue();    
        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){    
            return String.valueOf(cell.getBooleanCellValue());    
        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){    
            return cell.getCellFormula() ;    
        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){    
            return String.valueOf(df.format(cell.getNumericCellValue()) );    
        }
        return "";    
    }
	    
	/**   
    * 获取单元格的值   
    * @param cell   
    * @return   
    */    
    @SuppressWarnings("deprecation")
	public String getCellValue(Cell cell){    
    
        if(cell == null) return "";    
        if(cell.getCellType() == Cell.CELL_TYPE_STRING){    
            return cell.getStringCellValue();    
        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){    
            return String.valueOf(cell.getBooleanCellValue());    
        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){    
            return cell.getCellFormula() ;    
        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){    
            return String.valueOf(cell.getNumericCellValue());    
        }
        return "";    
    }

}
