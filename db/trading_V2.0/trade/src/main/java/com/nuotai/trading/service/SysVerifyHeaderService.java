package com.nuotai.trading.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.SysVerifyPocessMapper;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.SysVerifyPocess;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class SysVerifyHeaderService {

    private static final Logger LOG = LoggerFactory.getLogger(SysVerifyHeaderService.class);

	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyPocessMapper sysVerifyPocessMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	
	public SysVerifyHeader get(String id){
		return sysVerifyHeaderMapper.get(id);
	}
	
	public List<SysVerifyHeader> queryList(Map<String, Object> map){
		return sysVerifyHeaderMapper.queryList(map);
	}
	
	public List<SysVerifyHeader> queryDataList(Map<String, Object> map){
		return sysVerifyHeaderMapper.queryDataList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sysVerifyHeaderMapper.queryCount(map);
	}
	
	public void add(SysVerifyHeader sysVerifyHeader){
		sysVerifyHeaderMapper.add(sysVerifyHeader);
	}
	
	public void update(SysVerifyHeader sysVerifyHeader){
		sysVerifyHeaderMapper.update(sysVerifyHeader);
	}
	
	public void delete(String id){
		sysVerifyHeaderMapper.delete(id);
	}

	
	public List<SysVerifyHeader> getPageList(SearchPageUtil searchPageUtil) {
		return sysVerifyHeaderMapper.getPageList(searchPageUtil);
	}

	/**
	 * 添加审批流程
	 * @param map
	 * @return
	 */
	public JSONObject saveAdd(Map<String, Object> map) {
		JSONObject json = new JSONObject();
		String menuId = map.get("menuId").toString();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("menuId", menuId);
		params.put("companyId", ShiroUtils.getCompId());
		int count = sysVerifyHeaderMapper.queryCount(params);
		if(count > 0){
			json.put("success", false);
			json.put("msg", "该菜单已经存在流程设置！");
			return json;
		}
		SysVerifyHeader header = new SysVerifyHeader();
		header.setId(ShiroUtils.getUid());
		header.setTitle(map.get("title").toString());
		header.setMenuId(menuId);
		header.setCompanyId(ShiroUtils.getCompId());
		header.setRemark(map.get("remark")==null?"":map.get("remark").toString());
		//审批类型:0-固定审批流程;1-自动审批流程
		header.setVerifyType(map.get("verifyType")==null?"1":map.get("verifyType").toString());
		//状态:0-启用;1-禁用
		header.setStatus("0");
		//去重规则：0-同一个审批人在流程中出现多次时，自动去重；1-同一个审批人仅在连续出现时，自动去重
		header.setRepeatType(map.get("repeatType")==null?"0":map.get("repeatType").toString());
		//抄送人设置：0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
		header.setCopyType(map.get("copyType")==null?"0":map.get("copyType").toString());
		header.setCreateId(ShiroUtils.getUserId());
		header.setCreateName(ShiroUtils.getUserName());
		header.setCreateDate(new Date());
		header.setUpdateUserId(ShiroUtils.getUserId());
		header.setUpdateUserName(ShiroUtils.getUserName());
		header.setUpdateDate(new Date());
		sysVerifyHeaderMapper.add(header);
		//审批流程
		if("0".equals(header.getVerifyType())){
			String processUsers = map.get("processUsers")==null?"":map.get("processUsers").toString();
			if(!processUsers.isEmpty()){
				String[] userArr = processUsers.split("@");
				int index = 1;
				for(String userStr : userArr){
					String userId = userStr.split(",")[0];
					String userName = userStr.split(",")[1];
					SysVerifyPocess pocess = new SysVerifyPocess();
					pocess.setId(ShiroUtils.getUid());
					pocess.setHeaderId(header.getId());
					pocess.setUserId(userId);
					pocess.setUserName(userName);
					pocess.setVerifyIndex(index);
					sysVerifyPocessMapper.add(pocess);
					index++;
				}
			}
		}
		//抄送人
		String copyUsers = map.get("copyUsers")==null?"":map.get("copyUsers").toString();
		if(!copyUsers.isEmpty()){
			String[] copyUserArr = copyUsers.split("@");
			for(String userStr : copyUserArr){
				String userId = userStr.split(",")[0];
				String userName = userStr.split(",")[1];
				SysVerifyCopy copy = new SysVerifyCopy();
				copy.setId(ShiroUtils.getUid());
				copy.setHeaderId(header.getId());
				copy.setUserId(userId);
				copy.setUserName(userName);
				sysVerifyCopyMapper.add(copy);
			}
		}
		json.put("success", true);
		json.put("msg", "成功！");
		return json;
	}

	/**
	 * 修改审批流程
	 * @param map
	 * @return
	 */
	public JSONObject saveUpdate(Map<String, Object> map) {
		JSONObject json = new JSONObject();
		String id = map.get("id").toString();
		String menuId = map.get("menuId").toString();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("menuId", menuId);
		params.put("companyId", ShiroUtils.getCompId());
		List<SysVerifyHeader> oldList = sysVerifyHeaderMapper.queryList(params);
		if(!oldList.isEmpty()){
			if(!id.equals(oldList.get(0).getId())){
				json.put("success", false);
				json.put("msg", "该菜单已经存在流程设置！");
				return json;
			}
		}
		SysVerifyHeader header = new SysVerifyHeader();
		header.setId(id);
		header.setTitle(map.get("title").toString());
		header.setMenuId(menuId);
		header.setCompanyId(ShiroUtils.getCompId());
		header.setRemark(map.get("remark")==null?"":map.get("remark").toString());
		//审批类型:0-固定审批流程;1-自动审批流程
		header.setVerifyType(map.get("verifyType")==null?"1":map.get("verifyType").toString());
		//状态:0-启用;1-禁用
		header.setStatus("0");
		//去重规则：0-同一个审批人在流程中出现多次时，自动去重；1-同一个审批人仅在连续出现时，自动去重
		header.setRepeatType(map.get("repeatType")==null?"0":map.get("repeatType").toString());
		//抄送人设置：0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
		header.setCopyType(map.get("copyType")==null?"0":map.get("copyType").toString());
		header.setCreateId(ShiroUtils.getUserId());
		header.setCreateName(ShiroUtils.getUserName());
		header.setCreateDate(new Date());
		header.setUpdateUserId(ShiroUtils.getUserId());
		header.setUpdateUserName(ShiroUtils.getUserName());
		header.setUpdateDate(new Date());
		sysVerifyHeaderMapper.update(header);
		//清除审批人员数据、抄送数据
		sysVerifyPocessMapper.deleteByHeaderId(id);
		sysVerifyCopyMapper.deleteByHeaderId(id);
		//审批流程
		if("0".equals(header.getVerifyType())){
			String processUsers = map.get("processUsers")==null?"":map.get("processUsers").toString();
			if(!processUsers.isEmpty()){
				String[] userArr = processUsers.split("@");
				int index = 1;
				for(String userStr : userArr){
					String userId = userStr.split(",")[0];
					String userName = userStr.split(",")[1];
					SysVerifyPocess pocess = new SysVerifyPocess();
					pocess.setId(ShiroUtils.getUid());
					pocess.setHeaderId(header.getId());
					pocess.setUserId(userId);
					pocess.setUserName(userName);
					pocess.setVerifyIndex(index);
					sysVerifyPocessMapper.add(pocess);
					index++;
				}
			}
		}
		//抄送人
		String copyUsers = map.get("copyUsers")==null?"":map.get("copyUsers").toString();
		if(!copyUsers.isEmpty()){
			String[] copyUserArr = copyUsers.split("@");
			for(String userStr : copyUserArr){
				String userId = userStr.split(",")[0];
				String userName = userStr.split(",")[1];
				SysVerifyCopy copy = new SysVerifyCopy();
				copy.setId(ShiroUtils.getUid());
				copy.setHeaderId(header.getId());
				copy.setUserId(userId);
				copy.setUserName(userName);
				sysVerifyCopyMapper.add(copy);
			}
		}
		json.put("success", true);
		json.put("msg", "成功！");
		return json;
	}

	/**
	 * 禁用、启用
	 * @param id
	 * @param status
	 * @return
	 */
	public JSONObject deleteVerify(String id, String status) {
		JSONObject json = new JSONObject();
		SysVerifyHeader header = sysVerifyHeaderMapper.get(id);
		//将原来的状态修改
		if ("0".equals(status)){
			status = "1";
		}else{
			status = "0";
		}
		header.setStatus(status);
		int result = sysVerifyHeaderMapper.update(header);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
			return json;
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
			return json;
		}
	}
}
