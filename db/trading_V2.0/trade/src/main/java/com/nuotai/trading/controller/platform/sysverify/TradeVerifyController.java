package com.nuotai.trading.controller.platform.sysverify;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 内部审批
 * @author Administrator
 *
 */
@Controller
@RequestMapping("platform/tradeVerify")
public class TradeVerifyController extends BaseController {
	@Autowired
	private TradeVerifyHeaderService tradeVerifyHeaderService;
	
	/**
	 * 待我审批列表
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("waitForMeList")
	public String waitForMeList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		map.put("userId", ShiroUtils.getUserId());
		String[] statusArr = {"0"};
		map.put("statusArr", statusArr);//待审批
		searchPageUtil.setObject(map);
		List<Map<String,Object>> verifyList = tradeVerifyHeaderService.getWaitForMeList(searchPageUtil);
		searchPageUtil.getPage().setList(verifyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/tradeverify/waitForMeList";
	}
	
	/**
	 * 我已审批
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("approvedList")
	public String approvedList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		map.put("userId", ShiroUtils.getUserId());
		String[] statusArr = {"1","2","3"};
		map.put("statusArr", statusArr);
		searchPageUtil.setObject(map);
		List<Map<String,Object>> verifyList = tradeVerifyHeaderService.approvedList(searchPageUtil);
		searchPageUtil.getPage().setList(verifyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/tradeverify/approvedList";
	}
	
	/**
	 * 我发起的
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("mineOriginateList")
	public String mineOriginateList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		map.put("userId", ShiroUtils.getUserId());
		searchPageUtil.setObject(map);
		List<Map<String,Object>> verifyList = tradeVerifyHeaderService.mineOriginateList(searchPageUtil);
		searchPageUtil.getPage().setList(verifyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/tradeverify/mineOriginateList";
	}
	
	/**
	 * 抄送我的
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("copyToMeList")
	public String copyToMeList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId", ShiroUtils.getCompId());
		map.put("userId", ShiroUtils.getUserId());
		searchPageUtil.setObject(map);
		List<Map<String,Object>> verifyList = tradeVerifyHeaderService.copyToMeList(searchPageUtil);
		searchPageUtil.getPage().setList(verifyList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/tradeverify/copyToMeList";
	}
	
	/**
	 * 获得审批类型
	 * @param map
	 * @return
	 */
	@RequestMapping("getVerifyType")
	@ResponseBody
	public String getVerifyType(@RequestParam Map<String,Object> map){
		List<Map<String,String>> verifyTypeList = tradeVerifyHeaderService.getVerifyType(map);
		return JSONObject.toJSONString(verifyTypeList);
	}
	
	/**
	 * 审批详情页
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyDetail")
	public String verifyDetail(String id){
		//获得审批数据
		tradeVerifyHeaderService.getVerifyDetail(model,id);
		return "platform/tradeverify/verifyDetail";
	}
	
	/**
	 * 审批详情共通
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyDetailCommon")
	public String verifyDetailCommon(String relatedId)throws ServiceException{
		//获得审批数据
		tradeVerifyHeaderService.verifyDetailCommon(model,relatedId);
		return "platform/tradeverify/verifyDetailCommon";
	}
	
	@RequestMapping("verifyDetailByRelatedId")
	public String verifyDetailByRelatedId(String id){
		//获得审批数据
		tradeVerifyHeaderService.getRelatedDetail(model,id);
		return "platform/tradeverify/verifyDetail";
	}
	
	/**
	 * 保存审批
	 * @param map
	 * @return
	 */
	@RequestMapping("saveVerify")
	@ResponseBody
	public String saveVerify(@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			json = tradeVerifyHeaderService.saveVerify(map);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("flag", false);
			json.put("msg", "失败");
		}
		return json.toJSONString();
	}
	
	/**
	 * 催办
	 * @param String
	 * @return
	 */
	@RequestMapping("urgeTrade")
	@ResponseBody
	public String urgeTrade(String tradeId){
		JSONObject json = new JSONObject();
		try {
			json = tradeVerifyHeaderService.urgeTrade(tradeId);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("flag", false);
			json.put("msg", "失败");
		}
		return json.toJSONString();
	}
}
