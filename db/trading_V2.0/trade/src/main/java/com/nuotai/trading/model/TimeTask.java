package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-09-19 09:41:48
 */
@Data
public class TimeTask implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//
	private String code;
	//
	private String name;
	//当前状态：0：正常等待；1：执行中；2：停止
	private String currentStatus;
	//预备状态：0：正常；1预备停止
	private String prepStatus;
	//最后同步时间
	private Date lastSynchronous;
	//最后执行时间
	private Date lastExecute;
}
