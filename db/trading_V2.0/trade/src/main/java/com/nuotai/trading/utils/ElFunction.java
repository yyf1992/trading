package com.nuotai.trading.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.model.BuyWarehouse;
import com.nuotai.trading.model.DicAreas;
import com.nuotai.trading.model.DicCities;
import com.nuotai.trading.model.DicProvinces;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.service.BuyWarehouseService;
import com.nuotai.trading.service.DicAreasService;
import com.nuotai.trading.service.DicCitiesService;
import com.nuotai.trading.service.DicProvincesService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.service.TBusinessWhareaService;

/**
 * 自定义函数
 * 
 * @ClassName ElFunction
 * @author dxl
 * @Date 2017年7月20日 上午9:21:49
 * @version 1.0.0
 */
public class ElFunction {

	public static <T> T getBean(Class<T> clazz) {
		ApplicationContext ac2 = WebApplicationContextUtils.getWebApplicationContext(ShiroUtils.getRequest().getServletContext());
		return ac2.getBean(clazz);
	}

	/**
	 * 根据id获省份名称
	 * 
	 * @Description 使用：${el:getProvinceById('id').属性 }
	 * @author: dxl
	 * @param id
	 * @return
	 */
	public static DicProvinces getProvinceById(Integer id) {
		DicProvincesService provincesService = getBean(DicProvincesService.class);
		DicProvinces province = provincesService.getProvinceById(id);
		return province;
	}
	
	/**
	 * 根据id获取城市名称
	 * @param id
	 * @return
	 */
	public static DicCities getCityById(String id){
		DicCitiesService cityService = getBean(DicCitiesService.class);
		DicCities city = cityService.getCityById(id);
		return city;
	}
	
	/**
	 * 根据id获取区域名称
	 * @param id
	 * @return
	 */
	public static DicAreas getAreaById(String id){
		DicAreasService areaService = getBean(DicAreasService.class);
		DicAreas area = areaService.getAreaById(id);
		return area;
	}
	
	/**
	 * 根据id获取单位名称
	 */
	public static BuyUnit getUnitById(String id){
		BuyUnitService unitService = getBean(BuyUnitService.class);
		BuyUnit unit = unitService.selectByPrimaryKey(id);
		return unit;
	}
	
	/**
	 * 根据关联id，type获取附件列表
	 */
	public static List<BuyAttachment> getAttachmentByRelatedId(String relatedId,String type){
		AttachmentService attachmentService = getBean(AttachmentService.class);
		Map<String, Object> map = new HashMap<String, Object>();
 		map.put("relatedId", relatedId);
 		map.put("type", type);
 		List<BuyAttachment> list = attachmentService.selectByMap(map);
 		return list;
	}
	
	/**
	 * 根据id获取公司
	 * @param id
	 * @return
	 */
	public static BuyCompany getCompanyById(String id){
		BuyCompanyService companyService = getBean(BuyCompanyService.class);
		BuyCompany company = companyService.selectByPrimaryKey(id);
		return company;
	}
	
	/**
	 * 根据id获取用户
	 * @param id
	 * @return
	 */
	public static SysUser getUserById(String id){
		SysUserService userService = getBean(SysUserService.class);
		SysUser user = userService.selectByPrimaryKey(id);
		return user;
	}
	
	/**
	 * 根据id获取仓库
	 * @param id
	 * @return
	 */
	public static BuyWarehouse getWarehouseById(String id){
		BuyWarehouseService warehouseService = getBean(BuyWarehouseService.class);
		BuyWarehouse warehouse = warehouseService.selectByPrimaryKey(id);
		return warehouse;
	}
	
	/**
	 * 根据code获取仓库
	 * @param id
	 * @return
	 */
	public static BuyWarehouse getWarehouseByCode(String code){
		BuyWarehouseService warehouseService = getBean(BuyWarehouseService.class);
		BuyWarehouse warehouse = warehouseService.selectByCode(code);
		return warehouse;
	}
	
	/**
	 * 根据店铺id获取店铺
	 * @param id
	 * @return
	 */
	public static SysShop getShopById(String id){
		SysShopService shopService = getBean(SysShopService.class);
		SysShop shop = shopService.selectByPrimaryKey(id);
		return shop;
	}
	
	/**
	 * 根据code获取仓库new
	 * @param id
	 * @return
	 */
	public static TBusinessWharea getBusinessWhareaByCode(String code){
		TBusinessWhareaService warehouseService = getBean(TBusinessWhareaService.class);
		TBusinessWharea warehouse = warehouseService.selectByCode(code);
		return warehouse;
	}
	
	/**
	 * 根据id获取仓库new
	 * @param id
	 * @return
	 */
	public static TBusinessWharea getBusinessWhareaById(String id){
		TBusinessWhareaService warehouseService = getBean(TBusinessWhareaService.class);
		TBusinessWharea warehouse = warehouseService.getWharea(id);
		return warehouse;
	}
}
