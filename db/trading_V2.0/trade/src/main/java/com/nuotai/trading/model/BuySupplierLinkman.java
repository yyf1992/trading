package com.nuotai.trading.model;

import lombok.Data;

@Data
public class BuySupplierLinkman {
	// 主键id
    private String id;
    // 供应商ID
    private String supplierId;
    // 联系人名称
    private String person;
    // 手机号
    private String phone;
    // 电话区号
    private String zone;
    // 电话号
    private String telNo;
    // 传真
    private String fax;
    // qq
    private String qq;
    // 旺旺号
    private String wangNo;
    // email
    private String email;
    // 是否是默认联系人1表示是；0表示否
    private Integer isDefault;
}