package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

/**
 * 基础Dao(还需在XML文件里，有对应的SQL语句)
 * @author liujie
 * @date 2017.7.19
 */
public interface BaseDao<T> {
	/**
	 * 保存实体对象
	 */
	void add(T t);

	/**
	 * 保存Map对象
	 */
	void add(Map<String, Object> map);

	/**
	 * 批量保存对象列表
	 */
	void addBatch(List<T> list);

	/**
	 * 更新实体对象
	 */
	int update(T t);

	/**
	 * 更新Map对象
	 */
	int update(Map<String, Object> map);

	/**
	 * 根据对象ID删除对象
	 */
	int delete(Object id);

	/**
	 * 根据Map删除对象
	 */
	int delete(Map<String, Object> map);

	/**
	 * 根据对象ID数组批量删除对象
	 */
	int deleteBatch(Object[] id);

	/**
	 * 根据对象ID查询对象
	 */
	T get(Object id);

	/**
	 * 根据Map查询对象列表
	 */
	List<T> queryList(Map<String, Object> map);

	/**
	 * 根据对象ID查询对象列表
	 */
	List<T> queryList(Object id);

	/**
	 * 根据Map查询对象总数
	 */
	int queryCount(Map<String, Object> map);
}
