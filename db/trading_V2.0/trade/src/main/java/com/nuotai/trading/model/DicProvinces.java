package com.nuotai.trading.model;

public class DicProvinces {
    // 主键id
    private Integer id;
    // 省份id
    private String provinceId;
    // 省份名称
    private String province;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getProvinceId() {
        return provinceId;
    }
    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
}