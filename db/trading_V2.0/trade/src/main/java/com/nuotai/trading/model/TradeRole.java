package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 权限角色
 * 
 * @author "
 * @date 2017-09-14 09:23:05
 */
@Data
public class TradeRole implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//
	private String roleCode;
	//角色
	private String roleName;
	//备注
	private String remark;
	//是否管理员 0-否;1-是
	private String isAdmin;
	//状态 0-正常;1-禁用
	private String status;
	//
	private String companyId;
}
