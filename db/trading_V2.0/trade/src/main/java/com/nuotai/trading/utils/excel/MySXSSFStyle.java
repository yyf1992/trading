package com.nuotai.trading.utils.excel;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

@SuppressWarnings("deprecation")
public class MySXSSFStyle {
	private XSSFCellStyle titleStyle;
	private XSSFCellStyle cellStyle;
	//加粗右上边框
	private MySXSSFFont myFont;
	
	public MySXSSFStyle(SXSSFWorkbook wb){
		myFont = new MySXSSFFont(wb);
		this.titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);// 字体左右居中
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(myFont.getBOLD());
		
		this.cellStyle = (XSSFCellStyle) wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);// 字体左右居中
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		cellStyle.setFont(myFont.getNORMAL());
	}

	public XSSFCellStyle getTitleStyle() {
		return titleStyle;
	}

	public void setTitleStyle(XSSFCellStyle titleStyle) {
		this.titleStyle = titleStyle;
	}

	public XSSFCellStyle getCellStyle() {
		return cellStyle;
	}

	public void setCellStyle(XSSFCellStyle cellStyle) {
		this.cellStyle = cellStyle;
	}
}
