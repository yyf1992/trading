package com.nuotai.trading.controller.common.shiro;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 认证
 * 
 * @author liujie
 */
public class UserRealm extends AuthorizingRealm {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private BuyCompanyService buyCompanyService;
    
    /**
     * 授权(验证权限时调用)
     */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SysUser user = (SysUser)principals.getPrimaryPrincipal();
		String userId = user.getId();
		
		List<String> permsList = new ArrayList<String>();
		
		//系统管理员，拥有最高权限
		if("admin".equals(userId)){
			/*List<SysMenu> menuList = sysMenuService.queryList(new HashMap<String, Object>());
			permsList = new ArrayList<>(menuList.size());
			for(SysMenu menu : menuList){
				permsList.add(menu.getPerms());
			}*/
		}else{
			//permsList = sysUserService.queryAllPerms(userId);
		}

		//用户权限列表
		Set<String> permsSet = new HashSet<String>();
		for(String perms : permsList){
			if(StringUtils.isBlank(perms)){
				continue;
			}
			permsSet.addAll(Arrays.asList(perms.trim().split(",")));
		}
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.setStringPermissions(permsSet);
		return info;
	}

	/**
	 * 认证(登录时调用)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {
		String loginName = (String) token.getPrincipal();
        String password = new String((char[]) token.getCredentials());
        
        //查询用户信息
        SysUser user = sysUserService.queryByLoginName(loginName);
        
        //账号不存在
        if(user == null) {
            throw new UnknownAccountException("账号不存在");
        }
        
        //公司等待审核
        BuyCompany company = buyCompanyService.selectByPrimaryKey(user.getCompanyId());
        if(company == null){
        	throw new UnknownAccountException("找不到公司信息，请联系管理员");
        }else if(company.getStatus() == 0){//待审核
        	throw new IncorrectCredentialsException("请等待审核后再进行登录");
        }else if(company.getStatus() == 2){//未通过
        	throw new IncorrectCredentialsException("审核未通过，原因："+company.getReason()+"，请重新注册");
        }
        
        //密码错误
        if(!password.toLowerCase().equals(user.getPassword())) {
            throw new IncorrectCredentialsException("密码不正确");
        }
        
        //账号删除
        if(user.getIsDel() == 1){
        	throw new LockedAccountException("账号已被删除,请联系管理员");
        }
        //放置session
		user.setCompanyName(company.getCompanyName());
        ShiroUtils.setSessionAttribute(Constant.CURRENT_USER, user);
        ShiroUtils.setSessionAttribute("company", company);
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, password, getName());
        return info;
	}
}
