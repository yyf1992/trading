package com.nuotai.trading.controller.platform.sysverify;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.SysMenu;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.SysVerifyPocess;
import com.nuotai.trading.service.SysMenuService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.service.SysVerifyCopyService;
import com.nuotai.trading.service.SysVerifyHeaderService;
import com.nuotai.trading.service.SysVerifyPocessService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 审批设置
 * @author Administrator
 *
 */
@Controller
@RequestMapping(Constant.SYSVERIFY)
public class SysVerifyHeaderController extends BaseController {
	@Autowired
	private SysVerifyHeaderService sysVerifyHeaderService;
	@Autowired
	private SysMenuService sysMenuService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysVerifyPocessService sysVerifyPocessService;
	@Autowired
	private SysVerifyCopyService sysVerifyCopyService;
	
	/**
	 * 列表页
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("sysVerifyList")
	public String sysVerifyList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
//		if(searchPageUtil.getPage()==null){
//			searchPageUtil.setPage(new Page());
//		}
//		map.put("companyId",ShiroUtils.getCompId());
//		searchPageUtil.setObject(map);
//		List<SysVerifyHeader> sysVerifyList = sysVerifyHeaderService.getPageList(searchPageUtil);
//		searchPageUtil.getPage().setList(sysVerifyList);
//		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sysverify/sysVerifyList";
	}
	
	/**
	 * 获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				params.put("companyId", ShiroUtils.getCompId());
				List<SysVerifyHeader> sysVerifyList = sysVerifyHeaderService.queryDataList(params);
				return sysVerifyList;
			}
		});
	}
	
	/**
	 * 加载新增页面
	 * @return
	 */
	@RequestMapping("loadAddHtml")
	public String loadAddHtml(){
		List<SysMenu> sysMenuList = sysMenuService.getCompanyMenu("0");
		model.addAttribute("sysMenuList", sysMenuList);
		return "platform/sysverify/add";
	}
	
	/**
	 * 加载人员
	 * @param map
	 * @return
	 */
	@RequestMapping("loadPerson")
	public String loadPerson(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(map.containsKey("selectUsers")&&map.get("selectUsers")!=null){
			String selectUsers = map.get("selectUsers").toString();
			String[] userList = selectUsers.split(",");
			map.put("notUserList", userList);
		}
		String companyId = ShiroUtils.getCompId();
		map.put("companyId", companyId);
		searchPageUtil.setObject(map);
		List<Map<String, Object>> sysUserList = sysUserService.getCompanyUser(searchPageUtil);
		searchPageUtil.getPage().setList(sysUserList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sysverify/chooseUser";
	}
	
	/**
	 * 设置审批人员-人员选择
	 * @param searchPageUtil
	 * @param map
	 * @return
	 */
	@RequestMapping("loadPersonAutomatic")
	public String loadPersonAutomatic(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(map.containsKey("selectUsers")&&map.get("selectUsers")!=null){
			String selectUsers = map.get("selectUsers").toString();
			String[] userList = selectUsers.split(",");
			map.put("notUserList", userList);
		}
		String companyId = Constant.COMPANY_ID;
		map.put("companyId", companyId);
		searchPageUtil.setObject(map);
		List<Map<String, Object>> sysUserList = sysUserService.getCompanyUser(searchPageUtil);
		searchPageUtil.getPage().setList(sysUserList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sysverify/chooseUserAutomatic";
	}
	
	/**
	 * 添加审批流程
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveAdd", method = RequestMethod.POST)
	@ResponseBody
	public String saveAdd(@RequestParam Map<String,Object> map){
		JSONObject json = sysVerifyHeaderService.saveAdd(map);
		return json.toString();
	}
	
	/**
	 * 加载修改页面
	 * @param id
	 * @return
	 */
	@RequestMapping("loadUpdateHtml")
	public String loadUpdateHtml(String id){
		SysVerifyHeader header = sysVerifyHeaderService.get(id);
		List<SysVerifyPocess> pocessList = sysVerifyPocessService.getListByHeaderId(id);
		List<SysVerifyCopy> copyList = sysVerifyCopyService.getListByHeaderId(id);
		model.addAttribute("verifyHeader", header);
		model.addAttribute("pocessList", pocessList);
		model.addAttribute("copyList", copyList);
		List<SysMenu> sysMenuList = sysMenuService.getCompanyMenu("0");
		model.addAttribute("sysMenuList", sysMenuList);
		return "platform/sysverify/update";
	}
	
	/**
	 * 修改保存
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdate(@RequestParam Map<String,Object> map){
		JSONObject json = sysVerifyHeaderService.saveUpdate(map);
		return json.toString();
	}
	
	/**
	 * 详情页面
	 * @param id
	 * @return
	 */
	@RequestMapping("loadDetails")
	public String loadDetails(String id){
		SysVerifyHeader header = sysVerifyHeaderService.get(id);
		List<SysVerifyPocess> pocessList = sysVerifyPocessService.getListByHeaderId(id);
		List<SysVerifyCopy> copyList = sysVerifyCopyService.getListByHeaderId(id);
		model.addAttribute("verifyHeader", header);
		model.addAttribute("pocessList", pocessList);
		model.addAttribute("copyList", copyList);
		List<SysMenu> sysMenuList = sysMenuService.getCompanyMenu("0");
		model.addAttribute("sysMenuList", sysMenuList);
		return "platform/sysverify/details";
	}
	
	/**
	 * 禁用、启用
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/deleteVerify", method = RequestMethod.POST)
	@ResponseBody
	public String deleteVerify(String id,String status){
		JSONObject json = sysVerifyHeaderService.deleteVerify(id,status);
		return json.toString();
	}
	
	/**
	 * 加载自动流程选择人员页面
	 * @param params
	 * @return
	 */
	@RequestMapping("setApprovalUser")
	public String setApprovalUser(){
		return "platform/sysverify/setApprovalUser";
	}
	
	/**
	 * 转交时选择人员
	 * @return
	 */
	@RequestMapping("setReferralUser")
	public String setReferralUser(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String[] userList = {ShiroUtils.getUserId()};
		map.put("notUserList", userList);
		String companyId = ShiroUtils.getCompId();
		map.put("companyId", companyId);
		searchPageUtil.setObject(map);
		List<Map<String, Object>> sysUserList = sysUserService.getCompanyUser(searchPageUtil);
		searchPageUtil.getPage().setList(sysUserList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sysverify/setReferralUser";
	}
}
