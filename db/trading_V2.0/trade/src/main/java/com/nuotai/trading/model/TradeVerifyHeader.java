package com.nuotai.trading.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-08-24 14:24:28
 */
@Data
public class TradeVerifyHeader implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//审批标题
	private String title;
	//关联id
	private String relatedId;
	//审批成功调用的方法
	private String verifySuccess;
	//审批失败调用的方法
	private String verifyError;
	//详细信息地址
	private String relatedUrl;
	//流程设置id
	private String siteId;
	//公司id
	private String companyId;
	//状态：0-审批中；1-通过；2-拒绝；3-撤销
	private String status;
	//备注
	private String remark;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	private String startName;
	//
	private Date createDate;
	private String createDateFormat;
	//修改人id
	private String updateUserId;
	//修改人姓名
	private String updateUserName;
	//修改时间
	private Date updateDate;
}
