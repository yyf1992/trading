package com.nuotai.trading.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.dao.SysRoleCompanyMapper;
import com.nuotai.trading.model.SysRoleCompany;
import com.nuotai.trading.model.SysRoleUser;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


@Service
public class SysRoleCompanyService implements SysRoleCompanyMapper {
	@Autowired
	private SysRoleCompanyMapper sysRoleCompanyMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return sysRoleCompanyMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysRoleCompany record) {
		return sysRoleCompanyMapper.insert(record);
	}

	@Override
	public int insertSelective(SysRoleCompany record) {
		return sysRoleCompanyMapper.insertSelective(record);
	}

	@Override
	public SysRoleCompany selectByPrimaryKey(String id) {
		return sysRoleCompanyMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysRoleCompany record) {
		return sysRoleCompanyMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysRoleCompany record) {
		return sysRoleCompanyMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<SysRoleCompany> selectRightByPageCompany(
			SearchPageUtil searchPageUtil) {
		return sysRoleCompanyMapper.selectRightByPageCompany(searchPageUtil);
	}
	
	public void addRoleCompany(String roleIds, String companyId) throws Exception {
		String[] roleIdList = roleIds.split(",");
		for (String roleId : roleIdList) {
			SysRoleCompany src = new SysRoleCompany();
			src.setId(ShiroUtils.getUid());
			src.setRoleId(roleId);
			src.setCompanyId(companyId);
			src.setCreateTime(new Date());
			sysRoleCompanyMapper.insertSelective(src);
		}
	}
	
	public void deleteRoleCompany(String roleUserIds) throws Exception {
		String[] idStr = roleUserIds.split(",");
		deleteByIdList(idStr);
	}
	
	@Override
	public int deleteByIdList(String[] idStr) {
		return sysRoleCompanyMapper.deleteByIdList(idStr);
	}

	@Override
	public List<String> getMenuIdListByCompanyId(String companyId) {
		return sysRoleCompanyMapper.getMenuIdListByCompanyId(companyId);
	}
	
}
