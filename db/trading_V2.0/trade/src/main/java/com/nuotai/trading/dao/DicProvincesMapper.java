package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.DicProvinces;

public interface DicProvincesMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(DicProvinces record);

	int insertSelective(DicProvinces record);

	DicProvinces selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(DicProvinces record);
	
	int updateByPrimaryKey(DicProvinces record);

	List<DicProvinces> selectByMap(Map<String, Object> map);
	
    DicProvinces getProvinceById(Integer provinceId);
}