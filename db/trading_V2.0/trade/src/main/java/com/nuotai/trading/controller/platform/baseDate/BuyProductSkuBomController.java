package com.nuotai.trading.controller.platform.baseDate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.service.BuyProductSkuBomService;
import com.nuotai.trading.service.BuyProductSkuComposeService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


/**
 * @author zyn
 * @date 2017-09-13 16:15:53
 */
@Controller
@RequestMapping("platform/baseDate/buyproductskubom")
public class BuyProductSkuBomController extends BaseController{
	@Autowired
	private BuyProductSkuBomService buyProductSkuBomService;
	@Autowired
	private BuyProductSkuComposeService  composeService;
	
	/**
	 * 商品物料配置列表
	 */
	@RequestMapping("/loadBomListHtml")
	public String loadBomListHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> param){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		param.put("companyId", ShiroUtils.getCompId());
		param.put("tabId","0");
		searchPageUtil.setObject(param);
		List<BuyProductSkuBom>  bomList = buyProductSkuBomService.selectList(searchPageUtil);
		for (BuyProductSkuBom buyProductSkuBom:bomList){
			param.put("productId", buyProductSkuBom.getProductId());
			List<Map<String,Object>>  materialList = composeService.selectByMap(param);
			buyProductSkuBom.setSkuComposeList(materialList);
		}
		searchPageUtil.getPage().setList(bomList);
		Map<String,Object> countMap=buyProductSkuBomService.getBomStatsCout(param);
		if(!param.containsKey("tabId")){
			param.put("tabId","0");
		}
		model.addAttribute("searchPageUtil",searchPageUtil);
		model.addAttribute("params11",param);
		model.addAttribute("countMap",countMap);
		return "platform/baseDate/materialManagement/bomList";
	}
	
//	/**
//	 * 原材料列表
//	 */
//	@RequestMapping("/loadMaterialHtml")
//	public String loadMaterialHtml(String id){
//		Map<String, Object> bomMap = new HashMap<>();
//		bomMap.put("companyId", buyProductSkuBom.getCompanyId());
//		bomMap.put("productId", buyProductSkuBom.getProductId());
//		List<Map<String,Object>>  materialList=composeService.selectByBomId(id);
//		model.addAttribute("materialList",materialList);
//		return "platform/baseDate/materialManagement/rawMaterialFrame";
//	}
	
	/**
	 * 修改物料配置
	 */
	@RequestMapping("/changeCompose")
	public String changeCompose(String productId){
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("productId", productId);
		List<Map<String,Object>>  materialList=composeService.selectByMap(map);
		BuyProductSkuBom bom =buyProductSkuBomService.selectByMap(map);
		model.addAttribute("bom",bom);
		model.addAttribute("materialList",materialList);
		model.addAttribute("changeCompose",true);
		return "platform/baseDate/materialManagement/productCompose";
	}
	
	/** 
	 * 删除物料配置
	 */
	@RequestMapping("/saveDeleteSKUBom")
	@ResponseBody
	public String saveDeleteSKUBom(String id){
		JSONObject json = new JSONObject();
		try {
			buyProductSkuBomService.delete(id);
			json.put("success", true);
			json.put("msg", "删除成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "删除失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 加载导入数据页面
	 * @return
	 */
	@RequestMapping(value="/loadImportExcelHtml",method=RequestMethod.POST)
	public String loadImportExcelHtml(){
		return "platform/baseDate/materialManagement/importExcel";
	}
	
	/**
	 * 下载模板
	 * @param response
	 * @param request
	 */
	@RequestMapping("/downloadExcel")
	public void downloadExcel(HttpServletResponse response,HttpServletRequest request) {
        try {
            //获取文件的路径
            String excelPath = request.getSession().getServletContext().getRealPath("statics/file/"+"BOM导入模板.xls");
            //String fileName = "采购价格设置模板.xlsx".toString(); // 文件的默认保存名
            // 读到流中
            //文件的存放路径
            InputStream inStream = new FileInputStream(excelPath);
            // 设置输出的格式
            response.reset();
            response.setContentType("bin");
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode("BOM导入模板.xls", "UTF-8"));
            OutputStream os = response.getOutputStream();
            // 循环取出流中的数据
            byte[] b = new byte[200];
            int len;

            while ((len = inStream.read(b)) > 0){
            	os.write(b, 0, len);
            }
            // 这里主要关闭。
            os.close();
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
