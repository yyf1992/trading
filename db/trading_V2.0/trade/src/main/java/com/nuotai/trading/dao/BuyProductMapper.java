package com.nuotai.trading.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.utils.SearchPageUtil;

public interface BuyProductMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyProduct record);

    int insertSelective(BuyProduct record);

    BuyProduct selectByPrimaryKey(String id);
    
    BuyProduct selectByProductCode(String id);

    int updateByPrimaryKeySelective(BuyProduct record);

    int updateByPrimaryKey(BuyProduct record);
    
    List<BuyProduct> selectByPage(SearchPageUtil searchPageUtil);
    
    List<BuyProduct> selectByMap(Map<String, Object> map);
    
    List<Map<String, Object>> selectProductByMap(Map<String, Object> params);

	List<Map<String, Object>> queryByPage(SearchPageUtil searchPageUtil);
	
	List<Map<String, Object>> selectMore(SearchPageUtil searchPageUtil);
}