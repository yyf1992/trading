package com.nuotai.trading.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuhui
 * 对象处理类
 * 2017-8-9 09:53:40
 */
public class ObjectUtil {
    /**
     * 判断传入的数据是否为空
     * @param o
     * @return
     */
    public static boolean isEmpty(Object o)
    {
        if (o == null)
        {
            return true;
        }
        if (o.equals("null"))
        {
            return true;
        }
        if (o instanceof String)
        {
            if (((String)o).trim().length() == 0)
            {
                return true;
            }
        }
        if (o instanceof Collection)
        {
            if (((Collection)o).isEmpty())
            {
                return true;
            }
        }
        if (o instanceof List)
        {
            if (((List)o).isEmpty())
            {
                return true;
            }

        }
        if (o.getClass().isArray())
        {
            if (((Object[])o).length == 0)
            {
                return true;
            }
        }
        if (o instanceof Map)
        {
            if (((Map)o).isEmpty())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * javaBean转为map
     * @param obj
     * @return
     */
    public static Map<String, Object> transBeanToMap(Object obj) {
        if(obj == null){
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                // 过滤class属性
                if (!key.equals("class")) {
                    // 得到property对应的getter方法
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(obj);
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            System.out.println("transBean2Map Error " + e);
        }
        return map;
    }
}
