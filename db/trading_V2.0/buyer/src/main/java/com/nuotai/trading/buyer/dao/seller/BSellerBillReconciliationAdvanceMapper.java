package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliationAdvance;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2018-02-24 13:04:37
 */
@Component
public interface BSellerBillReconciliationAdvanceMapper extends BaseDao<BSellerBillReconciliationAdvance> {
	//根据编号查询数量
    List<BSellerBillReconciliationAdvance> sellerRecAdvanceList(Map<String,Object> sellerAdvanceMap);
    //同步添加到卖家模块
    int saveSellerAdvance(BSellerBillReconciliationAdvance sellerBillReconciliationAdvance);
    //同步更新到卖家模块
    int updateSellerAdvance(BSellerBillReconciliationAdvance sellerBillReconciliationAdvance);
}
