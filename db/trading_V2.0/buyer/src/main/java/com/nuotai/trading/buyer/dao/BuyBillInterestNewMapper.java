package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.buyer.model.BuyBillInterestNew;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 14:52:16
 */
@Component
public interface BuyBillInterestNewMapper extends BaseDao<BuyBillInterestNew> {
	//添加申请关联利息
    int addNewInterest(BuyBillInterestNew buyBillInterestNew);
    //根据申请周期编号查询关联利息
    List<BuyBillInterestNew> queryNewInterest(String id);
}
