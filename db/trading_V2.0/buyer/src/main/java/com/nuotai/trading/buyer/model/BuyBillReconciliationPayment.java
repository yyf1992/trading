package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-11-01 11:02:22
 */
@Data
public class BuyBillReconciliationPayment implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//对账单号
	private String reconciliationId;
	//审批编号
	private String acceptPaymentId;
	//付款金额
	private BigDecimal actualPaymentAmount;
	//付款账号
	private String bankAccount;
	//付款账号
	private String bankAccountNum;
	//付款类型
	private String paymentType;
	//付款收据
	private String paymentAddr;
	//收款收据
	private String receivablesAddr;
	//付款时间
	private Date paymentDate;
	//格式化时间
	private String paymentDateStr;
	//付款申请开始时间
	private Date startBillStatementDate;
	//付款申请结束时间
	private Date endBillStatementDate;
	//收款确认时间
	private Date receivablesDate;
	//付款状态
	private String paymentStatus;
	//付款备注
	private String paymentRemarks;
	//收款备注
	private String receivablesRemarks;
	//供应商编号
	private String sellerCompanyId;
	//供应商名称
	private String sellerCompanyName;
	//采购员
	private String createBillUserName;
	//应付款总额
	private BigDecimal totalAmount;
	//实际付款总额
	private BigDecimal actualPaymentAmountSum;
	//剩余付款金额
	private BigDecimal residualPaymentAmount;
	//
	private String orderId;
}
