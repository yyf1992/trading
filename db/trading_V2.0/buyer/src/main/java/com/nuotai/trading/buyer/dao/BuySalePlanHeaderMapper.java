package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuySalePlanHeader;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 
 * 
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
public interface BuySalePlanHeaderMapper extends BaseDao<BuySalePlanHeader> {
	
	List<BuySalePlanHeader> selectByPage(SearchPageUtil searchPageUtil);
	
	int selectSalePlanNumByMap(Map<String, Object> map);
	
	List<Map<String,Object>> selectByStatistic(Map<String, Object> map);
	
	List<BuySalePlanHeader> selectApprovedByPage(SearchPageUtil searchPageUtil);
	
	List<String> selectApprovedId(Map<String, Object> params);
}
