package com.nuotai.trading.buyer.model.seller;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class SellerOrderSupplierProduct {

    //主键
    private String id;
    //订单主键Id
    private String orderId;
    //买家订单明细ID
    private String buyOrderItemId;
    //采购计划单号
    private String applyCode;
    //货号
    private String productCode;
    //商品名称
    private String productName;
    //规格代码
    private String skuCode;
    //规格名称
    private String skuName;
    //条形码
    private String barcode;
    //单价
    private BigDecimal price;
    //采购数量
    private Integer orderNum;
    //优惠金额
    private BigDecimal discountMoney;
    //总金额
    private BigDecimal totalMoney;
    //仓库
    private String warehouseId;
    //已发货数量
    private Integer deliveredNum;
    //到货数量
    private Integer arrivalNum;
    //备注
    private String remark;
    //单位ID
    private String unitId;
    //是否需要发票Y:是N:否
    private String isNeedInvoice;

    private String unitName;  //单位名称
    //要求到货日期
    private Date predictArred;
}