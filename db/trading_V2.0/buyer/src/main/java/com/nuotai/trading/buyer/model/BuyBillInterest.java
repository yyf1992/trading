package com.nuotai.trading.buyer.model;

public class BuyBillInterest {
    private String id;

    private String billCycleId;

    private Integer overdueDate;

    private Integer overdueInterest;
    
    private String interestCalculationMethod;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillCycleId() {
        return billCycleId;
    }

    public void setBillCycleId(String billCycleId) {
        this.billCycleId = billCycleId;
    }

    public Integer getOverdueDate() {
        return overdueDate;
    }

    public void setOverdueDate(Integer overdueDate) {
        this.overdueDate = overdueDate;
    }

    public Integer getOverdueInterest() {
        return overdueInterest;
    }

    public void setOverdueInterest(Integer overdueInterest) {
        this.overdueInterest = overdueInterest;
    }

	public String getInterestCalculationMethod() {
		return interestCalculationMethod;
	}

	public void setInterestCalculationMethod(String interestCalculationMethod) {
		this.interestCalculationMethod = interestCalculationMethod;
	}
}