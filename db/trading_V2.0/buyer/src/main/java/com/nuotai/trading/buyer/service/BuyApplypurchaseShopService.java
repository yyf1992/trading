package com.nuotai.trading.buyer.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuotai.trading.buyer.dao.BuyApplypurchaseShopMapper;
import com.nuotai.trading.buyer.model.BuyApplypurchaseShop;



@Service
public class BuyApplypurchaseShopService {

	@Autowired
	private BuyApplypurchaseShopMapper buyApplypurchaseShopMapper;
	
	public BuyApplypurchaseShop get(String id){
		return buyApplypurchaseShopMapper.get(id);
	}
	
	public List<BuyApplypurchaseShop> queryList(Map<String, Object> map){
		return buyApplypurchaseShopMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyApplypurchaseShopMapper.queryCount(map);
	}
	
	public void add(BuyApplypurchaseShop buyApplypurchaseShop){
		buyApplypurchaseShopMapper.add(buyApplypurchaseShop);
	}
	
	public void update(BuyApplypurchaseShop buyApplypurchaseShop){
		buyApplypurchaseShopMapper.update(buyApplypurchaseShop);
	}
	
	public void delete(String id){
		buyApplypurchaseShopMapper.delete(id);
	}
	
	public List<BuyApplypurchaseShop> selectByItemId(String itemId){
		return buyApplypurchaseShopMapper.selectByItemId(itemId);
	}

	public Map<String,Object> selectOverPlusNum(Map<String, Object> map){
		return buyApplypurchaseShopMapper.selectOverPlusNum(map);
	}
}
