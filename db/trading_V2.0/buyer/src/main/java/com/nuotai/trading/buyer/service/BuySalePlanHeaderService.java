package com.nuotai.trading.buyer.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyApplypurchaseShopMapper;
import com.nuotai.trading.buyer.dao.BuySalePlanHeaderMapper;
import com.nuotai.trading.buyer.dao.BuySalePlanItemMapper;
import com.nuotai.trading.buyer.model.BuySalePlanHeader;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.TradeVerifyCopy;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DateUtils;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class BuySalePlanHeaderService {

	@Autowired
	private BuySalePlanHeaderMapper buySalePlanHeaderMapper;
	@Autowired
	private BuySalePlanItemMapper buySalePlanItemMapper;
	@Autowired
	private BuyApplypurchaseShopMapper applypurchaseShopMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;

	public BuySalePlanHeader get(String id){
		BuySalePlanHeader header = buySalePlanHeaderMapper.get(id);
		if(header != null){
			List<BuySalePlanItem> itemList = buySalePlanItemMapper.selectByHeaderId(header.getId());
			if(itemList != null && itemList.size() > 0){
				header.setItemList(itemList);
			}
		}
		return header;
	}
	
	public List<BuySalePlanHeader> queryList(Map<String, Object> map){
		return buySalePlanHeaderMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buySalePlanHeaderMapper.queryCount(map);
	}
	
	public void add(BuySalePlanHeader buySalePlanHeader){
		buySalePlanHeaderMapper.add(buySalePlanHeader);
	}
	
	public void update(BuySalePlanHeader buySalePlanHeader){
		buySalePlanHeaderMapper.update(buySalePlanHeader);
	}
	
	public void delete(String id){
		buySalePlanHeaderMapper.delete(id);
	}
	
	public void saveAddSalePlan(Map<String,Object> map)throws Exception{
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Date date = new Date();
		BuySalePlanHeader header = new BuySalePlanHeader();
		header.setId(ShiroUtils.getUid());
		header.setCompanyId(ShiroUtils.getCompId());
		header.setPlanCode("XSJH" + DateUtils.format(new Date(),"yyyyMMddHHmmss") + ShiroUtils.getRandom(1));
		header.setStatus("1");//2.0版本系统默认审批通过
		header.setIfOrder("0");//是否下单0未下单，1已下单
		header.setIsDel(Constant.IsDel.NODEL.getValue());
		header.setCreateId(user.getId());
		header.setCreateName(user.getUserName());
		header.setCreateDate(date);
		
		List<BuySalePlanItem> itemList = com.alibaba.fastjson.JSONArray.parseArray(String.valueOf(map.get("goodsStr")),BuySalePlanItem.class);
		for (BuySalePlanItem item : itemList) {
			item.setId(ShiroUtils.getUid());
			item.setHeaderId(header.getId());
			item.setIsConfirm(0);//未确认
			buySalePlanItemMapper.add(item);
		}
		try {
			buySalePlanHeaderMapper.add(header);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<BuySalePlanHeader> selectByPage(SearchPageUtil searchPageUtil){
		List<BuySalePlanHeader> headerList = buySalePlanHeaderMapper.selectByPage(searchPageUtil);
		if(headerList != null && headerList.size() > 0){
			for(BuySalePlanHeader header : headerList){
				List<BuySalePlanItem> itemList = buySalePlanItemMapper.selectByHeaderId(header.getId());
				if(itemList != null && itemList.size() > 0){
					header.setItemList(itemList);
					for(BuySalePlanItem item : itemList){
						Map<String,Object> map = new HashMap<String, Object>();
						map.put("planCode", header.getPlanCode());
						map.put("barcode", item.getBarcode());
						map.put("shopId", item.getShopId());
						//剩余销售计划
						Map<String,Object> confirmSalesNumMap = applypurchaseShopMapper.selectOverPlusNum(map);
						if(confirmSalesNumMap != null){
							int confirmSalesNum = confirmSalesNumMap.get("confirmSalesNum") == null ? 0 : Integer.parseInt(confirmSalesNumMap.get("confirmSalesNum").toString());
							item.setConfirmSalesNum(confirmSalesNum);
						}else{
							item.setConfirmSalesNum(0);
						}
					}
				}
			}
		}
		return headerList;
	}
	
	/**
	 * 采购计划-列表数量
	 * @param params
	 */
	public void getSalePlanNum(Map<String, Object> params) {
		//待审核
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("status",Constant.PurchaseStatus.WAITVERIFY.getValue());
		int waitVerifyNum = selectSalePlanNumByMap(selectMap);
		//审核通过
		selectMap.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		int verifyPassNum = selectSalePlanNumByMap(selectMap);
		//审核不通过
		selectMap.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		int verifyNoPassNum = selectSalePlanNumByMap(selectMap);
		params.put("waitVerifyNum", waitVerifyNum);
		params.put("verifyPassNum", verifyPassNum);
		params.put("verifyNoPassNum", verifyNoPassNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			break;
		case 1:
			params.put("interest", "1");
			break;
		case 2:
			params.put("interest", "2");
			break;
		case 3:
			params.put("interest", "3");
			break;
		default:
			params.put("interest", "0");
			break;
		}
	}
	
	public int selectSalePlanNumByMap(Map<String, Object> params) {
		return buySalePlanHeaderMapper.selectSalePlanNumByMap(params);
	}
	
	/**
	 * 获得数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public String loadData(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue()+"");
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/applypurchase/salePlanList/";
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			returnPath += "allSalePlan";
			break;
		case 1:
			params.put("interest", "1");
			returnPath += "waitVerify";
			break;
		case 2:
			params.put("interest", "2");
			returnPath += "verifyPass";
			break;
		case 3:
			params.put("interest", "3");
			returnPath += "verifyNoPass";
			break;
		default:
			returnPath += "allPurchase";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待审批
			params.put("status", Constant.PurchaseStatus.WAITVERIFY.getValue());
		}else if("2".equals(interest)){
			//审核通过
			params.put("status", Constant.PurchaseStatus.VERIFYPASS.getValue());
		}else if("3".equals(interest)){
			//审核不通过
			params.put("status", Constant.PurchaseStatus.VERIFYNOPASS.getValue());
		}
		searchPageUtil.setObject(params);
		List<BuySalePlanHeader> headerList = selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(headerList);
		return returnPath;
	}
	
	/**
	 * 销售计划审核通过
	 * @param id
	 * @return
	 */
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		BuySalePlanHeader header = buySalePlanHeaderMapper.get(id);
		header.setStatus(Constant.PurchaseStatus.VERIFYPASS.getValue()+"");
		int result = buySalePlanHeaderMapper.update(header);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	
	/**
	 * 销售计划审核驳回
	 * @param id
	 * @return
	 */
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		BuySalePlanHeader header = buySalePlanHeaderMapper.get(id);
		header.setStatus(Constant.PurchaseStatus.VERIFYNOPASS.getValue()+"");
		int result = buySalePlanHeaderMapper.update(header);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	
	public List<Map<String,Object>> selectByStatistic(Map<String, Object> map){
		return buySalePlanHeaderMapper.selectByStatistic(map);
	}
	
	/**
	 * 获得待审批的销售计划
	 * @param searchPageUtil
	 * @param params
	 */
	public void loadApprovedData(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		params.put("companyId", ShiroUtils.getCompId());
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		params1.put("siteId", "18011517573773244811");
		//获得所有未审批的id
		List<String> applyIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> applyIdListNew = new ArrayList<>();

		String procode = params.containsKey("procode")?params.get("procode")!=null?params.get("procode").toString():"":"";
		String shopId = params.containsKey("shopId")?params.get("shopId")!=null?params.get("shopId").toString():"":"";
//		String skucode = params.containsKey("skucode")?params.get("skucode")!=null?params.get("skucode").toString():"":"";
//		String skuname = params.containsKey("skuname")?params.get("skuname")!=null?params.get("skuname").toString():"":"";
		String skuoid = params.containsKey("skuoid")?params.get("skuoid")!=null?params.get("skuoid").toString():"":"";
		if(!StringUtils.isEmpty(procode)
			||!StringUtils.isEmpty(shopId)
//			||!StringUtils.isEmpty(skucode)
//			||!StringUtils.isEmpty(skuname)
			||!StringUtils.isEmpty(skuoid)
		){
			List<String> applyIdList2 = buySalePlanHeaderMapper.selectApprovedId(params);
			if(!applyIdList2.isEmpty()){
				Map<String, String> applyIdMap = new HashMap<>();
				for(String applyId : applyIdList2){
					applyIdMap.put(applyId, applyId);
				}
				if(!applyIdList.isEmpty()){
					for(String applyId : applyIdList){
						if(applyIdMap.containsKey(applyId)){
							applyIdListNew.add(applyId);
						}
					}
				}
			}
		}else{
			applyIdListNew = applyIdList;
		}
		params.put("approved", true);
		params.put("applyIdList", applyIdListNew);
		searchPageUtil.setObject(params);
		List<BuySalePlanHeader> applyPurchaseList = buySalePlanHeaderMapper.selectApprovedByPage(searchPageUtil);
		if(!applyPurchaseList.isEmpty()){
			for(BuySalePlanHeader buySalePlanHeader:applyPurchaseList){
				List<BuySalePlanItem> applyItemList = buySalePlanItemMapper.selectByHeaderId(buySalePlanHeader.getId());
				if(!applyItemList.isEmpty()){
					buySalePlanHeader.setItemList(applyItemList);
				}
			}
		}
		searchPageUtil.getPage().setList(applyPurchaseList);
	}
	
	/**
	 * 一键审批
	 * @param applyId
	 * @return
	 */
	public JSONObject verifyAll(String applyId) {
		JSONObject json = new JSONObject();
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(applyId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						verifySuccess(applyId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
		return json;
	}

	public void saveAddSalePlanNew(Map<String, Object> map,
			List<BuySalePlanItem> itemList) throws Exception{
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Date date = new Date();
		BuySalePlanHeader header = new BuySalePlanHeader();
		header.setId(ShiroUtils.getUid());
		header.setCompanyId(ShiroUtils.getCompId());
		header.setPlanCode("XSJH" + DateUtils.format(new Date(),"yyyyMMddHHmmss") + ShiroUtils.getRandom(3));
		header.setStatus("1");
		header.setIfOrder("0");
		header.setRemark("【批量导入】");
		header.setIsDel(Constant.IsDel.NODEL.getValue());
		header.setCreateId(user.getId());
		header.setCreateName(user.getUserName());
		header.setCreateDate(date);
		for (BuySalePlanItem item : itemList) {
			item.setId(ShiroUtils.getUid());
			item.setHeaderId(header.getId());
			item.setShopId(map.get("shopId").toString());
			item.setShopCode(map.get("shopCode").toString());
			item.setShopName(map.get("shopName").toString());
			String start_date = map.containsKey("importStartDate") ? map.get("importStartDate") != null
					&& !"".equals(map.get("importStartDate")) ? map.get("importStartDate").toString() : "" : "";
			String end_date = map.containsKey("importEndDate") ? map.get("importEndDate") != null
					&& !"".equals(map.get("importEndDate")) ? map.get("importEndDate").toString() : "" : "";
			if (!"".equals(start_date)) {
				item.setStartDate(sf.parse(start_date));
				item.setEndDate(sf.parse(end_date));
				item.setSaleDays(Integer.parseInt(map.get("importSaleDays").toString()));//天数
			}
			item.setPlanType(Integer.parseInt(map.get("importPlanType").toString()));
			buySalePlanItemMapper.add(item);
		}
		buySalePlanHeaderMapper.add(header);
	}
	
	/**
	 * 确认销售计划
	 * @param id
	 * @param confirmSalesNum
	 * @return
	 * @throws Exception
	 */
	public JSONObject saveConfirmSalePlan(String id,int confirmSalesNum) throws Exception{
		JSONObject json = new JSONObject();
		BuySalePlanItem salePlanItem = buySalePlanItemMapper.get(id);
		salePlanItem.setIsConfirm(1);
		salePlanItem.setConfirmSalesNum(confirmSalesNum);
		int result = buySalePlanItemMapper.update(salePlanItem);
		if(result > 0){
			json.put("success", true);
            json.put("msg", "确认成功！");
		}else{
			json.put("success", false);
            json.put("msg", "确认失败！");
		}
		return json;
	}

	/**
	 * 转交采购计划
	 * @param params
	 * @return
	 */
	public JSONObject nextStep(Map<String, Object> params) {
		JSONObject json = new JSONObject();
		if(params.containsKey("salePlanArray")){
			JSONArray salePlanArray = JSONArray.fromObject(params.get("salePlanArray"));
			
		}else{
			json.put("success", false);
            json.put("msg", "转交失败！");
		}
		return null;
	}
}
