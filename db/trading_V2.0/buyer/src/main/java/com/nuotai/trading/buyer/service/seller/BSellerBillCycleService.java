package com.nuotai.trading.buyer.service.seller;

import com.nuotai.trading.buyer.dao.seller.BSellerBillCycleManagementMapper;
import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 账单周期卖家
 * @author yuyafei
 * @date 2017-8-17
 *
 */
@Service
public class BSellerBillCycleService {
	@Autowired
	private BSellerBillCycleManagementMapper bSellerBillCycleManagementMapper;

	public int deleteByPrimaryKey(String id) {
		return bSellerBillCycleManagementMapper.deleteByPrimaryKey(id);
	}

	//根据买家添加的数据同步到卖家数据库中
	public int addSellerBillCycle(BSellerBillCycleManagement record) {
		return bSellerBillCycleManagementMapper.addSellerBillCycle(record);
	}

	public int insertSelective(BSellerBillCycleManagement record) {
		return 0;
	}

	//根据编号查询数据
	public BSellerBillCycleManagement selectByPrimaryKey(String id) {
		return bSellerBillCycleManagementMapper.selectByPrimaryKey(id);
	}

	//根据买家修改的数据同步到卖家数据库中
	public int updateByPrimaryKeySelective(BSellerBillCycleManagement record) {
		return bSellerBillCycleManagementMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BSellerBillCycleManagement record) {
		return 0;
	}

	
}
