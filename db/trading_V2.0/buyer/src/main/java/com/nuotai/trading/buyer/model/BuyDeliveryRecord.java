package com.nuotai.trading.buyer.model;

import com.nuotai.trading.model.BuyAttachment;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 收货单信息
 * @author gsf
 * @date 2017-08-10
 */
@Data
public class BuyDeliveryRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	//
	private String id;
	//所属公司
	private String companyId;
	//发货单号
	private String deliverId;
	private String deliverNo;
	//发货类型0:采购发货 1:换货发货
	private Integer deliverType;
	//供货商id
	private String supplierId;
	//供货商代码
	private String supplierCode;
	//供货商名称
	private String supplierName;
	//发货状态[0、全部发货；1、部分发货]
	private Integer status;
	//发货单[0发货中；1待对方确认发货中；2交易完成]状态
	private Integer recordstatus;
	//是否已对账（0：未对账， 1：正在对帐，2：已完成对账）
	private String reconciliationStatus;
	//是否已经开票（0 未开票 1 正在开票 2 开票成功 3开票失败）
	private String isInvoiceStatus;
	//是否已付款（0：未付款， 1：正在付款，2：已付款）
	private String isPaymentStatus;
	//收货凭证地址
	private String receiptvoucherAddr;
	//收货凭证确认状态 0:未确认 1:已确认 2:驳回
	private String receiptvoucherCheck;
	//备注
	private String remark;
	//代发货类型（0.正常发货1.代发客户2.代发菜鸟仓3.代发京东仓）
	private String dropshipType;
	//收货地址
	private String receiveAddr;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//创建人id
	private String createrId;
	//创建人姓名
	private String createrName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人名称
	private String delName;
	//删除时间
	private Date delDate;
	//确认时间
	private  Date arrivalDate;
	//确认时间
	private String arrivalDateStr;
	//发货明细
	List<BuyDeliveryRecordItem> itemList;
	//是否从WMS仓库抓取 Y:是 N：否
	private String isFromWms;
	/**
	 *收货凭证附件
	 */
	private List<BuyAttachment> receiptVoucherList;
	//运费
	private String freight;
	//物流id
	private String logisticsId;
	//二维码地址
	private String qrcodeAddr;
	//收货仓库
	private String warehouseName;
	//收货仓库
	private String pushType;
}
