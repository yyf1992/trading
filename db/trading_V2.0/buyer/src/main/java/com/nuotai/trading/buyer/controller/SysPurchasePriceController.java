package com.nuotai.trading.buyer.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysPurchasePrice;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.SysPurchasePriceService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;



/**
 * 商品采购价格表
 * @author "
 * @date 2017-10-25 14:32:58
 */
@Controller
@RequestMapping("platform/buyer/syspurchaseprice")
public class SysPurchasePriceController extends BaseController{
	@Autowired
	private SysPurchasePriceService sysPurchasePriceService;
	@Autowired
	private BuyOrderProductService buyOrderProductService;
	
	/**
	 * 加载采购统计数据
	 */
	@RequestMapping("/loadPurchaseData")
	public String loadPurchaseData(@RequestParam Map<String,String> map){
		if(!map.containsKey("companyId")){
			map.put("companyId", ShiroUtils.getCompId());
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(!map.containsKey("startDate")){
			map.put("startDate", yesterday);
		}
		if(!map.containsKey("endDate")){
			map.put("endDate", yesterday);
		}
		List<Map<String,Object>>  list=sysPurchasePriceService.getPurchaseData(map);
		List<Map<String,Object>>  purchaseList=new ArrayList<>();
		for (Map<String, Object> purchase:list) {
			//供应商价格
			Double price=Double.parseDouble(purchase.get("price").toString());
			//实际到货数量
			int factNumber=Integer.parseInt(purchase.get("fact_number").toString()) ;
			//采购价
			Double purchasePrice=price;
			//加价值/加价百分比
			Double priceLatitude=purchase.containsKey("price_latitude")?Double.parseDouble(purchase.get("price_latitude").toString()):0.0;
			//商品提价类型   0 加价  1 提成
			String priceStatus=purchase.containsKey("price_status")?purchase.get("price_status").toString():"";
			
			if(priceStatus!=null && priceStatus != ""){
				if("0".equals(priceStatus)){
					purchasePrice=price + priceLatitude;
				}else if("1".equals(priceStatus)){
					purchasePrice=price *(100+priceLatitude)/100 ;
				}
			}
			purchase.put("purchasePrice", purchasePrice);
			//毛利
			purchase.put("grossProfit", (purchasePrice-price)*factNumber);
			//采购总金额
			purchase.put("totalMoney", price*factNumber);
			purchaseList.add(purchase);
		}
		model.addAttribute("map",map);
		model.addAttribute("purchaseList",purchaseList);
		return "platform/buyer/sysShopProduct/purchaseList";
	}
	
	/**
	 * 加载导入数据页面
	 * @return
	 */
	@RequestMapping(value="/loadImportExcelHtml",method=RequestMethod.POST)
	public String loadImportExcelHtml(){
		return "platform/buyer/sysShopProduct/importExcel";
	}
	
	/**
	 * 导入数据
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/importDate", method = RequestMethod.POST)
	@ResponseBody
	public String readExcel(@RequestParam(required = false) MultipartFile excel)
			throws Exception {
		JSONObject json = new JSONObject();
		if (excel != null) {
			String fileName = excel.getOriginalFilename();// 获取文件名
			String ext = FilenameUtils.getExtension(fileName);// 获取扩展名
			InputStream is = excel.getInputStream();
			if ("xls".equals(ext) || "xlsx".equals(ext)) {// 判断文件格式
				boolean is03file = "xls".equals(ext) ? true : false;
				Workbook workbook = is03file ? new HSSFWorkbook(is)
						: new XSSFWorkbook(is);
				Sheet sheet = workbook.getSheetAt(0);//获得第一个表单 
				List<SysPurchasePrice> purchaseList = new ArrayList<SysPurchasePrice>();
				int rowNum = sheet.getLastRowNum();// 获取excel行数
				for (int i = 1; i <= rowNum; i++) {
					try {
						Row row = sheet.getRow(i);
						if (!ExcelUtil.isBlankRow(row)) {
							SysPurchasePrice sysPurchase = new SysPurchasePrice();
							if (row.getCell(0).getStringCellValue() != null && row.getCell(0).getStringCellValue()!= "") {// 设置货号
								sysPurchase.setProductCode(row.getCell(0).getStringCellValue());
							}
							// 设置商品条形码
							String barcode = "";
							if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
								DecimalFormat df = new DecimalFormat("0");
								barcode = df.format(row.getCell(1)
										.getNumericCellValue());
							} else {
								barcode = row.getCell(1).getStringCellValue();
							}
							sysPurchase.setBarcode(barcode);
							// 设置商品条形码
							String priceStatus = "";
							if (row.getCell(2).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
								DecimalFormat df = new DecimalFormat("0");
								priceStatus = df.format(row.getCell(2).getNumericCellValue());
							} else {
								priceStatus = row.getCell(2).getStringCellValue();
							}
							sysPurchase.setPriceStatus(priceStatus);
							if (row.getCell(3).getNumericCellValue() != 0) {
								sysPurchase.setPriceLatitude(new BigDecimal(row
										.getCell(3).getNumericCellValue()));
							}
							purchaseList.add(sysPurchase);
						}

					} catch (Exception e) {
						json.put("status", "fail");
						json.put("msg", e.getMessage());
						e.printStackTrace();
						return json.toString();
					}
				}
				try {
					String ss = JSONArray.fromObject(purchaseList)
							.toString();
					System.out.println(ss);
					// 根据 货号、条形码、店铺名称做修改操作
					sysPurchasePriceService.updateFromExcel(purchaseList);

					json.put("status", "success");
					json.put("msg", "导入数据成功!");
				} catch (Exception e) {
					json.put("status", "fail");
					json.put("msg", e.getMessage());
					e.printStackTrace();
				}
			} else {
				json.put("status", "fail");
				json.put("msg", "导入文件格式不正确");
			}
		} else {
			json.put("status", "fail");
			json.put("msg", "必须导入一个excel文件");
		}
		return json.toString();
	}
	
	/**
	 * 下载模板
	 * @param response
	 * @param request
	 */
	@RequestMapping("/downloadExcel")
	public void downloadExcel(HttpServletResponse response,HttpServletRequest request) {
        try {
            //获取文件的路径
            String excelPath = request.getSession().getServletContext().getRealPath("statics/file/"+"产品采购价格设置导入模板.xlsx");
            //String fileName = "采购价格设置模板.xlsx".toString(); // 文件的默认保存名
            // 读到流中
            InputStream inStream = new FileInputStream(excelPath);//文件的存放路径
            // 设置输出的格式
            response.reset();
            response.setContentType("bin");
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode("产品采购价格设置导入模板.xlsx", "UTF-8"));
            OutputStream os = response.getOutputStream();
            // 循环取出流中的数据
            byte[] b = new byte[200];
            int len;

            while ((len = inStream.read(b)) > 0){
            	os.write(b, 0, len);
            }
            // 这里主要关闭。
            os.close();
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
}
