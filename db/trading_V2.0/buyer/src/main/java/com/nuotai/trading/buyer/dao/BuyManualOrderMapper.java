package com.nuotai.trading.buyer.dao;


import com.nuotai.trading.buyer.model.BuyManualOrder;

public interface BuyManualOrderMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyManualOrder record);

    int insertSelective(BuyManualOrder record);

    BuyManualOrder selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyManualOrder record);

    int updateByPrimaryKey(BuyManualOrder record);
    
    BuyManualOrder selectByOrderId(String orderId);
}