package com.nuotai.trading.buyer.service;


import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuySupplierMapper;
import com.nuotai.trading.buyer.dao.BuySupplierUserMapper;
import com.nuotai.trading.buyer.model.BuySupplierUser;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuyShopProduct;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.BuySupplierLinkman;
import com.nuotai.trading.model.SysRoleCompany;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.service.BuySupplierLinkmanService;
import com.nuotai.trading.service.SysRoleCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BuySupplierService {
	@Autowired
	private BuySupplierMapper mapper;
	@Autowired
	private BuySupplierLinkmanService supplierLinkmanService;
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private BuySupplierFriendService friendService;
	@Autowired
	private SysUserService userService;
	@Autowired
	private SysRoleCompanyService sysRoleCompanyService;
	@Autowired
	private BuyCompanyService buyCompanyService;
	@Autowired
	private BuyShopProductService buyShopProductService;
	@Autowired
	private BuySupplierUserMapper buySupplierUserMapper;
	
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	public int insert(BuySupplier record) {
		return mapper.insert(record);
	}

	public int insertSelective(BuySupplier record) {
		return mapper.insertSelective(record);
	}

	public BuySupplier selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuySupplier record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuySupplier record) {
		return mapper.updateByPrimaryKey(record);
	}

	public List<BuySupplier> selectByMap(Map<String, Object> map) {
		List<BuySupplier> supplierList = mapper.selectByMap(map);
		if(supplierList != null && supplierList.size() > 0){
			for(BuySupplier supplier : supplierList){
				BuySupplierLinkman suppLinkMan = supplierLinkmanService.selectDefaultlinkMan(supplier.getId());
				if(!ObjectUtil.isEmpty(suppLinkMan)){
					supplier.setPerson(suppLinkMan.getPerson());
					supplier.setPhone(suppLinkMan.getPhone());
				}

			}
		}
		return supplierList;
	}
	
	public List<BuySupplier> getSupplier(){
		Map<String,String> map =new HashMap<>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("isDel","0");
		return mapper.getSupplier(map);
	}

	public List<Map<String,Object>> selectByPage(SearchPageUtil searchPageUtil) {
		List<Map<String,Object>> buySupplierList = mapper.selectByPage(searchPageUtil);
		if(buySupplierList != null && !buySupplierList.isEmpty()){
			for(Map<String,Object> supplier : buySupplierList){
				String supplierId = supplier.get("id").toString();
				List<BuySupplierLinkman> linkmanList = supplierLinkmanService.selectBySupplierId(supplierId);
				if (!ObjectUtil.isEmpty(linkmanList)){
					String person = linkmanList.get(0).getPerson();
					String phone = linkmanList.get(0).getPhone();
					supplier.put("person", person);
					supplier.put("phone", phone);
				}
			}
		}
		return buySupplierList;
	}

	public int saveAddSupplier(BuySupplier supplier, Map<String, Object> mapLinkman)
			throws Exception {
		//解析供应商联系人
	    List<BuySupplierLinkman> linmanStrList = JSONObject.parseArray(String.valueOf(mapLinkman.get("linkmanStr")),BuySupplierLinkman.class);
		int result = 0;

		//往公司表中插入数据
		BuyCompany buyCompany = new BuyCompany();
		buyCompany.setId(ShiroUtils.getUid());
		buyCompany.setCompanyCode("GYS"+Calendar.getInstance().getTimeInMillis());
		buyCompany.setCompanyName(supplier.getSuppName());
		buyCompany.setProvince(supplier.getProvince());
		buyCompany.setCity(supplier.getCity());
		buyCompany.setArea(supplier.getArea());
		buyCompany.setDetailAddress(supplier.getAddress());
		buyCompany.setLinkMan(linmanStrList.get(0).getPerson());
		buyCompany.setZone(linmanStrList.get(0).getZone());
		buyCompany.setTelNo(linmanStrList.get(0).getTelNo());
		buyCompany.setFax1(supplier.getSuppName());//此字段与sysUser的userName必须一致，否则登录不上卖家角色
		/*buyCompany.setFax2(linmanStrList.get(1).getFax());*/
		buyCompany.setQq(linmanStrList.get(0).getQq());
		buyCompany.setWangNo(linmanStrList.get(0).getWangNo());
		buyCompany.setEmail(linmanStrList.get(0).getEmail());
		buyCompany.setStatus(1);//审核通过
		buyCompany.setReason("");//审核意见
		buyCompany.setIsDel(Constant.IsDel.NODEL.getValue());//是否删除
		buyCompany.setVerifyDate(new Date());//审核时间
		buyCompany.setCreateDate(new Date());
		buyCompany.setCreateId(ShiroUtils.getUserId());
		buyCompany.setCreateName(ShiroUtils.getUserName());
		Map<String ,Object> amap = new HashMap<String, Object>();
		amap.put("companyName", supplier.getSuppName());
		List<BuyCompany> companyList = buyCompanyService.selectByMap(amap);
		if(companyList != null && companyList.size() > 0){
			throw new Exception("该公司名称已存在！");
		}
		buyCompanyService.insertSelective(buyCompany);
		
		//供应商表插入数据
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isDel", Constant.IsDel.NODEL.getValue()); // 未删除
		map.put("companyId", buyCompany.getId());
		map.put("friendId", ShiroUtils.getCompId());
		List<BuySupplier> supplierList = mapper.selectByMap(map);
		if(supplierList != null && supplierList.size() > 0){
			// 名字存在
			throw new Exception("该公司已存在！");
		}
		Date date = new Date();
		supplier.setId(ShiroUtils.getUid());
		supplier.setCompanyId(buyCompany.getId());
		supplier.setAddType("0");// 手工添加
		supplier.setType("0");//互通
		supplier.setFriendId(ShiroUtils.getCompId());
		supplier.setIsDel(Constant.IsDel.NODEL.getValue()); // 未删除
		supplier.setCreateId(user.getId());
		supplier.setCreateName(user.getUserName());
		supplier.setCreateDate(date);
		result = mapper.insert(supplier);
		
		//把供应商数据插入到互通好友表中
		BuySupplierFriend  friend=new BuySupplierFriend();
		friend.setId(ShiroUtils.getUid());
		friend.setSupplierId(supplier.getId());
		friend.setBuyersId(ShiroUtils.getCompId());
		friend.setBuyersName(ShiroUtils.getCompanyName());
		friend.setIsDel(Constant.IsDel.NODEL.getValue());
		friend.setCreateDate(new Date());
		friend.setCreateId(ShiroUtils.getUserId());
		friend.setCreateName(ShiroUtils.getUserName());
		friend.setSellerName(buyCompany.getCompanyName());
		friend.setSellerId(buyCompany.getId());
		Map<String,Object> friendMap =new HashMap<>();
		friendMap.put("sellerId", friend.getSellerId());
		friendMap.put("buyersId", friend.getBuyersId());
		List<BuySupplierFriend> friendList=friendService.selectByMap(friendMap);
		if(friendList != null && friendList.size()>0){
			throw new Exception("互通好友已存在！");
		}
		friendService.insertSelective(friend);
		
		//创建互通供应商的登录账号
		SysUser  suppUser=new SysUser();
		suppUser.setId(ShiroUtils.getUid());
		suppUser.setCompanyId(buyCompany.getId());
		suppUser.setCompanyName(buyCompany.getCompanyName());
		suppUser.setLoginName(buyCompany.getCompanyName());
		suppUser.setIsDel(Constant.IsDel.NODEL.getValue());
		suppUser.setUserName(buyCompany.getCompanyName());
		suppUser.setPassword(ShiroUtils.getMD5("123456", "UTF-8", 0).toLowerCase());
		suppUser.setCreateDate(new Date());
		suppUser.setCreateId(ShiroUtils.getUserId());
		suppUser.setCreateName(ShiroUtils.getUserName());
		JSONObject json=userService.saveSupplierInsert(suppUser);
		if(!json.getBooleanValue("success")){
			throw new Exception("添加供应商失败！");
		}
		
		//给供应商添加默认卖家角色
		SysRoleCompany roleCompany = new SysRoleCompany();
		roleCompany.setId(ShiroUtils.getUid());
		roleCompany.setCompanyId(buyCompany.getId());
		roleCompany.setRoleId("17070710405726016419");
		roleCompany.setCreateTime(new Date());
		sysRoleCompanyService.insert(roleCompany);
		
		// 添加联系人表
		if(!ObjectUtil.isEmpty(linmanStrList)&&linmanStrList.size()>0){
			for (BuySupplierLinkman supplierLinkman : linmanStrList) {
				supplierLinkman.setId(ShiroUtils.getUid());
				supplierLinkman.setSupplierId(supplier.getId());

				supplierLinkmanService.insert(supplierLinkman);
			}
		}
		//附件表添加
		attachmentService.addAttachment(mapLinkman, supplier.getId());
		return result;
		
	}

	public void saveDeleteSupplier(String id) {
		Date date = new Date();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		BuySupplier supp = new BuySupplier();
		supp.setId(id);
		supp.setIsDel(-1);
		supp.setDelId(user.getId());
		supp.setDelName(user.getUserName());
		supp.setDelDate(date);
		mapper.updateByPrimaryKeySelective(supp);
	}
	
	public void saveFrozenSupplier(String id,String suppName,String frozenReason,int status) {
		BuySupplierFriend friend = friendService.selectBySellerName(suppName);
		if (!ObjectUtil.isEmpty(friend)){
			BuyShopProduct buyShopProduct = new BuyShopProduct();
			buyShopProduct.setSupplierId(friend.getSellerId());
			Date date = new Date();
			BuySupplier supp = new BuySupplier();
			supp.setId(id);
			supp.setIsFrozen(status);
			supp.setFrozenDate(date);
			supp.setFrozenName(ShiroUtils.getUserName());
			//取消冻结  删除冻结原因
			if(status==0){
				supp.setFrozenReason("");
				buyShopProduct.setIsDel(Constant.IsDel.NODEL.getValue());
				buyShopProductService.updateBySupplierId(buyShopProduct);
			}else if(status==1){//冻结供应商
				if(frozenReason!=null && !frozenReason.isEmpty()){
					supp.setFrozenReason(frozenReason);
				}
				buyShopProduct.setIsDel(Constant.IsDel.YESDEL.getValue());
				buyShopProductService.updateBySupplierId(buyShopProduct);
			}
			mapper.updateByPrimaryKeySelective(supp);
		}
	}

	public void saveBeatchDeleteSupplier(String ids) {
		BuySupplier supp = new BuySupplier();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		String[] idStr = ids.split(",");
		Date date = new Date();
		for (int i = 0; i < idStr.length; i++) {
			supp.setId(idStr[i]);
			supp.setIsDel(-1);
			supp.setDelId(user.getId());
			supp.setDelName(user.getUserName());
			supp.setDelDate(date);
			mapper.updateByPrimaryKeySelective(supp);
		}
	}

	/**
	 * 修改供应商保存
	 */
	public void saveUpdateSupplier(BuySupplier supplier, Map<String, Object> mapLinkman)
			throws Exception {
		//解析供应商联系人
	    List<BuySupplierLinkman> linmanStrList = JSONObject.parseArray(String.valueOf(mapLinkman.get("linkmanStr")),BuySupplierLinkman.class);
	    
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isDel", Constant.IsDel.NODEL.getValue()); // 未删除
		map.put("suppName", supplier.getSuppName());
		map.put("notId", supplier.getId());
//		List<BuySupplier> supplierList = mapper.selectByMap(map);
//		if(supplierList != null && supplierList.size() > 0){
//			// 名字存在
//			throw new Exception("该名称已存在！");
//		}
		Date date = new Date();
		supplier.setUpdateId(user.getId());
		supplier.setUpdateName(user.getCreateName());
		supplier.setUpdateDate(date);
		mapper.updateByPrimaryKeySelective(supplier);
		
		// 添加联系人表
		List<BuySupplierLinkman> linkmanList = supplierLinkmanService.selectBySupplierId(supplier.getId());
		for(BuySupplierLinkman lm: linkmanList){
			supplierLinkmanService.deleteByPrimaryKey(lm.getId());
		}

		//保存供应商联系人
		if(!ObjectUtil.isEmpty(linmanStrList)&&linmanStrList.size()>0){
			for (BuySupplierLinkman supplierLinkman : linmanStrList) {
				supplierLinkman.setId(ShiroUtils.getUid());
				supplierLinkman.setSupplierId(supplier.getId());

				supplierLinkmanService.insert(supplierLinkman);
			}
		}
		//删除附件
		attachmentService.deleteByRelatedId(supplier.getId());
		//附件表添加
		attachmentService.addAttachment(mapLinkman, supplier.getId());
		
		//更新互通好友表
		BuySupplierFriend  friend=new BuySupplierFriend();
		friend.setSupplierId(supplier.getId());
		friend.setSellerName(supplier.getSuppName());
		friend.setUpdateId(ShiroUtils.getUserId());
		friend.setUpdateName(ShiroUtils.getUserName());
		friend.setUpdateDate(date);
		friendService.updateBySupplierId(friend);
		friend = friendService.selectBySellerName(supplier.getSuppName());
		
		//更新公司
//		Map<String ,Object> amap = new HashMap<String, Object>();
//		amap.put("companyName", supplier.getSuppName());
//		List<BuyCompany> companyList = buyCompanyService.selectByMap(amap);
//		if(companyList != null && companyList.size() > 0){
//			throw new Exception("该公司名称已存在！");
//		}
		BuyCompany buyCompany = new BuyCompany();
		buyCompany.setId(friend.getSellerId());
		buyCompany.setCompanyName(supplier.getSuppName());
		//此字段与sysUser的userName必须一致，否则登录不上卖家角色
		buyCompany.setFax1(supplier.getSuppName());
		buyCompany.setUpdateId(ShiroUtils.getUserId());
		buyCompany.setUpdateName(ShiroUtils.getUserName());
		buyCompany.setUpdateDate(date);
		buyCompanyService.updateByPrimaryKeySelective(buyCompany);	
		
		//更新互通供应商的登录账号
		SysUser  suppUser=new SysUser();
		suppUser.setCompanyId(friend.getSellerId());
		suppUser.setLoginName(supplier.getSuppName());
		suppUser.setUserName(supplier.getSuppName());
		suppUser.setUpdateId(ShiroUtils.getUserId());
		suppUser.setUpdateName(ShiroUtils.getUserName());
		suppUser.setUpdateDate(date);
		userService.updateByCompanyId(suppUser);
	}

	public List<Map<String, Object>> selectSupplierByMap(Map<String, Object> map) {
		return mapper.selectSupplierByMap(map);
	}
	
	/**
	 * 查询所有供应商
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> selectAllSupplier() {
		return mapper.selectAllSupplier();
	}

	/**
	 * 保存同步OMS供应商数据
	 * @param mapLinkman
	 * @return
	 * @throws Exception
	 */
	public int saveAddOMSSupplier(net.sf.json.JSONObject mapLinkman) throws Exception {
		int result = 0;

		//供应商表插入数据
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		BuySupplier supplier = new BuySupplier();
		Date date = new Date();
		supplier.setId(ShiroUtils.getUid());
		supplier.setCompanyId(ShiroUtils.getCompId());
		//供应商代码
		supplier.setCompanyCode(mapLinkman.getString("code"));
		supplier.setSuppName(mapLinkman.getString("name").trim());
		//级别
		supplier.setLinklevel(mapLinkman.getString("linklevel"));
		//OMS供应商ID
		supplier.setSupplierId(mapLinkman.getString("id"));
		if(mapLinkman.containsKey("linkaddr")){
			supplier.setAddress(mapLinkman.getString("linkaddr").trim());
		}
		supplier.setAddType("0");// 手工添加
		supplier.setType("0");//互通
		supplier.setFriendId(Constant.COMPANY_ID);
		supplier.setIsDel(Constant.IsDel.NODEL.getValue()); // 未删除
		supplier.setCreateId(user.getId());
		supplier.setCreateName(user.getUserName());
        Long dateLong = Long.valueOf(mapLinkman.getString("created"));
        String  dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(dateLong)); 
        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
		supplier.setCreateDate(date);
		Map<String, Object> map = new HashMap<>();
		map.put("suppName", supplier.getSuppName());
		List<BuySupplier> buySupplierList = mapper.selectByMap(map);
		if (ObjectUtil.isEmpty(buySupplierList)){
			result = mapper.insert(supplier);
			
			//往公司表中插入数据
			BuyCompany buyCompany = new BuyCompany();
			buyCompany.setId(mapLinkman.getString("id"));
			buyCompany.setCompanyCode(mapLinkman.getString("code"));
			buyCompany.setCompanyName(mapLinkman.getString("name").trim());
			if(mapLinkman.containsKey("linkaddr")){
				buyCompany.setDetailAddress(mapLinkman.getString("linkaddr").trim());
			}
			if(mapLinkman.containsKey("linkpsn")){
				buyCompany.setLinkMan(mapLinkman.getString("linkpsn"));
			}	
			//此字段与sysUser的userName必须一致，否则登录不上卖家角色
			buyCompany.setFax1(mapLinkman.getString("name").trim());
			if(mapLinkman.containsKey("linkmobile")){
				buyCompany.setFax2(mapLinkman.getString("linkmobile"));
			}		
			buyCompany.setStatus(1);//审核通过
			buyCompany.setReason("");//审核意见
			buyCompany.setIsDel(Constant.IsDel.NODEL.getValue());//是否删除
			buyCompany.setVerifyDate(new Date());//审核时间
			buyCompany.setCreateDate(new Date());
			buyCompany.setCreateId(ShiroUtils.getUserId());
			buyCompany.setCreateName(ShiroUtils.getUserName());
			Map<String ,Object> amap = new HashMap<String, Object>();
			amap.put("companyName", mapLinkman.getString("name"));
			List<BuyCompany> companyList = buyCompanyService.selectByMap(amap);
			if(companyList != null && companyList.size() > 0){
				throw new Exception("该公司名称已存在！");
			}
			buyCompanyService.insertSelective(buyCompany);		
			
			//把供应商数据插入到互通好友表中
			BuySupplierFriend  friend=new BuySupplierFriend();
			friend.setId(ShiroUtils.getUid());
			friend.setSupplierId(supplier.getId());
			friend.setBuyersId(ShiroUtils.getCompId());
			friend.setBuyersName(ShiroUtils.getCompanyName());
			friend.setIsDel(Constant.IsDel.NODEL.getValue());
			friend.setCreateDate(new Date());
			friend.setCreateId(ShiroUtils.getUserId());
			friend.setCreateName(ShiroUtils.getUserName());
			friend.setSellerName(buyCompany.getCompanyName());
			friend.setSellerId(buyCompany.getId());
			if(mapLinkman.containsKey("linkpsn")){
				friend.setSellerPerson(mapLinkman.getString("linkpsn"));
			}	
			if(mapLinkman.containsKey("linkphone")){
				friend.setSellerPhone(mapLinkman.getString("linkphone"));
			}
			Map<String,Object> friendMap =new HashMap<>();
			friendMap.put("sellerId", friend.getSellerId());
			friendMap.put("buyersId", friend.getBuyersId());
			List<BuySupplierFriend> friendList=friendService.selectByMap(friendMap);
			if(friendList != null && friendList.size()>0){
				throw new Exception("互通好友已存在！");
			}
			friendService.insertSelective(friend);
			
			//创建互通供应商的登录账号
			SysUser  suppUser=new SysUser();
			suppUser.setId(ShiroUtils.getUid());
			suppUser.setCompanyId(buyCompany.getId());
			suppUser.setCompanyName(buyCompany.getCompanyName());
			suppUser.setLoginName(buyCompany.getCompanyName());
			suppUser.setIsDel(Constant.IsDel.NODEL.getValue());
			suppUser.setUserName(buyCompany.getCompanyName());
			suppUser.setPassword(ShiroUtils.getMD5("123456", "UTF-8", 0).toLowerCase());
			suppUser.setCreateDate(new Date());
			suppUser.setCreateId(ShiroUtils.getUserId());
			suppUser.setCreateName(ShiroUtils.getUserName());
			JSONObject json=userService.saveSupplierInsert(suppUser);
			if(!json.getBooleanValue("success")){
				throw new Exception("添加供应商失败！");
			}
			
			//给供应商添加默认卖家角色
			SysRoleCompany roleCompany = new SysRoleCompany();
			roleCompany.setId(ShiroUtils.getUid());
			roleCompany.setCompanyId(buyCompany.getId());
			roleCompany.setRoleId("17070710405726016419");
			roleCompany.setCreateTime(new Date());
			sysRoleCompanyService.insert(roleCompany);
		}else{
			if (buySupplierList.size()==1){
				supplier.setId(buySupplierList.get(0).getId());
				mapper.updateByPrimaryKeySelective(supplier);
			}
		}
		
		// 添加联系人表
		if(!ObjectUtil.isEmpty(mapLinkman.getString("name"))){
			BuySupplierLinkman supplierLinkman = new BuySupplierLinkman();
			supplierLinkman.setId(ShiroUtils.getUid());
			supplierLinkman.setSupplierId(supplier.getId());
			if(mapLinkman.containsKey("linkpsn")){
				supplierLinkman.setPerson(mapLinkman.getString("linkpsn").trim());
			}	
			if(mapLinkman.containsKey("linkmobile")){
				supplierLinkman.setPhone(mapLinkman.getString("linkmobile"));
			}
			if(mapLinkman.containsKey("linkphone")){
				supplierLinkman.setTelNo(mapLinkman.getString("linkphone"));
			}
			map.put("supplierId", supplier.getId());
			map.put("person", mapLinkman.getString("linkpsn").trim());
			BuySupplierLinkman  buySupplierLinkman = supplierLinkmanService.selectByMap(map);
			if (ObjectUtil.isEmpty(buySupplierLinkman)){
				supplierLinkmanService.insert(supplierLinkman);
			}else{
				supplierLinkman.setId(buySupplierLinkman.getId());
				supplierLinkmanService.updateByPrimaryKey(supplierLinkman);
			}
			
			
		}

		return result;
		
	}
	
	/**
	 * 获得左侧没有的供应商数据
	 * @param model
	 * @param userId
	 */
	public List<Map<String, Object>> getLeftNotRole(String userId,String supplierName) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("notUserId", userId);
		params.put("supplierName", supplierName.trim());
		//获得没有的供应商
		List<Map<String, Object>> supplierList = mapper.getBuySupplierByMap(params);
		return supplierList;
	}
	
	/**
	 * 获得右侧有的供应商数据
	 * @param model
	 * @param userId
	 */
	public List<Map<String, Object>> getRightRole(String userId,String supplierName) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("companyId", ShiroUtils.getCompId());
		params.put("userId", userId);
		params.put("supplierName", supplierName.trim());
		//获得已有的供应商
		List<Map<String, Object>> supplierList = mapper.getBuySupplierByMap(params);
		return supplierList;
	}

	/**
	 * 人员设置供应商
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject addUserSupplier(String userId, String supplierIdStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(supplierIdStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "供应商信息不能为空");
			return json;
		}
		String[] supplierIdArray = supplierIdStr.split(",");
		for(String supplierId : supplierIdArray){
			BuySupplierUser buySupplierUser = new BuySupplierUser();
			buySupplierUser.setId(ShiroUtils.getUid());
			buySupplierUser.setCompanyId(ShiroUtils.getCompId());
			buySupplierUser.setSupplierId(supplierId);
			buySupplierUser.setUserId(userId);
			buySupplierUser.setCreateTime(new Date());
			buySupplierUserMapper.add(buySupplierUser);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}
	
	/**
	 * 人员删除供应商
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	public JSONObject deleteUserSupplier(String userId, String supplierIdStr) {
		JSONObject json = new JSONObject();
		if(userId.isEmpty()){
			json.put("flag", false);
			json.put("msg", "人员信息不能为空");
			return json;
		}
		if(supplierIdStr.isEmpty()){
			json.put("flag", false);
			json.put("msg", "供应商信息不能为空");
			return json;
		}
		String[] supplierIdArray = supplierIdStr.split(",");
		for(String supplierId : supplierIdArray){
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("supplierId", supplierId);
			params.put("userId", userId);
			buySupplierUserMapper.deleteByParams(params);
		}
		json.put("flag", true);
		json.put("msg", "成功");
		return json;
	}
	
}
