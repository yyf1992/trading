package com.nuotai.trading.buyer.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyCustomerMapper;
import com.nuotai.trading.buyer.dao.BuyDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.BuyDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.BuyOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderProductMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderSupplierProductMapper;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyDeliveryRecord;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.model.ConfirmReceiveData;
import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecord;
import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecordItem;
import com.nuotai.trading.buyer.model.seller.SellerOrder;
import com.nuotai.trading.dao.BuyAttachmentMapper;
import com.nuotai.trading.dao.BuyWarehouseLogMapper;
import com.nuotai.trading.dao.BuyWarehouseProdcutsMapper;
import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.seller.service.SellerLogisticsService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.oms.client.OMSResponse;


@Service
@Transactional
public class BuyDeliveryRecordService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyDeliveryRecordService.class);

	@Autowired
	private BuyDeliveryRecordMapper buyDeliveryRecordMapper;
	
	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;  //发货商品详情信息
	
	@Autowired
	private SellerLogisticsService buyLogisticsService;  //物流信息

	@Autowired
	private BSellerDeliveryRecordMapper bSellerDeliveryRecordMapper;

	@Autowired
	private BSellerDeliveryRecordItemMapper bSellerDeliveryRecordItemMapper;

	@Autowired
	private BuyOrderMapper buyOrderMapper;

	@Autowired
	private BuyOrderProductMapper buyOrderProductMapper;

	@Autowired
	private BuyWarehouseLogMapper buyWarehouseLogMapper;

	@Autowired
	private BuyWarehouseProdcutsMapper buyWarehouseProdcutsMapper;

	@Autowired
	private BuyAttachmentMapper buyAttachmentMapper;

	@Autowired
	private TBusinessWhareaMapper tBusinessWhareaMapper;

	@Autowired
	private BSellerOrderMapper bSellerOrderMapper;

	@Autowired
	private BSellerOrderSupplierProductMapper bSellerOrderSupplierProductMapper;
	//售后订单
	@Autowired
	private BuyCustomerMapper buyCustomerMapper;

	public BuyDeliveryRecord get(String id){
		return buyDeliveryRecordMapper.get(id);
	}

	//根据条件查询发货详细
	public List<BuyDeliveryRecord> queryDeliveryRecordList(Map<String, Object> map){
		return buyDeliveryRecordMapper.queryDeliveryRecordList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyDeliveryRecordMapper.queryCount(map);
	}
	
	public void add(BuyDeliveryRecord buyDeliveryRecord){
		buyDeliveryRecordMapper.add(buyDeliveryRecord);
	}
	
	public void update(BuyDeliveryRecord buyDeliveryRecord){
		buyDeliveryRecordMapper.update(buyDeliveryRecord);
	}
	
	public void delete(String id){
		buyDeliveryRecordMapper.delete(id);
	}
	/**
	 * Gets record status num.
	 * 获得不同状态数量
	 * @param params the params
	 */
	public void getRecordStatusNum(Map<String, Object> params) {
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("companyId", ShiroUtils.getCompId());//供应商ID
		selectMap.put("isDel", Constant.IsDel.NODEL.getValue());
		//所有
		selectMap.put("recordstatus", "");
		int allNum = buyDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("allNum", allNum);
		//待收货
		selectMap.put("recordstatus", "0");
		int waitReceiveNum = buyDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("waitReceiveNum", waitReceiveNum);
		//已确认收货
		selectMap.put("recordstatus", "1");
		int receivedNum = buyDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
		params.put("receivedNum", receivedNum);
		//交易完成
//		selectMap.put("recordstatus", "2");
//		int receivedNum = buyDeliveryRecordMapper.selectDeliveryNumByMap(selectMap);
//		params.put("okNum", receivedNum);

		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			default:
				params.put("interest", "0");
				break;
		}
	}

	/**
	 * Load delivery record data search page util.
	 * 获取收货单列表数据
	 * @param searchPageUtil the search page util
	 * @param params         the params
	 * @return the search page util
	 */
	public SearchPageUtil loadDeliveryRecordData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("companyId", ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		if(tabId!=0){
			switch (tabId) {
				case 0:
					params.put("interest", "0");
					break;
				case 1:
					params.put("interest", "1");
					break;
				case 2:
					params.put("interest", "2");
					break;
				default:
					params.put("interest", "0");
					break;
			}
		}
		//所有的tab
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待收货
			params.put("recordstatus", "0");
		}else if("2".equals(interest)){
			//已收货
			params.put("recordstatus", "1");
		}
		params.put("tabId",tabId);
		searchPageUtil.setObject(params);
		List<BuyDeliveryRecord> deliveryList = buyDeliveryRecordMapper.selectByPage(searchPageUtil);
		if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
			for(BuyDeliveryRecord delivery : deliveryList){
				String deliveryId = delivery.getId();
				Map<String,Object> map = new HashMap<>(1);
				map.put("deliveryId",deliveryId);
				List<BuyDeliveryRecordItem> itemList = buyDeliveryRecordItemMapper.getItemByDeliveryId(map);
				/********发货明细按照条形码合并 begin***********/
				Map<String,BuyDeliveryRecordItem> sdItemMap = new HashMap<>();
				Map<String,Integer> orderNumMap = new HashMap<>();
				List<BuyDeliveryRecordItem> addList = new ArrayList<>();
				for (BuyDeliveryRecordItem item:itemList) {
					if(sdItemMap.containsKey(item.getBarcode())){
						BuyDeliveryRecordItem oldItem = sdItemMap.get(item.getBarcode());
						//发货数量合计
						int oldDeliveryNum = ObjectUtil.isEmpty(oldItem.getDeliveryNum())?0:oldItem.getDeliveryNum();
						int deliveryNum = ObjectUtil.isEmpty(item.getDeliveryNum())?0:item.getDeliveryNum();
						item.setDeliveryNum(oldDeliveryNum+deliveryNum);
						//到货数量合计
						int oldArrivalNum = ObjectUtil.isEmpty(oldItem.getArrivalNum())?0:oldItem.getArrivalNum();
						int arrivalNum = ObjectUtil.isEmpty(item.getArrivalNum())?0:item.getArrivalNum();
						item.setArrivalNum(oldArrivalNum+arrivalNum);
						//判断部分发货还是全部发货,如果有部分发货，则该发货明细合并之后也是部分发货
						if(oldItem.getStatus()==1){
							item.setStatus(1);
						}
						//订单号拼接
						String orderCode = oldItem.getOrderCode();
						if(!orderCode.contains(item.getOrderCode())){
							orderCode=orderCode+","+item.getOrderCode();
						}
						item.setOrderCode(orderCode);
					}
					//获取该发货单下相同订单号的商品，用于计算订单数量
					Map<String,Object> orderItemMap = new HashMap<>(2);
					orderItemMap.put("barcode",item.getBarcode());
					orderItemMap.put("orderId",item.getOrderId());
					List<BuyOrderProduct> orderItemList = buyOrderProductMapper.queryList(orderItemMap);
					int orderNumSum = 0;
					for (BuyOrderProduct orderItem:orderItemList) {
						orderNumSum += orderItem.getGoodsNumber()==null?0:orderItem.getGoodsNumber();
					}
					item.setOrderNum(orderNumSum);

					sdItemMap.put(item.getBarcode(),item);
				}
				for (Map.Entry<String,BuyDeliveryRecordItem> entry : sdItemMap.entrySet()){
					addList.add(entry.getValue());
				}
				delivery.setItemList(addList);
				/********发货明细按照条形码合并 end***********/
				//是否从WMS仓库抓取 Y:是 N：否
				String isFromWms = "Y";
				for (BuyDeliveryRecordItem item : itemList) {
					TBusinessWharea warehouse = tBusinessWhareaMapper.getWharea(item.getWarehouseId());
					if(ObjectUtil.isEmpty(warehouse) || "CN0001".equalsIgnoreCase(warehouse.getWhareaCode())||"JD0001".equalsIgnoreCase(warehouse.getWhareaCode())){
						isFromWms = "N";
						break;
					}
				}
				delivery.setIsFromWms(isFromWms);
				//设置收货凭证信息
				Map<String,Object> attachmentMap = new HashMap<>(1);
				attachmentMap.put("relatedId",delivery.getDeliverNo());
				List<BuyAttachment> attachmentList = buyAttachmentMapper.selectByMap(attachmentMap);
				delivery.setReceiptVoucherList(attachmentList);
			}
		}
		//设置收货仓库
		for (BuyDeliveryRecord deliveryRecord:deliveryList) {
			String warehouseName = "";
			List<BuyDeliveryRecordItem> itemList = deliveryRecord.getItemList();
			if(!ObjectUtil.isEmpty(itemList)&&itemList.size()>0){
				warehouseName = itemList.get(0).getWarehouseName();
			}
			deliveryRecord.setWarehouseName(warehouseName);
		}
		searchPageUtil.getPage().setList(deliveryList);
		return searchPageUtil;
	}

	/**
	 * Check receipt.
	 * 确认或者拒绝签收回单
	 * @param params the params
	 */
	public void checkReceipt(Map<String, Object> params) {
		//更新收货单状态
		String id = String.valueOf(params.get("id"));
		String result = String.valueOf(params.get("result"));
		BuyDeliveryRecord buyDeliveryRecord = new BuyDeliveryRecord();
		buyDeliveryRecord.setId(id);
		buyDeliveryRecord.setReceiptvoucherCheck(result);
		buyDeliveryRecordMapper.updateByPrimaryKeySelective(buyDeliveryRecord);
		//更新发货单状态
		buyDeliveryRecord = buyDeliveryRecordMapper.get(id);
		String deliverId = buyDeliveryRecord.getDeliverId();
		SellerDeliveryRecord sellerDeliveryRecord = new SellerDeliveryRecord();
		sellerDeliveryRecord.setId(deliverId);
		sellerDeliveryRecord.setReceiptvoucherCheck(result);
		bSellerDeliveryRecordMapper.updateByPrimaryKeySelective(sellerDeliveryRecord);
	}

	/**
	 * Query confirm receive data list.
	 * 确认收货数据
	 * @param id the id
	 * @return the list
	 */
	public List<ConfirmReceiveData> queryConfirmReceiveData(String id) {
		List<ConfirmReceiveData> list = new ArrayList<>();
		//获取收货单主表
		BuyDeliveryRecord buyDeliveryRecord = buyDeliveryRecordMapper.get(id);
		//订单ID数组，用于存放收货单下的订单ID
		List<String> orderIds = new ArrayList<>();
		Map<String,Object> itemMap = new HashMap<>();
		itemMap.put("deliveryId",id);
		//获取收货单明细
		List<BuyDeliveryRecordItem> deliveryItemList = buyDeliveryRecordItemMapper.getItemByDeliveryId(itemMap);
		if(!ObjectUtil.isEmpty(deliveryItemList)&&deliveryItemList.size()>0){
			//获取表头订单信息
			Map<String,String> idMap = new HashMap<>();
			for (BuyDeliveryRecordItem item: deliveryItemList) {
				String orderId = item.getOrderId();
				if(!idMap.containsKey(orderId)&&!ObjectUtil.isEmpty(orderId)){
					idMap.put(orderId,orderId);
					//订单信息
					BuyOrder buyOrder = null;
					BuyCustomer buyCustomer =  null;
					if (buyDeliveryRecord.getDeliverType() == 0){
						buyOrder = buyOrderMapper.selectByPrimaryKey(orderId);
					}else{
						buyCustomer = buyCustomerMapper.get(orderId);
					}
					
					ConfirmReceiveData data = new ConfirmReceiveData();
					data.setBuyerOrderId(orderId);
					if(!ObjectUtil.isEmpty(buyOrder)){
						data.setOrderCode(buyOrder.getOrderCode());
						data.setOrderDate(buyOrder.getCreateDate());
						data.setOrderType(buyOrder.getOrderType());
						data.setSupplierId(buyOrder.getSuppId());
						data.setSupplierName(buyOrder.getSuppName());
					}else{
						if(!ObjectUtil.isEmpty(buyCustomer)){
							data.setOrderCode(buyCustomer.getCustomerCode());
							data.setOrderDate(buyCustomer.getCreateDate());
							data.setSupplierId(buyCustomer.getSupplierId());
							data.setSupplierName(buyCustomer.getSupplierName());
						}
					}
					data.setDeliverNo(buyDeliveryRecord.getDeliverNo());
					data.setDeliverDate(buyDeliveryRecord.getCreateDate());
					list.add(data);
				}
			}
			//组装明细信息
			for (ConfirmReceiveData data : list) {
				itemMap.clear();
				itemMap.put("orderId",data.getBuyerOrderId());
				itemMap.put("recordId",id);
				List<BuyDeliveryRecordItem> itemList = buyDeliveryRecordItemMapper.queryList(itemMap);
				data.setItemList(itemList);
			}
		}
	 return list;
	}

	/**
	 * Storage goods.
	 * 确认收货，入库
	 * @param params the params
	 */
	public void storageGoods(Map<String, Object> params) {
		//1.更新收货单主表状态已收货
		BuyDeliveryRecord buyDeliveryRecord = new BuyDeliveryRecord();
		buyDeliveryRecord.setId(String.valueOf(params.get("deliveryId")));
		//交易完成
		buyDeliveryRecord.setRecordstatus(1);
		buyDeliveryRecord.setArrivalDate(new Date());
		buyDeliveryRecordMapper.updateByPrimaryKeySelective(buyDeliveryRecord);

		buyDeliveryRecord = buyDeliveryRecordMapper.get(String.valueOf(params.get("deliveryId")));
		if(!ObjectUtil.isEmpty(buyDeliveryRecord)&&!ObjectUtil.isEmpty(buyDeliveryRecord.getDeliverId())){
			SellerDeliveryRecord sellerDeliveryRecord = new SellerDeliveryRecord();
			sellerDeliveryRecord.setId(buyDeliveryRecord.getDeliverId());
			//已收货
			sellerDeliveryRecord.setRecordstatus(2);
			sellerDeliveryRecord.setArrivalDate(new Date());
			bSellerDeliveryRecordMapper.updateByPrimaryKeySelective(sellerDeliveryRecord);
		}

		//解析收货明细
		List<BuyDeliveryRecordItem> deliveryItemList = JSONObject.parseArray(String.valueOf(params.get("deliveryItems")),BuyDeliveryRecordItem.class);
		//2.更新收货单和发货单的到货数量
		List<String> orderIds = new ArrayList<>();
		for (BuyDeliveryRecordItem item:deliveryItemList) {
			//收货单
			BuyDeliveryRecordItem itemUpd = new BuyDeliveryRecordItem();
			itemUpd.setId(item.getId());
			itemUpd.setArrivalNum(item.getArrivalNum());
			itemUpd.setArrivalDate(new Date());
			buyDeliveryRecordItemMapper.updateByPrimaryKeySelective(itemUpd);
			//采购订单分录更新到货数量
			buyOrderProductMapper.addArrivalNum(item.getOrderItemId(),item.getArrivalNum());

			//发货单
			SellerDeliveryRecordItem sdItemUpd = new SellerDeliveryRecordItem();
			sdItemUpd.setId(item.getDeliveryItemId());
			sdItemUpd.setArrivalNum(item.getArrivalNum());
			sdItemUpd.setArrivalDate(new Date());
			bSellerDeliveryRecordItemMapper.updateByPrimaryKeySelective(sdItemUpd);
			//卖家订单更新到货数量
			bSellerOrderSupplierProductMapper.addArrivalNum(item.getSellerOrderItemId(),item.getArrivalNum());

			if(!orderIds.contains(item.getOrderId())){
				orderIds.add(item.getOrderId());
			}
		}
		//3.更新订单中的总到货数量
		for (String orderId:orderIds) {
			BuyOrder buyOrder = new BuyOrder();
			buyOrder.setId(orderId);
			boolean isFinished = false;
			List<BuyOrderProduct> buyOrderProductList = buyOrderProductMapper.selectByOrderId(orderId);
			for (BuyOrderProduct boItem:buyOrderProductList) {
				if(boItem.getGoodsNumber()-boItem.getDeliveredNum()>0){
					isFinished = false;
					break;
				}else {
					isFinished = true;
				}
			}
			if(isFinished){
				//已收货
				buyOrder.setStatus(Constant.OrderStatus.RECEIVED.getValue());
				buyOrderMapper.updateByPrimaryKeySelective(buyOrder);
				SellerOrder so = bSellerOrderMapper.getByBuyerOrderId(orderId);
				SellerOrder soUpd = new SellerOrder();
				soUpd.setId(so.getId());
				soUpd.setStatus(Constant.SellerOrderStatus.RECEIVED.getValue());
				bSellerOrderMapper.updateByPrimaryKeySelective(soUpd);
			}
		}
		//4.入库
		for (BuyDeliveryRecordItem recItem:deliveryItemList) {
			String itemId = recItem.getId();
			//获取收货单的具体信息
			BuyDeliveryRecord main = buyDeliveryRecordMapper.get(recItem.getRecordId());
			BuyDeliveryRecordItem item = buyDeliveryRecordItemMapper.get(itemId);
			//保存入库日志记录
			BuyWarehouseLog log = new BuyWarehouseLog();
			log.setId(ShiroUtils.getUid());
			log.setBatchNo(main.getDeliverNo());
			log.setOrderId(item.getOrderId());
			log.setOrderCode(item.getOrderCode());
			log.setBuyDeliveryItemId(itemId);
			log.setCompanyId(main.getCompanyId());
			log.setWarehouseId(item.getWarehouseId());
			log.setShopId(item.getShopId());
			log.setShopCode(item.getShopCode());
			log.setShopName(item.getShopName());
			log.setChangeType(1);//入库
			if("0".equalsIgnoreCase(item.getOrderType())){
				//采购订单
				log.setStockType(0);
			}else if ("1".equalsIgnoreCase(item.getOrderType())){
				//换货订单
				log.setStockType(1);
			}else {
				log.setStockType(0);
			}
			log.setProductCode(item.getProductCode());
			log.setProductName(item.getProductName());
			log.setSkuCode(item.getSkuCode());
			log.setSkuName(item.getSkuName());
			log.setBarcode(item.getBarcode());
			log.setPrice(item.getSalePrice());
			log.setNumber(item.getArrivalNum());
			log.setFactNumber(item.getArrivalNum());
			log.setStatus(0);//库存状态 0 已完结，1未完结，2已取消
			log.setStorageDate(new Date());
			log.setIsDel(0);
			log.setCreateId(ShiroUtils.getUserId());
			log.setCreateDate(new Date());
			log.setCreateName(ShiroUtils.getUserName());
			buyWarehouseLogMapper.add(log);
			//更新库存，先判断库存有没有该记录，有则更新数量，否则新插入一条数据
			Map<String,Object> stockMap = new HashMap<>();
			stockMap.put("warehouseId",item.getWarehouseId());
			stockMap.put("shopId",item.getShopId());
			stockMap.put("barcode",item.getBarcode());
			List<BuyWarehouseProdcuts> buyWarehouseProdcutsList = buyWarehouseProdcutsMapper.queryList(stockMap);
			BuyWarehouseProdcuts buyWarehouseProdcuts = new BuyWarehouseProdcuts();
			if(!ObjectUtil.isEmpty(buyWarehouseProdcutsList)&&buyWarehouseProdcutsList.size()>0){
				buyWarehouseProdcuts = buyWarehouseProdcutsList.get(0);
			}
			if(ObjectUtil.isEmpty(buyWarehouseProdcuts.getId())){
				buyWarehouseProdcuts.setId(ShiroUtils.getUid());
				buyWarehouseProdcuts.setCompanyId(main.getCompanyId());
				buyWarehouseProdcuts.setWarehouseId(item.getWarehouseId());
				buyWarehouseProdcuts.setShopId(item.getShopId());
				buyWarehouseProdcuts.setShopCode(item.getShopCode());
				buyWarehouseProdcuts.setShopName(item.getShopName());
				buyWarehouseProdcuts.setProductCode(item.getProductCode());
				buyWarehouseProdcuts.setProductName(item.getProductName());
				buyWarehouseProdcuts.setSkuCode(item.getSkuCode());
				buyWarehouseProdcuts.setSkuName(item.getSkuName());
				buyWarehouseProdcuts.setBarcode(item.getBarcode());
				buyWarehouseProdcuts.setColor("");
				buyWarehouseProdcuts.setTotalCost(item.getSalePrice().multiply(new BigDecimal(item.getArrivalNum())));
				buyWarehouseProdcuts.setStocks(item.getArrivalNum());
				buyWarehouseProdcuts.setStorageDate(new Date());
				buyWarehouseProdcuts.setStatus("1");
				buyWarehouseProdcuts.setRemark("");
				buyWarehouseProdcuts.setIsDel(0);
				buyWarehouseProdcuts.setCreateId(ShiroUtils.getUserId());
				buyWarehouseProdcuts.setCreateDate(new Date());
				buyWarehouseProdcuts.setCreateName(ShiroUtils.getUserName());
				buyWarehouseProdcutsMapper.add(buyWarehouseProdcuts);
			}else{
				buyWarehouseProdcuts.setStocks(buyWarehouseProdcuts.getStocks()+item.getArrivalNum());
				buyWarehouseProdcuts.setTotalCost(buyWarehouseProdcuts.getTotalCost().add(item.getSalePrice().multiply(new BigDecimal(item.getArrivalNum())) ));
				buyWarehouseProdcutsMapper.update(buyWarehouseProdcuts);
			}
		}
	}

	/**
	 * Save receipt attachment.
	 * 保存附件
	 * @param deliverNo   the deliver no
	 * @param attachments the attachments
	 */
	public void saveReceiptAttachment(String deliverNo, String attachments) {
		if(!ObjectUtil.isEmpty(attachments)){
			buyAttachmentMapper.deleteByRelatedId(deliverNo);
			List<String> list = JSONArray.parseArray(attachments,String.class);
			for (String url:list) {
				BuyAttachment attachment = new BuyAttachment();
				attachment.setId(ShiroUtils.getUid());
				attachment.setUrl(url);
				attachment.setRelatedId(deliverNo);
				attachment.setType(null);
				buyAttachmentMapper.insert(attachment);
			}
		}
	}

    public List<BuyDeliveryRecord> getExportData(Map<String, Object> params) {
		params.put("companyId", ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
//			case 0:
//				params.put("interest", "0");
//				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
//			default:
//				params.put("interest", "0");
//				break;
		}
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待收货
			params.put("recordstatus", "0");
		}else if("2".equals(interest)){
			//已收货
			params.put("recordstatus", "1");
		}
		List<BuyDeliveryRecord> deliveryList = buyDeliveryRecordMapper.selectByMap(params);
		if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
			for(BuyDeliveryRecord delivery : deliveryList){
				String deliveryId = delivery.getId();
				Map<String,Object> map = new HashMap<>(1);
				map.put("deliveryId",deliveryId);
				List<BuyDeliveryRecordItem> itemList = buyDeliveryRecordItemMapper.getItemByDeliveryId(map);
				/********发货明细按照条形码合并 begin***********/
				Map<String,BuyDeliveryRecordItem> sdItemMap = new HashMap<>();
				List<BuyDeliveryRecordItem> addList = new ArrayList<>();
				for (BuyDeliveryRecordItem item:itemList) {
					if(sdItemMap.containsKey(item.getBarcode())){
						BuyDeliveryRecordItem oldItem = sdItemMap.get(item.getBarcode());
						item.setDeliveryNum(oldItem.getDeliveryNum()+item.getDeliveryNum());
					}
					//获取该发货单下相同订单号的商品，用于计算订单数量
					Map<String,Object> orderItemMap = new HashMap<>(2);
					orderItemMap.put("barcode",item.getBarcode());
					orderItemMap.put("orderId",item.getOrderId());
					List<BuyOrderProduct> orderItemList = buyOrderProductMapper.queryList(orderItemMap);
					int orderNumSum = 0;
					for (BuyOrderProduct orderItem:orderItemList) {
						orderNumSum += orderItem.getGoodsNumber()==null?0:orderItem.getGoodsNumber();
					}
					item.setOrderNum(orderNumSum);
					sdItemMap.put(item.getBarcode(),item);
				}
				for (Map.Entry<String,BuyDeliveryRecordItem> entry : sdItemMap.entrySet()){
					addList.add(entry.getValue());
				}
				delivery.setItemList(addList);
				
				String warehouseName = "";
				if(!ObjectUtil.isEmpty(itemList)&&itemList.size()>0){
					warehouseName = itemList.get(0).getWarehouseName();
				}
				delivery.setWarehouseName(warehouseName);
			}
		}
		return deliveryList;
    }
    
    /**
     * 推送oms出库
     * @return
     */
    public void pushOMS(String id) {
    	BuyDeliveryRecord buyDeliveryRecord = buyDeliveryRecordMapper.get(id);
    	Map<String,Object> map = new HashMap<>(1);
		map.put("recordId",id);
    	List<BuyDeliveryRecordItem> itemList = buyDeliveryRecordItemMapper.queryList(map);
    	if(!"0".equals(buyDeliveryRecord.getDropshipType())){
			//代发数据
			if("1".equals(buyDeliveryRecord.getDropshipType())){
				//代发客户
				outLibrary(buyDeliveryRecord,itemList,0);
			}else{
				//代发菜鸟、京东
				outLibrary(buyDeliveryRecord,itemList,1);
			}
		}else{
			//正常发货
			String warehouseName = "";
			if(itemList != null && !itemList.isEmpty()){
				warehouseName = itemList.get(0).getWarehouseName();
			}
			if("代发仓".equals(warehouseName)){
				//代发数据
				outLibrary(buyDeliveryRecord,itemList,0);
			}
		}
	}
    
    /**
     * 推送oms出库
     * @param deliveryHeader
     * @return
     */
    public boolean outLibrary(BuyDeliveryRecord buyDeliveryRecord,
    		List<BuyDeliveryRecordItem> recordItemList,int type){
    	boolean result = false;
    	if(buyDeliveryRecord.getRecordstatus()!=1){
    		return false;
    	}
    	Map<String,Object> header = new HashMap<String,Object>();
		header.put("title", "代发入库数据出库");
		header.put("sourceno", buyDeliveryRecord.getDeliverNo());
		header.put("whcode", "0004");
		header.put("inputdesc", "买卖系统代发采购数据wms入库后，自动创建oms出库数据");
		header.put("creater", "买卖系统");
		List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
		for(BuyDeliveryRecordItem recordItem : recordItemList){
			Map<String,Object> item = new HashMap<String,Object>();
			item.put("skucode", recordItem.getSkuCode());
			item.put("skuname", recordItem.getSkuName());
			item.put("proname", recordItem.getProductName());
			item.put("procode", recordItem.getProductCode());
			item.put("skuoid", recordItem.getBarcode());
			item.put("plancount", recordItem.getArrivalNum());
			itemList.add(item);
		}
		header.put("itemList", itemList);
		try {
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(header);
			if(type==0){
				//代发出库
				OMSResponse response = InterfaceUtil.NuotaiOmsClient("addOutLibrary", buyDeliveryRecord.getDeliverNo(), jsonObj);
				result = response.isSuccess();
			}else{
				//杉橙没有菜鸟、京东仓，先走出库
				//OMSResponse response = InterfaceUtil.NuotaiOmsClient("addTransfer", buyDeliveryRecord.getDeliverNo(), jsonObj);
				OMSResponse response = InterfaceUtil.NuotaiOmsClient("addOutLibrary", buyDeliveryRecord.getDeliverNo(), jsonObj);
				result = response.isSuccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(result){
			buyDeliveryRecordMapper.updatePushType(buyDeliveryRecord.getDeliverNo());
		}
    	return result;
    }
}
