package com.nuotai.trading.buyer.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.SysShopDeliveryData;
import com.nuotai.trading.buyer.model.SysShopProduct;
import com.nuotai.trading.buyer.service.SysShopDeliveryDataService;
import com.nuotai.trading.buyer.service.SysShopProductService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Controller
@RequestMapping("buyer/sysshopsaledata")
public class SysShopSaleDataController extends BaseController {
	@Autowired
	private SysShopDeliveryDataService sysShopDeliveryDataService;
	@Autowired
	private SysShopService sysShopService;
	@Autowired
	private SysShopProductService sysShopProductService;

	/**
	 * 加载店铺销售统计数据
	 * 
	 * @param searchPageUtil
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/shopSale", method = RequestMethod.GET)
	public String shopShipData(@RequestParam Map<String, String> map)
			throws Exception {
		if (map.containsKey("shopcode") && map.get("shopcode") != null) {
			String shopcodeStr = map.get("shopcode").toString();
			if (shopcodeStr.contains(",")) {
				map.put("shopcode", shopcodeStr.split(",")[1]);
			}
		}
		
		// 去掉从发货统计取数的店铺
		String notShopcode = "";
		Map<String, Object> amap = new HashMap<String, Object>();
		amap.put("companyId", ShiroUtils.getCompId());
		List<SysShopDeliveryData> shipshopList = sysShopDeliveryDataService.getShopByMap(amap);
		if (shipshopList != null && shipshopList.size() > 0) {
			for (SysShopDeliveryData shop : shipshopList) {
				notShopcode += shop.getShopCode() + ",";
			}
		} else {
			notShopcode = "-1";
		}
		if (notShopcode.contains(",")) {
			notShopcode = notShopcode.substring(0, notShopcode.length() - 1);
		}
		map.put("notShopcode", notShopcode);

		// 开始时间
		String startDate = "";
		// 结束时间
		String endDate = "";
		if (map.containsKey("startDate") && map.get("startDate") != null
				&& !"".equals(map.get("startDate"))) {
			startDate = map.get("startDate").toString();
			if (startDate.contains("-")) {
				startDate = startDate.replaceAll("-", "");
			}
		} else {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal
					.getTime());
			startDate = yesterday.replaceAll("-", "");
			map.put("startDate", yesterday);
		}
		if (map.containsKey("endDate") && map.get("endDate") != null
				&& !"".equals(map.get("endDate"))) {
			endDate = map.get("endDate").toString();
			if (endDate.contains("-")) {
				endDate = endDate.replaceAll("-", "");
			}
		} else {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal
					.getTime());
			endDate = yesterday.replaceAll("-", "");
			map.put("endDate", yesterday);
		}

		String omsUrl = "http://121.199.179.23:30003/oms_interface/"+ "getOmsSalesReportByMap?";

		String selectUrl = "startDate=" + startDate + "&endDate=" + endDate;
		if (map.containsKey("procode") && map.get("procode") != null
				&& !"".equals(map.get("procode").toString())) {
			selectUrl += "&procode=" + map.get("procode").toString();
		}
		if (map.containsKey("skuoid") && map.get("skuoid") != null
				&& !"".equals(map.get("skuoid").toString())) {
			selectUrl += "&skuoid=" + map.get("skuoid").toString();
		}
		if (map.containsKey("shopcode") && map.get("shopcode") != null
				&& !"".equals(map.get("shopcode").toString())) {
			selectUrl += "&shopcode=" + map.get("shopcode").toString();
		}
		if (map.containsKey("notShopcode") && map.get("notShopcode") != null
				&& !"".equals(map.get("notShopcode").toString())) {
			selectUrl += "&notShopcode=" + map.get("notShopcode").toString();
		}
		selectUrl += "&shiptime=shiptime";
		if (map.containsKey("latitude") && map.get("latitude") != null&& !"".equals(map.get("latitude").toString())) {
			selectUrl += "&latitude=" + map.get("latitude").toString();
		} else {
			map.put("latitude", "sday");
		}

		omsUrl += selectUrl;
		String latitude = map.get("latitude");
		// 获得退货数据
		String tuiHuoUrl = "http://121.199.179.23:30003/oms_interface/"+ "getOmsSendskuByMap?";
		tuiHuoUrl += selectUrl;
		tuiHuoUrl = tuiHuoUrl.replaceAll(" ", "");
		Map<String, net.sf.json.JSONObject> tuiHuoMap = getTuiHuo(tuiHuoUrl,latitude);

		omsUrl = omsUrl.replaceAll(" ", "");
		// 获得销售数据
		String shopSalesString = InterfaceUtil.searchLoginService(omsUrl);
		net.sf.json.JSONObject shopSalesJson = net.sf.json.JSONObject.fromObject(shopSalesString);
		String shopSalesListStr = shopSalesJson.getString("list");
		net.sf.json.JSONArray shopSalesArray = net.sf.json.JSONArray.fromObject(shopSalesListStr);
		List<net.sf.json.JSONObject> shopSalesList = (List<net.sf.json.JSONObject>) JSONArray.toCollection(shopSalesArray, net.sf.json.JSONObject.class);

		// 获得销售单价
		Map<String, Double> sspMap = new HashMap<String, Double>();
		List<SysShopProduct> sspList = sysShopProductService.selectByMap(null);
		if (sspList != null && sspList.size() > 0) {
			for (SysShopProduct ssp : sspList) {
				String shopcode = ssp.getShopCode();
				String skuoid = ssp.getBarcode();
				double price = ssp.getSellPrice().doubleValue();
				sspMap.put(shopcode + "," + skuoid, price);
			}
		}

		Map<String, net.sf.json.JSONObject> shopMap = new TreeMap<String, net.sf.json.JSONObject>();
		Map<String, net.sf.json.JSONObject> productMap = new TreeMap<String, net.sf.json.JSONObject>();
		Map<String, net.sf.json.JSONObject> shopProductMap = new TreeMap<String, net.sf.json.JSONObject>();
		Map<String, net.sf.json.JSONObject> latitudeMap = new TreeMap<String, net.sf.json.JSONObject>();
		Map<String, net.sf.json.JSONObject> tempMap = new TreeMap<String, net.sf.json.JSONObject>();
		
		JSONObject shopSalesAlready = new JSONObject();
		// 获得成本单价
		if (shopSalesList != null && shopSalesList.size() > 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (net.sf.json.JSONObject shopSales : shopSalesList) {
				long shiptime = shopSales.getLong("shiptime");
				Date shiptimeDate = new Date(shiptime);
				String shiptimeStr = sdf.format(shiptimeDate);
				String[] shipTimeArr = shiptimeStr.split("-");
				String syear = shipTimeArr[0];
				String smonth = shipTimeArr[1];
				String sday = shipTimeArr[2];

				shopSales.put("syear", syear);
				shopSales.put("smonth", smonth);
				shopSales.put("sday", sday);

				String shopcode = shopSales.getString("shopcode");
				String skuoid = shopSales.getString("skuoid");

				// 发货数
				int num = shopSales.getInt("num");
				shopSales.put("skucount", num);
				// 发货成本（总成本）
				double costprice = shopSales.getDouble("costprice");
				double costpriceNew = Math.round((costprice / num) * 10000) / 10000.0;
				costprice = Math.round(costprice * 10000) / 10000.0;
				// 成本单价
				shopSales.put("costprice", costpriceNew);
				// 获得销售单价
				if (sspMap.containsKey(shopcode + "," + skuoid)) {
					double price = sspMap.get(shopcode + "," + skuoid);
					shopSales.put("soldPrice", price);
					// 出货金额
					shopSales.put("deliveryAmount",Math.round((num * price) * 10000) / 10000.0);
				} else {
					shopSales.put("soldPrice", costpriceNew);
					// 发货金额
					shopSales.put("deliveryAmount", costprice);
				}

				// 发货成本
				shopSales.put("deliveryCosts", costprice);
				double grossProfit = shopSales.getDouble("deliveryAmount")- shopSales.getDouble("deliveryCosts");
				shopSales.put("grossProfit",Math.round(grossProfit * 10000) / 10000.0);
				String key = shopcode + "," + skuoid;
				if ("sday".equals(latitude)) {
					key += "," + syear + "," + smonth + "," + sday;
				} else if ("smonth".equals(latitude)) {
					key += "," + syear + "," + smonth;
				} else if ("syear".equals(latitude)) {
					key += "," + syear;
				}
				if(latitudeMap.containsKey(key)){
					net.sf.json.JSONObject oldJSON = latitudeMap.get(key);
					shopSales.put("skucount", (oldJSON.getInt("skucount")+shopSales.getInt("skucount")));
					shopSales.put("deliveryAmount", (oldJSON.getDouble("deliveryAmount")+shopSales.getDouble("deliveryAmount")));
					shopSales.put("deliveryCosts", (oldJSON.getDouble("deliveryCosts")+shopSales.getDouble("deliveryCosts")));
					shopSales.put("grossProfit", (oldJSON.getDouble("grossProfit")+shopSales.getDouble("grossProfit")));
				}
				latitudeMap.put(key, shopSales);
			}
		}
		//汇总退货数量
		if(latitudeMap != null && !latitudeMap.isEmpty()){
			for(Map.Entry<String,net.sf.json.JSONObject> entry : latitudeMap.entrySet()){
				String key = entry.getKey();
				net.sf.json.JSONObject shopSales = entry.getValue();
				// 退货数
				int returncount = tuiHuoMap.containsKey(key) ? tuiHuoMap.get(key).getInt("returncount") : 0;
				// 退货金额
				double returncostfee = tuiHuoMap.containsKey(key) ? tuiHuoMap.get(key).getDouble("returncostfee") : 0;
				returncostfee = Math.round(returncostfee * 10000) / 10000.0;
				shopSales.put("returncount", returncount);
				Double price = shopSales.getDouble("soldPrice");
				// 退货金额
				shopSales.put("returnAmount",Math.round((returncount * price) * 10000) / 10000.0);
				// 退货成本
				shopSales.put("returnCosts", returncostfee);
				//已有数据
				shopSalesAlready.put(key, key);
			}
		}
		// 判断是否有遗漏的退货数据
		if (tuiHuoMap != null && tuiHuoMap.size() > 0) {
			for (Map.Entry<String, net.sf.json.JSONObject> tuiHuo : tuiHuoMap.entrySet()) {
				net.sf.json.JSONObject tuiHuoJson = tuiHuo.getValue();
				String skuoid = tuiHuoJson.getString("skuoid");
				String shopcode = tuiHuoJson.getString("shopcode");

				String key = tuiHuo.getKey();
				if (shopSalesAlready.containsKey(key)) {
					continue;
				}
				// 退货数
				int returncount = tuiHuoJson.getInt("returncount");
				// 退货金额
				double returncostfee = tuiHuoJson.getDouble("returncostfee");
				// 退货成本单价
				double returncostrice = 0;
				if (returncount > 0) {
					returncostrice = Math.round((returncostfee / returncount) * 10000) / 10000.0;
				}
				// 获得销售单价
				if (sspMap.containsKey(shopcode + "," + skuoid)) {
					double price = sspMap.get(shopcode + "," + skuoid);
					// 品牌中心出货价
					tuiHuoJson.put("soldPrice", price);
					// 退货金额
					tuiHuoJson.put("returnAmount",Math.round((returncount * price) * 10000) / 10000.0);
				} else {
					// 品牌中心出货价
					tuiHuoJson.put("soldPrice", returncostrice);
					// 退货金额
					tuiHuoJson.put("returnAmount", returncostfee);
				}
				// 出货数
				tuiHuoJson.put("skucount", 0);
				// 出货金额
				tuiHuoJson.put("deliveryAmount", 0);
				// 出货成本
				tuiHuoJson.put("deliveryCosts", 0);
				// 出货毛利
				tuiHuoJson.put("grossProfit", 0);
				tuiHuoJson.put("costprice", returncostrice);
				tuiHuoJson.put("returnCosts", returncostfee);
				latitudeMap.put(key, tuiHuoJson);
			}
		}
		
		if(latitudeMap != null && !latitudeMap.isEmpty()){
			for(Map.Entry<String,net.sf.json.JSONObject> entry : latitudeMap.entrySet()){
				net.sf.json.JSONObject shopSales = entry.getValue();
				String shopcode = shopSales.getString("shopcode");
				String productCode = shopSales.getString("procode");
				String syear = shopSales.getString("syear");
				String smonth = shopSales.getString("smonth");
				String sday = shopSales.getString("sday");
				String key = "";
				if ("sday".equals(latitude)) {
					key += "," + syear + "," + smonth + "," + sday;
				} else if ("smonth".equals(latitude)) {
					key += "," + syear + "," + smonth;
				} else if ("syear".equals(latitude)) {
					key += "," + syear;
				}
				
				//发货数
				int num = shopSales.getInt("skucount");
				if (map.containsKey("shop") && map.containsKey("productCode")) {
					// 根据店铺+货号汇总
					if (shopProductMap.containsKey(shopcode + productCode+key)) {
						// 汇总过得数据
						net.sf.json.JSONObject shopProductOld = shopProductMap.get(shopcode + productCode+key);
						// 发货数
						num += shopProductOld.getInt("skucount");
						shopProductOld.put("skucount", num);
						// 发货金额
						shopProductOld.put("deliveryAmount",shopProductOld.getDouble("deliveryAmount")+ shopSales.getDouble("deliveryAmount"));
						// 退货数
						shopProductOld.put("returncount",shopProductOld.getInt("returncount")+ shopSales.getInt("returncount"));
						// 退货金额
						shopProductOld.put("returnAmount",shopProductOld.getDouble("returnAmount")+ shopSales.getDouble("returnAmount"));
						shopProductOld.put("deliveryCosts",shopProductOld.getDouble("deliveryCosts")+ shopSales.getDouble("deliveryCosts"));
						shopProductOld.put("returnCosts",shopProductOld.getDouble("returnCosts")+ shopSales.getDouble("returnCosts"));
						shopProductMap.put(shopcode + productCode+key,shopProductOld);
					} else {
						// 第一次汇总
						shopProductMap.put(shopcode + productCode+key,shopSales);
					}
				} else if (map.containsKey("shop")) {
					// 按店铺汇总
					if (shopMap.containsKey(shopcode+key)) {
						// 汇总过得数据
						net.sf.json.JSONObject shopSalesOld = shopMap.get(shopcode+key);
						// 发货数
						num += shopSalesOld.getInt("skucount");
						shopSalesOld.put("skucount", num);
						// 发货金额
						shopSalesOld.put("deliveryAmount",shopSalesOld.getDouble("deliveryAmount")+ shopSales.getDouble("deliveryAmount"));
						// 退货数
						shopSalesOld.put("returncount",shopSalesOld.getInt("returncount")+ shopSales.getInt("returncount"));
						// 退货金额
						shopSalesOld.put("returnAmount",shopSalesOld.getDouble("returnAmount")+ shopSales.getDouble("returnAmount"));
						shopSalesOld.put("deliveryCosts",shopSalesOld.getDouble("deliveryCosts")+ shopSales.getDouble("deliveryCosts"));
						shopSalesOld.put("returnCosts",shopSalesOld.getDouble("returnCosts")+ shopSales.getDouble("returnCosts"));
						shopMap.put(shopcode+key, shopSalesOld);
					} else {
						// 第一次汇总
						shopMap.put(shopcode+key, shopSales);
					}
				} else if (map.containsKey("productCode")) {
					// 按货号汇总
					if (productMap.containsKey(productCode+key)) {
						// 汇总过得数据
						net.sf.json.JSONObject productOld = productMap.get(productCode+key);
						// 发货数
						num += productOld.getInt("skucount");
						productOld.put("skucount", num);
						// 发货金额
						productOld.put("deliveryAmount",productOld.getDouble("deliveryAmount")+ shopSales.getDouble("deliveryAmount"));
						// 退货数
						productOld.put("returncount",productOld.getInt("returncount")+ shopSales.getInt("returncount"));
						// 退货金额
						productOld.put("returnAmount",productOld.getDouble("returnAmount")+ shopSales.getDouble("returnAmount"));
						productOld.put("deliveryCosts",productOld.getDouble("deliveryCosts")+ shopSales.getDouble("deliveryCosts"));
						productOld.put("returnCosts",productOld.getDouble("returnCosts")+ shopSales.getDouble("returnCosts"));
						shopMap.put(productCode+key, productOld);
					} else {
						// 第一次汇总
						productMap.put(productCode+key, shopSales);
					}
				}else{
					// 按年月日
					if (tempMap.containsKey(key)) {
						// 汇总过得数据
						net.sf.json.JSONObject old = tempMap.get(key);
						// 发货数
						num += old.getInt("skucount");
						old.put("skucount", num);
						// 发货金额
						old.put("deliveryAmount",old.getDouble("deliveryAmount")+ shopSales.getDouble("deliveryAmount"));
						// 退货数
						old.put("returncount",old.getInt("returncount")+ shopSales.getInt("returncount"));
						// 退货金额
						old.put("returnAmount",old.getDouble("returnAmount")+ shopSales.getDouble("returnAmount"));
						old.put("deliveryCosts",old.getDouble("deliveryCosts")+ shopSales.getDouble("deliveryCosts"));
						old.put("returnCosts",old.getDouble("returnCosts")+ shopSales.getDouble("returnCosts"));
						tempMap.put(key, old);
					} else {
						// 第一次汇总
						tempMap.put(key, shopSales);
					}
				}
			}
		}
		
		List<String> deliveryShop = sysShopDeliveryDataService.selectShopId(ShiroUtils.getCompId());
		Map<String, Object> deliverymap = new HashMap<String, Object>();
		deliverymap.put("deliveryShopId", deliveryShop);
		List<SysShop> shopList = sysShopService.selectByMap(deliverymap);
		model.addAttribute("params", map);
		model.addAttribute("shopList", shopList);
		if (map.containsKey("shop") && map.containsKey("productCode")) {
			model.addAttribute("shopProductMap", shopProductMap);
		} else if (map.containsKey("shop")) {
			model.addAttribute("shopMap", shopMap);
		} else if (map.containsKey("productCode")) {
			model.addAttribute("productMap", productMap);
		} else {
			model.addAttribute("shopSalesList", tempMap);
		}
		return "platform/buyer/sysShopProduct/shopSaleData";
	}

	/**
	 * 获得退货数据
	 * 
	 * @param url
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, net.sf.json.JSONObject> getTuiHuo(String url,
			String latitude) {
		Map<String, net.sf.json.JSONObject> tuiHuoMap = new HashMap<String, net.sf.json.JSONObject>();
		String shopSalesString = InterfaceUtil.searchLoginService(url);
		net.sf.json.JSONObject shopSalesJson = net.sf.json.JSONObject
				.fromObject(shopSalesString);
		String shopSalesListStr = shopSalesJson.getString("list");
		net.sf.json.JSONArray shopSalesArray = net.sf.json.JSONArray
				.fromObject(shopSalesListStr);
		List<net.sf.json.JSONObject> shopSalesList = (List<net.sf.json.JSONObject>) net.sf.json.JSONArray
				.toCollection(shopSalesArray, net.sf.json.JSONObject.class);
		if (shopSalesList != null && shopSalesList.size() > 0) {
			for (net.sf.json.JSONObject shopSales : shopSalesList) {

				String shopcode = shopSales.getString("shopcode");
				String shopname = shopSales.getString("shopname");
				String skuoid = shopSales.getString("skuoid");
				String procode = "";
				if (shopSales.containsKey("procode")) {
					procode += shopSales.getString("procode");
				}
				String syear = "";
				String smonth = "";
				String sday = "";
				if (shopSales.containsKey("syear")) {
					syear += shopSales.getString("syear");
				}
				if (shopSales.containsKey("smonth")) {
					smonth += shopSales.getString("smonth");
				}
				if (shopSales.containsKey("sday")) {
					sday += shopSales.getString("sday");
				}
				String key = shopcode + "," + skuoid;
				if ("sday".equals(latitude)) {
					key += "," + syear + "," + smonth + "," + sday;
				} else if ("smonth".equals(latitude)) {
					key += "," + syear + "," + smonth;
				} else if ("syear".equals(latitude)) {
					key += "," + syear;
				}
				// 退货数
				int returncount = shopSales.getInt("returncount");
				double returncostfee = shopSales.getDouble("returncostfee");
				net.sf.json.JSONObject json = new net.sf.json.JSONObject();
				if (tuiHuoMap.containsKey(key)) {
					json = tuiHuoMap.get(key);
					returncount += json.getInt("returncount");
					returncostfee += json.getDouble("returncostfee");
				}
				json.put("syear", syear);
				json.put("smonth", smonth);
				json.put("sday", sday);
				json.put("shopcode", shopcode);
				json.put("shopname", shopname);
				json.put("skuoid", skuoid);
				json.put("procode", procode);
				json.put("returncount", returncount);
				json.put("returncostfee", returncostfee);
				tuiHuoMap.put(key, json);
			}
		}
		return tuiHuoMap;
	}

}