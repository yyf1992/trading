package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuyApplypurchaseHeader;
import com.nuotai.trading.utils.SearchPageUtil;

public interface BuyApplypurchaseHeaderMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyApplypurchaseHeader record);

    int insertSelective(BuyApplypurchaseHeader record);

    BuyApplypurchaseHeader selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyApplypurchaseHeader record);

    int updateByPrimaryKey(BuyApplypurchaseHeader record);
    
    List<BuyApplypurchaseHeader> selectByPage(SearchPageUtil searchPageUtil);
    
    int selectPurchaseNumByMap(Map<String, Object> map);
    
    List<BuyApplypurchaseHeader> selectByStatistic(Map<String, Object> map);

	List<String> selectApprovedId(Map<String, Object> params);

	List<BuyApplypurchaseHeader> selectApprovedByPage(
			SearchPageUtil searchPageUtil);
}