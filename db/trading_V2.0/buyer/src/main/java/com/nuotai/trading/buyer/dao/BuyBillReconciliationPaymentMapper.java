package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillReconciliationPayment;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-11-01 11:02:22
 */
@Component
public interface BuyBillReconciliationPaymentMapper extends BaseDao<BuyBillReconciliationPayment> {
    //添加付款单号
	int addPaymentInfo(BuyBillReconciliationPayment buyBillReconciliationPayment);
	//根据账单号查询付款记录
    List<BuyBillReconciliationPayment> getPaymentListByRecId(Map<String,Object> map);
    //付款明细待审批列表查询
    List<BuyBillReconciliationPayment> getBillRecPaymentList(SearchPageUtil searchPageUtil);
    //根据审批情况修改记录状态
    int updatePaymentInfo(Map<String,Object> updatePaymentMap);
    //根据条件查询付款详情
    BuyBillReconciliationPayment queryRecPaymentInfo(Map<String,Object> map);
    //添加原材料采购单的付款数据
    int insertOrderPayment(BuyBillReconciliationPayment buyBillReconciliationPayment);
	//根据审批编号查询原材料采购单Id
    String selectOrderIdByAcceptPaymnentId(String acceptPaymentId);
    int deleteBuyAcceptId(String acceptPaymentId);
}
