package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuyApplypurchaseShop;
import com.nuotai.trading.dao.BaseDao;

/**
 * 
 * 
 * @author dxl"
 * @date 2018-01-02 09:11:33
 */
public interface BuyApplypurchaseShopMapper extends BaseDao<BuyApplypurchaseShop> {
	
	List<BuyApplypurchaseShop> selectByItemId(String itemId);
	
	Map<String,Object> selectOverPlusNum(Map<String, Object> map);
}
