package com.nuotai.trading.buyer.controller;

import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nuotai.trading.buyer.model.BuyPlanTime;
import com.nuotai.trading.buyer.service.BuyPlanTimeService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.utils.ObjectUtil;


/**
 * 
 * 
 * @author "
 * @date 2018-03-30 14:26:18
 */
@Controller
@RequestMapping("buyer/planTime")
public class BuyPlanTimeController extends BaseController{
	@Autowired
	private BuyPlanTimeService buyPlanTimeService;
	
	/**
	 * 计划提报时间设置
	 * @return
	 */
	@RequestMapping("planTimeSetPage")
	public String planTimeSetPage(){
		BuyPlanTime planTime = buyPlanTimeService.queryOne();
		model.addAttribute("planTime", ObjectUtil.isEmpty(planTime)?"{}":net.sf.json.JSONObject.fromObject(planTime));
		return "platform/buyer/applypurchase/planTime/loadPlanTimeSet";
	}
	
	/**
	 * 计划提报时间设置保存
	 * @param infoStr
	 * @return
	 */
	@RequestMapping(value = "savePlanTimeSet", method = RequestMethod.POST)
    @ResponseBody
    public String savePlanTimeSet(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
        	buyPlanTimeService.savePlanTimeSet(params);
            json.put("success", true);
            json.put("msg", "保存成功！");
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "保存失败！");
        }
        return json.toString();
    }
	
	/**
	 * 获得操作时间
	 * @return
	 */
	@RequestMapping("getPlanTime")
	@ResponseBody
	public JSONObject getPlanTime(){
		BuyPlanTime planTime = buyPlanTimeService.queryOne();
		JSONObject result = ObjectUtil.isEmpty(planTime)?new JSONObject():JSONObject.fromObject(planTime);
		return result;
	}
}
