package com.nuotai.trading.buyer.model.seller;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liuhui
 * @date 2017-09-20 16:10
 * @description
 **/
@Data
public class SellerDeliveryRecordItem implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String id;
    //发货id
    private String recordId;
    //订单id
    private String orderId;
    //订单明细id
    private String orderItemId;
    //采购计划单号
    private String applyCode;
    //订单号（外部）
    private String orderCode;
    //采购商订单id
    private String buyOrderId;
    //采购商订单明细id
    private String buyOrderItemId;
    //货号
    private String productCode;
    //商品名称
    private String productName;
    //规格代码
    private String skuCode;
    //规格名称
    private String skuName;
    //条形码
    private String barcode;
    //单位ID
    private String unitId;
    //单位ID（外部）
    private String unitName;
    //销售单价
    private BigDecimal salePrice;
    //仓库id
    private String warehouseId;
    //仓库code
    private String warehouseCode;
    //仓库名称
    private String warehouseName;
    //本次发货数量
    private Integer deliveryNum;
    //已发货数量（外部）
    private Integer deliveredNum;
    //订单数量（外部）
    private Integer orderNum;
    //到货数
    private Integer arrivalNum;
    //到货时间
    private Date arrivalDate;
    //入库人
    private String warehouseHolder;
    //发货状态[0、全部发货；1、部分发货]
    private Integer status;
    //备注
    private String remark;
    //是否需要发票Y:是N:否
    private String isNeedInvoice;
}
