package com.nuotai.trading.buyer.service;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.buyer.dao.BuyCustomerItemMapper;
import com.nuotai.trading.buyer.model.BuyCustomerItem;



@Service
@Transactional
public class BuyCustomerItemService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyCustomerItemService.class);

	@Autowired
	private BuyCustomerItemMapper buyCustomerItemMapper;
	
	public BuyCustomerItem get(String id){
		return buyCustomerItemMapper.get(id);
	}
	
	public List<BuyCustomerItem> queryList(Map<String, Object> map){
		return buyCustomerItemMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyCustomerItemMapper.queryCount(map);
	}
	
	public void add(BuyCustomerItem buyCustomerItem){
		buyCustomerItemMapper.add(buyCustomerItem);
	}
	
	public void update(BuyCustomerItem buyCustomerItem){
		buyCustomerItemMapper.update(buyCustomerItem);
	}
	
	public void delete(String id){
		buyCustomerItemMapper.delete(id);
	}
	
	public List<BuyCustomerItem> selectCustomerItemByCustomerId(String id){
		return buyCustomerItemMapper.selectCustomerItemByCustomerId(id);
	}

	//根据账单条件查询详细
	public List<BuyCustomerItem> queryCustomerItem(Map<String,Object> map){
		return buyCustomerItemMapper.queryCustomerItem(map);
	}

	public List<Map<String,Object>> selectTuiHuoNum(Map<String, Object> param) {
		return buyCustomerItemMapper.selectTuiHuoNum(param);
	}

}
