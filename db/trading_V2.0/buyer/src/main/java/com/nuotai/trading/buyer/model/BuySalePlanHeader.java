package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 
 * 销售计划主表
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
@Data
public class BuySalePlanHeader implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键id
	private String id;
	//公司id
	private String companyId;
	//计划编号
	private String planCode;
	//标题
	private String title;
	//备注
	private String remark;
	//状态0-等待审核；1-审核通过；2-审核不通过；
	private String status;
	//是否下单0未下单，1已下单
	private String ifOrder;
	//是否删除
	private Integer isDel;
	//创建人姓名
	private String createName;
	//创建人id
	private String createId;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人
	private String delName;
	//删除时间
	private Date delDate;
	//附件
	private String proof;
	private List<BuySalePlanItem> itemList;
}
