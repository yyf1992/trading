package com.nuotai.trading.buyer.model.seller;

import java.util.Date;
/**
 * 
 * 
 * @YYF "
 * @date 2017-09-13 16:01:52
 */
public class BSellerBillCycleManagementNew {
	//
	private String id;
	//正式申请编号
	private String newBillCycleId;
	//
	private String buyCompanyId;
	//
	private String sellerCompanyId;
	//
	private Integer cycleStartDate;
	//
	private Integer cycleEndDate;
	//
	private Integer checkoutCycle;
	//
	private Integer billStatementDate;
	private Date outStatementDate;
	//
	private String billDealStatus;
	//
	private String createUser;
	//
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNewBillCycleId() {
		return newBillCycleId;
	}

	public void setNewBillCycleId(String newBillCycleId) {
		this.newBillCycleId = newBillCycleId;
	}

	public String getBuyCompanyId() {
		return buyCompanyId;
	}

	public void setBuyCompanyId(String buyCompanyId) {
		this.buyCompanyId = buyCompanyId;
	}

	public String getSellerCompanyId() {
		return sellerCompanyId;
	}

	public void setSellerCompanyId(String sellerCompanyId) {
		this.sellerCompanyId = sellerCompanyId;
	}

	public Integer getCycleStartDate() {
		return cycleStartDate;
	}

	public void setCycleStartDate(Integer cycleStartDate) {
		this.cycleStartDate = cycleStartDate;
	}

	public Integer getCycleEndDate() {
		return cycleEndDate;
	}

	public void setCycleEndDate(Integer cycleEndDate) {
		this.cycleEndDate = cycleEndDate;
	}

	public Integer getCheckoutCycle() {
		return checkoutCycle;
	}

	public void setCheckoutCycle(Integer checkoutCycle) {
		this.checkoutCycle = checkoutCycle;
	}

	public Integer getBillStatementDate() {
		return billStatementDate;
	}

	public void setBillStatementDate(Integer billStatementDate) {
		this.billStatementDate = billStatementDate;
	}

	public String getBillDealStatus() {
		return billDealStatus;
	}

	public void setBillDealStatus(String billDealStatus) {
		this.billDealStatus = billDealStatus;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getOutStatementDate() {
		return outStatementDate;
	}

	public void setOutStatementDate(Date outStatementDate) {
		this.outStatementDate = outStatementDate;
	}
}
