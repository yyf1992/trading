package com.nuotai.trading.buyer.service;

import com.nuotai.trading.buyer.dao.BuyDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.BuyOrderProduct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class BuyDeliveryRecordItemService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyDeliveryRecordItemService.class);

	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;
	@Autowired
	private BuyOrderProductService orderProductService;
	
	public BuyDeliveryRecordItem get(String id){
		return buyDeliveryRecordItemMapper.get(id);
	}

	//根据条件查询数据详细
	public List<BuyDeliveryRecordItem> queryDeliveryRecordItem(Map<String,Object> map){
		return buyDeliveryRecordItemMapper.queryDeliveryRecordItem(map);
	}
	public List<BuyDeliveryRecordItem> queryList(Map<String, Object> map){
		return buyDeliveryRecordItemMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyDeliveryRecordItemMapper.queryCount(map);
	}
	
	public void add(BuyDeliveryRecordItem buyDeliveryRecordItem){
		buyDeliveryRecordItemMapper.add(buyDeliveryRecordItem);
	}
	
	public void update(BuyDeliveryRecordItem buyDeliveryRecordItem){
		buyDeliveryRecordItemMapper.update(buyDeliveryRecordItem);
	}
	
	public void delete(String id){
		buyDeliveryRecordItemMapper.delete(id);
	}
	
	/**
	 * 创建售后单选择商品
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> selectProductByDelivery(Map<String, Object> map){
		return buyDeliveryRecordItemMapper.selectProductByDelivery(map);
	}

	public List<Map<String,Object>> selectArrivalNum(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return buyDeliveryRecordItemMapper.selectArrivalNum(param);
	}

}
