package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-12 16:49:24
 */
@Component("buyCustomerMapper")
public interface BuyCustomerMapper extends BaseDao<BuyCustomer> {
	
	List<BuyCustomer> queryCustomerList(SearchPageUtil searchPageUtil);

	//根据对账条件查询
	List<BuyCustomer> queryCustomerListByMap(Map<String,Object> map);
	
	int selectCustomerListNumByMap(Map<String, Object> map);

	//根据对账状态修改售后
	int updateCustomerByReconciliation(Map<String, Object> map);
	
	List<BuyCustomer> queryCustomerListByStatistic(Map<String, Object> map);

	List<Map<String, Object>> getAftermarketProduct(SearchPageUtil searchPageUtil);
	List<Map<String, Object>> getAftermarketProductNew(Map<String, Object> map);

	List<BuyCustomer> queryListNew();
	
	BuyCustomer selectByOrderCode(String orderCode);
	
}
