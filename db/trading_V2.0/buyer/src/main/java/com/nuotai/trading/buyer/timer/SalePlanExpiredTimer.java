package com.nuotai.trading.buyer.timer;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.buyer.service.BuySalePlanItemService;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;

/**
 * 销售计划过期处理定时器
 * @author Administrator
 *
 */
@Component
public class SalePlanExpiredTimer {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private BuySalePlanItemService buySalePlanItemService;
	
	/**
	 * 每天1点执行
	 */
	@Scheduled(cron="0 0 1 * * ? ")
	//@Scheduled(cron="0 0/1 * * * ?")
	public void dealWithSalePlanExpired(){
		TimeTask timeTask = timeTaskService.selectByCode("SALE_PLAN_EXPIRED");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					//更新销售计划过期状态
					buySalePlanItemService.dealWithSalePlanExpired();
					
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setCurrentStatus("2");
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
}
