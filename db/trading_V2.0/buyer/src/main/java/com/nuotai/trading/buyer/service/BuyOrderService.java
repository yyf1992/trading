package com.nuotai.trading.buyer.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationPaymentMapper;
import com.nuotai.trading.buyer.dao.BuyDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.BuyManualOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderMapper;
import com.nuotai.trading.buyer.dao.BuyOrderProductMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderSupplierProductMapper;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.BuyManualOrder;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderExportData;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.model.seller.SellerOrder;
import com.nuotai.trading.buyer.model.seller.SellerOrderSupplierProduct;
import com.nuotai.trading.buyer.model.seller.SellerDeliveryRecord;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.dao.SysVerifyCopyMapper;
import com.nuotai.trading.dao.SysVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyCopyMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierLinkman;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.SysVerifyCopy;
import com.nuotai.trading.model.SysVerifyHeader;
import com.nuotai.trading.model.TradeVerifyCopy;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.BuySupplierLinkmanService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DateUtils;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * The type Buy order service.
 */
@Service
@Transactional
public class BuyOrderService {
	@Autowired
	private BuyOrderMapper orderMapper;
	@Autowired
	private BuyManualOrderMapper manualOrderMapper;
	@Autowired
	private BuyOrderProductMapper buyOrderProductMapper;
	@Autowired
	private BuyAddressService buyAddressService;
	@Autowired
	private BuyShopProductService buyShopProductService;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	@Autowired
	private BSellerOrderMapper sellerOrderMapper;
	@Autowired
	private BSellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;
	@Autowired
	private BuySupplierService supplierService;
	@Autowired
	private BuySupplierLinkmanService supplierLinkmanService;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private BuyCustomerService buyCustomerService;
	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	@Autowired
	private BuyBillReconciliationPaymentMapper paymentMapper;
	@Autowired
	private BuyProductSkuService productSkuService;
	@Autowired
	private BuyBillReconciliationMapper reconciliationMapper;
	@Autowired
	private BSellerDeliveryRecordItemMapper bSellerDeliveryRecordItemMapper;
	@Autowired
	private BSellerDeliveryRecordMapper bSellerDeliveryRecordMapper;
	@Autowired
	private BSellerBillReconciliationMapper bSellerBillReconciliationMapper;
	
	/**
	 * Select by page list.
	 *
	 * @param searchPageUtil the search page util
	 * @return the list
	 */
	public List<BuyOrder> selectByPage(SearchPageUtil searchPageUtil) {
		List<BuyOrder> orderList = orderMapper.selectByPage(searchPageUtil);
		//根据条形码来组装商品信息，条形码一样就合并商品
		if(!orderList.isEmpty()){
			for(BuyOrder order : orderList){
				Map<String,BuyOrderProduct> productMap = new HashMap<>();
				String orderId = order.getId();
				List<BuyOrderProduct> orderProductList = buyOrderProductMapper.selectByOrderId(orderId);
				for (BuyOrderProduct product:orderProductList) {
					List<Map<String,Object>> deliveryList = buyDeliveryRecordItemMapper.getDeliveryByItemIdList(product.getId());
					if(productMap.containsKey(product.getSkuOid())){
						BuyOrderProduct oldProduct = productMap.get(product.getSkuOid());
						List<Map<String,Object>> deliveryListOld = oldProduct.getDeliveryList();
						int oldGoodsNum = ObjectUtil.isEmpty(oldProduct.getGoodsNumber())?0:oldProduct.getGoodsNumber();
						int goodsNum = ObjectUtil.isEmpty(product.getGoodsNumber())?0:product.getGoodsNumber();
						int oldArrivalNum = ObjectUtil.isEmpty(oldProduct.getArrivalNum())?0:oldProduct.getArrivalNum();
						int arrvalNum = ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
						product.setGoodsNumber(oldGoodsNum+goodsNum);
						product.setArrivalNum(oldArrivalNum+arrvalNum);
						deliveryList.addAll(deliveryListOld);
					}
					product.setDeliveryList(deliveryList);
					productMap.put(product.getSkuOid(),product);
				}
				List<BuyOrderProduct> addList = new ArrayList<>(productMap.size());
				for (Map.Entry<String,BuyOrderProduct> entry : productMap.entrySet()){
					addList.add(entry.getValue());
				}
				order.setOrderProductList(addList);
				//判断是否有人审批，用于判断订单列表的修改按钮，如果有人审批通过就不能修改订单
				boolean isApproved = false;
				TradeVerifyHeader verifyHeader = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(orderId);
				if(!ObjectUtil.isEmpty(verifyHeader)){
					List<TradeVerifyPocess> verifyPocessList = tradeVerifyPocessMapper.queryListByHeaderId(verifyHeader.getId());
					for (TradeVerifyPocess process:verifyPocessList) {
						if("1".equalsIgnoreCase(process.getStatus())){
							isApproved = true;
							break;
						}
					}
				}
				order.setIsApproved(isApproved);
				//售后信息
				if(order.getCustomerId() != null){
					BuyCustomer customer = buyCustomerService.get(order.getCustomerId());
					order.setCustomerCreateName(customer.getCreateName());
					order.setCustomerCreateDate(customer.getCreateDate());
					order.setCustomerCode(customer.getCustomerCode());
				}
			}
		}
		return orderList;
	}

	/**
	 * Save order json object.
	 *
	 * @param params the params
	 * @return the json object
	 */
	public JSONObject saveOrder(Map<String, Object> params) {
		JSONObject json = new JSONObject();
		try {
			BuyOrder order = new BuyOrder();
			order.setId(ShiroUtils.getUid());
			order.setOrderCode(ShiroUtils.getOrderNum());
			order.setSuppId(params.containsKey("supplierId")?params.get("supplierId").toString():"");
			order.setSuppName(params.containsKey("supplierName")?params.get("supplierName").toString():"");
			order.setPerson(params.containsKey("sellerPerson")?params.get("sellerPerson").toString():"");
			order.setPhone(params.containsKey("sellerPhone")?params.get("sellerPhone").toString():"");
			order.setGoodsNum(Integer.parseInt(params.get("numSum").toString()));
			order.setGoodsPrice(new BigDecimal(params.get("totalPrice").toString()));
			order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
			order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
			order.setOrderKind(Constant.OrderKind.AT.getValue());//互通买卖好友下单
			order.setRemark(params.containsKey("supplierRemark")?params.get("supplierRemark").toString():"");
			order.setIsDel(Constant.IsDel.NODEL.getValue());
			order.setCreateDate(new Date());
			order.setCreateId(ShiroUtils.getUserId());
			order.setCreateName(ShiroUtils.getUserName());
			order.setCompanyId(ShiroUtils.getCompId());
			//收货地址
			int isSince=params.containsKey("is_since")?Integer.parseInt(params.get("is_since").toString()):1;
			order.setIsSince(isSince);
			if(Constant.LogisticsType.logistis.getValue()==isSince){
				//需要物流
				BuyAddress address = buyAddressService.selectByPrimaryKey(params.get("addressId").toString());
				order.setPersonName(address.getPersonName());//收货人
				order.setProvince(address.getProvince());//省
				order.setCity(address.getCity());//市
				order.setArea(address.getArea());//区
				order.setAddrName(address.getAddrName());//详细地址
				order.setAreaCode(address.getZone());//区号
				order.setPlaneNumber(address.getTelNo());//座机号
				order.setReceiptPhone(address.getPhone());//手机号
			}
			Map<String,Object> goodsMap = new HashMap<String,Object>();
			goodsMap.put("supplierCompanyId", order.getSuppId());
			goodsMap.put("clientCompanyId", order.getCompanyId());
			//商品信息
			String goodsStrArr = params.get("goodsStr").toString();
			String[] goodsArr = goodsStrArr.split("@");
			for(String goodsStr : goodsArr){
				String[] goodsItem = goodsStr.split(",");
				BuyOrderProduct product = new BuyOrderProduct();
				product.setId(ShiroUtils.getUid());
				product.setOrderId(order.getId());
				product.setSkuOid(goodsItem[0]);
				product.setProCode(goodsItem[1]);
				product.setProName(goodsItem[2]);
				product.setSkuCode(goodsItem[3]);
				product.setSkuName(goodsItem[4]);
				product.setUnitId(goodsItem[5]);
				product.setGoodsNumber(Integer.parseInt(goodsItem[7]));
				product.setPrice(new BigDecimal(goodsItem[8]));
				product.setPriceSum(new BigDecimal(goodsItem[9]));
				product.setWareHouseId(goodsItem[10]);
				product.setIsNeedInvoice(goodsItem[12]);
				product.setRemark(goodsItem.length==14?goodsItem[13]:"");
				buyOrderProductMapper.insertSelective(product);
				
				SellerOrderSupplierProduct supplierProduct = new SellerOrderSupplierProduct();
				supplierProduct.setId(ShiroUtils.getUid());
				supplierProduct.setOrderId(order.getId());
				//取得供应商商品信息
				goodsMap.put("clientSkuoid", product.getSkuOid());
				List<Map<String,Object>> supplierProductList = buyShopProductService.getSupplierProductByClient(goodsMap);
				if(!supplierProductList.isEmpty()){
					Map<String,Object> supplierProductMap = supplierProductList.get(0);
					supplierProduct.setProductCode(supplierProductMap.get("product_code").toString());
					supplierProduct.setProductName(supplierProductMap.get("product_name").toString());
					supplierProduct.setSkuCode(supplierProductMap.get("sku_code").toString());
					supplierProduct.setSkuName(supplierProductMap.get("sku_name").toString());
					supplierProduct.setBarcode(supplierProductMap.get("barcode").toString());
					supplierProduct.setUnitId(supplierProductMap.get("unit_id").toString());
				}else{
					supplierProduct.setProductCode(product.getProCode());
					supplierProduct.setProductName(product.getProName());
					supplierProduct.setSkuCode(product.getSkuCode());
					supplierProduct.setSkuName(product.getSkuName());
					supplierProduct.setBarcode(product.getSkuOid());
					supplierProduct.setUnitId(product.getUnitId());
				}
				supplierProduct.setOrderNum(product.getGoodsNumber());
				supplierProduct.setPrice(product.getPrice());
				supplierProduct.setTotalMoney(product.getPriceSum());
				supplierProduct.setWarehouseId(product.getWareHouseId());
				supplierProduct.setIsNeedInvoice(product.getIsNeedInvoice());
				supplierProduct.setRemark(product.getRemark());
//				buyOrderSupplierProductService.insertSelective(supplierProduct);
			}
			orderMapper.insertOrder(order);
			json.put("success", true);
			json.put("msg", "保存成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "保存失败！");
		}
		return json;
	}

	/**
	 * 向供应商采购商品订单-列表订单数量
	 *
	 * @param params the params
	 */
	public void getOrderNum(Map<String, Object> params) {
		Map<String, Object> selectMap = new HashMap<String, Object>();
		//订单类型  0-采购订单  1-原材料订单
		String orderKind=params.containsKey("orderKind")?params.get("orderKind").toString():"0";
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("orderKind",orderKind);
//		selectMap.put("isCheck",Constant.IsCheck.WAITVERIFY.getValue());
		//待内部审批数量
		selectMap.put("internalApprove","internalApprove");//内部审批状态
		int internalNum = selectOrdersNumByMap(selectMap);
		params.put("internalNum", internalNum);
		selectMap.put("internalApprove","");
		if("0".equals(orderKind)){
			selectMap.put("orderKind",Constant.OrderKind.AT.getValue());//订单类型  0-采购订单  1-原材料订单
			//待接单
			selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
			selectMap.put("status", Constant.OrderStatus.WAITORDER.getValue());
			int waitOrderNum = selectOrdersNumByMap(selectMap);
			params.put("waitOrderNum", waitOrderNum);
			//待对方发货
			selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
			selectMap.put("status", Constant.OrderStatus.WAITDELIVERY.getValue());
			int waitDeliveryNum = selectOrdersNumByMap(selectMap);
			params.put("waitDeliveryNum", waitDeliveryNum);
			//已取消
			selectMap.put("isCheck","");
			selectMap.put("status", Constant.OrderStatus.CANCEL.getValue());
			int cancelNum = selectOrdersNumByMap(selectMap);
			params.put("cancelNum", cancelNum);
			//已驳回
			selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
			selectMap.put("status", Constant.OrderStatus.REJECT.getValue());
			int rejectNum = selectOrdersNumByMap(selectMap);
			params.put("rejectNum", rejectNum);
			//已终止
			selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
			selectMap.put("status", Constant.OrderStatus.STOP.getValue());
			int stopNum = selectOrdersNumByMap(selectMap);
			params.put("stopNum", stopNum);
		}
		//待收货
		selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
		selectMap.put("status", Constant.OrderStatus.WAITRECEIVE.getValue());
		int waitReceiveNum = selectOrdersNumByMap(selectMap);
		params.put("waitReceiveNum", waitReceiveNum);
		//已收货
		selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
		selectMap.put("status", Constant.OrderStatus.RECEIVED.getValue());
		int receivedNum = selectOrdersNumByMap(selectMap);
		params.put("receivedNum", receivedNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		//根据tabId设置interest
		this.setOrderListInterest(params,tabId);
	}
	
	/**
	 * 设置订单列表interest选项
	 * @param params
	 * @param tabId
	 */
	private void setOrderListInterest(Map<String, Object> params,int tabId) {
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			case 5:
				params.put("interest", "5");
				break;
			case 6:
				params.put("interest", "6");
				break;
			case 7:
				params.put("interest", "7");
				break;
			case 8:
				params.put("interest", "8");
				break;
			case 9:
				params.put("interest", "9");
				break;
			case 10:
				params.put("interest", "10");
				break;
			default:
				params.put("interest", "0");
				break;
		}
	}

	/**
	 * Select orders num by map int.
	 *
	 * @param params the params
	 * @return the int
	 */
	public int selectOrdersNumByMap(Map<String, Object> params) {
		return orderMapper.selectOrdersNumByMap(params);
	}

	/**
	 * 获得数据
	 *
	 * @param searchPageUtil the search page util
	 * @param params         the params
	 * @return string
	 */
	public String loadData(SearchPageUtil searchPageUtil,Map<String, Object> params) {
		String orderKind=params.containsKey("orderKind")?params.get("orderKind").toString():"0";
		if("1".equals(orderKind)){
			params.put("orderKind", Constant.OrderKind.MT.getValue());
		}else{
			params.put("orderKind", Constant.OrderKind.AT.getValue());
		}
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/interwork/buyOrder/";
		//根据tabId设置interest
		this.setOrderListInterest(params,tabId);
		//根据interest设置订单状态查询条件
		this.setQueryParamStatus(params);
		searchPageUtil.setObject(params);
		List<BuyOrder> orderList = selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(orderList);
		return returnPath;
	}

	/**
	 * Set query param status.
	 * 根据interest设置订单状态查询条件
	 * @param params the params
	 */
	public void setQueryParamStatus(Map<String, Object> params){
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		switch (interest){
			//待内部审批
			case "1":
//				params.put("isCheck", Constant.IsCheck.WAITVERIFY.getValue());
				params.put("internalApprove", "internalApprove");
				break;
			//待接单
			case "2":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.WAITORDER.getValue());
				break;
			//待确认协议
			case "3" :
				break;
			//待对方发货
			case "4":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.WAITDELIVERY.getValue());
				break;
			//待收货
			case "5":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.WAITRECEIVE.getValue());
				break;
			//已收货
			case "6":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.RECEIVED.getValue());
				break;
			//交易完成
			case "7":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.FINISHED.getValue());
				break;
			//已取消
			case "8":
				params.put("isCheck", "");
				params.put("status", Constant.OrderStatus.CANCEL.getValue());
				break;
			//已驳回
			case "9":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.REJECT.getValue());
				break;
			//已终止
			case "10":
				params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
				params.put("status", Constant.OrderStatus.STOP.getValue());
				break;
			default:
				break;
		}
	}

	/**
	 * 向供应商下单保存
	 *
	 * @param params the params
	 * @return list
	 */
	public List<String> saveOrderNew(Map<String, Object> params) {
		List<String> idList = new ArrayList<String>();
		BuyOrder order = new BuyOrder();
		order.setId(ShiroUtils.getUid());
		order.setOrderCode(ShiroUtils.getOrderNum());
		order.setSuppId(params.containsKey("supplierId")?params.get("supplierId").toString():"");
		order.setSuppName(params.containsKey("supplierName")?params.get("supplierName").toString():"");
		order.setPerson(params.containsKey("sellerPerson")?params.get("sellerPerson").toString():"");
		order.setPhone(params.containsKey("sellerPhone")?params.get("sellerPhone").toString():"");
		order.setGoodsNum(Integer.parseInt(params.get("numSum").toString()));
		order.setGoodsPrice(new BigDecimal(params.get("totalPrice").toString()));
		order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
		order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
		order.setOrderKind(Constant.OrderKind.AT.getValue());//互通买卖好友下单
		order.setRemark(params.containsKey("supplierRemark")?params.get("supplierRemark").toString():"");
		order.setIsDel(Constant.IsDel.NODEL.getValue());
		order.setCreateDate(new Date());
		order.setCreateId(ShiroUtils.getUserId());
		order.setCreateName(ShiroUtils.getUserName());
		order.setCompanyId(ShiroUtils.getCompId());
		order.setOrderType("0");
		//收货地址
		int isSince=params.containsKey("is_since")?Integer.parseInt(params.get("is_since").toString()):1;
		order.setIsSince(isSince);
		if(Constant.LogisticsType.logistis.getValue()==isSince){
			//需要物流
			BuyAddress address = buyAddressService.selectByPrimaryKey(params.get("addressId").toString());
			order.setPersonName(address.getPersonName());//收货人
			order.setProvince(address.getProvince());//省
			order.setCity(address.getCity());//市
			order.setArea(address.getArea());//区
			order.setAddrName(address.getAddrName());//详细地址
			order.setAreaCode(address.getZone());//区号
			order.setPlaneNumber(address.getTelNo());//座机号
			order.setReceiptPhone(address.getPhone());//手机号
		}
		Map<String,Object> goodsMap = new HashMap<String,Object>();
		goodsMap.put("supplierCompanyId", order.getSuppId());
		goodsMap.put("clientCompanyId", order.getCompanyId());
		//商品信息
		String goodsStrArr = params.get("goodsStr").toString();
		String[] goodsArr = goodsStrArr.split("@");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split(",");
			BuyOrderProduct product = new BuyOrderProduct();
			product.setId(ShiroUtils.getUid());
			product.setOrderId(order.getId());
			product.setSkuOid(goodsItem[0]);
			product.setProCode(goodsItem[1]);
			product.setProName(goodsItem[2]);
			product.setSkuCode(goodsItem[3]);
			product.setSkuName(goodsItem[4]);
			product.setUnitId(goodsItem[5]);
			product.setGoodsNumber(Integer.parseInt(goodsItem[7]));
			product.setPrice(new BigDecimal(goodsItem[8]));
			product.setPriceSum(new BigDecimal(goodsItem[9]));
			product.setWareHouseId(goodsItem[10]);
			product.setIsNeedInvoice(goodsItem[12]);
			product.setRemark(goodsItem.length==14?goodsItem[13]:"");
			buyOrderProductMapper.insertSelective(product);
		}
		orderMapper.insertOrder(order);
		idList.add(order.getId());
		return idList;
	}

	/**
	 * Select by primary key buy order.
	 *
	 * @param id the id
	 * @return the buy order
	 */
	public BuyOrder selectByPrimaryKey(String id) {
		// 审核时间
		BuyOrder order = orderMapper.selectByPrimaryKey(id);
		if(order != null && !"".equals(order.getId())){
			TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(id);
			if(header != null && !"".equals(header.getId())){
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				if(!pocessList.isEmpty()){
					for(TradeVerifyPocess pocess : pocessList){
						order.setVerifyDateFormat(pocess.getStartDate());
					}
				}
			}
		}
		return order;
	}

	/**
	 * Select by order code buy order.
	 * 根据订单号查询订单信息
	 * @param orderCode the order code
	 * @return the buy order
	 */
	public BuyOrder selectByOrderCode(String orderCode) {
		BuyOrder order = orderMapper.selectByOrderCode(orderCode);
		if(!ObjectUtil.isEmpty(order)){
			List<BuyOrderProduct> productList = buyOrderProductMapper.selectByOrderId(order.getId());
			int totalDeliveryNum = 0;
			int totalArrivalNum = 0;
			for (BuyOrderProduct product:productList) {
				totalDeliveryNum+= ObjectUtil.isEmpty(product.getDeliveredNum())?0:product.getDeliveredNum();
				totalArrivalNum+= ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
			}
			order.setTotalDeliveryNum(totalDeliveryNum);
			order.setTotalArrivalNum(totalArrivalNum);
			order.setOrderProductList(productList);
		}
		return order;
	}
	/**
	 * Verify success json object.
	 *
	 * @param id the id
	 * @return the json object
	 */
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		BuyOrder order = new BuyOrder();
		order.setId(id);
		order.setStatus(0);//待接单
		order.setIsCheck(1);//已审核
		int result = orderMapper.updateByPrimaryKeySelective(order);
		// 商品明细表锁定数量修改为0
		List<BuyOrderProduct> buyOrderProducts = buyOrderProductMapper.selectByOrderId(order.getId());
		if(buyOrderProducts != null && buyOrderProducts.size() > 0){
			for(BuyOrderProduct buyOrderProduct : buyOrderProducts){
				buyOrderProduct.setLockGoodsNumber(0);
				buyOrderProductMapper.updateByPrimaryKeySelective(buyOrderProduct);
			}
		}
		if(result>0){
			//将订单保存到卖家
			order = orderMapper.selectByPrimaryKey(id);
			saveSellerOrder(order);
			json.put("success", true);
			json.put("msg", "订单已成功推送至卖家！");
		}else{
			json.put("success", false);
			json.put("msg", "订单审核状态修改失败！");
		}
		return json;
	}
	
	public JSONObject verifyRefuse(String id) {
		JSONObject json = new JSONObject();
		BuyOrder order = new BuyOrder();
		order.setId(id);
		order.setIsCheck(2);//已拒绝
		orderMapper.updateByPrimaryKeySelective(order);
		json.put("success", true);
		json.put("msg", "审批已拒绝！");
		return json;
	}

	
	private void saveSellerOrder(BuyOrder order) {
		BuyCompany buyCompany = buyCompanyMapper.selectByPrimaryKey(order.getCompanyId());
		//保存主表
		SellerOrder sellerOrder = sellerOrderMapper.getByOrderCode(order.getOrderCode());
		if(!ObjectUtil.isEmpty(sellerOrder)){
			sellerOrderMapper.deleteByPrimaryKey(sellerOrder.getId());
			sellerOrderSupplierProductMapper.deleteByOrderId(sellerOrder.getId());
		}
		sellerOrder = new SellerOrder();
		sellerOrder.setId(ShiroUtils.getUid());
		sellerOrder.setOrderCode(order.getOrderCode());
		sellerOrder.setSuppId(order.getSuppId());
		sellerOrder.setSuppName(order.getSuppName());
		sellerOrder.setPerson(order.getPerson());
		sellerOrder.setPhone(order.getPhone());
		sellerOrder.setGoodsNum(order.getGoodsNum());
		sellerOrder.setGoodsPrice(order.getGoodsPrice());
		sellerOrder.setStatus(0);
		sellerOrder.setIsCheck(0);
		sellerOrder.setIsSince(0);//是否自提 0，不自提；1，自提
		sellerOrder.setOrderKind(order.getOrderKind());//类型 0，自动；1，手动
		sellerOrder.setRemark(order.getRemark());
		sellerOrder.setPersonName(order.getPersonName());
		sellerOrder.setProvince(order.getProvince());
		sellerOrder.setCity(order.getCity());
		sellerOrder.setArea(order.getArea());
		sellerOrder.setAddrName(order.getAddrName());
		sellerOrder.setAreaCode(order.getAreaCode());
		sellerOrder.setPlaneNumber(order.getPlaneNumber());
		sellerOrder.setReceiptPhone(order.getReceiptPhone());
		sellerOrder.setCreateId(order.getCreateId());
		sellerOrder.setCreateName(order.getCreateName());
		sellerOrder.setCreateDate(new Date());
		sellerOrder.setCompanyId(ObjectUtil.isEmpty(buyCompany)?"":buyCompany.getId());
		sellerOrder.setCompanyName(ObjectUtil.isEmpty(buyCompany)?"":buyCompany.getCompanyName());
		sellerOrder.setBuyerOrderId(order.getId());
		sellerOrder.setOrderType(order.getOrderType());
		sellerOrder.setIsChange(0);
		//sellerOrder.setPredictArred(order.getPredictArred());
		sellerOrderMapper.add(sellerOrder);
		//保存明细表
		List<BuyOrderProduct> buyOrderProducts = buyOrderProductMapper.selectByOrderId(order.getId());
		for (BuyOrderProduct buyOrderProduct : buyOrderProducts) {
			SellerOrderSupplierProduct sellerOrderProduct = new SellerOrderSupplierProduct();
			sellerOrderProduct.setId(ShiroUtils.getUid());
			sellerOrderProduct.setOrderId(sellerOrder.getId());
			sellerOrderProduct.setBuyOrderItemId(buyOrderProduct.getId());
			sellerOrderProduct.setApplyCode(buyOrderProduct.getApplyCode());
			sellerOrderProduct.setProductCode(buyOrderProduct.getProCode());
			sellerOrderProduct.setProductName(buyOrderProduct.getProName());
			sellerOrderProduct.setSkuCode(buyOrderProduct.getSkuCode());
			sellerOrderProduct.setSkuName(buyOrderProduct.getSkuName());
			sellerOrderProduct.setBarcode(buyOrderProduct.getSkuOid());
			sellerOrderProduct.setPrice(buyOrderProduct.getPrice());
			sellerOrderProduct.setOrderNum(buyOrderProduct.getGoodsNumber());
			sellerOrderProduct.setDiscountMoney(new BigDecimal(0.0000));
			sellerOrderProduct.setTotalMoney(buyOrderProduct.getPriceSum());
			sellerOrderProduct.setWarehouseId(buyOrderProduct.getWareHouseId());
			sellerOrderProduct.setDeliveredNum(0);
			sellerOrderProduct.setArrivalNum(0);
			sellerOrderProduct.setRemark(buyOrderProduct.getRemark());
			sellerOrderProduct.setUnitId(buyOrderProduct.getUnitId());
			sellerOrderProduct.setIsNeedInvoice(buyOrderProduct.getIsNeedInvoice());
			sellerOrderProduct.setPredictArred(buyOrderProduct.getPredictArred());
			sellerOrderSupplierProductMapper.add(sellerOrderProduct);

		}
	}

	/**
	 * Verify error json object.
	 *
	 * @param id the id
	 * @return the json object
	 */
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		BuyOrder order = new BuyOrder();
		order.setId(id);
		order.setIsCheck(2);
		int result = orderMapper.updateByPrimaryKeySelective(order);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 手工下单商品订单-列表订单数量
	 *
	 * @param params the params
	 */
	public void getManualOrderNum(Map<String, Object> params) {
		//待内部审批数量
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("orderKind",Constant.OrderKind.MT.getValue());//订单类型
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("isCheck",Constant.IsCheck.WAITVERIFY.getValue());
		int waitVerifyNum = selectOrdersNumByMap(selectMap);
		//已取消
		selectMap.put("status", Constant.OrderStatus.CANCEL.getValue());
		int cancelNum = selectOrdersNumByMap(selectMap);
		//已完成采购数
		selectMap.put("isCheck",Constant.IsCheck.ALREADYVERIFY.getValue());
		selectMap.put("status", Constant.OrderStatus.FINISHED.getValue());
		int overNum = selectOrdersNumByMap(selectMap);
		//已驳回
		selectMap.put("status", Constant.OrderStatus.REJECT.getValue());
		int rejectNum = selectOrdersNumByMap(selectMap);
		params.put("waitVerifyNum", waitVerifyNum);
		params.put("overNum", overNum);
		params.put("cancelNum", cancelNum);
		params.put("rejectNum", rejectNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			break;
		case 1:
			params.put("interest", "1");
			break;
		case 2:
			params.put("interest", "2");
			break;
		case 3:
			params.put("interest", "3");
			break;
		default:
			params.put("interest", "0");
			break;
		}
	}

	/**
	 * 获得手工下单商品订单数据
	 *
	 * @param searchPageUtil the search page util
	 * @param params         the params
	 * @return string
	 */
	public String loadManualData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("orderKind", Constant.OrderKind.MT.getValue());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/manualorder/buyorder/";
		switch (tabId) {
		case 0:
			params.put("interest", "0");
			returnPath += "allOrder";
			break;
		case 1:
			params.put("interest", "1");
			returnPath += "waitVerify";
			break;
		case 2:
			params.put("interest", "2");
			returnPath += "overOrder";
			break;
		case 3:
			params.put("interest", "3");
			returnPath += "cancelOrder";
			break;
		case 4:
			params.put("interest", "4");
			returnPath += "rejectOrder";
			break;
		default:
			returnPath += "allOrder";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("1".equals(interest)){
			//待内部审批
			params.put("isCheck", Constant.IsCheck.WAITVERIFY.getValue());
		}else if("2".equals(interest)){
			//已完成
			params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
			params.put("status", Constant.OrderStatus.FINISHED.getValue());
		}else if("3".equals(interest)){
			//已取消
			params.put("isCheck", Constant.IsCheck.WAITVERIFY.getValue());
			params.put("status", Constant.OrderStatus.CANCEL.getValue());
		}else if("4".equals(interest)){
			//已驳回
			params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
			params.put("status", Constant.OrderStatus.REJECT.getValue());
		}
		searchPageUtil.setObject(params);
		List<BuyOrder> orderList = selectByPage(searchPageUtil);
		if(orderList != null && orderList.size() > 0){
			for(BuyOrder order : orderList){
				//获得审批数据
				TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(order.getId());
				//tradeVerifyHeaderService.getVerifyDetail(model,tradeVerifyHeader.getId());
				if(header != null){
					PrettyTime p = new PrettyTime(Constant.locale);
					//获得审批主表
					//TradeVerifyHeader header = tradeVerifyHeaderMapper.get(id);
					
					if(header.getCreateId().equals(ShiroUtils.getUserId())){
						header.setStartName(header.getCreateName());
						//自己创建的
						if("0".equals(header.getStatus())){
							//审批中的数据
							//详情页中不展示撤销
							// revoke = true;
						}
					}else{
						header.setStartName(header.getCreateName());
					}
					//获得审批流程
					//是否需要审批
					List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
					if(!pocessList.isEmpty()){
						int n = 0;
						for(TradeVerifyPocess pocess : pocessList){
							pocess.setStartIndext(false);
							if("0".equals(pocess.getStatus())){
								n++;
							}else{
								pocess.setStartDateFormat(p.format(pocess.getEndDate()).replaceAll(" ", ""));
							}
							if(n==1){
								//第一个待审批数据
								pocess.setStartDateFormat(p.format(pocess.getStartDate()).replaceAll(" ", ""));
								pocess.setStartIndext(true);
							}
						}
					}
					//获得抄送数据
					order.setTradeHeader(header);
					order.setPocessList(pocessList);
				}
			}
		}
		searchPageUtil.getPage().setList(orderList);
		return returnPath;
	}

	/**
	 * 手工下单商品保存
	 *
	 * @param map the map
	 * @return list
	 * @throws Exception the exception
	 */
	public List<String> saveAddManualOrder(Map<String,Object> map) throws Exception{
		List<String> idList = new ArrayList<String>();
		
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		// 默认收货地址查询
		Map<String ,Object> mapAdd = new HashMap<String, Object>();
		mapAdd.put("compId", ShiroUtils.getCompId());
		mapAdd.put("isDefault", 0);
		List<BuyAddress> addressList = buyAddressService.selectByMap(mapAdd);
		BuyAddress address = new BuyAddress();
		if(addressList != null && addressList.size() > 0){
			address = addressList.get(0);
		}
		String suppId = map.get("suppId") == null ? "" : map.get("suppId").toString();
		if(suppId.equals("")){
			//新增一条供应商
			BuySupplier supplier = new BuySupplier();
			Map<String,Object> mapPare = new HashMap<String, Object>();
			mapPare.put("isDel", Constant.IsDel.NODEL.getValue()); // 未删除
			mapPare.put("suppName", map.get("suppName").toString());
			List<BuySupplier> supplierList = supplierService.selectByMap(mapPare);
			if(supplierList != null && supplierList.size() > 0){
				// 名字存在
				throw new Exception("供应商名称已存在！");
			}
			supplier.setId(ShiroUtils.getUid());
			supplier.setSuppName(map.get("suppName").toString());
			supplier.setCompanyId(user.getCompanyId());
			supplier.setAddType("0");// 手工添加
			supplier.setType("1");//非互通
			supplier.setIsDel(Constant.IsDel.NODEL.getValue()); // 未删除
			supplier.setCreateId(user.getId());
			supplier.setCreateName(user.getUserName());
			supplier.setCreateDate(date);
			supplierService.insert(supplier);
			// 添加联系人表
			BuySupplierLinkman supplierLinkman = new BuySupplierLinkman();
			supplierLinkman.setId(ShiroUtils.getUid());
			supplierLinkman.setSupplierId(supplier.getId());
			supplierLinkman.setPerson(map.get("person").toString());
			supplierLinkman.setPhone(map.get("phone").toString());
			supplierLinkman.setIsDefault(1);
			supplierLinkmanService.insert(supplierLinkman);
			//附件表添加
		}
		// 订单表添加
		BuyOrder order = new BuyOrder();
		order.setId(ShiroUtils.getUid());
		order.setOrderCode(ShiroUtils.getUid());
		order.setSuppId(suppId);
		order.setSuppName(map.get("suppName").toString());
		order.setPerson(map.get("person").toString());
		order.setPhone(map.get("phone").toString());
		order.setGoodsNum(Integer.parseInt(map.get("goodsNum").toString()));
		order.setGoodsPrice(new BigDecimal(map.get("goodsPrice").toString()));
		order.setStatus(Constant.OrderStatus.WAITORDER.getValue());
		order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());
		order.setOrderKind(Constant.OrderKind.MT.getValue());
		order.setIsDel(Constant.IsDel.NODEL.getValue());
		order.setCreateId(user.getId());
		order.setCreateName(user.getUserName());
		order.setCreateDate(date);
		order.setCompanyId(ShiroUtils.getCompId());
		order.setPersonName(address.getPersonName());
		order.setProvince(address.getProvince());
		order.setCity(address.getCity());
		order.setArea(address.getArea());
		order.setAddrName(address.getAddrName());
		order.setAreaCode(address.getZone());
		order.setPlaneNumber(address.getTelNo());
		order.setReceiptPhone(address.getPhone());
		orderMapper.insert(order);
		// 手工录入订单表添加
		BuyManualOrder manualOrder = new BuyManualOrder();
		manualOrder.setId(ShiroUtils.getUid());
		manualOrder.setOrderId(order.getId());
		String postage = map.get("postage") == null ? "" : map.get("postage").toString();
		if(!postage.equals("")){
			manualOrder.setPostage(new BigDecimal(postage));
		}
		String purchasePrice = map.get("purchasePrice") == null ? "" : map.get("purchasePrice").toString();
		if(!purchasePrice.equals("")){
			manualOrder.setPurchasePrice(new BigDecimal(purchasePrice));
		}
		String thisPrice = map.get("thisPrice") == null ? "" : map.get("thisPrice").toString();
		if(!thisPrice.equals("")){
			manualOrder.setThisPrice(new BigDecimal(thisPrice));
		}
		String paymentPrice = map.get("paymentPrice") == null ? "" : map.get("paymentPrice").toString();
		if(!paymentPrice.equals("")){
			manualOrder.setPaymentPrice(new BigDecimal(paymentPrice));
		}
		String paymentType = map.get("paymentType") == null ? "" : map.get("paymentType").toString();
		if(!paymentType.equals("")){
			manualOrder.setPaymentType(Integer.parseInt(paymentType));
		}
		String paymentDate = map.containsKey("paymentDate") ? map.get("paymentDate") != null
				&& !"".equals(map.get("paymentDate")) ? map.get(
				"paymentDate").toString() : "" : "";
		if (!"".equals(paymentDate)) {
			manualOrder.setPaymentDate(sf.parse(paymentDate));
		}
		manualOrder.setPaymentPerson(map.get("paymentPerson") == null ? "" : map.get("paymentPerson").toString());
		manualOrder.setHandler(map.get("handler") == null ? "" : map.get("handler").toString());
		manualOrder.setCreateId(user.getId());
		manualOrder.setCreateName(user.getUserName());
		manualOrder.setCreateDate(date);
		manualOrder.setIsDel(Constant.IsDel.NODEL.getValue());
		manualOrder.setPurchVoucher(map.get("a3Str") == null ? "" : map.get("a3Str").toString());
		manualOrder.setPaymentVoucher(map.get("a4Str") == null ? "" : map.get("a4Str").toString());
		manualOrder.setContractFile(map.get("a5Str") == null ? "" : map.get("a5Str").toString());
		manualOrderMapper.insert(manualOrder);
		// 订单商品关联表添加
		BuyOrderProduct orderProduct = new BuyOrderProduct();
		String[] productInfo = map.get("productInfo").toString().split("@");
		for(int i = 0;i < productInfo.length;i++){
			String[] product = productInfo[i].split(",");
			orderProduct.setId(ShiroUtils.getUid());
			orderProduct.setOrderId(order.getId());
			orderProduct.setProCode(product[0]);
			orderProduct.setProName(product[1]);
			orderProduct.setSkuCode(product[2]);
			orderProduct.setSkuName(product[3]);
			orderProduct.setSkuOid(product[4]);
			orderProduct.setUnitId(product[5]);
			orderProduct.setPrice(new BigDecimal(product[6]));
			orderProduct.setGoodsNumber(Integer.parseInt(product[7]));
			orderProduct.setPreferentialPrice(new BigDecimal(product[8]));
			orderProduct.setPriceSum(new BigDecimal(product[9]));
			orderProduct.setWareHouseId(product[11]);
			orderProduct.setRemark(product[10]);
			buyOrderProductMapper.insert(orderProduct);
		}
		idList.add(order.getId());
		return idList;
	}

	/**
	 * 保存批量生成采购订单
	 *
	 * @param params the params
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<String> saveBeatchCreateOrder(Map<String, Object> params) throws Exception{
		List<String> idList = new ArrayList<String>();
		String infoStr = params.get("infoStr") == null ? "" : params.get("infoStr").toString();
		String[] s = infoStr.split("@");
		for(int i = 0;i < s.length;i++){
			String[] orderStr = s[i].split(",");
			BuyOrder order = new BuyOrder();
			order.setId(ShiroUtils.getUid());
			order.setOrderCode(ShiroUtils.getOrderNum());
			order.setSuppId(orderStr[0]);
			order.setSuppName(orderStr[1]);
			order.setPerson(orderStr[2]);
			order.setPhone(orderStr[3]);
			order.setGoodsNum(Integer.parseInt(orderStr[4]));
			order.setGoodsPrice(new BigDecimal(orderStr[5]));
			order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
			order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
			order.setRemark(orderStr[8]);
			/*if (!"".equals(orderStr[9])) {
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
				order.setPredictArred(sf.parse(orderStr[9]));//要求到货日期
			}*/
			order.setIsDel(Constant.IsDel.NODEL.getValue());
			order.setCreateDate(new Date());
			order.setCreateId(ShiroUtils.getUserId());
			order.setCreateName(ShiroUtils.getUserName());
			order.setCompanyId(ShiroUtils.getCompId());
			order.setOrderType("0");
			//收货地址
			int isSince = Integer.parseInt(orderStr[6]);
			order.setIsSince(isSince);
			if(Constant.LogisticsType.logistis.getValue()== isSince){
				//需要物流
				BuyAddress address = buyAddressService.selectByPrimaryKey(orderStr[7]);
				order.setPersonName(address.getPersonName());//收货人
				order.setProvince(address.getProvince());//省
				order.setCity(address.getCity());//市
				order.setArea(address.getArea());//区
				order.setAddrName(address.getAddrName());//详细地址
				order.setAreaCode(address.getZone());//区号
				order.setPlaneNumber(address.getTelNo());//座机号
				order.setReceiptPhone(address.getPhone());//手机号
			}
			String item = s[i].substring(s[i].indexOf("{"),s[i].length());
			net.sf.json.JSONObject jsonItem = net.sf.json.JSONObject.fromObject(item);
		    JSONArray productArray = jsonItem.getJSONArray("productList");
		    String skuOid = "";
		    for(int j = 0 ; j < productArray.size(); j++){
		    	net.sf.json.JSONObject jsonShop = productArray.getJSONObject(j);
		    	BuyOrderProduct product = new BuyOrderProduct();
				product.setId(ShiroUtils.getUid());
				product.setOrderId(order.getId());
				product.setApplyCode(jsonShop.get("applyCode").toString());
				skuOid = jsonShop.get("skuOid").toString();
				product.setSkuOid(skuOid);
				product.setProCode(jsonShop.get("productCode").toString());
				product.setProName(jsonShop.get("productName").toString());
				product.setSkuCode(jsonShop.get("skuCode").toString());
				product.setSkuName(jsonShop.get("skuName").toString());
				product.setUnitId(jsonShop.get("unitId").toString());
				product.setGoodsNumber(Integer.parseInt(jsonShop.get("num").toString()));
				product.setLockGoodsNumber(product.getGoodsNumber());//锁定数量
				product.setPrice(new BigDecimal(jsonShop.get("price").toString()));
				//定制商品修改的价格
				if (jsonShop.containsKey("afterPrice")){
					if (!ObjectUtil.isEmpty(jsonShop.get("afterPrice"))){
						product.setUpdatePrice(new BigDecimal(jsonShop.get("afterPrice").toString()));
					}
				}
				product.setPriceSum(new BigDecimal(jsonShop.get("priceSum").toString()));
				product.setWareHouseId(jsonShop.get("warehouseId").toString());
				product.setIsNeedInvoice(jsonShop.get("invoice").toString());
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
				product.setPredictArred(sf.parse(jsonShop.get("predictArred").toString()));
				product.setRemark(jsonShop.get("remark").toString());
				buyOrderProductMapper.insertSelective(product);
		    }
		    //根据条形码查询商品信息
			List<BuyProductSku> productList = productSkuService.selectByBarCode(skuOid);
			if(productList != null && productList.size() > 0){
				BuyProductSku sku = productList.get(0);
				if(sku.getProductType() == 1){//原材料
					order.setOrderKind(Constant.OrderKind.MT.getValue());
				}else{
					order.setOrderKind(Constant.OrderKind.AT.getValue());
				}
			}
		    orderMapper.insertOrder(order);
		    idList.add(order.getId());
		}
		return idList;
	}

	/**
	 * Parse to map map.
	 *
	 * @param args the args
	 * @return the map
	 */
	public Map<String, String> parseToMap(String[] args) {

		Map<String, String> map = new HashMap<String, String>();
        String firstNumber = "";
        String other = "";
        for (String str: args) {
            firstNumber = str.split(",")[0];
            other = str.substring(str.indexOf(",")+1, str.length());
            String strs = map.get(firstNumber);
            if (strs != null && strs != "") {
                strs = strs + other + "@";
            } else {
                strs = other + "@";
            }
            map.put(firstNumber, strs);
        }
        return map;
    }

	/**
	 * 取消订单保存
	 *
	 * @param orderId the order id
	 * @throws Exception the exception
	 */
	public void saveCancelOrder(String orderId) throws Exception{
		BuyOrder order = selectByPrimaryKey(orderId);
		if(order != null){
			if(order.getIsCheck() == Constant.IsCheck.WAITVERIFY.getValue()){
				if(order.getStatus() == Constant.OrderStatus.WAITORDER.getValue()){
					order.setStatus(Constant.OrderStatus.CANCEL.getValue());
					orderMapper.updateByPrimaryKeySelective(order);
				}else{
					throw new Exception("该订单已审核，不允许取消！");
				}
			}else{
				throw new Exception("该订单已审核，不允许取消！");
			}
		}else{
			throw new Exception("未找到订单信息，请刷新页面重试！");
		}
	}

	/**
	 * Upd order.
	 * 修改订单
	 *
	 * @param params the params
	 */
	public void updOrder(Map<String, Object> params) throws ServiceException{
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String orderId = String.valueOf(params.get("orderId"));
		BuyOrder bo = orderMapper.selectByPrimaryKey(orderId);
		int totalNum = 0;
		BigDecimal totalMoney = new BigDecimal(0.00);
		//要删除的明细
		List<String> delList = com.alibaba.fastjson.JSONArray.parseArray(String.valueOf(params.get("delArray")),String.class);
		//要更新的明细
		List<BuyOrderProduct> updList = com.alibaba.fastjson.JSONArray.parseArray(String.valueOf(params.get("updArray")),BuyOrderProduct.class);
		//1.删除明细
		if(!ObjectUtil.isEmpty(delList)&&delList.size()>0){
			for (String id:delList) {
				buyOrderProductMapper.deleteByPrimaryKey(id);
			}
		}
		//2.更新明细
		for (BuyOrderProduct item:updList) {
			BuyOrderProduct updItem = new BuyOrderProduct();
			updItem.setId(item.getId());
			updItem.setGoodsNumber(item.getGoodsNumber());
			updItem.setLockGoodsNumber(item.getGoodsNumber());
			BigDecimal price = ObjectUtil.isEmpty(item.getPrice())?new BigDecimal(0.00):item.getPrice();
			BigDecimal priceSum = price.multiply(new BigDecimal(item.getGoodsNumber()));
			updItem.setPriceSum(priceSum);
			updItem.setRemark(item.getRemark());
			updItem.setPredictArred(item.getPredictArred());
			buyOrderProductMapper.updateByPrimaryKeySelective(updItem);
			totalNum += item.getGoodsNumber();
			totalMoney = totalMoney.add(priceSum);
		}
		//3.更新订单主表
		//String predictArred = String.valueOf(params.get("predictArred"));
		bo.setId(orderId);
		//待接单
		bo.setStatus(Constant.OrderStatus.WAITORDER.getValue());
		//待内部审批
		bo.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());
		bo.setGoodsNum(totalNum);
		bo.setGoodsPrice(totalMoney);
		bo.setSellerRejectReason("");
		/*try {
			bo.setPredictArred(sdf.parse(predictArred));
		} catch (ParseException e) {
			bo.setPredictArred(null);
		}finally {*/
			orderMapper.updateByPrimaryKeySelective(bo);
		//}
		//4.若有审批，则重置审批状态
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(orderId);
		if(!ObjectUtil.isEmpty(header)){
			header.setStatus("0");
			tradeVerifyHeaderMapper.update(header);
			//获得最大index
			int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
			//添加审批
			TradeVerifyPocess pocessUpdate = new TradeVerifyPocess();
			pocessUpdate.setId(ShiroUtils.getUid());
			pocessUpdate.setHeaderId(header.getId());
			pocessUpdate.setUserId(ShiroUtils.getUserId());
			pocessUpdate.setUserName(ShiroUtils.getUserName());
			pocessUpdate.setStatus("7");//买家修改
			pocessUpdate.setRemark("订单修改后再次提交");
			pocessUpdate.setVerifyIndex(maxIndex+1);
			pocessUpdate.setStartDate(new Date());
			pocessUpdate.setEndDate(new Date());
			tradeVerifyPocessMapper.add(pocessUpdate);
			maxIndex+=1;
			int index = maxIndex;
			List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
			//删除未审批的数据
			tradeVerifyPocessMapper.deleteNotVerify(header.getId());
			
			if(!pocessList.isEmpty()){
				Map<String,String> userMap = new HashMap<>();
				for(TradeVerifyPocess pocess : pocessList){
					if("0".endsWith(pocess.getStatus()) 
						|| "1".endsWith(pocess.getStatus()) 
						|| "2".endsWith(pocess.getStatus())){
						if(userMap.containsKey(pocess.getUserId())){
							continue;
						}
						TradeVerifyPocess pocessNew = new TradeVerifyPocess();
						pocessNew.setId(ShiroUtils.getUid());
						pocessNew.setHeaderId(header.getId());
						pocessNew.setUserId(pocess.getUserId());
						pocessNew.setUserName(pocess.getUserName());
						pocessNew.setStatus("0");//等待审批
						pocessNew.setRemark(null);
						pocessNew.setVerifyIndex(index+1);
						pocessNew.setStartDate(new Date());
						pocessNew.setEndDate(new Date());
						tradeVerifyPocessMapper.add(pocessNew);
						userMap.put(pocess.getUserId(), pocess.getUserId());
						if(index == maxIndex){
							//待审批人员发送钉钉消息
							DingDingUtils.verifyDingDingMessage(pocessNew.getUserId(), pocessNew.getHeaderId());
						}
						index++;
					}
				}
			}
		}
	}

	/**
	 * Cancel order.
	 * 取消订单
	 * @param orderId the order id
	 */
	public void cancelOrder(String orderId) {
		//1.设置取消状态
		BuyOrder bo = new BuyOrder();
		bo.setId(orderId);
		bo.setStatus(Constant.OrderStatus.CANCEL.getValue());
		int result = orderMapper.updateByPrimaryKeySelective(bo);
		if(result > 0){
			//修改审批状态
			TradeVerifyHeader verifyHeader = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(orderId);
			verifyHeader.setStatus("3");//撤销
			verifyHeader.setUpdateDate(new Date());
			verifyHeader.setUpdateUserId(ShiroUtils.getUserId());
			verifyHeader.setUpdateUserName(ShiroUtils.getUserName());
			tradeVerifyHeaderMapper.update(verifyHeader);
			if(!ObjectUtil.isEmpty(verifyHeader)){
				//删除所有未审批的数据
				tradeVerifyPocessMapper.deleteNotVerify(verifyHeader.getId());
				//获得最大index
				int maxIndex = tradeVerifyPocessMapper.getMaxIndex(verifyHeader.getId());
				//添加审批
				TradeVerifyPocess pocess = new TradeVerifyPocess();
				pocess.setId(ShiroUtils.getUid());
				pocess.setHeaderId(verifyHeader.getId());
				pocess.setUserId(ShiroUtils.getUserId());
				pocess.setUserName(ShiroUtils.getUserName());
				pocess.setStatus("4");//已撤销
				pocess.setRemark("已撤销");
				pocess.setVerifyIndex(maxIndex+1);
				pocess.setStartDate(new Date());
				pocess.setEndDate(new Date());
				tradeVerifyPocessMapper.add(pocess);
			}
		}
	}

	/**
	 * Stop order.
	 * 终止订单
	 * @param  params
	 */
	public void stopOrder(Map<String,Object> params) {
		String orderId = String.valueOf(params.get("orderId"));
		String stopReason = String.valueOf(params.get("stopReason"));
		//1.设置买家订单状态
		BuyOrder bo = new BuyOrder();
		bo.setId(orderId);
		bo.setStatus(Constant.OrderStatus.STOP.getValue());
		bo.setStopPerson(ShiroUtils.getUserName());
		bo.setStopDate(new Date());
		bo.setStopReason(stopReason);
		orderMapper.updateByPrimaryKeySelective(bo);
		//2.设置卖家订单状态
		SellerOrder so = sellerOrderMapper.getByBuyerOrderId(orderId);
		if(!ObjectUtil.isEmpty(so)){
			SellerOrder soUpd = new SellerOrder();
			soUpd.setId(so.getId());
			soUpd.setStatus(Constant.SellerOrderStatus.STOP.getValue());
			soUpd.setStopPerson(ShiroUtils.getUserName());
			soUpd.setStopDate(new Date());
			soUpd.setStopReason(stopReason);
			sellerOrderMapper.updateByPrimaryKeySelective(soUpd);
			//3.设置卖家发货表中未发货的状态
			List<Map<String,Object>> sellerDeliveryList = bSellerDeliveryRecordItemMapper.getItemByOrderId(so.getId());
			if (!ObjectUtil.isEmpty(sellerDeliveryList)){
				for (Map<String,Object> sellerDeliveryMap : sellerDeliveryList){
					SellerDeliveryRecord sellerDeliveryRecord = new SellerDeliveryRecord();
					sellerDeliveryRecord.setId(sellerDeliveryMap.get("recordId").toString());
					sellerDeliveryRecord.setRecordstatus(Constant.DeliveryRecordStatus.CANCELED.getValue());
					bSellerDeliveryRecordMapper.updateByPrimaryKeySelective(sellerDeliveryRecord);
				}
			}
		}
	}

	/**
	 * Gets export data.
	 * 获取导出的数据
	 * @param map
	 * @return the export data
	 */
	public List<BuyOrderExportData> getExportData(Map<String,Object> map) {
		map.put("orderKind", map.containsKey("orderKind")?map.get("orderKind"):"0");
		map.put("isDel", Constant.IsDel.NODEL.getValue());
		map.put("companyId",ShiroUtils.getCompId());
		map.put("isCheckNotIn","('2')");
		map.put("statusNotIn","('6','7','8')");
		this.setQueryParamStatus(map);
		List<BuyOrder> orderList = orderMapper.selectByMap(map);
		List<BuyOrderExportData> exportDataList = new ArrayList<>();
		//根据条形码来组装商品信息，条形码一样就合并商品
		if(!orderList.isEmpty()){
			for(BuyOrder order : orderList){
				Map<String,BuyOrderProduct> productMap = new HashMap<>();
				String orderId = order.getId();
				List<BuyOrderProduct> orderProductList = buyOrderProductMapper.selectByOrderId(orderId);
				for (BuyOrderProduct product:orderProductList) {
					if(productMap.containsKey(product.getSkuOid())){
						BuyOrderProduct oldProduct = productMap.get(product.getSkuOid());
						int oldGoodsNum = ObjectUtil.isEmpty(oldProduct.getGoodsNumber())?0:oldProduct.getGoodsNumber();
						int goodsNum = ObjectUtil.isEmpty(product.getGoodsNumber())?0:product.getGoodsNumber();
						int oldArrivalNum = ObjectUtil.isEmpty(oldProduct.getArrivalNum())?0:oldProduct.getArrivalNum();
						int arrivalNum = ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
						BigDecimal oldPriceSum = ObjectUtil.isEmpty(oldProduct.getPriceSum())?new BigDecimal(0):oldProduct.getPriceSum();
						BigDecimal priceSum = ObjectUtil.isEmpty(product.getPriceSum())?new BigDecimal(0):product.getPriceSum();
						product.setGoodsNumber(oldGoodsNum+goodsNum);
						product.setArrivalNum(oldArrivalNum+arrivalNum);
						product.setUnArrivalNum(oldGoodsNum+goodsNum-oldArrivalNum-arrivalNum);
						product.setPriceSum(oldPriceSum.add(priceSum));
					}else {
						product.setGoodsNumber(ObjectUtil.isEmpty(product.getGoodsNumber())?0:product.getGoodsNumber());
						product.setArrivalNum(ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum());
						product.setUnArrivalNum(product.getGoodsNumber()-product.getArrivalNum());
					}
					productMap.put(product.getSkuOid(),product);
				}
				for (Map.Entry<String,BuyOrderProduct> entry : productMap.entrySet()){
					BuyOrderProduct product = entry.getValue();
					//这里组装订单明细的发货信息
					Map<String,Object> deliveryMap = new HashMap<>(2);
					deliveryMap.put("orderId",orderId);
					deliveryMap.put("barcode",product.getSkuOid());
					List<BuyDeliveryRecordItem> deliveryRecordItemList = buyDeliveryRecordItemMapper.queryList(deliveryMap);
					if(!ObjectUtil.isEmpty(deliveryRecordItemList)&&deliveryRecordItemList.size()>0){
						Map<String,BuyDeliveryRecordItem> deliveryRecordItemMap = new HashMap<>();
						for (BuyDeliveryRecordItem deliveryItem:deliveryRecordItemList) {
							if(deliveryRecordItemMap.containsKey(deliveryItem.getDeliverNo())){
								BuyDeliveryRecordItem oldItem = deliveryRecordItemMap.get(deliveryItem.getDeliverNo());
								int oldDeliveryNum = ObjectUtil.isEmpty(oldItem.getDeliveryNum())?0:oldItem.getDeliveryNum();
								int oldArrivalNum = ObjectUtil.isEmpty(oldItem.getArrivalNum())?0:oldItem.getArrivalNum();
								int newDeliveryNum = ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum();
								int newArrivalNum = ObjectUtil.isEmpty(deliveryItem.getArrivalNum())?0:deliveryItem.getArrivalNum();
								deliveryItem.setDeliveryNum(oldDeliveryNum+newDeliveryNum);
								deliveryItem.setArrivalNum(oldArrivalNum+newArrivalNum);

							}else {
								deliveryItem.setDeliveryNum(ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum());
								deliveryItem.setArrivalNum(ObjectUtil.isEmpty(deliveryItem.getArrivalNum())?0:deliveryItem.getArrivalNum());
							}
							deliveryRecordItemMap.put(deliveryItem.getDeliverNo(),deliveryItem);
						}
						String deliveryStr = "";
						int deliveryNumSum = 0;
						int arrivalNumSum = 0;
						Date arrivalDate = null;
						for(Map.Entry<String,BuyDeliveryRecordItem> entryDelivery : deliveryRecordItemMap.entrySet()){
							BuyDeliveryRecordItem deliveryRecordItem = entryDelivery.getValue();
							//发货单号
							String deliveryCode = deliveryRecordItem.getDeliverNo();
							//发货数量
							int deliveryNum = deliveryRecordItem.getDeliveryNum();
							//到货数量
							int arrivalNum = ObjectUtil.isEmpty(deliveryRecordItem.getArrivalNum())?0:deliveryRecordItem.getArrivalNum();
							deliveryStr += "发货单号:"+ deliveryCode+",发货数量:"+deliveryNum+",";
							if(deliveryRecordItem.getArrivalDate() == null){
								deliveryStr += "到货数量:未到货";
							}else{
								deliveryStr += "到货数量:"+arrivalNum+",到货日期:"+DateUtils.format(deliveryRecordItem.getArrivalDate())+";";
								//到货日期
								if(arrivalDate != null){
									if(arrivalDate.getTime()<deliveryRecordItem.getArrivalDate().getTime()){
										arrivalDate = deliveryRecordItem.getArrivalDate();
									}
								}else{
									arrivalDate = deliveryRecordItem.getArrivalDate();
								}
							}
							deliveryStr += "\n";
							deliveryNumSum+=deliveryNum;
							arrivalNumSum+=arrivalNum;
						}
						BuyOrderExportData exportData = new BuyOrderExportData();
						exportData.setOrderId(orderId);
						exportData.setOrderCode(order.getOrderCode());
						exportData.setOrderDate(order.getCreateDate());
						exportData.setOrderStatus(String.valueOf(order.getStatus()));
						exportData.setProductCode(product.getProCode());
						exportData.setProductName(product.getProName());
						exportData.setSkuCode(product.getSkuCode());
						exportData.setSkuName(product.getSkuName());
						exportData.setBarcode(product.getSkuOid());
						exportData.setOrderNum(product.getGoodsNumber());
						exportData.setUnArrivalNum(product.getUnArrivalNum());
						exportData.setPrice(product.getPrice());
						exportData.setTotalPrice(product.getPriceSum());
						exportData.setReqArrivalDate(product.getPredictArred());
						exportData.setOrderCreator(order.getCreateName());
						exportData.setWarehouseName(product.getWareHouseName());
						exportData.setOrderRemark(order.getRemark());
						exportData.setDeliverNo(deliveryStr);
						exportData.setDeliveryNum(deliveryNumSum);
						exportData.setArrivalNum(arrivalNumSum);
						exportData.setSupplierName(order.getSuppName());
						exportData.setArrivalDate(arrivalDate);
						exportData.setOrderItemRemark(product.getRemark());
						exportDataList.add(exportData);
					}else {
						//没有发货记录，则只组装商品信息
						BuyOrderExportData exportData = new BuyOrderExportData();
						exportData.setOrderId(orderId);
						exportData.setOrderCode(order.getOrderCode());
						exportData.setOrderDate(order.getCreateDate());
						exportData.setOrderStatus(String.valueOf(order.getStatus()));
						exportData.setProductCode(product.getProCode());
						exportData.setProductName(product.getProName());
						exportData.setSkuCode(product.getSkuCode());
						exportData.setSkuName(product.getSkuName());
						exportData.setBarcode(product.getSkuOid());
						exportData.setOrderNum(product.getGoodsNumber());
						exportData.setUnArrivalNum(product.getGoodsNumber());
						exportData.setPrice(product.getPrice());
						exportData.setTotalPrice(product.getPriceSum());
						exportData.setReqArrivalDate(product.getPredictArred());
						exportData.setSupplierName(order.getSuppName());
						exportData.setOrderCreator(order.getCreateName());
						exportData.setWarehouseName(product.getWareHouseName());
						exportData.setOrderRemark(order.getRemark());
						exportData.setOrderItemRemark(product.getRemark());
						exportDataList.add(exportData);
					}
				}
			}
		}
		return exportDataList;
    }


	public List<BuyOrderExportData> getNotArrivalExportData(Map<String, Object> map) {
		List<BuyOrderExportData> exportDataList = new ArrayList<>();
		map.put("orderKind", map.containsKey("orderKind")?map.get("orderKind"):"0");
		map.put("isDel", Constant.IsDel.NODEL.getValue());
		map.put("companyId",ShiroUtils.getCompId());
		setQueryParamStatus(map);
		List<BuyOrderProduct> orderItemList = buyOrderProductMapper.getNotArrivalExportData(map);
		for (BuyOrderProduct item:orderItemList){
			BuyOrderExportData exportData = new BuyOrderExportData();
			exportData.setOrderId(item.getOrderId());
			exportData.setOrderCode(item.getOrderCode());
			exportData.setOrderDate(item.getOrderDate());
			exportData.setOrderStatus(String.valueOf(item.getOrderStatus()));
			exportData.setProductCode(item.getProCode());
			exportData.setProductName(item.getProName());
			exportData.setSkuCode(item.getSkuCode());
			exportData.setSkuName(item.getSkuName());
			exportData.setBarcode(item.getSkuOid());
			exportData.setOrderNum(item.getGoodsNumber());
			exportData.setArrivalNum(ObjectUtil.isEmpty(item.getArrivalNum())?0:item.getArrivalNum());
			exportData.setUnArrivalNum(ObjectUtil.isEmpty(item.getUnArrivalNum())?0:item.getUnArrivalNum());
			exportData.setUnArrivalTotalPrice(item.getPrice().multiply(new BigDecimal(ObjectUtil.isEmpty(item.getUnArrivalNum())?0:item.getUnArrivalNum())));
			exportData.setPrice(item.getPrice());
			exportData.setTotalPrice(item.getPriceSum());
			exportData.setReqArrivalDate(item.getPredictArred());
			exportData.setSupplierName(item.getSuppName());
			exportData.setOrderCreator(item.getCreateName());
			exportData.setWarehouseName(item.getWareHouseName());
			exportData.setOrderItemRemark(item.getRemark());
			exportData.setOrderRemark(item.getOrderRemark());
			exportDataList.add(exportData);
		}
		return exportDataList;
	}
	
	/**
	 * 加载供应商使用率
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getSupplierUsageRate(Map<String, Object> map) {
		return orderMapper.getSupplierUsageRate(map);
	}
	
	/**
	 * 待我审批的采购单
	 * @param searchPageUtil
	 * @param params
	 */
	public void loadApprovedData(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		params1.put("siteId", "17111514041191591091");
		//获得所有未审批的id
		List<String> orderIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> orderIdListNew = new ArrayList<>();
		//
		String procode = params.containsKey("procode")?params.get("procode")!=null?params.get("procode").toString():"":"";
		String proname = params.containsKey("proname")?params.get("proname")!=null?params.get("proname").toString():"":"";
		String skucode = params.containsKey("skucode")?params.get("skucode")!=null?params.get("skucode").toString():"":"";
		String skuname = params.containsKey("skuname")?params.get("skuname")!=null?params.get("skuname").toString():"":"";
		String skuoid = params.containsKey("skuoid")?params.get("skuoid")!=null?params.get("skuoid").toString():"":"";
		if(!StringUtils.isEmpty(procode)
			||!StringUtils.isEmpty(proname)
			||!StringUtils.isEmpty(skucode)
			||!StringUtils.isEmpty(skuname)
			||!StringUtils.isEmpty(skuoid)
		){
			List<String> orderIdList2 = orderMapper.selectApprovedId(params);
			if(!orderIdList2.isEmpty()){
				Map<String, String> orderIdMap = new HashMap<>();
				for(String orderId : orderIdList2){
					orderIdMap.put(orderId, orderId);
				}
				if(!orderIdList.isEmpty()){
					for(String orderId : orderIdList){
						if(orderIdMap.containsKey(orderId)){
							orderIdListNew.add(orderId);
						}
					}
				}
			}
		}else{
			orderIdListNew = orderIdList;
		}
		params.put("approved", true);
		params.put("orderIdList", orderIdListNew);
		searchPageUtil.setObject(params);
		List<BuyOrder> orderList = orderMapper.selectApprovedOrderByPage(searchPageUtil);
		if(!orderList.isEmpty()){
			for(BuyOrder order:orderList){
				Map<String,BuyOrderProduct> productMap = new HashMap<>();
				String orderId = order.getId();
				List<BuyOrderProduct> orderProductList = buyOrderProductMapper.selectByOrderId(orderId);
				for (BuyOrderProduct product:orderProductList) {
					if(productMap.containsKey(product.getSkuOid())){
						BuyOrderProduct oldProduct = productMap.get(product.getSkuOid());
						int oldGoodsNum = ObjectUtil.isEmpty(oldProduct.getGoodsNumber())?0:oldProduct.getGoodsNumber();
						int goodsNum = ObjectUtil.isEmpty(product.getGoodsNumber())?0:product.getGoodsNumber();
						int oldArrivalNum = ObjectUtil.isEmpty(oldProduct.getArrivalNum())?0:oldProduct.getArrivalNum();
						int arrvalNum = ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
						product.setGoodsNumber(oldGoodsNum+goodsNum);
						product.setArrivalNum(oldArrivalNum+arrvalNum);
					}
					productMap.put(product.getSkuOid(),product);
				}
				List<BuyOrderProduct> addList = new ArrayList<>(productMap.size());
				for (Map.Entry<String,BuyOrderProduct> entry : productMap.entrySet()){
					addList.add(entry.getValue());
				}
				order.setOrderProductList(addList);
			}
		}
		searchPageUtil.getPage().setList(orderList);
	}

	/**
	 * 一键通过
	 * @param id
	 * @return
	 */
	public JSONObject verifyAll(String id) {
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(id)){
			String[] idStr = id.split(",");
			for(String orderId : idStr){
				verifyApproved(orderId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json;
	}
	
	/**
	 * 审批
	 * @param orderId
	 */
	public void verifyApproved(String orderId){
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(orderId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						verifySuccess(orderId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
	}

	public List<Double> getLastPrice(String barcode, String supplierId) {
		return orderMapper.getLastPrice(barcode,supplierId);
	}
	
	/**
	 * 卖家-转化采购单保存
	 *
	 * @param params the params
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<String> saveChangePurchaseOrder(Map<String, Object> params) throws Exception{
		List<String> idList = new ArrayList<String>();
		String buyOrderId = params.get("buyOrderId") == null ? "" : params.get("buyOrderId").toString();
		SellerOrder sOrder = sellerOrderMapper.getByBuyerOrderId(buyOrderId);
		String infoStr = params.get("infoStr") == null ? "" : params.get("infoStr").toString();
		String[] s = infoStr.split("@");
		for(int i = 0;i < s.length;i++){
			String[] orderStr = s[i].split(",");
			BuyOrder order = new BuyOrder();
			order.setId(ShiroUtils.getUid());
			order.setOrderCode(ShiroUtils.getOrderNum());
			order.setSuppId(orderStr[0]);
			order.setSuppName(orderStr[1]);
			order.setPerson(orderStr[2]);
			order.setPhone(orderStr[3]);
			order.setGoodsNum(Integer.parseInt(orderStr[4]));
			order.setGoodsPrice(new BigDecimal(orderStr[5]));
			order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
			order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
			order.setOrderKind(Constant.OrderKind.MT.getValue());//类型  转化订单
			order.setRemark(orderStr[8]);
			order.setIsDel(Constant.IsDel.NODEL.getValue());
			order.setCreateDate(new Date());
			order.setCreateId(ShiroUtils.getUserId());
			order.setCreateName(ShiroUtils.getUserName());
			order.setCompanyId(Constant.COMPANY_ID);
			order.setOrderType("0");
			order.setSellerChangeId(sOrder.getId());//卖家转化订单id
			//收货地址
			int isSince = Integer.parseInt(orderStr[6]);
			order.setIsSince(isSince);
			if(Constant.LogisticsType.logistis.getValue()== isSince){
				//需要物流
				BuyAddress address = buyAddressService.selectByPrimaryKey(orderStr[7]);
				order.setPersonName(address.getPersonName());//收货人
				order.setProvince(address.getProvince());//省
				order.setCity(address.getCity());//市
				order.setArea(address.getArea());//区
				order.setAddrName(address.getAddrName());//详细地址
				order.setAreaCode(address.getZone());//区号
				order.setPlaneNumber(address.getTelNo());//座机号
				order.setReceiptPhone(address.getPhone());//手机号
			}
			String item = s[i].substring(s[i].indexOf("{"),s[i].length());
			net.sf.json.JSONObject jsonItem = net.sf.json.JSONObject.fromObject(item);
		    JSONArray productArray = jsonItem.getJSONArray("productList");
		    for(int j = 0 ; j < productArray.size(); j++){
		    	net.sf.json.JSONObject jsonShop = productArray.getJSONObject(j);
		    	BuyOrderProduct product = new BuyOrderProduct();
				product.setId(ShiroUtils.getUid());
				product.setOrderId(order.getId());
				product.setSkuOid(jsonShop.get("skuOid").toString());
				product.setProCode(jsonShop.get("productCode").toString());
				product.setProName(jsonShop.get("productName").toString());
				product.setSkuCode(jsonShop.get("skuCode").toString());
				product.setSkuName(jsonShop.get("skuName").toString());
				product.setUnitId(jsonShop.get("unitId").toString());
				product.setGoodsNumber(Integer.parseInt(jsonShop.get("num").toString()));
				product.setLockGoodsNumber(product.getGoodsNumber());//锁定数量
				product.setPrice(new BigDecimal(jsonShop.get("price").toString()));
				product.setPriceSum(new BigDecimal(jsonShop.get("priceSum").toString()));
				product.setWareHouseId(jsonShop.get("warehouseId").toString());
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
				product.setPredictArred(sf.parse(jsonShop.get("predictArred").toString()));
				product.setRemark(jsonShop.get("remark").toString());
				buyOrderProductMapper.insertSelective(product);
		    }
		    orderMapper.insertOrder(order);
		    idList.add(order.getId());
		}
		sOrder.setIsChange(1);//已转化
		sellerOrderMapper.updateByPrimaryKeySelective(sOrder);
		return idList;
	}
	
	//修改对账状态
	public void changeStatus(String orderId,String stutus) {
		BuyOrder order = new BuyOrder();
		order.setId(orderId);
		order.setReconciliationStatus(stutus);//是否已对账（0：未对账， 1：正在对帐，2：对账完成，3：对账驳回，4：部分对账)
		orderMapper.updateByPrimaryKeySelective(order);
		SellerOrder sellerOrder = sellerOrderMapper.getByBuyerOrderId(orderId);
		sellerOrder.setBuyerOrderId(orderId);
		sellerOrder.setReconciliationStatus(stutus);
		sellerOrderMapper.updateByPrimaryKeySelective(sellerOrder);
	}

	//修改付款状态
	public void changePaymentStatus(String orderId,String stutus){
		BuyOrder order = new BuyOrder();
		order.setId(orderId);
		order.setIsPaymentStatus(stutus);//是否已付款（0：待付款， 1：付款待卖家确认，2：已付款，3部分付款,4.付款审批中，5.内部审批拒绝）
		orderMapper.updateByPrimaryKeySelective(order);
		SellerOrder sellerOrder = sellerOrderMapper.getByBuyerOrderId(orderId);
		sellerOrder.setBuyerOrderId(orderId);
		sellerOrder.setIsPaymentStatus(stutus);
		sellerOrderMapper.updateByPrimaryKeySelective(sellerOrder);
	}

}
