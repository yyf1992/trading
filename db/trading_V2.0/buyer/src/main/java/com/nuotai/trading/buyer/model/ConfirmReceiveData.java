package com.nuotai.trading.buyer.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author liuhui
 * @date 2017-10-19 9:13
 * @description 确认收货数据展示
 *
 **/
@Data
public class ConfirmReceiveData implements Serializable {
    private static final long serialVersionUID = 1L;

    //卖家订单ID
    private String sellerOrderId;
    //买家订单ID
    private String buyerOrderId;
    //订单号
    private String orderCode;
    //订单日期
    private Date orderDate;
    //订单类型 0:采购订单 1:其他订单
    private String orderType;
    //供货商id
    private String supplierId;
    //供货商名称
    private String supplierName;
    //采购商公司ID
    private String buycompanyId;
    //采购商公司Name
    private String buycompanyName;
    //发货单号
    private String deliverNo;
    //发货日期
    private Date deliverDate;
    //收货明细
    List<BuyDeliveryRecordItem> itemList;
}
