package com.nuotai.trading.buyer.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jetty.util.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyCustomerItemMapper;
import com.nuotai.trading.buyer.dao.BuyCustomerMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerCustomerItemMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerCustomerMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordMapper;
import com.nuotai.trading.buyer.model.BuyCustomExportData;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyCustomerItem;
import com.nuotai.trading.buyer.model.seller.BSellerCustomer;
import com.nuotai.trading.buyer.model.seller.BSellerCustomerItem;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.seller.service.SyncWmsService;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ElFunction;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.page.PageModel;
import com.oms.client.OMSResponse;



@Service
@Transactional
@SuppressWarnings("unchecked")
public class BuyCustomerService {

    private static final Logger log = LoggerFactory.getLogger(BuyCustomerService.class);

	@Autowired
	private BuyCustomerMapper buyCustomerMapper;
	@Autowired
	private BuyCustomerItemMapper buyCustomerItemMapper;
	@Autowired
	private BuyAddressService buyAddressService;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	@Autowired
	private BSellerCustomerMapper sellerCustomerMapper;
	@Autowired
	private BSellerCustomerItemMapper sellerCustomerItemMapper;
	@Autowired
	private BuyOrderService orderService;
	@Autowired
	private BuySupplierFriendService supplierFriendService;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private BSellerDeliveryRecordMapper bSellerDeliveryRecordMapper;
	@Autowired
	private BSellerDeliveryRecordItemMapper bSellerDeliveryRecordItemMapper;
	
	public BuyCustomer get(String id){
		return buyCustomerMapper.get(id);
	}
	
	public List<BuyCustomer> queryList(Map<String, Object> map){
		return buyCustomerMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyCustomerMapper.queryCount(map);
	}
	
	public void add(BuyCustomer buyCustomer){
		buyCustomerMapper.add(buyCustomer);
	}
	
	public void update(BuyCustomer buyCustomer){
		buyCustomerMapper.update(buyCustomer);
	}
	
	public void delete(String id){
		buyCustomerMapper.delete(id);
	}
	
	/**
	 * 创建售后单保存
	 * @param mapParem
	 * @throws Exception
	 */
	public List<String> saveAddCustomer(Map<String,Object> mapParem) throws Exception{
		List<String> idList = new ArrayList<String>();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		BuyCustomer customer = new BuyCustomer();
		String[] goodsStr = mapParem.get("goodsStr").toString().split("@");
		Map<String, String> map = orderService.parseToMap(goodsStr);
		for(Map.Entry<String, String> entry : map.entrySet()){
			log.info("Key = "+entry.getKey()+",value="+entry.getValue());
			String value = entry.getValue().substring(0,entry.getValue().length() - 1);
		    String[] supplier = value.split(",");
			customer.setId(ShiroUtils.getUid());
			customer.setCustomerCode(ShiroUtils.getCustomerNum());
			customer.setCompanyId(ShiroUtils.getCompId());
			customer.setSupplierId(entry.getKey());
			customer.setSupplierName(supplier[0]);
			Map<String, Object> mapSF = new HashMap<String, Object>();
			mapSF.put("sellerId",entry.getKey());
			List<BuySupplierFriend> sfList = supplierFriendService.selectByMap(mapSF);
			if(sfList != null && sfList.size() > 0){
				BuySupplierFriend sf = sfList.get(0);
				customer.setPerson(sf.getSellerPerson());
				customer.setPhone(sf.getSellerPhone());
			}
			customer.setIsDel(Constant.IsDel.NODEL.getValue()+"");
			customer.setStatus(Constant.CustomerStatus.WAITVERIFY.getValue()+"");
			customer.setReason(mapParem.get("reason").toString());
			customer.setProof(mapParem.get("proof").toString());
			customer.setTitle(mapParem.get("title").toString());//售后标题
			customer.setWarehouseId(mapParem.get("warehouseId").toString());//出库仓库
			//收货地址
			String addressId = mapParem.get("addressId") == null ? "" : mapParem.get("addressId").toString();
			if(addressId != ""){
				BuyAddress address = buyAddressService.selectByPrimaryKey(addressId);
				customer.setAddressId(addressId);
				customer.setProvince(address.getProvince());//省
				customer.setCity(address.getCity());//市
				customer.setArea(address.getArea());//区
				customer.setAddrName(address.getAddrName());//详细地址
				customer.setLinkman(address.getPersonName());//收货人
				customer.setZone(address.getZone());//区号
				customer.setTelNo(address.getTelNo());//座机号
				customer.setLinkmanPhone(address.getPhone());//手机号
			}
			customer.setCreateId(user.getId());
			customer.setCreateName(user.getUserName());
			customer.setCreateDate(new Date());
			
			BuyCustomerItem customerItem = new BuyCustomerItem();
			String[] item = value.split("@");
			for(int i = 0;i < item.length;i++){
				String[] product = item[i].split(",");
				customerItem.setId(ShiroUtils.getUid());
				customerItem.setCustomerId(customer.getId());
				//supplierId + "," + supplierName + "," + productCode + "," + productName + "," + skuCode + "," + skuName + "," + skuOid
				//+ "," + number + "," + price + "," + warehouseId + "," + repairPrice + "," + type + "," + exchangeMoney  + "@";
				customerItem.setProductCode(product[1]);
				customerItem.setProductName(product[2]);
				customerItem.setSkuCode(product[3]);
				customerItem.setSkuName(product[4]);
				customerItem.setSkuOid(product[5]);
				customerItem.setGoodsNumber(Integer.parseInt(product[6]));//售后数量
				customerItem.setPrice(new BigDecimal(product[7]));
				customerItem.setWareHouseId(product[8]);
				customerItem.setRepairPrice(new BigDecimal(product[9]));
				customerItem.setType(product[10]);//售后类型  0-换货，1-退款退货
				customerItem.setExchangePrice(new BigDecimal(product[11]));
				customerItem.setTransformType("0");//转换类型[0-未转换, 1-换货转退货]
				customerItem.setIsNeedInvoice("");
				buyCustomerItemMapper.add(customerItem);
			}
			buyCustomerMapper.add(customer);
			idList.add(customer.getId());
		}
		return idList;
	}
	
	/**
	 * 售后审核通过
	 * @param id
	 * @return
	 */
	public JSONObject verifySuccess(String id) {
		//更新售后表的审核状态
		JSONObject json = new JSONObject();
		BuyCustomer bCustomer = buyCustomerMapper.get(id);
		bCustomer.setId(id);
		bCustomer.setStatus(Constant.CustomerStatus.VERIFYPASS.getValue()+"");
		int result = buyCustomerMapper.update(bCustomer);
		if(result>0){
			// 同步添加到卖家表中
			saveAddSellerCustomer(bCustomer);
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");			
		}
		return json;
	}
	
	/**
	 * 售后审核驳回
	 * @param id
	 * @return
	 */
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		BuyCustomer bCustomer = buyCustomerMapper.get(id);
		bCustomer.setStatus(Constant.CustomerStatus.VERIFYNOPASS.getValue()+"");
		int result = buyCustomerMapper.update(bCustomer);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
	
	private void saveAddSellerCustomer(BuyCustomer customer) {
		BuyCompany buyCompany = buyCompanyMapper.selectByPrimaryKey(customer.getCompanyId());
		//保存主表
		BSellerCustomer sCustomer = sellerCustomerMapper.getByBuyCustomerId(customer.getId());
		if(!ObjectUtil.isEmpty(sCustomer)){
			sellerCustomerMapper.delete(sCustomer.getId());
			sellerCustomerItemMapper.deleteByCustomerId(sCustomer.getId());
		}
		sCustomer = new BSellerCustomer();
		sCustomer.setId(ShiroUtils.getUid());
		sCustomer.setCustomerCode(customer.getCustomerCode());
		sCustomer.setSupplierId(customer.getSupplierId());
		sCustomer.setSupplierName(customer.getSupplierName());
		sCustomer.setPerson(customer.getPerson());
		sCustomer.setPhone(customer.getPhone());
		sCustomer.setIsDel(customer.getIsDel());
		sCustomer.setStatus("0");
		sCustomer.setReason(customer.getReason());
		sCustomer.setProof(customer.getProof());
		sCustomer.setCompanyId(ObjectUtil.isEmpty(buyCompany)?"":buyCompany.getId());
		sCustomer.setBuyCustomerId(customer.getId());
		sCustomer.setAddressId(customer.getAddressId());
		sCustomer.setProvince(customer.getProvince());//省
		sCustomer.setCity(customer.getCity());//市
		sCustomer.setArea(customer.getArea());//区
		sCustomer.setAddrName(customer.getAddrName());//详细地址
		sCustomer.setLinkman(customer.getLinkman());//收货人
		sCustomer.setZone(customer.getZone());//区号
		sCustomer.setTelNo(customer.getTelNo());//座机号
		sCustomer.setLinkmanPhone(customer.getLinkmanPhone());//手机号
		sCustomer.setCreateId(customer.getCreateId());
		sCustomer.setCreateName(customer.getCreateName());
		sCustomer.setCreateDate(new Date());
		sCustomer.setTitle(customer.getTitle());//售后标题
		sCustomer.setWarehouseId(customer.getWarehouseId());//出库仓库
		sellerCustomerMapper.add(sCustomer);
		//保存明细表
		List<BuyCustomerItem> buyCustomerItems = buyCustomerItemMapper.selectCustomerItemByCustomerId(customer.getId());
		for (BuyCustomerItem buyCustomerItem : buyCustomerItems) {
			BSellerCustomerItem sellerCustomerItem = new BSellerCustomerItem();
			sellerCustomerItem.setId(ShiroUtils.getUid());
			sellerCustomerItem.setCustomerId(sCustomer.getId());
			sellerCustomerItem.setProductCode(buyCustomerItem.getProductCode());
			sellerCustomerItem.setProductName(buyCustomerItem.getProductName());
			sellerCustomerItem.setSkuCode(buyCustomerItem.getSkuCode());
			sellerCustomerItem.setSkuName(buyCustomerItem.getSkuName());
			sellerCustomerItem.setSkuOid(buyCustomerItem.getSkuOid());
			sellerCustomerItem.setUnitId(buyCustomerItem.getUnitId());
			sellerCustomerItem.setDeliveryCode(buyCustomerItem.getDeliveryCode());
			sellerCustomerItem.setGoodsNumber(buyCustomerItem.getGoodsNumber());
			sellerCustomerItem.setPrice(buyCustomerItem.getPrice());
			sellerCustomerItem.setReceiveNumber(buyCustomerItem.getReceiveNumber());
			sellerCustomerItem.setDeliveryItemId(buyCustomerItem.getDeliveryItemId());
			sellerCustomerItem.setWareHouseId(buyCustomerItem.getWareHouseId());
			sellerCustomerItem.setRemark(buyCustomerItem.getRemark());
			sellerCustomerItem.setIsNeedInvoice(buyCustomerItem.getIsNeedInvoice());
			sellerCustomerItem.setApplyCode(buyCustomerItem.getApplyCode());
			sellerCustomerItem.setExchangePrice(buyCustomerItem.getExchangePrice());
			sellerCustomerItem.setRepairPrice(buyCustomerItem.getRepairPrice());
			sellerCustomerItem.setBuyCustomerItemId(buyCustomerItem.getId());
			sellerCustomerItem.setType(buyCustomerItem.getType());
			sellerCustomerItemMapper.add(sellerCustomerItem);
		}
	}
	
	/**
	 * 互通售后服务管理列表
	 * @param searchPageUtil
	 * @return
	 */
	public List<BuyCustomer> queryCustomerList(SearchPageUtil searchPageUtil) {
		List<BuyCustomer> customerList = buyCustomerMapper.queryCustomerList(searchPageUtil);
		if(customerList != null && customerList.size() > 0){
			for(BuyCustomer customer : customerList){
				BigDecimal sumPrice = new BigDecimal("0.0000");
				List<BuyCustomerItem> itemList = buyCustomerItemMapper.selectCustomerItemByCustomerId(customer.getId());
				if(itemList != null && itemList.size() > 0){
					customer.setItemList(itemList);
					for(BuyCustomerItem item : itemList){
						sumPrice = sumPrice.add(item.getPrice().multiply(new BigDecimal(item.getGoodsNumber())));
						//0-换货，1-退货，2-换货转退货
						if(!"1".equals(item.getType())){
							//换货数据
							int appNum = item.getConfirmNumber()==null?item.getGoodsNumber():item.getConfirmNumber();
							int deliveryNum = 0;
							//获得发货数据
							List<Map<String,Object>> deliveryList = buyCustomerItemMapper.getDeliveryByItemIdList(item.getId());
							if(deliveryList != null && !deliveryList.isEmpty()){
								for(Map<String,Object> delivery : deliveryList){
									deliveryNum += delivery.get("arrival_num") == null?(Integer)delivery.get("delivery_num"):(Integer)delivery.get("arrival_num");
								}
							}
							item.setDeliveryNum(deliveryNum);
							if("0".equals(item.getType())){
								if(appNum > deliveryNum){
									//还有换货没有售后完成
									item.setUpdateTypeFlag(true);
								}else{
									item.setUpdateTypeFlag(false);
								}
							}else{
								item.setUpdateTypeFlag(false);
							}
							item.setDeliveryList(deliveryList);
						}else{
							item.setDeliveryList(null);
							item.setDeliveryNum(0);
							item.setUpdateTypeFlag(false);
						}
					}
					customer.setSumPrice(sumPrice);
				}
			}
		}
		return customerList;
	}
	
	/**
	 * 售后导出数据
	 * @param searchPageUtil
	 * @return
	 */
	public List<BuyCustomer> exportCustomerList(Map<String,Object>params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		if(tabId==0){
			//待内部审核
			params.put("status", Constant.CustomerStatus.WAITVERIFY.getValue()+"");
		}else if(tabId==1){
			//待对方同意
			params.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		}else if(tabId==2){
			//已驳回
			params.put("status", Constant.CustomerStatus.VERIFYNOPASS.getValue());
		}else if(tabId==3){
			//已取消
			params.put("status", Constant.CustomerStatus.CANCEL.getValue());
		}else if(tabId==4){
			//对方已确认
			params.put("status", Constant.CustomerStatus.ALREADYCONFIRM.getValue());
		}else if(tabId==5){
			//待对方确认
			params.put("status",5);
		}
		List<BuyCustomer> customerList = buyCustomerMapper.queryCustomerListByStatistic(params);
		if(customerList != null && customerList.size() > 0){
			for(BuyCustomer customer : customerList){
				BigDecimal sumPrice = new BigDecimal("0.0000");
				List<BuyCustomerItem> itemList = buyCustomerItemMapper.selectCustomerItemByCustomerId(customer.getId());
				if(itemList != null && itemList.size() > 0){
					customer.setItemList(itemList);
					for(BuyCustomerItem item : itemList){
						sumPrice = sumPrice.add(item.getPrice().multiply(new BigDecimal(item.getGoodsNumber())));
						//0-换货，1-退货，2-换货转退货
						if(!"1".equals(item.getType())){
							//换货数据
							int appNum = item.getConfirmNumber()==null?item.getGoodsNumber():item.getConfirmNumber();
							int deliveryNum = 0;
							//获得发货数据
							List<Map<String,Object>> deliveryList = buyCustomerItemMapper.getDeliveryByItemIdList(item.getId());
							if(deliveryList != null && !deliveryList.isEmpty()){
								for(Map<String,Object> delivery : deliveryList){
									deliveryNum += delivery.get("arrival_num") == null?(Integer)delivery.get("delivery_num"):(Integer)delivery.get("arrival_num");
								}
							}
							item.setDeliveryNum(deliveryNum);
							if("0".equals(item.getType())){
								if(appNum > deliveryNum){
									//还有换货没有售后完成
									item.setUpdateTypeFlag(true);
								}else{
									item.setUpdateTypeFlag(false);
								}
							}else{
								item.setUpdateTypeFlag(false);
							}
							item.setDeliveryList(deliveryList);
						}else{
							item.setDeliveryList(null);
							item.setDeliveryNum(0);
							item.setUpdateTypeFlag(false);
						}
					}
					customer.setSumPrice(sumPrice);
				}
			}
		}
		return customerList;
	}
	
	
	public List<BuyCustomExportData> getExportList(Map<String,Object>params){
		List<BuyCustomer> exportDataList = this.exportCustomerList(params);
		List<BuyCustomExportData> exportList=new ArrayList<>();
		for (int i = 0; i < exportDataList.size(); i++) {
			BuyCustomer data = exportDataList.get(i);
			List<BuyCustomerItem> item=data.getItemList();
			if(item != null && item.size()>0){
				for (int j = 0; j < item.size(); j++) {
					BuyCustomerItem buyCustomerItem=item.get(j);
					BuyCustomExportData exportData=new BuyCustomExportData();
					//1.售后单号
					exportData.setCustomerCode(data.getCustomerCode());
					//2.订单日期
					exportData.setCreateDate(data.getCreateDate());
					String arrivalDate = "";
					int daohuoTotal=0;
					int fahuoTotal=0;
					//3.商品货号
					exportData.setProductCode(buyCustomerItem.getProductCode());
					//4.商品名称
					exportData.setProductName(buyCustomerItem.getProductName());
					//5.规格名称
					exportData.setSkuName(buyCustomerItem.getSkuName());
					//6.条形码
					exportData.setSkuOid(buyCustomerItem.getSkuOid());
					//7.售后数量
					exportData.setGoodsNumber(buyCustomerItem.getGoodsNumber());
					//8.oms出库数量
					exportData.setReceiveNumber(ObjectUtil.isEmpty(buyCustomerItem.getReceiveNumber())?0:buyCustomerItem.getReceiveNumber());
					//9.卖家收货数量
					exportData.setConfirmNumber(ObjectUtil.isEmpty(buyCustomerItem.getConfirmNumber())?0:buyCustomerItem.getConfirmNumber());
					//10.售后类型
					if("0".equals(buyCustomerItem.getType())){
						exportData.setType("换货");
					}else if("1".equals(buyCustomerItem.getType())){
						exportData.setType("退货");
					}else if("2".equals(buyCustomerItem.getType())){
						exportData.setType("换货转退货");
					}
					if(buyCustomerItem.getDeliveryList() != null && buyCustomerItem.getDeliveryList().size()>0){
						List<Map<String,Object>> deliveryList=buyCustomerItem.getDeliveryList();
						for (Map<String, Object> map : deliveryList) {
							//13.发货详情
							String recordstatus=map.get("recordstatus").toString();
							String daohuoFlag="";
							if("0".equals(recordstatus)){
								daohuoFlag+="未到货"+",到货日期：未到货";
								arrivalDate += "未到货";
							}else if("1".equals(recordstatus)){
								daohuoTotal+=Integer.parseInt(map.get("arrival_num").toString());
								daohuoFlag+=map.get("arrival_num")+",到货日期："+map.get("arrival_date");
								if(arrivalDate != null && !"".equals(arrivalDate)){
									if(arrivalDate.compareTo(map.get("arrival_date").toString())<0){
										arrivalDate =map.get("arrival_date").toString();
									}
								}else{
									arrivalDate =map.get("arrival_date").toString();
								}
							}
							fahuoTotal+=Integer.parseInt(map.get("delivery_num").toString());
							exportData.setDeliveryDetial("发货单号："+map.get("deliver_no")+",发货数量："+map.get("delivery_num")+",到货数量："+daohuoFlag+";");
						}
					}else{
						exportData.setDeliveryDetial("未发货");
						arrivalDate += "未发货";
					}
				//11.申请原因
				exportData.setReason(data.getReason());
				//12.创建人
				exportData.setCreateName(data.getCreateName());
				//14.发货数量
				exportData.setDeliveryNum(fahuoTotal);
				//15.到货数量
				exportData.setArrivalNum(daohuoTotal);
				//16.供应商
				exportData.setSupplierName(data.getSupplierName());
				//17.最近到货日期
				exportData.setArrivalDate(arrivalDate);
				//18.采购单价
				exportData.setPrice(buyCustomerItem.getPrice());
				//19.单个返修金额
				exportData.setExchangePrice(buyCustomerItem.getExchangePrice());
				//20.售后总金额
				exportData.setRepairPrice(buyCustomerItem.getRepairPrice());
				//21.oms出库日期
				exportData.setOmsFinishdate(data.getOmsFinishdate());
				//22.卖家确认收货日期
				exportData.setSellerConfirmDate(data.getUpdateDate());
				exportList.add(exportData);
				}
			}
		}
		return exportList;	
	}
	
	/**
	 * 售后服务-列表数量
	 * @param params
	 */
	public void getCustomerListNum(Map<String, Object> params) {
		//待审批数量
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("status", Constant.CustomerStatus.WAITVERIFY.getValue());
		int waitVerifyNum = selectCustomerListNumByMap(selectMap);
		//待对方同意
		selectMap.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		int waitAgreeNum = selectCustomerListNumByMap(selectMap);
		//待对方确认
		selectMap.put("status", 5);
		int passNum = selectCustomerListNumByMap(selectMap);
		//已驳回
		selectMap.put("status", Constant.CustomerStatus.VERIFYNOPASS.getValue());
		int rejectNum = selectCustomerListNumByMap(selectMap);
		//已取消
		selectMap.put("status", Constant.CustomerStatus.CANCEL.getValue());
		int cancelNum = selectCustomerListNumByMap(selectMap);
		//对方已确认
		selectMap.put("status", Constant.CustomerStatus.ALREADYCONFIRM.getValue());
		int overNum = selectCustomerListNumByMap(selectMap);
		
		params.put("waitVerifyNum", waitVerifyNum);
		params.put("waitAgreeNum", waitAgreeNum);
		params.put("passNum", passNum);
		params.put("rejectNum", rejectNum);
		params.put("cancelNum", cancelNum);
		params.put("overNum", overNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		switch (tabId) {
		case 99:
			params.put("interest", "99");
			break;
		case 0:
			params.put("interest", "0");
			break;
		case 1:
			params.put("interest", "1");
			break;
		case 2:
			params.put("interest", "2");
			break;
		case 3:
			params.put("interest", "3");
			break;
		case 4:
			params.put("interest", "4");
			break;
		default:
			params.put("interest", "99");
			break;
		}
	}
	
	public int selectCustomerListNumByMap(Map<String, Object> params) {
		return buyCustomerMapper.selectCustomerListNumByMap(params);
	}
	
	/**
	 * 获得售后服务列表数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public String loadCustomerData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		String returnPath = "platform/buyer/customer/";
		switch (tabId) {
		case 99:
			params.put("interest", "99");
			returnPath += "allCustomer";
			break;
		case 0:
			params.put("interest", "0");
			returnPath += "waitVerify";
			break;
		case 1:
			params.put("interest", "1");//待对方同意
			returnPath += "waitAgreeCustomer";
			break;
		case 2:
			params.put("interest", "2");
			returnPath += "rejectCustomer";
			break;
		case 3:
			params.put("interest", "3");
			returnPath += "cancelCustomer";
			break;
		case 4:
			params.put("interest", "4");
			returnPath += "overCustomer";
			break;
		case 5:
			params.put("interest", "5");
			returnPath += "passCustomer";
			break;
		default:
			returnPath += "allCustomer";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"99";
		if("0".equals(interest)){
			//待内部审核
			params.put("status", Constant.CustomerStatus.WAITVERIFY.getValue()+"");
		}else if("1".equals(interest)){
			//待对方同意
			params.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		}else if("2".equals(interest)){
			//已驳回
			params.put("status", Constant.CustomerStatus.VERIFYNOPASS.getValue());
		}else if("3".equals(interest)){
			//已取消
			params.put("status", Constant.CustomerStatus.CANCEL.getValue());
		}else if("4".equals(interest)){
			//对方已确认
			params.put("status", Constant.CustomerStatus.ALREADYCONFIRM.getValue());
		}else if("5".equals(interest)){
		//待对方确认
		params.put("status",5);
		}
		searchPageUtil.setObject(params);
		List<BuyCustomer> customerList = queryCustomerList(searchPageUtil);
		searchPageUtil.getPage().setList(customerList);
		return returnPath;
	}
	
	/**
	 * 取消售后申请保存
	 * @param orderId
	 */
	public void saveCancelCustomer(String orderId) throws Exception{
		BuyCustomer customer = get(orderId);
		if(customer != null){
			if(customer.getStatus().equals(Constant.CustomerStatus.WAITVERIFY.getValue()+"")
					|| customer.getStatus().equals(Constant.CustomerStatus.VERIFYNOPASS.getValue()+"")){
				customer.setStatus(Constant.CustomerStatus.CANCEL.getValue() + "");
				int result=buyCustomerMapper.update(customer);
				if(result>0){
					//修改审批状态
					TradeVerifyHeader verifyHeader = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(orderId);
					verifyHeader.setStatus("3");//撤销
					verifyHeader.setUpdateDate(new Date());
					verifyHeader.setUpdateUserId(ShiroUtils.getUserId());
					verifyHeader.setUpdateUserName(ShiroUtils.getUserName());
					tradeVerifyHeaderMapper.update(verifyHeader);
					if(!ObjectUtil.isEmpty(verifyHeader)){
						//删除所有未审批的数据
						tradeVerifyPocessMapper.deleteNotVerify(verifyHeader.getId());
						//获得最大index
						int maxIndex = tradeVerifyPocessMapper.getMaxIndex(verifyHeader.getId());
						//添加审批
						TradeVerifyPocess pocess = new TradeVerifyPocess();
						pocess.setId(ShiroUtils.getUid());
						pocess.setHeaderId(verifyHeader.getId());
						pocess.setUserId(ShiroUtils.getUserId());
						pocess.setUserName(ShiroUtils.getUserName());
						pocess.setStatus("4");//已撤销
						pocess.setRemark("已撤销");
						pocess.setVerifyIndex(maxIndex+1);
						pocess.setStartDate(new Date());
						pocess.setEndDate(new Date());
						tradeVerifyPocessMapper.add(pocess);
					}
				}
				
			}else{
				throw new Exception("该售后申请已审核通过，不允许取消！");
			}
		}else{
			throw new Exception("未找到申请信息，请刷新页面重试！");
		}
	}
	
	public List<String> saveUpdateCustomer(Map<String,Object> mapParem) throws Exception{
		List<String> idList = new ArrayList<String>();
		SysUser user = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		BuyCustomer customer = buyCustomerMapper.get(mapParem.get("updateId"));
		if(!customer.getStatus().equals("0") && !customer.getStatus().equals("2")){
			throw new Exception("该售后申请已审核，不允许修改！");
		}else{
			//String type = mapParem.get("type") == null ? "" : mapParem.get("type").toString();
			//customer.setType(type);
			customer.setReason(mapParem.get("reason").toString());
			customer.setProof(mapParem.get("proof").toString());
			customer.setStatus(Constant.CustomerStatus.WAITVERIFY.getValue()+"");//待审核
			customer.setTitle(mapParem.get("title").toString());//售后标题
			customer.setWarehouseId(mapParem.get("warehouseId").toString());//出库仓库
			//收货地址
			String addressId = mapParem.get("addressId") == null ? "" : mapParem.get("addressId").toString();
			if(addressId != ""){
				BuyAddress address = buyAddressService.selectByPrimaryKey(addressId);
				customer.setAddressId(addressId);
				customer.setProvince(address.getProvince());//省
				customer.setCity(address.getCity());//市
				customer.setArea(address.getArea());//区
				customer.setAddrName(address.getAddrName());//详细地址
				customer.setLinkman(address.getPersonName());//收货人
				customer.setZone(address.getZone());//区号
				customer.setTelNo(address.getTelNo());//座机号
				customer.setLinkmanPhone(address.getPhone());//手机号
			}
			customer.setUpdateId(user.getId());
			customer.setUpdateName(user.getUserName());
			//customer.setUpdateDate(new Date());
			
			BuyCustomerItem customerItem = new BuyCustomerItem();
			buyCustomerItemMapper.deleteByCustomerId(customer.getId());//删除
			String[] goodsStr = mapParem.get("goodsStr").toString().split("@");
			//goodsStr=NT16Y122,眼保仪,NT16Y122 绿色,NT16Y122 绿色,6928019303001,123,207.0000,49,123,0,
			for (int j = 0; j < goodsStr.length; j++) {
				String s2[] = goodsStr[j].split(",");
				customerItem.setId(ShiroUtils.getUid());
				customerItem.setCustomerId(customer.getId());
				//productCode + "," + productName + "," + skuCode + "," + skuName + "," + skuOid
				//+ "," + number + "," + price + "," + warehouseId + "," + repairPrice + "," + type + + "," + exchangeMoney "@";
				customerItem.setProductCode(s2[0]);
				customerItem.setProductName(s2[1]);
				customerItem.setSkuCode(s2[2]);
				customerItem.setSkuName(s2[3]);
				customerItem.setSkuOid(s2[4]);
				customerItem.setGoodsNumber(Integer.parseInt(s2[5]));
				customerItem.setPrice(new BigDecimal(s2[6]));
				customerItem.setWareHouseId(s2[7]);
				customerItem.setRepairPrice(new BigDecimal(s2[8]));
				customerItem.setType(s2[9]);
				customerItem.setExchangePrice(new BigDecimal(s2[10]));
				buyCustomerItemMapper.add(customerItem);
			}
			buyCustomerMapper.update(customer);
			idList.add(customer.getId());
		}
		return idList;
	}
	

	//根据对账相关条件查询退货数据
	public List<BuyCustomer> queryCustomerListByMap(Map<String,Object> map){
		return buyCustomerMapper.queryCustomerListByMap(map);
	}
	
	public List<BuyCustomer> queryCustomerListByStatistic(Map<String,Object> map){
		return buyCustomerMapper.queryCustomerListByStatistic(map);
	}

	public void getAftermarketProduct(SearchPageUtil searchPageUtil,
			Map<String, Object> params) {
		//取得买卖系统中所有采购到货的商品及最近采购单价
		String notskuoId = params.containsKey("notskuoId")?
				params.get("notskuoId")!=null?params.get("notskuoId").toString():"":"";
		if(!StringUtils.isEmpty(notskuoId)){
			params.put("notskuoIdArray", notskuoId.split(","));
		}else{
			params.put("notskuoIdArray", null);
		}
		params.put("companyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<Map<String, Object>> productList = buyCustomerMapper.getAftermarketProductNew(params);
		Map<String,String> purductMap = new HashMap<>();
		if(productList != null && !productList.isEmpty()){
			for(Map<String, Object> product : productList){
				String barcode = product.get("barcode")==null?"":product.get("barcode").toString();
				String supplierId = product.get("supplier_id")==null?"":product.get("supplier_id").toString();
				if(!StringUtils.isEmpty(barcode)&&!StringUtils.isEmpty(supplierId)){
					//获得最近采购单价
					List<Double> price = orderService.getLastPrice(barcode,supplierId);
					if(price != null && !price.isEmpty()){
						product.put("price", price.get(0));
					}else{
						product.put("price", "-");
					}
				}else{
					product.put("price", "-");
				}
				purductMap.put(barcode+supplierId, barcode+supplierId);
			}
		}else{
			productList = new ArrayList<>();
		}
		
		String url = Constant.PURCHASE_INTERFACE_URL +"getUnshippedList?";
		int i = 0;
		for(Map.Entry<String, Object> purchaseMap : params.entrySet()){
			if(purchaseMap.getValue() == null)continue;
			if("notskuoIdArray".equals(purchaseMap.getKey()))continue;
			if(i==0){
				url += purchaseMap.getKey() + "=" + purchaseMap.getValue();
			}else{
				url += "&" + purchaseMap.getKey() + "=" + purchaseMap.getValue();
			}
			i++;
		}
		String cgArrayString = InterfaceUtil.searchLoginService(url);
		net.sf.json.JSONArray cgProductArray = net.sf.json.JSONArray.fromObject(cgArrayString);
		if(cgProductArray != null && cgProductArray.size() > 0){
			for(int a = 0; a < cgProductArray.size(); a++){
				net.sf.json.JSONObject json = cgProductArray.getJSONObject(a);
				String skuoid = json.getString("skuoid");
				String supplierid = json.getString("supplierid");
				if(!purductMap.containsKey(skuoid+supplierid)){
					Map<String, Object> product = new HashMap<>();
					product.put("product_code", json.getString("procode"));
					product.put("product_name", json.getString("proname"));
					product.put("sku_code", json.getString("skucode"));
					product.put("sku_name", json.getString("skuname"));
					product.put("barcode", json.getString("skuoid"));
					product.put("supplier_id", json.getString("supplierid"));
					product.put("supplier_name", json.getString("suppliername"));
					product.put("price", json.get("price"));
					productList.add(product);
				}
			}
		}
		Page page = searchPageUtil.getPage();
		searchPageUtil.getPage().setCount(productList.size());
		PageModel pm = new PageModel(productList, page.getPageSize());
		List<Map<String, Object>> dataList = pm.getObjects(page.getPageNo());
		searchPageUtil.getPage().setList(dataList);
	}

	/**
	 * 推送oms出库
	 * @param bCustomer
	 * @return
	 */
	public JSONObject pushOms(String id){
		BuyCustomer bCustomer = buyCustomerMapper.get(id);
		JSONObject json = new JSONObject();
		boolean result = false;
		//往oms推出库操作
		Map<String,Object> header = new HashMap<String,Object>();
		header.put("title", bCustomer.getTitle());
		header.put("sourceno", bCustomer.getCustomerCode());
		TBusinessWharea wharea = ElFunction.getBusinessWhareaById(bCustomer.getWarehouseId());
		header.put("whcode", wharea.getWhareaCode());
		header.put("inputdesc", bCustomer.getReason());
		header.put("creater", bCustomer.getCreateName());
		List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
		List<BuyCustomerItem> bCustomerItems = buyCustomerItemMapper.selectCustomerItemByCustomerId(bCustomer.getId());
		if(bCustomerItems != null && bCustomerItems.size() > 0){
			for(BuyCustomerItem bCustomerItem: bCustomerItems){
				Map<String,Object> item = new HashMap<String,Object>();
				item.put("skucode", bCustomerItem.getSkuCode());
				item.put("skuname", bCustomerItem.getSkuName());
				item.put("proname", bCustomerItem.getProductName());
				item.put("procode", bCustomerItem.getProductCode());
				item.put("skuoid", bCustomerItem.getSkuOid());
				item.put("plancount", bCustomerItem.getGoodsNumber());
				itemList.add(item);
			}
			header.put("itemList", itemList);
			try {
				net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(header);
				OMSResponse oMSResponse = InterfaceUtil.NuotaiOmsClient("addOutLibrary", bCustomer.getCustomerCode(), jsonObj);
				result = oMSResponse.isSuccess();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(result){
			bCustomer.setPushType("1");
			buyCustomerMapper.update(bCustomer);
			json.put("success", true);
			json.put("msg", "成功");
		}else{
			json.put("success", false);
			json.put("msg", "失败");
		}
		return json;
	}

	/**
	 * 抓取售后出库数据
	 */
	public void aftermarketRealCount() throws Exception{
		List<BuyCustomer> dhList = buyCustomerMapper.queryListNew();
		if (dhList != null && dhList.size() > 0) {
			String bcCode = "";
			Map<String,BuyCustomer> bcMap = new HashMap<>();
			for(BuyCustomer bc : dhList){
				bcCode += bc.getCustomerCode() + ",";
				bcMap.put(bc.getCustomerCode(), bc);
			}
			System.out.println("未出库售后单号=[" + bcCode+"]");
			JSONArray whstockArray = this.getOutFromWms(bcCode);
			if (whstockArray != null && whstockArray.size() > 0) {
    			// ***********************循环开始*******************************
    			for (int i = 0; i < whstockArray.size(); i++) {
    				// 循环
    				JSONObject logJson = whstockArray.getJSONObject(i);
    				String sourceno = logJson.get("sourceno") == null ? ""
							: logJson.getString("sourceno");
    				Date finishDate = logJson.get("finishdate") == null ? new Date()
						: new Date(logJson.getLong("finishdate"));
    				BuyCustomer bc = bcMap.get(sourceno);
    				bc.setArrivalType("1");
    				bc.setOmsFinishdate(finishDate);
    				buyCustomerMapper.update(bc);
    				BSellerCustomer bsc = sellerCustomerMapper.getByBuyCustomerId(bc.getId());
    				bsc.setArrivalType("1");
    				sellerCustomerMapper.update(bsc);
    				JSONArray itemArray = logJson.getJSONArray("itemsList");
					for (int a = 0; a < itemArray.size(); a++) {
						JSONObject itemJson = itemArray.getJSONObject(a);
						String skuoid = itemJson.get("skuoid") == null ? ""
								: itemJson.getString("skuoid");
						long realCount = itemJson.get("realcount") == null ? 0
								: itemJson.getLong("realcount");
						Date chuKuDate = itemJson.get("created") == null ? new Date()
						: new Date(itemJson.getLong("created"));
						Map<String,Object> itemMap = new HashMap<>();
						itemMap.put("headerId", bc.getId());
						itemMap.put("skuoid", skuoid);
						itemMap.put("realCount", realCount);
						buyCustomerItemMapper.updateRealCount(itemMap);
						itemMap.put("headerId", bsc.getId());
						sellerCustomerItemMapper.updateRealCount(itemMap);
					}
    			}
    		}
		}
	}

	/**
	 * 抓取oms出库
	 * @param bcCode
	 * @return
	 * @throws Exception
	 */
	private JSONArray getOutFromWms(String bcCode) throws Exception {
            System.out.println("**************************从WMS获取出库信息 Begin****************************************");
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String path = Constant.OMS_INTERFACE_URL + "getTradeAftermarket?sourceno=" + bcCode;
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 调用
            InputStream inputStream = conn.getInputStream();
            // 接收
            InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader budr = new BufferedReader(isr);
            String omsResult = budr.readLine();
            JSONArray whstockArray = JSONArray.parseArray(omsResult);
		return whstockArray;
	}

	/**
	 * 换货转退货
	 * @param itemId
	 * @return
	 */
	public JSONObject conversionType(String itemId) {
		JSONObject json = new JSONObject();
		//买家数据改变
		BuyCustomerItem buyItem = buyCustomerItemMapper.get(itemId);
		buyItem.setType("2");//换货转退货
		buyCustomerItemMapper.update(buyItem);
		//卖家改变
		BSellerCustomerItem sellerItem = sellerCustomerItemMapper.getByBuyItemId(itemId);
		if(sellerItem != null){
			sellerItem.setType("2");
			sellerCustomerItemMapper.update(sellerItem);
			//删除卖家未发货的发货单
			Map<String,Object> params = new HashMap<>();
			params.put("orderItemId", sellerItem.getId());
			params.put("recordstatus", "0");
			List<Map<String,Object>> deliveryItemList = bSellerDeliveryRecordItemMapper.queryNotDeliveryList(params);
			if(deliveryItemList != null && !deliveryItemList.isEmpty()){
				for(Map<String,Object> di : deliveryItemList){
					int count = bSellerDeliveryRecordItemMapper.getItemCount(di.get("record_id").toString());
					if(count == 1){
						//发货只有自己，删除主表
						bSellerDeliveryRecordMapper.deleteById(di.get("record_id").toString());
					}
					//删除自己
					bSellerDeliveryRecordItemMapper.delete(di.get("id").toString());
				}
			}
		}
		json.put("success", true);
		json.put("msg", "成功");
		return json;
	}
	
	/**
	 * Select by order code buy order.
	 * 点击订单号查询订单信息
	 * @param orderCode the order code
	 * @return the buy order
	 */
	public BuyCustomer selectByOrderCode(String orderCode) {
		BuyCustomer order = buyCustomerMapper.selectByOrderCode(orderCode);
		if(!ObjectUtil.isEmpty(order)){
			List<BuyCustomerItem> buyCustomerItemList = buyCustomerItemMapper.selectCustomerItemByCustomerId(order.getId());
			int totalDeliveryNum = 0;
			int totalArrivalNum = 0;
			int goodsNum = 0;
			int recordstatus = 0;
			BigDecimal goodsPrice = new BigDecimal(0);
			for (BuyCustomerItem buyCustomerItem:buyCustomerItemList) {
				goodsNum+= ObjectUtil.isEmpty(buyCustomerItem.getGoodsNumber())?0:buyCustomerItem.getGoodsNumber();
				goodsPrice = goodsPrice.add((BigDecimal) (ObjectUtil.isEmpty(buyCustomerItem.getRepairPrice())?0:buyCustomerItem.getRepairPrice()));
				totalDeliveryNum += ObjectUtil.isEmpty(buyCustomerItem.getDeliveryNum())?0:buyCustomerItem.getDeliveryNum();
				totalArrivalNum += ObjectUtil.isEmpty(buyCustomerItem.getReceiveNumber())?0:buyCustomerItem.getReceiveNumber();
			}
			order.setRecordstatus(recordstatus);
			order.setGoodsNum(goodsNum);
			order.setGoodsPrice(goodsPrice);
			order.setTotalDeliveryNum(totalDeliveryNum);
			order.setTotalArrivalNum(totalArrivalNum);
			order.setItemList(buyCustomerItemList);
		}
		return order;
	}
}
