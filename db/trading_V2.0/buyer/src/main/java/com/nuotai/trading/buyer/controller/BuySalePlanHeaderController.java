package com.nuotai.trading.buyer.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyPlanTime;
import com.nuotai.trading.buyer.model.BuySalePlanHeader;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.buyer.service.BuyPlanTimeService;
import com.nuotai.trading.buyer.service.BuySalePlanHeaderService;
import com.nuotai.trading.buyer.service.BuySalePlanItemService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;


/**
 * 
 * 销售计划
 * @author dxl"
 * @date 2017-12-21 10:23:52
 */
@Controller
@RequestMapping("buyer/salePlan")
public class BuySalePlanHeaderController extends BaseController {
	@Autowired
	private SysShopService shopService;
	@Autowired
	private BuySalePlanHeaderService salePlanHeaderService;
	@Autowired
	private BuySalePlanItemService salePlanItemService;
	@Autowired
	private BuyPlanTimeService buyPlanTimeService;
	
	/**
	 * 提报销售计划
	 * @return
	 */
	@RequestMapping("addSalePlan")
	public String addSalePlan(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId",ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		List<SysShop> shopList = shopService.selectByMap(map);
		model.addAttribute("shopList", net.sf.json.JSONArray.fromObject(shopList));
		model.addAttribute("shopList2", shopList);
		//销售周期取值
		BuyPlanTime planTime = buyPlanTimeService.queryOne();
		int salePlanInterval = planTime.getSalePlanInterval();//销售计划间隔
		int salePlanArea = planTime.getSalePlanArea();//销售周期范围
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(Calendar.DATE, salePlanInterval);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		model.addAttribute("startDate", sf.format(gc.getTime()));//开始时间
		gc.add(Calendar.DATE, salePlanArea-1);
		model.addAttribute("endDate", sf.format(gc.getTime()));//结束时间
		model.addAttribute("saleDays", planTime.getSalePlanArea());//天数
		return "newpage/buyer/addSalePlan/addSalePlan";
	}
	
	/**
	 * 提报销售计划保存
	 * @param user
	 * @param company
	 * @return
	 */
	@RequestMapping("saveAddSalePlan")
	@ResponseBody
    public String saveAddPurchase(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
        	salePlanHeaderService.saveAddSalePlan(params);
            json.put("success", true);
            json.put("msg", "提交销售计划成功！");
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "提交销售计划失败！");
        }
        return json.toString();
    }
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = salePlanHeaderService.verifySuccess(id);
		return json.toString();
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyError", method = RequestMethod.GET)
	@ResponseBody
	public String verifyError(String id){
		JSONObject json = salePlanHeaderService.verifyError(id);
		return json.toString();
	}
	
	/**
	 * 销售计划报表
	 */
	@RequestMapping("salePlanAllList")
	public String salePlanAllList(@RequestParam Map<String,Object> map){
		return "newpage/buyer/salePlan/salePlanAllList";
	}
	
	/**
	 * 销售计划列表展示
	 */
	@RequestMapping("salePlanList")
	public String salePlanList(@RequestParam Map<String,Object> map){
		return "newpage/buyer/salePlan/confirmSalePlanList";
	}
	
	/**
	 * 获得数据
	 * @param params
	 */
	@RequestMapping(value = "loadDataJson", method = RequestMethod.GET)
	public void loadDataJson(@RequestParam Map<String,Object> params) {
		layuiTableData(params, new IService(){
			@Override
			public List init(Map<String, Object> params)
					throws ServiceException {
				//人员权限添加
				if(!"admin".equals(ShiroUtils.getUserId())){
					//不是超级管理员
					List<String> shopCodeList = ShiroUtils.getUserShop();
					params.put("shopCodeFlag", true);
					params.put("shopCodeList", shopCodeList);
				}
				List<Map<String,Object>> salePlanList = salePlanItemService.getProductGroupByMap(params);
				return salePlanList;
			}
		});
	}
	
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = salePlanHeaderService.loadData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 审批下单页面
	 */
	@RequestMapping("loadVerifyCreateOrder")
	public String loadVerifyCreateOrder(String idStr){
		if (idStr != null && !"".equals(idStr)) {
			String[] ids = idStr.split(",");
			List<BuySalePlanItem> productList = salePlanItemService.getProductGroupByBarcode(ids);
			Map<String,Object> map = new HashMap<String, Object>();  
			map.put("idStr", ids);
			if(productList != null && productList.size() > 0){
				for(BuySalePlanItem item : productList){
					map.put("barcode", item.getBarcode());
					List<Map<String,Object>> shopItemList = salePlanItemService.getProductGroupByMap(map);
					item.setShopItemList(shopItemList);
				}
			}
			model.addAttribute("productList", productList);
		}
		return "platform/buyer/applypurchase/salePlanList/verifyCreateOrder";
	}
	
	/**
	 * 查看销售计划详情页面 
	 * @return
	 */
	@RequestMapping("/loadSalePlanDetails")
	public String loadApplyPurchaseDetails(String id,String verifyDetails){
		BuySalePlanHeader header = salePlanHeaderService.get(id);
		model.addAttribute("salePlanHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/salePlanList/loadSalePlanDetails";
	}
	
	
	/**
	 * 生成采购计划-销售计划商品汇总列表页面
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadSalePlanProductSum", method = RequestMethod.GET)
	public String loadSalePlanProductSum(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        params.put("companyId", ShiroUtils.getCompId());
        params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
        searchPageUtil.setObject(params);
        List<Map<String,Object>> productList = salePlanItemService.selectSalePlanProductSumByPage(searchPageUtil);
        searchPageUtil.getPage().setList(productList);
        model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanProductSum/productSumList";
	}
	
	/**
	 * 加载销售计划明细页面-部门列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadSalePlanShopList", method = RequestMethod.POST)
	public String loadSalePlanShopList(String barcode) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId", ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("barcode", barcode);
		List<Map<String,Object>> shopList = salePlanItemService.selectSalePlanShopListByBarcode(map);
		model.addAttribute("shopList", shopList);
		return "platform/buyer/applypurchase/salePlanProductSum/shopListDetails";
	}
	
	/**
	 * 批量生成采购计划操作
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("beatchCreatePurchasePlan")
	public String beatchCreatePurchasePlan(String idStr) throws Exception {
		if (idStr != null && !"".equals(idStr)) {
			String[] ids = idStr.split(",");
			List<BuySalePlanItem> productList = salePlanItemService.getProductGroupByBarcode(ids);
			Map<String,Object> map = new HashMap<String, Object>();  
			//map.put("idStr", ids);
			if(productList != null && productList.size() > 0){
				for(BuySalePlanItem item : productList){
					map.put("barcode", item.getBarcode());
					List<Map<String,Object>> shopList = salePlanItemService.selectSalePlanShopListByBarcode(map);
					if(shopList != null && shopList.size() > 0){
						item.setShopItemList(shopList);
					}
				}
			}
			model.addAttribute("productList", productList);
		}
		return "platform/buyer/applypurchase/salePlanList/verifyCreateOrder";
	}
	
	/**
	 * 销售计划审批-待我审批首页
	 * @return
	 */
	@RequestMapping("approvedHtml")
	public String approvedHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		params.put("companyId",ShiroUtils.getCompId());
		params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		//获取部门的名称
		List<SysShop> shopList = shopService.selectByMap(params);
		model.addAttribute("shopList", shopList);
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanList";
	}
	
	/**
	 * 销售计划审批-待我审批数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("approvedData")
	public String approvedData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		salePlanHeaderService.loadApprovedData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanData";
	}
	
	/**
	 * 销售计划审批-查看销售计划详情页面 
	 * @return
	 */
	@RequestMapping("approvedDetails")
	public String approvedDetails(String id,String verifyDetails){
		BuySalePlanHeader header = salePlanHeaderService.get(id);
		model.addAttribute("salePlanHeader", header);
		model.addAttribute("verifyDetails", verifyDetails);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/applypurchase/salePlanList/approvedSalePlanDataDetails";
	}
	
	/**
	 * 一键通过
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyAll")     
	@ResponseBody
	public String verifyAll(String id){
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(id)){
			String[] idStr = id.split(",");
			for(String orderId : idStr){
				json = salePlanHeaderService.verifyAll(orderId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json.toJSONString();
	}
	
	/**
	 * 提报计划外销售
	 * @return
	 */
	@RequestMapping("addSalePlanOut")
	public String addSalePlanOut(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId",ShiroUtils.getCompId());
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		List<SysShop> shopList = shopService.selectByMap(map);
		model.addAttribute("shopList", net.sf.json.JSONArray.fromObject(shopList));
		model.addAttribute("shopList2", shopList);
		return "newpage/buyer/addSalePlan/addSalePlanOut";
	}
	
	/**
	 * 确认销售计划
	 * @param dataStr
	 * @return
	 */
	@RequestMapping(value = "/confirmSales", method = RequestMethod.POST)
	@ResponseBody
    public JSONObject saveConfirmSalePlan(String id,int confirmSalesNum){
        JSONObject json = new JSONObject();
        try {
        	json = salePlanHeaderService.saveConfirmSalePlan(id,confirmSalesNum);
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "确认失败！");
        }
        return json;
    }
	
	/**
	 * 转交采购计划
	 * @param salePlanArray
	 * @return
	 */
	@RequestMapping(value = "/nextStep", method = RequestMethod.POST)
	@ResponseBody
    public JSONObject nextStep(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
        	json = salePlanHeaderService.nextStep(params);
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "确认失败！");
        }
        return json;
    }
}
