package com.nuotai.trading.buyer.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 发货商品详细信息
 * @author gsf
 * @date 2017-08-10
 */

@Data
public class BuyDeliveryRecordItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//发货id
	private String recordId;
	//发货单号
	private String deliverNo;
	//订单id
	private String orderId;
	//订单明细id
	private String orderItemId;
	//卖家发货明细ID
	private String deliveryItemId;
	//卖家订单明细ID
	private String sellerOrderItemId;
	//采购计划单号
	private String applyCode;
	//订单号（外部）
	private String orderCode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//单位ID
	private String unitId;
	private String unitName;
	//
	private BigDecimal salePrice;
	//仓库id
	private String warehouseId;
	//仓库code
	private String warehouseCode;
	//仓库名称
	private String warehouseName;
	//订单数量（外部）
	private Integer orderNum;
	//本次发货数量
	private Integer deliveryNum;
	//已发货数量(外部)
	private Integer deliveredNum;
	//到货数
	private Integer arrivalNum;
	//订单商品到货数
	private Integer orderArrivalNum;
	//到货时间
	private Date arrivalDate;
	//入库人
	private String warehouseHolder;
	//发货状态[0、全部发货；1、部分发货]
	private Integer status;
	//备注
	private String remark;
	//是否需要发票Y:是N:否
	private String isNeedInvoice;

	//店铺(外部)
	private String shopId;
	private String shopCode;
	private String shopName;
	//采购订单类型 0:采购订单 1:换货订单
	private String orderType;
	//售后订单商品数量
	private Integer customerNum;
}
