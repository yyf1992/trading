package com.nuotai.trading.buyer.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyDeliveryRecord;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.ConfirmReceiveData;
import com.nuotai.trading.buyer.service.BuyDeliveryRecordItemService;
import com.nuotai.trading.buyer.service.BuyDeliveryRecordService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.seller.model.SellerLogistics;
import com.nuotai.trading.seller.service.SellerLogisticsService;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.UplaodUtil;

/**
 * The type Receive controller.
 * 收货Controller
 *
 * @author liuhui
 * @date 2017 -09-20 9:26
 */
@Controller
@RequestMapping("platform/buyer/receive")
public class ReceiveController extends BaseController{
    @Autowired
    private BuyDeliveryRecordService buyDeliveryRecordService;
    @Autowired
    private BuyDeliveryRecordItemService buyDeliveryRecordItemService;
    @Autowired
    private AttachmentService attachmentService;
    @Autowired
    private SellerLogisticsService sellerLogisticsService;
    @Autowired
    private TBusinessWhareaService tBusinessWhareaService;
    /**
     * Receive list string.
     * 收货单列表界面
     *
     * @param searchPageUtil the search page util
     * @param params         the params
     * @return the string
     */
    @RequestMapping("receiveList")
    public String receiveList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
        if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        if(!params.containsKey("tabId")||params.get("tabId")==null){
            params.put("tabId", "0");
        }
        //获得不同状态数量
        buyDeliveryRecordService.getRecordStatusNum(params);
        model.addAttribute("params", params);
        model.addAttribute("searchPageUtil", searchPageUtil);
        List<TBusinessWharea> warehouseList = tBusinessWhareaService.getCompanyId();
        model.addAttribute("warehouseList", warehouseList);
        return "platform/buyer/receive/receiveList";
    }

    /**
     * Load receive data string.
     * 获取收货单列表数据
     * @param searchPageUtil the search page util
     * @param params         the params
     * @return the string
     */
    @RequestMapping(value = "loadReceiveData", method = RequestMethod.GET)
    public String loadReceiveData(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
        if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        searchPageUtil = buyDeliveryRecordService.loadDeliveryRecordData(searchPageUtil, params);
        model.addAttribute("searchPageUtil", searchPageUtil);
        return "platform/buyer/receive/receiveListData";
    }

    /**
     * Confirm receive data string.
     * 确认收货界面
     * @param id 收货单ID
     * @return the string
     */
    @RequestMapping("confirmReceiveData")
    public String confirmReceiveData(String id,String isFromWms,String recordstatus){
        //获取收货主表信息
        BuyDeliveryRecord buyDeliveryRecord = buyDeliveryRecordService.get(id);
        //获得不同状态数量
        List<ConfirmReceiveData> receiveList = buyDeliveryRecordService.queryConfirmReceiveData(id);
        model.addAttribute("receiveList", receiveList);
        model.addAttribute("isFromWms", isFromWms);
        model.addAttribute("recordstatus", recordstatus);
        //获取物流信息
        SellerLogistics logistics = sellerLogisticsService.get(buyDeliveryRecord.getLogisticsId());
        model.addAttribute("logistics", logistics);
        //返回时回填搜索条件
      	String condition = ShiroUtils.returnSearchCondition();
      	model.addAttribute("form", condition);
        return "platform/buyer/receive/confirmReceiveData";
    }


    /**
     * Storage goods string.
     * 确认收货，入库
     * @param params the params
     * @return the string
     */
    @ResponseBody
    @RequestMapping("/storageGoods")
    public String storageGoods(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
            buyDeliveryRecordService.storageGoods(params);
            json.put("success", true);
        } catch (Exception e) {
            json.put("false", false);
            json.put("msg", e.getMessage());
            e.printStackTrace();
        }
        return json.toString();
    }
    /**
     * Send goods sub string.
     * 确认或者拒绝签收回单
     * @param params the params
     * @return the string
     */
    @ResponseBody
    @RequestMapping("/checkReceipt")
    public String checkReceipt(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
            buyDeliveryRecordService.checkReceipt(params);
            json.put("success", true);
        } catch (Exception e) {
            json.put("false", false);
            json.put("msg", e.getMessage());
            e.printStackTrace();
        }
        return json.toString();
    }

    @RequestMapping("openQRcode")
    public String openQRcode(String deliverNo){
        Map<String,Object> map = new HashMap<>();
        map.put("relatedId",deliverNo);
        List<BuyAttachment> list = attachmentService.selectByMap(map);
        model.addAttribute("attachmentList", list);
        model.addAttribute("deliverNo", deliverNo);
        return "platform/buyer/receive/openQRcode";
    }


    /**
     * Upload receipt attachment string.
     * 上传收货单附件
     * @param files the files
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value="/uploadReceiptAttachment",method=RequestMethod.POST)
    @ResponseBody
    public String uploadReceiptAttachment(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
            throws Exception{
        JSONObject json = new JSONObject();
        try {
            List urlList = new ArrayList();
            for (MultipartFile file : files) {
                //获取文件名
                String fileName = file.getOriginalFilename();
                json.put("fileName", fileName);
                //获取文件后缀名
                String fileExtensionName = FilenameUtils.getExtension(fileName);
                if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
                        && !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
					json.put("result", "fail");
					json.put("msg", "上传文件格式不正确！");
					return json.toString();
                }
                //获取文件大小
                String fileSize = ShiroUtils.convertFileSize(file.getSize());
                UplaodUtil uplaodUtil = new UplaodUtil();
                String url = uplaodUtil.uploadFile(file,null,true);
                urlList.add(url);
            }
            json.put("urlList", urlList);
            json.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "fail");
            json.put("msg", "上传失败！请联系管理员！");
            return json.toString();
        }
        return json.toString();
    }


    /**
     * Save receipt attachment string.
     * 保存收货凭证
     * @param deliverNo 发货单号
     * @param attachments the attachments
     * @return the string
     */
    @RequestMapping(value = "saveReceiptAttachment", method = RequestMethod.POST)
    @ResponseBody
    public String saveReceiptAttachment(String deliverNo,String attachments){
        JSONObject json = new JSONObject();
        try {
            buyDeliveryRecordService.saveReceiptAttachment(deliverNo,attachments);
            json.put("success", true);
            json.put("msg", "保存成功！");
        } catch (Exception e) {
            json.put("success", false);
            json.put("msg", "保存失败！");
        }
        return json.toString();
    }

    /**
     * Load attachment string.
     * 收货单附件查询
     * @param relatedId the related id
     * @return the string
     */
    @RequestMapping("loadAttachment")
    @ResponseBody
    public String loadAttachment(String relatedId){
        JSONArray array = new JSONArray();
        Map<String,Object> map = new HashMap<>();
        map.put("relatedId",relatedId);
        List<BuyAttachment> list = attachmentService.selectByMap(map);
        return JSONObject.toJSONString(list);
    }
    
    /**
     * 推送oms
     * @param id
     */
    @RequestMapping("pushOMS")
    @ResponseBody
    public void pushOMS(String id){
    	buyDeliveryRecordService.pushOMS(id);
    }

    /**
     * exportReceive.
     * 收货单导出
     * @param params the params
     * @throws Exception the exception
     */
    @RequestMapping(value = "/exportReceive")
    public void exportReceive(@RequestParam Map<String,Object> params) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<BuyDeliveryRecord> deliveryList = buyDeliveryRecordService.getExportData(params);
        // 第一步，创建一个webbook，对应一个Excel文件
        SXSSFWorkbook wb = new SXSSFWorkbook();
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        Sheet sheet = wb.createSheet("收货单");
        sheet.setDefaultColumnWidth(20);
        //字体
        XSSFFont font = (XSSFFont) wb.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
        headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // 下边框
        headStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        headStyle.setBorderLeft(BorderStyle.THIN);
        // 右边框
        headStyle.setBorderRight(BorderStyle.THIN);
        // 上边框
        headStyle.setBorderTop(BorderStyle.THIN);
        // 字体左右居中
        headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
        headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headStyle.setFont(font);
        // 创建单元格格式，并设置表头居中
        XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
        //字体左右居中
        normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
        // 下边框
        normalStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        normalStyle.setBorderLeft(BorderStyle.THIN);
        // 右边框
        normalStyle.setBorderRight(BorderStyle.THIN);
        // 上边框
        normalStyle.setBorderTop(BorderStyle.THIN);
        //数字小数位格式
        DecimalFormat fnum = new DecimalFormat("##0.0000");
        //在sheet中添加表头第0行
        Row row = sheet.createRow(0);
        // 设置excel的数据头信息
        Cell cell;
        String[] cellHeadArray = {"发货单号","发货日期","收货日期","状态","订单号","商品名称","货号","条形码","规格代码","规格名称","单位","发货数量","到货数量","单价","总价","供应商","仓库","备注","对账状态"};
        for (int i = 0; i < cellHeadArray.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(cellHeadArray[i]);
            cell.setCellStyle(headStyle);
        }
        int rowCount = 1;
        if (deliveryList != null && deliveryList.size() > 0) {
            for (int i=0;i<deliveryList.size();i++) {
                BuyDeliveryRecord delivery = deliveryList.get(i);
                List<BuyDeliveryRecordItem> deliveryItemList = delivery.getItemList();
                String statusName = "";
                if(delivery.getRecordstatus()==0){
                    statusName = "待收货";
                }else if (delivery.getRecordstatus()==1){
                    statusName = "已收货";
                }
                //订单明细
                for(int j=0;j<deliveryItemList.size();j++){
                    BuyDeliveryRecordItem deliveryItem = deliveryItemList.get(j);
                    row=sheet.createRow(rowCount);
                    //1.发货单号
                    cell = row.createCell(0);
                    cell.setCellValue(delivery.getDeliverNo());
                    cell.setCellStyle(normalStyle);
                    //2.发货日期
                    cell = row.createCell(1);
                    cell.setCellValue(ObjectUtil.isEmpty(delivery.getCreateDate())?"":sdf.format(delivery.getCreateDate()));
                    cell.setCellStyle(normalStyle);
                    //3.收货日期
                    cell = row.createCell(2);
                    cell.setCellValue(ObjectUtil.isEmpty(delivery.getArrivalDate())?"":sdf.format(delivery.getArrivalDate()));
                    cell.setCellStyle(normalStyle);
                    //4.状态
                    cell = row.createCell(3);
                    cell.setCellValue(statusName);
                    cell.setCellStyle(normalStyle);
                    //5.订单号
                    cell = row.createCell(4);
                    cell.setCellValue(deliveryItem.getOrderCode());
                    cell.setCellStyle(normalStyle);
                    //6.商品名称
                    cell = row.createCell(5);
                    cell.setCellValue(deliveryItem.getProductName());
                    cell.setCellStyle(normalStyle);
                    //7.货号
                    cell = row.createCell(6);
                    cell.setCellValue(deliveryItem.getProductCode());
                    cell.setCellStyle(normalStyle);
                    //8.条形码
                    cell = row.createCell(7);
                    cell.setCellValue(deliveryItem.getBarcode());
                    cell.setCellStyle(normalStyle);
                    //9.规格代码
                    cell = row.createCell(8);
                    cell.setCellValue(deliveryItem.getSkuCode());
                    cell.setCellStyle(normalStyle);
                    //10.规格名称
                    cell = row.createCell(9);
                    cell.setCellValue(deliveryItem.getSkuName());
                    cell.setCellStyle(normalStyle);
                    //11.单位
                    cell = row.createCell(10);
                    cell.setCellValue(deliveryItem.getUnitName());
                    cell.setCellStyle(normalStyle);
                    //12.发货数量
                    cell = row.createCell(11);
                    cell.setCellValue(deliveryItem.getDeliveryNum());
                    cell.setCellStyle(normalStyle);
                    //13.到货数量
                    cell = row.createCell(12);
                    cell.setCellValue(deliveryItem.getArrivalNum());
                    cell.setCellStyle(normalStyle);
                    //14.单价
                    cell = row.createCell(13);
                    cell.setCellValue(fnum.format(deliveryItem.getSalePrice()));
                    cell.setCellStyle(normalStyle);
                    //15.总价
                    cell = row.createCell(14);
                    cell.setCellValue(fnum.format(deliveryItem.getSalePrice().multiply(new BigDecimal(deliveryItem.getDeliveryNum()))));
                    cell.setCellStyle(normalStyle);
                    //16.供应商
                    cell = row.createCell(15);
                    cell.setCellValue(delivery.getSupplierName());
                    cell.setCellStyle(normalStyle);
                    //17.入仓
                    cell = row.createCell(16);
                    cell.setCellValue(delivery.getWarehouseName());
                    cell.setCellStyle(normalStyle);
                    //18.备注
                    cell = row.createCell(17);
                    cell.setCellValue(deliveryItem.getRemark());
                    cell.setCellStyle(normalStyle);
                    //19.对账状态
                    int reconciliationStatus = 0;
                    if (!ObjectUtil.isEmpty( delivery.getReconciliationStatus())){
                    	reconciliationStatus = Integer.parseInt( delivery.getReconciliationStatus());
                    }
                    String reconciliation ="";
            		switch (reconciliationStatus) {
        			case 0:
        				reconciliation = "未对账";
        				break;
        			case 1:
        				reconciliation = "对帐中";
        				break;
        			case 2:
        				reconciliation = "已对账";
        				break;
        			default:
        				reconciliation = "";
        				break;
            		}
                    cell = row.createCell(18);
                    cell.setCellValue(reconciliation);
                    cell.setCellStyle(normalStyle);
                    rowCount++;
                }
            }
        }
        ExcelUtil.preExport("收货单导出", response);
        ExcelUtil.export(wb, response);
    }
}
