package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagement;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

@Component("bSellerBillCycleManagementMapper")
public interface BSellerBillCycleManagementMapper extends BaseDao<BSellerBillCycleManagement>{
    int deleteByPrimaryKey(String id);

    int addSellerBillCycle(BSellerBillCycleManagement record);

    int insertSelective(BSellerBillCycleManagement record);

    BSellerBillCycleManagement selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BSellerBillCycleManagement record);

    int updateByPrimaryKey(BSellerBillCycleManagement record);
}