package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyPlanTime;
import com.nuotai.trading.dao.BaseDao;

/**
 * 
 * 
 * @author "
 * @date 2018-04-03 20:24:53
 */
public interface BuyPlanTimeMapper extends BaseDao<BuyPlanTime> {
	
}
