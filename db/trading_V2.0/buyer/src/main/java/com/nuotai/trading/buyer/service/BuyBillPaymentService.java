package com.nuotai.trading.buyer.service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.*;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerCustomerMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerDeliveryRecordMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerLogisticsMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerOrderMapper;
import com.nuotai.trading.buyer.model.*;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliation;
import com.nuotai.trading.buyer.model.seller.BSellerLogistics;
import com.nuotai.trading.dao.*;
import com.nuotai.trading.model.*;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class BuyBillPaymentService {
	@Autowired
	private BuyBillReconciliationMapper billReconciliationMapper;
	@Autowired
	private BuyBillReconciliationItemMapper buyBillReconciliationItemMapper;
	@Autowired
	private BSellerBillReconciliationMapper bSellerBillReconciliationMapper;
	@Autowired
	private BuyBillReconciliationPaymentMapper buyBillReconciliationPaymentMapper;
	@Autowired
	private BuyBillInvoiceMapper buyBillInvoiceMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	@Autowired
	private BuyDeliveryRecordMapper buyDeliveryRecordMapper;//卖家发货
	@Autowired
	private BSellerDeliveryRecordMapper bSellerDeliveryRecordMapper;
	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;//发货明细
	@Autowired
	private BuyOrderMapper orderMapper;//买家订单
	@Autowired
	private BSellerOrderMapper sellerOrderMapper;//卖家订单
	@Autowired
	private BuyCustomerMapper buyCustomerMapper;
	@Autowired
	private BSellerCustomerMapper bSellerCustomerMapper;//卖家退货
	@Autowired
	private BuyOrderProductMapper productMapper;
	@Autowired
	private BSellerLogisticsMapper logisticsMapper;
    @Autowired
    private BuyOrderService buyOrderService;


	//查询付款对账列表信息
	public List<BuyBillReconciliation> getRecToPamyment(
			SearchPageUtil searchPageUtil) {
		return billReconciliationMapper.getRecToPamyment(searchPageUtil);
	}

	//根据审批状态查询数量
	public int queryRecPaymentCount(Map<String, Object> map) {
		return billReconciliationMapper.queryRecPaymentCount(map);
	}

	//向账单周期管理页面提供不同状态下的数量
	public void queryPaymentCount(Map<String, Object> billMap){
		//查询待内部审批周期数
		billMap.put("paymentStatusCount", "0");
		int approvalBillReconciliationCount = queryRecPaymentCount(billMap);
		//查询待对方审批周期数
		billMap.put("paymentStatusCount", "1");
		int acceptBillReconciliationCount = queryRecPaymentCount(billMap);
		//查询审批已通过周期数
		billMap.put("paymentStatusCount", "2");
		int apprEndBillReconciliationCount = queryRecPaymentCount(billMap);
		//查询付款待开票
		billMap.put("paymentStatusCount", "3");
		int needBillReconciliationCount = queryRecPaymentCount(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillReconciliationCount", approvalBillReconciliationCount);
		billMap.put("acceptBillReconciliationCount", acceptBillReconciliationCount);
		billMap.put("apprEndBillReconciliationCount", apprEndBillReconciliationCount);
		billMap.put("needBillReconciliationCount", needBillReconciliationCount);

	}

	//根据条件查询付款导出数据
	public List<BuyBillReconciliation> queryPamymentList(Map<String,Object> map){
		return billReconciliationMapper.queryPamymentList(map);
	}

	//根据付款状态修改付款信息
	public int updateSavePayment(Map<String,Object> updateSavePaymentMap){
		String reconciliationId = updateSavePaymentMap.get("reconciliationId").toString();
		//String paymentNo = updateSavePaymentMap.get("paymentNo").toString();
		String actualPaymentAmount = updateSavePaymentMap.get("actualPaymentAmount").toString();
		String residualPaymentAmount = updateSavePaymentMap.get("residualPaymentAmount").toString();
		String actualPaymentAmountNew = updateSavePaymentMap.get("actualPaymentAmountNew").toString();

		BigDecimal actualPaymentAmountMath = new BigDecimal(actualPaymentAmount);
		BigDecimal residualPaymentAmountMath = new BigDecimal(residualPaymentAmount);
		BigDecimal actualPaymentAmountNewMath = new BigDecimal(actualPaymentAmountNew);

		BigDecimal actualPaymentAmountEnd = actualPaymentAmountNewMath.add(actualPaymentAmountMath);
		BigDecimal residualPaymentAmountEnd = residualPaymentAmountMath.subtract(actualPaymentAmountNewMath);
		int isZero = residualPaymentAmountEnd.compareTo(BigDecimal.ZERO);

		String bankAccount = updateSavePaymentMap.get("bankAccount").toString();
		String paymentType = updateSavePaymentMap.get("paymentType").toString();
		String paymentFileStr = updateSavePaymentMap.get("paymentFileStr").toString();
		String paymentRemarks = updateSavePaymentMap.get("paymentRemarks").toString();

		BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
		buyBillReconciliation.setId(reconciliationId);
		buyBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
		buyBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
		buyBillReconciliation.setBankAccount(bankAccount);
		if(isZero == 0){
			buyBillReconciliation.setPaymentStatus("1");
		}
		buyBillReconciliation.setPaymentType(paymentType);
		buyBillReconciliation.setPaymentFileAddr(paymentFileStr);
		buyBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);

		int updatePaymentCount = billReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
		if(updatePaymentCount > 0){
			//添加数据到付款关联表
			BuyBillReconciliationPayment paymentInfo = new BuyBillReconciliationPayment();
			paymentInfo.setId(ShiroUtils.getUid());
			paymentInfo.setReconciliationId(reconciliationId);
			paymentInfo.setActualPaymentAmount(actualPaymentAmountNewMath);
			paymentInfo.setBankAccount(bankAccount);
			paymentInfo.setPaymentType(paymentType);
			paymentInfo.setPaymentAddr(paymentFileStr);
			paymentInfo.setPaymentStatus("0");
			paymentInfo.setPaymentRemarks(paymentRemarks);
			buyBillReconciliationPaymentMapper.addPaymentInfo(paymentInfo);

			BSellerBillReconciliation bSellerBillReconciliation = new BSellerBillReconciliation();
			bSellerBillReconciliation.setId(reconciliationId);
			bSellerBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
			bSellerBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
			bSellerBillReconciliation.setBankAccount(bankAccount);
			bSellerBillReconciliation.setIsFullInvoice("1");
			bSellerBillReconciliation.setPaymentStatus("1");
			bSellerBillReconciliation.setPaymentType(paymentType);
			bSellerBillReconciliation.setPaymentFileAddr(paymentFileStr);
			bSellerBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);
			bSellerBillReconciliationMapper.updateByPrimaryKeySelective(bSellerBillReconciliation);
		}
		return updatePaymentCount;
	}

	//付款待审批查询
	public void approveBillPayment(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("paymentStatus", "0");
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		//params1.put("siteId", "18022814245915101199");//测试
		params1.put("siteId", "18030316164774916589");//阿里
		//获得所有未审批的id
		List<String> acceptPaymentIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> acceptPaymentIdListNew = new ArrayList<>();

		List<BuyBillReconciliationPayment> billRecPaymentList = buyBillReconciliationPaymentMapper.getPaymentListByRecId(params);
		//List<BuyBillReconciliation> billRecList = billReconciliationMapper.selectByPrimaryKeyList(params);
		if(!billRecPaymentList.isEmpty()){
			Map<String, String> acceptPaymentIdMap = new HashMap<>();
			for(BuyBillReconciliationPayment billRecPayment : billRecPaymentList){
				if(null != billRecPayment.getAcceptPaymentId()){
					String acceptId =billRecPayment.getAcceptPaymentId();
					acceptPaymentIdMap.put(acceptId, acceptId);
				}

			}
			if(!acceptPaymentIdList.isEmpty()){
				for(String acceptPaymentId : acceptPaymentIdList){
					if(acceptPaymentIdMap.containsKey(acceptPaymentId)){
						acceptPaymentIdListNew.add(acceptPaymentId);
					}
				}
			}
		}else{
			acceptPaymentIdListNew = acceptPaymentIdList;
		}

		params.put("approved", true);
		params.put("acceptPaymentIdList", acceptPaymentIdListNew);
		searchPageUtil.setObject(params);
		List<BuyBillReconciliationPayment> billRecPaymentInfoList = buyBillReconciliationPaymentMapper.getBillRecPaymentList(searchPageUtil);
		searchPageUtil.getPage().setList(billRecPaymentInfoList);
	}

	//一键通过
	public JSONObject verifyBillPaymentAll(String acceptPaymentIdStr) {
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(acceptPaymentIdStr)){
			String[] idStr = acceptPaymentIdStr.split(",");
			for(String acceptPaymentId : idStr){
				verifyApproved(acceptPaymentId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json;
	}

	/**
	 * 审批
	 * @param acceptPaymentId
	 */
	public void verifyApproved(String acceptPaymentId){
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(acceptPaymentId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						acceptPaymentSuccess(acceptPaymentId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
	}

	//根据付款状态修改付款信息
	public List<String> acceptBillPayment(Map<String,Object> updateSavePaymentMap){
		List<String> idList = new ArrayList<String>();
		String reconciliationId = updateSavePaymentMap.get("reconciliationId").toString();
		String acceptPaymentId = ShiroUtils.getUid();

		String actualPaymentAmount = updateSavePaymentMap.get("actualPaymentAmount").toString();
		String residualPaymentAmount = updateSavePaymentMap.get("residualPaymentAmount").toString();
		String actualPaymentAmountNew = updateSavePaymentMap.get("actualPaymentAmountNew").toString();

		BigDecimal actualPaymentAmountMath = new BigDecimal(actualPaymentAmount);
		BigDecimal residualPaymentAmountMath = new BigDecimal(residualPaymentAmount);
		BigDecimal actualPaymentAmountNewMath = new BigDecimal(actualPaymentAmountNew);

		BigDecimal actualPaymentAmountEnd = actualPaymentAmountNewMath.add(actualPaymentAmountMath);
		BigDecimal residualPaymentAmountEnd = residualPaymentAmountMath.subtract(actualPaymentAmountNewMath);
		//int isZero = residualPaymentAmountEnd.compareTo(BigDecimal.ZERO);

		String bankAccount = updateSavePaymentMap.get("bankAccount").toString();
		String paymentType = updateSavePaymentMap.get("paymentType").toString();
		//String paymentFileStr = updateSavePaymentMap.get("paymentFileStr").toString();
		String paymentRemarks = updateSavePaymentMap.get("paymentRemarks").toString();

		BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
		buyBillReconciliation.setId(reconciliationId);
		buyBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
		buyBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
		buyBillReconciliation.setBankAccount(bankAccount);
		/*if(isZero == 0){
			buyBillReconciliation.setPaymentStatus("1");
		}else {
			buyBillReconciliation.setPaymentStatus("5");
		}*/
		buyBillReconciliation.setPaymentStatus("5");
		buyBillReconciliation.setPaymentType(paymentType);
		buyBillReconciliation.setAcceptPaymentId(acceptPaymentId);
		//buyBillReconciliation.setPaymentFileAddr(paymentFileStr);
		buyBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);

		int updatePaymentCount = billReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
		if(updatePaymentCount > 0){
			//添加数据到付款关联表
			BuyBillReconciliationPayment paymentInfo = new BuyBillReconciliationPayment();
			paymentInfo.setId(ShiroUtils.getUid());
			paymentInfo.setAcceptPaymentId(acceptPaymentId);
			paymentInfo.setReconciliationId(reconciliationId);
			paymentInfo.setActualPaymentAmount(actualPaymentAmountNewMath);
			paymentInfo.setBankAccount(bankAccount);
			paymentInfo.setPaymentType(paymentType);
			//paymentInfo.setPaymentAddr(paymentFileStr);
			paymentInfo.setPaymentStatus("0");
			paymentInfo.setPaymentRemarks(paymentRemarks);
			buyBillReconciliationPaymentMapper.addPaymentInfo(paymentInfo);

			/*BSellerBillReconciliation bSellerBillReconciliation = new BSellerBillReconciliation();
			bSellerBillReconciliation.setId(reconciliationId);
			//bSellerBillReconciliation.setAcceptPaymentId(acceptPaymentId);
			bSellerBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
			bSellerBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
			bSellerBillReconciliation.setBankAccount(bankAccount);
			bSellerBillReconciliation.setIsFullInvoice("1");
			bSellerBillReconciliation.setPaymentStatus("5");
			bSellerBillReconciliation.setPaymentType(paymentType);
			//bSellerBillReconciliation.setPaymentFileAddr(paymentFileStr);
			bSellerBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);
			bSellerBillReconciliationMapper.updateByPrimaryKeySelective(bSellerBillReconciliation);*/

			idList.add(acceptPaymentId);
		}

		return idList;
	}

	//根据付款状态修改付款信息 驳回再次修改
	public void acceptUpdateBillPayment(Map<String,Object> updateSavePaymentMap){
		String reconciliationId = updateSavePaymentMap.get("reconciliationId").toString();
		String actualPaymentAmount = updateSavePaymentMap.get("actualPaymentAmount").toString();
		String residualPaymentAmount = updateSavePaymentMap.get("residualPaymentAmount").toString();
		String actualPaymentAmountNew = updateSavePaymentMap.get("actualPaymentAmountNew").toString();

		BigDecimal actualPaymentAmountMath = new BigDecimal(actualPaymentAmount);
		BigDecimal residualPaymentAmountMath = new BigDecimal(residualPaymentAmount);
		BigDecimal actualPaymentAmountNewMath = new BigDecimal(actualPaymentAmountNew);

		BigDecimal actualPaymentAmountEnd = actualPaymentAmountNewMath.add(actualPaymentAmountMath);
		BigDecimal residualPaymentAmountEnd = residualPaymentAmountMath.subtract(actualPaymentAmountNewMath);
		int isZero = residualPaymentAmountEnd.compareTo(BigDecimal.ZERO);

		String bankAccount = updateSavePaymentMap.get("bankAccount").toString();
		String paymentType = updateSavePaymentMap.get("paymentType").toString();
		String paymentFileStr = updateSavePaymentMap.get("paymentFileStr").toString();
		String paymentRemarks = updateSavePaymentMap.get("paymentRemarks").toString();

		BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
		buyBillReconciliation.setId(reconciliationId);
		buyBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
		buyBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
		buyBillReconciliation.setBankAccount(bankAccount);
		if(isZero == 0){
			buyBillReconciliation.setPaymentStatus("1");
		}else {
			buyBillReconciliation.setPaymentStatus("5");
		}
		buyBillReconciliation.setPaymentType(paymentType);
		buyBillReconciliation.setPaymentFileAddr(paymentFileStr);
		buyBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);

		int updatePaymentCount = billReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
		if(updatePaymentCount > 0){
			//添加数据到付款关联表
			BuyBillReconciliationPayment paymentInfo = new BuyBillReconciliationPayment();
			paymentInfo.setId(ShiroUtils.getUid());
			paymentInfo.setReconciliationId(reconciliationId);
			paymentInfo.setActualPaymentAmount(actualPaymentAmountNewMath);
			paymentInfo.setBankAccount(bankAccount);
			paymentInfo.setPaymentType(paymentType);
			paymentInfo.setPaymentAddr(paymentFileStr);
			paymentInfo.setPaymentStatus("0");
			paymentInfo.setPaymentRemarks(paymentRemarks);
			buyBillReconciliationPaymentMapper.addPaymentInfo(paymentInfo);

			BSellerBillReconciliation bSellerBillReconciliation = new BSellerBillReconciliation();
			bSellerBillReconciliation.setId(reconciliationId);
			bSellerBillReconciliation.setActualPaymentAmount(actualPaymentAmountEnd);
			bSellerBillReconciliation.setResidualPaymentAmount(residualPaymentAmountEnd);
			bSellerBillReconciliation.setBankAccount(bankAccount);
			bSellerBillReconciliation.setIsFullInvoice("1");
			bSellerBillReconciliation.setPaymentStatus("5");
			bSellerBillReconciliation.setPaymentType(paymentType);
			bSellerBillReconciliation.setPaymentFileAddr(paymentFileStr);
			bSellerBillReconciliation.setBuyerPaymentRemarks(paymentRemarks);
			bSellerBillReconciliationMapper.updateByPrimaryKeySelective(bSellerBillReconciliation);
	/*	}

		if(updateCount > 0){*/
	//修改内部审批数据
			String acceptPaymentId = updateSavePaymentMap.get("acceptPaymentId").toString();
			//4.若有审批，则重置审批状态
			TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(acceptPaymentId);
			if(!ObjectUtil.isEmpty(header)){
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				//获得最大index
				int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
				//添加审批
				TradeVerifyPocess pocessUpdate = new TradeVerifyPocess();
				pocessUpdate.setId(ShiroUtils.getUid());
				pocessUpdate.setHeaderId(header.getId());
				pocessUpdate.setUserId(ShiroUtils.getUserId());
				pocessUpdate.setUserName(ShiroUtils.getUserName());
				pocessUpdate.setStatus("7");//买家修改
				pocessUpdate.setRemark("订单修改后再次提交");
				pocessUpdate.setVerifyIndex(maxIndex+1);
				pocessUpdate.setStartDate(new Date());
				pocessUpdate.setEndDate(new Date());
				tradeVerifyPocessMapper.add(pocessUpdate);
				maxIndex+=1;
				int index = maxIndex;
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				//删除未审批的数据
				tradeVerifyPocessMapper.deleteNotVerify(header.getId());

				if(!pocessList.isEmpty()){
					Map<String,String> userMap = new HashMap<>();
					for(TradeVerifyPocess pocess : pocessList){
						if("0".endsWith(pocess.getStatus())
								|| "1".endsWith(pocess.getStatus())
								|| "2".endsWith(pocess.getStatus())){
							if(userMap.containsKey(pocess.getUserId())){
								continue;
							}
							TradeVerifyPocess pocessNew = new TradeVerifyPocess();
							pocessNew.setId(ShiroUtils.getUid());
							pocessNew.setHeaderId(header.getId());
							pocessNew.setUserId(pocess.getUserId());
							pocessNew.setUserName(pocess.getUserName());
							pocessNew.setStatus("0");//等待审批
							pocessNew.setRemark(null);
							pocessNew.setVerifyIndex(index+1);
							pocessNew.setStartDate(new Date());
							pocessNew.setEndDate(new Date());
							tradeVerifyPocessMapper.add(pocessNew);
							userMap.put(pocess.getUserId(), pocess.getUserId());
							if(index == maxIndex){
								//待审批人员发送钉钉消息
								DingDingUtils.verifyDingDingMessage(pocessNew.getUserId(), pocessNew.getHeaderId());
							}
							index++;
						}
					}
				}
			}
		}

	}

	//内部审批通过
	public JSONObject acceptPaymentSuccess(String acceptPaymentId){
		JSONObject json = new JSONObject();
		//账单对账详情
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("acceptPaymentId",acceptPaymentId);
		BuyBillReconciliation buyBillRecInfo = billReconciliationMapper.selectByPrimaryKey(map);
		String reconciliationId = "";
		if(null != buyBillRecInfo){
			reconciliationId = buyBillRecInfo.getId();
		}
		BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
		buyBillReconciliation.setId(reconciliationId);
		buyBillReconciliation.setPaymentStatus("1");

		int updatePaymentCount = billReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
		if(updatePaymentCount > 0){
			//修改记录状态
			Map<String,Object> updatePaymentMap = new HashMap<String,Object>();
			updatePaymentMap.put("acceptPaymentId",acceptPaymentId);
			updatePaymentMap.put("paymentStatus","1");
			//updatePaymentMap.put("returnStatus","0");
			buyBillReconciliationPaymentMapper.updatePaymentInfo(updatePaymentMap);
			//同步到卖家
			BSellerBillReconciliation bSellerBillReconciliation = new BSellerBillReconciliation();
			bSellerBillReconciliation.setId(reconciliationId);
			bSellerBillReconciliation.setActualPaymentAmount(buyBillRecInfo.getActualPaymentAmount());
			bSellerBillReconciliation.setResidualPaymentAmount(buyBillRecInfo.getResidualPaymentAmount());
			bSellerBillReconciliation.setBankAccount(buyBillRecInfo.getBankAccount());
			bSellerBillReconciliation.setIsFullInvoice("1");
			bSellerBillReconciliation.setPaymentStatus("1");
			bSellerBillReconciliation.setPaymentType(buyBillRecInfo.getPaymentType());
			bSellerBillReconciliation.setBuyerPaymentRemarks(buyBillRecInfo.getBuyerPaymentRemarks());
			bSellerBillReconciliationMapper.updateByPrimaryKeySelective(bSellerBillReconciliation);

			BigDecimal residualPaymentAmount = buyBillRecInfo.getResidualPaymentAmount();
			int isZero = residualPaymentAmount.compareTo(BigDecimal.ZERO);
			if(isZero == 0){
				updateDeliveryAndCustmor(reconciliationId);
			}
			String orderId=billReconciliationMapper.selectOrderIdbyAcceptId(acceptPaymentId);
			if(!orderId.isEmpty()){
				buyOrderService.changePaymentStatus(orderId,"1");//是否已付款（0：待付款， 1：付款待卖家确认，2：已付款，3部分付款,4.付款审批中，5.内部审批拒绝）
			}
			
			json.put("success", true);
			json.put("msg", "成功！");

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//同步更新到采购、售后、订单模块状态
	public void updateDeliveryAndCustmor(String reconciliationId){
		//根据对账详情修改发货、退货模块对账状态
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<BuyBillReconciliationItem> recItemInfo = buyBillReconciliationItemMapper.queryRecItemList(recItemMap);

		Map<String,Object> recordIdMap = new HashMap<String,Object>();
		for(BuyBillReconciliationItem recItem:recItemInfo){
			String recordCode = recItem.getDeliverNo();
			String billReconciliationType = recItem.getBillReconciliationType();
			//String reconciliationStatus = "2";
			if(!recordIdMap.containsKey(recordCode)){
				recordIdMap.put(recordCode,billReconciliationType);
			}
		}

		//根据审批状态修改预付款金额
		for(BuyBillReconciliationItem recItem : recItemInfo){
			//同步订单对账、付款状态
			Map<String,Object> buyOrder = orderMapper.selSellerOrder(recItem.getOrderId());
			//同步修改发货订单对账状态
			if(!ObjectUtil.isEmpty(buyOrder)){
				if(null != buyOrder.get("orderCode")&&!"".equals(buyOrder.get("orderCode").toString())){
					String orderCode = buyOrder.get("orderCode").toString();//订单号
					Map<String,Object> deliveryOrderMap = new HashMap<String,Object>();
					deliveryOrderMap.put("orderCode",orderCode);
					deliveryOrderMap.put("isPaymentStatus","1");

					orderMapper.updateOrderBuyBill(deliveryOrderMap);
					sellerOrderMapper.updateOrderBuyBill(deliveryOrderMap);
				}
			}
		}

		//修改发货、退货对账状态为对账驳回状态 是否已对账（0：未对账， 1：已对帐，2：对账驳回）
		for(Map.Entry<String,Object> entry:recordIdMap.entrySet()){
			String recordCode = entry.getKey();
			String recType = entry.getValue().toString();
			if("0".equals(recType)){
				Map<String,Object> recRordItemMap = new HashMap<String,Object>();
				recRordItemMap.put("recordCode",recordCode);
				recRordItemMap.put("isPaymentStatus","1");
				buyDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
				bSellerDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
			}
			if("1".equals(recType)){
				Map<String,Object> recRordItemMap = new HashMap<String,Object>();
				recRordItemMap.put("recordCode",recordCode);
				recRordItemMap.put("isPaymentStatus","1");
				buyDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
				bSellerDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
			}
			if("2".equals(recType)){
				Map<String,Object> recRordItemMap = new HashMap<String,Object>();
				recRordItemMap.put("recordCode",recordCode);
				recRordItemMap.put("isPaymentStatus","1");
				buyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
				bSellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
			}
			if("3".equals(recType)){
				Map<String,Object> recRordItemMap = new HashMap<String,Object>();
				recRordItemMap.put("recordCode",recordCode);
				recRordItemMap.put("isPaymentStatus","1");
				buyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
				bSellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
			}
		}
	}

	//内部付款审批驳回
	public JSONObject acceptPaymentError(String acceptPaymentId){
		JSONObject json = new JSONObject();
		//账单对账详情
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("acceptPaymentId",acceptPaymentId);
		BuyBillReconciliation buyBillRecInfo = billReconciliationMapper.selectByPrimaryKey(map);
		String reconciliationId = "";
		if(null != buyBillRecInfo){
			reconciliationId = buyBillRecInfo.getId();
		}
		//查询驳回账单记录
		BigDecimal actualPaymentAmountOld = buyBillRecInfo.getActualPaymentAmount();
		BigDecimal residualPaymentAmountOld = buyBillRecInfo.getResidualPaymentAmount();

		//查询驳回付款记录
		Map<String,Object> paymentOldMap = new HashMap<String,Object>();
		paymentOldMap.put("acceptPaymentId",acceptPaymentId);
		//paymentOldMap.put("paymentStatus","0");
		List<BuyBillReconciliationPayment> recPaymentOld = buyBillReconciliationPaymentMapper.getPaymentListByRecId(paymentOldMap);
		for(BuyBillReconciliationPayment recPayment : recPaymentOld){
			BigDecimal paymentTotalOld = recPayment.getActualPaymentAmount();
			actualPaymentAmountOld = actualPaymentAmountOld.subtract(paymentTotalOld);
			residualPaymentAmountOld = residualPaymentAmountOld.add(paymentTotalOld);
		}

		BuyBillReconciliation buyBillReconciliation = new BuyBillReconciliation();
		buyBillReconciliation.setId(reconciliationId);
		buyBillReconciliation.setPaymentStatus("6");//内部审批驳回
		buyBillReconciliation.setActualPaymentAmount(actualPaymentAmountOld);
		buyBillReconciliation.setResidualPaymentAmount(residualPaymentAmountOld);

		int updatePaymentCount = billReconciliationMapper.updateByPrimaryKeySelective(buyBillReconciliation);
		if(updatePaymentCount > 0){
			Map<String,Object> updatePaymentMap = new HashMap<String,Object>();
			updatePaymentMap.put("acceptPaymentId",acceptPaymentId);
			updatePaymentMap.put("paymentStatus","2");
			//updatePaymentMap.put("returnStatus","0");
			buyBillReconciliationPaymentMapper.updatePaymentInfo(updatePaymentMap);
			
			String orderId=billReconciliationMapper.selectOrderIdbyAcceptId(acceptPaymentId);
			if(!orderId.isEmpty()){
				buyOrderService.changePaymentStatus(orderId,"5");//是否已付款（0：待付款， 1：付款待卖家确认，2：已付款，3部分付款,4.付款审批中，5.内部审批拒绝）
			}

			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//内部审批付款数据查询
	public BuyBillReconciliationPayment queryPaymentDetail(String acceptPaymentId){
		//账单对账详情
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("acceptPaymentId",acceptPaymentId);
		//账单对账详情
		//BuyBillReconciliation buyBillReconciliation = billReconciliationMapper.selectByPrimaryKey(map);
		BuyBillReconciliationPayment billRecPaymentInfo = buyBillReconciliationPaymentMapper.queryRecPaymentInfo(map);
		return billRecPaymentInfo;
	}

	//根据账单号查询付款单详情
	public Map<String,Object> getRecItemInfo(String reconciliationId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		BuyBillReconciliation buyBillReconciliation = selectByPrimaryKey(map);
		int totalSumCount = buyBillReconciliation.getTotalSumCount();
		BigDecimal totalAmount = buyBillReconciliation.getTotalAmount();
		String sellerCompanyId = buyBillReconciliation.getSellerCompanyId();
		String sellerCompanyName = buyBillReconciliation.getSellerCompanyName();
		String reconciliationStatus = buyBillReconciliation.getBillDealStatus();
		String startBillStatementDateStr = buyBillReconciliation.getStartBillStatementDateStr();
		String endBillstatementDateStr = buyBillReconciliation.getEndBillStatementDateStr();
		BigDecimal invoiceTotalSum = new BigDecimal(0);//发票票据总金额

		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<BuyBillReconciliationItem> billRecItemList = buyBillReconciliationItemMapper.queryRecItemList(recItemMap);
		List<Map<String,Object>> recordItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> recordReplaceItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> customerItemList = new ArrayList<Map<String,Object>>();//收获详细

		if(null != billRecItemList && billRecItemList.size()>0){
			for(BuyBillReconciliationItem buyBillReconciliationItem : billRecItemList){
				String recItemId = buyBillReconciliationItem.getId();
				String billRecType = buyBillReconciliationItem.getBillReconciliationType();
				String deliverNo = buyBillReconciliationItem.getDeliverNo();
				String itemId = buyBillReconciliationItem.getItemId();
				String arrivalDateStr = buyBillReconciliationItem.getArrivalDateStr();

				//根据账单详情编号查询发票
				Map<String,Object> invoiceMap = new HashMap<String,Object>();
				invoiceMap.put("recItemId",recItemId);
				BuyBillInvoice billInvoice = buyBillInvoiceMapper.queryInvoiceByRecItem(invoiceMap);
				String invoiceNo = "";
				String invoiceType = "";
				String invoiceFileAddr = "";
				BigDecimal invoiceTotal = new BigDecimal(0);
				if(null != billInvoice){
					 invoiceNo = billInvoice.getInvoiceNo();//票据号
					invoiceTotal = billInvoice.getTotalInvoiceValue();//票据金额
					invoiceTotalSum = invoiceTotalSum.add(invoiceTotal);//票据总额
					invoiceType = billInvoice.getInvoiceType();//票据类型
					invoiceFileAddr = billInvoice.getInvoiceFileAddr();//发票票据
				}

				//根据账单详情编号查询订单号
				Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
				deliveryItemMap.put("deliveryItemId",itemId);
				BuyDeliveryRecordItem deRecItem = buyDeliveryRecordItemMapper.queryOrderCodeByItemId(deliveryItemMap);
				String orderCode = "";
				if(null != deRecItem){
					if(null != deRecItem.getOrderCode() && !"".equals(deRecItem.getOrderCode())){
						orderCode = deRecItem.getOrderCode();
					}
				}

				//根据发货单号查询物流信息
				String waybillNo = "";
				if(null != deliverNo && !"".equals(deliverNo)){
					Map<String,Object> logisctMap = new HashMap<String,Object>();
					logisctMap.put("deliverNo",deliverNo);
					Map<String,Object> logMap = buyDeliveryRecordMapper.queryFreightByRecId(logisctMap);
					if(null != logMap && logMap.size() > 0){
						if(null != logMap.get("waybillNo") && !"".equals(logMap.get("waybillNo"))){
							waybillNo = logMap.get("waybillNo").toString();
						}
					}
				}

				int arrivalNum = buyBillReconciliationItem.getArrivalNum();//数量
				BigDecimal salePrice = buyBillReconciliationItem.getSalePrice();//单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额
				if("1".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					String receiptvoucherAddr = buyBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("itemId",itemId);
					recordAndTtemMap.put("orderCode",orderCode);
					recordAndTtemMap.put("waybillNo",waybillNo);
					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("buyDeliveryRecordItem",buyBillReconciliationItem);
					recordAndTtemMap.put("salePriceSum",salePriceSum);

					recordAndTtemMap.put("invoiceNo",invoiceNo);
					recordAndTtemMap.put("invoiceTotal",invoiceTotal);
					recordAndTtemMap.put("invoiceType",invoiceType);
					recordAndTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					recordItemList.add(recordAndTtemMap);
				}else if("2".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					String receiptvoucherAddr = buyBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("itemId",itemId);
					recordAndTtemMap.put("orderCode",orderCode);
					recordAndTtemMap.put("waybillNo",waybillNo);
					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("buyRecordReplaceItem",buyBillReconciliationItem);
					recordAndTtemMap.put("replacePriceSum",salePriceSum);

					recordAndTtemMap.put("invoiceNo",invoiceNo);
					recordAndTtemMap.put("invoiceTotal",invoiceTotal);
					recordAndTtemMap.put("invoiceType",invoiceType);
					recordAndTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					recordReplaceItemList.add(recordAndTtemMap);
				}else if("3".equals(billRecType)){
					Map<String,Object> customerTtemMap = new HashMap<String,Object>();
					String customerCode = buyBillReconciliationItem.getCustomerCode();
					String proof = buyBillReconciliationItem.getProof();

					customerTtemMap.put("itemId",itemId);
					customerTtemMap.put("orderCode",orderCode);
					customerTtemMap.put("waybillNo",waybillNo);
					customerTtemMap.put("customerCode",customerCode);
					customerTtemMap.put("verifyDateStr",arrivalDateStr);
					customerTtemMap.put("proof",proof);
					customerTtemMap.put("buyCustomerItem",buyBillReconciliationItem);
					customerTtemMap.put("goodsPriceSum",salePriceSum);

					customerTtemMap.put("invoiceNo",invoiceNo);
					customerTtemMap.put("invoiceTotal",invoiceTotal);
					customerTtemMap.put("invoiceType",invoiceType);
					customerTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					customerItemList.add(customerTtemMap);
				}
			}
		}
		//拼装返回值
		Map<String,Object> recordAndReturnMap = new HashMap<String,Object>();
		recordAndReturnMap.put("reconciliationId",reconciliationId);
		recordAndReturnMap.put("totalSumCount",totalSumCount);
		recordAndReturnMap.put("invoiceTotalSum",invoiceTotalSum);
		recordAndReturnMap.put("totalAmount",totalAmount);
		recordAndReturnMap.put("reconciliationStatus",reconciliationStatus);
		recordAndReturnMap.put("sellerCompanyName",sellerCompanyName);
		recordAndReturnMap.put("startBillStatementDateStr",startBillStatementDateStr);
		recordAndReturnMap.put("endBillstatementDateStr",endBillstatementDateStr);
		recordAndReturnMap.put("recordItemList",recordItemList);
		recordAndReturnMap.put("recordReplaceItemList",recordReplaceItemList);
		recordAndReturnMap.put("customerItemList",customerItemList);
		return recordAndReturnMap;
	}

	//根据账单编号查询付款记录
	public List<BuyBillReconciliationPayment> getPaymentList(String reconciliationId){
		Map<String,Object> paymentMap = new HashMap<String,Object>();
		paymentMap.put("reconciliationId",reconciliationId);
		return buyBillReconciliationPaymentMapper.getPaymentListByRecId(paymentMap);
	}
	// 根据条件查询账单信息bean
	public BuyBillReconciliation selectByPrimaryKey(Map<String,Object> map) {
		return billReconciliationMapper.selectByPrimaryKey(map);
	}

	// 根据条件查询账单信息list
	public List<BuyBillReconciliation> selectByPrimaryKeyList(Map<String,Object> map) {
		return billReconciliationMapper.selectByPrimaryKeyList(map);
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(BuyBillReconciliation record) {
		return billReconciliationMapper.updateByPrimaryKeySelective(record);
	}

	//对账详情查看附件
	public String queryFileUrlList(Map<String,Object> queryUrlMap){
		String itemType = queryUrlMap.get("itemTtpe").toString();
		String deliverNo = queryUrlMap.get("deliverNo").toString();
		String attachmentAddrStr = "";
		if("1".equals(itemType)){
			//根据发货单号查询票据附件
			List<Map<String,Object>> attachmentList = buyBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
			if(null != attachmentList && attachmentList.size()>0){
				for(Map<String,Object> attachmentMap : attachmentList){
					attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString()+",";

				}
				attachmentAddrStr = attachmentAddrStr.substring(0,attachmentAddrStr.length()-1);
			}
		}else {
			//售后附件
			Map<String,Object> customerProof = buyBillReconciliationItemMapper.queryCustomerProof(deliverNo);
			if(null != customerProof && customerProof.size()>0){
				attachmentAddrStr = customerProof.get("proof").toString();
			}
		}
		return attachmentAddrStr.trim();
	}

	/**
	 * 对账详情
	 * @param reconciliationId
	 * @param model
	 * @return
	 */
	public void queryRecordAndReturnNew(String reconciliationId,String paymentType, Model model) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		BuyBillReconciliation buyBillReconciliation = billReconciliationMapper.selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<BuyBillReconciliationItem> billRecItemList = buyBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(BuyBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = buyBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = buyBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> recReplaceItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recReplaceItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recReplaceItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recReplaceItemList && recReplaceItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : recReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> daohuoItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> replaceItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> customerItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					customerItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put(logisticsId, itemMap);
				}
			}
		}
		model.addAttribute("buyBillReconciliation", buyBillReconciliation);
		model.addAttribute("daohuoMap", daohuoMap);
		model.addAttribute("recordReplaceMap",recordReplaceMap);
		model.addAttribute("replaceMap", replaceMap);
		model.addAttribute("customerMap", customerMap);
		model.addAttribute("paymentType",paymentType);
	}

	/**
	 * 付款详情导出
	 * @param reconciliationId
	 * @return
	 */
	public Map<String,Object> queryPaymentRecItemExport(String reconciliationId) {
		Map<String,Object> recItemExportMap = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		BuyBillReconciliation buyBillReconciliation = billReconciliationMapper.selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<BuyBillReconciliationItem> billRecItemList = buyBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(BuyBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = buyBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = buyBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> recReplaceItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recReplaceItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recReplaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recReplaceItemList && recReplaceItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : recReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> daohuoItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> replaceItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<BuyBillReconciliationItem> customerItemList = new ArrayList<BuyBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					//customerItemList = (List<BuyBillReconciliationItem>)itemMap.get("itemList");
					/*itemMap.put("waybillNo", item.getWaybillNo());
					itemMap.put("logisticsCompany", item.getLogisticsCompany());
					itemMap.put("driverName", item.getDriverName());
					itemMap.put("freight", item.getFreight());*/

					customerItemList.add(item);

					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(BuyBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put(logisticsId, itemMap);
				}
			}
		}
		recItemExportMap.put("buyBillReconciliation", buyBillReconciliation);
		recItemExportMap.put("daohuoMap", daohuoMap);
		recItemExportMap.put("recordReplaceMap",recordReplaceMap);
		recItemExportMap.put("replaceMap", replaceMap);
		recItemExportMap.put("customerMap", customerMap);
		return recItemExportMap;
	}

	public BuyBillReconciliationPayment queryPaymentDetailNew(String id) {
		//付款详情
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("acceptPaymentId",id);
		//账单对账详情
		BuyBillReconciliationPayment billRecPaymentInfo = buyBillReconciliationPaymentMapper.queryRecPaymentInfo(map);
		return billRecPaymentInfo;
	}
	
	//添加采购单付款数据
	public List<String> insertOrderPayment(String orderId){
		List<String> idList=new ArrayList<>(); 
		BuyBillReconciliationPayment payment=new BuyBillReconciliationPayment();
		payment.setId(ShiroUtils.getUid());
		payment.setPaymentDate(new Date());
		payment.setAcceptPaymentId(ShiroUtils.getUid());
		payment.setPaymentStatus("0");//付款状态：0 付款待确认，1 付款内部已确认, 2 内部审批驳回
		payment.setOrderId(orderId);
		buyBillReconciliationPaymentMapper.insertOrderPayment(payment);
		idList.add(payment.getAcceptPaymentId());
		return idList;
	}
	
	//添加采购单对账数据
	public void insertOrderReconciliation(String orderId){
		BuyOrder order=orderMapper.selectByPrimaryKey(orderId);
		//买家账单主表插入数据
		BuyBillReconciliation reconciliation=new BuyBillReconciliation();
		reconciliation.setId(ShiroUtils.getUid());
		reconciliation.setCreateBillUserName(order.getCreateName());
		reconciliation.setCreateBillUserId(order.getCreateId());
		reconciliation.setCompanyId(ShiroUtils.getCompId());
		reconciliation.setSellerCompanyId(order.getSuppId());
		reconciliation.setStartBillStatementDate(order.getCreateDate());
		reconciliation.setEndBillStatementDate(new Date());
		reconciliation.setFreightSumCount(buyDeliveryRecordMapper.getFreightByOrderId(orderId));//发货总运费
		reconciliation.setRecordConfirmCount(order.getGoodsNum());
		reconciliation.setRecordConfirmTotal(order.getGoodsPrice());
		reconciliation.setCreateDate(new Date());
		reconciliation.setBillDealStatus("2");//'审批状态2：账单待审批 3：已确认对账4：已驳回对账 5：内部审批中7：内部已驳回
		reconciliation.setTotalAmount(order.getGoodsPrice().add(reconciliation.getFreightSumCount()));
		reconciliation.setPurchaseId(ShiroUtils.getUid());
		reconciliation.setResidualPaymentAmount(reconciliation.getTotalAmount());//应付款余额
		reconciliation.setOrderId(orderId);
		
		Map<String,Object> map=new HashMap<>();
		map.put("orderId", orderId);
		//map.put("companyId", ShiroUtils.getCompId());
		List<Map<String,Object>> deliveryList=buyDeliveryRecordItemMapper.selectProductByOrderId(map);
		for (Map<String, Object> map2 : deliveryList) {
			BuyBillReconciliationItem reconciliationItem=new BuyBillReconciliationItem();
			reconciliationItem.setId(ShiroUtils.getUid());
			reconciliationItem.setBuyCompanyId(order.getCompanyId());
			reconciliationItem.setSellerCompanyId(order.getSuppId());
			reconciliationItem.setCreateBillId(order.getCreateId());
			reconciliationItem.setCreateBillName(order.getCreateName());
			reconciliationItem.setReconciliationId(reconciliation.getId());//对账编号
			reconciliationItem.setRecordId(map2.get("deliver_id").toString());
			reconciliationItem.setDeliverNo(map2.get("deliver_no").toString());
			reconciliationItem.setItemId(map2.get("order_item_id").toString());//
			reconciliationItem.setOrderId(map2.get("order_id").toString());//
			reconciliationItem.setBillReconciliationType("1");//订单类型：0 采购换货 1 采购发货，2 售后换货，3 售后退货
			reconciliationItem.setIsNeedInvoice(map2.get("is_need_invoice").toString());
			if("Y".equals(map2.get("is_need_invoice").toString())){
				reconciliation.setIsFullInvoice("0");
			}
			reconciliationItem.setProductCode(map2.get("product_code").toString());
	        reconciliationItem.setProductName(map2.containsKey("product_name")?map2.get("product_name").toString():"");
	        reconciliationItem.setSkuCode(map2.get("sku_code").toString());
	        reconciliationItem.setSkuName(map2.get("sku_name").toString());
	        reconciliationItem.setBarcode(map2.get("barcode").toString());
	        reconciliationItem.setUnitId(map2.get("unit_id").toString());
	        reconciliationItem.setSalePrice(new BigDecimal(map2.get("sale_price").toString()));
	        reconciliationItem.setArrivalNum(Integer.parseInt(map2.get("arrival_num").toString()));
	        DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd");
			try {
				reconciliationItem.setArrivalDate(fmt.parse(map2.get("arrival_date").toString()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
	        reconciliationItem.setLogisticsId(map2.get("logistics_id").toString());
	        BSellerLogistics logistics=logisticsMapper.get(reconciliationItem.getLogisticsId());
	        reconciliationItem.setFreight(logistics.getFreight());//运费
	        //reconciliationItem.setWaybillNo("");//运单号
	        //reconciliationItem.setLogisticsCompany("");//物流公司
	        reconciliationItem.setDriverName("");
	        reconciliationItem.setIsInvoiceStatus("0");//是否已开票  0 未开 1已开
	        reconciliationItem.setReconciliationDealStatus("1");//审批状态
	        buyBillReconciliationItemMapper.addRecItem(reconciliationItem);
		}
		billReconciliationMapper.insertOrderReconciliation(reconciliation);
		
		
		//卖家账单主表添加数据
		BSellerBillReconciliation bs=new BSellerBillReconciliation();
		bs.setId(reconciliation.getId());
		bs.setCreateBillUserName(reconciliation.getCreateBillUserName());
		bs.setCreateBillUserId(reconciliation.getCreateBillUserId());
		bs.setCompanyId(reconciliation.getSellerCompanyId());
		bs.setBuyCompanyId(reconciliation.getCompanyId());
		bs.setStartBillStatementDate(reconciliation.getStartBillStatementDate());
		bs.setEndBillStatementDate(reconciliation.getEndBillStatementDate());
		bs.setFreightSumCount(reconciliation.getFreightSumCount());//发货总运费
		bs.setRecordConfirmCount(reconciliation.getRecordConfirmCount());
		bs.setRecordConfirmTotal(reconciliation.getRecordConfirmTotal());
		bs.setCreateDate(reconciliation.getCreateDate());
		bs.setBillDealStatus("2");//'审批状态2：账单待审批 3：已确认对账4：已驳回对账 5：内部审批中7：内部已驳回
		bs.setTotalAmount(reconciliation.getTotalAmount());
		bs.setResidualPaymentAmount(reconciliation.getResidualPaymentAmount());//应付款余额
		bs.setOrderId(reconciliation.getOrderId());
		bs.setIsFullInvoice(reconciliation.getIsFullInvoice());
		bSellerBillReconciliationMapper.insertOrderReconcilion(bs);
	}
}
