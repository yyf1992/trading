package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author dxl"
 * @date 2018-01-02 09:11:33
 */
@Data
public class BuyApplypurchaseShop implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键id
	private String id;
	//itemId
	private String itemId;
	//plan_code
	private String planCode;
	//店铺id
	private String shopId;
	//店铺code
	private String shopCode;
	//店铺名称
	private String shopName;
	//销售开始时间
	private Date saleStartDate;
	//销售结束时间
	private Date saleEndDate;
	//销售天数
	private Integer saleDays;
	//销售计划
	private Integer salesNum;
	//确认销售计划
	private Integer confirmSalesNum;
	//入仓量
	private Integer putStorageNum;
	//确认入仓量
	private Integer confirmPutStorageNum;
}
