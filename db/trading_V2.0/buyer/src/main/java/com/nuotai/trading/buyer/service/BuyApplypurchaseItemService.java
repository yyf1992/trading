package com.nuotai.trading.buyer.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.nuotai.trading.buyer.dao.BuyApplypurchaseItemMapper;
import com.nuotai.trading.buyer.model.BuyApplypurchaseItem;
import com.nuotai.trading.buyer.model.BuySalePlanHeader;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.OutWhareaLog;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.OutWhareaLogService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Service
public class BuyApplypurchaseItemService implements BuyApplypurchaseItemMapper{

	@Autowired
	private BuyApplypurchaseItemMapper mapper;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private BuyShopProductService shopProductService;
	@Autowired
	private BuySalePlanHeaderService salePlanHeaderService;
	@Autowired
	private BuySalePlanItemService salePlanItemService;
	@Autowired
	private OutWhareaLogService outWhareaLogService;
	@Autowired
	private BuyProductSkuService productSkuService;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BuyApplypurchaseItem record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(BuyApplypurchaseItem record) {
		return mapper.insertSelective(record);
	}

	@Override
	public BuyApplypurchaseItem selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKey(record);
	}
	
	@Override
	public int updateByPrimaryKeyAndBarcode(BuyApplypurchaseItem record) {
		return mapper.updateByPrimaryKeyAndBarcode(record);
	}
	
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByPage(SearchPageUtil searchPageUtil) {
		return mapper.selectAllProductByPage(searchPageUtil);
	}
	
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByNoPage(Map<String, Object> map) {
		return mapper.selectAllProductByNoPage(map);
	}

	@Override
	public List<Map<String, Object>> selectProductByBarcode(Map<String, Object> map) {
		//显示剩余要采购的商品
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> detailsList = mapper.selectProductByBarcode(map);
		if(detailsList != null && detailsList.size() > 0){
			for(Map<String, Object> mapResult : detailsList){
				// 用编号和条形码查询已下单商品数量
				Map<String,Object> mapPare = new HashMap<String,Object>();
				mapPare.put("companyId", ShiroUtils.getCompId());
				mapPare.put("applyCode", mapResult.get("apply_code"));
				mapPare.put("skuOid", mapResult.get("barcode"));
				Map<String,Object> mapAlready = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapPare);
				if(mapAlready != null){
					int alreadyPlanPurchaseNum = mapAlready.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(mapAlready.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapPare);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapPare);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					//已驳回
					Map<String,Object> mapReject = orderProductService.selectRejectPlanPurchaseNumBySkuOid(mapPare);
					if(mapReject != null){
						int rejectPurchaseNum = mapReject.get("rejectPurchaseNum") == null ? 0 : Integer.parseInt(mapReject.get("rejectPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - rejectPurchaseNum;
					}
					if(alreadyPlanPurchaseNum < Integer.parseInt(mapResult.get("apply_count").toString())){
						// 查询已下单未审核数量
						Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
						if(mapLock != null){
							int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum - lockGoodsNumber);
						}else{
							mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()) - alreadyPlanPurchaseNum);
						}
					}else{
						mapResult.put("overplusNum", 0);
					}
				}else{
					// 查询已下单未审核数量
					Map<String,Object> mapLock = orderProductService.selectLockGoodsnumberBySkuOid(mapPare);
					if(mapLock != null){
						int lockGoodsNumber = mapLock.get("lockGoodsNumber") == null ? 0 : Integer.parseInt(mapLock.get("lockGoodsNumber").toString());
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString())  - lockGoodsNumber);
					}else{
						mapResult.put("overplusNum", Integer.parseInt(mapResult.get("apply_count").toString()));
					}
				}
				if(Integer.parseInt(mapResult.get("overplusNum").toString()) != 0){//剩余采购数量为0的不显示
					resultList.add(mapResult);
				}
			}
		}
		return resultList;
	}

	@Override
	public List<BuyApplypurchaseItem> selectByHeaderId(String applyId) {
		return mapper.selectByHeaderId(applyId);
	}

	@Override
	public List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrder(
			Map<String, Object> mapPare) {
		Map<String, Object> mapP = new HashMap<String, Object>();
		// 已计划采购量
		List<BuyApplypurchaseItem> itemList = mapper.selectAllProductByBeatchCreateOrder(mapPare);
		if(itemList != null && itemList.size() > 0){
			for(BuyApplypurchaseItem item : itemList){
				mapP.put("skuOid", item.getBarcode());
				mapP.put("companyId", ShiroUtils.getCompId());
				Map<String,Object> map = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapP);
				if(map != null){
					int alreadyPlanPurchaseNum = map.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(map.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapP);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapP);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					//已驳回
					Map<String,Object> mapReject = orderProductService.selectRejectPlanPurchaseNumBySkuOid(mapP);
					if(mapReject != null){
						int rejectPurchaseNum = mapReject.get("rejectPurchaseNum") == null ? 0 : Integer.parseInt(mapReject.get("rejectPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - rejectPurchaseNum;
					}
					item.setAlreadyPlanPurchaseNum(alreadyPlanPurchaseNum);
				}else{
					item.setAlreadyPlanPurchaseNum(0);
				}
				//根据商品条形码查询关联的供应商列表
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
				params.put("linktype", "0");
				params.put("status", "3");//状态已通过
				params.put("searchCompanyId", ShiroUtils.getCompId());
				params.put("skuOid", item.getBarcode());
				List<Map<String,Object>> supplierLinksList = shopProductService.loadProductLinksByMap(params);
				if(supplierLinksList != null && supplierLinksList.size() > 0){
					for(Map<String,Object> supplierLinks : supplierLinksList){
						//产能
						int dailyOutput = Integer.parseInt(supplierLinks.get("dailyOutput").toString());
						//到货天数
						int storageDay = Integer.parseInt(supplierLinks.get("storageDay").toString());
						int goodsNum = item.getApplyCount() - item.getAlreadyPlanPurchaseNum() - item.getLockGoodsNumber();
						int c = 0;
						if(dailyOutput != 0){
							c = goodsNum%dailyOutput == 0 ? (goodsNum/dailyOutput) : (goodsNum/dailyOutput)+1;
						}
						//数量/产能+到货天数+审批天数
						int dayNum = c + storageDay + Constant.TRADEVERIFYDAY;
						SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.DATE, dayNum);//计算dayNum天后的时间
						String str2 = s.format(calendar.getTime());
						supplierLinks.put("predictArred", str2);
					}
					item.setSupplierLinksList(supplierLinksList);
				}
			}
		}
		return itemList;
	}

	@Override
	public List<Map<String, Object>> selectProductByStatistic(
			Map<String, Object> map) {
		return mapper.selectProductByStatistic(map);
	}
	
	public void getProductTypeNum(Map<String, Object> params) {
		//成品
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue()+"");
		selectMap.put("companyId",ShiroUtils.getCompId());
		selectMap.put("productType",Constant.ProductType.PRODUCT.getValue()+"");
		int finishedProductNum = getWaitOrderNum(selectMap);
		//原材料
		selectMap.put("productType", Constant.ProductType.MATERIAL.getValue()+"");
		int rawStockNum = getWaitOrderNum(selectMap);
		//辅料
		selectMap.put("productType", Constant.ProductType.ACCESSORIES.getValue()+"");
		int accessoriesNum = getWaitOrderNum(selectMap);
		//虚拟产品
		selectMap.put("productType", Constant.ProductType.VIRTUALPRODUCTS.getValue()+"");
		int inventedNum = getWaitOrderNum(selectMap);
		params.put("finishedProductNum", finishedProductNum);
		params.put("rawStockNum", rawStockNum);
		params.put("accessoriesNum", accessoriesNum);
		params.put("inventedNum", inventedNum);
	}
	
	public String loadPurchaseProductListData(SearchPageUtil searchPageUtil,Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue()+"");
		params.put("companyId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		String returnPath = "platform/buyer/applypurchase/productSum/";
		switch (tabId) {
		case 0://成品
			params.put("interest", "0");
			returnPath += "finishedProduct";
			break;
		case 1://原材料
			params.put("interest", "1");
			returnPath += "rawStock";
			break;
		case 2://辅料
			params.put("interest", "2");
			returnPath += "accessories";
			break;
		case 3://虚拟产品
			params.put("interest", "3");
			returnPath += "invented";
			break;
		default:
			returnPath += "finishedProduct";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("0".equals(interest)){
			//成品
			params.put("productType",Constant.ProductType.PRODUCT.getValue()+"");
		}else if("1".equals(interest)){
			//原材料
			params.put("productType", Constant.ProductType.MATERIAL.getValue()+"");
		}else if("2".equals(interest)){
			//辅料
			params.put("productType", Constant.ProductType.ACCESSORIES.getValue()+"");
		}
		else if("3".equals(interest)){
			//虚拟产品
			params.put("productType", Constant.ProductType.VIRTUALPRODUCTS.getValue()+"");
		}
		searchPageUtil.setObject(params);
		List<Map<String,Object>> purchaseList = getWaitOrderByPage(searchPageUtil);
		searchPageUtil.getPage().setList(purchaseList);
		return returnPath;
	}

	/**
	 * 查询定制商品修改后的价格
	 */
	@Override
	public List<BuyApplypurchaseItem> selectAllProductByBeatchCreateOrderAndaddCustomPrice(Map<String, Object> mapPare) {

		Map<String, Object> mapP = new HashMap<String, Object>();
		// 已计划采购量
		List<BuyApplypurchaseItem> itemList = mapper.selectAllProductByBeatchCreateOrderAndaddCustomPrice(mapPare);
		if(itemList != null && itemList.size() > 0){
			for(BuyApplypurchaseItem item : itemList){
				mapP.put("skuOid", item.getBarcode());
				mapP.put("companyId", ShiroUtils.getCompId());
				Map<String,Object> map = orderProductService.selectAlreadyPlanPurchaseNumBySkuOid(mapP);
				if(map != null){
					int alreadyPlanPurchaseNum = map.get("alreadyPlanPurchaseNum") == null ? 0 : Integer.parseInt(map.get("alreadyPlanPurchaseNum").toString());
					//已取消
					Map<String,Object> mapCancel = orderProductService.selectCancelPlanPurchaseNumBySkuOid(mapP);
					if(mapCancel != null){
						int cancelPurchaseNum = mapCancel.get("cancelPurchaseNum") == null ? 0 : Integer.parseInt(mapCancel.get("cancelPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - cancelPurchaseNum;
					}
					//已终止
					Map<String,Object> mapStop = orderProductService.selectStopPlanPurchaseNumBySkuOid(mapP);
					if(mapStop != null){
						int stopPurchaseNum = mapStop.get("stopPurchaseNum") == null ? 0 : Integer.parseInt(mapStop.get("stopPurchaseNum").toString());
						alreadyPlanPurchaseNum = alreadyPlanPurchaseNum - stopPurchaseNum;
					}
					item.setAlreadyPlanPurchaseNum(alreadyPlanPurchaseNum);
				}else{
					item.setAlreadyPlanPurchaseNum(0);
				}
				//根据商品条形码查询关联的供应商列表
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("isDel", Constant.IsDel.NODEL.getValue() + "");
				params.put("linktype", "0");
				params.put("status", "3");//状态已通过
				params.put("searchCompanyId", ShiroUtils.getCompId());
				params.put("skuOid", item.getBarcode());
				List<Map<String,Object>> supplierLinksList = shopProductService.loadProductLinksByMap(params);
				if(supplierLinksList != null && supplierLinksList.size() > 0){
					item.setSupplierLinksList(supplierLinksList);
				}
			}
		}
		return itemList;
	}
	
	public Map<String,Object> selectAlreadyPlanNumBySkuOid(Map<String, Object> map) {
		return mapper.selectAlreadyPlanNumBySkuOid(map);
	}
	
	public Map<String,Object> selectCancelPlanNumBySkuOid(Map<String, Object> map) {
		return mapper.selectCancelPlanNumBySkuOid(map);
	}

	@Override
	public int getWaitOrderNum(Map<String, Object> map) {
		return mapper.getWaitOrderNum(map);
	}

	@Override
	public List<Map<String, Object>> getWaitOrderByPage(SearchPageUtil searchPageUtil) {
		return mapper.getWaitOrderByPage(searchPageUtil);
	}
	
	@Override
	public List<Map<String, Object>> getWaitOrderByNoPage(Map<String, Object> map) {
		return mapper.getWaitOrderByNoPage(map);
	}
	
	@Override
	public List<Map<String,Object>> getProductGroupByMap(Map<String,Object> map){
		List<Map<String,Object>> planList = mapper.getProductGroupByMap(map);
		if(planList != null && planList.size() > 0){
			for(Map<String,Object> planMap : planList){
				String barcode = planMap.get("barcode").toString();
				//取杉橙商品成本价
				//String levelUrl = Constant.OMS_INTERFACE_URL + "getGoods?skuoid=" + planMap.get("barcode").toString();
				String levelUrl = "http://121.199.179.23:30003/oms_interface/getGoods?skuoid=" + barcode;
				String arrayString = InterfaceUtil.searchLoginService(levelUrl);
				if(arrayString != null){
					net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(arrayString);
					net.sf.json.JSONArray productArray = net.sf.json.JSONArray.fromObject(jsonObject.get("list"));
					List<net.sf.json.JSONObject> list = (List<net.sf.json.JSONObject>) net.sf.json.JSONArray
							.toCollection(productArray, net.sf.json.JSONObject.class);
					if(list != null && list.size() > 0){
						net.sf.json.JSONObject stockObject = list.get(0);
						double cost = stockObject.getDouble("cost");
						planMap.put("cost", cost);
						//质检仓
						String stockMapStr = stockObject.getString("stockMap");
						Map<String, Object> mapTypes = JSON.parseObject(stockMapStr); 
						int stockZZC = 0;
						String strZZC = mapTypes.get("质检仓") == null ? "" : mapTypes.get("质检仓").toString();
						if(!strZZC.equals("")){
							strZZC = strZZC.replace("[", "");
							strZZC = strZZC.replace("]", "");
							stockZZC = Integer.parseInt(strZZC.split(",")[0])+ Integer.parseInt(strZZC.split(",")[1]);
						}
						planMap.put("stockZZC", stockZZC);
						//大仓
						int stockDC = 0;
						String strDC = mapTypes.get("泰安大仓") == null ? "" : mapTypes.get("泰安大仓").toString();
						if(!strDC.equals("")){
							strDC = strDC.replace("[", "");
							strDC = strDC.replace("]", "");
							stockDC = Integer.parseInt(strDC.split(",")[0])+ Integer.parseInt(strDC.split(",")[1]);
						}
						planMap.put("stockDC", stockDC);
					}else{
						planMap.put("cost", 0);
						planMap.put("stockZZC", 0);
						planMap.put("stockDC", 0);
					}
				}else{
					planMap.put("cost", 0);
					planMap.put("stockZZC", 0);
					planMap.put("stockDC", 0);
				}
				//需求时间
				String salePlanItemId = planMap.get("sale_plan_item_id") == null ? "" : planMap.get("sale_plan_item_id").toString();
				if(!"".equals(salePlanItemId)){
					BuySalePlanItem si = salePlanItemService.get(salePlanItemId);
					if(si != null){
						BuySalePlanHeader sh = salePlanHeaderService.get(si.getHeaderId());
						planMap.put("requiredTime", sh.getCreateDate());
					}
				}
				//在途数量
				Map<String,Object> mapParem = new HashMap<String, Object>();
				mapParem.put("companyId", ShiroUtils.getCompId());
				mapParem.put("skuOid", barcode);
				String unArrivalNum2 = orderProductService.selectOrderUnArrivalNumByBarcode(mapParem);
				if(unArrivalNum2 == null){
					unArrivalNum2 = "0";
				}
				planMap.put("transitNum", Integer.parseInt(unArrivalNum2));//在途订单数量
				// 外仓数量查询
				Map<String,Object> mapOWL = new HashMap<String,Object>();
				mapOWL.put("companyId", ShiroUtils.getCompId());
				mapOWL.put("barcode", barcode);
				int stockJD = 0;//京东
				mapOWL.put("whareaName", "京东仓");
				List<OutWhareaLog> outWhareaLogList = outWhareaLogService.queryByMap(mapOWL);
				if(outWhareaLogList != null && outWhareaLogList.size() > 0){
					OutWhareaLog owlJD = outWhareaLogList.get(0);
					stockJD = owlJD.getOutWhareaStock() + owlJD.getOutWhareaWayStock();
					planMap.put("stockJD", stockJD);
				}else{
					planMap.put("stockJD", 0);
				}
				int stockCN = 0;//菜鸟
				mapOWL.put("whareaName", "菜鸟仓");
				outWhareaLogList = outWhareaLogService.queryByMap(mapOWL);
				if(outWhareaLogList != null && outWhareaLogList.size() > 0){
					OutWhareaLog owlCN = outWhareaLogList.get(0);
					stockCN = owlCN.getOutWhareaStock() + owlCN.getOutWhareaWayStock();
					planMap.put("stockCN", stockCN);
				}else{
					planMap.put("stockCN", 0);
				}
				int stockMC = 0;//猫超
				
				planMap.put("stockMC", stockMC);
				planMap.put("stockTotal", Integer.parseInt(planMap.get("stockZZC").toString()) 
						+ Integer.parseInt(planMap.get("stockDC").toString())
						+ Integer.parseInt(planMap.get("stockJD").toString())
						+ Integer.parseInt(planMap.get("stockCN").toString())
						+ Integer.parseInt(planMap.get("stockMC").toString()));//合计库存总数
				//安全库存数
				BuyProductSku ps =  productSkuService.getByBarcode(mapOWL);
				planMap.put("safeStock", ps.getStandardStock()==null?0:ps.getStandardStock());
				//可用库存数(合计库存总数-安全库存数)
				planMap.put("stockUsable", Integer.parseInt(planMap.get("stockTotal").toString())-
						Integer.parseInt(planMap.get("safeStock").toString()));
				//采购计划数(销售需求数-可用库存数-在途数量)
				planMap.put("purchaseNum", Integer.parseInt(planMap.get("confirm_sale_plan")==null?"0":planMap.get("confirm_sale_plan").toString())-
						Integer.parseInt(planMap.get("stockUsable").toString())-
						Integer.parseInt(planMap.get("transitNum").toString()));
				//金额（采购计划数*单价）
				double a = Double.parseDouble(planMap.get("purchaseNum").toString());
				double b = Double.parseDouble(planMap.get("cost").toString());
				double result = a * b;
				BigDecimal   r   =   new   BigDecimal(result);  
				planMap.put("price", r.setScale(4,   BigDecimal.ROUND_HALF_UP).doubleValue());
			}
		}
		return planList;
	}
}
