package com.nuotai.trading.buyer.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nuotai.trading.buyer.dao.BuyOrderMapper;
import com.nuotai.trading.buyer.model.BuySupplierAssessment;
import com.nuotai.trading.buyer.service.BuyCustomerItemService;
import com.nuotai.trading.buyer.service.BuyDeliveryRecordItemService;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.buyer.service.BuySupplierAssessmentService;
import com.nuotai.trading.buyer.service.BuySupplierService;
import com.nuotai.trading.buyer.service.seller.BSellerCustomerService;
import net.sf.json.JSONArray;

import org.apache.commons.collections4.map.LinkedMap;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierLinkman;
import com.nuotai.trading.service.BuySupplierLinkmanService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Controller
@RequestMapping("platform/buyer/supplier")
public class BuySupplierController extends BaseController{
	private static final Logger LOG = LoggerFactory.getLogger(BuySupplierController.class);
	@Autowired
	private BuySupplierService buySupplierService;
	@Autowired
	private BuySupplierLinkmanService buySupplierLinkmanService;
	@Autowired
	private BuyOrderService buyOrderService;
	@Autowired
	private BuyOrderMapper buyOrderMapper;
	@Autowired
	private BuyDeliveryRecordItemService buyDeliveryRecordItemService;
	@Autowired
	private BuyCustomerItemService customerItemService;
	@Autowired
	private BSellerCustomerService buBSellerCustomerService;
	@Autowired
	private BuySupplierAssessmentService buySupplierAssessmentService;
	
	/**
	 * 供应商列表页面
	 * @param searchPageUtil
	 * @return
	 */
	@RequestMapping("supplierList")
	public String supplierList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map){
		String linktype=map.containsKey("linktype")?map.get("linktype").toString():"0";
		String returnUrl="";
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(map);
		List<Map<String,Object>> supplierList = buySupplierService.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(supplierList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		if("2".equals(linktype)){//外协列表
			returnUrl="platform/sellers/assist/assistList";
		}else{//供应商列表
			returnUrl="platform/buyer/supplier/supplierIndex";
		}
		return returnUrl;
	}
	
	/**
	 * 跳转到添加供应商页面
	 * @author:     dxl
	 * @return
	 */
	@RequestMapping("addSupplier")
	public String addSupplier(){
		return "platform/buyer/supplier/addSupplier";
	}
	
	/**
	 * 手动添加供应商保存
	 * @param supplier
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveAddSupplier", method = RequestMethod.POST)
	@ResponseBody
	public String saveAddSupplier(BuySupplier supplier,@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveAddSupplier(supplier,map);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 添加成功页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/addSucc")
	public String addSucc(HttpServletRequest request){
		return "platform/buyer/supplier/addSucc";
	}
	
	/**
	 * 删除供应商保存
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveDeleteSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveDeleteSupplier(String id){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveDeleteSupplier(id);
			json.put("success", true);
			json.put("msg", "删除成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "删除失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 冻结供应商保存
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveFrozenSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveFrozenSupplier(String id,String suppName,String frozenReason,int status){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveFrozenSupplier(id,suppName,frozenReason,status);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 批量删除供应商
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/saveBeatchDeleteSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveBeatchDeleteSupplier(String ids){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveBeatchDeleteSupplier(ids);
			json.put("success", true);
			json.put("msg", "删除成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "删除失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 加载查询供应商详情页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loadSupplierDetails")
	public String loadSupplierDetails(String id) throws Exception{
		BuySupplier supp = buySupplierService.selectByPrimaryKey(id);
		model.addAttribute("supplier", supp);
		// 供应商联系人信息
		List<BuySupplierLinkman> linkmanList = buySupplierLinkmanService.selectBySupplierId(id);
		model.addAttribute("linkmanList", linkmanList);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/supplier/loadSupplierDetails";
	}
	
	/**
	 * 跳转到修改供应商页面
	 * @author:     dxl
	 * @return
	 */
	@RequestMapping("/updateSupplier")
	public String updateSupplier(String id){
		BuySupplier supp = buySupplierService.selectByPrimaryKey(id);
		model.addAttribute("supplier", supp);
        // 供应商联系人信息
 		List<BuySupplierLinkman> linkmanList = buySupplierLinkmanService.selectBySupplierId(id);
 		model.addAttribute("linkmanList", linkmanList);
		return "platform/buyer/supplier/updateSupplier";
	}
	
	/**
	 * 修改供应商保存
	 * @param supplier
	 * @return
	 */
	@RequestMapping(value="/saveUpdateSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdateSupplier(BuySupplier supplier,@RequestParam Map<String ,Object> map){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveUpdateSupplier(supplier,map);
			json.put("success", true);
			json.put("msg", "保存成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 供应商信息导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("exportSupplierData")
	@RequestMapping(value = "/exportSupplierData")
	public void exportSupplierData(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		String reportName = "供应商信息表";
		long currTime = System.currentTimeMillis();
		// 内存中缓存记录行数
		int rowaccess = 100;
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		// 下边框
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		// 左边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		// 右边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
		// 上边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 字体左右居中
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		// 下边框
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		// 左边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		// 右边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		// 上边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setFont(font2);
		String[] title = {"供应商名称","联系人","手机号","电话","类型","地址","创建日期"};
		// 大标题
		Row row = sheet.createRow(0);
		// 合并单元格
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		// 设置列宽
		sheet.setColumnWidth(0, 4500);
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0 || i == 5 || i == 6 ){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4500);
			}
		}
		int startRow = 2;
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("friendId", ShiroUtils.getCompId());
		List<Map<String,Object>> suppList = buySupplierService.selectSupplierByMap(map);
		if (suppList != null && suppList.size() > 0) {
			for (int i = 0; i < suppList.size(); i++) {
				Map<String,Object> item = suppList.get(i);
				// 创建行s
				row = sheet.createRow(startRow);
				// 供应商名称
				String supplierName = item.containsKey("supp_name") ? item.get("supp_name").toString() : "";
				Cell cell = row.createCell(0);
				cell.setCellValue(supplierName);
				cell.setCellStyle(cellStyle);
				
				// 联系人
				String person = item.containsKey("person") ? item.get("person").toString() : "";
				cell = row.createCell(1);
				cell.setCellValue(person);
				cell.setCellStyle(cellStyle);
				
				// 手机号
				String phone = item.containsKey("phone") ? item.get("phone").toString() : "";
				cell = row.createCell(2);
				cell.setCellValue(phone);
				cell.setCellStyle(cellStyle);
				
				// 电话
				String zone = item.containsKey("zone") ? item.get("zone").toString() : "";
				String telNo = item.containsKey("tel_no") ? item.get("tel_no").toString() : "";
				cell = row.createCell(3);
				cell.setCellValue(zone+"-"+telNo);
				cell.setCellStyle(cellStyle);
				
				// 类型
				String type="";
				if("0".equals(item.get("type"))){
					type+="互通";
				}else{
					type+="非互通";
				}
				cell = row.createCell(4);
				cell.setCellValue(type);
				cell.setCellStyle(cellStyle);
				
				// 地址
				String address = item.containsKey("address") ? item.get("address").toString() : "";
				cell = row.createCell(5);
				cell.setCellValue(address);
				cell.setCellStyle(cellStyle);
				
				SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				// 创建日期
				cell = row.createCell(6);
				cell.setCellValue(sdf.format(item.get("create_date")));
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
				}
		}
		LOG.debug("耗时:" + (System.currentTimeMillis() - currTime) / 1000);
//		System.out.println("耗时:" + (System.currentTimeMillis() - currTime) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
	
	/**
	 * 加载供应商使用率界面
	 * @return
	 */
	@RequestMapping(value="/loadSupplierUsageRateHtml")
	public String getSupplierUsageRateHtml(@RequestParam Map<String,Object> map){
		map.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(map.get("startDate")==null && map.get("startDate")==null){
			map.put("startDate",weekAgo);
			map.put("endDate", now);
		}
		List<Map<String,Object>> supplierList=buyOrderService.getSupplierUsageRate(map);
		int sum=0;
		for (Map<String, Object> supplier : supplierList) {
			if(supplier.get("order_num") != null || "".equals(supplier.get("order_num"))){
				sum+=Integer.parseInt((supplier.get("order_num").toString()));
			}
		}
		model.addAttribute("map",map);
		model.addAttribute("sum",sum);
		model.addAttribute("supplierList",net.sf.json.JSONArray.fromObject(supplierList));
		return "platform/buyer/supplier/supplierUsageRate";
	}
	
	/**
	 * 加载供应商到货及时率界面
	 * @return
	 */
	@RequestMapping(value="getSupplierArrivalRateHtml")
	public String getSupplierArrivalRate(@RequestParam Map<String,Object> map){
		map.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(map.get("startDate")==null && map.get("endDate")==null){
			map.put("startDate",weekAgo);
			map.put("endDate",now);
		}
		Map<String,net.sf.json.JSONObject> arrivalList=getSupplierArrivalDate(map);
		model.addAttribute("map",map);
		model.addAttribute("arrivalList",net.sf.json.JSONObject.fromObject(arrivalList));
		return "platform/buyer/supplier/supplierArrivalRate";
	}
	
	/**
	 * 加载供应商计划完成率界面
	 * @return
	 */
	@RequestMapping(value="getSupplierCompletionRate")
	public String getSupplierCompletionRate(@RequestParam Map<String,Object> param){
		param.put("companyId", ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(param.get("startDate")==null && param.get("endDate")==null){
			param.put("startDate",weekAgo);
			param.put("endDate", now);
		}
		List<Map<String,Object>> completionList=buyOrderMapper.getSupplierCompletionRate(param);
		model.addAttribute("map",param);
		model.addAttribute("supplierList",net.sf.json.JSONArray.fromObject(completionList));
		return "platform/buyer/supplier/supplierCompletionRate";
	}
	
	/**
	 * 加载供应商合格率界面
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/loadSupplierQualificationRateHtml")
	public String getSupplierQualificationRateHtml(@RequestParam Map<String,Object> amap){
		if(!amap.containsKey("companyId")){
			amap.put("companyId", ShiroUtils.getCompId());
		}
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(amap.get("startDate")==null && amap.get("endDate")==null){
			amap.put("startDate",weekAgo);
			amap.put("endDate", now);
		}
		Map<String,net.sf.json.JSONObject> map=getSupplierQualificationData(amap);
		model.addAttribute("map",net.sf.json.JSONObject.fromObject(map));
		model.addAttribute("amap",amap);
		return "platform/buyer/supplier/supplierQualificationRate";
	}
	
	/**加载供应商售后响应及时率界面
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/loadSupplierAfterSaleRateHtml")
	public String loadSupplierAfterSaleRateHtml(@RequestParam Map<String,Object> amap){
		if(!amap.containsKey("companyId")){
			amap.put("companyId", ShiroUtils.getCompId());
		}
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(amap.get("startDate")==null && amap.get("endDate")==null){
			amap.put("startDate",weekAgo);
			amap.put("endDate", now);
		}
		Map<String,net.sf.json.JSONObject> map=getSupplierAfterSaleData(amap);
		model.addAttribute("map",net.sf.json.JSONObject.fromObject(map));
		model.addAttribute("amap",amap);
		return "platform/buyer/supplier/supplierAfterSaleRate";
	}
	
	/**
	 * 加载供应商绩效考核界面
	 * @return
	 */
	@RequestMapping(value="/loadSupplierPerformanceAppraisalHtml")
	public String loadSupplierPerformanceAppraisalHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object>param){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		param.put("companyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(param);
		List <BuySupplierAssessment> assesmentList=buySupplierAssessmentService.queryList(param);
		searchPageUtil.getPage().setList(assesmentList);
		model.addAttribute("searchPageUtil",searchPageUtil);
		return "platform/buyer/supplier/supplierPerformanceAppraisal";
	}
	
	/**
	 * 加载新增供应商绩效考核界面
	 * @return
	 */
	@RequestMapping(value="/loadAddSupplierAssesmentHtml")
	public String loadAddSupplierAssesmentHtml(){
		return "platform/buyer/supplier/addSupplierAssesment";
	}
	
	/**
	 * 保存新增供应商绩效考核界面
	 * @return
	 */
	@RequestMapping(value="/saveInsertAssesment", method = RequestMethod.POST)
	@ResponseBody
	public String saveInsertAssesment(BuySupplierAssessment assesment){
		JSONObject json = new JSONObject();
		try {
			buySupplierAssessmentService.insert(assesment);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 加载编辑供应商绩效考核界面
	 * @return
	 */
	@RequestMapping(value="/loadEditSupplierAssesmentHtml")
	public String loadEditSupplierAssesmentHtml(String id){
		BuySupplierAssessment assesment=buySupplierAssessmentService.get(id);
		model.addAttribute("assesment",assesment);
		return "platform/buyer/supplier/editSupplierAssesment";
	}
	
	/**
	 * 保存编辑供应商绩效考核
	 * @return
	 */
	@RequestMapping(value="/saveEditSupplierAssesment",method=RequestMethod.POST)
	@ResponseBody
	public String saveEditSupplierAssesment(BuySupplierAssessment assesment){
		JSONObject json=new JSONObject();
		try {
			buySupplierAssessmentService.updateAssesment(assesment);
				json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toJSONString();
	}
	
	/**
	 * 加载绩效考核得分界面
	 * @param param
	 * @param assesmentId
	 * @return
	 */
	@RequestMapping(value="/loadSupplierPerformanceScoreHtml")
	public String loadSupplierPerformanceHtml(@RequestParam Map<String,Object> pmap){
		pmap.put("companyId",ShiroUtils.getCompId());
		Calendar cal = Calendar.getInstance();
		String now = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		cal.add(Calendar.DATE, -7);
		String weekAgo = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		if(pmap.get("startDate")==null && pmap.get("endDate")==null){
			pmap.put("startDate",weekAgo);
			pmap.put("endDate", now);
		}
		pmap.put("state",0);
		List <BuySupplierAssessment> assesmentList=buySupplierAssessmentService.queryList(pmap);
		String assesmentId ="";
		if(pmap.containsKey("assesmentId") && !"".equals(pmap.get("assesmentId"))){
			assesmentId=pmap.get("assesmentId").toString();
		}else if(assesmentList.size()>0){
			assesmentId=assesmentList.get(0).getId();
		}else{
			model.addAttribute("msg","暂无考核标准，请转到供应商绩效考核模块新增考核项！");
			return "platform/buyer/supplier/supplierPerformanceScore";
		}
		BuySupplierAssessment assesment=buySupplierAssessmentService.get(assesmentId);
		//及时率
		Map<String,net.sf.json.JSONObject> arrivalMap=getSupplierArrivalDate(pmap);
		//完成率
		List<Map<String,Object>> completionList=buyOrderMapper.getSupplierCompletionRate(pmap);
		//合格率
		Map<String,net.sf.json.JSONObject> qualificationMap=getSupplierQualificationData(pmap);
		//售后响应及时率
		Map<String,net.sf.json.JSONObject> afterSaleMap=getSupplierAfterSaleData(pmap);
		
		Map<String,net.sf.json.JSONObject> performanceMap=new HashMap<>();
		//计算及时率得分
		for (Map.Entry<String, net.sf.json.JSONObject> json : arrivalMap.entrySet()) {
			String supplierId=json.getKey();
			net.sf.json.JSONObject obj=json.getValue();
			String supplierName=obj.getString("supp_name");
			int a=obj.getInt("successNum");
			int b=obj.getInt("orderSum");
			double rate=a/(b*1.0);
			if(performanceMap.containsKey(supplierId)){
				net.sf.json.JSONObject old=performanceMap.get(supplierId);
				if(assesment.getScoreType()==0){
					old.put("arrivalScore", rate*assesment.getArrivalRateScore());
				}else if(assesment.getScoreType()==1){
					old.put("arrivalScore", rate*assesment.getArrivalRateScore()*assesment.getScore()/100);
				}else{
					old.put("arrivalScore", 0.00);
				}
				performanceMap.put(supplierId, old);
			}else{
				net.sf.json.JSONObject newJson=new net.sf.json.JSONObject();
				newJson.put("supplierId", supplierId);
				newJson.put("supplierName", supplierName);
				newJson.put("score", assesment.getScore());
				newJson.put("compleScore", 0.00);
				newJson.put("qualityScore", 0.00);
				newJson.put("afterScore", 0.00);
				if(assesment.getScoreType()==0){//分值类型   0-分值   1-分数百分比
					newJson.put("arrivalScore", rate*assesment.getArrivalRateScore());
				}else if(assesment.getScoreType()==1){
					newJson.put("arrivalScore", rate*assesment.getArrivalRateScore()*assesment.getScore()/100);
				}else{
					newJson.put("arrivalScore", 0.00);
				}
				performanceMap.put(supplierId, newJson);
			}
		}
		//计算完成率得分
		for (Map<String, Object> map : completionList) {
			String supplierId=map.get("supp_id").toString();
			String supplierName=map.get("supp_name").toString();
			double rate=Double.parseDouble(map.get("rate").toString());
			if(performanceMap.containsKey(supplierId)){
				net.sf.json.JSONObject old=performanceMap.get(supplierId);
				if(assesment.getScoreType()==0){
					old.put("compleScore", rate*assesment.getCompletionRateScore()/100);
				}else if(assesment.getScoreType()==1){
					old.put("compleScore", rate*assesment.getCompletionRateScore()*assesment.getScore()/100/100);
				}else{
					old.put("compleScore", 0.00);
				}
				performanceMap.put(supplierId, old);
			}else{
				net.sf.json.JSONObject newJson=new net.sf.json.JSONObject();
				newJson.put("supplierId", supplierId);
				newJson.put("supplierName", supplierName);
				newJson.put("score",assesment.getScore());
				newJson.put("arrivalScore",0.00);
				newJson.put("qualityScore", 0.00);
				newJson.put("afterScore", 0.00);
				if(assesment.getScoreType()==0){
					newJson.put("compleScore", rate*assesment.getCompletionRateScore()/100);
				}else if(assesment.getScoreType()==1){
					newJson.put("compleScore", rate*assesment.getCompletionRateScore()*assesment.getScore()/100/100);
				}else{
					newJson.put("compleScore", 0.00);
				}
				performanceMap.put(supplierId, newJson);
			}
		}
		//计算合格率得分
		for (Map.Entry<String, net.sf.json.JSONObject> json: qualificationMap.entrySet()) {
			String supplierId=json.getKey();
			net.sf.json.JSONObject obj=json.getValue();
			String supplierName=obj.getString("supplier_name");
			double rate=obj.getDouble("rate");
			if(performanceMap.containsKey(supplierId)){
				net.sf.json.JSONObject old=performanceMap.get(supplierId);
				if(assesment.getScoreType()==0){
					old.put("qualityScore", assesment.getQualificationRateScore()*rate/100);
				}else if(assesment.getScoreType()==1){
					old.put("qualityScore", rate/100*assesment.getQualificationRateScore()*assesment.getScore()/100);
				}else{
					old.put("qualityScore", 0.00);
				}
				performanceMap.put(supplierId, old);
			}else{
				net.sf.json.JSONObject newJson=new net.sf.json.JSONObject();
				newJson.put("supplierId", supplierId);
				newJson.put("supplierName", supplierName);
				newJson.put("score",assesment.getScore());
				newJson.put("arrivalScore", 0.00);
				newJson.put("compleScore", 0.00);
				newJson.put("afterScore", 0.00);
				if(assesment.getScoreType()==0){
					newJson.put("qualityScore", assesment.getQualificationRateScore()*rate/100);
				}else if(assesment.getScoreType()==1){
					newJson.put("qualityScore", rate/100*assesment.getQualificationRateScore()*assesment.getScore()/100);
				}else{
					newJson.put("qualityScore", 0.00);
				}
				performanceMap.put(supplierId, newJson);
			}
		}
		//计算售后响应及时率得分
		for (Map.Entry<String, net.sf.json.JSONObject> json: afterSaleMap.entrySet()) {
			String supplierId=json.getKey();
			net.sf.json.JSONObject obj=json.getValue();
			String supplierName=obj.getString("supplier_name");
			double rate =obj.getInt("inTime")/obj.getInt("total");
			if(performanceMap.containsKey(supplierId)){
				net.sf.json.JSONObject old=performanceMap.get(supplierId);
				if(assesment.getScoreType()==0){
					old.put("afterScore",rate *assesment.getAfterSaleRateScore());
				}else if(assesment.getScoreType()==1){
					old.put("afterScore", rate*assesment.getAfterSaleRateScore()*assesment.getScore()/100);
				}else{
					old.put("afterScore", 0.00);
				}
				performanceMap.put(supplierId, old);
			}else{
				net.sf.json.JSONObject newJson=new net.sf.json.JSONObject();
				newJson.put("supplierId", supplierId);
				newJson.put("supplierName", supplierName);
				newJson.put("score",assesment.getScore());
				newJson.put("arrivalScore", 0.00);
				newJson.put("compleScore", 0.00);
				newJson.put("qualityScore", 0.00);
				if(assesment.getScoreType()==0){
					newJson.put("afterScore",rate *assesment.getAfterSaleRateScore());
				}else if(assesment.getScoreType()==1){
					newJson.put("afterScore", rate*assesment.getAfterSaleRateScore()*assesment.getScore()/100);
				}else{
					newJson.put("afterScore", 0.00);
				}
				performanceMap.put(supplierId, newJson);
			}
		}
		
		model.addAttribute("performanceMap",performanceMap);
		model.addAttribute("assesmentList",assesmentList);
		model.addAttribute("pmap",pmap);
		return "platform/buyer/supplier/supplierPerformanceScore";
	}
	
	/**
	 * 及时率数据
	 * @param map
	 * @return
	 */
	public Map<String,net.sf.json.JSONObject> getSupplierArrivalDate(Map<String,Object> map){
		List<Map<String,Object>> supplierList=buyOrderMapper.getSupplierArrivalRate(map);
		Map<String,net.sf.json.JSONObject> suppMap =new LinkedMap<String,net.sf.json.JSONObject>();
		for (Map<String, Object> supplier : supplierList) {
			String suppId = supplier.get("supp_id").toString();
			String suppName = supplier.get("supp_name").toString();
			String predictArred = supplier.containsKey("predict_arred") ? supplier.get("predict_arred").toString() : "";//要求到货时间
			String arrivalDate = supplier.containsKey("arrival_date") ? supplier.get("arrival_date").toString() : "";//到货时间
			// 汇总过得数据
			if (suppMap.containsKey(suppId)) {
				net.sf.json.JSONObject old = suppMap.get(suppId);
				old.put("orderSum",old.getInt("orderSum")+1);
				if(arrivalDate.isEmpty()){//未到货
					old.put("failNum",old.getInt("failNum")+1);
				}else{//已到货
					if(predictArred.compareTo(arrivalDate)>=0){
						old.put("successNum",old.getInt("successNum")+1);
					}else{
						old.put("failNum",old.getInt("failNum")+1);
					}
				}
				suppMap.put(suppId, old);
			} else {// 没有汇总过得数据
				net.sf.json.JSONObject newJson = new net.sf.json.JSONObject();
				newJson.put("supp_name",suppName);
				newJson.put("supp_id",suppId);
				newJson.put("orderSum",1);
				newJson.put("failNum",0);
				newJson.put("successNum",0);
				if(arrivalDate.isEmpty()){//未到货
					newJson.put("failNum",1);
				}else{//已到货
					if(predictArred.compareTo(arrivalDate)>=0){
						newJson.put("successNum",1);
					}else{
						newJson.put("failNum",1);
					}
				}
				suppMap.put(suppId, newJson);
			}
		}
		return suppMap;
	}
	
	/**
	 * 合格率数据汇总
	 * @param amap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,net.sf.json.JSONObject> getSupplierQualificationData(Map<String,Object> amap){
		//获取退货数量
		List<Map<String,Object>> tuiHuoList=customerItemService.selectTuiHuoNum(amap);
		//获取到货数量
		List<Map<String,Object>> arrivalList=buyDeliveryRecordItemService.selectArrivalNum(amap);
		
		List<net.sf.json.JSONObject> tuiJsonList=JSONArray.fromObject(tuiHuoList);
		List<net.sf.json.JSONObject> arrivalJsonList=JSONArray.fromObject(arrivalList);
		
		Map<String,net.sf.json.JSONObject> map =new LinkedMap<String,net.sf.json.JSONObject>();
		
		for (net.sf.json.JSONObject json : tuiJsonList) {
			String supplierId=json.getString("supplier_id");
			String type=json.getString("type");
			if(map.containsKey(supplierId)){
				net.sf.json.JSONObject old=map.get(supplierId);
				//返修数据
				if("0".equals(type)){
					if(old.containsKey("exchangeNum")){
						old.put("exchangeNum", old.getInt("exchangeNum")+json.getInt("num"));
						map.put(supplierId, old);
					}else{
						old.put("exchangeNum", json.getInt("num"));
						map.put(supplierId, old);
					}
				}
				//退货数据
				if("1".equals(type)){
					if(old.containsKey("tuiHuoNum")){
						old.put("tuiHuoNum", old.getInt("tuiHuoNum")+json.getInt("num"));
						map.put(supplierId, old);
					}else{
						old.put("tuiHuoNum", json.getInt("num"));
						map.put(supplierId, old);
					}
				}
			}else{//第一次汇总
				net.sf.json.JSONObject newJson=new  net.sf.json.JSONObject();
				newJson.put("supplier_id", supplierId);
				newJson.put("supplier_name", json.getString("supplier_name"));
				if("0".equals(type)){
					newJson.put("exchangeNum", json.getInt("num"));
					newJson.put("tuiHuoNum", 0);
				}
				if("1".equals(type)){
					newJson.put("tuiHuoNum", json.getInt("num"));
					newJson.put("exchangeNum", 0);
				}
				map.put(supplierId, newJson);
			}
		}
		
		for (net.sf.json.JSONObject json : arrivalJsonList) {
			String supplierId=json.getString("supplier_id");
			if(map.containsKey(supplierId)){
				net.sf.json.JSONObject old=map.get(supplierId);
				if(old.containsKey("arrivalNum")){
					old.put("arrivalNum", old.getInt("arrivalNum")+json.getInt("arrivalNum"));
					map.put(supplierId, old);
				}else{
					old.put("arrivalNum", json.getInt("arrivalNum"));
					map.put(supplierId, old);
				}
			}else{
				map.put(supplierId, json);
			}
		}
		
		for (Map.Entry<String, net.sf.json.JSONObject> json : map.entrySet()) {
			String key = json.getKey();
			net.sf.json.JSONObject obj = json.getValue();
			int tuiHuoNum = 0;// 退货数量
			int exchangeNum = 0;// 返修数量
			int arrivalNum = 0;// 到货数量
			if (obj.containsKey("tuiHuoNum")) {
				tuiHuoNum += obj.getInt("tuiHuoNum");
			} else {
				obj.put("tuiHuoNum", tuiHuoNum);
			}
			if (obj.containsKey("exchangeNum")) {
				tuiHuoNum += obj.getInt("exchangeNum");
			} else {
				obj.put("exchangeNum", exchangeNum);
			}
			if (obj.containsKey("arrivalNum")) {
				arrivalNum += obj.getInt("arrivalNum");
			} else {
				obj.put("arrivalNum", arrivalNum);
			}
			if(obj.getInt("arrivalNum")==0){
				obj.put("rate", 0);
			}else if(arrivalNum-exchangeNum-tuiHuoNum>0){
				int a=arrivalNum-exchangeNum-tuiHuoNum;
				double rate=a/(arrivalNum*1.0)*100;
				obj.put("rate",Math.round(rate*100)/100.0);
			}else{
				obj.put("rate", 0);
			}
			map.put(key, obj);
		}
		return map;
	}
	
	/**
	 * 售后数据汇总
	 * @param amap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,net.sf.json.JSONObject> getSupplierAfterSaleData(Map<String, Object> amap){
		List<Map<String,Object>> list=buBSellerCustomerService.getSupplierAfterSaleRate(amap);
		List<net.sf.json.JSONObject> jsonList=JSONArray.fromObject(list);
		Map<String,net.sf.json.JSONObject> map =new LinkedMap<String,net.sf.json.JSONObject>();
		for (net.sf.json.JSONObject json : jsonList) {
			String supplierId=json.getString("supplier_id");
			int day=json.getInt("day");
			if(map.containsKey(supplierId)){
				net.sf.json.JSONObject old=map.get(supplierId);
				if(day<=1){
					json.put("inTime", old.getInt("inTime")+1);
				}else{
					json.put("notInTime", old.getInt("notInTime")+1);
				}
				json.put("total", old.getInt("total")+1);
				map.put(supplierId, old);
			}else{
				json.put("total", 1);
				if(day<=1){
					json.put("inTime", 1);
					json.put("notInTime", 0);
				}else{
					json.put("notInTime", 1);
					json.put("inTime", 0);
				}
				map.put(supplierId, json);
			}
		}
		return map;
	}
	
	/**
	 * 同步OMS供应商数据
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("synOMSSupplier")
	@ResponseBody
	public String synchronousOMSSupplier(){
		JSONObject data = new JSONObject();
		//获取买卖系统供应商所有数据
//		List<Map<String,Object>> supplierList = buySupplierService.selectAllSupplier();
		net.sf.json.JSONObject jsonString = new  net.sf.json.JSONObject();
		//获取OMS所有供应商数据
		String levelUrl = Constant.OMS_INTERFACE_URL + "getAllScmSupplier";   
		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		jsonString.put("list", arrayString);
		arrayString = jsonString.toString();
		int num = 0;
		if (!ObjectUtil.isEmpty(arrayString)){
			net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(arrayString);		
			JSONArray supplierArray = JSONArray.fromObject(json.get("list"));
			
			Iterator<Object> it = supplierArray.iterator();
	        while (it.hasNext()) {
	        	net.sf.json.JSONObject ob = (net.sf.json.JSONObject) it.next();
	            if (ob.getString("id")!=null){
	            	if (ob.getString("name")!=null){
//	            		String supplierName = ob.getString("name").toString().replaceAll("\\s*", "");
//	            		boolean result = true;
//	            		
//	            		if (supplierList != null && supplierList.size() > 0) {
//	            			for (int i = 0; i < supplierList.size(); i++) {
//	            				Map<String,Object> item = supplierList.get(i);
//	            				
//	            				if (supplierName.equals(item.get("supp_name").toString().replaceAll("\\s*", "")) ){
//	            					result = false;
//	            				}
//	            			}
//		            	}
	            		//如果OMS中供应商不存在买卖系统中，将供应商添加到买卖系统
//	            		if (result){            			
            				try {
								num = buySupplierService.saveAddOMSSupplier(ob);
							} catch (Exception e) {
								data.put("success", false);
	            				data.put("msg", e.getMessage());
								e.printStackTrace();
							}
//	            		}

		            }
		        }
			}	
		}
		if (num > 0){
			data.put("success", true);
			data.put("msg","操作成功");			
		}else{
			data.put("success", true);
			data.put("msg","无数据同步");
		}
		
		return data.toString();
	}
	
	/**
	 * 加载人员设置关联供应商页面
	 * @param id
	 * @return
	 */
	@RequestMapping("instalSupplierUser")
	public String instalSupplierUser(String userId){
		model.addAttribute("instalSupplierUserId", userId);
		return "platform/system/supplier/instalSupplierUser";
	}
	
	/**
	 * 获得左侧没有的供应商数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getLeftNotSupplier")
	@ResponseBody
	public String getLeftNotSupplier(String userId,String supplierName){
		List<Map<String, Object>> supplierList = buySupplierService.getLeftNotRole(userId,supplierName);
		return JSONArray.fromObject(supplierList).toString();
	}
	
	/**
	 * 获得右侧有的供应商数据
	 * @param userId
	 * @return
	 */
	@RequestMapping("getRightSupplier")
	@ResponseBody
	public String getRightSupplier(String userId,String supplierName){
		List<Map<String, Object>> supplierList = buySupplierService.getRightRole(userId,supplierName);
		return JSONArray.fromObject(supplierList).toString();
	}
	
	/**
	 * 人员添加供应商
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("addUserSupplier")
	@ResponseBody
	public String addUserSupplier(String userId,String supplierIdStr){
		JSONObject json = buySupplierService.addUserSupplier(userId,supplierIdStr);
		return json.toJSONString();
	}

	/**
	 * 人员删除供应商
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@RequestMapping("deleteUserSupplier")
	@ResponseBody
	public String deleteUserSupplier(String userId,String supplierIdStr){
		JSONObject json = buySupplierService.deleteUserSupplier(userId,supplierIdStr);
		return json.toJSONString();
	}
	
}
