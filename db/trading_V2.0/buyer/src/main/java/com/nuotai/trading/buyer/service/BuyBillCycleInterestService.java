package com.nuotai.trading.buyer.service;

import com.nuotai.trading.buyer.dao.BuyBillInterestMapper;
import com.nuotai.trading.buyer.model.BuyBillInterest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 账单结算周期关联利息
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
public class BuyBillCycleInterestService {
	@Autowired
	private BuyBillInterestMapper buyBillInterestMapper;

	//保存账单关联利息
	public int saveInterestInfo(Map<String, Object> map) {
		return buyBillInterestMapper.saveInterestInfo(map);
	}

	//获取账单关联利息
	public List<BuyBillInterest> getBillInteresInfo(String billCycleId) {
		return buyBillInterestMapper.getBillInteresInfo(billCycleId);
	}

	//根据账单周期编号删除关联利息
	public int deleteByPrimaryKey(String billCycleId) {
		return buyBillInterestMapper.deleteByPrimaryKey(billCycleId);
	}

	public int insert(BuyBillInterest record) {
		return 0;
	}

	public int insertSelective(BuyBillInterest record) {
		return 0;
	}

	public BuyBillInterest selectByPrimaryKey(String id) {
		return null;
	}

	public int updateByPrimaryKeySelective(BuyBillInterest record) {
		return 0;
	}

	public int updateByPrimaryKey(BuyBillInterest record) {
		return 0;
	}

}
