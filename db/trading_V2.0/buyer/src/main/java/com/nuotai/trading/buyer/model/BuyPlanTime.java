package com.nuotai.trading.buyer.model;

import java.io.Serializable;

import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-04-03 20:24:53
 */
@Data
public class BuyPlanTime implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//销售计划提报日期
	private String salePlanWeek;
	//销售计划间隔
	private Integer salePlanInterval;
	//销售周期范围
	private Integer salePlanArea;
	//确认销售计划日期
	private String confirmPlanWeek;
	//采购计划提报日期
	private String applyPlanWeek;
	//采购下单日期
	private String purchaseWeek;
	//计划外销售计划人员超级权限设置
	private String outPlanUser;
	//确认计划人员超级权限设置
	private String confirmSaleplanUser;
	//采购计划超级权限设置
	private String applyPurchaseUser;
	//采购下单超级权限设置
	private String purchaseUser;
}
