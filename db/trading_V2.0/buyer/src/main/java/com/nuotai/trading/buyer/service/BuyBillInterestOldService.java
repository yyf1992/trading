package com.nuotai.trading.buyer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.dao.BuyBillInterestOldMapper;
import com.nuotai.trading.buyer.model.BuyBillInterestOld;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillInterestOldService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillInterestOldService.class);

	@Autowired
	private BuyBillInterestOldMapper buyBillInterestOldMapper;
	
	public BuyBillInterestOld get(String id){
		return buyBillInterestOldMapper.get(id);
	}
	
	public List<BuyBillInterestOld> queryList(Map<String, Object> map){
		return buyBillInterestOldMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillInterestOldMapper.queryCount(map);
	}

	//添加新历史关联利息
	public int addNewInterestOld(BuyBillInterestOld buyBillInterestOld){
		return buyBillInterestOldMapper.addNewInterestOld(buyBillInterestOld);
	}
	//查询最近历史纪录
	public List<BuyBillInterestOld> queryBillInterestOld(String billNewCycleOldId){
		return buyBillInterestOldMapper.queryBillInterestOld(billNewCycleOldId);
	}
	public void update(BuyBillInterestOld buyBillInterestOld){
		buyBillInterestOldMapper.update(buyBillInterestOld);
	}

	//删除历史账单关联利息
	public int deleteNewInterestOld(String id){
		return buyBillInterestOldMapper.deleteNewInterestOld(id);
	}
	

}
