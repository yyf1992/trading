package com.nuotai.trading.buyer.timer;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nuotai.trading.buyer.dao.BuySalePlanItemMapper;
import com.nuotai.trading.buyer.model.BuySalePlanItem;
import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.service.TimeTaskService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 库存积压预警定时任务
 * @author
 *
 */
@Component
public class SalePlanAccomplishTiming {
	@Autowired
	private TimeTaskService timeTaskService;
	@Autowired
	private BuySalePlanItemMapper buySalePlanItemMapper;
	
	@Scheduled(cron="0 0 2 * * ? ")
	//0 */1 * * * ? 每隔一分钟
	//0 0 2 * * ? 每天2点
	public void catchBuyWarehouseBacklog(){
		TimeTask timeTask = timeTaskService.selectByCode("SALE_PLAN_ACCOMPLISH");
		if(timeTask!=null){
			if("0".equals(timeTask.getCurrentStatus())){
				//当前状态是：正常等待
				if("0".equals(timeTask.getPrepStatus())){
					//预备状态是：正常
					//当前状态改为执行中
					timeTask.setCurrentStatus("1");
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
					//更新销售计划达成率
					updateSalesCompletionRate();
					
					//当前状态改为正常等待
					timeTask.setCurrentStatus("0");
					timeTask.setLastSynchronous(new Date());
					timeTask.setLastExecute(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
					
				}else if("1".equals(timeTask.getPrepStatus())){
					//预备状态是：预备停止
					//当前状态：0：正常等待；1：执行中；2：停止
					timeTask.setCurrentStatus("2");
					timeTask.setLastSynchronous(new Date());
					timeTaskService.updateByPrimaryKeySelective(timeTask);
				}
			}else if("1".equals(timeTask.getCurrentStatus())){
				//执行中
				timeTask.setLastSynchronous(new Date());
				timeTaskService.updateByPrimaryKeySelective(timeTask);
			}else if("2".equals(timeTask.getCurrentStatus())){
				//停止
			}
		}
	}
	
	public void updateSalesCompletionRate(){
		
		//获取当前日期的前一天
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		Map<String, Object> map = new HashMap<>();
		map.put("yesterdayDate", yesterday);
		
		//获取计划结束日期小于当天日期前一天的数据
		List<BuySalePlanItem> buySalePlanItemList = buySalePlanItemMapper.selectSalePlanShopListByMap(map);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		for (BuySalePlanItem buySalePlanItem : buySalePlanItemList){
			//获取oms销售报表数据
			String levelUrl =  Constant.OMS_INTERFACE_URL + "getOmsSalesReportByMap?startDate="+sdf.format(buySalePlanItem.getStartDate())
					+"&endDate="+sdf.format(buySalePlanItem.getEndDate())
					+"&shopcode="+buySalePlanItem.getShopCode()
					+"&skuoidStr="+buySalePlanItem.getBarcode();
			String arrayString = InterfaceUtil.searchLoginService(levelUrl);
			JSONObject json = JSONObject.fromObject(arrayString);
			JSONArray salesReportArray = JSONArray.fromObject(json.get("list"));
			if (!ObjectUtil.isEmpty(salesReportArray)){
				@SuppressWarnings("unchecked")
				List<BuySalePlanItem> list = (List<BuySalePlanItem>) JSONArray.toCollection(salesReportArray, BuySalePlanItem.class);
				if (!ObjectUtil.isEmpty(list)&&list.size()==1){
					for (BuySalePlanItem buySalePlanItemOMS : list){
						buySalePlanItem.setNum(buySalePlanItemOMS.getNum());
						DecimalFormat df=new DecimalFormat("0.00");
						int num = buySalePlanItemOMS.getNum();
						int planNum = buySalePlanItem.getSalesNum()+buySalePlanItem.getPutStorageNum();
						BigDecimal rate = new BigDecimal(df.format((float)num/planNum));
						buySalePlanItem.setSalesCompletionRate(rate);
						buySalePlanItemMapper.updateByBuySalePlanItem(buySalePlanItem);
					}
				}
			}
		}
	
	}


}
