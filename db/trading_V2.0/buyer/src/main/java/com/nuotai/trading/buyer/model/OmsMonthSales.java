package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author dxl"
 * @date 2018-03-19 17:24:29
 */
@Data
public class OmsMonthSales implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键id
	private String id;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//本月销量
	private Integer num;
	//开始日期
	private String startDate;
	//结束日期
	private String endDate;
}
