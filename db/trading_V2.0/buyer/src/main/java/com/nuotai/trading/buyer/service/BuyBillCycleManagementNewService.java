package com.nuotai.trading.buyer.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.model.BuyBillCycleManagement;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementOld;
import com.nuotai.trading.buyer.model.BuyBillInterestOld;
import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagement;
import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagementNew;
import com.nuotai.trading.buyer.service.seller.BSellerBillCycleManagementNewService;
import com.nuotai.trading.buyer.service.seller.BSellerBillCycleService;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.dao.BuyBillCycleManagementNewMapper;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementNew;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillCycleManagementNewService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillCycleManagementNewService.class);

	@Autowired
	private BuyBillCycleManagementNewMapper buyBillCycleManagementNewMapper;
	@Autowired
	private BuyBillInterestNewService buyBillInterestNewService;
	@Autowired
	private BSellerBillCycleManagementNewService bSellerBillCycleManagementNewService;
	@Autowired
	private BuyBillCycleService buyBillCycleService;
	@Autowired
	private BSellerBillCycleService bSellerBillCycleService;

	//根据编号查询修改数据
	public BuyBillCycleManagementNew queryBillCycleByNewId(String id){
		return buyBillCycleManagementNewMapper.queryBillCycleByNewId(id);
	}

	//查询最近修改数据
	//根据编号查询修改数据
	public BuyBillCycleManagementNew queryNowBillCycleByNewId(String id){
		return buyBillCycleManagementNewMapper.queryNowBillCycleByNewId(id);
	}

	//修改申请数据列表查询
	public List<BuyBillCycleManagementNew> queryListNewBillCycle(Map<String, Object> map){
		return buyBillCycleManagementNewMapper.queryListNewBillCycle(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyBillCycleManagementNewMapper.queryCount(map);
	}

	//添加修改申请
	public int addNewBillCycle(BuyBillCycleManagementNew buyBillCycleManagementNew){
		return buyBillCycleManagementNewMapper.addNewBillCycle(buyBillCycleManagementNew);
	}

	//修改申请数据状态
	public int updateNewBillCycle(BuyBillCycleManagementNew buyBillCycleManagementNew){
		return buyBillCycleManagementNewMapper.updateNewBillCycle(buyBillCycleManagementNew);
	}

	//买家通过
	public JSONObject verifyNewBillSuccess(String id) {
		JSONObject json = new JSONObject();
		BuyBillCycleManagementNew newBuyBillCycle = new BuyBillCycleManagementNew();
		newBuyBillCycle.setId(id);
		newBuyBillCycle.setBillDealStatus("2");
		int result =  updateNewBillCycle(newBuyBillCycle);
		if(result>0){
			//根据编号查询买家申请信息
			BuyBillCycleManagementNew newBuyBillCycle1 = queryBillCycleByNewId(id);
			//List<Map<String, Object>> oldInterestBean = buyBillCycleInterestService.getBillInteresInfo(id);
			BSellerBillCycleManagementNew sellerBillCycleNew = new BSellerBillCycleManagementNew();
			sellerBillCycleNew.setId(newBuyBillCycle1.getId());
			sellerBillCycleNew.setNewBillCycleId(newBuyBillCycle1.getNewBillCycleId());
			sellerBillCycleNew.setBuyCompanyId(newBuyBillCycle1.getBuyCompanyId());
			sellerBillCycleNew.setSellerCompanyId(newBuyBillCycle1.getSellerCompanyId());
			sellerBillCycleNew.setCycleStartDate(newBuyBillCycle1.getCycleStartDate());
			sellerBillCycleNew.setCycleEndDate(newBuyBillCycle1.getCycleEndDate());
			sellerBillCycleNew.setCheckoutCycle(newBuyBillCycle1.getCheckoutCycle());
			sellerBillCycleNew.setBillStatementDate(newBuyBillCycle1.getBillStatementDate());
			sellerBillCycleNew.setOutStatementDate(newBuyBillCycle1.getOutStatementDate());
			sellerBillCycleNew.setBillDealStatus(newBuyBillCycle1.getBillDealStatus());
			sellerBillCycleNew.setCreateUser(newBuyBillCycle1.getCreateUser());
			sellerBillCycleNew.setCreateTime(newBuyBillCycle1.getCreateTime());
			bSellerBillCycleManagementNewService.addNewSellerBillCycle(sellerBillCycleNew);//添加卖家账单周期申请
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//买家修改申请内部驳回
	public JSONObject verifyNewBillError(String id) {
		JSONObject json = new JSONObject();
		BuyBillCycleManagementNew newBuyBillCycle = new BuyBillCycleManagementNew();
		newBuyBillCycle.setId(id);
		newBuyBillCycle.setBillDealStatus("4");
		int result =  updateNewBillCycle(newBuyBillCycle);

		BuyBillCycleManagementNew newBuyBillCycleBean = queryBillCycleByNewId(id);
		//更改正式表修改数据状态为 修改申请审批中
		String buyBillCycleId = newBuyBillCycleBean.getNewBillCycleId();//获取正式账单编号
		//根据正式编号查询卖家同步数据
		BSellerBillCycleManagement bSellerBillCycle = bSellerBillCycleService.selectByPrimaryKey(buyBillCycleId);
		BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();
		buyBillCycleManagement.setId(buyBillCycleId);
		buyBillCycleManagement.setBillDealStatus(bSellerBillCycle.getBillDealStatus());//获取卖家同步数据状态
		buyBillCycleService.updateByPrimaryKeySelective(buyBillCycleManagement);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}


	public void delete(String id){
		buyBillCycleManagementNewMapper.delete(id);
	}

}
