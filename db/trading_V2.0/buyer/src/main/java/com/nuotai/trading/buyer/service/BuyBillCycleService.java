package com.nuotai.trading.buyer.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyBillCycleManagementMapper;
import com.nuotai.trading.buyer.model.*;
import com.nuotai.trading.buyer.model.seller.BSellerBillCycleManagement;
import com.nuotai.trading.buyer.service.seller.BSellerBillCycleService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class BuyBillCycleService {
	@Autowired
	private BuyBillCycleManagementMapper billMapper;
	@Autowired
	private BuyBillCycleInterestService buyBillCycleInterestService;
	@Autowired
	private BuyBillCycleManagementNewService buyBillCycleManagementNewService;
	@Autowired
	private BuyBillInterestNewService buyBillInterestNewService;
	@Autowired
	private BuyBillCycleManagementOldService buyBillCycleManagementOldService;
	@Autowired
	private BuyBillInterestOldService buyBillInterestOldService;
	@Autowired
	private BSellerBillCycleService sellerBillCycleService;

	public int deleteByPrimaryKey(String id) {
		return billMapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyBillCycleManagement record) {
		return 0;
	}

	public int insertSelective(BuyBillCycleManagement record) {
		return 0;
	}

	// 根据id编号查询账单信息
	public BuyBillCycleManagement selectByPrimaryKey(String id) {
		return billMapper.selectByPrimaryKey(id);
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(BuyBillCycleManagement record) {
		return billMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuyBillCycleManagement record) {
		return 0;
	}

	//查询账单周期列表信息
	public List<Map<String, Object>> getBillCycleList(
			SearchPageUtil searchPageUtil) {
		return billMapper.getBillCycleList(searchPageUtil);
	}

	//查询所有账单周期信息
	public List<BuyBillCycleManagement> queryBillCycleList(Map<String, Object> map) {
		return billMapper.queryBillCycleList(map);
	}

	//向账单周期管理页面提供不同状态下的数量
	public void getBillCountByStatus(Map<String, Object> billMap){
		//Map<String, Object> billCycleCountMap = new HashMap<String, Object>();
		//查询所有状态数量
		/*billMap.put("billDealStatus", "");
		int allBillCycleCount = queryBillStatusCount(billMap);*/
		//查询待内部审批周期数
		billMap.put("billStatusToCount", "1");
		int approvalBillCycleCount = queryBillStatusCount(billMap);
		//查询待对方审批周期数
		billMap.put("billStatusToCount", "2");
		int acceptBillCycleCount = queryBillStatusCount(billMap);
		//查询审批已通过周期数
		billMap.put("billStatusToCount", "3");
		int apprEndBillCycleCount = queryBillStatusCount(billMap);
		//查询审批已通过周期数
		billMap.put("billStatusToCount", "6");
		int updateEndBillCycleCount = queryBillStatusCount(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillCycleCount", approvalBillCycleCount);
		billMap.put("acceptBillCycleCount", acceptBillCycleCount);
		billMap.put("apprEndBillCycleCount", apprEndBillCycleCount);
		billMap.put("updateEndBillCycleCount", updateEndBillCycleCount);
	}
	
	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return billMapper.queryBillStatusCount(map);
	}

	//查询账单周期唯一性
	public List<BuyBillCycleManagement> queryHasBillCycle(Map<String,Object> map){
		//String billStatementDate = (String) map.get("billStatementDate");//出账日期
		String sellerCompanyId = (String) map.get("supplierId");//买家或卖家公司

		Map<String,Object> billCycleMap = new HashMap<String,Object>();
		billCycleMap.put("buyCompanyId", ShiroUtils.getCompId());
		billCycleMap.put("sellerCompanyId",sellerCompanyId);
		return billMapper.queryBillCycleList(billCycleMap);
	}

	//添加账单周期信息
	public List<String> saveBillCycleInfo(Map<String, Object> saveMap) {
		//账单周期数据map
		Map<String, Object> billCycleInfoMap = new HashMap<String, Object>();
		List<String> idList = new ArrayList<String>();
		//String cycleStartDate = (String) saveMap.get("cycleStartDate");//开始周期
		//int cycleStartDatei = Integer.parseInt(cycleStartDate);
		//String cycleEndDate = (String) saveMap.get("cycleEndDate");//截止周期
		//int cycleEndDatei = Integer.parseInt(cycleEndDate);
		String billStatementDate = (String) saveMap.get("billStatementDate");//出账日期
		String outStatementDateStr = saveMap.get("outStatementDate").toString();//出账日期date
		int billStatementDatei = Integer.parseInt(billStatementDate);
		SimpleDateFormat sdfddd = new SimpleDateFormat("yyyy-MM-dd");
		Date outStatementDate = new Date();
		try {
			outStatementDate = sdfddd.parse(outStatementDateStr);
		}catch (Exception e){
			e.printStackTrace();
		}

		String supplierId = (String) saveMap.get("supplierId");//买家或卖家公司
		String billDealStatus = "1";
		String billCycleId = ShiroUtils.getUid();//账单周期编号
		Date createTime = new Date();//创建时间
		
		billCycleInfoMap.put("id", billCycleId);
		billCycleInfoMap.put("buyCompanyId", ShiroUtils.getCompId());
		billCycleInfoMap.put("sellerCompanyId", supplierId);
		/*billCycleInfoMap.put("cycleStartDate", cycleStartDatei);
		billCycleInfoMap.put("cycleEndDate", cycleEndDatei);
		billCycleInfoMap.put("checkoutCycle", cycleEndDatei-cycleStartDatei);*/
		billCycleInfoMap.put("outStatementDate",outStatementDate);
		billCycleInfoMap.put("billStatementDate", billStatementDatei);
		billCycleInfoMap.put("billDealStatus", billDealStatus);
		billCycleInfoMap.put("createUser", ShiroUtils.getUserId());
		billCycleInfoMap.put("createTime", createTime);
		billCycleInfoMap.put("updateUser", ShiroUtils.getUserId());
		int billCycleCount = billMapper.saveBillCycleInfo(billCycleInfoMap);//添加买家账单周期
		//判断买家数据是否添加成功
		if(billCycleCount > 0){
			idList.add(billCycleId);
			//关联利息的添加
			String interestList = (String) saveMap.get("interestList");//利息数据
			List<BuyBillInterest> interList = JSONArray.parseArray(interestList,BuyBillInterest.class);
			for(BuyBillInterest buyBillInterest : interList){
				Map<String, Object> interMap = new HashMap<String, Object>();
				interMap.put("id", ShiroUtils.getUid());
				interMap.put("billCycleId", billCycleId);
				interMap.put("overdueDate", buyBillInterest.getOverdueDate());
				interMap.put("overdueInterest", buyBillInterest.getOverdueInterest());
				interMap.put("interestCalculationMethod", buyBillInterest.getInterestCalculationMethod());
				buyBillCycleInterestService.saveInterestInfo(interMap);
			}
		}

		return idList;
	}

	//根据编号修改账单及关联利息表信息
	public List<String> updateSaveBillCycleInfo(Map<String, Object> updateSavemap) {
		List<String> idList = new ArrayList<String>();
		//BuyBillCycleManagementOld newBillCycleOld = new BuyBillCycleManagementOld();//新历史
		//BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();//申请数据
		String billCycleId = (String) updateSavemap.get("billCycleId");//历史账单周期编号
		String onselfCompId = ShiroUtils.getCompId();//操作人所在公司
		String onselfId = ShiroUtils.getUserId();//操作人id
		//String cycleStartDate = (String) updateSavemap.get("cycleStartDate");//开始周期
		//int cycleStartDatei = Integer.parseInt(cycleStartDate);
		//String cycleEndDate = (String) updateSavemap.get("cycleEndDate");//截止周期
		//int cycleEndDatei = Integer.parseInt(cycleEndDate);
		String outStatementDateStr = updateSavemap.get("outStatementDate").toString();
		String billStatementDate = (String) updateSavemap.get("billStatementDate");//出账日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date outStatementDate = new Date();
		try {
			outStatementDate = sdf.parse(outStatementDateStr);
		}catch (Exception e){
			e.printStackTrace();
		}
		int billStatementDatei = Integer.parseInt(billStatementDate);

		String sellerCompanyId = (String) updateSavemap.get("sellerCompanyId");//买家或卖家公司
		Date date = new Date();
		//先添加修改数据到修改申请表
		String newBillCycleId = ShiroUtils.getUid();//申请修改数据编号
		BuyBillCycleManagementNew buyBillCycleManagementNew = new BuyBillCycleManagementNew();
		buyBillCycleManagementNew.setId(newBillCycleId);
		buyBillCycleManagementNew.setNewBillCycleId(billCycleId);
		buyBillCycleManagementNew.setBuyCompanyId(ShiroUtils.getCompId());
		buyBillCycleManagementNew.setSellerCompanyId(sellerCompanyId);
		/*buyBillCycleManagementNew.setCycleStartDate(cycleStartDatei);
		buyBillCycleManagementNew.setCycleEndDate(cycleEndDatei);
		buyBillCycleManagementNew.setCheckoutCycle(cycleEndDatei-cycleStartDatei);*/
		buyBillCycleManagementNew.setBillStatementDate(billStatementDatei);
		buyBillCycleManagementNew.setOutStatementDate(outStatementDate);
		buyBillCycleManagementNew.setBillDealStatus("1");
		buyBillCycleManagementNew.setCreateUser(onselfId);
		buyBillCycleManagementNew.setCreateTime(date);
		int addNewBillCycleCount =  buyBillCycleManagementNewService.addNewBillCycle(buyBillCycleManagementNew);
		if(addNewBillCycleCount > 0) {
			//更改正式表修改数据状态为 修改申请审批中
			BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();
			buyBillCycleManagement.setId(billCycleId);
			buyBillCycleManagement.setBillDealStatus("6");
			updateByPrimaryKeySelective(buyBillCycleManagement);

			idList.add(newBillCycleId);//返回id编号
			String interestList = (String) updateSavemap.get("interestList");//利息数据
			List<BuyBillInterest> interList = JSONArray.parseArray(interestList, BuyBillInterest.class);
			for (BuyBillInterest buyBillInterest : interList) {
				BuyBillInterestNew buyBillInterestNew = new BuyBillInterestNew();
				buyBillInterestNew.setId(ShiroUtils.getUid());
				buyBillInterestNew.setBillCycleIdNew(newBillCycleId);
				buyBillInterestNew.setOverdueDate(buyBillInterest.getOverdueDate());
				buyBillInterestNew.setOverdueInterest(buyBillInterest.getOverdueInterest());
				buyBillInterestNew.setInterestCalculationMethod(buyBillInterest.getInterestCalculationMethod());
				buyBillInterestNewService.addNewInterest(buyBillInterestNew);
			}
		}
		/*//根据编号查询历史周期数据及关联利息
		BuyBillCycleManagement oldBillCycleBean = selectByPrimaryKey(oldBillCycleId);
		List<BuyBillInterest> oldInterestBean = buyBillCycleInterestService.getBillInteresInfo(oldBillCycleId);
		//拼装新历史数据
		String newBillCycleOldId = ShiroUtils.getUid();//新历史编号
		newBillCycleOld.setId(newBillCycleOldId);
		newBillCycleOld.setOldBillCycleId(oldBillCycleBean.getId());
		newBillCycleOld.setBuyCompanyId(oldBillCycleBean.getBuyCompanyId());
		newBillCycleOld.setSellerCompanyId(oldBillCycleBean.getSellerCompanyId());
		newBillCycleOld.setCycleStartDate(oldBillCycleBean.getCycleStartDate());
		newBillCycleOld.setCycleEndDate(oldBillCycleBean.getCycleEndDate());
		newBillCycleOld.setCheckoutCycle(oldBillCycleBean.getCheckoutCycle());
		newBillCycleOld.setBillStatementDate(oldBillCycleBean.getBillStatementDate());
		newBillCycleOld.setBillDealStatus(oldBillCycleBean.getBillDealStatus());
		newBillCycleOld.setCreateUser(oldBillCycleBean.getCreateUser());
		newBillCycleOld.setCreateTime(oldBillCycleBean.getCreateTime());
		//添加新历史数据
		int addNewBillCycleOldCount = buyBillCycleManagementOldService.addNewBillCycleOld(newBillCycleOld);
		//判断添加是否成功
		int billCycleCount = 0;
		if(addNewBillCycleOldCount >0){
			//添加新历史关联利息
			for(int i = 0;i<oldInterestBean.size();i++){
				BuyBillInterestOld newBillInterestOld = new BuyBillInterestOld();//新历史关联
				String newBillInterestOldId = ShiroUtils.getUid();//新历史关联利息编号
				newBillInterestOld.setId(newBillInterestOldId);
				newBillInterestOld.setBillCycleIdOld(newBillCycleOldId);//新历史周期编号
				int overdate = oldInterestBean.get(i).getOverdueDate();
				newBillInterestOld.setOverdueDate(oldInterestBean.get(i).getOverdueDate());
				newBillInterestOld.setOverdueInterest(oldInterestBean.get(i).getOverdueInterest());
				newBillInterestOld.setInterestCalculationMethod(oldInterestBean.get(i).getInterestCalculationMethod());
				int addNewInterestCount = buyBillInterestOldService.addNewInterestOld(newBillInterestOld);
			}
			int deleteOldInterestCount = buyBillCycleInterestService.deleteByPrimaryKey(oldBillCycleId);//删除历史关联利息
			sellerBillCycleService.deleteByPrimaryKey(oldBillCycleId);//删除卖家同步的数据
			//修改申请表中历史数据
			if(deleteOldInterestCount >0){
				buyBillCycleManagement.setId(oldBillCycleId);
				buyBillCycleManagement.setBuyCompanyId(onselfCompId);
				buyBillCycleManagement.setSellerCompanyId(supplierId);
				buyBillCycleManagement.setCycleStartDate(cycleStartDatei);
				buyBillCycleManagement.setCycleEndDate(cycleEndDatei);
				buyBillCycleManagement.setCheckoutCycle(cycleEndDatei-cycleStartDatei);
				buyBillCycleManagement.setBillStatementDate(billStatementDatei);
				buyBillCycleManagement.setBillDealStatus("1");
				buyBillCycleManagement.setCreateUser(onselfId);
				buyBillCycleManagement.setUpdateUser(onselfId);
				buyBillCycleManagement.setCreateTime(date);

				 billCycleCount = updateByPrimaryKeySelective(buyBillCycleManagement);
				if(billCycleCount > 0){
					String interestList = (String) updateSavemap.get("interestList");//利息数据
					List<BuyBillInterest> interList = JSONArray.parseArray(interestList,BuyBillInterest.class);
					for(BuyBillInterest buyBillInterest : interList){
						Map<String, Object> interMap = new HashMap<String, Object>();
						interMap.put("id", ShiroUtils.getUid());
						interMap.put("billCycleId", oldBillCycleId);
						interMap.put("overdueDate", buyBillInterest.getOverdueDate());
						interMap.put("overdueInterest", buyBillInterest.getOverdueInterest());
						interMap.put("interestCalculationMethod", buyBillInterest.getInterestCalculationMethod());
						buyBillCycleInterestService.saveInterestInfo(interMap);
					}
				}
			}
		}*/
		return idList;
	}

	//买家通过
	public JSONObject verifySuccess(String id) {
		JSONObject json = new JSONObject();
		//Map<String, Object> map = new HashMap<String,Object>();
		BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();
		buyBillCycleManagement.setId(id);
		buyBillCycleManagement.setBillDealStatus("2");
		//buyBillCycleManagement.setUpdateUser(onselfId);
		//order.setId(id);
		//order.setIsCheck(1);
		int result =  updateByPrimaryKeySelective(buyBillCycleManagement);
		if(result>0){
			//根据编号查询历史周期数据及关联利息
			BuyBillCycleManagement oldBillCycleBean = selectByPrimaryKey(id);
			//List<Map<String, Object>> oldInterestBean = buyBillCycleInterestService.getBillInteresInfo(id);
			BSellerBillCycleManagement sellerBillCycle = new BSellerBillCycleManagement();
			sellerBillCycle.setId(oldBillCycleBean.getId());
			sellerBillCycle.setBuyCompanyId(oldBillCycleBean.getBuyCompanyId());
			sellerBillCycle.setSellerCompanyId(oldBillCycleBean.getSellerCompanyId());
			sellerBillCycle.setCycleStartDate(oldBillCycleBean.getCycleStartDate());
			sellerBillCycle.setCycleEndDate(oldBillCycleBean.getCycleEndDate());
			sellerBillCycle.setCheckoutCycle(oldBillCycleBean.getCheckoutCycle());
			sellerBillCycle.setBillStatementDate(oldBillCycleBean.getBillStatementDate());
			sellerBillCycle.setOutStatementDate(oldBillCycleBean.getOutStatementDate());
			sellerBillCycle.setBillDealStatus(oldBillCycleBean.getBillDealStatus());
			sellerBillCycle.setCreateUser(oldBillCycleBean.getCreateUser());
			sellerBillCycle.setCreateTime(oldBillCycleBean.getCreateTime());

			sellerBillCycleService.addSellerBillCycle(sellerBillCycle);//添加卖家账单周期
			json.put("success", true);
			json.put("msg", "成功！");

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//买家内部驳回
	public JSONObject verifyError(String id) {
		JSONObject json = new JSONObject();
		BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();

		buyBillCycleManagement.setId(id);
		buyBillCycleManagement.setBillDealStatus("4");
		int result = updateByPrimaryKeySelective(buyBillCycleManagement);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
		//根据编号查询历史账单周期
		/*BuyBillCycleManagementOld buyBillCycleManagementOld = buyBillCycleManagementOldService.queryLastDateBillCycleOld(id);
		int billCycleCount = 0;
		if(null != buyBillCycleManagementOld){
			//回退更新申请表历史数据
			//BuyBillCycleManagement buyBillCycleManagement = new BuyBillCycleManagement();
			//buyBillCycleManagement.setId(buyBillCycleManagementOld.getOldBillCycleId());
			buyBillCycleManagement.setId(id);
			buyBillCycleManagement.setBuyCompanyId(buyBillCycleManagementOld.getBuyCompanyId());
			buyBillCycleManagement.setSellerCompanyId(buyBillCycleManagementOld.getSellerCompanyId());
			buyBillCycleManagement.setCycleStartDate(buyBillCycleManagementOld.getCycleStartDate());
			buyBillCycleManagement.setCycleEndDate(buyBillCycleManagementOld.getCycleEndDate());
			buyBillCycleManagement.setCheckoutCycle(buyBillCycleManagementOld.getCheckoutCycle());
			buyBillCycleManagement.setBillStatementDate(buyBillCycleManagementOld.getBillStatementDate());
			buyBillCycleManagement.setBillDealStatus(buyBillCycleManagementOld.getBillDealStatus());
			buyBillCycleManagement.setCreateUser(buyBillCycleManagementOld.getCreateUser());
			buyBillCycleManagement.setCreateTime(buyBillCycleManagementOld.getCreateTime());
			//驳回后恢复历史账单周期数据
			billCycleCount = updateByPrimaryKeySelective(buyBillCycleManagement);

			//回复卖家历史数据
			BSellerBillCycleManagement sellerBillCycle = new BSellerBillCycleManagement();
			sellerBillCycle.setId(id);
			sellerBillCycle.setBuyCompanyId(buyBillCycleManagementOld.getBuyCompanyId());
			sellerBillCycle.setSellerCompanyId(buyBillCycleManagementOld.getSellerCompanyId());
			sellerBillCycle.setCycleStartDate(buyBillCycleManagementOld.getCycleStartDate());
			sellerBillCycle.setCycleEndDate(buyBillCycleManagementOld.getCycleEndDate());
			sellerBillCycle.setCheckoutCycle(buyBillCycleManagementOld.getCheckoutCycle());
			sellerBillCycle.setBillStatementDate(buyBillCycleManagementOld.getBillStatementDate());
			sellerBillCycle.setBillDealStatus(buyBillCycleManagementOld.getBillDealStatus());
			sellerBillCycle.setCreateUser(buyBillCycleManagementOld.getCreateUser());
			sellerBillCycle.setCreateTime(buyBillCycleManagementOld.getCreateTime());
			//恢复卖家账单周期
			sellerBillCycleService.addSellerBillCycle(sellerBillCycle);
			if(billCycleCount >0){
				//删除申请表中关联的利息
				buyBillCycleInterestService.deleteByPrimaryKey(id);
				String lasteDateBillCycleOldId = buyBillCycleManagementOld.getId();
				//根据历史最近账单周期编号查询关联利息
				List<BuyBillInterestOld> billInterestOldList = buyBillInterestOldService.queryBillInterestOld(lasteDateBillCycleOldId);

				//驳回后恢复历史账单关联利息
				for(BuyBillInterestOld buyBillInterestOld : billInterestOldList){
					Map<String, Object> interMap = new HashMap<String, Object>();
					interMap.put("id", ShiroUtils.getUid());
					//interMap.put("billCycleId", buyBillCycleManagementOld.getOldBillCycleId());
					interMap.put("billCycleId", id);
					interMap.put("overdueDate", buyBillInterestOld.getOverdueDate());
					interMap.put("overdueInterest", buyBillInterestOld.getOverdueInterest());
					interMap.put("interestCalculationMethod", buyBillInterestOld.getInterestCalculationMethod());
					buyBillCycleInterestService.saveInterestInfo(interMap);
				}
				//根据账单周期编号删除历史最近周期数据
				int deleteOldCycleCount = buyBillCycleManagementOldService.deleteNewBillCycleOld(lasteDateBillCycleOldId);
				//根据账单周期编号删除历史账单周期关联历史利息
				int deleteInterestOldCount = buyBillInterestOldService.deleteNewInterestOld(lasteDateBillCycleOldId);
				if(deleteInterestOldCount>0){
					json.put("success", true);
					json.put("msg", "成功！");
				}else{
					json.put("success", false);
					json.put("msg", "失败！");
				}
			}
		}else{
			buyBillCycleManagement.setId(id);
			buyBillCycleManagement.setBillDealStatus("4");
			int result = updateByPrimaryKeySelective(buyBillCycleManagement);
			if(result>0){
				json.put("success", true);
				json.put("msg", "成功！");
			}else{
				json.put("success", false);
				json.put("msg", "失败！");
			}
		}
		return json;*/
	}
}
