package com.nuotai.trading.buyer.service;

import com.nuotai.trading.buyer.dao.BuyOrderProductMapper;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.utils.SearchPageUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BuyOrderProductService {
	@Autowired
	private BuyOrderProductMapper orderProductMapper;

	public int deleteByPrimaryKey(String id) {
		return orderProductMapper.deleteByPrimaryKey(id);
	}

	public int insert(BuyOrderProduct record) {
		return orderProductMapper.insert(record);
	}

	public int insertSelective(BuyOrderProduct record) {
		return orderProductMapper.insertSelective(record);
	}

	public BuyOrderProduct selectByPrimaryKey(String id) {
		return orderProductMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BuyOrderProduct record) {
		return orderProductMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(BuyOrderProduct record) {
		return orderProductMapper.updateByPrimaryKey(record);
	}

	public List<BuyOrderProduct> selectByOrderId(String orderId) {
		return orderProductMapper.selectByOrderId(orderId);
	}

	/**
	 * 采购商品汇总-已计划采购数量
	 * @param skuOid
	 * @return
	 */
	public Map<String,Object> selectAlreadyPlanPurchaseNumBySkuOid(Map<String, Object> map) {
		return orderProductMapper.selectAlreadyPlanPurchaseNumBySkuOid(map);
	}
	
	public Map<String,Object> selectCancelPlanPurchaseNumBySkuOid(Map<String, Object> map) {
		return orderProductMapper.selectCancelPurchaseNumBySkuOid(map);
	}
	
	public Map<String,Object> selectRejectPlanPurchaseNumBySkuOid(Map<String, Object> map) {
		return orderProductMapper.selectRejectPurchaseNumBySkuOid(map);
	}
	
	public Map<String,Object> selectStopPlanPurchaseNumBySkuOid(Map<String, Object> map) {
		return orderProductMapper.selectStopPurchaseNumBySkuOid(map);
	}
	
	public BuyOrderProduct selectByOrderIdAndBarcode(Map<String, Object> map){
		return orderProductMapper.selectByOrderIdAndBarcode(map);
	}

	/**
	 * 采购商品汇总-已下单未审核数量
	 * @param skuOid
	 * @return
	 */
	public Map<String,Object> selectLockGoodsnumberBySkuOid(Map<String, Object> map) {
		return orderProductMapper.selectLockGoodsnumberBySkuOid(map);
	}
	
	public String selectOrderUnArrivalNumByBarcode(Map<String, Object> map) {
		return orderProductMapper.selectOrderUnArrivalNumByBarcode(map);
	}
	
	//售后模块-查询所有已收货商品
    public List<Map<String,Object>> selectOrderReceiveProducts(SearchPageUtil searchPageUtil){
    	return orderProductMapper.selectOrderReceiveProducts(searchPageUtil);
    }
    
    public List<BuyOrderProduct> selectByApplyCode(String applyCode){
    	return orderProductMapper.selectByApplyCode(applyCode);
    }
}
