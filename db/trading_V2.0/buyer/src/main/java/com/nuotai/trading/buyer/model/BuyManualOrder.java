package com.nuotai.trading.buyer.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BuyManualOrder {
    private String id;
    // 订单主键Id
    private String orderId;
    // 运费
    private BigDecimal postage;
    // 采购费用
    private BigDecimal purchasePrice;
    // 本次费用
    private BigDecimal thisPrice;
    // 付款金额
    private BigDecimal paymentPrice;
    // 付款方式0:银行卡转账 1:现金 2:网银支付
    private Integer paymentType;
    // 付款日期
    private Date paymentDate;
    // 付款人
    private String paymentPerson;
    // 经办人
    private String handler;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 创建时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人姓名
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人姓名
    private String delName;
    // 删除时间
    private Date delDate;
    // 采购凭证(多个，分割)
    private String purchVoucher;
    // 付款凭证(多个，分割)
    private String paymentVoucher;
    // 合同原件(多个，分割)
    private String contractFile;

}