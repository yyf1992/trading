package com.nuotai.trading.buyer.service.seller;

import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationAdvanceMapper;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliationAdvance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BSellerBillReconciliationAdvanceService {

    private static final Logger LOG = LoggerFactory.getLogger(BSellerBillReconciliationAdvanceService.class);

	@Autowired
	private BSellerBillReconciliationAdvanceMapper sellerBillReconciliationAdvanceMapper;
	
	public BSellerBillReconciliationAdvance get(String id){
		return sellerBillReconciliationAdvanceMapper.get(id);
	}
	
	public List<BSellerBillReconciliationAdvance> queryList(Map<String, Object> map){
		return sellerBillReconciliationAdvanceMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerBillReconciliationAdvanceMapper.queryCount(map);
	}
	
	public void add(BSellerBillReconciliationAdvance sellerBillReconciliationAdvance){
		sellerBillReconciliationAdvanceMapper.add(sellerBillReconciliationAdvance);
	}
	
	public void update(BSellerBillReconciliationAdvance sellerBillReconciliationAdvance){
		sellerBillReconciliationAdvanceMapper.update(sellerBillReconciliationAdvance);
	}
	
	public void delete(String id){
		sellerBillReconciliationAdvanceMapper.delete(id);
	}
	

}
