package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.SellerContract;
import com.nuotai.trading.dao.BaseDao;

/**
 * 
 * 卖家合同
 * @author liuhui
 * 2017-08-07 11:01:28
 */
public interface BSellerContractMapper extends BaseDao<SellerContract> {

    SellerContract getByBuyContractId(String buyContractId);
}
