package com.nuotai.trading.buyer.service.seller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nuotai.trading.buyer.dao.seller.BSellerCustomerMapper;

@Service
public class BSellerCustomerService {
	@Autowired
	private BSellerCustomerMapper buBSellerCustomerMapper;

	public List<Map<String, Object>> getSupplierAfterSaleRate(Map<String, Object> param) {
		return buBSellerCustomerMapper.getSupplierAfterSaleRate(param);
	}
}
