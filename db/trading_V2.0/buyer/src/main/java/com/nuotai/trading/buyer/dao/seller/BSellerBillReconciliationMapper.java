package com.nuotai.trading.buyer.dao.seller;

import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliation;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface BSellerBillReconciliationMapper extends BaseDao<BSellerBillReconciliation>{
	//查询账单周期列表
	List<BSellerBillReconciliation> getBillReconciliationList(SearchPageUtil searchPageUtil);

	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);

	//添加账单对账数据
	int saveBillReconciliationInfo(BSellerBillReconciliation bSellerBillReconciliation);

	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);

	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);

    int deleteByPrimaryKey(String id);

    int insert(BSellerBillReconciliation record);
    int insertOrderReconcilion(BSellerBillReconciliation record);

    int insertSelective(BSellerBillReconciliation record);

    //根据条件查询账单详细
    BSellerBillReconciliation selectByPrimaryKey(Map<String, Object> map);

    //根据编号修改账单状态
    int updateByPrimaryKeySelective(BSellerBillReconciliation record);

    int updateByPrimaryKey(BSellerBillReconciliation record);
    int updateDealStatusByOrderId(Map<String, Object> map);
    int deleteByOrderId(String orderId);
}