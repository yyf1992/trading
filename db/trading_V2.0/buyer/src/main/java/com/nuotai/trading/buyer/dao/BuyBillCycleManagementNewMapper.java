package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.buyer.model.BuyBillCycleManagementNew;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 11:45:51
 */
@Component()
public interface BuyBillCycleManagementNewMapper extends BaseDao<BuyBillCycleManagementNew> {
    //修改申请数据列表查询
    List<BuyBillCycleManagementNew> queryListNewBillCycle(Map<String,Object> map);
	//添加修改申请
    int addNewBillCycle(BuyBillCycleManagementNew newBuyBillCycle);
    //根据编号查询修改数据
    BuyBillCycleManagementNew queryBillCycleByNewId(String id);
    //查询最近修改数据
    BuyBillCycleManagementNew queryNowBillCycleByNewId(String id);
    //根据编号修改
    int updateNewBillCycle(BuyBillCycleManagementNew newBuyBillCycle);
}
