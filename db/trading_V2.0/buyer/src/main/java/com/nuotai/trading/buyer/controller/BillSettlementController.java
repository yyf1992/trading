package com.nuotai.trading.buyer.controller;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

//import com.nuotai.trading.seller.service.SellerBillCycleService;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-9-1
 */

@Controller
@RequestMapping("platform/buyer/billSettlement")
public class BillSettlementController extends BaseController {
	@Autowired
	private SysUserService sysUserService;//用户信息
	@Autowired
	private BuyCompanyService buyCompanyService; //公司管理
	
	/**
	 * 发票列表查询
	 * @return
	 */
	@RequestMapping("billSettlementList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		return "platform/buyer/billcycles/billsettlement/billSettlementList";
	}
}
