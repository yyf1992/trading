package com.nuotai.trading.buyer.model.seller;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 卖家合同
 * 
 * @author liuhui
 * @date 2017-08-25 09:04:10
 */
@Data
public class SellerContract implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键ID
	private String id;
	//买方合同ID
	private String buyerContractId;
	//合同编号
	private String contractNo;
	//合同名称
	private String contractName;
	//发起人公司ID
	private String sponsorId;
	//发起人公司名称
	private String sponsorName;
	//发起人公司联系人
	private String sponsorLinkman;
	//发起人公司电话
	private String sponsorPhone;
	//发起方式(0:买家发起 1:卖家发起)
	private String sponsorType;
	//客户id
	private String customerId;
	//客户公司名称
	private String customerName;
	//客户联系人
	private String customerLinkman;
	//客户手机号
	private String customerPhone;
	//合同内容地址
	private String contentAddress;
	//状态0:草稿箱1：审批中 2：对方审批中3待传合同照4：协议达成5：已驳回
	private Integer status;
	//客户审批状态 0:待审批 1:审批通过 2:审批未通过
	private Integer buyerApproveStatus;
	//我的审批状态 0:待审批 1:审批通过 2:审批未通过
	private Integer sellerApproveStatus;
	//备注
	private String remark;
	//开始有效期
	private Date validPeriodStart;
	//截止有效期
	private Date validPeriodEnd;
	//创建人公司
	private String createrCompanyId;
	//创建人id
	private String createrId;
	//创建人姓名
	private String createrName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isDel;
	//删除人id
	private String delId;
	//删除人名称
	private String delName;
	//删除时间
	private Date delDate;
}
