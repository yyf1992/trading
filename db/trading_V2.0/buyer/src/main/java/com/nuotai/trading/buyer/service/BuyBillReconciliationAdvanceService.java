package com.nuotai.trading.buyer.service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationAdvanceMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationAdvanceMapper;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvance;
import com.nuotai.trading.buyer.model.BuyBillReconciliationPayment;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliation;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliationAdvance;
import com.nuotai.trading.dao.*;
import com.nuotai.trading.model.*;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BuyBillReconciliationAdvanceService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyBillReconciliationAdvanceService.class);

	@Autowired
	private BuyBillReconciliationAdvanceMapper buyBillRecAdvanceMapper;
	@Autowired
	private BSellerBillReconciliationAdvanceMapper sellerBillRecAdvanceMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	@Autowired
	private SysVerifyHeaderMapper sysVerifyHeaderMapper;
	@Autowired
	private SysVerifyCopyMapper sysVerifyCopyMapper;
	@Autowired
	private TradeVerifyCopyMapper tradeVerifyCopyMapper;
	
	public BuyBillReconciliationAdvance get(String id){
		return buyBillRecAdvanceMapper.get(id);
	}

	//列表数据查询
	public List<BuyBillReconciliationAdvance> queryAdvanceList(SearchPageUtil searchPageUtil){
		return buyBillRecAdvanceMapper.queryAdvanceList(searchPageUtil);
	}

	//根据审批状态查询数量
	public int getAdvanceCountByStatus(Map<String, Object> map) {
		return buyBillRecAdvanceMapper.getAdvanceCountByStatus(map);
	}
	//根据状态查询预付款数量
	public void queryAdvanceCountByStatus(Map<String, Object> advanceMap){
		//查询所有状态数量
		advanceMap.put("advanceStatusCount", "0");
		int advanceCount = getAdvanceCountByStatus(advanceMap);

		//查询待内部审批
		advanceMap.put("advanceStatusCount", "1");
		int insideApprovalCount = getAdvanceCountByStatus(advanceMap);
		//内部审批通过/待提交卖家
		advanceMap.put("advanceStatusCount", "2");
		int insideAcceptCenter = getAdvanceCountByStatus(advanceMap);
		//内部审批通过/待提交卖家
		advanceMap.put("advanceStatusCount", "3");
		int insideAcceptCount = getAdvanceCountByStatus(advanceMap);
		//查询内部审批驳回数量
		advanceMap.put("advanceStatusCount", "4");
		int insideRejectCount = getAdvanceCountByStatus(advanceMap);
		//待卖家审批中数量
		advanceMap.put("advanceStatusCount", "5");
		int advanceApproveCount = getAdvanceCountByStatus(advanceMap);
		//卖家审批通过数量
		advanceMap.put("advanceStatusCount", "6");
		int advancePassCount = getAdvanceCountByStatus(advanceMap);
		//卖家审批驳回数量
		advanceMap.put("advanceStatusCount", "7");
		int advanceRejectCount = getAdvanceCountByStatus(advanceMap);

		advanceMap.put("advanceCount", advanceCount);
		advanceMap.put("insideAcceptCenter",insideAcceptCenter);
		advanceMap.put("insideApprovalCount", insideApprovalCount);
		advanceMap.put("insideAcceptCount", insideAcceptCount);
		advanceMap.put("insideRejectCount", insideRejectCount);
		advanceMap.put("advanceApproveCount", advanceApproveCount);
		advanceMap.put("advancePassCount",advancePassCount);
		advanceMap.put("advanceRejectCount",advanceRejectCount);
	}

	//根据公司名称查询下单人
	public List<Map<String,Object>> queryBillUser(Map<String,Object> billUserMap){
		List<Map<String,Object>> createBillUserList = buyBillRecAdvanceMapper.queryUserBuyCompany(billUserMap);
		return createBillUserList;
	}

	//根据公司编号及下单人编号查询是否存在
	public JSONObject isOrNoBillUser(Map<String,Object> isOrNoMap){
		JSONObject json = new JSONObject();
		int advanceCount = buyBillRecAdvanceMapper.isOrNoBillUser(isOrNoMap);

		if(advanceCount == 0){
			json.put("success", true);
			json.put("msg", "该用户不存在！");
		}else {
			json.put("error", false);
			json.put("msg", "该采购员已存在预付款账户，请修改或选择其他下单人！");
		}
		return json;
	}

	//添加预付款数据
	public JSONObject saveAdvanceInfo(Map<String,Object> advanceMap){
		JSONObject json = new JSONObject();
		String advanceId = ShiroUtils.getUid();
		String buyCompanyId = ShiroUtils.getCompId();
		String buyCompanyName = ShiroUtils.getCompanyName();
		String createUserId = ShiroUtils.getUserId();
		String createUserName = ShiroUtils.getUserName();

		String sellerCompanyId = advanceMap.get("sellerCompanyId").toString();
		String createBillUserId = advanceMap.get("userId").toString();
		String taskRemarks = advanceMap.get("taskRemarks").toString();

		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setId(advanceId);
		buyBillRecAdvance.setSellerCompanyId(sellerCompanyId);
		buyBillRecAdvance.setBuyCompanyId(buyCompanyId);
		buyBillRecAdvance.setBuyCompanyName(buyCompanyName);
		buyBillRecAdvance.setCreateBillUserId(createBillUserId);
		buyBillRecAdvance.setCreateUserId(createUserId);
		buyBillRecAdvance.setCreateUserName(createUserName);
		buyBillRecAdvance.setCreateRemarks(taskRemarks);

		int saveCount = buyBillRecAdvanceMapper.saveAdvanceInfo(buyBillRecAdvance);
		if(saveCount > 0){
			Map<String,Object> buyAdvancemap = new HashMap<String,Object>();
			buyAdvancemap.put("id",advanceId);
			//账单对账详情
			BuyBillReconciliationAdvance buyAdvance = buyBillRecAdvanceMapper.queryAdvanceInfo(buyAdvancemap);
			//同步到卖家预付款
			BSellerBillReconciliationAdvance sellerAdvance = new BSellerBillReconciliationAdvance();
			sellerAdvance.setId(advanceId);
			sellerAdvance.setBuyCompanyId(buyAdvance.getBuyCompanyId());
			sellerAdvance.setBuyCompanyName(buyAdvance.getBuyCompanyName());
			sellerAdvance.setSellerCompanyId(buyAdvance.getSellerCompanyId());
			sellerAdvance.setSellerCompanyName(buyAdvance.getSellerCompanyName());
			sellerAdvance.setCreateBillUserId(buyAdvance.getCreateBillUserId());
			sellerAdvance.setCreateBillUserName(buyAdvance.getCreateBillUserName());
			sellerAdvance.setCreateUserId(buyAdvance.getCreateUserId());
			sellerAdvance.setCreateUserName(buyAdvance.getCreateUserName());
			sellerAdvance.setCreateTime(buyAdvance.getCreateTime());
			sellerAdvance.setCreateRemarks(buyAdvance.getCreateRemarks());
			sellerAdvance.setFilesAddress(buyAdvance.getFileAddress());
			sellerAdvance.setUsableTotal(buyAdvance.getUsableTotal());
			sellerAdvance.setLockTotal(buyAdvance.getLockTotal());
			sellerAdvance.setAdvanceTotal(buyAdvance.getAdvanceTotal());

			sellerBillRecAdvanceMapper.saveSellerAdvance(sellerAdvance);

			json.put("success", true);
			json.put("msg", "预付款账户添加成功！");
		}else {
			json.put("error", false);
			json.put("msg", "预付款账户添加失败！");
		}
		return json;
	}

	//付款待审批查询
	public void approveBillAdvance(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		//params.put("advanceStatus", "2");
		Map<String, String> params1 = new HashMap<>();
		params1.put("companyId", ShiroUtils.getCompId());
		params1.put("userId", ShiroUtils.getUserId());
		params1.put("siteId", "18030316174277880699");//阿里
		//params1.put("siteId", "18022814242829641325");//测试
		//获得所有未审批的id
		List<String> acceptAdvanceIdList = tradeVerifyHeaderMapper.getApprovedOrderId(params1);
		List<String> acceptAdvanceIdListNew = new ArrayList<>();

		List<BuyBillReconciliationAdvance> billAdvanceList = buyBillRecAdvanceMapper.selAdvanceList(params);
		//List<BuyBillReconciliation> billRecList = billReconciliationMapper.selectByPrimaryKeyList(params);
		if(!billAdvanceList.isEmpty()){
			Map<String, String> acceptAdvanceIdMap = new HashMap<>();
			for(BuyBillReconciliationAdvance billAdvance : billAdvanceList){
				if(null != billAdvance.getAcceptId()){
					String acceptId =billAdvance.getAcceptId();
					acceptAdvanceIdMap.put(acceptId, acceptId);
				}

			}
			if(!acceptAdvanceIdList.isEmpty()){
				for(String acceptId : acceptAdvanceIdList){
					if(acceptAdvanceIdMap.containsKey(acceptId)){
						acceptAdvanceIdListNew.add(acceptId);
					}
				}
			}
		}else{
			acceptAdvanceIdListNew = acceptAdvanceIdList;
		}

		params.put("approved", true);
		params.put("acceptAdvanceIdList", acceptAdvanceIdListNew);
		searchPageUtil.setObject(params);
		List<BuyBillReconciliationAdvance> billAdvanceInfoList = buyBillRecAdvanceMapper.queryAdvanceList(searchPageUtil);
		searchPageUtil.getPage().setList(billAdvanceInfoList);
	}

	//一键通过
	public JSONObject verifyBillAdvanceAll(String acceptIdStr) {
		JSONObject json = new JSONObject();
		if(!StringUtils.isEmpty(acceptIdStr)){
			String[] idStr = acceptIdStr.split(",");
			for(String acceptId : idStr){
				verifyApproved(acceptId);
				json.put("flag",true);
				json.put("msg","成功");
			}
		}else{
			json.put("flag",false);
			json.put("msg","请选择数据");
		}
		return json;
	}

	/**
	 * 审批
	 * @param acceptId
	 */
	public void verifyApproved(String acceptId){
		//获得审批主表
		TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(acceptId);
		//获得审批子表，自己审批的子表
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("headerId", header.getId());
		params.put("userId", ShiroUtils.getUserId());
		params.put("status", "0");
		TradeVerifyPocess pocessOwn = tradeVerifyPocessMapper.getPocessOwn(params);
		if(pocessOwn != null && pocessOwn.getId() != null){
			pocessOwn.setStatus("1");
			pocessOwn.setRemark("同意(一键通过)");
			pocessOwn.setEndDate(new Date());
			int upadte = tradeVerifyPocessMapper.update(pocessOwn);
			if(upadte>0){
				//获得之后的审批流程
				params.put("verifyIndex", pocessOwn.getVerifyIndex());
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.getNextVerify(params);
				if(pocessList.isEmpty()){
					//审批流程结束
					header.setStatus("1");
					header.setUpdateDate(new Date());
					int updateHeader = tradeVerifyHeaderMapper.update(header);
					if(updateHeader > 0){
						//判断是否需要抄送
						SysVerifyHeader verifyHeader = sysVerifyHeaderMapper.get(header.getSiteId());
						//0-仅全部同意后通知；1-仅发起时通知；2-发起时和全部同意后均通知
						if("0".equals(verifyHeader.getCopyType())){
							List<SysVerifyCopy> verifyCopyList = sysVerifyCopyMapper.getListByHeaderId(verifyHeader.getId());
							if(!verifyCopyList.isEmpty()){
								for(SysVerifyCopy verifyCopy : verifyCopyList){
									TradeVerifyCopy copy = new TradeVerifyCopy();
									copy.setId(ShiroUtils.getUid());
									copy.setHeaderId(header.getId());
									copy.setUserId(verifyCopy.getUserId());
									copy.setUserName(verifyCopy.getUserName());
									tradeVerifyCopyMapper.add(copy);
									//发送顶顶消息
									DingDingUtils.verifyDingDingCopy(copy.getUserId(), header.getId());
								}
							}
						}
						//审批同意之后的操作
						//通知发起人
						DingDingUtils.verifyDingDingOwn(header.getId());
						//审批通过之后的操作
						acceptAdvanceSuccess(acceptId);
					}
				}else{
					TradeVerifyPocess pocess = pocessList.get(0);
					pocess.setStartDate(new Date());
					tradeVerifyPocessMapper.update(pocess);
					//发送钉钉消息
					DingDingUtils.verifyDingDingMessage(pocess.getUserId(), pocess.getHeaderId());
				}
			}
		}
	}


	//根据付款状态修改付款信息
	public List<String> acceptAdvance(Map<String,Object> updateAdvanceMap){
		List<String> idList = new ArrayList<String>();
		String acceptId = updateAdvanceMap.get("acceptId").toString();

		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setAcceptAdvanceId(acceptId);
		buyBillRecAdvance.setStatus("2");//内部审批中
		int updateCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);
		if(updateCount > 0){
			idList.add(acceptId);
		}
		return idList;
	}

	//内部审批通过
	public JSONObject acceptAdvanceSuccess(String acceptAdvanceId){
		JSONObject json = new JSONObject();
		//预付款数据拼装
		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setAcceptAdvanceId(acceptAdvanceId);
		buyBillRecAdvance.setStatus("3");

		int updatePaymentCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);
		if(updatePaymentCount > 0){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("acceptAdvanceId",acceptAdvanceId);
			//账单对账详情
			BuyBillReconciliationAdvance buyAdvanceOld = buyBillRecAdvanceMapper.queryAdvanceInfo(map);
			//修改驳回的修改记录
			Map<String,Object> updateAdvanceEditMap = new HashMap<String,Object>();
			updateAdvanceEditMap.put("advanceId",buyAdvanceOld.getId());
			updateAdvanceEditMap.put("createBillUserId",buyAdvanceOld.getCreateBillUserId());
			updateAdvanceEditMap.put("editStatus","2");//修改状态： 1 提交申请 ，2 审批通过， 3 审批驳回
			buyBillRecAdvanceMapper.updateAdvanceEdit(updateAdvanceEditMap);
			json.put("success", true);
			json.put("msg", "成功！");

		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//内部付款审批驳回
	public JSONObject acceptAdvanceError(String acceptAdvanceId){
		JSONObject json = new JSONObject();
		//预付款数据拼装
		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setAcceptAdvanceId(acceptAdvanceId);
		buyBillRecAdvance.setStatus("4");

		int updateAdvanceCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);
		if(updateAdvanceCount > 0){
			Map<String,Object> map = new HashMap<String,Object>();
			//map.put("sellerCompanyId",ShiroUtils.getCompId());
			map.put("acceptAdvanceId",acceptAdvanceId);
			//账单对账详情
			BuyBillReconciliationAdvance buyAdvanceOld = buyBillRecAdvanceMapper.queryAdvanceInfo(map);
			//修改驳回的修改记录
			Map<String,Object> updateAdvanceEditMap = new HashMap<String,Object>();
			updateAdvanceEditMap.put("advanceId",buyAdvanceOld.getId());
			updateAdvanceEditMap.put("createBillUserId",buyAdvanceOld.getCreateBillUserId());
			updateAdvanceEditMap.put("editStatus","3");//修改状态： 1 提交申请 ，2 审批通过， 3 审批驳回
			buyBillRecAdvanceMapper.updateAdvanceEdit(updateAdvanceEditMap);

			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	//根据付款状态修改付款信息 驳回再次修改
	public void acceptUpdateAdvance(Map<String,Object> updateAdvanceMap){
		List<String> idList = new ArrayList<String>();
		String acceptId = updateAdvanceMap.get("acceptId").toString();
		//预付款数据拼装
		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setAcceptAdvanceId(acceptId);
		buyBillRecAdvance.setStatus("2");

		int updateAdvanceCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);
		if(updateAdvanceCount > 0){
			//String acceptPaymentId = updateAdvanceMap.get("acceptPaymentId").toString();
			//4.若有审批，则重置审批状态
			TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(acceptId);
			if(!ObjectUtil.isEmpty(header)){
				header.setStatus("0");
				tradeVerifyHeaderMapper.update(header);
				//获得最大index
				int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
				//添加审批
				TradeVerifyPocess pocessUpdate = new TradeVerifyPocess();
				pocessUpdate.setId(ShiroUtils.getUid());
				pocessUpdate.setHeaderId(header.getId());
				pocessUpdate.setUserId(ShiroUtils.getUserId());
				pocessUpdate.setUserName(ShiroUtils.getUserName());
				pocessUpdate.setStatus("7");//买家修改
				pocessUpdate.setRemark("订单修改后再次提交");
				pocessUpdate.setVerifyIndex(maxIndex+1);
				pocessUpdate.setStartDate(new Date());
				pocessUpdate.setEndDate(new Date());
				tradeVerifyPocessMapper.add(pocessUpdate);
				maxIndex+=1;
				int index = maxIndex;
				List<TradeVerifyPocess> pocessList = tradeVerifyPocessMapper.queryListByHeaderId(header.getId());
				//删除未审批的数据
				tradeVerifyPocessMapper.deleteNotVerify(header.getId());

				if(!pocessList.isEmpty()){
					Map<String,String> userMap = new HashMap<>();
					for(TradeVerifyPocess pocess : pocessList){
						if("0".endsWith(pocess.getStatus())
								|| "1".endsWith(pocess.getStatus())
								|| "2".endsWith(pocess.getStatus())){
							if(userMap.containsKey(pocess.getUserId())){
								continue;
							}
							TradeVerifyPocess pocessNew = new TradeVerifyPocess();
							pocessNew.setId(ShiroUtils.getUid());
							pocessNew.setHeaderId(header.getId());
							pocessNew.setUserId(pocess.getUserId());
							pocessNew.setUserName(pocess.getUserName());
							pocessNew.setStatus("0");//等待审批
							pocessNew.setRemark(null);
							pocessNew.setVerifyIndex(index+1);
							pocessNew.setStartDate(new Date());
							pocessNew.setEndDate(new Date());
							tradeVerifyPocessMapper.add(pocessNew);
							userMap.put(pocess.getUserId(), pocess.getUserId());
							if(index == maxIndex){
								//待审批人员发送钉钉消息
								DingDingUtils.verifyDingDingMessage(pocessNew.getUserId(), pocessNew.getHeaderId());
							}
							index++;
						}
					}
				}
			}
		}
	}

	//内部审批回显查询
	public Map<String,Object> showAcceptAdvanceInfo(String acceptAdvanceId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("acceptAdvanceId",acceptAdvanceId);
		//账单对账详情
		BuyBillReconciliationAdvance buyAdvanceOld = buyBillRecAdvanceMapper.queryAdvanceInfo(map);
		//账单对账详情
		Map<String,Object> editMap = new HashMap<String,Object>();
		editMap.put("advanceId",buyAdvanceOld.getId());
		editMap.put("createBillUserId",buyAdvanceOld.getCreateBillUserId());
		String advanceStatus = buyAdvanceOld.getStatus();
		if("2".equals(advanceStatus)||"3".equals(advanceStatus)||"5".equals(advanceStatus)){
			editMap.put("editStatus","1");
		}else if("6".equals(advanceStatus)){
			editMap.put("editStatus","2");
		}else if("4".equals(advanceStatus)||"7".equals(advanceStatus)){
			editMap.put("editStatus","3");
		}
		List<Map<String,Object>> buyBillRecAdvanceList = buyBillRecAdvanceMapper.queryAdvanceEditList(editMap);
		//返回审批详情
		Map<String,Object> buyBillRecAdvanceMap = new HashMap<String,Object>();
		buyBillRecAdvanceMap.put("buyBillRecAdvanceList",buyBillRecAdvanceList);
		buyBillRecAdvanceMap.put("createBillUserName",buyAdvanceOld.getCreateBillUserName());
		buyBillRecAdvanceMap.put("status",buyAdvanceOld.getStatus());
		buyBillRecAdvanceMap.put("sellerCompanyName",buyAdvanceOld.getSellerCompanyName());
		return buyBillRecAdvanceMap;
	}

	//内部数据查询
	public BuyBillReconciliationAdvance queryAcceptAdvanceInfo(String advanceId){
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put("buyCompanyId",ShiroUtils.getCompId());
		map.put("id",advanceId);
		//账单对账详情
		BuyBillReconciliationAdvance buyBillRecAdvance = buyBillRecAdvanceMapper.queryAdvanceInfo(map);
		return buyBillRecAdvance;
	}

	//提交卖家审批
	public int launchAdvance(String advanceId,String advanceStatus){
		//预付款数据拼装
		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setId(advanceId);
		buyBillRecAdvance.setStatus(advanceStatus);
		int updateAdvanceCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);
		if(updateAdvanceCount > 0){
			//同步数据到卖家
			Map<String,Object> sellerAdvanceMap = new HashMap<String,Object>();
			sellerAdvanceMap.put("id",advanceId);
			List<BSellerBillReconciliationAdvance> sellerRecAdvanceList = sellerBillRecAdvanceMapper.sellerRecAdvanceList(sellerAdvanceMap);
			if(null != sellerRecAdvanceList){
				Map<String,Object> buyAdvancemap = new HashMap<String,Object>();
				//buyAdvancemap.put("buyCompanyId",ShiroUtils.getCompId());
				buyAdvancemap.put("id",advanceId);
				//账单对账详情
				BuyBillReconciliationAdvance buyAdvance = buyBillRecAdvanceMapper.queryAdvanceInfo(buyAdvancemap);

				BSellerBillReconciliationAdvance sellerAdvance = new BSellerBillReconciliationAdvance();
				sellerAdvance.setId(advanceId);
				sellerAdvance.setStatus(advanceStatus);
				sellerAdvance.setBuyCompanyId(buyAdvance.getBuyCompanyId());
				sellerAdvance.setBuyCompanyName(buyAdvance.getBuyCompanyName());
				sellerAdvance.setSellerCompanyId(buyAdvance.getSellerCompanyId());
				sellerAdvance.setSellerCompanyName(buyAdvance.getSellerCompanyName());
				sellerAdvance.setCreateBillUserId(buyAdvance.getCreateBillUserId());
				sellerAdvance.setCreateBillUserName(buyAdvance.getCreateBillUserName());
				sellerAdvance.setCreateUserId(buyAdvance.getCreateUserId());
				sellerAdvance.setCreateUserName(buyAdvance.getCreateUserName());
				sellerAdvance.setCreateTime(buyAdvance.getCreateTime());
				sellerAdvance.setCreateRemarks(buyAdvance.getCreateRemarks());
				sellerAdvance.setFilesAddress(buyAdvance.getFileAddress());
				sellerAdvance.setUsableTotal(buyAdvance.getUsableTotal());
				sellerAdvance.setLockTotal(buyAdvance.getLockTotal());
				sellerAdvance.setAdvanceTotal(buyAdvance.getAdvanceTotal());
			  if(sellerRecAdvanceList.size()>0){//再次修改
				  sellerBillRecAdvanceMapper.updateSellerAdvance(sellerAdvance);
			  }else {//新添加
				  sellerBillRecAdvanceMapper.saveSellerAdvance(sellerAdvance);
			  }
			}
		}
		return updateAdvanceCount;
	}

	//根据编号修改预付款
	public JSONObject updateAdvanceInfo(Map<String,Object> updateAdvanceMap){
		JSONObject json = new JSONObject();
		String advanceId = updateAdvanceMap.get("advanceId").toString();
		String usableTotalStr = updateAdvanceMap.get("usableTotal").toString();
		String advanceRemarks = updateAdvanceMap.get("advanceRemarks").toString();
		String advanceFileStr = updateAdvanceMap.get("advanceFileStr").toString();

		BigDecimal usableTotalNew = new BigDecimal(usableTotalStr);
		//查询原预付款信息
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put("buyCompanyId",ShiroUtils.getCompId());
		map.put("id",advanceId);
		//预付款详情
		BuyBillReconciliationAdvance buyBillRecAdvanceOld = buyBillRecAdvanceMapper.queryAdvanceInfo(map);


		BigDecimal usableTotalOld = buyBillRecAdvanceOld.getUsableTotal();
		BigDecimal advanceTotalOld = buyBillRecAdvanceOld.getAdvanceTotal();

		BigDecimal usableTotalEnd = usableTotalNew.add(usableTotalOld);
		BigDecimal advanceTotalEnd = usableTotalNew.add(advanceTotalOld);

		//查询驳回的充值记录
		Map<String,Object> editOldMap = new HashMap<String,Object>();
		editOldMap.put("advanceId",advanceId);
		editOldMap.put("editStatus","3");
		List<Map<String,Object>> advanceEditOld = buyBillRecAdvanceMapper.queryAdvanceEditOld(editOldMap);
		BigDecimal editUsaTotal = new BigDecimal(0);
		if(null != advanceEditOld && advanceEditOld.size()>0){
			for(Map<String,Object> advanceEditMap:advanceEditOld){
				String updateTotal = advanceEditMap.get("updateTotal").toString();
				editUsaTotal = new BigDecimal(updateTotal);
				usableTotalEnd = usableTotalEnd.subtract(editUsaTotal);
				advanceTotalEnd = advanceTotalEnd.subtract(editUsaTotal);
			}
		}

		//预付款数据拼装
		BuyBillReconciliationAdvance buyBillRecAdvance = new BuyBillReconciliationAdvance();
		buyBillRecAdvance.setId(advanceId);
		buyBillRecAdvance.setAcceptId(ShiroUtils.getUid());
		buyBillRecAdvance.setStatus("1");
		buyBillRecAdvance.setUsableTotal(usableTotalEnd);
		buyBillRecAdvance.setAdvanceTotal(advanceTotalEnd);
		buyBillRecAdvance.setCreateRemarks(advanceRemarks);
		//buyBillRecAdvance.setFileAddress(advanceFileStr);
		int updateAdvanceCount = buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);

		if(updateAdvanceCount > 0){
			//删除驳回的修改记录
			Map<String,Object> deleteAdvanceEditMap = new HashMap<String,Object>();
			deleteAdvanceEditMap.put("advanceId",advanceId);
			deleteAdvanceEditMap.put("createBillUserId",buyBillRecAdvanceOld.getCreateBillUserId());
			deleteAdvanceEditMap.put("editStatus","3");
			buyBillRecAdvanceMapper.deleteAdvanceEdit(deleteAdvanceEditMap);

			//添加预付款修改记录
			Map<String,Object> addAdvanceEditMap = new HashMap<String,Object>();
			addAdvanceEditMap.put("id",ShiroUtils.getUid());
			//addAdvanceEditMap.put("reconciliationId",reconciliationId);
			addAdvanceEditMap.put("advanceId",advanceId);
			addAdvanceEditMap.put("updateTotal",usableTotalNew);
			addAdvanceEditMap.put("createUserId",ShiroUtils.getUserId());
			addAdvanceEditMap.put("createUserName",ShiroUtils.getUserName());
			addAdvanceEditMap.put("createBillUserId",buyBillRecAdvanceOld.getCreateBillUserId());
			addAdvanceEditMap.put("createBillUserName",buyBillRecAdvanceOld.getCreateBillUserName());
			addAdvanceEditMap.put("fileAddress",advanceFileStr);
			addAdvanceEditMap.put("advanceRemarks",advanceRemarks);
			addAdvanceEditMap.put("editStatus","1");
			buyBillRecAdvanceMapper.addAdvanceEdit(addAdvanceEditMap);

			json.put("success", true);
			json.put("msg", "预付款充值成功！");
		}else {
			json.put("error", false);
			json.put("msg", "预付款充值失败！");
		}
		return json;
	}

	//根据对账单号查询自定义付款附件信息
	public List<Map<String,Object>> queryAdvanceEditList(Map<String,Object> editMap){
		List<Map<String,Object>> advanceEditList = buyBillRecAdvanceMapper.queryAdvanceEditList(editMap);
		return advanceEditList;
	}

	//根据条件查询预付款
	public List<BuyBillReconciliationAdvance> selAdvanceList(Map<String,Object> selMap){
		//根据条件查询预付款
		List<BuyBillReconciliationAdvance> advanceList = buyBillRecAdvanceMapper.selAdvanceList(selMap);
		return  advanceList;
	}

}
