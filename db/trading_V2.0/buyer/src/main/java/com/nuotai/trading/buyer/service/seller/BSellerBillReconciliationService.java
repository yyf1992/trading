package com.nuotai.trading.buyer.service.seller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.seller.BSellerBillReconciliationMapper;
import com.nuotai.trading.buyer.model.BuyBillCycleManagement;
import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.buyer.model.BuyDeliveryRecord;
import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.buyer.model.seller.BSellerBillReconciliation;
import com.nuotai.trading.buyer.service.BuyBillCycleService;
import com.nuotai.trading.buyer.service.BuyDeliveryRecordItemService;
import com.nuotai.trading.buyer.service.BuyDeliveryRecordService;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

//import com.nuotai.trading.seller.model.InterflowSellerBillCycleManagement;
//import com.nuotai.trading.seller.service.SellerBillCycleService;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class BSellerBillReconciliationService {
	@Autowired
	private BSellerBillReconciliationMapper sellerBillReconciliationMapper;
	@Autowired
	private BuyBillCycleService buyBillCycleService;//账单周期
	@Autowired
	private BuyDeliveryRecordService buyDeliveryRecordService;//买家发货
	@Autowired
	private BuyDeliveryRecordItemService buyDeliveryRecordItemService;//发货明细
	//@Autowired
	//private SellerBillCycleService sellerBillCycleService;

	// 根据id编号查询账单信息
	public BSellerBillReconciliation selectByPrimaryKey(Map<String,Object> map) {
		return sellerBillReconciliationMapper.selectByPrimaryKey(map);
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(BSellerBillReconciliation record) {
		return sellerBillReconciliationMapper.updateByPrimaryKeySelective(record);
	}

	//查询账单对账列表信息
	public List<BSellerBillReconciliation> getBillReconciliationList(
			SearchPageUtil searchPageUtil) {
		return sellerBillReconciliationMapper.getBillReconciliationList(searchPageUtil);
	}
	//向账单周期管理页面提供不同状态下的数量
	public void getBillCountByStatus(Map<String, Object> billMap){
		//查询待内部审批周期数
		billMap.put("billDealStatusCount", "1");
		int approvalBillReconciliationCount = queryBillStatusCount(billMap);
		//查询待对方审批周期数
		billMap.put("billDealStatusCount", "2");
		int acceptBillReconciliationCount = queryBillStatusCount(billMap);
		//查询审批已通过周期数
		billMap.put("billDealStatusCount", "3");
		int apprEndBillReconciliationCount = queryBillStatusCount(billMap);
		
		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillReconciliationCount", approvalBillReconciliationCount);
		billMap.put("acceptBillReconciliationCount", acceptBillReconciliationCount);
		billMap.put("apprEndBillReconciliationCount", apprEndBillReconciliationCount);
		
	}
	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return sellerBillReconciliationMapper.queryBillStatusCount(map);
	}

	//添加账单数据
	public void  insertSellerReconciliation(BSellerBillReconciliation sellerBuyBillReconciliation){
		sellerBillReconciliationMapper.saveBillReconciliationInfo(sellerBuyBillReconciliation);
	}
}
