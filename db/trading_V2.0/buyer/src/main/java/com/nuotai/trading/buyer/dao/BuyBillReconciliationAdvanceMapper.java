package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillReconciliationAdvance;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2018-02-10 14:13:51
 */
@Component
public interface BuyBillReconciliationAdvanceMapper extends BaseDao<BuyBillReconciliationAdvance> {
    //预付款列表查询
    List<BuyBillReconciliationAdvance> queryAdvanceList(SearchPageUtil searchPageUtil);
    //根据条件查询预付款
    List<BuyBillReconciliationAdvance> selAdvanceList(Map<String,Object> selMap);
    //根据状态查询数量
    int getAdvanceCountByStatus(Map<String,Object> map);
    //根据供应商查询下单人
    List<Map<String,Object>> queryUserBuyCompany(Map<String,Object> map);
    //查询下单人是否已经存在
    int isOrNoBillUser(Map<String,Object> isOrNoMap);
    //添加预付款信息
    int saveAdvanceInfo(BuyBillReconciliationAdvance buyBillReconciliationAdvance);
    //修改预付款信息
    int updateAdvanceInfo(BuyBillReconciliationAdvance buyBillReconciliationAdvance);
    //根据新增账单添加预付款记录
    int addAdvanceEdit(Map<String,Object> addMap);
    //删除预付款修改记录
    int deleteAdvanceEdit(Map<String,Object> deleteMap);
    //修改预修改记录付款状态
    int updateAdvanceEdit(Map<String,Object> editMap);
    //根据编号查询预付款详情
    BuyBillReconciliationAdvance queryAdvanceInfo(Map<String,Object> advanceMap);
    //根据条件查询预付款修改
    List<Map<String,Object>> queryAdvanceEditList(Map<String,Object> editMap);
    //根据编号查询预付款修改记录
    List<Map<String,Object>> queryAdvanceEditOld(Map<String,Object> editOldMap);
}
