package com.nuotai.trading.buyer.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.buyer.dao.BuySupplierAssessmentMapper;
import com.nuotai.trading.buyer.model.BuySupplierAssessment;
import com.nuotai.trading.utils.ShiroUtils;


@Service
@Transactional
public class BuySupplierAssessmentService {

    private static final Logger LOG = LoggerFactory.getLogger(BuySupplierAssessmentService.class);

	@Autowired
	private BuySupplierAssessmentMapper buySupplierAssessmentMapper;
	
	public BuySupplierAssessment get(String id){
		return buySupplierAssessmentMapper.get(id);
	}
	
	public List<BuySupplierAssessment> queryList(Map<String, Object> map){
		return buySupplierAssessmentMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buySupplierAssessmentMapper.queryCount(map);
	}
	
	public void add(BuySupplierAssessment buySupplierAssessment){
		buySupplierAssessmentMapper.add(buySupplierAssessment);
	}
	
	public void update(BuySupplierAssessment buySupplierAssessment){
		buySupplierAssessmentMapper.update(buySupplierAssessment);
	}
	
	public void delete(String id){
		buySupplierAssessmentMapper.delete(id);
	}
	 public int insert(BuySupplierAssessment buySupplierAssessment){
		 buySupplierAssessment.setId(ShiroUtils.getUid());
		 buySupplierAssessment.setCompanyId(ShiroUtils.getCompId());
		 buySupplierAssessment.setCreateDate(new Date());
		 buySupplierAssessment.setCreateId(ShiroUtils.getUserId());
		 buySupplierAssessment.setCreateName(ShiroUtils.getUserName());
		 return buySupplierAssessmentMapper.insert(buySupplierAssessment);
	 }

	public void updateAssesment(BuySupplierAssessment assesment) {
		assesment.setUpdateDate(new Date());
		assesment.setUpdateId(ShiroUtils.getUserId());
		assesment.setUpdateName(ShiroUtils.getUserName());
		buySupplierAssessmentMapper.updateAssesment(assesment);
	}

}
