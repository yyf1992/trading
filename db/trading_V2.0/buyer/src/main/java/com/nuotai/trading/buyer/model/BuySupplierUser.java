package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 用户与供应商关联表
 * 
 * @author "
 * @date 2018-03-23 15:16:33
 */
@Data
public class BuySupplierUser implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//用户主键
	private String userId;
	//供应商ID
	private String supplierId;
	//创建时间
	private Date createTime;
	//
	private String companyId;
}
