package com.nuotai.trading.buyer.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuyDeliveryRecordItem;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

/**
 * The interface Buy delivery record item mapper.
 *
 * @author gsf
 * @date 2017 -08-10
 */
@Component
public interface BuyDeliveryRecordItemMapper extends BaseDao<BuyDeliveryRecordItem> {

	/**
	 * Select product by delivery list.
	 *
	 * @param map the map
	 * @return the list
	 */
	List<Map<String, Object>> selectProductByDelivery(Map<String,Object> map);
	
	List<Map<String, Object>> selectProductByOrderId(Map<String,Object> map);

	/**
	 * Query delivery record item buy delivery record item.
	 * 根据条件查询详细
	 *
	 * @param map the map
	 * @return the buy delivery record item
	 */
	List<BuyDeliveryRecordItem> queryDeliveryRecordItem(Map<String,Object> map);

	/**
	 * Gets item by delivery id.
	 * 根据收货单ID获取明细
	 *
	 * @param map the map
	 * @return the item by delivery id
	 */
	List<BuyDeliveryRecordItem> getItemByDeliveryId(Map<String, Object> map);

	/**
	 * Update by primary key selective.
	 * 根据ID更新item信息
	 * @param itemUpd the item upd
	 */
	void updateByPrimaryKeySelective(BuyDeliveryRecordItem itemUpd);

	//根据详情编号查询订单号
	BuyDeliveryRecordItem queryOrderCodeByItemId(Map<String,Object> map);
	//获取到货数量
	List<Map<String,Object>> selectArrivalNum(Map<String, Object> param);
	
	List<Map<String,Object>> queryByOrderCode(String orderCode);
	
	List<BuyDeliveryRecordItem> queryOrderByItemId(String orderItemId);

	List<Map<String, Object>> getDeliveryByItemIdList(String id);
    
    /**
     * 计算到货数量
     * @param orderId
     * @return
     */
    int countArrivalNum(String orderId);
}
