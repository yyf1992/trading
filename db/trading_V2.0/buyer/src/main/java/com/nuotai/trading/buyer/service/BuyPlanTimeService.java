package com.nuotai.trading.buyer.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.buyer.dao.BuyPlanTimeMapper;
import com.nuotai.trading.buyer.model.BuyPlanTime;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class BuyPlanTimeService {

    private static final Logger LOG = LoggerFactory.getLogger(BuyPlanTimeService.class);

	@Autowired
	private BuyPlanTimeMapper buyPlanTimeMapper;
	
	public BuyPlanTime get(Integer id){
		return buyPlanTimeMapper.get(id);
	}
	
	public BuyPlanTime queryOne(){
		List<BuyPlanTime> planTimeList = buyPlanTimeMapper.queryList(null);
		return ObjectUtil.isEmpty(planTimeList)?null:planTimeList.get(0);
	}
	
	public List<BuyPlanTime> queryList(Map<String, Object> map){
		return buyPlanTimeMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return buyPlanTimeMapper.queryCount(map);
	}
	
	public void add(BuyPlanTime buyPlanTime){
		buyPlanTimeMapper.add(buyPlanTime);
	}
	
	public void update(BuyPlanTime buyPlanTime){
		buyPlanTimeMapper.update(buyPlanTime);
	}
	
	public void delete(Integer id){
		buyPlanTimeMapper.delete(id);
	}

	public void savePlanTimeSet(Map<String, Object> params) {
		BuyPlanTime planTime = queryOne();
		if(ObjectUtil.isEmpty(planTime)){
			planTime = new BuyPlanTime();
			planTime.setId(ShiroUtils.getUid());
			planTime.setSalePlanWeek(params.get("salePlanWeek")==null?"":params.get("salePlanWeek").toString());
			planTime.setConfirmPlanWeek(params.get("confirmPlanWeek")==null?"":params.get("confirmPlanWeek").toString());
			planTime.setApplyPlanWeek(params.get("applyPlanWeek")==null?"":params.get("applyPlanWeek").toString());
			planTime.setPurchaseWeek(params.get("purchaseWeek")==null?"":params.get("purchaseWeek").toString());
			planTime.setSalePlanInterval(Integer.parseInt(params.get("salePlanInterval")==null?"30":params.get("salePlanInterval").toString()));
			planTime.setSalePlanArea(Integer.parseInt(params.get("salePlanArea")==null?"7":params.get("salePlanArea").toString()));
			planTime.setOutPlanUser(params.get("outPlanUser")==null?"":params.get("outPlanUser").toString());
			planTime.setApplyPurchaseUser(params.get("applyPurchaseUser")==null?"":params.get("applyPurchaseUser").toString());
			planTime.setConfirmSaleplanUser(params.get("confirmSaleplanUser")==null?"":params.get("confirmSaleplanUser").toString());
			planTime.setPurchaseUser(params.get("purchaseUser")==null?"":params.get("purchaseUser").toString());
			buyPlanTimeMapper.add(planTime);
		}else{
			planTime.setSalePlanWeek(params.get("salePlanWeek")==null?"":params.get("salePlanWeek").toString());
			planTime.setConfirmPlanWeek(params.get("confirmPlanWeek")==null?"":params.get("confirmPlanWeek").toString());
			planTime.setApplyPlanWeek(params.get("applyPlanWeek")==null?"":params.get("applyPlanWeek").toString());
			planTime.setPurchaseWeek(params.get("purchaseWeek")==null?"":params.get("purchaseWeek").toString());
			planTime.setSalePlanInterval(Integer.parseInt(params.get("salePlanInterval")==null?"30":params.get("salePlanInterval").toString()));
			planTime.setSalePlanArea(Integer.parseInt(params.get("salePlanArea")==null?"7":params.get("salePlanArea").toString()));
			planTime.setOutPlanUser(params.get("outPlanUser")==null?"":params.get("outPlanUser").toString());
			planTime.setApplyPurchaseUser(params.get("applyPurchaseUser")==null?"":params.get("applyPurchaseUser").toString());
			planTime.setConfirmSaleplanUser(params.get("confirmSaleplanUser")==null?"":params.get("confirmSaleplanUser").toString());
			planTime.setPurchaseUser(params.get("purchaseUser")==null?"":params.get("purchaseUser").toString());
			buyPlanTimeMapper.update(planTime);
		}
	}
}
