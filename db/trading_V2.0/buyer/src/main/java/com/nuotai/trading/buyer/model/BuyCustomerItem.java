package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import lombok.Data;


/**
 * 
 * 售后服务明细表
 * @author "dxl
 * @date 2017-09-12 16:49:24
 */
@Data
public class BuyCustomerItem implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后id
	private String customerId;
	//采购计划单号
	private String applyCode;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String skuOid;
	//单位
	private String unitId;
	//发货号
	private String deliveryCode;
	//发货明细id
	private String deliveryItemId;
	//售后类型  0-换货，1-退款退货
	private String type;
	//转换类型[0-未转换, 1-换货转退货]
	private String transformType;
	//售后数量
	private Integer goodsNumber;
	//换货到货数量
	private Integer exchangeArrivalNum;
	//退货数量
	private Integer returnNum;
	//价格
	private BigDecimal price;
	//收货数量
	private Integer receiveNumber;
	//仓库id
	private String wareHouseId;
	//是否索要发票
	private String isNeedInvoice;
	//备注
	private String remark;
	//返修金额
	private BigDecimal repairPrice;
	//卖家确认数量
	private Integer confirmNumber;
	//换货发货列表
	private List<Map<String,Object>> deliveryList;
	//换货已发货数
	private int deliveryNum;
	//是否允许变更售后类型
	private boolean updateTypeFlag;
	//换货单价
	private BigDecimal exchangePrice;
}
