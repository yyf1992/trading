package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 供应商绩效考核表
 * 
 * @author "
 * @date 2018-01-03 10:12:29
 */
@Data
public class BuySupplierAssessment implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司id
	private String companyId;
	//考核名称
	private String assessmentName;
	//分值
	private Float score;
	//备注
	private String remark;
	//分值类型   0-分值   1-分数百分比
	private Integer scoreType;
	//到货及时率分值
	private Float arrivalRateScore;
	//计划完成率分值
	private Float completionRateScore;
	//到货合格率分值
	private Float qualificationRateScore;
	//售后响应及时率分值
	private Float afterSaleRateScore;
	//状态 0启用，1禁用
	private Integer state;
	//创建人id
	private String createId;
	//创建人姓名
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人姓名
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人姓名
	private String delName;
	//删除时间
	private Date delDate;
}
