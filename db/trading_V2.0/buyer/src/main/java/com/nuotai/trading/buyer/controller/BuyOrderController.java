package com.nuotai.trading.buyer.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationMapper;
import com.nuotai.trading.buyer.dao.BuyBillReconciliationPaymentMapper;
import com.nuotai.trading.buyer.dao.BuyDeliveryRecordItemMapper;
import com.nuotai.trading.buyer.model.BuyCustomer;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderExportData;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.service.BuyBillPaymentService;
import com.nuotai.trading.buyer.service.BuyCustomerService;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.buyer.service.seller.BSellerDeliveryRecordService;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyAddress;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.service.BuyAddressService;
import com.nuotai.trading.service.BuyProductSkuBomService;
import com.nuotai.trading.service.BuyProductSkuComposeService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyShopProductService;
import com.nuotai.trading.service.TradeVerifyHeaderService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 向供应商下单列表
 * @author Administrator
 *
 */
@Controller
@RequestMapping("platform/buyer/buyOrder")
public class BuyOrderController extends BaseController {
	@Autowired
	private BuyOrderService buyOrderService;
	@Autowired
	private BuyOrderProductService buyOrderProductService;
	@Autowired
	private BuyDeliveryRecordItemMapper buyDeliveryRecordItemMapper;
	@Autowired
	private BuyCustomerService buyCustomerService;
	@Autowired
	private BSellerDeliveryRecordService bSellerDeliveryRecordService;
	@Autowired
	private BuyShopProductService shopProductService;
	@Autowired
	private BuyProductSkuBomService buyProductSkuBomService;
	@Autowired
	private BuyProductSkuComposeService buyProductSkuComposeService;
	@Autowired
	private BuyProductSkuService buyProductSkuService;
	@Autowired
	private BuyAddressService buyAddressService;
	@Autowired
	private TradeVerifyHeaderService  tradeVerifyHeaderService;
	@Autowired
	private BuyBillPaymentService paymentService;
	@Autowired
	private BuyBillReconciliationPaymentMapper paymentMapper;
	@Autowired
	private BuyBillReconciliationMapper reconciliationMapper;
	
	/**
	 * 加载采购订单页面
	 * @return
	 */
	@RequestMapping("buyOrderList")
	public String buyOrderList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("tabId")||params.get("tabId")==null){
			params.put("tabId", "0");
		}
		params.put("orderKind", "0");
		//获得不同状态订单数
		buyOrderService.getOrderNum(params);
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/interwork/buyOrder/buyOrderList";
	}
	
	/**
	 * 加载原材料采购订单页面
	 * @return
	 */
	@RequestMapping("/rawMaterialOrderList")
	public String rawMaterialOrderList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("tabId")||params.get("tabId")==null){
			params.put("tabId", "0");
		}
		params.put("orderKind", "1");
		//获得不同状态订单数
		buyOrderService.getOrderNum(params);
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/interwork/buyOrder/rawMaterialOrderList";
	}

	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		buyOrderService.loadData(searchPageUtil,params);
		String returnUrl="";
		String orderKind=params.containsKey("orderKind")?params.get("orderKind").toString():"0";
		if("1".equals(orderKind)){//0-采购订单 1-原材料订单
			returnUrl += "platform/buyer/interwork/buyOrder/allRawMaterialOrder";
		}else{
			returnUrl += "platform/buyer/interwork/buyOrder/allOrder";
		}
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}

	/**
	 * 收货订单管理- 点击订单号查看订单详情
	 * @param params
	 * @return
	 */
	@RequestMapping("showOrderDetail")
	public String showOrderDetail(@RequestParam Map<String,Object> params){
		String orderCode = (String) params.get("orderCode");
		String returnUrl = (String) params.get("returnUrl");
		String menuId = (String) params.get("menuId");
		List<Map<String,Object>> buyDeliveryRecordItemlist = buyDeliveryRecordItemMapper.queryByOrderCode(orderCode);
		String deliverType = "0";
		if (!ObjectUtil.isEmpty(buyDeliveryRecordItemlist)){
			for (Map<String,Object> map:buyDeliveryRecordItemlist){
				deliverType = map.get("deliverType").toString();
			}
		}
		BuyOrder order = null;
		JSONObject json = null;
		if (deliverType.equals("0")){
			order = buyOrderService.selectByOrderCode(orderCode);
			json = getYstep(order);
			model.addAttribute("ystep", json);
			model.addAttribute("order", order);
		}else{
			BuyCustomer buyCustomer = buyCustomerService.selectByOrderCode(orderCode);
			model.addAttribute("order", buyCustomer);
		}
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("menuId", menuId);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		if (deliverType.equals("0")){
			return "platform/buyer/interwork/buyOrder/orderDetail";
		}else{
			return "platform/buyer/interwork/buyOrder/buyCustomerDetail";
		}
		
	}

	/**
	 * 设置流程
	 * @param order
	 * @return
	 */
	private JSONObject getYstep(BuyOrder order) {
		if(order == null || "".equals(order.getId())){
			return null;
		}
		JSONObject result = new JSONObject();
		JSONArray stepArray = new JSONArray();
		JSONObject step1 = new JSONObject();
		step1.put("title", "提交订单");
		stepArray.add(step1);
		if(order.getIsCheck()==Constant.IsCheck.VERIFYNOTPASSED.getValue()){
			//内部审批-驳回
			JSONObject step2 = new JSONObject();
			step2.put("title", "内部审批被驳回");
			stepArray.add(step2);
			result.put("length", 2);
			result.put("flag", "内部审批被驳回");
			result.put("msg", "订单被审批驳回，您可以修改后再次提交");
			result.put("type", "1");
		}else if(order.getIsCheck()==Constant.IsCheck.ALREADYVERIFY.getValue()){
			//已审核
			JSONObject step2 = new JSONObject();
			step2.put("title", "内部已审批");
			stepArray.add(step2);
			if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//取消订单
				JSONObject step3 = new JSONObject();
				step3.put("title", "已取消");
				stepArray.add(step3);
				result.put("length", 3);
				result.put("flag", "已取消");
				result.put("msg", "订单被取消，订单完结");
				result.put("type", "1");
			}else if(order.getStatus()==Constant.OrderStatus.WAITORDER.getValue()){
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 3);
				result.put("flag", "待接单");
				result.put("msg", "订单已经推送至卖家，请耐心等待对方接单");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.WAITDELIVERY.getValue()){
				//待对方发货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 4);
				result.put("flag", "待发货");
				result.put("msg", "卖家已接单，请耐心等待对方发货");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.WAITRECEIVE.getValue()){
				//待收货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "卖家已发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 5);
				result.put("flag", "待收货");
				result.put("msg", "卖家已发货，请耐心等待到货");
				result.put("type", "0");
			}else if(order.getStatus()==Constant.OrderStatus.RECEIVED.getValue()){
				//已收货
				JSONObject step3 = new JSONObject();
				step3.put("title", "卖家已接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "卖家已发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "已收货");
				stepArray.add(step5);
				result.put("length", 5);
				result.put("flag", "已收货");
				result.put("msg", "您已收货，订单完结");
				result.put("type", "2");
			}else if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//已取消
				JSONObject step3 = new JSONObject();
				step3.put("title", "已取消订单");
				stepArray.add(step3);
				result.put("length", 1);
				result.put("flag", "已取消订单");
				result.put("msg", "订单已取消");
				result.put("type", "2");
			}else if(order.getStatus()==Constant.OrderStatus.STOP.getValue()){
				//已终止
				JSONObject step3 = new JSONObject();
				step3.put("title", "已终止订单");
				stepArray.add(step3);
				result.put("length", 1);
				result.put("flag", "已终止订单");
				result.put("msg", order.getStopReason());
				result.put("type", "2");
			}else{
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 3);
				result.put("flag", "待接单");
				result.put("msg", "订单已经推送至卖家，请耐心等待对方接单");
				result.put("type", "0");
			}
		}else{
			if(order.getStatus()==Constant.OrderStatus.CANCEL.getValue()){
				//已取消
				JSONObject step2 = new JSONObject();
				step2.put("title", "已取消订单");
				stepArray.add(step2);
				result.put("length",2);
				result.put("flag", "已取消订单");
				result.put("msg", "订单已取消");
				result.put("type", "2");
			}else {
				JSONObject step2 = new JSONObject();
				step2.put("title", "待内部审批");
				stepArray.add(step2);
				//待接单
				JSONObject step3 = new JSONObject();
				step3.put("title", "待接单");
				stepArray.add(step3);
				JSONObject step4 = new JSONObject();
				step4.put("title", "待发货");
				stepArray.add(step4);
				JSONObject step5 = new JSONObject();
				step5.put("title", "待收货");
				stepArray.add(step5);
				result.put("length", 2);
				result.put("flag", "待审批");
				result.put("msg", "订单正在进行内部审批，审批通过后会自动推送至卖家");
				result.put("type", "0");
			}

		}
		result.put("stepArray", stepArray);
		return result;
	}
	/**
	 * 取消订单保存
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "/saveCancelOrder", method = RequestMethod.POST)
	@ResponseBody
	public String saveCancelOrder(String orderId){
		JSONObject json = new JSONObject();
		try {
			buyOrderService.saveCancelOrder(orderId);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 加载我的换货待发货订单（买家）
	 * @return
	 */
	@RequestMapping("exchangeGoodsList")
	public String exchangeGoodsList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		params.put("orderKind", Constant.OrderKind.AT.getValue());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
		params.put("status", Constant.OrderStatus.WAITDELIVERY.getValue());
		params.put("orderType", "1");//换货
		searchPageUtil.setObject(params);
		List<BuyOrder> exchangeGoodsList = buyOrderService.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(exchangeGoodsList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/exchangeGoods/exchangeGoodsList";
	}

	/**
	 * Show upd order string.
	 * 订单修改界面展示
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping(value = "showUpdOrder")
	public String showUpdOrder(@RequestParam Map<String,Object> params){
		BuyOrder bo = buyOrderService.selectByPrimaryKey(String.valueOf(params.get("id")));
		if(!ObjectUtil.isEmpty(bo)){
			List<BuyOrderProduct> itemsList = buyOrderProductService.selectByOrderId(bo.getId());
			bo.setOrderProductList(itemsList);
		}
		model.addAttribute("order", bo);
		return "platform/buyer/interwork/buyOrder/updOrder";
	}

	/**
	 * Upd order.
	 * 订单修改保存
	 * @param params the params
	 */
	@RequestMapping("updOrder")
	public void updOrder(@RequestParam Map<String,Object> params){
		//更新的时候判断是否走审批，no的话不用走update里边的monitorUpdate方法，其他正常
		params.put("checkApprove","no");
		update(String.valueOf(params.get("orderId")),params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params) throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/purchase/verifySuccess");
				json.put("verifyError", "platform/buyer/purchase/verifyError");
				json.put("relatedUrl", "platform/buyer/purchase/verifyDetail");
				List<String> idList = new ArrayList<>();
				idList.add(String.valueOf(params.get("orderId")));
				json.put("idList", idList);
				buyOrderService.updOrder(params);
				return json;
			}
		});
	}

	/**
	 * Cancel link order string.
	 * 取消订单
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping(value = "cancelOrder", method = RequestMethod.POST)
	@ResponseBody
	public String cancelOrder(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			String orderId = String.valueOf(params.get("orderId"));
			buyOrderService.cancelOrder(orderId);
			json.put("success", true);
			json.put("msg", "操作成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "操作失败！");
		}
		return json.toString();
	}
	/**
	 * stopOrder.
	 * 终止订单
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping(value = "stopOrder", method = RequestMethod.POST)
	@ResponseBody
	public String stopOrder(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			buyOrderService.stopOrder(params);
			json.put("success", true);
			json.put("msg", "操作成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "操作失败！");
		}
		return json.toString();
	}

	/**
	 * Export order.
	 * 订单导出
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportOrder")
	public void exportOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<BuyOrderExportData> exportDataList = buyOrderService.getExportData(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		normalStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		normalStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		normalStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		normalStyle.setBorderTop(BorderStyle.THIN);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"采购单号","订单日期",
				"商品货号","商品名称","规格名称","条形码","采购数量","采购未到货数量","采购单价","采购金额","采购要求到货日期","下单人","仓库","采购留言","商品备注",
				"发货详情","发货数量","到货数量","供应商","最近到货日期"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i=0;i<exportDataList.size();i++) {
				BuyOrderExportData data = exportDataList.get(i);
				//因为第0行已经设置标题了,所以从行2开始写入数据
				row=sheet.createRow(i+1);
				//1.采购单号
				cell = row.createCell(0);
				cell.setCellValue(data.getOrderCode());
				cell.setCellStyle(normalStyle);
				//2.订单日期
				cell = row.createCell(1);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
				cell.setCellStyle(normalStyle);
				//3.商品货号
				cell = row.createCell(2);
				cell.setCellValue(data.getProductCode());
				cell.setCellStyle(normalStyle);
				//4.商品名称
				cell = row.createCell(3);
				cell.setCellValue(data.getProductName());
				cell.setCellStyle(normalStyle);
				//5.规格名称
				cell = row.createCell(4);
				cell.setCellValue(data.getSkuName());
				cell.setCellStyle(normalStyle);
				//6.条形码
				cell = row.createCell(5);
				cell.setCellValue(data.getBarcode());
				cell.setCellStyle(normalStyle);
				//7.采购数量
				cell = row.createCell(6);
				cell.setCellValue(data.getOrderNum());
				cell.setCellStyle(normalStyle);
				//8.采购未到货数量
				cell = row.createCell(7);
				cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
				cell.setCellStyle(normalStyle);
				//9.采购单价
				cell = row.createCell(8);
				cell.setCellValue(fnum.format(data.getPrice()));
				cell.setCellStyle(normalStyle);
				//10.采购金额
				cell = row.createCell(9);
				cell.setCellValue(fnum.format(data.getTotalPrice()));
				cell.setCellStyle(normalStyle);
				//11.采购要求到货日期
				cell = row.createCell(10);
				cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
				cell.setCellStyle(normalStyle);
				//12.下单人
				cell = row.createCell(11);
				cell.setCellValue(data.getOrderCreator());
				cell.setCellStyle(normalStyle);
				//13.入仓
				cell = row.createCell(12);
				cell.setCellValue(data.getWarehouseName());
				cell.setCellStyle(normalStyle);
				//14.采购留言
				cell = row.createCell(13);
				cell.setCellValue(data.getOrderRemark());
				cell.setCellStyle(normalStyle);
				//14.商品备注
				cell = row.createCell(14);
				cell.setCellValue(data.getOrderItemRemark());
				cell.setCellStyle(normalStyle);
				if(!ObjectUtil.isEmpty(data.getDeliverNo())){
					//15.发货单号
					cell = row.createCell(15);
					cell.setCellValue(data.getDeliverNo());
					cell.setCellStyle(normalStyle);
					//16.发货数量
					cell = row.createCell(16);
					cell.setCellValue(ObjectUtil.isEmpty(data.getDeliveryNum())?0:data.getDeliveryNum());
					cell.setCellStyle(normalStyle);
					if(!ObjectUtil.isEmpty(data.getArrivalDate())){
						//17.到货数量
						cell = row.createCell(17);
						cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//19.到货日期
						cell = row.createCell(19);
						cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalDate())?"":sdf.format(data.getArrivalDate()));
						cell.setCellStyle(normalStyle);
					}else{
						//17.到货数量
						cell = row.createCell(17);
						cell.setCellValue("未到货");
						cell.setCellStyle(normalStyle);
						//19.到货日期
						cell = row.createCell(19);
						cell.setCellValue("未到货");
						cell.setCellStyle(normalStyle);
					}
				}else{
					//15.发货单号
					cell = row.createCell(15);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//16.发货数量
					cell = row.createCell(16);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//17.到货数量
					cell = row.createCell(17);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
					//19.到货日期
					cell = row.createCell(19);
					cell.setCellValue("未发货");
					cell.setCellStyle(normalStyle);
				}
				//18.供应商
				cell = row.createCell(18);
				cell.setCellValue(data.getSupplierName());
				cell.setCellStyle(normalStyle);
			}
		}
		ExcelUtil.preExport("采购订单导出", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * Export not arrival order.
	 * 未到货订单导出
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportNotArrivalOrder")
	public void exportNotArrivalOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<BuyOrderExportData> exportDataList = buyOrderService.getNotArrivalExportData(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("未到货订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);

		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		// 下边框
		normalStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		normalStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		normalStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		normalStyle.setBorderTop(BorderStyle.THIN);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"采购单号","产品名称",
				"货号","条形码","规格名称","采购单价","采购数量","实际到货数量","未到货数量","未到货金额","供应商",
				"采购日期","要求到货日期","下单人","仓库","订单备注","商品备注"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i=0;i<exportDataList.size();i++) {
				BuyOrderExportData data = exportDataList.get(i);
				//因为第0行已经设置标题了,所以从行2开始写入数据
				row=sheet.createRow(i+1);
				//1.采购单号
				cell = row.createCell(0);
				cell.setCellValue(data.getOrderCode());
				cell.setCellStyle(normalStyle);
				//2.产品名称
				cell = row.createCell(1);
				cell.setCellValue(data.getProductName());
				cell.setCellStyle(normalStyle);
				//3.货号
				cell = row.createCell(2);
				cell.setCellValue(data.getProductCode());
				cell.setCellStyle(normalStyle);
				//4.条形码
				cell = row.createCell(3);
				cell.setCellValue(data.getBarcode());
				cell.setCellStyle(normalStyle);
				//5.规格名称
				cell = row.createCell(4);
				cell.setCellValue(data.getSkuName());
				cell.setCellStyle(normalStyle);
				//6.采购单价
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(data.getPrice()));
				cell.setCellStyle(normalStyle);
				//7.采购数量
				cell = row.createCell(6);
				cell.setCellValue(data.getOrderNum());
				cell.setCellStyle(normalStyle);
				//8.实际到货数量
				cell = row.createCell(7);
				cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
				cell.setCellStyle(normalStyle);
				//9.未到货数量
				cell = row.createCell(8);
				cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
				cell.setCellStyle(normalStyle);
				//10.未到货金额
				cell = row.createCell(9);
				cell.setCellValue(fnum.format(data.getUnArrivalTotalPrice()));
				cell.setCellStyle(normalStyle);
				//11.供应商
				cell = row.createCell(10);
				cell.setCellValue(data.getSupplierName());
				cell.setCellStyle(normalStyle);
				//12.采购日期
				cell = row.createCell(11);
				cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
				cell.setCellStyle(normalStyle);
				//13.要求到货日期
				cell = row.createCell(12);
				cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
				cell.setCellStyle(normalStyle);
				//14.采购提报人
				cell = row.createCell(13);
				cell.setCellValue(data.getOrderCreator());
				cell.setCellStyle(normalStyle);
				//15.入仓
				cell = row.createCell(14);
				cell.setCellValue(data.getWarehouseName());
				cell.setCellStyle(normalStyle);
				//16.訂單备注
				cell = row.createCell(15);
				cell.setCellValue(data.getOrderRemark());
				cell.setCellStyle(normalStyle);
				//17.訂單商品备注
				cell = row.createCell(16);
				cell.setCellValue(data.getOrderItemRemark());
				cell.setCellStyle(normalStyle);
			}
		}
		ExcelUtil.preExport("未到货订单", response);
		ExcelUtil.export(wb, response);
	}
	
	/**
	 * 采购订单审批-待我审批首页
	 * @return
	 */
	@RequestMapping("approvedHtml")
	public String approvedHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		model.addAttribute("params", params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/interwork/buyOrder/approvedOrderList";
	}
	
	/**
	 * 采购订单审批-待我审批数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping("approvedData")
	public String approvedData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		buyOrderService.loadApprovedData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/interwork/buyOrder/approvedOrderData";
	}
	
	/**
	 * 采购订单审批一键通过
	 * @param id
	 * @return
	 */
	@RequestMapping("verifyAll")
	@ResponseBody
	public String verifyAll(String id){
		JSONObject json = buyOrderService.verifyAll(id);
		return json.toJSONString();
	}
	
	/**
	 * 采购订单审批 - 查看详情
	 * @param params
	 * @return
	 */
	@RequestMapping("approvedDataDetail")
	public String approvedDataDetail(@RequestParam Map<String,Object> params){
		String orderCode = (String) params.get("orderCode");
		String returnUrl = (String) params.get("returnUrl");
		String menuId = (String) params.get("menuId");
		BuyOrder order = buyOrderService.selectByOrderCode(orderCode);
		JSONObject json = getYstep(order);
		model.addAttribute("order", order);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("menuId", menuId);
		model.addAttribute("ystep", json);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		return "platform/buyer/interwork/buyOrder/approvedOrderDataDetail";
	}
	
	/**
	 * 卖家-转化采购单页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("changePurchaseOrder")
	public String changePurchaseOrder(String id) throws Exception {
		Map<String, Object> map = new HashMap<String,Object>();
		List<Map<String, Object>> productList = new ArrayList<Map<String, Object>>();
		List<BuyOrderProduct> orderProductList = buyOrderProductService.selectByOrderId(id);
		if(orderProductList != null && orderProductList.size() > 0){
			for(BuyOrderProduct orderProduct : orderProductList){
				//查询bom表
				map.put("barcode", orderProduct.getSkuOid());
				map.put("companyId", ShiroUtils.getCompId());
				BuyProductSkuBom buyProductSkuBom = buyProductSkuBomService.selectByBarcodeAndCompanyId(map);
				if (!ObjectUtil.isEmpty(buyProductSkuBom)){
					map.put("productId", buyProductSkuBom.getProductId());
					List<Map<String, Object>> skuComposeList = buyProductSkuComposeService.selectByMap(map);
					if(skuComposeList != null && skuComposeList.size() > 0){
						for(Map<String, Object> skuComposeMap : skuComposeList){
							skuComposeMap.put("cpNum", orderProduct.getGoodsNumber());
						}
						productList.addAll(skuComposeList);
					}
				}
			}
		}
		if(productList != null && productList.size() > 0){
			for(Map<String,Object> product : productList){
				//商品单位
				map.clear();
				map.put("barcode", product.get("skuBarcode").toString());
				map.put("companyId", Constant.COMPANY_ID);
				BuyProductSku productSku = buyProductSkuService.getByBarcode(map);
				if(productSku != null){
					product.put("unitId", productSku.getUnitId());
				}
				//根据条形码查询关联的供应商列表
				map.clear();
				map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
				map.put("linktype", "0");
				map.put("status", "3");//状态已通过
				map.put("searchCompanyId", Constant.COMPANY_ID);
				map.put("skuOid", product.get("skuBarcode").toString());
				List<Map<String,Object>> supplierLinksList = shopProductService.loadProductLinksByMap(map);
				if(supplierLinksList != null && supplierLinksList.size() > 0){
					product.put("supplierLinksList", supplierLinksList);
				}
			}
		}
		model.addAttribute("productList", productList);
		model.addAttribute("buyOrderId",id);
		return "platform/sellers/interwork/changePurchaseOrder";
	}
	
	/**
	 * 卖家-转化采购单下一步页面
	 * @param params
	 * @return
	 */
	@RequestMapping("changePurchaseOrderNext")
	public String changePurchaseOrderNext(@RequestParam Map<String,String> params){
		String buyOrderId = params.get("buyOrderId") == null ? "" : params.get("buyOrderId").toString();
		JSONObject json = new JSONObject();
		JSONArray itemArray = new JSONArray();
		String infoStr = params.get("infoStr");
		String[] s = infoStr.split("@");
		Map<String, String> map = buyOrderService.parseToMap(s);
		for(Map.Entry<String, String> entry : map.entrySet()){
			String value = entry.getValue().substring(1,entry.getValue().length() - 1);
		    String[] supplier = value.split(".NTNT.");
			JSONObject itemJson = new JSONObject();
			itemJson.put("supplierId", entry.getKey());
			itemJson.put("supplierName", supplier[2]);
			itemJson.put("person", supplier[0]);
			itemJson.put("phone", supplier[1]);
			String[] shop = value.split("@");
			
			JSONArray spArray = new JSONArray();
		    for(int i = 0;i < shop.length;i++){
		    	JSONObject shopJson = new JSONObject();
		    	String item = shop[i].substring(shop[i].indexOf("{"),shop[i].length());
				net.sf.json.JSONObject jsonItem = net.sf.json.JSONObject.fromObject(item);
			    net.sf.json.JSONArray shopArray = jsonItem.getJSONArray("shopList");
			    for(int j = 0 ; j < shopArray.size(); j++){
			    	net.sf.json.JSONObject jsonShop = shopArray.getJSONObject(j);
			    	shopJson.put("goodsNumber", jsonShop.get("goodsNumber").toString());
			    	shopJson.put("productCode", jsonShop.get("productCode").toString());
			    	shopJson.put("productName", jsonShop.get("productName").toString());
			    	shopJson.put("skuCode", jsonShop.get("skuCode").toString());
			    	shopJson.put("skuName", jsonShop.get("skuName").toString());
			    	shopJson.put("skuOid", jsonShop.get("skuOid").toString());
			    	shopJson.put("unitId", jsonShop.get("unitId").toString());
			    	shopJson.put("price", jsonShop.get("price").toString());
			    	shopJson.put("warehouseId", jsonShop.get("warehouseId").toString());
			    	shopJson.put("remark", jsonShop.get("remark").toString());
			    	
			    	if (jsonShop.get("predictArred") != null){
			    		shopJson.put("predictArred", jsonShop.get("predictArred").toString());
			    	}
			    	String jsonString = JSON.toJSONString(shopJson, SerializerFeature.DisableCircularReferenceDetect);
			    	spArray.add(JSONObject.parse(jsonString));
			    }
		    }
			itemJson.put("shopArray", spArray);
			itemArray.add(itemJson);
		}
		json.put("itemArray", itemArray);
		
		//查询收获地址
		Map<String,Object> addressParams = new HashMap<String,Object>();
		addressParams.put("isDel", Constant.IsDel.NODEL.getValue()+"");
		addressParams.put("compId", Constant.COMPANY_ID);
		List<BuyAddress> addressList = buyAddressService.selectByMap(addressParams);
		model.addAttribute("addressList", addressList);
		model.addAttribute("nextData", json);
		model.addAttribute("buyOrderId", buyOrderId);
		return "platform/sellers/interwork/changePurchaseOrderNext";
	}
	
	/**
	 * 卖家-转化采购单保存
	 */
	@RequestMapping("saveChangePurchaseOrder")
	public void saveChangePurchaseOrder(@RequestParam Map<String,Object> params){
		insert(params, new IService(){
			@Override
			public JSONObject init(Map<String,Object> params)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "platform/buyer/buyOrder/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/buyOrder/verifyRefuse");//审批失败调用的方法
				json.put("relatedUrl", "platform/buyer/buyOrder/showRawMaterialOrderDetail");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = buyOrderService.saveChangePurchaseOrder(params);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = new JSONObject();
		try {
			//修改buy_order表数据  同步添加到卖家表中
			json=buyOrderService.verifySuccess(id);
			//卖家自动创建发货
			bSellerDeliveryRecordService.saveDeliveryRecord(id,false);
			json.put("success",true);
			json.put("msg","卖家已发货");
			SearchPageUtil searchPageUtil=new SearchPageUtil();
			Map<String,Object>params=new HashMap<>();
			this.rawMaterialOrderList(searchPageUtil, params);
		} catch (Exception e) {
			json.put("success",false);
			json.put("msg",e.getMessage());
		}
		return json.toString();
	}
	
	/**
	 * 原材料采购单补发
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "/deliveryAgain", method = RequestMethod.GET)
	@ResponseBody
	public JSONObject deliveryAgain(String id){
		JSONObject json = new JSONObject();
		try {
			//卖家自动创建发货
			bSellerDeliveryRecordService.saveDeliveryRecord(id,true);
			json.put("success",true);
			json.put("msg","卖家已发货");
		} catch (Exception e) {
			json.put("success",false);
			json.put("msg",e.getMessage());
		}
		return json;
	}
	
	/**
	 * 查看原材料采购订单详情页面 
	 * @return
	 */
	@RequestMapping(value="/showRawMaterialOrderDetail", method = RequestMethod.GET)
	public String verifyDetail(@RequestParam Map<String,Object> params){
		String id = (String) params.get("id");
		String returnUrl = (String) params.get("returnUrl");
		String menuId = (String) params.get("menuId");
		BuyOrder order = buyOrderService.selectByPrimaryKey(id);
		List<BuyOrderProduct> productList=buyOrderProductService.selectByOrderId(id);
		//获得审批数据
		TradeVerifyHeader verifyDetail = tradeVerifyHeaderService.getVerifyHeaderByRelatedId(id);
		//返回时回填搜索条件
		String condition = ShiroUtils.returnSearchCondition();
		model.addAttribute("form", condition);
		order = buyOrderService.selectByOrderCode(order.getOrderCode());
		model.addAttribute("productList", productList);
		model.addAttribute("order",order);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("menuId", menuId);
		model.addAttribute("verifyDetail",verifyDetail);
		return "platform/buyer/interwork/buyOrder/rawMaterialDetail";
	}
	
	/**
	 * 审批拒绝
	 * @param id
	 */
	@RequestMapping(value = "/verifyRefuse", method = RequestMethod.GET)
	@ResponseBody
	public String verifyRefuse(String id){
		JSONObject json = new JSONObject();
		try {
			json=buyOrderService.verifyRefuse(id);
		} catch (Exception e) {
			json.put("success",false);
			json.put("msg",e.getMessage());
		}
		return json.toString();
	}
	
	/**生成对账单
	 * @param id
	 */
	@RequestMapping(value = "/startReconciliation", method = RequestMethod.GET)
	@ResponseBody
	public String startReconciliation(@RequestParam Map<String,Object> params){
		JSONObject json = new JSONObject();
		try {
			String orderId = params.get("orderId").toString();
			buyOrderService.changeStatus(orderId,"5");
			paymentService.insertOrderReconciliation(orderId);
			json.put("success",true);
			json.put("msg","账单保存成功！");
		} catch (Exception e) {
			json.put("success",false);
			json.put("msg",e.getMessage());
		}
		return json.toString();
	}
	
	
/*	*//**
	 * 修改付款状态
	 * @param id
	 * @param status
	 *//*
	@RequestMapping(value = "/updatePayStutus", method = RequestMethod.GET)
	@ResponseBody
	public void updatePayStutus(String id,String result){
		Map<String,Object> params=new HashMap<>();
		params.put("acceptPaymentId", id);
		params.put("result", result);
		buyOrderService.updateOrderIsPaymentStatus(params);
	}*/
	
	/**
	 * 查看采购订单详情页面 
	 * @return
	 */
	@RequestMapping(value="/paymentDetial", method = RequestMethod.GET)
	public String paymentDetial(String id,String flag){
		String orderId=null;
		if("1".equals(flag)){
			orderId=reconciliationMapper.selectOrderIdbyPurchaseId(id);
		}else{
			orderId=paymentMapper.selectOrderIdByAcceptPaymnentId(id);
		}
		BuyOrder order = buyOrderService.selectByPrimaryKey(orderId);
		List<BuyOrderProduct> productList=buyOrderProductService.selectByOrderId(orderId);
		//获得审批数据
		TradeVerifyHeader verifyDetail = tradeVerifyHeaderService.getVerifyHeaderByRelatedId(id);
		order = buyOrderService.selectByOrderCode(order.getOrderCode());
		model.addAttribute("productList", productList);
		model.addAttribute("order",order);
		model.addAttribute("verifyDetail",verifyDetail);
		return "platform/buyer/interwork/buyOrder/rawMaterialDetail";
	}
}
