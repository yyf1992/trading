package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyBillReconciliation;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface BuyBillReconciliationMapper extends BaseDao<BuyBillReconciliation>{

    //查询账单周期列表
    List<BuyBillReconciliation> getRecToPamyment(SearchPageUtil searchPageUtil);
    //根据条件查询导出付款信息
    List<BuyBillReconciliation> queryPamymentList(Map<String,Object> map);

    //查询付款确认不同状态下的数据量
    int queryRecPaymentCount(Map<String,Object> map);

	//查询账单周期列表
	List<BuyBillReconciliation> getBillReconciliationList(SearchPageUtil searchPageUtil);
	
	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);
	
	//添加账单对账数据
	int saveBillReconciliationInfo(BuyBillReconciliation buyBillReconciliation);
	
	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);
	
	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);
	
    int deleteByPrimaryKey(String id);
    int deleteByPurchaseId(String purchaseId);

    int insert(BuyBillReconciliation record);

    int insertSelective(BuyBillReconciliation record);

    //添加账单附件
    int addBillFileInfo(Map<String,Object> addBillFileMap);

    //根据账单单号查询附件
    List<Map<String,Object>> queryBillFileList(String reconciliationId);

    //根据条件查询账单详细
    BuyBillReconciliation selectByPrimaryKey(Map<String,Object> map);

    //根据买家卖家和出账周期查询数量
    BuyBillReconciliation queryRecByBSAndDate(Map<String,Object> map);

    //根据同步数据条件查询最近对账数据
    List<BuyBillReconciliation> queryMaxRecList(Map<String,Object> map);

    //根据条件查询账单详细
    List<BuyBillReconciliation> selectByPrimaryKeyList(Map<String,Object> map);

    //根据账单单号查询自定义付款附件
    List<Map<String,Object>> queryBillCustomList(Map<String,Object> map);
    //根据对账单号添加自定义金额
    int addBillRecCustom(Map<String,Object> customMap);
    //根据编号修改账单状态
    int updateByPrimaryKeySelective(BuyBillReconciliation record);
    //添加付款内部审批
    int addAcceptPaymentInfo(Map<String,Object> map);
    //根据编号查询待审批付款
    Map<String,Object> queryAcceptPaymentInfo(String acceptPaymentId);
    //根据审批数据编号修改状态
    int updateAcceptPayment(Map<String,Object> map);
    //添加采购单对账数据
    int insertOrderReconciliation(BuyBillReconciliation record);
    
    String selectOrderIdbyPurchaseId(String purchaseId);
    
    String selectOrderIdbyAcceptId(String acceptId);
    
    String selectAcceptIdByOrderId(String orderId);
}