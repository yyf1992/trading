package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;


/**
 * 商品采购价格表
 * 
 * @author "
 * @date 2017-10-25 14:32:58
 */
@Data
public class SysPurchasePrice implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//公司id
	private String companyId;
	//商品id
	private String productId;
	//货号
	private String productCode;
	//商品名称
	private String productName;
	//规格代码
	private String skuCode;
	//规格名称
	private String skuName;
	//条形码
	private String barcode;
	//单位id
	private String unitId;
	//商品价格增加状态  0加价 1提成
	private String priceStatus;
	//价格增加纬度   increase_price_status=0 代表加价金额 increase_price_status=1 代表提成的百分比
	private BigDecimal priceLatitude;
	//创建时间
	private Date createDate;
	//修改时间
	private Date updateDate;
	//删除时间
	private Date delDate;
}
