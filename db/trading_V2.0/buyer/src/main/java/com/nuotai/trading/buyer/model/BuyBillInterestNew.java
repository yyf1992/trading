package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @yyf "
 * @date 2017-09-13 14:52:16
 */
public class BuyBillInterestNew {
	//
	private String id;
	//新申请账单周期编号
	private String billCycleIdNew;
	//逾期天数
	private Integer overdueDate;
	//逾期利息
	private Integer overdueInterest;
	//利息计算方式：1 单利，2 复利
	private String interestCalculationMethod;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBillCycleIdNew() {
		return billCycleIdNew;
	}

	public void setBillCycleIdNew(String billCycleIdNew) {
		this.billCycleIdNew = billCycleIdNew;
	}

	public Integer getOverdueDate() {
		return overdueDate;
	}

	public void setOverdueDate(Integer overdueDate) {
		this.overdueDate = overdueDate;
	}

	public Integer getOverdueInterest() {
		return overdueInterest;
	}

	public void setOverdueInterest(Integer overdueInterest) {
		this.overdueInterest = overdueInterest;
	}

	public String getInterestCalculationMethod() {
		return interestCalculationMethod;
	}

	public void setInterestCalculationMethod(String interestCalculationMethod) {
		this.interestCalculationMethod = interestCalculationMethod;
	}
}
