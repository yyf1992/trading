package com.nuotai.trading.buyer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nuotai.trading.buyer.model.BuyManualOrder;
import com.nuotai.trading.buyer.model.BuyOrder;
import com.nuotai.trading.buyer.model.BuyOrderProduct;
import com.nuotai.trading.buyer.service.BuyManualOrderService;
import com.nuotai.trading.buyer.service.BuyOrderProductService;
import com.nuotai.trading.buyer.service.BuyOrderService;
import com.nuotai.trading.buyer.service.BuySupplierService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuyUnit;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.service.BuyUnitService;
import com.nuotai.trading.service.BuyWarehouseService;
import com.nuotai.trading.service.TBusinessWhareaService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.nuotai.trading.utils.json.ServiceException;

/**
 * 手工录入采购商品
 * @author dxl
 *
 */
@Controller
@RequestMapping("buyer/manualOrder")
public class ManualOrderController extends BaseController {

	@Autowired
	private BuyProductSkuService productSkuService;
	@Autowired
	private BuyUnitService unitService;
	@Autowired
	private BuyWarehouseService warehouseService;
	@Autowired
	private BuyManualOrderService manualOrderService;
	@Autowired
	private BuyOrderService orderService;
	@Autowired
	private BuyOrderProductService orderProductService;
	@Autowired
	private TBusinessWhareaService tBusinessWhareaService;
	@Autowired
	private BuySupplierService supplierService;
	
	/**
	 * 手工录入采购商品页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("addManualOrder")
	public String addProduct() throws Exception {
		// 商品
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("companyId",ShiroUtils.getCompId());
		List<BuyProductSku> list = productSkuService.querySkuList(map);
		model.addAttribute("productList", list);
		//单位管理
		List<BuyUnit> unitList = unitService.getBuyUnitList();
		model.addAttribute("unitList", unitList);
		// 仓库管理
		TBusinessWharea tBusinessWharea = new TBusinessWharea();
		List<TBusinessWharea> warehouseList = tBusinessWhareaService.selectWhareaByMap(tBusinessWharea, map);
		model.addAttribute("warehouseList", warehouseList);
		return "platform/buyer/manualorder/purchase/addManualOrder";
	}
	
	/**
	 * 手工录入采购商品保存
	 */
	@RequestMapping("saveAddManualOrder")
	public void saveAddManualOrder(@RequestParam Map<String, Object> map){
		insert(map, new IService(){
			@Override
			public JSONObject init(Map<String,Object> map)
					throws ServiceException {
				JSONObject json = new JSONObject();
				json.put("verifySuccess", "buyer/manualOrder/verifySuccess");//审批成功调用的方法
				json.put("verifyError", "platform/buyer/purchase/verifyError");//审批失败调用的方法
				json.put("relatedUrl", "buyer/manualOrder/manualOrderDetails");//审批详细信息地址
				List<String> idList = new ArrayList<String>();
				try {
					idList = orderService.saveAddManualOrder(map);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json.put("idList", idList);
				return json;
			}
		});
	}
	
	/**
	 * 审批通过 
	 * @param id
	 */
	@RequestMapping(value = "/verifySuccess", method = RequestMethod.GET)
	@ResponseBody
	public String verifySuccess(String id){
		JSONObject json = manualOrderService.verifySuccess(id);
		return json.toString();
	}
	
	
	/**
	 * 手工录入采购商品保存成功页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("addManualOrderSucc")
	public String addManualOrderSucc(String orderId) throws Exception {
		model.addAttribute("orderId", orderId);
		return "platform/buyer/manualorder/purchase/addManualOrderSucc";
	}
	
	/**
	 * 查看手工下单商品详情
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("manualOrderDetails")
	public String manualOrderDetails(String id) throws Exception {
		BuyOrder order = orderService.selectByPrimaryKey(id);
		model.addAttribute("order", order);
		BuyManualOrder manualOrder = manualOrderService.selectByOrderId(id);
		model.addAttribute("manualOrder", manualOrder);
		List<BuyOrderProduct> orderProductList = orderProductService.selectByOrderId(id);
		model.addAttribute("orderProductList", orderProductList);
		return "platform/buyer/manualorder/purchase/manualOrderDetails";
	}
	
	/**
	 * 打印订单页面
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("printOrder")
	public String printOrder(String orderId) throws Exception {
		BuyOrder order = orderService.selectByPrimaryKey(orderId);
		model.addAttribute("order", order);
		BuyManualOrder manualOrder = manualOrderService.selectByOrderId(orderId);
		model.addAttribute("manualOrder", manualOrder);
		List<BuyOrderProduct> orderProductList = orderProductService.selectByOrderId(orderId);
		model.addAttribute("orderProductList", orderProductList);
		return "platform/buyer/manualorder/purchase/printOrder";
	}
	
	/**
	 * 手工录入采购商品订单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("manualOrderList")
	public String manualOrderList(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map) throws Exception {
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		if(!map.containsKey("tabId")||map.get("tabId")==null){
			map.put("tabId", "0");
		}
		//获得不同状态数量
		orderService.getManualOrderNum(map);
		model.addAttribute("params", map);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/buyer/manualorder/buyorder/manualOrderList";
	}
	
	/**
	 * 获取数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "loadData", method = RequestMethod.GET)
	public String loadData(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		String returnUrl = orderService.loadManualData(searchPageUtil,params);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return returnUrl;
	}
	
	/**
	 * 供应商
	 * @param params
	 * @return
	 */
	@RequestMapping("selectSuppliers")
	@ResponseBody
	public String selectSuppliers(@RequestParam Map<String,Object> params){
		params.put("companyId", ShiroUtils.getCompId());
		params.put("type", "1");
		List<BuySupplier> supplierList = supplierService.selectByMap(params);
		return JSONObject.toJSONString(supplierList);
	}
}
