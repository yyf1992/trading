package com.nuotai.trading.buyer.model;

import com.nuotai.trading.buyer.model.seller.SellerOrderSupplierProduct;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class BuyOrder {
    private String id;
    // 订单号
    private String orderCode;
    // 供应商Id
    private String suppId;
    // 供应商名称
    private String suppName;
    // 供应商负责人
    private String person;
    // 手机号码
    private String phone;
    // 产品总量
    private Integer goodsNum;
    // 产品总价格
    private BigDecimal goodsPrice;
    // 状态 0、待接单；1、待确认协议；2、待对方发货；3、待收货；4、已收货；5、待审批；6、已取消；7、已驳回；8、已完成
    private Integer status;
    // 是否审核 0,待审核；1，已审核
    private Integer isCheck;
    // 是否自提 0，不自提；1，自提
    private Integer isSince;
    // 类型  0，采购下单；1，转化订单
    private Integer orderKind;
    // 收货人姓名
    private String personName;
    // 省
    private String province;
    // 市
    private String city;
    // 区
    private String area;
    // 区号
    private String areaCode;
    // 座机号
    private String planeNumber;
    // 收货手机号码
    private String receiptPhone;
    // 是否删除 0表示未删除；-1表示已删除
    private Integer isDel;
    // 创建人id
    private String createId;
    // 创建人名称
    private String createName;
    // 创建时间
    private Date createDate;
    // 修改人id
    private String updateId;
    // 修改人姓名
    private String updateName;
    // 修改时间
    private Date updateDate;
    // 删除人id
    private String delId;
    // 删除人姓名
    private String delName;
    // 删除时间
    private Date delDate;
    // 备注
    private String remark;
    //卖家驳回原因
    private String sellerRejectReason;
    //终止原因
    private String stopReason;
    //终止人
    private String stopPerson;
    //终止时间
    private Date stopDate;
    // 收货地址
    private String addrName;
    //公司id
    private String companyId;
    private String orderType;   //订单类型（0:采购发货 1:换货发货）
    private BuyManualOrder manualOrder;
    private List<BuyOrderProduct> orderProductList;
    private List<SellerOrderSupplierProduct> supplierProductList;
    private TradeVerifyHeader tradeHeader;
	private List<TradeVerifyPocess> pocessList;
	// 审核时间
	private Date verifyDateFormat;
	// 要求到货时间
	private Date predictArred;
    /**
     * 是否已经有人审批（用于判断订单列表的修改按钮，如果有人审批通过就不能修改）
     */
    private Boolean isApproved;
    //发货数量总计
    private Integer totalDeliveryNum;
    //到货数量总计
    private Integer totalArrivalNum;
    // 售后id
    private String customerId;
    // 售后单号
    private String customerCode;
    // 售后人
    private String customerCreateName;
    // 售后时间
    private Date customerCreateDate;
    // 卖家转化订单id
    private String sellerChangeId;
	//是否已对账（0：未对账， 1：正在对帐，2：对账完成)
	private String reconciliationStatus;
	//是否已付款（0：未付款， 1：正在付款，2：已付款）
	private String isPaymentStatus;
	//付款金额
    private BigDecimal payMoney;
}