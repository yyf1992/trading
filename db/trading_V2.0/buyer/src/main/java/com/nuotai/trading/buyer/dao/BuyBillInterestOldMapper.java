package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.buyer.model.BuyBillInterestOld;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author "
 * @date 2017-09-08 20:28:40
 */
@Component("buyBillInterestOldMapper")
public interface BuyBillInterestOldMapper extends BaseDao<BuyBillInterestOld> {
	//添加数据新关联利息
    int addNewInterestOld(BuyBillInterestOld buyBillInterestOld);
    //查询最近历史数据
    List<BuyBillInterestOld> queryBillInterestOld(String id);
    //根据账单编号删除关联历史利息
    int deleteNewInterestOld(String id);
}
