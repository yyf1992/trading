package com.nuotai.trading.buyer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2018-03-21 18:25:46
 */
@Data
public class BuyBillReconciliationAdvanceEdit implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//账单编号
	private String reconciliationId;
	//预付款编号
	private String advanceId;
	//添加人编号
	private String createUserId;
	//添加人名称
	private String createUserName;
	//添加时间
	private Date createTime;
	//充值时间
	private String createTimeStr;
	//开始时间
	private String startEditDate;
	//结束时间
	private String endEditDate;
	//下单人编号
	private String createBillUserId;
	//下单人名称
	private String createBillUserName;
	//修改/充值金额
	private BigDecimal updateTotal;
	//锁定金额
	private BigDecimal lockTotal;
	//可用金额
	private BigDecimal usableTotal;
	//总金额
	private BigDecimal advanceTotal;
	//充值附件
	private String fileAddress;
	//备注
	private String advanceRemarks;
	//修改状态： 1 提交申请 ，2 审批通过， 3 审批驳回 
	private String editStatus;
	//供应商编号
	private String sellerCompanyId;
	//供应商名称
	private String sellerCompanyName;
}
