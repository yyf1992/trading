package com.nuotai.trading.buyer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class BuyApplypurchaseItem {
	//
	private String id;
	//
	private String applyId;
	//
	private String productCode;
	//
	private String productName;
	//
	private String skuCode;
	//
	private String skuName;
	//
	private String barcode;
	//单位id
	private String unitId;
	//单位名称
	private String unitName;
	//
	private String categoryId;
	//
	private String categoryCode;
	//
	private String categoryName;
	//产品状态
	private String productStatus;
	//采购周期
	private Integer purchasCycle;
	//下单数量
	private Integer applyCount;
	//本月销量
	private Integer monthSaleNum;
	//本月日均销量
	private Integer monthDms;
	//杉橙当前库存
	private Integer scStock;
	//外仓库存数量
	private Integer outStock;
	//在途订单数量
	private Integer transitNum;
	//合计库存数量
	private Integer totalStock;
	//销售计划
	private Integer salePlan;
	//确认销售计划
	private Integer confirmSalePlan;
	//现需求差异量
	private Integer differenceNum;
	//预估下月到货
	private Integer predictNextMonthArrival;
	//预估11月底库存
	private Integer predictNextMonthStock;
	//备注
	private String remark;
    // 已计划采购数量
    private int alreadyPlanPurchaseNum;
 	// 锁定数量
    private int lockGoodsNumber;
    // 添加类型0-手动录入；1-excel导入
    private String type;
    // 销售开始日期
  	private Date saleStartDate;
  	// 销售结束日期
   	private Date saleEndDate;
   	// 销售天数
    private int saleDays;
    //商品类型 0成品1原材料2辅料3虚拟产品
    private int productType;
    //入仓量
    private int putStorageNum;
    //确认入仓量
    private int confirmPutStorageNum;
	//采购单价
	private BigDecimal purchasePrices;
	//是否定制(0:是 1:否)
	private Integer isCustomize;
	//定制类型(0:加价 1:百分比)
	private Integer customizeType;
	//加价金额
	private BigDecimal customizePrices;
	//加价后的单价
	private BigDecimal afterPrice;
	
	//关联供应商list
    private List<Map<String,Object>> supplierLinksList;
    //shopList
    private List<BuyApplypurchaseShop> shopList;
    // 采购明细
    private List<Map<String,Object>> purchaseList;
    //采购计划明细表id
    private String salePlanItemId;
    //30天预计销售计划
    private Integer predictSalesNum;
}