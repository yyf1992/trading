package com.nuotai.trading.buyer.dao;

import com.nuotai.trading.buyer.model.BuyContract;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author liuhui
 * @date 2017-08-07 11:01:28
 */
@Component("buyContractMapper")
public interface BuyContractMapper extends BaseDao<BuyContract> {
    /**
     * 分页加载合同列表
     * @param searchPageUtil
     * @return
     */
    List<BuyContract> queryContractList(SearchPageUtil searchPageUtil);

    List<BuyContract> queryContractListAll(Map<String, Object> params);

    int checkContractNo(@Param("contractId") String contractId, @Param("contractNo") String contractNo);
}
