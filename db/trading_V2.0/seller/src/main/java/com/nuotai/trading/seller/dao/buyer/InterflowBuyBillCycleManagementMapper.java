package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.seller.model.buyer.InterflowBuyBillCycleManagement;
import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("interflowBuyBillCycleManagementMapper")
public interface InterflowBuyBillCycleManagementMapper extends BaseDao<InterflowBuyBillCycleManagement>{
	//查询账单周期列表
	List<Map<String, Object>> getBillCycleList(SearchPageUtil searchPageUtil);
	
	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);
	
	//添加账单周期数据
	int saveBillCycleInfo(Map<String, Object> map);
	
	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);
	
	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);
	
    int deleteByPrimaryKey(String id);

    int insert(InterflowBuyBillCycleManagement record);

    int insertSelective(InterflowBuyBillCycleManagement record);

    InterflowBuyBillCycleManagement selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(InterflowBuyBillCycleManagement record);

    int updateByPrimaryKey(InterflowBuyBillCycleManagement record);
}