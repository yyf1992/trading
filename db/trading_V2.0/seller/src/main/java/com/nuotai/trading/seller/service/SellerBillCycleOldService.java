package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerBillCycleManagementOldMapper;
import com.nuotai.trading.seller.model.SellerBillCycleManagementOld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class SellerBillCycleOldService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerBillInterestOldService.class);

	@Autowired
	private SellerBillCycleManagementOldMapper sellerBillCycleManagementOldMapper;

	//查询最近历史账单数据
	public SellerBillCycleManagementOld queryLastDateBillCycleOld(String id){
		return sellerBillCycleManagementOldMapper.queryLastDateBillCycleOld(id);
	}
	
	public List<SellerBillCycleManagementOld> queryList(Map<String, Object> map){
		return sellerBillCycleManagementOldMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerBillCycleManagementOldMapper.queryCount(map);
	}

	//添加历史账单数据
	public int addNewBillCycleOld(SellerBillCycleManagementOld buyBillCycleManagementOld){
		return  sellerBillCycleManagementOldMapper.addNewBillCycleOld(buyBillCycleManagementOld);
	}
	
	public void update(SellerBillCycleManagementOld buyBillCycleManagementOld){
		sellerBillCycleManagementOldMapper.update(buyBillCycleManagementOld);
	}
	//删除历史账单数据
	public int deleteNewBillCycleOld(String id){
		return sellerBillCycleManagementOldMapper.deleteNewBillCycleOld(id);
	}
	

}
