package com.nuotai.trading.seller.service.buyer;

import com.nuotai.trading.seller.dao.buyer.InterflowBuyBillCycleManagementMapper;
import com.nuotai.trading.seller.model.buyer.InterflowBuyBillCycleManagement;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class InterflowBuyBillCycleService {
	@Autowired
	private InterflowBuyBillCycleManagementMapper billMapper;

	//修改账单周期信息
	public int updateByPrimaryKeySelective(InterflowBuyBillCycleManagement record) {
		return billMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(InterflowBuyBillCycleManagement record) {
		return 0;
	}


	//根据编号修改账单及关联利息表信息
	public int updateSaveBillCycleInfo(Map<String, Object> updateSavemap) {
		//账单周期数据map
				//Map<String, Object> billCycleInfoMap = new HashMap<String, Object>();
		        InterflowBuyBillCycleManagement buyBillCycleManagement = new InterflowBuyBillCycleManagement();
		        //SellerBillCycleManagement sellerBillCycle = new SellerBillCycleManagement();
				String billDealStatus = (String) updateSavemap.get("billDealStatus");//修改状态
				String billCycleId = (String) updateSavemap.get("billCycleId");//账单周期编号
				String onselfCompId = ShiroUtils.getCompId();//操作人所在公司
				String onselfId = ShiroUtils.getUserId();//操作人id
				
				Date date = new Date();
				/*SimpleDateFormat dsf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dealTime = dsf.format(date);*/
				
				int billCycleCount = 0;
				if("1".equals(billDealStatus)){
					
					//String queryType = (String) updateSavemap.get("queryType");//请求状态
					String cycleStartDate = (String) updateSavemap.get("cycleStartDate");//开始周期
					int cycleStartDatei = Integer.parseInt(cycleStartDate);
					String cycleEndDate = (String) updateSavemap.get("cycleEndDate");//截止周期
					int cycleEndDatei = Integer.parseInt(cycleEndDate);
					String billStatementDate = (String) updateSavemap.get("billStatementDate");//出账日期
					int billStatementDatei = Integer.parseInt(billStatementDate);
					String supplierId = (String) updateSavemap.get("supplierId");//买家或卖家公司
					
					buyBillCycleManagement.setId(billCycleId);
					//根据请求类型判断是买家卖家(0 买家，1 卖家)
					//if("0".equals(queryType)){
						buyBillCycleManagement.setBuyCompanyId(onselfCompId);
						buyBillCycleManagement.setSellerCompanyId(supplierId);
					/*}else if("1".equals(queryType)){
						buyBillCycleManagement.setBuyCompanyId(supplierId);
						buyBillCycleManagement.setSellerCompanyId(onselfCompId);
					}*/
					
					buyBillCycleManagement.setCycleStartDate(cycleStartDatei);
					buyBillCycleManagement.setCycleEndDate(cycleEndDatei);
					buyBillCycleManagement.setCheckoutCycle(cycleEndDatei-cycleStartDatei);
					buyBillCycleManagement.setBillStatementDate(billStatementDatei);
					buyBillCycleManagement.setBillDealStatus(billDealStatus);
					buyBillCycleManagement.setCreateUser(onselfId);
					buyBillCycleManagement.setUpdateUser(onselfId);
					buyBillCycleManagement.setCreateTime(date);
					
					//billCycleCount = billMapper.updateByPrimaryKeySelective(buyBillCycleManagement);//修改
					billCycleCount = updateByPrimaryKeySelective(buyBillCycleManagement);
					
					if(billCycleCount > 0){
						//再次修改信息同步到卖家数据库中
						
						/*sellerBillCycle.setId(billCycleId);
						sellerBillCycle.setBuyCompanyId(onselfCompId);
						sellerBillCycle.setSellerCompanyId(supplierId);
						sellerBillCycle.setCycleStartDate(cycleStartDatei);
						sellerBillCycle.setCycleEndDate(cycleEndDatei);
						sellerBillCycle.setCheckoutCycle(cycleEndDatei-cycleStartDatei);
						sellerBillCycle.setBillStatementDate(billStatementDatei);
						sellerBillCycle.setBillDealStatus(billDealStatus);
						sellerBillCycle.setCreateUser(onselfId);
						sellerBillCycle.setCreateTime(date);
						sellerBillCycle.setUpdateUser(onselfId);
						int sellerCycleCount = sellerBillCycleService.updateByPrimaryKeySelective(sellerBillCycle);//添加卖家账单周期*/
						
					}
				}else {
					String dealRemarks = (String) updateSavemap.get("dealRemarks");
					//买家修改
					buyBillCycleManagement.setId(billCycleId);
					buyBillCycleManagement.setBillDealStatus(billDealStatus);
					buyBillCycleManagement.setUpdateUser(onselfId);
					//同步到卖家
					/*sellerBillCycle.setId(billCycleId);
					sellerBillCycle.setBillDealStatus(billDealStatus);
					sellerBillCycle.setUpdateUser(onselfCompId);*/
					if("2".equals(billDealStatus)){
						//买家修改
						buyBillCycleManagement.setApprovalUser(onselfId);
						buyBillCycleManagement.setApprovalTime(date);
						buyBillCycleManagement.setApprovalRemarks(dealRemarks);
						//同步到卖家
						/*sellerBillCycle.setApprovalUser(onselfCompId);
						sellerBillCycle.setApprovalTime(date);
						sellerBillCycle.setApprovalRemarks(dealRemarks);*/
					}else if("3".equals(billDealStatus)){
						//买家修改
						buyBillCycleManagement.setAcceptUser(onselfId);
						buyBillCycleManagement.setAcceptTime(date);
						buyBillCycleManagement.setAcceptRemarks(dealRemarks);
						//同步到卖家
						/*sellerBillCycle.setAcceptUser(onselfId);
						sellerBillCycle.setAcceptTime(date);
						sellerBillCycle.setAcceptRemarks(dealRemarks);*/
					}else if("4".equals(billDealStatus)||"5".equals(billDealStatus)){
						//买家修改
						buyBillCycleManagement.setRejectUser(onselfId);
						buyBillCycleManagement.setRejectTime(date);
						buyBillCycleManagement.setRejectRemarks(dealRemarks);
						//卖家同步
						/*sellerBillCycle.setRejectUser(onselfId);
						sellerBillCycle.setRejectTime(date);
						sellerBillCycle.setRejectRemarks(dealRemarks);*/
					}
					
					billCycleCount = updateByPrimaryKeySelective(buyBillCycleManagement);
					//sellerBillCycleService.updateByPrimaryKeySelective(sellerBillCycle);//同步修改到卖家
				}
				return billCycleCount;
	}

}
