package com.nuotai.trading.seller.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 客户表
 * 
 * @author "
 * @date 2017-07-26 14:39:11
 */
@Data
public class SellerClient implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//主键
	private String id;
	//客户名称
	private String clientName;
	//是否删除 0表示未删除；-1表示已删除
	private Integer isdel;
	//创建ID
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateUserid;
	//修改人姓名
	private String updateUsername;
	//修改时间
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date updateDate;
	//银行账号
	private String bankAccount;
	//开户行
	private String openBank;
	//户名
	private String accountName;
	//纳税人识别号
	private String taxidenNum;
	//所属公司id
	private String companyId;
	//互通好友ID
	private String friendId;
}
