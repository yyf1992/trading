package com.nuotai.trading.seller.model.buyer;

import java.util.Date;


/**
 * 
 * 
 * @author "
 * @date 2017-09-13 11:45:51
 */
public class SBuyBillCycleManagementNew {
	//
	private String id;
	//正式申请编号
	private String newBillCycleId;
	//
	private String buyCompanyId;
	//
	private String sellerCompanyId;
	//账期开始日期
	private Integer cycleStartDate;
	//账期结束时间
	private Integer cycleEndDate;
	//结账周期
	private Integer checkoutCycle;
	//出账日期
	private Integer billStatementDate;
	//出账日期（date）
	private Date outStatementDate;
	//账单周期状态：1 待我审批，2 待对方审核，3 审核通过，4 审批驳回(买家)，5 审核驳回（卖家）
	private String billDealStatus;
	//
	private String createUser;
	//账单周期状态：1 待我审批，2 待对方审核，3 审核通过，4 审批驳回(买家)，5 审核驳回（卖家）
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNewBillCycleId() {
		return newBillCycleId;
	}

	public void setNewBillCycleId(String newBillCycleId) {
		this.newBillCycleId = newBillCycleId;
	}

	public String getBuyCompanyId() {
		return buyCompanyId;
	}

	public void setBuyCompanyId(String buyCompanyId) {
		this.buyCompanyId = buyCompanyId;
	}

	public String getSellerCompanyId() {
		return sellerCompanyId;
	}

	public void setSellerCompanyId(String sellerCompanyId) {
		this.sellerCompanyId = sellerCompanyId;
	}

	public Integer getCycleStartDate() {
		return cycleStartDate;
	}

	public void setCycleStartDate(Integer cycleStartDate) {
		this.cycleStartDate = cycleStartDate;
	}

	public Integer getCycleEndDate() {
		return cycleEndDate;
	}

	public void setCycleEndDate(Integer cycleEndDate) {
		this.cycleEndDate = cycleEndDate;
	}

	public Integer getCheckoutCycle() {
		return checkoutCycle;
	}

	public void setCheckoutCycle(Integer checkoutCycle) {
		this.checkoutCycle = checkoutCycle;
	}

	public Integer getBillStatementDate() {
		return billStatementDate;
	}

	public void setBillStatementDate(Integer billStatementDate) {
		this.billStatementDate = billStatementDate;
	}

	public String getBillDealStatus() {
		return billDealStatus;
	}

	public void setBillDealStatus(String billDealStatus) {
		this.billDealStatus = billDealStatus;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getOutStatementDate() {
		return outStatementDate;
	}

	public void setOutStatementDate(Date outStatementDate) {
		this.outStatementDate = outStatementDate;
	}
}
