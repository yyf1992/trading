package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillCycleManagement;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("sellerBillCycleMapper")
public interface SellerBillCycleManagementMapper extends BaseDao<SellerBillCycleManagement>{

    //卖家列表查询账单周期数据
    List<Map<String, Object>> getSellerBillCycleList(SearchPageUtil searchPageUtil);

    //根据审批状态查询数量
    int queryBillStatusCount(Map<String, Object> map);
    int deleteByPrimaryKey(String id);

    int insert(SellerBillCycleManagement record);

    int insertSelective(SellerBillCycleManagement record);

    //根据编号查询数据
    SellerBillCycleManagement selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SellerBillCycleManagement record);

    //int updateByPrimaryKey(SellerBillCycleManagement record);
}