package com.nuotai.trading.seller.dao.buyer;


import java.util.List;
import java.util.Map;

import com.nuotai.trading.seller.model.buyer.BuyOrderProduct;
import org.apache.ibatis.annotations.Param;

/**
 * The interface S buy order product mapper.
 */
public interface SBuyOrderProductMapper {
    /**
     * Delete by primary key int.
     *
     * @param id the id
     * @return the int
     */
    int deleteByPrimaryKey(String id);

    /**
     * Insert int.
     *
     * @param record the record
     * @return the int
     */
    int insert(BuyOrderProduct record);

    /**
     * Insert selective int.
     *
     * @param record the record
     * @return the int
     */
    int insertSelective(BuyOrderProduct record);

    /**
     * Select by primary key buy order product.
     *
     * @param id the id
     * @return the buy order product
     */
    BuyOrderProduct selectByPrimaryKey(String id);

    /**
     * Update by primary key selective int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKeySelective(BuyOrderProduct record);

    /**
     * Update by primary key int.
     *
     * @param record the record
     * @return the int
     */
    int updateByPrimaryKey(BuyOrderProduct record);

    /**
     * Select by order id list.
     *
     * @param orderId the order id
     * @return the list
     */
    List<BuyOrderProduct> selectByOrderId(String orderId);

    /**
     * 采购商品汇总-已计划采购数量
     *
     * @param map the map
     * @return map map
     */
    Map<String,Object> selectAlreadyPlanPurchaseNumBySkuOid(Map<String, Object> map);

    /**
     * Select by order id and barcode buy order product.
     *
     * @param map the map
     * @return the buy order product
     */
    BuyOrderProduct selectByOrderIdAndBarcode(Map<String, Object> map);

    /**
     * Update delivered num.
     * 更新已发货数量
     *
     * @param id          the id
     * @param deliveryNum the delivery num
     */
    void updateDeliveredNum(@Param("id") String id,@Param("deliveryNum")  int deliveryNum);

    /**
     * Add arrival num.
     * 增加到货数量
     * @param orderItemId the buy order item id
     * @param addNum     the addNum num
     */
    void addArrivalNum(@Param("orderItemId") String orderItemId,@Param("addNum") int addNum);
}