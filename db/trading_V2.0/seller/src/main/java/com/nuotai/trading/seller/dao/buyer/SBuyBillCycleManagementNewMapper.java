package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.seller.model.buyer.SBuyBillCycleManagementNew;
import com.nuotai.trading.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-09-13 11:45:51
 */
@Component()
public interface SBuyBillCycleManagementNewMapper extends BaseDao<SBuyBillCycleManagementNew> {
    //修改申请数据列表查询
    List<SBuyBillCycleManagementNew> queryListNewBillCycle(Map<String, Object> map);
	//添加修改申请
    int addNewBillCycle(SBuyBillCycleManagementNew newBuyBillCycle);
    //根据编号查询修改数据
    SBuyBillCycleManagementNew queryBillCycleByNewId(String id);
    //根据编号修改
    int updateNewBillCycle(SBuyBillCycleManagementNew newBuyBillCycle);
}
