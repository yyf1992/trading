package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecord;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import com.nuotai.trading.seller.service.*;
import com.nuotai.trading.utils.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeoutException;

/**
 * The type Delivery controller.
 *
 * @author liuhui
 * @date 2017 -09-14 16:30
 * @description 发货
 */
@Controller
@RequestMapping("platform/seller/delivery")
public class DeliveryController extends BaseController {
    @Autowired
    private SellerDeliveryRecordService sellerDeliveryRecordService;
    @Autowired
    private SellerDeliveryRecordItemService sellerDeliveryRecordItemService;
    @Autowired
    private SellerOrderSupplierProductService sellerOrderSupplierProductService;
    @Autowired
    private SellerOrderService sellerOrderService;
    @Autowired
    private SellerLogisticsService sellerLogisticsService;

    /**
     * 发货单列表
     *
     * @param searchPageUtil the search page util
     * @param params         the params
     * @return the string
     */
    @RequestMapping("deliveryRecordList")
    public String deliveryRecordList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
        if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        if(!params.containsKey("tabId")||params.get("tabId")==null){
            params.put("tabId", "0");
        }
        //获得不同状态发货单数
        sellerDeliveryRecordService.getDeliveryRecordNum(params);
        model.addAttribute("params", params);
        model.addAttribute("searchPageUtil", searchPageUtil);
        return "platform/sellers/delivery/deliveryRecordList";
    }

    /**
     * 加载发货单列表数据
     *
     * @param searchPageUtil the search page util
     * @param params         the params
     * @return the string
     */
    @RequestMapping(value = "loadDeliveryRecordData", method = RequestMethod.GET)
    public String loadDeliveryRecordData(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
        if(searchPageUtil.getPage()==null){
            searchPageUtil.setPage(new Page());
        }
        searchPageUtil = sellerDeliveryRecordService.loadDeliveryRecordData(searchPageUtil, params);
        model.addAttribute("searchPageUtil", searchPageUtil);
        return "platform/sellers/delivery/deliveryRecordData";
    }


    /**
     * Delivery record detail string.
     * 发货单详情
     * @param params the params
     * @return the string
     */
    @RequestMapping("deliveryRecordDetail")
    public String deliveryRecordDetail(@RequestParam Map<String,Object> params){

        //发货单信息
        Map<String,Object> map = sellerDeliveryRecordService.getDeliveryRecordDetail(params);
        List<SellerDeliveryRecordDetailView> deliveryList = (List<SellerDeliveryRecordDetailView>) map.get("deliveryList");
        model.addAttribute("deliveryList", deliveryList);
        SellerDeliveryRecord deliveryRecord = (SellerDeliveryRecord) map.get("deliveryRecord");
        model.addAttribute("deliveryRecord", deliveryRecord);
        //物流信息
        String logisticsId = "";
        if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
            logisticsId = deliveryList.get(0).getLogisticsId();
        }
        SellerLogistics logistics = sellerLogisticsService.get(logisticsId);
        model.addAttribute("logistics", logistics);
        //返回时回填搜索条件
      	String condition = ShiroUtils.returnSearchCondition();
      	model.addAttribute("form", condition);
        return "platform/sellers/delivery/deliveryRecordDetail";
    }

    /**
     * 单独发货界面
     *
     * @param params the params
     * @return String string
     * @author gsf
     * @date 2017 -08-11
     */
    @RequestMapping(value = "singleSendGoods", method = RequestMethod.GET)
    public String singleSendGoods(@RequestParam Map<String,Object> params){
        if(!params.get("id").equals("")){
            //订单信息
            SellerOrder orderBean = sellerOrderService.get(params.get("id").toString());
            //订单产品信息
            if(orderBean != null){
                List<SellerOrderSupplierProduct> supplierProductList = sellerOrderSupplierProductService.selectSupplierProductByOrderIdMerge(params.get("id").toString());
                orderBean.setSupplierProductList(supplierProductList);
            }
            //库存数量

            model.addAttribute("orderBean", orderBean);
            model.addAttribute("deliverType", ObjectUtil.isEmpty(orderBean.getOrderType())?"0":orderBean.getOrderType());
        }
        return "platform/sellers/delivery/singleSendGoods";
    }

    /**
     * 合并发货界面
     *
     * @param orderIds the order ids
     * @return string string
     */
    @RequestMapping(value = "mergeSendGoods", method = RequestMethod.GET)
    public String mergeSendGoods(String orderIds,String buyCompanyId){
        //订单信息
        List<SellerOrder> orderList = sellerOrderService.queryListByOrderIdsMergeItem(orderIds);
        model.addAttribute("buyCompanyId", orderList.get(0).getCompanyId());
        model.addAttribute("orderList", orderList);
        model.addAttribute("deliverType", ObjectUtil.isEmpty(orderList.get(0).getOrderType())?"0":orderList.get(0).getOrderType());
        return "platform/sellers/delivery/mergeSendGoods";
    }

    /**
     * 发货单保存
     *
     * @param params the params
     * @return String string
     * @author gsf
     */
    @ResponseBody
    @RequestMapping("/sendGoodsSub")
    public String sendGoodsSub(@RequestParam Map<String,Object> params){
        JSONObject json = new JSONObject();
        try {
            sellerDeliveryRecordService.saveDeliveryRecord(params);
            json.put("success", true);
        } catch (Exception e) {
            json.put("false", false);
            json.put("msg", e.getMessage());
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Confirm delivery string.
     * 确认发货
     *
     * @param deliveryId the delivery id
     * @return the string
     */
    @ResponseBody
    @RequestMapping("/confirmDelivery")
    public String confirmDelivery(String deliveryId){
        JSONObject json = new JSONObject();
        try {
            sellerDeliveryRecordService.confirmDelivery(deliveryId);
            json.put("success", true);
        } catch (Exception e) {
            json.put("false", false);
            json.put("msg", e.getMessage());
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Print delivery string.
     * 打印发货单
     *
     * @param deliveryId the delivery id
     * @return the string
     */
    @RequestMapping(value = "printDelivery", method = RequestMethod.GET)
    public String printDelivery(String deliveryId){
        //获取打印的发货单数据
        Map<String,Object> map = sellerDeliveryRecordService.getPrintDeliveryData(deliveryId);
        //主表数据
        SellerDeliveryRecord delivery = (SellerDeliveryRecord) map.get("delivery");
        //明细数据
        List<SellerDeliveryRecordItem> deliveryItemList = (List<SellerDeliveryRecordItem>) map.get("deliveryItemList");
        //订单数据
        SellerOrder order = (SellerOrder) map.get("order");
        //运单数据
        SellerLogistics logistics = (SellerLogistics) map.get("logistics");
        model.addAttribute("delivery",delivery);
        model.addAttribute("deliveryItemList",deliveryItemList);
        model.addAttribute("order",order);
        model.addAttribute("logistics",logistics);
        return "platform/sellers/delivery/deliveryRecordPrint";
    }

    /**
     * Upload receipt string.
     * 上传收货回单附件
     *
     * @param files the files
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value="/uploadReceipt",method=RequestMethod.POST)
    @ResponseBody
    public String uploadReceipt(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
            throws Exception{
        JSONObject json = new JSONObject();
        try {
            List<String> urlList = new ArrayList<>();
            for (MultipartFile file : files) {
                //获取文件名
                String fileName = file.getOriginalFilename();
                json.put("fileName", fileName);
                //获取文件后缀名
                String fileExtensionName = FilenameUtils.getExtension(fileName);
                if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
                        && !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
                    continue;
                }
                //获取文件大小
                String fileSize = ShiroUtils.convertFileSize(file.getSize());
                UplaodUtil uplaodUtil = new UplaodUtil();
                String url = uplaodUtil.uploadFile(file,null,true);
                urlList.add(url);
            }
            json.put("urlList", urlList);
            json.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            json.put("result", "fail");
            json.put("msg", "上传失败！请联系管理员！");
            return json.toString();
        }
        return json.toString();
    }


    /**
     * Send wms input plan string.
     * 推送WMS入库计划，用于确认发货时推送失败的数据
     * @param deliverNo the deliver no
     * @return the string
     */
    @RequestMapping(value = "sendWmsInputPlan", method = RequestMethod.POST)
    @ResponseBody
    public String sendWmsInputPlan(String deliverNo){
        JSONObject json = new JSONObject();
        try {
            sellerDeliveryRecordService.sendWmsInputPlan(deliverNo);
            json.put("success", true);
            json.put("msg", "推送成功！");
        }catch (TimeoutException e){
            json.put("success", false);
            json.put("msg", "接口连接超时！");
        }catch (Exception e) {
            json.put("success", false);
            json.put("msg", "推送失败！");
        }
        return json.toString();
    }
    /**
     * exportDelivery.
     * 发货单导出
     * @param params the params
     * @throws Exception the exception
     */
    @RequestMapping(value = "/exportDelivery")
    public void exportDelivery(@RequestParam Map<String,Object> params) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<SellerDeliveryRecord> deliveryList = sellerDeliveryRecordService.getExportData(params);
        // 第一步，创建一个webbook，对应一个Excel文件
        SXSSFWorkbook wb = new SXSSFWorkbook();
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        Sheet sheet = wb.createSheet("发货单");
        sheet.setDefaultColumnWidth(20);
        //字体
        XSSFFont font = (XSSFFont) wb.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
        headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // 下边框
        headStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        headStyle.setBorderLeft(BorderStyle.THIN);
        // 右边框
        headStyle.setBorderRight(BorderStyle.THIN);
        // 上边框
        headStyle.setBorderTop(BorderStyle.THIN);
        // 字体左右居中
        headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
        headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headStyle.setFont(font);
        // 创建单元格格式，并设置表头居中
        XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
        //字体左右居中
        normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
        // 下边框
        normalStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        normalStyle.setBorderLeft(BorderStyle.THIN);
        // 右边框
        normalStyle.setBorderRight(BorderStyle.THIN);
        // 上边框
        normalStyle.setBorderTop(BorderStyle.THIN);
        //数字小数位格式
        DecimalFormat fnum = new DecimalFormat("##0.0000");
        // 设置excel的数据头信息
        Cell cell;
        String[] cellHeadArray = {"发货单号","发货日期","到货日期","状态","订单号","商品名称","货号","条形码","规格代码","规格名称","单位","发货数量","到货数量","单价","总价","采购商","备注"};
        //在sheet中添加表头第0行
        Row row = sheet.createRow(0);
        for (int i = 0; i < cellHeadArray.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(cellHeadArray[i]);
            cell.setCellStyle(headStyle);
        }
        int rowCount = 1;
        if (deliveryList != null && deliveryList.size() > 0) {
            for (int i=0;i<deliveryList.size();i++) {
                SellerDeliveryRecord delivery = deliveryList.get(i);
                List<SellerDeliveryRecordItem> deliveryItemList = delivery.getItemList();
                String statusName = "";
                if(delivery.getRecordstatus()==0){
                    statusName = "待发货";
                }else if (delivery.getRecordstatus()==1){
                    statusName = "发货中";
                }else if (delivery.getRecordstatus()==2){
                    statusName = "已收货";
                }
                //订单明细
                for(int j=0;j<deliveryItemList.size();j++){
                    SellerDeliveryRecordItem deliveryItem = deliveryItemList.get(j);
                    row=sheet.createRow(rowCount);
                    //1.发货单号
                    cell = row.createCell(0);
                    cell.setCellValue(delivery.getDeliverNo());
                    cell.setCellStyle(normalStyle);
                    //2.发货日期
                    cell = row.createCell(1);
                    cell.setCellValue(ObjectUtil.isEmpty(delivery.getCreateDate())?"":sdf.format(delivery.getCreateDate()));
                    cell.setCellStyle(normalStyle);
                    //3.收货日期
                    cell = row.createCell(2);
                    cell.setCellValue(ObjectUtil.isEmpty(delivery.getArrivalDate())?"":sdf.format(delivery.getArrivalDate()));
                    cell.setCellStyle(normalStyle);
                    //4.状态
                    cell = row.createCell(3);
                    cell.setCellValue(statusName);
                    cell.setCellStyle(normalStyle);
                    //5.订单号
                    cell = row.createCell(4);
                    cell.setCellValue(deliveryItem.getOrderCode());
                    cell.setCellStyle(normalStyle);
                    //6.商品名称
                    cell = row.createCell(5);
                    cell.setCellValue(deliveryItem.getProductName());
                    cell.setCellStyle(normalStyle);
                    //7.货号
                    cell = row.createCell(6);
                    cell.setCellValue(deliveryItem.getProductCode());
                    cell.setCellStyle(normalStyle);
                    //8.条形码
                    cell = row.createCell(7);
                    cell.setCellValue(deliveryItem.getBarcode());
                    cell.setCellStyle(normalStyle);
                    //9.规格代码
                    cell = row.createCell(8);
                    cell.setCellValue(deliveryItem.getSkuCode());
                    cell.setCellStyle(normalStyle);
                    //10.规格名称
                    cell = row.createCell(9);
                    cell.setCellValue(deliveryItem.getSkuName());
                    cell.setCellStyle(normalStyle);
                    //11.单位
                    cell = row.createCell(10);
                    cell.setCellValue(deliveryItem.getUnitName());
                    cell.setCellStyle(normalStyle);
                    //12.发货数量
                    cell = row.createCell(11);
                    cell.setCellValue(deliveryItem.getDeliveryNum());
                    cell.setCellStyle(normalStyle);
                    //13.到货数量
                    cell = row.createCell(12);
                    cell.setCellValue(deliveryItem.getArrivalNum());
                    cell.setCellStyle(normalStyle);
                    //14.单价
                    cell = row.createCell(13);
                    cell.setCellValue(fnum.format(deliveryItem.getSalePrice()));
                    cell.setCellStyle(normalStyle);
                    //15.总价
                    cell = row.createCell(14);
                    cell.setCellValue(fnum.format(deliveryItem.getSalePrice().multiply(new BigDecimal(deliveryItem.getDeliveryNum()))));
                    cell.setCellStyle(normalStyle);
                    //16.采购商
                    cell = row.createCell(15);
                    cell.setCellValue(delivery.getBuycompanyName());
                    cell.setCellStyle(normalStyle);
                    //17.备注
                    cell = row.createCell(16);
                    cell.setCellValue(deliveryItem.getRemark());
                    cell.setCellStyle(normalStyle);
                    rowCount++;
                }
            }
        }
        ExcelUtil.preExport("发货单导出", response);
        ExcelUtil.export(wb, response);
    }
}
