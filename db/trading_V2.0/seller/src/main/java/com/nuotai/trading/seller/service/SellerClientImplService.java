package com.nuotai.trading.seller.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.nuotai.trading.model.SysUser;
import com.nuotai.trading.seller.dao.SellerClientMapper;
import com.nuotai.trading.seller.model.SellerClient;
import com.nuotai.trading.seller.model.SellerClientLinkman;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

/**
 * 
 * @author wangli
 *
 */
@Service
public class SellerClientImplService {

	//private static final Logger LOG = LoggerFactory.getLogger(SellerClientImplService.class);
	
	@Autowired
	private SellerClientMapper sellerClientMapper;
	
	@Autowired
	private SellerClientLinkmanImplService sellerClientLinkmanImplService;
	
	/**
	 * 添加客户联系人
	 * @param sellerClient
	 */
	public void saveSellerClient(SellerClient sellerClient,Map<String,Object> mapLinkman) throws Exception {
		
		if(sellerClient == null){
			return;
		}

		SysUser sysUser = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isDel", Constant.IsDel.NODEL.getValue()); // 未删除
		map.put("clientName", sellerClient.getClientName());
		List<SellerClient> sellerClientList = sellerClientMapper.selectByInfoMap(map);
		if(sellerClientList != null && sellerClientList.size() > 0){
			// 名字存在
			throw new Exception("该名称已存在，请到我的客户中查看！");
		}
		
		sellerClient.setId(ShiroUtils.getUid());
		sellerClient.setIsdel(Constant.IsDel.NODEL.getValue());
		sellerClient.setCompanyId(ShiroUtils.getCompId());
		sellerClient.setCreateId(sysUser.getId());
		sellerClient.setCreateName(sysUser.getUserName());
		Date date = new Date();
		sellerClient.setCreateDate(date);
		
		//将客户保存到客户表
		sellerClientMapper.saveSellerClient(sellerClient);
		
		//保存客户联系人
		sellerClientLinkmanImplService.saveSellerClientLinkman(sellerClient,mapLinkman);

	}

	
	
	/**
	 * 手工订单添加客户联系人
	 * @param sellerClient
	 */
	public void manualSaveSellerClient(SellerClient sellerClient,Map<String,Object> mapLinkman) throws Exception {
		
		if(sellerClient == null){
			return;
		}

		SysUser sysUser = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isDel", Constant.IsDel.NODEL.getValue()); // 未删除
		map.put("clientName", sellerClient.getClientName());
		List<SellerClient> sellerClientList = sellerClientMapper.selectByInfoMap(map);
		if(sellerClientList != null && sellerClientList.size() > 0){
			// 名字存在
			return;
		}
		
		sellerClient.setId(ShiroUtils.getUid());
		sellerClient.setIsdel(Constant.IsDel.NODEL.getValue());
		sellerClient.setCompanyId(ShiroUtils.getCompId());
		sellerClient.setCreateId(sysUser.getId());
		sellerClient.setCreateName(sysUser.getUserName());
		Date date = new Date();
		sellerClient.setCreateDate(date);
		
		//将客户保存到客户表
		sellerClientMapper.saveSellerClient(sellerClient);
		
		//保存客户联系人
		sellerClientLinkmanImplService.saveSellerClientLinkman(sellerClient,mapLinkman);

	}

	/**
	 * 删除客户及联系人
	 * @param id
	 * @return
	 */
	public int deleteSellerClient(String id) throws Exception {
		
		
		if (id != null){
				sellerClientMapper.deleteSellerClient(id);		
		}
		
		return 0;
	}

	/**
	 * 批量删除客户
	 * @param ids
	 * @return
	 */
	public int deleteBeatchSellerClient(String ids) throws Exception {
		if (ids != null){
			String[] idStr = ids.split(",");
			for (String id :idStr){
				sellerClientMapper.deleteSellerClient(id);
			}
		}
		return 0;
	}

	/**
	 * 修改客户联系人
	 * @param sellerClient
	 * @param map
	 */
	public void updateSellerClientById(SellerClient sellerClient,@RequestParam Map<String, Object> mapLinkman) throws Exception {
		
		if(sellerClient == null){
			return;
		}

		SysUser sysUser = (SysUser) ShiroUtils.getSessionAttribute(Constant.CURRENT_USER);

		sellerClient.setUpdateUserid(sysUser.getUpdateId());
		sellerClient.setUpdateUsername(sysUser.getUpdateName());
		Date date = new Date();
		sellerClient.setUpdateDate(date);
		//修改客户信息
		sellerClientMapper.updateSellerClientById(sellerClient);
		if (mapLinkman.size()<=0){
			return;
		}else{
			//修改联系人的信息
			sellerClientLinkmanImplService.updateLinkmanById(sellerClient,mapLinkman);
		}

	}


	/**
	 * 分页查询客户联系人
	 * @param searchPageUtil
	 * @return
	 */
	public List<Map<String, Object>> selectSellerClientByPage(SearchPageUtil searchPageUtil) {

		return sellerClientMapper.selectSellerClientByPage(searchPageUtil);
	}
	
	/**
	 * 查询客户联系人详情
	 * @param id
	 * @return
	 */
	public SellerClient selectSellerClientById(String id){
		
		if (id != null && id != ""){
			SellerClient sellerClient = sellerClientMapper.selectSellerClientById(id);
			return sellerClient;
		}
		
		return null;
		
	}

	/**
	 * 查询客户
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> selectByMap(Map<String, Object> map) {
		map.put("isDel", Constant.IsDel.NODEL.getValue());
		return sellerClientMapper.selectByMap(map);
	}
	
	/**
	 * 根据客户信息查询联系人的信息
	 * @param map
	 * @return
	 */
	public List<SellerClientLinkman> selectaddress(Map<String, Object> map){
		
		if (map.size() == 0){
			return null;
		}
		SellerClient sellerClient = sellerClientMapper.selectSellerClientByclientName(map);
		
		if (sellerClient == null && ObjectUtil.isEmpty(sellerClient)){
			return null;
		}
		
		List<SellerClientLinkman> sellerClientLinkmanList =  sellerClientLinkmanImplService.selectBySellerClientId(sellerClient.getId());
		
		return sellerClientLinkmanList;
		
	}

	/**
	 * 查询客户列
	 * @param map
	 * @return
	 */
	public List<SellerClient> queryList(Map<String, Object> map) {
		return sellerClientMapper.queryList(map);
	}
}
