package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerContract;
import com.nuotai.trading.utils.SearchPageUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author liuhui
 * 2017-08-07 11:01:28
 */
public interface SellerContractMapper extends BaseDao<SellerContract> {
    /**
     * 分页加载合同列表
     * @param searchPageUtil
     * @return
     */
    List<SellerContract> queryContractList(SearchPageUtil searchPageUtil);

    List<SellerContract> queryContractListAll(Map<String, Object> params);

    int checkContractNo(@Param("contractId") String contractId, @Param("contractNo") String contractNo);
}
