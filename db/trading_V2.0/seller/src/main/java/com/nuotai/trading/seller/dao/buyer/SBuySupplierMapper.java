package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.utils.SearchPageUtil;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;
@Mapper
public interface SBuySupplierMapper {
	int deleteByPrimaryKey(String id);

    int insert(BuySupplier record);

    int insertSelective(BuySupplier record);

    BuySupplier selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuySupplier record);

    int updateByPrimaryKey(BuySupplier record);
    
    List<Map<String,Object>> selectByPage(SearchPageUtil searchPageUtil);
    
    List<BuySupplier> selectByMap(Map<String, Object> map);
    
    // 添加供应商保存
    int saveAddSupplier(BuySupplier record, Map<String, Object> map) throws Exception;

    // 删除供应商保存
    void saveDeleteSupplier(String id);

    /**
     * 批量删除供应商保存
     * @param ids
     * @return
     */
    void saveBeatchDeleteSupplier(String ids);

    /**
     * 添加供应商保存
     * @param record
     * @param map
     * @throws Exception
     */
    void saveUpdateSupplier(BuySupplier record, Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSupplierByMap(Map<String, Object> map);
	
	//查询所有的供应商
	List<Map<String, Object>> selectAllSupplier();

	List<BuySupplier> getSupplier(Map<String, String> map);
	
	List<Map<String, Object>> getBuySupplierByMap(Map<String,Object> params);
	
}