package com.nuotai.trading.seller.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.SellerBillCycleManagement;
import com.nuotai.trading.seller.model.SellerBillCycleManagementNew;
import com.nuotai.trading.seller.model.SellerBillInterest;
import com.nuotai.trading.seller.model.SellerBillInterestNew;
import com.nuotai.trading.seller.service.SellerBillCycleInterestService;
import com.nuotai.trading.seller.service.SellerBillCycleService;
import com.nuotai.trading.seller.service.SellerBillInterestNewService;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import com.nuotai.trading.seller.service.SellerBillCycleManagementNewService;

/**
 * 
 * 
 * @yyf "
 * @date 2017-09-13 16:01:52
 */
@Controller
@RequestMapping("platform/seller/billCycleNew")
public class SellerBillCycleManagementNewController extends BaseController{
	@Autowired
	private SellerBillCycleManagementNewService sellerBillCycleManagementNewService;
	@Autowired
	private SellerBillInterestNewService sellerBillInterestNewService;
	@Autowired
	private SellerBillCycleService sellerBillCycleService;
	@Autowired
	private SellerBillCycleInterestService sellerBillCycleInterestService;//关联利息

	/**
	 * 账单周期管理列表查询
	 * @return
	 */
	@RequestMapping("billCycleNewList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("billDealStatus")){
			params.put("billDealStatus","0");
		}
		params.put("newSelCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<SellerBillCycleManagementNew> billCycleNewList = sellerBillCycleManagementNewService.querySellerBillCycleNewList(searchPageUtil);
		sellerBillCycleManagementNewService.queryBillCycleNewCountByStatus(params);
		searchPageUtil.getPage().setList(billCycleNewList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/sellers/billcycles/applybillcycle/billCycleManagement";
	}

	/**
	 * 卖家审批申请
	 */
	@ResponseBody
	@RequestMapping("updateBillCycleNew")
	public String updateBillCycleNew(@RequestParam Map<String, Object> updateMap){
		JSONObject json = new JSONObject();
		try {
			int updateCount = sellerBillCycleManagementNewService.updateBillCycleNew(updateMap);
			if(updateCount > 0){
				json.put("success", true);
				json.put("msg", "修改申请审批成功！");
			}else {
				json.put("success", false);
				json.put("msg", "修改申请审批失败！");
			}
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 根据申请账单周期编号查询关联利息
	 * @param interestMap
	 * @return
	 */
	@RequestMapping("queryInterestNewList")
	@ResponseBody
	public String queryInterestNewList(@RequestParam Map<String,Object> interestMap){
		String billCycleId = (String) interestMap.get("billCycleId");
		List<SellerBillInterestNew> interestList = sellerBillInterestNewService.queryNewInterestList(billCycleId);
		return JSONObject.toJSONString(interestList);
	}

	/**
	 * 查询审批对比数据
	 * @param interestMap
	 * @return
	 */
	@RequestMapping("queryContrastList")
	@ResponseBody
	public String queryContrastList(@RequestParam Map<String,Object> interestMap){
		Map<String,Object> contrastMap = new HashMap<String,Object>();
		String billCycleNewId = (String) interestMap.get("billCycleId");//获取申请编号
		SellerBillCycleManagementNew sellerBillCycleManagementNew = sellerBillCycleManagementNewService.querySellerBillCycleNew(billCycleNewId);
		List<SellerBillInterestNew> sellerBillInterestNewList = sellerBillInterestNewService.queryNewInterestList(billCycleNewId);
		String billCycleId = sellerBillCycleManagementNew.getNewBillCycleId();//获取正式表编号
		SellerBillCycleManagement sellerBillCycleManagement = sellerBillCycleService.selectByPrimaryKey(billCycleId);
		List<SellerBillInterest> sellerBillInterestList = sellerBillCycleInterestService.getBillInteresInfo(billCycleId);
		//拼装返回值
		contrastMap.put("sellerBillCycleManagementNew",sellerBillCycleManagementNew);
		contrastMap.put("sellerBillInterestNewList",sellerBillInterestNewList);
		contrastMap.put("sellerBillCycleManagement",sellerBillCycleManagement);
		contrastMap.put("sellerBillInterestList",sellerBillInterestList);

		return JSONObject.toJSONString(contrastMap);
	}
}
