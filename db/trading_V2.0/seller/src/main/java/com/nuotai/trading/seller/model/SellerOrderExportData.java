package com.nuotai.trading.seller.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import com.nuotai.trading.model.BuyProductSkuBom;

/**
 * @author liuhui
 * @date 2017-12-01 8:41
 * 订单导出bean
 **/
@Data
public class SellerOrderExportData {
    //订单ID
    private String orderId;
    //订单号
    private String orderCode;
    //订单日期
    private Date orderDate;
    //订单状态
    private String orderStatus;
    //订单提报人
    private String orderCreator;
    //訂單备注
    private String orderRemark;
    //商品货号
    private String productCode;
    //商品名称
    private String productName;
    //规格代码
    private String skuCode;
    //规格名称
    private String skuName;
    //条形码
    private String barcode;
    //订单数量
    private Integer orderNum;
    //订单未到货数量
    private Integer unArrivalNum;
    //未到货金额
    private BigDecimal unArrivalTotalPrice;
    //采购单价
    private BigDecimal price;
    //采购总价
    private BigDecimal totalPrice;
    //要求到货日期
    private Date reqArrivalDate;
    //发货单号
    private String deliverNo;
    //发货数量
    private Integer deliveryNum;
    //到货数量
    private Integer arrivalNum;
    //訂單明細备注
    private String orderItemRemark;
    //采购商
    private String buyerName;
    //发货日期
    private Date sendDate;
    //到货日期
    private Date arrivalDate;
    
    //物料配置
    private BuyProductSkuBom buyProductSkuBom;

}
