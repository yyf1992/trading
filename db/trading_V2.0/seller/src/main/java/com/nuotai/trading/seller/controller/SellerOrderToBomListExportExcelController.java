package com.nuotai.trading.seller.controller;

import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.SellerOrderExportData;
import com.nuotai.trading.seller.service.SellerOrderService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.ObjectUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * 采购订单生成BOM表
 * @author wl
 *
 */
@Controller
@RequestMapping("platform/seller/order")
public class SellerOrderToBomListExportExcelController extends BaseController {
	@Autowired
	private SellerOrderService sellerOrderService;  //订单信息
	/**
	 * Export order.
	 * 生成BOM表
	 * @param params the params
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/exportBOMOrder")
	public void exportBOMOrder(@RequestParam Map<String,Object> params) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<SellerOrderExportData> exportDataList = sellerOrderService.getNotArrivalExportData(params);
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("订单");
		sheet.setDefaultColumnWidth(20);
		// 第三步，创建单元格，并设置值表头 设置表头居中
		//字体一
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBold(false);
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		headStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 下边框
		headStyle.setBorderBottom(BorderStyle.THIN);
		// 左边框
		headStyle.setBorderLeft(BorderStyle.THIN);
		// 右边框
		headStyle.setBorderRight(BorderStyle.THIN);
		// 上边框
		headStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		headStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
//		// 下边框
//		normalStyle.setBorderBottom(BorderStyle.THIN);
//		// 左边框
//		normalStyle.setBorderLeft(BorderStyle.THIN);
//		// 右边框
//		normalStyle.setBorderRight(BorderStyle.THIN);
//		// 上边框
//		normalStyle.setBorderTop(BorderStyle.THIN);
		// 字体左右居中
		normalStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		normalStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		//设置第0行标题信息
		Row row = sheet.createRow(0);
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"采购单号","产品名称",
				"货号","条形码","规格名称","采购单价","采购数量","实际到货数量","未到货数量","未到货金额","采购商",
				"采购日期","要求到货日期","采购商留言","商品备注",
				"物料条形码","物料配置数","所需物料数量","物料商品货号","物料商品名称","物料规格代码","物料规格名称","物料单位","物料单备注"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
			if(i==0 || i==3 || i==15){
				// 设置列宽
				sheet.setColumnWidth(i, 4600);
			}
		}
		if (exportDataList != null && exportDataList.size() > 0) {
			for (int i=0;i<exportDataList.size();i++) {
				SellerOrderExportData data = exportDataList.get(i);
				int tt = row.getRowNum();
				//从行2开始写入数据
				row=sheet.createRow(tt+1);
				tt = row.getRowNum();
				if (!ObjectUtil.isEmpty(data.getBuyProductSkuBom())){
					if (data.getBuyProductSkuBom().getSkuComposeList().size()>0){
						int size0 = data.getBuyProductSkuBom().getSkuComposeList().size();
						if ((tt+size0)-(tt+1)>0){
							// 合并单元格
							for (int x = 1;x<=15;x++){
								ExcelUtil.setRangeStyle(sheet, tt+1, tt+size0, x, x);
							}
						}
						
						for (int y=0;y<data.getBuyProductSkuBom().getSkuComposeList().size();y++){
							Map<String, Object> bom = data.getBuyProductSkuBom().getSkuComposeList().get(y);
							if (y>0){
								row=sheet.createRow(row.getRowNum()+1);
							}
							//1.采购单号
							cell = row.createCell(0);
							cell.setCellValue(data.getOrderCode());
							cell.setCellStyle(normalStyle);
							//2.产品名称
							cell = row.createCell(1);
							cell.setCellValue(data.getProductName());
							cell.setCellStyle(normalStyle);
							//3.货号
							cell = row.createCell(2);
							cell.setCellValue(data.getProductCode());
							cell.setCellStyle(normalStyle);
							//4.条形码
							cell = row.createCell(3);
							cell.setCellValue(data.getBarcode());
							cell.setCellStyle(normalStyle);
							//5.规格名称
							cell = row.createCell(4);
							cell.setCellValue(data.getSkuName());
							cell.setCellStyle(normalStyle);
							//6.采购单价
							cell = row.createCell(5);
							cell.setCellValue(fnum.format(data.getPrice()));
							cell.setCellStyle(normalStyle);
							//7.采购数量
							cell = row.createCell(6);
							cell.setCellValue(data.getOrderNum());
							cell.setCellStyle(normalStyle);
							//8.实际到货数量
							cell = row.createCell(7);
							cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
							cell.setCellStyle(normalStyle);
							//9.未到货数量
							cell = row.createCell(8);
							cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
							cell.setCellStyle(normalStyle);
							//10.未到货金额
							cell = row.createCell(9);
							cell.setCellValue(fnum.format(data.getUnArrivalTotalPrice()));
							cell.setCellStyle(normalStyle);
							//11.采购商
							cell = row.createCell(10);
							cell.setCellValue(data.getBuyerName());
							cell.setCellStyle(normalStyle);
							//12.采购日期
							cell = row.createCell(11);
							cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
							cell.setCellStyle(normalStyle);
							//13.要求到货日期
							cell = row.createCell(12);
							cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
							cell.setCellStyle(normalStyle);
							//14.采购商备注
							cell = row.createCell(13);
							cell.setCellValue(data.getOrderRemark());
							cell.setCellStyle(normalStyle);
							//15.商品备注
							cell = row.createCell(14);
							cell.setCellValue(data.getOrderItemRemark());
							cell.setCellStyle(normalStyle);
							//物料条形码
							cell = row.createCell(15);
							cell.setCellValue((String)bom.get("skuBarcode"));
							cell.setCellStyle(normalStyle);
							//物料配置数
							cell = row.createCell(16);
							cell.setCellValue(bom.get("composeNum").toString());
							cell.setCellStyle(normalStyle);
							//所需物料数量
							cell = row.createCell(17);
							int a = ObjectUtil.isEmpty(data.getUnArrivalNum())?
									Integer.parseInt(bom.get("composeNum").toString())*data.getOrderNum():Integer.parseInt(bom.get("composeNum").toString())*data.getUnArrivalNum();
							cell.setCellValue(a);
							cell.setCellStyle(normalStyle);
							//物料商品货号
							cell = row.createCell(18);
							cell.setCellValue((String)bom.get("productCode"));
							cell.setCellStyle(normalStyle);
							//物料商品名称
							cell = row.createCell(19);
							cell.setCellValue((String)bom.get("productName"));
							cell.setCellStyle(normalStyle);
							//物料规格代码
							cell = row.createCell(20);
							cell.setCellValue((String)bom.get("skuCode"));
							cell.setCellStyle(normalStyle);
							//物料规格名称
							cell = row.createCell(21);
							cell.setCellValue((String)bom.get("skuName"));
							cell.setCellStyle(normalStyle);
							//物料单位
							cell = row.createCell(22);
							cell.setCellValue((String)bom.get("unitName"));
							cell.setCellStyle(normalStyle);
							//物料单备注
							cell = row.createCell(23);
							cell.setCellValue(data.getBuyProductSkuBom().getRemark());
							cell.setCellStyle(normalStyle);
						}
					}
				}else{
					// 合并单元格
					ExcelUtil.setRangeStyle(sheet, tt+1, tt+1, 16, 24);
					cell = row.createCell(15);
					cell.setCellValue("无物料配置");
					cell.setCellStyle(normalStyle);
					//1.采购单号
					cell = row.createCell(0);
					cell.setCellValue(data.getOrderCode());
					cell.setCellStyle(normalStyle);
					//2.产品名称
					cell = row.createCell(1);
					cell.setCellValue(data.getProductName());
					cell.setCellStyle(normalStyle);
					//3.货号
					cell = row.createCell(2);
					cell.setCellValue(data.getProductCode());
					cell.setCellStyle(normalStyle);
					//4.条形码
					cell = row.createCell(3);
					cell.setCellValue(data.getBarcode());
					cell.setCellStyle(normalStyle);
					//5.规格名称
					cell = row.createCell(4);
					cell.setCellValue(data.getSkuName());
					cell.setCellStyle(normalStyle);
					//6.采购单价
					cell = row.createCell(5);
					cell.setCellValue(fnum.format(data.getPrice()));
					cell.setCellStyle(normalStyle);
					//7.采购数量
					cell = row.createCell(6);
					cell.setCellValue(data.getOrderNum());
					cell.setCellStyle(normalStyle);
					//8.实际到货数量
					cell = row.createCell(7);
					cell.setCellValue(ObjectUtil.isEmpty(data.getArrivalNum())?0:data.getArrivalNum());
					cell.setCellStyle(normalStyle);
					//9.未到货数量
					cell = row.createCell(8);
					cell.setCellValue(ObjectUtil.isEmpty(data.getUnArrivalNum())?0:data.getUnArrivalNum());
					cell.setCellStyle(normalStyle);
					//10.未到货金额
					cell = row.createCell(9);
					cell.setCellValue(fnum.format(data.getUnArrivalTotalPrice()));
					cell.setCellStyle(normalStyle);
					//11.采购商
					cell = row.createCell(10);
					cell.setCellValue(data.getBuyerName());
					cell.setCellStyle(normalStyle);
					//12.采购日期
					cell = row.createCell(11);
					cell.setCellValue(ObjectUtil.isEmpty(data.getOrderDate())?"":sdf.format(data.getOrderDate()));
					cell.setCellStyle(normalStyle);
					//13.要求到货日期
					cell = row.createCell(12);
					cell.setCellValue(ObjectUtil.isEmpty(data.getReqArrivalDate())?"":sdf.format(data.getReqArrivalDate()));
					cell.setCellStyle(normalStyle);
					//14.采购商备注
					cell = row.createCell(13);
					cell.setCellValue(data.getOrderRemark());
					cell.setCellStyle(normalStyle);
					//15.商品备注
					cell = row.createCell(14);
					cell.setCellValue(data.getOrderItemRemark());
					cell.setCellStyle(normalStyle);
				}

			}
		
		}
		ExcelUtil.preExport("BOM表", response);
		ExcelUtil.export(wb, response);
	}

}
