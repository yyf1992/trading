package com.nuotai.trading.seller.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * @author "
 * @date 2017-09-08 19:12:52
 */
@Data
public class SellerBillCycleManagementOld implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//历史账单周期编号
	private String oldBillCycleId;
	//买家商家：创建时指定卖家（查询时匹配卖家号）
	private String buyCompanyId;
	//卖家商家：创建时指定买家（查询时匹配买家号）
	private String sellerCompanyId;
	//账期开始日期
	private Integer cycleStartDate;
	//账期结束时间
	private Integer cycleEndDate;
	//结账周期
	private Integer checkoutCycle;
	//出账日期
	private Integer billStatementDate;
	//出账日期（date）
	private Date outStatementDate;
	//账单周期状态：1 待我审批，2 待对方审核，3 审核通过，4 审批驳回(买家)，5 审核驳回（卖家）
	private String billDealStatus;
	//创建人
	private String createUser;
	//
	private Date createTime;
}
