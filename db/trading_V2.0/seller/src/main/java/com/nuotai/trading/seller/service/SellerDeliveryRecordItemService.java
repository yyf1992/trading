package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerDeliveryRecordItemMapper;
import com.nuotai.trading.seller.model.SellerDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


/**
 * The type Seller delivery record item service.
 */
@Service
@Transactional
public class SellerDeliveryRecordItemService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerDeliveryRecordItemService.class);

	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;

	/**
	 * Get seller delivery record item.
	 *
	 * @param id the id
	 * @return the seller delivery record item
	 */
	public SellerDeliveryRecordItem get(String id){
		return sellerDeliveryRecordItemMapper.get(id);
	}

	/**
	 * Query list list.
	 *
	 * @param map the map
	 * @return the list
	 */
	public List<SellerDeliveryRecordItem> queryList(Map<String, Object> map){
		return sellerDeliveryRecordItemMapper.queryList(map);
	}

	/**
	 * Query count int.
	 *
	 * @param map the map
	 * @return the int
	 */
	public int queryCount(Map<String, Object> map){
		return sellerDeliveryRecordItemMapper.queryCount(map);
	}

	/**
	 * Add.
	 *
	 * @param sellerDeliveryRecordItem the seller delivery record item
	 */
	public void add(SellerDeliveryRecordItem sellerDeliveryRecordItem){
		sellerDeliveryRecordItemMapper.add(sellerDeliveryRecordItem);
	}

	/**
	 * Update.
	 *
	 * @param sellerDeliveryRecordItem the seller delivery record item
	 */
	public void update(SellerDeliveryRecordItem sellerDeliveryRecordItem){
		sellerDeliveryRecordItemMapper.update(sellerDeliveryRecordItem);
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(String id){
		sellerDeliveryRecordItemMapper.delete(id);
	}


	/**
	 * Gets item by delivery id.
	 * 根据发货单ID查询明细
	 * @param deliveryId the delivery id
	 * @return the item by delivery id
	 */
	public List<SellerDeliveryRecordItem> getItemByDeliveryId(String deliveryId) {
		Map<String,Object> map = new HashMap<>();
		map.put("deliveryId",deliveryId);
		return sellerDeliveryRecordItemMapper.getItemByDeliveryId(map);
	}

	//根据条件查询数据详细
	public List<SellerDeliveryRecordItem> queryDeliveryRecordItem(Map<String,Object> map){
		return sellerDeliveryRecordItemMapper.queryDeliveryRecordItem(map);
	}
}
