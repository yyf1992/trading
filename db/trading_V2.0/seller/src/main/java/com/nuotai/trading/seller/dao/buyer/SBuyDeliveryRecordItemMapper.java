package com.nuotai.trading.seller.dao.buyer;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;

/**
 * The interface S buy delivery record item mapper.
 *
 * @author "
 * @date 2017 -09-06 10:21:29
 */
public interface SBuyDeliveryRecordItemMapper extends BaseDao<BuyDeliveryRecordItem> {


    /**
     * Gets item by delivery item id.
     * 根据发货单明细ID获取收货单明细
     *
     * @param deliveryItemId the id
     * @return the item by delivery item id
     */
    BuyDeliveryRecordItem getItemByDeliveryItemId(String deliveryItemId);

    /**
     * Update by primary key selective.
     *
     * @param updBuyItem the upd buy item
     */
    void updateByPrimaryKeySelective(BuyDeliveryRecordItem updBuyItem);

	List<Map<String,Object>> getItemByRecordId(String deliveryNo);

}
