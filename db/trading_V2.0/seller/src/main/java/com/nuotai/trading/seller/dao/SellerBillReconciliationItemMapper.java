package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillReconciliationItem;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2017-10-18 11:06:21
 */
@Component
public interface SellerBillReconciliationItemMapper extends BaseDao<SellerBillReconciliationItem> {

    //待开票发货数据列表查询
    List<SellerBillReconciliationItem> queryRecDecliveryItemList(SearchPageUtil searchPageUtil);
    //根据账单编号查询列表详情
    List<SellerBillReconciliationItem> queryRecDecliveryItemExport(Map<String,Object> map);
    //根据条件查询数量
    int queryCountByInvoice(Map<String,Object> map);
    //添加对账详情
    int addRecItem(SellerBillReconciliationItem buyBillReconciliationItem);
    //修改对账单价
    int updateRecItemPrice(Map<String, Object> updateMap);
    //根据账单编号查询对账详情
    List<SellerBillReconciliationItem> queryRecItemList(Map<String,Object> map);
    //根据详情编号查询
    SellerBillReconciliationItem queryRecItemBuyId(Map<String,Object> map);
    //根据订单号查询附件
    List<Map<String,Object>> queryRecItemAddr(String deliverNo);
    //根据订单号查询售后附件
    Map<String,Object> queryCustomerProof(String deliverNo);
    //根据详情编号查询
    SellerBillReconciliationItem queryRecItem(String recItemId);
    //根据发货、退货编号查询详情数据量
    int queryRecItemCount(Map<String,Object> map);
    //根据账单编号删除详情
    int deleteRecItem(Map<String,Object> map);
}
