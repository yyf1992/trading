package com.nuotai.trading.seller.model;

import lombok.Data;

import java.io.Serializable;


/**
 * 
 * 
 * @author "
 * @date 2017-09-08 20:28:40
 */
public class SellerBillInterestOld {
	//
	private String id;
	//历史账单周期编号
	private String billCycleIdOld;
	//逾期天数
	private Integer overdueDate;
	//逾期利息
	private Integer overdueInterest;
	//利息计算方式：1 单利，2 复利
	private String interestCalculationMethod;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBillCycleIdOld() {
		return billCycleIdOld;
	}

	public void setBillCycleIdOld(String billCycleIdOld) {
		this.billCycleIdOld = billCycleIdOld;
	}

	public Integer getOverdueDate() {
		return overdueDate;
	}

	public void setOverdueDate(Integer overdueDate) {
		this.overdueDate = overdueDate;
	}

	public Integer getOverdueInterest() {
		return overdueInterest;
	}

	public void setOverdueInterest(Integer overdueInterest) {
		this.overdueInterest = overdueInterest;
	}

	public String getInterestCalculationMethod() {
		return interestCalculationMethod;
	}

	public void setInterestCalculationMethod(String interestCalculationMethod) {
		this.interestCalculationMethod = interestCalculationMethod;
	}
}
