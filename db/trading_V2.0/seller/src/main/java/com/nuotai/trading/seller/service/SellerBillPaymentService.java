package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.*;
import com.nuotai.trading.seller.dao.buyer.SBuyBillReconciliationMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyCustomerMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderMapper;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliation;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class SellerBillPaymentService {
	@Autowired
	private SellerBillReconciliationMapper sellerBillReconciliationMapper;
	@Autowired
	private SellerBillReconciliationItemMapper sellerBillReconciliationItemMapper;
	@Autowired
	private SBuyBillReconciliationMapper sBuyBillReconciliationMapper;
	@Autowired
	private SellerBillInvoiceMapper sellerBillInvoiceMapper;
	@Autowired
	private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;
	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;
	@Autowired
	private SellerBillReconciliationPaymentMapper sellerBillReconciliationPaymentMapper;
	@Autowired
	private SellerDeliveryRecordService sellerDeliveryRecordService;//卖家发货单号
	@Autowired
	private SBuyDeliveryRecordMapper sBuyDeliveryRecordMapper;//买家发货
	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;//卖家退货单号
	@Autowired
	private SBuyCustomerMapper sBuyCustomerMapper;//买家退或
	@Autowired
	private SellerOrderMapper sellerOrderMapper;  //卖家订单信息
	@Autowired
	private SBuyOrderMapper buyOrderMapper;


	//查询付款对账列表信息
	public List<SellerBillReconciliation> getRecToPamyment(
			SearchPageUtil searchPageUtil) {
		return sellerBillReconciliationMapper.getRecToPamyment(searchPageUtil);
	}

	//根据审批状态查询数量
	public int queryRecPaymentCount(Map<String, Object> map) {
		return sellerBillReconciliationMapper.queryRecPaymentCount(map);
	}

	//向账单周期管理页面提供不同状态下的数量
	public void queryPaymentCount(Map<String, Object> billMap){
		//查询待内部审批周期数
		billMap.put("paymentStatusCount", "0");
		int approvalBillReconciliationCount = queryRecPaymentCount(billMap);
		//查询待对方审批周期数
		billMap.put("paymentStatusCount", "1");
		int acceptBillReconciliationCount = queryRecPaymentCount(billMap);
		//查询审批已通过周期数
		billMap.put("paymentStatusCount", "2");
		int apprEndBillReconciliationCount = queryRecPaymentCount(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillReconciliationCount", approvalBillReconciliationCount);
		billMap.put("acceptBillReconciliationCount", acceptBillReconciliationCount);
		billMap.put("apprEndBillReconciliationCount", apprEndBillReconciliationCount);

	}

	//付款信息导出
	public List<SellerBillReconciliation> queryRecToPamymentList(Map<String,Object> paymentMap){
		return sellerBillReconciliationMapper.queryRecToPamymentList(paymentMap);
	}

	//卖家确认付款
	public  int updateSavePaymentInfo(Map<String,Object> updatePaymentMap){
		int updatePaymentCount = 0;
		String reconciliationId = updatePaymentMap.get("reconciliationId").toString();
		//String paymentStatus = updatePaymentMap.get("billDealStatus").toString();
		String sellerPaymentRemarks = updatePaymentMap.get("paymentRemarks").toString();

		//注意查询应付剩余款是否为0 residualPaymentAmount
		Map<String,Object> reconciliatonMap = new HashMap<String,Object>();
		reconciliatonMap.put("id",reconciliationId);
		SellerBillReconciliation sellerBillRec = sellerBillReconciliationMapper.selectByPrimaryKey(reconciliatonMap);
		BigDecimal residualPaymentAmount = sellerBillRec.getResidualPaymentAmount();
		int isPayment = residualPaymentAmount.compareTo(BigDecimal.ZERO);
		//String paymentStatus = "";
		SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
		sellerBillReconciliation.setId(reconciliationId);
		//String paymentStatus = "";
		if(isPayment == 0){
			sellerBillReconciliation.setPaymentStatus("2");
			//paymentStatus = "2";
		}
		sellerBillReconciliation.setSellerPaymentRemarks(sellerPaymentRemarks);
		updatePaymentCount = sellerBillReconciliationMapper.updateByPrimaryKeySelective(sellerBillReconciliation);
		if(updatePaymentCount > 0){
			SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
			sBuyBillReconciliation.setId(reconciliationId);
			if(isPayment == 0){
				sBuyBillReconciliation.setPaymentStatus("2");
			}
			sBuyBillReconciliation.setSellerPaymentRemarks(sellerPaymentRemarks);
			sBuyBillReconciliationMapper.updateByPrimaryKeySelective(sBuyBillReconciliation);

			//批量修改付款详情
			Map<String,Object> paymentItemMap = new HashMap<String,Object>();
			paymentItemMap.put("reconciliationId",reconciliationId);
			paymentItemMap.put("paymentStatus","3");
			paymentItemMap.put("returnStatus","1");
			paymentItemMap.put("receivablesRemarks",sellerPaymentRemarks);
			sellerBillReconciliationPaymentMapper.updatePaymentItem(paymentItemMap);

			if(isPayment == 0){
				//根据对账详情修改发货、退货模块对账状态
				Map<String,Object> recItemMap = new HashMap<String,Object>();
				recItemMap.put("reconciliationId",reconciliationId);
				List<SellerBillReconciliationItem> recItemInfo = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);

				Map<String,Object> recordIdMap = new HashMap<String,Object>();
				for(SellerBillReconciliationItem recItem:recItemInfo){
					String recordCode = recItem.getDeliverNo();
					String billReconciliationType = recItem.getBillReconciliationType();
					//String reconciliationStatus = "2";
					if(recordIdMap.containsKey(recordCode)){
						billReconciliationType = recordIdMap.get(recordCode).toString();
					}
					recordIdMap.put(recordCode,billReconciliationType);

					//根据订单号查询订单状态
					SellerOrder sellerOrder = sellerOrderMapper.getAllContent(recItem.getOrderId());
					//同步修改发货订单对账状态
					if(!ObjectUtil.isEmpty(sellerOrder)){
						if(null != sellerOrder.getOrderCode()&&!"".equals(sellerOrder.getOrderCode())){
							String orderCode = sellerOrder.getOrderCode();//订单号
							int orderStatus = sellerOrder.getStatus();//状态
							Map<String,Object> deliveryOrderMap = new HashMap<String,Object>();
							deliveryOrderMap.put("orderCode",orderCode);
							if(orderStatus == 4 || orderStatus ==8){
								deliveryOrderMap.put("isPaymentStatus","2");
							}else {
								deliveryOrderMap.put("isPaymentStatus","3");
							}
							sellerOrderMapper.updateOrderBuyBill(deliveryOrderMap);
							buyOrderMapper.updateOrderBuyBill(deliveryOrderMap);
						}
					}
				}

				//修改发货、退货对账状态
				for(Map.Entry<String,Object> entry:recordIdMap.entrySet()){
					String recordCode = entry.getKey();
					String recType = entry.getValue().toString();
					if("1".equals(recType)){
						Map<String,Object> recRordItemMap = new HashMap<String,Object>();
						recRordItemMap.put("recordCode",recordCode);
						recRordItemMap.put("isPaymentStatus","2");
						sellerDeliveryRecordService.updateDeliveryByReconciliation(recRordItemMap);
						sBuyDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
					}
					if("2".equals(recType)){
						Map<String,Object> recRordItemMap = new HashMap<String,Object>();
						recRordItemMap.put("recordCode",recordCode);
						recRordItemMap.put("isPaymentStatus","2");
						sellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
						sBuyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
					}
					if("3".equals(recType)){
						Map<String,Object> recRordItemMap = new HashMap<String,Object>();
						recRordItemMap.put("recordCode",recordCode);
						recRordItemMap.put("isPaymentStatus","2");
						sellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
						sBuyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
					}
				}
			}
		}
		return  updatePaymentCount;
	}

	//根据账单号查询付款单详情
	public Map<String,Object> getRecItemInfo(String reconciliationId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation buyBillReconciliation = selectByPrimaryKey(map);
		int totalSumCount = buyBillReconciliation.getTotalSumCount();
		BigDecimal totalAmount = buyBillReconciliation.getTotalAmount();
		String buyCompanyId = buyBillReconciliation.getBuyCompanyId();
		String buyCompanyName = buyBillReconciliation.getBuyCompanyName();
		String reconciliationStatus = buyBillReconciliation.getBillDealStatus();
		String startBillStatementDateStr = buyBillReconciliation.getStartBillStatementDateStr();
		String endBillstatementDateStr = buyBillReconciliation.getEndBillStatementDateStr();
		BigDecimal invoiceTotalSum = new BigDecimal(0);//发票票据总金额

		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		List<Map<String,Object>> recordItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> recordReplaceItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> customerItemList = new ArrayList<Map<String,Object>>();//收获详细

		if(null != billRecItemList && billRecItemList.size()>0){
			for(SellerBillReconciliationItem buyBillReconciliationItem : billRecItemList){
				String recItemId = buyBillReconciliationItem.getId();
				String billRecType = buyBillReconciliationItem.getBillReconciliationType();
				String deliverNo = buyBillReconciliationItem.getDeliverNo();
				String itemId = buyBillReconciliationItem.getItemId();
				String arrivalDateStr = buyBillReconciliationItem.getArrivalDateStr();

				//根据账单详情编号查询发票
				Map<String,Object> invoiceMap = new HashMap<String,Object>();
				invoiceMap.put("recItemId",recItemId);
				SellerBillInvoice billInvoice = sellerBillInvoiceMapper.queryInvoiceByRecItem(invoiceMap);
				String invoiceNo = "";
				String invoiceType = "";
				String invoiceFileAddr = "";
				BigDecimal invoiceTotal = new BigDecimal(0);
				if(null != billInvoice){
					invoiceNo = billInvoice.getInvoiceNo();//票据号
					invoiceTotal = billInvoice.getTotalInvoiceValue();//票据金额
					invoiceTotalSum = invoiceTotalSum.add(invoiceTotal);//票据总额
					invoiceType = billInvoice.getInvoiceType();//票据类型
					invoiceFileAddr = billInvoice.getInvoiceFileAddr();//发票票据
				}
				/*String invoiceNo = billInvoice.getInvoiceNo();//票据号
				BigDecimal invoiceTotal = billInvoice.getTotalInvoiceValue();//票据金额
				invoiceTotalSum = invoiceTotalSum.add(invoiceTotal);//票据总额
				String invoiceType = billInvoice.getInvoiceType();//票据类型
				String invoiceFileAddr = billInvoice.getInvoiceFileAddr();//发票票据*/
				//根据账单详情编号查询订单号
				Map<String,Object> deliveryItemMap = new HashMap<String,Object>();
				deliveryItemMap.put("deliveryItemId",itemId);
				SellerDeliveryRecordItem deRecItem = sellerDeliveryRecordItemMapper.queryOrderCodeByItemId(deliveryItemMap);
				String orderCode = "";
				if(null != deRecItem){
					if(null != deRecItem.getOrderCode() && !"".equals(deRecItem.getOrderCode())){
						orderCode = deRecItem.getOrderCode();
					}
				}

				//根据发货单号查询物流信息
				String waybillNo = "";
				if(null != deliverNo && !"".equals(deliverNo)){
					Map<String,Object> logisctMap = new HashMap<String,Object>();
					logisctMap.put("deliverNo",deliverNo);
					Map<String,Object> logMap = sellerDeliveryRecordMapper.queryFreightByRecId(logisctMap);
					if(null != logMap && logMap.size() > 0){
						if(null != logMap.get("waybillNo") && !"".equals(logMap.get("waybillNo"))){
							waybillNo = logMap.get("waybillNo").toString();
						}
					}
				}

				int arrivalNum = buyBillReconciliationItem.getArrivalNum();//数量
				BigDecimal salePrice = buyBillReconciliationItem.getSalePrice();//单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额
				if("1".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					String receiptvoucherAddr = buyBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("itemId",itemId);
					recordAndTtemMap.put("orderCode",orderCode);
					recordAndTtemMap.put("waybillNo",waybillNo);
					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("buyDeliveryRecordItem",buyBillReconciliationItem);
					recordAndTtemMap.put("salePriceSum",salePriceSum);

					recordAndTtemMap.put("invoiceNo",invoiceNo);
					recordAndTtemMap.put("invoiceTotal",invoiceTotal);
					recordAndTtemMap.put("invoiceType",invoiceType);
					recordAndTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					recordItemList.add(recordAndTtemMap);
				}else if("2".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					String receiptvoucherAddr = buyBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("itemId",itemId);
					recordAndTtemMap.put("orderCode",orderCode);
					recordAndTtemMap.put("waybillNo",waybillNo);
					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("buyRecordReplaceItem",buyBillReconciliationItem);
					recordAndTtemMap.put("replacePriceSum",salePriceSum);

					recordAndTtemMap.put("invoiceNo",invoiceNo);
					recordAndTtemMap.put("invoiceTotal",invoiceTotal);
					recordAndTtemMap.put("invoiceType",invoiceType);
					recordAndTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					recordReplaceItemList.add(recordAndTtemMap);
				}else if("3".equals(billRecType)){
					Map<String,Object> customerTtemMap = new HashMap<String,Object>();
					String customerCode = buyBillReconciliationItem.getCustomerCode();
					String proof = buyBillReconciliationItem.getProof();

					customerTtemMap.put("itemId",itemId);
					customerTtemMap.put("orderCode",orderCode);
					customerTtemMap.put("waybillNo",waybillNo);
					customerTtemMap.put("customerCode",customerCode);
					customerTtemMap.put("verifyDateStr",arrivalDateStr);
					customerTtemMap.put("proof",proof);
					customerTtemMap.put("buyCustomerItem",buyBillReconciliationItem);
					customerTtemMap.put("goodsPriceSum",salePriceSum);

					customerTtemMap.put("invoiceNo",invoiceNo);
					customerTtemMap.put("invoiceTotal",invoiceTotal);
					customerTtemMap.put("invoiceType",invoiceType);
					customerTtemMap.put("invoiceFileAddr",invoiceFileAddr);
					customerItemList.add(customerTtemMap);
				}
			}
		}
		//拼装返回值
		Map<String,Object> recordAndReturnMap = new HashMap<String,Object>();
		recordAndReturnMap.put("reconciliationId",reconciliationId);
		recordAndReturnMap.put("totalSumCount",totalSumCount);
		recordAndReturnMap.put("invoiceTotalSum",invoiceTotalSum);
		recordAndReturnMap.put("totalAmount",totalAmount);
		recordAndReturnMap.put("reconciliationStatus",reconciliationStatus);
		recordAndReturnMap.put("buyCompanyName",buyCompanyName);
		recordAndReturnMap.put("startBillStatementDateStr",startBillStatementDateStr);
		recordAndReturnMap.put("endBillstatementDateStr",endBillstatementDateStr);
		recordAndReturnMap.put("recordItemList",recordItemList);
		recordAndReturnMap.put("recordReplaceItemList",recordReplaceItemList);
		recordAndReturnMap.put("customerItemList",customerItemList);
		return recordAndReturnMap;
	}


	// 根据条件查询账单信息bean
	public SellerBillReconciliation selectByPrimaryKey(Map<String,Object> map) {
		return sellerBillReconciliationMapper.selectByPrimaryKey(map);
	}

	//根据账单编号查询付款记录
	public List<SellerBillReconciliationPayment> getPaymentList(String reconciliationId){
		Map<String,Object> paymentMap = new HashMap<String,Object>();
		paymentMap.put("reconciliationId",reconciliationId);
		return sellerBillReconciliationPaymentMapper.getPaymentListByRecId(paymentMap);
	}

	//根据编号修改付款状态
	public int updatePaymentItem(Map<String, Object> updateMap){
		String reconciliationId = updateMap.get("reconciliationId").toString();
		String paymentId = updateMap.get("id").toString();
		String sellerPaymentRemarks = updateMap.get("receivablesRemarks").toString();

		//注意查询应付剩余款是否为0 residualPaymentAmount
		Map<String,Object> reconciliatonMap = new HashMap<String,Object>();
		reconciliatonMap.put("id",reconciliationId);
		SellerBillReconciliation sellerBillRec = sellerBillReconciliationMapper.selectByPrimaryKey(reconciliatonMap);
		BigDecimal residualPaymentAmount = sellerBillRec.getResidualPaymentAmount();
		int isPayment = residualPaymentAmount.compareTo(BigDecimal.ZERO);
		//String paymentStatus = "";
		int updatePaymentCount = 0;
		if(isPayment == 0){
			String paymentStatus = "2";


			SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
			sellerBillReconciliation.setId(reconciliationId);
			sellerBillReconciliation.setPaymentStatus(paymentStatus);
			sellerBillReconciliation.setSellerPaymentRemarks(sellerPaymentRemarks);
			updatePaymentCount = sellerBillReconciliationMapper.updateByPrimaryKeySelective(sellerBillReconciliation);
			if(updatePaymentCount > 0){
				SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
				sBuyBillReconciliation.setId(reconciliationId);
				sBuyBillReconciliation.setPaymentStatus(paymentStatus);
				sBuyBillReconciliation.setSellerPaymentRemarks(sellerPaymentRemarks);
				sBuyBillReconciliationMapper.updateByPrimaryKeySelective(sBuyBillReconciliation);

				//批量修改付款详情
				/*Map<String,Object> paymentItemMap = new HashMap<String,Object>();
				paymentItemMap.put("reconciliationId",reconciliationId);
				paymentItemMap.put("paymentStatus","1");
				paymentItemMap.put("receivablesRemarks",sellerPaymentRemarks);
				sellerBillReconciliationPaymentMapper.updatePaymentItem(paymentItemMap);*/

				//if("2".equals(paymentStatus)){
					//根据对账详情修改发货、退货模块对账状态
					Map<String,Object> recItemMap = new HashMap<String,Object>();
					recItemMap.put("reconciliationId",reconciliationId);
					List<SellerBillReconciliationItem> recItemInfo = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);

					Map<String,Object> recordIdMap = new HashMap<String,Object>();
					for(SellerBillReconciliationItem recItem:recItemInfo){
						String recordCode = recItem.getDeliverNo();
						String billReconciliationType = recItem.getBillReconciliationType();
						//String reconciliationStatus = "2";
						if(recordIdMap.containsKey(recordCode)){
							billReconciliationType = recordIdMap.get(recordCode).toString();
						}
						recordIdMap.put(recordCode,billReconciliationType);
					}

					//修改发货、退货对账状态
					for(Map.Entry<String,Object> entry:recordIdMap.entrySet()){
						String recordCode = entry.getKey();
						String recType = entry.getValue().toString();
						if("1".equals(recType)){
							Map<String,Object> recRordItemMap = new HashMap<String,Object>();
							recRordItemMap.put("recordCode",recordCode);
							recRordItemMap.put("isPaymentStatus","2");
							sellerDeliveryRecordService.updateDeliveryByReconciliation(recRordItemMap);
							sBuyDeliveryRecordMapper.updateDeliveryByReconciliation(recRordItemMap);
						}
						if("2".equals(recType)){
							Map<String,Object> recRordItemMap = new HashMap<String,Object>();
							recRordItemMap.put("recordCode",recordCode);
							recRordItemMap.put("isPaymentStatus","2");
							sellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
							sBuyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
						}
						if("3".equals(recType)){
							Map<String,Object> recRordItemMap = new HashMap<String,Object>();
							recRordItemMap.put("recordCode",recordCode);
							recRordItemMap.put("isPaymentStatus","2");
							sellerCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
							sBuyCustomerMapper.updateCustomerByReconciliation(recRordItemMap);
						}
					}
				//}
			}
		}else {
			Map<String,Object> paymentMap = new HashMap<String,Object>();
			paymentMap.put("id",paymentId);
			paymentMap.put("paymentStatus","3");
			paymentMap.put("receivablesRemarks",sellerPaymentRemarks);
			updatePaymentCount = sellerBillReconciliationPaymentMapper.updatePaymentItem(paymentMap);
		}

		return updatePaymentCount;
	}

	//对账详情查看附件
	public String queryFileUrlList(Map<String,Object> queryUrlMap){
		String itemType = queryUrlMap.get("itemTtpe").toString();
		String deliverNo = queryUrlMap.get("deliverNo").toString();
		String attachmentAddrStr = "";
		if("1".equals(itemType)){
			//根据发货单号查询票据附件
			List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
			if(null != attachmentList && attachmentList.size()>0){
				for(Map<String,Object> attachmentMap : attachmentList){
					attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString()+",";

				}
				attachmentAddrStr = attachmentAddrStr.substring(0,attachmentAddrStr.length()-1);
			}
		}else {
			//售后附件
			Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
			if(null != customerProof && customerProof.size()>0){
				attachmentAddrStr = customerProof.get("proof").toString();
			}
		}
		return attachmentAddrStr.trim();
	}

	//查询详细
	/**
	 * 对账详情
	 * @param reconciliationId
	 * @param model
	 * @return
	 */
	public void queryRecordAndReturnNew(String reconciliationId, Model model) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation sellerBillReconciliation = selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(SellerBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> recordReplaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recordReplaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recordReplaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recordReplaceItemList && recordReplaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : recordReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recordReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> daohuoItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> replaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> customerItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					customerItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put("customerList", itemMap);
				}
			}
		}
		model.addAttribute("buyBillReconciliation", sellerBillReconciliation);
		model.addAttribute("recordReplaceMap",recordReplaceMap);
		model.addAttribute("daohuoMap", daohuoMap);
		model.addAttribute("replaceMap", replaceMap);
		model.addAttribute("customerMap", customerMap);
	}

	//付款账单详情导出
	public Map<String,Object> queryPaymentRecItemExport(String reconciliationId) {
		Map<String,Object> recItemExportMap = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation sellerBillReconciliation = selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(SellerBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> recordReplaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recordReplaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recordReplaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recordReplaceItemList && recordReplaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : recordReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recordReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> daohuoItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> replaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> customerItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					customerItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put("customerList", itemMap);
				}
			}
		}
		recItemExportMap.put("sellerBillReconciliation", sellerBillReconciliation);
		recItemExportMap.put("daohuoMap", daohuoMap);
		recItemExportMap.put("recordReplaceMap",recordReplaceMap);
		recItemExportMap.put("replaceMap", replaceMap);
		recItemExportMap.put("customerMap", customerMap);
		return recItemExportMap;
	}
}
