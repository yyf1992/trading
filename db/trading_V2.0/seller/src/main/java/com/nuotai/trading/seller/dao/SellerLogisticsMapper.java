package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerLogistics;

/**
 * 
 * 
 * @author "
 * @date 2017-08-10 14:24:37
 */
public interface SellerLogisticsMapper extends BaseDao<SellerLogistics> {
	//根据发货单获取物流信息
	public SellerLogistics getSellerLogisticsByDeliveryId(String deliveryid);
}
