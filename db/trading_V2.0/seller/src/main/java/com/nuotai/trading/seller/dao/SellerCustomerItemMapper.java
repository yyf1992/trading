package com.nuotai.trading.seller.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerCustomerItem;

/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
public interface SellerCustomerItemMapper extends BaseDao<SellerCustomerItem> {
	
	void deleteByCustomerId(String customerId);
	
	List<SellerCustomerItem> selectCustomerItemByCustomerId(String customerId);

	//根据条件查询售后退货详情
	List<SellerCustomerItem> queryCustomerItem(Map<String,Object> map);
	//根据对账状态修改
	int updateCustomerByReconciliation(Map<String,Object> map);

	List<SellerCustomerItem> getItemByCustomerId(@Param("customerId")String id);
	
	List<SellerCustomerItem> selectItemByCustomerId(@Param("customerId")String id);

	//根据采购商、周期、下单人生成对账账单
	List<SellerCustomerItem> queryCustmerItemList(Map<String,Object> map);
}
