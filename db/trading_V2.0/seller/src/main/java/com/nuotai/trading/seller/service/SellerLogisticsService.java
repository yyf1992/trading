package com.nuotai.trading.seller.service;

import com.nuotai.trading.seller.dao.SellerLogisticsMapper;
import com.nuotai.trading.seller.model.SellerLogistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class SellerLogisticsService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerLogisticsService.class);

	@Autowired
	private SellerLogisticsMapper sellerLogisticsMapper;
	
	public SellerLogistics get(String id){
		return sellerLogisticsMapper.get(id);
	}
	
	public List<SellerLogistics> queryList(Map<String, Object> map){
		return sellerLogisticsMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerLogisticsMapper.queryCount(map);
	}
	
	public void add(SellerLogistics buyLogistics){
		sellerLogisticsMapper.add(buyLogistics);
	}
	
	public void update(SellerLogistics buyLogistics){
		sellerLogisticsMapper.update(buyLogistics);
	}
	
	public void delete(String id){
		sellerLogisticsMapper.delete(id);
	}
	
	//根据发货单获取物流信息
	public SellerLogistics getSellerLogisticsByDeliveryId(String deliveryid){
		return sellerLogisticsMapper.getSellerLogisticsByDeliveryId(deliveryid);
	}
	

}
