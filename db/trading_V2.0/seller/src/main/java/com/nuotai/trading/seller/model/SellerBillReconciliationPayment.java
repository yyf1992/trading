package com.nuotai.trading.seller.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-11-01 11:02:22
 */
@Data
public class SellerBillReconciliationPayment implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//对账单号
	private String reconciliationId;
	//付款金额
	private BigDecimal actualPaymentAmount;
	//付款账号
	private String bankAccount;
	//付款账号
	private String bankAccountNum;
	//付款类型
	private String paymentType;
	//付款收据
	private String paymentAddr;
	//收款收据
	private String receivablesAddr;
	//付款时间
	private Date paymentDate;
	//格式化时间
	private String paymentDateStr;
	//收款确认时间
	private Date receivablesDate;
	//付款状态
	private String paymentStatus;
	//付款备注
	private String paymentRemarks;
	//收款备注
	private String receivablesRemarks;
}
