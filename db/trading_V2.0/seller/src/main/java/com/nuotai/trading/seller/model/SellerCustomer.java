package com.nuotai.trading.seller.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-19 16:34:55
 */
@Data
public class SellerCustomer implements Serializable {
 private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//售后编号
	private String customerCode;
	//公司id
	private String companyId;
	//公司名称
	private String buyCompanyName;
	//供应商id
	private String supplierId;
	//供应商名称
	private String supplierName;
	//
	private String person;
	//
	private String phone;
	//售后类型0-换货，1-退款退货
	private String type;
	//是否删除0-未删除；1-已删除
	private String isDel;
	//状态0-待我确认；1-已通过
	private String status;
	//申请原因
	private String reason;
	//商品图片
	private String proof;
	// 收货地址信息
	private String addressId;
	private String province;
	//
	private String city;
	//
	private String area;
	//
	private String addrName;
	//
	private String zone;
	//
	private String telNo;
	//
	private String linkman;
	//
	private String linkmanPhone;
	//创建人id
	private String createId;
	//创建人名称
	private String createName;
	//创建时间
	private Date createDate;
	//修改人id
	private String updateId;
	//修改人名称
	private String updateName;
	//修改时间
	private Date updateDate;
	//删除人id
	private String delId;
	//删除人名称
	private String delName;
	//删除时间
	private Date delDate;
	//买家售后id
	private String buyCustomerId;
	private List<SellerCustomerItem> itemList;
	//审核意见
	private String verifyRemark;
	//审核时间
	private Date verifyDate;

	//字符审核时间
	private String verifyDateStr;
	//开具发票状态
	private String isInvoiceStatus;
	//对账状态
	private String reconciliationStatus;
	//收款状态
	private String isPaymentStatus;
	//出库标题
	private String title;
	//出库仓库
	private String warehouseId;
	private String pushType;
	private String arrivalType;
	private boolean faHuoFlag;
	
	private String provinceName;
	private String personName;
	//收货手机号码
	private String receiptPhone;
	//市
	private String cityName;
	//区
	private String areaName;
	//订单号
	private String orderCode;
	
}
