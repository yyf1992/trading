package com.nuotai.trading.seller.model;

import java.util.Date;

public class SellerBillCycleManagement {
    private String id;

    private String buyCompanyId;

    private String buyCompanyName;

    private String sellerCompanyId;

    private String sellerCompanyName;

    private Integer cycleStartDate;

    private Integer cycleEndDate;

    private Integer checkoutCycle;

    private Integer billStatementDate;

    private Date outStatementDate;

    private String billDealStatus;

    private String createUser;

    private Date createTime;

    private String createDate;

    private String approvalUser;

    private Date approvalTime;

    private String approvalRemarks;

    private String acceptUser;

    private Date acceptTime;

    private String acceptRemarks;

    private String rejectUser;

    private Date rejectTime;

    private String rejectRemarks;

    private String createUserName;

    private String updateUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyCompanyId() {
        return buyCompanyId;
    }

    public void setBuyCompanyId(String buyCompanyId) {
        this.buyCompanyId = buyCompanyId;
    }

    public String getSellerCompanyId() {
        return sellerCompanyId;
    }

    public void setSellerCompanyId(String sellerCompanyId) {
        this.sellerCompanyId = sellerCompanyId;
    }

    public Integer getCycleStartDate() {
        return cycleStartDate;
    }

    public void setCycleStartDate(Integer cycleStartDate) {
        this.cycleStartDate = cycleStartDate;
    }

    public Integer getCycleEndDate() {
        return cycleEndDate;
    }

    public void setCycleEndDate(Integer cycleEndDate) {
        this.cycleEndDate = cycleEndDate;
    }

    public Integer getCheckoutCycle() {
        return checkoutCycle;
    }

    public void setCheckoutCycle(Integer checkoutCycle) {
        this.checkoutCycle = checkoutCycle;
    }

    public Integer getBillStatementDate() {
        return billStatementDate;
    }

    public void setBillStatementDate(Integer billStatementDate) {
        this.billStatementDate = billStatementDate;
    }

    public String getBillDealStatus() {
        return billDealStatus;
    }

    public void setBillDealStatus(String billDealStatus) {
        this.billDealStatus = billDealStatus;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getApprovalUser() {
        return approvalUser;
    }

    public void setApprovalUser(String approvalUser) {
        this.approvalUser = approvalUser;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String getAcceptUser() {
        return acceptUser;
    }

    public void setAcceptUser(String acceptUser) {
        this.acceptUser = acceptUser;
    }

    public Date getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getAcceptRemarks() {
        return acceptRemarks;
    }

    public void setAcceptRemarks(String acceptRemarks) {
        this.acceptRemarks = acceptRemarks;
    }

    public String getRejectUser() {
        return rejectUser;
    }

    public void setRejectUser(String rejectUser) {
        this.rejectUser = rejectUser;
    }

    public Date getRejectTime() {
        return rejectTime;
    }

    public void setRejectTime(Date rejectTime) {
        this.rejectTime = rejectTime;
    }

    public String getRejectRemarks() {
        return rejectRemarks;
    }

    public void setRejectRemarks(String rejectRemarks) {
        this.rejectRemarks = rejectRemarks;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getBuyCompanyName() {
        return buyCompanyName;
    }

    public void setBuyCompanyName(String buyCompanyName) {
        this.buyCompanyName = buyCompanyName;
    }

    public String getSellerCompanyName() {
        return sellerCompanyName;
    }

    public void setSellerCompanyName(String sellerCompanyName) {
        this.sellerCompanyName = sellerCompanyName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getOutStatementDate() {
        return outStatementDate;
    }

    public void setOutStatementDate(Date outStatementDate) {
        this.outStatementDate = outStatementDate;
    }
}