package com.nuotai.trading.seller.controller;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.seller.model.SellerBillReconciliationAdvance;
import com.nuotai.trading.seller.service.SellerBillReconciliationAdvanceService;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;



/**
 * 
 * 
 * @author "
 * @date 2018-02-24 13:04:37
 */
@Controller
@RequestMapping("platform/seller/billAdvance")
public class SellerBillReconciliationAdvanceController extends BaseController{
	@Autowired
	private SellerBillReconciliationAdvanceService sellerBillRecAdvanceService;

	/**
	 * 列表
	 */
	@RequestMapping("billAdvanceList")
	public String billAdvanceList(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("advanceStatus")){
			params.put("advanceStatus","0");
		}
		params.put("sellerCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);

		List<SellerBillReconciliationAdvance> buyBillRecAdvanceList = sellerBillRecAdvanceService.queryAdvanceList(searchPageUtil);
		sellerBillRecAdvanceService.queryAdvanceCountByStatus(params);
		searchPageUtil.getPage().setList(buyBillRecAdvanceList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/sellers/billcycles/billadvance/billAdvanceList";
	}

	//根据条件查询预付款明细
	@RequestMapping("selAdvanceChargeList")
	public String selAdvanceChargeList(@RequestParam Map<String,Object> selChargeMap){
		selChargeMap.put("editStatus","2");
		List<Map<String,Object>> advanceEditList = sellerBillRecAdvanceService.queryAdvanceEditList(selChargeMap);
		model.addAttribute("advanceEditList", advanceEditList);
		model.addAttribute("params", selChargeMap);
		return "platform/sellers/billcycles/billadvance/billAdvanceChargeList";
	}

	//根据条件查询预付款修改记录
	@RequestMapping("queryAdvanceEditList")
	@ResponseBody
	public String queryAdvanceEditList(@RequestParam Map<String,Object> editMap){
		List<Map<String,Object>> advanceEditList = sellerBillRecAdvanceService.queryAdvanceEditList(editMap);
		return JSONObject.toJSONString(advanceEditList);
	}

	/**
	 * 导出预付款数据
	 * @param advanceMap
	 */
	@RequestMapping("sellerAdvanceExport")
	public void sellerAdvanceExport(@RequestParam Map<String,Object> advanceMap){
		advanceMap.put("sellerCompanyId", ShiroUtils.getCompId());
		List<SellerBillReconciliationAdvance> advanceList = sellerBillRecAdvanceService.selAdvanceList(advanceMap);

		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("预付款报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		String[] cellHeadArray ={"账户编号","采购商名称","采购员","可用金额","冻结金额","账户余额","开户日期","备注"};
		// 大标题
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("预付款报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;
		if(null != advanceList && advanceList.size()>0){
			for(SellerBillReconciliationAdvance buyAdvanceInfo : advanceList){
				row=sheet.createRow(rowCount);
				//1.账户编号
				cell = row.createCell(0);
				cell.setCellValue(buyAdvanceInfo.getId());
				cell.setCellStyle(normalStyle);
				//1.供应商
				cell = row.createCell(1);
				cell.setCellValue(buyAdvanceInfo.getBuyCompanyName());
				cell.setCellStyle(normalStyle);
				//2.下单人
				cell = row.createCell(2);
				cell.setCellValue(buyAdvanceInfo.getCreateBillUserName());
				cell.setCellStyle(normalStyle);

				//5.可用金额
				cell = row.createCell(3);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getUsableTotal()));
				cell.setCellStyle(normalStyle);
				//6.锁定金额
				cell = row.createCell(4);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getLockTotal()));
				cell.setCellStyle(normalStyle);
				//7.总金额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(buyAdvanceInfo.getAdvanceTotal()));
				cell.setCellStyle(normalStyle);
				//3.添加日期
				cell = row.createCell(6);
				cell.setCellValue(buyAdvanceInfo.getCreateTimeStr());
				cell.setCellStyle(normalStyle);
				//4.添加备注
				cell = row.createCell(7);
				cell.setCellValue(buyAdvanceInfo.getCreateRemarks());
				cell.setCellStyle(normalStyle);

				rowCount++;
			}
		}
		ExcelUtil.preExport("预付款信息", response);
		ExcelUtil.export(wb, response);
	}
}
