package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.*;
import com.nuotai.trading.seller.dao.buyer.*;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecord;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliation;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliationAdvance;
import com.nuotai.trading.seller.service.buyer.SBuyBillReconciliationService;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class SellerBillReconciliationService {
	@Autowired
	private SellerBillReconciliationMapper sellerBillReconciliationMapper;
	@Autowired
	private SellerBillReconciliationItemMapper sellerBillReconciliationItemMapper;
	@Autowired
	private SBuyBillReconciliationService sBuyBillReconciliationService;//买家账单对账
	@Autowired
	private SBuyBillReconciliationMapper sBuyBillReconciliationMapper;//买家账单对账
	@Autowired
	private SellerDeliveryRecordService sellerDeliveryRecordService;//卖家发货单号
	@Autowired
	private SBuyDeliveryRecordMapper sBuyDeliveryRecordMapper;//买家发货
	@Autowired
	private SellerDeliveryRecordItemService sellerDeliveryRecordItemService;//卖家发货详情
	@Autowired
	private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;
	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;//卖家订单详情
	@Autowired
	private SellerOrderMapper sellerOrderMapper;  //卖家订单信息
	@Autowired
	private SBuyOrderMapper buyOrderMapper;
	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;//卖家售后
	@Autowired
	private SBuyCustomerMapper sBuyCustomerMapper;//买家退或
	@Autowired
	private SellerCustomerItemService sellerCustomerItemService;//卖家退货明细
	@Autowired
	private  SellerCustomerItemMapper sellerCustomerItemMapper;//每家售后明细
	@Autowired
	private SellerBillReconciliationAdvanceMapper sellerBillRecAdvanceMapper;//预付款
	@Autowired
	private SBuyBillReconciliationAdvanceMapper buyBillRecAdvanceMapper;//买家预付款

	// 根据id编号查询账单信息
	public SellerBillReconciliation selectByPrimaryKey(Map<String,Object> map) {
		return sellerBillReconciliationMapper.selectByPrimaryKey(map);
	}

	//根据周期、公司编号查询建单人
	public  JSONObject queryCreateBillUser(Map<String,Object> queryCreateBillUerMap){
		JSONObject json = new JSONObject();
		Map<String, Object> recordMap = new HashMap<String, Object>();
		Map<String, Object> customerMap = new HashMap<String, Object>();
		List<Map<String,Object>> createBillUserList = new ArrayList<>();

		String buyCompanyId = queryCreateBillUerMap.get("companyId").toString();
		String buyCompanyName = queryCreateBillUerMap.get("companyName").toString();
		String sellerCompanyId = ShiroUtils.getCompId();
		String startArrivalDateStr = queryCreateBillUerMap.get("startArrivalDate").toString();
		String endArrivalDateStr = queryCreateBillUerMap.get("endArrivalDate").toString();
		//格式化
		SimpleDateFormat sdfStatement = new SimpleDateFormat("yyyy-MM-dd");
		Date startArrivalDate = new Date();//上个周期终止月
		Date endArrivalDate = new Date();//上个周期起始月
		try {
			startArrivalDate = sdfStatement.parse(startArrivalDateStr);
			endArrivalDate = sdfStatement.parse(endArrivalDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		recordMap.put("buycompanyId", buyCompanyId);
		recordMap.put("companyId", sellerCompanyId);
		//recordMap.put("reconciliationStatusFirst", "0");
		recordMap.put("startArrivalDate", startArrivalDate);
		recordMap.put("endArrivalDate", endArrivalDate);
		List<Map<String,Object>> deliveryUerList = sellerDeliveryRecordItemMapper.queryCreateBillUser(recordMap);
		createBillUserList.addAll(deliveryUerList);//合并集合

		customerMap.put("companyId", buyCompanyId);
		customerMap.put("supplierId", sellerCompanyId);
		//customerMap.put("customerType","1");//0:售后换货，1：售后退货
		//customerMap.put("reconciliationStatusFirst", "0");
		customerMap.put("startArrivalDate", startArrivalDate);
		customerMap.put("endArrivalDate", endArrivalDate);
		List<Map<String,Object>> customerUserList = sellerCustomerMapper.queryCreateBillUser(customerMap);
		createBillUserList.addAll(customerUserList);//合并集合

		//去重
		if(null != createBillUserList && createBillUserList.size()>0){
			List<Map<String,Object>> createBillUserLink = new LinkedList<Map<String,Object>>();
			Set<Map> setMap = new HashSet<Map>();
			for(Map<String,Object> map1:createBillUserList){
				if(setMap.add(map1)){
					createBillUserLink.add(map1);
				}
			}

			//排序
			/*Collections.sort(createBillUserLink, new Comparator<Map<String,Object>>(){
				public int compare(Map<String,Object> o1,Map<String,Object> o2){
					return o1.get("date").toString().compareTo(o2.get("date").toString());
				}
			});*/

			json.put("success", true);
			json.put("msg", "添加对账成功！");
			json.put("createBillUserLink", createBillUserLink);
		}else {
			json.put("error", false);
			json.put("msg", "提示："+buyCompanyName+" 在本周期内没有到货记录，无法发起对账，请重新选择账单周期！");
			json.put("createBillUserLink", "");
		}
		return json;
	}

	//根据周期添加对账信息
	public JSONObject saveReconciliationInfo(Map<String,Object> saveRecmap){
		JSONObject json = new JSONObject();
		//List<SellerDeliveryRecord> deliveryRecordList = new ArrayList<SellerDeliveryRecord>();//发货
		//List<SellerDeliveryRecord> recordReplaceList = new ArrayList<SellerDeliveryRecord>();//发货换货
		//List<SellerCustomer> customerReplaceList = new ArrayList<SellerCustomer>();//换货
		//List<SellerCustomer> customerList = new ArrayList<SellerCustomer>();//退货

		List<SellerDeliveryRecordItem> deliveryRecordItemList = new ArrayList<>();//发货详情
		List<SellerDeliveryRecordItem> deliveryReplaceItemList = new ArrayList<>();//发货换货
		List<SellerCustomerItem> customerReplaceItemList = new ArrayList<SellerCustomerItem>();//换货
		List<SellerCustomerItem> customerItemList = new ArrayList<SellerCustomerItem>();//退货

		Map<String, Object> recordMap = new HashMap<String, Object>();
		Map<String, Object> recordReplaceMap = new HashMap<String, Object>();
		Map<String, Object> customerReplaceMap = new HashMap<String, Object>();
		Map<String, Object> customerMap = new HashMap<String, Object>();

		String buyCompanyId = saveRecmap.get("companyId").toString();
		String sellerCompanyId = ShiroUtils.getCompId();
		String startArrivalDateStr = saveRecmap.get("startArrivalDate").toString();
		String endArrivalDateStr = saveRecmap.get("endArrivalDate").toString();
		String createUserIdStr = "";
		String createUserName = "";

		if(null != saveRecmap.get("createUserId") && !"".equals(saveRecmap.get("createUserId"))){
			createUserIdStr = saveRecmap.get("createUserId").toString();
			createUserName = saveRecmap.get("createUserName").toString();
		}else {
			Map<String, Object> createBillUserMap = (Map<String, Object>) queryCreateBillUser(saveRecmap);
			if(null != createBillUserMap.get("createBillUserLink") && !"".equals(createBillUserMap.get("createBillUserLink"))){
				List<Map<String,Object>> createBillUserLink = (LinkedList<Map<String,Object>>)createBillUserMap.get("createBillUserLink");
				for(Map<String,Object> billUserMap : createBillUserLink){
					Map<String,Object> createBillUserMaps = billUserMap;
					createUserIdStr = createUserIdStr + createBillUserMaps.get("createId")+",";
					createUserName = createUserName + createBillUserMaps.get("createName")+",";
				}
				createUserIdStr = createUserIdStr.substring(0,createUserIdStr.length()-1).trim();
				createUserName = createUserName.substring(0,createUserName.length()-1).trim();
			}
		}

		String[] createUserIdStrList = createUserIdStr.split(",");
		String createUserIds = "";
		if(createUserIdStrList.length > 0){
			createUserIds = createUserIdStr.replaceAll(",","','");
			createUserIds = "'"+createUserIds+"'";
		}else {
			createUserIds = createUserIdStr;
		}

		//格式化
		SimpleDateFormat sdfStatement = new SimpleDateFormat("yyyy-MM-dd");
		Date startArrivalDate = new Date();//上个周期终止月
		Date endArrivalDate = new Date();//上个周期起始月
		try {
			startArrivalDate = sdfStatement.parse(startArrivalDateStr);
			endArrivalDate = sdfStatement.parse(endArrivalDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		//采购发货
		recordMap.put("createUserId", createUserIds);
		recordMap.put("buycompanyId", buyCompanyId);
		recordMap.put("companyId", sellerCompanyId);
		recordMap.put("reconciliationStatusFirst", "0");
		recordMap.put("deliverType", "0");//0:采购发货，1：换货发货
		//recordMap.put("deliverRecord", "deliverRecord");//0:采购收货，1：换货收货
		recordMap.put("startArrivalDate", startArrivalDate);
		recordMap.put("endArrivalDate", endArrivalDate);
		//deliveryRecordList = sellerDeliveryRecordService.queryDeliveryRecordList(recordMap);
		deliveryRecordItemList = sellerDeliveryRecordItemMapper.queryDeliveryRecordItemList(recordMap);

		//换货发货
		/*recordReplaceMap.put("createUserId", createUserIds);
		recordReplaceMap.put("buycompanyId", buyCompanyId);
		recordReplaceMap.put("companyId", sellerCompanyId);
		recordReplaceMap.put("reconciliationStatusFirst", "0");
		recordReplaceMap.put("deliverType", "1");//0:采购发货，1：换货发货
		//recordReplaceMap.put("deliverReplace", "deliverReplace");//0:采购收货，1：换货收货
		recordReplaceMap.put("startArrivalDate", startArrivalDate);
		recordReplaceMap.put("endArrivalDate", endArrivalDate);
		deliveryReplaceItemList = sellerDeliveryRecordItemMapper.queryDeliveryRecordItemList(recordReplaceMap);

		//售后换货
		customerReplaceMap.put("createUserId", createUserIds);
		customerReplaceMap.put("companyId", buyCompanyId);
		customerReplaceMap.put("supplierId", sellerCompanyId);
		customerReplaceMap.put("customerType","0");//0:售后换货，1：售后退货
		customerReplaceMap.put("reconciliationStatusFirst", "0");
		customerReplaceMap.put("startArrivalDate", startArrivalDate);
		customerReplaceMap.put("endArrivalDate", endArrivalDate);
		customerReplaceItemList = sellerCustomerItemMapper.queryCustmerItemList(customerReplaceMap);
		//退货订单
		customerMap.put("createUserId", createUserIds);
		customerMap.put("companyId", buyCompanyId);
		customerMap.put("supplierId", sellerCompanyId);
		customerMap.put("customerType","1");//0:售后换货，1：售后退货
		customerMap.put("reconciliationStatusFirst", "0");
		customerMap.put("startArrivalDate", startArrivalDate);
		customerMap.put("endArrivalDate", endArrivalDate);
		customerItemList = sellerCustomerItemMapper.queryCustmerItemList(customerMap);*/

		if (deliveryRecordItemList.size() > 0 || deliveryReplaceItemList.size() > 0 || customerReplaceItemList.size() > 0 || customerItemList.size() > 0) {
			//添加对账信息
			String recOldId = "";
			int saveRecCount = 0;
			saveRecCount = insertReconciliation(deliveryRecordItemList, deliveryReplaceItemList, customerReplaceItemList, customerItemList
					, buyCompanyId, sellerCompanyId, startArrivalDate, endArrivalDate,recOldId,createUserIdStr,createUserName);
			if(saveRecCount > 0){
				json.put("success", true);
				json.put("msg", "添加对账成功！");
			}else {
				json.put("error", false);
				json.put("msg", "添加对账失败！");
			}

		}else {
			json.put("error", false);
			json.put("msg", "该周期内暂无待对账账单或待对账账单已发起对账！");
		}

		return json;
	}

	//根据周期添加对账信息
	public JSONObject updateRecInfo(Map<String,Object> updateRecmap){
		JSONObject json = new JSONObject();
		//List<SellerDeliveryRecord> deliveryRecordList = new ArrayList<SellerDeliveryRecord>();//发货
		//List<SellerDeliveryRecord> recordReplaceList = new ArrayList<SellerDeliveryRecord>();//发货换货
		//List<SellerCustomer> customerReplaceList = new ArrayList<SellerCustomer>();//换货
		//List<SellerCustomer> customerList = new ArrayList<SellerCustomer>();//退货
		List<SellerDeliveryRecordItem> deliveryRecordItemList = new ArrayList<>();//发货详情
		List<SellerDeliveryRecordItem> deliveryReplaceItemList = new ArrayList<>();//发货换货
		List<SellerCustomerItem> customerReplaceItemList = new ArrayList<SellerCustomerItem>();//换货
		List<SellerCustomerItem> customerItemList = new ArrayList<SellerCustomerItem>();//退货
		Map<String, Object> recordMap = new HashMap<String, Object>();
		Map<String, Object> recordReplaceMap = new HashMap<String, Object>();
		Map<String, Object> customerReplaceMap = new HashMap<String, Object>();
		Map<String, Object> customerMap = new HashMap<String, Object>();

		String recOldId = updateRecmap.get("reconciliationId").toString();
		String buyCompanyId = updateRecmap.get("companyId").toString();
		String sellerCompanyId = ShiroUtils.getCompId();
		String startArrivalDateStr = updateRecmap.get("startArrivalDate").toString();
		String endArrivalDateStr = updateRecmap.get("endArrivalDate").toString();
		String createUserIdStr = "";
		String createUserName = "";

		if(null != updateRecmap.get("createUserId") && !"".equals(updateRecmap.get("createUserId"))){
			createUserIdStr = updateRecmap.get("createUserId").toString();
			createUserName = updateRecmap.get("createUserName").toString();
		}else {
			Map<String, Object> createBillUserMap = (Map<String, Object>) queryCreateBillUser(updateRecmap);
			if(null != createBillUserMap.get("createBillUserLink") && !"".equals(createBillUserMap.get("createBillUserLink"))){
				List<Map<String,Object>> createBillUserLink = (LinkedList<Map<String,Object>>)createBillUserMap.get("createBillUserLink");
				for(Map<String,Object> billUserMap : createBillUserLink){
					Map<String,Object> createBillUserMaps = billUserMap;
					createUserIdStr = createUserIdStr + createBillUserMaps.get("createId")+",";
					createUserName = createUserName + createBillUserMaps.get("createName")+",";
				}
				createUserIdStr = createUserIdStr.substring(0,createUserIdStr.length()-1).trim();
				createUserName = createUserName.substring(0,createUserName.length()-1).trim();
			}
		}

		String[] createUserIdStrList = createUserIdStr.split(",");
		String createUserIds = "";
		if(createUserIdStrList.length > 0){
			createUserIds = createUserIdStr.replaceAll(",","','");
			createUserIds = "'"+createUserIds+"'";
		}else {
			createUserIds = createUserIdStr;
		}

		//格式化
		SimpleDateFormat sdfStatement = new SimpleDateFormat("yyyy-MM-dd");
		Date startArrivalDate = new Date();//上个周期终止月
		Date endArrivalDate = new Date();//上个周期起始月
		try {
			startArrivalDate = sdfStatement.parse(startArrivalDateStr);
			endArrivalDate = sdfStatement.parse(endArrivalDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		//发货订单
		recordMap.put("createUserId", createUserIds);
		recordMap.put("buycompanyId", buyCompanyId);
		recordMap.put("companyId", sellerCompanyId);
		recordMap.put("deliverType", "0");//0:采购收货，1：换货收货
		//recordMap.put("deliverRecord", "deliverRecord");//0:采购收货，1：换货收货
		recordMap.put("startArrivalDate", startArrivalDate);
		recordMap.put("endArrivalDate", endArrivalDate);
		recordMap.put("reconciliationStatusRetrun","3");//驳回
		//deliveryRecordList = sellerDeliveryRecordService.queryDeliveryRecordList(recordMap);
		deliveryRecordItemList = sellerDeliveryRecordItemMapper.queryDeliveryRecordItemList(recordMap);

		//发货换货
		/*recordReplaceMap.put("createUserId", createUserIds);
		recordReplaceMap.put("buycompanyId", buyCompanyId);
		recordReplaceMap.put("companyId", sellerCompanyId);
		recordReplaceMap.put("reconciliationStatusRetrun", "3");
		recordReplaceMap.put("deliverType", "1");//0:采购收货，1：换货收货
		recordReplaceMap.put("startArrivalDate", startArrivalDate);
		recordReplaceMap.put("endArrivalDate", endArrivalDate);
		deliveryReplaceItemList = sellerDeliveryRecordItemMapper.queryDeliveryRecordItemList(recordReplaceMap);
		//退货换货
		customerReplaceMap.put("createUserId", createUserIds);
		customerReplaceMap.put("companyId", buyCompanyId);
		customerReplaceMap.put("supplierId", sellerCompanyId);
		customerReplaceMap.put("customerType","0");//0:售后换货，1：售后退货
		customerReplaceMap.put("startArrivalDate", startArrivalDate);
		customerReplaceMap.put("endArrivalDate", endArrivalDate);
		customerReplaceMap.put("reconciliationStatusRetrun","3");//驳回
		customerReplaceItemList = sellerCustomerItemMapper.queryCustmerItemList(customerReplaceMap);
		//退货
		customerMap.put("createUserId", createUserIds);
		customerMap.put("companyId", buyCompanyId);
		customerMap.put("supplierId", sellerCompanyId);
		customerMap.put("customerType","1");//0:售后换货，1：售后退货 item中type
		customerMap.put("startArrivalDate", startArrivalDate);
		customerMap.put("endArrivalDate", endArrivalDate);
		customerMap.put("reconciliationStatusRetrun","3");//驳回
		customerItemList = sellerCustomerItemMapper.queryCustmerItemList(customerMap);*/


		if (deliveryRecordItemList.size() > 0 || deliveryReplaceItemList.size() > 0 || customerReplaceItemList.size() > 0 || customerItemList.size() > 0) {
			//添加对账信息
			int updateRecCount = 0;
			updateRecCount = insertReconciliation(deliveryRecordItemList, deliveryReplaceItemList, customerReplaceItemList, customerItemList
					, buyCompanyId, sellerCompanyId, startArrivalDate, endArrivalDate,recOldId,createUserIdStr,createUserName);
			if(updateRecCount > 0){
				json.put("success", true);
				json.put("msg", "添加对账成功！");
			}else {
				json.put("error", false);
				json.put("msg", "添加对账失败！");
			}
		}else {
			json.put("error", false);
			json.put("msg", "该周期内暂无待对账账单或待对账账单已发起对账！");
		}
		return json;
	}

	//循环添加
	public int  insertReconciliation(List<SellerDeliveryRecordItem> deliveryRecordList
			,List<SellerDeliveryRecordItem> recordReplaceList, List<SellerCustomerItem> customerReplaceList, List<SellerCustomerItem> customerList
			, String buyCompanyId, String sellerCompanyId, Date startArrivalDate, Date endArrivalDate
			, String recOldId,String createBillUserId,String createBillUserName){
		String billReId = "";
		if(null != recOldId && !"".equals(recOldId)){
			/*Map<String,Object> deleteRecItemMap = new HashMap<String,Object>();
			deleteRecItemMap.put("reconciliationId",recOldId);
			deleteRecItemMap.put("reconciliationDealStatus","4");
			sellerBillReconciliationItemMapper.deleteRecItem(deleteRecItemMap);*/
			//根据账单编号删除关联奖惩信息
			Map<String,Object> deleteCustomMap = new HashMap<String,Object>();
			deleteCustomMap.put("reconciliationId",recOldId);
			sellerBillReconciliationMapper.deleteBillCustom(deleteCustomMap);
			//修改上次驳回关联发货、售后单为待对账状态
			Map<String,Object> recItemMap = new HashMap<String,Object>();
			recItemMap.put("reconciliationId",recOldId);
			List<SellerBillReconciliationItem> recItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
			if(null != recItemList && recItemList.size()>0){
				for(SellerBillReconciliationItem selRecItem: recItemList){
					//同步修改关联发货售后数据
					String billRecType = selRecItem.getBillReconciliationType();//账单类型
					String deliverNo = selRecItem.getDeliverNo();
					if("0".equals(billRecType) || "1".equals(billRecType)){
						Map<String,Object> deliveryRecMap = new HashMap<String,Object>();
						deliveryRecMap.put("recordCode",deliverNo);
						deliveryRecMap.put("reconciliationStatus","0");
						sellerDeliveryRecordService.updateDeliveryByReconciliation(deliveryRecMap);
						sBuyDeliveryRecordMapper.updateDeliveryByReconciliation(deliveryRecMap);
					}else if("2".equals(billRecType) || "3".equals(billRecType)){
						Map<String,Object> custMap = new HashMap<String,Object>();
						custMap.put("recordCode",deliverNo);
						custMap.put("reconciliationStatus","0");
						sellerCustomerMapper.updateCustomerByReconciliation(custMap);
						sBuyCustomerMapper.updateCustomerByReconciliation(custMap);
					}
				}
			}

			billReId = recOldId;
		}else {
			billReId = ShiroUtils.getUid();
		}

		//String billDealStatus = "1";
		BigDecimal advanceDeductTotal = new BigDecimal(0);//预付款抵扣金额
		int recordConfirmCount = 0;//发货总数量
		BigDecimal recordConfirmTotal = new BigDecimal(0);//发货总金额
		int recordReplaceCount = 0;//发货换货总数量
		BigDecimal recordReplaceTotal = new BigDecimal(0);//发货换货总金额
		int customerReplaceCount = 0;//换货总数量
		BigDecimal customerReplaceTotal = new BigDecimal(0);//换货总金额
		int returnGoodsCount = 0;//退回总数量
		BigDecimal returnGoodsTotal = new BigDecimal(0);//退回金额
		//BigDecimal totalAmount = new BigDecimal(0);//总金额

		//List<Map<String, Object>> logisticsMap = new ArrayList<Map<String, Object>>();
		Map<String,Object> freightMap = new HashMap<String,Object>();//运费

		//判断后循环计算订单添加
		if(null != deliveryRecordList && deliveryRecordList.size()>0){
			//计算订单明细
			for(SellerDeliveryRecordItem sellerDeliveryRecordItem : deliveryRecordList){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				String recordId = sellerDeliveryRecordItem.getRecordId();
				String deliveryNo = sellerDeliveryRecordItem.getDeliverNo();
				Date arrvieDate = sellerDeliveryRecordItem.getArrivalDate();

				itemMap.put("recordId",recordId);
				//根据发货编号查询运费
				Map<String,Object> logisMap = new HashMap<String,Object>();
				logisMap.put("deliveryRecordId",recordId);
				Map<String,Object> logMap = sellerDeliveryRecordMapper.queryFreightByRecId(logisMap);
				if(null != logMap && logMap.size()>0){
					String logId = "";
					String frei = "";
					if(null != logMap.get("id")){
						logId = logMap.get("id").toString();
					}
					if(null != logMap.get("freight")){
						frei = logMap.get("freight").toString();
					}
					if(!"".equals(logId)&&!freightMap.containsKey(logId)){
						freightMap.put(logId,frei);
					}
				}

				//List<SellerDeliveryRecordItem> buyDeliveryRecordItemList = sellerDeliveryRecordItemService.queryDeliveryRecordItem(itemMap);
				//int itemAddCount = 0;
				//for(SellerDeliveryRecordItem sellerDeliveryRecordItem : buyDeliveryRecordItemList){
				String createBillId = sellerDeliveryRecordItem.getCreateBillId();
				int arrivalNum = sellerDeliveryRecordItem.getArrivalNum();//数量
				BigDecimal salePrice = sellerDeliveryRecordItem.getSalePrice();//单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额
				BigDecimal addAdvanceSum = new BigDecimal(0);//判断是否是单价修改占位符

				//计算预付款金额
				Map<String,Object> advanceCalculateMap = advanceCalculate("add","",buyCompanyId,createBillId,salePriceSum,addAdvanceSum,billReId);
				BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
				BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
				String advanceEditId = advanceCalculateMap.get("advanceEditId").toString();
				recordConfirmTotal = recordConfirmTotal.add(salePriceSumEnd);
				//recordConfirmTotal = advanceCalculateMap.get("deliveryOrCustomTotal");
				advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
				recordConfirmCount = recordConfirmCount+arrivalNum;//出账总数量

				String billRecType = "1";//订单类型 1 发货，2 换货，3 退货
				int itemAddCount = addRecItemToDelivery(advanceEditId,sellerDeliveryRecordItem, billRecType, billReId,
						recordId,deliveryNo, buyCompanyId,sellerCompanyId, arrvieDate);

				if(itemAddCount > 0){
					//同步修改采购对账状态
					Map<String,Object> deliveryRecMap = new HashMap<String,Object>();
					deliveryRecMap.put("recordCode",deliveryNo);
					deliveryRecMap.put("reconciliationStatus","1");
					sellerDeliveryRecordService.updateDeliveryByReconciliation(deliveryRecMap);
					sBuyDeliveryRecordMapper.updateDeliveryByReconciliation(deliveryRecMap);

					//根据订单号查询订单状态
					SellerOrder sellerOrder = sellerOrderMapper.getAllContent(sellerDeliveryRecordItem.getOrderId());
					//同步修改发货订单对账状态
					if(!ObjectUtil.isEmpty(sellerOrder)){
						if(null != sellerOrder.getOrderCode()&&!"".equals(sellerOrder.getOrderCode())){
							String orderCode = sellerOrder.getOrderCode();//订单号
							Map<String,Object> deliveryOrderMap = new HashMap<String,Object>();
							deliveryOrderMap.put("orderCode",orderCode);
							deliveryOrderMap.put("reconciliationStatus","1");
							sellerOrderMapper.updateOrderBuyBill(deliveryOrderMap);
							buyOrderMapper.updateOrderBuyBill(deliveryOrderMap);
						}
					}
				}
			}
		}
		//判断后循环计算发货换货订单添加
		if(null != recordReplaceList && recordReplaceList.size()>0){
			//计算订单明细
			for(SellerDeliveryRecordItem sellerDeliveryRecordItem : recordReplaceList){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				String recordId = sellerDeliveryRecordItem.getRecordId();
				String deliveryNo = sellerDeliveryRecordItem.getDeliverNo();
				Date arrvieDate = sellerDeliveryRecordItem.getArrivalDate();

				itemMap.put("recordId",recordId);
				//根据发货编号查询运费
				Map<String,Object> logisMap = new HashMap<String,Object>();
				logisMap.put("deliveryRecordId",recordId);
				Map<String,Object> logMap = sellerDeliveryRecordMapper.queryFreightByRecId(logisMap);
				if(null != logMap && logMap.size()>0){
					String logId = "";
					String frei = "";
					if(null != logMap.get("id")){
						logId = logMap.get("id").toString();
					}
					if(null != logMap.get("freight")){
						frei = logMap.get("freight").toString();
					}
					if(!"".equals(logId)&&!freightMap.containsKey(logId)){
						freightMap.put(logId,frei);
					}
				}

				/*List<SellerDeliveryRecordItem> buyDeliveryRecordItemList = sellerDeliveryRecordItemService.queryDeliveryRecordItem(itemMap);
				int itemAddCount = 0;
				for(SellerDeliveryRecordItem sellerDeliveryRecordItem : buyDeliveryRecordItemList){*/
				String createBillId = sellerDeliveryRecordItem.getCreateBillId();
				int arrivalNum = sellerDeliveryRecordItem.getArrivalNum();//数量
				//BigDecimal salePrice = sellerDeliveryRecordItem.getSalePrice();//单价
				BigDecimal exchangePrice = sellerDeliveryRecordItem.getExchangePrice();//返修单价
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = exchangePrice.multiply(arrivalNumDecimal);//收获金额
				BigDecimal addAdvanceSum = new BigDecimal(0);//判断是否是单价修改占位符

				//计算预付款金额
				/*Map<String,BigDecimal> advanceCalculateMap = advanceCalculate("add",createBillId,salePriceSum,addAdvanceSum,billReId);
				BigDecimal salePriceSumEnd = advanceCalculateMap.get("salePriceSumEnd");
				BigDecimal advanceDeductEnd = advanceCalculateMap.get("advanceDeductTotal");*/

				Map<String,Object> advanceCalculateMap = advanceCalculate("add","",buyCompanyId,createBillId,salePriceSum,addAdvanceSum,billReId);
				BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
				BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
				String advanceEditId = advanceCalculateMap.get("advanceEditId").toString();

				recordReplaceTotal = recordReplaceTotal.add(salePriceSumEnd);
				advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
				recordReplaceCount = recordReplaceCount+arrivalNum;//出账总数量

				String billRecType = "0";//订单类型 0 发货换货 1 发货订单，2 换货，3 退货
				int itemAddCount = addRecItemToDelivery(advanceEditId,sellerDeliveryRecordItem, billRecType, billReId,
						recordId,deliveryNo, buyCompanyId,sellerCompanyId, arrvieDate);
				//}
				if(itemAddCount > 0){
					Map<String,Object> deliveryRecMap = new HashMap<String,Object>();
					deliveryRecMap.put("recordCode",deliveryNo);
					deliveryRecMap.put("reconciliationStatus","1");
					sellerDeliveryRecordService.updateDeliveryByReconciliation(deliveryRecMap);
					sBuyDeliveryRecordMapper.updateDeliveryByReconciliation(deliveryRecMap);

					//根据订单号查询订单状态
					SellerOrder sellerOrder = sellerOrderMapper.getAllContent(sellerDeliveryRecordItem.getOrderId());
					//同步修改发货订单对账状态
					if(!ObjectUtil.isEmpty(sellerOrder)){
						if(null != sellerOrder.getOrderCode()&&!"".equals(sellerOrder.getOrderCode())){
							String orderCode = sellerOrder.getOrderCode();//订单号
							Map<String,Object> deliveryOrderMap = new HashMap<String,Object>();
							deliveryOrderMap.put("orderCode",orderCode);
							deliveryOrderMap.put("reconciliationStatus","1");
							sellerOrderMapper.updateOrderBuyBill(deliveryOrderMap);
							buyOrderMapper.updateOrderBuyBill(deliveryOrderMap);
						}
					}
				}
			}
		}
		if(null != customerReplaceList && customerReplaceList.size()>0){
			//换货明细
			for(SellerCustomerItem sellerCustomerItem : customerReplaceList){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				String createName = sellerCustomerItem.getCreateName();//售后创建人
				String createId = sellerCustomerItem.getCreateId();//创建人编号

				String customerId = sellerCustomerItem.getCustomerId();
				String customerCode = sellerCustomerItem.getCustomerCode();
				//String proof = customerReplaceRe.getProof();
				Date updateDate = sellerCustomerItem.getUpdateDate();
				//itemMap.put("customerId",customerId);
				// 根据发货单号查询对账详情中是否已经存在
				/*int recItemCount = buyBillReconciliationItemMapper.queryRecItemCount(itemMap);
				if(recItemCount == 0){*/
				/*List<SellerCustomerItem> sellerCustomerItemList = sellerCustomerItemService.queryCustomerItem(itemMap);
				int itemAddCount = 0;
				for(SellerCustomerItem sellerCustomerItem : sellerCustomerItemList){*/
				int goodsNum = 0;//数量
				if(null != sellerCustomerItem.getConfirmNumber() && !"".equals(sellerCustomerItem.getConfirmNumber())){
					goodsNum = sellerCustomerItem.getConfirmNumber();//数量
				}

				//BigDecimal repairPrice = sellerCustomerItem.getRepairPrice();//单价
				BigDecimal repairPrice = sellerCustomerItem.getPrice();//单价
				BigDecimal goodsNumDecimal = new BigDecimal(goodsNum);//转换类型
				BigDecimal repairPriceSum = repairPrice.multiply(goodsNumDecimal);//退货金额

				customerReplaceTotal = customerReplaceTotal.add(repairPriceSum);
				customerReplaceCount = customerReplaceCount+goodsNum;//退货总数量

				String billRecType = "2";//订单类型 1 发货，2 换货，3 退货
				int itemAddCount = addRecItemToCustomer(sellerCustomerItem, billRecType, billReId,
						customerId,customerCode,buyCompanyId,sellerCompanyId, updateDate,createName,createId);
				//}
				if(itemAddCount >0){
					Map<String,Object> custMap = new HashMap<String,Object>();
					custMap.put("recordCode",customerCode);
					custMap.put("reconciliationStatus","1");
					sellerCustomerMapper.updateCustomerByReconciliation(custMap);
					sBuyCustomerMapper.updateCustomerByReconciliation(custMap);
				}
			}
		}
		if(null != customerList && customerList.size()>0){
			for(SellerCustomerItem sellerCustomerItem : customerList){
				Map<String,Object> itemMap = new HashMap<String,Object>();
				String createName = sellerCustomerItem.getCreateName();//售后创建人
				String createId = sellerCustomerItem.getCreateId();//创建人编号
				String customerId = sellerCustomerItem.getCustomerId();
				String customerCode = sellerCustomerItem.getCustomerCode();
				//String proof = sellerCustomer.getProof();
				Date updateDate = sellerCustomerItem.getUpdateDate();
				//itemMap.put("customerId",customerId);
				// 根据发货单号查询对账详情中是否已经存在
				/*int recItemCount = buyBillReconciliationItemMapper.queryRecItemCount(itemMap);
				if(recItemCount == 0){*/
				/*List<SellerCustomerItem> sellerCustomerItemList = sellerCustomerItemService.queryCustomerItem(itemMap);
				int addReturnItemCount = 0;
				for(SellerCustomerItem sellerCustomerItem : sellerCustomerItemList){*/
				//int goodsNum = sellerCustomerItem.getGoodsNumber();//数量
				int goodsNum = 0;//数量
				if(null != sellerCustomerItem.getConfirmNumber() && !"".equals(sellerCustomerItem.getConfirmNumber())){
					goodsNum = sellerCustomerItem.getConfirmNumber();//数量
				}
				BigDecimal goodsPrice = sellerCustomerItem.getPrice();//单价
				BigDecimal goodsNumDecimal = new BigDecimal(goodsNum);//转换类型
				BigDecimal goodsPriceSum = goodsPrice.multiply(goodsNumDecimal);//退货金额

				returnGoodsTotal = returnGoodsTotal.add(goodsPriceSum);
				returnGoodsCount = returnGoodsCount+goodsNum;//退货总数量

				String billRecType = "3";//订单类型 1 发货，2 换货，3 退货
				int addReturnItemCount = addRecItemToCustomer(sellerCustomerItem, billRecType, billReId,
						customerId,customerCode,buyCompanyId,sellerCompanyId, updateDate,createName,createId);
				//}
				if(addReturnItemCount > 0){
					Map<String,Object> custMap = new HashMap<String,Object>();
					custMap.put("recordCode",customerCode);
					custMap.put("reconciliationStatus","1");
					sellerCustomerMapper.updateCustomerByReconciliation(custMap);
					sBuyCustomerMapper.updateCustomerByReconciliation(custMap);
				}
			}
		}

		BigDecimal freightSum = new BigDecimal(0);
		if(null != freightMap&&freightMap.size()>0){
			Collection values = freightMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				//System.out.println("键值：" + object.toString()); //输出键值对象
				if(null != object&&!"".equals(object.toString())){
					String frei = object.toString();
					BigDecimal freight = new BigDecimal(frei);
					freightSum = freightSum.add(freight);
				}
			}
		}


		//根据对账编号及开票与否查询是否有需要开票的订单(如果没有直接付款，有先开票)
		Map<String,Object> itemIsNoInvoiceMap = new HashMap<String,Object>();
		itemIsNoInvoiceMap.put("reconciliationId",billReId);
		itemIsNoInvoiceMap.put("isNeedInvoice","Y");
		int recItemCount = sellerBillReconciliationItemMapper.queryRecItemCount(itemIsNoInvoiceMap);
		//判断后循环计算退货明细？？？？？（需要补充）
		//int totalSumCount = recordConfirmCount + recordReplaceCount + returnGoodsCount;//总数量
		BigDecimal totalAmount = recordConfirmTotal.add(recordReplaceTotal).subtract(returnGoodsTotal).subtract(customerReplaceTotal).add(freightSum);//总金额

		SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
		sellerBillReconciliation.setId(billReId);
		sellerBillReconciliation.setPaymentNo(ShiroUtils.getUid());
		sellerBillReconciliation.setCompanyId(sellerCompanyId);
		sellerBillReconciliation.setBuyCompanyId(buyCompanyId);
		sellerBillReconciliation.setCreateBillUserId(createBillUserId);
		sellerBillReconciliation.setCreateBillUserName(createBillUserName);
		sellerBillReconciliation.setStartBillStatementDate(startArrivalDate);
		sellerBillReconciliation.setEndBillStatementDate(endArrivalDate);
		sellerBillReconciliation.setRecordConfirmCount(recordConfirmCount);
		sellerBillReconciliation.setRecordConfirmTotal(recordConfirmTotal);
		sellerBillReconciliation.setRecordReplaceCount(recordReplaceCount);
		sellerBillReconciliation.setRecordReplaceTotal(recordReplaceTotal);
		sellerBillReconciliation.setReplaceConfirmCount(customerReplaceCount);
		sellerBillReconciliation.setReplaceConfirmTotal(customerReplaceTotal);
		sellerBillReconciliation.setFreightSumCount(freightSum);
		sellerBillReconciliation.setReturnGoodsCount(returnGoodsCount);
		sellerBillReconciliation.setReturnGoodsTotal(returnGoodsTotal);
		//buyBillReconciliation.setTotalSumCount(totalSumCount);
		sellerBillReconciliation.setAdvanceDeductTotal(advanceDeductTotal);
		sellerBillReconciliation.setTotalAmount(totalAmount);
		sellerBillReconciliation.setResidualPaymentAmount(totalAmount);
		sellerBillReconciliation.setBillDealStatus("1");
		if(recItemCount == 0){
			sellerBillReconciliation.setPaymentStatus("4");//内部待审批
		}else {
			sellerBillReconciliation.setPaymentStatus("3");
		}

		//添加账单数据
		int saveRecCount = 0;
		if(null != recOldId && !"".equals(recOldId)){
			//格式化
			SimpleDateFormat sdfStatement = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date nowDateTime = new Date();//上个周期终止月
			String date = sdfStatement.format(new Date());

			try {
				//nowDateTime = sdfStatement.format(nowDateTime);
				nowDateTime = sdfStatement.parse(date);
			}catch (ParseException e){
				e.printStackTrace();
			}
			sellerBillReconciliation.setCreateDate(nowDateTime);
			BigDecimal customAmount = new BigDecimal(0);
			sellerBillReconciliation.setCustomAmount(customAmount);
			saveRecCount = sellerBillReconciliationMapper.updateByPrimaryKeySelective(sellerBillReconciliation);
		}else {
			saveRecCount = sellerBillReconciliationMapper.saveBillReconciliationInfo(sellerBillReconciliation);
		}
		return saveRecCount;
	}

	//预付款计算
	public Map<String,Object> advanceCalculate(String playType,String advanceEditIdOld,String buyCompanyId,String createBillId
			,BigDecimal salePriceSum,BigDecimal billRecTotalOldSum,String reconciliationId){
		Map<String,Object> advanceRetMap = new HashMap<>();
		String advanceEditId ="";
		BigDecimal updateTotalOld = new BigDecimal(0);
		if(null != advanceEditIdOld && !"".equals(advanceEditIdOld)){
			advanceEditId = advanceEditIdOld;
			//预付款详情查询
			Map<String,Object> advanceEditMap = new HashMap<String,Object>();
			advanceEditMap.put("advanceEditId",advanceEditIdOld);
			Map<String,Object> advanceEditInfo = sellerBillRecAdvanceMapper.queryAdvanceEditInfo(advanceEditMap);
			if(null != advanceEditInfo){
				if(null != advanceEditInfo.get("updateTotal") && !"".equals(advanceEditInfo.get("updateTotal").toString())){
					updateTotalOld = new BigDecimal(advanceEditInfo.get("updateTotal").toString());
				}
			}
		}else {
			advanceEditId = ShiroUtils.getUid();
		}
		BigDecimal advanceDeductTotal = new BigDecimal(0);//预付款抵扣金额
		//BigDecimal deliveryOrCustomTotal = new BigDecimal(0);//发货或售后订单金额
		BigDecimal salePriceSumEnd = new BigDecimal(0);//最终
		//预付款查询
		Map<String,Object> advanceIsOrNoMap = new HashMap<String,Object>();
		advanceIsOrNoMap.put("sellerCompanyId",ShiroUtils.getCompId());
		advanceIsOrNoMap.put("createBillUserId",createBillId);

		int isOrNoCount = sellerBillRecAdvanceMapper.isOrNoBillUser(advanceIsOrNoMap);
		if(isOrNoCount > 0){
			//预付款查询
			Map<String,Object> advanceMap = new HashMap<String,Object>();
			advanceMap.put("sellerCompanyId",ShiroUtils.getCompId());
			advanceMap.put("createBillUserId",createBillId);
			SellerBillReconciliationAdvance sellerBillRecAdvance = sellerBillRecAdvanceMapper.queryAdvanceBuyMap(advanceMap);
			String advanceId = sellerBillRecAdvance.getId();
			BigDecimal usableTotalOld = sellerBillRecAdvance.getUsableTotal();
			BigDecimal lockTotalOld = sellerBillRecAdvance.getLockTotal();

			BigDecimal usabilTotalNew = new BigDecimal(0);
			BigDecimal lockTotalNew = new BigDecimal(0);
			BigDecimal updateTotal = new BigDecimal(0);//本次更新的金额

			int salePriceSumCount = salePriceSum.compareTo(new BigDecimal(0));//判断添加或修改金额大小
			if(salePriceSumCount >= 0){//添加或修改金额大于0
				int bigOrSmall = usableTotalOld.compareTo(salePriceSum);
				if(bigOrSmall >= 0){//可用金额大于需抵扣的金额
					//salePriceSumEnd = salePriceSum;
					lockTotalNew = lockTotalOld.add(salePriceSum);
					usabilTotalNew = usableTotalOld.subtract(salePriceSum);
					updateTotal = updateTotalOld.add(salePriceSum);
					advanceDeductTotal = salePriceSum;
				}
				if(bigOrSmall < 0){//可用金额小于需抵扣的金额
					salePriceSumEnd = salePriceSum.subtract(usableTotalOld);
					lockTotalNew = lockTotalOld.add(usableTotalOld);
					updateTotal = updateTotalOld.add(usableTotalOld);
					advanceDeductTotal = usableTotalOld;
				}
			}
			if(salePriceSumCount < 0){//添加或修改金额小于0
				BigDecimal billRecNeedSum = billRecTotalOldSum.add(salePriceSum);//修改后的金额
				//int lockWithNeedSumCount = lockTotalOld.compareTo(billRecNeedSum);//锁定金额对比修改后金额
				int lockWithNeedSumCount = updateTotalOld.compareTo(billRecNeedSum);//该单锁定金额对比修改后金额
				if(lockWithNeedSumCount >= 0){//锁定金额大于等于修改后金额
					salePriceSumEnd = salePriceSum.add(updateTotalOld).subtract(billRecNeedSum);
					usabilTotalNew = usableTotalOld.add(updateTotalOld).subtract(billRecNeedSum);
					lockTotalNew = lockTotalOld.add(billRecNeedSum).subtract(updateTotalOld);
					updateTotal = billRecNeedSum;
					advanceDeductTotal = billRecNeedSum.subtract(updateTotalOld);
				}
				if(lockWithNeedSumCount < 0){//锁定金额小于修改后金额
					BigDecimal needSubLockSum = billRecNeedSum.subtract(updateTotalOld);//仍需抵扣金额（修改后金额减去锁定金额）
					int usaWithNeedLokCount = usableTotalOld.compareTo(needSubLockSum);
					if(usaWithNeedLokCount >= 0){//可用金额大于需抵扣的金额
						salePriceSumEnd = salePriceSum.add(usableTotalOld).subtract(needSubLockSum);
						usabilTotalNew = usableTotalOld.subtract(needSubLockSum);
						lockTotalNew = lockTotalOld.add(needSubLockSum);
						updateTotal = updateTotalOld.subtract(needSubLockSum);
						advanceDeductTotal = needSubLockSum;
					}
					if(usaWithNeedLokCount < 0){//可用金额小于需抵扣的金额
						//salePriceSumEnd = salePriceSum.subtract(usableTotalOld).add(needSubLockSum);
						salePriceSumEnd = salePriceSum.subtract(usableTotalOld);
						//usabilTotalNew = usableTotalOld.subtract(needSubLockSum);
						lockTotalNew = lockTotalOld.add(usableTotalOld);
						updateTotal = updateTotalOld.add(usableTotalOld);
						advanceDeductTotal = usableTotalOld;
					}
				}
			}

			//添加/修改预付款修改记录
			int saveOrUpCount = saveOrUpdateAdvaceEdit(playType,advanceEditId,reconciliationId,advanceId
					,updateTotal,createBillId,sellerBillRecAdvance.getCreateBillUserName()
			        ,usabilTotalNew,lockTotalNew);

			//---------------------------------------分割线------------------------------
			if(saveOrUpCount > 0){
				Map<String,Object> recItemMap = new HashMap<String,Object>();
				recItemMap.put("reconciliationId",reconciliationId);
				recItemMap.put("buyCompanyId",buyCompanyId);
				recItemMap.put("sellerCompanyId",ShiroUtils.getCompId());
				recItemMap.put("reconciliationDealStatus","1");
				List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
				if(null != billRecItemList && billRecItemList.size()>0 && !billRecItemList.isEmpty()){
					for(SellerBillReconciliationItem sellerRecInfo : billRecItemList){
						String advanceEditIdByRecItem = sellerRecInfo.getAdvanceEditId();
						if(!advanceEditId.equals(advanceEditIdByRecItem)){
							String createBillId2 = sellerRecInfo.getCreateBillId();
							String buyCompanyId2 = sellerRecInfo.getBuyCompanyId();
							//预付款详情查询
							Map<String,Object> advanceEditMap2 = new HashMap<String,Object>();
							advanceEditMap2.put("advanceEditId",advanceEditIdByRecItem);
							Map<String,Object> advanceEditInfo2 = sellerBillRecAdvanceMapper.queryAdvanceEditInfo(advanceEditMap2);
							if(null != advanceEditInfo2 && !advanceEditInfo2.isEmpty()){//判断不为空
								if(null != advanceEditInfo2.get("updateTotal") && !"".equals(advanceEditInfo2.get("updateTotal").toString())){
									BigDecimal updateTotalOld2 = new BigDecimal(advanceEditInfo2.get("updateTotal").toString());
									String advanceId2 = advanceEditInfo2.get("advanceId").toString();

									//预付款账户查询
									Map<String,Object> advanceMap2 = new HashMap<String,Object>();
									advanceMap2.put("sellerCompanyId",ShiroUtils.getCompId());
									advanceMap2.put("buyCompanyId",buyCompanyId2);
									advanceMap2.put("createBillUserId",createBillId2);
									SellerBillReconciliationAdvance sellerBillRecAdvance2 = sellerBillRecAdvanceMapper.queryAdvanceBuyMap(advanceMap2);
									BigDecimal usableTotalOld2 = sellerBillRecAdvance2.getUsableTotal();
									BigDecimal lockTotalOld2 = sellerBillRecAdvance2.getLockTotal();
									int usaIsZero = usableTotalOld2.compareTo(new BigDecimal(0));//判断可用余额是否大于0
									if(usaIsZero > 0){
										String isUpdateSale = sellerRecInfo.getIsUpdateSale();
										//String advanceEditOldId2 = sellerRecInfo.getAdvanceEditId();//获取充值编号
										int arriveNum2 = sellerRecInfo.getArrivalNum();//订单数量multiply
										BigDecimal updateSalePrice2 = new BigDecimal(0);//对账单价
										if(!"1".equals(isUpdateSale)){
											updateSalePrice2 = sellerRecInfo.getSalePrice();
										}else {
											updateSalePrice2 = sellerRecInfo.getUpdateSalePrice();
										}
										BigDecimal updateSaleTotal2 = updateSalePrice2.multiply(new BigDecimal(arriveNum2));//仍需抵扣的金额

										BigDecimal usaTotalSt = usableTotalOld2.add(updateTotalOld2);
										int usaBillTotalCount2 = usaTotalSt.compareTo(updateSaleTotal2);//可用余额对比需抵扣金额

										BigDecimal usabilTotalNew2 = new BigDecimal(0);
										BigDecimal lockTotalNew2 = new BigDecimal(0);
										BigDecimal updateTotal2 = new BigDecimal(0);//本次更新的金额
										String playType2 = "";

										String advanceEditId2 = "";
										if(usaBillTotalCount2 >= 0){
											salePriceSumEnd = salePriceSumEnd.subtract(updateSaleTotal2).add(updateTotalOld2);
											//salePriceSumEnd = salePriceSumEnd.subtract(updateSaleTotal2);
											lockTotalNew2 = lockTotalOld2.add(updateSaleTotal2).subtract(updateTotalOld2);
											usabilTotalNew2 = usaTotalSt.subtract(updateSaleTotal2);
											if(null != advanceEditInfo2 && !advanceEditInfo2.isEmpty()){//isEmpty()方法判断Map是否有内容（即new分配空间后是否put键值对），若没有内容则true，否则false
												advanceEditId2 = advanceEditInfo2.get("id").toString();//获取充值编号
												updateTotal2 = updateSaleTotal2;//需抵扣加上历史
												playType2 = "update";
											}else {
												advanceEditId2 = ShiroUtils.getUid();//生成充值编号
												updateTotal2 = updateSaleTotal2;
												playType2 = "add";
											}
											//salePriceSumEnd = salePriceSumEnd.subtract(updateSaleTotal2);
											advanceDeductTotal = advanceDeductTotal.subtract(updateTotalOld2).add(updateSaleTotal2);
										}
										if(usaBillTotalCount2 < 0){
											salePriceSumEnd = salePriceSumEnd.subtract(usableTotalOld2);
											lockTotalNew2 = lockTotalOld2.add(usableTotalOld2);
											if(null != advanceEditInfo2 && !advanceEditInfo2.isEmpty()){//isEmpty()方法判断Map是否有内容（即new分配空间后是否put键值对），若没有内容则true，否则false
												advanceEditId2 = advanceEditInfo2.get("id").toString();//获取充值编号
												updateTotal2 = usaTotalSt;//需抵扣加上历史
												playType2 = "update";
											}else {
												advanceEditId2 = ShiroUtils.getUid();//生成充值编号
												updateTotal2 = usaTotalSt;
												playType2 = "add";
											}
											advanceDeductTotal = advanceDeductTotal.add(usableTotalOld2);
										}
										//添加/修改预付款修改记录
										saveOrUpdateAdvaceEdit(playType2,advanceEditId2,reconciliationId,advanceId2
												,updateTotal2,createBillId2,sellerBillRecAdvance2.getCreateBillUserName()
												,usabilTotalNew2,lockTotalNew2);

									}
								}
							}
						}
					}
				}
			}
		}else {
			salePriceSumEnd = salePriceSum;
		}
		//拼装返回
		advanceRetMap.put("salePriceSumEnd",salePriceSumEnd);
		advanceRetMap.put("advanceDeductTotal",advanceDeductTotal);
		advanceRetMap.put("advanceEditId",advanceEditId);

		return advanceRetMap;
	}

	//更新或添加充值流水
	public int saveOrUpdateAdvaceEdit(String playType,String advanceEditId,String reconciliationId
			,String advanceId,BigDecimal updateTotal,String createBillId,String createBillName
	        ,BigDecimal usabilTotalNew,BigDecimal lockTotalNew){
		//更新预付款表
		SellerBillReconciliationAdvance sellerRecAdvance = new SellerBillReconciliationAdvance();
		sellerRecAdvance.setId(advanceId);
		sellerRecAdvance.setUsableTotal(usabilTotalNew);
		sellerRecAdvance.setLockTotal(lockTotalNew);
		int updateCount = sellerBillRecAdvanceMapper.updateAdvance(sellerRecAdvance);
		//同步更新买家预付款
		SBuyBillReconciliationAdvance buyBillRecAdvance = new SBuyBillReconciliationAdvance();
		buyBillRecAdvance.setId(advanceId);
		buyBillRecAdvance.setUsableTotal(usabilTotalNew);
		buyBillRecAdvance.setLockTotal(lockTotalNew);
		buyBillRecAdvanceMapper.updateAdvanceInfo(buyBillRecAdvance);

		int saveOrUpCount = 0;
		if("add".equals(playType)){
			Map<String,Object> addAdvanceEditMap = new HashMap<String,Object>();
			addAdvanceEditMap.put("id",advanceEditId);
			addAdvanceEditMap.put("reconciliationId",reconciliationId);
			addAdvanceEditMap.put("advanceId",advanceId);
			addAdvanceEditMap.put("updateTotal",updateTotal);
			addAdvanceEditMap.put("createUserId",ShiroUtils.getUserId());
			addAdvanceEditMap.put("createUserName",ShiroUtils.getUserName());
			addAdvanceEditMap.put("createBillUserId",createBillId);
			addAdvanceEditMap.put("createBillUserName",createBillName);
			addAdvanceEditMap.put("editStatus","2");
			saveOrUpCount = sellerBillRecAdvanceMapper.addAdvanceEdit(addAdvanceEditMap);
		}
		if("update".equals(playType)){
			Map<String,Object> addAdvanceEditMap = new HashMap<String,Object>();
			addAdvanceEditMap.put("id",advanceEditId);
			addAdvanceEditMap.put("reconciliationId",reconciliationId);
			addAdvanceEditMap.put("updateTotal",updateTotal);
			addAdvanceEditMap.put("createBillUserId",createBillId);
			saveOrUpCount = sellerBillRecAdvanceMapper.updateAdvanceEdit(addAdvanceEditMap);
		}
		return saveOrUpCount;
	}
	//循环添加发货关联表
	public int addRecItemToDelivery(String advanceEditId,SellerDeliveryRecordItem sellerDeliveryRecordItem,
									String billRecType,String billReId,String deliveryRecordId,String deliveryNo,String buyCompanyId,
									String sellerCompanyId,Date arrvieDate){
		//根据发货编号查询运费
		Map<String,Object> logisMap = new HashMap<String,Object>();
		logisMap.put("deliveryRecordId",deliveryRecordId);
		Map<String,Object> logisticsMap = sellerDeliveryRecordMapper.queryFreightByRecId(logisMap);
		String logisticsId = "";
		String waybillNo = "";
		String logisticsCompany = "";
		String driverName = "";
		BigDecimal freight = new BigDecimal(0);
		if(null != logisticsMap && logisticsMap.size() >0){
			if(null != logisticsMap.get("id")){
				logisticsId = logisticsMap.get("id").toString();
			}
			if(null != logisticsMap.get("waybillNo")){
				waybillNo = logisticsMap.get("waybillNo").toString();
			}
			if(null != logisticsMap.get("logisticsCompany")){
				logisticsCompany = logisticsMap.get("logisticsCompany").toString();
			}
			if(null != logisticsMap.get("driverName")){
				driverName = logisticsMap.get("driverName").toString();
			}
			if(null != logisticsMap.get("freight")){
				freight =new BigDecimal(logisticsMap.get("freight").toString());
			}
		}

		//循环添加对账详情
		//String billRecType = "1";//订单类型 1 发货，2 换货，3 退货
		String createBillId = sellerDeliveryRecordItem.getCreateBillId();//下单人编号
		String createBillName = sellerDeliveryRecordItem.getCreateBillName();//下单人名称
		String isNeedInvoice = sellerDeliveryRecordItem.getIsNeedInvoice();//是否需要发票
		String recordItemId = sellerDeliveryRecordItem.getId();//发货详情id
		String proCode = sellerDeliveryRecordItem.getProductCode();//货号
		String proName = sellerDeliveryRecordItem.getProductName();//商品名称
		String orderId = sellerDeliveryRecordItem.getOrderId();//订单id
		//String orderCode = buyDeliveryRecordItem.getOrderCode();//订单号（外部）
		String skuCode = sellerDeliveryRecordItem.getSkuCode();//商品规格代码
		String skuName = sellerDeliveryRecordItem.getSkuName();//规格名称
		String barcode = sellerDeliveryRecordItem.getBarcode();//条形码
		String unitId = sellerDeliveryRecordItem.getUnitId();//单位
		//Date arrivalDate = buyDeliveryRecordItem.getArrivalDate();//到货时间
		int arrivalNum = sellerDeliveryRecordItem.getArrivalNum();//数量
		BigDecimal salePrice = new BigDecimal(0);//单价
		if("0".equals(billRecType)){
			salePrice = sellerDeliveryRecordItem.getExchangePrice();//单价
		}else if("1".equals(billRecType)){
			salePrice = sellerDeliveryRecordItem.getSalePrice();//单价
		}

		//开始拼装
		SellerBillReconciliationItem buyBillReconciliationItem = new SellerBillReconciliationItem();
		String billReItemId = ShiroUtils.getUid();
		buyBillReconciliationItem.setId(billReItemId);
		buyBillReconciliationItem.setAdvanceEditId(advanceEditId);
		buyBillReconciliationItem.setBuyCompanyId(buyCompanyId);
		buyBillReconciliationItem.setSellerCompanyId(sellerCompanyId);
		buyBillReconciliationItem.setCreateBillId(createBillId);
		buyBillReconciliationItem.setCreateBillName(createBillName);

		buyBillReconciliationItem.setReconciliationId(billReId);
		buyBillReconciliationItem.setRecordId(deliveryRecordId);
		buyBillReconciliationItem.setDeliverNo(deliveryNo);
		buyBillReconciliationItem.setItemId(recordItemId);
		buyBillReconciliationItem.setOrderId(orderId);
		buyBillReconciliationItem.setBillReconciliationType(billRecType);
		buyBillReconciliationItem.setIsNeedInvoice(isNeedInvoice);
		buyBillReconciliationItem.setProductCode(proCode);
		buyBillReconciliationItem.setProductName(proName);
		buyBillReconciliationItem.setSkuCode(skuCode);
		buyBillReconciliationItem.setSkuName(skuName);
		buyBillReconciliationItem.setBarcode(barcode);
		buyBillReconciliationItem.setUnitId(unitId);
		buyBillReconciliationItem.setArrivalDate(arrvieDate);
		buyBillReconciliationItem.setArrivalNum(arrivalNum);
		buyBillReconciliationItem.setLogisticsId(logisticsId);
		buyBillReconciliationItem.setWaybillNo(waybillNo);
		buyBillReconciliationItem.setLogisticsCompany(logisticsCompany);
		buyBillReconciliationItem.setDriverName(driverName);

		buyBillReconciliationItem.setFreight(freight);
		buyBillReconciliationItem.setSalePrice(salePrice);
		buyBillReconciliationItem.setIsInvoiceStatus("0");
		buyBillReconciliationItem.setReconciliationDealStatus("1");

		int addItemCount = sellerBillReconciliationItemMapper.addRecItem(buyBillReconciliationItem);
		return addItemCount;
	}

	//循环添加退货关联表
	public int addRecItemToCustomer(SellerCustomerItem sellerCustomerItem,
									String billRecType,String billReId,String customerId,String customerCode,
									String buyCompanyId, String sellerCompanyId,Date updateDate,
									String createBillName,String createBillId){
		//循环添加对账详情
		//String billRecType = "1";//订单类型 1 发货，2 换货，3 退货
		//String isNeedInvoice = buyCustomerItem.//是否需要发票
		String recordItemId = sellerCustomerItem.getId();//发货详情id
		String proCode = sellerCustomerItem.getProductCode();//货号
		String proName = sellerCustomerItem.getProductName();//商品名称
		//String orderId = buyDeliveryRecordItem.getOrderId();//订单id
		//String orderCode = buyDeliveryRecordItem.getOrderCode();//订单号（外部）
		String skuCode = sellerCustomerItem.getSkuCode();//商品规格代码
		String skuName = sellerCustomerItem.getSkuName();//规格名称
		String barcode = sellerCustomerItem.getSkuOid();//条形码
		String unitId = sellerCustomerItem.getUnitId();//单位
		//Date arrivalDate = buyDeliveryRecordItem.getArrivalDate();//到货时间
		//int arrivalNum = sellerCustomerItem.getConfirmNumber();//数量
		int arrivalNum = 0;//数量
		if(null != sellerCustomerItem.getConfirmNumber() && !"".equals(sellerCustomerItem.getConfirmNumber())){
			arrivalNum = sellerCustomerItem.getConfirmNumber();//数量
		}
		BigDecimal salePrice = new BigDecimal(0);//单价
		/*if("2".equals(billRecType)){
			salePrice = sellerCustomerItem.getRepairPrice();
		}else if("3".equals(billRecType)){
			salePrice = sellerCustomerItem.getPrice();
		}*/
		salePrice = sellerCustomerItem.getPrice();
		//开始拼装
		SellerBillReconciliationItem buyBillReconciliationItem = new SellerBillReconciliationItem();
		String billReItemId = ShiroUtils.getUid();
		buyBillReconciliationItem.setId(billReItemId);
		buyBillReconciliationItem.setBuyCompanyId(buyCompanyId);
		buyBillReconciliationItem.setSellerCompanyId(sellerCompanyId);
		buyBillReconciliationItem.setCreateBillId(createBillId);
		buyBillReconciliationItem.setCreateBillName(createBillName);

		buyBillReconciliationItem.setReconciliationId(billReId);
		buyBillReconciliationItem.setRecordId(customerId);
		buyBillReconciliationItem.setItemId(recordItemId);
		buyBillReconciliationItem.setDeliverNo(customerCode);
		buyBillReconciliationItem.setBillReconciliationType(billRecType);
		//buyBillReconciliationItem.setIsNeedInvoice(isNeedInvoice);
		buyBillReconciliationItem.setProductCode(proCode);
		buyBillReconciliationItem.setProductName(proName);
		buyBillReconciliationItem.setSkuCode(skuCode);
		buyBillReconciliationItem.setSkuName(skuName);
		buyBillReconciliationItem.setBarcode(barcode);
		buyBillReconciliationItem.setUnitId(unitId);
		buyBillReconciliationItem.setArrivalDate(updateDate);
		buyBillReconciliationItem.setArrivalNum(arrivalNum);
		buyBillReconciliationItem.setSalePrice(salePrice);
		buyBillReconciliationItem.setIsInvoiceStatus("0");
		buyBillReconciliationItem.setReconciliationDealStatus("1");

		int addItemCount = sellerBillReconciliationItemMapper.addRecItem(buyBillReconciliationItem);
		return addItemCount;
	}

	//买家自定义金额审批
	public int acceptBillCustom(Map<String,Object> acceptCustomMap){
		SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
		String reconciliationId = acceptCustomMap.get("reconciliationId").toString();
		String customStatus = acceptCustomMap.get("customStatus").toString();
		String customRemarks = acceptCustomMap.get("customRemarks").toString();

		Map<String,Object> queryRecmap = new HashMap<String,Object>();
		queryRecmap.put("id",reconciliationId);
		SellerBillReconciliation sellerBillRecInfo = sellerBillReconciliationMapper.selectByPrimaryKey(queryRecmap);
		BigDecimal totalAmountOld = sellerBillRecInfo.getTotalAmount();
		BigDecimal customAmountOld = sellerBillRecInfo.getCustomAmount();
		//根据条件查询奖惩详情
		Map<String,Object> queryCustommap = new HashMap<String,Object>();
		queryRecmap.put("reconciliationId",reconciliationId);
		queryRecmap.put("customStatus","9");
		Map<String,Object> queryCustomInfo = sellerBillReconciliationMapper.queryCustomInfo(queryRecmap);
		BigDecimal customAmountDOld = new BigDecimal(0);
		if(queryCustomInfo != null){
			String customAmountStr = queryCustomInfo.get("customPaymentMoney").toString();
			String customStatusStr = queryCustomInfo.get("customStatus").toString();
			customAmountDOld = new BigDecimal(customAmountStr);
		}


		BigDecimal totalAmount = new BigDecimal(0);
		BigDecimal customAmount = new BigDecimal(0);
		if("10".equals(customStatus)){
			totalAmount = totalAmountOld;
			customAmount = customAmountOld;
		}else if("11".equals(customStatus)){
			totalAmount = totalAmountOld.subtract(customAmountDOld);
			customAmount = customAmountOld.subtract(customAmountDOld);
		}
		sellerBillReconciliation.setId(reconciliationId);
		sellerBillReconciliation.setBillDealStatus(customStatus);
		sellerBillReconciliation.setTotalAmount(totalAmount);
		sellerBillReconciliation.setResidualPaymentAmount(totalAmount);
		sellerBillReconciliation.setCustomAmount(customAmount);
		int acceptCustomCount = sellerBillReconciliationMapper.updateByPrimaryKeySelective(sellerBillReconciliation);
		if(acceptCustomCount >0){
			//更新到详情
			Map<String,Object> customMoneyMap = new HashMap<String,Object>();
			customMoneyMap.put("reconciliationId",reconciliationId);
			customMoneyMap.put("updateCustomTime",new Date());
			customMoneyMap.put("updateCustomUserId",ShiroUtils.getUserId());
			customMoneyMap.put("customStatus",customStatus);//自定义付款状态：9 自定义付款待卖家确认 10 卖家通过 11 卖家驳回
			customMoneyMap.put("updateRemarks",customRemarks);
			sellerBillReconciliationMapper.updateBillRecCustom(customMoneyMap);
			//同步到买家
			SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
			sBuyBillReconciliation.setId(reconciliationId);
			sBuyBillReconciliation.setBillDealStatus(customStatus);
			sBuyBillReconciliation.setTotalAmount(totalAmount);
			sBuyBillReconciliation.setResidualPaymentAmount(totalAmount);
			sBuyBillReconciliation.setCustomAmount(customAmount);
			sBuyBillReconciliationMapper.updateByPrimaryKeySelective(sBuyBillReconciliation);
		}
		return acceptCustomCount;
	}

	//根据对账单号查询自定义付款附件信息
	public List<Map<String,Object>> queryBillCustomList(Map<String,Object> billFileMap){
		String reconciliationId = billFileMap.get("reconciliationId").toString();
		List<Map<String,Object>> billRecFileMap = sellerBillReconciliationMapper.queryBillCustomList(reconciliationId);
		return billRecFileMap;
	}
	//添加账单附件
	public int addBillFileInfo(Map<String,Object> addBillFileMap){
		String reconciliationId = addBillFileMap.get("reconciliationId").toString();
		String billFileStr = addBillFileMap.get("billFileStr").toString();
		String billFileRemarks = addBillFileMap.get("billFileRemarks").toString();

		Map<String,Object> billFileMap = new HashMap<String,Object>();
		billFileMap.put("id",ShiroUtils.getUid());
		billFileMap.put("createUserId",ShiroUtils.getUserId());
		billFileMap.put("reconciliationId",reconciliationId);
		billFileMap.put("billFileStr",billFileStr);
		billFileMap.put("billFileRemarks",billFileRemarks);

		int addBillFileCount = sellerBillReconciliationMapper.addBillFileInfo(billFileMap);
		return addBillFileCount;
	}

	//根据对账单号查询附件信息
	public List<Map<String,Object>> queryBillFileList(Map<String,Object> billFileMap){
		String reconciliationId = billFileMap.get("reconciliationId").toString();
		List<Map<String,Object>> billRecFileMap = sellerBillReconciliationMapper.queryBillFileList(reconciliationId);
		return billRecFileMap;
	}

	//根据编号修改状态
	public int launchReconciliation(Map<String,Object> approvalMap) {
		List<String> idList = new ArrayList<String>();
		SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
		String id = approvalMap.get("id").toString();
		sellerBillReconciliation.setId(id);
		sellerBillReconciliation.setBillDealStatus("2");
		int updateRecCount = updateByPrimaryKeySelective(sellerBillReconciliation);
		if(updateRecCount > 0){
			Map<String,Object> queryRecmap = new HashMap<String,Object>();
			queryRecmap.put("id",id);
			SellerBillReconciliation sellerBillRecInfo = sellerBillReconciliationMapper.selectByPrimaryKey(queryRecmap);

			int buyRecCount = sBuyBillReconciliationMapper.queryBillStatusCount(queryRecmap);
			//同步关联到买家对账
			SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
			if(buyRecCount == 0){

				sBuyBillReconciliation.setId(sellerBillRecInfo.getId());
				sBuyBillReconciliation.setPaymentNo(sellerBillRecInfo.getPaymentNo());
				sBuyBillReconciliation.setSellerCompanyId(sellerBillRecInfo.getCompanyId());
				sBuyBillReconciliation.setCompanyId(sellerBillRecInfo.getBuyCompanyId());
				sBuyBillReconciliation.setCreateBillUserId(sellerBillRecInfo.getCreateBillUserId());
				sBuyBillReconciliation.setCreateBillUserName(sellerBillRecInfo.getCreateBillUserName());
				sBuyBillReconciliation.setStartBillStatementDate(sellerBillRecInfo.getStartBillStatementDate());
				sBuyBillReconciliation.setEndBillStatementDate(sellerBillRecInfo.getEndBillStatementDate());
				sBuyBillReconciliation.setRecordConfirmCount(sellerBillRecInfo.getRecordConfirmCount());
				sBuyBillReconciliation.setRecordConfirmTotal(sellerBillRecInfo.getRecordConfirmTotal());
				sBuyBillReconciliation.setRecordReplaceCount(sellerBillRecInfo.getRecordReplaceCount());
				sBuyBillReconciliation.setRecordReplaceTotal(sellerBillRecInfo.getRecordReplaceTotal());
				sBuyBillReconciliation.setReplaceConfirmCount(sellerBillRecInfo.getReplaceConfirmCount());
				sBuyBillReconciliation.setReplaceConfirmTotal(sellerBillRecInfo.getReplaceConfirmTotal());
				sBuyBillReconciliation.setFreightSumCount(sellerBillRecInfo.getFreightSumCount());
				sBuyBillReconciliation.setReturnGoodsCount(sellerBillRecInfo.getReturnGoodsCount());
				sBuyBillReconciliation.setReturnGoodsTotal(sellerBillRecInfo.getReturnGoodsTotal());
				//SBuyBillReconciliation.setTotalSumCount(sellerBillRecInfo.getTotalSumCount());
				sBuyBillReconciliation.setTotalAmount(sellerBillRecInfo.getTotalAmount());
				sBuyBillReconciliation.setResidualPaymentAmount(sellerBillRecInfo.getResidualPaymentAmount());
				sBuyBillReconciliation.setAdvanceDeductTotal(sellerBillRecInfo.getAdvanceDeductTotal());
				sBuyBillReconciliation.setPaymentStatus(sellerBillRecInfo.getPaymentStatus());
				sBuyBillReconciliation.setBillDealStatus(sellerBillRecInfo.getBillDealStatus());
				sBuyBillReconciliation.setCreateDate(sellerBillRecInfo.getCreateDate());

				sBuyBillReconciliationMapper.saveBillReconciliationInfo(sBuyBillReconciliation);

			}else {
				/*sBuyBillReconciliation.setId(sellerBillRecInfo.getId());
				sBuyBillReconciliation.setBillDealStatus(sellerBillRecInfo.getBillDealStatus());*/
				sBuyBillReconciliation.setId(sellerBillRecInfo.getId());
				sBuyBillReconciliation.setPaymentNo(sellerBillRecInfo.getPaymentNo());
				sBuyBillReconciliation.setSellerCompanyId(sellerBillRecInfo.getCompanyId());
				sBuyBillReconciliation.setCompanyId(sellerBillRecInfo.getBuyCompanyId());
				sBuyBillReconciliation.setCreateBillUserId(sellerBillRecInfo.getCreateBillUserId());
				sBuyBillReconciliation.setCreateBillUserName(sellerBillRecInfo.getCreateBillUserName());
				sBuyBillReconciliation.setStartBillStatementDate(sellerBillRecInfo.getStartBillStatementDate());
				sBuyBillReconciliation.setEndBillStatementDate(sellerBillRecInfo.getEndBillStatementDate());
				sBuyBillReconciliation.setRecordConfirmCount(sellerBillRecInfo.getRecordConfirmCount());
				sBuyBillReconciliation.setRecordConfirmTotal(sellerBillRecInfo.getRecordConfirmTotal());
				sBuyBillReconciliation.setRecordReplaceCount(sellerBillRecInfo.getRecordReplaceCount());
				sBuyBillReconciliation.setRecordReplaceTotal(sellerBillRecInfo.getRecordReplaceTotal());
				sBuyBillReconciliation.setReplaceConfirmCount(sellerBillRecInfo.getReplaceConfirmCount());
				sBuyBillReconciliation.setReplaceConfirmTotal(sellerBillRecInfo.getReplaceConfirmTotal());
				sBuyBillReconciliation.setFreightSumCount(sellerBillRecInfo.getFreightSumCount());
				sBuyBillReconciliation.setReturnGoodsCount(sellerBillRecInfo.getReturnGoodsCount());
				sBuyBillReconciliation.setReturnGoodsTotal(sellerBillRecInfo.getReturnGoodsTotal());
				//SBuyBillReconciliation.setTotalSumCount(sellerBillRecInfo.getTotalSumCount());
				sBuyBillReconciliation.setTotalAmount(sellerBillRecInfo.getTotalAmount());
				sBuyBillReconciliation.setCustomAmount(sellerBillRecInfo.getCustomAmount());
				sBuyBillReconciliation.setResidualPaymentAmount(sellerBillRecInfo.getResidualPaymentAmount());
				sBuyBillReconciliation.setAdvanceDeductTotal(sellerBillRecInfo.getAdvanceDeductTotal());
				sBuyBillReconciliation.setPaymentStatus(sellerBillRecInfo.getPaymentStatus());
				sBuyBillReconciliation.setBillDealStatus(sellerBillRecInfo.getBillDealStatus());
				sBuyBillReconciliation.setCreateDate(sellerBillRecInfo.getCreateDate());
				sBuyBillReconciliationMapper.updateByPrimaryKeySelective(sBuyBillReconciliation);

				//删除驳回详情
				Map<String,Object> deleteRecItemMap = new HashMap<String,Object>();
				deleteRecItemMap.put("reconciliationId",sellerBillRecInfo.getId());
				deleteRecItemMap.put("billReconciliationStatus","4");
				sellerBillReconciliationItemMapper.deleteRecItem(deleteRecItemMap);
				//删除抵扣同步的驳回详情
				//删除预付款关联
				Map<String,Object> deleteEditMap = new HashMap<String,Object>();
				deleteEditMap.put("reconciliationId",sellerBillRecInfo.getId());
				deleteEditMap.put("editStatus","4");//充值状态： 1 内部审批中 ，2 审批通过， 3 审批驳回 ，4 对账驳回
				sellerBillRecAdvanceMapper.deleteAdvanceEdit(deleteEditMap);
			}
		}
		return updateRecCount;
	}

	//修改对账单价
	public JSONObject updateRecItemPrice(Map<String,Object> updateMap){
		JSONObject json = new JSONObject();
		String reconciliationId = updateMap.get("reconciliationId").toString();

		//根据对账编号查询对账详情
		//BuyBillReconciliationItem billRecItem = buyBillReconciliationItemMapper.queryRecItem(id);
		Map<String, Object> recMap = new HashMap<String,Object>();
		recMap.put("id",reconciliationId);
		SellerBillReconciliation sellerBillRecOld = sellerBillReconciliationMapper.selectByPrimaryKey(recMap);

		//String reconciliationDealStatus = billRecItem.getReconciliationDealStatus();
		String reconciliationDealStatus = sellerBillRecOld.getBillDealStatus();
		BigDecimal freightSum = sellerBillRecOld.getFreightSumCount();

		String recItemId = updateMap.get("recItemId").toString();
		String updateSalePrice = updateMap.get("updateSalePrice").toString();

		int result = 0;
		if(!"3".equals(reconciliationDealStatus)){
			SellerBillReconciliationItem buyBillReconciliationItem = sellerBillReconciliationItemMapper.queryRecItem(recItemId);
			BigDecimal updateSalePriceOld = buyBillReconciliationItem.getUpdateSalePrice();//获取上次修改单价
			Map<String,Object> updateSaleMap = new HashMap<String,Object>();
			updateSaleMap.put("id",recItemId);
			updateSaleMap.put("updateSalePrice",updateSalePrice);
			updateSaleMap.put("updateSalePriceOld",updateSalePriceOld);
			result = sellerBillReconciliationItemMapper.updateRecItemPrice(updateSaleMap);
		}
		if(result>0){
			//BigDecimal advanceDeductTotal = new BigDecimal(0);//预付款抵扣金额
			BigDecimal advanceDeductTotal = sellerBillRecOld.getAdvanceDeductTotal();//预付款抵扣金额
			BigDecimal deliveryPriceSum = sellerBillRecOld.getRecordConfirmTotal();
			BigDecimal recReplacePriceSum = sellerBillRecOld.getRecordReplaceTotal();
			BigDecimal replacePriceSum = sellerBillRecOld.getReplaceConfirmTotal();
			BigDecimal customerPriceSum = sellerBillRecOld.getReturnGoodsTotal();
			//BigDecimal totalAmountOld = buyBillRecOld.getTotalAmount();

			SellerBillReconciliationItem sellerBillReconciliationItem = sellerBillReconciliationItemMapper.queryRecItem(recItemId);
			String isUpdateSale = sellerBillReconciliationItem.getIsUpdateSale();
			String advanceEditId = "";
			String buyCompanyId = "";
			if(null != sellerBillReconciliationItem.getAdvanceEditId() && !"".equals(sellerBillReconciliationItem.getAdvanceEditId())){
				advanceEditId = sellerBillReconciliationItem.getAdvanceEditId();//关联预付款抵扣id
				buyCompanyId = sellerBillReconciliationItem.getBuyCompanyId();//获取采购商id
			}
			if(!"1".equals(isUpdateSale)){
				BigDecimal salePriceOld = sellerBillReconciliationItem.getSalePrice();//获取上次修改单价

				String billRecType = sellerBillReconciliationItem.getBillReconciliationType();
				String createBillId = sellerBillReconciliationItem.getCreateBillId();
				if("0".equals(billRecType)){

					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					//判断修改价格
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = salePriceOld.multiply(arrivalNumDecimal);//上次修改金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//recReplacePriceSum = recReplacePriceSum.subtract(salePriceSumOld).add(salePriceSumNew);
					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					recReplacePriceSum = recReplacePriceSum.add(salePriceSumEnd);
				}else if("1".equals(billRecType)){
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					//判断修改价格
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = salePriceOld.multiply(arrivalNumDecimal);//上次修改金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//deliveryPriceSum = deliveryPriceSum.subtract(salePriceSumOld).add(salePriceSumNew);
					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					deliveryPriceSum = deliveryPriceSum.add(salePriceSumEnd);
				}else if("2".equals(billRecType)){
					//replaceCount = replaceCount +arrivalNum;
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//修改单价
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = salePriceOld.multiply(arrivalNumDecimal);//收获金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//replacePriceSum = replacePriceSum.subtract(salePriceSumOld).add(salePriceSumNew);
					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					replacePriceSum = replacePriceSum.add(salePriceSumEnd);
				}else if("3".equals(billRecType)){
					//customerCount = customerCount + arrivalNum;
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = salePriceOld.multiply(arrivalNumDecimal);//收获金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//customerPriceSum = customerPriceSum.subtract(salePriceSumOld).add(salePriceSumNew);
					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					customerPriceSum = customerPriceSum.add(salePriceSumEnd);
				}
				//修改为单价变更
				Map<String,Object> updateSaleMap = new HashMap<String,Object>();
				updateSaleMap.put("id",recItemId);
				updateSaleMap.put("isUpdateSale","1");
				sellerBillReconciliationItemMapper.updateRecItemPrice(updateSaleMap);

			}else {
				BigDecimal updateSalePriceOld = sellerBillReconciliationItem.getUpdateSalePriceOld();//获取上次修改单价

				String createBillId = sellerBillReconciliationItem.getCreateBillId();
				String billRecType = sellerBillReconciliationItem.getBillReconciliationType();
				if("0".equals(billRecType)){
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					//判断修改价格
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = updateSalePriceOld.multiply(arrivalNumDecimal);//上次修改金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//recReplacePriceSum = recReplacePriceSum.subtract(salePriceSumOld).add(salePriceSumNew);

					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					recReplacePriceSum = recReplacePriceSum.add(salePriceSumEnd);
				}else if("1".equals(billRecType)){
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					//判断修改价格
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = updateSalePriceOld.multiply(arrivalNumDecimal);//上次修改金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//deliveryPriceSum = deliveryPriceSum.subtract(salePriceSumOld).add(salePriceSumNew);

					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					deliveryPriceSum = deliveryPriceSum.add(salePriceSumEnd);
				}else if("2".equals(billRecType)){
					//replaceCount = replaceCount +arrivalNum;
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//修改单价
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = updateSalePriceOld.multiply(arrivalNumDecimal);//收获金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//replacePriceSum = replacePriceSum.subtract(salePriceSumOld).add(salePriceSumNew);

					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					replacePriceSum = replacePriceSum.add(salePriceSumEnd);
				}else if("3".equals(billRecType)){
					//customerCount = customerCount + arrivalNum;
					int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
					BigDecimal updateSalePrice1 = sellerBillReconciliationItem.getUpdateSalePrice();//单价
					BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
					BigDecimal salePriceSumOld = updateSalePriceOld.multiply(arrivalNumDecimal);//收获金额
					BigDecimal salePriceSumNew = updateSalePrice1.multiply(arrivalNumDecimal);//收获金额
					//customerPriceSum = customerPriceSum.subtract(salePriceSumOld).add(salePriceSumNew);

					//计算预付款金额
					BigDecimal advanceEditSum = salePriceSumNew.subtract(salePriceSumOld);
					Map<String,Object> advanceCalculateMap = advanceCalculate("update",advanceEditId,buyCompanyId,createBillId,advanceEditSum,salePriceSumOld,reconciliationId);
					BigDecimal salePriceSumEnd = new BigDecimal(advanceCalculateMap.get("salePriceSumEnd").toString());
					BigDecimal advanceDeductEnd = new BigDecimal(advanceCalculateMap.get("advanceDeductTotal").toString());
					advanceDeductTotal = advanceDeductTotal.add(advanceDeductEnd);
					customerPriceSum = customerPriceSum.add(salePriceSumEnd);
				}
			}

			//int totalSumCount = deliveryCount + replaceCount + customerCount;
			BigDecimal totalAmount = deliveryPriceSum.add(recReplacePriceSum).subtract(replacePriceSum).subtract(customerPriceSum).add(freightSum);

			SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();
			sellerBillReconciliation.setId(reconciliationId);
			sellerBillReconciliation.setRecordConfirmTotal(deliveryPriceSum);
			sellerBillReconciliation.setRecordReplaceTotal(recReplacePriceSum);
			sellerBillReconciliation.setReplaceConfirmTotal(replacePriceSum);
			sellerBillReconciliation.setReturnGoodsTotal(customerPriceSum);
			sellerBillReconciliation.setTotalAmount(totalAmount);
			sellerBillReconciliation.setResidualPaymentAmount(totalAmount);
			sellerBillReconciliation.setAdvanceDeductTotal(advanceDeductTotal);

			updateByPrimaryKeySelective(sellerBillReconciliation);

			/*SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
			sBuyBillReconciliation.setId(reconciliationId);
			sBuyBillReconciliation.setRecordConfirmTotal(deliveryPriceSum);
			sBuyBillReconciliation.setReplaceConfirmTotal(replacePriceSum);
			sBuyBillReconciliation.setReturnGoodsTotal(customerPriceSum);
			sBuyBillReconciliation.setTotalAmount(totalAmount);
			sBuyBillReconciliation.setResidualPaymentAmount(totalAmount);
			sBuyBillReconciliationMapper.updateByPrimaryKeySelective(sBuyBillReconciliation);*/
			json.put("success", true);
			json.put("msg", "单价修改成功！");
		}else{
			if("3".equals(reconciliationDealStatus)){
				json.put("error", false);
				json.put("msg", "对账已经完成，不可修改单价！");
			}else {
				json.put("error", false);
				json.put("msg", "单价修改失败！");
			}
		}
		return json;
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(SellerBillReconciliation record) {
		return sellerBillReconciliationMapper.updateByPrimaryKeySelective(record);
	}

	//查询账单对账列表信息
	public List<SellerBillReconciliation> getBillReconciliationList(
			SearchPageUtil searchPageUtil) {
		return sellerBillReconciliationMapper.getBillReconciliationList(searchPageUtil);
	}
	//向账单周期管理页面提供不同状态下的数量
	public void getBillCountByStatus(Map<String, Object> billMap){
		//查询待发起对账
		billMap.put("billDealStatusCount", "1");
		int approvalBillReconciliationCount = queryBillStatusCount(billMap);
		//查询待对方审批
		billMap.put("billDealStatusCount", "2");
		int acceptBillReconciliationCount = queryBillStatusCount(billMap);
		//查询审批已通过
		billMap.put("billDealStatusCount", "3");
		int apprEndBillReconciliationCount = queryBillStatusCount(billMap);
		//查询对方审批驳回
		billMap.put("billDealStatusCount", "4");
		int stopBillReconciliationCount = queryBillStatusCount(billMap);

		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillReconciliationCount", approvalBillReconciliationCount);
		billMap.put("acceptBillReconciliationCount", acceptBillReconciliationCount);
		billMap.put("apprEndBillReconciliationCount", apprEndBillReconciliationCount);
		billMap.put("stopBillReconciliationCount", stopBillReconciliationCount);
	}
	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return sellerBillReconciliationMapper.queryBillStatusCount(map);
	}

	//查询对账数据导出
	public List<SellerBillReconciliation> selectByPrimaryKeyList(Map<String,Object> recMap){
		return sellerBillReconciliationMapper.selectByPrimaryKeyList(recMap);
	}

	//添加账单数据
	public void  insertSellerReconciliation(SellerBillReconciliation sellerBuyBillReconciliation){
		sellerBillReconciliationMapper.saveBillReconciliationInfo(sellerBuyBillReconciliation);
	}

	//修改数据
	public int updateReconciliation(Map<String,Object> ReconMap){
		int updateCount = 0;
		if(null != ReconMap){
			SellerBillReconciliation sellerBillReconciliation = new SellerBillReconciliation();

			String id = ReconMap.get("reconciliationId").toString();
			String billDealStatus = ReconMap.get("billDealStatus").toString();
			//String dealRemarks = ReconMap.get("approvalRemarks").toString();

			sellerBillReconciliation.setId(id);
			sellerBillReconciliation.setBillDealStatus(billDealStatus);
			updateCount = updateByPrimaryKeySelective(sellerBillReconciliation);
			if(updateCount > 0){
				//同步到买家
				SBuyBillReconciliation sBuyBillReconciliation = new SBuyBillReconciliation();
				sBuyBillReconciliation.setId(id);
				sBuyBillReconciliation.setBillDealStatus(billDealStatus);
				sBuyBillReconciliationService.updateByPrimaryKeySelective(sBuyBillReconciliation);
				//修改收货、退货模块状态为已对账
				//if(billDealStatus.equals("3")){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("reconciliationId",id);
				//map.put("isInvoiceStatus",billDealStatus);
				map.put("reconciliationDealStatus",billDealStatus);
				sellerBillReconciliationItemMapper.updateRecItemPrice(map);
			}
		}
		return  updateCount;
	}

	//查询详细
	public  Map<String,Object> queryRecordAndReturn(String reconciliationId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation sellerBillReconciliation = selectByPrimaryKey(map);
		int totalSumCount = sellerBillReconciliation.getTotalSumCount();
		BigDecimal totalAmount = sellerBillReconciliation.getTotalAmount();
		BigDecimal freightSum = sellerBillReconciliation.getFreightSumCount();
		String buyCompanyId = sellerBillReconciliation.getBuyCompanyId();
		String buyCompanyName = sellerBillReconciliation.getBuyCompanyName();
		//String buyCompanyId = sellerBillReconciliation.getBuyCompanyId();
		//String buyCompanyName = sellerBillReconciliation.getBuyCompanyName();
		String reconciliationStatus = sellerBillReconciliation.getBillDealStatus();
		String startBillStatementDateStr = sellerBillReconciliation.getStartBillStatementDateStr();
		String endBillstatementDateStr = sellerBillReconciliation.getEndBillStatementDateStr();

		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		List<Map<String,Object>> recordItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> recordReplaceItemList = new ArrayList<Map<String,Object>>();//收获详细
		List<Map<String,Object>> customerItemList = new ArrayList<Map<String,Object>>();//收获详细

		if(null != billRecItemList && billRecItemList.size()>0){
			for(SellerBillReconciliationItem sellerBillReconciliationItem : billRecItemList){
				String billRecType = sellerBillReconciliationItem.getBillReconciliationType();
				String deliverNo = sellerBillReconciliationItem.getDeliverNo();
				String itemId = sellerBillReconciliationItem.getItemId();
				String arrivalDateStr = sellerBillReconciliationItem.getArrivalDateStr();
				String isUpdateSale = sellerBillReconciliationItem.getIsUpdateSale();

				int arrivalNum = sellerBillReconciliationItem.getArrivalNum();//数量
				BigDecimal salePrice = new BigDecimal(0);//单价
				if("1".equals(isUpdateSale)){
					salePrice = sellerBillReconciliationItem.getUpdateSalePrice();
				}else {
					salePrice = sellerBillReconciliationItem.getSalePrice();
				}
				BigDecimal arrivalNumDecimal = new BigDecimal(arrivalNum);//转换类型
				BigDecimal salePriceSum = salePrice.multiply(arrivalNumDecimal);//收获金额

				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				if("1".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					//String receiptvoucherAddr = sellerBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("itemId",itemId);
					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					//recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("attachmentList",attachmentAddrStr);
					recordAndTtemMap.put("sellerDeliveryRecordItem",sellerBillReconciliationItem);
					recordAndTtemMap.put("salePriceSum",salePriceSum);
					recordItemList.add(recordAndTtemMap);
				}else if("2".equals(billRecType)){
					Map<String,Object> recordAndTtemMap = new HashMap<String,Object>();
					//String receiptvoucherAddr = sellerBillReconciliationItem.getDeliveryAddr();

					recordAndTtemMap.put("deliverNo",deliverNo);
					recordAndTtemMap.put("arrivalDateStr",arrivalDateStr);
					//recordAndTtemMap.put("receiptvoucherAddr",receiptvoucherAddr);
					recordAndTtemMap.put("attachmentList",attachmentAddrStr);
					recordAndTtemMap.put("sellerRecordReplaceItem",sellerBillReconciliationItem);
					recordAndTtemMap.put("replacePriceSum",salePriceSum);
					recordReplaceItemList.add(recordAndTtemMap);
				}else if("3".equals(billRecType)){
					Map<String,Object> customerTtemMap = new HashMap<String,Object>();
					String customerCode = sellerBillReconciliationItem.getCustomerCode();
					String proof = sellerBillReconciliationItem.getProof();

					customerTtemMap.put("customerCode",customerCode);
					customerTtemMap.put("verifyDateStr",arrivalDateStr);
					customerTtemMap.put("proof",proof);
					customerTtemMap.put("buyCustomerItem",sellerBillReconciliationItem);
					customerTtemMap.put("goodsPriceSum",salePriceSum);
					customerItemList.add(customerTtemMap);
				}
			}
		}

		//拼装返回值
		Map<String,Object> recordAndReturnMap = new HashMap<String,Object>();
		//recordAndReturnMap.put("deliveryRecordList",deliveryRecordList);
		recordAndReturnMap.put("totalSumCount",totalSumCount);
		recordAndReturnMap.put("freightSum",freightSum);
		recordAndReturnMap.put("totalAmount",totalAmount);
		recordAndReturnMap.put("reconciliationStatus",reconciliationStatus);
		recordAndReturnMap.put("buyCompanyName",buyCompanyName);
		recordAndReturnMap.put("startBillStatementDateStr",startBillStatementDateStr);
		recordAndReturnMap.put("endBillstatementDateStr",endBillstatementDateStr);
		recordAndReturnMap.put("recordItemList",recordItemList);
		recordAndReturnMap.put("recordReplaceItemList",recordReplaceItemList);
		recordAndReturnMap.put("customerItemList",customerItemList);
		return recordAndReturnMap;
	}

	//对账详情查看附件
	public String queryFileUrlList(Map<String,Object> queryUrlMap){
		String itemType = queryUrlMap.get("itemTtpe").toString();
		String deliverNo = queryUrlMap.get("deliverNo").toString();
		String attachmentAddrStr = "";
		if("1".equals(itemType)){
			//根据发货单号查询票据附件
			List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
			if(null != attachmentList && attachmentList.size()>0){
				for(Map<String,Object> attachmentMap : attachmentList){
					attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString()+",";

				}
				attachmentAddrStr = attachmentAddrStr.substring(0,attachmentAddrStr.length()-1);
			}
		}else {
			//售后附件
			Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
			if(null != customerProof && customerProof.size()>0){
				attachmentAddrStr = customerProof.get("proof").toString();
			}
		}
		return attachmentAddrStr.trim();
	}

	//查询详细
	/**
	 * 对账详情
	 * @param reconciliationId
	 * @param model
	 * @return
	 */
	public void queryRecordAndReturnNew(String reconciliationId, Model model) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation sellerBillReconciliation = selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		String billRecStatus = sellerBillReconciliation.getBillDealStatus();
		if("4".equals(billRecStatus)){
			recItemMap.put("reconciliationDealStatus","4");
		}else {
			recItemMap.put("recDealStatusPass","4");
		}

		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(SellerBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> recordReplaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recordReplaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recordReplaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recordReplaceItemList && recordReplaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : recordReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recordReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> daohuoItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> replaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> customerItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					customerItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put(logisticsId, itemMap);
				}
			}
		}
		model.addAttribute("buyBillReconciliation", sellerBillReconciliation);
		model.addAttribute("recordReplaceMap",recordReplaceMap);
		model.addAttribute("daohuoMap", daohuoMap);
		model.addAttribute("replaceMap", replaceMap);
		model.addAttribute("customerMap", customerMap);
	}

	//账单详情导出
	public Map<String,Object> queryRecItemExport(String reconciliationId) {
		Map<String,Object> recItemExportMap = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id",reconciliationId);
		//账单对账详情
		SellerBillReconciliation sellerBillReconciliation = selectByPrimaryKey(map);
		//根据对账编号查询对账详情
		Map<String,Object> recItemMap = new HashMap<String,Object>();
		recItemMap.put("reconciliationId",reconciliationId);
		String billRecStatus = sellerBillReconciliation.getBillDealStatus();
		if("4".equals(billRecStatus)){
			recItemMap.put("reconciliationDealStatus","4");
		}else {
			recItemMap.put("recDealStatusPass","4");
		}
		List<SellerBillReconciliationItem> billRecItemList = sellerBillReconciliationItemMapper.queryRecItemList(recItemMap);
		Map<String,Map<String,Object>> recordReplaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> daohuoMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> replaceMap = new HashMap<String,Map<String,Object>>();
		Map<String,Map<String,Object>> customerMap = new HashMap<String,Map<String,Object>>();
		if(billRecItemList != null && billRecItemList.size() > 0){
			for(SellerBillReconciliationItem item : billRecItemList){
				String deliverNo = item.getDeliverNo();
				//根据发货单号查询票据附件
				List<Map<String,Object>> attachmentList = sellerBillReconciliationItemMapper.queryRecItemAddr(deliverNo);
				String attachmentAddrStr = "";
				if(null != attachmentList && attachmentList.size()>0){
					for(Map<String,Object> attachmentMap : attachmentList){
						attachmentAddrStr = attachmentAddrStr + attachmentMap.get("url").toString();
					}
				}
				//售后附件
				Map<String,Object> customerProof = sellerBillReconciliationItemMapper.queryCustomerProof(deliverNo);
				String proof = "";
				if(null != customerProof && customerProof.size()>0){
					proof = customerProof.get("proof").toString();
				}
				if(!"1".equals(item.getIsUpdateSale())){
					//没有修改了
					item.setUpdateSalePrice(item.getSalePrice());
				}
				if("0".equals(item.getBillReconciliationType())){
					//发货换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> recordReplaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						recordReplaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					recordReplaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != recordReplaceItemList && recordReplaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : recordReplaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", recordReplaceItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					recordReplaceMap.put(logisticsId, itemMap);
				}
				if("1".equals(item.getBillReconciliationType())){
					//发货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> daohuoItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemRecPriceSum = new BigDecimal(0);
					BigDecimal itemRecPriceAndFreightSum = new BigDecimal(0);
					//货运单号
					String logisticsId = item.getLogisticsId();
					if(daohuoMap.containsKey(logisticsId)){
						itemMap = daohuoMap.get(logisticsId);
						daohuoItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemRecPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
						itemRecPriceAndFreightSum = (BigDecimal) itemMap.get("itemPriceAndFreightSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					daohuoItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					//BigDecimal itemPriceAndFreightSum = new BigDecimal(0);
					if(null != daohuoItemList && daohuoItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : daohuoItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemRecPriceSum = itemRecPriceSum.add(itemPriceSum);
					//itemPriceAndFreightSum = itemPriceSum.add(item.getFreight());
					itemRecPriceAndFreightSum = itemRecPriceAndFreightSum.add(itemPriceSum);

					itemMap.put("itemList", daohuoItemList);
					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemRecPriceSum);
					itemMap.put("itemPriceAndFreightSum",itemRecPriceAndFreightSum);
					itemMap.put("attachmentAddrStr",attachmentAddrStr);
					daohuoMap.put(logisticsId, itemMap);
				}
				if("2".equals(item.getBillReconciliationType())){
					//换货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> replaceItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemReplacePriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(replaceMap.containsKey(logisticsId)){
						itemMap = replaceMap.get(logisticsId);
						replaceItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemReplacePriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					replaceItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != replaceItemList && replaceItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : replaceItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemReplacePriceSum = itemReplacePriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemReplacePriceSum);
					itemMap.put("itemList", replaceItemList);
					itemMap.put("attachmentAddrStr",proof);
					replaceMap.put(logisticsId, itemMap);
				}
				if("3".equals(item.getBillReconciliationType())){
					//退货数据
					Map<String,Object> itemMap = new HashMap<String,Object>();
					List<SellerBillReconciliationItem> customerItemList = new ArrayList<SellerBillReconciliationItem>();
					BigDecimal itemCustomerPriceSum = new BigDecimal(0);
					//货运单号
					String logisticsId = "";
					if(customerMap.containsKey(logisticsId)){
						itemMap = customerMap.get(logisticsId);
						customerItemList = (List<SellerBillReconciliationItem>)itemMap.get("itemList");
						itemCustomerPriceSum = (BigDecimal) itemMap.get("itemPriceSum");
					}else{
						itemMap.put("waybillNo", item.getWaybillNo());
						itemMap.put("logisticsCompany", item.getLogisticsCompany());
						itemMap.put("driverName", item.getDriverName());
						itemMap.put("freight", item.getFreight());
					}
					customerItemList.add(item);
					//计算金额及数量
					int arrivalNumSum = 0;
					BigDecimal itemPriceSum = new BigDecimal(0);
					if(null != customerItemList && customerItemList.size()>0){
						for(SellerBillReconciliationItem billRecItem : customerItemList){
							arrivalNumSum = arrivalNumSum + billRecItem.getArrivalNum();
						}
					}
					BigDecimal salePrice = item.getUpdateSalePrice();
					int arrivalNum = item.getArrivalNum();
					BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
					itemPriceSum = salePrice.multiply(arrivalNumB);
					itemCustomerPriceSum = itemCustomerPriceSum.add(itemPriceSum);

					itemMap.put("arrivalNumSum",arrivalNumSum);
					itemMap.put("itemPriceSum",itemCustomerPriceSum);
					itemMap.put("itemList", customerItemList);
					itemMap.put("attachmentAddrStr",proof);
					customerMap.put(logisticsId, itemMap);
				}
			}
		}
		recItemExportMap.put("sellerBillReconciliation", sellerBillReconciliation);
		recItemExportMap.put("recordReplaceMap",recordReplaceMap);
		recItemExportMap.put("daohuoMap", daohuoMap);
		recItemExportMap.put("replaceMap", replaceMap);
		recItemExportMap.put("customerMap", customerMap);

		return recItemExportMap;
	}
}
