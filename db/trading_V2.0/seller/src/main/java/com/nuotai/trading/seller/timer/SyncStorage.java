package com.nuotai.trading.seller.timer;

import com.nuotai.trading.model.TimeTask;
import com.nuotai.trading.seller.service.SyncWmsService;
import com.nuotai.trading.service.TimeTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author liuhui
 * @date 2017-10-24 8:55
 * @description
 **/
@Component
public class SyncStorage {
    @Autowired
    private TimeTaskService timeTaskService;
    @Autowired
    private SyncWmsService syncWmsService;

    /**
     * 抓取入库的定时任务,更新发货记录，每5分钟执行一次
     * @throws Exception
     */
	@Scheduled(cron="0 0/3 * * * ?")
    public void syncDelivery() throws Exception {
        TimeTask timeTask = timeTaskService.selectByCode("CRAWL_STORAGE");
        if(timeTask!=null){
            if("0".equals(timeTask.getCurrentStatus())){
                //当前状态：0：正常等待；1：执行中；2：停止
                if("0".equals(timeTask.getPrepStatus())){
                    try {
                        //预备状态是：正常
                        //当前状态改为执行中
                        timeTask.setCurrentStatus("1");
                        timeTaskService.updateByPrimaryKeySelective(timeTask);
                        // 抓取入库并操作
                        syncWmsService.syncDelivery();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }finally {
                        //当前状态改为正常等待
                        timeTask.setCurrentStatus("0");
                        timeTask.setLastSynchronous(new Date());
                        timeTask.setLastExecute(new Date());
                        timeTaskService.updateByPrimaryKeySelective(timeTask);
                    }
                }else if("1".equals(timeTask.getPrepStatus())){
                    //预备状态是：预备停止,
                    timeTask.setCurrentStatus("2");
                    timeTask.setLastSynchronous(new Date());
                    timeTaskService.updateByPrimaryKeySelective(timeTask);
                }
            }else if("1".equals(timeTask.getCurrentStatus())){
                //执行中
                timeTask.setLastSynchronous(new Date());
                timeTaskService.updateByPrimaryKeySelective(timeTask);
            }else if("2".equals(timeTask.getCurrentStatus())){
                //停止
            }
        }
    }
}
