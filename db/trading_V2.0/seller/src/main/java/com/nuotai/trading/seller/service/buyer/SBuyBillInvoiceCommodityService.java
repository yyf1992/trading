package com.nuotai.trading.seller.service.buyer;

import com.nuotai.trading.seller.dao.buyer.SBuyBillInvoiceCommodityMapper;
import com.nuotai.trading.seller.model.buyer.SBuyBillInvoiceCommodity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SBuyBillInvoiceCommodityService {

    private static final Logger LOG = LoggerFactory.getLogger(SBuyBillInvoiceCommodityService.class);

	@Autowired
	private SBuyBillInvoiceCommodityMapper sBuyBillInvoiceCommodityMapper;
	
	public SBuyBillInvoiceCommodity get(String id){
		return sBuyBillInvoiceCommodityMapper.get(id);
	}
	
	public List<SBuyBillInvoiceCommodity> queryList(Map<String, Object> map){
		return sBuyBillInvoiceCommodityMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sBuyBillInvoiceCommodityMapper.queryCount(map);
	}
	
	public void add(SBuyBillInvoiceCommodity buyBillInvoiceCommodity){
		sBuyBillInvoiceCommodityMapper.add(buyBillInvoiceCommodity);
	}
	
	public void update(SBuyBillInvoiceCommodity buyBillInvoiceCommodity){
		sBuyBillInvoiceCommodityMapper.update(buyBillInvoiceCommodity);
	}
	
	public void delete(String id){
		sBuyBillInvoiceCommodityMapper.delete(id);
	}
	

}
