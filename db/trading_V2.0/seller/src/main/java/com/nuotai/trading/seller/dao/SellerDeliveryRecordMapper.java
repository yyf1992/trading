package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerDeliveryRecord;
import com.nuotai.trading.utils.SearchPageUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * The interface Seller delivery record mapper.
 * 发货Mapper
 *
 * @author liuhui
 * @date 2017 -08-17 10:03:15
 */
@Component
public interface SellerDeliveryRecordMapper extends BaseDao<SellerDeliveryRecord> {

    /**
     * Select delivery num by map int.
     * 获得不同状态发货单数
     *
     * @param selectMap the select map
     * @return the int
     */
    int selectDeliveryNumByMap(Map<String, Object> selectMap);

    /**
     * Select by page list.
     * 分页查询发货
     *
     * @param searchPageUtil the search page util
     * @return the list
     */
    List<SellerDeliveryRecord> selectByPage(SearchPageUtil searchPageUtil);

    /**
     * 根据发票条件分页查询发货
     *
     * @param searchPageUtil the search page util
     * @return list list
     */
    List<SellerDeliveryRecord> queryDeliveryListByInvoice(SearchPageUtil searchPageUtil);

    /**
     * Query declivery by id seller delivery record.
     *
     * @param deliveryId the delivery id
     * @return the seller delivery record
     */
//根据编号查询发货信息
    SellerDeliveryRecord queryDecliveryById(String deliveryId);

    /**
     * Update by primary key selective.
     * 按条件更新发货单
     *
     * @param deliveryRecord the delivery record
     */
    void updateByPrimaryKeySelective(SellerDeliveryRecord deliveryRecord);

    /**
     * 根据条件查询发货详细
     *
     * @param map the map
     * @return list list
     */
    List<SellerDeliveryRecord> queryDeliveryRecordList(Map<String, Object> map);

    /**
     * Select not arrival list.
     * 查询所有未到货的发货单
     *
     * @return the list
     */
    List<SellerDeliveryRecord> selectNotArrival();

    /**
     * 根据条件修改订单对账状态
     *
     * @param map the map
     * @return int int
     */
    int updateDeliveryByReconciliation(Map<String,Object> map);

    /**
     * 查询未开发票数量
     *
     * @param map the map
     * @return the int
     */
    int queryCountByInvoice(Map<String,Object> map);

    /**
     * Query freight by rec id map.
     *
     * @param logisticsMap the logistics map
     * @return the map
     */
//根据发货编号查询运费
    Map<String,Object> queryFreightByRecId(Map<String,Object> logisticsMap);

    /**
     * Select by map list.
     * 根据map条件查询
     *
     * @param params the params
     * @return the list
     */
    List<SellerDeliveryRecord> selectByMap(Map<String, Object> params);

    /**
     * Add qrcode addr.
     * 更新发货单二维码
     * @param deliverNo  the deliver no
     * @param qrcodeAddr the qrcode addr
     */
    int addQrcodeAddr(@Param("deliverNo") String deliverNo,@Param("qrcodeAddr") String qrcodeAddr);

	SellerDeliveryRecord getByDeliverNo(@Param("deliverNo")String deliverNo);
}
