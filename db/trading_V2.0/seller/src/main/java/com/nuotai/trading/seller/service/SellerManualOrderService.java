package com.nuotai.trading.seller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.model.BuyProduct;
import com.nuotai.trading.model.BuyProductSku;
import com.nuotai.trading.seller.dao.SellerManualOrderMapper;
import com.nuotai.trading.seller.model.SellerManualOrder;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import com.nuotai.trading.service.BuyProductService;
import com.nuotai.trading.service.BuyProductSkuService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ShiroUtils;



@Service
@Transactional
public class SellerManualOrderService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerManualOrderService.class);

	@Autowired
	private SellerManualOrderMapper sellerManualOrderMapper;
	
	@Autowired
	private SellerOrderService sellerOrderService;
	
	@Autowired
	private SellerOrderSupplierProductService sellerOrderSupplierProductService;
	
	@Autowired
	private BuyProductService buyProductService;
	
	@Autowired
	private BuyProductSkuService productSkuService;
	
	public SellerManualOrder get(String id){
		return sellerManualOrderMapper.get(id);
	}
	
	public List<SellerManualOrder> queryList(Map<String, Object> map){
		return sellerManualOrderMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerManualOrderMapper.queryCount(map);
	}
	
	public void add(SellerManualOrder sellerManualOrder){
		sellerManualOrderMapper.add(sellerManualOrder);
	}
	
	public void update(SellerManualOrder sellerManualOrder){
		sellerManualOrderMapper.update(sellerManualOrder);
	}
	
	public void delete(String id){
		sellerManualOrderMapper.delete(id);
	}
	
	public SellerManualOrder selectByOrderId(String orderId){
		return sellerManualOrderMapper.selectByOrderId(orderId);
	}
	
	/**
	 * 处理订单中的数据
	 * @param manualOrder
	 * @return
	 */
	public Map<String, Object> toAddManualOrderMapper(String manualOrder){
		JSONObject json = new JSONObject();
		Map<String, Object> manualOrderMap = new HashMap<String, Object>();
		String[] manualOrderproductInforArr = manualOrder.split(";");
		manualOrderMap.put("buyers", manualOrderproductInforArr[0]);
		manualOrderMap.put("contact_person", manualOrderproductInforArr[1]);
		manualOrderMap.put("clientPhone", manualOrderproductInforArr[2]);
		manualOrderMap.put("purchasePrice", manualOrderproductInforArr[3]);
		manualOrderMap.put("otherFee", manualOrderproductInforArr[4]);
		manualOrderMap.put("paymentPrice", manualOrderproductInforArr[5]);
		manualOrderMap.put("date", manualOrderproductInforArr[6]);
		manualOrderMap.put("paymentType", manualOrderproductInforArr[7]);
		manualOrderMap.put("paymentPerson", manualOrderproductInforArr[8]);
		manualOrderMap.put("handler", manualOrderproductInforArr[9]);
		manualOrderMap.put("sellerId", manualOrderproductInforArr[11]);
		manualOrderMap.put("num", manualOrderproductInforArr[12]);
		manualOrderMap.put("discount_sum", manualOrderproductInforArr[13]);
		manualOrderMap.put("total", manualOrderproductInforArr[14]);
		manualOrderMap.put("annexVoucher", manualOrderproductInforArr.length == 16?manualOrderproductInforArr[15]:"");
		
		manualOrderMap.put("productInfor", manualOrderproductInforArr[10]);
		String[] productInforArr = manualOrderproductInforArr[10].split("&");
		JSONArray itemArray = new JSONArray();
		for (String infor : productInforArr){
			String[] inforArr = infor.split("/");
			JSONObject productInfor = new JSONObject();
			productInfor.put("proName", inforArr[0]);
			productInfor.put("unitName", inforArr[1]);
			productInfor.put("price", inforArr[2]);
			productInfor.put("goodsNumber", inforArr[3]);
			productInfor.put("updatePrice", inforArr[4]);
			productInfor.put("priceSum", inforArr[5]);
			productInfor.put("colorCode", inforArr[6]);
			productInfor.put("proCode", inforArr[7]);
			productInfor.put("skuCode", inforArr[8] != ""?inforArr[8]:"");
			productInfor.put("skuOid", inforArr[9]);
			productInfor.put("unitId", inforArr[10]);
			productInfor.put("remark", inforArr.length == 12?inforArr[11]:"");
			itemArray.add(productInfor);
		}
		json.put("itemArray", itemArray);
		manualOrderMap.put("productInforList", json);
		
		return manualOrderMap;
		
	}

	
	/**
	 * 保存手工添加订单
	 * @param params
	 * @return
	 */
	public JSONObject saveOrder(Map<String, Object> params){
		JSONObject json = new JSONObject();
		  try {
			  //录入订单表
		    	SellerOrder order = new SellerOrder();
				order.setId(ShiroUtils.getUid());
				order.setOrderCode(ShiroUtils.getOrderNum());
				order.setSuppId(params.containsKey("sellerId")?params.get("sellerId").toString():"");
				order.setSuppName(params.containsKey("suppName")?params.get("suppName").toString():"");
				order.setPerson(params.containsKey("contact_person")?params.get("contact_person").toString():"");
				order.setPhone(params.containsKey("clientPhone")?params.get("clientPhone").toString():"");
				order.setGoodsNum(Integer.parseInt(params.get("num").toString()));
				order.setGoodsPrice(new BigDecimal(params.get("total").toString()));
				order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
				order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
				order.setOrderKind(Constant.OrderKind.MT.getValue());//手动下单
				order.setRemark(params.containsKey("supplierRemark")?params.get("supplierRemark").toString():"");
				order.setIsDel(Constant.IsDel.NODEL.getValue());
				order.setCreateDate(new Date());
				order.setCreateId(ShiroUtils.getUserId());
				order.setCreateName(ShiroUtils.getUserName());
				order.setCompanyId(ShiroUtils.getCompId());
				order.setCompanyName(ShiroUtils.getCompanyName());
				//收货地址
				int isSince=params.containsKey("is_since")?Integer.parseInt(params.get("is_since").toString()):1;
				order.setIsSince(isSince);
				if(Constant.LogisticsType.logistis.getValue()==isSince){
					//需要物流	
					order.setPersonName(params.containsKey("personName")?params.get("personName").toString():"");//收货人
					order.setAddrName(params.containsKey("addrName")?params.get("addrName").toString():"");//详细地址
//					order.setAreaCode(params.containsKey("sellerId")?params.get("sellerId").toString():"");//区号
//					order.setPlaneNumber(params.containsKey("sellerId")?params.get("sellerId").toString():"");//座机号
					order.setReceiptPhone(params.containsKey("receiptPhone")?params.get("receiptPhone").toString():"");//手机号
				}
						
				//录入手工添加订单表
			    SellerManualOrder  sellerManualOrder = new SellerManualOrder();
			    sellerManualOrder.setId(ShiroUtils.getUid());
			    sellerManualOrder.setCompanyId(ShiroUtils.getCompId());
			    sellerManualOrder.setOrderId(order.getId());
			    sellerManualOrder.setOtherFee(params.containsKey("receiptPhone")?new BigDecimal(params.get("otherFee").toString()):new BigDecimal(0));
			    sellerManualOrder.setPurchasePrice(new BigDecimal(params.get("purchasePrice").toString()));
			    sellerManualOrder.setPaymentType(Integer.parseInt(params.get("paymentType").toString()));
			    SimpleDateFormat sdf =   new SimpleDateFormat( " yyyy-MM-dd" );
			    try {
					sellerManualOrder.setPaymentDate(params.containsKey("date")?sdf.parse((params.get("date").toString())):new Date());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			    sellerManualOrder.setPaymentPerson(params.get("paymentPerson").toString());
			    sellerManualOrder.setAnnexVoucher(params.containsKey("discount_sum")?params.get("discount_sum").toString():"");
			    sellerManualOrder.setHandler(params.containsKey("handler")?params.get("handler").toString():"");
			    sellerManualOrder.setCreateId(ShiroUtils.getUserId());
			    sellerManualOrder.setCreateName(ShiroUtils.getUserName());		    			    
			    
			    sellerManualOrderMapper.insertSellerManualOrder(sellerManualOrder);
			    		
				Map<String,Object> goodsMap = new HashMap<String,Object>();
				goodsMap.put("supplierCompanyId", order.getSuppId());
				goodsMap.put("clientCompanyId", order.getCompanyId());
				//商品信息
				String goodsStrArr = params.get("productInfor").toString();
				String[] goodsArr = goodsStrArr.split("&");
				for(String goodsStr : goodsArr){
					String[] goodsItem = goodsStr.split("/");
					SellerOrderSupplierProduct product = new SellerOrderSupplierProduct();
					product.setId(ShiroUtils.getUid());
					product.setOrderId(order.getId());
					product.setProductCode(goodsItem[7]);
					product.setProductName(goodsItem[0]);					
					product.setSkuCode(goodsItem[8]);
					product.setSkuName(goodsItem[6]);
					product.setBarcode(goodsItem[9]);
					product.setPrice(new BigDecimal(goodsItem[2]));
					product.setOrderNum(Integer.parseInt(goodsItem[3]));
					product.setDiscountMoney(new BigDecimal(goodsItem[4]));
					product.setTotalMoney(new BigDecimal(goodsItem[5]));
					product.setRemark(goodsItem.length==11?goodsItem[10]:"");
					sellerOrderSupplierProductService.insertManualOrder(product);
				}
				
				sellerOrderService.add(order);
				json.put("success", true);			
				json.put("msg", "保存成功！");
			} catch (Exception e) {
				json.put("success", false);
				json.put("msg", "保存失败！");
			}	
		return json;	
	}
	

	/**
	 * 发起审批后保存手工订单
	 * @param params
	 * @return
	 */
	public List<String> saveApprovalOrder(Map<String, Object> params) {
		List<String> idList = new ArrayList<String>();

		//先录入订单表
    	SellerOrder order = new SellerOrder();
		order.setId(ShiroUtils.getUid());
		order.setOrderCode(ShiroUtils.getOrderNum());
		order.setSuppId(params.containsKey("sellerId")?params.get("sellerId").toString():"");
		order.setSuppName(params.containsKey("buyers")?params.get("buyers").toString():"");
		order.setPerson(params.containsKey("contact_person")?params.get("contact_person").toString():"");
		order.setPhone(params.containsKey("clientPhone")?params.get("clientPhone").toString():"");
		order.setGoodsNum(Integer.parseInt(params.get("num").toString()));
		order.setGoodsPrice(new BigDecimal(params.get("total").toString()));
		order.setStatus(Constant.OrderStatus.WAITORDER.getValue());//待接单
		order.setIsCheck(Constant.IsCheck.WAITVERIFY.getValue());//待内部审批
		order.setOrderKind(Constant.OrderKind.MT.getValue());//手动下单
		order.setRemark(params.containsKey("supplierRemark")?params.get("supplierRemark").toString():"");
		order.setIsDel(Constant.IsDel.NODEL.getValue());
		order.setCreateDate(new Date());
		order.setCreateId(ShiroUtils.getUserId());
		order.setCreateName(ShiroUtils.getUserName());
		order.setCompanyId(ShiroUtils.getCompId());
		order.setCompanyName(ShiroUtils.getCompanyName());
		//收货地址
		int isSince=params.containsKey("is_since")?Integer.parseInt(params.get("is_since").toString()):1;
		order.setIsSince(isSince);
		if(Constant.LogisticsType.logistis.getValue()==isSince){
			//需要物流	
			order.setPersonName(params.containsKey("personName")?params.get("personName").toString():"");//收货人
			order.setAddrName(params.containsKey("addrName")?params.get("addrName").toString():"");//详细地址
//			order.setAreaCode(params.containsKey("sellerId")?params.get("sellerId").toString():"");//区号
//			order.setPlaneNumber(params.containsKey("sellerId")?params.get("sellerId").toString():"");//座机号
			order.setReceiptPhone(params.containsKey("receiptPhone")?params.get("receiptPhone").toString():"");//手机号
		}
				
		//录入手工添加订单表
	    SellerManualOrder  sellerManualOrder = new SellerManualOrder();
	    sellerManualOrder.setId(ShiroUtils.getUid());
	    sellerManualOrder.setCompanyId(ShiroUtils.getCompId());
	    sellerManualOrder.setOrderId(order.getId());
	    sellerManualOrder.setOtherFee(params.containsKey("receiptPhone")?new BigDecimal(params.get("otherFee").toString()):new BigDecimal(0));
	    sellerManualOrder.setPurchasePrice(new BigDecimal(params.get("purchasePrice").toString()));
	    sellerManualOrder.setPaymentType(Integer.parseInt(params.get("paymentType").toString()));
	    SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
	    try {
	    	String date = params.get("date").toString();
			sellerManualOrder.setPaymentDate(params.containsKey("date")?sdf.parse(date):new Date());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    sellerManualOrder.setPaymentPerson(params.get("paymentPerson").toString());
	    sellerManualOrder.setAnnexVoucher(params.containsKey("discount_sum")?params.get("discount_sum").toString():"");
	    sellerManualOrder.setHandler(params.containsKey("handler")?params.get("handler").toString():"");
	    sellerManualOrder.setCreateId(ShiroUtils.getUserId());
	    sellerManualOrder.setCreateName(ShiroUtils.getUserName());		    			    
	    sellerManualOrder.setCreateDate(new Date());
	    
	    sellerManualOrderMapper.insertSellerManualOrder(sellerManualOrder);
	    		
		Map<String,Object> goodsMap = new HashMap<String,Object>();
		goodsMap.put("supplierCompanyId", order.getSuppId());
		goodsMap.put("clientCompanyId", order.getCompanyId());
		//商品信息
		String goodsStrArr = params.get("productInfor").toString();
		String[] goodsArr = goodsStrArr.split("&");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split("/");
			SellerOrderSupplierProduct product = new SellerOrderSupplierProduct();
			product.setId(ShiroUtils.getUid());
			product.setOrderId(order.getId());		
			product.setProductName(goodsItem[0]);
			product.setPrice(new BigDecimal(goodsItem[2]));
			product.setOrderNum(Integer.parseInt(goodsItem[3]));
			product.setDiscountMoney(new BigDecimal(goodsItem[4]));
			product.setTotalMoney(new BigDecimal(goodsItem[5]));
			product.setSkuName(goodsItem[6]);
			product.setProductCode(goodsItem[7]);
			product.setSkuCode(goodsItem[8]);			
			product.setBarcode(goodsItem[9]);					
			product.setUnitId(goodsItem[10]);
			product.setRemark(goodsItem.length == 12?goodsItem[11]:"");
			product.setDeliveredNum(0);
			sellerOrderSupplierProductService.insertManualOrder(product);
		}
		
		sellerOrderService.add(order);
		idList.add(order.getId());
		return idList;
	}
	
	
	public JSONObject saveNewProduct(@RequestParam Map<String,Object> params){
		JSONObject jsonObject = new JSONObject();
		BuyProduct buyProduct = new BuyProduct();
		BuyProductSku buyProductSku = new BuyProductSku();
		
		String goodsStrArr = params.get("newProductInfor").toString();
		String[] goodsArr = goodsStrArr.split("&");
		for(String goodsStr : goodsArr){
			String[] goodsItem = goodsStr.split("/");
			buyProduct.setProductCode(goodsItem[0]);
			buyProduct.setProductName(goodsItem[1]);
			buyProductSku.setProductCode(goodsItem[0]);
			buyProductSku.setProductName(goodsItem[1]);
			buyProductSku.setSkuCode(goodsItem[2]);
			buyProductSku.setSkuName(goodsItem[3]);
			buyProductSku.setBarcode(goodsItem[4]);
			buyProductSku.setUnitId(goodsItem[5]);
			buyProductSku.setMinStock(Integer.parseInt(goodsItem[6]));
			buyProductSku.setStandardStock(Integer.parseInt(goodsItem[7]));
			buyProductSku.setPrice(new BigDecimal(goodsItem[8]));
			jsonObject=buyProductService.insertProduct(buyProduct);
			jsonObject=productSkuService.insertSelective(buyProductSku);
		}
		if(jsonObject.get("success").toString().equals(true)){
			jsonObject.put("success", true);
			jsonObject.put("msg", "新增商品成功！");
		}else{
			jsonObject.put("success", false);
			jsonObject.put("msg", "新增商品失败！");
		}
		
		return jsonObject;
	}
}
