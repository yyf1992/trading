package com.nuotai.trading.seller.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * The interface Seller order mapper.
 *
 * @author "
 * @date 2017 -08-25 16:00:47
 */
public interface SellerOrderMapper extends BaseDao<SellerOrder> {
	/**
	 * Select by page list.
	 *
	 * @param searchPageUtil the search page util
	 * @return the list
	 */
	List<SellerOrder> selectByPage(SearchPageUtil searchPageUtil);  //分页查询

	/**
	 * Select orders num by map int.
	 *
	 * @param params the params
	 * @return the int
	 */
	int selectOrdersNumByMap(Map<String, Object> params);  //订单数量

	/**
	 * 保存手工添加订单
	 *
	 * @param sellerOrder the seller order
	 */
	void insertSellerManualOrder(SellerOrder sellerOrder);

	/**
	 * 手工订单分页查询
	 *
	 * @param searchPageUtil the search page util
	 * @return list list
	 */
	List<SellerOrder> selectManualOrderByPage(SearchPageUtil searchPageUtil);

	/**
	 * 审批后对订单的修改
	 *
	 * @param sellerOrder the seller order
	 * @return int int
	 */
	int updateByPrimaryKeySelective(SellerOrder sellerOrder);

	/**
	 * 根据多个订单ID获取订单信息
	 *
	 * @param orderIds the order ids
	 * @return list list
	 */
	List<SellerOrder> queryListByOrderIds(String orderIds);

	/**
	 * Gets all content.
	 * 根据ID获取所有订单内容
	 *
	 * @param orderId the order id
	 * @return the all content
	 */
	SellerOrder getAllContent(String orderId);

	/**
	 * Select by map list.
	 * 根据查询条件查询
	 *
	 * @param params the params
	 * @return the list
	 */
	List<SellerOrder> selectByMap(Map<String, Object> params);

	/**
	 * Gets by order code.
	 * 根据订单号查询
	 * @param orderCode the order code
	 * @return the by order code
	 */
	SellerOrder getByOrderCode(String orderCode);

	//根据财务对账状态修改订单对账、付款状态
	int updateOrderBuyBill(Map<String, Object> params);
}
