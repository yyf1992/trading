package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyCustomer;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * 
 * 
 * @author dxl"
 * @date 2017-09-12 16:49:24
 */
@Component
public interface SBuyCustomerMapper extends BaseDao<SBuyCustomer> {
	//根据条件修改退货单对账状态
    int updateCustomerByReconciliation(Map<String,Object> map);
}
