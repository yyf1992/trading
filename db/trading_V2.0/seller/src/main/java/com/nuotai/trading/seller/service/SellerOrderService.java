package com.nuotai.trading.seller.service;

import com.nuotai.trading.dao.TradeVerifyHeaderMapper;
import com.nuotai.trading.dao.TradeVerifyPocessMapper;
import com.nuotai.trading.model.BuyProductSkuBom;
import com.nuotai.trading.model.TradeVerifyHeader;
import com.nuotai.trading.model.TradeVerifyPocess;
import com.nuotai.trading.seller.dao.SellerDeliveryRecordItemMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderMapper;
import com.nuotai.trading.seller.model.*;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.seller.dao.SellerManualOrderMapper;
import com.nuotai.trading.seller.dao.SellerOrderMapper;
import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.seller.model.buyer.BuyOrder;
import com.nuotai.trading.service.BuyProductSkuBomService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DateUtils;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;


/**
 * 卖家订单信息
 * @author gsf
 * @date 2017-08-17
 */
@Service
@Transactional
public class SellerOrderService {

	private static final Logger LOG = LoggerFactory.getLogger(SellerOrderService.class);

	@Autowired
	private SellerOrderMapper sellerOrderMapper;  //卖家订单信息
	@Autowired
	private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;  //卖家订单商品信息
	@Autowired
	private SellerManualOrderMapper sellerManualOrderMapper;
	@Autowired
	private SBuyOrderMapper buyOrderMapper;
	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;
	@Autowired
	private TradeVerifyHeaderMapper tradeVerifyHeaderMapper;
	@Autowired
	private TradeVerifyPocessMapper tradeVerifyPocessMapper;
	//BOM表
	@Autowired
	private BuyProductSkuBomService buyProductSkuBomService;
	

	public SellerOrder get(String id){
		return sellerOrderMapper.get(id);
	}
	public SellerOrder getByOrderCode(String orderCode) {
		return sellerOrderMapper.getByOrderCode(orderCode);
	}

	public List<SellerOrder> queryList(Map<String, Object> map){
		return sellerOrderMapper.queryList(map);
	}

	public int queryCount(Map<String, Object> map){
		return sellerOrderMapper.queryCount(map);
	}

	public void add(SellerOrder sellerOrder){
		sellerOrderMapper.add(sellerOrder);
	}

	public void update(SellerOrder sellerOrder){
		sellerOrderMapper.update(sellerOrder);
	}

	public void delete(String id){
		sellerOrderMapper.delete(id);
	}

	public int selectOrdersNumByMap(Map<String, Object> params) {
		return sellerOrderMapper.selectOrdersNumByMap(params);
	}


	/**
	 * 获得不同状态(互通客户购买商品)订单数
	 * @author gsf
	 * @date 2017-08-01
	 * @param params
	 */
	public void getInterworkGoodsOrderNum(Map<String, Object> params) {
		Map<String, Object> selectMap = new HashMap<String, Object>();
//		selectMap.put("orderKind", Constant.OrderKind.AT.getValue());//订单类型
		selectMap.put("isDel", Constant.IsDel.NODEL.getValue());
		selectMap.put("suppId", ShiroUtils.getCompId());  //卖家时作为供应商看
//		selectMap.put("suppId", "17061911301367785367");  //卖家时作为供应商看
//		selectMap.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
		//待接单
		selectMap.put("status", Constant.SellerOrderStatus.WAITORDER.getValue());
		int waitOrderNum = selectOrdersNumByMap(selectMap);
		params.put("waitOrderNum", waitOrderNum);
		//待发货
		selectMap.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		int waitDeliveryNum = selectOrdersNumByMap(selectMap);
		params.put("waitDeliveryNum", waitDeliveryNum);
		//发货中
		selectMap.put("status", Constant.SellerOrderStatus.DELIVERING.getValue());
		int deliveringNum = selectOrdersNumByMap(selectMap);
		params.put("deliveringNum", deliveringNum);
		//发货完成
		selectMap.put("status", Constant.SellerOrderStatus.DELIVERED.getValue());
		int deliveredNum = selectOrdersNumByMap(selectMap);
		params.put("deliveredNum", deliveredNum);
		//已收货
		selectMap.put("status", Constant.SellerOrderStatus.RECEIVED.getValue());
		int receivedNum = selectOrdersNumByMap(selectMap);
		params.put("receivedNum", receivedNum);
		//已取消（卖家这里不用已取消状态）
		selectMap.put("status", Constant.SellerOrderStatus.CANCEL.getValue());
		int cancelNum = selectOrdersNumByMap(selectMap);
		params.put("cancelNum", cancelNum);
		//已驳回
		selectMap.put("status", Constant.SellerOrderStatus.REJECT.getValue());
		int rejectNum = selectOrdersNumByMap(selectMap);
		params.put("rejectNum", rejectNum);
		//已终止
		selectMap.put("status", Constant.SellerOrderStatus.STOP.getValue());
		int stopNum = selectOrdersNumByMap(selectMap);
		params.put("stopNum", stopNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			case 5:
				params.put("interest", "5");
				break;
			case 6:
				params.put("interest", "6");
				break;
			case 7:
				params.put("interest", "7");
				break;
			case 8:
				params.put("interest", "8");
				break;
			case 99:
				params.put("interest", "99");
				break;
			default:
				params.put("interest", "99");
				break;
		}
	}

	/**
	 * 获取（互通客户购买商品）数据
	 * @author gsf
	 * @date 2017-08-01
	 * @param searchPageUtil
	 * @param params
	 */
	public SearchPageUtil loadInterworkGoodsData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
//		params.put("orderKind", Constant.OrderKind.AT.getValue());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("suppId", ShiroUtils.getCompId());
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		//审核状态，暂时忽略
		if("0".equals(interest)){
			//待接单
			params.put("status", Constant.OrderStatus.WAITORDER.getValue());
		}else if("1".equals(interest)){
			//待发货
			params.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		}else if("2".equals(interest)){
			//发货中(待收货)
			params.put("status", Constant.SellerOrderStatus.DELIVERING.getValue());
		}else if("3".equals(interest)){
			//发货完成
			params.put("status", Constant.SellerOrderStatus.DELIVERED.getValue());
		}else if("4".equals(interest)){
			//已收货
			params.put("status", Constant.SellerOrderStatus.RECEIVED.getValue());
		}else if("5".equals(interest)){
			//交易完成
			params.put("status", Constant.SellerOrderStatus.FINISH.getValue());
		}else if("6".equals(interest)){
			//已取消订单
			params.put("status", Constant.SellerOrderStatus.CANCEL.getValue());
		}else if("7".equals(interest)){
			//已驳回订单
			params.put("status", Constant.SellerOrderStatus.REJECT.getValue());
		}else if("8".equals(interest)){
			//已终止订单
			params.put("status", Constant.SellerOrderStatus.STOP.getValue());
		}
		//订单类型（0:采购订单 1:换货订单）
		params.put("orderType", "0");
		searchPageUtil.setObject(params);
		List<SellerOrder> orderList = sellerOrderMapper.selectByPage(searchPageUtil);
		if(!orderList.isEmpty()){
			for(SellerOrder order : orderList){
				String orderId = order.getId();
				List<SellerOrderSupplierProduct> orderProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
				/***根据条形码来组装商品信息，条形码一样就合并商品***/
				boolean showDeliveryButton = false;
				Map<String,SellerOrderSupplierProduct> productMap = new HashMap<>();
				for (SellerOrderSupplierProduct product:orderProductList) {
					int orderNum = ObjectUtil.isEmpty(product.getOrderNum())?0:product.getOrderNum();
					int deliveryNum = ObjectUtil.isEmpty(product.getDeliveredNum())?0:product.getDeliveredNum();
					int waitDeliveryNum = ObjectUtil.isEmpty(product.getWaitDeliveryNum())?0:product.getWaitDeliveryNum();
					List<Map<String,Object>> deliveryList = sellerDeliveryRecordItemMapper.getDeliveryByItemIdList(product.getId());
					if(productMap.containsKey(product.getBarcode())){
						SellerOrderSupplierProduct oldProduct = productMap.get(product.getBarcode());
						List<Map<String,Object>> deliveryListOld = oldProduct.getDeliveryList();
						int oldNum = ObjectUtil.isEmpty(oldProduct.getOrderNum())?0:oldProduct.getOrderNum();
						int newNum = ObjectUtil.isEmpty(product.getOrderNum())?0:product.getOrderNum();
						int oldArrivalNum = ObjectUtil.isEmpty(oldProduct.getArrivalNum())?0:oldProduct.getArrivalNum();
						int arrivalNum = ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
						int oldDeliveryNum = ObjectUtil.isEmpty(oldProduct.getDeliveredNum())?0:oldProduct.getDeliveredNum();
						int newDeliveryNum = ObjectUtil.isEmpty(product.getDeliveredNum())?0:product.getDeliveredNum();
						product.setOrderNum(oldNum+newNum);
						product.setArrivalNum(oldArrivalNum+arrivalNum);
						product.setDeliveredNum(oldDeliveryNum+newDeliveryNum);
						deliveryList.addAll(deliveryListOld);
					}
					product.setDeliveryList(deliveryList);
					productMap.put(product.getBarcode(),product);
					if(orderNum >(deliveryNum+waitDeliveryNum)){
						showDeliveryButton = true;
					}
				}
				order.setShowDeliveryButton(showDeliveryButton);
				List<SellerOrderSupplierProduct> addList = new ArrayList<>(productMap.size());
				for (Map.Entry<String,SellerOrderSupplierProduct> entry : productMap.entrySet()){
					addList.add(entry.getValue());
				}
				//到货数量总计
				int arrivalTotalNum = 0;
				for (SellerOrderSupplierProduct product:addList) {
					arrivalTotalNum += ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
				}
				order.setArrivalTotalNum(arrivalTotalNum);
				order.setSupplierProductList(addList);
				/***计算发货单总量***/
				Map<String,Object> map = new HashMap<>(1);
				map.put("orderId",orderId);
				/*List<SellerDeliveryRecordItem> deliveryRecordItems = sellerDeliveryRecordItemMapper.queryList(map);
				int deliveryTotalNum = 0;
				for (SellerDeliveryRecordItem item:deliveryRecordItems) {
					deliveryTotalNum += ObjectUtil.isEmpty(item.getDeliveryNum())?0:item.getDeliveryNum();
				}
				order.setDeliveryTotalNum(deliveryTotalNum);*/
				/***计算发货中的数量***/
				List<SellerDeliveryRecordItem> deliveryRecordItems2 = sellerDeliveryRecordItemMapper.queryDeliveringList(map);
				int deliveringNum = 0;
				for (SellerDeliveryRecordItem item:deliveryRecordItems2) {
					deliveringNum += ObjectUtil.isEmpty(item.getDeliveryNum())?0:item.getDeliveryNum();
				}
				order.setDeliveringTotalNum(deliveringNum);
			}
		}
		searchPageUtil.getPage().setList(orderList);

		return searchPageUtil;
	}

	/**
	 * 根据ID获取订单信息
	 * @param orderIds
	 * @return
	 */
	public List<SellerOrder> queryListByOrderIds(String orderIds) {
		orderIds = orderIds.replaceAll(",","','");
		orderIds = "'"+orderIds+"'";
		List<SellerOrder> orderList = sellerOrderMapper.queryListByOrderIds(orderIds);
		//订单产品信息
		for (SellerOrder order:orderList) {
			List<SellerOrderSupplierProduct> supplierProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(order.getId());
			//将已经发货的商品排除
			Iterator it = supplierProductList.iterator();
			while(it.hasNext()) {
				SellerOrderSupplierProduct product = (SellerOrderSupplierProduct) it.next();
				int orderNum = product.getOrderNum();
				int deliveryNum = 0;
				Map<String,Object> map = new HashMap<>(1);
				map.put("orderItemId",product.getId());
				List<SellerDeliveryRecordItem> deliveryRecordItems = sellerDeliveryRecordItemMapper.queryList(map);
				for (SellerDeliveryRecordItem deliveryRecordItem:deliveryRecordItems) {
					deliveryNum += ObjectUtil.isEmpty(deliveryRecordItem.getDeliveryNum())?0:deliveryRecordItem.getDeliveryNum();
				}
				if(orderNum-deliveryNum<=0){
					it.remove();
				}
			}
			order.setSupplierProductList(supplierProductList);
		}
		return orderList;
	}

	/**
	 * Query list by order ids merge item list.
	 * 查询订单信息，明细按条形码合并
	 * @param orderIds the order ids
	 * @return the list
	 */
	public List<SellerOrder> queryListByOrderIdsMergeItem(String orderIds) {
		orderIds = orderIds.replaceAll(",","','");
		orderIds = "'"+orderIds+"'";
		List<SellerOrder> orderList = sellerOrderMapper.queryListByOrderIds(orderIds);
		//订单产品信息
		for (SellerOrder order:orderList) {
			List<SellerOrderSupplierProduct> supplierProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(order.getId());
			List<SellerOrderSupplierProduct> addList = new ArrayList<>();
			Map<String,SellerOrderSupplierProduct> productMap = new HashMap<>();
			Iterator it = supplierProductList.iterator();
			while (it.hasNext()){
				SellerOrderSupplierProduct soItem = (SellerOrderSupplierProduct)it.next();
				if(soItem.getOrderNum()-soItem.getDeliveredNum()-soItem.getWaitDeliveryNum()<=0){
					it.remove();
				}else {
					if(productMap.containsKey(soItem.getBarcode())){
						SellerOrderSupplierProduct oldProduct = productMap.get(soItem.getBarcode());
						soItem.setOrderNum(soItem.getOrderNum()+oldProduct.getOrderNum());
						soItem.setDeliveredNum(soItem.getDeliveredNum()+oldProduct.getDeliveredNum());
						soItem.setWaitDeliveryNum(soItem.getWaitDeliveryNum()+oldProduct.getWaitDeliveryNum());
					}
					productMap.put(soItem.getBarcode(),soItem);
				}
			}
			for (Map.Entry<String,SellerOrderSupplierProduct> entry : productMap.entrySet()){
				addList.add(entry.getValue());
			}
			order.setSupplierProductList(addList);
		}
		return orderList;
	}

	/**
	 *  获得不同状态的手工订单数
	 *  @author wangli
	 *  @param params
	 */
	public Map<String, Object> getManualOrderNum(Map<String, Object> params) {
		Map<String, Object> selectMap = new HashMap<String, Object>();
//		selectMap.put("orderKind", Constant.OrderKind.MT.getValue());//手工订单类型
		selectMap.put("isDel", Constant.IsDel.NODEL.getValue());
		selectMap.put("companyId", ShiroUtils.getCompId());
		

		//待发货
		selectMap.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		int shipNum = selectOrdersNumByMap(selectMap);
		params.put("shipNum", shipNum);
		
		selectMap.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
		//交易已完成
		selectMap.put("status", Constant.SellerOrderStatus.FINISH.getValue());
		int overNum = selectOrdersNumByMap(selectMap);
		params.put("overNum", overNum);
		
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):0;
		switch (tabId) {
			case 0:
				params.put("interest", "0");
				break;
			case 1:
				params.put("interest", "1");
				break;
			case 2:
				params.put("interest", "2");
				break;
			case 3:
				params.put("interest", "3");
				break;
			case 4:
				params.put("interest", "4");
				break;
			case 5:
				params.put("interest", "5");
				break;
			case 6:
				params.put("interest", "6");
				break;
			case 7:
				params.put("interest", "7");
				break;
			case 99:
				params.put("interest", "99");
				break;
			default:
				params.put("interest", "99");
				break;
		}
		params.put("tabId", tabId);
		
		return params;

	}
	
	/**
	 * 获得手工下单商品订单数据
	 * @author wangli
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public SearchPageUtil loadManualData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
//		params.put("orderKind", Constant.OrderKind.MT.getValue());//订单类型0，自动；1，手动
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("companyId",ShiroUtils.getCompId());
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		if("0".equals(interest)){
			//待接单
			params.put("status", Constant.SellerOrderStatus.WAITORDER.getValue());
		}else if("1".equals(interest)){
			//待发货
			params.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		}else if("2".equals(interest)){
			//发货中(待收货)
			params.put("status", Constant.SellerOrderStatus.DELIVERING.getValue());
		}else if("3".equals(interest)){
			//发货完成
			params.put("status", Constant.SellerOrderStatus.DELIVERED.getValue());
		}else if("4".equals(interest)){
			//已收货
			params.put("status", Constant.SellerOrderStatus.RECEIVED.getValue());
		}else if("5".equals(interest)){
			//交易完成
			params.put("status", Constant.SellerOrderStatus.FINISH.getValue());
		}else if("6".equals(interest)){
			//已取消订单
			params.put("status", Constant.SellerOrderStatus.CANCEL.getValue());
		}
		else if("7".equals(interest)){
			//已驳回订单
			params.put("status", Constant.SellerOrderStatus.REJECT.getValue());
		}
		searchPageUtil.setObject(params);
		List<SellerOrder> orderList = sellerOrderMapper.selectManualOrderByPage(searchPageUtil);
		if(!orderList.isEmpty()){
			for(SellerOrder order : orderList){
				SellerManualOrder sellerManualOrder = sellerManualOrderMapper.selectByOrderId(order.getId());
				order.setSellerManualOrder(sellerManualOrder);				
				List<SellerOrderSupplierProduct> supplierProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(order.getId());
				order.setSupplierProductList(supplierProductList);
			}
		}
		searchPageUtil.getPage().setList(orderList);
		return searchPageUtil;
	}
	
	/**
	 * 调用审核通过时
	 * @param id
	 * @return
	 */
	public JSONObject approvalMOSuccess(String id) {
		JSONObject json = new JSONObject();
		SellerOrder order = new SellerOrder();
		order.setId(id);
		order.setIsCheck(Constant.IsCheck.ALREADYVERIFY.getValue());
		order.setStatus(Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		int result = sellerOrderMapper.updateByPrimaryKeySelective(order);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 调用审核不通过时
	 * @param id
	 * @return
	 */
	public JSONObject approvalMOError(String id) {
		JSONObject json = new JSONObject();
		SellerOrder order = new SellerOrder();
		order.setId(id);
		order.setIsCheck(Constant.IsCheck.VERIFYNOTPASSED.getValue());
		order.setStatus(Constant.SellerOrderStatus.REJECT.getValue());
		int result = sellerOrderMapper.updateByPrimaryKeySelective(order);
		if(result>0){
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 卖家接单
	 * @param orderId
	 */
    public void approveLinkOrder(String orderId) {
    	//卖家订单状态修改
		SellerOrder sellerOrder = new SellerOrder();
		sellerOrder.setId(orderId);
		sellerOrder.setIsCheck(Constant.IsCheck.ALREADYVERIFY.getValue());
		sellerOrder.setStatus(Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		sellerOrderMapper.updateByPrimaryKeySelective(sellerOrder);
		//买家订单状态修改
		sellerOrder = sellerOrderMapper.get(orderId);
		String buyerOrderId = ObjectUtil.isEmpty(sellerOrder)?"":sellerOrder.getBuyerOrderId();
		if(!ObjectUtil.isEmpty(buyerOrderId)){
			BuyOrder buyOrder = buyOrderMapper.selectByPrimaryKey(buyerOrderId);
			buyOrder.setStatus(Constant.OrderStatus.WAITDELIVERY.getValue());//待对方发货
			buyOrderMapper.updateByPrimaryKeySelective(buyOrder);
			//发送钉钉消息
			String message = "您的采购单【"+buyOrder.getOrderCode()+"】已被卖家接单，接单日期：【"+DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss")+"】";
			DingDingUtils.sendDingDingMessage(buyOrder.getCreateId(),message,"卖家接单通知");
		}
    }
    
    /**
     * 获取客户换货代发货订单
     * @param searchPageUtil
     * @param params
     * @return
     */
	public SearchPageUtil loadExchangeGoodsData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
//		params.put("orderKind", Constant.OrderKind.AT.getValue());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("suppId", ShiroUtils.getCompId());
		//审核状态，暂时忽略
//		params.put("isCheck", Constant.IsCheck.ALREADYVERIFY.getValue());
		//待发货
		params.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		params.put("deliverType", "1");//
		searchPageUtil.setObject(params);
		List<SellerOrder> orderList = sellerOrderMapper.selectByPage(searchPageUtil);
		if(!orderList.isEmpty()){
			for(SellerOrder order : orderList){
				String orderId = order.getId();
				List<SellerOrderSupplierProduct> supplierProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
				order.setSupplierProductList(supplierProductList);
			}
		}
		searchPageUtil.getPage().setList(orderList);

		return searchPageUtil;
	}

	/**
	 * Cancel link order.
	 * 取消订单
	 * @param params
	 */
	public void cancelLinkOrder(Map<String,Object> params) {
		String orderId = String.valueOf(params.get("orderId"));
		String sellerRejectReason = String.valueOf(params.get("sellerRejectReason"));
		SellerOrder sellerOrder = new SellerOrder();
		sellerOrder.setId(orderId);
		sellerOrder.setStatus(Constant.SellerOrderStatus.REJECT.getValue());
		sellerOrder.setSellerRejectReason(sellerRejectReason);
		sellerOrderMapper.updateByPrimaryKeySelective(sellerOrder);
		sellerOrder = sellerOrderMapper.get(orderId);
		if(!ObjectUtil.isEmpty(sellerOrder)){
			String buyOrderId = sellerOrder.getBuyerOrderId();
			BuyOrder bo = new BuyOrder();
			bo.setId(buyOrderId);
			//卖家驳回
			bo.setStatus(Constant.OrderStatus.REJECT.getValue());
			bo.setSellerRejectReason(sellerRejectReason);
			int result  = 0;
			result = buyOrderMapper.updateByPrimaryKeySelective(bo);
			if(result > 0 && !StringUtils.isEmpty(buyOrderId)){
				TradeVerifyHeader header = tradeVerifyHeaderMapper.getVerifyHeaderByRelatedId(buyOrderId);
				header.setStatus("2");
				tradeVerifyHeaderMapper.update(header);
				if(!ObjectUtil.isEmpty(header)){
					//删除所有未审批的数据
					tradeVerifyPocessMapper.deleteNotVerify(header.getId());
					//获得最大index
					int maxIndex = tradeVerifyPocessMapper.getMaxIndex(header.getId());
					//添加审批
					TradeVerifyPocess pocess = new TradeVerifyPocess();
					pocess.setId(ShiroUtils.getUid());
					pocess.setHeaderId(header.getId());
					pocess.setUserId(ShiroUtils.getUserId());
					pocess.setUserName(ShiroUtils.getUserName());
					pocess.setStatus("6");//卖家驳回
					pocess.setRemark(sellerRejectReason);
					pocess.setVerifyIndex(maxIndex+1);
					pocess.setStartDate(new Date());
					pocess.setEndDate(new Date());
					tradeVerifyPocessMapper.add(pocess);
					//给申请人发送钉钉消息
					String message = "您的采购单【"+sellerOrder.getOrderCode()+"】被卖家驳回，驳回理由：【"+sellerRejectReason+"】，驳回日期："+DateUtils.format(new Date(), "yyyy-MM-dd HH:mm");
					DingDingUtils.sendDingDingMessage(header.getCreateId(),message,"采购订单被驳回");
				}
			}
		}
	}

	/**
	 * Gets export data.
	 * 订单导出
	 * @param params the params
	 * @return the export data
	 */
	public List<SellerOrderExportData> getExportData(Map<String, Object> params) {
		List<SellerOrderExportData> exportDataList = new ArrayList<>();
//		params.put("orderKind", Constant.OrderKind.AT.getValue());
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("suppId", ShiroUtils.getCompId());
//		params.put("statusNotIn","('6','7','8')");
		String interest = params.containsKey("interest")?params.get("interest").toString():"0";
		//审核状态，暂时忽略
		if("0".equals(interest)){
			//待接单
			params.put("status", Constant.OrderStatus.WAITORDER.getValue());
		}else if("1".equals(interest)){
			//待发货
			params.put("status", Constant.SellerOrderStatus.WAITDELIVERY.getValue());
		}else if("2".equals(interest)){
			//发货中(待收货)
			params.put("status", Constant.SellerOrderStatus.DELIVERING.getValue());
		}else if("3".equals(interest)){
			//发货完成
			params.put("status", Constant.SellerOrderStatus.DELIVERED.getValue());
		}else if("4".equals(interest)){
			//已收货
			params.put("status", Constant.SellerOrderStatus.RECEIVED.getValue());
		}else if("5".equals(interest)){
			//交易完成
			params.put("status", Constant.SellerOrderStatus.FINISH.getValue());
		}else if("6".equals(interest)){
			//已取消订单
			params.put("status", Constant.SellerOrderStatus.CANCEL.getValue());
		}else if("7".equals(interest)){
			//已驳回订单
			params.put("status", Constant.SellerOrderStatus.REJECT.getValue());
		}else if("8".equals(interest)){
			//已终止订单
			params.put("status", Constant.SellerOrderStatus.STOP.getValue());
		}
		//订单类型（0:采购订单 1:换货订单）
		params.put("orderType", "0");
		List<SellerOrder> orderList = sellerOrderMapper.selectByMap(params);
		if(!orderList.isEmpty()){
			for(SellerOrder order : orderList){
				String orderId = order.getId();
				List<SellerOrderSupplierProduct> orderProductList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
				/***根据条形码来组装商品信息，条形码一样就合并商品***/
				Map<String,SellerOrderSupplierProduct> productMap = new HashMap<>();
				for (SellerOrderSupplierProduct product:orderProductList) {
					if(productMap.containsKey(product.getBarcode())){
						SellerOrderSupplierProduct oldProduct = productMap.get(product.getBarcode());
						int oldOrderNum = ObjectUtil.isEmpty(oldProduct.getOrderNum())?0:oldProduct.getOrderNum();
						int orderNum = ObjectUtil.isEmpty(product.getOrderNum())?0:product.getOrderNum();
						int oldArrivalNum = ObjectUtil.isEmpty(oldProduct.getArrivalNum())?0:oldProduct.getArrivalNum();
						int arrivalNum = ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum();
						BigDecimal oldTotalMoney = ObjectUtil.isEmpty(oldProduct.getTotalMoney())?new BigDecimal(0):oldProduct.getTotalMoney();
						BigDecimal totalMoney = ObjectUtil.isEmpty(product.getTotalMoney())?new BigDecimal(0):product.getTotalMoney();
						product.setOrderNum(oldOrderNum+orderNum);
						product.setArrivalNum(oldArrivalNum+arrivalNum);
						product.setUnArrivalNum(oldOrderNum+orderNum-oldArrivalNum-arrivalNum);
						product.setTotalMoney(oldTotalMoney.add(totalMoney));
					}else {
						product.setOrderNum(ObjectUtil.isEmpty(product.getOrderNum())?0:product.getOrderNum());
						product.setArrivalNum(ObjectUtil.isEmpty(product.getArrivalNum())?0:product.getArrivalNum());
						product.setUnArrivalNum(product.getOrderNum()-product.getArrivalNum());
					}
					productMap.put(product.getBarcode(),product);
				}
				for (Map.Entry<String,SellerOrderSupplierProduct> entry : productMap.entrySet()){
					SellerOrderSupplierProduct product = entry.getValue();
					//这里组装订单明细的发货信息
					Map<String,Object> deliveryMap = new HashMap<>(2);
					deliveryMap.put("orderId",orderId);
					deliveryMap.put("barcode",product.getBarcode());
					List<SellerDeliveryRecordItem> deliveryRecordItemList = sellerDeliveryRecordItemMapper.queryList(deliveryMap);
					if(!ObjectUtil.isEmpty(deliveryRecordItemList)&&deliveryRecordItemList.size()>0){
						Map<String,SellerDeliveryRecordItem> deliveryRecordItemMap = new HashMap<>();
						for (SellerDeliveryRecordItem deliveryItem:deliveryRecordItemList) {
							if(deliveryRecordItemMap.containsKey(deliveryItem.getDeliverNo())){
								SellerDeliveryRecordItem oldItem = deliveryRecordItemMap.get(deliveryItem.getDeliverNo());
								int oldDeliveryNum = ObjectUtil.isEmpty(oldItem.getDeliveryNum())?0:oldItem.getDeliveryNum();
								int oldArrivalNum = ObjectUtil.isEmpty(oldItem.getArrivalNum())?0:oldItem.getArrivalNum();
								int newDeliveryNum = ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum();
								int newArrivalNum = ObjectUtil.isEmpty(deliveryItem.getArrivalNum())?0:deliveryItem.getArrivalNum();
								deliveryItem.setDeliveryNum(oldDeliveryNum+newDeliveryNum);
								deliveryItem.setArrivalNum(oldArrivalNum+newArrivalNum);

							}else {
								deliveryItem.setDeliveryNum(ObjectUtil.isEmpty(deliveryItem.getDeliveryNum())?0:deliveryItem.getDeliveryNum());
								deliveryItem.setArrivalNum(ObjectUtil.isEmpty(deliveryItem.getArrivalNum())?0:deliveryItem.getArrivalNum());
							}
							deliveryRecordItemMap.put(deliveryItem.getDeliverNo(),deliveryItem);
						}
						String deliveryStr = "";
						int deliveryNumSum = 0;
						int arrivalNumSum = 0;
						Date arrivalDate = null;
						for(Map.Entry<String,SellerDeliveryRecordItem> entryDelivery : deliveryRecordItemMap.entrySet()){
							SellerDeliveryRecordItem deliveryRecordItem = entryDelivery.getValue();
							//发货单号
							String deliveryCode = deliveryRecordItem.getDeliverNo();
							//发货数量
							int deliveryNum = deliveryRecordItem.getDeliveryNum();
							//到货数量
							int arrivalNum = ObjectUtil.isEmpty(deliveryRecordItem.getArrivalNum())?0:deliveryRecordItem.getArrivalNum();
							deliveryStr += "发货单号:"+ deliveryCode+",发货数量:"+deliveryNum+",";
							if(deliveryRecordItem.getArrivalDate() == null){
								deliveryStr += "到货数量:未到货";
							}else{
								deliveryStr += "到货数量:"+arrivalNum+",到货日期:"+DateUtils.format(deliveryRecordItem.getArrivalDate())+";";
								//到货日期
								if(arrivalDate != null){
									if(arrivalDate.getTime()<deliveryRecordItem.getArrivalDate().getTime()){
										arrivalDate = deliveryRecordItem.getArrivalDate();
									}
								}else{
									arrivalDate = deliveryRecordItem.getArrivalDate();
								}
							}
							deliveryStr += "\n";
							deliveryNumSum+=deliveryNum;
							arrivalNumSum+=arrivalNum;
						}
						SellerOrderExportData exportData = new SellerOrderExportData();
						exportData.setOrderId(orderId);
						exportData.setOrderCode(order.getOrderCode());
						exportData.setOrderDate(order.getCreateDate());
						exportData.setOrderStatus(String.valueOf(order.getStatus()));
						exportData.setProductCode(product.getProductCode());
						exportData.setProductName(product.getProductName());
						exportData.setSkuCode(product.getSkuCode());
						exportData.setSkuName(product.getSkuName());
						exportData.setBarcode(product.getBarcode());
						exportData.setOrderNum(product.getOrderNum());
						exportData.setArrivalNum(arrivalNumSum);
						exportData.setUnArrivalNum(product.getUnArrivalNum());
						exportData.setPrice(product.getPrice());
						exportData.setTotalPrice(product.getTotalMoney());
						exportData.setReqArrivalDate(product.getPredictArred());
						exportData.setDeliverNo(deliveryStr);
						exportData.setDeliveryNum(deliveryNumSum);
						exportData.setBuyerName(order.getCompanyName());
						exportData.setArrivalDate(arrivalDate);
						exportData.setOrderRemark(order.getRemark());
						exportData.setOrderItemRemark(product.getRemark());
						exportDataList.add(exportData);
					}else {
						//没有发货记录，则只组装商品信息
						SellerOrderExportData exportData = new SellerOrderExportData();
						exportData.setOrderId(orderId);
						exportData.setOrderCode(order.getOrderCode());
						exportData.setOrderDate(order.getCreateDate());
						exportData.setOrderStatus(String.valueOf(order.getStatus()));
						exportData.setProductCode(product.getProductCode());
						exportData.setProductName(product.getProductName());
						exportData.setSkuCode(product.getSkuCode());
						exportData.setSkuName(product.getSkuName());
						exportData.setBarcode(product.getBarcode());
						exportData.setOrderNum(product.getOrderNum());
						exportData.setUnArrivalNum(product.getOrderNum());
						exportData.setPrice(product.getPrice());
						exportData.setTotalPrice(product.getTotalMoney());
						exportData.setReqArrivalDate(product.getPredictArred());
						exportData.setBuyerName(order.getCompanyName());
						exportData.setOrderRemark(order.getRemark());
						exportData.setOrderItemRemark(product.getRemark());
						exportDataList.add(exportData);
					}
				}
			}
		}
		return exportDataList;
    }


	/**
	 * Gets not arrival export data.
	 * 未到货订单导出
	 * @param map the map
	 * @return the not arrival export data
	 */
	public List<SellerOrderExportData> getNotArrivalExportData(Map<String, Object> map) {
		List<SellerOrderExportData> exportDataList = new ArrayList<>();
//		map.put("orderKind", Constant.OrderKind.AT.getValue());
		map.put("isDel", Constant.IsDel.NODEL.getValue());
		map.put("suppId", ShiroUtils.getCompId());
		map.put("orderType", "0");
		map.put("statusNotIn","('6','7','8')");
		List<SellerOrderSupplierProduct> orderItemList = sellerOrderSupplierProductMapper.getNotArrivalExportData(map);
		for (SellerOrderSupplierProduct item:orderItemList){
			SellerOrderExportData exportData = new SellerOrderExportData();
			//根据商品条形码查询物料配置
			map.put("barcode", item.getBarcode());
			BuyProductSkuBom buyProductSkuBom = buyProductSkuBomService.selectByBarcode(map);
			if(!ObjectUtil.isEmpty(buyProductSkuBom)){
				exportData.setBuyProductSkuBom(buyProductSkuBom);
			}
			exportData.setOrderId(item.getOrderId());
			exportData.setOrderCode(item.getOrderCode());
			exportData.setOrderDate(item.getOrderDate());
			exportData.setOrderStatus(String.valueOf(item.getOrderStatus()));
			exportData.setProductCode(item.getProductCode());
			exportData.setProductName(item.getProductName());
			exportData.setSkuCode(item.getSkuCode());
			exportData.setSkuName(item.getSkuName());
			exportData.setBarcode(item.getBarcode());
			exportData.setOrderNum(item.getOrderNum());
			exportData.setArrivalNum(ObjectUtil.isEmpty(item.getArrivalNum())?0:item.getArrivalNum());
			exportData.setUnArrivalNum(ObjectUtil.isEmpty(item.getUnArrivalNum())?0:item.getUnArrivalNum());
			exportData.setUnArrivalTotalPrice(item.getPrice().multiply(new BigDecimal(ObjectUtil.isEmpty(item.getUnArrivalNum())?0:item.getUnArrivalNum())));
			exportData.setPrice(item.getPrice());
			exportData.setTotalPrice(item.getTotalMoney());
			exportData.setReqArrivalDate(item.getPredictArred());
			exportData.setBuyerName(item.getBuyerName());
			exportData.setOrderItemRemark(item.getRemark());
			exportData.setOrderRemark(item.getOrderRemark());
			exportDataList.add(exportData);
		}
		return exportDataList;
    }
}
