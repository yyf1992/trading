package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliationAdvance;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author "
 * @date 2018-02-10 14:13:51
 */
@Component
public interface SBuyBillReconciliationAdvanceMapper extends BaseDao<SBuyBillReconciliationAdvance> {
    //预付款列表查询
    List<SBuyBillReconciliationAdvance> queryAdvanceList(SearchPageUtil searchPageUtil);
    //根据状态查询数量
    int getAdvanceCountByStatus(Map<String, Object> map);
    //根据供应商查询下单人
    List<Map<String,Object>> queryUserBuyCompany(Map<String, Object> map);
    //添加预付款信息
    int saveAdvanceInfo(SBuyBillReconciliationAdvance buyBillReconciliationAdvance);
    //修改预付款信息
    int updateAdvanceInfo(SBuyBillReconciliationAdvance buyBillReconciliationAdvance);
    //根据编号查询预付款详情
    SBuyBillReconciliationAdvance queryAdvanceInfo(Map<String, Object> advanceMap);
}
