package com.nuotai.trading.seller.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyCompanyMapper;
import com.nuotai.trading.model.BuyAttachment;
import com.nuotai.trading.model.BuyCompany;
import com.nuotai.trading.seller.dao.SellerContractMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyContractMapper;
import com.nuotai.trading.seller.model.buyer.BuyContract;
import com.nuotai.trading.seller.model.SellerContract;
import com.nuotai.trading.service.AttachmentService;
import com.nuotai.trading.utils.*;
import com.nuotai.trading.utils.json.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
@Transactional
public class SellerContractService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerContractService.class);

	@Autowired
	private SellerContractMapper contractMapper;
	@Autowired
	private BuyCompanyMapper buyCompanyMapper;
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private SBuyContractMapper sBuyContractMapper;

	public SellerContract get(String id){
		return contractMapper.get(id);
	}
	
	public List<SellerContract> queryList(Map<String, Object> map){
		return contractMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return contractMapper.queryCount(map);
	}
	
	public void add(SellerContract sellerContract){
		BuyCompany company = buyCompanyMapper.selectByPrimaryKey(ShiroUtils.getCompId());
		sellerContract.setId(ShiroUtils.getUid());
		sellerContract.setSponsorId(ShiroUtils.getCompId());
		sellerContract.setSponsorName(ShiroUtils.getCompanyName());
		sellerContract.setSponsorLinkman(company.getLinkMan());
		sellerContract.setSponsorPhone(company.getTelNo());
		sellerContract.setSponsorType("1");
		sellerContract.setCreaterCompanyId(ShiroUtils.getCompId());
		sellerContract.setCreaterId(ShiroUtils.getUserId());
		sellerContract.setCreateDate(new Date());
		sellerContract.setCreaterName(ShiroUtils.getUserName());
		contractMapper.add(sellerContract);
	}
	
	public void update(SellerContract sellerContract){
		sellerContract.setStatus(1);
		sellerContract.setBuyerApproveStatus(0);
		sellerContract.setSellerApproveStatus(0);
		sellerContract.setUpdateId(ShiroUtils.getUserId());
		sellerContract.setUpdateDate(new Date());
		sellerContract.setUpdateName(ShiroUtils.getUserName());
		contractMapper.update(sellerContract);
	}
	
	public void delete(String id){
		contractMapper.delete(id);
	}


    public Map<String,Object> queryContractList(String role, SearchPageUtil searchPageUtil, Map<String,Object> params) {
		Map<String,Object> map = new HashMap<String,Object>();
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
			params.put("status","");
		}
		params.put("companyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		//合同列表数据
		List<SellerContract> list = contractMapper.queryContractList(searchPageUtil);
		//合同总列表，用于计算数量
		List<SellerContract> listAll = contractMapper.queryContractListAll(params);
		//总记录数
		long allCount = searchPageUtil.getPage().getCount();
		map.put("allCount",allCount);
		//草稿数
		int draftCount = 0;
		//待我审批数
		int myApproval = 0;
		//待对方审批数
		int customerApproval = 0;
		//待传合同照数
		int contractPhotoCount = 0;
		//协议达成数
		int ok = 0;
		//驳回数
		int reject = 0;
		if(!ObjectUtil.isEmpty(listAll)&&listAll.size()>0){
			for (SellerContract SellerContract:listAll) {
				switch (SellerContract.getStatus()){
					case 0:
						draftCount++;
						break;
					case 1:
						myApproval++;
						break;
					case 2:
						customerApproval++;
						break;
					case 3:
						contractPhotoCount++;
						break;
					case 4:
						ok++;
						break;
					case 5:
						reject++;
						break;
					default:
						break;
				}
			}
		}
		map.put("total",listAll.size());
		map.put("draftCount",draftCount);
		map.put("myApprovalCount",myApproval);
		map.put("customerApprovalCount",customerApproval);
		map.put("contractPhotoCount",contractPhotoCount);
		map.put("okCount",ok);
		map.put("rejectCount",reject);

		map.put("contractList",list);
		return map;
    }

	/**
	 * 检查合同编号是否被占用
	 * @param contractNo
	 * @return
	 */
	public int checkContractNo(String contractId,String contractNo) {
		return contractMapper.checkContractNo(contractId,contractNo);
	}

	/**
	 * 协议达成
	 * @param oriAttachments 合同原件地址
	 * @param contractId
	 * @param validPeriodStart
	 * @param validPeriodEnd
	 * @param oriAttachments
	 */
	public void agreementReached(String contractId, String validPeriodStart, String validPeriodEnd, String oriAttachments) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//更新卖家合同状态
		SellerContract sellerContract = new SellerContract();
		sellerContract.setId(contractId);
		sellerContract.setStatus(4);
		sellerContract.setValidPeriodStart(sdf.parse(validPeriodStart));
		sellerContract.setValidPeriodEnd(sdf.parse(validPeriodEnd));
		contractMapper.update(sellerContract);
		//买家合同状态
		sellerContract = contractMapper.get(contractId);
		BuyContract buyContract = new BuyContract();
		buyContract.setId(sellerContract.getBuyerContractId());
		buyContract.setStatus(4);
		buyContract.setValidPeriodStart(sdf.parse(validPeriodStart));
		buyContract.setValidPeriodEnd(sdf.parse(validPeriodEnd));
		sBuyContractMapper.update(buyContract);
		//将合同原件地址插入表中
		if(!ObjectUtil.isEmpty(oriAttachments)){
			List<String> oriContracts = JSONArray.parseArray(oriAttachments,String.class);
 			for (String url:oriContracts ) {
 				//卖家合同原件
				BuyAttachment attachment = new BuyAttachment();
				attachment.setId(ShiroUtils.getUid());
				attachment.setUrl(url);
				attachment.setRelatedId(contractId);
				attachment.setType(0);
				attachmentService.insert(attachment);
				//买家合同原件
				attachment = new BuyAttachment();
				attachment.setId(ShiroUtils.getUid());
				attachment.setUrl(url);
				attachment.setRelatedId(sellerContract.getBuyerContractId());
				attachment.setType(0);
				attachmentService.insert(attachment);
			}
		}
	}

	/**
	 * 合同保存(新增，修改)
	 * @param sellerContract
	 * @return
	 */
	public Msg saveContract(SellerContract sellerContract) {
		Msg msg = new Msg();
		try {
			if(!ObjectUtil.isEmpty(sellerContract)){
				if(!ObjectUtil.isEmpty(sellerContract.getId())){//修改合同
					String contractId = sellerContract.getId();
					int count = this.checkContractNo(contractId,sellerContract.getContractNo());
					if(count>0){
						msg.setFlag(false);
						msg.setMsg("该合同编号已存在！");
						return msg;
					}
					this.update(sellerContract);
				}else{//新增合同
					int count = this.checkContractNo("",sellerContract.getContractNo());
					if(count>0){
						msg.setFlag(false);
						msg.setMsg("该合同编号已存在！");
						return msg;
					}
					sellerContract.setId(ShiroUtils.getUid());
					this.add(sellerContract);
				}
			}
			msg.setFlag(true);
			msg.setMsg(sellerContract.getId());//将合同ID放入msg
		} catch (Exception e) {
			e.printStackTrace();
			msg.setFlag(false);
			msg.setMsg("合同保存失败");
		}
		return msg;
	}

	/**
	 * 审批通过
	 * @param id
	 * @return
	 */
	public JSONObject approveSuccess(String id) {
		JSONObject json = new JSONObject();
		SellerContract contract = new SellerContract();
		contract.setId(id);
		contract.setStatus(3);
		contract.setBuyerApproveStatus(1);
		contract.setSellerApproveStatus(1);
		int result = contractMapper.update(contract);
		if(result>0){
			//更新买家合同表状态
			BuyContract buyContract = new BuyContract();
			contract = contractMapper.get(id);
			String buyContractId = ObjectUtil.isEmpty(contract)?"":contract.getBuyerContractId();
			buyContract.setId(buyContractId);
			buyContract.setBuyerApproveStatus(1);
			buyContract.setSellerApproveStatus(1);
			buyContract.setStatus(3);
			sBuyContractMapper.update(buyContract);
			json.put("success", true);
			json.put("msg", "成功！");
			//将合同发送到卖家合同里
//			sendContractToBuyer(id);
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}

	/**
	 * 保存合同到采购商
	 * @param id
	 */
	private void sendContractToBuyer(String id) {
		SellerContract sellerContract = contractMapper.get(id);
		if(!ObjectUtil.isEmpty(sellerContract)){
			BuyContract buyContract = sBuyContractMapper.get(id);
			//如果已经有该合同，则删除再新增
			if(!ObjectUtil.isEmpty(buyContract)){
				sBuyContractMapper.delete(id);
			}
			buyContract = new BuyContract();
			buyContract.setId(sellerContract.getId());//ID 与买家合同ID一致
			buyContract.setContractNo(sellerContract.getContractNo());//合同编号
			buyContract.setSponsorId(sellerContract.getSponsorId());//合同名称
			buyContract.setSponsorName(sellerContract.getSponsorName());//发起人公司ID
			buyContract.setSponsorLinkman(sellerContract.getSponsorLinkman());//发起人公司名称
			buyContract.setSponsorPhone(sellerContract.getSponsorPhone());//发起人公司联系人
			buyContract.setCustomerId(sellerContract.getCustomerId());//发起人公司电话
			buyContract.setSponsorType(sellerContract.getSponsorType());//发起方式(0:买家发起 1:卖家发起)
			buyContract.setCustomerId(sellerContract.getCustomerId());//客户公司id
			buyContract.setCustomerName(sellerContract.getCustomerName());//客户公司名称
			buyContract.setCustomerLinkman(sellerContract.getCustomerLinkman());//客户联系人
			buyContract.setCustomerPhone(sellerContract.getCustomerPhone());//客户手机号
			buyContract.setContentAddress(sellerContract.getContentAddress());//合同内容地址
			buyContract.setStatus(1);//状态 待我审批
			buyContract.setBuyerApproveStatus(1);//买方审批状态 通过
			buyContract.setSellerApproveStatus(0);//卖方审批状态 待审批
			buyContract.setRemark(sellerContract.getRemark());//备注
			buyContract.setCreaterCompanyId(sellerContract.getCustomerId());//创建人公司ID
			buyContract.setCreaterId("");//创建人id
			buyContract.setCreaterName("");//创建人
			buyContract.setCreateDate(new Date());//创建时间
			sBuyContractMapper.add(buyContract);
		}
	}


	/**
	 * 审批未通过
	 * @param id
	 * @return
	 */
	public JSONObject approveError(String id) {
		JSONObject json = new JSONObject();
		SellerContract contract = new SellerContract();
		contract.setId(id);
		contract.setStatus(1);
		contract.setSellerApproveStatus(2);
		int result = contractMapper.update(contract);
		if(result>0){
			//更新买家合同表状态
			BuyContract buyContract = new BuyContract();
			contract = contractMapper.get(id);
			String buyContractId = ObjectUtil.isEmpty(contract)?"":contract.getBuyerContractId();
			buyContract.setId(buyContractId);
			buyContract.setStatus(2);
			buyContract.setSellerApproveStatus(2);
			sBuyContractMapper.update(buyContract);
			json.put("success", true);
			json.put("msg", "成功！");
		}else{
			json.put("success", false);
			json.put("msg", "失败！");
		}
		return json;
	}
}
