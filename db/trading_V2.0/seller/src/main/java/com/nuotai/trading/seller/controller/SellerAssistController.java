package com.nuotai.trading.seller.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.model.BuySupplier;
import com.nuotai.trading.model.BuySupplierLinkman;
import com.nuotai.trading.seller.service.buyer.SBuySupplierService;
import com.nuotai.trading.service.BuySupplierLinkmanService;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ExcelUtil;
import com.nuotai.trading.utils.Page;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;

@Controller
@RequestMapping("platform/seller/assist")
public class SellerAssistController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(SellerAssistController.class);
	@Autowired
	private	SBuySupplierService buySupplierService;
	@Autowired
	private	BuySupplierLinkmanService buySupplierLinkmanService;

	/**
	 * 加载外协列表页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "loadAssistHtml", method = RequestMethod.GET)
	private String loadAssistHtml(SearchPageUtil searchPageUtil,@RequestParam Map<String,Object> map) {
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		if(searchPageUtil.getPage()==null){
			searchPageUtil.setPage(new Page());
		}
		map.put("companyId",ShiroUtils.getCompId());
		searchPageUtil.setObject(map);
		List<Map<String,Object>> supplierList = buySupplierService.selectByPage(searchPageUtil);
		searchPageUtil.getPage().setList(supplierList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		return "platform/sellers/assist/assistList";
	}

	/**
	 * 加载新增外协页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "loadAddAssistHtml")
	private String loadAddAssistHtml() {
		return "platform/sellers/assist/addAssist";
	}
	
	/**
	 * 新增外协保存
	 * @param supplier
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveAddAssist", method = RequestMethod.POST)
	@ResponseBody
	public String saveAddSupplier(BuySupplier supplier,@RequestParam Map<String,Object> map){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveAddSupplier(supplier,map);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 跳转到修改外协页面
	 * @author:     
	 * @return
	 */
	@RequestMapping("/updateSupplier")
	public String updateSupplier(String id){
		BuySupplier supp = buySupplierService.selectByPrimaryKey(id);
		model.addAttribute("supplier", supp);
        // 供应商联系人信息
 		List<BuySupplierLinkman> linkmanList = buySupplierLinkmanService.selectBySupplierId(id);
 		model.addAttribute("linkmanList", linkmanList);
		return "platform/sellers/assist/updateAssist";
	}
	
	/**
	 * 修改外协保存
	 * @param supplier
	 * @return
	 */
	@RequestMapping(value="/saveUpdateSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveUpdateSupplier(BuySupplier supplier,@RequestParam Map<String ,Object> map){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveUpdateSupplier(supplier,map);
			json.put("success", true);
			json.put("msg", "保存成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 冻结外协保存
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveFrozenSupplier",method = RequestMethod.POST)
	@ResponseBody
	public String saveFrozenSupplier(String id,String suppName,String frozenReason,int status){
		JSONObject json = new JSONObject();
		try {
			buySupplierService.saveFrozenSupplier(id,suppName,frozenReason,status);
			json.put("success", true);
			json.put("msg", "成功！");
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", "失败！");
			e.printStackTrace();
		}
		return json.toString();
	}
	
	/**
	 * 供应商信息导出
	 * @param map
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("exportSupplierData")
	@RequestMapping(value = "/exportSupplierData")
	public void exportSupplierData(@RequestParam Map<String, Object> map,
			HttpServletResponse response) throws Exception {
		String reportName = "外协信息表";
		long currTime = System.currentTimeMillis();
		// 内存中缓存记录行数
		int rowaccess = 100;
		/* keep 100 rowsin memory,exceeding rows will be flushed to disk */
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 字体一（加粗）
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		Sheet sheet = wb.createSheet(reportName);
		// 设置这些样式
		XSSFCellStyle titleStyle = (XSSFCellStyle) wb.createCellStyle();
		titleStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		// 下边框
		titleStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		// 左边框
		titleStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		// 右边框
		titleStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
		// 上边框
		titleStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 字体左右居中
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		titleStyle.setFont(font);
		// 样式一
		XSSFCellStyle cellStyle = (XSSFCellStyle) wb.createCellStyle();
		// 字体左右居中
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		// 下边框
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		// 左边框
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		// 右边框
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		// 上边框
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setFont(font2);
		String[] title = {"外协名称","联系人","手机号","电话","类型","地址","创建日期"};
		// 大标题
		Row row = sheet.createRow(0);
		// 合并单元格
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, title.length);
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue(reportName);
		titleCell.setCellStyle(titleStyle);
		// 设置列宽
		sheet.setColumnWidth(0, 4500);
		// 小标题
		row = sheet.createRow(1);
		for (int i = 0; i < title.length; i++) {
			titleCell = row.createCell(i);
			titleCell.setCellValue(title[i]);
			titleCell.setCellStyle(titleStyle);
			// 设置列宽
			if(i == 0 || i == 5 || i == 6 ){
				sheet.setColumnWidth(i, 8000);
			}else{
				sheet.setColumnWidth(i, 4500);
			}
		}
		int startRow = 2;
		map.put("isDel", Constant.IsDel.NODEL.getValue() + "");
		map.put("friendId", ShiroUtils.getCompId());
		List<Map<String,Object>> suppList = buySupplierService.selectSupplierByMap(map);
		if (suppList != null && suppList.size() > 0) {
			for (int i = 0; i < suppList.size(); i++) {
				Map<String,Object> item = suppList.get(i);
				// 创建行s
				row = sheet.createRow(startRow);
				// 供应商名称
				String supplierName = item.containsKey("supp_name") ? item.get("supp_name").toString() : "";
				Cell cell = row.createCell(0);
				cell.setCellValue(supplierName);
				cell.setCellStyle(cellStyle);
				
				// 联系人
				String person = item.containsKey("person") ? item.get("person").toString() : "";
				cell = row.createCell(1);
				cell.setCellValue(person);
				cell.setCellStyle(cellStyle);
				
				// 手机号
				String phone = item.containsKey("phone") ? item.get("phone").toString() : "";
				cell = row.createCell(2);
				cell.setCellValue(phone);
				cell.setCellStyle(cellStyle);
				
				// 电话
				String zone = item.containsKey("zone") ? item.get("zone").toString() : "";
				String telNo = item.containsKey("tel_no") ? item.get("tel_no").toString() : "";
				cell = row.createCell(3);
				cell.setCellValue(zone+"-"+telNo);
				cell.setCellStyle(cellStyle);
				
				// 类型
				String type="";
				if("0".equals(item.get("type"))){
					type+="互通";
				}else{
					type+="非互通";
				}
				cell = row.createCell(4);
				cell.setCellValue(type);
				cell.setCellStyle(cellStyle);
				
				// 地址
				String address = item.containsKey("address") ? item.get("address").toString() : "";
				cell = row.createCell(5);
				cell.setCellValue(address);
				cell.setCellStyle(cellStyle);
				
				SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				// 创建日期
				cell = row.createCell(6);
				cell.setCellValue(sdf.format(item.get("create_date")));
				cell.setCellStyle(cellStyle);
				
				if ((startRow - 1) % rowaccess == 0) {
					((SXSSFSheet) sheet).flushRows();
				}
				startRow++;
				}
		}
		log.debug("耗时:" + (System.currentTimeMillis() - currTime) / 1000);
		ExcelUtil.preExport(reportName, response);
		ExcelUtil.export(wb, response);
	}
}
