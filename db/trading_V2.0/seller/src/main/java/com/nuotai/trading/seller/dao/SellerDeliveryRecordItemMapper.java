package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * The interface Seller delivery record item mapper.
 * 发货明细Mapper
 *
 * @author liuhui
 * @date 2017 -09-01 16:23:54
 */
@Component
public interface SellerDeliveryRecordItemMapper extends BaseDao<SellerDeliveryRecordItem> {

    /**
     * Gets iterm by delivery id.
     * 根据发货单ID获取明细
     *
     * @param map the delivery id
     * @return the iterm by delivery id
     */
    List<SellerDeliveryRecordItem> getItemByDeliveryId(Map<String,Object> map);

    /**
     * Update by primary key selective.
     * 按条件更新
     *
     * @param updItem the upd item
     */
    void updateByPrimaryKeySelective(SellerDeliveryRecordItem updItem);

    /**
     * Query delivery record item list.
     *
     * @param map the map
     * @return the list
     */
//根据条件查询发货详情
    List<SellerDeliveryRecordItem> queryDeliveryRecordItem(Map<String,Object> map);

    /**
     * Query order code by item id seller delivery record item.
     * 根据详情编号查询订单号
     * @param map the map
     * @return the seller delivery record item
     */

    SellerDeliveryRecordItem queryOrderCodeByItemId(Map<String,Object> map);

    /**
     * Query delivering list list.
     * 查询发货中的数量
     * @param map the map
     * @return the list
     */
    List<SellerDeliveryRecordItem> queryDeliveringList(Map<String, Object> map);

    /**
     * 根据账单周期、买家总司编号查询建单人
     * @param queryCreateBillUserMap
     * @return
     */
    List<Map<String,Object>> queryCreateBillUser(Map<String,Object> queryCreateBillUserMap);

	List<Map<String, Object>> getDeliveryByItemIdList(@Param("orderItemId")String orderItemId);

	//根据采购商、下单周期、下单人查询发货详情生成对账
    List<SellerDeliveryRecordItem> queryDeliveryRecordItemList(Map<String,Object> queryDeliveryToBillMap);
}
