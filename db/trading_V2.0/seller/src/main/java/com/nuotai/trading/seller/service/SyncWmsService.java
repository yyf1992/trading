package com.nuotai.trading.seller.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.dao.BuyWarehouseLogMapper;
import com.nuotai.trading.dao.BuyWarehouseProdcutsMapper;
import com.nuotai.trading.dao.TBusinessWhareaMapper;
import com.nuotai.trading.model.BuyWarehouseLog;
import com.nuotai.trading.model.BuyWarehouseProdcuts;
import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.model.wms.WmsInputPlan;
import com.nuotai.trading.model.wms.WmsInputPlanDetail;
import com.nuotai.trading.seller.dao.SellerCustomerItemMapper;
import com.nuotai.trading.seller.dao.SellerDeliveryRecordItemMapper;
import com.nuotai.trading.seller.dao.SellerDeliveryRecordMapper;
import com.nuotai.trading.seller.dao.SellerOrderMapper;
import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyCustomerItemMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordItemMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyDeliveryRecordMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderProductMapper;
import com.nuotai.trading.seller.model.SellerCustomerItem;
import com.nuotai.trading.seller.model.SellerDeliveryRecord;
import com.nuotai.trading.seller.model.SellerDeliveryRecordItem;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecord;
import com.nuotai.trading.seller.model.buyer.BuyDeliveryRecordItem;
import com.nuotai.trading.seller.model.buyer.BuyOrder;
import com.nuotai.trading.seller.model.buyer.BuyOrderProduct;
import com.nuotai.trading.seller.model.buyer.SBuyCustomerItem;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.DateUtils;
import com.nuotai.trading.utils.DingDingUtils;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.oms.client.OMSResponse;

/**
 * 同步WMS系统到货数更新到买卖系统
 * @author liuhui
 * 2017-10-23 16:25
 **/
@Service
public class SyncWmsService {
    private static final Logger log = LoggerFactory.getLogger(SyncWmsService.class);

    @Autowired
    private SellerDeliveryRecordMapper sellerDeliveryRecordMapper;
    @Autowired
    private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;
    @Autowired
    private SellerOrderMapper sellerOrderMapper;
    @Autowired
    private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;
    @Autowired
    private SBuyDeliveryRecordMapper sBuyDeliveryRecordMapper;
    @Autowired
    private SBuyDeliveryRecordItemMapper sBuyDeliveryRecordItemMapper;
    @Autowired
    private SBuyOrderMapper sBuyOrderMapper;
    @Autowired
    private SBuyOrderProductMapper sBuyOrderProductMapper;
    @Autowired
    private BuyWarehouseLogMapper buyWarehouseLogMapper;
    @Autowired
    private BuyWarehouseProdcutsMapper buyWarehouseProdcutsMapper;
    @Autowired
    private TBusinessWhareaMapper tBusinessWhareaMapper;
    @Autowired
    private SellerCustomerItemMapper sellerCustomerItemMapper;
    @Autowired
    private SBuyCustomerItemMapper sBuyCustomerItemMapper;

    public static void main(String[] args) throws Exception {
//        SyncWmsService syncWmsService = new SyncWmsService();
//        syncWmsService.syncDelivery();
    }
    /**
     * 抓取库存信息并更新发货单
     * Sync delivery.
     */
    @Transactional(rollbackFor = Exception.class)
    public void syncDelivery (){
        System.out.println("************************抓取WMS到货，更新买卖系统数据开始************************");
        log.info("************************抓取WMS到货，更新买卖系统数据开始************************");
        //1.先取得所有未到货的发货单
        List<SellerDeliveryRecord> dhList = sellerDeliveryRecordMapper.selectNotArrival();
        if (dhList != null && dhList.size() > 0) {
            //已到货的卖家订单ID
            // 有发货单未到货
            String headCode = "";
            Map<String,SellerDeliveryRecord> headMap = new HashMap<>(dhList.size());
            for (SellerDeliveryRecord dh:dhList) {
                headCode += dh.getDeliverNo() + ",";
                headMap.put(dh.getDeliverNo(), dh);
            }
            log.info("未到货发货" + headCode);
            System.out.println("未到货发货单号=[" + headCode+"]");
            //2.从WMS获取入库信息
            List<WmsInputPlan> inputPlanList = this.getArrivalFromWms(headCode);
            //3.根据WMS入库信息更新买卖系统到货数量等信息
            if(inputPlanList != null && inputPlanList.size() > 0){
                //抓取到货
                for(WmsInputPlan wmsInputPlan : inputPlanList){
                    //发货主表信息
                    SellerDeliveryRecord deliveryHeader = headMap.get(wmsInputPlan.getSourceno());
                    if(deliveryHeader==null){
                        continue;
                    }
                    Integer Integer = deliveryHeader.getDeliverType();
                    if(Integer == 0){
                    	//采购发货
                    	purchaseDelivery(deliveryHeader,wmsInputPlan);
                    }else if(Integer == 1){
                    	//换货发货
                    	customerDelivery(deliveryHeader,wmsInputPlan);
                    }
                }
            }

        }
        log.info("************************抓取WMS到货，更新买卖系统数据结束************************");
        System.out.println("************************抓取WMS到货，更新买卖系统数据结束************************");
    }
    
    /**
     * 换货到货数据抓取
     * @param deliveryHeader
     * @param wmsInputPlan
     */
    private void customerDelivery(SellerDeliveryRecord deliveryHeader,
			WmsInputPlan wmsInputPlan) {
    	//需要做入库的收货单明细，用户入库操作
        List<BuyDeliveryRecordItem> storageItem = new ArrayList<>();
    	//取得到货发货自信息
    	Map<String,SellerDeliveryRecordItem> recordItemMap = new HashMap<>();
    	Map<String,Object> dItemMap = new HashMap<>(1);
        dItemMap.put("deliveryId",deliveryHeader.getId());
        List<SellerDeliveryRecordItem> deliveryItemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(dItemMap);
    	if(deliveryItemList != null && !deliveryItemList.isEmpty()){
    		for(SellerDeliveryRecordItem item : deliveryItemList){
    			recordItemMap.put(item.getBarcode(), item);
    		}
    	}
        //入库明细
        List<WmsInputPlanDetail> inputPlanDetailList = wmsInputPlan.getItemList();
        if(inputPlanDetailList != null && inputPlanDetailList.size() > 0){
        	Date createdDate = wmsInputPlan.getCreated();
        	for(WmsInputPlanDetail detail : inputPlanDetailList){
                //实际到货数量
                int realCount = new Long(detail.getRealcount()).intValue();
                //商品条形码
                String barcode = detail.getSkuoid();
                SellerDeliveryRecordItem item = recordItemMap.get(barcode);
                //更新卖家发货子表
                item.setArrivalNum(realCount);
                item.setArrivalDate(createdDate);
                sellerDeliveryRecordItemMapper.update(item);
                //更新买家发货子表
                BuyDeliveryRecordItem buyItem = sBuyDeliveryRecordItemMapper.getItemByDeliveryItemId(item.getId());
                buyItem.setArrivalNum(realCount);
                buyItem.setArrivalDate(createdDate);
                sBuyDeliveryRecordItemMapper.update(buyItem);
                storageItem.add(buyItem);
                //更新卖家售后子表
                SellerCustomerItem sci = sellerCustomerItemMapper.get(item.getOrderItemId());
                int huanHuoNum = sci.getExchangeArrivalNum()==null?0:sci.getExchangeArrivalNum();
                sci.setExchangeArrivalNum(huanHuoNum+realCount);
                sellerCustomerItemMapper.update(sci);
                //更新买家售后子表
                SBuyCustomerItem sbci = sBuyCustomerItemMapper.get(item.getBuyOrderItemId());
                int huanHuoNumBuy = sbci.getExchangeArrivalNum()==null?0:sbci.getExchangeArrivalNum();
                sci.setExchangeArrivalNum(huanHuoNumBuy+realCount);
                sBuyCustomerItemMapper.update(sbci);
                //添加log日志
                
        	}
        }
        /**更新发货主表 begin**/
        SellerDeliveryRecord sellerDeliveryRecordUpd = new SellerDeliveryRecord();
        sellerDeliveryRecordUpd.setId(deliveryHeader.getId());
        //已到货
        sellerDeliveryRecordUpd.setRecordstatus(2);
        //到货日期
        sellerDeliveryRecordUpd.setArrivalDate(ObjectUtil.isEmpty(wmsInputPlan.getOptdate())?new Date():wmsInputPlan.getOptdate());
        sellerDeliveryRecordMapper.updateByPrimaryKeySelective(sellerDeliveryRecordUpd);
        /**更新发货主表 end**/

        /**更新收货主表 begin**/
        BuyDeliveryRecord buyDeliveryRecordUpd = sBuyDeliveryRecordMapper.getByDeliverId(deliveryHeader.getId());
        buyDeliveryRecordUpd.setDeliverId(deliveryHeader.getId());
        //交易完成
        buyDeliveryRecordUpd.setRecordstatus(1);
        //到货日期
        buyDeliveryRecordUpd.setArrivalDate(ObjectUtil.isEmpty(wmsInputPlan.getOptdate())?new Date():wmsInputPlan.getOptdate());
        sBuyDeliveryRecordMapper.updateByDeliverId(buyDeliveryRecordUpd);
        //入库操作,入库内容取自WMS
        this.storage(wmsInputPlan,storageItem);
	}
    
    /**
	 * 更新商品成本
	 * 
	 * @param skuoid
	 *            商品条形码
	 * @param count
	 *            入库数量
	 * @param purcost
	 *            采购价格
	 */
	public void purchaseInput(String skuoid, Integer count, Double purcost) {
		String whcode = "0001,0005,0007";
		// 先取得现有库存数
		Integer nowcount = 0;
		// 取得商品接口信息
		String whstockUrl = Constant.OMS_INTERFACE_URL + "getWhstockByMap?skuoid="
				+ skuoid + "&whcode=" + whcode;
		String whstockString = InterfaceUtil.searchLoginService(whstockUrl);
		net.sf.json.JSONObject whstockJson = net.sf.json.JSONObject.fromObject(whstockString);
		net.sf.json.JSONArray whstockArray = net.sf.json.JSONArray.fromObject(whstockJson.get("list"));
		if(whstockArray != null && whstockArray.size() > 0){
			for(int i = 0; i < whstockArray.size(); i++){
				// 循环计算现有库存
				net.sf.json.JSONObject whstock = whstockArray.getJSONObject(i);
				nowcount += (!whstock.containsKey("stockcount")? 0 : whstock.getInt("stockcount"));
			}
		}
		// 取得商品接口信息
		String levelUrl = Constant.OMS_INTERFACE_URL + "getGoods?skuoid=" + skuoid;
		String arrayString = InterfaceUtil.searchLoginService(levelUrl);
		net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(arrayString);
		net.sf.json.JSONArray productArray = net.sf.json.JSONArray.fromObject(json.get("list"));
		if(productArray != null && productArray.size() > 0){
			for(int i = 0; i < productArray.size(); i++){
				net.sf.json.JSONObject sku = productArray.getJSONObject(0);
				double cost = !sku.containsKey("cost")? 0 : sku.getDouble("cost");
				if(nowcount != 0){
					//现有库存不为0
					// 计算公式：(当前成本*当前库存+采购单价*入库数量)/(当前库存+入库数量)
					Double sumCost = (cost * nowcount) + (count * purcost);
					Integer sumNum = nowcount + count;
					BigDecimal b1 = new BigDecimal(String.valueOf(sumCost));
					BigDecimal b2 = new BigDecimal(String.valueOf(sumNum));
					if (b1 != null && b2 != null && b2.doubleValue() > 0) {
						Double newCost = b1.divide(b2, 4, BigDecimal.ROUND_HALF_EVEN)
								.doubleValue();
						sku.put("lastCost", purcost);
						sku.put("cost", newCost);
					} else {
						sku.put("lastCost", purcost);
						sku.put("cost", purcost);
					}
				}else{
					sku.put("lastCost", purcost);
					sku.put("cost", purcost);
				}
				// 更新商品成本
				// 取得商品接口信息
				String updatePurchaseCostUrl = Constant.OMS_INTERFACE_URL
						+ "updatePurchaseCost?barcode=" + skuoid + "&lastCost="
						+ purcost + "&cost=" + sku.get("cost");
				String updateString = InterfaceUtil
						.searchLoginService(updatePurchaseCostUrl);
				net.sf.json.JSONObject updateJson = net.sf.json.JSONObject.fromObject(updateString);
				System.out.println(updateJson.getBoolean("success"));
			}
		}
	}
    
	private void purchaseDelivery(SellerDeliveryRecord deliveryHeader,WmsInputPlan wmsInputPlan) {
    	//需要做入库的收货单明细，用户入库操作
        List<BuyDeliveryRecordItem> storageItem = new ArrayList<>();
        //已到货的买家订单ID
        List<String> arrivedBuyerOrderIdList = new ArrayList<>();
    	//仓库信息
        TBusinessWharea warehouse = tBusinessWhareaMapper.selectByCode(wmsInputPlan.getWhcode());
        //入库明细
        List<WmsInputPlanDetail> inputPlanDetailList = wmsInputPlan.getItemList();
        Map<String,Object> dItemMap = new HashMap<>(1);
        dItemMap.put("deliveryId",deliveryHeader.getId());
        List<SellerDeliveryRecordItem> deliveryItemList = sellerDeliveryRecordItemMapper.getItemByDeliveryId(dItemMap);
        Map<String,String> orderIdMap = new HashMap<>();
        if(inputPlanDetailList != null && inputPlanDetailList.size() > 0){
            //判断是否到货
            //发货信息统计
            Map<String,Long> faHuoMap = new HashMap<>();
            for(SellerDeliveryRecordItem item : deliveryItemList){
                //采购数量
                long caiGouNum = item.getDeliveryNum() == null ? 0 : item.getDeliveryNum();
                if(faHuoMap.containsKey(item.getBarcode())){
                    caiGouNum += faHuoMap.get(item.getBarcode());
                }
                faHuoMap.put(item.getBarcode(), caiGouNum);
                //封装已到货的订单ID
                orderIdMap.put(item.getOrderId(), item.getOrderId());
                if(!ObjectUtil.isEmpty(item.getBuyOrderId())&&!arrivedBuyerOrderIdList.contains(item.getBuyOrderId())){
                    arrivedBuyerOrderIdList.add(item.getBuyOrderId());
                }
            }
            //到货信息统计
            Map<String,Long> daoHuoMap = new HashMap<>(inputPlanDetailList.size());
            for(WmsInputPlanDetail detail : inputPlanDetailList){
                long daoHuoNum = detail.getRealcount() == null ? 0 : detail.getRealcount();
                if(daoHuoMap.containsKey(detail.getSkuoid())){
                    daoHuoNum += faHuoMap.get(detail.getSkuoid());
                }
                daoHuoMap.put(detail.getSkuoid(), daoHuoNum);
            }
            //处理到货数和发货数
            Map<String,Object> processedMap = new HashMap<>();
            for(WmsInputPlanDetail detail : inputPlanDetailList){
                //实际到货数量
                long realCount = detail.getRealcount();
                if (realCount >= 0) {
                    //商品条形码
                    String barcode = detail.getSkuoid();
                    if(processedMap.containsKey(barcode)){
                        //已经处理
                        continue;
                    }
                    //判断发货和到货是否一致
                    long daoHuoNum = daoHuoMap.get(barcode) == null ? 0 : daoHuoMap.get(barcode);
                    //更新发货单明细
                    Map<String,Object> map = new HashMap<>(3);
                    map.put("recordId",deliveryHeader.getId());
                    map.put("barcode",barcode);
                    map.put("warehouseId",ObjectUtil.isEmpty(warehouse)?"":warehouse.getId());
                    List<SellerDeliveryRecordItem> sdItemList = sellerDeliveryRecordItemMapper.queryList(map);
                    //到货数量分摊到明细
                    for (SellerDeliveryRecordItem sdItem:sdItemList) {
                        SellerDeliveryRecordItem updItem = new SellerDeliveryRecordItem();
                        updItem.setId(sdItem.getId());
                        if(daoHuoNum-sdItem.getDeliveryNum()<=0){
                            updItem.setArrivalNum(Integer.parseInt(String.valueOf(daoHuoNum)));
                            updItem.setArrivalDate(wmsInputPlan.getOptdate());
                            daoHuoNum = 0;
                        }else {
                            updItem.setArrivalNum(sdItem.getDeliveryNum());
                            updItem.setArrivalDate(wmsInputPlan.getOptdate());
                            daoHuoNum = daoHuoNum-sdItem.getDeliveryNum();
                        }
                        sellerDeliveryRecordItemMapper.updateByPrimaryKeySelective(updItem);
                        sellerOrderSupplierProductMapper.addArrivalNum(sdItem.getOrderItemId(),updItem.getArrivalNum());
                        //更新收货单明细
                        BuyDeliveryRecordItem buyDeliveryRecordItem = sBuyDeliveryRecordItemMapper.get(sdItem.getId());
                        if(!ObjectUtil.isEmpty(buyDeliveryRecordItem)){
                            BuyDeliveryRecordItem updBuyItem = new BuyDeliveryRecordItem();
                            updBuyItem.setId(buyDeliveryRecordItem.getId());
                            updBuyItem.setArrivalNum(updItem.getArrivalNum());
                            updBuyItem.setArrivalDate(wmsInputPlan.getOptdate());
                            sBuyDeliveryRecordItemMapper.updateByPrimaryKeySelective(updBuyItem);
                            sBuyOrderProductMapper.addArrivalNum(sdItem.getBuyOrderItemId(),updItem.getArrivalNum());
                            storageItem.add(buyDeliveryRecordItem);
                        }
                        BuyOrderProduct orderProduct = sBuyOrderProductMapper.selectByPrimaryKey(sdItem.getBuyOrderItemId());
                        //更新oms成本价
                        purchaseInput(sdItem.getBarcode(),updItem.getArrivalNum(),orderProduct.getPrice().doubleValue());
                    }
                    processedMap.put(barcode,barcode);
                }
            }
            //********************item修改完毕******************
        }
        /**更新发货主表 begin**/
        SellerDeliveryRecord sellerDeliveryRecordUpd = new SellerDeliveryRecord();
        sellerDeliveryRecordUpd.setId(deliveryHeader.getId());
        //已到货
        sellerDeliveryRecordUpd.setRecordstatus(2);
        //到货日期
        sellerDeliveryRecordUpd.setArrivalDate(ObjectUtil.isEmpty(wmsInputPlan.getOptdate())?new Date():wmsInputPlan.getOptdate());
        sellerDeliveryRecordMapper.updateByPrimaryKeySelective(sellerDeliveryRecordUpd);
        /**更新发货主表 end**/

        /**更新收货主表 begin**/
        BuyDeliveryRecord buyDeliveryRecordUpd = sBuyDeliveryRecordMapper.getByDeliverId(deliveryHeader.getId());
        buyDeliveryRecordUpd.setDeliverId(deliveryHeader.getId());
        //交易完成
        buyDeliveryRecordUpd.setRecordstatus(1);
        //到货日期
        buyDeliveryRecordUpd.setArrivalDate(ObjectUtil.isEmpty(wmsInputPlan.getOptdate())?new Date():wmsInputPlan.getOptdate());
        sBuyDeliveryRecordMapper.updateByDeliverId(buyDeliveryRecordUpd);
        //推送wms
        pushOMS(buyDeliveryRecordUpd);
        /**更新收货主表 end**/
        //***********给下单人发送钉钉消息*********
        sendDingDing(deliveryHeader.getDeliverNo(),wmsInputPlan.getPlanno(),DateUtils.format(buyDeliveryRecordUpd.getArrivalDate()));

        //入库操作,入库内容取自WMS
        this.storage(wmsInputPlan,storageItem);
        if(orderIdMap != null){
        	for(Map.Entry<String, String> entry : orderIdMap.entrySet()){
        		//1.更新订单
        		this.syncOrder(entry.getKey());
        	}
        }
	}
	/**
     * 推送wms
     * @param deliveryHeader
     */
    private void pushOMS(BuyDeliveryRecord buyDeliveryRecord) {
    	//获得子信息
    	Map<String,Object> map = new HashMap<>(1);
		map.put("recordId",buyDeliveryRecord.getId());
		List<BuyDeliveryRecordItem> itemList = sBuyDeliveryRecordItemMapper.queryList(map);
		if(!"0".equals(buyDeliveryRecord.getDropshipType())){
			//代发数据
			if("1".equals(buyDeliveryRecord.getDropshipType())){
				//代发客户
				outLibrary(buyDeliveryRecord,itemList,0);
			}else{
				//代发菜鸟、京东
				outLibrary(buyDeliveryRecord,itemList,1);
			}
		}else{
			//正常发货
			String warehouseName = "";
			if(itemList != null && !itemList.isEmpty()){
				warehouseName = itemList.get(0).getWarehouseName();
			}
			if("代发仓".equals(warehouseName)){
				//代发数据
				outLibrary(buyDeliveryRecord,itemList,0);
			}
		}
	}
    
    /**
     * 推送oms出库
     * @param deliveryHeader
     * @return
     */
    public boolean outLibrary(BuyDeliveryRecord buyDeliveryRecord,
    		List<BuyDeliveryRecordItem> recordItemList,int type){
    	boolean result = false;
    	if(buyDeliveryRecord.getRecordstatus()!=1){
    		return false;
    	}
    	Map<String,Object> header = new HashMap<String,Object>();
		header.put("title", "代发入库数据出库");
		header.put("sourceno", buyDeliveryRecord.getDeliverNo());
		header.put("whcode", "0004");
		header.put("inputdesc", "买卖系统代发采购数据wms入库后，自动创建oms出库数据");
		header.put("creater", "买卖系统");
		List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
		for(BuyDeliveryRecordItem recordItem : recordItemList){
			Map<String,Object> item = new HashMap<String,Object>();
			item.put("skucode", recordItem.getSkuCode());
			item.put("skuname", recordItem.getSkuName());
			item.put("proname", recordItem.getProductName());
			item.put("procode", recordItem.getProductCode());
			item.put("skuoid", recordItem.getBarcode());
			item.put("plancount", recordItem.getArrivalNum());
			itemList.add(item);
		}
		header.put("itemList", itemList);
		try {
			net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(header);
			if(type==0){
				//代发出库
				OMSResponse response = InterfaceUtil.NuotaiOmsClient("addOutLibrary", buyDeliveryRecord.getDeliverNo(), jsonObj);
				result = response.isSuccess();
			}else{
				//杉橙没有菜鸟、京东仓，先走出库
				//OMSResponse response = InterfaceUtil.NuotaiOmsClient("addTransfer", buyDeliveryRecord.getDeliverNo(), jsonObj);
				OMSResponse response = InterfaceUtil.NuotaiOmsClient("addOutLibrary", buyDeliveryRecord.getDeliverNo(), jsonObj);
				result = response.isSuccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(result){
			sBuyDeliveryRecordMapper.updatePushType(buyDeliveryRecord.getDeliverNo());
		}
    	return result;
    }
    
	/**
     * 发送钉钉消息
     * @param deliveryId
     */
    public void sendDingDing(String deliveryNo,String planno,String arrivalDate){
    	//获取到货子信息
    	List<Map<String,Object>> itemList = sBuyDeliveryRecordItemMapper.getItemByRecordId(deliveryNo);
    	Map<String,List<Map<String,Object>>> dingdingMap = new HashMap<>();
    	if(itemList != null && itemList.size() > 0){
    		for(Map<String,Object> itemMap : itemList){
    			if(itemMap.containsKey("dingding_id")
					&& itemMap.get("dingding_id")!=null
					&& !"".equals(itemMap.get("dingding_id").toString())){
    				String order_code = itemMap.get("order_code").toString();
    				String create_id = itemMap.get("create_id").toString();
    				String dingding_id = itemMap.get("dingding_id").toString();
    				String key = order_code+","+create_id+","+dingding_id;
    				List<Map<String,Object>> list = null;
    				if(dingdingMap.containsKey(key)){
    					list = dingdingMap.get(key);
    				}else{
    					list = new ArrayList<>();
    				}
    				list.add(itemMap);
    				dingdingMap.put(key, list);
    			}
    		}
    	}
    	if(!dingdingMap.isEmpty()){
    		for(Map.Entry<String,List<Map<String,Object>>> entry : dingdingMap.entrySet()){
    			String key = entry.getKey();
    			String[] args = key.split(",");
    			String userId = args[2];
    			String message = "您的采购单【"+args[0]+"】有到货入库，发货单号【"+deliveryNo+"】,入库单号【"+planno+"】,入库日期："+arrivalDate;
    			DingDingUtils.daoHuoDingDingMessage(userId, message);
    		}
    	}
    }


    /**
     * 同步库存后更新订单
     * @param arrivedSellerOrderIdList 到货的订单ID
     */
    private void syncOrder(String orderId) {
        //更新订单主表
        SellerOrder order = new SellerOrder();
        order.setId(orderId);
        BuyOrder buyOrder = new BuyOrder();
        SellerOrder so = sellerOrderMapper.get(orderId);
        buyOrder.setId(so.getBuyerOrderId());
        boolean isFinished = false;
        List<SellerOrderSupplierProduct> orderItemList = sellerOrderSupplierProductMapper.selectSupplierProductByOrderId(orderId);
        for (SellerOrderSupplierProduct item : orderItemList) {
            if(item.getOrderNum()-item.getArrivalNum()>0){
                isFinished = false;
                break;
            }else {
                isFinished = true;
            }
        }
        if(isFinished){
            //已收货
            order.setStatus(Constant.SellerOrderStatus.RECEIVED.getValue());
            buyOrder.setStatus(Constant.OrderStatus.RECEIVED.getValue());
            sellerOrderMapper.updateByPrimaryKeySelective(order);
            sBuyOrderMapper.updateByPrimaryKeySelective(buyOrder);
        }
    }


    /**
     * 抓取所有到货
     * @param headCode 发货单号
     * @return 到货信息列表
     */
    private List<WmsInputPlan> getArrivalFromWms(String headCode) {
        try {
            System.out.println("**************************从WMS获取到货信息 Begin****************************************");
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String path = Constant.WMS_INTERFACE_URL + "loadArrival?sourceno=" + headCode;
            //String path = "http://192.168.0.129:9001/wms_interface/loadArrival?sourceno=" + headCode;
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 调用
            InputStream inputStream = conn.getInputStream();
            // 接收
            InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader budr = new BufferedReader(isr);
            String omsResult = budr.readLine();
            System.out.println("**********************************************");
            System.out.println("omsResult=["+omsResult+"]");
            System.out.println("**********************************************");
            JSONArray array = JSONArray.parseArray(omsResult);
            System.out.println("array=["+array.toString()+"]");
            System.out.println("array.size=[" + array.size()+"]");
            List<WmsInputPlan> list = new ArrayList<WmsInputPlan>();
            if(array.size() > 0){
                for(int i = 0; i < array.size(); i++){
                    System.out.println(i);
                    JSONObject json = array.getJSONObject(i);
                    System.out.println("json=["+json.toString()+"]");
                    WmsInputPlan wmsInputPlan = new WmsInputPlan();
                    //主键
                    wmsInputPlan.setId(json.getLong("id"));
                    //标题
                    wmsInputPlan.setTitle(json.getString("title"));
                    //单号
                    wmsInputPlan.setPlanno(json.getString("planno"));
                    //来源单号
                    wmsInputPlan.setSourceno(json.getString("sourceno"));
                    //库位
                    wmsInputPlan.setWhcode(json.getString("whcode"));
                    //类型
                    wmsInputPlan.setType(json.getString("type"));
                    //状态
                    wmsInputPlan.setStatus(json.getString("status"));
                    //创建人
                    wmsInputPlan.setCreater(json.getString("creater"));
                    //创建日期
                    Date createdDate = new Date();
                    if(json.containsKey("created")){
                        try {
                            createdDate =  new Date(json.getLong("created"));
                        } catch (Exception e) {
                            if(!"".equals(json.getString("created"))){
                                createdDate =  sf.parse(json.getString("created"));
                            }
                        }
                    }
                    wmsInputPlan.setCreated(createdDate);
                    //入库日期
                    Date optdate = new Date();
                    if(json.containsKey("optdate")){
                        try {
                            optdate =  new Date(json.getLong("optdate"));
                        } catch (Exception e) {
                            if(!"".equals(json.getString("optdate"))){
                                optdate =  sf.parse(json.getString("optdate"));
                            }
                        }
                    }
                    wmsInputPlan.setOptdate(optdate);
                    wmsInputPlan.setMemo(json.getString("memo"));

                    List<WmsInputPlanDetail> itemsList = JSONArray.parseArray(json.getString("itemList"), WmsInputPlanDetail.class);
                    wmsInputPlan.setItemList(itemsList);
                    list.add(wmsInputPlan);
                }
            }
            System.out.println("******************************************************************");
            if(list.size()>0){
                System.out.println("list=["+JSONObject.toJSONString(list)+"]");
                System.out.println("list.size=["+list.size()+"]");
            }
            System.out.println("*******************************************************************");
            return list;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }finally {
            System.out.println("**************************从仓库获取到货信息 End****************************************");
        }
    }

    /**
     * Storage.
     * 入库操作
     * @param buyDeliveryRecordItemList the delivery item list
     */
    public void storage(WmsInputPlan wmsInputPlan,List<BuyDeliveryRecordItem> buyDeliveryRecordItemList){
        for (BuyDeliveryRecordItem item:buyDeliveryRecordItemList) {
            //获取收货单的具体信息
            BuyDeliveryRecord main = sBuyDeliveryRecordMapper.get(item.getRecordId());
            item = sBuyDeliveryRecordItemMapper.get(item.getId());
            //检查该条记录是否已保存到入库记录
            Map<String,Object> checkMap = new HashMap<>();
            checkMap.put("batchNo",main.getDeliverNo());
            checkMap.put("orderId",item.getOrderId());
            checkMap.put("buyDeliveryItemId",item.getId());
            checkMap.put("applyCode",item.getApplyCode());
            checkMap.put("warehouseId",item.getWarehouseId());
            checkMap.put("companyId",main.getCompanyId());
            checkMap.put("barcode",item.getBarcode());
            List<BuyWarehouseLog> logCheckList = buyWarehouseLogMapper.queryList(checkMap);
            if(!ObjectUtil.isEmpty(logCheckList)&&logCheckList.size()>0){
                continue;
            }
            //保存入库日志记录
            BuyWarehouseLog log = new BuyWarehouseLog();
            log.setId(ShiroUtils.getUid());
            log.setBatchNo(main.getDeliverNo());
            log.setOrderId(item.getOrderId());
            log.setOrderCode(item.getOrderCode());
            log.setBuyDeliveryItemId(item.getId());
            log.setApplyCode(item.getApplyCode());
            log.setCompanyId(main.getCompanyId());
            log.setWarehouseId(item.getWarehouseId());
            log.setShopId(item.getShopId());
            log.setShopCode(item.getShopCode());
            log.setShopName(item.getShopName());
            log.setChangeType(1);//入库
            if("0".equalsIgnoreCase(item.getOrderType())){//采购订单
                log.setStockType(0);
            }else if ("1".equalsIgnoreCase(item.getOrderType())){//换货订单
                log.setStockType(1);
            }else {
                log.setStockType(0);
            }
            log.setProductCode(item.getProductCode());
            log.setProductName(item.getProductName());
            log.setSkuCode(item.getSkuCode());
            log.setSkuName(item.getSkuName());
            log.setBarcode(item.getBarcode());
            log.setPrice(item.getSalePrice());
            log.setNumber(item.getArrivalNum());
            log.setFactNumber(item.getArrivalNum());
            log.setStatus(0);//库存状态 0 已完结，1未完结，2已取消
            log.setStorageDate(wmsInputPlan.getOptdate());
            log.setIsDel(0);
            log.setCreateId("");
            log.setCreateDate(new Date());
            log.setCreateName(wmsInputPlan.getCreater());
            log.setStorageNo(wmsInputPlan.getPlanno());
            buyWarehouseLogMapper.add(log);
            //更新库存，先判断库存有没有该记录，有则更新数量，否则新插入一条数据
            Map<String,Object> stockMap = new HashMap<>();
            stockMap.put("warehouseId",item.getWarehouseId());
            stockMap.put("shopId",item.getShopId());
            stockMap.put("barcode",item.getBarcode());
            List<BuyWarehouseProdcuts> buyWarehouseProdcutsList = buyWarehouseProdcutsMapper.queryList(stockMap);
            BuyWarehouseProdcuts buyWarehouseProdcuts = new BuyWarehouseProdcuts();
            if(!ObjectUtil.isEmpty(buyWarehouseProdcutsList)&&buyWarehouseProdcutsList.size()>0){
                buyWarehouseProdcuts = buyWarehouseProdcutsList.get(0);
            }
            if(ObjectUtil.isEmpty(buyWarehouseProdcuts.getId())){
                buyWarehouseProdcuts.setId(ShiroUtils.getUid());
                buyWarehouseProdcuts.setCompanyId(main.getCompanyId());
                buyWarehouseProdcuts.setWarehouseId(item.getWarehouseId());
                buyWarehouseProdcuts.setShopId(item.getShopId());
                buyWarehouseProdcuts.setShopCode(item.getShopCode());
                buyWarehouseProdcuts.setShopName(item.getShopName());
                buyWarehouseProdcuts.setProductCode(item.getProductCode());
                buyWarehouseProdcuts.setProductName(item.getProductName());
                buyWarehouseProdcuts.setSkuCode(item.getSkuCode());
                buyWarehouseProdcuts.setSkuName(item.getSkuName());
                buyWarehouseProdcuts.setBarcode(item.getBarcode());
                buyWarehouseProdcuts.setColor("");
                buyWarehouseProdcuts.setTotalCost(item.getSalePrice().multiply(new BigDecimal(item.getArrivalNum())));
                buyWarehouseProdcuts.setStocks(item.getArrivalNum());
                buyWarehouseProdcuts.setStatus("1");
                buyWarehouseProdcuts.setStorageDate(wmsInputPlan.getOptdate());
                buyWarehouseProdcuts.setRemark("");
                buyWarehouseProdcuts.setIsDel(0);
                buyWarehouseProdcuts.setCreateId("");
                buyWarehouseProdcuts.setCreateDate(new Date());
                buyWarehouseProdcuts.setCreateName(wmsInputPlan.getCreater());
                buyWarehouseProdcutsMapper.add(buyWarehouseProdcuts);
            }else{
                buyWarehouseProdcuts.setStocks(buyWarehouseProdcuts.getStocks()+item.getArrivalNum());
                buyWarehouseProdcuts.setTotalCost(buyWarehouseProdcuts.getTotalCost().add(item.getSalePrice().multiply(new BigDecimal(item.getArrivalNum())) ));
                buyWarehouseProdcutsMapper.update(buyWarehouseProdcuts);
            }
        }
    }


}
