package com.nuotai.trading.seller.model.buyer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @yyf "
 * @date 2017-10-12 17:45:09
 */
public class SBuyBillInvoice{
	//
	private String id;
	//结算单号
	private String settlementNo;
	//公司id
	private String companyId;
	//供应商id
	private String supplierId;
	//票据单号
	private String invoiceNo;
	//票据类型（1 专用发票，2 普通发票，3 其它票据）
	private String invoiceType;
	//抬头（不要超过50个字符）
	private String invoiceHeader;
	//商品总金额
	private BigDecimal totalCommodityAmount;
	//票据金额
	private BigDecimal totalInvoiceValue;
	//发票附件
	private String invoiceFileAddr;
	//运费
	private BigDecimal freight;
	//逾期利息
	private BigDecimal overdueInterest;
	//应付款金额
	private BigDecimal amountPayable;
	//实际付款金额
	private BigDecimal actualPaymentAmount;
	//
	private BigDecimal residualPaymentAmount;
	//开票时间
	private Date createDate;
	//发票核对状态
	private String acceptInvoiceStatus;
	//付款状态
	private String paymentStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSettlementNo() {
		return settlementNo;
	}

	public void setSettlementNo(String settlementNo) {
		this.settlementNo = settlementNo;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceHeader() {
		return invoiceHeader;
	}

	public void setInvoiceHeader(String invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}

	public BigDecimal getTotalCommodityAmount() {
		return totalCommodityAmount;
	}

	public void setTotalCommodityAmount(BigDecimal totalCommodityAmount) {
		this.totalCommodityAmount = totalCommodityAmount;
	}

	public BigDecimal getTotalInvoiceValue() {
		return totalInvoiceValue;
	}

	public void setTotalInvoiceValue(BigDecimal totalInvoiceValue) {
		this.totalInvoiceValue = totalInvoiceValue;
	}

	public BigDecimal getFreight() {
		return freight;
	}

	public void setFreight(BigDecimal freight) {
		this.freight = freight;
	}

	public BigDecimal getOverdueInterest() {
		return overdueInterest;
	}

	public void setOverdueInterest(BigDecimal overdueInterest) {
		this.overdueInterest = overdueInterest;
	}

	public BigDecimal getAmountPayable() {
		return amountPayable;
	}

	public void setAmountPayable(BigDecimal amountPayable) {
		this.amountPayable = amountPayable;
	}

	public BigDecimal getActualPaymentAmount() {
		return actualPaymentAmount;
	}

	public void setActualPaymentAmount(BigDecimal actualPaymentAmount) {
		this.actualPaymentAmount = actualPaymentAmount;
	}

	public BigDecimal getResidualPaymentAmount() {
		return residualPaymentAmount;
	}

	public void setResidualPaymentAmount(BigDecimal residualPaymentAmount) {
		this.residualPaymentAmount = residualPaymentAmount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getAcceptInvoiceStatus() {
		return acceptInvoiceStatus;
	}

	public void setAcceptInvoiceStatus(String acceptInvoiceStatus) {
		this.acceptInvoiceStatus = acceptInvoiceStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getInvoiceFileAddr() {
		return invoiceFileAddr;
	}

	public void setInvoiceFileAddr(String invoiceFileAddr) {
		this.invoiceFileAddr = invoiceFileAddr;
	}
}
