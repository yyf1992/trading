package com.nuotai.trading.seller.dao;

import java.util.List;
import java.util.Map;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerClient;
import com.nuotai.trading.utils.SearchPageUtil;

/**
 * 客户表
 * 
 * @author "
 * @date 2017-07-21 13:58:14
 */
public interface SellerClientMapper extends BaseDao<SellerClient> {
	
	/**
	 * 分页查询客户
	 * @param searchPageUtil
	 * @return
	 */
	List<Map<String,Object>> selectSellerClientByPage(SearchPageUtil searchPageUtil);
	
	/**
	 * 添加客户
	 * @param sellerClient
	 * @return
	 */
	int saveSellerClient(SellerClient sellerClient);
	
	/**
	 * 查询客户是否重复添加
	 * @param map
	 * @return
	 */
	List<Map<String,Object>> selectByMap(Map<String, Object> map);
	
	List<SellerClient> selectByInfoMap(Map<String, Object> map);
	
	/**
	 * 删除客户
	 * @param
	 */
	void deleteSellerClient(String id);
	
	/**
	 * 批量删除客户
	 * @param
	 */
	void deleteBeatchSellerClient(List<String> ids);
	
	/**
	 * 查询客户及联系人详情
	 * @param id
	 * @return
	 */
	SellerClient selectSellerClientById(String id);
	
	/**
	 * 更新客户
	 * @param
	 * @return
	 */
	void updateSellerClientById(SellerClient sellerClient);

	/**
	 * 更新好友互通ID
	 * @param sellerClient
	 */
	void updateFriendId(SellerClient sellerClient);
	
	/**
	 * 查询客户信息
	 * @param map
	 * @return
	 */
	SellerClient selectSellerClientByclientName(Map<String, Object> map);
}
