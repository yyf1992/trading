package com.nuotai.trading.seller.dao.buyer;

import com.nuotai.trading.seller.model.buyer.BuyOrder;
import com.nuotai.trading.utils.SearchPageUtil;

import java.util.List;
import java.util.Map;

public interface SBuyOrderMapper {
    int deleteByPrimaryKey(String id);

    int insert(BuyOrder record);

    int insertSelective(BuyOrder record);

    BuyOrder selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BuyOrder record);

    int updateByPrimaryKey(BuyOrder record);
    
    List<BuyOrder> selectByPage(SearchPageUtil searchPageUtil);
    
    int selectOrdersNumByMap(Map<String, Object> params);

    //根据财务对账状态修改订单对账、付款状态
    int updateOrderBuyBill(Map<String, Object> params);
}