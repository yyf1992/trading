package com.nuotai.trading.seller.service.buyer;

import com.nuotai.trading.seller.dao.buyer.SBuyBillReconciliationMapper;
import com.nuotai.trading.seller.model.buyer.SBuyBillReconciliation;
import com.nuotai.trading.seller.service.SellerBillCycleService;
import com.nuotai.trading.seller.service.SellerDeliveryRecordItemService;
import com.nuotai.trading.seller.service.SellerDeliveryRecordService;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 账单结算周期管理
 * @author yuyafei
 * @date 2017-7-28
 */
@Service
@Transactional
public class SBuyBillReconciliationService {
	@Autowired
	private SBuyBillReconciliationMapper billReconciliationMapper;
	@Autowired
	private SellerBillCycleService buyBillCycleService;//账单周期
	@Autowired
	private SellerDeliveryRecordService buyDeliveryRecordService;//买家发货
	@Autowired
	private SellerDeliveryRecordItemService buyDeliveryRecordItemService;//发货明细

	// 根据id编号查询账单信息
	public SBuyBillReconciliation selectByPrimaryKey(Map<String,Object> map) {
		return billReconciliationMapper.selectByPrimaryKey(map);
	}

	//修改账单周期信息
	public int updateByPrimaryKeySelective(SBuyBillReconciliation record) {
		return billReconciliationMapper.updateByPrimaryKeySelective(record);
	}

	//查询账单对账列表信息
	public List<SBuyBillReconciliation> getBillReconciliationList(
			SearchPageUtil searchPageUtil) {
		return billReconciliationMapper.getBillReconciliationList(searchPageUtil);
	}
	//向账单周期管理页面提供不同状态下的数量
	public void getBillCountByStatus(Map<String, Object> billMap){
		//查询待内部审批周期数
		billMap.put("billDealStatusCount", "1");
		int approvalBillReconciliationCount = queryBillStatusCount(billMap);
		//查询待对方审批周期数
		billMap.put("billDealStatusCount", "2");
		int acceptBillReconciliationCount = queryBillStatusCount(billMap);
		//查询审批已通过周期数
		billMap.put("billDealStatusCount", "3");
		int apprEndBillReconciliationCount = queryBillStatusCount(billMap);
		
		//billMap.put("allBillCycleCount", allBillCycleCount);
		billMap.put("approvalBillReconciliationCount", approvalBillReconciliationCount);
		billMap.put("acceptBillReconciliationCount", acceptBillReconciliationCount);
		billMap.put("apprEndBillReconciliationCount", apprEndBillReconciliationCount);
		
	}
	//根据审批状态查询数量
	public int queryBillStatusCount(Map<String, Object> map) {
		return billReconciliationMapper.queryBillStatusCount(map);
	}

}
