package com.nuotai.trading.seller.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nuotai.trading.model.TBusinessWharea;
import com.nuotai.trading.seller.dao.SellerCustomerItemMapper;
import com.nuotai.trading.seller.dao.SellerCustomerMapper;
import com.nuotai.trading.seller.dao.SellerDeliveryRecordItemMapper;
import com.nuotai.trading.seller.dao.SellerOrderMapper;
import com.nuotai.trading.seller.dao.SellerOrderSupplierProductMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyCustomerItemMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyCustomerMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderMapper;
import com.nuotai.trading.seller.dao.buyer.SBuyOrderProductMapper;
import com.nuotai.trading.seller.model.SellerCustomer;
import com.nuotai.trading.seller.model.SellerCustomerItem;
import com.nuotai.trading.seller.model.SellerDeliveryRecordItem;
import com.nuotai.trading.seller.model.SellerOrder;
import com.nuotai.trading.seller.model.SellerOrderSupplierProduct;
import com.nuotai.trading.seller.model.buyer.BuyOrder;
import com.nuotai.trading.seller.model.buyer.BuyOrderProduct;
import com.nuotai.trading.seller.model.buyer.SBuyCustomer;
import com.nuotai.trading.seller.model.buyer.SBuyCustomerItem;
import com.nuotai.trading.utils.Constant;
import com.nuotai.trading.utils.ElFunction;
import com.nuotai.trading.utils.InterfaceUtil;
import com.nuotai.trading.utils.ObjectUtil;
import com.nuotai.trading.utils.SearchPageUtil;
import com.nuotai.trading.utils.ShiroUtils;
import com.oms.client.OMSResponse;


/**
 * 卖家售后
 * @author dxl
 *
 */
@Service
@Transactional
public class SellerCustomerService {

    private static final Logger LOG = LoggerFactory.getLogger(SellerCustomerService.class);

	@Autowired
	private SellerCustomerMapper sellerCustomerMapper;
	@Autowired
	private SellerCustomerItemMapper sellerCustomerItemMapper;
	@Autowired
	private SBuyCustomerMapper buyCustomerMapper;
	@Autowired
	private SBuyCustomerItemMapper buyCustomerItemMapper;
	@Autowired
	private SBuyOrderMapper buyOrderMapper;
	@Autowired
	private SBuyOrderProductMapper buyOrderProductMapper;
	@Autowired
	private SellerOrderMapper sellerOrderMapper;
	@Autowired
	private SellerOrderSupplierProductMapper sellerOrderSupplierProductMapper;
	@Autowired
	private SellerDeliveryRecordItemMapper sellerDeliveryRecordItemMapper;
	
	public SellerCustomer get(String id){
		return sellerCustomerMapper.get(id);
	}
	
	public List<SellerCustomer> queryList(Map<String, Object> map){
		return sellerCustomerMapper.queryList(map);
	}
	
	public int queryCount(Map<String, Object> map){
		return sellerCustomerMapper.queryCount(map);
	}
	
	public void add(SellerCustomer sellerCustomer){
		sellerCustomerMapper.add(sellerCustomer);
	}
	
	public void update(SellerCustomer sellerCustomer){
		sellerCustomerMapper.update(sellerCustomer);
	}
	
	public void delete(String id){
		sellerCustomerMapper.delete(id);
	}
	
	/**
	 * 售后服务-列表数量
	 * @param params
	 */
	public void getCustomerListNum(Map<String, Object> params) {
		//待审批数量
		Map<String, Object> selectMap = new HashMap<String, Object>();
		selectMap.put("isDel",Constant.IsDel.NODEL.getValue());
		selectMap.put("supplierId",ShiroUtils.getCompId());
		selectMap.put("status", Constant.CustomerStatus.WAITVERIFY.getValue());
		int agreeNum = selectCustomerListNumByMap(selectMap);
		//已同意
		selectMap.put("status", 5);
		int waitVerifyNum = selectCustomerListNumByMap(selectMap);
		//已通过
		selectMap.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		int passNum = selectCustomerListNumByMap(selectMap);
		//已驳回
		selectMap.put("status", Constant.CustomerStatus.VERIFYNOPASS.getValue());
		int rejectNum = selectCustomerListNumByMap(selectMap);
		//已取消
		selectMap.put("status", Constant.CustomerStatus.CANCEL.getValue());
		int cancelNum = selectCustomerListNumByMap(selectMap);
		//对方确认
		selectMap.put("status", Constant.CustomerStatus.ALREADYCONFIRM.getValue());
		int overNum = selectCustomerListNumByMap(selectMap);
		
		params.put("waitVerifyNum", waitVerifyNum);
		params.put("agreeNum", agreeNum);
		params.put("passNum", passNum);
		params.put("rejectNum", rejectNum);
		params.put("cancelNum", cancelNum);
		params.put("overNum", overNum);
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		switch (tabId) {
		case 99:
			params.put("interest", "99");
			break;
		case 0:
			params.put("interest", "0");
			break;
		case 1:
			params.put("interest", "1");
			break;
		case 2:
			params.put("interest", "2");
			break;
		case 3:
			params.put("interest", "3");
			break;
		case 4:
			params.put("interest", "4");
			break;
		case 5:
			params.put("interest", "5");
			break;
		default:
			params.put("interest", "99");
			break;
		}
	}
	
	public int selectCustomerListNumByMap(Map<String, Object> params) {
		return sellerCustomerMapper.selectCustomerListNumByMap(params);
	}
	
	/**
	 * 获得售后服务列表数据
	 * @param searchPageUtil
	 * @param params
	 * @return
	 */
	public String loadCustomerData(SearchPageUtil searchPageUtil, Map<String, Object> params) {
		params.put("isDel", Constant.IsDel.NODEL.getValue());
		params.put("supplierId",ShiroUtils.getCompId());
		int tabId = params.containsKey("tabId")?Integer.parseInt(params.get("tabId").toString()):99;
		String returnPath = "platform/sellers/customer/";
		switch (tabId) {
		case 99:
			params.put("interest", "99");
			returnPath += "allCustomer";
			break;
		case 0:
			params.put("interest", "0");//待我同意的售后
			returnPath += "waitAgreeCustomer";
			break;
		case 1:
			params.put("interest", "1");//已确认数量的售后
			returnPath += "passCustomer";
			break;
		case 2:
			params.put("interest", "2");
			returnPath += "rejectCustomer";
			break;
		case 3:
			params.put("interest", "3");
			returnPath += "cancelCustomer";
			break;
		case 4:
			params.put("interest", "4");
			returnPath += "overCustomer";
			break;
		case 5:
			params.put("interest", "5");//已同意的售后  待我确认数量
			returnPath += "waitVerify";
			break;
		default:
			returnPath += "allCustomer";
			break;
		}
		
		String interest = params.containsKey("interest")?params.get("interest").toString():"99";
		if("0".equals(interest)){
			//待我同意
			params.put("status", Constant.CustomerStatus.WAITVERIFY.getValue()+"");
		}else if("1".equals(interest)){
			//已确认
			params.put("status", Constant.CustomerStatus.VERIFYPASS.getValue());
		}else if("5".equals(interest)){
			//待我确认数量
			params.put("status", 5);
		}
		searchPageUtil.setObject(params);
		List<SellerCustomer> customerList = queryCustomerList(searchPageUtil);
		searchPageUtil.getPage().setList(customerList);
		return returnPath;
	}
	
	public List<SellerCustomer> queryCustomerList(SearchPageUtil searchPageUtil){
		List<SellerCustomer> customerList = sellerCustomerMapper.queryCustomerList(searchPageUtil);
		if(customerList != null && customerList.size() > 0){
			for(SellerCustomer customer : customerList){
				boolean faHuoFlag = true;
				if(!"1".equals(customer.getStatus())){
					//卖家已经确认收货
					faHuoFlag = false;
				}
				List<SellerCustomerItem> itemList = sellerCustomerItemMapper.getItemByCustomerId(customer.getId());
				if(itemList != null && itemList.size() > 0){
					customer.setItemList(itemList);
					if(faHuoFlag){
						boolean flag = false;
						for(SellerCustomerItem item : itemList){
							if("0".equals(item.getType())
									&& item.getConfirmNumber()>item.getDeliverNum()){
								//换货，并且换货数量大于已发货数量
								flag = true;
								break;
							}
						}
						if(!flag){
							faHuoFlag = false;
						}
					}
					for(SellerCustomerItem item : itemList){
						if(!"1".equals(item.getType())){
							//获得发货数据
							List<Map<String,Object>> deliveryList = sellerDeliveryRecordItemMapper.getDeliveryByItemIdList(item.getId());
							item.setDeliveryList(deliveryList);
						}else{
							item.setDeliveryList(null);
						}
					}
				}
				customer.setFaHuoFlag(faHuoFlag);
			}
		}
		return customerList;
	}
	
	/**
	 * 同意售后保存
	 * @param map
	 * @throws Exception
	 */
	public void saveAgreeCustomer(String id) throws Exception{
		SellerCustomer sCustomer = sellerCustomerMapper.get(id);
		if(sCustomer != null){
			if(sCustomer.getStatus().equals("0")){
				sCustomer.setStatus("5");//同意售后
				sCustomer.setVerifyDate(new Date());//卖家同意时间
				sellerCustomerMapper.update(sCustomer);
				//同步买家售后服务
				SBuyCustomer bCustomer = buyCustomerMapper.get(sCustomer.getBuyCustomerId());
				bCustomer.setStatus("5");//对方已同意售后
				bCustomer.setVerifyDate(new Date());//卖家同意时间
				bCustomer.setArrivalType("0");
				buyCustomerMapper.update(bCustomer);
				boolean result = pushOMS(bCustomer);
				if(result){
					bCustomer.setPushType("1");
					buyCustomerMapper.update(bCustomer);
				}
			}
		}
	}
	
	/**
	 * 推送oms出库
	 * @param bCustomer
	 * @return
	 */
	public boolean pushOMS(SBuyCustomer bCustomer){
		boolean result = false;
		//往oms推出库操作
		Map<String,Object> header = new HashMap<String,Object>();
		header.put("title", bCustomer.getTitle());
		header.put("sourceno", bCustomer.getCustomerCode());
		TBusinessWharea wharea = ElFunction.getBusinessWhareaById(bCustomer.getWarehouseId());
		header.put("whcode", wharea.getWhareaCode());
		header.put("inputdesc", bCustomer.getReason());
		header.put("creater", bCustomer.getCreateName());
		List<Map<String,Object>> itemList = new ArrayList<Map<String,Object>>();
		List<SBuyCustomerItem> bCustomerItems = buyCustomerItemMapper.selectCustomerItemByCustomerId(bCustomer.getId());
		if(bCustomerItems != null && bCustomerItems.size() > 0){
			for(SBuyCustomerItem bCustomerItem: bCustomerItems){
				Map<String,Object> item = new HashMap<String,Object>();
				item.put("skucode", bCustomerItem.getSkuCode());
				item.put("skuname", bCustomerItem.getSkuName());
				item.put("proname", bCustomerItem.getProductName());
				item.put("procode", bCustomerItem.getProductCode());
				item.put("skuoid", bCustomerItem.getSkuOid());
				item.put("plancount", bCustomerItem.getGoodsNumber());
				itemList.add(item);
			}
			header.put("itemList", itemList);
			try {
				net.sf.json.JSONObject jsonObj = net.sf.json.JSONObject.fromObject(header);
				OMSResponse oMSResponse = InterfaceUtil.NuotaiOmsClient("addOutLibrary", bCustomer.getCustomerCode(), jsonObj);
				result = oMSResponse.isSuccess();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
		
		/**
		 * 售后确认数量保存
		 * @param map
		 * @throws Exception
		 */
		public void saveVerifyCustomer(Map<String,Object> map) throws Exception{
		SellerCustomer sCustomer = sellerCustomerMapper.get(map.get("id"));
		if(sCustomer != null){
			if(sCustomer.getStatus().equals("5")){
				sCustomer.setStatus("4");//已确认
				sCustomer.setUpdateDate(new Date());//卖家确认时间
				sellerCustomerMapper.update(sCustomer);
				//同步买家售后服务
				SBuyCustomer bCustomer = buyCustomerMapper.get(sCustomer.getBuyCustomerId());
				bCustomer.setStatus(Constant.CustomerStatus.ALREADYCONFIRM.getValue()+"");//对方已确认
				bCustomer.setUpdateDate(new Date());//卖家确认时间
				buyCustomerMapper.update(bCustomer);
				
				//更新确认数量
				String[] confirmStr = map.get("confirmStr").toString().split("@");
				for (int j = 0; j < confirmStr.length; j++) {
					String s2[] = confirmStr[j].split(",");
					SellerCustomerItem item = sellerCustomerItemMapper.get(s2[0]);
					item.setConfirmNumber(Integer.parseInt(s2[1]));
					sellerCustomerItemMapper.update(item);
					//买家同步
					SBuyCustomerItem buyitem = buyCustomerItemMapper.get(item.getBuyCustomerItemId());
					buyitem.setConfirmNumber(Integer.parseInt(s2[1]));
					buyCustomerItemMapper.update(buyitem);
				}
			}else if(sCustomer.getStatus().equals(Constant.CustomerStatus.VERIFYPASS.getValue()+"")||
					sCustomer.getStatus().equals(Constant.CustomerStatus.VERIFYNOPASS.getValue()+"")){
				throw new Exception("该售后申请已审核，请勿重复审核！");
			}else if(sCustomer.getStatus().equals(Constant.CustomerStatus.CANCEL.getValue()+"")){
				throw new Exception("该售后申请已取消，审核失败！");
			}else{
				throw new Exception("该售后申请已完结，审核失败！");
			}
		}else{
			throw new Exception("未找到申请信息，请刷新页面重试！");
		}
	}
	
	public void addOrder(SBuyCustomer bCustomer){
		//买家
		BuyOrder order = new BuyOrder();
		order.setId(ShiroUtils.getUid());
		order.setOrderCode(ShiroUtils.getOrderNum());
		order.setSuppId(bCustomer.getSupplierId());
		order.setSuppName(bCustomer.getSupplierName());
		order.setPerson(bCustomer.getPerson());
		order.setPhone(bCustomer.getPhone());
		int GoodsNum = 0;//总数量
		//卖家
		SellerOrder sOrder = new SellerOrder();
		sOrder.setId(ShiroUtils.getUid());
		BigDecimal sumPrice = new BigDecimal("0.0000");//总价格
		List<SBuyCustomerItem> bCustomerItems = buyCustomerItemMapper.selectCustomerItemByCustomerId(bCustomer.getId());
		if(bCustomerItems != null && bCustomerItems.size() > 0){
			for(SBuyCustomerItem bCustomerItem : bCustomerItems){
				//发货单明细
				SellerDeliveryRecordItem sdItem = sellerDeliveryRecordItemMapper.get(bCustomerItem.getDeliveryItemId());
				GoodsNum = GoodsNum + bCustomerItem.getConfirmNumber();
				sumPrice = sumPrice.add(bCustomerItem.getPrice().multiply(new BigDecimal(bCustomerItem.getConfirmNumber())));
				BuyOrderProduct product = new BuyOrderProduct();
				product.setId(ShiroUtils.getUid());
				product.setOrderId(order.getId());
				product.setApplyCode(bCustomerItem.getApplyCode());
				product.setSkuOid(bCustomerItem.getSkuOid());
				product.setProCode(bCustomerItem.getProductCode());
				product.setProName(bCustomerItem.getProductName());
				product.setSkuCode(bCustomerItem.getSkuCode());
				product.setSkuName(bCustomerItem.getSkuName());
				product.setUnitId(bCustomerItem.getUnitId());
				product.setGoodsNumber(bCustomerItem.getConfirmNumber());
				product.setPrice(bCustomerItem.getPrice());
				product.setPriceSum(bCustomerItem.getPrice().multiply(new BigDecimal(bCustomerItem.getConfirmNumber())));
				product.setWareHouseId(ObjectUtil.isEmpty(sdItem)?"":sdItem.getWarehouseId());
				product.setIsNeedInvoice(ObjectUtil.isEmpty(sdItem)?"":sdItem.getIsNeedInvoice());
				product.setRemark(bCustomerItem.getRemark());
				buyOrderProductMapper.insertSelective(product);
				
				SellerOrderSupplierProduct sProduct = new SellerOrderSupplierProduct();
				sProduct.setId(ShiroUtils.getUid());
				sProduct.setOrderId(sOrder.getId());
				sProduct.setBuyOrderItemId(product.getId());
				sProduct.setApplyCode(product.getApplyCode());
				sProduct.setBarcode(product.getSkuOid());
				sProduct.setProductCode(product.getProCode());
				sProduct.setProductName(product.getProName());
				sProduct.setSkuCode(product.getSkuCode());
				sProduct.setSkuName(product.getSkuName());
				sProduct.setUnitId(product.getUnitId());
				sProduct.setPrice(product.getPrice());
				sProduct.setOrderNum(product.getGoodsNumber());
				sProduct.setDiscountMoney(new BigDecimal(0.0000));
				sProduct.setTotalMoney(product.getPriceSum());
				sProduct.setDeliveredNum(0);
				sProduct.setArrivalNum(0);
				sProduct.setRemark(product.getRemark());
				sProduct.setUnitId(product.getUnitId());
				sProduct.setIsNeedInvoice(product.getIsNeedInvoice());
				sProduct.setWarehouseId(product.getWareHouseId());
				sellerOrderSupplierProductMapper.insert(sProduct);
			}
		}
		order.setGoodsNum(GoodsNum);
		order.setGoodsPrice(sumPrice);
		order.setStatus(Constant.OrderStatus.WAITDELIVERY.getValue());//待发货
		order.setIsCheck(Constant.IsCheck.ALREADYVERIFY.getValue());//已审批
		order.setOrderKind(Constant.OrderKind.AT.getValue());//互通买卖好友下单
		order.setRemark("换货代发货订单");
		order.setIsDel(Constant.IsDel.NODEL.getValue());
		order.setCreateDate(new Date());
		order.setCreateId(bCustomer.getCreateId());
		order.setCreateName(bCustomer.getCreateName());
		order.setCompanyId(bCustomer.getCompanyId());
		order.setOrderType("1");//换货收货
		//收货地址
		order.setIsSince(0);
		//需要物流
		order.setPersonName(bCustomer.getLinkman());//收货人
		order.setProvince(bCustomer.getProvince());//省
		order.setCity(bCustomer.getCity());//市
		order.setArea(bCustomer.getArea());//区
		order.setAddrName(bCustomer.getAddrName());//详细地址
		order.setAreaCode(bCustomer.getZone());//区号
		order.setPlaneNumber(bCustomer.getTelNo());//座机号
		order.setReceiptPhone(bCustomer.getLinkmanPhone());//手机号
		order.setCustomerId(bCustomer.getId());//售后id
		//商品信息
		buyOrderMapper.insert(order);
		
		sOrder.setBuyerOrderId(order.getId());
		sOrder.setOrderCode(order.getOrderCode());
		sOrder.setSuppId(order.getSuppId());
		sOrder.setSuppName(order.getSuppName());
		sOrder.setPerson(order.getPerson());
		sOrder.setPhone(order.getPhone());
		sOrder.setGoodsNum(GoodsNum);
		sOrder.setGoodsPrice(sumPrice);
		sOrder.setStatus(Constant.SellerOrderStatus.WAITDELIVERY.getValue());//待发货
		sOrder.setIsCheck(Constant.IsCheck.ALREADYVERIFY.getValue());//已审批
		sOrder.setOrderKind(Constant.OrderKind.AT.getValue());//互通买卖好友下单
		sOrder.setRemark("换货代发货订单");
		sOrder.setIsDel(Constant.IsDel.NODEL.getValue());
		sOrder.setCreateDate(new Date());
		sOrder.setCreateId("");
		sOrder.setCreateName("");
		sOrder.setCompanyId(order.getCompanyId());
		sOrder.setCompanyName(ElFunction.getCompanyById(order.getCompanyId()).getCompanyName());
		sOrder.setOrderType(order.getOrderType());//换货收货
		//收货地址
		sOrder.setIsSince(order.getIsSince());
		//需要物流
		sOrder.setPersonName(order.getPersonName());//收货人
		sOrder.setProvince(order.getProvince());//省
		sOrder.setCity(order.getCity());//市
		sOrder.setArea(order.getArea());//区
		sOrder.setAddrName(order.getAddrName());//详细地址
		sOrder.setAreaCode(order.getAreaCode());//区号
		sOrder.setPlaneNumber(order.getPlaneNumber());//座机号
		sOrder.setReceiptPhone(order.getReceiptPhone());//手机号
		//商品信息
		sellerOrderMapper.add(sOrder);
	}

	//根据对账相关条件查询退货数据
	public List<SellerCustomer> queryCustomerListByMap(Map<String,Object> map){
		return sellerCustomerMapper.queryCustomerListByMap(map);
	}

	//根据条件修改退货对账状态
	public int updateCustomerByReconciliation(Map<String,Object> map){
		return sellerCustomerMapper.updateCustomerByReconciliation(map);
	}
}
