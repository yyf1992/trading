package com.nuotai.trading.seller.controller;

import com.alibaba.fastjson.JSONObject;
import com.nuotai.trading.controller.common.base.BaseController;
import com.nuotai.trading.controller.common.base.IService;
import com.nuotai.trading.model.BuySupplierFriend;
import com.nuotai.trading.model.SysShop;
import com.nuotai.trading.seller.model.*;
import com.nuotai.trading.seller.service.*;
import com.nuotai.trading.service.BuyCompanyService;
import com.nuotai.trading.service.BuySupplierFriendService;
import com.nuotai.trading.service.SysShopService;
import com.nuotai.trading.service.SysUserService;
import com.nuotai.trading.utils.*;
import com.nuotai.trading.utils.json.Msg;
import com.nuotai.trading.utils.json.Response;
import com.nuotai.trading.utils.json.ServiceException;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 账单对账管理
 * @author yuyafei
 * @date 2017-9-20
 */

@Controller
@RequestMapping("platform/seller/billReconciliation")
@Component
public class SellerBillReconciliationController extends BaseController {
	@Autowired
	private SellerBillReconciliationService sellerBillReconciliationService;//账单对账
	@Autowired
	private SellerCustomerService customerService;
	@Autowired
	private SellerCustomerItemService customerItemService;
	@Autowired
	private SellerDeliveryRecordService sellerDeliveryRecordService;
	@Autowired
	private SellerLogisticsService sellerLogisticsService;
	@Autowired
	private BuySupplierFriendService buySupplierFriendService;

	/**
	 * 账单对账管理列表查询
	 * @return
	 */
	@RequestMapping("billReconciliationList")
	public String loadProductLinks(SearchPageUtil searchPageUtil, @RequestParam Map<String,Object> params){
		if(searchPageUtil.getPage() == null){
			searchPageUtil.setPage(new Page());
		}
		if(!params.containsKey("billDealStatus")){
			params.put("billDealStatus","0");
		}
		params.put("selCompanyId", ShiroUtils.getCompId());
		searchPageUtil.setObject(params);
		List<SellerBillReconciliation> billCycleList = sellerBillReconciliationService.getBillReconciliationList(searchPageUtil);
		sellerBillReconciliationService.getBillCountByStatus(params);
		searchPageUtil.getPage().setList(billCycleList);
		model.addAttribute("searchPageUtil", searchPageUtil);
		model.addAttribute("params", params);
		return "platform/sellers/billcycles/billreconciliation/billReconciliationList";
	}

	/**
	 * 账单对账信息添加跳转
	 * @param
	 * @return
	 */
	@RequestMapping("addReconciliationJump")
	public String addReconciliationJump(HttpServletRequest request){
		return "platform/sellers/billcycles/billreconciliation/addReconciliationJump";
	}

	@RequestMapping("queryCreateBillUser")
	@ResponseBody
	public String queryCreateBillUser(@RequestParam Map<String,Object> queryCreateBillUserMap){
		JSONObject json = sellerBillReconciliationService.queryCreateBillUser(queryCreateBillUserMap);
		return json.toString();
	}

	//对账信息保存
	@RequestMapping("saveReconciliationInfo")
	@ResponseBody
	public String saveReconciliationInfo(@RequestParam Map<String,Object> saveReconciliationMap){
		JSONObject json = sellerBillReconciliationService.saveReconciliationInfo(saveReconciliationMap);
		return json.toString();
	}

	/**
	 * Upload receipt attachment string.
	 * 上传账单票据
	 * @param files the files
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value="/uploadBillFile",method= RequestMethod.POST)
	@ResponseBody
	public String uploadPaymentBill(@RequestParam(value = "file[]", required = false) MultipartFile[] files)
			throws Exception{
		JSONObject json = new JSONObject();
		try {
			List urlList = new ArrayList();
			for (MultipartFile file : files) {
				//获取文件名
				String fileName = file.getOriginalFilename();
				json.put("fileName", fileName);
				//获取文件后缀名
				String fileExtensionName = FilenameUtils.getExtension(fileName);
				if(!fileExtensionName.equalsIgnoreCase("JPG") && !fileExtensionName.equalsIgnoreCase("GIF")
						&& !fileExtensionName.equalsIgnoreCase("PNG") && !fileExtensionName.equalsIgnoreCase("JPEG")){
					json.put("result", "fail");
					json.put("msg", "上传文件格式不正确！");
					return json.toString();
				}
				//获取文件大小
				String fileSize = ShiroUtils.convertFileSize(file.getSize());
				UplaodUtil uplaodUtil = new UplaodUtil();
				String url = uplaodUtil.uploadFile(file,null,true);
				urlList.add(url);
			}
			json.put("urlList", urlList);
			json.put("result", "success");
		} catch (Exception e) {
			e.printStackTrace();
			json.put("result", "fail");
			json.put("msg", "上传失败！请联系管理员！");
			return json.toString();
		}
		return json.toString();
	}

	//自定义付款审批
	@RequestMapping("acceptBillCustomRec")
	@ResponseBody
	public String acceptBillCustomRec(@RequestParam Map<String,Object> acceptCustomMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillReconciliationService.acceptBillCustom(acceptCustomMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "提交成功！");
			}else {
				json.put("error", false);
				json.put("msg", "提交失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 *自定义付款附件回显
	 * @return
	 */
	@RequestMapping("queryBillCustomList")
	@ResponseBody
	public String queryBillCustomList(@RequestParam Map<String,Object> billRecIdMap){
		List<Map<String,Object>> recItemList = sellerBillReconciliationService.queryBillCustomList(billRecIdMap);
		return JSONObject.toJSONString(recItemList);
	}
	/**
	 *保存账单附件信息
	 * @return
	 */
	@RequestMapping("addBillFileInfo")
	@ResponseBody
	public String addBillFileInfo(@RequestParam Map<String,Object> updateSaveMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillReconciliationService.addBillFileInfo(updateSaveMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "添加成功！");
			}else {
				json.put("error", false);
				json.put("msg", "添加失败！");
			}
		} catch (Exception e) {
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 *账单附件回显
	 * @return
	 */
	@RequestMapping("queryBillFileList")
	@ResponseBody
	public String queryBillFileList(@RequestParam Map<String,Object> billRecIdMap){
		List<Map<String,Object>> recItemList = sellerBillReconciliationService.queryBillFileList(billRecIdMap);
		return JSONObject.toJSONString(recItemList);
	}

	//发起对账
	@RequestMapping("launchReconciliation")
	@ResponseBody
	public String launchReconciliation(@RequestParam Map<String, Object> approvalMap){
		JSONObject json = new JSONObject();
		try {
			int launchCount  = sellerBillReconciliationService.launchReconciliation(approvalMap);
			if(launchCount > 0){
				json.put("success", true);
				json.put("msg", "对账发起成功！");
			}else {
				json.put("error", false);
				json.put("msg", "对账发起失败！");
			}
		}catch (Exception e){
			json.put("error", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 修改对账单价
	 * @param updateMap
	 * @return
	 */
	@RequestMapping("updateRecItemPrice")
	@ResponseBody
	public String updateRecItemPrice(@RequestParam Map<String, Object> updateMap){
		JSONObject json = sellerBillReconciliationService.updateRecItemPrice(updateMap);
		return json.toString();
	}

	//再次修改账单周期
	@RequestMapping("updateBillRecJump")
	//@ResponseBody
	public String updateBillRecJump(HttpServletRequest request){
		String reconciliationId = request.getParameter("reconciliationId").toString();
		Map<String,Object> recMap = new HashMap<String,Object>();
		recMap.put("id",reconciliationId);
		SellerBillReconciliation sellerBillReconciliation = sellerBillReconciliationService.selectByPrimaryKey(recMap);
		model.addAttribute("sellerRecInfo",sellerBillReconciliation);
		return "platform/sellers/billcycles/billreconciliation/updateReconciliationJump";
	}

	//修改账单周期
	@RequestMapping("updateReconciliationInfo")
	@ResponseBody
	public String updateReconciliationInfo(@RequestParam Map<String, Object> updateMap){
		JSONObject json =sellerBillReconciliationService.updateRecInfo(updateMap);
		return json.toString();
	}

	/**
	 * 审批通过 (注意备注都在审批表中)
	 * @param
	 */
	@RequestMapping("updateReconciliation")
	@ResponseBody
	public String updateReconciliation(@RequestParam Map<String, Object> updateMap){
		JSONObject json = new JSONObject();
		try {
			int updateSaveCount = sellerBillReconciliationService.updateReconciliation(updateMap);
			if(updateSaveCount > 0){
				json.put("success", true);
				json.put("msg", "对账确认成功！");
			}else {
				json.put("success", false);
				json.put("msg", "对账确认失败！");
			}
		} catch (Exception e) {
			json.put("success", false);
			json.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	//账单对账详情
	@RequestMapping("getReconciliationDetails")
	//@ResponseBody
	public String getReconciliationDetails(String reconciliationId){
		Map<String,Object> recordAndReturnMap = sellerBillReconciliationService.queryRecordAndReturn(reconciliationId);
		model.addAttribute("recordAndReturnMap",recordAndReturnMap);
		return "platform/sellers/billcycles/billreconciliation/reconciliationDetail";
	}

	/**
	 * 账单对账详情
	 * @param reconciliationId
	 * @return
	 */
	@RequestMapping("getReconciliationDetailsNew")
	public String getReconciliationDetailsNew(String reconciliationId){
		sellerBillReconciliationService.queryRecordAndReturnNew(reconciliationId,model);
		return "platform/sellers/billcycles/billreconciliation/reconciliationDetailNew";
	}

	/**
	 * 账单详情查看票据
	 * @return
	 */
	@RequestMapping("queryFileUrlList")
	@ResponseBody
	public String queryFileUrlList(@RequestParam Map<String,Object> urlMap){
		JSONObject json = new JSONObject();
		String urlStrList = sellerBillReconciliationService.queryFileUrlList(urlMap);
		if(null != urlStrList && !"".equals(urlStrList)){
			model.addAttribute("urlStrList",urlStrList);
			json.put("urlList",urlStrList);
			json.put("success", true);
			json.put("msg", "账单附件加载成功！");
		}else if("".equals(urlStrList)){
			json.put("error", false);
			json.put("msg", "该账单未上传收货回单！");
		}else if(null == urlStrList) {
			json.put("error", false);
			json.put("msg", "该账单的收货回单凭据加载失败，请重新打开！");
		}
		return json.toString();
	}

	/**
	 * 查看售后明细页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("loadCustomerDetails")
	public String loadCustomerDetails(String id) throws Exception {
		SellerCustomer customer = customerService.get(id);
		model.addAttribute("customer", customer);
		List<SellerCustomerItem> itemList = customerItemService.selectCustomerItemByCustomerId(id);
		model.addAttribute("itemList", itemList);
		return "platform/sellers/billcycles/billreconciliation/customerDetails";
	}

	/**
	 * Delivery record detail string.
	 * 发货单详情
	 * @param params the params
	 * @return the string
	 */
	@RequestMapping("deliveryRecordDetail")
	public String deliveryRecordDetail(@RequestParam Map<String,Object> params){

		//发货单信息
		Map<String,Object> map = sellerDeliveryRecordService.getDeliveryRecordDetail(params);
		List<SellerDeliveryRecordDetailView> deliveryList = (List<SellerDeliveryRecordDetailView>) map.get("deliveryList");
		model.addAttribute("deliveryList", deliveryList);
		//物流信息
		String logisticsId = "";
		if(!ObjectUtil.isEmpty(deliveryList)&&deliveryList.size()>0){
			logisticsId = deliveryList.get(0).getLogisticsId();
		}
		SellerLogistics logistics = sellerLogisticsService.get(logisticsId);
		model.addAttribute("logistics", logistics);
		return "platform/sellers/billcycles/billreconciliation/deliveryRecordDetail";
	}

	/**
	 * 卖家账单信息导出
	 * @param sellerRecMap
	 */
	@RequestMapping("sellerRecExport")
	public void sellerRecExport(@RequestParam Map<String,Object> sellerRecMap){
		sellerRecMap.put("selCompanyId", ShiroUtils.getCompId());
		List<SellerBillReconciliation> billCycleList = sellerBillReconciliationService.selectByPrimaryKeyList(sellerRecMap);

		int rowaccess = 100;// 内存中缓存记录行数
		// 第一步，创建一个webbook，对应一个Excel文件
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单对账报表");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);

		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		//String[] cellHeadArray ={"账单开始日期","账单截止日期","采购员","采购商","到货数量","到货总金额","发货换货数量","发货换货金额","退货换货数量","退货换货金额","退货数量","退货总金额","运费","奖惩金额","预付款抵扣金额","总金额","状态","审批备注"};
		String[] cellHeadArray ={"对账单号","账单周期","采购员","采购商","发货总数量","发货总金额","售后总数量","售后总金额","运费","奖惩金额","预付款抵扣金额","总金额","状态","审批备注"};
		// 大标题
		//Row row = sheet.createRow(0);
		// 第三步，在sheet中添加表头第0行
		Row row = sheet.createRow(0);
		ExcelUtil.setRangeStyle(sheet, 1, 1, 1, cellHeadArray.length);// 合并单元格
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("账单对账报表");
		titleCell.setCellStyle(headStyle);
		sheet.setColumnWidth(0, 4000);// 设置列宽

		row = sheet.createRow(1);
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}
		int rowCount = 2;

		if(null != billCycleList && billCycleList.size()>0){
			for(SellerBillReconciliation sellerBillReconciliation : billCycleList){
				row=sheet.createRow(rowCount);
				//1.账单开始日期
				cell = row.createCell(0);
				cell.setCellValue(sellerBillReconciliation.getId());
				cell.setCellStyle(normalStyle);
				//2.账单截止日期
				cell = row.createCell(1);
				cell.setCellValue(sellerBillReconciliation.getStartBillStatementDateStr()+"至"+sellerBillReconciliation.getEndBillStatementDateStr());
				cell.setCellStyle(normalStyle);
				//2.下单人
				cell = row.createCell(2);
				cell.setCellValue(sellerBillReconciliation.getCreateBillUserName());
				cell.setCellStyle(normalStyle);
				//3.供应商
				cell = row.createCell(3);
				cell.setCellValue(sellerBillReconciliation.getBuyCompanyName());
				cell.setCellStyle(normalStyle);
				//4.到货数量
				cell = row.createCell(4);
				cell.setCellValue(sellerBillReconciliation.getRecordConfirmCount()+sellerBillReconciliation.getRecordReplaceCount());
				cell.setCellStyle(normalStyle);
				//5.到货总金额
				cell = row.createCell(5);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getRecordConfirmTotal().add(sellerBillReconciliation.getRecordReplaceTotal())));
				cell.setCellStyle(normalStyle);
				//6.发货换货数量
				/*cell = row.createCell(6);
				cell.setCellValue(sellerBillReconciliation.getRecordReplaceCount());
				cell.setCellStyle(normalStyle);
				//7.发货换货总金额
				cell = row.createCell(7);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getRecordReplaceTotal()));
				cell.setCellStyle(normalStyle);*/
				//6.退货换货数量
				cell = row.createCell(6);
				cell.setCellValue(sellerBillReconciliation.getReplaceConfirmCount()+sellerBillReconciliation.getReturnGoodsCount());
				cell.setCellStyle(normalStyle);
				//7.退货换货总金额
				cell = row.createCell(7);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getReplaceConfirmTotal().add(sellerBillReconciliation.getReturnGoodsTotal())));
				cell.setCellStyle(normalStyle);
				//8.退货数量
				/*cell = row.createCell(10);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getReturnGoodsCount()));
				cell.setCellStyle(normalStyle);
				//9.退货总金额
				cell = row.createCell(11);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getReturnGoodsTotal()));
				cell.setCellStyle(normalStyle);*/
				//10.运费
				cell = row.createCell(8);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getFreightSumCount()));
				cell.setCellStyle(normalStyle);
				//11.奖惩金额
				cell = row.createCell(9);
				//cell.setCellValue(fnum.format(sellerBillReconciliation.getCustomAmount()));
				if(sellerBillReconciliation.getCustomAmount() != null && !"".equals(sellerBillReconciliation.getCustomAmount())){
					cell.setCellValue(fnum.format(sellerBillReconciliation.getCustomAmount()));
				}else {
					cell.setCellValue(fnum.format(0.0000));
				}
				cell.setCellStyle(normalStyle);
				//11.奖惩金额
				cell = row.createCell(10);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getAdvanceDeductTotal()));
				cell.setCellStyle(normalStyle);
				//12.总金额
				cell = row.createCell(11);
				cell.setCellValue(fnum.format(sellerBillReconciliation.getTotalAmount()));
				cell.setCellStyle(normalStyle);
				//13.状态
				cell = row.createCell(12);
				String billDealStatus = sellerBillReconciliation.getBillDealStatus();
				String statusStr = "";
				if("1".equals(billDealStatus)){
					statusStr = "待发起对账";
				}else if("2".equals(billDealStatus)){
					statusStr = "待确认对账";
				}else if("3".equals(billDealStatus)){
					statusStr = "已确认对账";
				}else if("4".equals(billDealStatus)){
					statusStr = "已驳回对账";
				}else if("9".equals(billDealStatus)){
					statusStr = "奖惩待确认";
				}else if("10".equals(billDealStatus)){
					statusStr = "奖惩已确认";
				}else if("11".equals(billDealStatus)){
					statusStr = "奖惩已驳回";
				}
				cell.setCellValue(statusStr);
				cell.setCellStyle(normalStyle);
				//13.备注
				cell = row.createCell(13);
				cell.setCellValue(sellerBillReconciliation.getBuyerReconciliationRemarks());
				cell.setCellStyle(normalStyle);

				if ((rowCount - 1) % rowaccess == 0) {
					try {
						((SXSSFSheet)sheet).flushRows();
					}catch (Exception e){
						e.printStackTrace();
					}
				}
				rowCount++;
			}
		}
		ExcelUtil.preExport("账单对账信息", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * 导出对账详情
	 * @param reconciliationId
	 */
	@RequestMapping("sellerRecItemExport")
	public void sellerRecItemExport(String reconciliationId){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,Object> recItemExportMap = sellerBillReconciliationService.queryRecItemExport(reconciliationId);
		SellerBillReconciliation sellerBillReconciliation = (SellerBillReconciliation) recItemExportMap.get("sellerBillReconciliation");
		Map<String,Map<String,Object>> recordReplaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("recordReplaceMap");
		Map<String,Map<String,Object>> daohuoMap = (Map<String, Map<String,Object>>) recItemExportMap.get("daohuoMap");
		Map<String,Map<String,Object>> replaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("replaceMap");
		Map<String,Map<String,Object>> customerMap = (Map<String, Map<String,Object>>) recItemExportMap.get("customerMap");

		// 第一步，创建一个webbook，对应一个Excel文件
		int rowaccess = 100;// 内存中缓存记录行数
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单对账详情");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第三步，在sheet中添加表头第0行
		Row row = sheet.createRow(0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		/*String[] cellHeadArray ={"账单开始日期","账单截止日期","供应商","到货数量","到货总金额","换货数量","换货总金额","退货数量","退货总金额","运费","总金额","状态"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}*/
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum()+1, row.getRowNum()+1, 1, 15);
		cell = row.createCell(0);
		String billDealStatus = sellerBillReconciliation.getBillDealStatus();
		String dealStatusStr = "";
		if("1".equals(billDealStatus)){
			dealStatusStr = "待发起对账";
		}else if("2".equals(billDealStatus)){
			dealStatusStr = "待确认对账";
		}else if("3".equals(billDealStatus)){
			dealStatusStr = "已确认对账";
		}else if("4".equals(billDealStatus)){
			dealStatusStr = "已驳回对账";
		}else if("9".equals(billDealStatus)){
			dealStatusStr = "奖惩待确认";
		}else if("10".equals(billDealStatus)){
			dealStatusStr = "奖惩已确认";
		}else if("11".equals(billDealStatus)){
			dealStatusStr = "奖惩已驳回";
		}
		cell.setCellValue("采购商:"+sellerBillReconciliation.getBuyCompanyName()+"    出账周期:"+sellerBillReconciliation.getStartBillStatementDateStr()+"至"+sellerBillReconciliation.getEndBillStatementDateStr()+"    状态:"+dealStatusStr+"    奖惩金额:"+sellerBillReconciliation.getCustomAmount()+"    预付款抵扣金额:"+sellerBillReconciliation.getAdvanceDeductTotal());
		XSSFCellStyle borderStyle = (XSSFCellStyle)wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(headStyle);

		int rowCount = 1;
		//发货导出
		if(null != daohuoMap && daohuoMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = daohuoMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> daohuoMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				BigDecimal freight = (BigDecimal) daohuoMaps.get("freight");
				BigDecimal priceSum = (BigDecimal) daohuoMaps.get("itemPriceAndFreightSum");
				cell.setCellValue("订单类型:" + "采购发货" + "  运单号:" + daohuoMaps.get("waybillNo") + "  物流名称:" + daohuoMaps.get("logisticsCompany")
						+ "  司机名称:" + daohuoMaps.get("driverName") + "  运费:" + daohuoMaps.get("freight") + " 元" + "  总金额+运费:" + priceSum.add(freight));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号","采购员", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<SellerBillReconciliationItem> daohuoItemList = (List<SellerBillReconciliationItem>) daohuoMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.下单人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//14.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.下单人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(daohuoMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(daohuoMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//14.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}

		//发货换货导出
		if(null != recordReplaceMap && recordReplaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = recordReplaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> recReplaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				BigDecimal freight = (BigDecimal) recReplaceMaps.get("freight");
				BigDecimal priceSum = (BigDecimal) recReplaceMaps.get("itemPriceAndFreightSum");
				cell.setCellValue("订单类型:" + "发货换货" + "  运单号:" + recReplaceMaps.get("waybillNo") + "  物流名称:" + recReplaceMaps.get("logisticsCompany")
						+ "  司机名称:" + recReplaceMaps.get("driverName") + "  运费:" + recReplaceMaps.get("freight") + " 元" + "  总金额+运费:" + priceSum.add(freight));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号","采购员", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<SellerBillReconciliationItem> daohuoItemList = (List<SellerBillReconciliationItem>) recReplaceMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.下单人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.下单人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(recReplaceMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(recReplaceMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//6.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}

		//换货导出
		if(null != replaceMap && replaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = replaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> replaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				cell.setCellValue("订单类型:" + "换货" + "  总金额:" + replaceMaps.get("itemPriceSum"));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				//ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, row.getRowNum() + 1,  13);
				//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "发货单号", "到货日期", "到货总数量", "采购单价", "总金额"};
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号","创建人", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<SellerBillReconciliationItem> daohuoItemList = (List<SellerBillReconciliationItem>) replaceMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.创建人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(replaceMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					//BigDecimal itemPriceSum =new BigDecimal(dhMap.get("itemPriceSum"));
					cell.setCellValue(fnum.format(replaceMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//8.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);

					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//退货导出
		if(null != customerMap && customerMap.size()>0) {
			Collection values = customerMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> customerMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				row = sheet.createRow(rowCount);
				// 合并单元格,组装订单信息
				ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 15);
				cell = row.createCell(0);
				cell.setCellValue("订单类型:" + "退货" + "  总金额:" + customerMaps.get("itemPriceSum"));
				XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
				// 设置单元格边框颜色
				borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
				borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
				borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
				cell.setCellStyle(borderStyle1);
				rowCount++;

				row = sheet.createRow(rowCount);
				//ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, row.getRowNum() + 1,  13);
				//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "发货单号", "到货日期", "到货总数量", "采购单价", "对账单价", "总金额"};
				String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号","创建人", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额", "入库单号"};
				for (int i = 0; i < cellHeadArray.length; i++) {
					cell = row.createCell(i);
					cell.setCellValue(cellHeadArray[i]);
					cell.setCellStyle(headStyle);
				}

				rowCount++;
				List<SellerBillReconciliationItem> customerItemList = (List<SellerBillReconciliationItem>) customerMaps.get("itemList");
				if (null != customerItemList && customerItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : customerItemList) {
						row = sheet.createRow(rowCount);
						//1.货号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//6.采购订单号
						cell = row.createCell(5);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//8.到货日期
						cell = row.createCell(8);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//10.是否开票
						cell = row.createCell(10);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/
						//11.采购单价
						cell = row.createCell(11);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.对账单价
						cell = row.createCell(12);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(13);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息
					row = sheet.createRow(rowCount);
					//1.货号
					cell = row.createCell(0);
					cell.setCellValue("合计");
					cell.setCellStyle(normalStyle);
					//2.商品名称
					cell = row.createCell(1);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//3.规格代码
					cell = row.createCell(2);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//4.规格名称
					cell = row.createCell(3);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//5.条形码
					cell = row.createCell(4);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//6.采购订单号
					cell = row.createCell(5);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//6.创建人
					cell = row.createCell(6);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//7.发货单号
					cell = row.createCell(7);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//8.到货日期
					cell = row.createCell(8);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//9.到货总数量
					cell = row.createCell(9);
					int arrivalNumSum = Integer.parseInt(customerMaps.get("arrivalNumSum").toString());
					cell.setCellValue(arrivalNumSum);
					cell.setCellStyle(normalStyle);
					//10.是否开票
					cell = row.createCell(10);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);
					//11.采购单价
					cell = row.createCell(11);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.对账单价
					cell = row.createCell(12);
					cell.setCellValue("-");
					cell.setCellStyle(normalStyle);
					//11.总金额
					cell = row.createCell(13);
					cell.setCellValue(fnum.format(customerMaps.get("itemPriceSum")));
					cell.setCellStyle(normalStyle);
					//8.入库单号
					cell = row.createCell(14);
					cell.setCellValue("");
					cell.setCellStyle(normalStyle);

					if ((rowCount - 1) % rowaccess == 0) {
						try {
							((SXSSFSheet)sheet).flushRows();
						}catch (Exception e){
							e.printStackTrace();
						}
					}
					rowCount++;

				}
			}
		}
		//第一行是订单信息
		row = sheet.createRow(rowCount);
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum() + 1, row.getRowNum() + 1, 1, 14);
		cell = row.createCell(0);
		cell.setCellValue("总金额:" + sellerBillReconciliation.getTotalAmount());
		XSSFCellStyle borderStyle1 = (XSSFCellStyle) wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle1.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle1.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle1.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(borderStyle1);

		if ((rowCount - 1) % rowaccess == 0) {
			try {
				((SXSSFSheet)sheet).flushRows();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		rowCount++;

		ExcelUtil.preExport("账单详情导出", response);
		ExcelUtil.export(wb, response);
	}

	/**
	 * 根据账单编号导出帐单详情简略信息
	 * @param reconciliationId
	 */
	@RequestMapping("sellerRecItemRoughExport")
	public void sellerRecItemRoughExport(String reconciliationId){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,Object> recItemExportMap = sellerBillReconciliationService.queryRecItemExport(reconciliationId);
		//Map<String,Object> recItemExportMap = buyBillReconciliationService.queryRecItemExport(reconciliationId);
		//BuyBillReconciliation buyBillReconciliation = (BuyBillReconciliation) recItemExportMap.get("buyBillReconciliation");
		SellerBillReconciliation sellerBillReconciliation = (SellerBillReconciliation) recItemExportMap.get("sellerBillReconciliation");
		Map<String,Map<String,Object>> recordReplaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("recordReplaceMap");
		Map<String,Map<String,Object>> daohuoMap = (Map<String, Map<String,Object>>) recItemExportMap.get("daohuoMap");
		Map<String,Map<String,Object>> replaceMap = (Map<String, Map<String,Object>>) recItemExportMap.get("replaceMap");
		Map<String,Map<String,Object>> customerMap = (Map<String, Map<String,Object>>) recItemExportMap.get("customerMap");

		// 第一步，创建一个webbook，对应一个Excel文件
		int rowaccess = 100;// 内存中缓存记录行数
		SXSSFWorkbook wb = new SXSSFWorkbook(rowaccess);
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		Sheet sheet = wb.createSheet("账单对账详情");
		sheet.setDefaultColumnWidth(20);
		//字体
		XSSFFont font = (XSSFFont) wb.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体二
		XSSFFont font2 = (XSSFFont) wb.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 第三步，在sheet中添加表头第0行
		Row row = sheet.createRow(0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle headStyle = (XSSFCellStyle) wb.createCellStyle();
		// 创建一个居中格式
		headStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 蓝色背景色
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);// 下边框
		headStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		headStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		headStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setFont(font);
		// 创建单元格格式，并设置表头居中
		XSSFCellStyle normalStyle = (XSSFCellStyle) wb.createCellStyle();
		normalStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		//合并单元格样式
		XSSFCellStyle mergeStyle = (XSSFCellStyle) wb.createCellStyle();
		mergeStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		mergeStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		normalStyle.setBorderBottom(CellStyle.BORDER_THIN);// 下边框
		normalStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
		normalStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框
		normalStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
		normalStyle.setFont(font2);
		//数字小数位格式
		DecimalFormat fnum = new DecimalFormat("##0.0000");
		// 设置excel的数据头信息
		Cell cell;
		/*String[] cellHeadArray ={"账单开始日期","账单截止日期","供应商","到货数量","到货总金额","换货数量","换货总金额","退货数量","退货总金额","运费","总金额","状态"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}*/
		// 合并单元格,组装订单信息
		ExcelUtil.setRangeStyle(sheet, row.getRowNum()+1, row.getRowNum()+1, 1, 15);
		cell = row.createCell(0);
		String billDealStatus = sellerBillReconciliation.getBillDealStatus();
		String dealStatusStr = "";
		if("2".equals(billDealStatus)){
			dealStatusStr = "待确认对账";
		}else if("3".equals(billDealStatus)){
			dealStatusStr = "已确认对账";
		}else if("4".equals(billDealStatus)){
			dealStatusStr = "已驳回对账";
		}else if("9".equals(billDealStatus)){
			dealStatusStr = "奖惩待确认";
		}else if("10".equals(billDealStatus)){
			dealStatusStr = "奖惩已确认";
		}else if("11".equals(billDealStatus)){
			dealStatusStr = "奖惩已驳回";
		}
		cell.setCellValue("采购商:"+sellerBillReconciliation.getBuyCompanyName()+"    出账周期:"+sellerBillReconciliation.getStartBillStatementDateStr()+"至"+sellerBillReconciliation.getEndBillStatementDateStr()+"    状态:"+dealStatusStr+"    奖惩金额:"+sellerBillReconciliation.getCustomAmount()+"    预付款抵扣金额:"+sellerBillReconciliation.getAdvanceDeductTotal());
		//cell.setCellValue("供应商:"+buyBillReconciliation.getSellerCompanyName()+"    出账周期:"+buyBillReconciliation.getStartBillStatementDateStr()+"至"+buyBillReconciliation.getEndBillStatementDateStr()+"    状态:"+dealStatusStr);
		XSSFCellStyle borderStyle = (XSSFCellStyle)wb.createCellStyle();
		// 设置单元格边框颜色
		borderStyle.setBottomBorderColor(new XSSFColor(java.awt.Color.RED));
		borderStyle.setTopBorderColor(new XSSFColor(java.awt.Color.GREEN));
		borderStyle.setLeftBorderColor(new XSSFColor(java.awt.Color.BLUE));
		cell.setCellStyle(headStyle);

		int rowCount = 1;
		//因为第0行已经设置标题了,所以从行2开始写入数据
		//第一行是订单信息

		row = sheet.createRow(rowCount);
		//String[] cellHeadArray = {"货号", "商品名称", "规格代码", "规格名称", "条形码", "采购订单号", "发货单号", "到货日期", "到货总数量", "是否开票", "采购单价", "对账单价", "总金额"};
		String[] cellHeadArray = {"采购订单号", "创建人", "发货单号","货号", "商品名称", "规格代码", "规格名称", "条形码", "采购单价", "到货总数量", "总金额", "到货日期", "是否开票", "对账单价", "入库单号"};
		for (int i = 0; i < cellHeadArray.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(cellHeadArray[i]);
			cell.setCellStyle(headStyle);
		}

		rowCount++;
		//发货导出
		if(null != daohuoMap && daohuoMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = daohuoMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> daohuoMaps = (Map<String, Object>) object;

				List<SellerBillReconciliationItem> daohuoItemList = (List<SellerBillReconciliationItem>) daohuoMaps.get("itemList");
				//List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) daohuoMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}
						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}

		//发货换货导出
		if(null != recordReplaceMap && recordReplaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = recordReplaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> recordReplaceMaps = (Map<String, Object>) object;

				List<SellerBillReconciliationItem> recordReplaceItemList = (List<SellerBillReconciliationItem>) recordReplaceMaps.get("itemList");
				//List<BuyBillReconciliationItem> daohuoItemList = (List<BuyBillReconciliationItem>) daohuoMaps.get("itemList");
				if (null != recordReplaceItemList && recordReplaceItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : recordReplaceItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue(billRecItem.getOrderCode());
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}
						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue(billRecItem.getStorageNo());
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}
		//换货导出
		if(null != replaceMap && replaceMap.size()>0) {
			//Map<String,Object> dhMap = (Map)daohuoMap.get("dhMap");
			//String waybillNo = dhMap.get("waybillNo").toString();
			Collection values = replaceMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> replaceMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息
				List<SellerBillReconciliationItem> daohuoItemList = (List<SellerBillReconciliationItem>) replaceMaps.get("itemList");
				if (null != daohuoItemList && daohuoItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : daohuoItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}

				}
			}
		}
		//退货导出
		if(null != customerMap && customerMap.size()>0) {
			Collection values = customerMap.values();    //获取Map集合的value集合
			for (Object object : values) {
				Map<String,Object> customerMaps = (Map<String, Object>) object;

				//因为第0行已经设置标题了,所以从行2开始写入数据
				//第一行是订单信息

				List<SellerBillReconciliationItem> customerItemList = (List<SellerBillReconciliationItem>) customerMaps.get("itemList");
				if (null != customerItemList && customerItemList.size() > 0) {
					for (SellerBillReconciliationItem billRecItem : customerItemList) {
						row = sheet.createRow(rowCount);
						//6.采购订单号
						cell = row.createCell(0);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						//6.创建人
						cell = row.createCell(1);
						cell.setCellValue(billRecItem.getCreateBillName());
						cell.setCellStyle(normalStyle);
						//7.发货单号
						cell = row.createCell(2);
						cell.setCellValue(billRecItem.getDeliverNo());
						cell.setCellStyle(normalStyle);
						//1.货号
						cell = row.createCell(3);
						cell.setCellValue(billRecItem.getProductCode());
						cell.setCellStyle(normalStyle);
						//2.商品名称
						cell = row.createCell(4);
						cell.setCellValue(billRecItem.getProductName());
						cell.setCellStyle(normalStyle);
						//3.规格代码
						cell = row.createCell(5);
						cell.setCellValue(billRecItem.getSkuCode());
						cell.setCellStyle(normalStyle);
						//4.规格名称
						cell = row.createCell(6);
						cell.setCellValue(billRecItem.getSkuName());
						cell.setCellStyle(normalStyle);
						//5.条形码
						cell = row.createCell(7);
						cell.setCellValue(billRecItem.getBarcode());
						cell.setCellStyle(normalStyle);
						//11.采购单价
						cell = row.createCell(8);
						cell.setCellValue(fnum.format(billRecItem.getSalePrice()));
						cell.setCellStyle(normalStyle);
						//9.到货总数量
						cell = row.createCell(9);
						cell.setCellValue(billRecItem.getArrivalNum());
						cell.setCellStyle(normalStyle);
						//11.总金额
						cell = row.createCell(10);
						int arrivalNum = billRecItem.getArrivalNum();
						String isUpdateSale = billRecItem.getIsUpdateSale();
						BigDecimal salePrice = new BigDecimal(0);
						if ("1".equals(isUpdateSale)) {
							salePrice = billRecItem.getUpdateSalePrice();
						} else {
							salePrice = billRecItem.getSalePrice();
						}
						BigDecimal arrivalNumB = new BigDecimal(arrivalNum);
						BigDecimal salePriceSum = salePrice.multiply(arrivalNumB);
						cell.setCellValue(fnum.format(salePriceSum));
						cell.setCellStyle(normalStyle);

						//8.到货日期
						cell = row.createCell(11);
						if (null != billRecItem.getArrivalDate() && !"".equals(billRecItem.getArrivalDate())) {
							cell.setCellValue(sdf.format(billRecItem.getArrivalDate()));
						} else {
							cell.setCellValue("");
						}

						cell.setCellStyle(normalStyle);

						//10.是否开票
						cell = row.createCell(12);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);
						/*cell = row.createCell(9);
						String isNeedInvoice = billRecItem.getIsNeedInvoice();
						String isNeedInvoiceStr = "";
						if ("Y".equals(isNeedInvoice)) {
							isNeedInvoiceStr = "是";
						} else {
							isNeedInvoiceStr = "否";
						}
						cell.setCellValue(isNeedInvoiceStr);
						cell.setCellStyle(normalStyle);*/

						//11.对账单价
						cell = row.createCell(13);
						cell.setCellValue(fnum.format(billRecItem.getUpdateSalePrice()));
						cell.setCellStyle(normalStyle);
						//7.入库单号
						cell = row.createCell(14);
						cell.setCellValue("-");
						cell.setCellStyle(normalStyle);

						if ((rowCount - 1) % rowaccess == 0) {
							try {
								((SXSSFSheet)sheet).flushRows();
							}catch (Exception e){
								e.printStackTrace();
							}
						}
						rowCount++;
					}
					//因为第i++行已经设置内容,所以从行i++开始写入数据
					//第一行是订单信息

				}
			}
		}

		ExcelUtil.preExport("账单货品数据导出", response);
		ExcelUtil.export(wb, response);
	}
}
