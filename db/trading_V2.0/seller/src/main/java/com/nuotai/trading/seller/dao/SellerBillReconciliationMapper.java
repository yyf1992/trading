package com.nuotai.trading.seller.dao;

import com.nuotai.trading.dao.BaseDao;
import com.nuotai.trading.seller.model.SellerBillReconciliation;
import com.nuotai.trading.utils.SearchPageUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface SellerBillReconciliationMapper extends BaseDao<SellerBillReconciliation>{
    //查询账单周期列表
    List<SellerBillReconciliation> getRecToPamyment(SearchPageUtil searchPageUtil);

    List<SellerBillReconciliation> queryRecToPamymentList(Map<String,Object> map);

    //查询付款确认不同状态下的数据量
    int queryRecPaymentCount(Map<String,Object> map);

	//查询账单周期列表
	List<SellerBillReconciliation> getBillReconciliationList(SearchPageUtil searchPageUtil);

    //查询账单导出列表
    List<SellerBillReconciliation> selectByPrimaryKeyList(Map<String,Object> map);

	//查询不同审批状态的数量
	int queryBillStatusCount(Map<String, Object> map);

	//添加账单对账数据
	int saveBillReconciliationInfo(SellerBillReconciliation bSellerBillReconciliation);

	//修改账单周期信息
	//int updateSaveBillCycleInfo(Map<String, Object> updateSavemap);

	//根据id查询账单信息
	//Map<String, Object> getBillCycleInfo(Map<String, Object> map);

    int deleteByPrimaryKey(String id);

    int insert(SellerBillReconciliation record);

    int insertSelective(SellerBillReconciliation record);

    //添加账单附件
    int addBillFileInfo(Map<String,Object> addBillFileMap);
    //根据账单单号查询附件
    List<Map<String,Object>> queryBillFileList(String reconciliationId);

    //根据条件查询账单详细
    SellerBillReconciliation selectByPrimaryKey(Map<String, Object> map);

    //根据条件查询奖惩金额
    Map<String,Object> queryCustomInfo(Map<String,Object> map);

    //根据账单单号查询自定义付款附件
    List<Map<String,Object>> queryBillCustomList(String reconciliationId);
    //根据账单编号删除关联付款信息
    int deleteBillCustom(Map<String,Object> customMap);
    //根据对账单号修改最新自定义金额
    int updateBillRecCustom(Map<String,Object> customMap);
    //根据编号修改账单状态
    int updateByPrimaryKeySelective(SellerBillReconciliation record);

    int updateByPrimaryKey(SellerBillReconciliation record);

    //根据对账完成状态查询队长信息汇总到发票数据
    List<SellerBillReconciliation> queryRecByDealStatus(Map<String,Object> map);
}