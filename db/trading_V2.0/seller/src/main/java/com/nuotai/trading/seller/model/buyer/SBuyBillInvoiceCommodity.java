package com.nuotai.trading.seller.model.buyer;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * 
 * 
 * @author "
 * @date 2017-10-12 17:45:16
 */
public class SBuyBillInvoiceCommodity implements Serializable {
	//
	private String id;
	//关联发票id
	private String invoiceId;
	//商品批次
	private String reconciliationItemId;
	//商品交易类型（1 发货，2 退货）
	private String commodityType;
	//发票核对状态
	private String acceptInvoiceStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getAcceptInvoiceStatus() {
		return acceptInvoiceStatus;
	}

	public void setAcceptInvoiceStatus(String acceptInvoiceStatus) {
		this.acceptInvoiceStatus = acceptInvoiceStatus;
	}

	public String getReconciliationItemId() {
		return reconciliationItemId;
	}

	public void setReconciliationItemId(String reconciliationItemId) {
		this.reconciliationItemId = reconciliationItemId;
	}
}
